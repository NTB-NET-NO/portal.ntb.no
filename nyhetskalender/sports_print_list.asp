<%@ Language=VBScript %>

<!--#include file=func_lib.asp-->

<%
	if not CheckLogin(false) then
		'Response.Redirect("login.asp")
	end if

	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")

	valg = Request("valg")

	if valg = "doflags" then
		for i = 1 to Request.Form("count")

			dim sql
			sql = "UPDATE news_events SET "

			if Request.Form("flag-" & i) = 1 then
				sql = sql & "SportDoneFlag = 1 WHERE EventID = " & Request.Form("eventid-" & i)
			else
				sql = sql & "SportDoneFlag = 0 WHERE EventID = " & Request.Form("eventid-" & i)
			end if

			'response.write sql
			conn.execute(sql)

		next
	end if

	dim count
	count = 0

	category = 0
	area = 0
	tid = 7

	dim sql1
	sql1 = ""

	dim sql2
	sql2 = ""

	dim sql3
	sql3 = ""

	dim sql4
	sql4 = ""

	dim sql5
	sql5 = ""

	dim sql6
	sql6 = ""

	'Norsk fotball, dvs all annen fotball
	sql1 = "SELECT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE " & _
		  "(a.area & 4) > 0 AND ( b.category = 2048 AND b.subcategory = 1048576 ) AND " &_
		  "NOT ( title LIKE 'FOTBALL: Engelsk%' OR title LIKE 'FOTBALL: Skotsk%' OR title LIKE 'FOTBALL: Tysk%' OR " & _
		  "title LIKE 'FOTBALL: Italiensk%' OR title LIKE 'FOTBALL: Fransk%' OR title LIKE 'FOTBALL: Spansk%' OR  " & _
		  "title LIKE 'FOTBALL: Portugisisk%' OR title LIKE 'FOTBALL: Nederlandsk%' OR title LIKE 'FOTBALL: Belgisk%' OR " & _
		  "title LIKE 'FOTBALL: Gresk%' OR title LIKE 'FOTBALL: Tyrkisk%' OR title LIKE 'FOTBALL: Sveitsisk%' OR " & _
		  "title LIKE 'FOTBALL: �sterriksk%' OR title LIKE 'FOTBALL: Svensk%' OR title LIKE 'FOTBALL: Dansk%' OR " & _
		  "title LIKE 'FOTBALL: Finsk%' ) "

	'Alle idretter
	sql2 = "SELECT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE " & _
		  "(a.area & 4) > 0 AND ( b.category = 2048 AND b.subcategory NOT IN (1048576,16777216,576460752303423488) ) " &_
		  "AND NOT ( title LIKE '%NBA%' OR title LIKE '%NHL%' OR title LIKE '%NFL%' ) "

	'Engelsk fotball
	sql3 = "SELECT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE " & _
		  "(a.area & 4) > 0 AND ( b.category = 2048 AND b.subcategory = 1048576 ) AND " &_
		  "( title LIKE 'FOTBALL: Engelsk%' ) "

	'Spesifisert utenlandsk fotball
	sql4 = "SELECT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE " & _
		  "(a.area & 4) > 0 AND ( b.category = 2048 AND b.subcategory = 1048576 ) AND " &_
		  "( title LIKE 'FOTBALL: Skotsk%' OR title LIKE 'FOTBALL: Tysk%' OR " & _
		  "title LIKE 'FOTBALL: Italiensk%' OR title LIKE 'FOTBALL: Fransk%' OR title LIKE 'FOTBALL: Spansk%' OR  " & _
		  "title LIKE 'FOTBALL: Portugisisk%' OR title LIKE 'FOTBALL: Nederlandsk%' OR title LIKE 'FOTBALL: Belgisk%' OR " & _
		  "title LIKE 'FOTBALL: Gresk%' OR title LIKE 'FOTBALL: Tyrkisk%' OR title LIKE 'FOTBALL: Sveitsisk%' OR " & _
		  "title LIKE 'FOTBALL: �sterriksk%' OR title LIKE 'FOTBALL: Svensk%' OR title LIKE 'FOTBALL: Dansk%' OR " & _
		  "title LIKE 'FOTBALL: Finsk%' ) "

	'Trav
	sql5 = "SELECT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE " & _
		  "(a.area & 4) > 0 AND ( b.category = 2048 AND b.subcategory IN (16777216,576460752303423488) ) "

	'Amerikanske proffidretter
	sql6 = "SELECT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE " & _
		  "(a.area & 4) > 0 AND ( b.category = 2048 AND b.subcategory IN (4,64,2147483648) ) " &_
		  "AND ( title LIKE '%NBA%' OR title LIKE '%NHL%' OR title LIKE '%NFL%' ) "

	'Add time specification
	if CInt(Request("time")) > 0 then
		tid = CInt(Request("time"))
	end if

	if tid > 0 then

		dim fra
		dim til

		fra = DateSerial(Year(Date()),Month(Date()),Day(Date()))
		til = DateAdd("d",tid+1,fra)

		'sql1 = sql1 & "AND a.starttime >= '" & fra & "' AND a.starttime < '" & til & "' "
		'sql2 = sql2 & "AND a.starttime >= '" & fra & "' AND a.starttime < '" & til & "' "
		'sql3 = sql3 & "AND a.starttime >= '" & fra & "' AND a.starttime < '" & til & "' "
		'sql4 = sql4 & "AND a.starttime >= '" & fra & "' AND a.starttime < '" & til & "' "
		'sql5 = sql5 & "AND a.starttime >= '" & fra & "' AND a.starttime < '" & til & "' "
		'sql6 = sql6 & "AND a.starttime >= '" & fra & "' AND a.starttime < '" & til & "' "

		sql1 = sql1 & "AND a.endtime >= '" & fra & "' AND a.starttime <= '" & til & "' "
		sql2 = sql2 & "AND a.endtime >= '" & fra & "' AND a.starttime <= '" & til & "' "
		sql3 = sql3 & "AND a.endtime >= '" & fra & "' AND a.starttime <= '" & til & "' "
		sql4 = sql4 & "AND a.endtime >= '" & fra & "' AND a.starttime <= '" & til & "' "
		sql5 = sql5 & "AND a.endtime >= '" & fra & "' AND a.starttime <= '" & til & "' "
		sql6 = sql6 & "AND a.endtime >= '" & fra & "' AND a.starttime <= '" & til & "' "

	elseif tid = -1 then
		'Use specific dates
		dim blocks

		if Request.Form("from_date") <> "" then
			blocks = split(Request.Form("from_date"),".")
			sql1 = sql1 & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			sql2 = sql2 & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			sql3 = sql3 & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			sql4 = sql4 & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			sql5 = sql5 & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			sql6 = sql6 & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
		end if

		if Request.Form("to_date") <> "" then
			blocks = split(Request.Form("to_date"),".")
			sql1 = sql1 & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			sql2 = sql2 & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			sql3 = sql3 & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			sql4 = sql4 & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			sql5 = sql5 & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			sql6 = sql6 & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
		end if
	end if

	sql1 = sql1 & "ORDER BY CONVERT(varchar,a.starttime,102), a.title ASC"
	sql2 = sql2 & "ORDER BY CONVERT(varchar,a.starttime,102), a.title ASC"
	sql3 = sql3 & "ORDER BY CONVERT(varchar,a.starttime,102), a.title ASC"
	sql4 = sql4 & "ORDER BY CONVERT(varchar,a.starttime,102), a.title ASC"
	sql5 = sql5 & "ORDER BY CONVERT(varchar,a.starttime,102), a.title ASC"
	sql6 = sql6 & "ORDER BY CONVERT(varchar,a.starttime,102), a.title ASC"

	Set rs1 = Server.CreateObject("ADODB.Recordset")
	Set rs2 = Server.CreateObject("ADODB.Recordset")
	Set rs3 = Server.CreateObject("ADODB.Recordset")
	Set rs4 = Server.CreateObject("ADODB.Recordset")
	Set rs5 = Server.CreateObject("ADODB.Recordset")
	Set rs6 = Server.CreateObject("ADODB.Recordset")

	rs1.CursorLocation = adUseClient
	rs2.CursorLocation = adUseClient
	rs3.CursorLocation = adUseClient
	rs4.CursorLocation = adUseClient
	rs5.CursorLocation = adUseClient
	rs6.CursorLocation = adUseClient

	rs1.Open sql1,conn,adOpenDynamic,adLockOptimistic,adCmdText
	rs2.Open sql2,conn,adOpenDynamic,adLockOptimistic,adCmdText
	rs3.Open sql3,conn,adOpenDynamic,adLockOptimistic,adCmdText
	rs4.Open sql4,conn,adOpenDynamic,adLockOptimistic,adCmdText
	rs5.Open sql5,conn,adOpenDynamic,adLockOptimistic,adCmdText
	rs6.Open sql6,conn,adOpenDynamic,adLockOptimistic,adCmdText

%>
<HTML>
<HEAD>

<LINK rel="stylesheet" type="text/css" href="nyhetskalender.css">

<SCRIPT LANGUAGE=javascript>
<!--

//Delete event from the calendar
function deleteEvent(id)
{
	if ( window.confirm("Den valgte hendelsen vil bli slettet.\nKlikk 'OK' for � fortsette eller 'Avbryt' for � avbryte") ) {
		location.href = 'admin/delete.asp?valg=slett&eventid=' + id
	}
}

//-->
</script>

</HEAD>
<BODY class=print>
<!--
<p><%=sql1%></p>
<p><%=sql2%></p>
<p><%=sql3%></p>
<p><%=sql4%></p>
<p><%=sql5%></p>
<p><%=sql6%></p>
-->
<form name=sport_edit method=post>
<table width=900 cellpadding=0 cellspacing=0 class=eventNews>

	<%
	if rs1.EOF and rs2.EOF and rs3.EOF and rs4.eof and rs5.eof and rs6.eof then
		'No hits
	%>
		<tr><td class=eventNews>Ingen hendelser funnet.</td></tr>
	<%
	else
		%>
		<tr><td class=eventHeadline>

		<% if valg = "export" then  %>
			NTB-sportens uke-PM
		<% else  %>
			NTB-sportens uke-PM
		<% end if %>

		<% if valg <> "export" then Response.Write("| <a href='?valg=export' target='_blank' class='redlink'>Eksportliste</a> | <a href='?valg=export&time=31' target='_blank' class='redlink'>Eksportliste (31)</a> | <a href='javascript:document.sport_edit.submit()' class='redlink'>Oppdatere jobber</a>") %>
		</td></tr>
		<%

		dim ld
		dim cd
		ld = Dateserial(2004,1,1)

		do while not (rs1.eof and rs2.eof and rs3.eof and rs4.eof and rs5.eof and rs6.eof )

			if not rs1.eof then
				cd = CDate(Split(rs1("startTime"))(0))
			end if

			if not rs2.eof then
				if cd > CDate(Split(rs2("startTime"))(0)) or rs1.eof then
					cd = CDate(Split(rs2("startTime"))(0))
				end if
			end if

			if not rs3.eof then
				if cd > CDate(Split(rs3("startTime"))(0)) or ( rs1.eof and rs1.eof ) then
					cd = CDate(Split(rs3("startTime"))(0))
				end if
			end if

			if not rs4.eof then
				if cd > CDate(Split(rs4("startTime"))(0)) or ( rs1.eof and rs1.eof and rs3.eof) then
					cd = CDate(Split(rs4("startTime"))(0))
				end if
			end if

			if not rs5.eof then
				if cd > CDate(Split(rs5("startTime"))(0))  or ( rs1.eof and rs1.eof and rs3.eof and rs4.eof) then
					cd = CDate(Split(rs5("startTime"))(0))
				end if
			end if

			if not rs6.eof then
				if cd > CDate(Split(rs6("startTime"))(0))  or ( rs1.eof and rs1.eof and rs3.eof and rs4.eof and rs5.eof) then
					cd = CDate(Split(rs6("startTime"))(0))
				end if
			end if

			'if cd < Now then cd = Now

			'response.write(cd)
			'response.flush()

			if cd > ld then

				if valg = "export" then
					Response.Write("<tr><td class=eventNews>&nbsp;</td></tr><tr><td class=eventHeadline>[ing]" & GetWeekday(cd) & " " & LongDisplayDate(cd) & "</td></tr>")
				else
					Response.Write("<tr><td class=eventNews>&nbsp;</td></tr><tr><td class=eventHeadline>" & GetWeekday(cd) & " " & LongDisplayDate(cd) & "</td></tr>")
				end if
			end if


			do while not rs1.eof
				if cd < CDate(Split(rs1("startTime"))(0)) then
					exit do
				end if

				count = count + 1

				if rs1("SportDoneflag") and valg <> "export" then
					Response.Write("<tr><td class=eventNewsGrey>")
				elseif valg = "export" then
					Response.Write("<tr><td class=eventNews>[txt]")
				else
					Response.Write("<tr><td class=eventNews>")
				end if

				if valg <> "export" then
					Response.Write("<input type=hidden name=eventid-" & count & " value=" & rs1("eventid") & ">")
					Response.Write("<input type=checkbox name=flag-" & count & " value=1")
					if rs1("SportDoneflag") then Response.Write(" CHECKED")
					Response.Write("> | ")
					Response.Write("<a href=""admin/add_event.asp?valg=edit&eventid=" & rs1("EventID") & """ class=redlink>Rediger</a> | <a href=""javascript:deleteEvent('" & rs1("eventid") & "')"" class=redlink>Slett</a> | ")
				end if

				Response.Write(rs1("title") & "&nbsp;" & rs1("description") & "</td></tr>")
				rs1.MoveNext()
			loop


			do while not rs2.eof
				if cd < CDate(Split(rs2("startTime"))(0)) then
					exit do
				end if

				count = count + 1

				if rs2("SportDoneflag") and valg <> "export" then
					Response.Write("<tr><td class=eventNewsGrey>")
				elseif valg = "export" then
					Response.Write("<tr><td class=eventNews>[txt]")
				else
					Response.Write("<tr><td class=eventNews>")
				end if

				if valg <> "export" then
					Response.Write("<input type=hidden name=eventid-" & count & " value=" & rs2("eventid") & ">")
					Response.Write("<input type=checkbox name=flag-" & count & " value=1")
					if rs2("SportDoneflag") then Response.Write(" CHECKED")
					Response.Write("> | ")
					Response.Write("<a href=""admin/add_event.asp?valg=edit&eventid=" & rs2("EventID") & """ class=redlink>Rediger</a> | <a href=""javascript:deleteEvent('" & rs2("eventid") & "')"" class=redlink>Slett</a> | ")
				end if

				Response.Write(rs2("title") & "&nbsp;" & rs2("description") & "</td></tr>")
				rs2.MoveNext()
			loop


			do while not rs3.eof
				if cd < CDate(Split(rs3("startTime"))(0)) then
					exit do
				end if

				count = count + 1

				if rs3("SportDoneflag") and valg <> "export" then
					Response.Write("<tr><td class=eventNewsGrey>")
				elseif valg = "export" then
					Response.Write("<tr><td class=eventNews>[txt]")
				else
					Response.Write("<tr><td class=eventNews>")
				end if

				if valg <> "export" then
					Response.Write("<input type=hidden name=eventid-" & count & " value=" & rs3("eventid") & ">")
					Response.Write("<input type=checkbox name=flag-" & count & " value=1")
					if rs3("SportDoneflag") then Response.Write(" CHECKED")
					Response.Write("> | ")
					Response.Write("<a href=""admin/add_event.asp?valg=edit&eventid=" & rs3("EventID") & """ class=redlink>Rediger</a> | <a href=""javascript:deleteEvent('" & rs3("eventid") & "')"" class=redlink>Slett</a> | ")
				end if

				Response.Write(rs3("title") & "&nbsp;" & rs3("description") & "</td></tr>")
				rs3.MoveNext()
			loop

			do while not rs4.eof
				if cd < CDate(Split(rs4("startTime"))(0)) then
					exit do
				end if

				count = count + 1

				if rs4("SportDoneflag") and valg <> "export" then
					Response.Write("<tr><td class=eventNewsGrey>")
				elseif valg = "export" then
					Response.Write("<tr><td class=eventNews>[txt]")
				else
					Response.Write("<tr><td class=eventNews>")
				end if

				if valg <> "export" then
					Response.Write("<input type=hidden name=eventid-" & count & " value=" & rs4("eventid") & ">")
					Response.Write("<input type=checkbox name=flag-" & count & " value=1")
					if rs4("SportDoneflag") then Response.Write(" CHECKED")
					Response.Write("> | ")
					Response.Write("<a href=""admin/add_event.asp?valg=edit&eventid=" & rs4("EventID") & """ class=redlink>Rediger</a> | <a href=""javascript:deleteEvent('" & rs4("eventid") & "')"" class=redlink>Slett</a> | ")
				end if

				Response.Write(rs4("title") & "&nbsp;" & rs4("description") & "</td></tr>")
				rs4.MoveNext()
			loop

			do while not rs5.eof
				if cd < CDate(Split(rs5("startTime"))(0)) then
					exit do
				end if

				count = count + 1

				if rs5("SportDoneflag") and valg <> "export" then
					Response.Write("<tr><td class=eventNewsGrey>")
				elseif valg = "export" then
					Response.Write("<tr><td class=eventNews>[txt]")
				else
					Response.Write("<tr><td class=eventNews>")
				end if

				if valg <> "export" then
					Response.Write("<input type=hidden name=eventid-" & count & " value=" & rs5("eventid") & ">")
					Response.Write("<input type=checkbox name=flag-" & count & " value=1")
					if rs5("SportDoneflag") then Response.Write(" CHECKED")
					Response.Write("> | ")
					Response.Write("<a href=""admin/add_event.asp?valg=edit&eventid=" & rs5("EventID") & """ class=redlink>Rediger</a> | <a href=""javascript:deleteEvent('" & rs5("eventid") & "')"" class=redlink>Slett</a> | ")
				end if

				Response.Write(rs5("title") & "&nbsp;" & rs5("description") & "</td></tr>")
				rs5.MoveNext()
			loop

			do while not rs6.eof
				if cd < CDate(Split(rs6("startTime"))(0)) then
					exit do
				end if

				count = count + 1

				if rs6("SportDoneflag") and valg <> "export" then
					Response.Write("<tr><td class=eventNewsGrey>")
				elseif valg = "export" then
					Response.Write("<tr><td class=eventNews>[txt]")
				else
					Response.Write("<tr><td class=eventNews>")
				end if

				if valg <> "export" then
					Response.Write("<input type=hidden name=eventid-" & count & " value=" & rs6("eventid") & ">")
					Response.Write("<input type=checkbox name=flag-" & count & " value=1")
					if rs6("SportDoneflag") then Response.Write(" CHECKED")
					Response.Write("> | ")
					Response.Write("<a href=""admin/add_event.asp?valg=edit&eventid=" & rs6("EventID") & """ class=redlink>Rediger</a> | <a href=""javascript:deleteEvent('" & rs6("eventid") & "')"" class=redlink>Slett</a> | ")
				end if

				Response.Write(rs6("title") & "&nbsp;" & rs6("description") & "</td></tr>")
				rs6.MoveNext()
			loop

			ld = cd
		loop
		%>

	<%
	end if
	%>

</table>

<input type=hidden name=count value=<%=count%>>
<input type=hidden name=valg value=doflags>
</form>

<%
rs1.Close
rs2.Close
rs3.Close
rs4.Close
rs5.Close
rs6.Close

conn.close
%>
</BODY>
</HTML>
