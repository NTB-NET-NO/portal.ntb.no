<%@ Language=VBScript %>
<%Option Explicit%>

<!--#include file=../func_lib.asp-->

<%
	if not CheckLogin(true) then
		Response.write("Du har ikke tilgang til denne siden.")
		Response.End()
		'Redirect("../login.asp")
	end if

	Dim conn
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")

	dim valg
	dim msg

	dim username
	dim password
	dim name
	dim admin

	valg = Request("valg")

	if valg = "lagre" then

		username = Request.Form("username")
		password = Request.Form("password")
		name = Request.Form("name")
		admin = Request.Form("admin")

		if admin = "" then admin = 0

		'Sjekke om brukeren finnes fra f�r
		dim rsCheck
		Set rsCheck = Server.CreateObject("ADODB.RecordSet")
		rsCheck.CursorLocation = adUseClient
		rsCheck.Open "SELECT * FROM Users WHERE UserNAme = '" & username & "'",conn,adOpenDynamic,adLockOptimistic,adCmdText

		if rsCheck.RecordCount > 0 then
			msg = "<p class=alert_message>Brukernavnet finnes fra f�r!</p>"
		else

			'Legge inn bruker
			dim rsInsert
			Set rsInsert = Server.CreateObject("ADODB.RecordSet")
			rsInsert.CursorLocation = adUseClient
			rsInsert.Open "Users",conn,adOpenDynamic,adLockOptimistic,adCmdTableDirect

			rsInsert.AddNew()
			rsInsert("UserName") = username
			rsInsert("Password") = password
			rsInsert("FullName") = name
			rsInsert("Admin") = admin
			rsInsert("AddedBy") = Session("fullname")
			rsInsert("Created") = CDate(DateSerial(Year(Date()),Month(Date()),Day(Date())) & " " & Hour(Now()) & ":" & Minute(Now()))
			rsInsert.Update()
			rsInsert.Close()

			msg = "<p class=formText>Brukeren er lagret.</p>"
		end if

		rsCheck.Close
	end if

%>

<HTML>
<HEAD>
<LINK rel="stylesheet" type="text/css" href="../nyhetskalender.css">

<SCRIPT LANGUAGE=javascript>
<!--

//function to validate the data prior to submitting the form
function onSubmit_ValidateForm(activeform) {

	//Verify data
	if ( activeform.username.value == "" ) {
		alert("Du m� angi et brukernavn.");
		activeform.username.focus();
		return false;
	}

	if ( activeform.password.value == "" ) {
		alert("Du m� legge inn et passord.");
		activeform.password.focus();
		return false;
	}

	if ( activeform.name.value == "" ) {
		alert("Du m� legge inn fullt navn p� brukeren.");
		activeform.name.focus();
		return false;
	}
}

//-->
</SCRIPT>
</HEAD>
<BODY>
<div class=eventIngress>Ny bruker i Nyhetskalenderen</div>

<% if msg <> "" then %>
	<%=msg%>
<% end if %>

<P class=formText>
Fyll ut informasjon om brukeren som skal ha rettigheter til nyhetskalenderen, og klikk 'Legg til bruker'.
</p>


<form name=user_form action=add_user.asp method=post onSubmit="return onSubmit_ValidateForm(this)">
<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=200 style="border:none;">Brukernavn:</td>
	<td width=400 style="border:none;"><Input type=text name=username value='<%=username%>' maxlength=200 style="width:300px;"></td>
</tr>
<tr>
	<td width=200 style="border:none;">Fullt navn:</td>
	<td width=400 style="border:none;"><Input type=text name=name value='<%=password%>' maxlength=200 style="width:300px;"></td>
</tr>
<tr>
	<td width=200 style="border:none;">Passord:</td>
	<td width=400 style="border:none;"><Input type=text name=password value='<%=name%>' maxlength=200 style="width:300px;"></td>
</tr>
<tr>
	<td width=200 style="border:none;">Administrator?</td>
	<td width=400 style="border:none;"><Input type=checkbox name=admin value=1 <% if admin then Response.Write("CHECKED") %>></td>
</tr>
</table>

<input type=hidden name=valg value=lagre>
<br>
<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td align=right style="border:none;"><input type=submit class=button value="Legg til bruker"></td>
</tr>
</table>

</form>

<%
conn.Close()
%>

</BODY>
</HTML>
