<%@ Language=VBScript %>

<!--#include file=../func_lib.asp-->

<%
	if not CheckLogin(true) then
		Response.write("Du har ikke tilgang til denne siden.")
		Response.End()
		'Redirect("../login.asp")
	end if


	Dim conn
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")

	id = Request("EventID")
	valg = Request("valg")

	'Enable the saved search
	Session("load_saved") = true

	if IsNumeric(id) and valg = "publish" then

		sql = "UPDATE news_events SET Published = 1 WHERE EventId = " & id
		conn.Execute(sql)

		msg = "Hendelsen er publisert."

	elseif IsNumeric(id) and valg = "unpublish" then

		sql = "UPDATE news_events SET Published = 0 WHERE EventId = " & id
		conn.Execute(sql)

		msg = "Publisering er fjernet."

	else
		msg = "Hendelsen ble ikke funnet."
	end if

	conn.Close()
%>

<HTML>

<HEAD>
<LINK rel="stylesheet" type="text/css" href="../nyhetskalender.css">
<META HTTP-EQUIV=Refresh CONTENT='1; URL=../search_event.asp'>
</HEAD>

<BODY>

	<div class=eventIngress>Publisering av hendelse</div>
	<P class=alert_message>
	<%=msg%>
	</p>

</BODY>
</HTML>
