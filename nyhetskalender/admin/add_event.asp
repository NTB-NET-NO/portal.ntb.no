<%@ Language=VBScript %>
<%Option Explicit%>

<!--#include file=../func_lib.asp-->

<%
	if not CheckLogin(False) then
		Session("fullname") = "Gjestebruker"
	end if

	if Instr(Request.ServerVariables("HTTP_REFERER"),"sports_print_list.asp") > 0 then
		Session("editref") = "sport"
	end if

	Dim conn
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")


	dim valg
	dim msg
	dim refresh
	dim nmbr


	dim id
	dim addedby
	dim title
	dim description
	dim fromtime
	dim totime
	dim area
	dim category
	dim subcategory
	dim subarray
	dim subref
	dim address
	dim county
	dim contact
	dim email
	dim phone
	dim url
	dim pub

	subarray = Array()

	id = Request("EventID")
	valg = Request("valg")
	nmbr = Request("nmbr")

	if valg = "edit" and IsNumeric(id) then

		'First of all, enble the saved search
		Session("load_saved") = true

		'Load the data
		dim rsRetrieve
		Set rsRetrieve = Server.CreateObject("ADODB.RecordSet")
		rsRetrieve.CursorLocation = adUseClient
		rsRetrieve.Open "SELECT * FROM news_events WHERE EventID = " & id,conn,adOpenDynamic,adLockOptimistic,adCmdText

		if not rsRetrieve.EOF then
			title = rsRetrieve("title")
			description = rsRetrieve("description")

			'Times
			fromTime = Timestamp(rsRetrieve("startTime"))
			totime = Timestamp(rsRetrieve("endtime"))

			area = rsRetrieve("area")
			category = rsRetrieve("category")
			subcategory = rsRetrieve("subcategory")
			subref = rsRetrieve("hassubref")

			address = rsRetrieve("address")
			county = rsRetrieve("fylke")
			contact = rsRetrieve("contact")
			email = rsRetrieve("contactemail")
			phone = rsRetrieve("contactphone")
			url = rsRetrieve("url")
			pub = CBool(rsRetrieve("Published"))
		else
			valg = ""
			id = ""
		end if
		rsRetrieve.Close()

		'Load subcats
		if subref = -1 then
			rsRetrieve.Open "SELECT * FROM event_subrel WHERE EventID = " & id,conn,adOpenDynamic,adLockOptimistic,adCmdText

			Redim subarray(rsRetrieve.RecordCount)

			dim c
			for c = 1 to rsRetrieve.RecordCount
				subarray(c) = rsRetrieve("Category") & "_" & rsRetrieve("SubCategory")
				rsRetrieve.MoveNext()
			next

			rsRetrieve.Close()
		end if

	elseif valg = "lagre" then

		'Tekst
		addedby = Request.Form("addedby")
		title = Request.Form("title")
		description = Request.Form("description")

		if title = "" or description = "" then
			msg = "Det oppstod en feil ved lagring av hendelse i nyhetskalenderen.<P>Legg inn b�de tittel og beskrivelse og fors�k igjen."
		end if

		'Tid
		fromTime = Request.Form("from_date")
		toTime = Request.Form("to_date")
		if toTime = "" then toTime = fromTime

		dim blocks
		blocks = split(fromtime,".")
		fromtime = DateSerial(blocks(2),blocks(1),blocks(0))
		fromtime = fromtime & " " & Request.Form("from_time")

		blocks = split(totime,".")
		totime = DateSerial(blocks(2),blocks(1),blocks(0))
		totime = totime & " " & Request.Form("to_time")

		if not ( IsDate(fromtime) and IsDate(toTime) ) then
			msg = "Det oppstod en feil ved lagring av hendelse i nyhetskalenderen.<P>Sjekk det angitte tidsrom for hendelsen og fors�k igjen."
		end if

		'Omr�de
		area = 0
		dim count
		For count = 1 to Request.Form("area").Count
			area = area or Request.Form("area")(count)
		Next
		if not IsNumeric(area) or area = 0 then
			msg = "Det oppstod en feil ved lagring av hendelse i nyhetskalenderen.<P>Sjekk det angitte omr�det og pr�v igjen."
		end if

		'County (fylke)
		county = 0
		For count = 1 to Request.Form("county").Count
			county = county or Request.Form("county")(count)
		Next

		'Kategorier
		category = 0
		For count = 1 to Request.Form("category").Count
			category = category or Request.Form("category")(count)
		Next

		'Underkategorier
		subcategory = -1
		if Request.Form("subcategory").Count > 0 then
			subref = -1
		else
			subref = 0
		end if

		'Diverse annet
		address = Request.Form("address")
		contact = Request.Form("contact")
		email = Request.Form("email")
		phone = Request.Form("phone")
		url = Request.Form("url")

		if address = "" then address = null
		if contact = "" then contact = null
		if email = "" then email = null
		if phone = "" then phone = null
		if url = "" then url = null

		if id = "" then
			'Legge inn data
			dim rsInsert
			Set rsInsert = Server.CreateObject("ADODB.RecordSet")
			rsInsert.CursorLocation = adUseClient
			rsInsert.Open "news_events",conn,adOpenDynamic,adLockOptimistic,adCmdTableDirect

			rsInsert.AddNew()
			rsInsert("title") = title
			rsInsert("description") = description
			rsInsert("starttime") = CDate(fromtime)
			rsInsert("endtime") = CDate(totime)
			rsInsert("area") = area
			rsInsert("category") = category
			rsInsert("subcategory") = subcategory
			rsInsert("hasSubRef") = subref
			rsInsert("address") = address
			rsInsert("fylke") = county
			rsInsert("contact") = contact
			rsInsert("contactemail") = email
			rsInsert("contactphone") = phone
			rsInsert("url") = url
			rsInsert("AddedBy") = Session("fullname")

			if (rsInsert("AddedBy") = "Gjestebruker" or rsInsert("AddedBy") = "Portalbruker" or rsInsert("AddedBy") = "PRMbruker") and addedby <> "" then
				rsInsert("AddedBy") = addedby
			end if

			rsInsert("Modified") = CDate(DateSerial(Year(Date()),Month(Date()),Day(Date())) & " " & Hour(Now()) & ":" & Minute(Now()))
			rsInsert("Created") = CDate(DateSerial(Year(Date()),Month(Date()),Day(Date())) & " " & Hour(Now()) & ":" & Minute(Now()))

			if Request.Form("pub") and Session("admin") then
				rsInsert("Published") = 1
			else
				rsInsert("Published") = 0
			end if

			rsInsert.Update()

			id = rsInsert("EventID")
			rsInsert.Close

			'Refresh to here / Message
			refresh = "<META HTTP-EQUIV=Refresh CONTENT='1; URL=add_event.asp'>"
			msg = "Hendelsen er lagret i nyhetskalenderen."
		else
			'Oppdatere eksisterende hendelse
			dim rsUpdate
			Set rsUpdate = Server.CreateObject("ADODB.RecordSet")
			rsUpdate.CursorLocation = adUseClient
			rsUpdate.Open "SELECT * FROM news_events WHERE EventID = " & id,conn,adOpenDynamic,adLockOptimistic,adCmdText

			rsUpdate("title") = title
			rsUpdate("description") = description
			rsUpdate("starttime") = CDate(fromtime)
			rsUpdate("endtime") = CDate(totime)
			rsUpdate("area") = area
			rsUpdate("category") = category
			rsUpdate("subcategory") = subcategory
			rsUpdate("hasSubRef") = subref
			rsUpdate("address") = address
			rsUpdate("fylke") = county
			rsUpdate("contact") = contact
			rsUpdate("contactemail") = email
			rsUpdate("contactphone") = phone
			rsUpdate("url") = url
			rsUpdate("Modified") = CDate(DateSerial(Year(Date()),Month(Date()),Day(Date())) & " " & Hour(Now()) & ":" & Minute(Now()))

			if Request.Form("pub") and Session("admin") then
				rsUpdate("Published") = 1
			else
				rsUpdate("Published") = 0
			end if

			rsUpdate.Update()

			id = rsUpdate("EventID")
			rsUpdate.Close()

			'Delete subcats
			conn.Execute "DELETE FROM event_subrel WHERE EventID = " & id

			'Refresh to here / Message
			refresh = "<META HTTP-EQUIV=Refresh CONTENT='1; URL=../search_event.asp#item-" & nmbr & "'>"
			msg = "Hendelsen er oppdatert."
		end if

		'Legge inn subcats
		For count = 1 to Request.Form("subcategory").Count

			dim tmp
			tmp = Split( Request.Form("subcategory")(count), "_" )

			dim sql
			sql = "INSERT INTO event_subrel (EventID,Category,SubCategory) VALUES " & _
				  "(" & id & "," & tmp(0) & "," & tmp(1) & ")"

			conn.Execute (sql)

		Next

	end if


	'Check referer for sports
	if Session("editref") = "sport" and refresh <> "" then
		refresh = "<META HTTP-EQUIV=Refresh CONTENT='1; URL=../sports_print_list.asp'>"
		Session("editref") = ""
	end if


%>

<HTML>
<HEAD>
<LINK rel="stylesheet" type="text/css" href="../nyhetskalender.css">
<%=refresh%>

<SCRIPT LANGUAGE=javascript>
<!--

// Arrays containing subcategories
<%
	dim rsCategory
	dim rsSubCategory
	Set rsCategory = Server.CreateObject("ADODB.Recordset")
	Set rsSubCategory = Server.CreateObject("ADODB.Recordset")

	Response.Write("var subcat = new Array(" & vbCrLf)

	rsCategory.Open "Select * from BITNAMES where typeofname = 3 order by DescriptiveName asc", conn

	do while not rsCategory.eof

		rsSubCategory.Open "Select * from bitnames where typeofname = 4 and category = " & rsCategory("category") & " order by DescriptiveName asc", conn

		Response.Write(vbTab & "new Array(")

		do while not rsSubCategory.eof

			Response.Write("'" & rsCategory("category") & "_" & rsSubCategory("subcategory") & "','" & rsSubCategory("DescriptiveName") & "'")

			rsSubCategory.MoveNext()

			if not rsSubCategory.eof then
				Response.Write(",")
			end if
		loop

		rsSubCategory.Close()

		rsCategory.MoveNext()
		if not rsCategory.eof then
			Response.Write(")," & vbCrLf)
		else
			Response.Write(")" & vbCrLf)
		end if

	loop

	Response.Write(");")

	rsCategory.Close()

%>

//function to validate the data prior to submitting the form
function onSubmit_ValidateForm(activeform) {

	//Verify data

	<% if valg <> "edit" and ( Session("fullname") = "Gjestebruker" or Session("fullname") = "Portalbruker" or Session("fullname") = "PRMbruker" ) then %>

		if ( activeform.addedby.value == "" ) {
			alert("Du m� oppgi hvem som har lagt til hendelsen");
			activeform.addedby.focus();
			return false;
		}

	<% end if%>

	if ( activeform.title.value == "" ) {
		alert("Du m� angi en tittel p� hendelsen");
		activeform.title.focus();
		return false;
	}

	if ( activeform.description.value == "" ) {
		alert("Du m� legge inn en beskrivelse av hendelsen.");
		activeform.description.focus();
		return false;
	}

	if ( activeform.area.selectedIndex < 1 ) {
		alert("Du m� velge nyhetsomr�de for hendelsen.");
		activeform.area.focus();
		return false;
	}

	//Verify dates
	rxDate = /^\d{2}\.\d{2}\.\d{4}$/;
	rxTime = /^\d{2}:\d{2}$/;

	fd = activeform.from_date.value;
	td = activeform.to_date.value;

	ft = activeform.from_time.value;
	tt = activeform.to_time.value;

	//Check dates
	if ( fd == "") {
		alert("Du m� angi tidsrom for hendelsen.");
		activeform.from_date.focus();
		return false;
	}
	else if ( td == "") {
		alert("Du m� angi tidsrom for hendelsen.");
		activeform.to_date.focus();
		return false;
	}
	else if ( ! rxDate.test(fd) ) {
		alert("Du har angitt en ugyldig 'Fra'-dato.");
		activeform.from_date.focus();
		return false;
	}
	else if ( ! rxDate.test(td) ) {
		alert("Du har angitt en ugyldig 'Til'-dato.");
		activeform.to_date.focus();
		return false;
	}
	else if ( ! rxTime.test(ft) && ft != "" ) {
		alert("Du har angitt et ugyldig 'Fra'-klokkeslett.");
		activeform.from_time.focus();
		return false;
	}
	else if ( ! rxTime.test(tt) && tt != "" ) {
		alert("Du har angitt et ugyldig 'Til'-klokkeslett.");
		activeform.to_time.focus();
		return false;
	}

	else {

		var fa = new Array(3);
		var ta = new Array(3);
		var dt = new Date();

		//Verify date values
		fa = fd.split(".");
		if ( fa[0] > 31 || fa[1] > 12 || fa[0] < 1 || fa[1] < 1 ) {
			alert("Du har angitt en ugyldig 'Fra'-dato.");
			activeform.from_date.focus();
			return false;
		}

		ta = td.split(".");
		if ( ta[0] > 31 || ta[1] > 12 || ta[0] < 1 || ta[1] < 1 ) {
			alert("Du har angitt en ugyldig 'Til'-dato.");
			activeform.to_date.focus();
			return false;
		}

		if ( fa[2]+fa[1]+fa[0] > ta[2]+ta[1]+ta[0] ) {
			alert("Du har angitt et ugyldig tidsintervall. 'Fra'-dato m� v�re mindre enn 'Til'-dato!");
			activeform.to_date.focus();
			return false;
		}

		//Verify time values
		if ( ft != "" ) {
			fa = ft.split(":");
			if ( fa[0] > 23 || fa[1] > 59 || fa[0] < 0 || fa[1] < 0 ) {
				alert("Du har angitt et ugyldig 'Fra'-klokkeslett.");
				activeform.from_time.focus();
				return false;
			}
		}

		if ( tt != "" ) {
			ta = tt.split(":");
			if ( ta[0] > 23 || ta[1] > 59 || ta[0] < 0 || ta[1] < 0 ) {
				alert("Du har angitt et ugyldig 'Til'-klokkeslett.");
				activeform.to_time.focus();
				return false;
			}
		}

		if ( fd == td && ft != "" && tt != "" ) {
			if ( ft >= tt ) {
				alert("Du har angitt et ugyldig tidsinterval. 'Fra'-klokkeslett m� v�re mindre enn 'Til'-klokkeslett.");
				activeform.to_time.focus();
				return false;
			}
		}
	}

	return true;
}

function areaSelected(form)
{
	if ( form.elements.area.selectedIndex >= 0 ) {

		var mgOptionText = form.elements.area.options[form.elements.area.selectedIndex].text;
		var flag = false;
		var mcOptionText;

		for(i=0; i < form.elements.category.length; i++)
		{
			mcOptionText = form.elements.category.options[i].text
			if (mcOptionText == mgOptionText)
			{
				flag = true;
				form.elements.category.options[i].selected = true;
			} else {
				form.elements.category.options[i].selected = false;
			}
		}

		if ( ! flag ) //selected option in maingroup doesn't match any group in maincategory
		{
			form.elements.category.options[0].selected = true;
		}

		fillSubcategories(form)
	}
}

function fillSubcategories(form)
{
	form.subcategory.selectedIndex=-1;
	form.subcategory.options.length=0;

	locs = form.subcategory.options;

	for ( c = 1; c < form.category.options.length; c++ ) {

		if (form.category.options[c].selected) {

			for ( lc = 0; lc < subcat[c-1].length; lc += 2) {
				locs[locs.length] = new Option(subcat[c-1][lc+1],subcat[c-1][lc]);
			}
		}
	}
}

function autoFillDate(datefield) {

	var text = datefield.value;
	var d = new Date();
	var year;

	//Substitute separators
	rxSub = /[- \/]/g;
	text = text.replace(rxSub,".");

	//Test for missing zeros
	rxZ1 = /^(\d{1})$/;
	rxZ2 = /^(\d{1})(\..+)/;
	rxZ3 = /(.+\.)(\d{1})$/;
	rxZ4 = /(.+\.)(\d{1})(\..+)/;

	text = text.replace(rxZ1,"0$1");
	text = text.replace(rxZ2,"0$1$2");
	text = text.replace(rxZ3,"$10$2");
	text = text.replace(rxZ4,"$10$2$3");

	//Test for autofilling of dates
	rx1 = /^\d{2}$/;
	rx2 = /^\d{2}\.\d{2}$/;
	rx3 = /^(\d{2}\.\d{2}\.)(\d{2})$/;
	rx4 = /^\d{2}\.\d{2}\.\d{4}$/;

	if ( rx4.test(text) ) {
		//do nothing
	} else if ( rx3.exec(text) ) {
		year = RegExp.$2;
		if (year < 50) year = 20 + year;
		else if (year < 100) year = 19 + year;

		text = text.replace(rx3,"$1" + year);

	} else if ( rx2.test(text) ) {
		text += "." + d.getFullYear();
	} else if ( rx1.test(text) ) {
		var m = (d.getMonth()+1)
		if ( m < 10 ) m = "0" + m;
		text += "." + m + "." + d.getFullYear();
	}

	datefield.value = text;

	if ( datefield.name == 'from_date' )
		document.forms.event_form.elements.to_date.value = text;

}

function autoFillTime(timefield) {

	var text = timefield.value;
	var d = new Date();
	var year;

	//Substitute separators
	rxSub = /[-\. \/]/g;
	text = text.replace(rxSub,":");

	//Test for missing zeros
	rxZ1 = /^(\d{1})$/;
	rxZ2 = /^(\d{1})(:.+)/;
	rxZ3 = /(.+:)(\d{1})$/;

	text = text.replace(rxZ1,"0$1");
	text = text.replace(rxZ2,"0$1$2");
	text = text.replace(rxZ3,"$10$2");

	//Test for no ':' separator
	rxNosep = /^(\d{2})(\d{2})$/;

	//Test for autofilling of dates
	rx1 = /^\d{2}$/;
	rx2 = /^\d{2}:\d{2}$/;

	if ( rx2.test(text) ) {
		//do nothing
	} else if ( rx1.exec(text) ) {
		text += ":00";
	} else if ( rxNosep.exec(text) ) {
		text = RegExp.$1 + ":" + RegExp.$2;
	}

	timefield.value = text;

}

function loadValues() {

	<% if valg = "edit" Then %>
	document.event_form.title.value = '<%= Replace(title,"'","\'") %>'
	//Description is multiline, cant be loaded here. It is added directly to the form instead

	<%
	dim tm
	tm = Split(fromTime)
	Response.Write("document.event_form.from_date.value = '" & tm(0) & "';" & vbCrLf)
	if UBound(tm) > 0 then Response.Write("document.event_form.from_time.value = '" & Left(tm(1),5) & "';" & vbCrLf)

	tm = Split(toTime)
	Response.Write("document.event_form.to_date.value = '" & tm(0) & "';" & vbCrLf)
	if UBound(tm) > 0 then Response.Write("document.event_form.to_time.value = '" & Left(tm(1),5) & "';" & vbCrLf)
	%>

	document.event_form.address.value = '<% if address <> "" then Response.Write(Replace(address,"'","\'")) end if %>';
	document.event_form.county.value = '<%=county%>';

	document.event_form.contact.value = '<% if contact <> "" then Response.Write(Replace(contact,"'","\'")) end if %>';
	document.event_form.email.value = '<% if email <> "" then Response.Write(Replace(email,"'","\'")) end if %>';
	document.event_form.phone.value = '<% if phone <> "" then Response.Write(Replace(phone,"'","\'")) end if %>';
	document.event_form.url.value = '<% if url <> "" then Response.Write(Replace(url,"'","\'")) end if %>';

	document.event_form.pub.checked = <% if pub then response.write("true") else response.write("false") end if %>;

	//alert(document.event_form.area.options.length);

	for ( i = 0; i < document.event_form.area.options.length; i++ ) {

		//alert(document.event_form.area.options[i].value & <%=area%>);

		if ( (document.event_form.area.options[i].value & <%=area%>)) {
			document.event_form.area.options[i].selected = true;
		}
	}

	for ( i = 0; i < document.event_form.county.options.length; i++ ) {

		if ( (document.event_form.county.options[i].value & <%=county%>)) {
			document.event_form.county.options[i].selected = true;
		}
	}

	if ( <%=category%> == 0 ) document.event_form.category.selectedIndex = 0;
	else {
		for ( i = 1; i < document.event_form.category.options.length; i++ ) {

			if ( (document.event_form.category.options[i].value & <%=category%>)) {
				document.event_form.category.options[i].selected = true;
			}
		}
	}

	fillSubcategories(document.forms.event_form)

	//Subcategories
	<%
	'Use serverside script to generate some client side stuff here
	For count = 1 to UBound(subarray)
	%>
		for ( i = 0; i < document.event_form.subcategory.options.length; i++ ) {

			if ( (document.event_form.subcategory.options[i].value == '<%=subarray(count)%>' )) {
				document.event_form.subcategory.options[i].selected = true;
			}
		}
	<%
	Next
	%>

	<% end if %>

	//Set focus
	document.event_form.title.focus()
}

//-->
</SCRIPT>
</HEAD>
<% if msg <> "" then %>
	<BODY>
	<div class=eventIngress>Lagre i Nyhetskalenderen</div>
	<P class=formText>
	<%=msg%>
	</p>
<% elseif valg = "edit" then %>
	<BODY onLoad="loadValues()">
	<div class=eventIngress>Redigere hendelse i Nyhetskalenderen</div>
	<P class=formText>
	Rediger informasjonen om den valgte hendelsen, og klikk 'Lagre i Nyhetskalender'
	</p>
<% else %>
	<BODY onLoad="loadValues()">
	<div class=eventIngress>Ny hendelse i Nyhetskalenderen</div>
	<P class=formText>
	Fyll ut informasjonen om hendelsen som skal legges inn i nyhetskalenderen, og klikk 'Lagre i Nyhetskalender'
	</p>
<% end if %>

<% if msg = "" then %>

<form name=event_form action=add_event.asp method=post onSubmit="return onSubmit_ValidateForm(this)">
<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>

<% if valg <> "edit" and (Session("fullname") = "Gjestebruker" or Session("fullname") = "Portalbruker" or Session("fullname") = "PRMbruker") then %>
	<tr>
		<td style="border:none;">
		Lagt inn av (Navn, epost eller telefonnummer, gjerne alle tre.)
		<BR>
		<Input type=text name=addedby maxlength=200 style="width:594px;">
		</td>
	</tr>
<% end if%>

<tr>
	<td style="border:none;">
	Tittel
	<BR>
	<Input type=text name=title maxlength=200 style="width:594px;">
	</td>
</tr>

<tr>
	<td style="border:none;">
	Beskrivelse
	<BR>
	<textarea name=description wrap="virtual" cols=1 style="width:594px;height:80px;"><%=description%></textarea>
	</td>
</tr>
</table>

<BR>

<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=250 style="border:none;">
	Starttidspunkt (Format: dd.mm.���� tt:mm)
	<BR>
	Dato: <Input type=text name=from_date size=11 maxlength=10 onBlur="autoFillDate(this)">&nbsp;
	Klokkeslett: <Input type=text name=from_time size=6 maxlength=5 onBlur="autoFillTime(this)">
	</td>
	<td width=350 style="border:none;">
	Sluttidspunkt (Format: dd.mm.���� tt:mm)
	<BR>
	Dato: <Input type=text name=to_date size=11 maxlength=10 onBlur="autoFillDate(this)">&nbsp;
	Klokkeslett: <Input type=text name=to_time size=6 maxlength=5 onBlur="autoFillTime(this)">
	</td>
</tr>
</table>

<BR>

<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=135 style="border:none;">
	Omr�de
	<BR>
	<select name=area size=5 style="width:130px;" onChange="areaSelected(this.form)" multiple>
		<option value=0>&lt;Velg omr�de&gt;</option>
	<%

		dim rsArea
		Set rsArea = Server.CreateObject("ADODB.Recordset")
		rsArea.Open "Select * from BITNAMES where typeofname = 0 and category <= 4 order by displaysort asc", conn

		do while not rsArea.EOF
		%>

			<option value="<%=rsArea("Category")%>"><%=rsArea("DescriptiveName")%></option>

		<%
		rsArea.Movenext()
		loop
		rsArea.close

	%>
	</select>
	</td>

	<td width=150 style="border:none;">
	Fylke
	<br>
	<Select name=county size=5 style="width:145px;">
		<option value=0>&lt;Velg fylke&gt;</option>
		<%

			dim rsCounty
			Set rsCounty = Server.CreateObject("ADODB.Recordset")
			rsCounty.Open "Select * from BITNAMES where typeofname = 6 order by DescriptiveName asc", conn

			do while not rsCounty.EOF

				if rsCounty("DescriptiveName") <> "Riksnyheter" then
				%>
					<option value="<%=rsCounty("SubCategory")%>"><%=rsCounty("DescriptiveName")%></option>
				<%
				end if

			rsCounty.Movenext()
			loop
			rsCounty.close

		%>
	</select>
	</td>

	<td width=165 style="border:none;">
	Kategori
	<BR>
	<select name=category size=5 multiple style="width:160px;" onChange="fillSubcategories(this.form)">
		<option value=0>&lt;Velg kategori&gt;</option>
	<%

		dim rsCat
		Set rsCat = Server.CreateObject("ADODB.Recordset")
		rsCat.Open "Select * from BITNAMES where typeofname = 3 order by DescriptiveName asc", conn

		do while not rsCat.EOF
		%>

			<option value="<%=rsCat("category")%>"><%=rsCat("DescriptiveName")%></option>

		<%
		rsCat.Movenext()
		loop
		rsCat.close

	%>
	</select>
	</td>

	<td width=150 style="border:none;">
	Underkategori
	<BR>
	<select name=subcategory size=5 multiple style="width:145px;">
	</select>
	</td>
</tr>

<tr><td colspan="3" style="border:none;" class="hjelpetekst">&nbsp;Hold CTRL nede for � velge flere av gangen (Apple-knappen for Mac-brukere)</td></tr>
</table>

<br>

<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=90 style="border:none;">Adresse/Sted</td>
	<td width=510 style="border:none;"><Input type=text name=address size=80 maxlength=200 style="width:300px;"></td>
</tr>
<tr>
	<td width=90 style="border:none;">Kontaktperson</td>
	<td width=510 style="border:none;"><Input type=text name=contact size=80 maxlength=100 style="width:300px;"></td>
</tr>
<tr>
	<td width=90 style="border:none;">Telefonnummer</td>
	<td width=510 style="border:none;"><Input type=text name=phone size=80 maxlength=30 style="width:300px;"></td>
</tr>
<tr>
	<td width=90 style="border:none;">Epost</td>
	<td width=510 style="border:none;"><Input type=text name=email size=80 maxlength=100 style="width:300px;"></td>
</tr>
<tr>
	<td width=90 style="border:none;">Web-adresse</td>
	<td width=510 style="border:none;"><Input type=text name=url size=80 maxlength=100 style="width:300px;"></td>
</tr>
</table>

<BR>

<input type=hidden name=valg value=lagre>
<input type=hidden name=eventId value=<%=id%>>
<input type=hidden name=nmbr value=<%=nmbr%>>

<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<% if Session("admin") then %>
	<td align=left style="border:none;"><input type=checkbox class=button name=pub value="1" checked>&nbsp;&nbsp;Publiser denne hendelsen</td>
	<% end if %>

	<td align=right style="border:none;"><input type=submit class=button value="Lagre i Nyhetskalenderen"></td>
</tr>
</table>
</form>

<% end if %>

<%
conn.Close()
%>

</BODY>
</HTML>
