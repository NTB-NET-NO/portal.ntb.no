<%@ Language=VBScript %>

<!--#include file=../func_lib.asp-->

<%
	if not CheckLogin(true) then
		Response.write("Du har ikke tilgang til denne siden.")
		Response.End()
		'Redirect("../login.asp")
	end if


	Dim conn
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")

	id = Request("EventID")
	valg = Request("valg")

	'Enable the saved search
	Session("load_saved") = true

	if IsNumeric(id) and valg = "slett" then

		sql = "DELETE FROM event_subrel WHERE EventId = " & id
		conn.Execute(sql)

		sql = "DELETE FROM news_events WHERE EventId = " & id
		conn.Execute(sql)

		msg = "Hendelsen er slettet."
	else
		msg = "Hendelsen ble ikke funnet."
	end if

%>

<HTML>

<HEAD>
<LINK rel="stylesheet" type="text/css" href="../nyhetskalender.css">

<%
	'Check referer for sports
	if Instr(Request.ServerVariables("HTTP_REFERER"),"sports_print_list.asp") > 0 then
		Response.Write("<META HTTP-EQUIV=Refresh CONTENT='2; URL=../sports_print_list.asp'>")
	else
		Response.Write("<META HTTP-EQUIV=Refresh CONTENT='2; URL=../search_event.asp'>")
	end if

%>

</HEAD>

<BODY>

	<div class=eventIngress>Sletting av hendelse</div>
	<P class=alert_message>
	<%=msg%>
	</p>

</BODY>
</HTML>
