<%@ Language=VBScript %>


<!--#include file=../func_lib.asp-->

<%
	if not CheckLogin(true) then
		Response.write("Du har ikke tilgang til denne siden.")
		Response.End()
		'Redirect("../login.asp")
	end if

	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")


	valg = Request.Form("valg")
	tid = 2

	dim sql
	sql = ""

	if valg = "save" then

		dim id
		dim title
		dim description
		dim fromtime
		dim totime
		dim area
		dim category
		dim subcategory
		'dim subarray
		dim subref
		dim address
		dim county
		dim contact
		dim email
		dim phone
		dim url

		dim sc
		sc = 0

		'subarray = Array()

		for i = 1 to Request.Form("count")

			'Id
			id = Request.Form("id-" & i)

			'Tekst
			title = Request.Form("title-" & i)
			description = Request.Form("desc_" & i)

			if description = "" then
				description = " "
			end if

			'Misc
			place = Request.Form("sted-" & i)
			url = Request.Form("url-" & i)


			'Tid
			fromTime = Request.Form("from_date-" & i)
			toTime = Request.Form("to_date-" & i)
			if toTime = "" then toTime = fromTime

			if ( fromtime <> "" and totime <> "") then
				dim blocks
				blocks = split(fromtime,".")


				if UBound(blocks,1) = 2 then
					fromtime = DateSerial(blocks(2),blocks(1),blocks(0))
					fromtime = fromtime & " " & Replace(Request.Form("from_time-" & i),".",":")
				else
					fromtime = ""
				end if

				blocks = split(totime,".")
				if UBound(blocks,1) = 2 then
					totime = DateSerial(blocks(2),blocks(1),blocks(0))
					totime = totime & " " & Replace(Request.Form("to_time-" & i),".",":")
				else
					totime = ""
				end if

			end if

			'if not ( IsDate(fromtime) and IsDate(toTime) ) then
			'	msg = "Det oppstod en feil ved lagring av hendelse i nyhetskalenderen.<P>Sjekk det angitte tidsrom for hendelsen og fors�k igjen."
			'end if

			'Underkategorier
			subcategory = -1
			if Request.Form("subcategory_" & i).Count > 0 then
				subref = -1
			else
				subref = 0
			end if

			if IsDate(fromtime) and IsDate(toTime) and title <> "" and description <> "" then
				if IsNumeric(id) then

					'Oppdatere eksisterende hendelse
					dim rsUpdate
					Set rsUpdate = Server.CreateObject("ADODB.RecordSet")
					rsUpdate.CursorLocation = adUseClient
					rsUpdate.Open "SELECT * FROM news_events WHERE EventID = " & id,conn,adOpenDynamic,adLockOptimistic,adCmdText

					rsUpdate("title") = title
					rsUpdate("description") = description
					rsUpdate("starttime") = CDate(fromtime)
					rsUpdate("endtime") = CDate(totime)
					rsUpdate("address") = place
					rsUpdate("url") = url
					rsUpdate("hasSubRef") = subref
					rsUpdate("Modified") = CDate(DateSerial(Year(Date()),Month(Date()),Day(Date())) & " " & Hour(Now()) & ":" & Minute(Now()))
					rsUpdate.Update()

					id = rsUpdate("EventID")
					rsUpdate.Close()

					'Delete subcats
					conn.Execute "DELETE FROM event_subrel WHERE EventID = " & id

				else

					'Add an event
					dim rsInsert
					Set rsInsert = Server.CreateObject("ADODB.RecordSet")
					rsInsert.CursorLocation = adUseClient
					rsInsert.Open "news_events",conn,adOpenDynamic,adLockOptimistic,adCmdTableDirect

					rsInsert.AddNew()
					rsInsert("title") = title
					rsInsert("description") = description
					rsInsert("starttime") = CDate(fromtime)
					rsInsert("endtime") = CDate(totime)
					rsInsert("category") = 2048
					rsInsert("subcategory") = subcategory
					rsInsert("hasSubRef") = subref
					rsInsert("address") = place
					rsInsert("area") = 4
					rsInsert("fylke") = 0
					rsInsert("url") = url
					rsInsert("AddedBy") = Session("fullname")
					rsInsert("Modified") = CDate(DateSerial(Year(Date()),Month(Date()),Day(Date())) & " " & Hour(Now()) & ":" & Minute(Now()))
					rsInsert("Created") = CDate(DateSerial(Year(Date()),Month(Date()),Day(Date())) & " " & Hour(Now()) & ":" & Minute(Now()))
					rsInsert("Published") = 1
					rsInsert.Update()

					id = rsInsert("EventID")
					rsInsert.Close

				end if

				'Legge inn subcats
				For count = 1 to Request.Form("subcategory_" & i).Count

					dim tmp
					tmp = Split( Request.Form("subcategory_" & i)(count), "_" )

					dim sql2
					sql2 = "INSERT INTO event_subrel (EventID,Category,SubCategory) VALUES " & _
						  "(" & id & "," & tmp(0) & "," & tmp(1) & ")"

					'response.write(sql2 & "<br>")
					conn.Execute (sql2)
				Next

				sc = sc + 1
			else
				'Error
				Response.Write("Lagring feilet, data mangler: " & title & "<br>")
			end if
		next

		'Write feedback

		Response.Write("<P>Antall lagret: " & sc)
		Response.Write("<P><a href=""sports-edit.asp"">Tilbake til sportsredigeringsskjemaet</a>")
		Response.End

	end if

	if valg = "search" then

		'Build custom query
		sql = "SELECT DISTINCT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE "

		'Area and category - Must be sports
		sql = sql & "( a.area & 4 ) > 0 AND ( a.category & 2048 ) > 0 "

		'Free tekst search
		str = Trim(Replace(Request.Form("textstring"),"'"," "))
		if str <> "" then
			sql = sql & "AND (a.title LIKE '%" & str & "%' OR a.description LIKE '%" & str & "%') "
		end if

		'Time specification
		tid = CInt(Request.Form("time"))
		if tid > 0 then

			dim fra
			dim til

			fra = DateSerial(Year(Date()),Month(Date()),Day(Date()))
			if in_time = 30 then
				fra = DateAdd("d",1,fra)
			end if
			til = DateAdd("d",tid,fra)

			sql = sql & "AND a.endtime >= '" & fra & "' AND a.starttime < '" & til & "' "

		elseif tid = -1 then
			'Use specific dates
			'dim blocks

			if Request.Form("from_date") <> "" then
				blocks = split(Request.Form("from_date"),".")
				sql = sql & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			end if

			if Request.Form("to_date") <> "" then
				blocks = split(Request.Form("to_date"),".")
				sql = sql & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			end if
		end if

		'Subcategories
		if Request.Form("subCategory").Count > 0 then

			sql = sql & "AND ( "

			For catCount = 1 to Request.Form("subcategory").Count

				if catCount > 1 then sql = sql & "OR "
				tmp = Split( Request.Form("subcategory")(catCount), "_" )
				sql = sql & "( b.category = " & tmp(0) & " AND b.subcategory = " & tmp(1) & " ) "

			Next

			sql = sql & ") "
		end if

		sql = sql & "ORDER BY starttime ASC"
	else
		'Show everything one week ahead by default -
		sql = "SELECT * FROM news_events WHERE ( area & 4 ) > 0 AND ( category & 2048 ) > 0 AND endtime >= '" & Date() & "' AND starttime < '" & DateAdd("d",tid,Date()) & "' ORDER BY starttime ASC"
	end if


	if valg = "blanks" then
		rc = Request.Form("blankCount")
	else

		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.CursorLocation = adUseClient
		rs.Open sql,conn,adOpenDynamic,adLockOptimistic,adCmdText

		sql2 = sql
		rc = rs.recordcount
	end if


%>


<HTML>
<HEAD>

<LINK rel="stylesheet" type="text/css" href="../nyhetskalender.css">

<SCRIPT LANGUAGE=javascript>
<!--


//function to validate the different search options prior to submitting the form
function onSubmit_ValidateForm(activeform) {

	//New datecheck
	if ( activeform.time.options[activeform.time.selectedIndex].text == "Spesifiser tidsrom") {

		//New date check
		rx = /^\d{2}\.\d{2}\.\d{4}$/;
		fd = activeform.from_date.value;
		td = activeform.to_date.value;

		//Check dates
		if ( fd == "" && td == "" ) {
			alert("N�r du velger � spesifisere dato, m� disse fylles inn i datofeltene.");
			activeform.to_date.focus();
			return false;
		}
		else if ( ! rx.test(fd) && fd != "" ) {
			alert("Du har angitt en ugyldig 'Fra'-dato.");
			activeform.from_date.focus();
			return false;
		}
		else if ( ! rx.test(td) && td != "" ) {
			alert("Du har angitt en ugyldig 'Til'-dato.");
			activeform.to_date.focus();
			return false;
		}
		else {

			var fa = new Array(3);
			var ta = new Array(3);
			var dt = new Date();

			//Verify from date
			if ( fd != "" ) {
				fa = fd.split(".");

				if ( fa[0] > 31 || fa[1] > 12 || fa[0] < 1 || fa[1] < 1 ) {
					alert("Du har angitt en ugyldig 'Fra'-dato.");
					activeform.from_date.focus();
					return false;
				}
			}

			if ( td != "" ) {
				ta = td.split(".");

				if ( ta[0] > 31 || ta[1] > 12 || ta[0] < 1 || ta[1] < 1 ) {
					alert("Du har angitt en ugyldig 'Til'-dato.");
					activeform.to_date.focus();
					return false;
				}

			}

			if ( fd != "" && td != "" ) {

				if ( fa[2]+fa[1]+fa[0] > ta[2]+ta[1]+ta[0] ) {
					alert("Du har angitt et ugyldig tidsintervall. 'Fra'-dato m� v�re mindre enn 'Til'-dato!");
					activeform.to_date.focus();
					return false;
				}
			}
		}
	}

	return true;
}

function autoFillDate(datefield, id) {

	var text = datefield.value;
	var d = new Date();
	var year;

	//Substitute separators
	rxSub = /[- \/]/g;
	text = text.replace(rxSub,".");

	//Test for missing zeros
	rxZ1 = /^(\d{1})$/;
	rxZ2 = /^(\d{1})(\..+)/;
	rxZ3 = /(.+\.)(\d{1})$/;
	rxZ4 = /(.+\.)(\d{1})(\..+)/;

	text = text.replace(rxZ1,"0$1");
	text = text.replace(rxZ2,"0$1$2");
	text = text.replace(rxZ3,"$10$2");
	text = text.replace(rxZ4,"$10$2$3");

	//Test for autofilling of dates
	rx1 = /^\d{2}$/;
	rx2 = /^\d{2}\.\d{2}$/;
	rx3 = /^(\d{2}\.\d{2}\.)(\d{2})$/;
	rx4 = /^\d{2}\.\d{2}\.\d{4}$/;

	if ( rx4.test(text) ) {
		//do nothing
	} else if ( rx3.exec(text) ) {
		year = RegExp.$2;
		if (year < 50) year = 20 + year;
		else if (year < 100) year = 19 + year;

		text = text.replace(rx3,"$1" + year);

	} else if ( rx2.test(text) ) {
		text += "." + d.getFullYear();
	} else if ( rx1.test(text) ) {
		var m = (d.getMonth()+1)
		if ( m < 10 ) m = "0" + m;
		text += "." + m + "." + d.getFullYear();
	}

	datefield.value = text;

	if ( id > 0 ) {
		document.editform["to_date-" + id].value = text;

		for ( i = id+1; i <= <%=rc%>; i++ ) {

			if 	( document.editform["from_date-" + i].value == "" ) {
				document.editform["from_date-" + i].value = text;
				document.editform["to_date-" + i].value = text;
			}
		}

	} else {
		document.searchform.to_date.value = text;
	}

}

function autoFillTime(datefield, id) {

	var text = datefield.value;
	var year;

	//Substitute separators
	rxSub = /[\.\- ]/g;
	text = text.replace(rxSub,":");

	//Test for missing zeros
	rxZ1 = /^(\d{1})$/;
	rxZ2 = /^(\d{1})(:.+)/;
	rxZ3 = /(.+:)(\d{1})$/;

	text = text.replace(rxZ1,"0$1");
	text = text.replace(rxZ2,"0$1$2");
	text = text.replace(rxZ3,"$10$2");

	//Test for autofilling of dates
	rx1 = /^\d{2}$/;
	rx2 = /^(\d{2})(\d{2})$/;
	rx3 = /^\d{2}\.\d{2}$/;

	if ( rx3.test(text) ) {
		//do nothing
	} else if ( rx2.test(text) ) {
		text = text.replace(rx2,"$1:$2");
	} else if ( rx1.test(text) ) {
		text += ":00";
	}

	datefield.value = text;

	for ( i = id+1; i <= <%=rc%>; i++ ) {

		if 	( document.editform["from_time-" + i].value == "" ) {
			document.editform["from_time-" + i].value = text;
		}
	}

}


//Blank datefield upon other selection
function timeTypeChanged(form){

	if (form.time.options[form.time.selectedIndex].text != "Spesifiser tidsrom") {

		form.from_date.value = "";
		form.to_date.value = "";

	}
}

//Autoselects specify time in the time selectbox
function setSpecifyTime(idx){
	document.searchform.time.selectedIndex = idx;
}

function loadParams() {

	document.searchform.textstring.value = '<%=Request.Form("textstring")%>';
	document.searchform.from_date.value = '<%=Request.Form("from_date")%>';
	document.searchform.to_date.value = '<%=Request.Form("to_date")%>';

	for ( i = 0; i < document.searchform.time.options.length; i++ ) {

		if ( document.searchform.time.options[i].value == '<%=tid%>' ) {
			document.searchform.time.selectedIndex = i;
			break;
		}
	}


	//Subcategories
	<%
	'Use serverside script to generate some client side stuff here
	if IsObject(Request.Form("subcategory")) then
		For catCount = 1 to Request.Form("subcategory").Count
		%>
			for ( i = 0; i < document.searchform.subcategory.options.length; i++ ) {

				if ( (document.searchform.subcategory.options[i].value == '<%=Request.Form("subcategory")(catcount)%>' )) {
					document.searchform.subcategory.options[i].selected = true;
				}

			}
		<%
		Next
	end if
	%>

}

function blanks() {
	document.searchform.valg.value = 'blanks';
	document.searchform.submit();
}

function setSports(sel,id) {

	//alert (sel.options.length);
	var jump = false;
	var tmp = "";
	for ( i = id+1; i <= <%=rc%>; i++ ) {
		if ( document.editform["subcategory_" + i] != undefined ) {
			if ( document.editform["subcategory_" + i].selectedIndex == -1 ) {

				document.editform["subcategory_" + i].focus();

				for ( j=0; j<sel.options.length; j++) {
					if (sel.options[j].selected ) {
						document.editform["subcategory_" + i].options[j].selected = true;
						jump = true;
					}
				}
			}
		}
	}

	if ( jump ) document.editform["desc_1"].focus();
}

//-->
</script>

</HEAD>
<BODY onLoad="loadParams()">

<form name=searchform method=post action=sports-edit.asp onSubmit="return onSubmit_ValidateForm(this)">

<table  borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=400 style="border:none;" valign=top>
	S�k i fritekst i tittel og beskrivelse<BR>
	<Input type=text name=textstring size=25 onKeyPress="setSpecifyTime(4)" style="width: 396px">
	</td>
	<td width=200 style="border:none;">
	Sportsgren<BR>
	<select name=subcategory multiple size=3 style="width: 194px">
	<!--option value=-1>&lt; Alle &gt;</option-->

	<%
	dim rsSubCategory
	Set rsSubCategory = Server.CreateObject("ADODB.Recordset")

	rsSubCategory.Open "Select * from bitnames where typeofname = 4 and category = 2048 order by DescriptiveName asc", conn

	do while not rsSubCategory.eof
		Response.Write("<OPTION VALUE='2048_" & rsSubCategory("subcategory") & "'>" & rsSubCategory("DescriptiveName") & "</OPTION>")
		rsSubCategory.MoveNext()
	loop

	'rsSubCategory.Close()

	%>

	</select>
	</td>
</tr>
</table>

<BR>

<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=200 style="border:none;">
	Tidsrom<BR>
	<select name=time style="width: 195px" onChange="timeTypeChanged(this.form)">
	<option value="2">I dag og i morgen</option>
	<option value="7">Neste 7 dager</option>
	<option value="30">Neste 30 dager</option>
	<option value="-1">Spesifiser tidsrom</option>
	<option value="0">Alle hendelser</option>
	</select>
	</td>

	<td width=200 style="border:none;">
	Fra (Format: dd.mm.����)
	<BR>
	<Input type=text name=from_date size=25 maxlength=10 onKeyPress="setSpecifyTime(3)" onBlur="autoFillDate(this,0)">
	</td>

	<td width=200 style="border:none;">
	Til (Format: dd.mm.����)
	<BR>
	<Input type=text name=to_date size=25 maxlength=10 onKeyPress="setSpecifyTime(3)" onBlur="autoFillDate(this,0)">
	</td>

</tr>
</table>

<br>

<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td style="border:none;">
		Opprett <Input type=text name="blankcount" value="10" size=2 maxlength=2 > blanke hendelser
		<Input type=button value=" Opprett blanke " onclick="blanks();">
	</td>
	<td style="border:none;" align=right>
		<Input type=hidden name=valg value="search">
		<Input type=submit value=" S�k " >
	</td>
</tr>
</table>
</form>

<p>Resultater: (<%=rc%> hendelser)</p>

<table width=600 cellpadding=0 cellspacing=0 border=0 class=eventNews>

	<%
	if rc = 0 then
		'No hits
	%>
		<tr><td class=eventNews>Ingen hendelser funnet.</td></tr>
	<%
	else
	%>
		<tr><td>

		<form name=editform action=sports-edit.asp method=post>

		<%
		for count = 1 to rc

			dim stm
			dim etm

			if valg = "blanks" then
				stm = Split("  ")
				etm = Split("  ")
				subref = 0
			else
				id = rs("eventId")
				title = rs("title")
				sted = rs("address")
				url = rs("url")
				description = rs("description")
				subref = rs("hassubref")
				stm = Split(rs("startTime"))
				etm = Split(rs("endTime"))
			end if

			%>
			<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>

			<tr><td width=100 style="border:none;" >Tittel:</td>
			<td width=500 style="border:none;" >
			<input type=hidden name="id-<%=count%>" value=<%=id%>>
			<input type="text" name="title-<%=count%>" value="<%=title%>" style="width:496px;">
			</td>
			</tr>

			<tr><td style="border:none;" >Beskrivelse:</td></tr>
			<tr><td colspan=2 style="border:none;" >
			<textarea name="desc_<%=count%>" id="desc_<%=count%>" wrap="virtual" cols=1 style="width:596px;height:100px;"><%=description%></textarea>
			</td></tr>

			<tr><td width=100 style="border:none;" >Tidspunkt:</td>
			<td width=500 style="border:none;" >Fra:
			<input type="text" name="from_date-<%=count%>" value="<%=stm(0)%>" onBlur="autoFillDate(this,<%=count%>)" style="width:100px;">
			Kl.:<input type="text" name="from_time-<%=count%>" value="<% if UBound(stm) > 0 then Response.Write(Left(stm(1),5))%>" style="width:100px;" onBlur="autoFillTime(this,<%=count%>)">

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Til:
			<input type="text" name="to_date-<%=count%>" value="<%=etm(0)%>" onBlur="autoFillDate(this,<%=count%>)" style="width:100px;">
			Kl.:<input type="text" name="to_time-<%=count%>" value="<% if UBound(etm) > 0 then Response.Write(Left(etm(1),5))%>" style="width:100px;">
			</td>
			</tr>

			<tr>
			<td colspan=2  style="border:none;">
				<table width=595 border=0 cellpadding=0 cellspacing=0 class=formText>
					<tr>
					<td width=100 style="border:none;" >Sted:</td>
					<td width=250 style="border:none;" >
					<input type="text" name="sted-<%=count%>" value="<%=sted%>" style="width:250px;">
					</td>

					<td width=145 rowspan=2 valign=top>

					<%
						'Load subcats
						dim subs
						subs = ""

						if subref = -1 then
							dim rsRetrieve
							Set rsRetrieve = Server.CreateObject("ADODB.RecordSet")
							rsRetrieve.CursorLocation = adUseClient
							rsRetrieve.Open "SELECT * FROM event_subrel WHERE EventID = " & rs("EventID"),conn,adOpenDynamic,adLockOptimistic,adCmdText


							'Redim subarray(rsRetrieve.RecordCount)

							dim c
							for c = 1 to rsRetrieve.RecordCount
								'subarray(c) = rsRetrieve("Category") & "_" & rsRetrieve("SubCategory")
								subs = subs & "_" & rsRetrieve("Category") & "_" & rsRetrieve("SubCategory")& "_"
								rsRetrieve.MoveNext()
							next

							rsRetrieve.Close()

						end if
					%>


					<select name=subcategory_<%=count%> id=subcategory_<%=count%> size=3 style="width: 194px" onblur="<% if count = 1 then Response.Write("setSports(this," & count & ")") %>">
						<%
						rsSubCategory.MoveFirst()
						do while not rsSubCategory.eof
							if ( InStr(subs,"_2048_" & rsSubCategory("subcategory") & "_") > 0 ) then
								Response.Write("<OPTION VALUE='2048_" & rsSubCategory("subcategory") & "' SELECTED>" & rsSubCategory("DescriptiveName") & "</OPTION>")
							else
								Response.Write("<OPTION VALUE='2048_" & rsSubCategory("subcategory") & "'>" & rsSubCategory("DescriptiveName") & "</OPTION>")
							end if
							rsSubCategory.MoveNext()
						loop
						%>
					</select>

					</td>
					</tr>
					<tr><td width=100 style="border:none;" >Webadresse:</td>
					<td width=250 style="border:none;" >
					<input type="text" name="url-<%=count%>" value="<%=url%>" style="width:250px;">
					</td>
					</tr>
				</table>
			</td>
			</tr>

			</table>
			<br>
			<%
			if valg <> "blanks" then rs.MoveNext()
		next
		%>

		<tr><td>
		<input type=hidden name=count value=<%=rc%>>
		<input type=hidden name=valg value=save>
		<input type=submit value="Lagre endringer">
		</td></tr>
		</form>
		</table>

	<%
		rsSubCategory.Close()
	end if
	%>

</table>


<!--%=sql2%-->

<%
if valg <> "blanks" then rs.Close
conn.close
%>
</BODY>
</HTML>
