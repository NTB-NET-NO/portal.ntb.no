<%@ Language=VBScript %>

<!--#include file=date_func.asp-->

<%
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")
	
	valg = Request.Form("valg")
	
	category = 0
	area = 0
	tid = 7
	
	dim sql
	sql = ""
	if valg = "search" then
	
		'Build custom query
		sql = "SELECT DISTINCT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE "

		'Area (Stoffgruppe)
		For catCount = 1 to Request.Form("area").Count
			area = area or Request.Form("area")(catCount)
		Next
		if area = 0 then area = -1

		'County
		For catCount = 1 to Request.Form("county").Count
			county = county or Request.Form("county")(catCount)
		Next
		if county = 0 then county = -1

		'Main category
		For catCount = 1 to Request.Form("category").Count
			category = category or Request.Form("category")(catCount)
		Next
		if category = 0 then category = -1
		
		'Area and category		
		sql = sql & "(a.area & " & area & ") > 0 " 

		sql = sql & "AND ( (a.category & " & category & ") > 0 "
		if category = -1 then sql = sql & "OR a.category = 0 "
		sql = sql & ") " 

		sql = sql & "AND ( (a.fylke & " & county & ") > 0 "
		if county = -1 then sql = sql & "OR a.fylke = 0 "
		sql = sql & ") " 
			
		'Free tekst search
		str = Trim(Replace(Request.Form("textstring"),"'"," "))
		if str <> "" then
			sql = sql & "AND (a.title LIKE '%" & str & "%' OR a.description LIKE '%" & str & "%') "
		end if
			
		'Time specification
		tid = CInt(Request.Form("time"))
		if tid > 0 then
		
			dim fra
			dim til
			
			fra = DateSerial(Year(Date()),Month(Date()),Day(Date()))
			if in_time = 30 then 
				fra = DateAdd("d",1,fra)
			end if
			til = DateAdd("d",tid,fra)			
			
			sql = sql & "AND a.endtime >= '" & fra & "' AND a.starttime < '" & til & "' "

		elseif tid = -1 then
			'Use specific dates
			dim blocks
					
			if Request.Form("from_date") <> "" then 
				blocks = split(Request.Form("from_date"),".")
				sql = sql & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			end if
						
			if Request.Form("to_date") <> "" then 
				blocks = split(Request.Form("to_date"),".")
				sql = sql & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			end if
		end if
			
		'Subcategories	
		if Request.Form("subCategory").Count > 0 then

			sql = sql & "AND ( " 			

			For catCount = 1 to Request.Form("subcategory").Count

				if catCount > 1 then sql = sql & "OR "
				tmp = Split( Request.Form("subcategory")(catCount), "_" )
				sql = sql & "( b.category = " & tmp(0) & " AND b.subcategory = " & tmp(1) & " ) "
		  
			Next

			sql = sql & ") " 			
		end if
	
		sql = sql & "ORDER BY starttime ASC" 			
	else		
		'Show everything one week ahead by default -  
		sql = "SELECT * FROM news_events WHERE endtime >= '" & Date() & "' AND starttime < '" & DateAdd("d",tid,Date()) & "' ORDER BY starttime ASC"
	end if

	'Reset the vars for remembering the settings in the form
	if area = 0 then area = -1
	if category = 0 then category = -1


	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient
	rs.Open sql,conn,adOpenDynamic,adLockOptimistic,adCmdText
	
	sql2 = sql

%>
<HTML>
<HEAD>

<LINK rel="stylesheet" type="text/css" href="nyhetskalender.css">

<SCRIPT LANGUAGE=javascript>
<!--

//-->
</script>

</HEAD>
<BODY class=print>
<table width=600 cellpadding=0 cellspacing=0 class=eventNews>

	<%
	if rs.EOF then
		'No hits
	%>
		<tr><td class=eventNews>Ingen hendelser funnet.</td></tr>
	<%	
	else
	%>
	
		<tr><td class=eventIngress>Totalt: <%=rs.RecordCount%> hendelser.</td></tr>
		<tr><td class=eventIngress>&nbsp;</td></tr>
	
		<%
		do while not rs.eof
		%>
			<tr><td class=eventHeadline><%=rs("title")%></td></tr>
			<tr><td class=eventIngress>
			<%=LongDisplayDate(rs("startTime"))%>
			<%
			if FormatDateTime(rs("startTime"),vbShortTime) <> "00:00" then
				Response.Write(" kl. " & FormatDateTime(rs("startTime"),vbShortTime) )
			end if		
			
			if DateDiff("d",rs("startTime"),rs("endTime")) > 0 or FormatDateTime(rs("endTime"),vbShortTime) <> "00:00" then
				Response.Write(" - ")
				doKl = false
				
				if DateDiff("d",rs("startTime"),rs("endTime")) > 0 then
					Response.Write(LongDisplayDate(rs("endTime")))
					doKl = true
				end if		
			
				if FormatDateTime(rs("endTime"),vbShortTime) <> "00:00" then
					if doKl then Response.Write(" kl. ")
					Response.Write(FormatDateTime(rs("endTime"),vbShortTime))
				end if		
			end if		
			%>
			</td></tr>

			<%
			set rsCounty = Server.CreateObject("ADODB.RecordSet")		
			sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (news_events a RIGHT JOIN bitnames b ON (a.fylke & b.subcategory) > 0 AND b.typeofname = 6) WHERE a.eventid = " & rs("EventId") & " ORDER BY b.TypeOfName"
			rsCounty.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText
		
			if rs("address") <> "" or not rsCounty.EOF then 
				Response.Write("<tr><td class=eventIngress>Sted: " & rs("address"))
				
				if not rsCounty.EOF then 
					if rs("address") <> "" then Response.Write("&nbsp;-&nbsp;")
					Response.Write(rsCounty("DescriptiveName")) 
				end if 
				
				Response.Write("</td></tr>")
			end if
			rsCounty.Close()
			%>

			<%if rs("contact") <> "" or rs("contactEmail") <> "" or rs("contactPhone") <> "" or rs("url") <> "" then
				dash = false
				contact = ""
				if rs("contact") <> "" then
					contact = rs("contact")
					dash = true
				end if
				
				if rs("contactEmail") <> "" then
					if dash then contact = contact & "&nbsp;-&nbsp;"
					contact = contact & rs("contactEmail")
					dash = true
				end if
				
				if rs("contactphone") <> "" then
					if dash then contact = contact & "&nbsp;-&nbsp;"
					contact = contact & rs("contactphone")
					dash = true
				end if
				
				if rs("url") <> "" then
					if dash then contact = contact & "&nbsp;-&nbsp;"
					contact = contact & rs("url")
				end if
	
			%>
			<tr><td class=eventNews><span class=eventIngress>Kontakt:</span> <%=contact%></td></tr>
			<%end if%>
					
			<%
			'Kategorisering
			'Set rsCategory = Server.CreateObject("ADODB.RecordSet")
			'Set rsSubCat = Server.CreateObject("ADODB.RecordSet")

			'sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (news_events a RIGHT JOIN bitnames b ON (a.area & b.category > 0 AND b.typeofname = 0) "
		
			'if rs("category") <> -1 then
			'	sql = sql & "OR (a.category & b.category > 0 AND b.typeofname = 3) "
			'end if
		
			'sql = sql & ") WHERE a.eventid = " & rs("EventId") & " ORDER BY b.TypeOfName"
		
			'rsCategory.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText
			
			'if not rsCategory.EOF then
			'	Response.Write("<span class=eventIngress>Kategorisering:</span>" & vbcrlf & "<span class=eventNews>")
			'	do while not rsCategory.EOF
			'		Response.Write(rsCategory("DescriptiveName"))
			'		rsCategory.Movenext()
			'		if not rsCategory.EOF or rs("HasSubRef") = -1 then Response.Write(", ")
			'	loop
			'end if
			
			'if rs("HasSubRef") = -1 then
			'	sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (event_subrel a RIGHT JOIN bitnames b ON a.category = b.category AND a.subcategory = b.subcategory AND b.typeofname = 4) WHERE a.eventid = " & rs("EventId") & " ORDER BY b.TypeOfName"
			'	rsSubCat.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText
			'	do while not rsSubCat.EOF
			'		Response.Write(rsSubCat("DescriptiveName"))
			'		rsSubCat.Movenext()
			'		if not rsSubCat.EOF then Response.Write(", ")
			'	loop
			'	rsSubCat.Close()
			'end if
			
			'rsCategory.Close()
						
			%>
			<tr><td class=eventNews><%=rs("Description")%></td></tr>
			<tr><td class=eventNews>------------------------------</td></tr>
		<%
		rs.MoveNext()
		loop
		%>	

	<%
	end if
	%>

</table>
<!--%=sql2%-->

<%
rs.Close
conn.close
%>
</BODY>
</HTML>
