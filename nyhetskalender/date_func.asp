<%

dim months
months = Array("","januar","februar","mars","april","mai","juni","juli","august","september","oktober","november","desember")

function LongDisplayDate(byVal dt)

	dim d
	dim m
	dim y

	d = Day(dt)
	m = Month(dt)
	y = Year(dt)
	
	LongDisplayDate = d & ". " & months(m) & " " & y
end function

function Timestamp(byVal dt)

	dim ret
	
	dim d
	dim m
	dim y

	dim hr
	dim min
	
	d = Day(dt)
	m = Month(dt)
	y = Year(dt)
	
	hr = Hour(dt)
	min = Minute(dt)

	if d < 10 then d = "0" & d
	if m < 10 then m = "0" & m
	if hr < 10 then hr = "0" & hr
	if min < 10 then min = "0" & min

	ret = d & "." & m & "." & y
			
	if Hour(dt) > 0 or Minute(dt) > 0 then
		ret = ret & " " & hr & ":" & min
	end if

	Timestamp = ret
end function


%>