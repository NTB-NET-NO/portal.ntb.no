<%
	dim months
	dim days

	months = Array("","januar","februar","mars","april","mai","juni","juli","august","september","oktober","november","desember")
	days = Array("","S�ndag","Mandag","Tirsdag","Onsdag","Torsdag","Fredag","L�rdag")


	'Get name of weekday for given date
	function GetWeekday(byVal dt)

		GetWeekday = days(Datepart("w",dt))

	end function

	'Create long display date
	function LongDisplayDate(byVal dt)

		dim d
		dim m
		dim y

		d = Day(dt)
		m = Month(dt)
		y = Year(dt)

		LongDisplayDate = d & ". " & months(m) & " " & y
	end function


	'Genererate timestamp from a date
	function Timestamp(byVal dt)

		dim ret

		dim d
		dim m
		dim y

		dim hr
		dim min

		d = Day(dt)
		m = Month(dt)
		y = Year(dt)

		hr = Hour(dt)
		min = Minute(dt)

		if d < 10 then d = "0" & d
		if m < 10 then m = "0" & m
		if hr < 10 then hr = "0" & hr
		if min < 10 then min = "0" & min

		ret = d & "." & m & "." & y

		if Hour(dt) > 0 or Minute(dt) > 0 then
			ret = ret & " " & hr & ":" & min
		end if

		Timestamp = ret
	end function

	'Genererate timestamp including seconds from a date
	function TimestampSec(byVal dt)

		dim ret

		dim d
		dim m
		dim y

		dim hr
		dim min

		d = Day(dt)
		m = Month(dt)
		y = Year(dt)

		hr = Hour(dt)
		min = Minute(dt)
		sec = Second(dt)

		if d < 10 then d = "0" & d
		if m < 10 then m = "0" & m
		if hr < 10 then hr = "0" & hr
		if min < 10 then min = "0" & min
		if sec < 10 then sec = "0" & sec

		ret = d & "." & m & "." & y

		if Hour(dt) > 0 or Minute(dt) > 0 or Second(dt) > 0 then
			ret = ret & " " & hr & ":" & min & ":" & sec
		end if

		TimestampSec = ret
	end function


	'Check credentials, either from form/query-string or IP-range
	function CheckLogin (admin)
		Dim conn
		dim sql
		dim rs

		Set conn = Server.CreateObject("ADODB.Connection")
		conn.Open Application("News_Events_Connectionstring")

		dim un
		dim pwd

		un = Request("login")
		pwd = Request("pwd")

		'Check if user is already logged in
		if Session("username") <> "" then

			CheckLogin = not admin or Session("admin")
			exit function
		end if


		'First try the Username Password, return if ok
		if un <> "" and pwd <> "" then
			sql = "SELECT * FROM Users WHERE username = '" & un & "' AND password = '" & pwd & "'"
			set rs = conn.Execute(sql)

			if not rs.EOF then
				Session("username") = rs("Username")
				Session("fullname") = rs("Fullname")
				Session("admin") = rs("Admin")

				'response.write session("admin")

				CheckLogin = not admin or Session("admin")
				exit function
			end if

			rs.Close()
		end if


		'Then try an ipcheck
		sql = "SELECT * FROM Users WHERE ipcheck != '' and ipcheck is not NULL"
		set rs = conn.Execute(sql)

		while not rs.EOF

			dim ips
			ips = Split(rs("ipcheck"),",")

			dim ip
			for each ip in ips

				if InStr(ip,"-") > 0 then

					dim range
					range = Split(ip,"-")

					if Request.ServerVariables("REMOTE_ADDR") >= range(0) and Request.ServerVariables("REMOTE_ADDR") <= range(1) then

						Session("username") = rs("Username")
						Session("fullname") = rs("Fullname")
						Session("admin") = rs("Admin")

						'response.write session("admin")

						CheckLogin = not admin or Session("admin")
						exit function
					end if

				elseif InStr(Request.ServerVariables("REMOTE_ADDR"),ip) > 0 then

					Session("username") = rs("Username")
					Session("fullname") = rs("Fullname")
					Session("admin") = rs("Admin")

					'response.write session("admin")

					CheckLogin = not admin or Session("admin")
					exit function
				end if

			next

			rs.MoveNext()
		wend

		rs.Close()
		conn.Close()

		CheckLogin = False
	end function

%>