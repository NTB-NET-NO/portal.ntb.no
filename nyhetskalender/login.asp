<%@ Language=VBScript %>

<%
	Dim conn
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")

	valg = Request("valg")
	
	if valg = "login" then
		
		sql = "SELECT * FROM Users WHERE username = '" & Request.Form("login") & "' AND password = '" & Request.Form("pwd") & "' AND Admin = 1"
		set rs = conn.Execute(sql)
		
		if not rs.EOF then
			Session("username") = rs("Username")
			Session("fullname") = rs("Fullname")
			Session("admin") = rs("Admin")
			
			Response.Redirect("search_event.asp")
		else
			message = "Passord/Brukernavn er feil. Pr�v igjen."
		end if
		
		rs.Close()
		
	elseif valg = "logout" then
		Session.Abandon()
		message = "Du er logget ut fra Nyhetskalenderen."	
	end if

	conn.Close()
%>


<HTML>
<HEAD>
<LINK rel="stylesheet" type="text/css" href="nyhetskalender.css">
<TITLE>NTB Nyhetskalender: Administrasjon</TITLE>
</HEAD>

<BODY class=viewBody>

<p class=eventIngress>Innlogging</p>


<p class=alert_message><%=message%></p>

<form action=login.asp method=post>

<table width=300 cellpadding=2 cellspacing=0 border=0 class=formText>
<tr><td>
	For � kunne legge inn i og endre NTB Nyhetskalender m� du logge inn f�rst. Kontakt IT-avdelingen ved problemer.
</td><tr>
</table>

<br>

<table width=300 cellpadding=2 cellspacing=0 border=1 class=formText borderColor="#61c7d7">
<tr>
	<td width=100 style="border:none;">Brukernavn</td>
	<td width=200 style="border:none;"><input type=text name=login size=20></td>
</tr>
<tr>
	<td width=100 style="border:none;">Passord</td>
	<td width=200 style="border:none;"><input type=password name=pwd size=20></td>
</tr>

</table>

<BR>

<table width=300 cellpadding=0 cellspacing=0 border=0 class=formText>
<tr><td align=right>
	<input type=hidden name=valg value=login>
	<input type=submit value="Logg inn" class=button>
</td><tr>
</table>

</form>

</BODY>
</HTML>
