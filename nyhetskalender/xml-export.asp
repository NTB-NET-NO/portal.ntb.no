<%@ Language=VBScript %>

<!--#include file=func_lib.asp-->

<%
	if not CheckLogin(false) then
		response.Status="401 Unauthorized"
		response.Write(response.Status)
		response.End
	end if

	'Return pure xml
	response.ContentType = "text/xml"

	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")

	'No modified since check by default
	modified_after = ""

	'Show everything one month ahead by default
	from_date = Date()
	to_date = DateAdd("d",30,Date())

	'No filter by default
	area_filter = 0
	category_filter = 0
	top = ""

	'Look for some params
	if Request("modifiedAfter") <> "" then in_modified_after = Request("modifiedAfter")

	if Request("fromDate") <> "" then in_from_date = Request("fromDate")
	if Request("toDate") <> "" then in_to_date = Request("toDate")

	if Request("area") <> "" then area_filter = Request("area")
	if Request("category") <> "" then category_filter = Request("category")

	if ( IsNumeric(Request("max")) and Request("max") <> "" ) then top = " TOP " & Request("max")


	'Parse incoming dates
	if in_modified_after <> "" then
		parts = split(in_modified_after,"T")

		blocks = split(parts(0),"-")
		dt = DateSerial(blocks(0),blocks(1),blocks(2))

		modified_after = "AND Modified >= '" & dt

		if (InStr(in_modified_after,"T") ) then
			blocks = split(parts(1),":")
			tm = TimeSerial(blocks(0),blocks(1),blocks(2))
			modified_after = modified_after & " " & tm & "' "
		else
			modified_after = modified_after & "' "
		end if
	end if

	if in_from_date <> "" then
		blocks = split(in_from_date,"-")
		from_date = DateSerial(blocks(0),blocks(1),blocks(2))
		to_date = DateAdd("d",30,from_date)
	end if

	if in_to_date <> "" then
		blocks = split(in_to_date,"-")
		to_date = DateSerial(blocks(0),blocks(1),blocks(2))
	end if


	sql = "SELECT" & top & " * FROM news_events WHERE endtime >= '" & from_date & "' AND starttime < '" & to_date & " 23:59:59' "

	if ( area_filter > 0 ) then sql = sql & "AND (area & " & area_filter & ") > 0 "
	if ( category_filter > 0 ) then sql = sql & "AND (category & " & category_filter & ") > 0 "

	sql = sql & modified_after
	sql = sql & "AND published = 1 "
	sql = sql & "ORDER BY starttime ASC"

	'Response.Write(sql)

	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient
	rs.Open sql,conn,adOpenDynamic,adLockOptimistic,adCmdText


	'The object to build content in
	set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")

	Set xmldec = xmlDoc.createProcessingInstruction("xml", "version=""1.0"" encoding=""iso-8859-1"" standalone=""yes""")
	xmlDoc.insertBefore xmldec, xmlDoc.documentElement

	xmlDoc.documentElement = xmlDoc.CreateElement("news-events")
	xmlDoc.documentElement.setAttribute "item-count",rs.RecordCount
	xmlDoc.documentElement.setAttribute "timestamp",TimestampSec(now)


	do while not rs.eof

		set eventNode = xmlDoc.CreateElement("news-event")

		eventNode.setAttribute "event-id",rs("EventID")
		eventNode.setAttribute "modified",TimestampSec(rs("Modified"))

		set propNode = xmlDoc.CreateElement("Subject")
		propNode.Text = rs("title")
		eventNode.appendChild propNode

		set propNode = xmlDoc.CreateElement("Body")
		propNode.Text = rs("description")
		eventNode.appendChild propNode

		duration = 0
		fullday = 1

		dt = rs("startTime")
		tm = "00:00:00"

		if ( InStr(dt," ") ) then
			part = split(dt," ")
			dt = part(0)
			tm = part(1)
			fullday = 0
		end if

		dblocks = split(dt,".")
		tblocks = split(tm,":")
		startdate = DateSerial(dblocks(2),dblocks(1),dblocks(0)) & " " & TimeSerial(tblocks(0),tblocks(1),tblocks(2))

		set propNode = xmlDoc.CreateElement("Start")
		propNode.Text = dblocks(2) & "-" & dblocks(1) & "-" & dblocks(0) & " " & tblocks(0) & ":" &  tblocks(1) & ":" & tblocks(2)
		eventNode.appendChild propNode


		dt = rs("endTime")
		tm = "23:59:59"

		if ( InStr(dt," ") ) then
			part = split(dt," ")
			dt = part(0)
			tm = part(1)
			fullday = 0
		else
			duration = 1
		end if

		dblocks = split(dt,".")
		tblocks = split(tm,":")
		enddate = DateSerial(dblocks(2),dblocks(1),dblocks(0)) & " " & TimeSerial(tblocks(0),tblocks(1),tblocks(2))

		set propNode = xmlDoc.CreateElement("End")
		propNode.Text = dblocks(2) & "-" & dblocks(1) & "-" & dblocks(0) & " " & tblocks(0) & ":" &  tblocks(1) & ":" & tblocks(2)
		eventNode.appendChild propNode

		set propNode = xmlDoc.CreateElement("AllDayEvent")
		propNode.Text = fullday
		eventNode.appendChild propNode

		duration = duration + DateDiff("n",startdate,enddate)

		set propNode = xmlDoc.CreateElement("Duration")
		propNode.Text = duration
		eventNode.appendChild propNode


		'-- Categories --'
		set areaNode = xmlDoc.CreateElement("Areas")
		set categoryNode = xmlDoc.CreateElement("Categories")

		Set rsCategory = Server.CreateObject("ADODB.RecordSet")
		rsCategory.CursorLocation = adUseClient
		sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (news_events a RIGHT JOIN bitnames b ON (a.area & b.category > 0 AND b.typeofname = 0) "

		if rs("category") <> -1 then
			sql = sql & "OR (a.category & b.category > 0 AND b.typeofname = 3) "
		end if

		sql = sql & ") WHERE a.eventid = " & rs("EventId") & " ORDER BY b.TypeOfName"
		rsCategory.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText

		do while not rsCategory.eof
			if rsCategory("TypeOfName") = 0 then
				set subNode = xmlDoc.CreateElement("Area")
				subNode.Text = rsCategory("DescriptiveName")
				areaNode.appendChild subNode
			else
				set subNode = xmlDoc.CreateElement("Category")
				subNode.Text = rsCategory("DescriptiveName")
				categoryNode.appendChild subNode
			end if
			rsCategory.MoveNext
		loop
		rsCategory.Close

		eventNode.appendChild areaNode
		eventNode.appendChild categoryNode


		'-- Subcategories --'
		set subCatNode = xmlDoc.CreateElement("Subcategories")

		if rs("HasSubRef") = -1 then
			Set rsSubCat = Server.CreateObject("ADODB.RecordSet")
			rsSubCat.CursorLocation = adUseClient
			sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (event_subrel a RIGHT JOIN bitnames b ON a.category = b.category AND a.subcategory = b.subcategory AND b.typeofname = 4) WHERE a.eventid = " & rs("EventId") & " ORDER BY b.TypeOfName"
			rsSubCat.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText

			do while not rsSubCat.eof
				set subNode = xmlDoc.CreateElement("Subcategory")
				subNode.Text = rsSubCat("DescriptiveName")
				subCatNode.appendChild subNode
				rsSubCat.MoveNext
			loop

			rsSubCat.Close
		end if

		eventNode.appendChild subCatNode


		'-- Countys
		Set rsCounty = Server.CreateObject("ADODB.RecordSet")
		rsCounty.CursorLocation = adUseClient

		sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (news_events a RIGHT JOIN bitnames b ON (a.fylke & b.subcategory) > 0 AND b.typeofname = 6) WHERE a.eventid = " & rs("EventId") & " ORDER BY b.TypeOfName"
		rsCounty.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText

		set propNode = xmlDoc.CreateElement("County")
		if not rsCounty.EOF then
			propNode.Text = rsCounty("DescriptiveName")
		end if
		eventNode.appendChild propNode
		rsCounty.close


		'-- Other stuff
		set propNode = xmlDoc.CreateElement("Address")
		if ( rs("Address") <> DBnull ) then propNode.Text = rs("Address")
		eventNode.appendChild propNode

		set propNode = xmlDoc.CreateElement("Contact")
		if ( rs("Contact") <> DBnull ) then propNode.Text = rs("Contact")
		eventNode.appendChild propNode

		set propNode = xmlDoc.CreateElement("ContactEmail")
		if ( rs("ContactEmail") <> DBnull ) then propNode.Text = rs("ContactEmail")
		eventNode.appendChild propNode

		set propNode = xmlDoc.CreateElement("ContactPhone")
		if ( rs("ContactPhone") <> DBnull ) then propNode.Text = rs("ContactPhone")
		eventNode.appendChild propNode

		set propNode = xmlDoc.CreateElement("URL")
		if ( rs("URL") <> DBnull ) then propNode.Text = rs("URL")
		eventNode.appendChild propNode

		xmlDoc.documentElement.appendChild (eventNode)

		rs.MoveNext
	loop

	xmlDoc.save(Response)

	rs.Close
	conn.Close

%>