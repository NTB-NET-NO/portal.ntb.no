<%@ Language=VBScript %>

<!--#include file=date_func.asp-->

<%
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")
	
	valg = Request.Form("valg")
	view = Request.Form("view")
	
	if view = "" then view = "long"
	
	buildquery = false
	category = 0
	county = 0
	area = 0
	in_time = 7
	
	'Load data from the form
	if valg = "search" then
		
		set in_area = Request.Form("area")
		set in_county = Request.Form("county")
		set in_category = Request.Form("category")
		set in_subCategory = Request.Form("subcategory")
		
		in_textstring = Trim(Replace(Request.Form("textstring"),"'"," "))
		in_time = CInt(Request.Form("time"))
		in_from_date = Request.Form("from_date")
		in_to_date = Request.Form("to_date")

		buildquery = true
		
	'Or load a saved search if any
	elseif Session("saved_search") and Session("load_saved") then
	
		area = Session("area")
		county = Session("county")
		category = Session("category")
		
		in_textstring = Session("textstring")
		in_time = Session("time")
		in_from_date =Session("from_date")
		in_to_date = Session("to_date")
	
		buildquery = true
	
	end if
	
	dim sql
	sql = ""
		
	if buildquery then
	
		'Build custom query
		sql = "SELECT DISTINCT a.* FROM ( news_events a LEFT JOIN event_subrel b ON a.eventID = b.eventid ) WHERE "

		'Area (Stoffgruppe)
		if IsObject(in_area) then 
			For catCount = 1 to in_area.Count
				area = area or in_area(catCount)
			Next
			if area = 0 then area = -1
		
			'County
			For catCount = 1 to in_county.Count
				county = county or in_county(catCount)
			Next
			if county = 0 then county = -1

			'Main category
			For catCount = 1 to in_category.Count
				category = category or in_category(catCount)
			Next
			if category = 0 then category = -1
		end if
				
		'Area and category		
		sql = sql & "(a.area & " & area & ") > 0 " 
	
		sql = sql & "AND ( (a.category & " & category & ") > 0 "
		if category = -1 then sql = sql & "OR a.category = 0 "
		sql = sql & ") " 
	
		sql = sql & "AND ( (a.fylke & " & county & ") > 0 "
		if county = -1 then sql = sql & "OR a.fylke = 0 "
		sql = sql & ") " 
			
		'Free tekst search
		if in_textstring <> "" then
			sql = sql & "AND (a.title LIKE '%" & in_textstring & "%' OR a.description LIKE '%" & in_textstring & "%') "
		end if
			
		'Time specification
		if in_time > 0 then
		
			dim fra
			dim til
			
			fra = DateSerial(Year(Date()),Month(Date()),Day(Date()))
			if in_time = 30 then 
				fra = DateAdd("d",1,fra)
			end if
			til = DateAdd("d",in_time,fra)			
			
			sql = sql & "AND a.endtime >= '" & fra & "' AND a.starttime < '" & til & "' "

		elseif in_time = -1 then
			'Use specific dates
			dim blocks
					
			if in_from_date <> "" then 
				blocks = split(in_from_date,".")
				sql = sql & "AND a.endtime >= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & "' "
			end if
						
			if in_to_date <> "" then 
				blocks = split(in_to_date,".")
				sql = sql & "AND a.starttime <= '" & DateSerial(blocks(2),blocks(1),blocks(0)) & " 23:59' "
			end if
		end if
						
		'Subcategories
		if IsObject(in_subCategory) then	
			if in_subCategory.Count > 0 then

				sql = sql & "AND ( " 			

				For catCount = 1 to in_subcategory.Count
				
					if catCount > 1 then sql = sql & "OR "
					tmp = Split( in_subcategory(catCount), "_" )
					sql = sql & "( b.category = " & tmp(0) & " AND b.subcategory = " & tmp(1) & " ) "
			  
				Next

				sql = sql & ") " 			
			end if
		end if
			
		sql = sql & "ORDER BY starttime ASC"
		
		'Store in session here
		if not Session("load_saved") then
			Session("saved_search") = true 
			Session("area") = area
			Session("county") = county
			Session("category") = category
		
			Session("textstring") = in_textstring
			Session("time") = in_time
			Session("from_date") = in_from_date
			Session("to_date") = in_to_date
		else
			Session("load_saved") = false 
		end if

	else		
		'Show everything one week ahead by default -  
		sql = "SELECT * FROM news_events WHERE endtime >= '" & Date() & "' AND starttime < '" & DateAdd("d",in_time,Date()) & "' ORDER BY starttime ASC"
		Session("load_saved") = false 
		Session("saved_search") = false
	end if

	'Reset the vars for remembering the settings in the form
	if area = 0 then area = -1
	if county = 0 then county = -1
	if category = 0 then category = -1

	'Response.Write(sql)
	'Response.Flush()
	
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient
	rs.Open sql,conn,adOpenDynamic,adLockOptimistic,adCmdText

%>
<HTML>
<HEAD>

<LINK rel="stylesheet" type="text/css" href="nyhetskalender.css">

<SCRIPT LANGUAGE="JavaScript">
<!--

// Arrays containing subcategories
<%
	dim rsCategory
	dim rsSubCategory
	Set rsCategory = Server.CreateObject("ADODB.Recordset")
	Set rsSubCategory = Server.CreateObject("ADODB.Recordset")

	Response.Write("var subcat = new Array(" & vbCrLf)
	
	rsCategory.Open "Select * from BITNAMES where typeofname = 3 order by DescriptiveName asc", conn
	
	do while not rsCategory.eof
		
		rsSubCategory.Open "Select * from bitnames where typeofname = 4 and category = " & rsCategory("category") & " order by DescriptiveName asc", conn
		
		Response.Write(vbTab & "new Array(")
		
		do while not rsSubCategory.eof
		
			Response.Write("'" & rsCategory("category") & "_" & rsSubCategory("subcategory") & "','" & rsSubCategory("DescriptiveName") & "'")
				
			rsSubCategory.MoveNext()

			if not rsSubCategory.eof then
				Response.Write(",")
			end if
		loop
	
		rsSubCategory.Close()
		
		rsCategory.MoveNext()
		if not rsCategory.eof then 
			Response.Write(")," & vbCrLf)
		else  
			Response.Write(")" & vbCrLf)
		end if
		
	loop
	
	Response.Write(");")
	
	rsCategory.Close()
	
%>

//Display details in a separate window
var popupWnd;

function vis(EventID) {
	var url, name, options, leftPlacement;

	url = 'viewEvent.asp?EventId=' + EventID;

	if(popupWnd && !popupWnd.closed) { // window is open
		popupWnd.location = url;
		popupWnd.focus();
	} else {
		leftPlacement = screen.width - 610; //ensures that the window pops up to the right independent on screen resolution
		name = 'PopupPage';
		options = 'height=600,width=600,scrollbars=yes,resizable=yes,top=80, left='+ leftPlacement;
		popupWnd = window.open(url, name, options);
		popupWnd.focus();
	}
}


//function to validate the different search options prior to submitting the form
function onSubmit_ValidateForm(activeform) {

	//New datecheck
	if ( activeform.time.options[activeform.time.selectedIndex].text == "Spesifiser tidsrom") {

		//New date check
		rx = /^\d{2}\.\d{2}\.\d{4}$/;
		fd = activeform.from_date.value;
		td = activeform.to_date.value;

		//Check dates
		if ( fd == "" && td == "" ) {
			alert("N�r du velger � spesifisere dato, m� disse fylles inn i datofeltene.");
			activeform.to_date.focus();
			return false;
		} 
		else if ( ! rx.test(fd) && fd != "" ) {
			alert("Du har angitt en ugyldig 'Fra'-dato.");
			activeform.from_date.focus();
			return false;
		} 
		else if ( ! rx.test(td) && td != "" ) {
			alert("Du har angitt en ugyldig 'Til'-dato.");
			activeform.to_date.focus();
			return false;
		}
		else {
			
			var fa = new Array(3);
			var ta = new Array(3);
			var dt = new Date();
			
			//Verify from date
			if ( fd != "" ) { 
				fa = fd.split("."); 
				
				if ( fa[0] > 31 || fa[1] > 12 || fa[0] < 1 || fa[1] < 1 ) {
					alert("Du har angitt en ugyldig 'Fra'-dato.");
					activeform.from_date.focus();
					return false;
				}
			}

			if ( td != "" ) { 
				ta = td.split("."); 
			
				if ( ta[0] > 31 || ta[1] > 12 || ta[0] < 1 || ta[1] < 1 ) {
					alert("Du har angitt en ugyldig 'Til'-dato.");
					activeform.to_date.focus();
					return false;
				}
			
			}
			
			if ( fd != "" && td != "" ) {
				
				if ( fa[2]+fa[1]+fa[0] > ta[2]+ta[1]+ta[0] ) {
					alert("Du har angitt et ugyldig tidsintervall. 'Fra'-dato m� v�re mindre enn 'Til'-dato!");
					activeform.to_date.focus();
					return false;
				}
			}			
		}
	}

	return true;
}


function areaSelected(form) {

	if ( form.elements.area.selectedIndex >= 0 ) {
		
		var mgOptionText = form.elements.area.options[form.elements.area.selectedIndex].text;
		var flag = false;
		var mcOptionText;
		
		for(i=0; i < form.elements.category.length; i++)
		{
			mcOptionText = form.elements.category.options[i].text
			if (mcOptionText == mgOptionText)
			{
				flag = true;
				form.elements.category.options[i].selected = true;
			} else {
				form.elements.category.options[i].selected = false;
			}
		}
	
		if ( ! flag ) //selected option in maingroup doesn't match any group in maincategory
		{
			form.elements.category.options[0].selected = true;
		}
	
		fillSubcategories(form)	
	}
}

function fillSubcategories(form)
{
	form.subcategory.selectedIndex=-1;
	form.subcategory.options.length=0;
	
	locs = form.subcategory.options;
	
	for ( c = 1; c < form.category.options.length; c++ ) {
		
		if (form.category.options[c].selected) {

			for ( lc = 0; lc < subcat[c-1].length; lc += 2) {
				locs[locs.length] = new Option(subcat[c-1][lc+1],subcat[c-1][lc]);
			}	
		}
	}
}

function autoFillDate(datefield) {

	var text = datefield.value;
	var d = new Date();
	var year;

	//Substitute separators
	rxSub = /[- \/]/g;
	text = text.replace(rxSub,".");

	//Test for missing zeros
	rxZ1 = /^(\d{1})$/;
	rxZ2 = /^(\d{1})(\..+)/;
	rxZ3 = /(.+\.)(\d{1})$/;
	rxZ4 = /(.+\.)(\d{1})(\..+)/;

	text = text.replace(rxZ1,"0$1");
	text = text.replace(rxZ2,"0$1$2");
	text = text.replace(rxZ3,"$10$2");
	text = text.replace(rxZ4,"$10$2$3");

	//Test for autofilling of dates
	rx1 = /^\d{2}$/;
	rx2 = /^\d{2}\.\d{2}$/;
	rx3 = /^(\d{2}\.\d{2}\.)(\d{2})$/;
	rx4 = /^\d{2}\.\d{2}\.\d{4}$/;

	if ( rx4.test(text) ) {
		//do nothing
	} else if ( rx3.exec(text) ) {
		year = RegExp.$2;
		if (year < 50) year = 20 + year;
		else if (year < 100) year = 19 + year;
			
		text = text.replace(rx3,"$1" + year);
		
	} else if ( rx2.test(text) ) {
		text += "." + d.getFullYear();
	} else if ( rx1.test(text) ) {
		var m = (d.getMonth()+1)
		if ( m < 10 ) m = "0" + m;
		text += "." + m + "." + d.getFullYear();
	}
	
	datefield.value = text;
}

//Blank datefield upon other selection
function timeTypeChanged(form){

	if (form.time.options[form.time.selectedIndex].text != "Spesifiser tidsrom") {

		form.from_date.value = "";
		form.to_date.value = "";

	}
}

//Autoselects specify time in the time selectbox
function setSpecifyTime(idx){
	document.searchform.time.selectedIndex = idx;
}

function loadParams() {
	
	document.searchform.textstring.value = '<%=in_textstring%>';
	document.searchform.from_date.value = '<%=in_from_date%>';
	document.searchform.to_date.value = '<%=in_to_date%>';
		
	for ( i = 0; i < document.searchform.time.options.length; i++ ) {
		
		if ( document.searchform.time.options[i].value == '<%=in_time%>' ) {
			document.searchform.time.selectedIndex = i;
			break;
		}
	}

	if ( <%=area%> == -1 ) document.searchform.area.selectedIndex = 0;
	else {
		for ( i = 1; i < document.searchform.area.options.length; i++ ) {
			
			if ( (document.searchform.area.options[i].value & <%=area%>)) {
				document.searchform.area.options[i].selected = true;
			}
		}
	}

	if ( <%=county%> == -1 ) document.searchform.county.selectedIndex = 0;
	else {
		for ( i = 1; i < document.searchform.county.options.length; i++ ) {
			
			if ( (document.searchform.county.options[i].value & <%=county%>)) {
				document.searchform.county.options[i].selected = true;
			}
		}
	}
	
	if ( <%=category%> == -1 ) document.searchform.category.selectedIndex = 0;
	else {
		for ( i = 1; i < document.searchform.category.options.length; i++ ) {
			
			if ( (document.searchform.category.options[i].value & <%=category%>)) {
				document.searchform.category.options[i].selected = true;
			}
		}
	}
	
	fillSubcategories(document.searchform);

	//Subcategories	
	<%
	'Use serverside script to generate some client side stuff here
	if IsObject(in_subCategory) then
		For catCount = 1 to in_subCategory.Count
		%>
			for ( i = 0; i < document.searchform.subcategory.options.length; i++ ) {
				
				if ( (document.searchform.subcategory.options[i].value == '<%=in_subCategory(catcount)%>' )) {
					document.searchform.subcategory.options[i].selected = true;
				}
			}
		<%
		Next
	end if
	%>
	
}

function reload(str)
{
	document.searchform.view.value = str;
	document.searchform.submit();
}

function print_list()
{
	//Set the printpage
	document.searchform.action = 'print_list.asp';
	document.searchform.target = '_blank';
	document.searchform.submit();
	
	//Reset
	document.searchform.action = 'search_event.asp';
	document.searchform.target = '';
}

//Delete event from the calendar
function deleteEvent(id)
{
	if ( window.confirm("Den valgte hendelsen vil bli slettet.\nKlikk 'OK' for � fortsette eller 'Avbryt' for � avbryte") ) {
		location.href = 'delete.asp?valg=slett&eventid=' + id
	}
}

//-->
</script>

</HEAD>

<BODY onLoad="loadParams()">

<a href="#sok" class=redLink>S�k etter hendelser</a> | <a href="javascript:reload('short')" class=redLink>Kort liste</a> | <a href="javascript:reload('long')" class=redLink>Lang liste</a> | <a href="javascript:print_list()" class=redLink>Utskriftsvennlig liste</a>
<P>
<table width=600 cellpadding=0 cellspacing=0 border=0 class=eventNews>

	<%
	if rs.EOF then
		'No hits
	%>
		<tr><td>Ingen hendelser funnet.<p></td></tr>
		
	<%	
	else
	
		do while not rs.eof
		%>
		

		<%
		if Session("admin") then
		%>
			<tr>
			<td class=formtext><a href='add_event.asp?valg=edit&eventid=<%=rs("EventID")%>' class=redLink>Rediger</a> | <a href='javascript:deleteEvent(<%=rs("EventID")%>)' class=redLink>Slett</a> | Lagt inn av: <%=rs("AddedBy")%>
			</td>
			</tr>
		<%
		end if	
		%>

		<tr>
		<td class=eventHeadline><a href="viewEvent.asp?EventId=<%=rs("EventID")%>" onClick="vis(<%=rs("EventID")%>);return false;"><%=rs("title")%></a>
		</td>
		</tr>
	
			
		<tr>
		<td class=eventIngress>
		<%=LongDisplayDate(rs("startTime"))%>
		<%
		if FormatDateTime(rs("startTime"),vbShortTime) <> "00:00" then
			Response.Write(" kl. " & FormatDateTime(rs("startTime"),vbShortTime) )
		end if		
			
		if DateDiff("d",rs("startTime"),rs("endTime")) > 0 or FormatDateTime(rs("endTime"),vbShortTime) <> "00:00" then
			Response.Write(" - ")
			doKl = false
				
			if DateDiff("d",rs("startTime"),rs("endTime")) > 0 then
				Response.Write(LongDisplayDate(rs("endTime")))
				doKl = true
			end if		
			
			if FormatDateTime(rs("endTime"),vbShortTime) <> "00:00" then
				if doKl then Response.Write(" kl. ")
				Response.Write(FormatDateTime(rs("endTime"),vbShortTime))
			end if		
		end if
		%>
		</td>
		</tr>

		<%if rs("address") <> "" then%>
		<tr>
		<td class=eventIngress>
		Sted: <%=rs("address")%>
		</td>
		</tr>
		<%end if%>

		
		<%
		contact = ""
		if rs("contact") <> "" and view = "short" then
			if rs("contactEmail") <> "" then
				contact = "<a href='mailto:" & rs("contactEmail") & "'>" & rs("contact") & "</a>"
			else 
				contact =  rs("contact")
			end if
		%>
			<tr>
			<td class=eventNews>
			<span class=eventIngress>Kontakt:</span>
			<%=contact%>
			</td>
			</tr>
		<%elseif view = "long" and ( rs("contact") <> "" or rs("contactEmail") <> "" or rs("contactPhone") <> "" or rs("url") <> "" ) then
			dash = false
			if rs("contact") <> "" then
				if rs("contactEmail") <> "" then
					contact = rs("contact") & " ( <a href='mailto:" & rs("contactEmail") & "'>" & rs("contactEmail") & "</a> )"
				else 
					contact = rs("contact")
				end if
				dash = true
			end if
			
			if rs("contactphone") <> "" then
				if dash then contact = contact & "&nbsp;-&nbsp;"
				contact = contact & rs("contactphone")
				dash = true
			end if
			
			url = rs("url")
			if  url <> "" then
				if ( url <> "" and InStr(1,url,"http://") <> 1 ) then
					url = "http://" & url
				end if

				if dash then contact = contact & "&nbsp;-&nbsp;"
				contact = contact & " <a href='" & url & "' target='_blank'>" & url & "</a>"
			end if
	
		%>
			<tr>
			<td class=eventNews>
			<span class=eventIngress>Kontakt:</span>			
			<%=contact%>
			</td>
			</tr>
				
		<%end if%>

		<%
		if view = "long" then
		%>
			<tr>
			<td><%=Replace(rs("Description"),vbcrlf,"<BR>")%></td>
			</tr>
		<%
		end if
		%>

		<tr>
		<td>&nbsp;</td>
		</tr>
	
		<%
		rs.MoveNext()
		loop
		%>	

	<%
	end if
	%>

</table>

<hr size=1 width=600 align=left>
<a name=sok class=eventIngress>S�k etter hendelser</a>
<form name=searchform method=post action=search_event.asp onSubmit="return onSubmit_ValidateForm(this)">

<table  borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td style="border:none;">
	S�k i fritekst i tittel og beskrivelse<BR>
	<Input type=text name=textstring size=25 onKeyPress="setSpecifyTime(4)" style="width: 594px">
	</td>
</tr>
</table>

<BR>

<table borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=200 style="border:none;">
	Tidsrom<BR>
	<select name=time style="width: 195px" onChange="timeTypeChanged(this.form)">
	<option value="2">I dag og i morgen</option>
	<option value="7">Neste 7 dager</option>
	<option value="30">Neste 30 dager</option>
	<option value="-1">Spesifiser tidsrom</option>
	<option value="0">Alle hendelser</option>
	</select>
	</td>

	<td width=200 style="border:none;">
	Fra (Format: dd.mm.����)
	<BR>
	<Input type=text name=from_date size=25 maxlength=10 onKeyPress="setSpecifyTime(3)" onBlur="autoFillDate(this)">
	</td>
	
	<td width=200 style="border:none;">
	Til (Format: dd.mm.����)
	<BR>
	<Input type=text name=to_date size=25 maxlength=10 onKeyPress="setSpecifyTime(3)" onBlur="autoFillDate(this)">
	</td>

</tr>

</table>

<BR>

<table  borderColor="#61c7d7" width=600 cellpadding=2 cellspacing=0 border=1 class=formText>
<tr>
	<td width=135 style="border:none;" class=formtext>
	Omr�de
	<BR>
	<select name=area size=5 multiple style="width: 130px" onChange="areaSelected(this.form)">
		<option value=-1>Alle</option>
	<%
	
		dim rsArea
		Set rsArea = Server.CreateObject("ADODB.Recordset")
		rsArea.Open "Select * from BITNAMES where typeofname = 0 and category <= 4 order by displaysort asc", conn 

		do while not rsArea.EOF
		%>

			<option value="<%=rsArea("Category")%>"><%=rsArea("DescriptiveName")%></option>

		<%
		rsArea.Movenext()
		loop
		rsArea.close

	%>
	</select>
	</td>


	<td width=150 style="border:none;" class=formtext>
	Fylke
	<br>
	<Select name=county size=5 multiple style="width: 145px">
		<option value="-1">Alle</option>
		<%
	
			dim rsCounty
			Set rsCounty = Server.CreateObject("ADODB.Recordset")
			rsCounty.Open "Select * from BITNAMES where typeofname = 6 order by DescriptiveName asc", conn 

			do while not rsCounty.EOF
			
				if rsCounty("DescriptiveName") <> "Riksnyheter" then
				%>
					<option value="<%=rsCounty("SubCategory")%>"><%=rsCounty("DescriptiveName")%></option>
				<%
				end if
				
			rsCounty.Movenext()
			loop
			rsCounty.close

		%>
	</select>
	</td>


	<td width=165 style="border:none;">
	Kategori
	<BR>
	<select name=category size=5 multiple style="width: 160px" onChange="fillSubcategories(this.form)">
		<option value=-1>Alle</option>
	<%
	
		dim rsCat
		Set rsCat = Server.CreateObject("ADODB.Recordset")
		rsCat.Open "Select * from BITNAMES where typeofname = 3 order by DescriptiveName asc", conn 

		do while not rsCat.EOF
		%>

			<option value="<%=rsCat("category")%>"><%=rsCat("DescriptiveName")%></option>

		<%
		rsCat.Movenext()
		loop
		rsCat.close

	%>
	</select>
	</td>

	<td width=150 style="border:none;">
	Underkategori
	<BR>
	<select name=subcategory size=5 multiple style="width: 145px">
	</select>
	</td>
</tr>

<tr>
	<td colspan="3" style="border:none;" class="hjelpetekst">&nbsp;Hold CTRL nede for � velge flere av gangen (Apple-knappen for Mac-brukere)</td>
	<td align=right style="border:none;" class="hjelpetekst"><input type=submit value="  S�k  " class=button></td>
</tr>
</table>

<input type=hidden value=<%=view%> name=view>
<input type=hidden value=search name=valg>
</form>


<!--%=sql%-->

<%
rs.Close
conn.close
%>
</BODY>
</HTML>
