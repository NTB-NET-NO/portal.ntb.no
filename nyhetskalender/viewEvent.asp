<%@ Language=VBScript %>

<!--#include file=date_func.asp-->

<%
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open Application("News_Events_Connectionstring")

	id = Request("EventId")

	'Get the event data
	Set rsRetrieve = Server.CreateObject("ADODB.RecordSet")
	Set rsCounty = Server.CreateObject("ADODB.RecordSet")
	Set rsCategory = Server.CreateObject("ADODB.RecordSet")
	Set rsSubCat = Server.CreateObject("ADODB.RecordSet")
	
	rsRetrieve.CursorLocation = adUseClient
	rsCategory.CursorLocation = adUseClient
	rsSubCat.CursorLocation = adUseClient

	rsRetrieve.Open "SELECT * FROM news_events WHERE EventID = " & id, conn, adOpenDynamic, adLockOptimistic, adCmdText

	if not rsRetrieve.EOF then
	'Got a record
		
		sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (news_events a RIGHT JOIN bitnames b ON (a.area & b.category > 0 AND b.typeofname = 0) "
		
		if rsRetrieve("category") <> -1 then
			sql = sql & "OR (a.category & b.category > 0 AND b.typeofname = 3) "
		end if
		
		sql = sql & ") WHERE a.eventid = " & rsRetrieve("EventId") & " ORDER BY b.TypeOfName"
		
		rsCategory.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText

		sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (news_events a RIGHT JOIN bitnames b ON (a.fylke & b.subcategory) > 0 AND b.typeofname = 6) WHERE a.eventid = " & rsRetrieve("EventId") & " ORDER BY b.TypeOfName"
		rsCounty.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText

		if rsRetrieve("HasSubRef") = -1 then

			sql = "SELECT DISTINCT b.DescriptiveName, b.TypeOfName FROM (event_subrel a RIGHT JOIN bitnames b ON a.category = b.category AND a.subcategory = b.subcategory AND b.typeofname = 4) WHERE a.eventid = " & rsRetrieve("EventId") & " ORDER BY b.TypeOfName"

			rsSubCat.Open sql, conn, adOpenDynamic, adLockOptimistic, adCmdText
	
		end if
	
	end if

%>

<HTML>
<HEAD>
<title>NTB Nyhetskalender</title>
<LINK rel="stylesheet" type="text/css" href="nyhetskalender.css">
</HEAD>
<BODY>

<div class="eventHeadLine"><%=rsRetrieve("title")%></div>
<BR>
<div class="eventIngress">Tidspunkt: <%=LongDisplayDate(rsRetrieve("startTime"))%>
	<%
	if FormatDateTime(rsRetrieve("startTime"),vbShortTime) <> "00:00" then
		Response.Write(" kl. " & FormatDateTime(rsRetrieve("startTime"),vbShortTime) )
	end if		
		
	if DateDiff("d",rsRetrieve("startTime"),rsRetrieve("endTime")) > 0 or FormatDateTime(rsRetrieve("endTime"),vbShortTime) <> "00:00" then
		Response.Write(" - ")
		doKl = false
			
		if DateDiff("d",rsRetrieve("startTime"),rsRetrieve("endTime")) > 0 then
			Response.Write(LongDisplayDate(rsRetrieve("endTime")))
			doKl = true
		end if		
		
		if FormatDateTime(rsRetrieve("endTime"),vbShortTime) <> "00:00" then
			if doKl then Response.Write(" kl. ")
			Response.Write(FormatDateTime(rsRetrieve("endTime"),vbShortTime))
		end if		
	end if		
	%></div>

<%
	
	if rsRetrieve("address") <> "" then
		location = "Sted: " & rsRetrieve("address")
	end if 
	
	if not rsCounty.EOF then
		if location = "" then 
			location = "Sted: " & rsCounty("DescriptiveName")
		else
			location = location & ", " & rsCounty("DescriptiveName")
		end if
	end if 

%>

<div class="eventIngress"><%=location%></div>
<div class="eventIngress"><%=county%></div>

<hr size=1 width=500 align=left>

<table width=400 cellspacing=0 cellpadding=0 border=0 class="eventNews">
<tr><td><%=rsRetrieve("description")%></td></tr>
</table>

<%if rsRetrieve("contact") <> "" or rsRetrieve("contactEmail") <> "" or rsRetrieve("contactPhone") <> "" or rsRetrieve("url") <> "" then %>
	<div class="eventNews">&nbsp;</div>
	<div class="eventIngress">Kontaktinformasjon</div>

	<%
		nameStr = rsRetrieve("contact")
		if rsRetrieve("contactEmail") <> "" then
			nameStr = nameStr & " ( <a href='mailto:" & rsRetrieve("contactEmail") & "'>" & rsRetrieve("contactEmail") & "</a> )"		
		end if
	%>

	<div class="eventNews"><%=nameStr%></div>
	<div class="eventNews"><%=rsRetrieve("contactPhone")%></div>
	
	<%
		url = rsRetrieve("url")
		if  url <> "" then
			if ( url <> "" and InStr(1,url,"http://") <> 1 ) then
				url = "http://" & url
			end if

			url = "<a href='" & url & "' target='_blank'>" & url & "</a>"
		end if
	%>

	<div class="eventNews"><%=url%></div>
<%end if%>

<div class="eventNews">&nbsp;</div>
<div class="eventIngress">Kategorisering</div>

<%

	do while not rsCategory.EOF
		Response.Write("<div class='eventNews'>" & rsCategory("DescriptiveName") & "</div>")
		rsCategory.MoveNext()
	loop

	if rsRetrieve("HasSubRef") = -1 then
		do while not rsSubCat.EOF
			Response.Write("<div class='eventNews'>" & rsSubCat("DescriptiveName") & "</div>")
			rsSubCat.MoveNext()
		loop
	end if

%>

</BODY>

<%

if rsRetrieve("HasSubRef") = -1 then rsSubCat.Close
rsCategory.Close

rsRetrieve.Close

conn.Close

%>
</HTML>
