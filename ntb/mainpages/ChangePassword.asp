<%
'***********************************************************************************
'* ChangePassword.asp
'*
'* This file is used to force the user to change the password
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.16
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.25
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<%	Sub main()

	End sub
	
	Dim message, change, save, try, org, page, idOrg

	message = Request("print")
	change = Request("option")
	save = Request("changed")
	try = Request("error")
	org = Request("org")
	idOrg = Request("idOrg")
	page = Request("page")
	
	if message = "message" then
	'show message that the password should be change soon
		PrintMessage
	
	elseif message = "change" then
	'give the user a screen to to decide whether to change password or not
		PrintChangeOptionMessage
	
	elseif change = "yes" then
	'if the user wants to change password now
		PrintChangePassword
	
	elseif change = "no" then
	'if the user does not want to change password
		if page = "admin" then
			if Session("Role") = "Super" then
				Response.Redirect "UserList.asp"
			elseif Session("Role") = "Admin" then
				Response.Redirect "CompanyList.asp"
			else
				Response.Redirect "User_homepage.asp"
			end if
		else
			Response.Redirect "User_homepage.asp"
		end if
	
	elseif save = "password" then
	'if the user has changed the password
		Dim strNewPass1, strNewPass2, strNewPass, strOldPass
		
		strNewPass1 = Request("txtPassword1")
		strNewPass2 = Request("txtPassword2")
		strOldPass = Request("txtOldPassword")
		
		if (strNewPass1 = strNewPass2) then
		'if the passwords are similar
			
			if Len(strNewPass1) < 8 then
			'check that the new password is 8 or more
			Response.Redirect "ChangePassword.asp?option=yes&error=again"
			end if
			
			if idOrg = "" then 
				idOrg = GetCompany(Session("UserName"))
			end if
		
			if idOrg <> "" then				
				'since we are using AD we must provide an array with the old and the new password
				strNewPass = Array(strOldPass, strNewPass1)

				Call ChangePassword(strNewPass, idOrg)
				
				Response.Redirect "PasswordReceit.asp"

			else
				Response.Redirect "Login.asp"
			end if
			
		else
		'the passwords are not the same or the original password given is not correct
			Response.Redirect "ChangePassword.asp?option=yes&error=again"
		
		end if
	
	end if
%>

<%Sub PrintMessage() %>
<HTML>
<HEAD>
<%=Titletag()%>
<LINK href="../include/ntb.css" type="text/css" rel="stylesheet">
</HEAD>
<BODY>
<br><br>
<table border="0" width="40%" align="center">
<FORM NAME="frmMessage" ACTION="User_homepage.asp" Method="post">
<tr>
	<td><%=RenderTitleBar("Melding", "NoPhoto", 602)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<div class="news_heading">
			Det er n� p� tide � skifte passord for <%=org%>
		</div>
		<br>
		<div class="main_heading2">
			For � gj�re dette m� du logge p� som lokal administrator
		</div>
		<br>
	</td>
</tr>
<tr>
	<td align="center">
		<input type="submit" class="formbutton" name="action" value="OK" style="background:#bfccd9 none; color:#003366; width:75px">	
	</td>			
</tr>
</table>
</BODY>
</HTML>
<%End Sub%>


<%Sub PrintChangeOptionMessage()%>
<HTML>
<HEAD>
<%=Titletag()%>
<LINK href="../include/ntb.css" type="text/css" rel="stylesheet">
<script language="Javascript">
function SetAction(par){
	document.frmChange.option.value = par;
	
	if (par == "no") {
		nextPage = confirm("Vil du g� til brukeradministrasjon?")
		if (nextPage){
			document.frmChange.page.value = "admin";
		}
		else {
			document.frmChange.page.value = "ordinary";
		}
	}
	document.frmChange.submit();
}
</script>
</HEAD>
<BODY>
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td><%=RenderTitleBar("Endre passord?", "NoPhoto", 602)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<FORM NAME="frmChange" ACTION="ChangePassword.asp" METHOD="post">
		<div class="news_heading">
			Det er n� p� tide � skifte passord for <%=org%>
		</div>
		<div class="news_heading">Vil du skifte passord n�?</div>
		<br>
		<input type="hidden" name="page"> 
		<input type="hidden" name="option"> 
		<input type="button" class="formbutton" name="yes" value="Ja" style="background:#bfccd9 none; color:#003366; width:80px" onclick = "Javascript:SetAction('yes')">	
		<input type="button" class="formbutton" name="no"  value="Nei" style="background:#bfccd9 none; color:#003366; width:80px" onclick = "Javascript:SetAction('no')"> 
		</form>		
	</td>
</tr>
</table>
</BODY>
</HTML>
<%End sub%>


<%Sub PrintChangePassword() %>
<HTML>
<HEAD>
<LINK href="../include/ntb.css" type="text/css" rel="stylesheet">
<%=Titletag()%>
<script language="Javascript">

function CheckInput(){
		var tmpString = document.frmChangePW.txtPassword1.value
		
		if ((document.frmChangePW.txtPassword1.value == "") || (document.frmChangePW.txtPassword2.value == "")){
			alert ("Passordet m� skrives inn i begge boksene.");
		}
		else if (document.frmChangePW.txtPassword1.value != document.frmChangePW.txtPassword2.value) {
			alert ("Det nye passordet m� skrives p� samme m�te i begge boksene.");
		}
		else if (tmpString.length < 8){
			alert ("Passordet m� v�re p� minst 8 tegn.");
		}	
		else {
			document.frmChangePW.submit();
		}
	}

</script>
</HEAD>

<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td>
		<%=RenderTitleBar("Endre passord", "NoPhoto", 620)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<table border="0">
			<FORM NAME="frmChangePW" ACTION="ChangePassword.asp" METHOD="post">
			<%if try = "again" then%>
			<tr>
				<td colspan="2" height="40" align="center">
					<div class="main_heading2">Feil inntasting av passord. Husk at passordet m� v�re p� minimum 8 tegn</div>
				</td>
			</tr>
			<%end if%>
			<tr>
				<td colspan="2" height="40">
					<div class="news11">Tast inn ditt gamle passord og det nye passordet til
					brukerne.</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="main_heading2">Tast inn ditt gamle passord:
				</td>
				<td>
					<INPUT TYPE="password" NAME="txtOldPassword" class="news11" MAXLENGTH="15" style="width:70">
				</td>
			</tr>
			<tr>
				<td>
					<div class="main_heading2">Tast inn nytt passord: 
				</td>
				<td align="left">
					<INPUT TYPE="password" NAME="txtPassword1" class="news11" MAXLENGTH="15" style="width:70">
				</td>
			</tr>
			<tr>
				<td>
					<div class="main_heading2">Tast ditt nye passord igjen: 
				</td>
				<td>
					<INPUT TYPE="password" NAME="txtPassword2" class="news11" MAXLENGTH="15" style="width:70">
				</td>
			</tr>
			<tr><td colspan="2">&#160;</td></tr>
			<tr>
				<td colspan="2" align="center" height="60">	
					<input type="hidden" name="idOrg" value="<%=idOrg%>">
					<input type="hidden" name="changed" value="password">
					<input type="reset" class="formbutton" name="action" value="T�m felt" style="background:#bfccd9 none; color:#003366; width:90px"> 
					<input type="button" class="formbutton" name="action" onClick="CheckInput();" value="Endre passord" style="background:#bfccd9 none; color:#003366; width:90px">	
				</td>
			</tr>
			</form>
		</table>
	</td>
</tr>
</table>
</BODY>
</HTML>
<%End sub%>

