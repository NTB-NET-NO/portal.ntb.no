<%@ Language=VBScript %><%
'***********************************************************************************
' contactList.asp
'
' This file is used for displaying the "active" duty roster. This is an overview
' from Notabene giving the user an indication of staffing at the main positions in
' NTB the next 24 hours. The page will also contain a list of useful NTB phone
' numbers 
' 
' USES: contactListFrame.asp is the include file that displays the main content.
'		phonelist.xml is the file that holds the main content.
'		phonelist.xsl generates the style and form of the content to be displayed.
'
' NOTES:'none'
'
' CREATED BY: Vegar Paulsen, Andersen 
' CREATED DATE: 2002.04.15
' UPDATED BY: Vegar Paulsen, Andersen
' UPDATED DATE: 2002.04.15
' REVISION HISTORY: 
' UPDATED BY: Roar Vestre, NTB:
' Added chat icon and link to "vaktsjef" in titlebar
' UPDATED DATE: 2002.07.16
'
'
'***********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->

<HTML>
<HEAD>
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>
<%= TitleTag() %>

<script language="JavaScript">

//function to set a field and submit page to itself
function submitPage(valueToSet)
{
	document.SortList.sortBy.value = valueToSet;
	document.SortList.submit();
}

function open_chat(url)
{
	leftPlacement = screen.width - 410; //ensures that the window pops up to the right independent on screen resolution
	winOptions = 'titlebar=0,toolbar=0,location=0,menubar=0,scrollbars=0,resizable=1,width=400,height=440,top=80,left='+leftPlacement;
	var win=window.open(url,'chat',winOptions);
	win.focus();
}
	
</script>
</HEAD>

<BODY topmargin="0" leftmargin="0">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="../include/overlib.js"></script> 
  <TABLE width=100% ALIGN="LEFT" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
    <TR VALIGN="TOP">
      <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
      <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160;</TD>
      <TD WIDTH=100% ALIGN="LEFT">
      <FORM name="SortList" Action="contactList.asp" Method=post>
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="790">
        <TR>
          <TD colspan="5"><%=RenderTitleBar("NTBs sentralbord: 22 03 44 00, &nbsp;&nbsp;<a href=""Javascript:open_chat('../mainpages/chat.asp?channel=vakt')""><img src='../images/chatblue2.gif' WIDTH='9' HEIGHT='11' border='0'></a> Vaktsjef: 22 03 45 45", "noPhotoLink", 790)%></TD>            
        </TR>
        <TR HEIGHT="25" VALIGN="TOP">
          <TD style="width:180px;" align="left" class="main_heading2"><A class="x" href="JavaScript:submitPage('navn')" onmouseover="return overlib('Klikk her for � sortere listen. Klikk en ekstra gang for � skifte mellom stigende og synkende rekkef�lge.', ABOVE, CAPTION, 'Sorter p� navn') ;" onmouseout="return nd();">Navn</a></TD>
          <TD style="width:125px;" align="left" class="main_heading2"><A class="x" href="JavaScript:submitPage('avdeling')" onmouseover="return overlib('Klikk her for � sortere listen. Klikk en ekstra gang for � skifte mellom stigende og synkende rekkef�lge.', ABOVE, CAPTION, 'Sorter p� avdeling') ;" onmouseout="return nd();">Avdeling</a></TD>
          <TD style="width:110px;" align="left" class="main_heading2"><A class="x" href="JavaScript:submitPage('telefon')" onmouseover="return overlib('Klikk her for � sortere listen. Klikk en ekstra gang for � skifte mellom stigende og synkende rekkef�lge.', ABOVE, CAPTION, 'Sorter p� telefonnummer') ;" onmouseout="return nd();">Telefon</a></TD>
          <TD style="width:155px;" align="left" class="main_heading2"><A class="x" href="JavaScript:submitPage('mobil')" onmouseover="return overlib('Klikk her for � sortere listen. Klikk en ekstra gang for � skifte mellom stigende og synkende rekkef�lge.', ABOVE, CAPTION, 'Sorter p� mobilnummer') ;" onmouseout="return nd();">Mobil</a></TD>
          <TD style="width:220px;" align="left" class="main_heading2"><A class="x" href="JavaScript:submitPage('epost')" onmouseover="return overlib('Klikk her for � sortere listen. Klikk en ekstra gang for � skifte mellom stigende og synkende rekkef�lge.', ABOVE, CENTER, CAPTION, 'Sorter p� e-post') ;" onmouseout="return nd();">E-post</a></TD>
        </TR>
          
          <%
			'parameter telling how to sort list, toggles between descending and ascending
            dim strOrder
            strOrder = "ascending"
            if (Request.Form("sortBy")=Request.Form("oldSortBy"))then 
            'same sort has ben cliked twice
              if(Request.Form("oldStrOrder") = "ascending")then
              'list was sorted ascending, toggle
                strOrder = "descending"                        
              end if  
            end if
            
            'parameter telling which column to sort list by, default sorted by "navn"
            dim strColumn
            strColumn = Request.Form("sortBy")
            if (strColumn = "") then
              strColumn = "navn"
            end if            
            
            'storing two variabels in session, needed for changing sort order
            Response.Write("<input type=hidden name='oldSortBy' value=" & strColumn &">")  
            Response.Write("<input type=hidden name='oldStrOrder' value=" & strOrder &">")
            
            'displaying the phonelist matching the criteria set in strColumn and strOrder
'            ShowPhoneList strColumn, strOrder 
            
            %>
        <TR>
          <TD colspan="5"><IFRAME NAME="contactListFrame" WIDTH="100%" HEIGHT="470" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="contactListFrame.asp?sortBy=<%=strColumn%>&orderBy=<%=strOrder%>"></IFRAME></TD>
        </TR>
        <TR>          
          <TD colspan="5">
		  <!--field to be set by javascript function 'submitpage'-->
          <input type=hidden name="sortBy">
          </TD>
		</TR>
		</FORM>
      </TABLE>
    </TD>
    </TR>
  </TABLE>
</BODY>
</HTML>
