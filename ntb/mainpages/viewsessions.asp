<%@ Language=VBScript %>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY>

<%
	'Sessionviewer that displays the number of open sessions for each logged on user
	Application.lock
	
	dim user, valg, status	
	Dim c1, c2, c3
	Dim aryList, n1, n2
	
	valg = Request("valg")
	
	'Check for deletetion
	if IsArray(Application("UserList")) and valg = "nullstill" then
		aryList = Application("UserList")
	
		for n1 = 0 to UBound(aryList, 2)
		
			user = Request("nullstill-" & aryList(0, n1))
		
			if user = "ja" then
				aryList(1, n1) = 0
				status = status & "Bruker nullstilt: " & aryList(0, n1) & "<BR>"
			end if
		
		next
		
		Application("UserList") = aryList
	end if
		
	'Display the list
	c1=0
	c2=0
	c3=0
		
	if IsArray(Application("UserList")) then
		aryList = Application("UserList")

		'Bubblesort the array
		dim tmpName, tmpCount
	
		for n1 = 0 to UBound(aryList, 2)
		
			if (CInt(aryList(1, n1)) > 0) then 
				c2 = c2 + 1
				c3 = c3 + CInt(aryList(1, n1))
			end if

			for n2 = 0 to UBound(aryList, 2)
			
				if StrComp(aryList(0, n1), aryList(0, n2), 1) = -1 then
					tmpName = aryList(0, n2)
					tmpCount = aryList(1, n2)

					aryList(0, n2) = aryList(0, n1)
					aryList(1, n2) = aryList(1, n1)			

					aryList(0, n1) = tmpName
					aryList(1, n1) = tmpCount	
				end if
			next
		
			c1 = c1 +1
		next
	end if
		

%>

<Font face=Verdana size=2>

<font size=3>
<B>Liste over innloggede brukere p� portal.ntb.no</b>
</font>

<P>
Antall innloggede brukere: <%=c2%>
<BR>
Totalt antall innlogginger: <%=c3%>
<BR>
Totalt antall brukere: <%=c1%>
</p>

<font color=red>
<%=status%>
</font>

<form method=post>
<input type=hidden name=valg value=nullstill>

<p>
<input type=submit value="Nullstill valgte brukere" name=submit1>
<input type=button value="Oppdater liste" name=button1 onClick="javascript:location.href='viewsessions.asp'">
</p>

<table width=600 cellspacing=0 cellpadding=1 border=1>

<tr bgcolor=#dddddd>
<TD width=180><font size=2><B>Brukernavn</B></font></td>
<TD width=60 align=center><font size=2><B>Antall</B></font></td>
<TD width=60 align=center><font size=2><B>Null ut?</B></font></td>
<TD width=180><font size=2><B>Brukernavn</B></font></td>
<TD width=60 align=center><font size=2><B>Antall</B></font></td>
<TD width=60 align=center><font size=2><B>Null ut?</B></font></td>
</tr>

<%
	if c1 > 0 then
		'Print the array
		for n1 = 0 to UBound(aryList, 2)
		%>	
			<tr>
			<TD><font size=2><%=aryList(0, n1)%></font></td>
			<TD align=center><font size=2><%=aryList(1, n1)%></font></td>
			<TD align=center><font size=2><input type=checkbox name="nullstill-<%=aryList(0, n1)%>" value=ja></font></td>
		<%

		n1 = n1+1
		if n1 <= UBound(aryList, 2) then
		%>	
			<TD><font size=2><%=aryList(0, n1)%></font></td>
			<TD align=center><font size=2><%=aryList(1, n1)%></font></td>
			<TD align=center><font size=2><input type=checkbox name="nullstill-<%=aryList(0, n1)%>" value=ja></font></td>
			</tr>
		<%
		end if
		
		next
			
	else
		'Array is empty
		Response.Write("<tr><td colspan=6><font size=2>Listen er tom</font></td></tr>")
			
	end if
		
	'unlock
	Application.unlock

%>

</table>
<p>
<input type=submit value="Nullstill valgte brukere" name=submit2>
<input type=button value="Oppdater liste" name=button2 onClick="javascript:location.href='viewsessions.asp'">
</p>
</form>
</font>

</BODY>
</HTML>
