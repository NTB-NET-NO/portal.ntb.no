<%
'***********************************************************************************
'* Personalization_choose_window.asp
'*
'* This file is used for giving the user the opportunity to see which information
'* the personalized page consists of and to click on a spesified part of the site to
'* customize it.
'*
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar.
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen
'* CREATED DATE: 2002.04.16
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.27
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_personal_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/addr_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<%
Sub Main()

End Sub
%>

<%

Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

%>

<html>
<head>
	<link href="<%=style%>" type=text/css rel=stylesheet>
	<%
	Response.Write TitleTag()

	%>
</head>

<BODY topmargin="0" leftmargin="0" >
    <TABLE height="100%" width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
		<TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
	</TR>
	<TR VALIGN="TOP">
		<TD ALIGN="LEFT"  bgcolor="#003084">
			<!-- #INCLUDE FILE="../template/leftmenu.asp" -->
		</TD>
		<TD ALIGN="LEFT">&#160; </TD>
		<TD ALIGN="LEFT" width="100%">
			<%=RenderTitleBar("Spr�ket i NTB", "NoPhotolink", 620)%>

			<table border="0" width=620 class=news>
				<tr>
				<td><a href="http://intern.ntb.no/dokumenter/spr�k/spraakregler_juli04.htm" target="_new">NTBs Spr�khefte</a></td>
				</tr>
				<tr>
				<td><br><b>Ordlista:</b></td>
				</tr>
				<tr>
				<td>

					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/A.htm">A</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/b.htm">B</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/c.htm">C</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/d.htm">D</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/e.htm">E</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/f.htm">F</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/g.htm">G</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/h.htm">H</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/i.htm">I</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/j.htm">J</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/k.htm">K</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/l.htm">L</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/m.htm">M</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/n.htm">N</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/o.htm">O</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/p.htm">P</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/qr.htm">QR</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/s.htm">S</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/t.htm">T</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/u.htm">U</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/vw.htm">VW</a>&nbsp;
					<a href="?URL=http://intern.ntb.no/dokumenter/ordlista/xyz���.htm">X-�</a>

					<hr size=1 noshade>
				</td>
				</tr>

				<tr>
				<td><IFRAME NAME=wordlist FRAMEBORDER=0 WIDTH=615 HEIGHT=400 MARGINWIDTH=0 MARGINHEIGHT=0 SRC="
				<%
					if ( Request("URL") <> "" ) then
						response.write(Request("URL"))
					else
						response.write("about:blank")
					end if

				%>"></IFRAME></td>
				</tr>

	       	</table>

        </td></tr>
    </table>


</body>
</html>
