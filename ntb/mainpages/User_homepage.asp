<%
'***********************************************************************************
' User_homepage.asp
'
' This file is used to give the user his/hers personalized site based on the information
' saved in the profile
'
' USES:
'		NTB_layout_lib.asp - For rendering of titlebar
'		NTB_personal_render_lib.asp - For rendering the site
'		NTB_profile_lib.asp - For profilefunctions that search in the database
'
'
' NOTES:
'		none
'
' CREATED BY: Solveig Skjermo, Andersen 
' CREATED DATE: 2002.04.24
' UPDATED BY: Solveig Skjermo, Andersen
' UPDATED DATE: 2002.06.14
' REVISION HISTORY:
' 2002.08.09: Roar Vestre, NTB: Bug fix: Check Session("UserName") for redirect to CloseWindow.asp
' 2002.08.16: Roar Vestre, NTB: Added Change of style if Netscape
' 2002.08.16: Roar Vestre, NTB: Excluded some Includefiles not in use
'
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_personal_render_lib.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   

<!-- - INCLUDE FILE="../include/const.asp" -->
<!-- - INCLUDE FILE="../include/html_lib.asp" -->
<!-- - INCLUDE FILE="../include/catalog.asp" -->

<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->

<!-- - INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- - INCLUDE FILE="../include/std_cookie_lib.asp" -->

<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->


<%
Sub Main()
	if Session("UserName") = "" then
		Response.Redirect "../Authfiles/Login.asp"
	end if
	'Call EnsureAuthAccess()
	Response.cookies("ntbLastRequestedURL") = Request.ServerVariables("URL") & "?" & Request.ServerVariables("QUERY_STRING")
End Sub

Dim style
if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

%>

<HTML>
<HEAD>
<link href="<%=style%>" type=text/css rel=stylesheet>
	<%Response.Write TitleTag()
	
	'Fetch name of the user windowstyle and newsinfo
	Dim fullname, windowstyle, newsinfo, rsUser, telephone, email, strHeading, uid
	Dim strRetAsp
    
	if Request("saveProfile") = "Lagre endring" then
		'save the changes made in the update profile
		UpdateProfile(Request("huid"))
	end if 

	' Reset the session variable if is was set.
	if Session("VerifyProfile") = true then	Session("VerifyProfile") = false

	fullname = GetUserFullName(Session("Username"))
	Set rsUser = GetUserProfileByLoginName(Session("Username"))
	telephone = rsUser.fields("GeneralInfo.tel_number") & ""
	if telephone = "" then telephone = "Tlf mangler"
	email = rsUser.fields("GeneralInfo.email_address") & ""
	if email = "" then email = "E-post mangler"
	windowstyle = GetWindowStyle(Session("Username"))
	newsinfo = GetNewsInfo(Session("Username"))
	uid = rsUser.fields("GeneralInfo.user_id")

	strRetAsp = "../mainpages/EditProfile.asp?huid=" & rsUser.Fields("GeneralInfo.user_id") & "" & "&hcoid=" & rsUser.Fields("AccountInfo.org_id") & ""

	strHeading = "Nyheter for " & "<A title='Klikk her for � oppdatere profilen din' class='nav' HREF='" & strRetAsp & "'>" & fullname & "&nbsp;&nbsp;" & _
				 "<BR>Mobil: " & telephone & "&nbsp;&nbsp;" & _
				 "E-post: " & email	& "</A>"
	%>
<script language="javascript">
<!--
// Checks if the login form i displayed in an IFrame. If so, the browser refreshes and
// displays the same form in the top frame. (Bugfix)
if (window.frameElement != null) top.location.href = window.location.href;

//-->
</script>
	
</HEAD>
   
<BODY topmargin="0" leftmargin="0">
  <TABLE height="100%" width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
      <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
      <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160;</TD>
      <TD ALIGN="LEFT" WIDTH=100%>

            	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<TR>
            			<TD colspan="3"><%= RenderTitleBar(strHeading, "NoPhotoLink", 620) %></TD>
            		</TR>
	           		<tr>                 
						<%'Writes out the personal site with aid from RenderPersonalSite
						Response.write RenderPersonalSite(windowstyle, newsinfo)
						%>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</BODY>
</HTML>
