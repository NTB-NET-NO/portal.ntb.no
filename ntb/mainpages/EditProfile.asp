<%
'***********************************************************************************
'* EditProfile.asp
'*
'* This file is used to give the superuser the opportunity to edit the profiles 
'* belonging to his/hers company
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.24
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.05.10
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->


<%

Dim bNormalUser

Sub Main()

	'check whether the user has the rights to view this page
	if Session("Role") = "Normal" and not Session("VerifyProfile") = true then
		' Give the user option to change it's own data.
		bNormalUser = true
		'give an errormessage
'		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub

%>

<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">

	<%=TitleTag()%>
	
	<script language="Javascript">
	<!--
	function SetDefault(strUser) {
		
		//if strUser != " " {
		//	document.frmChangeProfile.txtFirstname.value = document.frmChangeProfile.txtUserName.defaultValue
		//	document.frmChangeProfile.txtLastname.value = document.frmChangeProfile.txtPassword.defaultValue
		//}
		
		document.frmChangeProfile.txtFirstname.value = document.frmChangeProfile.txtFirstname.defaultValue
		document.frmChangeProfile.txtLastname.value = document.frmChangeProfile.txtLastname.defaultValue
		document.frmChangeProfile.txtEmail.value = document.frmChangeProfile.txtEmail.defaultValue
		document.frmChangeProfile.txtPhone.value = document.frmChangeProfile.txtPhone.defaultValue
		document.frmChangeProfile.txtRole.defaultValue
	}
	
	function CheckInput(strNyBruker){

		// Regular expression for validating at most of the emailaddresses.
		re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

		if (strNyBruker == "1" || strNyBruker == "2")
			strMessage = " Kan ikke lagre endringene."
		else
			strMessage = " Kan ikke opprette ny bruker.";
		
		if (document.frmChangeProfile.txtUsername.value == "") {
			alert ("Ugyldig brukernavn." + strMessage);
		}
		else if (document.frmChangeProfile.txtFirstname.value == "") {
			alert ("Fornavn m� fylles ut." + strMessage);
			document.frmChangeProfile.txtFirstname.focus();
		}
		else if (document.frmChangeProfile.txtLastname.value == "") {
			alert ("Etternavn m� fylles ut." + strMessage);
			document.frmChangeProfile.txtLastname.focus();
		}
		else if (document.frmChangeProfile.txtEmail.value == "") {
			alert ("E-post m� fylles ut." + strMessage);
			document.frmChangeProfile.txtEmail.focus();
		}
		else if (document.frmChangeProfile.txtPhone.value == "") {
			alert ("Mobilnummer m� fylles ut." + strMessage);
			document.frmChangeProfile.txtPhone.focus();
		}
		else if (!re.test(document.frmChangeProfile.txtEmail.value)) {
			alert("Ugyldig e-post adresse.");
			document.frmChangeProfile.txtEmail.focus();
		}
		else {
			document.frmChangeProfile.submit();
		}
	}
	
	function GetToList(strRedirect){
		/*
			Changelog: 
			21.01.2008: Changed "0" to 0. 
		*/
		
		if (strRedirect == 0) 
			location.href = "UserList.asp";
		else
			location.href = "Login.asp";
	}
	-->
	</script>
</head>

<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7">

<% 
'get the userid and orgid
Dim huid, coid, bForceUpdate
huid = Request.Form("huid")
coid = Request.Form("hcoid")

if huid = "" then
	huid = request.QueryString("huid")
	' response.write "'"&huid&"'"
end if

if coid = "" then
	coid = request.QueryString("coid")
end if 

Dim strTitle, renderFunc, strRedirect

strRedirect = "UserList.asp"

'if no userid has been sent and a new user are being made
if huid = " " then
	strTitle = "Ny bruker "
	renderFunc = RenderNewUserProfile(coid)
else
	bForceUpdate = false
	' if there is a userid and the userprofile should be modified
	if huid = "" then 
		huid = Request("huid") 
		bForceUpdate = true
	end if
	if coid = "" then 
		coid = Request("hcoid")
		bForceUpdate = true
		strRedirect = "Login.asp" ' Changed from User_homepage.asp to Login.asp
	end if

	strTitle = "Oppdater Profil for " & GetUserName(huid)
	renderFunc = RenderEditUserProfile(huid, coid, bForceUpdate, bNormalUser)
end if

Response.Write(RenderTitleBar(strTitle, "NoPhotoLink", 620))%>

<TABLE border="0" class="news">
	<form name="frmChangeProfile" action="<%Response.Write strRedirect%>" method="Post">
	<%Response.Write renderFunc	%>
	</form>
</table>  
</body>
</html>

