<%
'***********************************************************************************
'* DeleteProfile.asp
'*
'* This file is used to delete a user profile
'*
'* USES:
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.04.09
'* REVISION HISTORY:
'*		none
'*		25.01.2008: Trond Hus�, NTB: 
'*		Changed redirect from CompanyList.asp to UserList.asp when user comes from 
'*		UserList.asp
'*		Should the user also be redirected to UserList.asp when he/she comes from 
'*		another page?
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->

<%
'check whether the user has the rights to view this page
if Session("Role") = "Normal" then
	'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
end if

Dim huid, uid, hcoid, coid

huid = Request("huid")
hcoid = Request("hcoid")

'place the brackets back on
if huid = "" then
'came from CompanyList
	DeleteCompany(hcoid)
	Response.Redirect "CompanyList.asp"
	
else
'came from UserList
	DeleteProfile(huid)
	
	'get org
	Dim idOrg, objOrgProfile, strOrg
	' This check is really not needed if we don't want to redirect to CompanyList.asp, which _I_ think is not needed. 
	idOrg = GetCompany(Session("UserName"))
	objOrgProfile = GetCompanyProfile (idOrg)
	strOrg = objOrgProfile.GeneralInfo.name
	
	if strOrg = "Norsk Telegrambyr�" then
		' Response.Redirect "CompanyList.asp"
		response.Redirect "UserList.asp"
	else
		Response.Redirect "UserList.asp"
	end if
end if
%>

