<%
'***********************************************************************************
' searchresults_ew.asp
'
' This file is used for displaying search results. Both freetext search results and 
' category search results will be displayed. Results from the sound search will 
' also be displayed on this page.
' The different search queries are formatted and sent to a search component. This 
' component returns a RecordSet of search results. The first 20 hits are displayed
' on the page and the RecordSet is saved in a session variable to make it easy to
' display the next pages with hits.
'
' USES:
'		NTB_layout_lib.asp - For rendering of titlebar.
'		Server.CreateObject("SearchInterface.comSearch") - search components that returns 
'														a recordset of search results
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen 
' CREATED DATE: 2002.04.09
' UPDATED BY: Trond Orrestad, Andersen
' UPDATED DATE: 2002.04.09
' REVISION HISTORY:
'		none
'
'**********************************************************************************
' -----------------------------------------------------------------------------
' FormatDate
'
' Description:	This function formats the date to norwegian format
'				The format is DD.MM.YYYY hh:mm
'
' Parameters:	inDate - date to be formated
'
' Returns:		String with the in date in correct format
'
' Notes :		None
' -----------------------------------------------------------------------------
Function GetTitleString(ByVal strCat, ByVal strLoc, ByVal strLocalLoc)
	Dim strTitle
	
	strTitle = GetCatString(strCat)
	If strTitle <> "" then
		strTitle = strTitle & ", " & strLocString(strLoc, false)
	else
		strTitle = strLocString(strLoc, false)
	end if
	If strTitle <> "" then
		strTitle = strTitle & ", " & strLocString(strLocalLoc, true)
	else
		strTitle = strLocString(strLocalLoc, true)
	end if

	GetTitleString = strTitle
End Function
	
	
Function GetCatString(ByVal strCat)

    Dim strTitle

    Dim strTemp()
    strTemp = strCat.Split(";")

    Dim iIterations
    iIterations = (strTemp.Length / 4)

    Dim i0
    For i0 = 0 To (iIterations - 1)

        Dim i1
        For i1 = 0 To 3

            Dim strBit()
            strBit = strTemp((i0 * 4) + i1).Split("+")

            Dim i2
            For i2 = 0 To strBit.Length - 1

                If strBit(i2) <> "" Then

                    Dim intTemp
                    intTemp = CInt(strBit(i2))

                    If intTemp >= 0 Then

                        Dim strName As String
                        strName = LookupDescription(typeOfName, intTemp, Nothing)
                        If strTitle = "" Then
                            strTitle = strName
                        Else
                            strTitle = strTitle & ", " & strName
                        End If

                    End If

                End If
            Next

        Next

    Next

    Return strTitle

End Function


Function GetLocationString(ByVal strLoc, ByVal isLocal)

    Dim strResult

    Dim strBit
    strBit = strLoc.Split("+")

    Dim i2
    For i2 = 0 To strBit.Length - 1

        If strBit(i2) <> "" Then

            Dim intTemp
            intTemp = CInt(strBit(i2))
            If intTemp >= 0 Then

                Dim strName
                'strName = LookupDescription(TypeOfName, intTemp, Nothing)
                
                If strResult = "" Then
                    strResult = strName
                Else
                    strResult = strResult & ", " & strName
                End If

            End If

        End If

    Next

    Return strResult

End Function


Function GetDescription(ByVal bitPosition, ByVal source)

	dim wrCounter, wrName, wrLength, theDescription

	Select Case source 
	Case 1
		wrCounter = getMainGroups()
	Case 2 
		wrCounter = getSubGroups()
	Case 3 
		wrCounter = getCategories()
	Case 4 		
		wrCounter = getSubcategories(strCat)
	Case 5
		wrCounter = getWorldRegions()
	Case 6
		wrCounter = getLocalRegions()
	End Select

	for each wrName in wrCounter
		if wrCounter(wrName) = bitPosition then
			theDescription = wrName
			exit
		end if
	next														

	GetDescription = theDescription

End Function

%>