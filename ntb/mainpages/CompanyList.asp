<%
'***********************************************************************************
'* CompanyList.asp
'*
'* This file is used to show a list of customers/organizations for NTB
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.24
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.05.31
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->
<!--#INCLUDE FILE="../include/DARoutines.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main()

	'check whether the user has the rights to view this page
	if Session("Role") <> "Admin" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if
	
	' Check if the session is still alive
	if Session("Role") = "" then
		response.Redirect "../Authfiles/Login.asp"
	end if

End Sub

%>

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">
<script language="JavaScript">
	function setParam(par, link){
	document.profile.hcoid.value = par;
	document.profile.action = link;
	
	if (link == "DeleteProfile.asp") {
		if (confirm("Slette kunde?")) {
 			document.profile.submit();
 			}
 		}
 	else if (link == "ReActivateProfile.asp") {
		if (confirm("Aktiver kunde?")) {
 			document.profile.submit();
 			}
 		}
  	else{
 		document.profile.submit(); 		
 		}
	}
</script>
</head>
<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7">

<%
Dim saveNew, editOrg, coid
saveNew = Request("makeNew")
editOrg = Request("changeOrg")
coid = Request("hcoid")

if saveNew = "newOrg" then
'make a new organization with the input information
	
	if CreateOrgProfile()= true then
	'all is ok
	ElseIf Err.number <> 0 then
		Response.Write (Err.number &" " & Err.Description & ".")
	Else 
		Response.Write("Company already exists. Try a different company name")
	End if
	
elseif saveNew = "newProfile" then
'make a new superuser for the organization

	if CreateUserProfile(coid)= true then
	'all is ok	
	ElseIf Err.number <> 0 then
		Response.Write (Err.Description & ". Try a different user id")
	Else 
		Response.Write("User already exists. Try a different user id")
	End if

elseif editOrg = "change" then
'update the organization with the changes made
	UpdateOrg(coid)
End if

'show the list
Response.Write(RenderTitleBar("Liste over NTBs kunder", "NoPhotoLink", 620)) 
Response.Write(RenderCompanyList())
%>

</table>
</body>
</html>