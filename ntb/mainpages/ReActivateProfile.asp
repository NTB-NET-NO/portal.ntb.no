<%
'***********************************************************************************
'* DeleteProfile.asp
'*
'* This file is used to delete a user profile
'*
'* USES:
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.06.19
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.19
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->

<%
'check whether the user has the rights to view this page
if Session("Role") <> "Admin" then
	'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
end if

Dim hcoid
hcoid = Request("hcoid")

Reactivate(hcoid)

Response.Redirect "CompanyList.asp"

%>

