<%@ Language=VBScript %><%
'***********************************************************************************
'* wapsms.asp
'*
'* This file is used for displaying the existing SMS / WAP functionality. This page
'* only includes the existing wap/sms page (http://wap.ntb.no/kunde/) in an iframe
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of iframes and titlebar.
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Trond Orrestad, Andersen 
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.12
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->


<HTML>
<HEAD>
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>
<STYLE TYPE="text/css">
A    { text-decoration:none
       }
 </STYLE>


<%= TitleTag() %>
</HEAD>
   
<BODY topmargin="0" leftmargin="0">
<TABLE height="100%" width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
      <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
      <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160;</TD>
      <TD ALIGN="LEFT" WIDTH=100%>
            <%= RenderTitleBar("Oppsett av profil for SMS", "noPhotoLink", 620) %>
            
            <IFRAME NAME="wapsmsFrame" WIDTH="620" HEIGHT="480" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="../wap_kunder/openbase.asp"></IFRAME>
            <!--Comment in the line above, and delete the line below when sms wap is ready-->
            <!--<P class=helpFont>&nbsp;SMS/Wap sidene v�re er under konstruksjon.</P>-->
            </TD>			    
        </TR>
    </TABLE>
    
    
  </td></tr>
</table>	
</BODY>
</HTML>
