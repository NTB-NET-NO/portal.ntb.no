<%
'***********************************************************************************
'* StatsReport.asp
'*
'* This file is used to show a a report of the criterias from ViewStats
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: �yvind Andersen 
'* CREATED DATE: 2003.12.03
'* UPDATED BY: 
'* UPDATED DATE: 
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_stat_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_stat_lib.asp" -->

<!-- #INCLUDE FILE="../services/include/ad_routines.asp" -->
<!-- #INCLUDE FILE="../services/include/const.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main()

	'check whether the user has the rights to view this page
	if lcase(left(Session("Username"), 3)) <> "ntb" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub
%>

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">

	<script type="text/javascript">
		//function to open news items in a seperate window
		var myWindow;

		function w(aid) {
			var myURL;
			// myURL = '../mainpages/viewnewsitem.asp?aid=' + aid;
			myURL = '../template/x.asp?a=' + aid;
		  if(myWindow && !myWindow.closed) { // window is open
		    myWindow.location = myURL;
		    myWindow.focus();
		  } else {
		    leftPlacement = screen.width - 610; //ensures that the window pops up to the right independent on screen resolution
		    myName = 'PopupPage';
		    myOptions = 'height=600,width=600,scrollbars=yes,resizable=yes,top=80, left='+ leftPlacement;
		    myWindow = window.open(myURL, myName, myOptions);
		    myWindow.focus();
		  }
		}
	</script>

</head>

<script type="text/javascript" src="../include/sortTable.js"></script>
<body onload="initTable('statReport', new Array(null, 'number', 'number'), 1, 1, 1);">

<%	
Dim strFromDate, strToDate, strCompany, strReport, strDateCriteria, strArchive
Dim strMainGroup, strArticles, strOlderThan, strRatingHigher, strRatingLower

if len(Request("btnReport1")) > 0 then
	strReport = "1"
elseif len(Request("btnReport2")) > 0 then
	strReport = "2"
elseif len(Request("btnReport3")) > 0 then
	strReport = "3"
end if

strCompany = Request.Form("txtCompany")
if len(Request.Form("chkArchive")) > 0 then
	strArchive = Request.Form("txtArchive")
	if lcase(Request.Form("ArchiveSelect")) = "eldre" then
		strOlderThan = true
	else
		strOlderThan = false
	end if
else
	strArchive = ""
end if

strDateCriteria = Request.Form("DateSelect")
select case lcase(strDateCriteria)
	case "today": 
		strFromDate = Date()
		strToDate = Date()
	case "yesterday"
		strFromDate = Date() - 1
		strToDate = Date() - 1
	case "thismonth"
		strFromDate = GetFirstDayOfMonth(Month(Date()))
		strToDate = GetLastDayOfMonth(Month(Date()))
	case "thisyear"
		strFromDate = GetFirstDayOfYear(Year(date()))
		strToDate = Date()
	case "lastweek"
		strFromDate = GetFirstDayOfLastWeek(Date())
		strToDate = GetLastDayOfLastWeek(Date())
	case "lastmonth"	
		strFromDate = GetFirstDayOfLastMonthByDate(Date())
		strToDate = GetLastDayOfLastMonthByDate(Date())
	case "specifydates"
		strFromDate = Request.Form("txtFromDate")
		strToDate = Request.Form("txtToDate")
	case else
		Response.Write "Invalid date selected" & "<br>"
end select

if Request.Form("chkArticles") = "1" then
	strArticles = Request.Form("txtArticle")
end if

if Request.Form("chkRatingHigher") = "1" then
	strRatingHigher = Request.Form("txtRatingHigher")
end if
if Request.Form("chkRatingLower") = "1" then
	strRatingLower = Request.Form("txtRatingLower")
end if

strMainGroup = Request.Form("lstMainGroup")

' Response.Write strReport & "<br>" & strCompany & "<br>" & strFromDate & "<br>" & strToDate & "<br>" & strArchive & "<br>" & strOlderThan & "<br>" & strMainGroup & "<br>" & strRatingHigher & "<br>" & strRatingLower

Response.Write(RenderTitleBar("Statistikk rapport for perioden " _
		& FormatDateTime(strFromDate, 2) & " til " _
		& FormatDateTime(strToDate, 2), "NoPhotoLink", 620)) 
Response.Write(RenderStatsReport(strCompany, strReport, strFromDate, strToDate, strArchive, strOlderThan, strMainGroup, strRatingHigher, strRatingLower))
%>
</body>
</html>