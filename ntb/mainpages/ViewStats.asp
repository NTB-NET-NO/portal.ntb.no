<%
'***********************************************************************************
'* ViewStats.asp
'*
'* This file is used to show a list of stats for some NTB users
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: �yvind Andersen 
'* CREATED DATE: 2003.12.03
'* UPDATED BY: 
'* UPDATED DATE: 
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_stat_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_stat_lib.asp" -->

<!-- #INCLUDE FILE="../services/include/ad_routines.asp" -->
<!-- #INCLUDE FILE="../services/include/const.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main()

	'check whether the user has the rights to view this page
	if lcase(left(Session("Username"), 3)) <> "ntb" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub
%>

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">

	<script type="text/javascript" language="Javascript">
	<!--
	function CheckInput(){
		if (document.forms("frmCriteria").lstCompany.value == "") {
			alert ("Du har ikke valgt noen kunder.");
			return false;
		}
		// Copy the value to the correct submit form.
		// This is done since we have two forms on the page.
		var objListCompany = document.frmCriteria.lstCompany.options;
		var strSelectedCompany = "";
		for (var i=0; i < objListCompany.length; i++) {
			if (objListCompany.options[i].selected == true) {
				if (strSelectedCompany == "")
					strSelectedCompany = objListCompany.options[i].value;
				else
					strSelectedCompany = strSelectedCompany + ", " + objListCompany.options[i].value;
			}			
		}
		document.forms("frmStats").txtCompany.value = strSelectedCompany;
		return true;
	}
	
	function AddGroup() {
		if (document.forms("frmCriteria").lstCompany.value == "") {
			alert ("Du har ikke valgt noen kunder.");
			return false;
		}
		var strName = prompt("Skriv inn navnet p� gruppen du �nsker � opprette.", "");
		if (strName == null)  // User selected cancel
			return false;
		else if(strName == "") {
			alert("");
			return false;
		}
		document.forms("frmCriteria").txtGroupName.value = strName;
		document.forms("frmCriteria").txtAction.value = "addgroup";
		document.forms("frmCriteria").submit();
		return true;
	}
	
	function DeleteGroup()
	{
		if (document.forms("frmCriteria").lstCompanyGroup.value == "") {
			alert ("Du har ikke valgt noen kundegruppe.");
			return false;
		}
		var x = window.confirm("Er du sikker p� at du vil slette gruppen?");
		if (x) {
			document.forms("frmCriteria").txtGroupName.value = "";
			document.forms("frmCriteria").txtAction.value = "deletegroup";
			document.forms("frmCriteria").submit();
			return true;
		}
		else {
			return false;
		}
	}
	
	function SelectCompany()
	{
		var objListCompanyGroup = document.frmCriteria.lstCompanyGroup.options;
		var objListCompany = document.frmCriteria.lstCompany.options;

		// Unselect all
		ChangeSelect(false, objListCompany);
		
		var strCompanyGroupId;
		// Loop through all selected groups and select the 
		// companies for each group.
		for (var i=0; i < objListCompanyGroup.length; i++) {
			if (objListCompanyGroup.options[i].selected) {
				strCompanyGroupId = objListCompanyGroup.options[i].value
				for (var j=0; j < objListCompany.length; j++) {
					if (strCompanyGroupId.search(objListCompany.options[j].value) > 0)
						objListCompany.options[j].selected = true;
				}
			}
		}
		return true;
	}

	// Function to (un)select all items in a select box.
	function ChangeSelect(bSelected, select)
	{
		//document.frmCriteria.lstCompany
		for (var i=0, length=select.length; i < length; i++) {
			select.options[i].selected = bSelected;
		}
		return true;
	}
	-->
	</script>
</head>

<%	
Dim strAction, strCompanyGroupName, strCompanyId, strCompanyGroupId

strAction = Request.Form("txtAction")
strCompanyGroupName = Request.Form("txtGroupName")
'Response.Write "Action='" & strAction & "'<BR>"
'Response.Write "CompanyGroup='" & strCompanyGroupName & "'<BR>"
'Response.Write "Selected companygroup='" & Request.Form("lstCompanyGroup") & "'<BR>"

select case strAction
	case "addgroup" : 
		strCompanyId = Request.Form("lstCompany")
		AddStatGroup strCompanyGroupName, strCompanyId
		
	case "deletegroup" :
		strCompanyGroupId = Request.Form("lstCompanyGroup")
		DeleteStatGroup strCompanyGroupId
end select

Response.Write(RenderTitleBar("Oversikt over statistikk", "NoPhotoLink", 620)) 
Response.Write(RenderViewStat())
%>
</body>
</html>