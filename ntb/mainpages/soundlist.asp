<%@ Language=VBScript %><%
'***********************************************************************************
'* Soundlist.asp
'*
'* This file is used for displaying all sound news last week. It consists of a 
'* iFrames listing different news items with sound. This page also has a section for
'* entering a sound search. 
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of iframe and titlebar.
'*
'* NOTES:
'*		none
'*		
'* CREATED BY: Trond Orrestad, Andersen 
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.12
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->

<HTML>
<HEAD>
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>

<script language="JavaScript">

//Call this function with this code from form tag:  onSubmit="return checkField()"
//function checkField()
//{
//	if (document.forms.soundsearchform.elements.freeSoundTextEntry.value == '')
//	{
//		window.alert("�nsker du � s�ke etter en lydsak, m� du gi s�kefeltet en verdi. �nsker du � s�ke p� alle lydsaker, bruk linken under s�kefeltet.")
//		return false
//	}
//}

//function to cleare soundsearch textfield and submit the form.
function submitPage()
{
	document.forms.soundsearchform.elements.freeSoundTextEntry.value = ""
	document.forms.soundsearchform.submit()
}
</script>

<% 
Response.Write ( TitleTag())
'delete search results session variable in case it exist
Session("SearchCache-ADTG") = null
 %>

</HEAD>

<BODY topmargin="0" leftmargin="0">
<TABLE height="100%" width=100% border="0" CELLSPACING="0" CELLPADDING="0">
  <TR VALIGN="TOP">
    <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
  </TR>
  <TR VALIGN="TOP">
    <TD ALIGN="LEFT"  bgcolor="#003084">
    <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
    </TD>
    <TD ALIGN="LEFT">&#160;</TD>
    <TD ALIGN="LEFT" WIDTH=100%>
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0">
      <TR>
        <TD><%= RenderTitleBar("Lyd p� NTBs nyhetsportal", "noPhotoLink", 620) %></TD>
      </TR>
      <TR>
	    <TD>
		<%= RenderIFrame("pr_sounds", "width1", "height1s", "no", "r", 1, 0) %>
		</TD>
      </TR>
      <TR>
	    <TD height="5"></TD>
      </TR>
      <TR>
        <TD>
		<form name="soundsearchform" Action="searchresults.asp" Method="post">
		<INPUT type="hidden" id=file1 name=hidLowRes>		
		<TABLE bordercolor="#61c7d7" border="1" cellpadding="2" cellspacing="3" valign="top">
          <tr valign="top" align="left">
            <td valign="top" style='border:none;' width="600" height="5" class="hjelpetekst"></td>
          </tr>
          <tr valign="top" align="left">
		    <td valign="top" style='border:none;' width="600" height="10" class="plaintext">
            <input type="text" id="text1" class="plaintext" name="freeSoundTextEntry" value="S�k i NTBs lydarkiv" size="55" onfocus="javascript:this.value=''">&#160;<input class="formbutton" type="submit" value="Lyds�k" id="button1" name="button1" style="background:#bfccd9 none; color:#003366;">	
			<a href="HelpSite.asp?anchor=SoundSearch">S�kehjelp</a></td>
          </tr>
		  <tr valign="top" align="left">
            <td valign="top" style='border:none;' width="600" height="10" class="plaintext">
			Vis liste over <a href="JavaScript:submitPage()">alle</a> nyheter med lyd</td>
          </tr>
          <input type="hidden" name="flagSoundSearch" value="flag">
        </form>
        </TABLE>
        </TD>
      </TR>
	</TABLE>
	</TD>
  </TR>
</TABLE>
</BODY>
</HTML>
