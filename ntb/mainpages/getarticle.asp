<%@ Language=VBScript %>
<!-- #INCLUDE FILE="../include/NTB_cache_lib.asp" -->
<%
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if


Dim aid, pm, xmlstr
aid = Request("aid")
pm = Request("download")
if aid <> "" then
	xmlstr = getArticleXML(aid)
	if Left(xmlstr,2) = "<?" then
		'if pm = "xml" then
		if Session("Browser") <> "ns" then
			Response.ContentType = "text/xml"
			'Response.ContentType = "text/plain"
			Response.AddHeader "Content-Disposition","attachment;filename=""ntb" & CStr(aid) & ".xml"""

				' Old Workaround for Helen to see XML-directly, problems with IE5.5 and XML-download!!
				' to only display XML instead of file downloading:
				' Comment out the above line: Response.AddHeader "Content-Disposition................

			Response.Write xmlstr
		else
			' make the file name to be 
			'Response.ContentType = "application/octet-stream; extension=""text/plain"""
			Response.ContentType = "text/plain"
			Response.AddHeader "Content-Disposition","attachment;filename=""ntb" & CStr(aid) & ".xml"""
			Response.Write xmlstr
		end if
	else
		Response.Write("<HTML><BODY><p>Error accessing article text: <b>" & _
		" - ID:" & aid & "</b><hr></p>" & xmlstr & "</BODY></HTML>")
		'Response.Write("<HTML><BODY><p>Error accessing article text: <b>" & _
		'" - ID:" & aid & "</b><hr></p></BODY></HTML>")
	end if
else
	' form didn't submit data, close the session to be sure :)
	Response.Write("<HTML><BODY><b>Feil i siden. Lukk dette vinduet og velg artikkel fra portalen p� nytt!</b></BODY></HTML>")
	'Response.Write("<HTML><BODY><b>Don't hack this site, there is nothing to take.</b></BODY></HTML>")
	Session.Abandon
end if
Response.End
%>
		