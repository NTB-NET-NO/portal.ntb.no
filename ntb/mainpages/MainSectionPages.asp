<%@ Language=VBScript%><%
'***********************************************************************************
' MainSectionPages.asp
'
' This file is used for displaying all main section pages. It consists of different
' iFrames listing different news items. Based on parameters in the calling URL,
' different news lists are displayed.
'
' USES:
'		NTB_layout_lib.asp - For rendering of iframes and titlebar.
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen
' CREATED DATE: 2002.04.09
' UPDATED BY: Solveig Skjermo, Andersen
' UPDATED DATE: 2002.06.17
' REVISION HISTORY:
' 2002.08.09: Roar Vestre, NTB: Bug fix: Check Session("UserName") for redirect to Logout.asp
' 2002.08.16: Roar Vestre, NTB: Included some Include files for Autentication
'
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- --INCLUDE FILE="../include/NTB_ticker_lib.asp" -->

<!-- --INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- --INCLUDE FILE="../include/NTB_personal_render_lib.asp" -->

<!-- --INCLUDE FILE="../include/setupenv.asp" -->
<!-- --INCLUDE FILE="../include/std_access_lib.asp" -->

<!-- --INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- --INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- --INCLUDE FILE="../include/std_util_lib.asp" -->

<%

Sub Main()
	if Session("UserName") = "" then
		Response.Redirect "../Authfiles/Login.asp"
	end if
	'Call EnsureAuthAccess()
End Sub

Response.cookies("ntbLastRequestedURL") = Request.ServerVariables("URL") & "?" & Request.ServerVariables("QUERY_STRING")

'For test to debug: logon problems with redirect to this page:
Dim fullname, windowstyle, newsinfo
'fullname = GetUserFullName(Session("Username"))
'windowstyle = GetWindowStyle(Session("Username"))
'newsinfo = GetNewsInfo(Session("Username"))

'delete search results session variable in case it exist
Session("SearchCache-ADTG") = null

Dim photoSelected
photoSelected = "No"

'Get parameters from URL to determine behaviour
If Request.QueryString("photoOption")<>"" Then
      If Request.QueryString("photoOption")= "Yes" Then
      photoSelected = "Yes"
      End If
End If


Dim PageID
PageID = "nopage"
'Get parameters from URL to determine behaviour
If Request.QueryString("pageID")<>"" Then
	PageID = Request.QueryString("pageID")
End If

Dim style
if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>

<html>
<head>
<link href="<%=style%>" type="text/css" rel="stylesheet">
<style TYPE="text/css">
A    { text-decoration:none
       }
</style>

<%= TitleTag() %>

<script language="javascript">
<!--
// Checks if the login form i displayed in an IFrame. If so, the browser refreshes and
// displays the same form in the top frame. (Bugfix)
if (window.frameElement != null) top.location.href = window.location.href;

//-->
</script>

</head>
<BODY topmargin="0" leftmargin="0">
  <TABLE height="100%" width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
      <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
      <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160;</TD>
      <TD ALIGN="LEFT" WIDTH=100%>

           <% Select Case PageID
				Case "AlleNyheter"%>
            	<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("Alle nyheter", "PhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td valign="right"><%= RenderIFrame("alle_innenriks", "width3", "height1", photoSelected, "r", 1, 0) %></td>
						<td valign="right"><%= RenderIFrame("alle_utenriks", "width3", "height1", photoSelected, "r", 2, 0) %></td>
						<td valign="right"><%= RenderIFrame("alle_sport", "width3", "height1", photoSelected, "r", 3, 0) %></td>
						</tr>
			    </table>
			    <%Case "Innenriks"%>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("Innenriks", "PhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("innenriks_alle", "width3", "height1", photoSelected, "r", 1, 0) %></td>
						<td valign="top"><%= RenderIFrame("innenriks_krim_ulykker", "width3", "height1", photoSelected, "r", 2, 0) %></td>
						<td valign="top">
								<table BORDER="0" CELLSPACING="0" CELLPADDING="0" valign="top">
									<tr><td valign="top">
									<%= RenderIFrame("innenriks_okonomi_naeringsliv", "width3", "height2", photoSelected, "r", 3, 0) %>
									</td></tr>
									<tr><td valign="top" height="3"></td></tr>
									<tr><td valign="top">
									 <%= RenderIFrame("innenriks_politikk", "width3", "height2", photoSelected, "r", 3, 0) %></td>
									</td></tr>
								</table>
						</tr>
			    </table>
			    <%Case "Utenriks"%>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("Utenriks", "PhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("utenriks_alle", "width2", "height1", photoSelected, "r", 1, 0) %></td>
						<td valign="top"><%= RenderIFrame("utenriks_bakgrunn", "width2", "height1", photoSelected, "r", 2, 0) %></td>
					</tr>
			    </table>
			    <%Case "Okonomi"%>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("�konomi", "PhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("okonomi_innenriks", "width2", "height1", photoSelected, "r", 1, 0) %></td>
						<td valign="top"><%= RenderIFrame("okonomi_utenriks", "width2", "height1", photoSelected, "r", 2, 0) %></td>
					</tr>
			    </table>
			    <%Case "Sport"%>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="2"><%= RenderTitleBar("Sport", "PhotoLink", 620) %></td>
            		</tr>
            		<tr>
            			<td align=left class="news"><a target='_blank' href='http://www.ntb.no/tipperes'>G� til siste resultater fra Norsk Tipping</a></td>
            			<td align=right class="news"><a target='_blank' href='http://www.ntb.no/tippetips'>NTBs fotball- og travtips</a></td>
            		</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("sport_alle", "width2", "height1", photoSelected, "r", 1, 0) %></td>
						<td valign="top"><%= RenderIFrame("sport_tabeller", "width2", "height1", photoSelected, "r", 2, 0) %></td>
					</tr>
			    </table>
			    <%Case "Kultur"%>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("Kultur og underholdning (spesialtjeneste fra NTB)", "PhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td rowspan="2" valign="top"><%= RenderIFrame("kul_reportasjer", "width3", "height1", photoSelected, "r", 1, 0) %></td>
						<td rowspan="2" valign="top"><%= RenderIFrame("kul_notiser", "width3", "height1", photoSelected, "r", 2, 0) %></td>
						<td valign="top"><%= RenderIFrame("kul_menyer", "width3", "height2", photoSelected, "r", 3, 0) %></td>
					</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("kul_showbiz", "width3", "height2", photoSelected, "r", 3, 0) %></td>
					</tr>
			    </table>
			    <%Case "PrivTilRed"%>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("Priv. til red.", "noPhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("priv_menyer", "width2", "height1", photoSelected, "r", 1, 0) %></td>
						<td valign="top"><%= RenderIFrame("priv_tilred", "width2", "height1", photoSelected, "r", 2, 0) %></td>
					</tr>
			    </table>
			    <%Case "PRM"%>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("Pressemeldinger", "noPhotoLink", 620) %></td>
            		</tr>
            		<tr>
            			<td colspan="3" class="news"><a target='_blank' href='<%=Application("presseKonfURL")%>'><%=Application("presseKonf")%></a></td></tr>
            		</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("prm_ntbpluss", "width1", "height1", photoSelected, "r", 1, 0) %></td>
						<!-- <td valign="top"><%= RenderIFrame("prm_bwi", "width3", "height1", photoSelected, "r", 2, 0) %></td> -->
						<td valign="top"><%= RenderIFrame("prm_obi", "width3", "height1", photoSelected, "r", 2, 0) %></td>
						<!-- <td valign="top"><%= RenderIFrame("prm_nwa", "width3", "height1", photoSelected, "r", 3, 0) %></td> -->
						</tr>
			    </table>
			    
			    <%Case "SAK"
					Dim strSak, intKeyWordID, intSakNr
					strSak = Request.QueryString("Sak")
					intSakNr = Request.QueryString("SakNr")
			    %>
			    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar(strSak, "noPhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td valign="top"><%= RenderIFrame("sak_alle", "width2", "height1", photoSelected, "r", 1, intSakNr) %></td>
						<td valign="top"><%= RenderIFrame("sak_bakgrunn", "width2", "height1", photoSelected, "r", 2, intSakNr) %></td>
					</tr>
			    </table>
			    <%
			    case else%>
            	<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
            		<tr>
            			<td colspan="3"><%= RenderTitleBar("Alle nyheter", "PhotoLink", 620) %></td>
            		</tr>
					<tr>
						<td valign="right"><%= RenderIFrame("alle_innenriks", "width3", "height1", photoSelected, "r", 1, 0) %></td>
						<td valign="right"><%= RenderIFrame("alle_utenriks", "width3", "height1", photoSelected, "r", 2, 0) %></td>
						<td valign="right"><%= RenderIFrame("alle_sport", "width3", "height1", photoSelected, "r", 3, 0) %></td>
						</tr>
			    </table>

				<%End Select%>

  </td></tr>
</table>
</body>
</html>
