<%
'***********************************************************************************
'* UserList.asp
'*
'* This file is used to show a list of users belonging to a spesific company or
'* for showing all the customers to NTB
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.26
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.11
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->

<!-- #INCLUDE FILE="../services/include/ad_routines.asp" -->
<!-- #INCLUDE FILE="../services/include/const.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main()

	'check whether the user has the rights to view this page
	if Session("Role") = "Normal" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if
	
	if Session("Role") = "" then
		response.Redirect "../Authfiles/Login.asp"
	end if

End Sub
%>

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">
<script language="JavaScript">
	function setParam(par, link){
		//set the parameters to know what to display
		document.profile.huid.value = par;
		// alert (link);
 		document.profile.action = link;
		
		/*
		
			Just to find out where the heck we are!
		
		*/
		
		if (link == "DeleteManyProfiles.asp") {
			if (confirm("Slette brukere?")) {
				document.profile.submit();
			}
		} else {
			document.profile.submit();
		}
		
		
		
		
		
		
		
		if (link == "DeleteProfile.asp") {
			if (confirm("Slette bruker?")) {
 				document.profile.submit();
 			}
 		} else {
 			document.profile.submit(); 		
 		}
	}
</script>
</head>

<script type="text/javascript" src="../include/sortTable.js"></script>
<body onload="initTable('sortUserList', new Array(null, null, null, null, null, null), 1);">

<%	
Dim saveNew, save, huid, hcoid
Dim strSelectedCompany, strSubject, strContent, iCount, strAdminsOnly

saveNew = Request("makeNew")
huid = Request("huid")
hcoid = Request("hcoid")
save = Request("saveProfile")	

	if Request.Form("sendMail") = "sendMail" then
		strSubject = Request.Form("txtSubject")
		strContent = Request.Form("txtContent")
		strSelectedCompany = Request.Form("lstCompany")
		if len(Request.Form("chkAdminsOnly")) > 0 then
			strAdminsOnly = "2"
		else
			strAdminsOnly = "0"
		end if
		iCount = SendMailToCompanies(strSelectedCompany, strSubject, strContent, strAdminsOnly)
		Response.Write "<DIV class='main_heading2'>" & iCount & " stk E-post er sendt.</DIV>" 
	end if

if saveNew = "newProfile" then
'must make a new profile
	if CreateUserProfile(hcoid)= true then
		' all is ok	
		' But we check if the user has decided to add another one or
		' If he/she wants to get to the list of customers and not users.
	ElseIf Err.number <> 0 then
		Response.Write (Err.Description & ". Try a different user id")
	Else 
		Response.Write ("User already exists. Try a different user id")
	End if
elseif save = "Lagre endring" then
'save the changes made
	UpdateProfile(huid)
End if

'check whether the user came from CompanyList or is a superuser
if hcoid = "" then
'get companyid from the superuser
	Dim CompanyID
	CompanyID = GetCompany(Session("Username"))
else
'Show users for a spesific organization, place brackets
	CompanyID = hcoid 	
end if

if saveNew = "newProfile" then
	if Request.Form("addaction") = "2" then
		' Then we shall redirect the user to the companylist
		response.Redirect "CompanyList.asp"
	elseif Request.Form("addaction") = "3" then
		' Then we shall redirect the user to the form again
		' response.Write "Bruker lagt til, n� skal vi egentlig legge til en til"
		Response.Redirect "EditProfile.asp?huid=%20&coid=" & CompanyID
	end if
end if 	
Response.Write(RenderTitleBar("Liste over brukere hos " & GetCompanyName(CompanyID), "NoPhotoLink", 620)) 
Response.Write "<table width='500' Border='1'>"    

Response.Write(RenderUserList(CompanyID))

%>
</body>
</html>