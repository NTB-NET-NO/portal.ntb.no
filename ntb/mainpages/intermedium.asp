<%@ Language=VBScript %><%
'***********************************************************************************
'* cyberwatcher.asp
'*
'* This file is used for displaying the Intermedium functionality
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of iframes and titlebar.
'*
'*
'* CREATED BY: Lars Hegde, NTB
'* CREATED DATE: 2003.12.17
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->


<HTML>
<HEAD>
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>
<STYLE TYPE="text/css">
A    { text-decoration:none
       }
 </STYLE>


<%= TitleTag() %>
</HEAD>

<BODY topmargin="0" leftmargin="0">
<TABLE height="100%" width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
      <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
      <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160;</TD>
      <TD ALIGN="LEFT" WIDTH=100%>

			<%
			'Access control against organizational access control bitpattern
			if IsNumeric(Session("ACF")) then
				if (CLng(Session("ACF")) and 2048) > 0 then
					'Access granted
					%>

					<script language="JavaScript">
					<!--

						window.location.href = 'http://overview.no/reports/show?pwd=N4T8B12&module=reports&usr=<%=Replace(Session("OrgName")," ","")%>'
						//window.open('http://overview.no/reports/show?pwd=N4T8B12&module=reports&usr=<%=Replace(Session("OrgName")," ","")%>')

					//-->
					</script>

					<!--
					<IFRAME NAME="cyberwatcherFrame" WIDTH="660" HEIGHT="550" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="http://ciconcept.com/reports/show?pwd=N4T8B12&module=reports&usr=<%=Replace(Session("OrgName")," ","")%>" ></IFRAME>
					-->
					<%
				else
					'Access denied
					%>
					<IFRAME NAME="cyberwatcherFrame" WIDTH="660" HEIGHT="550" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="../error/noauth.asp"></IFRAME>
					<%

				end if
			else
				'Access denied
				%>
				<IFRAME NAME="cyberwatcherFrame" WIDTH="660" HEIGHT="550" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="../error/noauth.asp"></IFRAME>
				<%
			end if

			%>

			</TD>
        </TR>
    </TABLE>

  </td></tr>
</table>
</BODY>
</HTML>
