<%
'***********************************************************************************
'* SendMail.asp
'*
'* This file is used to send e-mail to all users of the company, 
'* or many companies if the user is NTB
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: �yvind Andersen
'* CREATED DATE: 2003.11.28
'* UPDATED BY: 
'* UPDATED DATE: 
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->

<!-- #INCLUDE FILE="../services/include/ad_routines.asp" -->
<!-- #INCLUDE FILE="../services/include/const.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main()

	'check whether the user has the rights to view this page
	if Session("Role") = "Normal" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub
%>

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">
	<script language="Javascript">
	<!--
	function CheckInput(){

		if (document.frmSendMail.txtSubject.value == "") {
			alert ("Vennligst skriv inn en overskrift.");
			document.frmSendMail.txtSubject.focus();
		}
		else if (document.frmSendMail.txtContent.value == "") {
			alert ("Vennligst skriv inn en melding.");
			document.frmSendMail.txtContent.focus();
		}
		else if (document.frmSendMail.lstCompany.value == "") {
			alert ("Vennligst velg en eller flere kunder.");
			document.frmSendMail.lstCompany.focus();
		}
		else {
			document.frmSendMail.submit();
		}
	}	
	
	function GetToList(){
		location.href = "UserList.asp"
	}
	-->
	</script>
</head>

<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7">

<%	
Dim CompanyID, strSelectedCompany, strSubject, strContent, iCount, strAdminsOnly

	if Request.Form("sendMail") = "sendMail" then
		strSubject = Request.Form("txtSubject")
		strContent = Request.Form("txtContent")
		strSelectedCompany = Request.Form("lstCompany")
		if len(Request.Form("chkAdminsOnly")) > 0 then
			strAdminsOnly = "2"
		else
			strAdminsOnly = "0"
		end if
		iCount = SendMailToCompanies(strSelectedCompany, strSubject, strContent, strAdminsOnly)
		Response.Write "<DIV class='main_heading2'>" & iCount & " stk E-post er sendt.</DIV>" 
	end if

CompanyID = Request("idOrg")	
'check whether the user came from CompanyList or is a superuser
if CompanyID = "" then
	'get companyid from the superuser
	CompanyID = GetCompany(Session("Username"))
end if
	
	Response.Write(RenderTitleBar("Send e-post til brukerne", "NoPhotoLink", 620)) 
%>
<TABLE width="500" border="0" class="news">
	<form name="frmSendMail" action="UserList.asp" method="Post">
	<%Response.Write RenderSendMail(CompanyID)%>
	</form>
</table>  
</body>
</html>