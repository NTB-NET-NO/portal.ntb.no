<%
'***********************************************************************************
'* HelpReports.asp
'*
'* This file is used to show help descriptions for the reports
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: �yvind Andersen 
'* CREATED DATE: 2003.12.03
'* UPDATED BY: 
'* UPDATED DATE: 
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_stat_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_stat_lib.asp" -->

<!-- #INCLUDE FILE="../services/include/ad_routines.asp" -->
<!-- #INCLUDE FILE="../services/include/const.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main()

	'check whether the user has the rights to view this page
	if lcase(left(Session("Username"), 3)) <> "ntb" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub
%>

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">
</head>

<%	
Response.Write(RenderTitleBar("Hjelpebeskrivelse til rapportene", "NoPhotoLink", 620)) 
Response.Write(RenderHelpReport())
%>
</body>
</html>