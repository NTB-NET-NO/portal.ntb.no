
<%
Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>
<HTML>
<HEAD>
<link href="<%=style%>" type="text/css" rel="stylesheet">
</HEAD>
<BODY>

<CENTER>
  <BR><A href="#problem">TEKNISKE PROBLEMER?</A>
  <BR><A href="#personalization">PERSONALISERING</A>
  <BR><A href="#search">S�K I ARKIVET</A>
  <BR><A href="#newscalendar">NYHETSKALENDER</A>
  <BR><A href="#SMS/WAP">SMS/WAP-TJENESTER</A>
  <BR><A href="#SoundSearch">LYD</A>
  <BR><A href="#Chat">KONTAKT NTB</A>
  <BR><A href="#FAQ">FAQs (ofte stilte sp�rsm�l)</A> 
</CENTER>

<HR width='550'>


<P><BR>&nbsp;</P>
<DIV class=main_heading3><A name=problem>TEKNISKE PROBLEMER?</A></DIV>
<P class=helpFont>

Hvis du ikke finner svaret p� denne
hjelpesiden kan du kontakte <a href="mailto:webmaster@ntb.no">NTBs
IT-avdeling</a>.<br><br>

<b>P�logging:</b> Dersom du har behov for � jobbe p� flere maskiner samtidig, 
kan du ha inntil tre aktive brukersesjoner p� en gang. Men for � v�re sikker p� at 
du f�r logget p� fra en ny maskin (f.eks. en b�rbar), er det viktig at du husker � logge 
deg ut av NTB-portalen n�r du forlater PC-en. Klikk �Logg ut� �verst til h�yre i skjermbildet,
 eller lukk nettleseren.<br><br>

<b>Valg av nettleser:</b>
For � f� full nytte av NTB-portalen b�r du bruke en av disse nettleserne (kan lastes ned gratis):
<br><br>

<u>Hvis du jobber p� PC:</u><br>
<a href="http://www.microsoft.com/windows/ie/downloads/default.asp">Internet Explorer</a> 5.0 eller senere (anbefalt)<br>
<a href="http://wp.netscape.com/computing/download/">Netscape</a> 6.0 eller senere<br>
<a href="http://www.opera.com/download/">Opera </a>6.0 eller senere<br><br>

<u>Hvis du jobber p� Mac (OS 9.x eller OS X):</u><br>

<a href="http://www.microsoft.com/windows/ie/downloads/default.asp">Internet Explorer</a> 5.0 eller senere<br>
<a href="http://wp.netscape.com/computing/download/">Netscape</a> 6.0 eller senere (anbefalt)<br>
<a href="http://www.opera.com/download/">Opera </a>anbefales ikke<br><br>

Dessverre er ikke all funksjonalitet tilgjengelig i alle nettlesere. Unntakene er beskrevet under hvert enkelt kapittel.
 </P>

<DIV class=main_heading3><A name=personalization>PERSONALISERING</A></DIV>
<P class=helpFont>
F�rste gang du logger deg p� NTB-portalen vil siden �Mine nyheter� vise et nyhetsutvalg
 som er tilpasset behovet i din redaksjon/organisasjon. Ved hjelp av funksjonen 
 �Tilpass mine nyheter� kan du endre oppsettet, slik at siden viser det nyhetsutvalget
  som passer <i>deg</i> best. 
(Du kan n�r som helst g� tilbake til standardoppsettet for din redaksjon/organisasjon ved 
� klikke p� lenken �verst til h�yre p� personaliseringssiden).<br><br>

<u>Layout:</u> Ved � klikke p� �Endre vindusoppsett� f�r du mulighet til � velge hvor 
mange vinduer siden skal inneholde (maksimalt 6).<br>
<u>Innhold:</u> N�r du har valgt vindusoppsett f�r du opp en side der du kan velge innhold
 for hvert av vinduene. Klikk p� vinduet som du vil gi et nytt innhold. Du f�r da opp et 
 valg mellom Friteksts�k og Kategoris�k.<br><br> 

Velger du Friteksts�k vil dette vinduet bli st�ende tomt inntil du skriver inn et s�keord 
(boksen for innskriving finner du f�rst n�r du kommer til siden �Mine nyheter�). 
S�keordet kan endres n�r som helst (uten � g� veien om �Tilpass mine nyheter�). 
Friteksts�k oppdateres automatisk hvert 5. minutt - eller n�r du klikker p� S�k-knappen. 
<br><i>NB: Dersom du sitter p� Mac med operativsystemet OS X er det dessverre ikke mulig � velge 
friteksts�k i flere av rutene samtidig.</i><br><br>

Velger du Kategoris�k kan du definere innholdet i vinduet ut fra stofftype, stoffkategori og geografi. Du kan f.eks. velge en enkelt sportsgren, nyheter om et bestemt tema, eller nyheter fra et bestemt geografisk omr�de - i verden eller i Norge. Kategoris�k oppdateres automatisk hvert 60. sekund.

</P>

<DIV class=main_heading3><A name=search>S�K I ARKIVET</A></DIV>
<P class=helpFont>Hvis du bruker s�kevinduet i navigatoren (som er tilgjengelig 
uansett hvor du befinner deg i portalen), s�ker du i NTBs nyhetsproduksjon siste 
7 d�gn. <BR><BR>N�r du velger �Avansert s�k�, kan du s�ke i hele NTBs historiske 
database, som g�r tilbake til 1985. <I>For � lese artikler som er mer enn en uke 
gamle m� du v�re arkivkunde.. NTBs <A 
href="mailto: marked@ntb.no">markedsavdeling</A> - tel 22034509 - gir 
opplysninger om priser og betingelser.</I> <BR><BR><B>Friteksts�k</B> er basert 
p� programvare fra Autonomy, som gj�r det mulig � s�ke med utgangspunkt i hele 
tekstblokker - ikke bare enkeltord. Hvis du velger � s�ke ved hjelp av 
enkeltord, kan du bruke de samme hjelpemidlene som du er vant med fra andre 
friteksts�k. <BR><BR>Hvis du velger � s�ke ved hjelp av enkeltord, f�r du 
vanligvis flere treff. For � �ke treffsikkerheten kan du bruke de samme 
hjelpemidlene som du er vant med fra andre friteksts�k: <BR>
<UL class=helpFont>
  <LI>AND mellom s�keordene gir treff p� artikler som inneholder begge 
  s�keord</LI>
  <LI>OR mellom s�keordene gir treff p� artikler som inneholder ett av 
  s�keordene</LI>
  <LI>NOT foran et s�keord s�rger for at artikler som inneholder dette s�keordet 
  ikke registreres som treff</LI>
  <LI>anf�rselstegn (f.eks. <I>�Jens Stoltenberg�</I>) brukes til � sikre at du 
  f�r treff bare n�r ordene st�r sammen i teksten, noe som kan v�re spesielt 
  nyttig i forbindelse med personnavn. (Hvis du skriver inn to eller flere ord 
  uten noe tegn mellom, vil s�kemotoren tolke det som om du hadde skrevet 
  OR)<br>OBS: S�kemotoren leser ikke bindestrek! Hvis du vil s�ke p� kronprinsesse Mette-Marit m� du skrive
navnet i anf�rselstegn: �Mette-Marit� (hvis ikke f�r du treff p� alle som heter
enten Mette eller Marit)</LI>
  <LI>trunkering ved hjelp av stjerne (f.eks. <I>storting*</I>) gir deg treff p� 
  alle sammensatte ord med samme begynnelse (<I>stortingsbygning, 
  stortingsvedtak, stortingsrepresentant </I>osv.)</SPAN></LI></UL>

<P class=helpFont><B>Kategoris�k:</B> <BR>Du kan velge � avgrense friteksts�ket 
til bestemte stoffkategorier. Det gir bedre treffsikkerhet. Du kan ogs� utf�re 
s�k bare ved hjelp av kategorioversikten og den geografiske inndelingen av 
nyhetene som NTB har foretatt. <BR><BR>Hvis du f.eks. �nsker � finne alle 
�stfold-nyheter fra den siste m�neden, eller alt NTB har skrevet om curling det 
siste �ret, er kategorisk den raskeste og mest effektive metoden.. 
<BR><BR><B>Tidsavgrensing</B> <BR>For � avgrense s�ket til en bestemt tidsperiode 
velger du "Spesifiser dato" fra rullegardinmenyen under "Tid". Deretter kan du bruke 
Tab-tasten og piltastene til � velge dato, m�ned og �r.<BR><BR><I>TIPS: Hvis du vil 
s�ke etter meldinger fra en bestemt dato m� du velge samme dato ib�de i Fra- og 
Til-felt.</I> 

<BR><BR><B>S�keresultat:</B> <BR>P� listen over s�keresultater kommer den nyeste 
artikkelen �verst. N�r du bruker friteksts�k f�r du informasjon om hvor relevant 
artikkelen er i forhold til s�kekriteriet. Du kan ogs� velge � sortere listen 
etter relevans, slik at den saken som best matcher s�kekriteriene legger seg 
�verst. <BR><BR>Du kan se ingressen uten � �pne artikkelen ved � holde musepila 
over nyhetsoverskriften. <BR><BR>For � lese hele artikkelen klikker du p� overskriften, 
p� samme m�te som i nyhetslistene. <BR><BR><I>Hvis artikkelen er mer enn 7 dager gammel, 
m� du v�re arkivkunde for � f� leseadgang. NTBs 
<A href="mailto:marked@ntb.no">markedsavdeling</A> - tel 22034509 - gir 
opplysninger om priser og betingelser.</I> </P>

<DIV class=main_heading3><A name=newscalendar>NYHETSKALENDER</A></DIV>
<P class=helpFont>Standardvisningen omfatter alle dagens begivenheter - 
innenriks, utenriks og p� sportsfronten. Du kan avgrense eller utvide visningen 
p� flere m�ter: <BR><BR><B>Geografisk plassering:</B> Her velger du om du vil ha 
innenriks- eller utenriksbegivenheter. (Hvis du ikke gj�r noe valg f�r du 
begge). Hvis du bare vil se sportskalenderen skal du bruke Kategori-valget. 
<BR><BR><B>Dato:</B> Du kan velge � se begivenhetene for i morgen, neste uke, 
neste m�ned, eller enda lengre fram. Du kan ogs� bruke kalendervinduet til selv 
� bestemme et tidsrom. <BR><BR><B>Kategori:</B> Hendelsene i nyhetskalenderen er 
inndelt i de samme 17 hovedkategoriene som du finner i NTBs nyhetstjeneste. Bruk 
denne listen hvis du bare vil se sportskalenderen. Bruk CTRL-tasten hvis du vil 
velge flere kategorier. (Hvis du ikke gj�r noe valg f�r du alle). 
<BR><BR><B>Type:</B> Du kan skille mellom endagshendelser og hendelser som l�per 
over flere dager. (Hvis du ikke gj�r noe valg f�r du alle). 
<BR><BR><I><B>TIPS:</B> Du kan klikke og fra i stolpen som skiller de to delene 
av skjermen, for � gi st�rre plass til det vinduet der hendelsene er listet 
opp.</I> </P>

<DIV class=main_heading3><A name=SMS/WAP>SMS/WAP-TJENESTER</A></DIV>
<P class=helpFont><I>For � f� tilgang til denne tjenesten m� du v�re registrert 
som mobilkunde. NTBs <A href="mailto:marked@ntb.no">markedsavdeling</A> - tel 
22034509 - gir opplysninger om priser og betingelser.</I> <BR><BR>Du logger deg 
inn p� tjenesten ved hjelp av ditt personlige mobiltelefonnummer og et passord 
som du f�r fra NTB. Har du glemt passordet kan du f� tilsendt et nytt (se logg 
inn-siden). <BR><BR><B>SMS-tjenester:</B> <BR>Her kan du skreddersy din egen 
varslingstjeneste. Du har to valgmuligheter: <BR><BR><I>Breaking news:</I> Dette 
er NTBs hastemeldinger, tilsendt som SMS. Du velger selv om du vil ha full 
dekning (innenriks, utenriks, sport og priv. til red), eller bare enkelte 
stoffomr�der. Hvis du krysser av for samtlige kategorier, vil du i gjennomsnitt 
motta tre meldinger per dag (men antallet varierer selvsagt mye med 
nyhetsbildet). <BR><BR>Tjenesten kan enkelt sl�s av og p� n�r du selv �nsker. 
<BR><BR><I>Personalisert varsling:</I> Du kan velge � bli varslet via SMS n�r en 
bestemt type melding sendes ut i NTB-tjenesten. Du kan spesifisere varslingen 
til � gjelde meldinger som inneholder konkrete s�keord (f.eks. et person- eller 
bedriftsnavn). <BR><BR>Du kan ogs� velge � bli varslet om bestemte meldingstyper 
(f.eks. nyheter som ber�rer ditt fylke, nyheter fra en idrettsgren du f�lger 
spesielt med p� osv.). <BR><BR><I>OBS: N�r du velger personalisert varsling, er 
meldingene hentet direkte fra NTBs generelle nyhetstjeneste - dvs. at de ikke er 
spesialskrevet for det korte SMS-formatet. Det inneb�rer at SMS-meldingen du 
mottar kan v�re kuttet midt i en setning (gjelder ikke Breaking news).</I> 
<BR><BR><B>Wap:</B> <BR>Her kan du sette opp din egen personaliserte 
wap-tjeneste, som gj�r det mulig � f�lge NTB-nyhetene via mobiltelefon. Du 
velger selv hvilke nyhetskategorier du vil ha med (omtrent p� samme m�te som du 
kan personalisere siden �Mine nyheter� i NTB-portalen). <BR><BR>Adressen til din 
personlige wap-side er: <I>wap.ntb.no/xxxxxxxx</I> (x-ene st�r for ditt 
mobiltelefonnummer). NB: du f�r ikke tilgang til denne wap-siden f�r du er 
registrert som mobilkunde via NTB-portalen. <BR><BR>P� wap-siden vil du i 
tillegg til ditt personaliserte nyhetsutvalg ogs� finne en s�kefunksjon som lar 
deg utf�re stikkords�k i hele det siste d�gnets NTB-nyheter direkte fra 
mobiltelefonen. <BR><BR><I>TIPS: Sender du et s�k uten � spesifisere noe 
s�keord, f�r du opp alle nyheter som er sendt ut den siste timen.</I> </P>

<DIV class=main_heading3><A name=SoundSearch>LYD</A></DIV>
<P class=helpFont>P� portalsiden �Lydsaker� finner du lydklipp knyttet til 
nyhetssaker fra den siste uka. Lydklippene er normalt ca. 20 sekunder lange, og 
er beregnet for bruk p� nettsteder eller i radio. <BR><BR>

For � h�re lyden holder du musepila over h�yttalerikonet (dette forutsetter at du
 har installert Flash Player, som kan lastes ned gratis fra  
<a href="http://www.macromedia.com/downloads/" >www.macromedia.com/downloads/</a>. 

<br><br><i>OBS: For PC-brukere er denne funksjonaliteten dessverre <u>ikke</u> tilgjengelig dersom du 
har Netscape 6.x som nettleser. Det g�r bra med Internet Explorer 4.x, 5.x eller 6.x, 
med Opera 6.0 og med Netscape 3.x eller 4.x. <br>
For Mac-brukere er funksjonaliteten <u>bare</u> tilgjengelig dersom du bruker Netscape 3.x 
eller 4.x som nettleser (med Netscape 6.x, eller med Internet Explorer eller Opera 
virker det dessverre ikke). <br>
Som alternativ kan du h�re lyden ved � klikke p� lenken �MP3�. </i>

<BR><BR>For � laste ned lyden til 
videre bruk klikker du p� lenken �HTML-kode� (her har du valget mellom tre 
formater: Flash, MP3 eller Windows Media Player) <BR><BR>Det er fri tilgang til 
� h�re p� lydklippene, men for � laste ned lyd til videre bruk m� du v�re kunde 
p� lydtjenesten. NTBs <A href="mailto:marked@ntb.no">markedsavdeling</A> - tel 
22034509 - gir opplysninger om priser og betingelser.</I> 
<BR><BR><B>Lydarkiv</B> <BR>Nyhetsportalen inneholder et arkiv med alle 
lydillustrasjoner NTB har produsert siden denne tjenesten ble introdusert i 
august 2001. Ved � skrive inn et s�keord i s�kefeltet nederst p� lydsiden kan du 
finne fram til tidligere utsendte lydklipp <BR><BR><I>Tips: S�ket utf�res i 
filnavnet p� lydklippene (ikke i hele teksten p� meldingen som lydillustrasjonen 
er knyttet til). For � finne det du leter etter er det best � s�ke p� 
personnavn. Eksempel: </I>S�k etter <I>Jagland</I> vil gi treff p� alle lydklipp 
der Jagland uttaler seg. </P>

<DIV class=main_heading3><A name=Chat>KONTAKT NTB</A></DIV>
<P class=helpFont><B>Telefon:</B> <BR>Hvis du har sp�rsm�l om NTB-tjenesten 
finner du de viktigste telefonnumrene i navigatoren �verst p� skjermsiden. 
<BR><BR><A href="mailto:vaktsjef@ntb.no">Vaktsjef</A>: Bemannet hele d�gnet. 
Svarer p� alle sp�rsm�l om innenrikstjenesten. <BR><A 
href="mailto:reportasjeleder@ntb.no">Reportasjeleder</A>: Ansvarlig for 
planlegging av dagen og morgendagen. Svarer p� sp�rsm�l om hvilke saker NTB 
dekker, og hvordan. Bemannet hverdager 0630-2130. <BR><A 
href="mailto:utenriks@ntb.no">Utenriks</A>: Bemannet hele d�gnet. Svarer p� alle 
sp�rsm�l om utenrikstjenesten. <BR><A href="mailto:sporten@ntb.no">Sport</A>: 
Normalt bemannet fra kl 07 til kl 23. <BR><A 
href="mailto:plussvaktsjef@ntb.no">NTB Pluss </A>: Svarer p� sp�rsm�l om Kultur- 
og underholdningstjenesten, Feature-tjenesten og Pressemeldingstjenesten. 
Bemannet hverdager 08-18. <BR><A href="mailto:desk@scanpix.no">Foto </A>: Svarer 
p� sp�rsm�l om fototjenesten fra SCANPIX. Bemannet 06.00 - 24.00 hverdager, 
13.00 - 24.00 l�rdag og s�ndag. <BR><FONT color=#da6803>Reportere</FONT>: 
Klikker du p� lenken �Kontakt NTB� f�r du opp en liste over samtlige 
NTB-medarbeidere, med mobiltelefonnumre og e-postadresser. Klikk p� overskriften 
�Avdeling� hvis du vil sortere listen avdelingsvis. <BR><A 
href="mailto:webmaster@ntb.no">Webmaster </A>: Svarer p� tekniske sp�rsm�l om 
nyhetsportalen. <BR><A href="mailto:marked@ntb.no">Markedsavdelingen </A>: 
Svarer p� sp�rsm�l om priser og betingelser for de ulike NTB-tjenestene. 
<BR><BR><B>Chat:</B> <BR>Til vaktsjef, reportasjeleder utenriks- og sportsvakt 
er det opprettet direkteforbindelse, slik at du kan stille dine sp�rsm�l via en 
chat-kanal. Klikk p� ikonet som st�r til venstre for hver av 
stillingsbetegnelsene �verst p� skjermsiden (f.eks. �Vaktsjef�). <BR>Kanalen er 
beregnet p� korte sp�rsm�l og svar. Forbindelsen er satt opp slik at bare tre 
personer av gangen f�r tilgang. Hvis den du vil ha kontakt med er opptatt med � 
svare p� andre henvendelser, blir du bedt om � vente litt og pr�ve igjen. 
Alternativ kan du sende en e-post. <BR><BR><B>E-post:</B> <BR>Hvis du velger � 
sende e-post kan du klikke p� stillingsbetegnelsen til den du vil ha tak i. Da 
f�r du opp et e-postskjema der adressen er ferdig utfylt. <BR>Du kan sende 
e-post direkte til journalisten ved � klikke p� e-postadressen som st�r oppf�rt 
under hver enkelt nyhetsartikkel. </P>

<DIV class=main_heading3><A name=FAQ>FAQs (ofte stilte sp�rsm�l)</A></DIV>
<P class=helpFont><B>Hvordan blar jeg fram og tilbake i nyhetslistene?</B> 
<BR>N�r du klikker p� en nyhetsoverskrift �pner meldingen seg i et eget 
lesevindu. Her kommer det ogs� opp lenker til relaterte saker, hentet fra den 
siste ukens NTB-produksjon. <BR>Klikker du p� en av disse lenkene vil den nye 
saken erstatte den f�rste. Det samme skjer hvis du klikker p� en ny overskrift 
fra katalogen. <BR>
<I>TIPS: Hvis du har Internet Explorer som nettleser kan du bruke Backspace-tasten 
til � bla deg tilbake til tidligere leste saker. Alternativt kan du klikke p� Tilbake-lenken
 som st�r over artikkelen.
</I> 

<BR><BR><B>Kan jeg ha flere artikler �pne 
samtidig?</B> <BR>Du kan �pne en ny artikkel i et nytt vindu ved � h�yreklikke 
p� overskriften og velge ��pne i nytt vindu�. <BR><BR><B>Kan jeg endre 
lesevinduet?</B> <BR>Hold musepila over ett av hj�rnene, eller p� en av 
ytterkantene. Klikk og dra for � endre utseende/st�rrelse p� vinduet. Klikk p� 
den �verste bl� linjen for � flytte hele vinduet til et annet sted p� skjermen. 
<br>
Hvis du bruker Internet Explorer som nettleser kan du endre st�rrelsen p� skriften 
i lesevinduet ved � g� til Vis i nettlesermenyen i hovedvinduet og deretter velge 
Tekstst�rrelse. Denne funksjonaliteten er dessverre ikke tilgjengelig hvis du bruker 
Netscape eller Opera.

<BR><BR><B>Kan jeg s�ke i nyhetslistene?</B> <BR>Du kan gj�re hurtigs�k i 
nyhetslistene ved � trykke CTRL+F og fylle inn et s�keord. Da s�ker du i 
overskriften p� alle nyheter innenfor de siste 24 timene. S�ket g�r suksessivt 
gjennom samtlige lister p� den aktuelle siden. <BR><I>Eksempel:</I> Hvis du 
�pner siden <I>Alle nyheter</I> og trykker CTRL+F vil s�ket g� gjennom f�rst 
innenrikskatalogen, s� utenrikskatalogen og til slutt sprotskatalogen. <BR>OBS: 
Hvis du f�rst �pner og leser en utenrikssak, og deretter trykker CTRL+F, vil 
s�ket f�rst g� gjennom utenrikskatalogen og deretter sportskatalogen. Dermed g�r 
du glipp av det som m�tte v�re � finne i innenrikskatalogen. 

<br><br><b>Hvordan f� utskrift av nyhetslistene?</b><br>
Hvis du bruker Internet Explorer som nettleser kan du aktivere den listen du �nsker utskrift av (ved f�rst � �pne en sak p� listen og s� lukke den igjen) og deretter taste CTRL + P. 
<br>Hvis du bruker Netscape m� du f�rst merke hele listen (ved hjelp av musepila og/eller piltastene p� tastaturet). S� taster du CTRL + P. N�r utskriftsvinduet kommer opp velger du �Merket omr�de�. (Hvis du ikke velger �Merket omr�de� vil Netscape skrive ut hele skjermbildet.
<br>Hvis du bruker Opera kan du f�lge samme framgangsm�te som for Netscape, men listen vil skrives ut uten linjeskift. Dette problemet h�per Opera-utviklerne � l�se i neste versjon av nettleseren. 


<br><br><b>Hvor ofte oppdateres nyhetslistene?</b><br>
Alle standard nyhetslister oppdateres hvert 60. sekund. Eneste unntak er Lydsaker, der oppdatering kun skjer n�r du klikker p� lenken.
<br>P� den personaliserte siden �Mine nyheter� oppdateres nyhetskataloger basert p� kategoris�k hvert 60. sekund, mens friteksts�k oppdateres hvert 5. minutt (eller n�r du klikker p� S�k-knappen).


<BR><BR><B>Hvordan 
blir jeg kvitt klikkelyden som h�res hver gang nyhetslistene oppdateres?</B> 
<BR>Dette er en systeminnstilling i Windows. Du kan fjerne lyden (eller endre 
den) p� f�lgende vis: 
<UL class=helpFont>
  <LI>Klikk p� 'Start', velg 'Innstillinger' og deretter 'Kontrollpanel'</LI>
  <LI>Dobbeltklikk p� ikonet 'Lyd og multimedia (Sounds and multimedia)' for � 
  �pne dette valget.</LI>
  <LI>Velg 'Lyd'-fliken, og finn valget 'Windows Explorer - Start navigation' 
  fra listen. Velg [ingen] eller en annen lyd etter �nske.</LI>
  <LI>Klikk 'Ok' for � aktivere endringen.</LI></UL>
  <br>
</BODY>
</HTML>
