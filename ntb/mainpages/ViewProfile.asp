<%
'***********************************************************************************
'* ViewProfile.asp
'*
'* This file is used to show profile-information about a spesific user or a spesific
'* organization
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.18
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.04.18
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main()

	'check whether the user has the rights to view this page
	if Session("Role") = "Normal" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub

%>

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">
	<script language="JavaScript">
	function SetLink(link){
	
		document.frmProfile.action = link;
		document.frmProfile.submit();
	}
	</script>
</head>

<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7" >

<% 
Dim huid, hcoid
huid = Request("huid")
hcoid = Request("hcoid")

Response.Write(RenderTitleBar("Profil for " & GetUserName(huid), "NoPhotoLink", 620)) %>

<TABLE border="0" class="news">
	<%Response.Write(RenderUserProfile(huid))%>
	<tr><td>&nbsp</td></tr>
	<tr>
	    <TD colspan="2">
		<form name="frmProfile" method="post"> 
		<input type="hidden" name="huid" value="<%=huid%>">
		<input type="hidden" name="hcoid" value="<%=hcoid%>">
		<INPUT class="formbutton" type="button" value="Endre" onClick="SetLink('EditProfile.asp')" name="editProfile" style="background:#bfccd9 none; color:#003366; width:100px">
		<INPUT class="formbutton" type="button" value="Slett" onClick="SetLink('DeleteProfile.asp')" name="delProfile" style="background:#bfccd9 none; color:#003366; width:100px">
		<INPUT class="formbutton" type="button" value="Tilbake til listen" onClick="SetLink('UserList.asp')" name="toList" style="background:#bfccd9 none; color:#003366; width:100px">				
		</FORM>
		</td>
	</tr>
</table>  
</body>
</html>

