<%@ Language=VBScript %><%
'***********************************************************************************
'* nyhetskalender.asp
'*
'* This file is used for displaying the existing Agenda functionality. This page
'* only includes the existing Agenda page (http://www.asistir.no/agendaen/v2/eventlist.asp?offset=0)
'* in an iframe
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of iframes and titlebar.
'*
'* NOTES:
'*		The link must be updated!!!!!!!!!!!!!!!!!
'*
'* CREATED BY: Trond Orrestad, Andersen
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Trond Orrestad, Andersen
'* UPDATED DATE: 2002.06.12
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************


%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->


<%

'Response.Cookies("PORTAL")("PORTAL_REFERER") = "Nyhetskalender"
'Response.Cookies("PORTAL").Path = "/"

%>

<HTML>
<HEAD>
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>
<STYLE TYPE="text/css">
A    { text-decoration:none
       }
 </STYLE>


<%= TitleTag() %>
</HEAD>

<BODY topmargin="0" leftmargin="0">
<TABLE height="100%" width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
      <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
      <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160;</TD>
      <TD ALIGN="LEFT" WIDTH=100%>
            <%= RenderTitleBar("NTB Nyhetskalender", "noPhotoLink", 660) %>

			<%
			'Access control against organizational access control bitpattern
			if IsNumeric(Session("ACF")) then
				if (CLng(Session("ACF")) and 4096) > 0 then
					'Access granted
					%>
					<IFRAME NAME="nyhetskalenderFrame" WIDTH="660" HEIGHT="500" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="../../Nyhetskalender/search_event.asp?login=portal&pwd=portal"></IFRAME>
					<%
				else
					'Access denied
					%>
					<IFRAME NAME="nyhetskalenderFrame" WIDTH="660" HEIGHT="500" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="../error/noauth.asp"></IFRAME>
					<%

				end if
			else
				'Access denied
				%>
				<IFRAME NAME="nyhetskalenderFrame" WIDTH="660" HEIGHT="500" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="../error/noauth.asp"></IFRAME>
				<%
			end if

			%>

			<!--IFRAME NAME="nyhetskalenderFrame" WIDTH="660" HEIGHT="500" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="http://www.asistir.no/agendaen/v2/webagenda.asp"></IFRAME-->
			</TD>
        </TR>
    </TABLE>

  </td></tr>
</table>
</BODY>
</HTML>
