<%@ Language=VBScript %>
<%
'***********************************************************************************
' chat.asp
'
' This file is used for giving a message to a chat user, when the maximum # of
' user is already in the chat room. 
' 
' USES:
' NOTES:
' CREATED BY: Vegar Paulsen, Andersen 
' CREATED DATE: 2002.06.03
' UPDATED BY: Vegar Paulsen, Andersen
' UPDATED DATE: 2002.06.03
' REVISION HISTORY: none
'
'
'***********************************************************************************
%>

<HTML>
<HEAD>
<TITLE>Advarsel</TITLE>
</HEAD>
<%
Dim strReqChanName, reqChan, reqMail

reqChan = Request("channel")
select case reqChan
	case "report"
	strReqChanName = "Reportasjeleder"
	reqMail = "reportasjeleder@ntb.no"
	case "sport"
	strReqChanName = "Vaktsjef p� sporten"
	reqMail = "sporten@ntb.no"
	case "uriks"
	strReqChanName = "Utenriksvakt"
	reqMail = "utenriks@ntb.no"
	case "vakt"
	strReqChanName = "Vaktsjef"
	reqMail = "vaktsjef@ntb.no"
	case else
	strReqChanName = "Den du �nsker � kontakte"
	reqMail = "helen.vogt@ntb.no"
	
end select

%>

<BODY>
<P><STRONG>
<%=strReqChanName%> er dessverre opptatt med � snakke med noen andre. Vennligst fors�k igjen om noen minutter - eller send en <a href="mailto:<%=reqMail%>">e-post</a>.
</STRONG></P><hr>

</BODY>
</HTML>
