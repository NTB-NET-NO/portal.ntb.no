<%
'***********************************************************************************
'* Search.asp
'*
'* This file is used for entering search queries. Both freetext search and category
'* search is possible. The different drop down list are mostly populated from the
'* database. The date options in the search query is validated prior to submitting.
'* Several Javascripts are handling logic on the different form objects (check boxes,
'* listboxes etc) to make selection more user friendly and smart.
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar.
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Trond Orrestad, Andersen
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.28
'* REVISION HISTORY:
'* UPDATED 2002.11.19, by: LCH, NTB
'* UPDATED 2003.09.o1, by: LCH, NTB ( Adapted to Roars Autonomy search )
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_cache_lib.asp" -->
<%
Dim TimePeriod(6,1)
TimePeriod(0,0) = "Siste 7 dager"
TimePeriod(1,0) = "Siste 24 timer"
TimePeriod(2,0) = "Siste m�ned"
TimePeriod(3,0) = "Siste halv�r"
TimePeriod(4,0) = "Siste �r"
TimePeriod(5,0) = "Spesifiser dato"
TimePeriod(6,0) = "Hele arkivet"

TimePeriod(0,1) = "7"
TimePeriod(1,1) = "1"
TimePeriod(2,1) = "31"
TimePeriod(3,1) = "183"
TimePeriod(4,1) = "365"
TimePeriod(5,1) = "0"
TimePeriod(6,1) = "9"

Dim i, WorldRegionAll, LocalRegionAll

Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>

<html>
<head>
<script language="javascript">

//function to validate the different date options prior to submitting the form
function onSubmit_ValidateForm(activeform) {

	/*/Verify if a cross-search is in progress - OUTDATED we use Autonomy for all searches now

	var catUsed = false;

	for ( i = 1; i < activeform.MainGroupListBox.options.length; i++ ) {
		if ( activeform.MainGroupListBox.options[i].selected == true ) catUsed = true;
	}

	for ( i = 1; (i < activeform.SubGroupListBox.options.length) && (!catUsed); i++ ) {
		if ( activeform.SubGroupListBox.options[i].selected == true ) {
			catUsed = true;
			break;
		}
	}

	for ( i = 1; (i < activeform.MainCategoryListBox.options.length) && (!catUsed); i++ ) {
		if ( activeform.MainCategoryListBox.options[i].selected == true ) {
			catUsed = true;
			break;
		}
	}

	if ( ! catUsed ) {
		catUsed = !(activeform.LocalRegionAllCheckBox.checked && activeform.WorldRegionAllCheckBox.checked);
	}
	*/

	//New datecheck
	if ( activeform.TimePeriodListBox.options[activeform.TimePeriodListBox.selectedIndex].text == "Spesifiser dato") {

		//New date check
		rx = /^\d{2}\.\d{2}\.\d{4}$/;
		fd = activeform.f_date.value;
		td = activeform.t_date.value;

		//Check dates
		if ( fd == "" && td == "" ) {
			alert("Du har ikke angitt datoer.");
			activeform.f_date.focus();
			return false;
		}
		else if ( ! rx.test(fd) && fd != "" ) {
			alert("Du har angitt en ugyldig 'Fra'-dato.");
			activeform.f_date.focus();
			return false;
		}
		else if ( ! rx.test(td) && td != "" ) {
			alert("Du har angitt en ugyldig 'Til'-dato.");
			activeform.t_date.focus();
			return false;
		}
		else {

			var fa = new Array(3);
			var ta = new Array(3);
			var dt = new Date();

			if ( fd != "" ) { fa = fd.split("."); }
			else {
				fa = ["01","01","1985"];
			}

			if ( td != "" ) { ta = td.split("."); }
			else {
				var m = (dt.getMonth()+1)
				var d = dt.getDate()

				if ( m < 10 ) { m = "0" + m; }
				if ( d < 10 ) { d = "0" + d; }

				ta = [d,m,dt.getFullYear()];
			}


			if ( fa[0] > 31 || fa[1] > 12 || fa[0] < 1 || fa[1] < 1 ) {
				alert("Du har angitt en ugyldig 'Fra'-dato.");
				activeform.f_date.focus();
				return false;

			} else if ( ta[0] > 31 || ta[1] > 12 || ta[0] < 1 || ta[1] < 1 ) {
				alert("Du har angitt en ugyldig 'Til'-dato.");
				activeform.t_date.focus();
				return false;

			} else if ( fa[2] < 1985 ) {
				alert("Du kan ikke s�ke p� artikler fra f�r 1985.");
				activeform.f_date.focus();
				return false;

			} else if ( ta[2] < 1985 ) {
				alert("Du kan ikke s�ke p� artikler fra f�r 1985.");
				activeform.t_date.focus();
				return false;

			} else if ( fa[2]+fa[1]+fa[0] > ta[2]+ta[1]+ta[0] ) {
				alert("Du har angitt et ugyldig tidsintervall. 'Fra'-dato m� v�re mindre enn 'Til'-dato!");
				activeform.t_date.focus();
				return false;

			} else {
				activeform.dates.value = fa.join(";") + ";" + ta.join(";");
			}
		}
	}

/*
	if ( activeform.freeTextEntry.value != "" && catUsed )
	'{
		return confirm("Du har valgt � s�ke med b�de fritekst og kategoriangivelse.\nP� grunn av mindre problemer med s�kefunksjonen kan dette i noen tilfeller gi ufullstendig resultatsett.\nDette gjelder n�r en s�ker over store tidsintervaller. Det arbeides med � rette problemet.\n\nKlikk 'OK' for � fortsette s�ket, eller 'Avbryt' for � endre kriteriene.");
	}
*/
	return true;
}

function autoFillDate(datefield) {

	var text = datefield.value;
	var d = new Date();
	var year;

	//Substitute separators
	rxSub = /[- /]/g;
	text = text.replace(rxSub,".");

	//Test for missing zeros
	rxZ1 = /^(\d{1})$/;
	rxZ2 = /^(\d{1})(\..+)/;
	rxZ3 = /(.+\.)(\d{1})$/;
	rxZ4 = /(.+\.)(\d{1})(\..+)/;

	text = text.replace(rxZ1,"0$1");
	text = text.replace(rxZ2,"0$1$2");
	text = text.replace(rxZ3,"$10$2");
	text = text.replace(rxZ4,"$10$2$3");

	//Test for autofilling of dates
	rx1 = /^\d{2}$/;
	rx2 = /^\d{2}\.\d{2}$/;
	rx3 = /^(\d{2}\.\d{2}\.)(\d{2})$/;
	rx4 = /^\d{2}\.\d{2}\.\d{4}$/;

	if ( rx4.test(text) ) {
		//do nothing
	} else if ( rx3.exec(text) ) {
		year = RegExp.$2;
		if (year < 50) year = 20 + year;
		else if (year < 100) year = 19 + year;

		text = text.replace(rx3,"$1" + year);

	} else if ( rx2.test(text) ) {
		text += "." + d.getFullYear();
	} else if ( rx1.test(text) ) {
		var m = (d.getMonth()+1)
		if ( m < 10 ) m = "0" + m;
		text += "." + m + "." + d.getFullYear();
	}

	datefield.value = text;
}


 //function to uncheck all region checkbox options if the all option is selected
 function onClick_unCheckAll(chechkboxName){
  for(i=0; i < document.forms.searchform.length; i++){
   if(document.forms.searchform.elements[i].name == chechkboxName){
    document.forms.searchform.elements[i].checked = false;
   }
  }
 }


//function to blank the date text fields if not Specify date is selected.
function onClick_disableDate(){

	if (document.forms.searchform.elements.TimePeriodListBox.options[document.forms.searchform.elements.TimePeriodListBox.selectedIndex].text != "Spesifiser dato") {
		searchform.f_date.value = "";
		searchform.t_date.value = "";
	}
}

//Autoselects specify time in the time selectbox
function setSpecifyTime(form){
	form.TimePeriodListBox.selectedIndex = 5;
}


// function to preselect a value in main category listbox, based on choice of maingroup listbox.
function mgSelected()
{
	var myBrowserName = navigator.appName;
	var mgOptionText = document.forms.searchform.elements.MainGroupListBox.options[document.forms.searchform.elements.MainGroupListBox.selectedIndex].text
	var flag = false
	var mcOptionText
	for(i=1; i < document.forms.searchform.elements.MainCategoryListBox.length; i++)
	{
		mcOptionText = document.forms.searchform.elements.MainCategoryListBox.options[i].text
		if (mcOptionText == mgOptionText)
		{
			flag = true
			document.forms.searchform.MainCategoryListBox.selectedIndex=i;
	//		document.forms.searchform.elements.MainCategoryListBox.disabled = true;
		}
	}

	if (flag == false)//selected option in maingroup doesn't match any group in maincategory
	{
		document.forms.searchform.MainCategoryListBox.selectedIndex=0;
	//	document.forms.searchform.elements.MainCategoryListBox.disabled = false;

	}
	if (myBrowserName == 'Netscape') // Netscape needs a refresh to fill sub category listbox
	{
		window.location.reload();
	}
	fillsubcat(searchform)
}

</script>
<link href="<%=style%>" type="text/css" rel="stylesheet">
<style TYPE="text/css">
A    { text-decoration:none
       }
 </style>
<%= TitleTag() %>
</head>

<meta name="VI60_defaultClientScript" content="VBScript">
<BODY topmargin="0" leftmargin="0" onLoad="onClick_disableDate()">
<TABLE WIDTH=100% HEIGHT=100% border="0" CELLSPACING="0" CELLPADDING="0">
  <TR VALIGN="TOP">
    <TD colspan="3" bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
  </TR>
  <TR VALIGN="TOP">
    <TD ALIGN="LEFT" bgcolor="#003084">
    <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
    </TD>
    <TD ALIGN="LEFT">&#160; </TD>
    <TD ALIGN="LEFT" class="news11" width=100%>
    <FORM name="searchform" Action="searchresults.asp" Method="post" onSubmit="return onSubmit_ValidateForm(this)" >
    <input type=hidden name=dates value="">

             <%= RenderTitleBar("Avansert s�k", "noPhotoLink", 620) %>

           <table VALIGN="TOP" BORDER="0" CELLSPACING="0" CELLPADDING="0">
			<!-- ALIGN="LEFT" makes the categoryboxes jump in Netscape 6.2-->
             <tr><td>
				<table borderColor="#61c7d7" width="600" cellSpacing="0" cellPadding="1" align="left" border="0">
					<tr>
						<td class="news11" vAlign="top" width="600">
							<table class="news11" width="600" cellSpacing="0" cellPadding="0" align="left" border="0">
									<tr>
									  <td>P� denne siden har du mulighet til � s�ke i NTBs l�pende nyheter og i nyhetsarkivet. (For � lese nyheter fra arkivet er du n�dt til � abonnere p� arkivtjenesten. Les mer om dette <a href="HelpSite.asp?anchor=search">her</a>.</td>
									</tr>
									<tr>
									  <td height="5"></td>
									</tr>
        					</table>
        				</td>

					</tr>
				</table>
   			</td></tr>

   			<tr><td height="7">
			</td></tr>

            <tr><td>
				<table borderColor="#61c7d7" width="600" cellSpacing="0" cellPadding="1" align="left" border="1">
					<tr>
						<td class="news11" valign="middle" style="border:none;">&nbsp;
							S�k etter ord eller setning:
						</td>
						<td class="news11" vAlign="top" width="450" style="border:none;">
							<textarea id="text1" class="news11" name="freeTextEntry" style="height:40px; width:450px;"></textarea>
						</td>
					</tr>
				</table>
   			</td></tr>

   			<tr><td height="7">
			</td></tr>
   			<tr><td>
				<table borderColor="#61c7d7" height="50" width="600" cellSpacing="0" cellPadding="1" align="left" border="1">
					<tr>
						<td class="news11" vAlign="top" width="137" style="border:none;">
							Stoffgruppe
						</td>
						<td class="news11" vAlign="top" width="107" style="border:none;">
							Undergruppe
						</td>
						<td class="news11" vAlign="top" width="173" style="border:none;">
							Kategori
						</td>
						<td class="news11" vAlign="top" width="183" style="border:none;">
							Underkategori
						</td>
					</tr>
					<tr height="50">

						<!--Building listbox for "Stoffgruppe"-->
						<td class="news11" vAlign="top" width="110" height="50" style="border:none;">
							<select name="MainGroupListBox" size="5" multiple class="news11" style="width:115px" onChange="mgSelected()">
							<option value="-1" selected>Alle</option>
					        <%	dim mgCounter, mgName, kuMgName, prmname
								set mgCounter = getMaingroups()

							for each mgName in mgCounter

								if ((mgName = "Innenriks") or (mgName = "Utenriks") or (mgName = "Sport")) then
									response.write("<option value=" & mgCounter(mgName) & ">" & mgName & "</option>")

								elseif (mgName = "Kultur og underholdning") then
										'don't write Kultur & underholdning immediately, it should appear last in the list box
										kuMgName = mgName

								elseif (mgName = "PRM-NTB") then
										'don't write PRM immediately, it should appear last in the list box
										prmname = mgName
								end if

							next

							if (kuMgName <> "") then
								'write Kultur & underholdning as the last option in the list box
								response.write("<option value=" & mgCounter(kuMgName) & ">Kultur og underh.</option>")
							end if

							if (prmname <> "") then
								'write Kultur & underholdning as the last option in the list box
								response.write("<option value=" & mgCounter(prmname) & ">Pressemeldinger</option>")
							end if

							%>
							</select>
						</td>

						<!--Building listbox for "Undergruppe"-->
						<td class="news11" vAlign="top" width="130" height="50" style="border:none;">
							<select name="SubGroupListBox" size="5" multiple class="news11" style="width:115px">
							<option value="-1" selected>Alle</option>
							<%	dim sgCounter, sgName
								set sgCounter = getSubgroups()

							'Undergruppe-values should be merged into the following choices:
							'	NYHETER (Nyheter, Feature, Nekrologer, Profiler og intervjuer, Rettelser, Notiser)
							'	BAKGRUNN (Analyse, Bakgrunn, Faktaboks)
							'	TABELLER/RESULTATER (Tabeller og resultater)

							'Assign values to each option value:
							dim valNyh, valBak, valTab

							for each sgName in sgCounter
		 						if ((sgName = "Nyheter") or (sgName = "Feature") or (sgName = "Nekrologer") or (sgName = "Profiler og intervjuer") or (sgName = "Rettelser") or (sgName = "Notiser")) then
									valNyh = valNyh & "+" & sgCounter(sgName)
								end if
								if ((sgName = "Analyse") or (sgName = "Bakgrunn") or (sgName = "Faktaboks") ) then
									valBak = valBak & "+" & sgCounter(sgName)
								end if
								if (sgName = "Tabeller og resultater") then
									valTab = valTab & "+" & sgCounter(sgName)
								end if
							next %>

							<option value=<%=valNyh%>>Nyheter</option>
							<option value=<%=valBak%>>Bakgrunn</option>
							<option value=<%=valTab%>>Tabeller/resultater</option>
							</select>
						</td>

						<!--Building listbox for "Kategori"-->
						<td class="news11" vAlign="top" width="170" height="50" style="border:none;">
							<select name="MainCategoryListBox" onChange="fillsubcat(this.form)" size="5" multiple class="news11" style="width: 160px">
							<option value="-1" selected>Alle</option>
								<%
						' Changes are made to categories and subcategories functions.
						' Now functions return collections of 2-element arrays,
						' where element 0 is descriptive name and element 1 is value (bit position)
						' For subcategories collection there is also third element in the array
						' with [category] & "_" & [subcategory] content.
						' Similar collections for other lists are available from
						' the cache dictionary named "sorted", it contains collections.
						' Use function GetCategories() as template if sorted list is needed.
						' Ralfs 20020413
							dim mcCounter, mcName
							set mcCounter = getSortedCategories()
							for each mcName in mcCounter
							response.write("<option value=" & mcName(1) & ">" & mcName(0) & "</option>")
							next %>

							</select>
						</td>

						<!--Building listbox for "Underkategori"-->
						<td class="news11" vAlign="top" width="180" height="50" style="border:none;">
							<select name="SubCategoryListBox" size="5" multiple class="news11" style="width: 180px">
					        <%
					        response.write("Subcategories: <script language=""javascript""> var subcat = new Array(" )

							'*********************
					        'Functionality to create javascript code containing arrays with all subcategories
					        'This javascript code will be used client side to update the subcategory-field dependent on chosen maincategory
					        '*********************
					        dim tempC, tempSC, scCounter, scName
					        tempC = 0
					        tempSC = 0

					        'for each maincategory, get the id to be used to retrieve corresponding subcategory
					        for each mcName in mcCounter
					        	set scCounter = getSubcategories(mcName(1))

								if tempSC = 0 then
									response.write("new Array(")
								else
									response.write("),new Array(")
								end if

								'for each subcategory, populate the javascript-array with name and id
					        	if scCounter.count > 0 then 'only popluate if there are any subcategories
									for each scName in scCounter
										if tempC = 0 then
											response.write("'" & mcName(1) & "_-1','Alle " &  mcName(0) & "'")
											response.write(",'" & scName(2) & "','" &  scName(0) & "'")
										else
											response.write(",'" & scName(2) & "','" &  scName(0) & "'")
										end if
										tempC = 1
									next
								else
								response.write("'" & mcName(1) & "_-1','Alle " &  mcName(0) & "'")
								end if
								tempSC = 1
								tempC = 0
							next

							response.write(")); </script>")

							'this response.write will make the empty listbox have a width
							response.write("<option>Velg en kategori</option>")


							 %>
							</select>

							<script language="javascript">
							function fillsubcat(form)
							{
							 form.SubCategoryListBox.selectedIndex=-1;
							 form.SubCategoryListBox.options.length=0;
							 locs = form.SubCategoryListBox.options;
							 sel = new Array;
							 for (c=1;c<form.MainCategoryListBox.options.length;c++) {
							  if (form.MainCategoryListBox.options[c].selected) {
							  //locs[locs.length]=new Option('Hele '+form.MainCategoryListBox.options[c].text,form.MainCategoryListBox.options[c].value);
							   sel[sel.length]=locs.length;
							   for (lc=0;lc<subcat[c-1].length;lc+=2) {
							    locs[locs.length] = new Option(subcat[c-1][lc+1],subcat[c-1][lc]);
							   }
							  }
							 }
							 for (c=0;c<sel.length;c++) {
							  locs[sel[c]].selected = true;
							 }
							}

							function myonload(){
								fillsubcat(document.forms.searchform);
								onClick_disableDate();
							}

							window.onload = myonload;

							</script>

						</td>
					</tr>
					<tr><td colspan="4" style="border:none;" class="hjelpetekst">&nbsp;&nbsp;Hold CTRL nede for � velge flere av gangen (Apple-knappen for Mac-brukere)</td></tr>
				</table>
			</td></tr>

			<tr><td height="7">
			</td></tr>

			<tr><td>
				<table border="0" valign="top" align="left">
					<tr>
						<td valign="top" align="left">
							<table borderColor="#61c7d7" width="220" height="178" cellSpacing="0" cellPadding="1" align="left" valign="top" border="1">
								<tr>
									<td class="news11" vAlign="top" width="220" colspan="2" style="border:none;">
										Geografi - Verden
									</td>

								</tr>
								<tr>
									<%	'Logic for displaying world regions in two columns
										Dim wrCounter, wrName, wrLength, wrColLength, wcolCounter
										'Set wrCounter = getWorldRegions()
										Set wrCounter = getSortedWorldRegions()
										wrLength = wrCounter.count
										wrColLength = wrLength\2
									%>
									<td class="news11" vAlign="top" width="120" style="border:none;">
									<table width="95"><tr><td>
										<input type="checkbox" onclick="onClick_unCheckAll('WorldRegionCheckBox')" id="cb_&quot;<%=WorldRegionAll%>" name="WorldRegionAllCheckBox" checked value="-1"></td>
										<td class="news11">Alle</td>
								<%
									wcolCounter = 0
									for each wrName in wrCounter
									if wcolCounter = wrColLength  then
										response.write("</table></TD><TD class='news11' vAlign=top width=120 style='border:none;'><table>")''
									end if
									Response.Write "<tr><td style='width:2px'>"
									response.write("<INPUT type=""checkbox"" onclick=""onClick_unCheckAll('WorldRegionAllCheckBox')"" id=cb_" &  wrName(1) & " name=WorldRegionCheckBox value=" & wrName(1)& " >")
									Response.Write "</td><td class='news11'>" & wrName(0) & "</td>"
									Response.Write "</tr>"

									wcolCounter = wcolCounter + 1
									next %>
									</table>
								</td
								</tr>
							</table>
						</td>
						<td width="1"></td>
						<td>
							<table borderColor="#61c7d7" width="368" height="178" cellSpacing="0" cellPadding="1" align="left" valign="top" border="1">
								<tr>

									<td class="news11" vAlign="top" width="368" colspan="3" style="border:none;">
										Geografi - Norge
									</td>
								</tr>
								<tr>

									<%	'Logic for displaying local regions in three columns
										dim lrCounter, lrName, lrLength, lrColLength, colCounter
											set lrCounter = getSortedLocalRegions()
										lrLength = lrCounter.count
										lrColLength = lrLength\3
									%>
									<td class="news11" vAlign="top" width="110" style="border:none;">
										<input type="checkbox" onclick="onClick_unCheckAll('LocalRegionCheckBox')" id="cb_&quot;<%=LocalRegionAll%>" name="LocalRegionAllCheckBox" checked>Alle<br>

										<%
										colCounter = 0
										for each lrName in lrCounter
										if (colCounter = lrColLength) or (colCounter = (lrColLength * 2))  then
											response.write("</TD><TD class=news11 vAlign=top width=110 style='border:none;'>")
										end if
										response.write("<INPUT type=""checkbox"" onclick=""onClick_unCheckAll('LocalRegionAllCheckBox')"" id=cb_" &  lrName(1) & " name=LocalRegionCheckBox value=" & lrName(1) & " >" & lrName(0) & "<br>")
										colCounter = colCounter + 1
										next %>

									</td>
								</tr>
							</table>
						</td></tr>
					</table>
			</td></tr>

			<tr><td height="7">
			</td></tr>

			<tr><td>
				<table borderColor="#61c7d7" width="600" cellSpacing="0" cellPadding="1" align="left" border="1">
					<tr>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							Tid
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							Fra ( Format: dd.mm.���� )
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							Til ( Format: dd.mm.���� )
						</td>
					</tr>
					<tr>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							<select name="TimePeriodListBox" size="1" class="news11" onChange="onClick_disableDate()">
					        <%For i = 0 to 6
					          response.write("<option value=" & TimePeriod(i,1) & ">" & TimePeriod(i,0) & "</option>")
							Next %>
							</select>
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							<input type=text class="news11" name="f_date" value="" size=25 maxlength=10 onKeyPress="setSpecifyTime(this.form)" onBlur="autoFillDate(this)">
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							<input type=text class="news11" name="t_date" value="" size=25 maxlength=10 onKeyPress="setSpecifyTime(this.form)" onBlur="autoFillDate(this)">
						</td>
					</tr>
				</table>

			</td></tr>

			<tr><td height="7">
			</td></tr>

			<!--
		    <input type=hidden name=defaultSort value="date">
			<input type=hidden name=maxNumHits value="300">
			<input type=hidden name=threshold value="50">
			-->

			<tr><td>
				<table borderColor="#61c7d7" width="600" cellSpacing="0" cellPadding="1" align="left" border="1">
					<tr>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							Sortering av treff
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							Maks antall treff
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							Minste relevans <!--Minimum treffprosent-->
						</td>
					</tr>
					<tr>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							<select name="defaultSort" size="1" class="news11" style="width:150px;">
							<option value="date" SELECTED>Etter dato</option>
							<option value="relevance">Etter relevans</option>
							</select>
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							<select name="maxNumHits" size="1" class="news11" style="width:150px;">
							<option value="20">20</option>
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="200">200</option>
							<option value="300" SELECTED>300</option>
							<option value="500">500</option>
							<option value="1000">1000</option>
							</select>
						</td>
						<td class="news11" vAlign="top" width="200" style="border:none;">
							<select name="threshold" size="1" class="news11" style="width:150px;">
							<option value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option value="40">40</option>
							<option value="50" SELECTED>50</option>
							<option value="60">60</option>
							<option value="70">70</option>
							<option value="80">80</option>
							<option value="90">90</option>
							<option value="100">100</option>
							</select>
						</td>
					</tr>
				</table>

			</td></tr>

			<tr><td height="7">
			</td></tr>

			<tr><td>
				<table borderColor="#61c7d7" width="600" cellSpacing="0" cellPadding="1" align="left" border="0">
					<tr>
						<td class="news11" vAlign="top" width="600" align="right">
						<input class="formbutton" type="submit" value="Utf�r s�k" id="button1" name="button1" style="background:#bfccd9 none; color:#003366;">
						</td>
					</tr>
				</table>
   			</td>
   			</tr>
			<tr>
			  <td class="news11" vAlign="top" width="600">
			  <table class="news11" width="600" cellSpacing="0" cellPadding="0" align="left" border="0">
			  <tr>
		        <td height="5"></td>
		     </tr>
		     <tr>
		       <td>Det er tre hovedm�ter � s�ke p�:</td>
		     </tr>
			<tr>
				<td height="5"></td>
			</tr>
        	<tr>
		       <td>&nbsp;<img src="../images/rightarrow.gif" WIDTH="11" HEIGHT="9">&nbsp;Rent friteksts�k. Angi ett eller flere ord som du vil s�ke etter, eller skriv inn en hel setning for et mer konkret s�k.</td>
		     </tr>
		     <tr>
		       <td>&nbsp;<img src="../images/rightarrow.gif" WIDTH="11" HEIGHT="9">&nbsp;Rent kategoris�k. Velg en eller flere kategorier som du vil s�ke etter.</td>
		     </tr>
		     <tr>
		       <td>&nbsp;<img src="../images/rightarrow.gif" WIDTH="11" HEIGHT="9">&nbsp;Kombinasjon av friteksts�k og kategoris�k. Angi ett eller flere ord som du vil s�ke etter, eller skriv inn en hel setning for et mer konkret s�k. Velg en eller flere kategorier for � indikere i hvilken type nyheter du vil s�ke i. </td>
		     </tr>
		     <tr>
		       <td height="5"></td>
		     </tr>
		     <tr>
		       <td>�nsker du mer informasjon om hvordan best mulig utf�re et s�k, se v�r <a href="HelpSite.asp?anchor=search">hjelpeside</a>.</td>
		     </tr>
		     <tr>
				<td height="10"></td>
			</tr>
		   </table>
		  </td>
		  </tr>
	  	  </form>
	    </table>
  </table>
</body>
</html>

