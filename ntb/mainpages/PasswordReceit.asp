<%
'***********************************************************************************
'* Login.asp
'*
'* This file is used to give the administrator a receit after changing password
'* and to give an opportunity to choose where to go next
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.06.26
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.26
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->

<%
'decide which admin-page to go to
Dim page

if Session("Role") = "Super" then
	page = "UserList.asp"
elseif Session("Role") = "Admin" then
	page = "CompanyList.asp"
end if
%>

<HTML>
<HEAD>
<%=Titletag()%>
<LINK href="../include/ntb.css" type="text/css" rel="stylesheet">
</HEAD>
<BODY>
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td><%=RenderTitleBar("Skiftet passord", "NoPhoto", 602)%><br>
	</td>
</tr>
<tr>
	<td align="center">
	<table border="0" style="width:100" align="center">
	<tr><td align="left">
	<div class="main_heading2">
		Passordet er n� endret.	
	<br><br>
	G� videre til:
	<ul>
	<li><a href="<%=page%>">Brukeradministrasjon</a>
	<li><a href="User_homepage.asp">Den personlige siden</a>
	</ul>
	</td></tr>
	</table>
	</td>
</tr>
</table>
</BODY>
</HTML>