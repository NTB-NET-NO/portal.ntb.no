<%
'***********************************************************************************
'* Personalization_choose_window.asp
'*
'* This file is used for giving the user the opportunity to see which information
'* the personalized page consists of and to click on a spesified part of the site to
'* customize it.
'* 
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar.
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.16
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.27
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_personal_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/addr_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<%
Sub Main()

End Sub
%>

<%

Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

%>

<html>
<head>
	<link href="<%=style%>" type=text/css rel=stylesheet>
	<%
	Response.Write TitleTag() 
	
	'Trim the newsarray so it contains only newsinfo for the wanted windownumber
	Dim intWindow
	intWindow = Request("perradio")
	Call TrimNewsArray(intWindow)
	
	'Get the organizations name
	Dim idOrg, sOrg
	idOrg = GetCompany(Session("UserName"))
	sOrg = GetCompanyName(idOrg)
	%>
</head>
   
<BODY topmargin="0" leftmargin="0" >
    <TABLE width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
		<TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
	</TR>
	<TR VALIGN="TOP">
		<TD ALIGN="LEFT"  bgcolor="#003084">
			<!-- #INCLUDE FILE="../template/leftmenu.asp" -->
		</TD>
		<TD ALIGN="LEFT">&#160; </TD>
		<TD ALIGN="LEFT" width="100%">
			<%=RenderTitleBar("Personalisering av din egen side", "NoPhotolink", 620)%>
			<table border="0" width=600>
			<tr><td colspan=2 align=right class="news11" height="1,5">
					Tilbake til <a href=DefaultOrgPage.asp?default=back><%=sOrg%></a> sin side
				</td>
			</tr>
			<tr><td colspan=2 align=right class="news11" height="1,5">
					<%'set link to save this setting to the organization the user belongs to
					if Session("Role") = "Admin" OR Session("Role") = "Super" then
						Response.Write "Lagre som <a href=DefaultOrgPage.asp?default=new>" & sOrg & "</a> sin side"
					end if
					%>
				</td>
			</tr><tr>
				<td class=news11>
					P� denne siden kan du velge hvilken rute i vinduet ditt 
					du vil spesifisere et bestemt nyhetsutvalg for. �nsker du mer informasjon 
					om hvordan f� mest mulig ut av din personaliserte side, se v�r 
					<a href="HelpSite.asp?anchor=personalization">hjelpeside</A>. 			
				</td>
				<td align="right">
					<form action="Personalization_choose_style.asp" method="post" id="changeStyle" name="changeStyle">
					<br>
					<INPUT class="formbutton" type="submit" value="Endre vindusoppsett" id=changeStyle name=changeStyle style="background:#bfccd9 none; color:#003366; width:150">
					</form>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<br>
					<%=RenderPreferredWindow()%>
					<form action="User_homepage.asp" method="post" id="personalSite" name="personalSite">
					<INPUT class="formbutton" type="submit" value="G� til din personaliserte side" id=personalSite name=personalSite style="background:#bfccd9 none; color:#003366; width:170">
				</td>
			</form>
			</tr>        
        </table></td></tr>
    </table>
	
    
</body>
</html>
