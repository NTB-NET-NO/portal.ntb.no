<%@ Language=VBScript %>
<%
Option Explicit

	Dim objXsltDoc 
	Dim objXmlDoc

Sub LoadPhoneList
	Set objXsltDoc = Server.CreateObject("Msxml2.FreeThreadedDOMDocument")
	Set objXmlDoc = Server.CreateObject("Msxml2.DOMDocument")
	
	objXsltDoc.async = False
	objXsltDoc.Load Server.MapPath("../mainpages/phonelist.xsl")
	
	objXmlDoc.async = False
	objXmlDoc.Load Server.MapPath("../../ntb_files/contactlists/Phonelist.xml")

	set Session("xsltPhoneList") = objXsltDoc
	set Session("xmlPhoneList") = objXmlDoc

End Sub


Sub ShowPhoneList(strColumn, strOrder)
	dim xmlNode

	'If (typename(session("xmlPhoneList")) = "DOMDocument") AND (typename(session("xsltPhoneList")) = "DOMDocument") then
	'	set objXsltDoc = Session("xsltPhoneList")
	'	set objXmlDoc = Session("xmlPhoneList")
	'Else
		LoadPhoneList
	'End If
	
    Set xmlNode = objXsltDoc.selectSingleNode("//xsl:sort/@select")
    xmlNode.Text = strColumn
    Set xmlNode = objXsltDoc.selectSingleNode("//xsl:sort/@order")
    xmlNode.Text = strOrder
    'Set xmlNode = objXsltDoc.selectSingleNode("//xsl:apply-templates/@select")
    'xmlNode.Text = "person[avdeling != 'NTB Pluss']"
    'xmlNode.Text = "person[avdeling != 'NTB Pluss' and avdeling != 'NTB Pluss']"

	Response.Write objXmlDoc.transformNode(objXsltDoc)
End Sub

%>
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>
<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="2">
<%

' Recieving sort criterias from contactList.asp
dim strOrder
strOrder = Request("orderBy")
dim strColumn
strColumn = Request("sortBy")
'displaying the phonelist matching the criteria set in strColumn and strOrder
ShowPhoneList strColumn, strOrder
%>
</TABLE>