<%
'***********************************************************************************
'* EditOrProfile.asp
'*
'* This file is used to give the administrator the opportunity to edit the profiles 
'* to the various customer
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.24
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.19
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->


<%
Sub Main()

	'check whether the user has the rights to view this page
	if Session("Role") <> "Admin" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub

%>

<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">

	<%=TitleTag()%>
	
	<script language="Javascript">
	function SetDefault() {
	
		document.frmChangeProfile.txtName.value = document.frmChangeProfile.txtName.defaultValue
		document.frmChangeProfile.txtPassword.value = document.frmChangeProfile.txtPassword.defaultValue
		document.frmChangeProfile.txtPasswordExpire.value = document.frmChangeProfile.txtPasswordExpire.defaultValue
		document.frmChangeProfile.txtContact.value = document.frmChangeProfile.txtContact.defaultValue
		document.frmChangeProfile.chkAccessRight(0).value = document.frmChangeProfile.chkAccessRight(0).defaultValue
		document.frmChangeProfile.chkAccessRight(1).value = document.frmChangeProfile.chkAccessRight(1).defaultValue
		document.frmChangeProfile.chkAccessRight(2).value = document.frmChangeProfile.chkAccessRight(2).defaultValue
		document.frmChangeProfile.chkAccessRight(3).value = document.frmChangeProfile.chkAccessRight(3).defaultValue
		document.frmChangeProfile.chkAccessRight(4).value = document.frmChangeProfile.chkAccessRight(4).defaultValue
		document.frmChangeProfile.chkAccessRight(5).value = document.frmChangeProfile.chkAccessRight(5).defaultValue
		document.frmChangeProfile.chkAccessRight(6).value = document.frmChangeProfile.chkAccessRight(6).defaultValue
		document.frmChangeProfile.chkAccessRight(7).value = document.frmChangeProfile.chkAccessRight(7).defaultValue
		document.frmChangeProfile.chkAccessRight(8).value = document.frmChangeProfile.chkAccessRight(8).defaultValue
		document.frmChangeProfile.chkAccessRight(9).value = document.frmChangeProfile.chkAccessRight(9).defaultValue
		document.frmChangeProfile.chkAccessRight(10).value = document.frmChangeProfile.chkAccessRight(10).defaultValue
		document.frmChangeProfile.chkAccessRight(11).value = document.frmChangeProfile.chkAccessRight(11).defaultValue
		
}
	
	function CheckInput(){
		var tmpString = document.frmChangeProfile.txtPassword.value
	
		if ((document.frmChangeProfile.txtName.value == "") || (document.frmChangeProfile.txtPassword.value == "")){
			alert ("M� skrive inn kundenavn og passord for � opprette/endre kunde.");
		}
		else if (tmpString.length < 8)
			alert ("Passordet m� v�re p� minst 8 tegn.");
		
		else {
			document.frmChangeProfile.submit();
		}
	}
	
	function GetToList(){
		//history.back (1)
		location.href = "CompanyList.asp";
	}
	</script>
</head>

<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7">

<% 
'get the userid and orgid
Dim coid
coid = Request.Form("hcoid")

Dim strTitle, renderFunc

'if no userid has been sent and a new user are being made
if coid = "" then
	strTitle = "Ny kunde "
	renderFunc = RenderNewOrgProfile()
else
'if there are a companyid and the profile should be modified
	strTitle = "Oppdater profil for " & GetCompanyName(coid)
	renderFunc = RenderEditOrgProfile(coid)
end if

Response.Write(RenderTitleBar(strTitle, "NoPhotoLink", 620))%>

<TABLE border="0" class="news">
	<form name="frmChangeProfile" action="CompanyList.asp" method="post">
	<%Response.Write renderFunc	%>
	</form>
</table>  
</body>
</html>

