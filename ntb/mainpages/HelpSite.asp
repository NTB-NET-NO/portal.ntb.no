<%
'***********************************************************************************
'* HelpSite.asp
'*
'* This file builds up the help-file
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.24
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.06
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->

<script language="JavaScript">

//for some reason IE desided to open this .asp bottom left.
//function that scroll to the top left of the page
function setScroll()
{
	var y
	y=document.body.scrollTop;
	//alert(y);
	setTimeout('window.scrollTo(0,0)', 40);
}

</script>

<%
Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>

<HTML>
<HEAD>
<link href="<%=style%>" type="text/css" rel="stylesheet">
	<%= TitleTag() %>
</HEAD>
<%
'getting the anchor from the url, to be passed into the iframe.
dim anchor, source
anchor = Request("anchor")
'for some reason IE desided to open this page scrolled bottom and left.
'this if statement helps scroll to the top left of the page when no anchor in URL
if anchor <> "" then
	source = "../mainpages/help.asp#" &anchor
else
source = "../mainpages/help.asp"
end if
%>   
<BODY topmargin="0" leftmargin="0" onload="setScroll()">
  <TABLE border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
      <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
      <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160;</TD>
      <TD ALIGN="LEFT" WIDTH=100%>
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" width="620">
	    <TR>
          <TD colspan="3"><%= RenderTitleBar("Hjelpeside", "NoPhotolink", 620) %></TD>
        </TR>
        <TR>
          <TD>
          <IFRAME NAME="helpFrame" WIDTH="620" HEIGHT="500" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="<%=source%>"></IFRAME>
          </TD>
		</TR>
      </TABLE>
      </TD>
    </TR> 
  </TABLE>
</BODY>
</HTML>