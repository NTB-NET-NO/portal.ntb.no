<%@ Language=VBScript %>
<%
'***********************************************************************************
' chat.asp
'
' This file is used for chat.
' 
' USES:
' MSMQ ActiveX COM objects, Application("QueueServer") for
' queue paths, Session("chatcontent") string variable to store
' previous messages for this session.
' Includes: chatWindow.asp which is the logic in the iframe
' Includes: warning.htm and warning_max.htm for giving messages to the user.
' NOTES:
' 'none'
'
' CREATED BY: Ralfs Pladers, Andersen 
' CREATED DATE: 2002.04.17
' UPDATED BY: Vegar Paulsen, Andersen
' UPDATED DATE: 2002.04.21
' REVISION HISTORY: 
' UPDATED BY: Roar Vestre, NTB
' Added Error handling for JavaScript errors, line 89
' UPDATED DATE: 2002.07.10
'
'***********************************************************************************

option explicit

%>
<HTML>
<HEAD>
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>
<TITLE>Dialogvindu</TITLE>
<SCRIPT LANGUAGE="JavaScript">
<!--

// button "Avslutt" has ben used to close the chat window.
// submitting the page to itself.
function finishOffButton()
{
	channel = document.forms.isSubmittedForm.channel.value;
	location.href = "../mainpages/chat.asp?method=button&clean=yes&channel="+channel
}

function finishOff()
{
  // the onunload, calling this event, also runs when a form is submitted
  // using this if statement to check if it's the send button that triggers the onunload event,
  // or if the event is triggered by the close window button.
  if (document.forms.isSubmittedForm.isSubmitted.value=='false')
  {      
    channel = document.forms.isSubmittedForm.channel.value;
    newWindow = window.open("../mainpages/chat.asp?clean=yes&channel="+channel +"", "")
    newWindow.close();
    //window.alert("Din chat er n� avsluttet")
  }
}

-->
</SCRIPT>
</HEAD>
<%

Dim reqChannel, objChatters, strActiveChannel
Dim strQueueName, strQueuePath, strReturnQueuePath
Dim strThisUser, strActiveChat

Const MAX_CHAT_SESSIONS = 3
Const OWN_MSG_START = "<div class=""chatOne"">"
Const OWN_MSG_END = "</div>"

strThisUser = Request.ServerVariables("LOGON_USER")
Set objChatters = Application("ChatSessions")

' find our chat session
If IsNull(objChatters(strThisUser)) OR objChatters(strThisUser) = "" then
	strActiveChannel = ""
	else
	strActiveChannel = CStr(objChatters(strThisUser))
end if
	
reqChannel = Request("channel")
if reqChannel = "" then
	Response.Write("Chat channel parameter expected. Cannot proceed.")
	Response.End
end if

if Request("clean") <> "" OR strActiveChannel <> reqChannel then
%><BODY onunload=finishOff()><%
else
%><BODY onunload=finishOff() onload="javascript:document.forms.chatForm.elements.msg.focus();"><%
end if

%>	 
<FORM METHOD="POST" id=isSubmittedForm name=isSubmittedForm>
<INPUT type="hidden" name="isSubmitted" value=false>
<INPUT type="hidden" name="channel" value="<%=reqChannel%>">
</FORM>

<%
' we have no active chat session
' check if we may open a new chat session
if strActiveChannel = "" then
	Dim i, c
	c = 0
	For Each i in objChatters
	if objChatters(i) = reqChannel then c = c + 1
	Next
	if c >= MAX_CHAT_SESSIONS then
		'Server.transfer("warning_max.htm")
		Response.Redirect ("../mainpages/warning_max.asp?channel=" & reqChannel)
		Response.End
	else
	' add our user to registered chatters later, when channel
	' name will be checked
	end if
	
' we have registered chat session, let us check it	
else
	if strActiveChannel <> reqChannel then	
	' user attempted to have more than one chat window
	' don't open new session, show warning
	'Response.Redirect ("../mainpages/warning_max.asp)
	'Response.End
	'Server.transfer("warning_max.asp")
	Server.transfer("warning.htm")
	
	end if
End if

' if we are here this means that everything is good so far
' reqested channel must be valid channel name to open	
select case reqChannel
	case "report"
	strQueueName = "ntb_reportasjeleder"
	strActiveChat = "Reportasjeleder"
	case "sport"
	'strQueueName = "test1"
	'strActiveChat = "test1_retur"
	strQueueName = "ntb_sportsjef"
	strActiveChat = "vaktsjef p� sporten"
	case "uriks"
	strQueueName = "ntb_utenriksvakt"
	strActiveChat = "Utenriksvakt"
	case "vakt"
	strQueueName = "ntb_vaktsjef"
	strActiveChat = "Vaktsjef"
	case else
	Response.Write("Pr�v � �pne vinduet ved hjelp av navigatorene")
	Session.Abandon
	Response.End
end select

objChatters(strThisUser) = reqChannel
strQueuePath = Application("QueueServer") & "\" & strQueueName
strReturnQueuePath = strQueuePath & "_retur"

' closing the conection
Dim strCleanMsg
Dim oQueueInfo, oQueue, oMessage
if Request("clean") <> "" then

	'The button "Avslutt" has ben used to close the chat.
	if Request("method") <> "" then
		Response.Write("<div class=""news11"">Takk for at du koblet fra, slik at andre kan slippe til.</div><BR>")
		objChatters(strThisUser) = ""
		Response.write("<div class=""news11"">Innholdet i din siste samtale:<hr>" & Session("chatcontent") &"</div><hr>")
		strCleanMsg = "Koblingen til " & strThisUser & " er lukket." & vbCrLf & Now()
	'The "close window" or "EXIT command" has ben used to close the chat 
	else
		Response.Write("<div class=""news11"">Din kobling til chatten er lukket av NTB. </div>")
		Response.Write("<div class=""news11"">Pr�v � huske � lukke chat-vinduet n�r en samtale er over!</div><br>")
		objChatters(strThisUser) = ""
		Response.write("<div class=""news11"">Innholdet i din siste samtale:<hr>" & Session("chatcontent") &"</div><hr>")
		strCleanMsg = "Koblingen til " & strThisUser & " er lukket."  & vbCrLf & Now()
	end if
	'to prevent writing user closed message in there has not been any chat
	if Session("chatcontent") <> "" then
		Set oQueueInfo = Server.CreateObject("MSMQ.MSMQQueueInfo")
		Set oMessage = Server.CreateObject("MSMQ.MSMQMessage")
		oQueueInfo.PathName = strQueuePath
		Set oQueue = oQueueInfo.Open(2,0)
		oMessage.Label = strThisUser
		oMessage.Body = CStr(strCleanMsg)
		oMessage.Send oQueue
		Set oMessage = Nothing
		oQueue.Close
		Set oQueue = Nothing
		Set oQueueInfo = Nothing
	end if

	Session("chatcontent") = ""
	'Response.Flush
	'Response.End

else

	Dim strMsg
	strMsg = Request("msg")
	if strMsg <> "" then
		Set oQueueInfo = Server.CreateObject("MSMQ.MSMQQueueInfo")
		Set oMessage = Server.CreateObject("MSMQ.MSMQMessage")
		oQueueInfo.PathName = strQueuePath
		Set oQueue = oQueueInfo.Open(2,0)
		oMessage.Label = strThisUser
		oMessage.Body = CStr(strMsg)
		oMessage.Send oQueue
		Set oMessage = Nothing
		oQueue.Close
		Set oQueue = Nothing
		Set oQueueInfo = Nothing
		Session("chatcontent") = Session("chatcontent") & _
			OWN_MSG_START & strMsg & OWN_MSG_END
	end if

%>
<IFRAME NAME="chatframe" WIDTH="100%" HEIGHT="300" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="yes" SRC="chatWindow.asp?channel=<%=reqChannel%>"></IFRAME>
<HR size="1">
<!--submitting value to be used in Javascript to determin-->
<FORM onSubmit="Javascript:document.forms.isSubmittedForm.isSubmitted.value='true';" METHOD="POST" ACTION="chat.asp?channel=<%=reqChannel%>" id=chatForm name=chatForm>
<TABLE>
<TR>
  <TD ROWSPAN="2"><TEXTAREA name="msg" class="news11" style="width:310px; height:50px"></TEXTAREA></TD>
  <TD><INPUT class="formbutton" type="submit" name="submit" value="Send" style="background:#bfccd9 none; width:57px; color:#003366;"></TD>
</TR>
<TR>
  <TD><INPUT TYPE="BUTTON" VALUE="Avslutt" onclick="finishOffButton()" style="background:#bfccd9 none; width:57px; color:#003366;" class="formbutton" id=BUTTON1 name=BUTTON1></TD>
</FORM>
<!--div class="news11">Vennligst <A href="chat.asp?clean=yes&channel=<%=reqChannel%>"><b>klikk her</b></A> for � slippe til andre n�r din samtale er over.</div-->
<%
end if
%>
</BODY>
</HTML>
