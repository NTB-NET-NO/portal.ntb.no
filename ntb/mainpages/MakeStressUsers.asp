<%
'***********************************************************************************
'* MakeStressUsers.asp
'*
'* This file is used to make 2000 new users
'*
'* USES:
'*		NTB_profile_lib.asp - For profilefunctions that makes the users
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen
'* CREATED DATE: 2002.07.05
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.07.05
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->

<!-- #INCLUDE FILE="../services/include/ad_routines.asp" -->
<!-- #INCLUDE FILE="../services/include/const.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Sub Main

End Sub

Dim i

for i=2501 to 3000

	Dim goodie
	goodie = Cstr(CreateUserProfile(i))
	Response.write goodie

next

Function CreateUserProfile(ByVal i)
    Dim ProfileService, ProfileObj
  	Dim sOrg, username, password, firstname, lastname, email, phone, role
  	Dim sGroup

	'set the organizations name
	sOrg = "StressCompany"

	'set the needed parameters
	username = "kjempestressa" & i
	firstname = i
	LastName = "stress"
	role = "1"

	Set ProfileService = GetProfileServiceObject()

	'verify if user already exists
	Set ProfileObj = ProfileService.GetProfile(username, "UserObject", false)

	if not ProfileObj is nothing then
		'user already exist
		CreateUserProfile = False
		Exit Function
	end if

	On Error Resume Next
    Set ProfileObj = ProfileService.CreateProfile(username, "UserObject")

    If Err.number <> 0 then
        Dim nErrNumber, sErrSource, sErrDesc

        nErrNumber  = Err.Number
        sErrSource  = Err.Source
        sErrDesc    = Err.Description

        'If profile already exist an error code of 'C100400B' is returned.
        'In this case we should not delete the profile
        If Hex(Err.number) <> "C100400B" Then
            'If error occured and we have multiple sources in the profile
            'we may need to clean up.
            'The error returned from DeleteProfile should be ignored
            Call ProfileService.DeleteProfile(username, "UserObject")
        End If

        CreateUserProfile = False

        Err.Raise nErrNumber, sErrSource, sErrDesc
        'Exit Function
    End If

    'set the id of the updater
    Dim UserObj, userid

    UserObj = GetUserProfileByLoginName(Session("UserName"))
    userid = UserObj.GeneralInfo.user_id

	'set the right AD-group -> _USERGROUP
    sGroup = sOrg & "_USERGROUP"

    'get an unique id
    ProfileObj.GeneralInfo.user_id = MSCSGenID.GenGUIDString

    'Set profile properties
    ProfileObj.GeneralInfo.user_type = 1 'REGISTERED_PROFILE
    ProfileObj.AccountInfo.date_registered = Date()
    ProfileObj.AccountInfo.account_status = 1 'ACCOUNT_ACTIVE
    ProfileObj.ProfileSystem.date_last_changed = Date()

    'set the password (same for all in the org)
    Dim objOrgProfile, sPassword
    objOrgProfile = GetCompanyProfileByName(sOrg)
    sPassword = objOrgProfile.GeneralInfo.org_password

    ProfileObj.GeneralInfo.user_security_password = sPassword

    'set the id of the updater
    ProfileObj.GeneralInfo.user_id_changed_by = userid

	Dim orgid
	orgid = objOrgProfile.GeneralInfo.org_id

	'set profile information gathered from the site
	ProfileObj.AccountInfo.org_id = orgid
    ProfileObj.GeneralInfo.first_name = firstname
	ProfileObj.GeneralInfo.last_name = lastname
	ProfileObj.BusinessDesk.partner_desk_role = Cint(role)
	ProfileObj.AccountInfo.password_expire_date = Cstr(Date())
	'and more.....

	'set properties into the active directory
	ProfileObj.ProfileSystem.sam_account_name = username
	ProfileObj.ProfileSystem.ParentDN = "OU=" & sOrg & ",OU=" _
										& Application("MSCSCommerceSiteName2") & "," & "OU=MSCS_40_Root"
	ProfileObj.ProfileSystem.user_account_control = 512 'AD_USER_ACTIVE

    If Err.number <> 0 then
    'problems with making the user
       CreateUserProfile = false
    Else
    'update the profile and set the right AD-group

		Dim sUserDN, sUserDN2

		sUserDN2= "OU=" & sOrg & "," _
		& "OU=" & Application("MSCSCommerceSiteName2")  & "," _
		& "OU=MSCS_40_Root" & "," & MSCSActiveDirectoryDomain

		sUserDN = "CN=" & username & "," & sUserDN2

		on error resume next

        Call ProfileObj.Update()

        If Err.number <> 0 Then ' c1003e84
			'problems with making the user
			CreateUserProfile = BugFixCommerce(sUserDN2, username, sGroup, sPassword)
		Else

			if SetAD(sUserDN, sGroup) then
				CreateUserProfile = True
			else
				CreateUserProfile = False
			end if
		end if
    End If

End Function

%>

