<%
'***********************************************************************************
' Searchresults.asp
'
' This file is used for displaying search results. Both freetext search results and
' category search results will be displayed. Results from the sound search will
' also be displayed on this page.
' The different search queries are formatted and sent to a search component. This
' component returns a RecordSet of search results. The first 20 hits are displayed
' on the page and the RecordSet is saved in a session variable to make it easy to
' display the next pages with hits.
'
' USES:
'		NTB_layout_lib.asp - For rendering of titlebar.
'		NTB_search_lib.asp - For performing search and return a recordset of search results
'		overlib.js - For mouse over functionality for the search result (Javascript)
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen
' CREATED DATE: 2002.04.09
' UPDATED BY: Solveig Skjermo, Andersen
' UPDATED DATE: 2002.06.25
' REVISION HISTORY:
' UPDATED 2002.09.19, by: LCH, NTB
' UPDATED 2005.02.24, by: Roar Vestre: Added page name to "myURL", and added session variable "searchWord"						
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_search_lib.asp" -->

<%
Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>

<html>
<head>
<link href="<%=style%>" type="text/css" rel="stylesheet">

<%= TitleTag() %>

<script language="JavaScript">
//function to open news items in a seperate window
var myWindow;

function w(aid) {
	var myURL;
	myURL = '../template/x.asp?a=' + aid + '&page=searchresult';
	if(myWindow && !myWindow.closed) { // window is open
		myWindow.location = myURL;
		myWindow.focus();
	} else {
		leftPlacement = screen.width - 610; //ensures that the window pops up to the right independent on screen resolution
		myName = 'PopupPage';
		myOptions = 'height=600,width=600,scrollbars=yes,resizable=yes,top=80, left='+ leftPlacement;
		myWindow = window.open(myURL, myName, myOptions);
		myWindow.focus();
	}
}

//function to open sound html code in a separate window
var soundHtmlWindow;
function ws(aid, fname) {
	var myURL;
	myURL = '../template/sound_html.asp?id=' + aid + '\&fname=' + fname;
	if(soundHtmlWindow && !soundHtmlWindow.closed) { // window is open
		soundHtmlWindow.location = myURL;
		soundHtmlWindow.focus();
	} else {
		//leftPlacement = screen.width - 610; //ensures that the window pops up to the right independent on screen resolution
		myName = 'SoundPopupPage';
		myOptions = 'height=600,width=480,scrollbars=yes,resizable=yes,top=80, left=10';
		soundHtmlWindow = window.open(myURL, myName, myOptions);
		soundHtmlWindow.focus();
	}
}

//function to set some fields and submit page to itself
function submitPage(searchWord, fieldToSet, valueToSet){
	if (fieldToSet == "sortChoice") {
		document.SearchResults.sortChoice.value = valueToSet;
	}
	else if (fieldToSet == "pageNumber") {
		document.SearchResults.pageNumber.value = valueToSet;
	}
	document.SearchResults.searchWord.value = searchWord;
	document.SearchResults.submit();
}
</script>

</HEAD>

<BODY topmargin="0" leftmargin="0">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="../include/overlib.js"></script>

<TABLE WIDTH=100% HEIGHT="100%" border="0" CELLSPACING="0" CELLPADDING="0">
  <TR VALIGN="TOP">
    <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
  </TR>
  <TR VALIGN="TOP">
    <TD ALIGN="LEFT" bgcolor="#003084">
	<!-- #INCLUDE FILE="../template/leftmenu.asp" -->
    </TD>
    <TD ALIGN="LEFT">&#160;</TD>
    <TD WIDTH="100%" ALIGN="LEFT">
    <FORM name="SearchResults" Action="searchresults.asp" Method=post>

             <%= RenderTitleBar("S�keresultat", "noPhotoLink", 620) %>

            <TABLE VALIGN="TOP" ALIGN="LEFT" BORDER="0" CELLSPACING="0" CELLPADDING="0">

            <TR><TD>

			<input type=hidden name="sortChoice" value="<%=Request.Form("sortChoice")%>">
			<input type=hidden name="pageNumber">
			<input type=hidden name="searchWord" value="<%=Replace(Request.Form("searchWord"),"""","")%>">
   	 		<input type=hidden name="flagSoundSearch" value="<%=Request.Form("flagSoundSearch")%>">
   	 		<input type=hidden name="maxNumHits" value="<%=Request.Form("maxNumHits")%>">

   	<%
   	'******************************************************************************
   	'Retrieve recordset with search results, set it in session and display on page.
   	'******************************************************************************

   	Dim pageCounter, lastRecord 'variables used for right number of hits per page and displaying the right navigator links.
	Dim rsSearchResult ' As ADODB.Recordset
    Dim searchWord, pageSort, defaultSort
    Dim threshold, maxNumHits
    Dim strTempTitle, strTempIngress

	pageCounter = Request.Form("pageNumber")
	pageSort = Request.Form("sortChoice")

	threshold = Request.Form("threshold")

	maxNumHits = Request.Form("maxNumHits")
	if ( maxNumHits = "" ) then maxNumHits = 300

	defaultSort = Request.Form("defaultSort")
	if ( defaultSort = "relevance" ) then
		defaultSort = "Relevance DESC"
	else
		defaultSort = "CreationDateTime DESC"
	end if

	if (pageCounter = "") AND (pageSort = "") then
 		'the search result list is displayed for the first time, and the search results must be
 		'retrieved from the search component (not from session).
 		'----------------------------------------------------------------------------------------

 		'Retrieve the different search criteria from search page and format search query
		'-------------------------------------------------------------------------------

		'get searchword or searchsentence from either search page or topmenu
		if Request.Form("simpleFreeTextSearch") <> "" then
			searchWord = Request.Form("simpleFreeTextSearch")
		elseif Request.Form("freeTextEntry") <> "" then
			searchWord = Request.Form("freeTextEntry")
		elseif Request.Form("flagSoundSearch")  <> "" then
			if Request.Form("freeSoundTextEntry")  <> "" then
				searchWord = Replace(Request.Form("freeSoundTextEntry"), "�", "a")
				searchWord = Replace(searchWord, "�", "A")
				searchWord = Replace(searchWord, "�", "o")
				searchWord = Replace(searchWord, "�", "O")
				searchWord = Replace(searchWord, "�", "a")
				searchWord = Replace(searchWord, "�", "A")
			else
				searchWord = "%"
			end if

		'elseif Request.Form("freeSoundTextEntry")  <> "" then
		'	searchWord = Replace(Request.Form("freeSoundTextEntry"), "�", "a")
		'	searchWord = Replace(searchWord, "�", "A")
		'	searchWord = Replace(searchWord, "�", "o")
		'	searchWord = Replace(searchWord, "�", "O")
		'	searchWord = Replace(searchWord, "�", "a")
		'	searchWord = Replace(searchWord, "�", "A")
		else
			searchWord = ""
		end if

		'get timeperiod and date options
		Dim timePeriod, timeSpecification
		timePeriod = Request.Form("TimePeriodListBox")
		'******VEGAR: Det er mulig at du m� fjerne begrensning p� tid ved lyds�k
		'****** jo takk, men Edward bruker ikke tidsbegrensninger i lyds�k-komponenten
		if timePeriod = "" then
			'time period should be one week if search is requested from topmenu
			timePeriod = "7"
		end if
			'search the entire archive has the id TimePeriodListBox="9"
			'using the same serach as with specified date, only her with hardcoded date.
		if timePeriod = "9" then
			dim tmpToday, formatedToday
			tmpToday = date
			formatedToday = FormatDateSearch(tmpToday)
			timePeriod ="0"
			timeSpecification = "01;01;1970;" & formatedToday
		else
			timeSpecification = Request.Form("dates") 'Dates parsed by javascript, added with simplification of dateboxes
		end if

		'getWorldRegion
		Dim wrSum, iWorldAreaCount
		if (Request.Form("WorldRegionAllCheckBox")<> "") then
		'if all regions are selected (the all option is checked)
			wrSum = -1
		else
		'loop trough all the selected regions and add to string
			For iWorldAreaCount = 1 to Request.Form("WorldRegionCheckBox").Count
				wrSum = wrSum & "+" & Request.Form("WorldRegionCheckBox")(iWorldAreaCount)
			Next
		end if
		'if no area is selected, set it to -1 (equal to all selected)
		if wrSum = "" then
			wrSum = -1
		end if

		'getLocalRegion
		Dim lrSum, iLocalAreaCount
		if (Request.Form("LocalRegionAllCheckBox")<> "") then
		'if all local regions are selected (the all option is checked)
		  	lrSum = -1
		else
		'loop trough all the selected local regions and add to string
			For iLocalAreaCount = 1 to Request.Form("LocalRegionCheckBox").Count
				lrSum = lrSum & "+" & Request.Form("LocalRegionCheckBox")(iLocalAreaCount)
			Next
		end if
		'if no area is selected, set it to -1 (equal to all selected)
		if lrSum = "" then
			lrSum = -1
		end if

 		'Retrieve category search query
		Dim categorySearchQuery, searchQuery
		searchQuery = GetCategorySearchQuery

		'For Debug:
		'Response.Write ("Debug: " & searchWord & "|" & searchQuery & "|" &  timePeriod & "|" &  timeSpecification & "|" &  wrSum & "|" & lrSum)
		'Response.Flush

		'Create a connenction to searchcomponent, call the component and get reurned a recordset of hits
		'-----------------------------------------------------------------------------------------------
		Dim searchInterface, heading, list

		'Check if this is sound search
		if Request.Form("flagSoundSearch")  <> "" then
			DoSoundSearch rsSearchResult, searchWord
			rsSearchResult.PageSize = 15
		else

			'Sub DoSearchAutonomy(ByRef rsResultRecords, ByVal strQuery, ByVal strCategory, ByVal strTimePeriod, ByVal strDates, ByVal strGeography, ByVal strGeographyNorway, ByVal strMaxHits)
			DoSearchAutonomy rsSearchResult, searchWord, searchQuery, timePeriod, timeSpecification, wrSum, lrSum, maxNumHits, threshold
			'Old: DoSearch rsSearchResult, searchWord, searchQuery, timePeriod, timeSpecification, wrSum, lrSum, "300"

			'For Debug:
			'Response.Write ("Debug: Query: " & searchWord & "<br>Category: " & searchQuery & "<br>TimePeriod: " & timePeriod _
			'& "<br>Dates:" &  timeSpecification & "<br>Geography:" &  wrSum & "<br>GeographyNorway:" & lrSum & "<br>MaxHits:" & maxNumHits)
			'Response.write("<br>Count: " & rsSearchResult.RecordCount)
			'Response.Flush

			rsSearchResult.PageSize = 20
		end if

		rsSearchResult.Sort = defaultSort
		'rsSearchResult.Sort = "CreationDateTime DESC"

		'End of Create a connenction to searchcomponent, call the component and get reurned a recordset of hits

		'add the recordSet to the Session
		'-----------------------------------------------------------------------------
		Dim RecordSetADTG
		Dim SaveStream
		Set SaveStream = Server.CreateObject("ADODB.Stream")

		rsSearchResult.Save SaveStream
		RecordSetADTG = SaveStream.Read

		'tidy up the stream now we have finished with it
		SaveStream.Close
		Set SaveStream = nothing

		Session("SearchCache-ADTG") = RecordSetADTG
		'Response.Write("Bruker ny Sortering: " & pageSort & "<br>")

		' Inserted by Roar Vestre 2005.02.24:
		Session("searchWord") = searchWord

	else
		'a next or previous page of the search result list should be displayed, and the search
		'results must be retrieved from the session (to avoid re-running the search query).
		'-----------------------------------------------------------------------------------

		Dim LoadStream
		Set LoadStream = Server.CreateObject("ADODB.Stream")
		Set rsSearchResult = Server.CreateObject("ADODB.Recordset") 'Trond ut

		'recreate the stream from the ADTG in the cache
		LoadStream.Open
		LoadStream.Type = 1'adTypeBinary
		LoadStream.Write Session("SearchCache-ADTG")
		LoadStream.SetEOS

		'move to the start of the stream
		LoadStream.Position = 0

		'open the record set, based on the content of the load stream
		rsSearchResult.Open LoadStream

		if Request.Form("flagSoundSearch")  <> "" then
			rsSearchResult.PageSize = 15
		else
			rsSearchResult.PageSize = 20
		end if

		'sort the resultset, default is by date
		if pageSort = "sortByRelevance" then
			rsSearchResult.Sort = "Relevance DESC"
		elseif pageSort = "sortByDate" then
			rsSearchResult.Sort = "CreationDateTime DESC"
		end if

		'get searcWord from field
		searchWord = Request.Form("searchWord")

	end if 'end of use either search component or search session recordset


	'if there are any search hits, display them
	'--------------------------------------------------------------------------------
	if rsSearchResult.RecordCount > 0 then
	'only show result list if there are any hits


	'logic for checking if there exist a relevance
	dim boolRelevanceExist
	On Error Resume Next
	boolRelevanceExist = rsSearchResult.Fields("Relevance").Value > 0
	If Err.Number <> 0 Then
		boolRelevanceExist = false
	else
		boolRelevanceExist = true
	end if
	Err.Clear
	On Error GoTo 0

		Response.Write("<TABLE border=""0"" cellpadding=""0"" cellspacing=""0"" width=""620"">")
		'if a free text word is entered, sorting by relevance is possible: view sort links
		if (boolRelevanceExist) then
			'Write links to sort by either date or relevance (depending on which sort criteria is active)
			if pageSort = "sortByRelevance" or defaultSort = "Relevance DESC" then
				Response.Write("<tr class=""news"" align='right'><td>S�ket er sortert etter relevans. <A href=""javascript:submitPage('" & Replace(searchWord,"""","") & "', 'sortChoice', 'sortByDate')"">Sorter p� dato</a></td></tr>")
			else
				Response.Write("<tr class=""news"" align='right'><td>S�ket er sortert p� dato. <A href=""javascript:submitPage('" & Replace(searchWord,"""","") & "', 'sortChoice', 'sortByRelevance')"">Sorter etter relevans</a></td></tr>")
			end if
		end if


		'Write number of hits on the page (the wording depends on if a searchword is available)
		if searchWord = "" then
			'write a message if number of hits are more then numhits
			if rsSearchResult.RecordCount > maxNumHits-1 then
				Response.Write("<tr class=""main_heading2""><td>Ditt s�k returnerte mer enn " & maxNumHits & " treff. Kun de " & maxNumHits & " f�rste treffene vil vises.</td></tr>")
				Response.Write("<tr class=""plaintext""><td>Det kan v�re lurt � bruke filtrering i s�kesiden for � f� f�rre treff.</td></tr>")
				if Request.Form("flagSoundSearch")  <> "" then
					Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/soundlist.asp"">her</a> for � g� tilbake til s�kesiden.")
				elseif Request.Form("simpleFreeTextSearch") <> "" then
					Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/search.asp"">her</a> for � g� til s�kesiden.")
				else
					Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/search.asp"">her</a> for � g� tilbake til s�kesiden.")
				end if
				Response.Write(" Klikk <a href=""../mainpages/HelpSite.asp?anchor=search"">her</a> for s�kehjelp.</td></tr>")

			else
				Response.Write("<tr class=""main_heading2""><td>Ditt kategoris�k ga " & rsSearchResult.RecordCount & " treff.</td></tr>")
			end if
		elseif searchWord ="%" then
			Response.Write("<tr class=""main_heading2""><td>Ditt s�k etter alle lydsaker ga " & rsSearchResult.RecordCount & " treff.</td></tr>")
		else
			'write a message if number of hits are more then numhits
			if rsSearchResult.RecordCount > maxNumHits-1 then
				Response.Write("<tr class=""main_heading2""><td>Ditt s�k returnerte mer enn " & maxNumHits & " treff. Kun de " & maxNumHits & " f�rste treffene vil vises</td></tr>")
				Response.Write("<tr class=""plaintext""><td>Det kan v�re lurt � bruke filtrering i s�kesiden for � f� f�rre treff.</td></tr>")
				Response.Write("<tr class=""plaintext""><td>�nsker du mer informasjon om hvordan best mulig utf�re et s�k, se v�r <a href=""HelpSite.asp?anchor=search"">hjelpeside</a>.</td></tr>")

				if Request.Form("flagSoundSearch")  <> "" then
					Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/soundlist.asp"">her</a> for � g� tilbake til s�kesiden.</td></tr>")
				elseif Request.Form("simpleFreeTextSearch") <> "" then
					Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/search.asp"">her</a> for � g� til s�kesiden.</td></tr>")
				else
					Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/search.asp"">her</a> for � g� tilbake til s�kesiden.</td></tr>")
				end if

			else
				Response.Write("<tr class=""main_heading2""><td>Ditt s�k ga " & rsSearchResult.RecordCount & " treff.</td></tr>")
			end if
			Response.Write("<tr class=""news""><td>Du s�kte etter '" & searchWord & "' (med eventuelle kategorivalg).</td></tr>")
		end if
		Response.Write("</TABLE>")

		'Logic for moving cursor to displaying next hits in the result list when next or previois link is clicked
		'If (Request.QueryString("pageNumber")<>"") AND (Request.QueryString("pageNumber") > 1) Then
		If (pageCounter <> "") Then
		 ' 	pageCounter = Request.QueryString("pageNumber")
			if Request.Form("flagSoundSearch")<> "" then
				rsSearchResult.Move(15*(pageCounter-1))
				lastRecord = pageCounter*15
			else
				rsSearchResult.Move(20*(pageCounter-1))
				lastRecord = pageCounter*20
			end if
		else
			pageCounter = 1
			rsSearchResult.MoveFirst

			if Request.Form("flagSoundSearch")<> "" then
				lastRecord = pageCounter*15
			else
				lastRecord = pageCounter*20
			end if
		end If

		'setting last record variable to either 20, 40, 60 etc (20 hits per page), or to the last record if you're on the last page
		'lastRecord = pageCounter*20
		if lastRecord > rsSearchResult.RecordCount then
			lastRecord = rsSearchResult.RecordCount
		end if

		'write a table of results
		if Request.Form("flagSoundSearch")  <> "" then
			Response.Write("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""620""><tr height=""4""><td colspan=5>&#160;</td></tr>")
		elseif Session("Browser") = "ns" then
		'make air in the table
			Response.Write("<table border=""0"" cellpadding=""1"" cellspacing=""3"" width=""620""><tr height=""4""><td colspan=5>&#160;</td></tr>")
		else
		'not necessary in IE
			Response.Write("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""620""><tr height=""4""><td colspan=5>&#160;</td></tr>")
		end if

		Dim tmpBGColorToogle 'variable to show each second row with background color
		tmpBGColorToogle = True
		Dim i, bgcolor,tempSoundLink
		'if it was a sound search, display sound icons and links together with search result
		if 	Request.Form("flagSoundSearch")  <> "" then
			'add Javascript code for soundonweb functionality
			Response.Write ("<script language=""javascript"" src=""http://194.19.39.29/kunde/ntb/flashsound.js""></script>")
			'for all hits (per page) write title, date and relevance
			for i=lastRecord-14 to lastRecord
				if rsSearchResult.EOF = false then
					if tmpBGColorToogle then
					'functionality to display background color on each second row
						bgcolor="#F3F8FE"
						tmpBGColorToogle = false
					else
						bgcolor="white"
						tmpBGColorToogle = true
					end if

					'link without the html-text
					tempSoundLink = "<script language=""javascript"">var lyd%s = new FlashSound();" & "</" & "script>" & _
									"<td><a href=""javascript://"" onmouseover=""lyd%s.TGotoAndPlay('/','start')"" onmouseout=""lyd%s.TGotoAndPlay('/','stop')"">" & _
									"<img src=""../images/sound.gif"" border=""0""></a>&#160; "& _
									"<" & "script>lyd%s.embedSWF(""http://194.19.39.29/kunde/ntb/flash/%f.swf"");" & "</" & "script>&#160;" & _
									"</td><td> | <a href='http://194.19.39.29/kunde/ntb/mp3/%f.mp3'><font class='news'>MP3-fil</font></a> &#160; "& _
									"</td><td>| <a href=""sound_html.asp?id=%s&fname=%f&adate=%d"" onclick=""ws(%s, '%f');return false;""><font class='news'>HTML-kode</font></a></td>"
					tempSoundLink = Replace(tempSoundLink, "%f", rsSearchResult.Fields("SoundFileName").Value)
					tempSoundLink = Replace(tempSoundLink, "%s", rsSearchResult.Fields("RefID").Value)
					tempSoundLink = Replace(tempSoundLink, "%d", rsSearchResult.Fields("CreationDateTime").Value)

					Response.Write("<tr bgcolor=" & bgcolor & "><td width=""90""><font class=main_newstime>" & FormatDate(rsSearchResult.Fields("CreationDateTime").Value) & "</font></td>")

					'Response.Write("<td width=""350""><font class=plaintext> <A href=""../template/x.asp?a=" & rsSearchResult.Fields("RefID").Value & """ class=""list"" onclick=""w("& rsSearchResult.Fields("RefID").Value &");return false;"" onmouseover=""return overlib('" & replace(rsSearchResult.Fields("Ingress").Value,"""", "&quot;") & "', CAPTION, '" & replace(rsSearchResult.Fields("ArticleTitle").Value,"""", "&quot;") & "') ;"" onmouseout=""return nd();"">" & rsSearchResult.Fields("ArticleTitle").Value  & "</a></font></td>")

					strTempTitle = replace(rsSearchResult.Fields("ArticleTitle").Value,"""", "&quot;")
					strTempTitle = replace(strTempTitle,"'", "&#146;")
					strTempIngress = replace(rsSearchResult.Fields("Ingress").Value,"""", "&quot;")
					strTempIngress = replace(strTempIngress,"'", "&#146;")

					Response.Write("<td width=""350""><font class=n> <A class=x href=""../template/x.asp?a=" & rsSearchResult.Fields("RefID").Value & """ onclick=""w("& rsSearchResult.Fields("RefID").Value &");return false;"" onmouseover=""return overlib('" & strTempIngress & "', CAPTION, '" & strTempTitle & "') ;"" onmouseout=""return nd();"">" & rsSearchResult.Fields("ArticleTitle").Value  & "</a></font></td>")
					Response.Write tempSoundLink
					rsSearchResult.MoveNext
				end if
			next
		else
		'it was an ordinary search, not a sound search
			'for all hits (per page) write title, date and relevance (only show relevance
			'for items from Autonomy
			if boolRelevanceExist then 'there is a relevance to display
				for i=lastRecord-19 to lastRecord
					if rsSearchResult.EOF = false then
						if tmpBGColorToogle then
						'functionality to display background color on each second row
							bgcolor="#F3F8FE"
							tmpBGColorToogle = false
						else
							bgcolor="white"
							tmpBGColorToogle = true
						end if
						Response.Write("<tr height=""20"" bgcolor=" & bgcolor & "><td width=""90""><font class=main_newstime>" & FormatDate(rsSearchResult.Fields("CreationDateTime").Value) & "</font></td>")
						'Response.Write("<td width=""410"" colspan='3'><font class=plaintext> <A href=""../template/x.asp?a=" & rsSearchResult.Fields("RefID").Value & """ class=""list"" onclick=""w("& rsSearchResult.Fields("RefID").Value &");return false;"" onmouseover=""return overlib('" & replace(rsSearchResult.Fields("Ingress").Value,"""", "&quot;") & "', CAPTION, '" & replace(rsSearchResult.Fields("ArticleTitle").Value,"""", "&quot;") & "') ;"" onmouseout=""return nd();"">" & rsSearchResult.Fields("ArticleTitle").Value  & "</a></font></td>")

						strTempTitle = replace(rsSearchResult.Fields("ArticleTitle").Value,"""", "&quot;")
						strTempTitle = replace(strTempTitle,"'", "&#146;")
						strTempIngress = replace(rsSearchResult.Fields("Ingress").Value,"""", "&quot;")
						strTempIngress = replace(strTempIngress,"'", "&#146;")

						Response.Write("<td width=""410"" colspan='3'><font class=n> <a class=x href=""../template/x.asp?a=" & rsSearchResult.Fields("RefID").Value & """ onclick=""w("& rsSearchResult.Fields("RefID").Value &");return false;"" onmouseover=""return overlib('" & strTempIngress & "', CAPTION, '" & strTempTitle & "') ;"" onmouseout=""return nd();"">" & rsSearchResult.Fields("ArticleTitle").Value  & "</a></font></td>")
						'Response.Write("<td width=""410""><font class=plaintext> <A href=""Javascript:w("& rsSearchResult.Fields(0).Value &" );"" class=""list"" title=""" & rsSearchResult.Fields(2).Value & """>" & rsSearchResult.Fields(1).Value  & "</a></font></td>")

						'Response.Write("XX " & rsSearchResult.Fields("Ingress").Value)

						Response.Write("<td align=""right"" width=""120""><font class=news> (Relevans: " & rsSearchResult.Fields("Relevance").Value & "%)</font></td></tr>")
						rsSearchResult.MoveNext
					end if
				next
			else 'boolRelevanceExist = false and no relevance exist
				for i=lastRecord-19 to lastRecord
					if rsSearchResult.EOF = false then
						if tmpBGColorToogle then
						'functionality to display background color on each second row
							bgcolor="#F3F8FE"
							tmpBGColorToogle = false
						else
							bgcolor="white"
							tmpBGColorToogle = true
						end if
						Response.Write("<tr height=""20"" bgcolor=" & bgcolor & "><td width=""90""><font class=main_newstime>" & FormatDate(rsSearchResult.Fields("CreationDateTime").Value) & "</font></td>")

						strTempTitle = replace(rsSearchResult.Fields("ArticleTitle").Value,"""", "&quot;")
						strTempTitle = replace(strTempTitle,"'", "&#146;")
						strTempIngress = replace(rsSearchResult.Fields("Ingress").Value,"""", "&quot;")
						strTempIngress = replace(strTempIngress,"'", "&#146;")

						'Response.Write("<td width=""600"" colspan='4'><font class=plaintext> <A href=""../template/x.asp?a=" & rsSearchResult.Fields("RefID").Value & """ class=""list"" onclick=""w("& rsSearchResult.Fields("RefID").Value &");return false;"" onmouseover=""return overlib('" & replace(rsSearchResult.Fields("Ingress").Value,"""", "&quot;") & "', CAPTION, '" & replace(rsSearchResult.Fields("ArticleTitle").Value,"""", "&quot;") & "') ;"" onmouseout=""return nd();"">" & rsSearchResult.Fields("ArticleTitle").Value  & "</a></font></td>")
						Response.Write("<td width=""600"" colspan='4'><font class=n><A class=x href=""../template/x.asp?a=" & rsSearchResult.Fields("RefID").Value & """ onclick=""w("& rsSearchResult.Fields("RefID").Value &");return false;"" onmouseover=""return overlib('" & strTempIngress & "', CAPTION, '" & strTempTitle & "') ;"" onmouseout=""return nd();"">" & rsSearchResult.Fields("ArticleTitle").Value  & "</a></font></td>")

						rsSearchResult.MoveNext
					end if
				next
			end if 'end if there is a relevance or not
		end if 'end if it was a sound search or not
		Response.Write("</table>")


		'write which page is currently displayed (and total page number)
		Response.Write("<br><div class=""news"">Side " & pageCounter &  " av " & rsSearchResult.PageCount & "&#160;&#160;&#160;")

		'Check which page is active and decide to show next and previous links or not
		if (CInt(rsSearchResult.RecordCount) > CInt(rsSearchResult.PageSize)) then
		'there are more hits than can be displayed on only one page
			if (CInt(pageCounter) = CInt(rsSearchResult.PageCount)) then
				'do not have next as link
				Response.Write("<A href=""javascript:submitPage('" & searchWord & "', 'pageNumber'," & pageCounter - 1 & ")"">Forrige side</a> - Neste side ")
			elseif pageCounter = 1 then
				'do not have previous as link
				Response.Write("Forrige side -  <A href=""javascript:submitPage('" & searchWord & "', 'pageNumber'," & pageCounter + 1 & ")""> Neste side </a>")
			else
				'both next and previous should be links
				Response.Write("<A href=""javascript:submitPage('" & searchWord & "', 'pageNumber'," & pageCounter - 1 & ")"">Forrige side</a> <A href=""javascript:submitPage('" & searchWord & "', 'pageNumber'," & pageCounter + 1 & ")""> - Neste side </a>")
			end if
			Response.Write("</div>")
		end if

	'if there are NO search hits, display a message to the user
	'--------------------------------------------------------------------------------
	else 'there are no entries in the recordSet, no hits
	'show a message indicating that the user must refine her search

	Response.Write("<TABLE border=""0"" cellpadding=""0"" cellspacing=""0"" width=""620"">")
	if searchWord = "" then
			Response.Write("<tr class=""main_heading2""><td>Ditt kategoris�k ga dessverre ingen treff.</td></tr>")
		else
			Response.Write("<tr class=""main_heading2""><td>Ditt s�k etter '" & searchWord & "' ga dessverre ingen treff.</td></tr>")
		end if
		Response.Write("<tr class=""plaintext""><td>Det kan v�re lurt � utvide s�kekriteriet ditt for � f� flere treff. </td></tr>")
		Response.Write("<tr class=""plaintext""><td>�nsker du mer informasjon om hvordan best mulig utf�re et s�k, se v�r <a href=""HelpSite.asp?anchor=search"">hjelpeside</a>.<td></tr>")
		if Request.Form("flagSoundSearch")  <> "" then
			Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/soundlist.asp"">her</a> for � g� tilbake til s�kesiden.</td></tr>")
		elseif Request.Form("simpleFreeTextSearch") <> "" then
			Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/search.asp"">her</a> for � g� til s�kesiden.</td></tr>")
		else
			Response.Write("<tr class=""plaintext""><td>Klikk <A href=""../mainpages/search.asp"">her</a> for � g� tilbake til s�kesiden.</td></tr>")
		end if
	end if 'end if there are any hits or not
	Response.Write("</TABLE>")
	%>

    </TD>
  </TR>
</TABLE>
</FORM>
</BODY>
</HTML>
<%

' -----------------------------------------------------------------------------
' FormatDate
'
' Description:	This function formats the date to norwegian format
'				The format is DD.MM.YYYY hh:mm
'
' Parameters:	inDate - date to be formated
'
' Returns:		String with the in date in correct format
'
' Notes :		None
' -----------------------------------------------------------------------------
Function FormatDate(inDate)
	Dim tmpDate, tmpDay, tmpMonth
	tmpDate = CDate(inDate)
	tmpDay = DatePart("D", tmpDate)
	if CInt(tmpDay) < 10 then
	'add zero as prefix to days 1-9
		tmpDay = "0" & tmpDay
	end if
	tmpMonth = DatePart("M", tmpDate)
	if CInt(tmpMonth) < 10 then
	'add zero as prefix to months 1-9
		tmpMonth = "0" & tmpMonth
	end if
	FormatDate = tmpDay & "." & tmpMonth & "." & DatePart("YYYY", tmpDate) & "&#160;" & FormatDateTime(tmpDate,4)
End Function

' -----------------------------------------------------------------------------
' GetCategorySearchQuery
'
' Description:	This function retrieves all the different selected maingroups, subgroups
'				maincategories and belonging subcategories, and put all together in one
'				string.
'
' Parameters:	None
'
' Returns:		String of all selected categories to be used in the search call
'
' Notes :		None
' -----------------------------------------------------------------------------
Function GetCategorySearchQuery()
	'get main group
	Dim mgSum, iMainGroupListCount
	For iMainGroupListCount = 1 to Request.Form("MainGroupListBox").Count
		if (Request.Form("MainGroupListBox")(iMainGroupListCount) = "-1") then
			mgSum = -1
			exit for
		end if
		mgSum = mgSum & "+" & Request.Form("MainGroupListBox")(iMainGroupListCount)
	Next
	if mgSum = "" then
		mgSum = -1
	end if

	'get sub group
	Dim sgSum, iSubGroupListCount
	For iSubGroupListCount = 1 to Request.Form("SubGroupListBox").Count
		if (Request.Form("SubGroupListBox")(iSubGroupListCount) = "-1") then
			sgSum = -1
			exit for
		end if
		sgSum = sgSum & "+" & Request.Form("SubGroupListBox")(iSubGroupListCount)
	Next
	if sgSum = "" then
		sgSum = -1
	end if

	'get main categories and belonging sub categories, and put together to total categorystring
	Dim scSum, mcscTotal, iMCCount, iSCCount, tmpArray
	mcscTotal = ""
	'loop through all different main categories
	For iMCCount = 1 to Request.Form("MainCategoryListBox").Count
		scSum = null
		'if main category is -1 (all is selected), there is no need to check for other values or for sub categories
		if (Request.Form("MainCategoryListBox")(iMCCount)= "-1") then
			mcscTotal = mgSum & ";" & sgSum & ";-1;-1"
		 	exit for
		else
		'for each main category find corresponding sub categories
			'the next if statement is to prevent opera browsers from craching (for some reason)
			if Request.Form("SubCategoryListBox") <> "" then
				For iSCCount = 1 to Request.Form("SubCategoryListBox").Count
			 		tmpArray = Split(Request.Form("SubCategoryListBox")(iSCCount), "_")
	   				if tmpArray(0) = Request.Form("MainCategoryListBox")(iMCCount)then
			 			scSum = scSum & "+" & tmpArray(1)
			 		end if
			 	Next
			end if
		 	mcscTotal = mcscTotal & mgSum & ";" & sgSum & ";+" & Request.Form("MainCategoryListBox")(iMCCount) & ";"  & scSum & ";"
		end if
	Next
	if mcscTotal = "" then
		mcscTotal = mgSum & ";" & sgSum & ";-1;-1"
	end if
	GetCategorySearchQuery = mcscTotal
End Function

' -----------------------------------------------------------------------------
' FormatDateSearch
'
' Description:	This function formats the date to the format needed by serach
'				The format is DD;MM;YYYY
'
' Parameters:	inDate - date to be formated
'
' Returns:		String with the in date in correct format
'
' Notes :		None
' -----------------------------------------------------------------------------
Function FormatDateSearch(inDate)
	Dim tmpDate, tmpDay, tmpMonth
	tmpDate = CDate(inDate)
	tmpDay = DatePart("D", tmpDate)
	if CInt(tmpDay) < 10 then
	'add zero as prefix to days 1-9
		tmpDay = "0" & tmpDay
	end if
	tmpMonth = DatePart("M", tmpDate)
	if CInt(tmpMonth) < 10 then
	'add zero as prefix to months 1-9
		tmpMonth = "0" & tmpMonth
	end if
	FormatDateSearch = tmpDay & ";" & tmpMonth & ";" & DatePart("YYYY", tmpDate)
End Function

%>