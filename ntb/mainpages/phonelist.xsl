<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" standalone="yes"/>

<!-- 
	XSLT Stylesheet by NTB Roar Vestre 
	Last Change 16.04.2002 by RoV
-->

<xsl:param name="liste"></xsl:param>

<xsl:template match="/telefonliste">
	  	<xsl:apply-templates select="person">
			<xsl:sort select="navn" order="ascending"/>
		</xsl:apply-templates>				
		
	<td colspan="5" valign="bottom" height="25"><div class="hjelpetekst">Sist oppdatert: <xsl:value-of select="timestamp"/></div></td>

</xsl:template>

<!-- Templates -->

<xsl:template match="person">
	<tr>
	<td style="width:180;" class="news" bgcolor="#F3F8FE"><xsl:value-of select="navn"/></td>
	<td style="width:130;" class="news" bgcolor="#F3F8FE"><xsl:value-of select="avdeling"/></td>
	<td style="width:115;" class="news" bgcolor="#F3F8FE"><xsl:value-of select="telefon"/></td>
	<td style="width:155;" class="news" bgcolor="#F3F8FE"><xsl:value-of select="mobil"/></td>
	<td style="width:210;" class="news" bgcolor="#F3F8FE"><xsl:apply-templates select="epost"/></td>
	</tr>
</xsl:template>

<xsl:template match="epost">
	<a class="x"><xsl:attribute name='href'>mailto:<xsl:value-of select="."/></xsl:attribute>
	<xsl:value-of select="."/>
	</a>
</xsl:template>

</xsl:stylesheet>