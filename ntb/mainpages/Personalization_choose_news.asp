<%
'***********************************************************************************
'* Personalization_choose_news.asp
'*
'* This file is used for entering search queries that will be a part of the personal
'* profile. Both freetext search and category search is possible, but not together.
'* The different drop down list are mostly populated from the database.
'* Several Javascripts are handling logic on the different form objects (check boxes,
'* listboxes etc) to make selection more user friendly and smart.
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar.
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen
'* CREATED DATE: 2002.04.13
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.07.01
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_cache_lib.asp" -->

<%

Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

%>

<html>
<head>
	<link href="<%=style%>" type=text/css rel=stylesheet>
	<%= TitleTag() %>

	<script language="Javascript">
	//function to uncheck all region checkbox options if the all option is selected
	function onClick_unCheckAll(chechkboxName){
		for(i=0; i < document.forms.chooseNews.length; i++){
			if(document.forms.chooseNews.elements[i].name == chechkboxName){
			    document.forms.chooseNews.elements[i].checked = false;
			}
		}
		CheckSet()
	}
	//check if the right radiobutton is set
	function CheckSet(){
		if (document.forms.chooseNews.elements.SearchRadio[0].checked) {
			alert("Ved spesifisering av kategorier m� kategoris�k v�re valgt. Dette blir n� spesifisert for deg.");
			document.forms.chooseNews.elements.SearchRadio[1].checked = true
		}
	}

	//check whether the user have entered a title
	function CheckTitle(){
		if (document.forms.chooseNews.SearchRadio[1].checked){
			if (document.forms.chooseNews.elements.txtNewsHeading.value == "") {
				alert("Husk � skrive inn en tittel p� kategoris�ket ditt!");
			}
			else{
				document.forms.chooseNews.submit();
			}
		}
		else{
			document.forms.chooseNews.submit();
		}
	}

	function CheckMac(){
		if (document.forms.chooseNews.SearchRadio[0].checked){
			if((navigator.userAgent.indexOf("MSIE") >= 0) && (navigator.userAgent.indexOf("Mac") >= 0)){
				alert("Bruker du Internet Explorer for Mac OSX, kan du ha bare et friteksts�k som du m� plassere i det �verste feltet til venstre.");
			}
		}
	}


	function SetCat(){
		var myBrowserName = navigator.appName;
		var mgOptionText = document.forms.chooseNews.elements.MainGroupListBox.options[document.forms.chooseNews.elements.MainGroupListBox.selectedIndex].text
		var flag = false
		var mcOptionText
		for(i=1; i < document.forms.chooseNews.elements.MainCategoryListBox.length; i++)
		{
			mcOptionText = document.forms.chooseNews.elements.MainCategoryListBox.options[i].text
			if (mcOptionText == mgOptionText)
			{
				flag = true
				document.forms.chooseNews.MainCategoryListBox.selectedIndex=i;
		//		document.forms.chooseNews.elements.MainCategoryListBox.disabled = true;
			}
		}

		if (flag == false)//selected option in maingroup doesn't match any group in maincategory
		{
			document.forms.chooseNews.MainCategoryListBox.selectedIndex=0;
		//	document.forms.chooseNews.elements.MainCategoryListBox.disabled = false;

		}
		if (myBrowserName == 'Netscape') // Netscape needs a refresh to fill sub category listbox
		{
			window.location.reload();
		}
		fillsubcat(chooseNews)
	}
	</script>
</head>

<BODY topmargin="0" leftmargin="0" >
<TABLE width=100% height="100%" border="0" CELLSPACING="0" CELLPADDING="0">
<TR VALIGN="TOP">
	<TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
</TR>
<TR VALIGN="TOP">
	<TD ALIGN="LEFT"  bgcolor="#003084">
		<!-- #INCLUDE FILE="../template/leftmenu.asp" -->
	</TD>
	<TD ALIGN="LEFT">&#160; </TD>
	<TD ALIGN="LEFT" width="100%">
		<%=RenderTitleBar("Personalisering av din egen side", "NoPhotolink", 620)%>
		<table border="0" width="600">
		<tr>
			<td class="news11" height="120">
				P� denne siden kan du spesifisere et bestemt nyhetsutvalg for
				den ruten som ble valgt ved forrige steg. Det er to m�ter � lage nyhetsutvalg
				p� (disse kan ikke kombineres):<br>
				<br>&nbsp;<img src="../images/rightarrow.gif" WIDTH="11" HEIGHT="9">&nbsp;
				Friteksts�k. I den aktuelle ruten vil du finne et felt der du
				kan skrive inn din egen s�kestreng. Du kan skifte s�keord n�r som helst.
				<br>&nbsp;<img src="../images/rightarrow.gif" WIDTH="11" HEIGHT="9">&nbsp;
				Kategoris�k. Velg en eller flere stoffgrupper/kategorier/underkategorier,
				og kombiner med geografiske s�kekriterier. Husk � gi s�ket ditt et navn.

				<br><br>�nsker du mer informasjon om hvordan f� mest mulig ut av din
				personaliserte side, se v�r
				<a href="HelpSite.asp#personalization">hjelpeside</A>.
			</td>
			<td align="right">
				<form action="Personalization_choose_style.asp" method="post" name="changeStyle">
				<INPUT class="formbutton" type="submit" value="Endre vindusoppsett" id=changeStyle name=changeStyle  style="background:#bfccd9 none; color:#003366; width:125">
				</form>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<form action="savetoprofile.asp" method="post" name="chooseNews">
				<table borderColor="#61c7d7" width="600" cellSpacing="0" cellPadding="1" align="left" border="1">
				<tr>
					<td class="formbutton" vAlign="top" width="150" style="border:none;">
						<INPUT type="radio" value="TextSearch" name="SearchRadio" onClick="CheckMac()">Friteksts�k
					</td>
					<td align="right" style="border:none;">
						<INPUT class="formbutton" type="button" value="Bekreft valgt s�k" name=chooseNews onClick="CheckTitle()" style="background:#bfccd9 none; color:#003366; width:125">
					</td>
				</tr>
				</table>
			</td>
		<tr>
			<td class="formbutton" vAlign="top" width="150" style="border:none;" colspan="2">
				<table borderColor="#61c7d7" width="600" cellSpacing="0" cellPadding="1" align="left" border="1">
				<tr>
					<td class="formbutton" vAlign="top" style="border:none;">
					<table width="590" cellSpacing="0" cellPadding="1" align="left" border="0">
					<tr>
						<td class="formbutton" vAlign="top" width="255" style="border:none;">
							<INPUT type="radio" value="CatSearch" name="SearchRadio" checked>
							Kategoris�k
						</td>
						<td class="hjelpetekst" vAlign="top" style="border:none;">
							Overskrift p� det valgte nyhetss�ket:
							<INPUT class="news11" type="textbox" name="txtNewsHeading" style="HEIGHT: 16px; width: 160px;" maxlength="22">
						</td>
					</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="formbutton" vAlign="top" width="150" style="border:none;" colspan="2">
					<table borderColor="#61c7d7" height="50" width="590" cellSpacing="0" cellPadding="1" align="left" border="1">
					<tr>
						<td class="news11" vAlign="top" width="150" style="border:none;">
							Stoffgruppe
						</td>
						<td class="news11" vAlign="top" width="150" style="border:none;">
							Undergruppe
						</td>
						<td class="news11" vAlign="top" width="160" style="border:none;">
							Kategori
						</td>
						<td class="news11" vAlign="top" width="160" style="border:none;">
							Underkategori
						</td>
					</tr>
					<tr height="50">
						<td class="news11" vAlign="top" width="110" height="50" style="border:none;">
							<select name="MainGroupListBox" size="5" onClick="CheckSet()" onChange="SetCat()" multiple class="news11" style="width:115px">
							<option value="-1" selected>Alle</option>
					        <%	dim mgCounter, mgName, kuMgName, PrMgName, prmname
								set mgCounter = getMaingroups()

							for each mgName in mgCounter
								if ((mgName = "Innenriks") or (mgName = "Utenriks") or (mgName = "Sport")) then
									response.write("<option value=" & mgCounter(mgName) & ">" & mgName & "</option>")
								elseif (mgName = "Kultur og underholdning") then
								'don't write Kultur & underholdning immediately, it should appear last in the list box
										kuMgName = mgName

								elseif (mgName = "Priv-til-red") then
										'don't write Priv to Red immediately, it should appear second last in the list box
										PrMgName = mgName
								elseif (mgName = "PRM-NTB") then
										'don't write PRM immediately, it should appear last in the list box
										prmname = mgName
								end if
							next

							if (kuMgName <> "") then
							'write Kultur & underholdning as the second last option in the list box
								response.write("<option value=" & mgCounter(kuMgName) & ">Kultur og underh.</option>")
							end if

							if (PrMgName <> "") then
							'write Priv to Red as the last option in the list box
								response.write("<option value=" & mgCounter(PrMgName) & ">Priv. til red.</option>")
							end if

							if (prmname <> "") then
								'write Kultur & underholdning as the last option in the list box
								response.write("<option value=" & mgCounter(prmname) & ">Pressemeldinger</option>")
							end if
							%>

							</select>
						</td>
						<td class="news11" vAlign="top" width="130" height="50" style="border:none;">
							<select name="SubGroupListBox" size="5" onClick="CheckSet()" multiple class="news11" style="width:115px">
							<option value="-1" selected>Alle</option>
							<%	dim sgCounter, sgName
								set sgCounter = getSubgroups()

							'Undergruppe-values should be merged into the following choices:
							'	NYHETER (Nyheter, Feature, Nekrologer, Profiler og intervjuer, Rettelser)
							'	BAKGRUNN (Analyse, Bakgrunn, Faktaboks)
							'	TABELLER/RESULTATER (Tabeller og resultater)
							'	NOTISER (Notiser)
							'	OVERSIKTER (Overskrifter)

							'Assign values to each option value:
							dim valNyh, valBak, valTab, valNot, valOvr

							for each sgName in sgCounter
		 						if ((sgName = "Nyheter") or (sgName = "Feature") or (sgName = "Nekrologer") or (sgName = "Profiler og intervjuer") or (sgName = "Rettelser")) then
									valNyh = valNyh & "+" & sgCounter(sgName)
								end if
								if ((sgName = "Analyse") or (sgName = "Bakgrunn") or (sgName = "Faktaboks") ) then
									valBak = valBak & "+" & sgCounter(sgName)
								end if
								if (sgName = "Tabeller og resultater") then
									valTab = valTab & "+" & sgCounter(sgName)
								end if
								if (sgName = "Notiser") then
									valNot = valNot & "+" & sgCounter(sgName)
								end if
								if (sgName = "Oversikter") then
									valOvr = valOvr & "+" & sgCounter(sgName)
								end if
							next %>

							<option value=<%=valNyh%>>Nyheter</option>
							<option value=<%=valBak%>>Bakgrunn</option>
							<option value=<%=valTab%>>Tabeller/resultater</option>
							<option value=<%=valNot%>>Notiser</option>
							<option value=<%=valOvr%>>Oversikter</option>
							</select>
						</td>
						<td class="news11" vAlign="top" width="170" height="50" style="border:none;">
							<select name="MainCategoryListBox"  onClick="CheckSet()" onChange="fillsubcat(this.form)" size="5" multiple class="news11" style="width: 160px">
							<option value="-1" selected>Alle</option>
															<%
						' Changes are made to categories and subcategories functions.
						' Now functions return collections of 2-element arrays,
						' where element 0 is descriptive name and element 1 is value (bit position)
						' For subcategories collection there is also third element in the array
						' with [category] & "_" & [subcategory] content.
						' Similar collections for other lists are available from
						' the cache dictionary named "sorted", it contains collections.
						' Use function GetCategories() as template if sorted list is needed.
						' Ralfs 20020413
							dim mcCounter, mcName
							set mcCounter = getSortedCategories()
							for each mcName in mcCounter
							response.write("<option value=" & mcName(1) & ">" & mcName(0) & "</option>")
							next %>

							</select>
						</td>
						<td class="news11" vAlign="top" width="180" height="50" style="border:none;">
							<select name="SubCategoryListBox" size="5" onClick="CheckSet()" multiple class="news11" style="width: 180px">
					        <%response.write("Subcategories: <script language=""javascript""> var subcat = new Array(" )

							'*********************
					        'Functionality to create javascript code containing arrays with all subcategories
					        'This javascript code will be used client side to update the subcategory-field dependent on chosen maincategory
					        '*********************
					        dim tempC, tempSC, scCounter, scName
					        tempC = 0
					        tempSC = 0

					        'for each maincategory, get the id to be used to retrieve corresponding subcategory
					         for each mcName in mcCounter
					        	set scCounter = getSubcategories(mcName(1))

								if tempSC = 0 then
									response.write("new Array(")
								else
									response.write("),new Array(")
								end if

								'for each subcategory, populate the javascript-array with name and id
					        	if scCounter.count > 0 then 'only popluate if there are any subcategories
									for each scName in scCounter
										if tempC = 0 then
											response.write("'" & mcName(1) & "_-1','Alle " &  mcName(0) & "'")
											response.write(",'" & scName(2) & "','" &  scName(0) & "'")
										else
											response.write(",'" & scName(2) & "','" &  scName(0) & "'")
										end if
										tempC = 1
									next
								else
								response.write("'" & mcName(1) & "_-1','Alle " &  mcName(0) & "'")
								end if
								tempSC = 1
								tempC = 0
							next

							response.write(")); </script>")
							%>
							</select>

							<script language="javascript">
							function fillsubcat(form){

								form.SubCategoryListBox.selectedIndex = -1;
								form.SubCategoryListBox.options.length = 0;
								locs = form.SubCategoryListBox.options;
								sel = new Array;

								for ( c=1; c<form.MainCategoryListBox.options.length; c++ ) {

									if (form.MainCategoryListBox.options[c].selected) {
									//locs[locs.length]=new Option('Hele '+form.MainCategoryListBox.options[c].text,form.MainCategoryListBox.options[c].value);
										sel[sel.length]=locs.length;
										for (lc=0; lc<subcat[c-1].length; lc+=2 ) {
											locs[locs.length] = new Option(subcat[c-1][lc+1],subcat[c-1][lc]);
										}
									}
								}

								 for (c=0; c<sel.length; c++) {
									locs[sel[c]].selected = true;
								}
							}

							function myonload(){

								fillsubcat(document.forms.chooseNews);
							}

							window.onload = myonload;

							</script>

						</td>
					</tr>
					<tr>
						<td colspan="4" style="border:none;" class="hjelpetekst">&nbsp;&nbsp;Hold CTRL nede for � velge flere av gangen
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="7"></td>
			</tr>
			<tr>
				<td style="border:none;">
					<table border="0" valign="top" align="left">
					<tr>
						<td valign="top" align="left">
							<table borderColor="#61c7d7" width="210" height="178" cellSpacing="0" cellPadding="1" align="left" valign="top" border="1">
							<tr>
								<td class="news11" vAlign="top" width="210" colspan="2" style="border:none;">
									Geografi - Verden
								</td>
							</tr>
							<tr>
							<%	'Logic for displaying world regions in two columns
								dim wrCounter, wrName, wrLength, wrColLength, wcolCounter, WorldRegionAll, LocalRegionAll
								set wrCounter = getSortedWorldRegions()
								wrLength = wrCounter.count
								wrColLength = wrLength\2
							%>

								<td class="news11" vAlign="top" width="100" style="border:none;">
									<table><tr><td>
										<input type="checkbox" onclick="onClick_unCheckAll('WorldRegionCheckBox')" id="cb_&quot;<%=WorldRegionAll%>" name="WorldRegionAllCheckBox" checked value="-1"></td>
										<td class="news11">Alle</td>
								<%
									wcolCounter = 0
									for each wrName in wrCounter
									if wcolCounter = wrColLength  then
										response.write("</table></TD><TD class='news11' vAlign=top width=120 style='border:none;'><table>")''
									end if
									Response.Write "<tr><td style='width:2px'>"
									response.write("<INPUT type=""checkbox"" onclick=""onClick_unCheckAll('WorldRegionAllCheckBox')"" id=cb_" &  wrName(1) & " name=WorldRegionCheckBox value=" & wrName(1)& " >")
									Response.Write "</td><td class='news11'>" & wrName(0) & "</td>"
									Response.Write "</tr>"

									wcolCounter = wcolCounter + 1
									next %>
									</table>
								</td>
							</tr>
							</table>
						</td>
						<td width="1"></td>
						<td>
							<table borderColor="#61c7d7" width="350" height="178" cellSpacing="0" cellPadding="1" align="left" valign="top" border="1">
							<tr>
								<td class="news11" vAlign="top" width="366" colspan="3" style="border:none;">
									Geografi - Norge
								</td>
							</tr>
							<tr>
								<%	'Logic for displaying local regions in three columns
									dim lrCounter, lrName, lrLength, lrColLength, colCounter
										set lrCounter = getSortedLocalRegions()
										lrLength = lrCounter.count
										lrColLength = lrLength\3
								%>
								<td class="news11" vAlign="top" width="110" style="border:none;">
									<input type="checkbox" onclick="onClick_unCheckAll('LocalRegionCheckBox')" id="cb_&quot;<%=LocalRegionAll%>" name="LocalRegionAllCheckBox" checked>Alle<br>
								<%
									colCounter = 0
									for each lrName in lrCounter
									if (colCounter = lrColLength) or (colCounter = (lrColLength * 2))  then
										response.write("</TD><TD class=news11 vAlign=top width=110 style='border:none;'>")' '
									end if
									response.write("<INPUT type=""checkbox"" onclick=""onClick_unCheckAll('LocalRegionAllCheckBox')"" id=cb_" &  lrName(1) & " name=LocalRegionCheckBox value=" & lrName(1)& " >" & lrName(0) & "<br>")
									colCounter = colCounter + 1
									next %>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
		</td>
	</tr>
	</table>
	<tr>
		<td colspan="2">
			<table width="600" height="1" cellSpacing="0" cellPadding="1" align="left" border="0">
			<tr>
				<td class="news11" vAlign="top" width="600" align="right">
					<input type="hidden" name="ColumnID" value="<%=Request.QueryString("ColumnID")%>">
					<INPUT class="formbutton" type="button" value="Bekreft valgt s�k" name=chooseNews onClick="CheckTitle()" style="background:#bfccd9 none; color:#003366; width:125">
				</td>
			</tr>
			</form>
			</table>
   		</td>
   	</tr>
	</table>
    </td>
</tr>
</table>
</body>
</html>
