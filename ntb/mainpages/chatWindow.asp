<%@ Language=VBScript %>
<%
'***********************************************************************************
' chatWindow.asp
'
' This file is used for chat, and is 
' 
' USES:
' MSMQ ActiveX COM objects, Application("QueueServer") for
' queue paths, Session("chatcontent") string variable to store
' previous messages for this session.
' Includes: chatWindow.asp which is the logic in the iframe
' Includes: warning.htm and warning_max.htm for giving messages to the user.
' NOTES:
' 'none'
'
' CREATED BY: Ralfs Pladers, Andersen 
' CREATED DATE: 2002.04.17
' UPDATED BY: Vegar Paulsen, Andersen
' UPDATED DATE: 2002.04.21
' REVISION HISTORY: none
'
'
'***********************************************************************************
%>
<HTML>
<HEAD>
<SCRIPT LANGUAGE="JavaScript">
</SCRIPT>
<META http-equiv="REFRESH" CONTENT="5">
<LINK href="../include/ntb.css" type=text/css rel=stylesheet>
</HEAD>
<BODY onload="Javascript:window.scrollTo(0,50000);">
<%
Const REC_MSG_START = "<div class=""chatTwo"">"
Const REC_MSG_END = "</div>"

Dim objChatters, reqChannel, strActiveChannel
Dim strQueueName, strQueuePath, strReturnQueuePath
Dim strThisUser, strChatWith

strThisUser = Request.ServerVariables("LOGON_USER")
Set objChatters = Application("ChatSessions")
reqChannel = Request("channel")

' find our chat session, setting strActiveChannel.
If IsNull(objChatters(strThisUser)) OR objChatters(strThisUser) = "" then
	strActiveChannel = ""
	else
	strActiveChannel = CStr(objChatters(strThisUser))
end if

'Check to see if parameter channel is retrieved, give msg if not
if reqChannel = "" then
	Response.Write("Chat channel parameter expected. Cannot proceed.")
	Response.End
end if

' if we are here this means that everything is good so far
' reqested channel must be valid channel name to open	
select case reqChannel
	case "report"
	strQueueName = "ntb_reportasjeleder"
	strChatWith = "Reportasjelederen"
	case "sport"
	strQueueName = "ntb_sportsjef"
	strChatWith = "vaktsjef p� sporten"
	case "uriks"
	strQueueName = "ntb_utenriksvakt"
	strChatWith = "Utenriksvakten"
	case "vakt"
	strQueueName = "ntb_vaktsjef"
	strChatWith = "Vaktsjefen"
	case else
	Response.Write("Goodbye, hacker :)")
	Session.Abandon
	Response.End
end select

strQueuePath = Application("QueueServer") & "\" & strQueueName
strReturnQueuePath = strQueuePath & "_retur"

'Looping through the queues, finding reply messages for user 
Dim oQueueInfo, oQueue, oMessage
Set oQueueInfo = Server.CreateObject("MSMQ.MSMQQueueInfo")
oQueueInfo.PathName = strReturnQueuePath
'Response.Write("Opening " & strReturnQueuePath)
'Response.flush
'Set oQueue = Server.CreateObject("MSMQ.MSMQQueue")
'On error resume next
Set oQueue = oQueueInfo.Open(1,0)
'on error goto 0
oQueue.Reset
Set oMessage = oQueue.PeekCurrent(,,1)
'Response.Write(TypeName(oMessage))
while TypeName(oMessage) <> "Nothing"
'User name is used as primaryKey on the messages.	
	if oMessage.Label = strThisUser then
		Set oMessage = oQueue.ReceiveCurrent
		'Closing connection on keyword "EXIT"
		if Cstr (oMessage.Body) = "EXIT" then
			Response.Redirect ("../mainpages/chat.asp?clean=yes&channel=" & reqChannel)
		end if
		Session("chatcontent") = Session("chatcontent") & _
		CStr(REC_MSG_START & CStr(oMessage.Body) & REC_MSG_END)
		Set oMessage = oQueue.PeekCurrent (,,1)
	else	
		Set oMessage = oQueue.PeekNext (,,1)
	end if			
If Err.number <> 0 then Set oMessage = nothing
'Response.Write(CStr(oMessage.Body))
wend
Set oMessage = Nothing
oQueue.Close
Set oQueue = Nothing
Set oQueueInfo = Nothing
On Error Goto 0
	Response.Write("<div class=""news11"">Du har �pnet et vindu for � snakke med <b>" & strChatWith & "</b>. For � slippe til andre n�r samtalen er over, vennligst klikk <b>Avslutt</b>. (Innholdet i samtalen vil fortsatt v�re tilgjengelig i et separat vindu).</div><br>")
	Response.Write(Session("chatcontent"))
	Response.Write(Request.Form("isClosed"))
%>

</BODY>
</HTML>
