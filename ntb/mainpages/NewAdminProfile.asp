<%
'***********************************************************************************
'* NewAdminProfile.asp
'*
'* This file is used to give the adminuser the opportunity to make a superuser to an 
'* organization
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar
'*		NTB_admin_render_lib.asp - For rendering of the userlist
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.05.10
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.20
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->   
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->


<%
Sub Main()

	'check whether the user has the rights to view this page
	if Session("Role") <> "Admin" then
		'give an errormessage
		Response.Redirect "../Authfiles/error.asp?Err=uautorisert"			
	end if

End Sub

%>

<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="../include/ntb.css">

	<%=TitleTag()%>
	
	<script language="Javascript">
	function SetDefault() {
	
		document.frmChangeProfile.txtFirstname.value = document.frmChangeProfile.txtFirstname.defaultValue
		document.frmChangeProfile.txtLastname.value = document.frmChangeProfile.txtLastname.defaultValue
		document.frmChangeProfile.txtEmail.value = document.frmChangeProfile.txtEmail.defaultValue
		document.frmChangeProfile.txtPhone.value = document.frmChangeProfile.txtPhone.defaultValue
		document.frmChangeProfile.txtRole.defaultValue
	}
	
	function CheckInput(){
		// Regular expression for validating at most of the emailaddresses.
		re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

		strMessage = " Kan ikke opprette ny bruker.";
		
		if (document.frmChangeProfile.txtUsername.value == "") {
			alert ("Ugyldig brukernavn." + strMessage);
		}
		else if (document.frmChangeProfile.txtFirstname.value == "") {
			alert ("Fornavn m� fylles ut." + strMessage);
			document.frmChangeProfile.txtFirstname.focus();
		}
		else if (document.frmChangeProfile.txtLastname.value == "") {
			alert ("Etternavn m� fylles ut." + strMessage);
			document.frmChangeProfile.txtLastname.focus();
		}
		else if (document.frmChangeProfile.txtEmail.value == "") {
			alert ("E-post m� fylles ut." + strMessage);
			document.frmChangeProfile.txtEmail.focus();
		}
		else if (document.frmChangeProfile.txtPhone.value == "") {
			alert ("Mobilnummer m� fylles ut." + strMessage);
			document.frmChangeProfile.txtPhone.focus();
		}
		else if (!re.test(document.frmChangeProfile.txtEmail.value)) {
			alert("Ugyldig e-post adresse.");
			document.frmChangeProfile.txtEmail.focus();
		}
		else {
			document.frmChangeProfile.submit();
		}
	}

	function GetToList(){
		location.href = "UserList.asp"
	}
	</script>
</head>

<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7">

<% 
'get the orgid
Dim coid
coid = Request.Form("hcoid")

Response.Write(RenderTitleBar("Ny administrator ", "NoPhotoLink", 620))%>

<TABLE border="0" class="news">
	<form name="frmChangeProfile" action="CompanyList.asp" method="Post">
	<%Response.Write RenderNewAdminProfile(coid)%>
	</form>
</table>  
</body>
</html>

