<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->  
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->  

<%	
Dim NewsProfile

if  Request.Form("SearchRadio") = "TextSearch" then
	'Set the searchstring
	NewsProfile = "TextSearch"
	Response.Write NewsProfile

elseif Request.Form("SearchRadio") = "CatSearch" then
	'getWorldRegion
	Dim wrSum, iWorldAreaCount
	if (Request.Form("WorldRegionAllCheckBox")<> "") then
	'if all regions are selected (the all option is checked)
		wrSum = -1
	else
	'loop trough all the selected regions and add to string
		For iWorldAreaCount = 1 to Request.Form("WorldRegionCheckBox").Count
			wrSum = wrSum & "+" & Request.Form("WorldRegionCheckBox")(iWorldAreaCount)
		Next
	end if
	'if no area is selected, set it to -1 (equal to all selected)
	if wrSum = "" then 
		wrSum = -1
	end if
		
	'getLocalRegion
	Dim lrSum, iLocalAreaCount
	if (Request.Form("LocalRegionAllCheckBox")<> "") then
	'if all local regions are selected (the all option is checked)
		 lrSum = -1
	else
	'loop trough all the selected local regions and add to string
		For iLocalAreaCount = 1 to Request.Form("LocalRegionCheckBox").Count
			lrSum = lrSum & "+" & Request.Form("LocalRegionCheckBox")(iLocalAreaCount)
		Next
	end if
	'if no area is selected, set it to -1 (equal to all selected)
	if lrSum = "" then 
		lrSum = -1
	end if
		
 	'Retrieve category search query
	Dim categorySearchQuery, searchQuery
	searchQuery = GetCategorySearchQuery
	
	'Retrieve the news heading
	Dim heading
	heading = Request.Form ("txtNewsHeading")
	
	if heading = "" then
		heading = "Kategorisøk"
	end if
	
	'Set the searchstring
	NewsProfile = searchQuery & "|" & wrSum & "|" & lrSum & "$" & heading
end if
	
'Update det profile with the new newsinfo
Dim ColumnID
ColumnID = Request.Form("ColumnID")

Call UpdateNewsInfo(NewsProfile, ColumnID)
Response.Redirect("Personalization_choose_window.asp")


' -----------------------------------------------------------------------------
' GetCategorySearchQuery
'
' Description:	This function retrieves all the different selected maingroups, subgroups
'				maincategories and belonging subcategories, and put all together in one
'				string.
'
' Parameters:	None
'
' Returns:		String of all selected categories to be used in the search call
'
' Notes :		None
' -----------------------------------------------------------------------------
	Function GetCategorySearchQuery()

		'get main group
		Dim mgSum, iMainGroupListCount
		For iMainGroupListCount = 1 to Request.Form("MainGroupListBox").Count
			if (Request.Form("MainGroupListBox")(iMainGroupListCount) = "-1") then
				mgSum = -1
				exit for
			end if
			mgSum = mgSum & "+" & Request.Form("MainGroupListBox")(iMainGroupListCount)
		Next
		if mgSum = "" then 
			mgSum = -1
		end if
				
		'get sub group
		Dim sgSum, iSubGroupListCount
		For iSubGroupListCount = 1 to Request.Form("SubGroupListBox").Count
			if (Request.Form("SubGroupListBox")(iSubGroupListCount) = "-1") then
				sgSum = -1
				exit for
			end if
			sgSum = sgSum & "+" & Request.Form("SubGroupListBox")(iSubGroupListCount)
		Next
		if sgSum = "" then 
			sgSum = -1
		end if
				
		'get main categories and belonging sub categories, and put together to total categorystring
		Dim scSum, mcscTotal,iMCCount, iSCCount, tmpArray
		mcscTotal = ""
   		'loop through all different main categories 
   		For iMCCount = 1 to Request.Form("MainCategoryListBox").Count
   			scSum = null
   			'if main category is -1 (all is selected), there is no need to check for other values or for sub categories
   			if (Request.Form("MainCategoryListBox")(iMCCount)= "-1") then
   				mcscTotal = mgSum & ";" & sgSum & ";-1;-1"
			 	exit for
			else
			'for each main category find corresponding sub categories
				if Request.Form("SubCategoryListBox") <> "" then			   
   					For iSCCount = 1 to Request.Form("SubCategoryListBox").Count
   			 			tmpArray = Split(Request.Form("SubCategoryListBox")(iSCCount), "_")
   			 		
   			 			if tmpArray(0) = Request.Form("MainCategoryListBox")(iMCCount)then
   			 				scSum = scSum & "+" & tmpArray(1)
			 			end if
   			 		Next
			 		mcscTotal = mcscTotal & mgSum & ";" & sgSum & ";+" & Request.Form("MainCategoryListBox")(iMCCount) & ";"  & scSum & ";"
			 	end if
			end if
		Next
		if mcscTotal = "" then 
			mcscTotal = mgSum & ";" & sgSum & ";-1;-1"
		end if
		
		GetCategorySearchQuery = mcscTotal		
	End Function
%>