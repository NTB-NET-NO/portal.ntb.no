<%
'***********************************************************************************
'* DefaultOrgPage.asp
'*
'* This file is used to get the default page of the organization or to save the
'* userprofile as the organizations default page
'*
'* USES:
'*		NTB_profile_lib.asp - For profilefunctions that search in the database
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.05.13
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.05.13
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->

<%
Dim sDefault

sDefault = Request("default")

Dim oOrg, idOrg
Dim oProfile, userid

'get current userprofile
Set oProfile = GetUserProfileByLoginName(Session("UserName"))

'set the id of the updater
userid = oProfile.GeneralInfo.user_id

'get orgprofile for current user		
idOrg = oProfile.AccountInfo.org_id
Set oOrg = GetCompanyProfile(idOrg)


if sDefault = "new" then
'save a new default site
	oOrg.GeneralInfo.default_page = oProfile.GeneralInfo.WindowStyle _
						& "^" & oProfile.GeneralInfo.NewsInfo
	
	'update profilesystem-information
	oOrg.ProfileSystem.date_last_changed = Now()
	oOrg.GeneralInfo.user_id_changed_by = userid	
	
	'update the profile
	Call oOrg.Update()
					
elseif sDefault = "back" then
'save the default site as personal site
	Dim tmpArray
	
	if oOrg.GeneralInfo.default_page <> "" then
	'get the organizations default page
		tmpArray = Split(oOrg.GeneralInfo.default_page,"^",-1)
		
		oProfile.GeneralInfo.WindowStyle = tmpArray(0)
		oProfile.GeneralInfo.NewsInfo = tmpArray(1)
		
	else
	'get the NTB default page
		'get orgprofile for NTB
		Set oOrg = GetCompanyProfileByName("Norsk Telegrambyrå")
		
		tmpArray = Split(oOrg.GeneralInfo.default_page,"^",-1)
		
		oProfile.GeneralInfo.WindowStyle = tmpArray(0)
		oProfile.GeneralInfo.NewsInfo = tmpArray(1)
	
	end if
	
	'update the profile
	Call oProfile.Update()
	
end if

'redirect to the select site
Response.Redirect "Personalization_choose_window.asp"
%>

