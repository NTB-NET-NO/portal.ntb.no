<%
'***********************************************************************************
'* Personalization_choose_style.asp
'*
'* This file is used for give the user various options regarding
'* how the personalized page should look like (how many windows and how to order them).
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of titlebar.
'*
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Solveig Skjermo, Andersen 
'* CREATED DATE: 2002.04.16
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.27
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->

<%

Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

%>

<html>
<head>
	<link href="<%=style%>" type=text/css rel=stylesheet>
	<%= TitleTag() %>
</head>
   
<BODY topmargin="0" leftmargin="0" >
    <TABLE width=100% border="0" CELLSPACING="0" CELLPADDING="0">
    <TR VALIGN="TOP">
            <TD colspan="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
        </TR>
	<TR VALIGN="TOP">
		<TD ALIGN="LEFT"  bgcolor="#003084">
			<!-- #INCLUDE FILE="../template/leftmenu.asp" -->
		</TD>
		<TD ALIGN="LEFT">&#160; </TD>
		<td align="left" width=100%">
				<%=RenderTitleBar("Personalisering av din egen side", "NoPhotolink", 620)%>
				<form action="Personalization_choose_window.asp" method="post" name="personalization1">
				<table width="600" cellspacing="1" cellpadding="3" border="0">
					<tr>
						<td colspan=2 height=30><div class="news11">P� denne siden har du mulighet til � velge hvor mange vinduer
							du vil ha p� din personaliserte side. �nsker du mer informasjon om hvordan f� mest mulig ut av din
							personaliserte side, se v�r 
							<a href="HelpSite.asp?anchor=personalization">hjelpeside</A>. 
							</div>
						</td>
					</tr>
					<tr><td>&nbsp</td><tr>
					<tr>
						<td valign="top "align="center" height=80>
							<table border="0">
								<tr>
								<td><input type="radio" id="perradio1" name="perradio" value="1" checked></td>
								<td><img SRC="../images/option1.jpg" align="right" WIDTH="84" HEIGHT="69"></td>
								</tr>
								<tr>
								<td><input type="radio" id="perradio2" name="perradio" value="2"></td>
								<td><img SRC="../images/option2.jpg" align="right" WIDTH="84" HEIGHT="69"></td>
								</tr>
								<tr>
								<td><input type="radio" id="perradio3" name="perradio" value="3"></td>
								<td><img SRC="../images/option3.jpg" align="right" WIDTH="84" HEIGHT="69"></td>
								</tr>
								<tr>
								<td><input type="radio" id="perradio4" name="perradio" value="4"></td>
								<td><img SRC="../images/option4.jpg" align="right" WIDTH="84" HEIGHT="69"></td>
								</tr>
							</table>
						</td>
						<td valign="top" align="center">
							<table border="0">
								<tr>
								<td><input type="radio" id="perradio5" name="perradio" value="5"></td>
								<td><img SRC="../images/option5.jpg" align="right" WIDTH="84" HEIGHT="69"></td>
								</tr>
								<tr>
								<td><input type="radio" id="perradio6" name="perradio" value="6"></td>
								<td><img SRC="../images/option6.jpg" align="right" WIDTH="84" HEIGHT="69"></td>
								</tr>
								<tr>
								<td><input type="radio" id="perradio7" name="perradio" value="7"></td>
								<td><img SRC="../images/option7.jpg" align="right" WIDTH="84" HEIGHT="69"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td>&nbsp</td><tr>
					<tr>
						<td align="center" colspan="3" align="_top">
							<input class="formbutton" type="submit" value="Lagre stilvalg" id="saveStyle" name="saveStyle" style="background:#bfccd9 none; color:#003366; width:100px">
						</td>
					</tr>
				</table>
				</form>
            </td>
            
        </tr>
    </table>
	
    
</body>
</html>
