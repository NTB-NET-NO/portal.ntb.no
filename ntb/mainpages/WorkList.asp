<%
'***********************************************************************************
' WorkList.asp
'
' This file is used for displaying all worklists and NTB logs.
'
' USES:
'		NTB_layout_lib.asp - For rendering of iframes and titlebar.
' NOTES:
'		none
'
' CREATED BY: Vegar , Andersen
' CREATED DATE: 2002.07.09
' REVISION HISTORY:
' 2003.03.19: Roar Vestre, NTB: Added NTB Logs (Logger) only for NTB-employees
'
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->

<HTML>
<HEAD>
	<link href="../include/ntb.css" type=text/css rel=stylesheet>
	<%= TitleTag() %>
<script language="JavaScript">
//function to open news items in a seperate window
	var myWindow;

	function openList(type, offset)
	{
	  var myURL;
	  myURL = '../mainpages/ViewWorkList.asp?type=' + type +'&offset='+ offset;
	  if(myWindow && !myWindow.closed)
	  { // window is open
	    myWindow.location = myURL;
	    myWindow.focus();
	  }
	  else
	  {
	    leftPlacement = screen.width - 610; //ensures that the window pops up to the right independent on screen resolution
	    myName = 'PopupPage';
	    myOptions = 'height=600,width=600,scrollbars=yes,resizable=yes,top=80, left='+ leftPlacement;
	    myWindow = window.open(myURL, myName, myOptions);
	    myWindow.focus();
	  }
	}
</script>
</HEAD>

<%

dim tmpToday, formatedToday, tmpTomorrow, formatedTomorrow
tmpToday = date
tmpTomorrow = DateAdd("d", 1, tmpToday)
formatedToday = FormatDate(tmpToday)
formatedTomorrow = FormatDate(tmpTomorrow)

Function Format2 (inDate)
	Dim tmpDate, tmpDay, tmpMonth
	tmpDate = CDate(inDate)
	tmpDay = DatePart("D", tmpDate)
	if CInt(tmpDay) < 10 then
	'add zero as prefix to days 1-9
		tmpDay = "0" & tmpDay
	end if
	tmpMonth = DatePart("M", tmpDate)
	if CInt(tmpMonth) < 10 then
	'add zero as prefix to months 1-9
		tmpMonth = "0" & tmpMonth
	end if
	Format2 = DatePart("YYYY", tmpDate) & "-" & tmpMonth & "-" & tmpDay
End Function

function GetListTimestamp (group, offset)
	dim strDatePart
	dim strFileName
	dim strDate
	Dim objXmlDoc
	dim objXmlNode

	strDatePart = DateAdd("d", offset, Date)
	strFileName = group & "_" & Format2(strDatePart) & ".xml"

	strFileName = "../../ntb_files/worklists/" & strFileName

	Set objXmlDoc = Server.CreateObject("Msxml2.DOMDocument")
	objXmlDoc.async = False

	if objXmlDoc.Load(Server.MapPath(strFileName)) Then
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		On Error Resume Next
		strDate = objXmlDoc.selectSingleNode("/nitf/head/meta[@name = 'ntb-date']/@content").value
		strDate = objXmlDoc.selectSingleNode("/nitf/head/meta[@name = 'ntb-dato']/@content").value
		on error goto 0
	end if

	GetListTimestamp = strDate '& " " & strFileName

end function

%>

<BODY topmargin="0" leftmargin="0" >
  <TABLE width=100% ALIGN="LEFT" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
    <TR VALIGN="TOP">
      <TD COLSPAN="3"  bgcolor="#003084"><!-- #INCLUDE FILE="../template/topmenu.asp" --></TD>
    </TR>
    <TR VALIGN="TOP">
      <TD ALIGN="LEFT"  bgcolor="#003084">
	  <!-- #INCLUDE FILE="../template/leftmenu.asp" -->
      </TD>
      <TD ALIGN="LEFT">&#160; </TD>
      <TD width="100%" ALIGN="LEFT">
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" width="600">
	    <TR>
	      <TD colspan="3"><%= RenderTitleBar("Arbeidslister", "NoPhotolink", 620) %></TD>
	      <TD></TD>
	    </TR>
	    <TR>
	      <TD>
	      <TABLE bordercolor="#61c7d7" border="1" cellpadding="2" cellspacing="3" width="303">
	        <TR>
	          <TD style="border:none;" class="main_heading2">Dagens arbeidslister &nbsp;&nbsp;&nbsp;</TD>
	        </TR>
	        <TR>
	          <TD class="d" style="border:none;"><%=formatedToday%><HR size="1"></TD>
	        </TR>
	        <TR>
	          <TD class="main_heading2" style="border:none;"><A class="x" href="JavaScript:openList('innenriks', '0')">Innenriks</A><span class="d">&nbsp;&nbsp;&nbsp;&nbsp;(Oppdatert: <%=getlisttimestamp("innenriks",0)%>)</span></TD>
	        </TR>
	        <TR>
	          <TD class="main_heading2" style="border:none;"><A class="x" href="JavaScript:openList('utenriks', '0')">Utenriks</A><span class="d">&nbsp;&nbsp;&nbsp;&nbsp;(Oppdatert: <%=getlisttimestamp("utenriks",0)%>)</span></TD>
	        </TR>
	        <TR>
	          <TD class="main_heading2" style="border:none;"><A class="x" href="JavaScript:openList('sport', '0')">Sport</A><span class="d">&nbsp;&nbsp;&nbsp;&nbsp;(Oppdatert: <%=getlisttimestamp("sport",0)%>)</span></TD>
	        </TR>
	      </TABLE>
	      </TD>
	      <TD align="left">

	      <TABLE bordercolor="#61c7d7" border="1" cellpadding="2" cellspacing="3" width="303">
	        <TR>
	          <TD style="border:none;" class="main_heading2">Morgendagens arbeidslister</TD>
	        </TR>
	        <TR>
	          <TD class="d" style="border:none;"><%=formatedTomorrow%><HR size="1"></TD>
	        </TR>
	        <TR>
	          <TD class="main_heading2" style="border:none;"><A class="x" href="JavaScript:openList('innenriks', '1')">Innenriks</A><span class="d">&nbsp;&nbsp;&nbsp;&nbsp;(Oppdatert: <%=getlisttimestamp("innenriks",1)%>)</span></TD>
	        </TR>
	        <TR>
	          <TD class="main_heading2" style="border:none;"><A class="x" href="JavaScript:openList('utenriks', '1')">Utenriks</A><span class="d">&nbsp;&nbsp;&nbsp;&nbsp;(Oppdatert: <%=getlisttimestamp("utenriks",1)%>)</span></TD>
	        </TR>
	        <TR>
	          <TD class="main_heading2" style="border:none;"><A class="x" href="JavaScript:openList('sport', '1')">Sport</A><span class="d">&nbsp;&nbsp;&nbsp;&nbsp;(Oppdatert: <%=getlisttimestamp("sport",1)%>)</span></TD>
	        </TR>
	      </TABLE>
	      </TD>
	    </TR>

	    <%
	    ' Code for NTB Logs (Logger) only for NTB employees!
	    if Session("OrgName") = "Norsk Telegrambyrå" or Session("OrgName") = "Scanpix" then
	    %>
	    <TR><td><br/></td></TR>
	    <TR>
	      <TD colspan=1 align="left">
		    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
	        <TR>
			<td valign="top"><%=RenderIFrame("logger", "width2", "height1s", "", "r", 1, 0) %></td>
	        </TR>
		      </table>
	      </TD>
	      <TD colspan=1 align="left">
		    <table BORDER="0" CELLSPACING="0" CELLPADDING="0">
	        <TR>
			<td valign="top"><%=RenderIFrame("direkte", "width2", "height1s", "", "r", 1, 0) %></td>
	        </TR>
		      </table>
	      </TD>
	    </TR>
	    <%end if%>
	  </TABLE>
	</TR>
  </TABLE>
</BODY>
</HTML>
<%
' -----------------------------------------------------------------------------
' FormatDate
'
' Description:	This function formats the date to norwegian format
'				The format is DD.MM.YYYY
'
' Parameters:	inDate - date to be formated
'
' Returns:		String with the in date in correct format
'
' Notes :		None
' -----------------------------------------------------------------------------
Function FormatDate(inDate)
	Dim tmpDate, tmpDay, tmpMonth
	tmpDate = CDate(inDate)
	tmpDay = DatePart("D", tmpDate)
	if CInt(tmpDay) < 10 then
	'add zero as prefix to days 1-9
		tmpDay = "0" & tmpDay
	end if
	tmpMonth = DatePart("M", tmpDate)
	if CInt(tmpMonth) < 10 then
	'add zero as prefix to months 1-9
		tmpMonth = "0" & tmpMonth
	end if
	FormatDate = tmpDay & "." & tmpMonth & "." & DatePart("YYYY", tmpDate)
End Function
%>