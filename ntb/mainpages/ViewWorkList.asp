<%
'***********************************************************************************
'* ViewWorkList.asp
'*
'* This file is used for displaying the worklists (innenriks, utenriks and sport)
'*
'* USES:
'*		NTB_layout_lib.asp - For rendering of iframe and titlebar.
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Trond Orrestad, Andersen
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.25
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_cache_lib.asp" -->

<%
' -----------------------------------------------------------------------------
' FormatDate
'
' Description:	This function formats the date to format YYYY-MM-DD
'
' Parameters:	inDate - date to be formated
'
' Returns:		String with the in date in correct format
'
' Notes :		None
' -----------------------------------------------------------------------------
Function FormatDate2(inDate)
	Dim tmpDate, tmpDay, tmpMonth
	tmpDate = CDate(inDate)
	tmpDay = DatePart("D", tmpDate)
	if CInt(tmpDay) < 10 then
	'add zero as prefix to days 1-9
		tmpDay = "0" & tmpDay
	end if
	tmpMonth = DatePart("M", tmpDate)
	if CInt(tmpMonth) < 10 then
	'add zero as prefix to months 1-9
		tmpMonth = "0" & tmpMonth
	end if
	FormatDate2 = DatePart("YYYY", tmpDate) & "-" & tmpMonth & "-" & tmpDay
End Function

'set the right stylesheet
Dim style

'if Session("Browser") = "" then
'	Response.Redirect "../Authfiles/Login.asp"

if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

Dim pm, strType, xmlstr
pm = Request("download")
strType = Request.QueryString("Type")

Dim objXmlDoc

Sub GetWorklistXml
	Dim strFileName
	Dim strDatePart
	Dim dtToday
	Dim intOffset
	Dim intPos1
	Dim intPos2
	Dim intPos3
	Dim strDiv
	Dim objXmlDoc

	intOffset = Request.QueryString("Offset")

	' Debug
	'strType = "utenriks"
	'intOffset = 1

	strDatePart = DateAdd("d", intOffset, Date)
	strFileName = strType & "_" & FormatDate2(strDatePart) & ".xml"

	Session("FileName") = strFileName

	strFileName = "../../ntb_files/worklists/" & strFileName

	Dim objXsltDoc

	Set objXsltDoc = Server.CreateObject("Msxml2.FreeThreadedDOMDocument")
	Set objXmlDoc = Server.CreateObject("Msxml2.DOMDocument")

	objXsltDoc.async = False
	objXsltDoc.Load Server.MapPath("../template/ntb_sound.xsl")

	objXmlDoc.async = False
	objXmlDoc.Load Server.MapPath(strFileName)
	objXmlDoc.createProcessingInstruction "version2", "version2"

	'set Session("xsltWorkList") = objXsltDoc
	set Session("xmlWorkList") = objXmlDoc
	Session("FilePathName") = strFileName

	Response.Write objXmlDoc.transformNode(objXsltDoc)

End Sub

Sub GetXml

	dim strFilePathName
	strFilePathName = Session("FilePathName")

	set objXmlDoc = Session("xmlWorkList")

	'This fix is to clean up changed XML-format by the Msxml2.DOMDocument.xml command:
	xmlstr = replace(objXmlDoc.xml, "<?xml version=""1.0"" standalone=""yes""?>", "<?xml version=""1.0"" encoding=""iso-8859-1"" standalone=""yes""?>", 1, 1)
	xmlstr = replace(xmlstr, vbTab, "")

	if Left(xmlstr,5) = "<?xml" then
		if not pm = "xml" then
			' only display XML instead of file downloading
			Response.ContentType = "text/xml"
			Response.Write xmlstr
		else
			' make the file name to be
			Response.ContentType = "application/octet-stream; extension=""text/plain"""
			Response.AddHeader "Content-Disposition","attachment;filename=""" & Session("FileName") & """"
			Response.Write xmlstr
		end if
	else
		'Response.Write("<HTML><BODY><p>Error accessing article text: <b>" & _
		'xmlstr & " - ID:" & aid & "</b><hr></p></BODY></HTML>")
	end if

	Response.End

End Sub

if pm = "" then

	Dim intAcf

	'set the right bitposition
	if strType = "innenriks" then
		intAcf = Session("ACF") AND 1 'Innenriks bitposisjon
	elseif strType = "utenriks" then
		intAcf = Session("ACF") AND 2 'Utenriks bitposisjon
	elseif strType = "sport" then
		intAcf = Session("ACF") AND 4 'Sport bitposisjon
	end if

	if not (intAcf > 0) then
	'send to errorpage
		Response.Redirect "../error/noauth.asp"
	end if
%>


<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
<link href="<%=style%>" type="text/css" rel="stylesheet">
<%= TitleTag() %>
</HEAD>
<BODY class='viewBody'>

<table>
	<tr><td colspan="2">
		<%GetWorklistXml%>
	</td></tr>
	<tr>
		<FORM METHOD="POST" ACTION="ViewWorkList.asp" id=form1 name=form1>

		<td width="50%" align="right">
			<INPUT class="formbutton" type="button" value="Skriv ut" onClick="window.print();" name="print" style="background:#bfccd9 none; color:#003366; width:100px">
		</td>
		<td width="50%" align="left">
			<INPUT type="hidden" name="aid" value="<%'=CStr(aid)%>">
			<INPUT class="formbutton" type="submit" value="Last ned XML" name="download" style="background:#bfccd9 none; color:#003366; width:100px">

		</td>
		</FORM>
	</tr>
</table>


</BODY>
</HTML>

<%
else

GetXml

end if
%>

