<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<%
' =============================================================================
' opt-out.asp
' Opt out of direct mail.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

'
' QueryStrings: 
'		rcp_email	- recipient email address.  Should exactly match the one used in the mailing.
'		campitem_id	- campaign item id of the mailing for which the user is opting out.  If
'					  the campaign item id is not specified or is set to zero, then the user is
'					  added to the global opt-out list for the site.
'		campitem_name	- (optional) campaign item name (used for display to user only)
'

' prefix for dml item opt-out lists
const OPT_OUT_NAME = "Opt Out"
	
' name of the site-wide opt-out list
const GLOBAL_OPTOUT_LIST_NAME = "Opt Out"

' ListManager Flags
const LM_FLAG_DEFAULT = 0
const LM_FLAG_OPTOUT = 4

' friendly error message to display when things go bad
dim g_OperatorMessage'

' Sub Main
Sub Main()
	On Error Resume Next
	dim rcp_email, campitem_id, campitem_name

	' Get Query Strings
	rcp_email = Request.Querystring("rcp_email")
	campitem_id = Request.QueryString("campitem_id")
	campitem_name = Request.QueryString("campitem_name")
		
	' check for missing email address/ item id
	if rcp_email = "" then 
		g_OperatorMessage = mscsMessageManager.GetMessage("L_NoEmailAddressPassedToPage_ErrorMessage", sLanguage)
		call ErrorOut
	end if

	' do the work of opting-out
	call OptOut(rcp_email, campitem_id)
	if Err.Number <> 0 then call ErrorOut

	g_operatorMessage = "SUCCESS"

	' Got here - were were successful.  Display message
	dim sMsg, sCampitem
	if campitem_id <> "" then
		if campitem_name <> "" then
			sCampitem = campitem_name
		else
			sCampitem = cstr(campitem_id)
		end if
		sMsg = Replace(mscsMessageManager.GetMessage("L_SuccessfulListOptOut_Message", sLanguage), "%1", rcp_email)
		sMsg = Replace(sMsg, "%2", sCampitem)
	else
		sMsg = Replace(mscsMessageManager.GetMessage("L_SuccessfulSiteOptOut_Message", sLanguage), "%1", rcp_email)
	end if
	Response.Write(sMsg)
End Sub


' -----------------------------------------------------------------------------
' OptOut
'
' Description:
'	This is where the real work of opting out is done
'
' Parameters:
'	rcp_email			- recipient email
'	campitem_id			- (optional) campaign item id (NULL = site wide opt-out)
'
' Returns:
'
' Notes :
'   Uses ADO for direct access to data
' -----------------------------------------------------------------------------
Sub OptOut(rcp_email, campitem_id)
	dim listmanager, list_id, conn_campaigns, campaigns_connstr

	On Error Resume Next
		
	g_OperatorMessage = mscsMessageManager.GetMessage("L_GetConnectString_ErrorMessage", sLanguage)
	
	' get connection strings from Site Config
	campaigns_connstr = dictConfig.s_CampaignsConnectionString
	if Err.number <> 0 then call ErrorOut

	g_OperatorMessage = mscsMessageManager.GetMessage("L_ConnectToCampaignsDB_ErrorMessage", sLanguage)

	' open DB connection
	set conn_campaigns = CreateObject("ADODB.Connection")
	conn_campaigns.open campaigns_connstr
	if Err.number <> 0 then call ErrorOut

	g_OperatorMessage = mscsMessageManager.GetMessage("L_ListManagerInitialization_ErrorMessage", sLanguage)

	' create and initialize the ListManager
	set listmanager = CreateObject("Commerce.ListManager")
	listmanager.Initialize(campaigns_connstr)

	' Get List ID
	list_id = GetOptOutList(conn_campaigns, listmanager, campitem_id)
	if Err.number <> 0 then call ErrorOut
	
	' If the list doesn't exist, create a new one
	if isNull(list_id) then
		list_id = CreateOptOutList(listmanager, campitem_id)
		if Err.number <> 0 then call ErrorOut

		' Set Opt out list
		call SetOptOutList(conn_campaigns, campitem_id, list_id)
		if Err.number <> 0 then call ErrorOut
	end if
		
	' add user to the list
	g_OperatorMessage = mscsMessageManager.GetMessage("L_AddUser_ErrorMessage", sLanguage)
	listmanager.AddUserToMailingList CStr(list_id), rcp_email 
	if Err.number <> 0 then call ErrorOut
End Sub


' -----------------------------------------------------------------------------
' ErrorOut
'
' Description:
'	Display error message, log it to the event log, and exit
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub ErrorOut()
	dim sDescription
	sDescription = Err.Source & ": 0x" & hex(err.number) & " " & Err.Description
	on error resume next
		
	' in production mode, we display only a friendly FAILED message to the user
	' in development, we show details about the failure
	' in both modes we log the event
	if MSCSENV = PRODUCTION then
		Response.Write (mscsMessageManager.GetMessage("L_FriendlyFailed_ErrorMessage", sLanguage))
	else
		Response.Write (g_OperatorMessage)
		if Err.Number <> 0 then
			Response.Write ("<P>")
			Response.Write (sDescription)
		end if
	end if

	' LogNTEvent
	dim evtlog
	set evtlog = CreateObject("Commerce.AdminEventLog")
	call evtlog.WriteErrorEvent(Application("MSCSCommerceSiteName"), "Direct Mail Opt-Out.asp: " & g_OperatorMessage & vbcrlf & sDescription)

	' halt processing of this ASP
	Response.End
End Sub


' -----------------------------------------------------------------------------
' GetOptOutList
'
' Description:
'	Get the list id of the opt-out list associated with the
'	campaign item or NULL if there is no opt-out list yet
'	associated with the campaign item.
'
' Parameters:
'	conn_campaigns		- connection to the campaigns DB
'	listmanager			- initialized listmanager object
'	campitem_id			- campaign item id of the direct mail item
' Returns:
'	List ID (GUID)
'
' Notes :
'   Uses ADO to access data directly
' -----------------------------------------------------------------------------
Function GetOptOutList(conn_campaigns, listmanager, campitem_id)
	Dim sSQL, rs

	g_OperatorMessage = mscsMessageManager.GetMessage("L_GetListId_ErrorMessage", sLanguage)
	
	if campitem_id <> "" then
		sSQL = "SELECT g_dmitem_optout FROM dm_item WHERE i_campitem_id = " & campitem_id
		set rs = CreateObject("ADODB.Recordset")
		set rs.ActiveConnection = conn_campaigns
		rs.Open sSQL
		
		if rs.EOF then
			g_OperatorMessage = mscsMessageManager.GetMessage("L_DirectMailCampaignItemNotFound_ErrorMessage", sLanguage)
			Err.Raise &H80004005, mscsMessageManager.GetMessage("L_DirectMailCampaignItemNotFound_ErrorMessage", sLanguage)
		end if

		GetOptOutList = rs(0)
		rs.Close
	else
		on error resume next
		GetOptOutList = listmanager.GetListId(GLOBAL_OPTOUT_LIST_NAME)
		' $$ TODO: the following error isn't the right code.  this is bug 13583
		if err.number = &H80010105 or GetOptOutList = "" then
			err.clear
			GetOptOutList = null
		end if
	end if
End Function

	
' -----------------------------------------------------------------------------
' SetOptOutList
'
' Description:
'	Set the opt-out list ID for the campaign item
'
' Parameters:
'	conn_campaigns		- connection to the campaigns DB
'	campitem_id			- campaign item id of the direct mail item
'	list_id				- id of the opt-out list (a GUID string)' Returns:
'
' Notes :
'   Uses ADO to access data directly
' -----------------------------------------------------------------------------
Function SetOptOutList(conn_campaigns, campitem_id, list_id)
	dim sSQL
	
	g_OperatorMessage = mscsMessageManager.GetMessage("L_SetListId_ErrorMessage", sLanguage)

	' do nothing if this is the global opt-out list
	if (campitem_id <> "") then
		sSQL = "UPDATE dm_item SET g_dmitem_optout = '" & list_id & "' WHERE i_campitem_id = " & campitem_id
		conn_campaigns.Execute sSQL
	end if
End Function


' -----------------------------------------------------------------------------
' CreateOptOutList
'
' Description:
'	Create a new opt-out list
'
' Parameters:
'	listmanager			- initialized listmanager object
'	campitem_id			- campaign item id of the direct mail item
'
' Returns:
'		List id (GUID)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function CreateOptOutList(listmanager, campitem_id)
	dim sListName, sListDescription, lFlags
	
	g_OperatorMessage = mscsMessageManager.GetMessage("L_CreateList_ErrorMessage", sLanguage)
	if (campitem_id <> "") then
		sListName = OPT_OUT_NAME & " (" & campitem_id & ")"
		sListDescription = mscsMessageManager.GetMessage("L_ListDescription_Message", sLanguage) & campitem_id
	else
		sListName = GLOBAL_OPTOUT_LIST_NAME
		sListDescription = mscsMessageManager.GetMessage("L_GlobalOptoutListDescription_Message", sLanguage)
	end if

	lFlags = LM_FLAG_DEFAULT + LM_FLAG_OPTOUT

	CreateOptOutList = listmanager.CreateEmpty(sListName, sListDescription, lFlags, 0)
End Function
%>