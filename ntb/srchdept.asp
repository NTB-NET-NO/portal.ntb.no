<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' srchdept.asp
' Search department page
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim CatalogSetID
	Dim arrData, sItemSeparator

	Call EnsureAccess()

	htmPageContent = Null
	If bIsSearchDeptPageCached(htmPageContent, CatalogSetID) Then
		'Nothing left to do, everything's in place
	Else
		arrData = arrSearchDept(sItemSeparator)
		htmPageContent = htmRenderSearchDeptPageContent(arrData, sItemSeparator)
		Call CacheFragment("SearchDeptPageCache", CatalogSetID, htmPageContent)
	End If
End Sub


' -----------------------------------------------------------------------------
' bIsSearchDeptPageCached
'
' Description:
'	Check whether this page has already been generated and cached; if it is,
'	return its contents in htmCachedPage and the catalog set ID in
'	iCatalogSetID
'
' Parameters:
'	htmCachedPage			- if the page is cached, it gets stored in here
'	iCatalogSetID			- will contain the ID of the catalog set used
'
' Returns:
'	True	- success (yes, it was cached)
'	False	- failure (no, it wasn't cached)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bIsSearchDeptPageCached(ByRef htmCachedPage, ByRef iCatalogSetID)
	iCatalogSetID = mscsUserCatalogsetID()
	htmCachedPage = LookupCachedFragment("SearchDeptPageCache", iCatalogSetID)
	bIsSearchDeptPageCached = (htmCachedPage <> "")
End Function


' -----------------------------------------------------------------------------
' arrSearchDept
'
' Description:
'	Returns array of strings; every line contains the catalog name, an item
'	separator ("%%%%") and a category name
'
' Parameters:
'	sItemSeparator			- item separator, filled in by this function;
'								this item separator must be passed on to the
'								rendering function
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function arrSearchDept(ByRef sItemSeparator)
	Dim rsCatalogs, rsCategory
	Dim bAtLeastOneCategoryFoundInAllCatalogs
	Dim sCatalog, sCategoryName
	Dim oCatalog
	Dim sResult
	
	Const LINESEPARATOR = "%%%%" 'unlikely to be found in catalog/category name
	
	' Initialize
	sResult=""
	sItemSeparator = "::::" 'unlikely to be found in catalog/category name
	Set rsCatalogs = mscsUserCatalogs()
	bAtLeastOneCategoryFoundInAllCatalogs = False
	
	' Build list of catalogname+categoryname lines in sResult
	Do While Not rsCatalogs.EOF
		bAtLeastOneCategoryFoundInAllCatalogs = True
		sCatalog = rsCatalogs.Fields(CATALOG_NAME_PROPERTY_NAME).Value

		Set oCatalog = GetCatalogForUser(sCatalog, m_UserID)
		If Not oCatalog Is Nothing Then
			' Retrieve Searchable Categories for the catalog
			Set rsCategory = oCatalog.SearchableCategories
				
			' Iterate the searchable categories
			Do While Not rsCategory.EOF
				sCategoryName = rsCategory.Fields("Name").Value  'Note that the category name is held under Name, not CategoryName as you may expect
				sResult = sResult & sCatalog & sItemSeparator & sCategoryName & LINESEPARATOR
				rsCategory.MoveNext
			Loop
			
			rsCategory.Close
			Set rsCategory = Nothing
		End If
		rsCatalogs.MoveNext
	Loop
	' Chop off final line separator
	Set rsCatalogs = Nothing
	If Len(sResult) > Len(LINESEPARATOR) Then
		sResult = Left(sResult, Len(sResult)-Len(LINESEPARATOR))
	End If
	arrSearchDept = Split(sResult, LINESEPARATOR)
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderSearchDeptPageContent
'
' Description:
'   Renders the content from the array generated by arrSearchDept()
'
' Parameters:
'	arrData			- the input array of items
'	sItemSeparator	- the separator string that separates catalog name
'						from category name in every element of arrData
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderSearchDeptPageContent(arrData, sItemSeparator)
	Dim htmTitle, htmContent, htmLinkText
	Dim iIterator
	Dim sCatalogName, sCategoryName, sCurrentCatalogName
	Dim arrLine
	Dim url
	Dim bAtLeastOneCategoryFoundInAllCatalogs
	
	' Initialize
	sCatalogName = ""
	sCategoryName = ""
	sCurrentCatalogName = ""
	htmContent = ""
	sPageTitle = mscsMessageManager.GetMessage("L_AdvancedSearch_HTMLTitle", sLanguage)
	bAtLeastOneCategoryFoundInAllCatalogs = (UBound(arrData) <> 0)
	
	For iIterator = LBound(arrData) To UBound(arrData)
		arrLine = Split(arrData(iIterator), sItemSeparator)
		sCatalogName = arrLine(0)
		sCategoryName = arrLine(1)
		
		If (sCatalogName <> sCurrentCatalogName) Then
			sCurrentCatalogName = sCatalogName
			htmContent = htmContent & RenderText(sCatalogName, MSCSSiteStyle.Body) & CR
		End If
		
		url = GenerateURL(MSCSSitePages.StepSearch, Array(CATALOG_NAME_URL_KEY, DEF_NAME_URL_KEY), Array(sCatalogName, sCategoryName))
		htmLinkText = RenderText(sCategoryName, MSCSSiteStyle.Body)
		htmContent = htmContent & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
						RenderLink(url, htmLinkText, MSCSSiteStyle.Link) & CR
	Next
	
	If (bAtLeastOneCategoryFoundInAllCatalogs = True) Then
		htmContent = RenderText(mscsMessageManager.GetMessage("L_STEPSEARCH_SELECT_CATEGORY_TEXT", sLanguage), _
						MSCSSiteStyle.Body) & CRLF & htmContent
	Else
		htmContent = RenderText(mscsMessageManager.GetMessage("L_STEPSEARCH_NO_DEPARTMENT_TEXT", sLanguage), _
						MSCSSiteStyle.Body)
	End If

	htmTitle = RenderText(mscsMessageManager.GetMessage("L_Advanced_Search_Page_HTMLTitle", sLanguage), _
		MSCSSiteStyle.Title) & CRLF

	htmRenderSearchDeptPageContent = htmTitle & htmContent
End Function
%>
