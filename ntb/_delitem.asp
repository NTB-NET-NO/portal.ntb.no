<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/analysis.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<%
' =============================================================================
' _dellitem.asp
' Tween page for deleting single item from the user's basket.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim objItem, mscsOrderGrp
    Dim sCatalogName, sCategoryName, sProductID, sVariantID
    Dim sOrderFormName, iItemIndex, iItemCount, iErrorLevel
    
	Call EnsureAccess()

    sCatalogName = GetRequestString(CATALOG_NAME_URL_KEY, Null)
    sCategoryName = GetRequestString(CATEGORY_NAME_URL_KEY, "")
    sProductID = GetRequestString(PRODUCT_ID_URL_KEY, Null)
    sVariantID = GetRequestString(VARIANT_ID_URL_KEY, Null)
    
    If IsNull(sCatalogName) Or IsNull(sProductID) Then
        Response.Redirect(GenerateURL(MSCSSitePages.BadURL, Array(), Array()))
    End If

    sOrderFormName = GetRequestString(ORDERFORM_NAME, "Default")

	Set mscsOrderGrp = LoadBasket(m_UserID)

    if (Not IsNull(mscsOrderGrp.Value.OrderForms(sOrderFormName))) Then
    	iItemCount = mscsOrderGrp.Value.OrderForms(sOrderFormName).Items.Count    	
    End If
    If iItemCount = 0 Then
        Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array())) 
    End If 
    
    iItemIndex = MSCSAppFrameWork.RequestNumber(PO_ITEMINDEX_URL_KEY, Null, 0, iItemCount - 1)
        
    If IsNull(iItemIndex) Then
        Response.Redirect(GenerateURL(MSCSSitePages.BadURL, Array(), Array())) 
    End If

	Set objItem = mscsOrderGrp.GetItemInfo(iItemIndex, sOrderFormName)
	Set objItem = Nothing

	Call mscsOrderGrp.RemoveItem(iItemIndex, sOrderFormName)

	Call mscsOrderGrp.SaveAsBasket()
	Call Analysis_LogRemoveFromBasket( _
	                                     sCatalogName, _
	                                     sCategoryName, _
	                                     sProductID, _
	                                     sVariantID _
	                                 )
	Call Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))     
End Sub
%>
