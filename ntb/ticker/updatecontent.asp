<SCRIPT LANGUAGE=VBScript RUNAT=Server>
Sub updateTickerContent
Dim rs, mm, trr, msgs, tcn ' ticker rows
Application("lastmin") = Minute(Now)
Set rs = Server.CreateObject("ADODB.Recordset")
Set msgs = Server.CreateObject("ADODB.Stream")
rs.CursorLocation = 3
rs.Open "getTickers",Application("cns"), 0
trr = rs.RecordCount
msgs.Mode = adModeReadWrite
msgs.Type = 2
msgs.LineSeparator = -1 'adCRLF
msgs.Open
msgs.WriteText "!!LedSign ticker script",1
if not rs.EOF then
'update tickface
' process returned records
msgs.WriteText "!!Ticker has " & CStr(trr) & " messages.",1
Dim url,s
while not rs.EOF
Dim mark
s=CStr(rs.Fields("MessageText"))
mark = instr(1,s,"/*U:")
if mark > 0 then
mark = mark+4
url = Mid(s,mark,instr(1,s,"*/")- mark)
end if
if url <> "" then
msgs.WriteText "ScrollLeft URL=" & url & " text=" & rs.Fields("MessageText"),1
msgs.WriteText "Sleep delay=2000 URL=" & url,1
else
msgs.WriteText "ScrollLeft text=" & rs.Fields("MessageText"), 1
msgs.WriteText "Sleep delay=2000", 1
end if
rs.MoveNext
wend
msgs.WriteText "Reload",1
end if
rs.close
set rs = nothing
msgs.Position = 0
if Application("vt") = 1 then
Application("tbuffer2") = msgs.ReadText()
else
Application("tbuffer1") = msgs.ReadText()
End if
msgs.Close
set msgs = Nothing
if Application("vt") = 1 then
	if trr > 0 then
	Application("tickface2") = Application("tmarkup")
	else
	Application("tickface2") = ""
	end if
Application("vt") = 2
else
	if trr > 0 then
	Application("tickface1") = Application("tmarkup")
	else
	Application("tickface1") = ""
	end if
Application("vt") = 1
End if
if trr > 0 then
	Application("showticker") = trr
	else
	Application("showticker") = 0
End if
End sub
</SCRIPT>
