<%@ Language=VBScript %>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Ralfs Standard Edition">
<META http-equiv="REFRESH" Content="60">
</HEAD>
<BODY>

<P><FONT face=Arial>This sample 
demonstrates basic ticker functionality. If there are messages to display then 
they are displayed. If nothing is in queue then static text "No messages" will 
appear. To add ticker message go to <A 
href="http://devserver1/backdoor">http://devserver1/backdoor</A> and add one. To change 
letter colors add \w or \g or \b before letters. Default     
            
             color is red. </FONT></P>

<P><FONT face=Arial>Files in this sample do all the job, 
this could be imlemented as binary components as well.</FONT></P>
<P>
<TABLE cellSpacing=1 cellPadding=1 width="100%" border=1>
  <TR>
    <TD colspan="2" align="middle">Some static content goes there</TD>
  </TR>
  <TR>
    <TD colspan="2">
<%
If Application("showticker") > 0 then ' there is something to show
	if Application("vt") = 1 then
	Response.Write(Application("tickface1"))
	else
	Response.Write(Application("tickface2"))
	end if
else
	Response.Write("No messages")
End if
Dim cp, allnews, ticks
set cp = Application("ListCache")
%>		
</TD>
    </TR>
  <TR>
    <TD width="15%">Menu</TD>
    <TD>
<TABLE WIDTH="100%" BORDER="1">
<TR>
<TD valign="top">
<%

Set allnews = cp.GetCache("simplelists")
Response.Write(allnews("outer24"))    
%>
    </TD>
    <TD>
<%
Response.Write(allnews("local24"))    
%>
    </TD>
    <TD valign="top">
<%
Response.Write(allnews("sport24"))    
%>
    </TD>
    </TR>
    </TABLE>
</TD>
</TR>
</TABLE>
</P>
</BODY>
</HTML>
<%
'set s = cp.GetCache("categories")("maingroups")
'for each d in s
'Response.Write(d & " - " & s(d) & "<br>")
'next

'set s = cp.GetCache("categories")("subgroups")
'for each d in s
'Response.Write(d & " - " & s(d) & "<br>")
'next

'set s = cp.GetCache("categories")("geo_global")
'for each d in s
'Response.Write(d & " - " & s(d) & "<br>")
'next

'set s = cp.GetCache("categories")("geo_local")
'for each d in s
'Response.Write(d & " - " & s(d) & "<br>")
'next

'set s = cp.GetCache("categories")("cats")
'for each d in s
'Response.Write("<TR><TD>" & d & " - " & s(d) & "</TD><TD>")
'set u = cp.GetCache("categories")("subcats")(s(d))
'for each x in u
'Response.Write(u(x) & ", ")
'next
'Response.Write("</TD></TR>")
'next
%>
