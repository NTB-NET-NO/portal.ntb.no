<%@ Language=VBScript %>
<!-- #include file="../include/NTB_ticker_lib.asp" -->
<%
'
' This will attempt to get the ticker script
Dim mm
mm = Minute(Now)

if mm <> Application("lastmin") then
	call updateTickerContent()
end if

Response.ContentType = "text/plain"
Response.Expires = 0

if Application("showticker") > 0 then
	if Application("vt") = 1 then
		Response.Write(Application("tbuffer1"))
	else
		Response.Write(Application("tbuffer2"))
	end if
else
	Response.Write(" | | | ")
end if

Response.End
%>
