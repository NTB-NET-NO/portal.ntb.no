<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' Pickship.asp
' Display page for Picking Shipping Method
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim mscsOrderGrp, rsMethods, bCalculateCosts
    Dim bNoMethodsAvailable, sSelectedMethodID
    Dim objContext		'object context necessary for shipping components
    Dim dictAggregate	'dictionary object for shipping methods aggregation

	Call EnsureAccess()
	
	bCalculateCosts = False
	
	If Not bCalculateCosts Then
		' Start: Show shipping options without calculate shipping cost (more performant).
		Call PrepareShippingMethods(mscsOrderGrp, sSelectedMethodID) 	
		htmPageContent = htmCachedShippingMethods()
		If IsNull(htmPageContent) Then
			Set rsMethods = rsGetShippingMethods()
			htmPageContent = htmRenderShippingMethods(mscsOrderGrp, rsMethods)
			Call CacheFragment("StaticSectionsCache", "ShippingMethods", htmPageContent)
		End If
	
		If IsNull(sSelectedMethodID) Then 
			' Set the default shipping method to the first available shipping method.
			htmPageContent = Replace(htmPageContent, "_checked", "_checked CHECKED", 1, 1, vbBinaryCompare) 
		Else
			' Set the default shipping method to method user selected last time.
			htmPageContent = Replace(htmPageContent, sSelectedMethodID & "_checked", sSelectedMethodID & "_checked CHECKED", 1, 1, vbBinaryCompare) 
		End If
		' End: Show shipping options without calculate shipping cost (more performant).
	Else
		' Start: Show shipping options and calculate shipping cost (less performant).
		Call PreparePickShipForm(mscsOrderGrp, objContext, dictAggregate, bNoMethodsAvailable)
		htmPageContent = htmRenderPickshipForm(mscsOrderGrp, objContext, dictAggregate, bNoMethodsAvailable)
		' End: Show shipping options and calculate shipping cost (less performant).
	End If
End Sub


' -----------------------------------------------------------------------------
' PrepareShippingMethods
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareShippingMethods(ByRef mscsOrderGrp, ByRef sSelectedMethodID)
	Dim dictItem
	
	Set mscsOrderGrp = LoadBasket(m_UserID)
	
	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
	
	' Has user specified a shipping method yet?
	Set dictItem = GetAnyLineItem(mscsOrderGrp)
	sSelectedMethodID = dictItem.Value("shipping_method_id")
End Sub


' -----------------------------------------------------------------------------
' htmCachedShippingMethods
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmCachedShippingMethods()
	htmCachedShippingMethods = Null
	htmCachedShippingMethods = LookupCachedFragment("StaticSectionsCache", "ShippingMethods")
End Function


' -----------------------------------------------------------------------------
' rsGetShippingMethods
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetShippingMethods()
	Dim mscsShipMgr, arrCols
	
	Set mscsShipMgr = Server.CreateObject("Commerce.ShippingMethodManager")
	Call mscsShipMgr.Initialize(dictConfig.s_TransactionConfigConnectionString)
	
	arrCols = Array("shipping_method_id", "shipping_method_name", "description")
	
	Set rsGetShippingMethods = mscsShipMgr.GetInstalledMethodList("enabled=1", "shipping_method_name", arrCols)
End Function


' -----------------------------------------------------------------------------
' htmRenderShippingMethods
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderShippingMethods(mscsOrderGrp, rs)
	Dim arrHeader, arrData, htmBody, htmRadioBtn, urlLink, htmLinkText, sBtnText, urlAction, sAttList
	
	sPageTitle = mscsMessageManager.GetMessage("L_ShippingOptions_HTMLTitle", sLanguage)
	htmRenderShippingMethods = RenderText(mscsMessageManager.GetMessage("L_ShippingOptions_HTMLTitle", sLanguage), MSCSSiteStyle.Title) & CRLF
	
	If rs.EOF Then
		' No methods available.
        htmRenderShippingMethods = htmRenderShippingMethods & RenderText(mscsMessageManager.GetMessage("L_SHIPPING_NO_RATES_TEXT", sLanguage), MSCSSiteStyle.Body) & BR
        htmRenderShippingMethods = htmRenderShippingMethods & RenderText(mscsMessageManager.GetMessage("L_SHIPPING_RATES_CONTINUE_TEXT", sLanguage), MSCSSiteStyle.Body) & BR
        
        urlLink = GenerateURL(MSCSSitePages.Catalog, Array(), Array())
        htmLinkText = RenderText(mscsMessageManager.GetMessage("L_Browse_Our_Catalogs_HTMLText", sLanguage), MSCSSiteStyle.Body)
        htmRenderShippingMethods = htmRenderShippingMethods & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)		    
	Else	
		htmRenderShippingMethods = htmRenderShippingMethods & RenderText(mscsMessageManager.GetMessage("L_SHIPPING_RATES_REQ_TEXT", sLanguage), MSCSSiteStyle.Body) & CRLF
		
		arrHeader = Array("", _
			mscsMessageManager.GetMessage("L_ShippingMethod_Name_DisplayName_HTMLText", sLanguage), _
			mscsMessageManager.GetMessage("L_ShippingMethod_Description_DisplayName_HTMLText", sLanguage))

		htmBody = RenderTableHeaderRow(arrHeader, Array(), MSCSSiteStyle.TRCenter)
		
		While Not rs.EOF
			sAttList = MSCSSiteStyle.RadioButton & " " & rs.Fields("shipping_method_id").Value & "_checked"
			htmRadioBtn = RenderRadioButton(SHIPPING_METHOD_URL_KEY, rs.Fields("shipping_method_id").Value, False, sAttList)
			arrData = Array(htmRadioBtn, rs.Fields("shipping_method_name").Value, rs.Fields("description").Value)
			htmBody = htmBody & RenderTableDataRow(arrData, Array(), MSCSSiteStyle.TRLeft)
			rs.MoveNext
		Wend 
		
		htmBody = RenderTable(htmBody, MSCSSiteStyle.BorderedTable) & BR
		
		sBtnText = mscsMessageManager.GetMessage("L_CheckOut_Button", sLanguage)
        htmBody = htmBody & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)
			
        urlAction = GenerateURL(MSCSSitePages.SetShippingMethod, Array(), Array())
        htmRenderShippingMethods = htmRenderShippingMethods & RenderForm(urlAction, htmBody, HTTP_POST)
	End If
End Function




' -----------------------------------------------------------------------------
' sGetDefaultShippingMethod
'
' Description:
'    Returns the default shipping method as a string
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetDefaultShippingMethod(mscsOrderGrp, dictAggregate)
	Dim sMethodID, sFirstShippingMethod
	Dim bDefaultShippingMethodSet
	Dim sOrderFormName
	Dim dictPreview, dictItem
	Dim sDefaultMethodID
	
	bDefaultShippingMethodSet = False
	
	' Lookup the shipping_method_id from any line item
	Set dictItem = GetAnyLineItem(mscsOrderGrp)
	sFirstShippingMethod = dictItem.Value("shipping_method_id")
	
    For Each sMethodID In dictAggregate
		Set dictPreview = dictAggregate.Value(sMethodID)
		If dictPreview.Value(SHIPPING_METHOD_KEY) = sFirstShippingMethod Then
		    sDefaultMethodID = dictPreview.Value(SHIPPING_METHOD_KEY)
            bDefaultShippingMethodSet = True
		End If
    Next

    ' If no default shipping method, choose the first shipping method    
    If Not bDefaultShippingMethodSet Then
		For Each sMethodID In dictAggregate
			Set dictPreview = dictAggregate.Value(sMethodID)
			sDefaultMethodID = dictPreview.Value(SHIPPING_METHOD_KEY)
			Exit For
		Next
    End If
    
    sGetDefaultShippingMethod = sDefaultMethodID
End Function

' -----------------------------------------------------------------------------
' PreparePickShipForm
'
' Description:
'    Do the bulk of the work to prepare the shipping form for display
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PreparePickShipForm(ByRef mscsOrderGrp, ByRef objContext, ByRef dictAggregate, ByRef bNoMethodsAvailable)
	Dim iErrorLevel
	Dim slShipments, oOrderForm, objSCMRT
	Dim arrSplitters
	Dim dictPreviewsPerOrderForm, rsMethods
	Dim sOrderFormName

	bNoMethodsAvailable = False
	Set mscsOrderGrp = LoadBasket(m_UserID)

	Call CatchErrors(mscsOrderGrp)
	
	' Run the basket pipeline before calling shipments preview
	' (this loads in product weights and calculates subtotal, which
	'  may be required by some shipping components).
    iErrorLevel = RunMtsPipeline(MSCSPipelines.Basket, GetPipelineLogFile("Basket"), mscsOrderGrp)

	If iErrorLevel > 1 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
	
	Set rsMethods = rsGetShippingMethods()
	If rsMethods.EOF Then
		bNoMethodsAvailable = True
	End If

    ' Add any context information required by shipping components
    Set objContext = Server.CreateObject("Commerce.Dictionary")
    objContext.TransactionConfigConnectionString = dictConfig.s_TransactionConfigConnectionString  'Required for TableShipping
    Set objContext.CacheManager = Application("MSCSCacheManager")
    
	' Designate how to distinguish (split) the shipments.
	'   Typically, this is by shipping address and shipping method,
	'   but it can also include shipment date, etc
	arrSplitters = Array(SHIPPING_ADDRESS_ID, SHIPPING_METHOD_KEY)		
	
	' Preview each OrderForm
	Set objSCMRT = Server.CreateObject("Commerce.Shipping")
	Set dictPreviewsPerOrderForm = CreateObject("Commerce.Dictionary")
	For Each sOrderFormName in mscsOrderGrp.value(ORDERFORMS)
		Set oOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)

		'oOrderForm.value("_currency_decimal_places") = x ' Can be used to explicitly specify # of decimal places
		oOrderForm.value("_currency_lcid") = 0 ' Tells the pipeline components how to round the currency (0 for server default)
		                
		Set slShipments = objSCMRT.PreviewShipments(oOrderForm, objContext, arrSplitters)
		If slShipments.Count > 0 Then
			Set dictPreviewsPerOrderForm.Value(sOrderFormName) = slShipments(0).Previews
		End If
	Next
	Set slShipments = Nothing
	Set oOrderForm = Nothing
	Set objSCMRT = Nothing
	
	' Aggregate the shipping totals
	Set dictAggregate = dictAggregatePreviews(dictPreviewsPerOrderForm)    
	Set dictPreviewsPerOrderForm = Nothing
End Sub


' -----------------------------------------------------------------------------
' CatchErrors
'
' Description:
'    If you can catch any errors, redirect to the appropriate page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrors(ByVal mscsOrderGrp)
	Dim sOrderFormName, i
	Dim mscsOrderForm

	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
	
	' Remove empty orderforms to prevent basket pipeline from raising pur_noitems purchase error.
	Call RemoveEmptyOrderForms(mscsOrderGrp)
	
	For Each sOrderFormName In mscsOrderGrp.Value.OrderForms
	    Set mscsOrderForm = mscsOrderGrp.Value.OrderForms(sOrderFormName)
	    For i = 0 To mscsOrderForm.Items.Count - 1	        
	        ' Is shipping_address_id set on each line-item?
	        If IsNull(mscsOrderForm.Items(i).Value(SHIPPING_ADDRESS_ID)) Then
				Response.Redirect(GenerateURL(GetAddressPage(), Array(ADDRESS_TYPE), Array(SHIPPING_ADDRESS)))
	        End If
		Next
	Next
End Sub


' -----------------------------------------------------------------------------
' dictAggregatePreviews
'
' Description:
'    Aggregates shipping preview totals across the ordergroup
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictAggregatePreviews(dictPreviewsPerOrderForm)	
	Dim dictAggregate, dictAggregatedPreview, PreviewList, Preview, sOrderFormName
	Set dictAggregatedPreview = CreateObject("Commerce.Dictionary")
	Set dictAggregate = CreateObject("Commerce.Dictionary")
	For Each sOrderFormName In dictPreviewsPerOrderForm
		Set PreviewList = dictPreviewsPerOrderForm.Value(sOrderFormName)
		For Each Preview In PreviewList
			If IsNull(dictAggregate.Value(Preview.Value(SHIPPING_METHOD_KEY))) Then
				Set dictAggregate.Value(Preview.Value(SHIPPING_METHOD_KEY)) = Preview
			Else
				dictAggregate.Value(Preview.Value(SHIPPING_METHOD_KEY)).Value(SHIPPING_TOTAL) = _
					 dictAggregate.Value(Preview.Value(SHIPPING_METHOD_KEY)).Value(SHIPPING_TOTAL) + _
					 Preview.Value(SHIPPING_TOTAL)
			End If
		Next
	Next		
	Set dictAggregatePreviews = dictAggregate
End Function




' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderPickshipForm
'
' Description:
'   Renders the whole page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderPickshipForm(ByVal mscsOrderGrp, ByVal objContext, ByVal dictAggregate, bNoMethodsAvailable)
	Dim sPageTitle, sBtnText
	Dim htmContent, htmForm, htmTitle, htmLinkText
	Dim urlAction, urlLink
	
	sPageTitle = mscsMessageManager.GetMessage("L_ShippingOptions_HTMLTitle", sLanguage)
    htmContent = RenderText(mscsMessageManager.GetMessage("L_SHIPPING_RATES_REQ_TEXT", sLanguage), MSCSSiteStyle.Body) & CRLF

	If bNoMethodsAvailable Then
		' No methods available
        htmContent = RenderText(_
	        mscsMessageManager.GetMessage("L_SHIPPING_NO_RATES_TEXT", sLanguage), MSCSSiteStyle.Body) & CR
        htmContent = htmContent & RenderText(mscsMessageManager.GetMessage("L_SHIPPING_RATES_CONTINUE_TEXT", sLanguage), _
            MSCSSiteStyle.Body) & CR                
        
        urlLink = GenerateURL(MSCSSitePages.Catalog, Array(), Array())
        htmLinkText = RenderText(mscsMessageManager.GetMessage("L_Browse_Our_Catalogs_HTMLText", sLanguage), MSCSSiteStyle.Body)
        htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)		    
    Else
		htmForm = htmRenderShippingOptionsEx(mscsOrderGrp, dictAggregate) & BR

		sBtnText = mscsMessageManager.GetMessage("L_CheckOut_Button", sLanguage)
        htmForm = htmForm & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)
			
        urlAction = GenerateURL(MSCSSitePages.SetShippingMethod, Array(), Array())
        htmContent = htmContent & RenderForm(urlAction, htmForm, HTTP_POST)
    End If
        
    htmTitle = RenderText(mscsMessageManager.GetMessage("L_ShippingOptions_HTMLTitle", sLanguage), _
        MSCSSiteStyle.Title) & CRLF
    htmRenderPickshipForm = htmTitle & htmContent
End Function


' -----------------------------------------------------------------------------
' htmRenderShippingOptionsEx
'
' Description:
'    Renders the preview part
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderShippingOptionsEx(ByVal mscsOrderGrp, ByVal dictAggregate)
    Dim dictPreview
    Dim arrRows, arrAttLists
    Dim htmBody, htmRadioBtn
    Dim sDefaultMethodID, sMethodID
    
	' Render the table header row
	arrRows = Array("", mscsMessageManager.GetMessage("L_ShippingMethod_Name_DisplayName_HTMLText", sLanguage), _
	    mscsMessageManager.GetMessage("L_ShippingMethod_Description_DisplayName_HTMLText", sLanguage), _
	    mscsMessageManager.GetMessage("L_ShippingMethod_Cost_DisplayName_HTMLText", sLanguage))
	htmBody = RenderTableHeaderRow(arrRows, Array(), MSCSSiteStyle.TRCenter)

   	' Render the table data rows with the default shipping method checked
	sDefaultMethodID = sGetDefaultShippingMethod(mscsOrderGrp, dictAggregate)

	arrAttLists = Array(MSCSSiteStyle.TDCenter, _
		MSCSSiteStyle.TDLeft, _
		MSCSSiteStyle.TDLeft, _
		MSCSSiteStyle.TDRight)			
		
	For Each sMethodID In dictAggregate
		Set dictPreview = dictAggregate.Value(sMethodID)
		If dictPreview.Value("shipping_method_id") = sDefaultMethodID Then
			htmRadioBtn = RenderRadioButton(SHIPPING_METHOD_URL_KEY, dictPreview.Value("shipping_method_id"), True, MSCSSiteStyle.RadioButton)
		Else
			htmRadioBtn = RenderRadioButton(SHIPPING_METHOD_URL_KEY, dictPreview.Value("shipping_method_id"), False, MSCSSiteStyle.RadioButton)
		End If
        arrRows = Array(htmRadioBtn, dictPreview.Value("shipping_method_name"), dictPreview.Value("description"), htmRenderCurrency(dictPreview.Value(SHIPPING_TOTAL)))
		
		htmBody = htmBody & RenderTableDataRow(arrRows, arrAttLists, MSCSSiteStyle.TRLeft)
	Next

	htmRenderShippingOptionsEx = RenderTable(htmBody, MSCSSiteStyle.BorderedTable)
End Function
%>