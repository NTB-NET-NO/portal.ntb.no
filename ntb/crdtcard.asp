<!-- #INCLUDE FILE="include/txheader.asp" -->   
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/form_lib.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/analysis.asp" -->
<!-- #INCLUDE FILE="include/payment.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' Crdtcard.asp
' Input screen for creditcard information when checking out basket
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim bSuccess
	Dim listMethods, listFrmErrs
	Dim dictFldVals, dictFldErrs
	Dim sVerifyWithTotal
	Dim urlAction
	Dim iOrderGroupID, iErrorLevel
	
	Call EnsureAccess()
	
	Call InitializeCreditcardPage(iOrderGroupID, bSuccess, listMethods, listFrmErrs, _
			dictFldVals, dictFldErrs, sVerifyWithTotal, urlAction, iErrorLevel)
			
	htmPageContent = htmRenderCreditCardPage(iOrderGroupID, bSuccess, listMethods, listFrmErrs, _
			dictFldVals, dictFldErrs, sVerifyWithTotal, urlAction, iErrorLevel)
End Sub
	

' -----------------------------------------------------------------------------
' InitializeCreditcardPage
'
' Description:
'   Initialize the page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitializeCreditcardPage(ByRef iOrderGroupID, ByRef bSuccess, ByRef listMethods, ByRef listFrmErrs, _
			ByRef dictFldVals, ByRef dictFldErrs, ByRef sVerifyWithTotal, _
			ByRef urlAction, ByRef iErrorLevel)
	Dim mscsOrderGrp
	Dim listFlds

	' Initialize variables
	bSuccess = False	
	iErrorLevel = 0
	Set dictFldVals= Nothing
	Set dictFldErrs = Nothing
	Set mscsOrderGrp = LoadBasket(m_UserID)
	
	' Get all payment methods applicable to user.
	Set listMethods = GetPaymentMethodsForUser()
	Set listFlds = Application("MSCSForms").Value("CreditCard")

	Call CatchErrorsForPaymentPages(mscsOrderGrp, listMethods, CREDIT_CARD_PAYMENT_METHOD, sVerifyWithTotal)

	urlAction = GenerateURL(MSCSSitePages.CreditCard, Array("verify_with_total"), Array(sVerifyWithTotal))
	
	If IsFormSubmitted() Then 
		Set dictFldVals = GetSubmittedFieldValues(listFlds)
		iErrorLevel = ValidateSubmittedCreditCardData(mscsOrderGrp, listFlds, dictFldVals, dictFldErrs, listFrmErrs, sVerifyWithTotal)
		If iErrorLevel = 0 Then
			' Validation of submitted data succeeded.
			bSuccess = True
		End If
	End If
		
	If bSuccess Then Call SaveBasketAsOrder(mscsOrderGrp)
	
	If iErrorLevel = 0 Then
		Set dictFldVals = GetDefaultValuesForCreditCardFields(mscsOrderGrp, listFlds)
	End If
		
	' User has more than one payment method available to him.
	If listMethods.Count > 1 Then
		' Remove the active payment method from the list. 
		Call listMethods.Delete(GetListItemIndex(CREDIT_CARD_PAYMENT_METHOD, listMethods))
	End If
	
	iOrderGroupID = mscsOrderGrp.Value(ORDERGROUP_ID)
	
	' Clean up
	Set listFlds = Nothing
	Set mscsOrderGrp = Nothing
End Sub


' -----------------------------------------------------------------------------
' ValidateSubmittedCreditCardData
'
' Description:
'   Validates creditcard data (if you couldn't guess)
'
' Parameters:
'
' Returns:
'   0 = no errors detected.
'   1 = one or more field errors detected.
'   2 = one or more form errors detected.
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function ValidateSubmittedCreditCardData(mscsOrderGrp, listFlds, dictFldVals, dictFldErrs, listFrmErrs, sVerifyWithTotal)
	Dim iErrorLevel, iPipeErrorLevel, mscsOrderForm
	
	iErrorLevel = 0
	
	' Perform field-level validation.
 	Set dictFldErrs = GetFieldsErrorDictionary(listFlds, dictFldVals)

	If dictFldErrs.Count > 0 Then
		iErrorLevel = 1
	Else
		' Perform form-level validation.
		Call SetCreditCardInfo(mscsOrderGrp, dictFldVals)

		iPipeErrorLevel = CheckOut(mscsOrderGrp, sVerifyWithTotal)
		If iPipeErrorLevel > 1 Then
			Set mscsOrderForm = GetAnyOrderForm(mscsOrderGrp)
			Set listFrmErrs = mscsOrderForm.Value(PURCHASE_ERRORS)
			iErrorLevel = 2
		End If
	End If
	
	ValidateSubmittedCreditCardData = iErrorLevel
End Function
    

' -----------------------------------------------------------------------------
' SetCreditCardInfo
'
' Description:
'   Set the credit card information on all orderforms.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SetCreditCardInfo(mscsOrderGrp, dictKeys)
	Dim sKey

	Call RemovePurchaseOrderInfo(mscsOrderGrp)

	Call SetKeyOnOrderForms(mscsOrderGrp, PAYMENT_METHOD, CREDIT_CARD_PAYMENT_METHOD)
	For Each sKey In dictKeys
		Call SetKeyOnOrderForms(mscsOrderGrp, sKey, dictKeys.Value(sKey))
	Next
End Sub


' -----------------------------------------------------------------------------
' RemovePurchaseOrderInfo
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RemovePurchaseOrderInfo(mscsOrderGrp)
	Dim listFlds, sOrderFormName, dictFld

	Set listFlds = Application("MSCSForms").Value("PurchaseOrder")
	
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		For Each dictFld In listFlds
			mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName).Value(dictFld.Name) = Null
		Next
	Next
End Function


' -----------------------------------------------------------------------------
' GetDefaultValuesForCreditCardFields
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDefaultValuesForCreditCardFields(mscsOrderGrp, listFlds)
	Dim mscsOrderForm, dictFld, dictFldVals, i

	Set dictFldVals = GetDictionary()
	Set mscsOrderForm = GetAnyOrderForm(mscsOrderGrp)
	
	For Each dictFld In listFlds
		If Not IsNull(mscsOrderForm.Value(dictFld.Name)) Then
			dictFldVals.Value(dictFld.Name) = mscsOrderForm.Value(dictFld.Name)
		End If
	Next

	' Guess the name on card field, if possible.
	If IsNull(dictFldVals.Value(CC_NAME)) Then
		dictFldVals.Value(CC_NAME) = GetDefaultValueForNameOnCard(mscsOrderGrp)
	End If
	
	If Application("MSCSEnv") = DEVELOPMENT Then
		dictFldVals.Value(CC_NUMBER) = "4111-1111-1111-1111"
	End If
	
	If IsNull(dictFldVals.Value(CC_TYPE)) Then
		If Application("MSCSEnv") = DEVELOPMENT Then
			dictFldVals.Value(CC_TYPE) = mscsMessageManager.GetMessage("L_VISA_Text", sLanguage)
		Else
			dictFldVals.Value(CC_TYPE) = "-1"
		End If
	End If
	
	If IsNull(dictFldVals.Value(CC_MONTH)) Then
		dictFldVals.Value(CC_MONTH) = "-1"
	End If
	
	If IsNull(dictFldVals.Value(CC_YEAR)) Then
		dictFldVals.Value(CC_YEAR) = "-1"
	End If

	If IsNull(dictFldVals.Value(PREFERRED_BILLING_CURRENCY)) Then
		dictFldVals.Value(PREFERRED_BILLING_CURRENCY) = dictConfig.s_BaseCurrencyCode
	End If
	
	Set GetDefaultValuesForCreditCardFields = dictFldVals
End Function


' -----------------------------------------------------------------------------
' GetDefaultValueForNameOnCard
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDefaultValueForNameOnCard(mscsOrderGrp)
	Dim dictAddress, dictOrderForm
	
	' Assume there is only one billing address per ordergroup...
	Set dictOrderForm = GetAnyOrderForm(mscsOrderGrp)
	Set dictAddress = mscsOrderGrp.GetAddress(dictOrderForm.Value(BILLING_ADDRESS_ID))
	
	GetDefaultValueForNameOnCard = FormatOutput(FULLNAME_TEMPLATE, Array(dictAddress.Value(LAST_NAME), dictAddress.Value(FIRST_NAME)))
	Set dictOrderForm = Nothing
	Set dictAddress = Nothing
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderCreditCardPage
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCreditCardPage(ByVal iOrderGroupID, ByVal bSuccess, ByVal listMethods, ByVal listFrmErrs, _
			ByVal dictFldVals, ByVal dictFldErrs, ByVal sVerifyWithTotal, _
			ByVal urlAction, ByVal iErrorLevel)
	Dim arrParams, arrParamVals
	Dim htmContent, htmTitle
	Dim sErr
	
	htmContent = ""
	
	If bSuccess Then
	    arrParams = Array(ORDER_ID)
	    arrParamVals = Array(iOrderGroupID) 'mscsOrderGrp.Value(ORDERGROUP_ID)
	    Response.Redirect(GenerateURL(MSCSSitePages.Confirm, arrParams, arrParamVals))
	End If
		
	' User has payment method(s) available to him.
	If listMethods.Count > 0 Then
		htmTitle = htmRenderPaymentOptionsForUser(listMethods, sVerifyWithTotal)
	End If

	If iErrorLevel = 2 Then
		For Each sErr In listFrmErrs
			htmContent = htmContent & RenderText(sErr, MSCSSiteStyle.Warning) & BR
		Next
	End If
		
	sPageTitle = mscsMessageManager.GetMessage("L_CreditCard_HTMLTitle", sLanguage)
	htmContent = htmContent & htmRenderFillOutForm(urlAction, "CreditCard", dictFldVals, dictFldErrs)
	htmTitle = htmTitle & CRLF & RenderText(mscsMessageManager.GetMessage("L_CreditCard_HTMLTitle", sLanguage), MSCSSiteStyle.Title)
	htmRenderCreditCardPage = htmTitle & CRLF & htmContent
End Function
%>
