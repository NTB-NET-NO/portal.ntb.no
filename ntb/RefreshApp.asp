<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/global_forms_lib.asp" -->
<!-- #INCLUDE FILE="include/global_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/global_siteterms_lib.asp" -->
<!-- #INCLUDE FILE="include/global_data_lib.asp" -->
<!-- #INCLUDE FILE="include/global_siteconfig_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<%
'*****************************************************************************
'*                                                                          
'*   RefreshApp.asp                                                              
'*   Microsoft Commerce Server v4.00                                        
'*                                                                          
'*   Copyright (c) 2000    Microsoft Corporation.  All rights reserved.     
'*                                                                          
'*****************************************************************************

'Const SCHEMA_CATALOG = "Profile Definitions"

Dim bSuccess
Dim bRefreshflg
Dim sReturnTo
Dim sError

Dim sIndex
Dim dictConfig
Dim MSCSProfileService
Dim MSCSProfileServiceUsesActiveDirectory
Dim MSCSProfileProperties
Dim MSCSPageProperties
Dim MSCSCommerceSiteName
Dim MSCSAppConfig	
Dim MSCSActiveDirectoryDomain

'On Error Resume Next
On Error Resume Next

sReturnTo = Request.QueryString("return_to") 
sIndex    = Request.QueryString("sIndex") 
MSCSCommerceSiteName = Application("MSCSCommerceSiteName")
Application.Lock()
Call ProfileRefresh() 'Call to refresh profile service
Application.UnLock()

'Check for Error
If Err.Number  <> 0 Then
  Response.Write Err.Description 
  Response.Redirect sReturnTo & "?bRefreshed=False" & "&sIndex=" & sIndex
Else
  Response.Redirect sReturnTo & "?bRefreshed=True" & "&sIndex=" & sIndex
End If 


'------------------------------------------------------------------------
'ProfileMain is called from Refresh profile Service from the Bizdesk page
'------------------------------------------------------------------------ 
Sub ProfileRefresh()

 	Set MSCSAppConfig = Server.CreateObject("Commerce.AppConfig")
	Call MSCSAppConfig.Initialize(MSCSCommerceSiteName)	
   
	Set dictConfig = MSCSAppConfig.GetOptionsDictionary("")  	    
    Set MSCSProfileService = InitProfileService()
	
    Rem Check if Active Directory data store is supported
	MSCSProfileServiceUsesActiveDirectory = IsActiveDirectoryPresent()		

    Rem Get the active directory domain name
    If MSCSProfileServiceUsesActiveDirectory Then
        MSCSActiveDirectoryDomain = sGetActiveDirectoryDefaultNamingContext()
    End If
    	
	Rem Get the dictionary for all the profiles and properties
	Set MSCSProfileProperties = dictGetProfileSchemaForAllProfileTypes(MSCSProfileService)
	
    Set Application("MSCSProfileService") = MSCSProfileService 
	
	Application("MSCSProfileServiceUsesActiveDirectory") = MSCSProfileServiceUsesActiveDirectory
	If MSCSProfileServiceUsesActiveDirectory Then
        Application("MSCSActiveDirectoryDomain") = MSCSActiveDirectoryDomain
    End If        
    Set Application("MSCSProfileProperties") = MSCSProfileProperties
End Sub        


On Error Goto 0
%>