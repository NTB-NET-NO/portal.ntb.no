<!-- #INCLUDE FILE="../include/header.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/form_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../template/no_menu.asp" -->
<%
' =============================================================================
' newuser.asp
' Sign up new user; check username and password for rules.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim urlAction, dictFldVals, dictFldErrs
	
	' Registration page not available under certain situations.
	If bRegistrationPageNotAvailable() Then
		Response.Redirect(GenerateURL(MSCSSitePages.Home, Array(), Array()))
	End If

	Call PrepareNewUserPage(urlAction, dictFldVals, dictFldErrs)
	htmPageContent = htmRenderNewUserPage(urlAction, dictFldVals, dictFldErrs, sLanguage)
End Sub


' -----------------------------------------------------------------------------
' PrepareNewUserPage
'
' Description:
'	If a form was submitted, validate it and register the user; if invalid,
'	prepare to re-draw the new user form. If no form was submitted,  prepare
'	to draw the new user form.
'
' Parameters: (all parameters are ByRef, thus OUT)
'	urlAction					- Action parameter of New User form
'	dictFldVals					- Dictionary with field values
'	dictFldErrs					- Dictionary with field errors
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareNewUserPage(ByRef urlAction, ByRef dictFldVals, ByRef dictFldErrs)
	Dim listFlds, listFrmErrs
	Dim iErrorLevel, bSuccess, sErr
	Dim sUserID, mscsLoginUser, mscsGuestUser, urlLink, bMustTransferProfile

	' Initialize variables
	bSuccess = False
	bMustTransferProfile = False
	iErrorLevel = 0
	
	Set dictFldVals = Nothing
	Set dictFldErrs = Nothing
	Set listFlds = Application("MSCSForms").Value("Registration")
		
	urlAction = GenerateURL(MSCSSitePages.NewUser, Array(), Array())
		
	If IsFormSubmitted() Then 
		Set dictFldVals = GetSubmittedFieldValues(listFlds)
		iErrorLevel = iValidateSubmittedRegistrationData(listFlds, dictFldVals, dictFldErrs, listFrmErrs)
		' Validation of submitted data succeeded if no errors
		If iErrorLevel = 0 Then bSuccess = True
	End If

	If bSuccess Then					
		If m_UserType = GUEST_USER Then
			Set mscsGuestUser = GetCurrentUserProfile()
				
			If Not mscsGuestUser Is Nothing Then
				If mscsGuestUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, PROFILE_TYPE)).Value = GUEST_PROFILE Then
					bMustTransferProfile = True
				End If
			End If
		End If
				
		If bMustTransferProfile Then
			Set mscsLoginUser = mscsTransferGuestUserProfile(dictFldVals.Value(LOGON_NAME), dictFldVals.Value(LOGON_PASSWORD))
		Else
			Set mscsLoginUser = mscsNewRegisteredUserProfile(dictFldVals.Value(LOGON_NAME), dictFldVals.Value(LOGON_PASSWORD))
		End If
			
		sUserID = mscsLoginUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value
		
		If Request.Cookies("MSCS2000TestCookie") <> "" Then
			' If the user agent allows session cookies, issue auth ticket in a session cookie.
			Call mscsAuthMgr.SetAuthTicket(sUserID, WRITE_TICKET_TO_COOKIE, dictConfig.i_FormLoginTimeOut)
			' Remove the test cookie
			Response.Cookies("MSCS2000TestCookie").Expires = Now - 1
			' Store the ticket in Cookie.
			m_iTicketLocation = 2
			' Set the profile ticket to this userid, so that user is used on the next session 
			' and customer visits are tracked correctly.
			' (we're not concerned about testing permanent cookie support here, because if it isn't enabled, this feature isn't relevant)
			Call mscsAuthMgr.SetProfileTicket(sUserID, WRITE_TICKET_TO_COOKIE)
		Else
			' If the user agent does not allow session cookies, put the ticket in URL.
			Call mscsAuthMgr.SetAuthTicket(sUserID, WRITE_TICKET_TO_URL, dictConfig.i_FormLoginTimeOut)
			' Store the ticket in URL.
			m_iTicketLocation = 1
		End If
					
		Response.Redirect(GenerateURL(MSCSSitePages.Home, Array(), Array()))					
	End If

	If iErrorLevel = 0 Then
		If dictConfig.i_SiteTicketOptions = PUT_TICKET_IN_COOKIE Then
			Call SetTestCookie("session_cookie")
		End If
	End If
End Sub


' -----------------------------------------------------------------------------
' bRegistrationPageNotAvailable
'
' Description:
'	Helper function to determine if the Registration Page is available or not
'
' Parameters:
'
' Returns:
'	Boolean	(True: Page is NOT available; False: Page IS available)
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bRegistrationPageNotAvailable()
	bRegistrationPageNotAvailable = (dictConfig.i_SiteRegistrationOptions = REGISTRATION_NOT_SUPPORTED) Or _
		(dictConfig.i_FormLoginOptions = FORM_LOGIN_NOT_SUPPORTED) Or (dictConfig.i_FormLoginOptions = USE_IIS_AUTH)
End Function


' -----------------------------------------------------------------------------
' iValidateSubmittedRegistrationData
'
' Description:
'	Logs off the user and fills in parameters to pass on to htmRenderLogoffPage 
'
' Parameters:
'	listFlds				- SimpleList object with field names
'	dictFldVals				- Dictionary with field values
'	dictFldErrs				- Dictionary with field errors, in case of bad
'								form input
'	listFrmErrs				- SimpleList object with form errors
'
' Returns:
'	Error Level; 0 if all OK
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function iValidateSubmittedRegistrationData(listFlds, dictFldVals, dictFldErrs, listFrmErrs)
	Dim iErrorLevel, rsUser
	
	iErrorLevel = 0
	
	' Perform field-level validation.
 	Set dictFldErrs = GetFieldsErrorDictionary(listFlds, dictFldVals)

	If dictFldErrs.Count > 0 Then
		iErrorLevel = 1
	Else
		' Does the logon_name already exist?
		' Set rsGetProfile's bForceDBLookUp to True to force a database look-up.
		Set rsUser = rsGetProfile(dictFldVals.Value(LOGON_NAME), PROFILE_TYPE_USER, True)

		If Not rsUser Is Nothing Then
			Set dictFldErrs = GetDictionary()
			dictFldErrs.Value(LOGON_NAME) = mscsMessageManager.GetMessage("L_UserName_In_Use_ErrorMessage", sLanguage)
			iErrorLevel = 1
		End If
	
		If iErrorLevel = 0 Then
			If StrComp(dictFldVals.Value(LOGON_PASSWORD), dictFldVals.Value(CONFIRM_PASSWORD), vbBinaryCompare) <> 0 Then
				Set dictFldErrs = GetDictionary()
				dictFldErrs.Value(CONFIRM_PASSWORD) = mscsMessageManager.GetMessage("L_Bad_Confirm_Password_ErrorMessage", sLanguage)
				iErrorLevel = 1
			End If
		End If
	End If
		
	iValidateSubmittedRegistrationData = iErrorLevel
End Function


' -----------------------------------------------------------------------------
' mscsNewRegisteredUserProfile
'
' Description:
'	Generates new User Profile object, fills it in (i.e. registers the user)
'	and returns it.
'
' Parameters:
'	sLoginName					- Login name of the new user
'	sLoginPassword				- Login password of the new user
'
' Returns:
'	User Profile Object of new user
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsNewRegisteredUserProfile(ByVal sLoginName, ByVal sLoginPassword)
	Dim mscsUser
						
	Set mscsUser = MSCSProfileService.CreateProfile(sLoginName, PROFILE_TYPE_USER)
	mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value = MSCSGenID.GenGUIDString
	mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, LOGON_PASSWORD)).Value = sLoginPassword
	mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, MODIFIED_BY)).Value = sLoginName
	mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, PROFILE_TYPE)).Value = REGISTERED_PROFILE
	
	mscsUser.Fields(GetQualifiedName(ACCOUNT_INFO_GROUP, ACCOUNT_STATUS)).Value = ACCOUNT_ACTIVE
	mscsUser.Fields(GetQualifiedName(ACCOUNT_INFO_GROUP, DATE_REGISTERED)).Value = Now()

	mscsUser.Fields(GetQualifiedName(BUSINESS_DESK_GROUP, FIELD_USER_PARTNERDESK_ROLE)).Value = ROLE_USER
	
	mscsUser.Fields(GetQualifiedName(PROFILE_SYSTEM_GROUP, DATE_CREATED)).Value = Now()
	Call UpdateUserProfile(mscsUser)	'	handles DATE_UPDATED
	Set mscsNewRegisteredUserProfile = mscsUser
End Function					


' -----------------------------------------------------------------------------
' mscsTransferGuestUserProfile
'
' Description:
'	If there's a currently active user, update the data in his User Profile
'	Object with the "new user" data; if there isn't a currently active user,
'	generate a new profile. 
'
' Parameters:
'	sLoginName					- Login name of the new user
'	sLoginPassword				- Login password of the new user
'
' Returns:
'	User Profile Object of new user
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsTransferGuestUserProfile(ByVal sLoginName, ByVal sLoginPassword)
	Dim mscsUser
	
	Set mscsUser = GetCurrentUserProfile()
	If mscsUser Is Nothing Then
		Set mscsUser = mscsNewRegisteredUserProfile(sLoginName, sLoginPassword)
	Else		
		mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, LOGON_NAME)).Value = sLoginName
		mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, LOGON_PASSWORD)).Value = sLoginPassword
		mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, MODIFIED_BY)).Value = sLoginName	
		mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, PROFILE_TYPE)).Value = REGISTERED_PROFILE
		
		mscsUser.Fields(GetQualifiedName(ACCOUNT_INFO_GROUP, ACCOUNT_STATUS)).Value = ACCOUNT_ACTIVE
		mscsUser.Fields(GetQualifiedName(ACCOUNT_INFO_GROUP, DATE_REGISTERED)).Value = Now()

		Call UpdateUserProfile(mscsUser)
	End If
	
	Set mscsTransferGuestUserProfile = mscsUser
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderNewUserPage
'
' Description:
'	Render all page output
'
' Parameters:
'	Identical to PrepareNewUserPage
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderNewUserPage(urlAction, dictFldVals, dictFldErrs, sLanguage)
Dim sPageTitle, htmContent, htmTitle
	sPageTitle = mscsMessageManager.GetMessage("L_Registration_HTMLTitle", sLanguage)	
	htmContent = htmRenderFillOutForm(urlAction, "Registration", dictFldVals, dictFldErrs)
	htmTitle = RenderText(sPageTitle, MSCSSiteStyle.Title) & CRLF
	htmRenderNewUserPage = htmTitle & CRLF & htmContent
End Function
%>
