<!-- #INCLUDE FILE="../include/header.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<%
' =============================================================================
' _guest.asp
' Tweening page to redirect user to homepage as guest rather than as authen-
' ticated user.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim sUserID, rsUser, bCookieAllowed
    
	' If user has an auth ticket, remove it.
	If m_UserAccessType = TICKET_AUTH Then 
		Response.Cookies(AUTH_TICKET_NAME).Expires = Now -1			    	        
	End If

	bCookieAllowed = IsPersistentCookieAllowed()
			
	' Create a profile for new guest user if site privacy option allows profiling.
	Set rsUser = GetNewGuestUserProfile()

	sUserID = sGetUserIDForNewGuestUser(rsUser)
	If bCookieAllowed Then
		Call mscsAuthMgr.SetProfileTicket(sUserID, WRITE_TICKET_TO_COOKIE)	
		m_iTicketLocation = 2
	Else
		Call mscsAuthMgr.SetProfileTicket(sUserID, WRITE_TICKET_TO_URL)
		m_iTicketLocation = 1
	End If
	
	Response.Redirect(GenerateURL(MSCSSitePages.Home, Array(), Array()))
End Sub
%>

