<!-- #INCLUDE FILE="../include/header.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../template/no_menu.asp" -->
<%
' =============================================================================
' logoff.asp
' Log off active user.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim bReEnterWithFormLogin, bReEnterAsGuestOption
	
	Call PrepareLogoffPage(bReEnterWithFormLogin, bReEnterAsGuestOption)
	sPageTitle = mscsMessageManager.GetMessage("L_SignOff_HTMLTitle", sLanguage)
	htmPageContent = htmRenderLogoffPage(bReEnterWithFormLogin, bReEnterAsGuestOption)
End Sub


' -----------------------------------------------------------------------------
' PrepareLogoffPage
'
' Description:
'	Logs off the user and fills in parameters to pass on to htmRenderLogoffPage 
'
' Parameters:
'	bReEnterWithFormLogin		- Present user with option to re-enter with
'									Login form?
'	bReEnterAsGuestOption		- Present user with option to re-enter as
'									Guest?
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareLogoffPage(ByRef bReEnterWithFormLogin, ByRef bReEnterAsGuestOption)
	' Redirec to home page if:
	'	Form login is disabled.
	'	User on this page is not authenticated.
	If (dictConfig.i_FormLoginOptions = FORM_LOGIN_NOT_SUPPORTED) Or _
		(m_UserType <> AUTH_USER) Then
		Response.Redirect(GenerateURL(MSCSSitePages.Home, Array(), Array()))
	End If
	
	' If user has an auth ticket, remove it.
	If m_UserAccessType = TICKET_AUTH Then 
		Response.Cookies(AUTH_TICKET_NAME).Expires = Now -1			    	        
	End If

	' If user has a Webfarm timestamp cookie, remove it.
	If (Application("WebServerCount") > 1) And (m_iTicketLocation <> 1) Then
		Response.Cookies("UserProfileTimeStamp").Expires = Now - 1
	End If
	
	' Users authenticated to IIS cannot re-enter the site using form login.
	If m_UserAccessType <> IIS_AUTH Then
		bReEnterWithFormLogin = True
	End If

	' If site allows deferred login, user has also the option to re-enter as guest visitor.
	If (dictConfig.i_FormLoginOptions <> FORCE_LOGIN_ON_ENTRANCE) And _
		(dictConfig.i_FormLoginOptions <> USE_IIS_AUTH) Then
		bReEnterAsGuestOption = True
	End If
End Sub


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderLogoffPage
'
' Description:
'	Render the Log Off Page.
'
' Parameters:
'	See PrepareLogoffPage.
'
' Returns:
'	String with HTML
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderLogoffPage(ByVal bReEnterWithFormLogin, ByVal bReEnterAsGuestOption)
	Dim htmTitle, htmContent
	Dim iUserType, urlLink, htmLinkText
	
	' Print cautionary statements
	htmContent = RenderText(mscsMessageManager.GetMessage("L_ToEndSession_HTMLText", sLanguage), MSCSSiteStyle.Warning) & CRLF _
		& RenderText(mscsMessageManager.GetMessage("L_CloseYourBrowser_HTMLText", sLanguage), MSCSSiteStyle.Warning) & BR _
		& RenderText(mscsMessageManager.GetMessage("L_ClearBrowserHistory_HTMLText", sLanguage), MSCSSiteStyle.Warning) & CRLF _
		& RenderText(mscsMessageManager.GetMessage("L_EndOfSessionWarning_HTMLText", sLanguage), MSCSSiteStyle.Warning) & CRLF
	
	' Users authenticated to IIS cannot re-enter the site using form login.
	If bReEnterWithFormLogin Then
		' Cannot use GenerateURL since it automatically attaches UserID in URL mode.
		urlLink = mscsAuthMgr.GetURL(MSCSSitePages.Login, True, False, Array(), Array())
		htmLinkText = RenderText(mscsMessageManager.GetMessage("L_AuthVisitOption_HTMLText", sLanguage), MSCSSiteStyle.Body)
		htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & CRLF
	End If
	
	' If site allows deferred login, user has also the option to re-enter as guest visitor.
	If bReEnterAsGuestOption Then
		' Cannot use GenerateURL since it automatically attaches UserID in URL mode.
		urlLink = mscsAuthMgr.GetURL(MSCSSitePages.GuestLogin, True, False, Array(), Array())
		htmLinkText = RenderText(mscsMessageManager.GetMessage("L_GuestVisitOption_HTMLText", sLanguage), MSCSSiteStyle.Body)
		htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
	End If

	htmTitle = RenderText(mscsMessageManager.GetMessage("L_SignOff_HTMLTitle", sLanguage), MSCSSiteStyle.Title) & CRLF
	htmRenderLogoffPage = htmTitle & htmContent
End Function
%>
