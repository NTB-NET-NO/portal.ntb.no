<!-- #INCLUDE FILE="include/txheader.asp" -->   
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/form_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/analysis.asp" -->
<!-- #INCLUDE FILE="include/payment.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' Po.asp
' Input screen for purchase order information when checking out basket
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim mscsOrderGrp, listMethods, listFlds, dictFldVals, dictFldErrs, listFrmErrs
	Dim htmTitle, htmContent, iErrorLevel, urlAction, bSuccess, sVerifyWithTotal
	
	iErrorLevel = 0

	Call EnsureAuthAccess()
	Call InitializePO(mscsOrderGrp, listMethods, sVerifyWithTotal, bSuccess, dictFldVals, dictFldErrs, listFlds, urlAction)
	
    ' Initialize variables
    sPageTitle = mscsMessageManager.GetMessage("L_PurchaseOrder_HTMLTitle", sLanguage)
	
	If IsFormSubmitted() Then 
		Set dictFldVals = GetSubmittedFieldValues(listFlds)
		iErrorLevel = ValidateSubmittedPurchaseOrderData(mscsOrderGrp, listFlds, dictFldVals, dictFldErrs, listFrmErrs, sVerifyWithTotal)
		If iErrorLevel = 0 Then
			' Validation of submitted data succeeded.
			bSuccess = True
		End If
	End If

	If bSuccess Then
		Call RedirectToPOConfirmation(mscsOrderGrp)
	End If

	If iErrorLevel = 0 Then
		Set dictFldVals = GetDefaultValuesForPurchaseOrderFields(mscsOrderGrp, listFlds)
	End If
		
	' User has more than one payment method available to him.
	If listMethods.Count > 1 Then
		' Remove the active payment method from the list. 
		Call listMethods.Delete(GetListItemIndex(PURCHASE_ORDER_PAYMENT_METHOD, listMethods))
	End If

	htmPageContent = htmRenderPaymentOptionsPage(listMethods.Count,urlAction,dictFldVals,dictFldErrs)
	Set listMethods = Nothing
	Call TerminatePO(mscsOrderGrp, listMethods, dictFldVals, dictFldErrs, listFlds) 
End Sub    


' -----------------------------------------------------------------------------
' InitializePO
'
' Description:
'   Initialize main variables for payment options page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitializePO(ByRef mscsOrderGrp, ByRef listMethods, ByRef sVerifyWithTotal, _
				 ByRef bSuccess, ByRef dictFldVals, ByRef dictFldErrs, _
				 ByRef listFlds, ByRef urlAction)
	Set mscsOrderGrp = LoadBasket(m_UserID)
	
	' Get all payment methods applicable to user.
	Set listMethods = GetPaymentMethodsForUser()

	Call CatchErrorsForPaymentPages(mscsOrderGrp, listMethods, PURCHASE_ORDER_PAYMENT_METHOD, sVerifyWithTotal)
	bSuccess = False	
	Set dictFldVals = Nothing
	Set dictFldErrs = Nothing
	
	Set listFlds = Application("MSCSForms").Value("PurchaseOrder")
	urlAction = GenerateURL(MSCSSitePages.PurchaseOrder, Array("verify_with_total"), Array(sVerifyWithTotal))
End Sub


' -----------------------------------------------------------------------------
' TerminatePO
'
' Description:
'   Helper sub to terminate variables; called by Main (actually not necessary
'	in ASP because ASP destroys objects to which no more references exist)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub TerminatePO(ByRef mscsOrderGrp, ByRef listMethods, ByRef dictFldVals, _
				ByRef dictFldErrs, ByRef listFlds)
	Set mscsOrderGrp = Nothing
	Set listMethods = Nothing
	Set dictFldVals = Nothing
	Set dictFldErrs = Nothing
	Set listFlds = Nothing
End Sub


' -----------------------------------------------------------------------------
' ValidateSubmittedPurchaseOrderData
'
' Description:
'
' Parameters:
'
' Returns:
'   0 = no errors detected.
'   1 = one or more field errors detected.
'   2 = one or more form errors detected.
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function ValidateSubmittedPurchaseOrderData(mscsOrderGrp, listFlds, dictFldVals, dictFldErrs, listFrmErrs, ByVal sVerifyWithTotal)
	Dim iErrorLevel, iPipeErrorLevel, mscsOrderForm
	
	iErrorLevel = 0
	
	' Perform field-level validation.
 	Set dictFldErrs = GetFieldsErrorDictionary(listFlds, dictFldVals)

	If dictFldErrs.Count > 0 Then
		iErrorLevel = 1
	Else
		' Perform form-level validation.
		Call SetPurchaseOrderInfo(mscsOrderGrp, dictFldVals)

		iPipeErrorLevel = CheckOut(mscsOrderGrp, sVerifyWithTotal)
		If iPipeErrorLevel > 1 Then
			Set mscsOrderForm = GetAnyOrderForm(mscsOrderGrp)
			Set listFrmErrs = mscsOrderForm.Value(PURCHASE_ERRORS)
			iErrorLevel = 2
		End If
	End If
	
	ValidateSubmittedPurchaseOrderData = iErrorLevel
End Function


' -----------------------------------------------------------------------------
' SetPurchaseOrderInfo
'
' Description:
'   Set the purchase order information on all orderforms.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SetPurchaseOrderInfo(mscsOrderGrp, dictKeys)
	Dim sKey

	' Remove credit card information, if any, from all orderforms.
	Call RemoveCreditCardInfo(mscsOrderGrp)

	Call SetKeyOnOrderForms(mscsOrderGrp, PAYMENT_METHOD, PURCHASE_ORDER_PAYMENT_METHOD)
	For Each sKey In dictKeys
		Call SetKeyOnOrderForms(mscsOrderGrp, sKey, dictKeys.Value(sKey))
	Next
End Sub


' -----------------------------------------------------------------------------
' RemoveCreditCardInfo
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RemoveCreditCardInfo(mscsOrderGrp)
	Dim listFlds, sOrderFormName, dictFld

	Set listFlds = Application("MSCSForms").Value("CreditCard")
	
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		For Each dictFld In listFlds
			mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName).Value(dictFld.Name) = Null
		Next
	Next
End Function


' -----------------------------------------------------------------------------
' GetDefaultValuesForPurchaseOrderFields
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDefaultValuesForPurchaseOrderFields(mscsOrderGrp, listFlds)
	Dim mscsOrderForm, dictFld, dictFldVals, i

	Set dictFldVals = GetDictionary()
	Set mscsOrderForm = GetAnyOrderForm(mscsOrderGrp)
	
	For Each dictFld In listFlds
		If Not IsNull(mscsOrderForm.Value(dictFld.Name)) Then
			dictFldVals.Value(dictFld.Name) = mscsOrderForm.Value(dictFld.Name)
		End If
	Next

	If IsNull(dictFldVals.Value(PREFERRED_BILLING_CURRENCY)) Then
		dictFldVals.Value(PREFERRED_BILLING_CURRENCY) = dictConfig.s_BaseCurrencyCode
	End If
	
	Set GetDefaultValuesForPurchaseOrderFields = dictFldVals
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderPaymentOptionsPage
'
' Description:
'   Render payment options page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderPaymentOptionsPage(iPaymentMethodsCount, urlAction, dictFldVals, dictFldErrs)
	Dim htmContent, htmTitle
	
	If iPaymentMethodsCount>1 Then
		htmTitle = htmRenderPaymentOptionsForUser(listMethods, sVerifyWithTotal)
	End If
	
	htmContent = htmContent & htmRenderFillOutForm(urlAction, "PurchaseOrder", dictFldVals, dictFldErrs)
	htmTitle = htmTitle & CRLF & RenderText(mscsMessageManager.GetMessage("L_PurchaseOrder_HTMLTitle", sLanguage), MSCSSiteStyle.Title)
	htmRenderPaymentOptionsPage = htmTitle & CRLF & htmContent
End Function


' -----------------------------------------------------------------------------
' RedirectToPOConfirmation
'
' Description:
'   Redirect to payment option confirmation page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RedirectToPOConfirmation(mscsOrderGrp)
	Dim arrParams, arrParamVals

	Call SaveBasketAsOrder(mscsOrderGrp)
	    
	arrParams = Array(ORDER_ID)
	arrParamVals = Array(mscsOrderGrp.Value(ORDERGROUP_ID))
	Set mscsOrderGrp = Nothing
	Response.Redirect(GenerateURL(MSCSSitePages.Confirm, arrParams, arrParamVals))
End Sub
%>
