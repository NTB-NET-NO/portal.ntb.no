<%
'---------------------------------------------------------------------
'
'	ReceiveStandard.asp
'
'	Microsoft Biztalk Server 2000
'	Copyright (C) Microsoft Corporation, 1998-2000.  All rights reserved.
'
'	Description: 
'	Sample Biztalk Server / Biztalk Framework 2.0 receive page 
'
'---------------------------------------------------------------------
	Option Explicit
   
	Response.Buffer = True

	Dim interchange
	Dim SubmissionHandle
	Dim PostedDocument
	Dim ContentType
	Dim CharSet
	Dim EntityBody
	Dim Stream
	Dim StartPos
	Dim EndPos

	Set interchange = CreateObject( "BizTalk.Interchange" )

	ContentType = Request.ServerVariables( "CONTENT_TYPE" )

	'
	' Determine request entity body character set (default to us-ascii)
	'
	CharSet = "us-ascii"
	StartPos = InStr( 1, ContentType, "CharSet=""", 1)
	If (StartPos > 0 ) then
		StartPos = StartPos + Len("CharSet=""")
		EndPos = InStr( StartPos, ContentType, """",1 )
		CharSet = Mid (ContentType, StartPos, EndPos - StartPos )
	End if
  
	'
	' Check for multipart MIME message
	'
	PostedDocument = ""

	if ( ContentType = "" or Request.TotalBytes = 0) then
		'
		' Content-Type is required as well as an entity body
		'
		Response.Status = "406 Not Acceptable"
		Response.Write "Content-type or Entity body is missing" & VbCrlf
		Response.Write "Message headers follow below:"  & VbCrlf
		Response.Write Request.ServerVariables("ALL_RAW") & VbCrlf
		Response.End
	else
		if ( InStr( 1,ContentType,"multipart/" ) > 0 ) then
			'
			' MIME multipart message. Build MIME header
			'
			PostedDocument = "MIME-Version: 1.0" & vbCrLf & "Content-Type: " & ContentType & vbCrLf & vbCrLf
			PostedDocument = PostedDocument & "This is a multi-part message in MIME format." & vbCrLf 
		End if
		   
		'
		' Get the post entity body
		'
	
		EntityBody = Request.BinaryRead (Request.TotalBytes )
		   
		'
		' Convert to UNICODE
		'
		Set Stream = Server.CreateObject("AdoDB.Stream")
		Stream.Type = 1						'adTypeBinary
		stream.Open
		Stream.Write EntityBody
		Stream.Position = 0
		Stream.Type = 2						'adTypeText
		Stream.Charset = CharSet
		PostedDocument = PostedDocument & Stream.ReadText
		Stream.Close
		Set Stream = Nothing

		'
		' Submit document asynchronously to Biztalk Server
		'
   
		SubmissionHandle = interchange.submit( 1, PostedDocument )
   
		Set interchange = Nothing
   
		'
		' indicate that the message has been received, but that processing may not be complete
		'
  
		Response.Status = "202 Accepted"
		Response.End
	End if
%>
