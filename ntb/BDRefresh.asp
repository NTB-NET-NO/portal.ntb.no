<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/global_catalog_lib.asp" -->
<%
' =============================================================================
' BDRefresh.asp
' Refresh all caches
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

	Dim sCacheName, sUrlReturnTo, oCacheManager, MSCSCatalogManager

	sCacheName = Request.QueryString("cache_name")
	sUrlReturnTo = Request.QueryString("return_to")

	Set oCacheManager = Application("MSCSCacheManager")

	' Refresh the supplied cachename... 
	If StrComp(sCacheName, "CatalogCache", 1) <> 0 Then
		oCacheManager.RefreshCache(sCacheName)
		
		If sCacheName = "ShippingManagerCache" Then
			' Expire the cached HTML fragment for shipping methods.
			oCacheManager.RefreshCache("StaticSectionsCache")
		End If
			
		Response.Redirect(sUrlReturnTo)
	Else
		' If the cachename is catalog, we refresh all catalog-related
		' cache's in one swoop.  
		On Error Resume Next
			oCacheManager.RefreshCache("ProductListCache")
			oCacheManager.RefreshCache("QueryCatalogInfoCache")
			oCacheManager.RefreshCache("DiscountProductInfo")
			oCacheManager.RefreshCache("DefaultPageCache")
			oCacheManager.RefreshCache("ProductPageCache")
			oCacheManager.RefreshCache("StaticSectionsCache")
			oCacheManager.RefreshCache("SearchDeptPageCache")
			oCacheManager.RefreshCache("StepSearchPageCache")
			oCacheManager.RefreshCache("FTSearchPageCache")
			Set Application("MSCSCatalogSets") = InitCatalogSets()
			
			' Refresh cached property attributes for all catalogs
			Set MSCSCatalogManager = Application("MSCSCatalogManager")
			Set Application("MSCSCatalogAttribs") = dictGetCatalogAttributes(MSCSCatalogManager)

			If Err.Number <> 0 Then
				Response.Write sGetScriptError (Err)
				Err.Clear
			End If
		On Error Goto 0
	End If


' -----------------------------------------------------------------------------
' sGetScriptError
'
' Description:
'	Returns error string populated from error object
'
' Parameters:
'   oErr			Object. VBScript Err object
'
' Returns:
'   String. error string with object properties displayed in an HTML table
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetScriptError(oErr)
	dim sText
	
	const L_ScriptErrorTitle_ErrorMessage = "Script Error:"
	const L_ScriptErrorNumber_ErrorMessage = "Number:"
	const L_ScriptErrorDescription_ErrorMessage = "Description"
	const L_ScriptErrorSource_ErrorMessage = "Source:"
	const L_ScriptErrorHelpFile_ErrorMessage = "Help File:"
	const L_ScriptErrorHelpContext_ErrorMessage = "Help Context:"

	sText = ""
	sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
	sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_ScriptErrorTitle_ErrorMessage
	sText = sText & "<TR><TH>" & L_ScriptErrorNumber_ErrorMessage & "<TD>0x" &		Hex(oErr.number)
	sText = sText & "<TR><TH>" & L_ScriptErrorDescription_ErrorMessage & "<TD>" &	oErr.description
	sText = sText & "<TR><TH>" & L_ScriptErrorSource_ErrorMessage & "<TD>" &		oErr.source
	sText = sText & "<TR><TH>" & L_ScriptErrorHelpFile_ErrorMessage & "<TD>" &		oErr.helpFile
	sText = sText & "<TR><TH>" & L_ScriptErrorHelpContext_ErrorMessage & "<TD>" &	oErr.helpContext
	sText = sText & "</TABLE>"
	sGetScriptError = sText
End Function
%>
