<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/no_menu.asp" -->
<%
' =============================================================================
' welcome.asp
' Welcome message
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim htmTitle, htmContent, htmLinkText, urlLink
	
	sPageTitle = mscsMessageManager.GetMessage("L_Welcome_HTMLTitle", sLanguage)
	
	htmTitle = RenderText(mscsMessageManager.GetMessage("L_Welcome_HTMLTitle", sLanguage), MSCSSiteStyle.Title) 
	
	' Cannot use GenerateURL since it automatically attaches UserID in URL mode.
	urlLink = mscsAuthMgr.GetURL(MSCSSitePages.Login, True, False, Array(), Array())
	htmLinkText = RenderText(mscsMessageManager.GetMessage("L_AuthVisitOption_HTMLText", sLanguage), MSCSSiteStyle.Body)
	htmContent = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & CRLF
	
	' Cannot use GenerateURL since it automatically attaches UserID in URL mode.
	urlLink = mscsAuthMgr.GetURL(MSCSSitePages.GuestLogin, True, False, Array(), Array())
	htmLinkText = RenderText(mscsMessageManager.GetMessage("L_GuestVisitOption_HTMLText", sLanguage), MSCSSiteStyle.Body)
	htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR

	htmPageContent = htmTitle & CRLF & htmContent
End Sub
%>