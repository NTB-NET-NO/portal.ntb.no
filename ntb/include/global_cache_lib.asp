<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_cache_lib.asp
' Global Initialization library with Cache functions; used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitCatalogSets
'
' Description:
'	Initialization of CacheManager for ads, discounts, shipping and tax
'   CacheManager cache's are initialized with the following properties
'     RefreshInterval
'     RetryInterval
'     CacheObjectProgId
'     LoaderProgId
'     LoaderConfig
'     WriterProgId
'     WriterConfig 
'   See docs for more info
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitCacheManager(oEvaluator)
    Dim oCacheManager,dictCampaignConfig,dictTransactionConfig
	Dim dictProductListCacheConfig, dictQCICacheConfig, dictDiscountProductInfoConfig
	Dim dictDefaultPageConfig, dictProductPageConfig, dictStaticSectionsConfig
	Dim dictSearchDeptPageConfig, dictStepSearchPageConfig, dictFTSearchPageConfig
	Dim sMachineBaseURL

	sMachineBaseURL = GetMachineBaseURL()

    ' Create CacheManager object
    Set oCacheManager = Server.CreateObject("Commerce.CacheManager")
    oCacheManager.AppUrl = sMachineBaseUrl

    ' Create the LoaderConfig dictionaries
    Set dictCampaignConfig = GetDictionary()
    dictCampaignConfig("ConnectionString") = dictConfig.s_CampaignsConnectionString
	Set dictCampaignConfig("Evaluator") = oEvaluator

    Set dictTransactionConfig = GetDictionary()
    dictTransactionConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
    
	'**** Begin catalog-related LoaderConfigs ***
   	' Note the first specified catalog-related loaderconfig must specify 
   	' ConnectionString/CacheSize/CacheName/AppUrl for Commerce.LRUCache
   	' (subsequent ones do not, as they piggyback the first in bdrefresh.asp)
   	Set dictProductListCacheConfig = GetDictionary()
    dictProductListCacheConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
    dictProductListCacheConfig("CacheSize") = 10000
    dictProductListCacheConfig("TableName") = "CatalogCache_Virtual_Directory"
    dictProductListCacheConfig("CacheName") = "CatalogCache"
    dictProductListCacheConfig("AppUrl") = sMachineBaseURL

    Set dictQCICacheConfig = Server.CreateObject("Commerce.Dictionary")
    dictQCICacheConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
    dictQCICacheConfig("CacheSize") = 10000
    dictQCICacheConfig("TableName") = "CatalogCache_Virtual_Directory"
    dictQCICacheConfig("CacheName") = "CatalogCache"
    dictQCICacheConfig("AppUrl") = sMachineBaseURL

	Set dictDiscountProductInfoConfig = Server.CreateObject("Commerce.Dictionary")
    dictDiscountProductInfoConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
    dictDiscountProductInfoConfig("CacheSize") = 10000
    dictDiscountProductInfoConfig("TableName") = "CatalogCache_Virtual_Directory"
    dictDiscountProductInfoConfig("CacheName") = "CatalogCache"
    dictDiscountProductInfoConfig("AppUrl") = sMachineBaseURL

	Set dictDefaultPageConfig = Server.CreateObject("Commerce.Dictionary")
    dictDefaultPageConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
    dictDefaultPageConfig("CacheSize") = 10000
    dictDefaultPageConfig("TableName") = "CatalogCache_Virtual_Directory"
    dictDefaultPageConfig("CacheName") = "CatalogCache"
    dictDefaultPageConfig("AppUrl") = sMachineBaseURL

	Set dictProductPageConfig = Server.CreateObject("Commerce.Dictionary")
    dictProductPageConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
    dictProductPageConfig("CacheSize") = 10000
    dictProductPageConfig("TableName") = "CatalogCache_Virtual_Directory"
    dictProductPageConfig("CacheName") = "CatalogCache"
    dictProductPageConfig("AppUrl") = sMachineBaseURL

	Set dictStaticSectionsConfig = Server.CreateObject("Commerce.Dictionary")
    dictStaticSectionsConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
    dictStaticSectionsConfig("CacheSize") = 10000
    dictStaticSectionsConfig("TableName") = "CatalogCache_Virtual_Directory"
    dictStaticSectionsConfig("CacheName") = "CatalogCache"
    dictStaticSectionsConfig("AppUrl") = sMachineBaseURL

	Set dictSearchDeptPageConfig = Server.CreateObject("Commerce.Dictionary")
	dictSearchDeptPageConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
	dictSearchDeptPageConfig("CacheSize") = 10000
	dictSearchDeptPageConfig("TableName") = "CatalogCache_Virtual_Directory"
	dictSearchDeptPageConfig("CacheName") = "CatalogCache"
	dictSearchDeptPageConfig("AppUrl") = sMachineBaseURL

	Set dictStepSearchPageConfig = Server.CreateObject("Commerce.Dictionary")
	dictStepSearchPageConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
	dictStepSearchPageConfig("CacheSize") = 10000
	dictStepSearchPageConfig("TableName") = "CatalogCache_Virtual_Directory"
	dictStepSearchPageConfig("CacheName") = "CatalogCache"
	dictStepSearchPageConfig("AppUrl") = sMachineBaseURL

	Set dictFTSearchPageConfig = Server.CreateObject("Commerce.Dictionary")
	dictFTSearchPageConfig("ConnectionString") = dictConfig.s_TransactionConfigConnectionString
	dictFTSearchPageConfig("CacheSize") = 10000
	dictFTSearchPageConfig("TableName") = "CatalogCache_Virtual_Directory"
	dictFTSearchPageConfig("CacheName") = "CatalogCache"
	dictFTSearchPageConfig("AppUrl") = sMachineBaseURL

	'**** end catalog-related cache configs ***

	' 
	' Create the Caches, inject them with configuration

    ' Configure CacheManager for ads
    oCacheManager.RefreshInterval("advertising") = 5*60
    oCacheManager.RetryInterval("advertising") = 60
    oCacheManager.LoaderProgId("advertising") = "Commerce.CSFLoadAdvertisements"
    oCacheManager.WriterProgId("advertising") = "Commerce.CSFWriteEvents"
    Set oCacheManager.LoaderConfig("advertising") = dictCampaignConfig
    Set oCacheManager.WriterConfig("advertising") = dictCampaignConfig

    ' Configure CacheManager for discounts
    ' Note that the RefreshInterval for discounts is set to zero, which disables automatic
	' cache refreshing.  With this setting, discounts must manually be refreshed using the
	' BizDesk "Production Refresh" Module under Campaigns.  It is set this way to prevent
	' the servers in a web farm from having disparate sets of discounts loaded at any given time.
	' If you aren't using DNS round-robin, you could set the RefreshInterval to a nonzero number
	' so that discounts automatically go into production during background refreshes.
	oCacheManager.RefreshInterval("discounts") = 0
    oCacheManager.RetryInterval("discounts") = 60
    oCacheManager.LoaderProgId("discounts") = "Commerce.CSFLoadDiscounts"
    oCacheManager.WriterProgId("discounts") = "Commerce.CSFWriteEvents"
    Set oCacheManager.LoaderConfig("discounts") = dictCampaignConfig
    Set oCacheManager.WriterConfig("discounts") = dictCampaignConfig
        
    ' Configure CacheManager for Tax
    oCacheManager.RefreshInterval("SampleRegionalTaxCache") = 0
    oCacheManager.RetryInterval("SampleRegionalTaxCache") = 60
    oCacheManager.LoaderProgId("SampleRegionalTaxCache") = "Commerce.SampleRegionalTaxCache"
    Set oCacheManager.LoaderConfig("SampleRegionalTaxCache") = dictTransactionConfig
    
    ' Call GetCache() on the tax cache to ensure that the Business Desk is informed that this app
    '  is caching tax info
    Call oCacheManager.GetCache("SampleRegionalTaxCache")

    ' Configure CacheManager for Shipping
    oCacheManager.RefreshInterval("ShippingManagerCache") = 0
    oCacheManager.RetryInterval("ShippingManagerCache") = 60
    oCacheManager.LoaderProgId("ShippingManagerCache") = "Commerce.ShippingManagerCache"
    Set oCacheManager.LoaderConfig("ShippingManagerCache") = dictTransactionConfig
    
    ' Call GetCache() on the shipping cache to ensure that the Business Desk is informed that this app
    '  is caching shipping info
    Call oCacheManager.GetCache("ShippingManagerCache")

    ' Configure CacheManager For ProductList Fragment Caching
    oCacheManager.RefreshInterval("ProductListCache") = 0
    oCacheManager.RetryInterval("ProductListCache") = 5 * 60
    oCacheManager.CacheObjectProgId("ProductListCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("ProductListCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("ProductListCache") = dictProductListCacheConfig

    ' Call GetCache() on one of the catalog caches to ensure that the Business Desk is informed that this app
    ' is caching catalog info.
    Call oCacheManager.GetCache("ProductListCache")

    ' Configure CacheManager For QueryCatalogInfoCache
    oCacheManager.RefreshInterval("QueryCatalogInfoCache") = 0
    oCacheManager.RetryInterval("QueryCatalogInfoCache") = 5 * 60
    oCacheManager.CacheObjectProgId("QueryCatalogInfoCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("QueryCatalogInfoCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("QueryCatalogInfoCache") = dictQCICacheConfig
	
	' Configure CacheManager For Discounts Product Information
    oCacheManager.RefreshInterval("DiscountProductInfo") = 0
    oCacheManager.RetryInterval("DiscountProductInfo") = 5 * 60
    oCacheManager.CacheObjectProgId("DiscountProductInfo") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("DiscountProductInfo") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("DiscountProductInfo") = dictDiscountProductInfoConfig

	' Configure CacheManager For Default.asp 
    oCacheManager.RefreshInterval("DefaultPageCache") = 0
    oCacheManager.RetryInterval("DefaultPageCache") = 5 * 60
    oCacheManager.CacheObjectProgId("DefaultPageCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("DefaultPageCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("DefaultPageCache") = dictDefaultPageConfig

	' Configure CacheManager For Product.asp
    oCacheManager.RefreshInterval("ProductPageCache") = 0
    oCacheManager.RetryInterval("ProductPageCache") = 5 * 60
    oCacheManager.CacheObjectProgId("ProductPageCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("ProductPageCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("ProductPageCache") = dictProductPageConfig

	' Configure CacheManager For Menu.asp
    oCacheManager.RefreshInterval("StaticSectionsCache") = 0
    oCacheManager.RetryInterval("StaticSectionsCache") = 5 * 60
    oCacheManager.CacheObjectProgId("StaticSectionsCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("StaticSectionsCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("StaticSectionsCache") = dictStaticSectionsConfig
	
	' Configure CacheManager For srchdept.asp 
    oCacheManager.RefreshInterval("SearchDeptPageCache") = 0
    oCacheManager.RetryInterval("SearchDeptPageCache") = 5 * 60
    oCacheManager.CacheObjectProgId("SearchDeptPageCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("SearchDeptPageCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("SearchDeptPageCache") = dictSearchDeptPageConfig

	' Configure CacheManager For stepsrch.asp 
    oCacheManager.RefreshInterval("StepSearchPageCache") = 0
    oCacheManager.RetryInterval("StepSearchPageCache") = 5 * 60
    oCacheManager.CacheObjectProgId("StepSearchPageCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("StepSearchPageCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("StepSearchPageCache") = dictStepSearchPageConfig

	' Configure CacheManager For search.asp 
    oCacheManager.RefreshInterval("FTSearchPageCache") = 0
    oCacheManager.RetryInterval("FTSearchPageCache") = 5 * 60
    oCacheManager.CacheObjectProgId("FTSearchPageCache") = "Commerce.LRUCache"
    oCacheManager.LoaderProgId("FTSearchPageCache") = "Commerce.LRUCacheFlush"
    Set oCacheManager.LoaderConfig("FTSearchPageCache") = dictFTSearchPageConfig

	' Return the CacheManager instance, to be stored in the Application collection
    Set InitCacheManager = oCacheManager
End Function
</SCRIPT>