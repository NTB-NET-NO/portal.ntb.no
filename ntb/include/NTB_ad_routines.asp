<%
' =============================================================================
' ad_routines.asp
' Active Directory routines for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' bPaintACLForUser
'
' Description:
'	Function to paint ACLS for a user in Active Directory data store
'   This is achieved by adding/removing user to/from 
'   <COMPANY_NAME>_ADMINGROUP or <COMPANY_NAME>_USERGROUP
'
' Parameters:
'
' Returns:
'	Boolean: True (Success), False (Failure)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bPaintACLForUser(ByVal sUserCN, ByVal CoID)

	Dim sUserOrgName
	sUserOrgName = GetCompanyName(CoID)
	
	' Construct the full DN for this particular user
	sUserDN = AD_COMMON_NAME_PREFIX & sUserCN & _
	          "," & _
	          AD_SITE_CONTAINER_PREFIX & sUserOrgName & _
			  "," & _
			  AD_SITE_CONTAINER_PREFIX & "NTB_LIVE" & _
			  "," & _
			  AD_CONTAINER_CS40 & _
			  "," & _
			  MSCSActiveDirectoryDomain
            	    		          	
	' Add user to the <COMPANY_NAME>_USERGROUP
	bPaintACLForUser = bAddUserToGroup(sUserOrgName & AD_USER_GROUP_SUFFIX, sUserDN)	

End Function	


' -----------------------------------------------------------------------------
' bAddUserToGroup
'
' Description:
'	Function to add a user to an existing Active Directory group
'
' Parameters:
'	szGroupName					- Name of the group to add user to
'	sUserDN						- DN of User to add to that group
'
' Returns:
'	Boolean: True (Success), False (Failure)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bAddUserToGroup(ByVal szGroupName, ByVal sUserDN)	
	Dim bAdded
	Dim iIndex
	Dim members
	Dim objProfile
	
	bAdded = True
	iIndex = 0
    ' Get the profile for the group
	Set objProfile = rsGetProfile(szGroupName, GROUP_PROFILE, True)
		
	' Get existing members of group
	members = objProfile("GeneralInfo.member").Value	
	' ReDim the members array and append the new value
	If Not IsNull(members) Then
	    If Not IsEmpty(members) Then
	        If UBound(members) >= 0 Then
	            ReDim Preserve members(UBound(members) + 1)
	            members(UBound(members)) = sUserDN
            End If  	            
        End If
	Else
	    ReDim members(iIndex)
	    members(iIndex) = sUserDN
	End If
	
	' Assign the new members array to the property
	objProfile("GeneralInfo.member").value = members	
	
	' Update the Active Directory group profile with new member values
	On Error Resume Next
	    Call objProfile.Update()
	    If Err.number Then
	        bAdded = False
	        Err.Clear
	    End if
	On Error GoTo 0
		
	bAddUserToGroup = bAdded
End Function
%>