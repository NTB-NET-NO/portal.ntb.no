<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_internationalization_lib.asp
' Global Initialization library with Internationalization functions; used by
' global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitAltCurrencyDisp
'
' Description:
'	Initialize display of alternate currency (i.e. UKPound and Euro simulta-
'	neously)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitAltCurrencyDisp()
	Dim mscsAltCurrencyDisp

	Set mscsAltCurrencyDisp = CreateObject("Commerce.EuroDisplay")
	Call mscsAltCurrencyDisp.Initialize( _
	                                      dictConfig.s_BaseCurrencySymbol, _
	                                      dictConfig.i_BaseCurrencyLocale, _
	                                      dictConfig.s_AltCurrencySymbol, _
	                                      dictConfig.i_AltCurrencyLocale, _
	                                      dictConfig.f_AltCurrencyConversionRate _
	                                )
	Set InitAltCurrencyDisp = mscsAltCurrencyDisp
End Function


' -----------------------------------------------------------------------------
' GetCountryNamesList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCountryNamesList()
	Dim sCountry, listCountryNames
	Dim MSCSMessageManager, sLanguage
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage
	
	Set listCountryNames = GetSimpleList()
	Call listCountryNames.Add(MSCSMessageManager.GetMessage("L_SelectOne_Text", sLanguage))
	
	For Each sCountry In Application("MSCSAppConfig").GetCountryNamesList()
		Call listCountryNames.Add(sCountry)
	Next
	
	Set GetCountryNamesList = listCountryNames
End Function


' -----------------------------------------------------------------------------
' GetCountryCodesList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCountryCodesList()
	Dim sCountry, listCountryCodes
	
	Set listCountryCodes = GetSimpleList()
	Call listCountryCodes.Add("-1")
	
	For Each sCountry In Application("MSCSAppConfig").GetCountryNamesList()
		Call listCountryCodes.Add(Application("MSCSAppConfig").GetCountryCodeFromCountryName(sCountry))
	Next
	
	Set GetCountryCodesList = listCountryCodes
End Function


' -----------------------------------------------------------------------------
' GetBillToCurrencyFieldDefinition
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetBillToCurrencyFieldDefinition()
	Dim MSCSMessageManager, sLanguage
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

	Dim dictFld
	
	Set dictFld = GetDictionary()
	dictFld.Name = PREFERRED_BILLING_CURRENCY
	dictFld.Label = MSCSMessageManager.GetMessage("L_BillToCurrency_HTMLText", sLanguage)
	dictFld.Size = 1
	dictFld.InputType = LISTBOX
	Set dictFld.ListItems = GetBillToCurrenciesListItems()
	Set dictFld.ListValues = GetBillToCurrenciesListValues()
	dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BillToCurrency_ErrorMessage", sLanguage)
	dictFld.IsRequired = True    

	Set GetBillToCurrencyFieldDefinition = dictFld
End Function


' -----------------------------------------------------------------------------
' GetBillToCurrenciesListItems
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetBillToCurrenciesListItems()
	Dim listItems
	Dim MSCSMessageManager, sLanguage
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

	Set listItems = GetSimpleList()
	Call listItems.Add(MSCSMessageManager.GetMessage("L_SelectOne_Text", sLanguage))
	Call listItems.Add(dictConfig.s_BaseCurrencySymbol)
	Call listItems.Add(dictConfig.s_AltCurrencySymbol)

    Set GetBillToCurrenciesListItems = listItems
End Function


' -----------------------------------------------------------------------------
' GetBillToCurrenciesListValues
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetBillToCurrenciesListValues()
	Dim listItems
	
	Set listItems = GetSimpleList()
	Call listItems.Add(-1)
	Call listItems.Add(dictConfig.s_BaseCurrencyCode)
	Call listItems.Add(dictConfig.s_AltCurrencyCode)

    Set GetBillToCurrenciesListValues = listItems
End Function
</SCRIPT>