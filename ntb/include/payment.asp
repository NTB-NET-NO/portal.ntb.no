<%
' =============================================================================
' Payment.asp
' Functions for use on payment-related pages (po.asp, crdtcard.asp, 
' dispatch.asp, summary.asp)
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Const iBIZTALKOPENNESS = 1		' where BizTalk will find its connection information:  1=Document, 2=Source, 4=Destination

' -----------------------------------------------------------------------------
' CatchErrorsForPaymentPages
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrorsForPaymentPages(mscsOrderGrp, listMethods, sPaymentMethod, ByRef sVerifyWithTotal)
	Dim sOrderFormName, i, mscsOrderForm

	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
	
	sVerifyWithTotal = GetRequestString("verify_with_total", Null)
	' If order total not submitted as a VerifyWith hidden field then user has not seen the
	'	order summary page yet. 
	If IsNull(sVerifyWithTotal) Then
		Response.Redirect(GenerateURL(MSCSSitePages.OrderSummary, Array(), Array()))
	End If
	
	' Remove empty orderforms to eliminate pur_noitems purchase error.
	Call RemoveEmptyOrderForms(mscsOrderGrp)

	' Can user use the specified payment method?
	Call EnsureSupportForPaymentMethod(listMethods, sPaymentMethod)

	For Each sOrderFormName In mscsOrderGrp.Value.OrderForms
	    Set mscsOrderForm = mscsOrderGrp.Value.OrderForms(sOrderFormName)
	    For i = 0 To mscsOrderForm.Items.Count - 1	        
	        ' Is shipping_address_id set on each line-item?
	        If IsNull(mscsOrderForm.Items(i).Value(SHIPPING_ADDRESS_ID)) Then
				Response.Redirect(GenerateURL(GetAddressPage(), Array(ADDRESS_TYPE), Array(SHIPPING_ADDRESS)))
	        End If
	        
	        ' Is shipping_method_id set on each line-item?
	        If IsNull(mscsOrderForm.Items(i).Value(SHIPPING_METHOD_KEY)) Then
				Response.Redirect(GenerateURL(MSCSSitePages.ShippingMethods, Array(), Array()))
            End If
		Next
		
		' Is billing_address_id set on each orderform?
		If IsNull(mscsOrderForm.Value(BILLING_ADDRESS_ID)) Then
			Response.Redirect(GenerateURL(GetAddressPage(), Array(ADDRESS_TYPE), Array(BILLING_ADDRESS)))
		End If

	Next
End Sub


' -----------------------------------------------------------------------------
' EnsureSupportForPaymentMethod
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub EnsureSupportForPaymentMethod(listMethods, sPaymentMethod)
	Dim urlRedirect

	' There must be at least one applicable payment method.
	If listMethods.Count > 0 Then
		' Is payment method supported for user?
		If Not IsEntityInList(sPaymentMethod, listMethods) Then
			sPaymentMethod = GetDefaultPaymentMethodForUser(listMethods)
			urlRedirect = GenerateURL(GetPaymentPageFromPaymentMethod(sPaymentMethod), Array(), Array())
			Response.Redirect(urlRedirect)
		End If
	Else
		Err.Raise HRESULT_E_FAIL, "Site", mscsMessageManager.GetMessage("L_Bad_User_PaymentOptions_ErrorMessage", sLanguage)
	End If
End Sub


' -----------------------------------------------------------------------------
' CheckOut
'
' Description:
'	Runs basket, total, and checkout pipelines.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function CheckOut(mscsOrderGrp, ByVal sVerifyWithTotal)
	Dim iErrorLevel

	CheckOut = 0

    iErrorLevel = RunMtsPipeline(MSCSPipelines.Basket, GetPipelineLogFile("Basket"), mscsOrderGrp)
	
    If iErrorLevel > 1 Then 
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
    End If     
    
    iErrorLevel = RunMtsPipeline(MSCSPipelines.Total, GetPipelineLogFile("Total"), mscsOrderGrp)
    
    If iErrorLevel > 1 Then 
		Response.Redirect(GenerateURL(MSCSSitePages.OrderSummary, Array(), Array()))
    End If     

    ' If the order total as last seen by the user does not match the order total just
    '	calculated, redirect to the order summary so user can review his order.
    If sVerifyWithTotal <> CStr(mscsOrderGrp.Value(SAVED_TOTAL_TOTAL)) Then
		' Put the order total (as last seen by the user) on the order group and save it.
		mscsOrderGrp.Value("verify_with_total") = sVerifyWithTotal
		Call mscsOrderGrp.SaveAsBasket()
		Response.Redirect(GenerateURL(MSCSSitePages.OrderSummary, Array(), Array()))
	End If
		
    iErrorLevel = RunMtsPipeline(MSCSPipelines.Checkout, GetPipelineLogFile("Checkout"), mscsOrderGrp)

    If iErrorLevel > 1 Then 
        If GetErrorCount(mscsOrderGrp, BASKET_ERRORS) > 0 Then
			Err.Raise vbObjectError + 2111, , mscsMessageManager.GetMessage("L_Bad_Pipeline_Warning_ErrorMessage", sLanguage)
        ElseIf GetErrorCount(mscsOrderGrp, PURCHASE_ERRORS) > 0 Then
			CheckOut = iErrorLevel
        Else
			Err.Raise vbObjectError + 2110, , mscsMessageManager.GetMessage("L_Unspecified_Pipeline_Warning_ErrorMessage", sLanguage)
        End If
    End If     
End Function


' -----------------------------------------------------------------------------
' SaveBasketAsOrder
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveBasketAsOrder(mscsOrderGrp)
	Dim mscsOrderGrpMgr
	
	' Remove any addresses from the orderform which are not referenced
	Call mscsOrderGrp.PurgeUnreferencedAddresses()
	
	Call mscsOrderGrp.SaveAsOrder()

	' Submit appropriate OrderForms using BizTalk
	Call InvokeBizTalk(mscsOrderGrp)

	Set mscsOrderGrpMgr = GetOrderGroupManager()
	Call mscsOrderGrpMgr.DeleteOrderGroupFromDisk(m_UserID)

	Call Analysis_LogSubmitOrder(mscsOrderGrp.Value(ORDERGROUP_ID)) 'Do this last, as it can't be rolled back

End Sub


' -----------------------------------------------------------------------------
' InvokeBizTalk
'
' Description:
'	Submit appropriate OrderForms using BizTalk
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InvokeBizTalk(ByVal mscsOrderGrp)
	Dim sOrderName, mscsOrderForm, oVendor, sXML
	Dim sVendorQual, sVendorQualValue

	If dictConfig.i_BizTalkOptions = BIZTALK_PO_XFER_ENABLED Then
		For Each sOrderName In mscsOrderGrp.Value.OrderForms
			Set mscsOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderName)
			For Each oVendor In mscsOrderForm.value("_vendors")
				If StrComp(oVendor.vendorID, DEFAULT_ORDERFORM, vbTextCompare) <> 0 Then
					sXML = GetXMLForVendorItems(mscsOrderForm, oVendor)
					sVendorQual = mscsOrderForm.items(oVendor.itemindexes(0)).vendor_qual
					sVendorQualValue = mscsOrderForm.items(oVendor.itemindexes(0)).vendor_qual_value					
					Call SubmitUsingBizTalk(sXML, oVendor.vendorID, sVendorQual, sVendorQualValue)
				end if
			Next 
		Next
	End If
End Sub


' -----------------------------------------------------------------------------
' GetXMLForVendorItems
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetXMLForVendorItems(ByVal mscsOrderForm, ByVal oVendor)
	Dim oXMLTransforms, oXMLSchema, oOrderformXML, oXMLDoc
	Dim oNode, oAttribute
	Dim sFilePath

	Set oXMLTransforms = Server.CreateObject("Commerce.DictionaryXMLTransforms")

	sFilePath = Server.MapPath("\" & MSCSAppFrameWork.VirtualDirectory) & "\poschema.xml"
	Set oXMLSchema = oXMLTransforms.GetXMLFromFile(sFilePath) ' Could also be read from WebDAV
	Set oOrderformXML = oXMLTransforms.GenerateXMLForDictionaryUsingSchema(mscsOrderForm, oXMLSchema)

	Set oXMLDoc = Server.CreateObject("MSXML.DOMDOCUMENT")
    oXMLDoc.loadXML oOrderformXML.xml
    
    
    For Each oNode In oXMLDoc.documentElement.childNodes
		If (oNode.nodeName = "Items") Then
            For Each oAttribute In oNode.Attributes
                If (oAttribute.nodeName = "vendorid") Then
                    If Not(oAttribute.nodeValue = oVendor.vendorID) Then
                        oNode.parentNode.removeChild oNode
                    End If
                    Exit For
                Else
                End If
            Next 
        End If
    Next

    GetXMLForVendorItems = oXMLDoc.xml
End Function


' -----------------------------------------------------------------------------
' SubmitUsingBizTalk
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function SubmitUsingBizTalk(ByVal sXML, ByVal sVendorName, ByVal sDestQual, ByVal sDestQualValue)
    Dim sSubmitType
    Dim sDocName, sSourceQualifierID, sSourceQualifierValue, sDestQualifierID, sDestQualifierValue
    Dim oDBConfig, oOrg, sID, sName, sDef, oInterchange, oRes
    
   	sSubmitType = dictConfig.s_BizTalkSubmittypeQueue 
	sDocName = dictConfig.s_BizTalkOrderDocType 

	sSourceQualifierID  = dictConfig.s_BizTalkSourceQualifierID 
	sSourceQualifierValue  = dictConfig.s_BizTalkSourceQualifierValue 

	Set oInterchange = Server.CreateObject("BizTalk.Interchange")
	
	oRes = oInterchange.Submit(iBIZTALKOPENNESS, _
								sXML, _
								sDocName, _
								sSourceQualifierID, _
								sSourceQualifierValue, _
								sDestQual, _
								sDestQualValue) 
End Function


' -----------------------------------------------------------------------------
' GetPaymentMethodsForUser
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetPaymentMethodsForUser()
	Dim listMethods, sPaymentMethod

	Set listMethods = GetSimpleList()

	If m_UserType = GUEST_USER Then
		If IsEntityInList(CREDIT_CARD_PAYMENT_METHOD, Application("MSCSSitePaymentMethods")) Then
			Call listMethods.Add(CREDIT_CARD_PAYMENT_METHOD)
		End If
	ElseIf m_UserType = AUTH_USER Then
		For Each sPaymentMethod In Application("MSCSSitePaymentMethods")
			Call listMethods.Add(sPaymentMethod)
		Next
	End If	

	Set GetPaymentMethodsForUser = listMethods 
End Function


' -----------------------------------------------------------------------------
' GetDefaultPaymentMethodForUser
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDefaultPaymentMethodForUser(listMethods)
	GetDefaultPaymentMethodForUser = listMethods(0)
End Function


' -----------------------------------------------------------------------------
' GetPaymentPageFromPaymentMethod
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetPaymentPageFromPaymentMethod(ByVal sPaymentMethod)
	Select Case sPaymentMethod
		Case CREDIT_CARD_PAYMENT_METHOD 
			GetPaymentPageFromPaymentMethod = MSCSSitePages.CreditCard
		Case PURCHASE_ORDER_PAYMENT_METHOD
			GetPaymentPageFromPaymentMethod = MSCSSitePages.PurchaseOrder
	End Select
End Function


' -----------------------------------------------------------------------------
' GetPaymentMethodDisplayName
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetPaymentMethodDisplayName(ByVal sPaymentMethod)
	Select Case sPaymentMethod
		Case CREDIT_CARD_PAYMENT_METHOD
			GetPaymentMethodDisplayName = mscsMessageManager.GetMessage("L_CreditCard_Payment_DisplayName_Text", sLanguage)
		Case PURCHASE_ORDER_PAYMENT_METHOD
			GetPaymentMethodDisplayName = mscsMessageManager.GetMessage("L_PurchaseOrder_Payment_DisplayName_Text", sLanguage)
	End Select
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderPaymentOptionsForUser
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderPaymentOptionsForUser(listMethods, ByVal sVerifyWithTotal)
	Dim listMethodNames, sMethod, urlAction, sBtnText, htmBody, sSelectedOption, iSize, bMultiple
	
	iSize = 1
	bMultiple = False

	Set listMethodNames = GetSimpleList()
	
	For Each sMethod In listMethods
		Call listMethodNames.Add(GetPaymentMethodDisplayName(sMethod))
	Next

	htmBody = RenderText(FormatOutput(LABEL_TEMPLATE, Array(mscsMessageManager.GetMessage("L_PaymentOptions_HTMLText", sLanguage))), MSCSSiteStyle.Body)
	
	htmBody = htmBody & RenderHiddenField("verify_with_total", sVerifyWithTotal)
	
	sSelectedOption = listMethods(0)
	htmBody = htmBody & RenderListBoxFromSimpleList(PAYMENT_METHOD, listMethodNames, listMethods, sSelectedOption, iSize, bMultiple, MSCSSiteStyle.ListBox)
	
	sBtnText = mscsMessageManager.GetMessage("L_Submit_Button", sLanguage)
	htmBody = htmBody & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button) & "<HR>"
	
	urlAction = GenerateURL("dispatch.asp", Array(), Array())
	htmRenderPaymentOptionsForUser = RenderForm(urlAction, htmBody, HTTP_POST)
End Function
%>
