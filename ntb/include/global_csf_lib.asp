<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_csf_lib.asp
' Global Initialization library with CSF functions; used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitCSF
'
' Description:
'	Initialize the Content Selection Framework
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitCSF(oPipelineMap, oCacheManager, oExpressionEvaluator)
    Dim oPipe
    Dim CSFAdvertisingContext, CSFDiscountContext
    Dim sRedirectUrl

    ' The RedirectURL should be set to the full http path to "redir.asp"
    sRedirectUrl = GetBaseUrl() & "/redir.asp"

    ' Create Global Context for CSF Advertising
    Set CSFAdvertisingContext = CreateObject("Commerce.Dictionary")
    Set CSFAdvertisingContext("CacheManager") = oCacheManager
    CSFAdvertisingContext("CacheName") = "Advertising"
    Set CSFAdvertisingContext("Evaluator") = oExpressionEvaluator
    CSFAdvertisingContext("RedirectUrl") = sRedirectUrl
    Set oPipe = Server.CreateObject("Commerce.OrderPipeline")
    oPipe.LoadPipe(oPipelineMap.Advertising)
    Set CSFAdvertisingContext("Pipeline") = oPipe
    Set Application("CSFAdvertisingContext") = CSFAdvertisingContext

    ' Create Global Context for CSF Discounts
    Set CSFDiscountContext = CreateObject("Commerce.Dictionary")
    Set CSFDiscountContext("CacheManager") = oCacheManager
    CSFDiscountContext("CacheName") = "Discounts"
    Set CSFDiscountContext("Evaluator") = oExpressionEvaluator
    CSFDiscountContext("RedirectUrl") = sRedirectUrl
    Set oPipe = Server.CreateObject("Commerce.OrderPipeline")
    oPipe.LoadPipe(oPipelineMap.Discounts)
    Set CSFDiscountContext("Pipeline") = oPipe
    Set Application("CSFDiscountContext") = CSFDiscountContext

    ' Create an event processing pipeline which we call directly from redir.asp
    Set oPipe = Server.CreateObject("Commerce.OrderPipeline")
    oPipe.LoadPipe(oPipelineMap.RecordEvent)
    Set Application("CampaignsCSFEventPipe") = oPipe
End Sub
</SCRIPT>