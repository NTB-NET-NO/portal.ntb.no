  <%
  
  '***********************************************************************************
' NTB_personal_render_lib.asp
'
' Support functions for rendering the personalized pages
' 
' NOTES:
'		'none'
'
' CREATED BY: Solveig Skjermo 
' CREATED DATE: 2002.05.15
' UPDATED BY: Solveig Skjermo
' UPDATED DATE: 2002.06.11
' REVISION HISTORY:
'		'none'
'
'***********************************************************************************
  
' -----------------------------------------------------------------------------
' RenderPreferredWindow()
'
' Description: This renderfunction determines where the user came from and takes action 
'				accordingly. It calls up RenderPerTable to generate the appropriate 
'				windowstyle.
'
' Parameters:  None
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------
	 
  Function RenderPreferredWindow()
  'A function that draw the right windowstyle based on the users preferences
 
  Dim newsinfo, windowstyle, height
  
  'get the personalization information for the user
  newsinfo = GetNewsInfo(Session("Username"))
  windowstyle = GetWindowStyle(Session("Username"))
  height = Session("LowRes")
  
  if Request.Form("perradio")<> "" then
	'If there exist a perradio then the user came from Personalization_choose_style
	windowstyle = Request.Form("perradio")
	'and it will be saved in the profile
	UpdateWindowStyle(windowstyle)
  end if

  'Make the windowtable
  RenderPreferredWindow = RenderPerTable(windowstyle, newsinfo, height)
  
  End Function
 
' -----------------------------------------------------------------------------
' RenderPerTable(Byval WindowStyle, Byval ProfileNewsInfo, ByVal sHeight)
'
' Description: This renderfunction generates a table that displays the windowstyle
'				with links to the site where the users can personalize their news
'				(Personalization_choose_news.asp)
'
' Parameters:  WindowStyle, the style the user wants the personalized page to look like
'				ProfileNewsInfo, the information about the user already chosen news
'				sHeight	if this is lowres or highres window
'
' Returns:
'
' Notes :	None
'
' -----------------------------------------------------------------------------
 
  Function RenderPerTable(Byval WindowStyle, Byval ProfileNewsInfo, ByVal sHeight)
  'A function that draws a table based on the users preferences
  
  Dim NewsArray, CategoryArray, ColumnID
  Dim i, tmpArray, column, heading
  Dim height, width1, width2
  
  ColumnID = 1
  i = 0
  
  if sHeight = "true" then
	height = 280
	width1 = 240
	width2 = 160
  else
	height = 350
	width1 = 300
	width2 = 200
  end if
  
  RenderPerTable= "<table border='1' borderColor='#61c7d7' height='" & height & "'><tr>"
  
  Select Case WindowStyle
  
  Case "1"
    
	if ProfileNewsInfo <> "" then
	
		'Get categories from ProfileNewsInfo
		NewsArray = Split(ProfileNewsInfo, "#", -1)
		ReDim Preserve NewsArray(2)
		ReDim CategoryArray(2)
		
		For each column in NewsArray
			if Left(column,10) = "TextSearch" then
			'if textsearch then show it
				tmpArray = Split (column, "$", -1)
				
				if Ubound(tmpArray) = 1 then
				'if there are a searchword present
					CategoryArray(i) = "Friteksts�k: " & tmpArray(1)
				else
					CategoryArray(i) = "Friteksts�k"
				end if
			else
			'if not check whether there are written any heading
				tmpArray = Split(column, "$", -1)

				if Ubound(tmpArray)= 1 then
				'if there are more than one entry in the array
					CategoryArray(i) = tmpArray(1)
				elseif Ubound(tmpArray)= 0 then
				'if no header is written - display Kategoris�k
					CategoryArray(i) = "Kategoris�k"
				else
					CategoryArray(i) = "Nytt s�k"
				end if
				
			end if
			'increment the counter
			i = i + 1
		next
		
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 2)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"

			ColumnID = ColumnID + 1	
		end if
		
		'Second column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 2)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
		'	
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
	end if
  
  
  Case "2"
 
	if ProfileNewsInfo <> "" then
	
		'Get categories from ProfileNewsInfo
		NewsArray = Split(ProfileNewsInfo, "#", -1)
		ReDim Preserve NewsArray(3)
		ReDim CategoryArray(3)
		
		For each column in NewsArray
			if Left(column,10) = "TextSearch" then
			'if textsearch then show it
				tmpArray = Split (column, "$", -1)
				
				if Ubound(tmpArray) = 1 then
				'if there are a searchword present
					CategoryArray(i) = "Friteksts�k: " & tmpArray(1)
				else
					CategoryArray(i) = "Friteksts�k"
				end if
			else
			'if not check whether there are written any heading
				tmpArray = Split(column, "$", -1)
				Response.Flush

				if Ubound(tmpArray)= 1 then
				'if there are more than one entry in the array
					CategoryArray(i) = tmpArray(1)
				elseif Ubound(tmpArray)= 0 then
				'if no header is written - display Kategoris�k
					CategoryArray(i) = "Kategoris�k"
				else
					CategoryArray(i) = "Nytt s�k"
				end if
				
			end if
			'increment the counter
			i = i + 1
		next
		
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center rowspan=2 width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"

			ColumnID = ColumnID + 1
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"

			ColumnID = ColumnID + 1
		end if
	end if
  
  
  Case "3"
  
	if ProfileNewsInfo <> "" then
	
		'Get categories from ProfileNewsInfo
		NewsArray = Split(ProfileNewsInfo, "#", -1)
		ReDim Preserve NewsArray(4)
		ReDim CategoryArray(4)

		For each column in NewsArray
			if Left(column,10) = "TextSearch" then
			'if textsearch then show it
				tmpArray = Split (column, "$", -1)
				
				if Ubound(tmpArray) = 1 then
				'if there are a searchword present
					CategoryArray(i) = "Friteksts�k: " & tmpArray(1)
				else
					CategoryArray(i) = "Friteksts�k"
				end if
			else
			'if not check whether there are written any heading
				tmpArray = Split(column, "$", -1)
				Response.Flush

				if Ubound(tmpArray)= 1 then
				'if there are more than one entry in the array
					CategoryArray(i) = tmpArray(1)
				elseif Ubound(tmpArray)= 0 then
				'if no header is written - display Kategoris�k
					CategoryArray(i) = "Kategoris�k"
				else
					CategoryArray(i) = "Nytt s�k"
				end if
				
			end if
			'increment the counter
			i = i + 1
		next
		
		'First column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
		end if
		
		'First column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		''Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width1 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
	end if
  
  
  Case "4"
  
	if ProfileNewsInfo <> "" then
	
		'Get categories from ProfileNewsInfo
		NewsArray = Split(ProfileNewsInfo, "#", -1)
		ReDim Preserve NewsArray(3)
		ReDim CategoryArray(3)

		For each column in NewsArray
			if Left(column,10) = "TextSearch" then
			'if textsearch then show it
				tmpArray = Split (column, "$", -1)
				
				if Ubound(tmpArray) = 1 then
				'if there are a searchword present
					CategoryArray(i) = "Friteksts�k: " & tmpArray(1)
				else
					CategoryArray(i) = "Friteksts�k"
				end if
			else
			'if not check whether there are written any heading
				tmpArray = Split(column, "$", -1)
				Response.Flush
				
				if Ubound(tmpArray)= 1 then
				'if there are more than one entry in the array
					CategoryArray(i) = tmpArray(1)
				elseif Ubound(tmpArray)= 0 then
				'if no header is written - display Kategoris�k
					CategoryArray(i) = "Kategoris�k"
				else
					CategoryArray(i) = "Nytt s�k"
				end if
				
			end if
			'increment the counter
			i = i + 1
		next
		
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Third column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"

			ColumnID = ColumnID + 1
		end if
	end if
  
  
  Case "5"
  
	if ProfileNewsInfo <> "" then
	
		'Get categories from ProfileNewsInfo
		NewsArray = Split(ProfileNewsInfo, "#", -1)
		ReDim Preserve NewsArray(4)
		ReDim CategoryArray(4)

		For each column in NewsArray
			if Left(column,10) = "TextSearch" then
			'if textsearch then show it
				tmpArray = Split (column, "$", -1)
				
				if Ubound(tmpArray) = 1 then
				'if there are a searchword present
					CategoryArray(i) = "Friteksts�k: " & tmpArray(1)
				else
					CategoryArray(i) = "Friteksts�k"
				end if
			else
			'if not check whether there are written any heading
				tmpArray = Split(column, "$", -1)
				Response.Flush
				
				if Ubound(tmpArray)= 1 then
				'if there are more than one entry in the array
					CategoryArray(i) = tmpArray(1)
				elseif Ubound(tmpArray)= 0 then
				'if no header is written - display Kategoris�k
					CategoryArray(i) = "Kategoris�k"
				else
					CategoryArray(i) = "Nytt s�k"
				end if
				
			end if
			'increment the counter
			i = i + 1
		next
		
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center rowspan=2 width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center rowspan=2 width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Third column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Third column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1	
		end if
	end if
  
  
  Case "6"
  
	if ProfileNewsInfo <> "" then
	
		'Get categories from ProfileNewsInfo
		NewsArray = Split(ProfileNewsInfo, "#", -1)
		ReDim Preserve NewsArray(5)
		ReDim CategoryArray(5)

		For each column in NewsArray
			if Left(column,10) = "TextSearch" then
			'if textsearch then show it
				tmpArray = Split (column, "$", -1)
				
				if Ubound(tmpArray) = 1 then
				'if there are a searchword present
					CategoryArray(i) = "Friteksts�k: " & tmpArray(1)
				else
					CategoryArray(i) = "Friteksts�k"
				end if
			else
			'if not check whether there are written any heading
				tmpArray = Split(column, "$", -1)
				Response.Flush

				if Ubound(tmpArray)= 1 then
				'if there are more than one entry in the array
					CategoryArray(i) = tmpArray(1)
				elseif Ubound(tmpArray)= 0 then
				'if no header is written - display Kategoris�k
					CategoryArray(i) = "Kategoris�k"
				else
					CategoryArray(i) = "Nytt s�k"
				end if
				
			end if
			'increment the counter
			i = i + 1
		next
		
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center rowspan=2 width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Third column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Third column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
	end if
  
  
  Case "7"
  
	if ProfileNewsInfo <> "" then
	
		'Get categories from ProfileNewsInfo
		NewsArray = Split(ProfileNewsInfo, "#", -1)
		ReDim Preserve NewsArray(6)
		ReDim CategoryArray(6)

		For each column in NewsArray
			if Left(column,10) = "TextSearch" then
			'if textsearch then show it
				tmpArray = Split (column, "$", -1)
				
				if Ubound(tmpArray) = 1 then
				'if there are a searchword present
					CategoryArray(i) = "Friteksts�k: " & tmpArray(1)
				else
					CategoryArray(i) = "Friteksts�k"
				end if
			else
			'if not check whether there are written any heading
				tmpArray = Split(column, "$", -1)
				Response.Flush

				if Ubound(tmpArray)= 1 then
				'if there are more than one entry in the array
					CategoryArray(i) = tmpArray(1)
				elseif Ubound(tmpArray)= 0 then
				'if no header is written - display Kategoris�k
					CategoryArray(i) = "Kategoris�k"
				else
					CategoryArray(i) = "Nytt s�k"
				end if
				
			end if
			'increment the counter
			i = i + 1
		next
		
		'First column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
							
			ColumnID = ColumnID + 1
		end if
		
		'Third column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
		end if
		
		'First column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
		
		'Third column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPerTable = RenderPerTable & "<td align=center width='" &width2 & "'>"_
							& "<a href=""Personalization_choose_news.asp?ColumnID="_
							& ColumnID & """>"_
							& CategoryArray(ColumnID-1) & "</a></td>"
			
			ColumnID = ColumnID + 1
		end if
	end if
  
  End Select
  
  RenderPerTable = RenderPerTable & "</tr></table>"
  
  End function
  
' -----------------------------------------------------------------------------
' RenderPersonalSite(Byval WindowStyle, Byval ProfileNewsInfo)
'
' Description: This renderfunction generates the users personal site
'
' Parameters:  
'
' Returns:
'
' Notes :	
'
' -----------------------------------------------------------------------------
  
  Function RenderPersonalSite(Byval WindowStyle, Byval ProfileNewsInfo)
  
  Dim CategoryArray, ColumnID
  ColumnID = 1
  RenderPersonalSite = ""
  
	if ProfileNewsInfo <> "" then
	
	'Get categories from ProfileNewsInfo
	CategoryArray = Split(ProfileNewsInfo, "#", -1)

	Select Case WindowStyle
  
	Case "1"
    	'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 2)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_ 
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height1")_
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width2", "height1")_
							& "</td>"
		end if
		
		'Second column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 2)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height1") _
							& "</td>"
	
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width2", "height1") _
							& "</td>"
		end if										
  
	Case "2"
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" rowspan=""2"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height1") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" rowspan=""2"">"_
							& RenderPerIFrame("", ColumnID, "width2", "height1") _
							& "</td>"
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height2") _
							& "</td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame("", ColumnID, "width2", "height2") _
							& "</td>"_
							& "</tr><tr>"
		end if
		
		'Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame("", ColumnID, "width2", "height2") _
							& "</td>"
		end if

	Case "3"
		'First column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame("", ColumnID, "width2", "height2") _
							& "</td>"
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height2") _
							& "</td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width2", "height2") _
							& "</td>"_
							& "</tr><tr>"
		end if
		
		'First column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width2", "height2") _
							& "</td>"
		end if
		
		'Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width2", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame("", ColumnID, "width2", "height2") _
							& "</td>"
		end if								

	Case "4"
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height1") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height1") & "</td>"
		end if
		
		'Second column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height1") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height1") _
							& "</td>"
		end if
		
		'Third column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 3)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height1") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height1") & "</td>"
		end if			
  
	Case "5"
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" rowspan=""2"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height1") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" rowspan=""2"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height1") _
							& "</td>"
		end if
		
		'Second column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" rowspan=""2"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height1") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" rowspan=""2"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height1") _
							& "</td>"
		end if
		
		'Third column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"_
							& "</tr><tr>"
		end if
		
		'Third column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 4)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"								
		end if

	Case "6"
		'First column
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" style=""border:none;""" _
							& " width=""150"" rowspan=""2"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height1") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"" rowspan=""2"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height1") _
							& "</td>"
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if
		
		'Third column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"_
							& "</tr><tr>"
		end if
		
		'Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if
		
		'Third column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 5)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if

	Case "7"
		'First column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=center>"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if
		
		'Second column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
							
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if
		
		'Third column, first row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">" _
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"_
							& "</tr><tr>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"_
							& "</tr><tr>"
		end if
		
		'First column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if
		
		'Second column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if
		
		'Third column, second row
		if ((CategoryArray(ColumnID-1) <> "") AND not(ColumnID > 6)) then
		' if the array still have elements and there are more columns left, 
		' put in categoryinformation
			
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame(CategoryArray(ColumnID-1), ColumnID, "width3", "height2") _
							& "</td>"
			
			ColumnID = ColumnID + 1
			 
		else
			'No more categorization
			RenderPersonalSite = RenderPersonalSite & "<td align=""center"">"_
							& RenderPerIFrame("", ColumnID, "width3", "height2") _
							& "</td>"
		end if	

	End Select
  
  end if	

  End Function
  
' -----------------------------------------------------------------------------
' RenderPerIFrame(ByVal IFrameNews, ByVal SizeTypeWidth, ByVal SizeTypeHeight)
'
' Description: This renderfunction generates the iframe for the users personal news choices
'
' Parameters:  
'
' Returns:
'
' Notes :	
'
' -----------------------------------------------------------------------------

  Function RenderPerIFrame(ByVal IFrameNews, ByVal intPlace, ByVal SizeTypeWidth, ByVal SizeTypeHeight)
  
  'logic for displaying right heading
  Dim tmpArray, header
  
  'set the right header
	if Left(IFrameNews,10) = "TextSearch" then
	'if textsearch then show it as header
		header = "<div class='main_heading2'>Friteksts�k</div>" 
		
		'set the iframe-place
		IFrameNews = IFrameNews & "@" & intPlace
	else
	'if not check whether there are written any heading
		tmpArray = Split(IFrameNews, "$", -1)

		if Ubound(tmpArray)= 1 then
		'if there are more than one entry in the array
			header = "<div class='main_heading2'>" & tmpArray(1) &"</div>" 
			'set the framenews to contain only the category-part
			IFrameNews = tmpArray(0)
		else
		'if no header is written - display Kategoris�k
			header = "<div class='main_heading2'>Kategoris�k</div>" 
		end if
				
	end if

  'logic for displaying frames for different screen resolutions
  Dim defaultHeight1, defaultHeight1s, defaultHeight2 
  
  Dim lowRes 'parameter to indicate low or high screen resolution
	lowRes = False
	if LCase(CStr(Session("LowRes"))) = "true" then
	'a session variable is set on login indicating screen resolution
		lowRes = true
	end if
	
	
	If (lowRes = true) Then
		defaultHeight1 = 321
		defaultHeight1s = 220
		defaultHeight2 = 145
	Else
		defaultHeight1 = 473
		defaultHeight1s = 380
		defaultHeight2 = 221
	End If
	  
  Dim frameHeight
  Dim frameWidth
  Dim sourceFile
  sourceFile = "../template/periframe.asp?newsSel=" & IFrameNews
  
  If (SizeTypeHeight = "height1")then
	frameHeight = defaultHeight1 
  elseIf (SizeTypeHeight = "height1s") Then
		frameHeight = defaultHeight1s 
  else
	frameHeight = defaultHeight2
  End If

  If (SizeTypeWidth = "width2")then
	frameWidth = 291
  else 
	frameWidth = 188
  End If
	
  RenderPerIFrame = "<table bordercolor=""#61c7d7"" border=""1"" cellpadding=""2"" cellspacing=""3"" valign=""top"">" _
				& "<tr valign=""top"" align=""left"">" _
				& "<td valign=""top"" style='border:none;'>" + header + "<IFRAME NAME=""Frame"" FRAMEBORDER=""0"" WIDTH=""" & frameWidth & """ HEIGHT=""" & frameHeight & """ MARGINWIDTH=""0"" MARGINHEIGHT=""0"" SRC=""" & sourceFile & """></IFRAME></td>" _
				& "</tr>" _
				& "</table>" 

  End Function

' -----------------------------------------------------------------------------
' RenderPerSearch(ByVal sSearch)
'
' Description: This renderfunction generates the search-frame for the personal site
'
' Parameters:  
'
' Returns:
'
' Notes :	
'
' -----------------------------------------------------------------------------

  Function RenderPerSearch(ByVal sSearch, ByVal sInfoPlace)
  
  Dim htmContent
  
  htmContent="<table><tr><td><form name='perSearch' action='periframe.asp' method='post'>"_
				& "<input type='textbox' name='txtSearch' value='" & sSearch & "' class='news' "_
				& "style='height: 19px; width: 135px;' maxlength='120'>"_
				& "<input type='submit' name='btnSearch' value='S�k' class='formbutton'"_
				& "style='background:#bfccd9 none; color:#003366; height: 19px; width:26' "_
				& "title='S�ket vil bli oppdatert hvert 5. minutt (eller ved � trykke p� s�k-knappen)'><br>"_
				& "<input type='hidden' name='hidSearch' value='" & sSearch & "' class='formbutton'>"_
				& "<input type='hidden' name='hidPlace' value='" & sInfoPlace & "' class='formbutton'>"_
				& "<input type='hidden' name='hidChange' value='startSearch' class='formbutton'>"

  RenderPerSearch = htmContent
  
  End Function

%>