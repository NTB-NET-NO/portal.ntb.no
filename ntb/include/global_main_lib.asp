<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_main_lib.asp
' #include file used by global.asa to do the hard work
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim MSCSCatalogManager, MSCSCatalogAttribs, MSCSProfileService
    Dim MSCSAltCurrencyDisp, MSCSCacheManager, MSCSAdoConnection
	Dim MSCSDataFunctions, MSCSPageSets, MSCSSitePages, MSCSCatalogSets, MSCSPageProperties
    Dim MSCSSiteStyle, MSCSPipelines, MSCSMessageManager
	Dim MSCSExpressionEvaluator, MSCSAuthManager, iWebServerCount
	
	Dim MSCSProfileServiceUsesActiveDirectory, MSCSActiveDirectoryDomain
    Dim MSCSProfileProperties
    Dim MSCSPredictor
    
    Dim MSCSForms
            
	Set appframework = Server.CreateObject("Commerce.AppFrameWork")
	
	' Get the commerce site name from the config file.
	MSCSCommerceSiteName = GetINIString(GetINIFileName(), SITE_NAME)
	Application("MSCSCommerceSiteName") = MSCSCommerceSiteName
    	
	' Get the commerce site application name from the config file.
	MSCSCommerceAppName = GetINIString(GetINIFileName(), APP_NAME)
	Application("MSCSCommerceAppName") = MSCSCommerceAppName

	' AppCenter integration
	Call JoinWebFarm(iWebServerCount)
	Application("WebServerCount") = iWebServerCount
	
	Set dictConfig = GetConfigDictionary()
	Application("MSCSPageEncodingCharset") = dictConfig.s_PageEncodingCharset
	
	' Create an instance of the expression evaluator
    Set MSCSExpressionEvaluator = Server.CreateObject("Commerce.ExpressionEvaluator")
    MSCSExpressionEvaluator.Connect(dictConfig.s_BizDataStoreConnectionString)
    
	Set MSCSSitePages = GetSitePages()
    Set MSCSProfileService = InitProfileService()
    Set MSCSAdoConnection = oGetOpenConnectionObject()
    Set MSCSPageSets = InitPageSets(MSCSSitePages)
    Set MSCSPipelines = InitSitePipelines()
    Set MSCSSiteStyle = GetSiteStyles()    
    Set MSCSMessageManager = GetMessageManagerObject()
    Set MSCSDataFunctions = InitDataFunctions()
    Set MSCSCacheManager = InitCacheManager(MSCSExpressionEvaluator)    
    Set MSCSAltCurrencyDisp = InitAltCurrencyDisp()    
    Set MSCSCatalogManager = InitCatalogManager()
    Set MSCSCatalogAttribs = dictGetCatalogAttributes(MSCSCatalogManager)
    Set MSCSPredictor = InitPredictor()
    Set MSCSAuthManager = InitAuthManager()
    Set MSCSCatalogSets = InitCatalogSets()
    
    ' Check if Active Directory data store is supported
	MSCSProfileServiceUsesActiveDirectory = IsActiveDirectoryPresent()		

    ' Get the active directory domain name
    If MSCSProfileServiceUsesActiveDirectory Then
        MSCSActiveDirectoryDomain = sGetActiveDirectoryDefaultNamingContext()
    End If
    	
	' Get the dictionary for all the profiles and properties
	Set MSCSProfileProperties = dictGetProfileSchemaForAllProfileTypes(MSCSProfileService)
	
	' Get the dictionary of dictionaries (one for each page in partner
	'     and customer service) containing properties for all pages
	Set MSCSPageProperties = GetPartnerAndCustomerServicePageAttributes(MSCSSitePages)

    Call InitCSF(MSCSPipelines, MSCSCacheManager, MSCSExpressionEvaluator)
    
    Set Application("MSCSAppConfig") = GetAppConfigObject()
    Set Application("MSCSProfileService") = MSCSProfileService 
    Set Application("MSCSAdoConnection") = MSCSAdoConnection
	Set Application("MSCSAltCurrencyDisp") = MSCSAltCurrencyDisp
	Set Application("MSCSCacheManager") = MSCSCacheManager
	Set Application("MSCSAuthManager") = MSCSAuthManager
		
	Application("MSCSProfileServiceUsesActiveDirectory") = MSCSProfileServiceUsesActiveDirectory
	If MSCSProfileServiceUsesActiveDirectory Then
        Application("MSCSActiveDirectoryDomain") = MSCSActiveDirectoryDomain
    End If        
    Set Application("MSCSProfileProperties") = MSCSProfileProperties

    Set Application("MSCSSitePages") = MSCSSitePages
    Set Application("MSCSSecurePages") = GetSecurePagesDictionary()

	' Get the dictionary of dictionaries (one for each page in partner
	'     and customer service) containing properties for all pages
    Set Application("MSCSPageProperties") = GetPartnerAndCustomerServicePageAttributes(Application("MSCSSitePages"))
        
    Set Application("MSCSPageSets") = MSCSPageSets
    Set Application("MSCSPipelines") = MSCSPipelines
    Set Application("MSCSSiteStyle") = MSCSSiteStyle
    Set Application("MSCSMessageManager") = MSCSMessageManager
    Set Application("MSCSGenID") = oGetGenIDObject()
	Set Application("MSCSDataFunctions") = MSCSDataFunctions    
	Set Application("MSCSCatalogManager") = MSCSCatalogManager
	Set Application("MSCSCatalogAttribs") = MSCSCatalogAttribs
	Set Application("MSCSExpressionEvaluator") = MSCSExpressionEvaluator
	Set Application("MSCSPredictor") = MSCSPredictor
	Set Application("MSCSCatalogSets") = MSCSCatalogSets
	Set Application("MSCSSitePaymentMethods") = GetPaymentMethodsForSite()	
	Set Application("MSCSForms") = GetFormDefinitions()
	Set Application("MSCSObjectInstances") = GetObjectInstanceDictionary()
	
	Set Application("MSCSAppFrameWork") = Server.CreateObject("Commerce.AppFrameWork")
End Sub
</SCRIPT>