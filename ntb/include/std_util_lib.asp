<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' std_util_lib.asp
' Standard library with Utility functions; also used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetAddressPage
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAddressPage()
	If m_UserType = GUEST_USER Then
		GetAddressPage = MSCSSitePages.AddressForm
	ElseIf dictConfig.i_AddressBookOptions <> ADDRESSBOOK_DISABLED Then
		GetAddressPage = MSCSSitePages.AddressBook
	Else
		GetAddressPage = MSCSSitePages.AddressForm
	End If
End Function 		


' -----------------------------------------------------------------------------
' DoubleQuote
'
' Description:
'	Puts double quotes around string.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function DoubleQuote(ByVal s)
	DoubleQuote = """" & s & """"
End Function


' -----------------------------------------------------------------------------
' GetListItemIndex
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetListItemIndex(ByVal sItem, list)
	Dim i	
	
	For i = 0 To list.Count - 1
		If StrComp(sItem, list(i), vbTextCompare) = 0 Then
			GetListItemIndex = i
			Exit For
		End If
	Next
End Function 


' -----------------------------------------------------------------------------
' GetArrayFromList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetArrayFromList(ByVal listFields)
	Dim nCount , i	
	Dim arrFields()
	
	nCount =  listFields.count
	ReDim arrFields(nCount-1)
	
	For i = 0 to nCount - 1
		arrFields(i) = listFields(i)
	Next
	
	GetArrayFromList = arrFields
End Function


' -----------------------------------------------------------------------------
' ConvertListToArray
'
' Description:
'	Converts a SimpleList to array
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function ConvertListToArray(ByVal list)
	Dim iCount, i	
	Dim arrList()
	
	iCount = list.Count

	ReDim arrList(iCount - 1)
	
	For i = 0 to iCount - 1
		arrList(i) = list(i)
	Next
	
	ConvertListToArray = arrList
End Function


' -----------------------------------------------------------------------------
' FormatOutput
'
' Description:
'	This function formats a string by replacing %n's with strings
'   supplied in the aArgs array
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function FormatOutput(ByVal sTemplate, arrArgs)
	Dim i
	
	FormatOutput = sTemplate
	
	For i = LBound(arrArgs) To UBound(arrArgs)
		FormatOutput = Replace(FormatOutput , "%" & i + 1, arrArgs(i))
	Next
End Function


' -----------------------------------------------------------------------------
' GetDictionary
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDictionary()
	Set GetDictionary = Server.CreateObject("Commerce.Dictionary")
End Function


' -----------------------------------------------------------------------------
' SortDictionaryKeys
'
' Description:
'	This function sorts a dictionary's keys, and puts the result into a	SimpleList
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
' $$ can we use a more performant algorithm here?
Function SortDictionaryKeys(Dictionary)
    Dim e1, e2, sLowestAllowed, sPassLowest
    Set SortDictionaryKeys = CreateObject("Commerce.SimpleList")
    
    sLowestAllowed = Chr(1)
    For Each e1 In Dictionary
        sPassLowest = ""
        For Each e2 In Dictionary
            If (sPassLowest = "" And LCase(e2) > LCase(sLowestAllowed)) Or _
                (LCase(e2) < LCase(sPassLowest) And LCase(e2) > LCase(sLowestAllowed)) Then
                sPassLowest = e2
            End If
        Next
        SortDictionaryKeys.Add sPassLowest
        sLowestAllowed = sPassLowest
    Next
End Function


' -----------------------------------------------------------------------------
' GetRegExp
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetRegExp()
    Set GetRegExp = Server.CreateObject("VBScript.RegExp")
End Function    


' -----------------------------------------------------------------------------
' GetSimpleList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSimpleList()
	Set GetSimpleList = Server.CreateObject("Commerce.SimpleList")
End Function


' -----------------------------------------------------------------------------
' GetObjectInstanceDictionary
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetObjectInstanceDictionary()
	Dim dict
	
	Set dict = GetDictionary()
	Set dict.Value("Dictionary") = GetDictionary()
	Set dict.Value("SimpleList") = GetSimpleList()
	
	Set GetObjectInstanceDictionary = dict
End Function


' -----------------------------------------------------------------------------
' GetQualifiedName
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetQualifiedName(ByVal sGroupName, ByVal sFieldName)
	GetQualifiedName = sGroupName & "." & sFieldName
End Function


' -----------------------------------------------------------------------------
' IsBitValueSet
'
' Description:
'	Returns true if for every bit flag that is set to on (0=off, 1=on) in
'	iBitValue, there is a corresponding bit flag set to on in iValue.
'	For example, for iBitValue=5 (00000101) and iValue=7 (00000111),
'	IsBitValueSet returns true since bit flags set to on in iBitValue
'	(bit flags at locations 0 and 2 from right) are also set in iValue.)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsBitValueSet(ByVal iValue, ByVal iBitValue)
	IsBitValueSet = ((iValue And iBitValue) = iBitValue)
End Function

	
' -----------------------------------------------------------------------------
' IsNumberInRange
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsNumberInRange(iNumber, iMin, iMax)
	IsNumberInRange = ((iNumber >= iMin) And (iNumber <= iMax))
End Function


' -----------------------------------------------------------------------------
' GetRecordCount
'
' Description:
'	Can't use RecordCount because of ForwardOnly cursors on recordsets
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetRecordCount(ByRef rs)
	Dim i
	
	i = 0
	If rs.EOF Or rs.BOF Then
		GetRecordCount = i
	Else
		While Not rs.EOF
			i = i + 1
			rs.MoveNext
		Wend
		rs.MoveFirst		
		GetRecordCount = i
	End If
End Function


' -----------------------------------------------------------------------------
' CreateSet
'
' Description:
'   Creates a set given an array of items.  The sets are used by IsEntityInSet
'
' Parameters:
'
' Returns:
'
' Notes :
'   A "set", in this context, is just a comma-separated string
' -----------------------------------------------------------------------------
Function CreateSet(ByVal arrItems)
    Dim i
    
    For i = 0 To UBound(arrItems)
        CreateSet = CreateSet & arrItems(i) & ", " 
    Next
    
    If UBound(arrItems) + 1 > 0 Then
        CreateSet = Left(CreateSet, Len(CreateSet) - 2)
    End If
End Function


' -----------------------------------------------------------------------------
' IsEntityInSet
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsEntityInSet(ByVal sEntity, ByVal sSet)
    If InStr(1, sSet, sEntity, vbTextCompare) > 0 Then
        IsEntityInSet = True
    Else
        IsEntityInSet = False
    End If
End Function


' -----------------------------------------------------------------------------
' IsEntityInList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsEntityInList(ByVal sEntity, list)
    Dim sItem
    
    IsEntityInList = False    
    
    For Each sItem In list
		If StrComp(sItem, sEntity, vbTextCompare) = 0 Then
			IsEntityInList = True    
			Exit For
		End If
	Next
End Function


' -----------------------------------------------------------------------------
' IsEntityInArray
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsEntityInArray(ByVal sEntity, ByVal arrItems)
    Dim i
    
    For i = 0 To UBound(arrItems)
		If StrComp(sEntity, arrItems(i), vbTextCompare) = 0 Then
			IsEntityInArray = True    
			Exit Function
		Else
			IsEntityInArray = False    
		End If
	Next
End Function



' -----------------------------------------------------------------------------
' GetDiscountDescription
'
' Description:
'	Searches the the contentlist for the given ciid
'	sets return value = "D" & ciid & description (if found)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDiscountDescription(ByVal ciid, ByVal oContentList)		
	Dim oRows, oFields
	
	Set oRows = oContentList.Search("item_id", ciid)
	If Not oRows.EOF Then
		Set oFields = oRows.Fields
		GetDiscountDescription = "D" & CStr(ciid) & NBSP & oFields.description & CR
	Else
		GetDiscountDescription = "D" & CStr(ciid) & NBSP & mscsMessageManager.GetMessage("L_BASKET_DISCOUNT_NODESCRITION_TEXT", sLanguage) & CR
	End If
End Function


' -----------------------------------------------------------------------------
' htmRenderCurrency
'
' Description:
'	Renders base and alternate currency depending on site's settings
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCurrency(ByVal cyCurrency)
	Dim cyAltCurrency, sBaseCurrency, sAltCurrency
	
	' Get base currency
	sBaseCurrency = MSCSDataFunctions.LocalizeCurrency(cyCurrency, dictConfig.i_BaseCurrencyLocale, dictConfig.s_BaseCurrencySymbol)
	
	' Replace spaces with non-breaking space to avoid wrapping of currency symbol.
	sBaseCurrency = Replace(sBaseCurrency, " ", "&nbsp;")
										 
	If dictConfig.i_AltCurrencyOptions = ALTCURRENCY_DISPLAY_ENABLED Then
		cyAltCurrency = MSCSAltCurrencyDisp.ConvertMoney(cyCurrency)
		sAltCurrency = MSCSAltCurrencyDisp.FormatAltMoney(CCur(cyAltCurrency))
		
		' Replace spaces with non-breaking space to avoid wrapping of currency symbol.
		sAltCurrency = Replace(sAltCurrency, " ", "&nbsp;")
		
		If dictConfig.i_CurrencyDisplayOrderOptions = DISPLAY_BASE_CURRENCY_FIRST Then
			' Base currency comes before alt currency
			htmRenderCurrency = RenderText(sBaseCurrency, MSCSSiteStyle.BaseCurrency) & " " & RenderText(sAltCurrency, MSCSSiteStyle.AltCurrency)
		Else
			' Alt currency comes before base currency
			htmRenderCurrency = RenderText(sAltCurrency, MSCSSiteStyle.AltCurrency) & " " & RenderText(sBaseCurrency, MSCSSiteStyle.BaseCurrency)
		End If
	Else
		htmRenderCurrency = RenderText(sBaseCurrency, MSCSSiteStyle.BaseCurrency)
	End If
	
End Function


' -----------------------------------------------------------------------------
' GetFileSystemObject
'
' Description:
'	Site styles are used by HTMLLib functions 
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetFileSystemObject()
	Set GetFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
End Function


' -----------------------------------------------------------------------------
' GetKeyValue
'
' Description:
'	Returns the value part of a key=value pair in a string
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetKeyValue(ByVal sKeyValuePair, ByVal sKey)
	Dim iPos
	Dim s

	' Replace tab character with space
	sKeyValuePair = Trim(Replace(sKeyValuePair, Chr(9), Chr(32)))
	sKey = Trim(sKey)
	
	iPos = InStr(1, sKeyValuePair, "=", vbTextCompare)	

	' Return empty string if invalid key=value pair syntax
	If IsNull(iPos) Or iPos = 0 Then
		GetKeyValue = ""
	Else
		' Extract the key part from key=value pair
		s = Trim(Left(sKeyValuePair, (iPos - 1)))
		' If specified key exists in key=value pair
		If StrComp(sKey, s, vbTextCompare) = 0 Then
			GetKeyValue = Trim(Right(sKeyValuePair, Len(sKeyValuePair) - iPos))
		Else
			' Otherwise, return empty string
			GetKeyValue = ""
		End If
	End If
End Function


' -----------------------------------------------------------------------------
' GetErrorSource
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetErrorSource(ByVal sFunctionName)
	GetErrorSource = FormatOutput(L_TermDefinition_Text, Array(GetCommerceSiteName(), sFunctionName))
End Function


' -----------------------------------------------------------------------------
' RsToDict
'
' Description:
'   This function copies the fields from a recordset's current cursor position into a new dictionary
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RsToDict(rs)
    Dim dict, fld
    Set dict = GetDictionary()
    For Each fld in rs
       dict.Value(fld.Name) = fld.Value
    Next
    Set RsToDict = dict
End Function
</SCRIPT>