<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' Added BugFix at Line 102. 2002.06.11 Roar Vestre, NTB
'
' global_catalog_lib.asp
' Global Initialization library with Catalog functions; used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitCatalogManager
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitCatalogManager()
    Dim mscsCatalogMgr
    
    Set mscsCatalogMgr = Server.CreateObject("Commerce.CatalogManager")
    Call mscsCatalogMgr.Initialize(dictConfig.s_CatalogConnectionString, ADO_CONNECTION_STRING)    
    
    Set InitCatalogManager = mscsCatalogMgr
End Function


' -----------------------------------------------------------------------------
' InitCatalogSets
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitCatalogSets()
    Dim oCatalogSets
    Dim dictCatalogSets, dictCatalogSet, rsCatalogSets, rsCatalogSet
	Dim dictConfig
	Set dictConfig = Application("MSCSAppConfig").GetOptionsDictionary("")
    
    Set oCatalogSets = Server.CreateObject("Commerce.CatalogSets")
    Call oCatalogSets.Initialize(dictConfig.s_CatalogConnectionString, dictConfig.s_TransactionConfigConnectionString)
    
    Set dictCatalogSets = CreateObject("Commerce.Dictionary")
    Set rsCatalogSets = oCatalogSets.GetCatalogSets()
    While Not rsCatalogSets.EOF
        Set rsCatalogSet = oCatalogSets.GetCatalogsInCatalogSet(rsCatalogSets(0))
        Set dictCatalogSet = CreateObject("Commerce.Dictionary")
        While Not rsCatalogSet.EOF
            dictCatalogSet.Value(rsCatalogSet(0).Value) = True
            rsCatalogSet.MoveNext
        Wend
        Set dictCatalogSets.Value(rsCatalogSets(0)) = dictCatalogSet
        rsCatalogSets.MoveNext
    Wend
    
    Set InitCatalogSets = dictCatalogSets
End Function


' -----------------------------------------------------------------------------
' dictGetCatalogAttributes
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictGetCatalogAttributes(MSCSCatalogManager)
	Dim rsProps, dictAttribs
	
	Set dictGetCatalogAttributes = GetDictionary()
	Set rsProps = MSCSCatalogManager.Properties	
	
	' Added after bug, by RoV 2002.06.11
	If Not rsProps.EOF Then
		rsProps.MoveFirst
	End If
	While Not rsProps.EOF
		Set dictAttribs = dictGetRSFields(rsProps)
		Set dictGetCatalogAttributes.Value(rsProps.Fields("PropertyName")) = dictAttribs
		rsProps.MoveNext
	Wend
End Function


' -----------------------------------------------------------------------------
' dictGetRSFields
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictGetRSFields(rs)
	Dim fld
	
	Set dictGetRSFields = GetDictionary()
	
	For Each fld In rs.Fields
		dictGetRSFields.Value(fld.Name) = rs.Fields(fld.Name).Value
	Next
End Function
</SCRIPT>