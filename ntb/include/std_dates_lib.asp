<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' std_dates_lib.asp
' Standard library with Dates functions; also used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetMonthNamesList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetMonthNamesList()
	Dim MSCSMessageManager
	Dim sLanguage
    Dim listItems
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

	Set listItems = GetSimpleList()
	Call listItems.Add(MSCSMessageManager.GetMessage("L_SelectOne_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_January_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_February_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_March_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_April_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_May_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_June_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_July_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_August_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_September_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_October_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_November_Text", sLanguage))
	Call listItems.Add(MSCSMessageManager.GetMessage("L_December_Text", sLanguage))
        
    Set GetMonthNamesList = listItems
End Function     


' -----------------------------------------------------------------------------
' GetMonthCodesList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetMonthCodesList()
    Dim listVals

	Set listVals = GetSimpleList()
	Call listVals.Add("-1")
	Call listVals.Add("01")
	Call listVals.Add("02")
	Call listVals.Add("03")
	Call listVals.Add("04")
	Call listVals.Add("05")
	Call listVals.Add("06")
	Call listVals.Add("07")
	Call listVals.Add("08")
	Call listVals.Add("09")
	Call listVals.Add("10")
	Call listVals.Add("11")
	Call listVals.Add("12")
        
    Set GetMonthCodesList = listVals
End Function     


' -----------------------------------------------------------------------------
' GetYearsListItems
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetYearsListItems()
    Dim listItems, iCurrYear, i
    Dim MSCSMessageManager, sLanguage
    
    Set MSCSMessageManager = Application("MSCSMessageManager")
    sLanguage = MSCSMessageManager.DefaultLanguage
    
    
	Set listItems = GetSimpleList()
	listItems.Add(MSCSMessageManager.GetMessage("L_SelectOne_Text", sLanguage))
	
	iCurrYear = Year(Now)
	For i = 0 To 5 
		listItems.Add(iCurrYear + i)
	Next
	
    Set GetYearsListItems = listItems
End Function


' -----------------------------------------------------------------------------
' GetYearsListValues
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetYearsListValues()
	Dim listVals
	
	Set listVals = GetYearsListItems()
	listVals(0) = "-1"
	
	Set GetYearsListValues = listVals
End Function
</SCRIPT>
