<%
' SETUPENV.ASP
'   This file  sets up environment variables and calls Main()
'   Main() should build htmPageContent, which gets Response.Written by template/layout1.asp

' Global.asa variables

Dim MSCSErrorInGlobalASA, MSCSEnv
Dim MSCSCommerceSiteName, MSCSAppConfig, MSCSProfileService
Dim MSCSAltCurrencyDisp, MSCSGenID, MSCSAdoConnection
Dim MSCSCatalogManager, MSCSCatalogAttribs, MSCSSitePages, MSCSSiteStyle
Dim MSCSPipelines, MSCSMessageManager, sLanguage, MSCSDataFunctions
Dim MSCSProfileServiceUsesActiveDirectory
Dim MSCSActiveDirectoryDomain, MSCSPageSets, MSCSCatalogSets
Dim MSCSAppFrameWork
Dim MSCSCacheManager

' Page variables 
Dim m_UserAccessType, m_UserType, m_UserID, m_iTicketLocation, m_iGlobalProfileLookUpOption
Dim dictConfig, mscsAuthMgr
Dim sPageTitle, sThisPage, mscsUserProfile
Dim htmPageContent
Dim htmDiscountBannerSlot

' Handle global.asa errors
MSCSErrorInGlobalASA = Application("MSCSErrorInGlobalASA")
If IsEmpty(MSCSErrorInGlobalASA) Or MSCSErrorInGlobalASA = True Then
    Err.Raise HRESULT_E_FAIL '500error.asp will handle this...
End If

' Fetch Application Variables
MSCSEnv = Application("MSCSEnv")
Set MSCSAppConfig = Application("MSCSAppConfig")       
Set MSCSProfileService = Application("MSCSProfileService")       
Set MSCSAdoConnection = Application("MSCSAdoConnection")    ' Accessing the Commerce OLE DB Provider (CSOLEDB)
Set MSCSSitePages = Application("MSCSSitePages")        
Set MSCSPageSets = Application("MSCSPageSets")
Set MSCSAppFrameWork = Application("MSCSAppFrameWork")
Set MSCSSiteStyle = Application("MSCSSiteStyle")
Set MSCSPipelines = Application("MSCSPipelines")    
Set MSCSMessageManager = Application("MSCSMessageManager")
Set MSCSGenID = Application("MSCSGenID")
Set MSCSDataFunctions  = Application("MSCSDataFunctions")
Set MSCSAltCurrencyDisp = Application("MSCSAltCurrencyDisp")
Set MSCSCatalogManager = Application("MSCSCatalogManager")
Set MSCSCatalogAttribs = Application("MSCSCatalogAttribs")        
Set MSCSCatalogSets = Application("MSCSCatalogSets")
Set MSCSCacheManager = Application("MSCSCacheManager")
MSCSCommerceSiteName = Application("MSCSCommerceSiteName")
MSCSProfileServiceUsesActiveDirectory = Application("MSCSProfileServiceUsesActiveDirectory")
MSCSActiveDirectoryDomain = Application("MSCSActiveDirectoryDomain")
    
' In production we allow centralized error handling
Call SetupPage()

Sub SetupPage()
	' Setup page variables
	Set mscsAuthMgr = GetAuthManagerObject()
	sThisPage = Request.ServerVariables("SCRIPT_NAME")
	If Len(MSCSAppFrameWork.VirtualDirectory) = 0 Then
		sThisPage = Mid(sThisPage, 2) 'Site Installed at root, trim off leading \
	Else
		sThisPage = Mid(sThisPage, Len(MSCSAppFrameWork.VirtualDirectory) + 3) 'Trim off leading vdir
	End If
	Set dictConfig = Application("MSCSAppConfig").GetOptionsDictionary("")

	' Correct the request (handles case-sensitivity issue with cookies)
	'Call CorrectRequest()
	
	' Profile service maintains separate caches on each Web server in a Webfarm. 
	'	If a profile that is cached on Web Server 1 changes on Web Server 2 
	'	(updated or deleted), then the Web Server 1 cache will not reflect the change(s). 
	'	If the nature of activities on your Web Site are such that this lack of 
	'	synchronization poses a problem, you can set m_iGlobalProfileLookUpOption to always 
	'	force a database look-up. Obviously, this has performance implications 
	'	(you are effectively disabling profile caching). 
	
	' 1 indicates always look up from database (accuracy), 2 indicates minimize database look-ups (perf)
	m_iGlobalProfileLookUpOption = 2

	' Returns 1 if the ticket is in URL, 2 if it is in cookie, 0 if there is no ticket.
	m_iTicketLocation = GetTicketLocation()

	' Fetch info about the user
	'   m_userID -- user id
	'   m_UserAccessType -- IIS_AUTH / TICKET_AUTH / GUEST_VISIT / ANON_VISIT
	'   m_UserType -- AUTH_USER / GUEST_USER / ANON_USER
	Call GetUserInfo()
	
	' Set the language for the user
	sLanguage = MSCSMessageManager.DefaultLanguage

	' Call Main(), which should build htmPageContent.  
	'   Main should call EnsureAccess or EnsureAuthAccess to implement access control
	Call Main()
End Sub
%>

