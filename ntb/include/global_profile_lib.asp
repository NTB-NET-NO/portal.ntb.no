<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_profile_lib.asp
' Global Initialization library with Profile Service functions; used by
' global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitProfileService
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitProfileService()
    Dim mscsProfileService
    
	Set mscsProfileService = Server.CreateObject("Commerce.ProfileService")
    Call mscsProfileService.Initialize(dictConfig.s_ProfileServiceConnectionString, SCHEMA_CATALOG)
    
    Set InitProfileService = mscsProfileService
End Function


' -----------------------------------------------------------------------------
' InitAuthManager
'
' Description:
'	Initialize AuthManager Component
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitAuthManager()
	Set InitAuthManager = Server.CreateObject("Commerce.AuthManager")
End Function


' -----------------------------------------------------------------------------
' dictGetProfileSchemaForAllProfileTypes
'
' Description:
'	Function to create a dictionary for all the profiles and their
'   properties
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictGetProfileSchemaForAllProfileTypes(ByVal oProfileService)
	Const MODEL_ORG         = "ModelOrganizationProfile"
    Const MODEL_USER        = "ModelUserProfile"
    Const MODEL_ADDRESS     = "ModelAddressProfile"    
    
	Dim oProfileProperties	
    Dim oOrg
    Dim oUser
    Dim oAddress

	Dim oBizData
	Dim oSiteTermsProfile
	Dim oProfileDefsCatalog

    Set oProfileProperties = GetDictionary()

    Set oOrg = oProfileService.CreateProfile(MODEL_ORG, PROFILE_TYPE_ORG)
    Set oUser = oProfileService.CreateProfile(MODEL_USER,PROFILE_TYPE_USER)
    Set oAddress = oProfileService.CreateProfile(MODEL_ADDRESS,PROFILE_TYPE_ADDRESS)

    Set oBizData = oGetBizDataObject()
    Set oSiteTermsProfile = oGetProfile (oBizData, "Site Terms.MSCommerce")

	Set oProfileDefsCatalog = oGetProfile (oBizData, PROFILE_TYPE_ORG)
    Set oProfileProperties(PROFILE_TYPE_ORG) = listGetProfileSchema (oOrg, _
													oProfileDefsCatalog, oSiteTermsProfile)
    Set oOrg = Nothing
    Set oProfileDefsCatalog = Nothing

	Set oProfileDefsCatalog = oGetProfile (oBizData, PROFILE_TYPE_USER)
    Set oProfileProperties(PROFILE_TYPE_USER) = listGetProfileSchema (oUser, _
													oProfileDefsCatalog, oSiteTermsProfile)
    Set oUser = Nothing
    Set oProfileDefsCatalog = Nothing

	Set oProfileDefsCatalog = oGetProfile (oBizData, PROFILE_TYPE_ADDRESS)
    Set oProfileProperties(PROFILE_TYPE_ADDRESS) = listGetProfileSchema (oAddress, _
													oProfileDefsCatalog, oSiteTermsProfile)
    Set oAddress = Nothing
    Set oProfileDefsCatalog = Nothing
    
    Set dictGetProfileSchemaForAllProfileTypes = oProfileProperties
End Function    


' -----------------------------------------------------------------------------
' listGetProfileSchema
'
' Description:
'	Function to create a simplelist of dictionaries for each property in the
'   profile passed to this function
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function listGetProfileSchema (ByVal oProfile, _
							   ByVal oProfileDefsCatalog, _
							   ByVal oSiteTermsProfile)
    Dim oGroup
    Dim listProperties

    Set listProperties = Server.CreateObject("Commerce.SimpleList")

    For Each oGroup In oProfile.Fields
        Call AddGroupAndPropertySchemas(oGroup, listProperties, _
										oProfileDefsCatalog, oSiteTermsProfile)
    Next

    Set oGroup = Nothing

    Set listGetProfileSchema = listProperties    
End Function


' -----------------------------------------------------------------------------
' AddGroupAndPropertySchemas
'
' Description:
'	Recursive function to add all group and properties for a profile
'	in a flat list of dictionaries
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub AddGroupAndPropertySchemas(ByVal oGroup, ByRef listProperties, _
							   ByRef oProfileDefsCatalog, ByRef oSiteTermsProfile)
    Const PROFILE_PROPERTY_GROUP = 132
    Const SITE_PROPERTY_HIDDEN   = 3

    Dim oProperty, dictGroup, dictProperty

    ' DO NOT Add group level properties as groups!!!
    If (typename(oGroup.Value) <> "Fields") Then Exit Sub

    ' Add Group attributes
    Set dictGroup = GetDictionary()
    dictGroup.Name = oGroup.Name        
    dictGroup.IsGroup = True
    dictGroup.InputType = SITE_PROPERTY_HIDDEN        
    dictGroup.DisplayName = oGroup.Properties("DisplayName").Value        
    Call listProperties.Add(dictGroup)

    ' Add properties in this group
	For Each oProperty In oGroup.Value
		If (oProperty.Type = PROFILE_PROPERTY_GROUP) Then
			' The property is of type GROUP, call the same function(recursion)                
			Call AddGroupAndPropertySchemas(oProperty, listProperties, _
											oProfileDefsCatalog, oSiteTermsProfile)
		Else
			Set dictProperty = dictGetPropertySchema (oGroup, oProperty, _
									oProfileDefsCatalog, oSiteTermsProfile)
			Call listProperties.Add(dictProperty)
		End If
	Next

    Set dictGroup = Nothing
    Set dictProperty = Nothing
End Sub


' -----------------------------------------------------------------------------
' oGetProfile
'
' Description:
'	Returns an instance of a Profile Defs / Site Terms Catalog XMLDOM
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function oGetProfile(ByVal oBizData, ByVal sProfileName)
	Dim sPrefix
	Dim oXMLProfile

	Set oGetProfile = Nothing
    If (oBizData Is Nothing) Then Exit Function
	If (InStr (sProfileName, ".") = 0) Then sPrefix = "Profile Definitions."

    On Error Resume Next
	Set oXMLProfile = oBizData.GetProfile (CStr(sPrefix & sProfileName))
	If (Err.Number <> 0) Then
		Err.Clear
		Exit Function
	End If

	Set oGetProfile = oXMLProfile
End Function


' -----------------------------------------------------------------------------
' oGetOpenConnectionObject
'
' Description:
'	Returns an open ADO connection to Commerce Provider.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function oGetOpenConnectionObject()
	Set oGetOpenConnectionObject = Server.CreateObject("ADODB.Connection")
	Call oGetOpenConnectionObject.Open(dictConfig.s_CommerceProviderConnectionString)
End Function
</SCRIPT>