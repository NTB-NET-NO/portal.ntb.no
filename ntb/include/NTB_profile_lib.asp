<%
'***********************************************************************************
' NTB_profile_lib.asp
'
' Support functions for getting and setting profileobjects and inserting information
' in userprofiles
'
' NOTES:
'		'none'
'
' CREATED BY: Solveig Skjermo
' CREATED DATE: 2002.05.15
' UPDATED BY: Solveig Skjermo
' UPDATED DATE: 2002.06.20
' REVISION HISTORY:
'		'none'
'
'***********************************************************************************

' -----------------------------------------------------------------------------
' GetSiteName()
'
' Description: Gets the sitename
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetSiteName()

	Dim sSiteName
	' Handle inconsistent naming in MSCS code
	sSiteName = Application("MSCSCommerceSiteName")

	If not Len(sSiteName) > 0 then
		sSiteName = Application("MSCSSiteName")
	end if

	GetSiteName = sSiteName

  End Function

' -----------------------------------------------------------------------------
' GetAppConfigObject()
'
' Description: Gets the configuration
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetAppConfigObject()

    Dim sSiteName
    Dim oAppConfigObject

    sSiteName = GetSiteName()

    Set oAppConfigObject = Server.CreateObject("Commerce.AppConfig")
    Call oAppConfigObject.Initialize(sSiteName)

    Set GetAppConfigObject = oAppConfigObject

  End Function

' -----------------------------------------------------------------------------
' InitProfileService()
'
' Description: Initializes the profileservice
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------
  Function InitProfileService()

    Dim oAppConfig
    Dim sConnString
    Dim oProfileService

	' Get connect string of profile service.
	' The GetAppConfigObject function returns an initialized
	' App config object
    Set oAppConfig = GetAppConfigObject().GetOptionsDictionary("")
    sConnString = oAppConfig.s_ProfileServiceConnectionString

    Set oProfileService = Server.CreateObject("Commerce.ProfileService")
    Call oProfileService.Initialize(sConnString)

    Set InitProfileService = oProfileService

  End Function

' -----------------------------------------------------------------------------
' GetProfileServiceObject()
'
' Description: Gets the ProfileServiceObject or initializes the object if it
'				does not exist
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetProfileServiceObject()

	If IsObject(Application("MSCSProfileService")) Then'
		Set GetProfileServiceObject = Application("MSCSProfileService")
	Else
	    Set Application("MSCSProfileService") = InitProfileService
		Set GetProfileServiceObject = Application("MSCSProfileService")
	End If
  End Function

' -----------------------------------------------------------------------------
' GetUserName(ByVal UID)
'
' Description: Fetches the username of a user based on the userid
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function GetUserName(ByVal UID)

	Dim ProfileService, ProfileObj
	Dim sUserName

	Set ProfileService = GetProfileServiceObject()

	' Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfileByKey("user_id", UID, "UserObject")

	sUserName = ProfileObj.Fields("GeneralInfo").Value("logon_name")

	Set ProfileObj = Nothing

    GetUserName = sUserName

  End Function

' -----------------------------------------------------------------------------
' GetUserProfile(ByVal UID)
'
' Description: Fetches the profile of the user set by the userid
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetUserProfile(ByVal UID)

	Dim ProfileService, ProfileObj

	Set ProfileService = GetProfileServiceObject()

	' Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfileByKey("user_id", UID, "UserObject")
	Set GetUserProfile = ProfileObj

  End Function

' -----------------------------------------------------------------------------
' GetCompanyProfile(CoID)
'
' Description: Fetches the profile of the organization set by the orgid
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetCompanyProfile(ByVal CoID)

	Dim ProfileService, ProfileObj

	Set ProfileService = GetProfileServiceObject()

	' Get CompanyProfile for the organization
	Set ProfileObj = ProfileService.GetProfileByKey("org_id", CoID, "Organization")
	Set GetCompanyProfile = ProfileObj

  End Function

' -----------------------------------------------------------------------------
' GetAdressProfile(CoID)
'
' Description: Fetches the profile of the organization set by the orgid
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetAddressProfile(ByVal CoID, ByRef iTotalCount)

	Dim sSQLStmt, rsAdr

   ' Build SQL statement
   sSQLStmt = "SELECT [GeneralInfo.address_id], [GeneralInfo.address_line1], " & _
				"[GeneralInfo.postal_code], [GeneralInfo.city], [GeneralInfo.tel_number]" & _
	            "FROM [Address] " & _
				"WHERE [GeneralInfo.id] = '" & CoID & "'"

   ' Get the company recordset
   Set rsAdr = Server.CreateObject("ADODB.Recordset")
   rsAdr.Open sSQLStmt, MSCSAdoConnection

   iTotalCount = 1

   ' Check if the recordset returned is empty
   If rsAdr.EOF And rsAdr.BOF Then
      iTotalCount = 0
   End If

   Set GetAddressProfile = rsAdr

  End Function



' -----------------------------------------------------------------------------
' GetADProfileByName(ByVal sName)
'
' Description: Fetches the profile of the user set by the username
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetADProfileByName(ByVal sName)

    Dim oProfileService
    Dim oProfileObject

    If Len(sName) > 0 Then
        Set oProfileService = GetProfileServiceObject()
		Set oProfileObject = oProfileService.GetProfile(sName, "ADGroup", False)
    Else
        Set oProfileObject = Nothing
    End If

    Set GetADProfileByName = oProfileObject

End Function


' -----------------------------------------------------------------------------
' GetUserProfileByLoginName(ByVal sName)
'
' Description: Fetches the profile of the user set by the username
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetUserProfileByLoginName(ByVal sName)

    Dim oProfileService
    Dim oProfileObject

    If Len(sName) > 0 Then
        Set oProfileService = GetProfileServiceObject()
        Set oProfileObject = oProfileService.GetProfile(sName, "UserObject", False)
    Else
        Set oProfileObject = Nothing
    End If

	Set GetUserProfileByLoginName = oProfileObject

End Function

' -----------------------------------------------------------------------------
' GetCompanyProfileByName(ByVal sName)
'
' Description: Fetches the profile of the organization set by the name
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetCompanyProfileByName(ByVal sName)

    Dim oProfileService
    Dim oProfileObject

    If Len(sName) > 0 Then
        Set oProfileService = GetProfileServiceObject()
		Set oProfileObject = oProfileService.GetProfile(sName, "Organization", False)
    Else
        Set oProfileObject = Nothing
    End If

    Set GetCompanyProfileByName = oProfileObject

End Function

' -----------------------------------------------------------------------------
' GetCompany(ByVal sUserName)
'
' Description: Fetches the organisation the user belongs to
'
' Parameters:  sUserName - the users username
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function GetCompany(ByVal sUserName)

	Dim ProfileService, ProfileObj

	Set ProfileService = GetProfileServiceObject()

	' Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfile(sUserName, "UserObject", False)
	if (ProfileObj is nothing) then
		Response.Redirect "Login.asp"
	end if

	GetCompany = ProfileObj.Fields("AccountInfo").Value("org_id")

End Function

' -----------------------------------------------------------------------------
' RetrieveCompanyUsers(ByVal OrgID, ByRef iTotalCount)
'
' Description: Fetches all the user belonging to a spesific organization
'
' Parameters:  OrgID - the organisations id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RetrieveCompanyUsers(ByVal OrgID, ByRef iTotalCount)

Dim sSQLStmt
Dim rsUsers
Dim connection
    Set connection = Server.CreateObject("ADODB.Connection")							

    connection.ConnectionString = Application("commerce_cns")
    connection.Open
    connection.CursorLocation = adUseClient
    


   ' Build SQL statement
   sSQLStmt = "SELECT u_logon_user AS [GeneralInfo.logon_name], g_user_id AS [GeneralInfo.user_id], u_last_name AS [GeneralInfo.last_name], u_first_name AS [GeneralInfo.first_name], " & _
   "u_email_address AS [GeneralInfo.email_address], u_tel_number AS [GeneralInfo.tel_number], u_tel_extension AS [GeneralInfo.tel_extension],  i_partner_desk_role AS [BusinessDesk.partner_desk_role] " & _
   "FROM UserObject " & _
   "WHERE g_org_id = '" & orgID &"' " & _
   "ORDER BY u_last_name, u_first_name"
   'sSQLStmt = "SELECT [GeneralInfo.logon_name], [GeneralInfo.user_id], [GeneralInfo.last_name], [GeneralInfo.first_name], [GeneralInfo.email_address], [GeneralInfo.tel_number], [GeneralInfo.tel_extension], [BusinessDesk.partner_desk_role] " & _
'	            "FROM [UserObject] " & _
'				"WHERE [AccountInfo.org_id] = '" & orgID _
'	            & "'" & " ORDER BY [GeneralInfo.last_name], [GeneralInfo.first_name]"

   ' Get the company recordset
   Set rsUsers = Server.CreateObject("ADODB.Recordset")
   rsUsers.Open sSQLStmt, connection
'   rsUsers.Open sSQLStmt, MSCSAdoConnection

   iTotalCount = 1

   ' Check if the recordset returned is empty
   If rsUsers.EOF And rsUsers.BOF Then
      iTotalCount = 0
   End If

   set rsUsers.ActiveConnection = Nothing
   connection.Close
   Set RetrieveCompanyUsers = rsUsers

End Function

'------------------------------------------
' New function, trying to get only 5 of the users
'------------------------------------------

'Function RetrieveCompanyUsers(ByVal OrgID, ByRef iTotalCount)

'Dim sSQLStmt
'Dim rsUsers



   ' Build SQL statement
'   sSQLStmt = "SELECT [GeneralInfo.logon_name], [GeneralInfo.user_id], [GeneralInfo.last_name], [GeneralInfo.first_name], [GeneralInfo.email_address], [GeneralInfo.tel_number], [GeneralInfo.tel_extension], [BusinessDesk.partner_desk_role] " & _
'	            "FROM [UserObject] " & _
'				"WHERE [AccountInfo.org_id] = '" & orgID _
'	            & "'" & " ORDER BY [GeneralInfo.last_name], [GeneralInfo.first_name]"

   ' Get the company recordset
'   Set rsUsers = Server.CreateObject("ADODB.Recordset")
'   rsUsers.CursorLocation = 3
'   rsUsers.CursorType = 3
'   rsUsers.Open sSQLStmt, MSCSAdoConnection
'   rsUsers.PageSize = 2
   
   
'   iTotalCount = 1

   ' Check if the recordset returned is empty
'   If rsUsers.EOF And rsUsers.BOF Then
'      iTotalCount = 0
'   End If

'   Set RetrieveCompanyUsers = rsUsers

'End Function



' -----------------------------------------------------------------------------
' RetrieveAllActiveCompany(ByRef iTotalCount)
'
' Description: Fetches all the companies that are active customers of ntb
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RetrieveAllActiveCompany(ByRef iTotalCount)

Dim sSQLStmt
Dim rsCompany

   ' Build SQL statement, show only active organizations (account_status=1)
   sSQLStmt = "SELECT [GeneralInfo.name], [GeneralInfo.org_id], [GeneralInfo.account_status] " _
	            & "FROM [Organization]" _
				& "WHERE [GeneralInfo.account_status] = 1"_
	            & "ORDER BY [GeneralInfo.name]"

   ' Get the company recordset
   Set rsCompany = Server.CreateObject("ADODB.Recordset")
   rsCompany.Open sSQLStmt, MSCSAdoConnection

   iTotalCount = 1

   ' Check if the recordset returned is empty
   If rsCompany.EOF And rsCompany.BOF Then
      iTotalCount = 0
   End If

   Set RetrieveAllActiveCompany = rsCompany

End Function

' -----------------------------------------------------------------------------
' RetrieveAllInActiveCompany(ByRef iTotalCount)
'
' Description: Fetches all the companies that have been customers of ntb,
'				but aren't anymore
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RetrieveAllInActiveCompany(ByRef iTotalCount)

Dim sSQLStmt
Dim rsCompany

   ' Build SQL statement, show only active organizations (account_status=1)
   sSQLStmt = "SELECT [GeneralInfo.name], [GeneralInfo.org_id], [GeneralInfo.account_status] " _
	            & "FROM [Organization]" _
				& "WHERE [GeneralInfo.account_status] = 0"_
	            & "ORDER BY [GeneralInfo.name]"

   ' Get the company recordset
   Set rsCompany = Server.CreateObject("ADODB.Recordset")
   rsCompany.Open sSQLStmt, MSCSAdoConnection

   iTotalCount = 1

   ' Check if the recordset returned is empty
   If rsCompany.EOF And rsCompany.BOF Then
      iTotalCount = 0
   End If

   Set RetrieveAllInActiveCompany = rsCompany

End Function

' -----------------------------------------------------------------------------
' GetCompanyName(ByVal orgID )
'
' Description: Fetches the organization's name
'
' Parameters:  OrgID - the organisations id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function GetCompanyName(ByVal orgID)
Dim sSQLStmt
Dim rsCompany
Dim sCompanyName

   ' Build SQL statement
   sSQLStmt = "SELECT [GeneralInfo.name] FROM Organization WHERE [GeneralInfo.org_id] = '" & orgID & "'"

   ' Get the company recordset
   Set rsCompany = Server.CreateObject("ADODB.Recordset")
   rsCompany.Open sSQLStmt, MSCSAdoConnection

   ' Check if the recorset returned is empty
   If Not rsCompany.EOF Then
      Set sCompanyName = rsCompany(0)
   else
      sCompanyName = "Unknown"
   End If

   ' Return company name
   GetCompanyName = sCompanyName

   rsCompany.Close

End Function

' -----------------------------------------------------------------------------
' GetUserFullName(Byval UserName)
'
' Description: Fetches the full name of the current user
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetUserFullName(Byval UserName)

	Dim ProfileService, ProfileObj
	Dim fullName, firstName, lastName

	Set ProfileService = GetProfileServiceObject()

	' Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfile(UserName, "UserObject", False)	' GetUserProfileByLoginName(strUsername)
	if (ProfileObj is nothing) then
		Response.Redirect "Login.asp"
	end if

	firstName = ProfileObj.Fields("GeneralInfo").Value("first_name")
	lastName = ProfileObj.Fields("GeneralInfo").Value("last_name")

	Set ProfileObj = Nothing

	If IsNull(firstName) Then
		If Not IsNull(lastName) Then
			fullName = lastName
		Else
			fullName = ""
		End If

	ElseIf IsNull(lastName) Then
		fullName = firstName

	Else
		fullName =  firstName & " " & lastName
	End If

	GetUserFullName = fullName

  End Function


' -----------------------------------------------------------------------------
' GetWindowStyle(Byval UserName)
'
' Description: Fetches the windowstyle to the current user
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetWindowStyle(Byval UserName)

	Dim ProfileService, ProfileObj
	Dim style

	Set ProfileService = GetProfileServiceObject()

	' Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfile(UserName, "UserObject", False)	' GetUserProfileByLoginName(strUsername)
	if (ProfileObj is nothing) then
		Response.Redirect "Login.asp"
	end if

	style = ProfileObj.Fields("GeneralInfo").Value("WindowStyle")

	if IsNull(style) then
	'get the default page from the user's organization
		Dim oProfile, oOrg
		Dim idOrg
		Dim tmpArray

		'get orgprofile for current user
		idOrg = ProfileObj.AccountInfo.org_id
		Set oOrg = GetCompanyProfile(idOrg)

		if oOrg.GeneralInfo.default_page <> "" then
		'get the organizations default page
			tmpArray = Split(oOrg.GeneralInfo.default_page,"^",-1)

			ProfileObj.GeneralInfo.WindowStyle = tmpArray(0)

		else
		'get the NTB default page
			'get orgprofile for NTB
			Set oOrg = GetCompanyProfileByName("Norsk Telegrambyr�")

			tmpArray = Split(oOrg.GeneralInfo.default_page,"^",-1)

			ProfileObj.GeneralInfo.WindowStyle = tmpArray(0)
		end if

		'set the newsinfo
		style = tmpArray(0)
	end if

	Set ProfileObj = Nothing

	GetWindowStyle = style

  End Function


' -----------------------------------------------------------------------------
' GetNewsInfo(Byval UserName)
'
' Description: Fetches the newsinfo belonging to the current user
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function GetNewsInfo(Byval UserName)

	Dim ProfileService, ProfileObj
	Dim ninfo

	Set ProfileService = GetProfileServiceObject()

	' Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfile(UserName, "UserObject", False)	' GetUserProfileByLoginName(strUsername)
	if (ProfileObj is nothing) then
		Response.Redirect "Login.asp"
	end if

	ninfo = ProfileObj.Fields("GeneralInfo").Value("NewsInfo")

	if IsNull(ninfo) then
	'get the default page from the user's organization
		Dim oProfile, oOrg
		Dim idOrg
		Dim tmpArray

		'get orgprofile for current user
		idOrg = ProfileObj.AccountInfo.org_id
		Set oOrg = GetCompanyProfile(idOrg)

		if oOrg.GeneralInfo.default_page <> "" then
		'get the organizations default page
			tmpArray = Split(oOrg.GeneralInfo.default_page,"^",-1)

			ProfileObj.GeneralInfo.NewsInfo = tmpArray(1)

		else
		'get the NTB default page
			'get orgprofile for NTB
			Set oOrg = GetCompanyProfileByName("Norsk Telegrambyr�")

			tmpArray = Split(oOrg.GeneralInfo.default_page,"^",-1)

			ProfileObj.GeneralInfo.NewsInfo = tmpArray(1)
		end if

		'set the newsinfo
		ninfo = tmpArray(1)
	end if

	Set ProfileObj = Nothing

	GetNewsInfo = ninfo

  End Function

' -----------------------------------------------------------------------------
' UpdateWindowStyle(ByVal WindowStyle)
'
' Description: Saves the windowstyle to the profile in the Commerce Server
'
' Parameters:  WindowStyle, the style the user wants the personalized page to look like
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------


  Function UpdateWindowStyle(ByVal WindowStyle)

	Dim ProfileService, ProfileObj, UserName

	Set ProfileService = GetProfileServiceObject()
	Username = Session("Username")

	' Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfile(UserName, "UserObject", False)	' GetUserProfileByLoginName(strUsername)

	if not ProfileObj is nothing then
		ProfileObj.GeneralInfo.WindowStyle = WindowStyle
		ProfileObj.ProfileSystem.date_last_changed = Now()
		Call ProfileObj.Update()
		Set ProfileObj = Nothing
	end if

  End Function


' -----------------------------------------------------------------------------
' TrimNewsArray(ByVal WindowNumber)
'
' Description: Update the newsarray so it contain the right number news
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

    Sub TrimNewsArray(ByVal WindowNumber)

    Dim ProfileService, ProfileObj
    Dim UserName, OldNewsInfo, NewsInfoArray, NewsInfoString
  	Dim intWindowStyle, intWindows

  	'fetch the username
  	Username = Session("Username")

  	'fetch the windowstyle
  	intWindowStyle = WindowNumber

  	'get the old newsinfo from the profile
	OldNewsInfo = GetNewsInfo(Username)

  	'get the number of windows that should be present
  	Select case intWindowStyle
  		case 1
	  		intWindows = 2
  		case 2,4
  			intWindows = 3
  		case 3,5
  			intWindows = 4
  		case 6
  			intWindows = 5
  		case 7
  			intWindows = 6
  	end select

  	 'make it an integer
	intWindows = Cint(intWindows)

	if OldNewsInfo <> "" then
	'if there are any information present, split it in an array

		NewsInfoArray = Split(OldNewsInfo, "#", -1)
		'dimension the array to be the lenght of intWindows
		ReDim Preserve NewsInfoArray(intWindows)

		Dim i
		For i=0 to (intWindows-1)
			NewsInfoString = NewsInfoString & NewsInfoArray(i) &"#"
		Next
	end if

	Set ProfileService = GetProfileServiceObject()

	'Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfile(UserName, "UserObject", False)

	if not ProfileObj is nothing then
		ProfileObj.GeneralInfo.NewsInfo = NewsInfoString
		ProfileObj.ProfileSystem.date_last_changed = Now()

 		'WorkAround Error-Handling: RoV 2002.07.04
 		'Also added Primary key on "g_user_id" in "ntb_commerce..UserObject"
 		'to avoid multiple rows of same user, as result of this bug.
 		on error resume next
 		Call ProfileObj.Update()
 			if err.number = -1056948213 then
 				Response.Redirect "../error/ntb_error.asp?Err=newuser"
 			end if
 		on error goto 0
 		'End WorkAround

		Set ProfileObj = Nothing
	end if

  End Sub

' -----------------------------------------------------------------------------
' UpdateNewsInfo(ByVal NewsInfo)
'
' Description: Saves the newslist to the profile in the Commerce Server
'
' Parameters:  NewsInfo, the information about the user already chosen news
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------


  Function UpdateNewsInfo(ByVal NewsInfo, ByVal InfoPlace)

  	Dim ProfileService, ProfileObj, UserName
  	Dim OldNewsInfo, NewsInfoArray, NewsInfoString
  	Dim intWindowStyle, intWindows

  	'fetch the username
  	Username = Session("Username")

  	'fetch the windowstyle
  	intWindowStyle = GetWindowStyle(Username)

  	'get the number of windows that should be present
  	Select case intWindowStyle
  		case 1
	  		intWindows = 2
  		case 2,4
  			intWindows = 3
  		case 3,5
  			intWindows = 4
  		case 6
  			intWindows = 5
  		case 7
  			intWindows = 6
  	end select

  	'make it an integer
	intWindows = Cint(intWindows)

  	'get the old newsinfo from the profile
	OldNewsInfo = GetNewsInfo(Username)

	if OldNewsInfo <> "" then
	'if there are any information present, split it in an array
		NewsInfoArray = Split(OldNewsInfo, "#", -1)

		'dim a new array with the right dimension
		ReDim Preserve NewsInfoArray(intWindows)

	else
	'dim a new array
		ReDim NewsInfoArray(intWindows)
	end if

	'set the new entry in the array
	NewsInfoArray(InfoPlace-1) = NewsInfo

	Dim i
	For i=0 to (intWindows-1)
		NewsInfoString = NewsInfoString & NewsInfoArray(i) &"#"
	Next

	Set ProfileService = GetProfileServiceObject()

	'Get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfile(UserName, "UserObject", False)

	if not ProfileObj is nothing then
		ProfileObj.GeneralInfo.NewsInfo = NewsInfoString
		ProfileObj.ProfileSystem.date_last_changed = Now()
		Call ProfileObj.Update()
		Set ProfileObj = Nothing
	end if

  End Function

' -----------------------------------------------------------------------------
' UpdateProfile(ByVal UID)
'
' Description: Saves the various information to the profile in the Commerce Server
'
' Parameters:  UID, the user id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Sub UpdateProfile(ByVal UID)
	Dim ProfileService, ProfileObj
  	Dim firstname, middelname, lastname, email, phone, role

	'all the parameters from the editsite
	firstname = Request("txtFirstname")
	lastname = Request("txtLastname")
	email = Request("txtEmail")
	phone = Request("txtPhone")
	role = Request("txtRole")

	Set ProfileService = GetProfileServiceObject()

	'get UserProfileObj for the user
	Set ProfileObj = ProfileService.GetProfileByKey("user_id", UID, "UserObject")

	if not ProfileObj is nothing then
		ProfileObj.GeneralInfo.first_name = firstname
		ProfileObj.GeneralInfo.last_name = lastname
		ProfileObj.GeneralInfo.email_address = email
		ProfileObj.GeneralInfo.tel_number = phone
		ProfileObj.BusinessDesk.partner_desk_role = role
		'and more.....

		ProfileObj.ProfileSystem.date_last_changed = Now()

		'set the id of the updater
		Dim UserObj, userid

		UserObj = GetUserProfileByLoginName(Session("UserName"))
		userid = UserObj.GeneralInfo.user_id

		ProfileObj.GeneralInfo.user_id_changed_by = userid

		'WorkAround Error-Handling: RoV 2002.07.04
 		'Also added Primary key on "g_user_id" in "ntb_commerce..UserObject"
 		'to avoid multiple rows of same user, as result of this bug.
 		on error resume next
 		Call ProfileObj.Update()
 			if err.number = -1056948213 then
 				Response.Redirect "../error/ntb_error.asp?Err=creatednewuser"
 			end if
 		on error goto 0
 		'End WorkAround

		Set ProfileObj = Nothing
	end if

  End Sub

' -----------------------------------------------------------------------------
' UpdateOrg(ByVal CoID)
'
' Description: Saves the various information to the profile in the Commerce Server
'
' Parameters:  CoID, the organization id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function UpdateOrg(ByVal CoID)
	Dim ProfileService, ProfileObj, AdrObj
  	Dim orgName, name, contact, sPassword, sPasswordExpire, sAddress, sPostalCode, sCity, sPhone, rsAdr, idAdr, iCount

	'all the parameters from the editsite
	contact = Request("txtContact")
	sPassword = Request("txtPassword")
	sPasswordExpire = Request("txtPasswordExpire")
	sAddress = Request("txtAddress")
	sPostalCode = Request("txtPostalCode")
	sCity = Request("txtCity")
	sPhone = Request("txtPhone")

	'find the name of the profile
	'orgName = GetCompanyName(CoID) ->Trengs dette??

	'get ProfileObj for the organization
	Set ProfileObj = GetCompanyProfile(CoID)

	if not ProfileObj is nothing then
	'set profile information gathered from the site

		'get accesslevels
		Dim iAccessSum, iAccessCount
		if (Request("chkAccessRight")<> "") then
		'loop trough all the selected chkboxes and add to string
			For iAccessCount = 1 to Request("chkAccessRight").Count
				iAccessSum = iAccessSum & "+" & Request("chkAccessRight")(iAccessCount)
			Next
			iAccessSum = Right (iAccessSum,(Len(iAccessSum)-1))
		end if

		'set the right bitformat for the accesslevel
		Dim objFormatter, acf
		Set objFormatter = Server.CreateObject("Formating.bitwise")
		'objFormatter.LongToHTMLCheckboxArray(acf) will give an array of strings with some HTML code
		'objFormatter.LongToBitArray(acf) will give an array of intergers representing bit values

		acf = objFormatter.QueryStringToLong(CStr(iAccessSum))
		Set objFormatter = Nothing

		'update the password for the org and
		'all users belonging to this organization
		Dim strOldPass, strNewPass

		strOldPass = ProfileObj.GeneralInfo.org_password

		if strOldPass <> sPassword then
			strNewPass = Array(strOldPass, sPassword)

			Call ChangePassword(strNewPass, CoID)

		end if

		'update the profile-information
		ProfileObj.GeneralInfo.user_id_admin_contact = contact
		ProfileObj.GeneralInfo.accesslevel = acf
		ProfileObj.GeneralInfo.org_password = sPassword
		'if CInt(sPasswordExpire) = Application("DefaultNumberOfDaysBeforePasswordExpire") then
		'	ProfileObj.GeneralInfo.org_password_expire = 0
		'else
			ProfileObj.GeneralInfo.org_password_expire = sPasswordExpire
		'end if
		'and more.....

		'update profilesystem-information
		ProfileObj.ProfileSystem.date_last_changed = Now()

		'set the id of the updater
		Dim UserObj, userid, username
		username = Session("UserName")

		UserObj = GetUserProfileByLoginName(username)
		userid = UserObj.GeneralInfo.user_id

		ProfileObj.GeneralInfo.user_id_changed_by = userid

		'get the address
		Set rsAdr = GetAddressProfile(CoID, iCount)

		if iCount = 0 then
		'create Addressobject
			Dim orgid
			orgid = ProfileObj.GeneralInfo.org_id

			'unique idstring
			idAdr = MSCSGenID.GenGUIDString

			'make profileserviceobject and create the addressobject
			Set ProfileService = GetProfileServiceObject()
			Set AdrObj = ProfileService.CreateProfile(idAdr, "Address")
			AdrObj.GeneralInfo.id = orgid

		else
			'get the id for the addressobject
			idAdr = rsAdr.Fields("GeneralInfo.address_id")

			'make profileserviceobject and get the addressobject
			Set ProfileService = GetProfileServiceObject()
			Set AdrObj = ProfileService.GetProfile(idAdr, "Address")

		end if

		'update the addressinformation
		AdrObj.GeneralInfo.address_line1 = sAddress
		AdrObj.GeneralInfo.postal_code = sPostalCode
		AdrObj.GeneralInfo.city = sCity
		AdrObj.GeneralInfo.tel_number = sPhone

		'update system-information
		AdrObj.ProfileSystem.date_last_changed = Now()
		'AdrObj.GeneralInfo.user_id_changed_by =

		'update the objects and set them to nothing
		Call ProfileObj.Update()
		Call AdrObj.Update()

		Set ProfileObj = Nothing
		Set AdrObj = Nothing

	end if

  End Function

' -----------------------------------------------------------------------------
' Reactivate(ByVal CoID)
'
' Description: Reactivates the customer to be an active member of the portal
'
' Parameters:  UID, the user id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Sub Reactivate(ByVal CoID)
    Dim ProfileObj
    Dim UserObj, userid, username

	Set ProfileObj = GetCompanyProfile(CoID)

	if not ProfileObj is nothing then
	'set profile information gathered from the site

		'set the customer active
		ProfileObj.GeneralInfo.account_status = 1

		'update profilesystem-information
		ProfileObj.ProfileSystem.date_last_changed = Now()

		'set the id of the updater
		username = Session("UserName")

		UserObj = GetUserProfileByLoginName(username)
		userid = UserObj.GeneralInfo.user_id

		ProfileObj.GeneralInfo.user_id_changed_by = userid

		'update the objects and set them to nothing
		Call ProfileObj.Update()

		Set ProfileObj = Nothing
		Set UserObj = Nothing

	end if

  End Sub

' -----------------------------------------------------------------------------
' DeleteProfile(ByVal UID)
'
' Description: Deletes the profile in the Commerce Server
'
' Parameters:  UID, the user id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Sub DeleteProfile(ByVal UID)
	Dim ProfileService

	on error resume next
	Set ProfileService = GetProfileServiceObject()
	Call ProfileService.DeleteProfileByKey("user_id", UID, "UserObject")

  End Sub


' -----------------------------------------------------------------------------
' DeleteProfiles(ByVal UIDS)
'
' Description: Deletes the profiles in the Commerce Server
'
' Parameters:  UID, the users ids
'
' Returns:
'
' Notes :
'	This is no longer in use. (12.02.2008 - TH)
' -----------------------------------------------------------------------------

  Sub DeleteProfiles(ByVal UID)
	Dim ProfileService

	' on error resume next
	
	Set ProfileService = GetProfileServiceObject()
	
	
	for a = 0 to ai
		Call ProfileService.DeleteProfileByKey("user_id", UID, "UserObject")	
	next 
	
	
	' Here we have to find the different arrays - or we do a loop in other places in the code / portal. 
	

  End Sub
' -----------------------------------------------------------------------------
' DeleteCompany(ByVal CoID)
'
' Description: Sets the account_status to 0 (inactive) in the Commerce Server
'
' Parameters:  CoID, the company id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Sub DeleteCompany(ByVal CoID)
	Dim ProfileObj

	'not delete from database, but set inactive
	Set ProfileObj = GetCompanyProfile(CoID)
	ProfileObj.GeneralInfo.account_status = 0

	Call ProfileObj.Update()

  End Sub

' -----------------------------------------------------------------------------
' CreateUserProfile(ByVal CoID)
'
' Description: Creates a new userprofile in the Commerce Server
'
' Parameters: CoID - the organizations id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function CreateUserProfile(ByVal CoID)
    Dim ProfileService, ProfileObj
  	Dim sOrg, username, password, firstname, lastname, email, phone, role
  	Dim sGroup

	'get the organizations name
	sOrg = GetCompanyName(CoID)

	'all the parameters from the editsite
	username = Request("txtUsername")
	firstname = Request("txtFirstname")
	lastname = Request("txtLastname")
	email = Request("txtEmail")
	phone = Request("txtPhone")
	role = Request("txtRole")

	Set ProfileService = GetProfileServiceObject()

	'verify if user already exists
	Set ProfileObj = ProfileService.GetProfile(username, "UserObject", false)

	if not ProfileObj is nothing then
		'user already exist
		CreateUserProfile = False
		Exit Function
	end if

	'On Error Resume Next
    Set ProfileObj = ProfileService.CreateProfile(username, "UserObject")

    If Err.number <> 0 then
        Dim nErrNumber, sErrSource, sErrDesc

        nErrNumber  = Err.Number
        sErrSource  = Err.Source
        sErrDesc    = Err.Description

        'If profile already exist an error code of 'C100400B' is returned.
        'In this case we should not delete the profile
        If Hex(Err.number) <> "C100400B" Then
            'If error occured and we have multiple sources in the profile
            'we may need to clean up.
            'The error returned from DeleteProfile should be ignored
            Call ProfileService.DeleteProfile(username, "UserObject")
        End If

        CreateUserProfile = False

        Err.Raise nErrNumber, sErrSource, sErrDesc
        'Exit Function
    End If

    'set the id of the updater
    Dim UserObj, userid

    UserObj = GetUserProfileByLoginName(Session("UserName"))
    userid = UserObj.GeneralInfo.user_id

    'get an unique id
    ProfileObj.GeneralInfo.user_id = MSCSGenID.GenGUIDString

    'Set profile properties
    ProfileObj.GeneralInfo.user_type = 1 'REGISTERED_PROFILE
    ProfileObj.AccountInfo.date_registered = Date()
    ProfileObj.AccountInfo.account_status = 1 'ACCOUNT_ACTIVE
    ProfileObj.ProfileSystem.date_last_changed = Date()

    'get the organization-profile
    Dim objOrgProfile, sPassword
    Set objOrgProfile = GetCompanyProfile(CoID)

    if role = "2" then
        'set the right AD-group -> _ADMINGROUP
        sGroup = sOrg & "_ADMINGROUP"

        'set an property in the organization-profile that tells that
        'this organization has an administrator
        objOrgProfile.GeneralInfo.admin = 1

        call objOrgProfile.Update()

    else
		'set the right AD-group -> _USERGROUP
        sGroup = sOrg & "_USERGROUP"
	end if

    'set the password (same for all in the org)
    sPassword = objOrgProfile.GeneralInfo.org_password

    ProfileObj.GeneralInfo.user_security_password = sPassword

    'set the id of the updater
    ProfileObj.GeneralInfo.user_id_changed_by = userid

    'set profile information gathered from the site
    ProfileObj.AccountInfo.org_id = CoID
    ProfileObj.GeneralInfo.first_name = firstname
	ProfileObj.GeneralInfo.last_name = lastname
	ProfileObj.GeneralInfo.email_address = email
	ProfileObj.GeneralInfo.tel_number = phone
	ProfileObj.BusinessDesk.partner_desk_role = Cint(role)
	ProfileObj.AccountInfo.password_expire_date = Cstr(Date())
	'and more.....

	'set properties into the active directory
	ProfileObj.ProfileSystem.sam_account_name = username
	ProfileObj.ProfileSystem.ParentDN = "OU=" & sOrg & ",OU=" _
										& Application("MSCSCommerceSiteName2") & "," & "OU=MSCS_40_Root"
	ProfileObj.ProfileSystem.user_account_control = 512 'AD_USER_ACTIVE

    If Err.number <> 0 then
    'problems with making the user
       CreateUserProfile = false
    Else
    'update the profile and set the right AD-group

		Dim sUserDN, sUserDN2

		sUserDN2= "OU=" & sOrg & "," _
		& "OU=" & Application("MSCSCommerceSiteName2")  & "," _
		& "OU=MSCS_40_Root" & "," & MSCSActiveDirectoryDomain

		sUserDN = "CN=" & username & "," & sUserDN2

		on error resume next
        Call ProfileObj.Update()

        If Err.number <> 0 Then ' c1003e84
			'problems with making the user
			CreateUserProfile = BugFixCommerce(sUserDN2, username, sGroup, sPassword)
		Else

			if SetAD(sUserDN, sGroup) then
				CreateUserProfile = True
			else
				CreateUserProfile = False
			end if
		end if
    End If

End Function


' -----------------------------------------------------------------------------
' SetAD(ByVal sUserDN, ByVal sADgroup)
'
' Description: Saves the user in the right AD-group
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function SetAD(ByVal sUserDN, ByVal sADgroup)

	Dim objProfile

	Set objProfile = GetADProfileByName(sADgroup)

	'if not IsEmpty(objProfile) then
	if not objProfile is nothing then
	'set the profile in the right AD-group
		Dim members

		' Get existing members of group
		members = objProfile("GeneralInfo.member").Value

		' ReDim the members array and append the new value
		If Not IsNull(members) Then
			If Not IsEmpty(members) Then
				If UBound(members) >= 0 Then
					ReDim Preserve members(UBound(members) + 1)
					members(UBound(members)) = sUserDN
				End If
			End If
		Else
			ReDim members(0)
			members(0) = sUserDN
		End If

		' Assign the new members array to the property
		objProfile("GeneralInfo.member").value = members

		' Update the Active Directory group profile with new member values
		On Error Resume Next
		Call objProfile.Update()
		If Err.number Then
			SetAD = False
		else
			SetAD = True
		End if
	end if

End Function

' -----------------------------------------------------------------------------
' CreateOrgProfile()
'
' Description: Creates a new organization profile in the Commerce Server
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

  Function CreateOrgProfile()
    Dim ProfileService, ProfileObj, AdrObj
  	Dim name, password, passwordExpire, contact, orgid, sAddress, sPostalCode, sCity, sPhone, idAdr

	'all the parameters from the editsite
	name = Request("txtName")
	password = Request("txtPassword")
	passwordExpire = Request("txtPasswordExpire")
	contact = Request("txtContact")
	sAddress = Request("txtAddress")
	sPostalCode = Request("txtPostalCode")
	sCity = Request("txtCity")
	sPhone = Request("txtPhone")

	Set ProfileService = GetProfileServiceObject()

	'verify if user already exists
	Set ProfileObj = ProfileService.GetProfile(name, "Organization", false)

	if not ProfileObj is nothing then
		'user already exist
		CreateOrgProfile = False
		Exit Function
	end if

	'make profile
	'On Error Resume Next
    Set ProfileObj = ProfileService.CreateProfile(name, "Organization")
    orgid = MSCSGenID.GenGUIDString
    ProfileObj.GeneralInfo.org_id = orgid

    If Err.number <> 0 then
        Dim nErrNumber, sErrSource, sErrDesc

        nErrNumber  = Err.Number
        sErrSource  = Err.Source
        sErrDesc    = Err.Description

        'If profile already exist an error code of 'C100400B' is returned.
        'In this case we should not delete the profile
        If Hex(Err.number) <> "C100400B" Then
            'If error occured and we have multiple soruces in the profile
            'we may need to clean up.
            'The error returned from DeleteProfile should be ignored
            Call ProfileService.DeleteProfile(name, "Organization")
        End If

        CreateUserProfile = False
        Err.Raise nErrNumber, sErrSource, sErrDesc
        Exit Function
    End If

    'Set profile properties
    ProfileObj.GeneralInfo.account_status = 1
    'objOrgProfile.GeneralInfo.admin = 0
    ProfileObj.GeneralInfo.admin = 0
    ProfileObj.GeneralInfo.org_password = password
	ProfileObj.GeneralInfo.org_password_expire = PasswordExpire
    ProfileObj.ProfileSystem.date_created = Now()
    ProfileObj.ProfileSystem.date_last_changed = Now()

    'set the id of the updater
    Dim UserObj, userid

    UserObj = GetUserProfileByLoginName(Session("UserName"))
    userid = UserObj.GeneralInfo.user_id

    ProfileObj.GeneralInfo.user_id_changed_by = userid

    'set profile information gathered from the site
    'get accesslevels
	Dim iAccessSum, iAccessCount
	If (Request("chkAccessRight")<> "") then
	'loop trough all the selected chkboxes and add to string
		For iAccessCount = 1 to Request("chkAccessRight").Count
			iAccessSum = iAccessSum & "+" & Request("chkAccessRight")(iAccessCount)
		Next
		iAccessSum = Right (iAccessSum,(Len(iAccessSum)-1))
	end if

	'set the right bitformat for the accesslevel
	Dim objFormatter, acf
	Set objFormatter = Server.CreateObject("Formating.bitwise")

	acf = objFormatter.QueryStringToLong(CStr(iAccessSum))
	Set objFormatter = Nothing

    'set information gather from the site
    ProfileObj.GeneralInfo.name = name
	ProfileObj.GeneralInfo.user_id_admin_contact = contact
	ProfileObj.GeneralInfo.accesslevel = acf
	'and more.....

    Call ProfileObj.Update()

	'create ADGroups
	Call CreateADGroup(name)

	'create Addressobject
	idAdr = MSCSGenID.GenGUIDString
	Set AdrObj = ProfileService.CreateProfile(idAdr, "Address")

	'set the addressinformation
    AdrObj.GeneralInfo.id = orgid
    AdrObj.GeneralInfo.address_line1 = sAddress
    AdrObj.GeneralInfo.postal_code = sPostalCode
    AdrObj.GeneralInfo.city = sCity
    AdrObj.GeneralInfo.tel_number = sPhone

    AdrObj.ProfileSystem.date_created = Now()
    AdrObj.ProfileSystem.date_last_changed = Now()

    Call AdrObj.Update()
	Set ProfileObj = Nothing
	Set AdrObj = Nothing

    If Err.number <> 0 then
        CreateOrgProfile = false
    Else
        CreateOrgProfile = True
    End If

  End Function


' -----------------------------------------------------------------------------
' CreateADGroup(ByVal sName)
'
' Description: Creates the Active directory groups to an organization
'				_USERGROUP and _ADMINGROUP
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function CreateADGroup(ByVal sName)

    Dim oProfileService
    Dim oAdminProfile, oUserProfile

    If Len(sName) > 0 Then
        Set oProfileService = GetProfileServiceObject()

        'make admingroup
		Set oAdminProfile = oProfileService.CreateProfile(sName & "_ADMINGROUP", "ADGroup")

		if not oAdminProfile is nothing then
		'set properties in the active directory
			oAdminProfile.GeneralInfo.SamAccountName = sName & "_AG"
			oAdminProfile.GeneralInfo.GroupType = -2147483646
			oAdminProfile.GeneralInfo.ProfileID = MSCSGenID.GenGUIDString
			oAdminProfile.ProfileSystem.ParentDN = "OU=" & sName & ",OU=" _
										& Application("MSCSCommerceSiteName2") & "," & "OU=MSCS_40_Root"

			oAdminProfile.Update()

		end if

		'make usergroup
		Set oUserProfile = oProfileService.CreateProfile(sName & "_USERGROUP", "ADGroup")

		if not oUserProfile is nothing then
		'set properties in the active directory
			oUserProfile.GeneralInfo.SamAccountName = sName & "_UG"
			oUserProfile.GeneralInfo.GroupType = -2147483646
			oUserProfile.GeneralInfo.ProfileID = MSCSGenID.GenGUIDString
			oUserProfile.ProfileSystem.ParentDN = "OU=" & sName & ",OU=" _
										& Application("MSCSCommerceSiteName2") & "," & "OU=MSCS_40_Root"

			oUserProfile.Update()

		end if

		'set security properties(ACL/ACE)
		Dim objDSE, ldapConn, oAD, secDesc, oDACL, newAce1, newAce2

		'get default domain
		Set objDSE = GetObject("LDAP://rootDSE")

		'construct connectionstring
		ldapConn = "LDAP://OU=" & sName & ",OU=" & Application("MSCSCommerceSiteName2") _
				& ",OU=MSCS_40_Root," & objDSE.Get("defaultNamingContext")

		'Bind to the specified object
		Set oAD = GetObject(ldapConn)

		'Read the security descriptor on the object
		Set secDesc = oAD.Get("ntSecurityDescriptor")

		'Get the DACL from the security descriptor.
		Set oDACL = secDesc.DiscretionaryAcl

		Set newAce1 = Server.CreateObject("AccessControlEntry")
		Set newAce2 = Server.CreateObject("AccessControlEntry")

		'set the admin-rights
		newAce1.AceType = ADS_ACETYPE_ACCESS_ALLOWED
		newAce1.AccessMask = ADS_RIGHT_GENERIC_ALL
		newAce1.AceFlags = ADS_ACEFLAG_INHERIT_ACE
		newAce1.Trustee = Application("PortalDomainName") & "\" & sName & "_AG"

		'add the ACE to the DACL
		oDACL.AddAce(newAce1)

		'set the user-rights
		newAce2.AceType = ADS_ACETYPE_ACCESS_ALLOWED
		newAce2.AccessMask = ADS_RIGHT_DS_READ_PROP
		newAce2.Trustee = Application("PortalDomainName") & "\" & sName & "_UG"
		newAce2.AceFlags = ADS_ACEFLAG_INHERIT_ACE + ADS_ACEFLAG_INHERIT_ONLY_ACE

		'add the ACE to the DACL
		oDACL.AddAce(newAce2)

		'add the ACE to the security descriptor
		secDesc.DiscretionaryAcl = oDACL

		oAD.Put "ntSecurityDescriptor", secDesc
		oAD.SetInfo

    End If

	Set oAdminProfile = Nothing
	Set oUserProfile = Nothing

End Function


' -----------------------------------------------------------------------------
'ChangePassword(ByVal strNewPass, ByVal idOrg)
'
' Description: Changes the password for all users in the organization
'
' Parameters:  strNewPass - the new password
'				idOrg - the org id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function ChangePassword(ByVal arrNewPass, ByVal idOrg)
  	Dim idChangeUser, idUser, iUserCount
  	Dim rsUsers, objProfile, objUserProfile, objOrgProfile
  	Dim strSubject, strContent

  	'get the user that changes the password
	Set objProfile = GetUserProfileByLoginName(Session("UserName"))
	idChangeUser = objProfile.GeneralInfo.user_id

	'update the organizations profile
	Set objOrgProfile = GetCompanyProfile(idOrg)
	objOrgProfile.GeneralInfo.org_password = arrNewPass(1)
	objOrgProfile.GeneralInfo.user_id_changed_by = idChangeUser
	objOrgProfile.ProfileSystem.date_last_changed = Now()

	'and update
	objOrgProfile.Update()

	Set rsUsers = RetrieveCompanyUsers(idOrg, iUserCount)

	if iUserCount <> "0" then
	'if there are records given back
	  	while not rsUsers.EOF
	  	'as long as there are users left that has not updated their password

			'must have profileobject to update correctly
			idUser = rsUsers.fields("GeneralInfo.user_id")

			Set objUserProfile = GetUserProfile(idUser)

			if not objUserProfile is nothing then

				on error resume next
				'objUserProfile.GeneralInfo.user_security_password = arrNewPass

					'problems with changing the password ->bugfix

					Dim strUser, sOrg, ldapConn
					Dim strOldPassword, strNewPassword
					Dim sUserDN, oAD

					strOldPassword = arrNewPass(0)
					strNewPassword = arrNewPass(1)

					strUser = rsUsers.fields("GeneralInfo.logon_name")
					sOrg = objOrgProfile.fields("GeneralInfo.name")

					ldapConn = "OU=" & sOrg & "," _
						& "OU=" & Application("MSCSCommerceSiteName2")  & "," _
						& "OU=MSCS_40_Root" & "," & MSCSActiveDirectoryDomain

					sUserDN = "LDAP://CN=" & strUser & "," & ldapConn

					'Bind to the specified object
					Set oAD = GetObject(sUserDN)

					oAD.SetPassword strNewPassword
					' If something failed, we'll try to set the password
					' with another method.
					if err.number <> 0 then
						oAD.ChangePassword strOldPassword, strNewPassword
					end if
					Set oAD = Nothing
					'end of bugfix

				objUserProfile.GeneralInfo.user_id_changed_by = idChangeUser
				objUserProfile.ProfileSystem.date_last_changed = Now()
				objUserProfile.AccountInfo.password_expire_date = Cstr(Date())
				objUserProfile.AccountInfo.password_warning = "0"

				'update the user
				Call objUserProfile.Update()

				' Send mail to inform about new password
				if len(objUserProfile.GeneralInfo.email_address) > 0 then
					strSubject = "Passord for NTB Portalen har blitt endret"
					strContent = "Ditt nye passord er: " & strNewPassword
					SendMail objUserProfile.GeneralInfo.email_address, strSubject, strContent, 1, ""
				end if
			end if

			'go to next user
			rsUsers.MoveNext
		wend
	end if

End Function

Function BugFixCommerce(ldapConn, strUser, strUserGroup, strPassword)

	Dim sUserDN, GroupDN
	Dim oAD, oGroupCompany, flag, newFlag

	sUserDN = "LDAP://CN=" & strUser & "," & ldapConn

	'Bind to the specified object
	Set oAD = GetObject(sUserDN)

	oAD.ChangePassword "", strPassword

	flag = oAD.Get("userAccountControl")
	newFlag = flag Or &H10000 'ADS_UF_DONT_EXPIRE_PASSWD
	oAD.Put "userAccountControl", newFlag
	oAD.SetInfo

	GroupDN = "LDAP://CN=" & strUserGroup & "," & ldapConn
	Set oGroupCompany = GetObject(GroupDN)

	oGroupCompany.Add (sUserDN)

	Set oGroupCompany = Nothing
	Set oAD = Nothing

	BugFixCommerce = True

End Function


Function VerifyProfile(oProfile)

	if oProfile is nothing then
		VerifyProfile = False
	end if

	Dim rsUser
	' Set rsUser = GetUserProfile(UID)
	Set rsUser = oProfile
	' Verify that we have all mandatory fields.
	if len(rsUser.fields("GeneralInfo.first_name") & "") = 0 or _
	   len(rsUser.fields("GeneralInfo.last_name") & "") = 0 or _
	   len(rsUser.fields("GeneralInfo.email_address") & "") = 0 or _
	   len(rsUser.fields("GeneralInfo.tel_number") & "") = 0 then
		VerifyProfile = false
	else
		VerifyProfile = true
	end if

End Function

Function ForgotPassword(strLogonUser, bEmail, bSMS)

	Dim oUserProfile, oCompanyProfile
	Dim strPassword, strEmail, strPhone, strSubject, strContent
	Dim strReturn, bErrEmail, bErrSMS

	bErrEmail = false
	bErrSMS = false

	Set oUserProfile = GetUserProfileByLoginName(strLogonUser)
	Set oCompanyProfile = GetCompanyProfile(oUserProfile.Fields("AccountInfo").Value("org_id"))

	strPassword = oCompanyProfile.Fields("GeneralInfo").Value("org_password")
	strPhone = oUserProfile.Fields("GeneralInfo").Value("tel_number")
	strEmail = oUserProfile.Fields("GeneralInfo.email_address")

	if len(strPassword) = 0 then
		Response.Redirect "error.asp?Err=password"
		Exit Function
	end if

	if bEmail then
		if len(strEmail) > 0 then
			strSubject = "Login informasjon til NTB Portalen"
			strContent = "Ditt passord er: " & strPassword
			if SendMail(strEmail, strSubject, strContent,1,"") then
				strReturn = "Passordet er sendt p� e-post."
				' Log success
			else
				bErrEmail = true
				' Log error
			end if
		else
			strReturn = "E-post ikke sendt fordi du mangler e-post i profilen din."
		end if
	end if

	if bSMS then
		if len(strPhone) > 0 then
			strContent = "Login informasjon til NTB Portalen. Ditt passord er: " & strPassword
			if SendSMS(strPhone, strContent) then
				' Log success
				if Len(strReturn) > 0 then strReturn = strReturn & "<BR>"
				strReturn = strReturn & "Passordet er sendt p� SMS."
			else
				bErrSMS = true
				' Log error
			end if
		else
			if Len(strReturn) > 0 then strReturn = strReturn & "<BR>"
			strReturn = strReturn & "SMS ikke sendt fordi du mangler mobilnummer i profilen din."
		end if
	end if

	if bErrEmail then
		Response.Redirect "error.asp?Err=sendemail"
	elseif bErrSMS then
		Response.Redirect "error.asp?Err=sendsms"
	end if

	ForgotPassword = strReturn
End Function

Function RegisterUser(strName, strEmail, strMobile, strCompany)

	Dim strReturn
	Dim strSubject, strContent

	strSubject = strName & " fra " & strCompany & " vil bli registrert bruker i NTB Portalen"
	strContent = strName & " fra " & strCompany & " �nsker � registrere seg som bruker i NTB Portalen." & vbCrLf & vbCrLf


	strContent = strContent & "Navn: " & strName & vbCrLf
	strContent = strContent & "Firma: " & strCompany & vbCrLf
	strContent = strContent & "Epost: " & strEmail & vbCrLf
	strContent = strContent & "Mobilnummer: " & strMobile & vbCrLf
	strContent = strContent & vbCrLf & "Denne meldingen er sendt fra NTBs Nyhetsportal." & vbCrLf

	if SendMail("marked@ntb.no", strSubject, strContent,1,strEmail) then
		strReturn = "Din foresp�rsel er sendt! NTBs markedsavdeling vil ta kontakt med deg s� snart som mulig."
	else
		Response.Redirect "error.asp?Err=password"
		Exit Function
	end if

	RegisterUser = strReturn
End Function

Function SendMail(strEmailAddress, strSubject, strContent, strCount, strSender)

	Dim strDirectory, strFileMail
	Dim objFSO, objFile

	if strSender = "" then
		strSender = Application("EmailFromAddress")
	end if

	strDirectory = ProperPath(Application("EMailOutputDirectory"))
	if len(strDirectory) = 0 then
		SendMail = false
		Exit function
	end if

	strFileMail = FormatDateTime(Now, 2) & "-" & FormatDateTime(Now, 3) & "-" & strEmailAddress & "-" & strCount & ".txt"

	' A simple check to make sure the filename is valid.
	strFileMail = ProperFilename(strFileMail)
	'response.write(strFileMail & "<br>")
	'response.flush()

	Set objFSO = GetFileSystemObject()
	Set objFile = objFSO.CreateTextFile(strDirectory & strFileMail, true)

	objFile.WriteLine "From:" & strSender
	objFile.WriteLine "To:" & strEmailAddress
	objFile.WriteLine "Subject: " & strSubject & vbcrlf
	objFile.WriteLine strContent
	objFile.Close

	SendMail = true

End Function

Function SendSMS(strPhoneTo, strMessage)

	Dim strDirectory, strFileSMS
	Dim objFSO, objFile

	strDirectory = ProperPath(Application("SMSOutputDirectory"))
	if len(strDirectory) = 0 then
		SendSMS = false
		Exit function
	end if

	strFileSMS = "sm" & FormatDateTime(Now, 2) & " " & FormatDateTime(Now, 3) & " " & strPhoneTo & ".gsm"

	strFileSMS = ProperFilename(strFileSMS)

	Set objFSO = GetFileSystemObject()
	Set objFile = objFSO.CreateTextFile(strDirectory & strFileSMS, true)

	objFile.WriteLine "[SMS]"
	objFile.WriteLine "To=" & strPhoneTo
	objFile.WriteLine "From=" & Application("SMSFromText")
	objFile.Write "Text=" & strMessage
	objFile.Close

	SendSMS = true

End Function

' Sending mail to all companies in a string.
Function SendMailToCompanies(strSelectedCompany, strSubject, strContent, strAdminsOnly)

	Dim arrCompany, rsCompany, strOrgId, rsUser
	Dim i, iTotalCount, iCountMailSent

	Dim strCount
	strCount = 1

	iCountMailSent = 0
	Set rsCompany = RetrieveAllActiveCompany(iTotalCount)
	arrCompany = Split(strSelectedCompany, ", ")
	do until rsCompany.EOF
		strOrgId = Trim(rsCompany.Fields("GeneralInfo.org_id"))
		' Check if this org is one of the selected ones.
		for i = lbound(arrCompany) to ubound(arrCompany)
			if strOrgId = arrCompany(i) then
				Set rsUser = RetrieveCompanyUsers(strOrgId, iTotalCount)
				do until rsUser.eof
					if len(rsUser.Fields("GeneralInfo.email_address")) > 0 then
						if CInt(strAdminsOnly) = rsUser.Fields("BusinessDesk.partner_desk_role") Or CInt(strAdminsOnly) = 0 then
							if SendMail(rsUser.Fields("GeneralInfo.email_address"), strSubject, strContent, strCount,"") then
								strCount = strCount + 1
								iCountMailSent = iCountMailSent + 1
							end if
						end if
					end if
					rsUser.MoveNext
				loop
			end if
		next
		rsCompany.MoveNext
	loop

	SendMailToCompanies = iCountMailSent
End Function

' A simple check to make sure the path is ending with a slash or backslash.
Function ProperPath(ByRef strDirectory)

	if len(strDirectory) = 0 then
		ProperPath = ""
		Exit Function
	end if

	if right(strDirectory, 1) <> "\" or right(strDirectory, 1) <> "/" then
		ProperPath = strDirectory & "\"
	else
		ProperPath = strDirectory
	end if

End Function

' A simple check to make sure the filename is valid.
' Could be updated further.
Function ProperFilename(ByRef strFilename)

	if len(strFilename) = 0 then
		ProperFilename = ""
		Exit Function
	end if

	strFilename = Replace(strFilename, "/", "")
	strFilename = Replace(strFilename, ":", ".")

	ProperFilename = strFilename

End Function

Function GetPasswordExpire(ByRef vPasswordExpire)

	if IsNull(vPasswordExpire) then
		GetPasswordExpire = Application("DefaultNumberOfDaysBeforePasswordExpire")
	else
		if vPasswordExpire = 0 then
			GetPasswordExpire = Application("DefaultNumberOfDaysBeforePasswordExpire")
		else
			GetPasswordExpire = vPasswordExpire
		end if
	end if

End Function

%>
