<%
' =============================================================================
' Form_Lib.asp
' Function library for building and handling forms
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' IsFormSubmitted
'
' Description:
'	Call IsFormSubmitted to determine if the form is being submitted
'	If it is being submitted, typically you call:
'		GetSubmittedFieldValues(listFlds)
'		GetFieldsErrorDictionary(listFlds, dictFieldVals)  (will return an
'			empty dictionary with no errors)
'		... and save if validation passes (it is up to the page to implement
'			cross-field validation)
'		Render the form with htmRenderFillOutForm(urlAction, listFields,
'			dictFieldVals, dictMessages) with either new data, or the posted
'			data which was deemed invalid.
'
'	Define forms in Global.asa, under dictForms
'	Each form is described by a simplelist of dictionaries (listFlds)
'   Each dictionary represents a form field, and can specify:
'     Name, Label, Size, MaxLength, MinLength, InputType (TEXTBOX, PASSWORD,
'		LISTBOX, CHECKBOX)
'     ErrorMessage, IsRequired, Pattern (RegEx validation), PatternErrorMessage,
'     RawInput, ListItems/ListValues
'
' Parameters:
'
' Returns:
'
' Notes :
'	At runtime, the form values are passed amongst the functions in
'	dictFieldVals, a dictionary of field values, indexed by field name
' -----------------------------------------------------------------------------
Function IsFormSubmitted()
    If IsNull(MSCSAppFrameWork.RequestNumber(MODE, Null)) Then
		IsFormSubmitted = False
    Else
		IsFormSubmitted = True
	End If
End Function


' -----------------------------------------------------------------------------
' GetSubmittedFieldValues
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSubmittedFieldValues(listFlds)
	Dim dictVals, dictFld

	Set dictVals = GetDictionary()
	
	For Each dictFld In listFlds
		If IsNull(dictFld.RawInput) Then
			dictVals.Value(dictFld.Name) = GetRequestString(dictFld.Name, "")
		Else
			dictVals.Value(dictFld.Name) = MSCSAppFramework.RequestString(dictFld.Name, "")
		End If
	Next
	
	Set GetSubmittedFieldValues = dictVals
End Function


' -----------------------------------------------------------------------------
' GetFieldsErrorDictionary
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetFieldsErrorDictionary(listFields, dictFieldVals)
	Dim dictField, dictErrors, oRegEx
    Dim i, sValue
    
	Set dictErrors = GetDictionary()

    For Each dictField In listFields
		If dictField.IsRequired Then
			If IsNull(dictFieldVals(dictField.Name)) Then
				dictErrors(dictField.Name) = dictField.ErrorMessage
	        Else
				Select Case dictField.InputType
				    Case TEXTBOX, PASSWORD
						If Len(dictFieldVals(dictField.Name)) > dictField.MaxLength Then
							dictErrors(dictField.Name) = dictField.ErrorMessage
							dictFieldVals(dictField.Name) = Left(dictFieldVals(dictField.Name), dictField.MaxLength)
						ElseIf Len(dictFieldVals(dictField.Name)) < dictField.MinLength Then
							dictErrors(dictField.Name) = dictField.ErrorMessage
						End If 
					Case LISTBOX
						If dictFieldVals(dictField.Name) = "-1" Then
							dictErrors(dictField.Name) = dictField.ErrorMessage
						ElseIf Not IsEntityInList(dictFieldVals(dictField.Name), dictField.ListValues) Then
							dictErrors(dictField.Name) = dictField.ErrorMessage
						End If
				End Select
			End If
		End If

		If Not IsNull(dictField.Pattern) Then
			If IsEmpty(oRegEx) Then
				Set oRegEx = New RegExp
			End If
                       
			If Not bMatchesPattern(oRegEx, dictFieldVals(dictField.Name), dictField.Pattern) Then 
				If Not IsNull(dictField.PatternErrorMessage) Then
					dictErrors(dictField.Name) = dictField.PatternErrorMessage
				Else
					dictErrors(dictField.Name) = dictField.ErrorMessage
				End If
			End If
		End If
	Next
	
	Set GetFieldsErrorDictionary = dictErrors
End Function


' -----------------------------------------------------------------------------
' bMatchesPattern
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bMatchesPattern(oRegEx, sValue, sPattern)
	Dim oMatches

	oRegEx.IgnoreCase = True
	oRegEx.Global = False
	oRegEx.Pattern = sPattern

	Set oMatches = oRegEx.Execute(sValue)
	If oMatches.Count = 1 Then
        	bMatchesPattern = sValue = oMatches.Item(0)
	Else
	        bMatchesPattern = False
	End If
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderFillOutForm
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderFillOutForm(ByVal urlAction, ByVal sFormName, dictFieldVals, dictMessages)
	Const MODE = "mode"

	Dim listFields, dictField, style, bChecked
	Dim htmCol1, htmCol2, htmRows, htmSubmitBtn, htmResetBtn, htmBody
	Dim i, arrDataAttLists, sLabel, sValue, sBtnText, sSelectedOption, iSize, bMultiple
	
	iSize = 1
	bMultiple = False

    ' Get the form definition
    Set listFields = Application("MSCSForms").Value(sFormName)
    
    htmRenderFillOutForm = Null
    
    ' No need to render a blank form if it is already cached.
    If dictFieldVals Is Nothing Then
		htmRenderFillOutForm = LookupCachedFragment("StaticSectionsCache", sFormName)
	End If
	
	' If form is not blank or that blank but not cached
	If IsNull(htmRenderFillOutForm) Then
		For i = 0 To listFields.Count - 1
			Set dictField = listFields(i)
    
			sLabel = dictField.Label
			If dictFieldVals Is Nothing Then
				sValue = ""
			Else
				sValue = dictFieldVals.Value(dictField.Name)
				If Not IsNull(sValue) And Not IsNull(dictField.RawInput) Then ' if this field takes raw input, don't echo back <>"
					sValue = Replace(sValue, "<", "")
					sValue = Replace(sValue, ">", "")
					sValue = Replace(sValue, """", "")
				End If
			End If
			
		    Select Case dictField.InputType
		        Case TEXTBOX
					htmCol1 = RenderText(FormatOutput(LABEL_TEMPLATE, Array(sLabel)), MSCSSiteStyle.Body)
			        htmCol2 = RenderTextBox(dictField.Name, sValue, dictField.Size, dictField.MaxLength, MSCSSiteStyle.TextBox)
		        Case PASSWORD
					htmCol1 = RenderText(FormatOutput(LABEL_TEMPLATE, Array(sLabel)), MSCSSiteStyle.Body)
			        htmCol2 = RenderPasswordBox(dictField.Name, sValue, dictField.Size, dictField.MaxLength, MSCSSiteStyle.PasswordBox)
		        Case LISTBOX
					htmCol1 = RenderText(FormatOutput(LABEL_TEMPLATE, Array(sLabel)), MSCSSiteStyle.Body)
					sSelectedOption = sValue
					htmCol2 = RenderListBoxFromSimpleList(dictField.Name, dictField.ListItems, dictField.ListValues, sSelectedOption, iSize, bMultiple, MSCSSiteStyle.ListBox)
				Case CHECKBOX	
		            If sValue = "" Then
						bChecked = False
					Else
						bChecked = True
					End If
		            Rem Swap column 1 and column2.
		            htmCol1 = RenderCheckBox(dictField.Name, dictField.DefaultValue, bChecked, MSCSSiteStyle.CheckBox)
					htmCol2 = RenderText(sLabel, MSCSSiteStyle.Body)            
		    End Select
		    
			' Display the message (if any) beneath the field.
		    If Not dictMessages Is Nothing Then
				If dictMessages.Count > 0 Then
					If Not IsNull(dictMessages.Value(dictField.Name)) Then
					    htmCol2 = htmCol2 & BR & RenderText(FormatOutput(CONTROL_ERROR_TEMPLATE, Array(mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage), dictMessages.Value(dictField.Name))), MSCSSiteStyle.Warning)
					End If  
				End If
			End If
			
			arrDataAttLists = Array(MSCSSiteStyle.TDRight, MSCSSiteStyle.TDLeft)
			htmRows = htmRows & RenderTableDataRow(Array(htmCol1, htmCol2), arrDataAttLists, MSCSSiteStyle.TRTop)
		Next
    
		htmRows = htmRows & RenderHiddenField(MODE, 1)
	
		sBtnText = mscsMessageManager.GetMessage("L_Submit_Button", sLanguage)
		htmSubmitBtn = RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)
		
		sBtnText = mscsMessageManager.GetMessage("L_Reset_Button", sLanguage)
		htmResetBtn = RenderResetButton(RESET_BUTTON, sBtnText, MSCSSiteStyle.Button)
		
		htmRows = htmRows & RenderTableDataRow(Array("", htmResetBtn & " " & htmSubmitBtn), GetRepeatStyle(2, MSCSSiteStyle.TDLeft), MSCSSiteStyle.TRTop)
	
		htmBody = RenderTable(htmRows, MSCSSiteStyle.NoBorderTable)
		htmRenderFillOutForm = RenderForm(urlAction, htmBody, HTTP_POST)
		
		' Cache the blank form.
		If dictFieldVals Is Nothing Then
			Call CacheFragment("StaticSectionsCache", sFormName, htmRenderFillOutForm)
		End If
	End If 
End Function
%>
