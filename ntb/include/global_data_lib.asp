<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_data_lib.asp
' Global Initialization library with Data and Warehouse functions; used by
' global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitDataFunctions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitDataFunctions()
    Dim objDataFunctions

    Set objDataFunctions = Server.CreateObject("Commerce.DataFunctions")
    objDataFunctions.Locale = dictConfig.i_SiteDefaultLocale

    Set InitDataFunctions = objDataFunctions
End Function


' -----------------------------------------------------------------------------
' GetDWConnectionString
'
' Description:
'	Look up the SQL OLEDB connection string for the Data Warehouse that
'	corresponds to this site.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDWConnectionString()
	Dim objConfig
	Dim strConn

	On Error Resume Next	
		
	Set objConfig = Server.CreateObject("Commerce.SiteConfigReadOnly")
	Call objConfig.Initialize(MSCSCommerceSiteName)
	strConn = objConfig.Fields("Global Data Warehouse").Value.Fields("connstr_db_dw").Value
	If Err.Number <> 0 Then
		strConn = ""
	End If

	GetDWConnectionString = strConn
End Function


' -----------------------------------------------------------------------------
' oGetBizDataObject
'
' Description:
'	Returns an instance of BizDataSore object
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function oGetBizDataObject()

	Dim oBizData

	Set oGetBizDataObject = Nothing

	On Error Resume Next
    Set oBizData = Server.CreateObject("Commerce.BusinessDataAdmin")
	If (Err.Number <> 0) Then
		Err.Clear
		Exit Function
	End If

    Call oBizData.Connect(dictConfig.s_BizDataStoreConnectionString)
	If (Err.Number <> 0) Then
		Err.Clear
		Exit Function
	End If

	Set oGetBizDataObject = oBizData
End Function


' -----------------------------------------------------------------------------
' oGetGenIDObject
'
' Description:
'	Returns an instance of a GenID object
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function oGetGenIDObject()
	Set oGetGenIDObject = Server.CreateObject("Commerce.GenID")	
End Function

</SCRIPT>