<%
' =============================================================================
' std_cache_lib.asp
' Standard library with Cache Manager functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================


' -----------------------------------------------------------------------------
' CacheFragment
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CacheFragment(ByVal sCacheName, ByVal sCacheItemName, ByVal htmlString)
	Dim oLRUCache, UserIDTag
	
	Set oLRUCache = MSCSCacheManager.GetCache(sCacheName)
	
	' Note that we use a separate cache to store fragments that have
	'	been genericized. To genericize a fragment, we replace
	'	all ticket instances with tokens.
	
	' If the ticket is in URL, the fragment must be genericized (genericize out ticket).
	If m_iTicketLocation = 1 Then
		UserIDTag = GetTicketInUrlArg()
		htmlString = Replace(htmlString, UserIDTag, TICKET_REPLACEMENT_TOKEN)
		Call oLRUCache.Insert(sCacheItemName & sLanguage & "URLMODE", htmlString)
	Else
		' No need to genericize if the ticket is in the cookie or there is no ticket (IIS auth).
		Call oLRUCache.Insert(sCacheItemName & sLanguage & "COOKIEMODE", htmlString)
	End If
End Sub


' -----------------------------------------------------------------------------
' LookupCachedFragment
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function LookupCachedFragment(ByVal sCacheName, ByVal sCacheItemName)
	Dim oLRUCache, UserIDTag
	
	Set oLRUCache = MSCSCacheManager.GetCache(sCacheName)
	
	' Note that we have to templatize fragments that have been genericized. To templatize
	'	we replace all token instances with correct userid.

	' If the ticket is in URL, the fragment must be templatized.
	If m_iTicketLocation = 1 Then
		LookupCachedFragment = oLRUCache.Lookup(sCacheItemName & sLanguage & "URLMODE")
		If Not IsNull(LookupCachedFragment) Then
			 UserIDTag = GetTicketInUrlArg
			 LookupCachedFragment = Replace(LookupCachedFragment, TICKET_REPLACEMENT_TOKEN, UserIDTag)
		End If
	Else
		' No need to templatize if the ticket is in the cookie or there is no ticket (IIS auth).
		LookupCachedFragment = oLRUCache.Lookup(sCacheItemName & sLanguage & "COOKIEMODE")	
	End If
	
	' Debug code that will show if a fragment was cached...
	'If Not IsNull(LookupCachedFragment) Then
	'	LookupCachedFragment = "<TABLE BORDER=1><TR><TD>Cached: " & sCacheName & BR & sCacheItemName& sLanguage & "</TD></TR><TR><TD>" & LookupCachedFragment & "</TD></TR></TABLE>"
	'End If
End Function


' -----------------------------------------------------------------------------
' LookupCachedObject
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function LookupCachedObject(ByVal sCacheName, ByVal sCacheItemName)
	Dim oLRUCache
	Set oLRUCache = MSCSCacheManager.GetCache(sCacheName)
	Set LookupCachedObject = oLRUCache.LookupObject(sCacheItemName)
End Function


' -----------------------------------------------------------------------------
' CacheObject
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CacheObject(ByVal sCacheName, ByVal sCacheItemName, ByVal oObject)
	Dim oLRUCache
	Set oLRUCache = MSCSCacheManager.GetCache(sCacheName)
	Call oLRUCache.Insert(sCacheItemName, oObject)
End Sub
%>