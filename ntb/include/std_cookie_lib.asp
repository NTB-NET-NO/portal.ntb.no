<%
' =============================================================================
' std_cookie_lib.asp
' Standard library with Cookie functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' NETSCAPE ISSUES:
' Note that Netscape Communicator 4.75 (and possibly earlier as well as more 
'	recent versions of Netscape browsers) are mindful of port number when accepting
'	and releasing cookies (Internet Explorer 5.5 is not). In other words if Netscape
'	browser accepts a cookie from a request such as http://hostname:81/default.asp, 
'	it will not release the cookie to a https://hostname:444/payment.asp request. 
'	This has implications for applications that use both secure and non-secure ports: 
'	the application will "lose" the user once user enters a secure area of the site 
'	since it can no longer retrieve the cookie from the user's browser.
'
' To work around this problem, we recommend you avoid using non-standard ports numbers for 
'	either TCP or SSL ports (the standard port numbers are 80 for TCP port and 443 for SSL
'	port) since non-standard port numbers will appear in the request (standard ports will not).
'	If you need to use non-standard port(s), then we recommend configuring the site
'	to work in URL mode (as opposed to cookie mode which is the default setting). This way
'	the application will not rely on cookies and will instead carry the user 
'	information in URL.


' -----------------------------------------------------------------------------
' CorrectRequest
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CorrectRequest()
	Dim url, bCookiePathCorrected, bHostNameCorrected

	bCookiePathCorrected = False
	bHostNameCorrected = False
	
	url = MSCSAppFrameWork.GetPageURL()
	
	If dictConfig.i_CookiePathCorrectionOptions = APP_BASED_CORRECTION Then 
		bCookiePathCorrected = GetCorrectedCookiePath(url)
	End If
	
	If dictConfig.i_HostNameCorrectionOptions = HOSTNAME_CORRECTION_ENABLED Then
		bHostNameCorrected = GetCorrectedHostName(url)
	End IF
	
	If bCookiePathCorrected Or bHostNameCorrected Then
		Response.Redirect(url)
	End If
End Sub


' -----------------------------------------------------------------------------
' GetCorrectedCookiePath
'
' Description:
'	Converts the casing of request's cookie path to match IIS application name.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCorrectedCookiePath(ByRef url)
	Dim iPos, sIISAppName
	
	sIISAppName = GetIISApplicationName()
	
	GetCorrectedCookiePath = False
	
	' URL correction not required for a commerce site installed at the Web Site's root directory. 
	If sIISAppName <> "\" Then
		If dictConfig.i_SiteTicketOptions = PUT_TICKET_IN_COOKIE Then
			' Does the casing of request's cookie path match that of IIS application path?
			iPos = InStr(1, url, sIISAppName, vbBinaryCompare)
	
			If iPos = 0 Then
				' Replace the request's cookie path wtih IIS application path.
				url = Replace(url, sIISAppName, sIISAppName, 1, 1, vbTextCompare)
				GetCorrectedCookiePath = True
			End If
		End If
	End If
End Function


' -----------------------------------------------------------------------------
' GetCorrectedHostName
'
' Description:
'	This function returns true if the hostname should be corrected
'	If the hostname should be corrected, it will return it in the ByRef param
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
'$$NOTE: implemented in MSCSAppFramework
Function GetCorrectedHostName(ByRef url)
	Dim sRequestHostName, sTrueHostName, iPos
	
	' Look up the true host name. 
	If StrComp(Request.ServerVariables("HTTPS"), "off", vbTextCompare) = 0 Then
		sTrueHostName = dictConfig.s_NonSecureHostname
	Else
		sTrueHostName = dictConfig.s_SecureHostname
	End If
	
	' Note that the host name as stored in s_NonSecureHostname or s_SecureHostname key 
	'	may include both the server name and the port number (hostname:port). 
	'	Only server name part requires correction.
	iPos = InStr(1, sTrueHostName, ":", vbTextCompare)	
	If iPos > 0 Then
		sTrueHostName = Left(sTrueHostName, iPos - 1)
	End If
	
	' Retrieve the host name of the incoming request.
	sRequestHostName = Request.ServerVariables("SERVER_NAME")
	
	' Substitute the host name of the incoming request with the true host name if different.
	If StrComp(sRequestHostName, sTrueHostName, vbTextCompare) <> 0 Then
		url = Replace(url, sRequestHostName, sTrueHostName, 1, 1, vbTextCompare)
		GetCorrectedHostName = True
	Else
		GetCorrectedHostName = False
	End If
End Function


' -----------------------------------------------------------------------------
' GetRequestType
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetRequestType()
	GetRequestType = Request.ServerVariables("REQUEST_METHOD")
End Function


' -----------------------------------------------------------------------------
' SetTestCookie
'
' Description:
'	Set a test cookie to learn if browser allows cookies.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SetTestCookie(ByVal sCookieType)
	Dim sIISAppName
	
	sIISAppName = GetIISApplicationName()
	Response.Cookies("MSCS2000TestCookie") = 1

	If sIISAppName = "/" Then
		Response.Cookies("MSCS2000TestCookie").Path = "/"
	Else
		Response.Cookies("MSCS2000TestCookie").Path = "/" & sIISAppName
	End If
	
	If sCookieType = "persistent_cookie" Then
		' Set the cookie to expire tommorrow (any time in the future would do)
		Response.Cookies("MSCS2000TestCookie").Expires = Now + 1
	End If
End Sub


' -----------------------------------------------------------------------------
' GetIISApplicationName
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetIISApplicationName()
	Dim iPos, sPath
	
	GetIISApplicationName = Application("IISApplicationName")
	
	If GetIISApplicationName = "" Then
		sPath = Request.ServerVariables("APPL_MD_PATH")
		iPos = InStr(1, sPath, "/ROOT/", vbTextCompare)
		If iPos > 0 Then
			GetIISApplicationName = Mid(sPath, iPos + Len("/ROOT/"))
		Else
			GetIISApplicationName = "/"
		End If
		Application("IISApplicationName") = GetIISApplicationName
		
		' Specify the cookie domain to maintain history across multiple machines 
		' (machine1.microsoft.com and machine2.microsoft.com) located in the same 
		' domain (microsoft.com). The domain should contain at least two periods 
		' (.microsoft.com) and match the domain name of the server setting the cookie.
		Application("CSFAdvertisingContext").CookieDomain = Null
		Application("CSFDiscountContext").CookieDomain = Null

		' If the site is installed at root level
		If GetIISApplicationName = "/" Then
			Application("CSFAdvertisingContext").CookiePath = "/"
			Application("CSFDiscountContext").CookiePath = "/"
		Else
			Application("CSFAdvertisingContext").CookiePath = "/" & GetIISApplicationName
			Application("CSFDiscountContext").CookiePath = "/" & GetIISApplicationName
		End If
	End If
End Function
%>
