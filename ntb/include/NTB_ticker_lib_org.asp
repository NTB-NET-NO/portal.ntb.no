<SCRIPT LANGUAGE=VBScript RUNAT=Server>

Sub initTicker()
	Application("vt") = 2
	Application("tbuffer1") = ""
	Application("tbuffer2") = ""
	Application("tcontentURL") = "../ticker/content.asp"
	Application("showticker") = 0
	Application("lastmin") = Minute(Now)
	Application("tmarkup") = "<applet codebase=""../ticker"" code=""com.dc3.applet.LEDSign.LED.class"" " & _
		"width=640 height=24 align=right>" & _
		"<param name=""ledsize"" value=""1"">" & _
		"<param name=""script"" value=""" & Application("tcontentURL") & """>" & _
		"<param name=""font"" value=""../ticker/lucida2.font"">" & _
		"<param name=""wth"" value=""320"">" & _
		"<param name=""ht"" value=""12"">" & _
		"</applet>"

	Application("tickface1") = ""
	Application("tickface2") = ""

	call updateTickerContent()

End Sub

Sub updateTickerContent()
	Dim rs, mm, trr, msgs, tcn ' ticker rows
	Application("lastmin") = Minute(Now)
	Set rs = Server.CreateObject("ADODB.Recordset")
	Set msgs = Server.CreateObject("ADODB.Stream")
	rs.CursorLocation = 3
	rs.Open "getTickers",Application("cns"), 0
	trr = rs.RecordCount
	msgs.Mode = 3
	msgs.Type = 2
	msgs.LineSeparator = -1 'adCRLF
	msgs.Open
	msgs.WriteText "!!LedSign ticker script",1

	if not rs.EOF then
		'update tickface
		' process returned records
		msgs.WriteText "!!Ticker has " & CStr(trr) & " messages.",1
		Dim url, s, msgtext
		Dim mark
		mark = 0
		while not rs.EOF
			s=CStr(rs.Fields("MessageText"))

			mark = instr(1,s,"/*U:")
			if mark > 0 then
				mark = mark+4
				url = Mid(s,mark,instr(mark,s,"*/")- mark)
			end if

			mark = instr(1,s,"/*M:")
			if mark > 0 then
				mark = mark+4
				msgtext = Mid(s,mark,instr(mark,s,"*/")- mark)
			end if

			if url <> "" then
				msgs.WriteText "ScrollLeft URL=" & url & " text=" & msgtext,1
				msgs.WriteText "Sleep delay=2000 URL=" & url,1
			else
				msgs.WriteText "ScrollLeft text=" & msgtext, 1
				msgs.WriteText "Sleep delay=2000", 1
			end if
			rs.MoveNext
		wend
		msgs.WriteText "Reload",1
	end if
	rs.close
	set rs = nothing

	msgs.Position = 0
	if Application("vt") = 1 then
		Application("tbuffer2") = msgs.ReadText()
	else
		Application("tbuffer1") = msgs.ReadText()
	End if
	msgs.Close
	set msgs = Nothing

	if Application("vt") = 1 then
		if trr > 0 then
			Application("tickface2") = Application("tmarkup")
		else
			Application("tickface2") = ""
		end if
		Application("vt") = 2
	else
		if trr > 0 then
			Application("tickface1") = Application("tmarkup")
		else
			Application("tickface1") = ""
		end if
		Application("vt") = 1
	End if
	if trr > 0 then
			Application("showticker") = trr
		else
			Application("showticker") = 0
	End if
End sub

Function getTickerFace()
	' Returns <APPLET.... fragment surrounded by table row tags or
	' nothing if there are no pending messages
	Dim ts, te
	ts = "<TR><TD colspan=""8"" width=""100%"" align=""right"">"
	te = "</TD></TR>"
	If Application("showticker") > 0 then ' there is something to show
		if Application("vt") = 1 then
			getTickerFace = Application("tickface1")
		else
			getTickerFace = Application("tickface2")
		end if
	else
		getTickerFace = ""
	End if
End Function
</SCRIPT>
