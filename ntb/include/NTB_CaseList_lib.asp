<SCRIPT LANGUAGE=VBScript RUNAT=Server>

Sub initCaseList()
	Application("CaseListBuffer1") = ""
	Application("CaseListBuffer2") = ""
	Application("vtCL") = 2
	updateCaseList()
End Sub

Sub updateCaseList
	' Open Connection for Key Link and read Recordset
	dim rsKeyword, cn
	Dim msgs
	dim strSak, strPictHref
	Dim i
	
	set cn = Server.Createobject("ADODB.Connection")
	cn.CursorLocation = adUseClient
	cn.Open Application("cns")
	
    set rsKeyword = Server.Createobject("ADODB.Recordset")
    rsKeyword.CursorLocation = adUseClient
	rsKeyword.CursorType = adOpenForwardOnly
	
    Set rsKeyword.ActiveConnection = cn  
   
	rsKeyword.Open "getCaseList" 
	rsKeyword.ActiveConnection = nothing
	cn.Close 
	set cn = nothing
	
	Set msgs = Server.CreateObject("ADODB.Stream")
	msgs.Mode = 3
	msgs.Type = 2
	msgs.LineSeparator = -1 'adCRLF
	msgs.Open

	i = 1
	Do Until rsKeyword.EOF 
		' If first write heading:
		If i = 1 Then 
			msgs.WriteText "<tr><td class=left_button width=100><a class=nav>Aktuelle saker:</a></td></tr>"
		End if

		' Write Picture and text for Case links. Maximum three!	
		If i > 3 Then Exit Do
		strSak = rsKeyword.Fields(2)
		strPictHref = "http://80.91.34.200/cgi-bin/picture?/4/UNKNOWN/Q_" & rsKeyword.Fields(4) & ".jpg"
		
		msgs.WriteText "<tr>"
		msgs.WriteText "<td class=""left_button"" width=""100"" height=""80"" VALIGN=""bottom"">" 
		'msgs.WriteText " BACKGROUND=""" & rsKeyword.Fields(4) & """>"
		msgs.WriteText "<a href='../mainpages/MainSectionPages.asp?pageID=SAK&SakNr=" & i & "&Sak=" & strSak & "' class=""nav"">"
		msgs.WriteText "<IMG border=""0"" HEIGHT=""65"" class='case_border' SRC=""" & strPictHref & """></a>"
		msgs.WriteText "<br />"
		msgs.WriteText "<a href='../mainpages/MainSectionPages.asp?pageID=SAK&SakNr=" & i & "&Sak=" & strSak & "' class=""nav"">"
		msgs.WriteText strSak & "<br />"
		msgs.WriteText "</a></td></tr>"
		
		Application("textSak" & i) = rsKeyword.Fields(3)
		
		Application("intSakRel" & i) = rsKeyword.Fields(6)
		Application("intSak2Rel" & i) = rsKeyword.Fields(7)
		Application("intSakDays" & i) = rsKeyword.Fields(8)
		Application("intSak2Days" & i) = rsKeyword.Fields(9)
		Application("strSakGruppe" & i) = rsKeyword.Fields(10)

		i = i + 1
		rsKeyword.MoveNext
	Loop				
	rsKeyword.Close 
	Set rsKeyword = nothing

	Dim n
	For n = i to 3
		' Pad with empty rows at bottom, so total (filled and empty rows) will allways be 3
		msgs.WriteText "<tr height=""80"">"
		msgs.WriteText "<td class=""left_button"" width=""81"" height=""81""></td>"
		msgs.WriteText "</tr>"
	Next						

	msgs.Position = 0

	If Application("vtCL") = 1 then
		Application("CaseListBuffer2") = msgs.ReadText()
		Application("vtCL") = 2
	Else
		Application("CaseListBuffer1") = msgs.ReadText()
		Application("vtCL") = 1
	End if

	msgs.Close
	Set msgs = Nothing

End Sub

Sub updateCaseList_old()
	Dim rs, mm, trr, msgs, tcn ' Case rows
	Application("lastmin") = Minute(Now)
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = 3
	rs.Open "getCaseList",Application("cns"), 0
	
	trr = rs.RecordCount

	Set msgs = Server.CreateObject("ADODB.Stream")
	msgs.Mode = 3
	msgs.Type = 2
	msgs.LineSeparator = -1 'adCRLF
	msgs.Open
	'msgs.WriteText "!!LedSign ticker script", 1

	If not rs.EOF then
		' process returned records
		msgs.WriteText "!!Ticker has " & CStr(trr) & " messages.",1
		Dim url, s, msgtext

		while not rs.EOF
			s=CStr(rs.Fields("MessageText"))

			mark = instr(1,s,"/*U:")
			if mark > 0 then
				mark = mark+4
				url = Mid(s,mark,instr(mark,s,"*/")- mark)
			end if

			mark = instr(1,s,"/*M:")
			if mark > 0 then
				mark = mark+4
				msgtext = Mid(s,mark,instr(mark,s,"*/")- mark)
			end if

			if url <> "" then
				msgs.WriteText "ScrollLeft URL=" & url & " text=" & msgtext,1
				msgs.WriteText "Sleep delay=2000 URL=" & url,1
			else
				msgs.WriteText "ScrollLeft text=" & msgtext, 1
				msgs.WriteText "Sleep delay=2000", 1
			end if
			rs.MoveNext
		wend
		msgs.WriteText "Reload",1
	end if
	rs.close
	set rs = nothing

	msgs.Position = 0
	Application("CaseListBuffer") = msgs.ReadText()
	msgs.Close
	set msgs = Nothing

End sub

</SCRIPT>
