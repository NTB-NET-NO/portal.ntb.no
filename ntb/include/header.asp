<%@ LANGUAGE=VBScript EnableSessionState=False %>
<%
' =============================================================================
' header.asp
' ASP Page header
' 
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================


' You must add the character set (CodePage) directive for the scripting engine 
' to the line above if it is different from the system's character set.

Option Explicit

' Specify the character set to send to client browser.
' Response.Charset = "windows-1252"
If Not IsEmpty(Application("MSCSPageEncodingCharSet")) Then
	Response.CharSet = Application("MSCSPageEncodingCharSet")
End If

Response.Buffer = True

Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"	
Response.Expires = -1

Dim g_sPipelineProgID 
g_sPipelineProgID = "Commerce.PooledPipeline"
%>
