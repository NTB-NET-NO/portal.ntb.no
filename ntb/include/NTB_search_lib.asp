<%
'***********************************************************************************
' NTB_search_lib.asp
'
' Support functions for search functionality. Covers quick, advanced and sound search
' 
' NOTES:
'		'none'
'
' CREATED BY: Edward Woodhouse, Andersen 
' CREATED DATE: 2002.05.15
' UPDATED BY: Edward Woodhouse, KCI
' UPDATED DATE: 2002.07.02
' REVISION HISTORY:
'		'none'
'
'***********************************************************************************

' -----------------------------------------------------------------------------
' FindSearchSources
'
' Description:	Finds which datasource to use based on timeperiod or dates
'
' Parameters:	strTimePeriod - period in days from now, use strDates if zero
'				strDates - semicolon seperated datenumbers
'
' Returns:		blnMemCacheSearch - boolean parameter true if cache is to be used
'				
' Notes :		
'				
' -----------------------------------------------------------------------------
Sub FindSearchSources(ByRef blnMemCacheSearch, _
					  ByVal strTimePeriod, _
                      ByVal strDates)

   Dim intTimePeriod
      
   blnMemCacheSearch = False

   strTimePeriod = Trim(strTimePeriod)
   If strTimePeriod <> "" Then
      intTimePeriod = CInt(strTimePeriod)
   Else
      'Setter perioden til siste d�gn dersom den ikke er spesifisert
      intTimePeriod = 1
   End If

	If (intTimePeriod > 0) AND (intTimePeriod <= 7) Then
		blnMemCacheSearch = True
    ElseIf (intTimePeriod = 0) Then
		'check if oldest date is within last 7 days

		'For now
		blnMemCacheSearch = False
	Else
   
		blnMemCacheSearch = False
	End If
        
End Sub


' -----------------------------------------------------------------------------
' ProcessDateInformation
'
' Description:	Finds date information from strings provided
'
' Parameters:	strTimePeriod - No of days from now, if zero use dates
'				strDates datestring with semicolon seperated numbers 
'
' Returns:		intDays - integer variant of strTimePeriod but converted to memDB convention
'				dtFromDate - startdate from strDates
'				dtToDate - enddate from strDates				
'
' Notes :		
'				
' -----------------------------------------------------------------------------
Sub ProcessDateInformation(ByRef intDays, _
							ByRef dtFromDate, _
							ByRef dtToDate, _
							ByVal strTimePeriod, _
							ByVal strDates)
	Dim intTimePeriod
	intTimePeriod = CInt(strTimePeriod)
	dtToDate = Date
	dtFromDate = dtToDate - intTimePeriod


	If intTimePeriod = 0 Then
		intDays = -1
		
        Dim strDateBlocks      
        strDateBlocks = Split(strDates, ";", -1)
        
        'dtFromDate = strDateBlocks(1) & "/" & strDateBlocks(0) & "/" & strDateBlocks(2)
        'dtToDate = strDateBlocks(4) & "/" & strDateBlocks(3) & "/" & strDateBlocks(5)
        dtFromDate = DateSerial(strDateBlocks(2), strDateBlocks(1), strDateBlocks(0))
        dtToDate = DateSerial(strDateBlocks(5), strDateBlocks(4), strDateBlocks(3))

	ElseIf intTimePeriod = 1 Then
		intDays = 0
		
	ElseIf intTimePeriod = 2 Then
		intDays = 1
		
	Else 
		intDays = intTimePeriod
		
	End If

    'Response.Write "<br/>ProcessDateInformation - |dtFromDate: " & dtFromDate & "  |dtToDate: " & dtToDate
    'Response.Flush

End sub

' -----------------------------------------------------------------------------
' SearchDatabase
'
' Description:	Search for articles in database
'
' Parameters:	rsResultRecords - articles found in database that match query filters
'				strQuery - category filter (maingroup, subgroup, categories ...)
'				dtFromDate - startdate for query
'				dtToDate - enddate for query
'
' Returns:		rsResultRecords - articles found in database that match query filters					
'
' Notes :		
'				
' -----------------------------------------------------------------------------
Sub SearchDatabase(ByRef rsResultRecords, ByVal strQuery, ByVal dtFromDate, ByVal dtToDate)

	'Open connection
	Dim connection
    Set connection = Server.CreateObject("ADODB.Connection")							
    connection.ConnectionString = Application("archive_cns")
    'for test
    'connection.ConnectionString = Application("cns")
    connection.Open
    connection.CursorLocation = adUseClient

    
    'Setup Recordset
	Dim rsDatabaseSearch
    Set rsDatabaseSearch = Server.CreateObject("ADODB.Recordset")							  
    rsDatabaseSearch.CursorType = adOpenForwardOnly
    rsDatabaseSearch.LockType = adLockBatchOptimistic
    rsDatabaseSearch.CursorLocation = adUseClient

	'Create Stored procedure commend
    Dim cmd
    Set cmd = Server.CreateObject("ADODB.Command")							  
    cmd.ActiveConnection = connection
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "fetchForSearch"
    
    'Set parameters
    Dim par
    Set par = Server.CreateObject("ADODB.Parameter")							
    
    'test
    'strQuery = "(Maingroup & 4 > 0 AND Categories & 2048 > 0 AND SubCategories & '2305843009213693952' > 0)"
    
	Set par = cmd.CreateParameter("@querystring", adVarChar, adParamInput, 256, CStr(strQuery))
	'Response.Write "|querystring: " & CStr(strQuery)
	'Response.Flush
	
	cmd.Parameters.Append par

    Set par = cmd.CreateParameter("@startdate", adVarChar, adParamInput, 64, CStr(dtFromDate) )
    'Response.Write "<br/>SearchDatabase - |dtFromDate: " & CStr(dtFromDate)
    cmd.Parameters.Append par
    
    if dtToDate <> "" then
	    Set par = cmd.CreateParameter("@enddate", adVarChar, adParamInput, 64, CStr(dtToDate))
	    'Response.Write "|dtToDate: " & CStr(dtToDate)
		cmd.Parameters.Append par
	end if
    
	'Testcode
    'Response.Write "From: " & dtFromDate & "- To: " & dtToDate & " - Query: " & strQuery

    rsDatabaseSearch.Open cmd
   
	'Testcode
    'Response.Write "<BR/>DB RecordCount: " & rsDatabaseSearch.RecordCount
    ''Response.Flush
     
    Set rsResultRecords = rsDatabaseSearch
	
End Sub


' -----------------------------------------------------------------------------
' DoSearchAutonomy
'
' Description:	Main procedure for searching autonomy and/or cache or database
'
' Parameters:	rsResultRecords - Records from article table that match queries
'				strQuery - freetext string
'				strCategory - filter string for categories (maingroup, subGroup,...)
'				strTimePeriod - number of days back from now, if zero use strdates
'				strDates - from and to dates separated by semicolon
'				strGeography - filterstring for global regions
'				strGeographyNorway - filterstring for norwegian regions
'				strMaxHits - Maximum number of hits to return				
'
' Returns:		rsResultRecords - Records from article table that match queries					
'
' Notes :		
'				
' -----------------------------------------------------------------------------
'New search for only Autonomy:
Sub DoSearchAutonomy(ByRef rsResultRecords, ByVal strQuery, ByVal strCategory, ByVal strTimePeriod, ByVal strDates, _
			ByVal strGeography, ByVal strGeographyNorway, ByVal strMaxHits, ByVal threshold)

    Dim blnDatabaseSearch
    Dim blnAutonomySearch
	

    blnAutonomySearch = True   
	'Check if Autonomy search is to be performed
    'if strQuery = "" then
    '    blnDatabaseSearch = True
    'else
    '    blnAutonomySearch = True   
    'end if  

	'For Debug:
	'Response.Write ("Debug: Query: " & strQuery & "<br>Category: " & strCategory & "<br>TimePeriod: " & strTimePeriod _ 
	'& "<br>Dates:" &  strDates & "<br>Geography:" &  strGeography & "<br>GeographyNorway:" & strGeographyNorway & "<br>MaxHits:" & strMaxHits)
	'Response.Flush

	'Find search period and dates    
	if strTimePeriod = "0" AND len(strDates) < 10 then
	  strTimePeriod = "7"
	end if
    
    if blnDatabaseSearch then 
        Dim rsDatabaseResult
		Dim intTimePeriod
		Dim dtFromDate
		Dim dtToDate
		Dim strFromDate
		Dim strToDate
		ProcessDateInformation intTimePeriod, dtFromDate, dtToDate, strTimePeriod, strDates          	
		'Search in Database
		'Parse selected query options
		Dim objQueryBuilder
		Set objQueryBuilder = Server.CreateObject("QueryParser.comQueryParser")							
		Dim strWhereClause
      	'Response.Write "<br/>Fetching data from database"
		strWhereClause = objQueryBuilder.MakeDBFilterString(strCategory, strGeography, strGeographyNorway )
		if strWhereClause <> "" then
			'SearchDatabase rsDatabaseResult, strWhereClause, FormatDateTime(dtFromDate, 2), FormatDateTime(dtToDate, 2)
			SearchDatabase rsDatabaseResult, strWhereClause, dtFromDate, dtToDate
			if not (rsDatabaseResult is nothing) then
				Set rsResultRecords = rsDatabaseResult
			end if						
			blnAutonomySearch = False
		else
			blnAutonomySearch = True
		end if
	end if
	
    if blnAutonomySearch then            
	    Dim searchInterface
		Dim rsAutonomyResult
		Dim a_csn, a_db, a_min
					
		a_csn = application("autonomy_all_cns")
		a_db = Application("autonomy_database")
		
		if IsNumeric(threshold) then
			a_min = threshold
		else 
			a_min = Application("autonomy_minimum")
		end if			
			
		if strQuery = "" then
			strQuery = "*"
		end if
		Set searchInterface = Server.CreateObject("SearchInterface.comSearch")							
		Set rsAutonomyResult = searchInterface.doSearch(strQuery, strCategory, strTimePeriod, _
		                             strDates, strGeography, strGeographyNorway, _
		                             a_csn, a_db, a_min, strMaxHits)  

		if not (rsAutonomyResult is nothing) then
			Set rsResultRecords = rsAutonomyResult
		Else
		   	Response.Write ("Debug: " & strQuery & "<br>Category: " & strCategory & "<br>TimePeriod: " & strTimePeriod)
			Response.Flush
		end if
	
	End If
	
End Sub

'Old search rutine, not in use anymore:
Sub DoSearch(ByRef rsResultRecords, ByVal strQuery, ByVal strCategory, ByVal strTimePeriod, ByVal strDates, _
			ByVal strGeography, ByVal strGeographyNorway, ByVal strMaxHits)
                  
    Dim blnDatabaseSearch
    Dim blnAutonomySearch
	'For database searches distinguish between cache and direct database 
    Dim blnMemCache
   
    blnMemCache = False

	'Check if Autonomy search is to be performed
    if strQuery = "" then
        blnAutonomySearch = False
    else
        blnAutonomySearch = True   
    end if
  
    'Test
	'Response.Write "<br/>Filter: " & strWhereClause

	'Find search period and dates    
	if strTimePeriod = "0" AND len(strDates) < 10 then
	  strTimePeriod = "7"
	end if
	Dim intTimePeriod
	Dim dtFromDate
	Dim dtToDate
	Dim strFromDate
	Dim strToDate
	ProcessDateInformation intTimePeriod, dtFromDate, dtToDate, strTimePeriod, strDates          	
    
	'Check what if any DB/memCache search is to be performed
	FindSearchSources blnMemCache, strTimePeriod, strDates
						
	'Parse selected query options
	Dim objQueryBuilder
	Set objQueryBuilder = Server.CreateObject("QueryParser.comQueryParser")							
	Dim strWhereClause

	'Do Memory Cache search if time period is within last week, else go directly to database
    Dim rsDatabaseResult
	blnDatabaseSearch = True
	if blnMemCache then
      	'Response.Write "<br/>Fetching data from oMemDb"        	
		strWhereClause = objQueryBuilder.MakeFilterString(strCategory, strGeography, strGeographyNorway )
		if strWhereClause <> "" then
			set rsDatabaseResult = oMemDb.FetchForSearch(strWhereClause, 0, intTimePeriod, dtFromDate, dtToDate)
		else
			blnDatabaseSearch = False
			blnAutonomySearch = True
		end if
	else
      	'Response.Write "<br/>Fetching data from database"
		strWhereClause = objQueryBuilder.MakeDBFilterString(strCategory, strGeography, strGeographyNorway )
		if strWhereClause <> "" then
			SearchDatabase rsDatabaseResult, strWhereClause, FormatDateTime(dtFromDate,2), FormatDateTime(dtToDate,2)
		else
			blnDatabaseSearch = False
			blnAutonomySearch = True
		end if
	end if
		     
    'Do Autonomy search if query string contains something            
    Dim searchInterface
    Dim rsAutonomyResult
    if blnAutonomySearch then            
 
		'Test
       	'Response.Write "<br/>Fetching data from Autonomy"
		Dim a_csn, a_db, a_min
				
		a_csn = application("autonomy_all_cns")
        a_db = Application("autonomy_database")
		a_min = Application("autonomy_minimum")
		
		if strQuery = "" then
			strQuery = "*"
		end if
		Set searchInterface = Server.CreateObject("SearchInterface.comSearch")							
		Set rsAutonomyResult = searchInterface.doSearch(strQuery, strCategory, strTimePeriod, _
		                             strDates, strGeography, strGeographyNorway, _
		                             a_csn, a_db, a_min, "300")  

    end if
    
    if IsEmpty(rsAutonomyResult) then
		blnAutonomySearch = False
    end if

    if IsEmpty(rsDatabaseResult) then
		blnDatabaseSearch = False
	end if
    
	'If combined search
    if blnAutonomySearch and blnDatabaseSearch then
		'Test
       	'Response.Write "<br/>Processing results from database and Autonomy"
		Dim objSearchProcessing
		Set objSearchProcessing = Server.CreateObject("SearchProcessing.comSearchProcessing")							
		Set rsResultRecords = objSearchProcessing.doProcessRecords(rsDatabaseResult, rsAutonomyResult)
		
    'if only autonomy
	elseif blnAutonomySearch then
		'Test
		'Response.Write("<br/>Using results from Autonomy")
		if not (rsAutonomyResult is nothing) then
			Set rsResultRecords = rsAutonomyResult
		end if
		
	'if only database
	else
		'Test
		'Response.Write("<br/>Using results from database")
		if not (rsDatabaseResult is nothing) then

			'Make sure there are 300 or less records returned
			If rsDatabaseResult.RecordCount > 300 then
			
				'Test code
				'Response.Write "<br/>Too many records returned:" & CStr(rsDatabaseResult.RecordCount) & "<br/>"
									
				dim index
				index = 0
				rsDatabaseResult.MoveFirst
				rsDatabaseResult.Sort = "CreationDateTime DESC"
				while not rsDatabaseResult.EOF 
					if index >= 300 then
						rsDatabaseResult.Delete
					end if
					rsDatabaseResult.MoveNext
					index = index + 1
				wend
		
			end if 

			Set rsResultRecords = rsDatabaseResult
		end if

	end if
	
End Sub

' -----------------------------------------------------------------------------
' DoSoundSearch
'
' Description:	Search for articles with sound attached
'
' Parameters:	rsResultRecords - returned recordset with articles found
'				strQuery - string containing query word
'
' Returns:		rsResultRecords - returned recordset with articles found	
'
' Notes :		Looks for search word in sound filename
'				
' -----------------------------------------------------------------------------
Sub DoSoundSearch(ByRef rsResultRecords, ByVal strQuery)
                  

    Dim rsDatabaseResult
    Set rsDatabaseResult = Server.CreateObject("ADODB.Recordset")							  
    
	'Open connection
	Dim connection
    Set connection = Server.CreateObject("ADODB.Connection")							
    connection.ConnectionString = Application("archive_cns")
    'For test
    'connection.ConnectionString = Application("cns")
    connection.Open
    connection.CursorLocation = adUseClient
    
    'Setup Recordset
	Dim rsDatabaseSearch

	'Create Stored procedure commend
    Dim cmd
    Set cmd = Server.CreateObject("ADODB.Command")							  
    cmd.ActiveConnection = connection
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "fetchForSoundSearch"
    
    'Set parameters
    Dim par
    Set par = Server.CreateObject("ADODB.Parameter")							
	Set par = cmd.CreateParameter("@querystring", adVarChar, adParamInput, 256, CStr(strQuery))
    cmd.Parameters.Append par
          
	Set rsDatabaseSearch = cmd.Execute()
           
    Set rsResultRecords = rsDatabaseSearch
    
	'For test, EW
    'Response.Write "Records: " & rsResultRecords.RecordCount
	
End Sub

' -----------------------------------------------------------------------------
' DateToUSDate
'
' Description:	Finds date information from strings provided
'
' Parameters:	strDates - Date in Norwegian or US format
'
' Returns:		Date in US format
'
' Notes :		
'				
' -----------------------------------------------------------------------------
Function DateToUSDate(ByVal strDates)
    Dim strDateBlocks, strToDate      
		
	If instr(1, strDates, ".") > 0 then
	    strDateBlocks = Split(strDates, ".")       
        'dtToDate = DateSerial(strDateBlocks(2), strDateBlocks(1), strDateBlocks(0))
        strToDate = strDateBlocks(1) & "/" & strDateBlocks(0) & "/" & strDateBlocks(2)
	End If

    'Response.Write "<br/>ProcessDateInformation - |strDates: " & strDates & "  |strToDate: " & strToDate
    'Response.Flush
	DateToUSDate = strToDate
	
End Function
%>

