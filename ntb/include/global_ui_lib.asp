<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_ui_lib.asp
' Global Initialization library with UI functions; used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetSiteStyles
'
' Description:
'	Site styles are used by HTMLLib functions 
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSiteStyles()
    Dim dictStyles
    
    Set dictStyles = GetDictionary()
    
    ' Table ATTLIST: ALIGN, WIDTH, BORDER, CELLSPACING, CELLPADDING
    dictStyles.BorderlessTable = " BORDER='0'"
    dictStyles.BorderedTable = " BORDER='1' CELLSPACING='0' CELLPADDING='5'"
    dictStyles.AddressTable = " BORDER='1' CELLSPACING='2' CELLPADDING='5'"
	dictStyles.Form = " BORDER='0' CELLSPACING='10'"    
    dictStyles.MenuTable = " BORDER='0' CELLSPACING='10'"
    
	' Basket styles
    dictStyles.BasketTable = " BORDER='1' CELLSPACING='0' CELLPADDING='1'"
		
	' Font ATTLIST: SIZE, COLOR
	dictStyles.SiteName = " SIZE='+3'"
	dictStyles.Title = " SIZE='+1'"
	dictStyles.MenuHead = ""
	dictStyles.MenuBody = ""            		            	
	dictStyles.BasketHeader = ""
	dictStyles.BasketData = ""
	dictStyles.Body = ""
	dictStyles.Warning = " COLOR='Red'"
	dictStyles.Tip = " COLOR='Red'"
	
	dictStyles.BaseCurrency = ""
	dictStyles.AltCurrency = " COLOR='Red'"
	
	dictStyles.TextBox = ""
	dictStyles.PasswordBox = ""
	dictStyles.CheckBox = ""
	dictStyles.RadioButton = ""
	dictStyles.Button = ""
	dictStyles.Link = ""
	dictStyles.ListBox = ""

	Set dictStyles.Unspecified = GetDictionary()
	
	' table ATTLIST: ALIGN, WIDTH, BORDER, CELLSPACING, CELLPADDING
	dictStyles.NoBorderTable = " BORDER='0'"

	' tr ATTLIST: ALIGN, VALIGN
	dictStyles.TRLeft = " ALIGN='Left'"
	dictStyles.TRCenter = " ALIGN='Center'"
	dictStyles.TRRight = " ALIGN='Right'"
	dictStyles.TRTop = " VALIGN='Top'"
	dictStyles.TRMiddle = " VALIGN='Middle'"
	dictStyles.TRBottom = " VALIGN='Bottom'"		
	
	' th/td ATTLIST: NOWRAP, ROWSPAN, COLSPAN, WIDTH, HEIGHT, ALIGN, VALIGN, 
	dictStyles.TDLeft = " ALIGN='Left'"
	dictStyles.TDCenter = " ALIGN='Center'"
	dictStyles.TDRight = " ALIGN='Right'"

    ' For Partner/Customer service page's styles	
    Call AddPartnerAndCustomerServiceStyles(dictStyles)
    
    Set GetSiteStyles = dictStyles
End Function


' -----------------------------------------------------------------------------
' GetStyles
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetStyles()
	Dim dictStyles
	
	dictStyles = GetDictionary()
	
	dictStyles.StandardTextBox = "size='" & L_Standard_TextBox_Size_Number & "'"
	dictStyles.StandardPasswordBox = "size='" & L_Standard_TextBox_Size_Number & "'"
	
	Set GetStyles = dicStyles 
End Function
</SCRIPT>
