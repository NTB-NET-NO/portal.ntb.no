 <%
' =============================================================================
' std_profile_lib.asp
' Standard library with Profile Service functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Dim Private_CurrentUserProfile


' -----------------------------------------------------------------------------
' GetProfileService
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetProfileService()
	Set GetProfileService = Server.CreateObject("Commerce.ProfileService")
    Call GetProfileService.Initialize(dictConfig.s_ProfileServiceConnectionString, PROFILE_CATALOG)
End Function


' -----------------------------------------------------------------------------
' UpdateUserProfile
'
' Description:
'	Updates current user's profile. This is a wrapper subroutine for IProfileObject.Update()
'	method. Call this subroutine whenever you update the current user's profile or create a
'	profile for the current user. UpdateUserProfile will automatically set date_last_changed
'	in the profile, and it will also write this information to a cookie (where supported), to
'	handle machine switching in a Webfarm.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub UpdateUserProfile(ByRef rsUser)
	Dim dTime

	' Store the current time in a temporary variable.
	dTime = Now()

	' Set the date_last_changed explicitly so the database won't set it by default.
	rsUser.Fields("ProfileSystem.date_last_changed").Value = dTime

	' Handle machine switching in a Webfarm.
	If Application("WebServerCount") > 1 Then
		' Since testing cookie support on every page that calls UpdateUserProfile is
		'	expensive, we assume cookie support unless we detect a ticket in URL.
		If m_iTicketLocation <> 1 Then
			' Set the cookie if the ticket is not in URL, or it cannot be located (IIS auth).
			Response.Cookies("UserProfileTimeStamp") = dTime
		End If
	End If

	Call rsUser.Update()

	' Cache current user's profile for the calling page.
	Set Private_CurrentUserProfile = rsUser
End Sub


' -----------------------------------------------------------------------------
' EnsureUserProfile
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function EnsureUserProfile()
	Dim rsUser

	Set rsUser = GetCurrentUserProfile()

	If rsUser Is Nothing Then
		Response.Redirect(GenerateURL(MSCSSitePages.ProfileMissing, Array(), Array()))
	Else
		Set EnsureUserProfile = rsUser
	End If
End Function


' -----------------------------------------------------------------------------
' GetNewGuestUserProfile
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetNewGuestUserProfile()
	If dictConfig.i_SitePrivacyOptions = PROFILE_GUEST_USER Then
		Set GetNewGuestUserProfile = MSCSProfileService.CreateProfile(MSCSGenID.GenGUIDString, PROFILE_TYPE_USER)

		GetNewGuestUserProfile.Fields("GeneralInfo.user_id_changed_by").Value = GetNewGuestUserProfile.Fields("GeneralInfo.user_id").Value
		GetNewGuestUserProfile.Fields("GeneralInfo.user_type").Value = GUEST_PROFILE
		GetNewGuestUserProfile.Fields("AccountInfo.account_status").Value = ACCOUNT_ACTIVE
		GetNewGuestUserProfile.Fields("AccountInfo.date_registered").Value = Null
		GetNewGuestUserProfile.Fields("BusinessDesk.partner_desk_role").Value = ROLE_USER
		GetNewGuestUserProfile.Fields("ProfileSystem.date_created").Value = Now()

		' Handles machine switching in a Webfarm.
		Call UpdateUserProfile(GetNewGuestUserProfile)
	Else
		Set GetNewGuestUserProfile = Nothing
	End If
End Function


' -----------------------------------------------------------------------------
' GetCurrentUserProfile
'
' Description:
'   This function always returns the last saved copy of the user profile thus handling
'	machine switching in a Webfarm.
'
' Parameters:
'
' Returns:
'
' Notes :
'    1) Optimized to return Nothing when we know the user has no profile.
'    2) Optimized to cache results for multiple uses on same page (in
'		Private_CurrentUserProfile)
' -----------------------------------------------------------------------------
Function GetCurrentUserProfile()
	' See if we already have the current user's profile cached for the calling page.
	If Not IsEmpty(Private_CurrentUserProfile) Then
		Set GetCurrentUserProfile = Private_CurrentUserProfile
		Exit Function
	End If

	' If the privacy policy of the Web Site indicates no profiling of guest users ...
	If (m_UserType = GUEST_USER) And (dictConfig.i_SitePrivacyOptions = TRACK_GUEST_VISITS) Then
		Set GetCurrentUserProfile = Nothing
		Set Private_CurrentUserProfile = GetCurrentUserProfile
		Exit Function
	End If

	' Get the profile.
    Set GetCurrentUserProfile = MSCSProfileService.GetProfileByKey(FIELD_USER_ID, m_UserID, PROFILE_TYPE_USER, RETURN_NOTHING_ON_NO_PROFILE)

	If GetCurrentUserProfile Is Nothing Then
		Set Private_CurrentUserProfile = GetCurrentUserProfile
		Exit Function
	End If

	' Handles machine switching in a Webfarm.
	If Application("WebServerCount") > 1 Then
		If m_iGlobalProfileLookUpOption = 1 Then
			' Retrieve the user profile from the database.
			Call GetProfileFromDB(GetCurrentUserProfile)
		Else
			' Retrieve the user profile from cache if it is up-to-date, otherwise from DB.
			Call EnsureUpToDateUserProfile(GetCurrentUserProfile)
		End If
	End If

	' Cache the current user's profile for the calling page.
	Set Private_CurrentUserProfile = GetCurrentUserProfile
End Function


' -----------------------------------------------------------------------------
' GetProfileFromDB
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub GetProfileFromDB(ByRef rsProfile)
	' Use On "Error Resume Next" to handle the case where profile is
	'	deleted from the database but is still cached on this Web server.
	On Error Resume Next
	' .GetInfo forces database look-up.
	Call rsProfile.GetInfo()
	If Err.number <> 0 Then
		' Trap DB_E_NOTFOUND error (it indicates a deleted profile), bubble up any other hresults.
		If Err.Number = &H80040E19 Then
			On Error GoTo 0
			Set rsProfile = Nothing
		Else
			' Back up the error object
			iErrNumber = Err.Number
			sErrSource = Err.Source
			sErrDesc = Err.Description

			On Error GoTo 0

			' Raise an error
			Err.Raise iErrNumber, sErrSource, sErrDesc
		End If
	End If
End Sub


' -----------------------------------------------------------------------------
' EnsureUpToDateUserProfile
'
' Description:
'	' Retrieve the user profile from cache if it is up-to-date, otherwise from DB.
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub EnsureUpToDateUserProfile(ByRef rsUser)
	Dim sCookieTimeStamp, sCacheTimeStamp

	' Since testing cookie support on every page that calls UpdateUserProfile is
	'	expensive, we assume cookie support unless we detect a ticket in URL.
	If m_iTicketLocation <> 1 Then
		sCookieTimeStamp = Request.Cookies("UserProfileTimeStamp")
		sCacheTimeStamp = CStr(rsUser.Fields("ProfileSystem.date_last_changed").Value)

		If (sCookieTimeStamp = "") Or (sCacheTimeStamp <> sCookieTimeStamp) Then
			Call GetProfileFromDB(rsUser)

			' Set/update the cookie timestamp
			If rsUser Is Nothing Then
				' Delete the cookie for a deleted profile.
				Response.Cookies("UserProfileTimeStamp").Expires = Now - 1
			Else
				Response.Cookies("UserProfileTimeStamp") = rsUser.Fields("ProfileSystem.date_last_changed").Value
			End If
		End If
	Else
		' Get the current user's profile from the dtabase if cookies are not supported.
		Call GetProfileFromDB(rsUser)
	End If
End Sub


' -----------------------------------------------------------------------------
' GetUserProfileByLoginName
'
' Description:
'	The function is to be called for current user only.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetUserProfileByLoginName(ByVal sLogonName)
	Dim rsUser

	' Get the profile.
	Set rsUser = MSCSProfileService.GetProfile(sLogonName, PROFILE_TYPE_USER, RETURN_NOTHING_ON_NO_PROFILE)

	If rsUser Is Nothing Then
        Set GetUserProfileByLoginName = Nothing
        Set Private_CurrentUserProfile = GetCurrentUserProfile
		Exit Function
	End If

	' Handle machine switching in a WebFarm
	If Application("WebServerCount") > 1 Then
		If m_iGlobalProfileLookUpOption = 1 Then
			' Retrieve the user profile from the database.
			Call GetProfileFromDB(rsUser)
		Else
			' Retrieve the user profile from cache if it is up-to-date, otherwise from DB.
			Call EnsureUpToDateUserProfile(rsUser)
		End If
	End If

	Set GetUserProfileByLoginName = rsUser

	' Cache the current user's profile for the calling page.
	Set Private_CurrentUserProfile = GetUserProfileByLoginName
End Function


' -----------------------------------------------------------------------------
' rsGetProfile
'
' Description:
'
' Parameters:
'	bForceDBLookUp parameter is used to force database look-up, overruling m_iGlobalProfileLookUpOption.
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetProfile(ByVal sProfileID, ByVal sProfileType, ByVal bForceDBLookUp)
	Set rsGetProfile = MSCSProfileService.GetProfile(sProfileID, sProfileType, RETURN_NOTHING_ON_NO_PROFILE)

	If rsGetProfile Is Nothing Then
		Exit Function
	End If

	' Handles machine switching in a Webfarm.
	If Application("WebServerCount") > 1 Then
		If (m_iGlobalProfileLookUpOption = 1) Or bForceDBLookUp Then
			' Retrieve the profile from the database.
			Call GetProfileFromDB(rsGetProfile)
		End If
	End If
End Function


' -----------------------------------------------------------------------------
' rsGetProfileByKey
'
' Description:
'
' Parameters:
'	bForceDBLookUp parameter is used to force database look-up, overruling m_iGlobalProfileLookUpOption.
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetProfileByKey(ByVal sKeyName, ByVal sKeyValue, ByVal sProfileType, ByVal bForceDBLookUp)
	Set rsGetProfileByKey = MSCSProfileService.GetProfileByKey(sKeyName, sKeyValue, sProfileType, RETURN_NOTHING_ON_NO_PROFILE)

	If rsGetProfileByKey Is Nothing Then
		Exit Function
	End If

	' Handles machine switching in a Webfarm.
	If Application("WebServerCount") > 1 Then
		If (m_iGlobalProfileLookUpOption = 1) Or bForceDBLookUp Then
			' Retrieve the profile from the database.
			Call GetProfileFromDB(rsGetProfileByKey)
		End If
	End If
End Function
%>