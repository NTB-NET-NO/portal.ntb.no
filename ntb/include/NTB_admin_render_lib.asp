<%
  '***********************************************************************************
' NTB_admin_render_lib.asp
'
' Support functions for rendering the admin pages
'
' NOTES:
'		'none'
'
' CREATED BY: Solveig Skjermo
' CREATED DATE: 2002.05.15
' UPDATED BY: Solveig Skjermo
' UPDATED DATE: 2002.06.27
' REVISION HISTORY:
' Last update: 2003.04.02 Roar Vestre changed guide-text in RenderNewUserProfile
' Updated by: Richard Husevaag: Removed check box for request password by SMS
' Updated date: 2007.02.26
' Updated by: Trond Hus�: Added arrows, checkboxes when rendering userlist
' Updated date: 2008.02.07
' Updated by: Trond Hus�: Added dropdown in adding users so that administrators can
'							add more users without going back to the userlist.
' BUGFIX (IE7): When name is set to action it is not possible to click the links on this page. 
' Solution worked without bugfix in Firefox
' Updated date: 2008.02.07
'***********************************************************************************
' -----------------------------------------------------------------------------
' RenderUserList(ByVal OrgID)
'
' Description: Simple list retrieval function will return HTML.
'
' Parameters:  OrgID - the companyid
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderUserList(ByVal OrgID)

	Dim rsUsers
	Dim htmContent
	Dim iUserCount
	Dim uid, org


	Set rsUsers = RetrieveCompanyUsers(OrgID, iUserCount)
	org = GetCompanyName(OrgID)


	htmContent = "<form name='profile' method='post'><TABLE border='0' width='620'>"_
				& "<TR><TD width='60' class='main_heading2'>Bruker</TD>"_
				& "<TD class='adminBlueLink' width='120'><A class='admin' HREF=""javascript:setParam(' ', 'EditProfile.asp')"">"_
				& "Opprett ny bruker</TD>"

	if Session("Role") = "Admin" then
		htmContent = htmContent	& "<TD class='adminBlueLink' width='140'><A class='admin' HREF=""javascript:setParam(' ', 'CompanyList.asp')"">"_
				& "Tilbake til kundelisten</TD>"
	end if

	htmContent = htmContent	& "<TD class='adminBlueLink' width='90' align='right'><A class='admin' HREF='ChangePassword.asp?option=yes&idOrg=" & OrgID & "'>Endre passord</a></TD>" _
				& "<TD class='adminBlueLink' width='95' align='right'><A class='admin' HREF='SendMail.asp?idOrg=" & OrgID & "'>Send e-post</a></TD>" _
				& "<TD class='adminBlueLink' width='70' align='right'><A class='admin' HREF=""javascript:setParam(' ', 'User_homepage.asp')"">"_
				& "Til portalen</TD>" _
				& "<td class='adminBlueLink' width='60' align='right'><A class='admin' HREF=""javascript:setParam(' ', '../Authfiles/Logout.asp')"">"_
				& "Logg ut</TD></TR>" _
				& "</TABLE>"

	htmContent = htmContent & "<TABLE id='sortUserList' border='0' class='news'>"_
				& "<input type='hidden' name='huid'>"_
				& "<input type='hidden' name='hcoid' value='" &OrgID& "'>"

	' Headings
	htmContent = htmContent & "<THEAD><TR>"_
				& "<TH align='left' width='10'>&nbsp;</td>" _
				& "<TH align='left' Width='85'>Brukernavn</TH>" _
				& "<TH align='left' width='160'>Navn</TH>" _
				& "<TH align='left' width='90'> Mobiltelefon</TH>" _
				& "<TH align='left' width='150'>E-post</TH>" _
				& "<TH align='left' width='50'>Endre</TH>" _
				& "<TH align='left' width='55'>Slette</TH>" _
				& "</TR></THEAD><TBODY>" & vbcrlf


	' Search bar
'	htmContent = htmContent & "<TR><TD align='left'><input type='text' name='txtUsername' style='HEIGHT: 16px; width: 80px;' class='news11' value=''></TD>" _
'				& "<TD align='left'><input type='text' name='txtName' style='HEIGHT: 16px; width: 170px;' class='news11' value=''></TD>" _
'				& "<TD align='left'><input type='text' name='txtTelefon' style='HEIGHT: 16px; width: 75px;' class='news11' value=''></TD>" _
'				& "<TD align='left'><input type='text' name='txtEpost' style='HEIGHT: 16px; width: 150px;' class='news11' value=''></TD>" _
'				& "<TD><INPUT class='formbutton' type='button' value='S�k' name='searchUser' style='background:#bfccd9 none; color:#003366; width:50px'></TD>"_
'				& "</TR>"

	' Trying to get only 5 of the users
	

  	' while not rsUsers.EOF
  	dim intRecord, intPage
  	dim intPageCount 
  	' intPageCount = rsUsers.PageCount
  	
  	Select Case Request("Action")
		case "&lt;&lt;"
			intpage = 1
		case "&lt;"
			intpage = Request("intpage")-1
			if intpage < 1 then intpage = 1
		case "&gt;"
			intpage = Request("intpage")+1
			if intpage > intPageCount then intpage = IntPageCount
		Case "&gt;&gt;"
			intpage = intPageCount
		case else
			intpage = 1
	end select
	
	' Line below is marked out because we want the list to work while we wait
	' for an updated MDAC. 
	' rsUsers.AbsolutePage = intPage
	
	' removed name=action in form below. 
	' htmContent = htmContent & "<tr><td colspan = 7 align = center><input type=""submit"" value=""&lt;&lt;"">" & _
' "<input type=""submit"" value=""&lt;"">" & _
' "<input type=""submit"" value=""&gt;"">" & _
' "<input type=""submit"" value=""&gt;&gt;""></td></tr>" & vbcrlf

	dim icounter
	icounter = 0
	response.write (htmContent) 
	htmContent = ""
	while not rsUsers.EOF
  	' for intRecord = 1 to rsUsers.PageSize
  	

  		' uid = rsUsers.fields("GeneralInfo.user_id")
  		uid = rsUsers.fields(1)
	  	htmContent = htmContent & ("<TR>" & vbcrlf )

	  	htmContent = htmContent & ("<td width = 10><input type = 'checkbox' name = 'del[]' value = '" & uid & "'></td>" & vbcrlf )
	  	' htmContent = htmContent & "<input type = 'hidden' name = 'pdel[]' value = '" & rsUsers.fields("GeneralInfo.logon_name") & "'>" & vbcrlf
	  	htmContent = htmContent & ("<input type = 'hidden' name = 'pdel[]' value = '" & rsUsers.fields(0) & "'>" & vbcrlf )
	  	htmContent = htmContent & ("<TD Width='85'>" & vbcrlf)
	  	

'		htmContent = htmContent & "<A class='admin' HREF=""javascript:setParam('" &uid& "','ViewProfile.asp')"">" _
'					& rsUsers.fields("GeneralInfo.logon_name") & " : </A></TD><TD width='170'>" & vbcrlf
	htmContent = htmContent & ("<A class='admin' HREF=""javascript:setParam('" &uid& "','ViewProfile.asp')"">" _
				& rsUsers.fields(0) & " : </A></TD><TD width='170'>" & vbcrlf )
		' If the user is admin, we'll show that with a color.
		' if rsUsers.Fields("BusinessDesk.partner_desk_role") = 2 then
		if rsUsers.Fields(7) = 2 then
 			htmContent = htmContent & ("<FONT color=magenta>") ' Administrator
		end if
		' htmContent = htmContent & rsUsers.fields("GeneralInfo.last_name") & ", " _
		'			& rsUsers.fields("GeneralInfo.first_name") & "&nbsp;"
		htmContent = htmContent & (rsUsers.fields(2) & ", " _
					& rsUsers.fields(3) & "&nbsp;")

		' If the user is admin, we'll show that with a color.
		' if rsUsers.Fields("BusinessDesk.partner_desk_role") = 2 then
		if rsUsers.Fields(7) = 2 then
 			htmContent = htmContent & ("</FONT>") ' Administrator
		end if
		'htmContent = htmContent & "</TD><TD width='80'>" & vbcrlf _
		'			& rsUsers.fields("GeneralInfo.tel_extension") & "&nbsp;" & vbcrlf _
		'			& rsUsers.fields("GeneralInfo.tel_number") & "&nbsp;</TD><TD width='150'>" & vbcrlf _
		'			& "<A HREF=""mailto:" & rsUsers.fields("GeneralInfo.email_address") & """>" & rsUsers.fields("GeneralInfo.email_address") & "</A>&nbsp;" & vbcrlf _
		'			& "</TD><TD>" & vbcrlf _
		'			& "<A HREF=""javascript:setParam('" &uid& "','EditProfile.asp')""" & vbcrlf _
		'			& ">endre</a>" & vbcrlf _
		'			& "</TD><TD width 55>" & vbcrlf _
		'			& "<A HREF=""javascript:setParam('" &uid& "','DeleteProfile.asp')"">slette</a>" & vbcrlf _
		'			& "</TD>" & vbcrlf
					
		htmContent = htmContent & ("</TD><TD width='80'>" & vbcrlf _
					& rsUsers.fields(6) & "&nbsp;" & vbcrlf _
					& rsUsers.fields(5) & "&nbsp;</TD><TD width='150'>" & vbcrlf _
					& "<A HREF=""mailto:" & rsUsers.fields(4) & """>" & rsUsers.fields(4) & "</A>&nbsp;" & vbcrlf _
					& "</TD><TD>" & vbcrlf _
					& "<A HREF=""javascript:setParam('" &uid& "','EditProfile.asp')""" & vbcrlf _
					& ">endre</a>" & vbcrlf _
					& "</TD><TD width 55>" & vbcrlf _
					& "<A HREF=""javascript:setParam('" &uid& "','DeleteProfile.asp')"">slette</a>" & vbcrlf _
					& "</TD>" & vbcrlf)

  		htmContent = htmContent & ("</TR>" & vbcrlf)
  		icounter=icounter+1 
  		
  		if icounter = 50 then
  			response.write (htmContent)
  			response.flush()
  			htmContent = ""
  			icounter = 0
  		end if

		rsUsers.MoveNext
	wend
	response.write (htmContent)
	htmContent = ""

	' if rsUsers.EOF then exit for
	' next
	' BUGFIX (IE7): When name is set to action it is not possible to click the links on this page. 
	' Removed name=action from below code.
	
	'htmContent = htmContent & "<tr><td colspan = 7 align = center><input type=""submit"" value=""&lt;&lt;"">" & _
'"<input type=""submit"" value=""&lt;"">" & _
'"<input type=""submit"" value=""&gt;"">" & _
'"<input type=""submit"" value=""&gt;&gt;""></td></tr>" & vbcrlf
	
	' htmContent = htmContent & "<tr><td colspan = 7 align = left><input type = ""submit"" name = ""delete"" value = ""Slett valgte brukere"" onclick=""javascript:setParam('" &uid& "','DeleteManyProfiles.asp');""></td></tr>" & vbcrlf
	htmContent = htmContent & "<tr><td colspan = 7 align = left><input type = ""submit"" name = ""delete"" value = ""Slett valgte brukere"" onclick=""javascript:setParam(' ','DeleteManyProfiles.asp');""></td></tr>" & vbcrlf

	If iUserCount = 0 Then
		htmContent = htmContent & "Ingen brukere er registrert"
	End If

	htmContent = htmContent & "</TBODY></TABLE></table></form>"

	RenderUserList =  htmContent
End Function

' -----------------------------------------------------------------------------
' RenderCompanyList()
'
' Description: Simple list retrieval function that will return all the company's registered
' in the commerce server in HTML.
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderCompanyList()

	Dim rsActiveCompany, rsInActiveCompany
	Dim htmContent
	Dim iActiveCompanyCount, iInActiveCompanyCount
	Dim coid

	Set rsActiveCompany = RetrieveAllActiveCompany(iActiveCompanyCount)
	Set rsInActiveCompany = RetrieveAllInActiveCompany(iInActiveCompanyCount)

	htmContent = "<TABLE border='0' width='600'><form name='profile' method='post'>"_
				& "<TR><TD width='300' class='main_heading2'>Navn</TD>"_
				& "<TD class='adminBlueLink' width='150'><A class='admin' HREF=""javascript:setParam('', 'EditOrgProfile.asp')"">"_
				& "Opprett ny kunde</TD>" _
				& "<TD class='adminBlueLink' width='70' align='right'><A class='admin' HREF=""javascript:setParam(' ', 'User_homepage.asp')"">"_
				& "Til portalen</TD>" _
				& "<TD class='adminBlueLink' width='65' align='right'><A class='admin' HREF=""javascript:setParam(' ', '../Authfiles/Logout.asp')"">"_
				& "Logg ut</TD></TR>" _
				& "</TABLE>"

	If iActiveCompanyCount > 0 Then
		htmContent = htmContent & "<TABLE border='0' class='news'>"_
						& "<input type='hidden' name='hcoid'>"

  		while not rsActiveCompany.EOF
  			coid = rsActiveCompany.fields("GeneralInfo.org_id")

			Dim objOrgProfile, admin

			admin = false

			Set objOrgProfile = GetCompanyProfile(coid)

			if objOrgProfile.GeneralInfo.admin = "1" then
				admin = true
			end if

			'instead of this, just check the property in the organization-profile
			'it should take care of the problem with slow loading of pages SS 5/7 2002
			'******************************************************
			'Dim rsUsers, iCount, admin
			'Set rsUsers = RetrieveCompanyUsers(coid, iCount)

			'admin = false

			'if iCount = 0 then
			'	admin = false
			'else
			'	while not rsUsers.EOF
	  			'as long as there are more users

			'		Dim idUser, ObjUserProfile

					'must get profileobject to find wheter an admin exists
			'		idUser = rsUsers.fields("GeneralInfo.user_id")

			'		Set objUserProfile = GetUserProfile(idUser)

			'		if not objUserProfile is nothing then
			'			Dim role
			'			role = objUserProfile.BusinessDesk.partner_desk_role
			'			if role = "2" then
			'				admin = true
			'			end if
			'		end if

			'		'go to next user
			'		rsUsers.MoveNext
			'	wend
			'end if
			'********************************************************

  			htmContent = htmContent & "<TR><TD Width=""300"">"_
  						& "<A class='admin' HREF=""javascript:setParam('" &coid& "', 'ViewOrg.asp')"">" _
						& rsActiveCompany.fields("GeneralInfo.name") & "</A>"_
  						& "</TD><TD width='40'>" _
  						& "<A HREF=""javascript:setParam('" &coid& "', 'EditOrgProfile.asp')""" _
  						& ">endre</a>" _
  						& "</TD><TD width='40'>"_
  						& "<A HREF=""javascript:setParam('" &coid& "', 'DeleteProfile.asp')"">slette</a>" _
						& "</TD><TD width='70'>"_
						& "<A HREF=""javascript:setParam('" &coid& "', 'UserList.asp')""" _
  						& ">se brukere</a>"

  			if not admin then
  				htmContent = htmContent & "</TD><TD align=""bottom"">"_
							& "<A HREF=""javascript:setParam('" &coid& "', 'NewAdminProfile.asp')""" _
  							& ">opprett ny administrator</a>"

  			end if

  			htmContent = htmContent & "</TD><TD>"_
  						& "</TD></TR>"

			rsActiveCompany.MoveNext
		wend
		htmContent = htmContent & "</form></TABLE><hr align='left' width='620px'>"
	else
		htmContent = htmContent & "<div class='main_heading2'>Ingen aktive kunder er registrert.</div><hr align='left' width='620px'>"
	End If

	if iInActiveCompanyCount > 0 Then
		htmContent = htmContent & "<TABLE border='0' class='news'>"_
						& "<input type='hidden' name='hcoid'>"

		while not rsInActiveCompany.EOF
  			coid = rsInActiveCompany.fields("GeneralInfo.org_id")

  			htmContent = htmContent & "<TR><TD Width=""300"">"_
  						& "<A class='admin' HREF=""javascript:setParam('" &coid& "', 'ViewOrg.asp')"">" _
						& rsInActiveCompany.fields("GeneralInfo.name") & "</A>"_
  						& "</TD><TD width='40'>"_
  						& "<A HREF=""javascript:setParam('" &coid& "', 'ReactivateProfile.asp')"">aktiver</a>" _
  						& "</TD></TR>"

			rsInActiveCompany.MoveNext
		wend
		htmContent = htmContent & "</form></TABLE>"

	else
		htmContent = htmContent & "<div class='main_heading2'>Ingen inaktive kunder er registrert.</div>"
	End If

	RenderCompanyList =  htmContent
End Function

' -----------------------------------------------------------------------------
' RenderUserProfile(ByVal UID)
'
' Description: A function that returns HTML that show the profile for the spesified user
'
' Parameters:  UID - the users id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderUserProfile(ByVal UID)
	Dim htmContent, org, role
	Dim rsUser

	Set rsUser = GetUserProfile(UID)
	org = GetCompanyName(rsUser.fields("AccountInfo.org_id"))
	role = rsUser.Fields("BusinessDesk").Value("partner_desk_role")

	htmContent = "<TR><TD Width='100'>Fornavn </TD><TD>" & rsUser.Fields("GeneralInfo").Value("first_name") & "</TD></TR>" _
	             & "<TR><TD>Etternavn: </TD><TD>" & rsUser.Fields("GeneralInfo").Value("last_name") & "</TD></TR>" _
	             & "<TR><TD>E-post: </TD><TD>" & rsUser.Fields("GeneralInfo").Value("email_address") & "</TD></TR>" _
	             & "<TR><TD>Mobiltelefon: </TD><TD>" & rsUser.Fields("GeneralInfo").Value("tel_number") & "</TD></TR>" _
	             & "<TR><TD>Firma: </TD><TD>" &org& "</TD></TR>"_
	             & "<TR><TD>Rolle: </TD><TD>"

				 if role = "2" then
	             	htmContent = htmContent & "Administrator"
	             else ' if role = "1" then
					htmContent = htmContent & "Vanlig bruker"
'	             elseif role = "3" then
'	             	htmContent = htmContent & "Superbruker"
	             end if

	htmContent = htmContent & "</TD></TR>"

	RenderUserProfile = htmContent

End Function


' -----------------------------------------------------------------------------
' RenderOrgProfile(ByVal CoID)
'
' Description: A function that returns HTML that show the profile for the spesified company
'
' Parameters:  CoID - the users id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderOrgProfile(ByVal CoID)
	Dim htmContent, rsCompany, rsAdr, iCount,idChange, strChangeName

	Set rsCompany = GetCompanyProfile(CoID)
	Set rsAdr = GetAddressProfile(CoID, iCount)

	idChange = rsCompany.Fields("GeneralInfo").Value("user_id_changed_by")

	if Left(idChange, 1) = "{" then
		strChangeName = GetUserName(idChange)
	else
		strChangeName = idChange
	end if

	htmContent = "<TR><TD Width='100'>Navn: </TD><TD>" & rsCompany.Fields("GeneralInfo").Value("name") & "</TD></TR>" _
	            & "<TR><TD>Kontakt: </TD><TD>" & rsCompany.Fields("GeneralInfo").Value("user_id_admin_contact") & "</TD></TR>" _
	            & "<TR><TD>Passord: </TD><TD>" & rsCompany.Fields("GeneralInfo").Value("org_password") & "</TD></TR>"_
				& "<TR><TD>Passordet byttes etter: </TD><TD>" & rsCompany.Fields("GeneralInfo").Value("org_password_expire") & " dager</TD></TR>" _
				& "<TR><TD>Siste endring: </TD><TD>" & rsCompany.Fields("ProfileSystem").Value("date_last_changed") & "</TD></TR>" _
				& "<TR><TD>Endring foretatt av: </TD><TD>" & strChangeName  & "</TD></TR>" _
				& "<TR><TD colspan='2'><hr align='left' width='300'></TD></TR>"


	if iCount = 1 then
	'show address-information
		htmContent = htmContent & "<TR><TD>Adresse: </TD><TD>" _
				& rsAdr.fields("GeneralInfo.address_line1") &"</TD></TR>" _
				& "<TR><TD>Postnummer: </TD><TD>" _
				& rsAdr.fields("GeneralInfo.postal_code") & "</TD></TR>" _
				& "<TR><TD>Poststed: </TD><TD>" _
				& rsAdr.fields("GeneralInfo.city") & "</TD></TR>" _
				& "<TR><TD>Telefonnummer: </TD><TD>" _
				& rsAdr.fields("GeneralInfo.tel_number") & "</TD></TR>" _
				& "<tr><td colspan='2'>&nbsp</td></tr>"
	end if

	htmContent = htmContent & "<TR><TD colspan='2' height='20px'>Tilgang til nyheter:</TD>"_
				& "<tr><TD>" _
				& "<INPUT type='checkbox' disabled id='cb_0' name='chkAccessRight' value='0'>Innenriks<br>"_
				& "<INPUT type='checkbox' disabled id='cb_1' name='chkAccessRight' value='1'>Utenriks<br>"_
				& "<INPUT type='checkbox' disabled id='cb_2' name='chkAccessRight' value='2'>Sport<br>"_
				& "<INPUT type='checkbox' disabled id='cb_3' name='chkAccessRight' value='3'>Kultur og underholdning<br>"_
				& "<INPUT type='checkbox' disabled id='cb_4' name='chkAccessRight' value='4'>Priv til red<br>"_
				& "<INPUT type='checkbox' disabled id='cb_11' name='chkAccessRight' value='11'>Cyberwatcher<br>"_
				& "<INPUT type='checkbox' disabled id='cb_12' name='chkAccessRight' value='12'>Nyhetskalenderen<br>"_
				& "<hr>"_
				& "<INPUT type='checkbox' disabled id='cb_13' name='chkAccessRight' value='13'>SMS WAP<br>"_
				& "<INPUT type='checkbox' disabled id='cb_14' name='chkAccessRight' value='14'>Lyd<br>"_
				& "</td><td>"_
				& "<INPUT type='checkbox' disabled id='cb_16' name='chkAccessRight' value='16'>Innenriksarkiv<br>"_
				& "<INPUT type='checkbox' disabled id='cb_17' name='chkAccessRight' value='17'>Utenriksarkiv<br>"_
				& "<INPUT type='checkbox' disabled id='cb_18' name='chkAccessRight' value='18'>Sportsarkiv<br>"_
				& "<INPUT type='checkbox' disabled id='cb_19' name='chkAccessRight' value='19'>Kultur- og underholdningarkiv<br>"_
				& "<INPUT type='checkbox' disabled id='cb_20' name='chkAccessRight' value='20'>Priv til red-arkiv<br>"_
				& "<hr>"_
				& "<INPUT type='checkbox' disabled id='cb_30' name='chkAccessRight' value='30'>Lydarkiv<br>"_
				& "<br>"_
				& "</td></tr><tr><td colspan='2'>&nbsp</td></tr>"

	'get accesslevel
	Dim iAccess
	iAccess = rsCompany.Fields("GeneralInfo").Value("accesslevel")

	if iAccess <> "" then
		'get the right format for the accesslevel
		Dim objFormatter, acf, iAcf
		Set objFormatter = Server.CreateObject("Formating.bitwise")
		'objFormatter.LongToHTMLCheckboxArray(acf) will give an array of strings with some HTML code
		'objFormatter.LongToBitArray(acf) will give an array of integers representing bit values

		acf = objFormatter.LongToBitArray(CStr(iAccess))
		Set objFormatter = Nothing

		htmContent = htmContent & "<script language='javascript'>"

		'show the selected accesslevel
		For iAcf=0 to Ubound(acf)
			if acf(iAcf) = 1 then
				Select case iAcf
					case 0,1,2,3,4
						htmContent = htmContent & "document.forms.frmProfile.elements.chkAccessRight[" & iAcf & "].checked=true;"
					case 14,13,12,11
						htmContent = htmContent & "document.forms.frmProfile.elements.chkAccessRight[" & (iAcf-6) & "].checked=true;"
					case 16,17,18,19,20
						htmContent = htmContent & "document.forms.frmProfile.elements.chkAccessRight[" & (iAcf-7) & "].checked=true;"
					case 30
						htmContent = htmContent & "document.forms.frmProfile.elements.chkAccessRight[" & (iAcf-16) & "].checked=true;"
				end Select
			end if
		next
		htmContent = htmContent & "</script>"
	end if

	RenderOrgProfile = htmContent

End Function

' -----------------------------------------------------------------------------
' RenderEditUserProfile(ByVal UID, ByVal CoID)
'
' Description: A function that returns HTML that show a edit window for the spesified user
'
' Parameters:  UID - the users id
'				CoID - the company id
'				UpdateProfile - If a normal user must update his profile
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderEditUserProfile(ByVal UID, ByVal CoID, ByVal bUpdateProfile, ByVal bNormalUpdate)

  Dim htmContent
  Dim rsUser, sCompanyName, role

  sCompanyName = GetCompanyName(CoID)

  Set rsUser = GetUserProfile(UID)
  role = rsUser.Fields("BusinessDesk").Value("partner_desk_role")

  if bUpdateProfile and not bNormalUpdate then
	htmContent = htmContent & "<TR><TD Width='350' colspan='2' class='news'><FONT color='red'>Profilen din er ikke fullstending. Vennligst oppdater de feltene som mangler.</FONT></TD></TR>"
  end if

  htmContent = htmContent & "<TR><TD Width='350' colspan='2' class='news'>Feltene merket med stjerne er obligatoriske. </TD></TR>"

  'all textboxes with content
  htmContent = htmContent & "<TR><TD Width=""120"">Fornavn: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtFirstname"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value=""" & vbcrlf _
				& rsUser.fields("GeneralInfo.first_name") & """> *</TD></TR>" & vbcrlf _
				& "<TR><TD>Etternavn: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtLastname"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value=""" & vbcrlf _
				& rsUser.fields("GeneralInfo.last_name") & """> *</TD></TR>" & vbcrlf _
				& "<TR><TD>E-post: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtEmail"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value=""" & vbcrlf _
				& rsUser.fields("GeneralInfo.email_address") & """> *</TD></TR>" & vbcrlf _
				& "<TR><TD>Mobiltelefon: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtPhone"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value=""" & vbcrlf _
				& rsUser.fields("GeneralInfo.tel_number") & """> *</TD></TR>" & vbcrlf _
				& "<TR><TD>Firma: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtFirm"" disabled style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value=""" & vbcrlf _
				& sCompanyName &"""></TD></TR>" & vbcrlf 

	if not bUpdateProfile then
	' Commented out the role 'Superbruker'
'		if role = "3" then
'			htmContent = htmContent & "<select name=""txtRole"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"">" _
'					& "<option value='1'>Vanlig bruker</option>"_
'					& "<option value='3' selected>Superbruker</option>"_
'					& "</TD></TR>"
'		elseif role = "2" then
		htmContent = htmContent & "<TR><TD>Rolle: </TD><TD align=""left"">"
		if role = "2" then
			htmContent = htmContent & "<select name=""txtRole"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"">" _
					& "<option value='1'>Vanlig bruker</option>"_
					& "<option value='2' selected>Administrator</option>"_
					& "</TD></TR>"
		else
			htmContent = htmContent & "<select name=""txtRole"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"">" _
					& "<option value='1' selected>Vanlig bruker</option>"_
					& "</TD></TR><TR><TD>&nbsp</TD></TR>"
		end if
	end if

  'buttons and hidden fields
  htmContent = htmContent & "<input type=""hidden"" name=""huid"" value=""" &huid& """>" _
			& "<input type=""hidden"" name=""hcoid"" value=""" &CoID& """>" _
			& "<input type=""hidden"" name=""saveProfile"" value='Lagre endring'>"_
			& "<input type=""hidden"" name=""txtUsername"" value=""" & rsUser.fields("GeneralInfo.logon_name") & """" & """>" _
			& "<tr>"

	' Show only Save button if the UpdateProfile is true
	htmContent = htmContent & "<TD colspan='2'>"
	if not bUpdateProfile then
		htmContent = htmContent & "<INPUT class='formbutton' type='button' value='Tilbake til listen' name='back' onClick='javascript:GetToList(0)' style='background:#bfccd9 none; color:#003366; width:100px'> "_
			& "<INPUT class='formbutton' type='button' value='Angre' onClick='SetDefault()' name='reset' style='background:#bfccd9 none; color:#003366; width:100px'> " _
			& "<INPUT class='formbutton' type='button' value='Lagre endring' name='saveProfile' onClick='CheckInput(1)' style='background:#bfccd9 none; color:#003366; width:100px'> "
	else
		' Add a back button for users that explicit want to update their profile.
		if bNormalUpdate then
			htmContent = htmContent & "<INPUT class='formbutton' type='button' value='Tilbake til listen' name='back' onClick='GetToList(1)' style='background:#bfccd9 none; color:#003366; width:100px'> "
		end if
		htmContent = htmContent & "<INPUT class='formbutton' type='button' value='Lagre endring' name='saveProfile' onClick='CheckInput(2)' style='background:#bfccd9 none; color:#003366; width:100px'> "
	end if

    htmContent = htmContent & "<TR><TD Width='350' colspan='2' class='news'><BR>Til informasjon:" & _
			"<BR>Mobiltelefon og e-post er n�dvendig for � varsle om passordbytte eller hvis du glemmer passordet." & _
			"<BR>I tillegg til utsending av e-post med informasjon fra administratorer." & _
			"<BR>Kontaktinformasjonen vil ikke bli gitt videre og kun benyttet i forbindelse med NTB Portalen.</TD></TR>"
	htmContent = htmContent & "</td></tr>"

  RenderEditUserProfile = htmContent

End Function

' -----------------------------------------------------------------------------
' RenderEditOrgProfile(ByVal CoID)
'
' Description: A function that returns HTML that show a edit window for the spesified company
'
' Parameters:  CoID - the company id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderEditOrgProfile(ByVal CoID)
  Dim htmContent
  Dim rsOrg, rsAdr, iCount

  Set rsOrg = GetCompanyProfile(CoID)
  Set rsAdr = GetAddressProfile(CoID, iCount)

  'all textboxes with content
  htmContent = "<input type=""hidden"" name=""txtName"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				& rsOrg.fields("GeneralInfo.name") & """>" _
				& "<TR><TD>Kontakt: </TD><TD>" _
				& "<input type=""text"" name=""txtContact"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				& rsOrg.fields("GeneralInfo.user_id_admin_contact") & """></TD></TR>" _
				& "<TR><TD>Passord: </TD><TD>" _
				& "<input type=""text"" name=""txtPassword"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				& rsOrg.fields("GeneralInfo.org_password") & """></TD></TR>" _
				& "<TR><TD>Passordet byttes etter: </TD><TD>" _
				& "<input type=""text"" name=""txtPasswordExpire"" style=""HEIGHT: 16px; width: 30px;"" class=""news11"" value="""_
				& rsOrg.fields("GeneralInfo.org_password_expire") & """> dager (0 = disable)</TD></TR>"

  if iCount = 1 then
  'show address-information
		htmContent = htmContent & "<TR><TD>Adresse: </TD><TD>" _
				& "<input type=""text"" name=""txtAddress"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				& rsAdr.fields("GeneralInfo.address_line1") & """></TD></TR>" _
				& "<TR><TD>Postnummer: </TD><TD>" _
				& "<input type=""text"" name=""txtPostalCode"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				& rsAdr.fields("GeneralInfo.postal_code") & """></TD></TR>" _
				& "<TR><TD>Poststed: </TD><TD>" _
				& "<input type=""text"" name=""txtCity"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				& rsAdr.fields("GeneralInfo.city") & """></TD></TR>" _
				& "<TR><TD>Telefonnummer: </TD><TD>" _
				& "<input type=""text"" name=""txtPhone"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				& rsAdr.fields("GeneralInfo.tel_number") & """></TD></TR>" _
				& "<tr><td colspan='2'>&nbsp</td></tr>"
   else
   'show empty textboxes
		htmContent = htmContent & "<TR><TD>Adresse: </TD><TD>" _
				& "<input type=""text"" name=""txtAddress"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""></TD></TR>" _
				& "<TR><TD>Postnummer: </TD><TD>" _
				& "<input type=""text"" name=""txtPostalCode"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""></TD></TR>" _
				& "<TR><TD>Poststed: </TD><TD>" _
				& "<input type=""text"" name=""txtCity"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""></TD></TR>" _
				& "<TR><TD>Telefonnummer: </TD><TD>" _
				& "<input type=""text"" name=""txtPhone"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""></TD></TR>" _
				& "<tr><td colspan='2'>&nbsp</td></tr>"
   end if

	'render checkboxes to show accesslevel
	htmContent = htmContent & "<TR><TD colspan='2' height='20px'>Tilgang til nyheter:</TD>"_
				& "<tr><TD>" _
				& "<INPUT type='checkbox' id='cb_0' name='chkAccessRight' value='0'>Innenriks<br>"_
				& "<INPUT type='checkbox' id='cb_1' name='chkAccessRight' value='1'>Utenriks<br>"_
				& "<INPUT type='checkbox' id='cb_2' name='chkAccessRight' value='2'>Sport<br>"_
				& "<INPUT type='checkbox' id='cb_3' name='chkAccessRight' value='3'>Kultur og underholdning<br>"_
				& "<INPUT type='checkbox' id='cb_4' name='chkAccessRight' value='4'>Priv til red<br>"_
				& "<INPUT type='checkbox' id='cb_11' name='chkAccessRight' value='11'>Cyberwatcher<br>"_
				& "<INPUT type='checkbox' id='cb_12' name='chkAccessRight' value='12'>Nyhetskalenderen<br>"_
				& "<hr>"_
				& "<INPUT type='checkbox' id='cb_13' name='chkAccessRight' value='13'>SMS WAP<br>"_
				& "<INPUT type='checkbox' id='cb_14' name='chkAccessRight' value='14'>Lyd<br>"_
				& "</td><td>"_
				& "<INPUT type='checkbox' id='cb_16' name='chkAccessRight' value='16'>Innenriksarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_17' name='chkAccessRight' value='17'>Utenriksarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_18' name='chkAccessRight' value='18'>Sportsarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_19' name='chkAccessRight' value='19'>Kultur- og underholdningarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_20' name='chkAccessRight' value='20'>Priv til red-arkiv<br>"_
				& "<hr>"_
				& "<INPUT type='checkbox' id='cb_30' name='chkAccessRight' value='30'>Lydarkiv<br>"_
				& "<br>"_
				& "</td></tr><tr><td colspan='2'>&nbsp</td></tr>"

	'get accesslevel
	Dim iAccess
	iAccess = rsOrg.Fields("GeneralInfo").Value("accesslevel")

   	if iAccess <> "" then
		'get the right format for the accesslevel
		Dim objFormatter, acf, iAcf
		Set objFormatter = Server.CreateObject("Formating.bitwise")
		'objFormatter.LongToHTMLCheckboxArray(acf) will give an array of strings with some HTML code
		'objFormatter.LongToBitArray(acf) will give an array of integers representing bit values

		acf = objFormatter.LongToBitArray(CStr(iAccess))
		Set objFormatter = Nothing

		htmContent = htmContent & "<script language='javascript'>"

		'show the selected accesslevel
		For iAcf=0 to Ubound(acf)
			if acf(iAcf) = 1 then
				Select case iAcf
					case 0,1,2,3,4
						htmContent = htmContent & "document.forms.frmChangeProfile.elements.chkAccessRight[" & iAcf & "].checked=true;"
					case 14, 13, 12, 11
						htmContent = htmContent & "document.forms.frmChangeProfile.elements.chkAccessRight[" & (iAcf-6) & "].checked=true;"
					case 16,17,18,19,20
						htmContent = htmContent & "document.forms.frmChangeProfile.elements.chkAccessRight[" & (iAcf-7) & "].checked=true;"
					case 30
						htmContent = htmContent & "document.forms.frmChangeProfile.elements.chkAccessRight[" & (iAcf-16) & "].checked=true;"
				end Select
			end if
		next
		htmContent = htmContent & "</script>"

	end if

  'buttons and hidden fields
  htmContent = htmContent & "<input type=""hidden"" name=""hcoid"" value=""" & CoID & """>" _
			& "<input type=""hidden"" name=""changeOrg"" value=""change"">" _
			& "<tr><TD colspan='2'><a href=""http://myportal.cyberwatcher.com/ntb/login.aspx?user=" & GetCompanyName(coid) & """ target=""_blank"">" & "Redigere Cyberwatcher-oppsett for " & GetCompanyName(coid) & "</A>" _
			& "</TD></TR>" _
			& "<tr><TD colspan='2'>&nbsp;</TD></TR>" _
			& "<tr><TD colspan='2'>" _
			& "<INPUT class='formbutton' type='button' value='Tilbake til listen' name='back' onClick='GetToList()' style='background:#bfccd9 none; color:#003366; width:100px'>"_
			& "<INPUT class='formbutton' type='button' value='Angre' onClick='SetDefault()' name='reset' style='background:#bfccd9 none; color:#003366; width:100px'>"_
			& "<INPUT class='formbutton' type='button' value='Lagre endring' name='saveProfile' onClick='CheckInput()' style='background:#bfccd9 none; color:#003366; width:100px'>"_
			& "</td></tr>"

  RenderEditOrgProfile = htmContent

End Function

' -----------------------------------------------------------------------------
' RenderNewUserProfile(ByVal CoID)
'
' Description: A function that returns HTML that show a input window for a new user
'
' Parameters:  CoID - the company id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderNewUserProfile(ByVal CoID)
  Dim htmContent, sCompanyName

  sCompanyName = GetCompanyName(CoID)

  htmContent = "<TR><TD Width='350' colspan='2' class='news'>Alle brukernavn skal best� av organisasjonsforkortelse, etterfulgt av bindestrek og selve brukeridentiteten, som godt kan v�re den samme som brukes ellers i organisasjonen. <br/>(Det er felles passord for alle brukere i organisasjonen)</TD></TR>" _
				& "<TR><TD Width='350' colspan='2' class='news'>Feltene merket med stjerne er obligatoriske. </TD></TR>"

  htmContent = htmContent & "<TR><TD Width=""100"">Brukernavn: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtUsername"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" & vbcrlf 

  htmContent =  htmContent & "<TR><TD Width=""100"">Fornavn: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtFirstname"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" & vbcrlf _
				& "<TR><TD>Etternavn: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtLastname"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" & vbcrlf _
				& "<TR><TD>E-post: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtEmail"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" & vbcrlf _
				& "<TR><TD>Mobiltelefon: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtPhone"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" & vbcrlf _
				& "<TR><TD>Firma: </TD><TD align=""left"">" & vbcrlf _
				& "<input type=""text"" name=""txtFirm"" disabled style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value='" & vbcrlf _
				& sCompanyName & "'></TD></TR>" & vbcrlf _
				& "<TR><TD>Rolle: </TD><TD align=""left"">" & vbcrlf _
				& "<select name=""txtRole"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"">" & vbcrlf _
				& "<option value='1'>Vanlig bruker</option></TD></TR>" & vbcrlf _
				& "<tr><td>Etter lagring vil jeg</td><td align = ""left"">" & vbcrlf _
				& "<select name =""addaction"" style =""height: 16px; width:150px;"" class=""news11"">" & vbcrlf _
				& "<option value='1'>Tilbake til brukerlisten</option>" & vbcrlf _
				& "<option value='2'>Tilbake til kundelisten</option>" & vbcrlf _
				& "<option value='3'>Legge til ny bruker</option>" & vbcrlf _
				& "</td></tr>" & vbcrlf 

  'buttons and hidden fields
  htmContent = htmContent & "<tr><td colspan='2'>" & vbcrlf _
			& "<input type=""hidden"" name=""makeNew"" value='newProfile'>" & vbcrlf _
			& "<input type=""hidden"" name=""hcoid"" value=""" &CoID& """>" & vbcrlf _
			& "<INPUT class='formbutton' type='button' value='Tilbake til listen' name='back' onClick='GetToList(0)' style='background:#bfccd9 none; color:#003366; width:100px'>" & vbcrlf _
			& "<INPUT class='formbutton' type='button' value='Angre' onClick='SetDefault()' name='reset' style='background:#bfccd9 none; color:#003366; width:100px'>" & vbcrlf _
			& "<INPUT class='formbutton' type='button' value='Lagre profil' onClick='CheckInput(0)' name='saveProfile' style='background:#bfccd9 none; color:#003366; width:100px'>" & vbcrlf _
			& "</td></tr>" & vbcrlf 

  RenderNewUserProfile = htmContent

End Function

' -----------------------------------------------------------------------------
' RenderNewAdminProfile(ByVal CoID)
'
' Description: A function that returns HTML that show a input window for a new admin user
'
' Parameters:  CoID - the company id
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderNewAdminProfile(ByVal CoID)
  Dim htmContent, sCompanyName

  sCompanyName = GetCompanyName(CoID)

  htmContent = "<TR><TD Width='350' colspan='2' class='news'>Feltene merket med stjerne er obligatoriske. </TD></TR>"

  'password should be the same as superuser, can be skipped
  htmContent = htmContent & "<TR><TD Width=""100"">Brukernavn: </TD><TD align=""left"">" _
				& "<input type=""text"" name=""txtUsername"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>"

  htmContent =  htmContent & "<TR><TD Width=""100"">Fornavn: </TD><TD align=""left"">" _
				& "<input type=""text"" name=""txtFirstname"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" _
				& "<TR><TD>Etternavn: </TD><TD align=""left"">" _
				& "<input type=""text"" name=""txtLastname"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" _
				& "<TR><TD>E-post: </TD><TD align=""left"">" _
				& "<input type=""text"" name=""txtEmail"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" _
				& "<TR><TD>Mobiltelefon: </TD><TD align=""left"">" _
				& "<input type=""text"" name=""txtPhone"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""> *</TD></TR>" _
				& "<TR><TD>Firma: </TD><TD align=""left"">" _
				& "<input type=""text"" name=""txtFirm"" disabled style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value='"_
				& sCompanyName & "'></TD></TR>"_
				& "<TR><TD>Rolle: </TD><TD align=""left"">"_
				& "<select name=""txtRole"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"">"_
				& "<option value='2' selected>Administrator</option>"_
				& "</TD></TR>"

  'buttons and hidden fields
  htmContent = htmContent & "<tr><td colspan='2'>" _
			& "<input type=""hidden"" name=""makeNew"" value='newProfile'>"_
			& "<input type=""hidden"" name=""hcoid"" value=""" &CoID& """>"_
			& "<INPUT class='formbutton' type='button' value='Tilbake til listen' name='back' onClick='GetToList()' style='background:#bfccd9 none; color:#003366; width:100px'> "_
			& "<INPUT class='formbutton' type='button' value='Angre' onClick='SetDefault()' name='reset' style='background:#bfccd9 none; color:#003366; width:100px'> "_
			& "<INPUT class='formbutton' type='button' value='Lagre profil' onClick='CheckInput()' name='saveProfile' style='background:#bfccd9 none; color:#003366; width:100px'>"_
			& "</td></tr>"

  RenderNewAdminProfile = htmContent

End Function


' -----------------------------------------------------------------------------
' RenderNewOrgProfile()
'
' Description: A function that returns HTML that show a input window for a new organization
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderNewOrgProfile()
  Dim htmContent

  htmContent = "<TR><TD Width=""100"">Navn: </TD><TD>" _
				& "<input type=""text"" name=""txtName"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""></TD></TR>" _
				& "<TR><TD>Kontakt: </TD><TD>"_
				& "<input type=""text"" name=""txtContact"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""></TD></TR>"_
				& "<TR><TD>Passord: </TD><TD>"_
				& "<input type=""text"" name=""txtPassword"" style=""HEIGHT: 16px; width: 150px;"" class=""news11""></TD></TR>"_
				& "<TR><TD>Adresse: </TD><TD>" _
				& "<input type=""text"" name=""txtAddress"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				&	"""></TD></TR>" _
				& "<TR><TD>Postnummer: </TD><TD>" _
				& "<input type=""text"" name=""txtPostalCode"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				&	"""></TD></TR>" _
				& "<TR><TD>Poststed: </TD><TD>" _
				& "<input type=""text"" name=""txtCity"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				&	"""></TD></TR>" _
				& "<TR><TD>Telefonnummer: </TD><TD>" _
				& "<input type=""text"" name=""txtPhone"" style=""HEIGHT: 16px; width: 150px;"" class=""news11"" value="""_
				&	"""></TD></TR>" _
				& "<tr><td colspan='2'>&nbsp</td></tr>"_
				& "<TR><TD colspan='2' height='20px'>Tilgang til nyheter:</TD>"_
				& "<tr><TD>" _
				& "<INPUT type='checkbox' id='cb_0' name='chkAccessRight' value='0'>Innenriks<br>"_
				& "<INPUT type='checkbox' id='cb_1' name='chkAccessRight' value='1'>Utenriks<br>"_
				& "<INPUT type='checkbox' id='cb_2' name='chkAccessRight' value='2'>Sport<br>"_
				& "<INPUT type='checkbox' id='cb_3' name='chkAccessRight' value='3'>Kultur og underholdning<br>"_
				& "<INPUT type='checkbox' id='cb_4' name='chkAccessRight' value='4'>Priv til red<br>"_
				& "<INPUT type='checkbox' id='cb_11' name='chkAccessRight' value='11'>Cyberwatcher<br>"_
				& "<INPUT type='checkbox' id='cb_12' name='chkAccessRight' value='12'>Nyhetskalenderen<br>"_
				& "<hr>"_
				& "<INPUT type='checkbox' id='cb_13' name='chkAccessRight' value='13'>SMS WAP<br>"_
				& "<INPUT type='checkbox' id='cb_14' name='chkAccessRight' value='14'>Lyd<br>"_
				& "</td><td>"_
				& "<INPUT type='checkbox' id='cb_16' name='chkAccessRight' value='16'>Innenriksarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_17' name='chkAccessRight' value='17'>Utenriksarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_18' name='chkAccessRight' value='18'>Sportsarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_19' name='chkAccessRight' value='19'>Kultur- og underholdningarkiv<br>"_
				& "<INPUT type='checkbox' id='cb_20' name='chkAccessRight' value='20'>Priv til red-arkiv<br>"_
				& "<hr>"_
				& "<INPUT type='checkbox' id='cb_30' name='chkAccessRight' value='30'>Lydarkiv<br>"_
				& "<br>"_
				& "</td></tr><tr><td colspan='2'>&nbsp</td></tr>"

  'buttons and hidden fields
  htmContent = htmContent _
			& "<input type=""hidden"" name=""makeNew"" value='newOrg'>"_
			& "<tr><TD colspan='2'>" _
			& "<INPUT class='formbutton' type='button' value='Tilbake til listen' name='back' onClick='GetToList()' style='background:#bfccd9 none; color:#003366; width:100px'>"_
			& "<INPUT class='formbutton' type='button' value='Angre' onClick='SetDefault()' name='reset' style='background:#bfccd9 none; color:#003366; width:100px'>"_
			& "<INPUT class='formbutton' type='button' value='Lagre endring' onClick='CheckInput()'name='saveProfile' style='background:#bfccd9 none; color:#003366; width:100px'>"_
			& "</td></tr>"

  RenderNewOrgProfile = htmContent

End Function

' -----------------------------------------------------------------------------
' RenderForgotPassword()
'
' Description: A function that returns HTML that show a input window for forgot password
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderForgotPassword()
  Dim htmContent

  htmContent = "<table border=""0"">" _
			   & "<FORM NAME=""frmLogin"" ACTION=""login.asp"" METHOD=""post""><tr>" _
			   & "<td colspan=""2"" height=""40""><div class=""news11"">Har du glemt passordet? Vennligst angi ditt brukernavn.</td></tr>" _
			   & "<tr><td><div class=""main_heading2"">Brukernavn: </td>" _
			   & "<td><INPUT TYPE=""text"" NAME=""txtUsername"" class=""news11"" MAXLENGTH=32></td></tr>" _
			   & "<td height=""60""></td>" _
			   & "<td><div class=""news11""><input type=""checkbox"" name=""chkEmail"" CHECKED>Send passord p� E-post<BR>" _
			   & "<tr><td colspan=""2"" align=""center"" height=""40""><input type=""hidden"" name=""realSubmit"" value=""sendpassword"">" _
			   & "<input type=""submit"" class=""formbutton"" name=""action"" id=L_Submit_Button value=""Send meg passordet"" style=""background:#bfccd9 none; color:#003366; width:150px""></td>" _
			   & "</tr>"_
			   & "<tr><td colspan=""2"" align=""center""><BR>" _
			   & "<div class=""news11"">Ved problemer ta kontakt med markedsavdelingen.<br/><br/>Telefon: 22 03 45 76. <br/>E-post: <a href='mailto:marked@ntb.no'>marked@ntb.no</a></div></td>" _
			   & "</tr></form></table>"
			   '& "<input type=""checkbox"" name=""chkSMS"" CHECKED>Send passord p� SMS</DIV></td></tr>" _

  RenderForgotPassword = htmContent

End Function

' -----------------------------------------------------------------------------
' RenderForgotPassword()
'
' Description: A function that returns HTML that show a input window for forgot password
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function RenderRegistrationForm()
  Dim htmContent

  htmContent = "<table border=""0"" cellpadding=""2"">" _
			   & "<FORM NAME=""frmLogin"" ACTION=""login.asp"" METHOD=""post""><tr>" _
			   & "<td colspan=""2"" height=""40""><div class=""news11"">NTBs nyhetsportal er tilgjengelig for bedrifter og organisasjoner som abonnerer p� NTBs nyheter. Ettersom registreringen skjer manuelt, vil henvendelser etter kontortid normalt ikke bli behandlet f�r dagen etter. <p>Vil du vite mer om v�re tjenester? Kontakt markedsavdelingen p� telefon 22 03 45 76 eller e-post marked@ntb.no. <p></td></tr>" _
			   & "<tr><td width=""35%"" align=right><div class=""main_heading2"">Navn: </td>" _
			   & "<td width=""65%""><INPUT TYPE=""text"" NAME=""txtName"" class=""news11"" MAXLENGTH=64 style=""width: 200px""></td></tr>" _
			   & "<tr><td align=right><div class=""main_heading2"">E-postadresse: </td>" _
			   & "<td><INPUT TYPE=""text"" NAME=""txtEmail"" class=""news11"" MAXLENGTH=64 style=""width: 200px""></td></tr>" _
			   & "<tr><td align=right><div class=""main_heading2"">Mobilnummer: </td>" _
			   & "<td><INPUT TYPE=""text"" NAME=""txtMobile"" class=""news11"" MAXLENGTH=64 style=""width: 200px""></td></tr>" _
			   & "<tr><td align=right><div class=""main_heading2"">Ansatt i: </td>" _
			   & "<td><INPUT TYPE=""text"" NAME=""txtCompany"" class=""news11"" MAXLENGTH=64 style=""width: 200px""></td></tr>" _

			   & "<tr><td colspan=""2"" align=""center"" height=""40""><input type=""hidden"" name=""realSubmit"" value=""registeruser"">" _
			   & "<input type=""submit"" class=""formbutton"" name=""action"" id=L_Submit_Button value=""Registrer meg som bruker"" style=""background:#bfccd9 none; color:#003366; width:250px""></td>" _
			   & "</tr>"_
			   & "<tr><td colspan=""2"" align=""center""><BR>" _
			   & "<div class=""news11"">Ved problemer ta kontakt med markedsavdelingen.<br/><br/>Telefon: 22 03 45 76. <br/>E-post: <a href='mailto:marked@ntb.no'>marked@ntb.no</a></div></td>" _
			   & "</tr></form></table>"

  RenderRegistrationForm = htmContent

End Function


' -----------------------------------------------------------------------------
' RenderSendMail()
'
' Description: A function that returns HTML that let the user send emails
'
' Parameters: CompanyID
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------
Function RenderSendMail(ByVal CompanyID)

  Dim htmContent
  Dim rsCompany, sCompanyName, strSubject, strContent

  if GetCompanyName(GetCompany(Session("Username"))) = "Norsk Telegrambyr�" Then
	Set rsCompany = RetrieveAllActiveCompany(0)
	htmContent = htmContent & "<TR><TD Width=""500"">Velg kunder � sende e-post til:</TD></TR>" _
				& "<TR><TD><select class=""news11"" id=""lstCompany"" name=""lstCompany"" size=10 multiple>"
	do until rsCompany.EOF
		htmContent = htmContent & "<option value='" & rsCompany.Fields("GeneralInfo.org_id") & "'>" & rsCompany.Fields("GeneralInfo.name") & "</OPTION>"
		rsCompany.MoveNext
	Loop
	htmContent = htmContent & "</select></TD></TR>"
	htmContent = htmContent & "<TR><TD><INPUT TYPE=checkbox NAME='chkAdminsOnly' value='1'>Send mail kun til administratorer</INPUT></TD></TR>"
	htmContent = htmContent & "<TR><TD>Tips: hold ctrl nede for � velge flere kunder samtidig.<BR><BR></TD></TR>"
  else
	htmContent = htmContent & "<input type=""hidden"" name=""lstCompany"" value='" & CompanyID & "'>"
  end if

  'all textboxes with content
  htmContent = htmContent & "<TR><TD Width=""120"">Overskrift: </TD></TR>" _
				& "<TR><TD align=""left""><input type=""text"" name=""txtSubject"" style=""HEIGHT: 16px; width: 500px;"" class=""news11""></TD></TR>" _
				& "<TR><TD>Melding: </TD></TR>" _
				& "<TR><TD align=""left""><textarea name=""txtContent"" style=""HEIGHT: 128px; width: 500px;"" class=""news11""></textarea></TD></TR>"

  'buttons and hidden fields
  htmContent = htmContent & "<input type=""hidden"" name=""sendMail"" value='sendMail'>" _
						& "<tr><td col span ""2"" align=""center"">"_
						& "<INPUT class='formbutton' type='button' value='Tilbake til listen' name='back' onClick='GetToList()' style='background:#bfccd9 none; color:#003366; width:100px'> " _
						& "<input class='formbutton' type='button' name=""btnsendMail"" onClick='CheckInput()' value='Send e-post' style='background:#bfccd9 none; color:#003366; width:100px'></td></tr>"

  RenderSendMail = htmContent

End Function

%>
