<SCRIPT Language=VBScript RUNAT=Server>
'option explicit
' NTB simple list cache routines
' Add extra cache manager for older articles, we cannot mix
' time intervals here
'
' Copyright (c) 2002 Andersen
'-----------------------------
' Modified by Ralfs Pladers 2002.04.21
' All simple lists and weekly items without sound now run
' with ntbcache.smartloader
' Old lines are still there if we need to revert to dumbloader
' for any reason - please don't delete commented lines.

' Modified by Roar Vestre 2002.06.03
'	Added Error handling for Sound news in procedure: getWeeklyItemsList
' Modified by Roar Vestre 2002.06.11
'	Added BugFix: new connection string from Global.asa: Application("cns0"): line 198
' Modified by Roar Vestre 2002.07.18:
'	Finished getKeyWordItemsList and deleted lot of old code.

Sub createAutoCache()

	Dim htmldict, htmlSoundDict, dictConfig, dictConfig72, listconfig, prmdictConfig, prmlistconfig, catconfig, CacheManager
	Dim df
	Set htmldict = Server.CreateObject("Commerce.Dictionary")
	Set htmlSoundDict = Server.CreateObject("Commerce.Dictionary")
	Set dictConfig = Server.CreateObject("Commerce.Dictionary")
	'Set dictConfig72 = Server.CreateObject("Commerce.Dictionary")
	Set listConfig = Server.CreateObject("Commerce.Dictionary")

	Set prmlistConfig = Server.CreateObject("Commerce.Dictionary")
	Set prmdictConfig = Server.CreateObject("Commerce.Dictionary")

'	Set sakListConfig = Server.CreateObject("Commerce.Dictionary")
'	Set sakDictConfig = Server.CreateObject("Commerce.Dictionary")

	'Set df = server.CreateObject("Commerce.DataFunctions")

	'-- HTML formatting options
	htmldict("dummy") = "dummyitem" ' at least one item must be there
	'htmldict("viewpage") = "show-the-full-article.asp?refid="
	'htmldict("anchor") = "class=x HREF=""x.asp?a=%s"" onclick=""w(%s);return false;""" ' or similar

	htmldict("anchor") = "class=x HREF=""x.asp?a=%s"" onclick=""w(%s);return false;""" ' or similar

	htmldict("timefont") = "<font class=d>"
	htmldict("urlfont") = "<font class=n>"
	htmldict("hotfont") = "<font class=in>"

	' Added by RoV 03.04.2002
	'-- HTML formatting options
	htmlSoundDict("dummy") = "dummyitem" ' at least one item must be there
	'htmldict("viewpage") = "show-the-full-article.asp?refid="
	htmlSoundDict("anchor") = "class=x HREF=""x.asp?a=%s"" onclick=""w(%s);return false;""" ' or similar
	htmlSoundDict("timefont") = "<font class=d>"
	htmlSoundDict("urlfont") = "<font class=n>"
	htmlSoundDict("hotfont") = "<font class=in>"

	' Number of columns in soundlist
	htmlSoundDict("soundcolumns") = 4
	' Format for soundlist
	htmlSoundDict("soundanchor") = "<" & "script language=""javascript"">var lyd%s = new FlashSound();" & "</" & "script>" & vbCrLf & _
				"<a href=""javascript://"" onmouseover=""lyd%s.TGotoAndPlay('/','start')"" onmouseout=""lyd%s.TGotoAndPlay('/','stop')"">" & vbCrLf & _
				"<img src=""../images/sound.gif"" border=""0"" align=""right"" /></a>" & vbCrLf & _
				"<" & "script>lyd%s.embedSWF(""http://194.19.39.29/kunde/ntb/flash/%f.swf"");" & "</" & "script>" & vbCrLf & _
				"</td><td> | <a href='http://194.19.39.29/kunde/ntb/mp3/%f.mp3' onmouseover=""return overlib('Klikk her for � laste ned og h�re det aktuelle lydopptaket i filformatet mp3.', LEFT, CAPTION, 'NTB Nyhetslyd i mp3-format') ;"" onmouseout=""return nd();""><font class='news'>MP3</font></a>" & vbCrLf & _
				"</td><td> | <a href=""sound_html.asp?id=%s&fname=%f"" onclick=""ws(%s, '%f');return false;"" onmouseover=""return overlib('Klikk her for � se HTML-koden for den aktuelle nyhetslyden. Lydillustrasjonen egner seg til bruk i nettaviser eller p� hjemmesider.', LEFT, CAPTION, 'HTML-kode for NTB Nyhetslyd') ;"" onmouseout=""return nd();""><font class='news'>HTML-kode</font></a>" & vbCrLf
				'"</td><td> | <a href='http://194.19.39.29/kunde/ntb/asx/%f.asx'><font class='news'>Win-media</font></a>" & vbCrLf & _

	dictConfig("cns") = Application("cns")
	dictConfig("qs") = "getAllLogger"
	dictConfig("dirtycheck") = "getLatestRefID"

	'dictConfig72("cns") = Application("cns")
	'dictConfig72("qs") = "getAllArticles72"
	'dictConfig72("dirtycheck") = "getLatestRefID"

	'----
	'
	' Simple list configuration must include a pair of listname
	' and ADODB.Recordset filter property parameter. Read ADO SDK
	' documentation for details on ADODB.Recordset filters.
	' HTML content will be created from the same data but filtered
	' for each list.

	' 7 days only PRM list
	'prmlistConfig("prm_ntbpluss") = "Maingroup = 32 AND Subgroup = 16"
	'prmlistConfig("prm_bwi") = "Maingroup = 32 AND Subgroup = 4096"
	'prmlistConfig("prm_nwa") = "Maingroup = 32 AND Subgroup = 2048"
	'prmlistConfig("priv_menyer") = "Subgroup = 8 AND Maingroup <> 16"
	'prmlistConfig("priv_tilred") = "Maingroup = 16 AND Subgroup <> 16"

	prmlistConfig("sounds") = "HasSounds > 0"

	' Kultur lists
	'prmlistConfig("kul_reportasjer") = "Maingroup = 8 AND Subgroup <> 512 AND Subgroup <> 8"
	'prmlistConfig("kul_notiser") = "Maingroup = 8 AND Subgroup = 512"
	'prmlistConfig("kul_menyer") = "Maingroup = 8 AND Subgroup = 8"

	'prmlistConfig("kul_reportasjer_foto") = "HasPictures > 0 AND Maingroup = 8 AND Subgroup <> 512 AND Subgroup <> 8"
	'prmlistConfig("kul_notiser_foto") = "HasPictures > 0 AND Maingroup = 8 AND Subgroup = 512"
	'prmlistConfig("kul_menyer_foto") = "HasPictures > 0 AND Maingroup = 8 AND Subgroup = 8"

	' Sakslister (Case lists) Max 3 Cases, only for Ut-Satellitt
	'prmlistConfig("sak_alle_1") = "ListNumber = 1 AND NtbFolderID = 1"
	'prmlistConfig("sak_alle_2") = "ListNumber = 2 AND NtbFolderID = 1"
	'prmlistConfig("sak_alle_3") = "ListNumber = 3 AND NtbFolderID = 1"

	' Saklister "Bakgrunn=128", "Analyse=64", "Faktaboks=256"
	'prmlistConfig("sak_bakgrunn_1") = "(ListNumber = 1 AND NtbFolderID = 1 AND Subgroup =64) OR (ListNumber = 1 AND NtbFolderID = 1 AND Subgroup =128) OR (ListNumber = 1 AND NtbFolderID = 1 AND Subgroup =256)"
	'prmlistConfig("sak_bakgrunn_2") = "(ListNumber = 2 AND NtbFolderID = 1 AND Subgroup =64) OR (ListNumber = 2 AND NtbFolderID = 2 AND Subgroup =128) OR (ListNumber = 2 AND NtbFolderID = 1 AND Subgroup =256)"
	'prmlistConfig("sak_bakgrunn_3") = "(ListNumber = 3 AND NtbFolderID = 1 AND Subgroup =64) OR (ListNumber = 3 AND NtbFolderID = 3 AND Subgroup =128) OR (ListNumber = 3 AND NtbFolderID = 1 AND Subgroup =256)"
	'prmlistConfig("sak_alle_1_foto") = "HasPictures > 0 AND Maingroup = 2"

	'prmlistConfig("utenriks_bakgrunn") = "(Maingroup = 2 AND Subgroup =64) OR (Maingroup = 2 AND Subgroup = 128) OR (Maingroup = 2 AND Subgroup = 65536) OR (Maingroup = 2 AND Subgroup = 4) OR (Maingroup = 2 AND Subgroup = 256) OR (Maingroup = 2 AND Subgroup = 8)"
	'prmlistConfig("utenriks_bakgrunn_foto") = "(HasPictures > 0 AND Maingroup = 2 AND Subgroup =64) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 65536) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 4) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 128) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 256) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 8)"

	prmdictConfig("cns") = Application("cns")
	prmdictConfig("qs") = "getWeeklyItems"
	prmdictConfig("dirtycheck") = "getLatestRefID"

	Set prmdictConfig("fm") = htmlSoundDict
	Set prmdictConfig("lists") = prmlistConfig
	' PRM ends here

	listConfig("logger") = "NtbFolderID = 16"
	listConfig("direkte") = "NtbFolderID = 2"
	'listConfig("innenriks_alle") = "Maingroup = 1" ' filter condition for ADODB.Recordset
	'listConfig("utenriks_alle") = "Maingroup = 2"
	'listConfig("sport_alle") = "Maingroup = 4"
	'listConfig("innenriks_okonomi") = "(Maingroup = 1 AND Categories = 65536) OR (Maingroup = 1 AND Categories = 1)"
	'listConfig("innenriks_politikk") = "Maingroup = 1 AND Categories = 256"
	'listConfig("innenriks_krim") = "(Maingroup = 1 AND Categories = 8) OR (Maingroup = 1 AND Categories = 4096)"
	''listConfig("utenriks_bakgrunn") = "(Maingroup = 2 AND Subgroup =64) OR (Maingroup = 2 AND Subgroup = 128) OR (Maingroup = 2 AND Subgroup = 65536) OR (Maingroup = 2 AND Subgroup = 4) OR (Maingroup = 2 AND Subgroup = 256)"
	'listConfig("utenriks_filtered") = "(Maingroup = 2 AND Subgroup <> 64) AND (Maingroup = 2 AND Subgroup <> 128) AND (Maingroup = 2 AND Subgroup <> 65536) AND (Maingroup = 2 AND Subgroup <> 4) AND (Maingroup = 2 AND Subgroup <> 256)"
	'listConfig("okonomi_innenriks") = "(Maingroup = 1 AND Categories = 65536)"
	'listConfig("okonomi_utenriks") = "(Maingroup = 2 AND Categories = 65536)"
	'listConfig("sport_filtered") = "(Maingroup = 4 AND Subgroup <> 2)"
	'listConfig("sport_tabeller") = "(Maingroup = 4 AND Subgroup = 2)"
	' -- same list with photo only option
	'listConfig("innenriks_alle_foto") = "Maingroup = 1 AND HasPictures > 0"
	'listConfig("utenriks_alle_foto") = "Maingroup = 2 AND HasPictures > 0"
	'listConfig("sport_alle_foto") = "Maingroup = 4 AND HasPictures > 0"
	'listConfig("innenriks_okonomi_foto") = "(HasPictures > 0 AND Maingroup = 1 AND Categories = 65536) OR (HasPictures > 0 AND Maingroup = 1 AND Categories = 1)"
	'listConfig("innenriks_politikk_foto") = "HasPictures > 0 AND Maingroup = 1 AND Categories = 256"
	'listConfig("innenriks_krim_foto") = "(HasPictures > 0 AND Maingroup = 1 AND Categories = 8) OR (HasPictures > 0 AND Maingroup = 1 AND Categories = 4096)"
	''listConfig("utenriks_bakgrunn_foto") = "(HasPictures > 0 AND Maingroup = 2 AND Subgroup =64) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 65536) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 4) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 128) OR (HasPictures > 0 AND Maingroup = 2 AND Subgroup = 256)"
	'listConfig("utenriks_filtered_foto") = "HasPictures > 0 AND Maingroup = 2 AND Subgroup <> 64  AND Subgroup <> 65536 AND Subgroup <> 4 AND Subgroup <> 128 AND Subgroup <> 256"
	'listConfig("okonomi_innenriks_foto") = "HasPictures > 0 AND Maingroup = 1 AND Categories = 65536"
	'listConfig("okonomi_utenriks_foto") = "HasPictures > 0 AND Maingroup = 2 AND Categories = 65536"
	'listConfig("sport_filtered_foto") = "HasPictures > 0 AND Maingroup = 4 AND Subgroup <> 2"
	'listConfig("sport_tabeller_foto") = "HasPictures > 0 AND Maingroup = 4 AND Subgroup = 2"

	Set dictConfig("fm") = htmldict
	Set dictConfig("lists") = listConfig

	'Set dictConfig72("fm") = htmldict
	'Set dictConfig72("lists") = listConfig

	Set CacheManager = Server.CreateObject("Commerce.CacheManager")

	CacheManager.CacheObjectProgId("prmsimplelists") = "Commerce.Dictionary"
	CacheManager.LoaderProgId("prmsimplelists") = "ntbcache.dumbloader"
	Set CacheManager.LoaderConfig("prmsimplelists") = prmdictConfig
	if Request.ServerVariables("SERVER_NAME") = "tibet" then
		CacheManager.RefreshInterval("prmsimplelists") = 1
		CacheManager.RetryInterval("prmsimplelists") = 10
	else 
		CacheManager.RefreshInterval("prmsimplelists") = 600
		CacheManager.RetryInterval("prmsimplelists") = 10
	end if

	CacheManager.CacheObjectProgId("simplelists24") = "Commerce.Dictionary"
	CacheManager.LoaderProgId("simplelists24") = "ntbcache.dumbloader"
	Set CacheManager.LoaderConfig("simplelists24") = dictConfig
	if Request.ServerVariables("SERVER_NAME") = "tibet" then
		CacheManager.RefreshInterval("simplelists24") = 1
		CacheManager.RetryInterval("simplelists24") = 10
	else 
		CacheManager.RefreshInterval("simplelists24") = 120
		CacheManager.RetryInterval("simplelists24") = 10

	end if

	'CacheManager.CacheObjectProgId("simplelists72") = "Commerce.Dictionary"
	'CacheManager.LoaderProgId("simplelists72") = "ntbcache.dumbloader"
	'Set CacheManager.LoaderConfig("simplelists72") = dictConfig72
	'CacheManager.RefreshInterval("simplelists72") = 120
	'CacheManager.RetryInterval("simplelists72") = 30

	Set catConfig = Server.CreateObject("Commerce.Dictionary")
	catConfig("cns") = Application("cns")
	catConfig("dirtycheck") = "getLatestNameID"
	catConfig("qs_maingroups") = "getNITF_Types 0"
	catConfig("qs_subgroups") = "getNITF_Types 1"
	catConfig("qs_geo_global") = "getNITF_Types 5"
	catConfig("qs_geo_local") = "getLocalGeo 256"
	catConfig("qs_cat") = "getNITF_Types 3"
	catConfig("qs_subcat") = "getSubCats"

	CacheManager.CacheObjectProgId("categories") = "Commerce.Dictionary"
	CacheManager.LoaderProgId("categories") = "ntbcache.categories"
	Set CacheManager.LoaderConfig("categories") = catConfig
	CacheManager.RefreshInterval("categories") = 86400
	CacheManager.RetryInterval("categories") = 300

'--------------------------
' 2002-04-16 added by Ralfs
'
' Memory data cache	for smartloader, search and custom pages
'
'	Dim oMemDb
'	Set oMemDb = Server.CreateObject("memdb.portaldata")
	oMemDb.ConnectionString = Application("cns0") ' = "data source=DEVSERVER1;initial catalog=ntb;password=password;user id=sa;workstation id=DEVSERVER1;packet size=8912"
	oMemDb.ArticleCommand = "getArticleRange"
    oMemDb.SubrelCommand = "getSubrelRange"
    oMemDb.RangeCommand = "getRangeBounds"
    Call oMemDb.LoadCache()
'	Set Application("MemoryDatastore") = oMemDb
'
' Smartloader will need this object as parameter
' Dictionary for smartloader components
' Parameter "dataage" should be 0 for 24h, 1 for previous 48h,
' anything else for 7 days(the whole cache).
' Parameter "photofilter" should be used for simple filter to
' select messages for "xxx_photo" lists.
'
	Dim oSmartDict24, oSmartDict72, oSmartDictWeekly, oSmartLists, oWeeklySmartLists
	Set oSmartDict24 = Server.CreateObject("Commerce.Dictionary")
	Set oSmartDict72 = Server.CreateObject("Commerce.Dictionary")
	Set oSmartDictWeekly = Server.CreateObject("Commerce.Dictionary")
	Set oSmartLists = Server.CreateObject("Commerce.Dictionary")
	Set oWeeklySmartLists = Server.CreateObject("Commerce.Dictionary")

	oSmartLists("alle_nyheter_photo") = "Maingroup = 1 OR Maingroup = 2 OR Maingroup = 4 OR Maingroup = 16"
	oSmartLists("innenriks_alle_photo") = "Maingroup = 1"
	oSmartLists("utenriks_alle_photo") = "Maingroup = 2"
	oSmartLists("sport_alle_photo") = "Maingroup = 4"
	oSmartLists("innenriks_okonomi_photo") = "Maingroup = 1 AND Categories & 65537 > 0"
	oSmartLists("innenriks_politikk_photo") = "Maingroup = 1 AND Categories & 256 > 0"
	oSmartLists("innenriks_krim_photo") = "Maingroup = 1 AND Categories & 4104 > 0"
	oSmartLists("utenriks_filtered_photo") = "Maingroup = 2 AND Subgroup <> 64  AND Subgroup <> 65536 AND Subgroup <> 4 AND Subgroup <> 128 AND Subgroup <> 256 AND Subgroup <> 8"
	'comment out this list since it should be equal to innenriks_okonomi_photo
	'oSmartLists("okonomi_innenriks_photo") = "(Maingroup = 1 AND Categories & 65536 > 0)"
	oSmartLists("okonomi_utenriks_photo") = "Maingroup = 2 AND Categories & 65537 > 0"
	oSmartLists("sport_filtered_photo") = "Maingroup = 4 AND Subgroup <> 2"
	oSmartLists("sport_tabeller_photo") = "Maingroup = 4 AND Subgroup = 2"

	Set oSmartDict24("memdb") = oMemDb ' IMPORTANT: must pass object
	Set oSmartDict24("lists") = oSmartLists
	Set oSmartDict24("fm") = htmldict
	oSmartDict24("dirtycheck") = "yes"
	oSmartDict24("dataage") = 0
	' two lines below actually are not needed - these are defaults
	oSmartDict24("photofilter") = "HasPictures > 0"
	oSmartDict24("sortstring") = "CreationDateTime DESC"

	CacheManager.CacheObjectProgId("smartlists24") = "Commerce.Dictionary"
	CacheManager.LoaderProgId("smartlists24") = "ntbcache.smartloader"
	Set CacheManager.LoaderConfig("smartlists24") = oSmartDict24
	CacheManager.RefreshInterval("smartlists24") = 60
	CacheManager.RetryInterval("smartlists24") = 10

	Set oSmartDict72("memdb") = oMemDb
	Set oSmartDict72("lists") = oSmartLists
	Set oSmartDict72("fm") = htmldict
	oSmartDict72("dirtycheck") = "yes"
	oSmartDict72("dataage") = 1
	oSmartDict72("photofilter") = "HasPictures > 0"
	oSmartDict72("sortstring") = "CreationDateTime DESC"

	CacheManager.CacheObjectProgId("smartlists72") = "Commerce.Dictionary"
	CacheManager.LoaderProgId("smartlists72") = "ntbcache.smartloader"
	Set CacheManager.LoaderConfig("smartlists72") = oSmartDict72
	CacheManager.RefreshInterval("smartlists72") = 180
	CacheManager.RetryInterval("smartlists72") = 10

	' 7 days only PRM list
	oWeeklySmartLists("prm_ntbpluss") = "(Maingroup = 32 AND Subgroup = 16) OR ( Maingroup = 8192 )"
	oWeeklySmartLists("prm_bwi") = "(Maingroup = 32 AND Subgroup = 4096) OR ( Maingroup = 16384 )"
	oWeeklySmartLists("prm_nwa") = "(Maingroup = 32 AND Subgroup = 2048) OR ( Maingroup = 32768 )"
	oWeeklySmartLists("prm_obi") = "(Maingroup = 32 AND Subgroup = 262144) OR ( Maingroup = 16777216 )"

	oWeeklySmartLists("priv_menyer") = "Subgroup = 8 AND Maingroup <> 16"
	oWeeklySmartLists("priv_tilred") = "Maingroup = 16 AND Subgroup <> 16"


	oWeeklySmartLists("kul_alle") = "(Maingroup = 8 AND Subgroup <> 8) OR (Categories & 16 > 0 AND Subgroup <> 8)"

	oWeeklySmartLists("kul_reportasjer_photo") = "(Maingroup = 8 AND Subgroup <> 512 AND Subgroup <> 8) OR (Categories & 16 > 0 AND Subgroup <> 512 AND Subgroup <> 8)"
	oWeeklySmartLists("kul_notiser_photo") = "Maingroup = 8 AND Subgroup = 512 AND Subcategories & 63"
	oWeeklySmartLists("kul_showbiz_photo") = "Maingroup = 8 AND Subgroup = 512 AND Subcategories & 64"
	oWeeklySmartLists("kul_menyer_photo") = "Maingroup = 8 AND Subgroup = 8"
'	oWeeklySmartLists("utenriks_bakgrunn_photo") = "(Maingroup = 2 AND Subgroup =64) OR (Maingroup = 2 AND Subgroup = 65536) OR (Maingroup = 2 AND Subgroup = 4) OR (Maingroup = 2 AND Subgroup = 128) OR (Maingroup = 2 AND Subgroup = 256) OR (Maingroup = 2 AND Subgroup = 8)"
	oWeeklySmartLists("utenriks_bakgrunn_photo") = "Maingroup = 2 AND Subgroup & 65996 > 0"


	Set oSmartDictWeekly("memdb") = oMemDb ' IMPORTANT: must pass object
	Set oSmartDictWeekly("lists") = oWeeklySmartLists
	Set oSmartDictWeekly("fm") = htmldict
	oSmartDictWeekly("dirtycheck") = "yes"
	oSmartDictWeekly("dataage") = 7
	' two lines below actually are not needed - these are defaults
	oSmartDictWeekly("photofilter") = "HasPictures > 0"
	oSmartDictWeekly("sortstring") = "CreationDateTime DESC"

	CacheManager.CacheObjectProgId("smartlistsweekly") = "Commerce.Dictionary"
	CacheManager.LoaderProgId("smartlistsweekly") = "ntbcache.smartloader"
	Set CacheManager.LoaderConfig("smartlistsweekly") = oSmartDictWeekly
	CacheManager.RefreshInterval("smartlistsweekly") = 300
	CacheManager.RetryInterval("smartlistsweekly") = 10

	Set Application("ListCache") = CacheManager

End Sub

'----------------------
' Simple list retrieval function will return HTML.
' List with this name must exist in the cache.
Function getLoggerList(listname, photo, age)
	dim lname, cachename
	lname = listname
	getLoggerList = "<table cellspacing='0' cellpadding='0'>" & Application("ListCache").getCache("simplelists24").value(lname) & "</table>"
End Function

'----------------------
' Simple list retrieval function will return HTML.
' List with this name must exist in the cache.
Function getArticleList(listname, photo, age)
	dim lname, cachename
	lname = listname
	if photo="Yes" then lname = lname & "_foto"
	if age="48" then cachename = "simplelists72" else cachename="simplelists24"
	getArticleList = Application("ListCache").getCache(cachename).value(lname)
End Function

'=====================
' Smart list retrieval function will return HTML.
' List with this name must exist in the cache.
Function getArticleList_v2(listname, photo, age)
	dim lname, cachename
	lname = listname
	if photo="Yes" then lname = lname & "_photo"
	if age="48" then cachename = "smartlists72" else cachename="smartlists24"
	getArticleList_v2 = Application("ListCache").getCache(cachename).value(lname)
End Function

Function getWeeklyItemsList(listname, photo)
	dim lname, strList
	lname = listname
	if photo="Yes" then lname = lname & "_foto"
	'On Error Resume Next
	getWeeklyItemsList = Application("ListCache").getCache("prmsimplelists").value(lname)
	'if Err.Number > 0 then
	'	getWeeklyItemsList = "<div class=news>Ingen lydnyheter denne uken</div>"
	'	Err.Clear
	'end if
End Function

Function getWeeklyItemsList_v2(listname, photo)
	dim lname
	lname = listname
	if photo="Yes" then lname = lname & "_photo"
	getWeeklyItemsList_v2 = Application("ListCache").getCache("smartlistsweekly").value(lname)
End Function

' Added for Sak (by KeyWordID) by Roar Vestre, NTB 2002.05.31
Function getKeyWordItemsList(listname, photo, intListNumber)

	if photo="Yes" then lname = lname & "_foto"
	dim sSearch, strFields, strSakGruppe
	Dim al, list
	Dim a_csn, intHits, intDays, intRelevance

	sSearch = Application("textSak" & intListNumber)
	strSakGruppe = Application("strSakGruppe" & intListNumber)
	If strSakGruppe = "Alle" Or strSakGruppe = "" then
		strSakGruppe = ""
	Else
		'strSakGruppe = "fnameMaingroup=OR(" & strSakGruppe & ")"
		'Ny Bitwise AND s�k:
		strSakGruppe = "fnameMaingroup=BITAND(" & strSakGruppe & ")"
	End if

	if sSearch <> "" then
	'if a searchstring is present we ask Autonomy for help
		'use the autonomy COM-object to get list over searched item
		if listname = "sak_bakgrunn" then
			'strFields = "fnameSubgroup=OR(Bakgrunn Faktaboks Analyse)"
			strFields = "fnameSubgroup=BITAND(5C0)"
			If strSakGruppe <> "" then
				'strFields = "fnameSubgroup=OR(Bakgrunn Faktaboks Analyse)&" & strSakGruppe
				strFields = strFields & "&" & strSakGruppe
			End If
			a_csn = application("autonomy_all_cns")
			Set al = Server.CreateObject("AutonomyInterface.comAutonomy")
			intRelevance = Application("intSak2Rel" & intListNumber)
			intDays = Application("intSak2Days" & intListNumber)
			intHits = 100

		else
			strFields = strSakGruppe
			a_csn = application("autonomy_all_cns")
			Set al = Server.CreateObject("AutonomyInterface.comAutonomy")

			intRelevance = Application("intSakRel" & intListNumber)
			intDays = Application("intSakDays" & intListNumber)
			intHits = 300

		End If

		'response.write (strFields & ":" & sSearch & ":" & a_csn)
		list = al.getSearchGroupsResultList(sSearch, a_csn, "NTB", intRelevance, "x.asp", intDays, intHits, strFields)

		if list <> "" then
			Response.Write list
		else
			Response.Write ("<br><br><div class='news11'>Dessverre ingen treff.</div>")
		end if
	end if
End Function

Function getKeyWordItemsList_old(listname, photo, intListNumber)
	'dim lname
	'
	'lname = listname & "_" & intListNumber
	'
	'if photo="Yes" then lname = lname & "_foto"
	'getKeyWordItemsList = Application("ListCache").getCache("prmsimplelists").value(lname)
End Function

' Function to check access rights for this article
Function checkAccess(aid)
' get ACF from Session, must be set at user logon, taken from Organization profile
' call oMemDb.CheckArticleAccessRights(aid, acf)
' if returns 0 then show article, if return > 0 don't show, if returns -1
' then go to NTB_Archive database and call checkAccessRights from here.
' Will return one record with one field called "Result", meaning is the same.
'
' Be careful and make some modifications to timings first - currently
' it is possible that article is not in the cache and not yet in archive,
' but still in NTB database. If necessary, modify stored procedure to
' look in NTB as well.

	Dim intACF, intMem

	'get session-variable and freearticlebit
	intACF = Session("ACF") + Application("freearticlebit")

	if intACF <> "" then
		intMem = oMemDb.CheckArticleAccessRights(aid, intACF)

		if intMem = 0 then
			checkAccess = True ' :-))

		elseif intMem > 0 then
			checkAccess = False ' :-((

		elseif intMem = -1 then
		'check the accessrights in the database
			'Open connection
			Dim connection
			Set connection = Server.CreateObject("ADODB.Connection")
			connection.ConnectionString = Application("archive_cns")
			connection.Open
			connection.CursorLocation = adUseClient

			'Setup Recordset
			Dim rsDatabaseSearch
			Set rsDatabaseSearch = Server.CreateObject("ADODB.RecordSet")

			'Create Stored procedure commend
			Dim cmd
			Set cmd = Server.CreateObject("ADODB.Command")
			cmd.ActiveConnection = connection
			cmd.CommandType = adCmdStoredProc
			cmd.CommandText = "CheckAccessRights"

			'Set parameters
			Dim par
			Set par = Server.CreateObject("ADODB.Parameter")
			Set par = cmd.CreateParameter("@aid", adInteger, adParamInput, , aid)
			cmd.Parameters.Append par

			Set par = cmd.CreateParameter("@acf", adInteger, adParamInput, , intACF)
			cmd.Parameters.Append par

			rsDatabaseSearch.Open cmd
			rsDatabaseSearch.MoveFirst

			if not rsDatabaseSearch.EOF then
				intMem = rsDatabaseSearch.Fields("Result")

				if intMem = 0 then
					checkAccess = True ' :-))
				elseif intMem > 0 then
					checkAccess = False ' :-((
				else
					checkAccess = False ' :-((
				end if
			end if

		else
			checkAccess = False ' :-((
		end if

	else
	'empty ACF - then no access
		'checkAccess = False ' :-((
		'change to false when we want this to work on prod.server
		checkAccess = true
	end if

End Function

' Function to retrieve the article text by RefID
Function getArticleText(aid)
	if not checkAccess(aid) Then
		getArticleText = "NOT"
		Exit Function
	End if

	'Log statistics entry for page viewed
	Dim idProf
	idProf = Session("UID")

	call objUsageEntry.AddEntry(idProf, aid, 1) ' 1 is the statstypeid for open article

	Dim artCache, artpage
	Set artCache = Application("PageCache")
	artpage = artCache.Lookup(CStr(aid))
	if artpage <> "" then
		'getArticleText = "CACHED<br>" & artpage
		getArticleText = artpage
		Exit Function
	End if

	Dim xml, xsl, tempstr
	'Server.CreateObject("Microsoft.XMLDOM")
	tempstr = getArticleXML(aid)
	if tempstr = "E_A_NOT_FOUND" OR tempstr = "E_A_NO_ACCESS" then
		getArticleText = tempstr
		Exit Function
	end if

	Set xml = Server.CreateObject("MSXML2.FreeThreadedDOMDocument")
	xml.async = False
	Set xsl = Application("ArticleXSL")
	xml.loadXML tempstr

	Dim pictCache
	Set pictCache = Application("pictCache")
	pictCache.Insert CStr(aid), xml.selectNodes("/nitf/body/body.content/media[@media-type='image' and media-reference/@source != '']")

	'Build List of articles
	Dim verCache
	Set verCache = Application("verCache")

	Dim tmp
	Dim rs
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open "getVersions '" & xml.selectSingleNode("/nitf/head/meta[@name = 'NTBID' or @name = 'ntb-id' or @name = 'ntb-dato']/@content").Value & "', " & aid, Application("cns")

	'if rs.EOF then
	'	tmp = "<font class=n>Ingen flere versjoner funnet.</font>"
	'else
		while not rs.EOF
			tmp = tmp & "<font class=d>" & FormatDate(rs("CreationDateTime")) & "&nbsp;&nbsp;&nbsp;&nbsp;</font><a class=x href='../template/x.asp?a=" & rs("RefID") & "'onclick='w(" & rs("RefID") & ");return false;' ><font class=n>" & rs("ArticleTitle") & "</font></a><br>" & vbcrlf
			rs.MoveNext
		wend
	'end if

	rs.Close
	Set rs =  Nothing

	verCache.Insert CStr(aid), tmp

	artCache.Insert CStr(aid), xml.transformNode(xsl)
	getArticleText = artCache.Lookup(CStr(aid))
	'getArticleText = xsl.text

End Function

' Function to retrieve all versions of an article by RefID
Function getArticleVersions(aid)

	Dim verCache
	Set verCache = Application("verCache")

	getArticleVersions = verCache.Lookup(CStr(aid))

End Function


' Function to retrieve the article PicturesNodes by RefID
Function getArticlePict(aid)

	Dim pictCache, pictNodes
	Set pictCache = Application("pictCache")
	'Set pictNodes = Server.CreateObject("MSXML2.IXMLDOMNodeList")

' if Set... then must return object not variant

	Set getArticlePict = pictCache.LookupObject(CStr(aid))
	'if pictNodes.length > 0 then
	'else
	'	Set getArticlePict = Nothing
	'End if

End Function

' Function to retrieve the full XML by RefID
Function getArticleXML(aid)
' First we check access rights
	If Not checkAccess(aid) then
		getArticleXML = "E_A_NO_ACCESS"
	Else
		Dim rs
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open "getXML " & CStr(aid), Application("cns")
		if not rs.EOF then
			getArticleXML = rs.Fields(0)
		else
			getArticleXML = "E_A_NOT_FOUND"
		end if
		rs.Close
		Set rs =  Nothing
	End if
End Function

'----------------------
'Category retrieval functions will return a reference to
'the Commerce.Dictionary object filled with data

Function getMainGroups()
	Set getMainGroups = Application("ListCache").getCache("categories").value("maingroups")
End Function

Function getSubGroups()
	Set getSubGroups = Application("ListCache").getCache("categories").value("subgroups")
End Function

Function getWorldRegions()
	Set getWorldRegions = Application("ListCache").getCache("categories").value("geo_global")
End Function

Function getSortedWorldRegions()
	Dim c
	Set c = Application("ListCache").getCache("categories").value("sorted")
	Set getSortedWorldRegions = c("geo_global")
End Function

Function getLocalRegions() ' Currently only Norway is described
	Set getLocalRegions = Application("ListCache").getCache("categories").value("geo_local")
End Function

Function getSortedLocalRegions()
	Dim c
	Set c = Application("ListCache").getCache("categories").value("sorted")
	Set getSortedLocalRegions = c("geo_local")
End Function

Function getCategories()
	Set getCategories = Application("ListCache").getCache("categories").value("cats")
End Function

Function getSortedCategories()
	Dim c
	Set c = Application("ListCache").getCache("categories").value("sorted")
	Set getSortedCategories = c("cats")
End Function

Function getSubcategories(s) ' pass category number here, NOT name
	dim c
	Set c = Application("ListCache").getCache("categories").value("subcats")
	'Response.Write("Requested " & s & "<hr>")
	Set getSubcategories = c(s)
End function

' -----------------------------------------------------------------------------
' FormatDate
'
' Description:	This function formats the date to norwegian format
'				The format is DD.MM.YYYY hh:mm
'
' Parameters:	inDate - date to be formated
'
' Returns:		String with the in date in correct format
'
' Notes :		None
' -----------------------------------------------------------------------------
Function FormatDate(inDate)
	Dim tmpDate, tmpDay, tmpMonth
	tmpDate = CDate(inDate)
	tmpDay = DatePart("D", tmpDate)
	if CInt(tmpDay) < 10 then
	'add zero as prefix to days 1-9
		tmpDay = "0" & tmpDay
	end if
	tmpMonth = DatePart("M", tmpDate)
	if CInt(tmpMonth) < 10 then
	'add zero as prefix to months 1-9
		tmpMonth = "0" & tmpMonth
	end if
	FormatDate = tmpDay & "." & tmpMonth & "." & DatePart("YYYY", tmpDate) & "&#160;" & FormatDateTime(tmpDate,4)
End Function

</SCRIPT>
