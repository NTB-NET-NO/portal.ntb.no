<%  
  '***********************************************************************************
' NTB_stats_render_lib.asp
'
' Support functions for rendering the statistic pages
' 
' NOTES:
'		'none'
'
' CREATED BY: �yvind Andersen 
' CREATED DATE: 2004.01.20
' UPDATED BY: 
' UPDATED DATE: 
' REVISION HISTORY:
' Last update: 
'
'***********************************************************************************
' -----------------------------------------------------------------------------
' RenderViewStat()
'
' Description: A function that returns HTML that let the select criteria for
' statistics
'
' Parameters: 
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------
Function RenderViewStat()

	Dim htmContent
	Dim rsCompany, rsCompanyGroup
	Dim iCompanyCount

	Set rsCompany = RetrieveAllActiveCompany(iCompanyCount)
	Set rsCompanyGroup = GetAllCompanyGroup()

	htmContent = "<DIV class='adminBlueLink'><A class='admin' HREF=""User_homepage.asp"">Til portalen</A>&nbsp;&nbsp;&nbsp;<A class='admin' HREF=""HelpReports.asp"">Forklaring til rapportene</A></DIV>" 
	htmContent = htmContent & "<TABLE border='0' cellpadding='1' cellspacing='1' class='news'>" _
				& "<FORM name='frmCriteria' action='ViewStats.asp' method='post'>" _
				& "<TR><THEAD>" _
				& "<TH align='left'>Velg kunde(r)</TH>"_
				& "<TH align='left'>Velg kriterie</TH>"_
				& "<TH align='left'>Velg rapport</TH>"_
				& "</THEAD>"

	' A list with all companies
	htmContent = htmContent & "<TBODY>" _
				& "<TR><TD valign='top'>Kundegruppe:<BR>"_
				& "<SELECT class=""news11"" id=""lstCompanyGroup"" name=""lstCompanyGroup"" onchange='return SelectCompany()' size=8 multiple style='width=160 px'><BR>"
				
	' Fill option with CompanyGroup
	do until rsCompanyGroup.EOF
		htmContent = htmContent & "<option value='" & rsCompanyGroup.Fields("GroupId") & "[" & rsCompanyGroup.Fields("CompanyId") & "]" & "'>" & rsCompanyGroup.Fields("Name") & "</OPTION>"
		rsCompanyGroup.MoveNext
	loop

	htmContent = htmContent & "</SELECT><BR>" _
				& "<INPUT class='formbutton' type='button' name='btnAddCustomerGroup' onclick='return AddGroup()' value='Legg til' style='background:#bfccd9; width: 78px; none; color:#003366'> " _
				& "<INPUT class='formbutton' type='button' name='btnDeleteCustomerGroup' onclick='return DeleteGroup()' value='Slett' style='background:#bfccd9; width: 78px; none; color:#003366'>" _
				& "<INPUT class='text' type='hidden' name='txtGroupName' value=''>" _
				& "<INPUT class='text' type='hidden' name='txtAction' value=''><BR><BR>" 
				
	htmContent = htmContent & "Kundeliste:<BR>" _
				& "<SELECT class=""news11"" id=""lstCompany"" name=""lstCompany"" size=17 multiple  style='width=160 px'>"
	do until rsCompany.EOF
		htmContent = htmContent & "<option value='" & rsCompany.Fields("GeneralInfo.org_id") & "'>" & rsCompany.Fields("GeneralInfo.name") & "</OPTION>"
		rsCompany.MoveNext
	Loop
	htmContent = htmContent & "</SELECT><BR>" _
				& "<INPUT class='formbutton' type='button' name='btnSelectAll' onclick='return ChangeSelect(true, lstCompany)' value='Velg alle' style='background:#bfccd9; width: 78px; none; color:#003366'> " _
				& "<INPUT class='formbutton' type='button' name='btnUnSelectAll' onclick='return ChangeSelect(false, lstCompany)' value='Fjern valg' style='background:#bfccd9; width: 78px; none; color:#003366'>" _
				& "</TD>" _
				& "</FORM><FORM name='frmStats' onSubmit='return CheckInput()' action='StatsReport.asp' method='post'>" _
				& "<TD valign='top' align='left'>"
	' Setup different date criterias
	htmContent = htmContent & "<INPUT TYPE=radio NAME='DateSelect' value='Today' CHECKED>I dag (" & Date() & ")</INPUT><BR>" _
				& "<INPUT TYPE=radio NAME='DateSelect' value='Yesterday'>I g�r (" & Date()-1 & ")</INPUT><BR>" _
				& "<INPUT TYPE=radio NAME='DateSelect' value='ThisMonth'>S� langt i " & MonthName(Month(Date())) & "</INPUT><BR>" _
				& "<INPUT TYPE=radio NAME='DateSelect' value='ThisYear'>S� langt i �r</INPUT><BR>" _
				& "<INPUT TYPE=radio NAME='DateSelect' value='LastWeek'>Siste uke (" & GetLastWeekDates() & ")</INPUT><BR>" _
				& "<INPUT TYPE=radio NAME='DateSelect' value='LastMonth'>Siste m�ned (" & GetLastMonthName() & ")</INPUT><BR>" _
				& "<INPUT TYPE=radio NAME='DateSelect' value='SpecifyDates'>Velg dato:<BR>" _
					& "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fra: <INPUT type='text' name='txtFromDate' style='HEIGHT: 18px; width: 80px;' class='news11' value='" & Date-7 & "'></INPUT>&nbsp;&nbsp;" _
					& "Til: <INPUT type='text' name='txtToDate' style='HEIGHT: 18px; width: 80px;' class='news11' value='" & Date() & "'></INPUT></INPUT>" _
				& "</SELECT><BR><BR>"

	' Setup checkboxes below dates for various criterias
	htmContent = htmContent & "<INPUT TYPE=checkbox NAME='chkArchive' value='1'>Arkiv</INPUT><BR>" _
					& "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE=radio NAME='ArchiveSelect' value='Eldre' CHECKED>Eldre enn</INPUT><BR>" _
					& "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE=radio NAME='ArchiveSelect' value='Nyere'>Nyere enn</INPUT><BR>" _
					& "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Angi antall dager: <INPUT type='text' name='txtArchive' style='HEIGHT: 18px; width: 20px;' class='news11' value='7'></INPUT> dager<BR><BR>" 

	htmContent = htmContent & "<INPUT TYPE=checkbox NAME='chkRatingHigher' value='1'>Rating h�yere enn</INPUT>&nbsp;<INPUT type='text' name='txtRatingHigher' style='HEIGHT: 18px; width: 20px;' class='news11' value='10'></INPUT><BR>" _
					& "<INPUT TYPE=checkbox NAME='chkRatingLower' value='1'>Rating lavere enn</INPUT>&nbsp;<INPUT type='text' name='txtRatingLower' style='HEIGHT: 18px; width: 20px;' class='news11' value='10'></INPUT><BR><BR>" _
					
	' Maingroup select box
	htmContent = htmContent & "Mediagruppe:<BR>"
	htmContent = htmContent & "<SELECT class=""news11"" id=""lstMainGroup"" name=""lstMainGroup"" size=6 multiple style='width=160 px'>"
	htmContent = htmContent & "<OPTION value='1'>Innenriks</OPTION>"
	htmContent = htmContent & "<OPTION value='2'>Utenriks</OPTION>"
	htmContent = htmContent & "<OPTION value='4'>Sport</OPTION>"
	htmContent = htmContent & "<OPTION value='8'>Kultur & Underholdning</OPTION>"
	htmContent = htmContent & "<OPTION value='16'>Privat til redaksjonen</OPTION>"
	htmContent = htmContent & "<OPTION value='32'>Pressemeldinger</OPTION>"
	htmContent = htmContent & "</SELECT><BR>"
	htmContent = htmContent & "<INPUT class='formbutton' type='button' name='btnSelectAll' onclick='return ChangeSelect(true, lstMainGroup)' value='Velg alle' style='background:#bfccd9; width: 78px; none; color:#003366'> " _
				& "<INPUT class='formbutton' type='button' name='btnUnSelectAll' onclick='return ChangeSelect(false, lstMainGroup)' value='Fjern valg' style='background:#bfccd9; width: 78px; none; color:#003366'>" _
				& "</TD>" 

	htmContent = htmContent & "<TD valign='top'>" _
				& "<INPUT class='text' type='hidden' name='txtCompany' value=''>" _
				& "<INPUT class='formbutton' type='submit' name='btnReport1' value='Artikkel' style='background:#bfccd9; width: 120px; none; color:#003366'><BR><BR>" _
				& "<INPUT class='formbutton' type='submit' name='btnReport2' value='Rating' style='background:#bfccd9; width: 120px; none; color:#003366'><BR><BR>" _
				& "<INPUT class='formbutton' type='submit' name='btnReport3' value='P�logginger' style='background:#bfccd9; width: 120px; none; color:#003366'>" _
				& "</TD>"

	htmContent = htmContent & "</TR>" _
				& "<TR><TD colspan=3>Tips: hold ctrl nede for � velge flere kunder samtidig.</TD></TR>"
	
	htmContent = htmContent & "</TBODY></FORM></TABLE>"

	RenderViewStat = htmContent

End Function

' -----------------------------------------------------------------------------
' RenderStatsReport()
'
' Description: A function that returns HTML for a given report
'
' Parameters: 
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------
Function RenderStatsReport(strCompany, strReport, strFromDate, strToDate, strArchive, strOlderThan, strMainGroup, strRatingHigher, strRatingLower)

	Dim htmContent, lngTimeout
	
	lngTimeout = Server.ScriptTimeout 
	Server.ScriptTimeout = Application("ReportServerTimeout")
	
	htmContent = "<DIV class='adminBlueLink'><A class='admin' HREF=""User_homepage.asp"">Til portalen</A>&nbsp;&nbsp;&nbsp;<A class='admin' HREF=""HelpReports.asp"">Forklaring til rapportene</A></DIV>" 
	
	select case CLng(strReport)
		case 1: htmContent = htmContent & ReportUsagePrCompany(strCompany, strFromDate, strToDate, strArchive, strOlderThan, strMainGroup)
		case 2: htmContent = htmContent & ReportRating(strCompany, strFromDate, strToDate, strArchive, strOlderThan, strMainGroup, strRatingHigher, strRatingLower)
		case 3: htmContent = htmContent & ReportLogin(strCompany, strFromDate, strToDate)
	end select 

	' Add buttons
	htmContent = htmContent & "<INPUT class='formbutton' type='button' value='Nytt s�k' name='back' onClick='document.location=""viewStats.asp""' style='background:#bfccd9 none; color:#003366'> "
	RenderStatsReport = htmContent ' & "<p>" & lngTimeout & " - " & Server.ScriptTimeout
	
	Server.ScriptTimeout = lngTimeout

End Function

Function ReportUsagePrCompany(strCompany, strFromDate, strToDate, strArchive, strOlderThan, strMainGroup)

	Dim htmContent, rsStats, rsUser, strUser, aCompany, i, j, sSqlStmt, strNTBID
	Dim iUserCount, iArticleCount, iTotalUserCount, iTotalArticleCount, iTotalRowCount
	Dim aMainGroup(), aTotalMainGroup, strUserId
	Redim aMainGroup(9)
	Redim aTotalMainGroup(9)

	iTotalUserCount = 0
	iTotalArticleCount = 0
	iTotalRowCount = 0
	for j = 0 to 8
		aTotalMainGroup(j) = 0
	next
	
	aCompany = Split(strCompany, ", ")
	
	htmContent = htmContent & "<TABLE border=0 id='statReport' class='news'>" _
				& "<THEAD><TH align='left' width=200px>Kunde</TH>" _
				& "<TH align='right' width=120px>Ant unike lesere</TH>" _
				& "<TH align='right' width=75px>Ant artikler</TH>" _
				& "<TH align='right' width=65px>Innenriks</TH>" _
				& "<TH align='right' width=60px>Utenriks</TH>" _
				& "<TH align='right' width=40px>Sport</TH>" _
				& "<TH align='right' width=40px>K&U</TH>" _
				& "<TH align='right' width=40px>Priv</TH>" _
				& "<TH align='right' width=45px>Prm</TH>" _
				& "<TBODY>"
	' Loop through all companies
	for i = lbound(aCompany) to ubound(aCompany)
		Set rsUser = RetrieveCompanyUsers(aCompany(i), 0)
		strUser = ""
		' Create a string of all the users to include in the sql
		do until rsUser.EOF
			if len(strUser) > 0 then 
				strUser = strUser & ", "
			end if
			strUser = strUser & "'" & rsUser.Fields("GeneralInfo.user_id") & "'"
			rsUser.MoveNext
		loop
	
		' Get data if we have users
		if len(strUser) > 0 then
			Set rsStats = Server.CreateObject("ADODB.Recordset")
			sSqlStmt = "SELECT count(distinct Articles.refid) AS NumberOfArticles, MainGroup, userid FROM ntb_archive.dbo.ARTICLES AS Articles, ntb.dbo.STATS AS Stats " _
					& "WHERE Stats.ArticleID = Articles.RefID " _
					& "AND Stats.RecordDate >= CONVERT(DATETIME, '" & CDate(strFromDate) & "', 104) AND Stats.RecordDate < CONVERT(DATETIME, '" & CDate(strToDate) + 1 & "', 104) "
			' Add criteria for articles older than x days
			if len(strArchive) > 0 then
				if CBool(strOlderThan) then
					sSqlStmt = sSqlStmt & "AND DateDiff(d, articles.creationdatetime, stats.recorddate) >= " & CLng(strArchive) & " "
				else
					sSqlStmt = sSqlStmt & "AND DateDiff(d, articles.creationdatetime, stats.recorddate) < " & CLng(strArchive) & " "
				end if
			end if

			' Include only selected maingroups
			if len(strMainGroup) > 0 then
				' Include only selected maingroups
				if Instr(1, strMainGroup, "32") > 0 then
					strMainGroup = strMainGroup & ", 8192, 16384, 32768"
				end if
				sSqlStmt = sSqlStmt & "AND Articles.MainGroup IN (" & strMainGroup & ") "
			end if
			' Include only Nyhetstjenesten and Formidlingstjenesten
			sSqlStmt = sSqlStmt & "AND Articles.ntbFolderId IN (1, 2048) "
			
			sSqlStmt = sSqlStmt & "AND Stats.UserID IN (" & strUser & ") " _
						& "GROUP BY stats.userid, MainGroup " _
						& "ORDER BY Stats.userid, MainGroup"

			rsStats.Open sSqlStmt, Application("cns")
			iUserCount = 0 ' Since we starts with an empty string.
			iArticleCount = 0
			for j = 0 to 8
				aMainGroup(j) = 0
			next
			
			' Add data for all users
			strUserId = ""
			do until rsStats.EOF
				if strUserId <> rsStats.Fields("UserID") then
					iUserCount = iUserCount + 1
					strUserId = rsStats.Fields("UserID")
				end if 
				iArticleCount = iArticleCount + rsStats.Fields("NumberOfArticles")
				' Count all Maingroups
				select case rsStats.Fields("MainGroup") 
					case 1:		aMainGroup(0) = aMainGroup(0) + rsStats.Fields("NumberOfArticles")
					case 2:		aMainGroup(1) = aMainGroup(1) + rsStats.Fields("NumberOfArticles")
					case 4:		aMainGroup(2) = aMainGroup(2) + rsStats.Fields("NumberOfArticles")
					case 8:		aMainGroup(3) = aMainGroup(3) + rsStats.Fields("NumberOfArticles")
					case 16:	aMainGroup(4) = aMainGroup(4) + rsStats.Fields("NumberOfArticles")
					case 32:	aMainGroup(5) = aMainGroup(5) + rsStats.Fields("NumberOfArticles")
					case 8192:	aMainGroup(6) = aMainGroup(6) + rsStats.Fields("NumberOfArticles")
					case 16384:	aMainGroup(7) = aMainGroup(7) + rsStats.Fields("NumberOfArticles")
					case 32768:	aMainGroup(8) = aMainGroup(8) + rsStats.Fields("NumberOfArticles")
				end select
				rsStats.MoveNext
			loop
			rsStats.Close
			Set rsStats = Nothing
			if iUserCount > 0 then
				iTotalUserCount = iTotalUserCount + iUserCount
				iTotalArticleCount = iTotalArticleCount + iArticleCount
				iTotalRowCount = iTotalRowCount + 1
				for j = 0 to 8
					aTotalMainGroup(j) = aTotalMainGroup(j) + aMainGroup(j)
				next
				
				htmContent = htmContent & "<TR>" _
						& "<TD align='left'>" & GetCompanyName(aCompany(i)) & "</TD>" _
						& "<TD align='right'>" & CLng(iUserCount) & "</TD>" _
						& "<TD align='right'>" & CLng(iArticleCount) & "</TD>" _
						& "<TD align='right'>" & CLng(aMainGroup(0)) & "</TD>" _
						& "<TD align='right'>" & CLng(aMainGroup(1)) & "</TD>" _
						& "<TD align='right'>" & CLng(aMainGroup(2)) & "</TD>" _
						& "<TD align='right'>" & CLng(aMainGroup(3)) & "</TD>" _
						& "<TD align='right'>" & CLng(aMainGroup(4)) & "</TD>" _
						& "<TD align='right'>" & CLng(aMainGroup(5)) + CLng(aMainGroup(6)) + CLng(aMainGroup(7)) + CLng(aMainGroup(8)) & "</TD>" _
						& "</TR>"
			end if
		end if
	next

	' Add total in the bottom of the table.
	htmContent = htmContent & "<TR>" _
						& "<TD><B>Totalt " & iTotalRowCount & " kunder</B></TD>" _
						& "<TD align='right'><B>" & iTotalUserCount & "</B></TD>" _
						& "<TD align='right'><B>" & iTotalArticleCount & "</B></TD>" _
						& "<TD align='right'><B>" & CLng(aTotalMainGroup(0)) & "</TD>" _
						& "<TD align='right'><B>" & CLng(aTotalMainGroup(1)) & "</TD>" _
						& "<TD align='right'><B>" & CLng(aTotalMainGroup(2)) & "</TD>" _
						& "<TD align='right'><B>" & CLng(aTotalMainGroup(3)) & "</TD>" _
						& "<TD align='right'><B>" & CLng(aTotalMainGroup(4)) & "</TD>" _
						& "<TD align='right'><B>" & CLng(aTotalMainGroup(5)) + CLng(aTotalMainGroup(6)) + CLng(aTotalMainGroup(7)) + CLng(aTotalMainGroup(8)) & "</TD>" _
						& "</TR>"
	htmContent = htmContent & "</TBODY></TABLE>"

	ReportUsagePrCompany = htmContent ' & "<BR>" & sSqlStmt & "<BR>"

End Function

Function ReportRating(strCompany, strFromDate, strToDate, strArchive, strOlderThan, strMainGroup, strRatingHigher, strRatingLower)

	Dim htmContent, rsStats, rsUser, strUser, aCompany, i, sSqlStmt, strTemp
	Dim strArticleTitle, strMainGroupName, iMainGroup, iFound
	Dim iUserCount, iArticleCount, iTotalUserCount, iTotalArticleCount, iTotalRowCount
	Dim bOutput, iRatingHigher, iRatingLower, strArticleId, iTotal, strNTBID

	aCompany = Split(strCompany, ", ")
	
	htmContent = htmContent & "<TABLE border=0 id='statReport' class='news'>" _
				& "<THEAD><TH align='left' width=225px>Artikkel</TH>" _
				& "<TH align='right' width=120px>Ant unike lesere</TH>" _
				& "<TH align='right' width=80px>Ant oppslag</TH>" _
				& "<TH align='left'>Mediagruppe</TH>" _
				& "</THEAD><TBODY>"
	' Loop through all companies
	strUser = ""
	for i = lbound(aCompany) to ubound(aCompany)
		Set rsUser = RetrieveCompanyUsers(aCompany(i), 0)
		' Create a string of all the users to include in the sql
		do until rsUser.EOF
			if len(strUser) > 0 then 
				strUser = strUser & ", "
			end if
			strUser = strUser & "'" & rsUser.Fields("GeneralInfo.user_id") & "'"
			rsUser.MoveNext
		loop
	next
	
	iTotalUserCount = 0
	iTotalArticleCount = 0
	iTotalRowCount = 0

	' Get data if we have users
	if len(strUser) > 0 then
		Set rsStats = Server.CreateObject("ADODB.Recordset")
		' Check if we should link to archive article database
		sSqlStmt = "SELECT count(stats.StatsID) AS NumberOfArticles, ArticleTitle, MainGroup, ArticleID, NTB_ID, userid " _
				& "FROM ntb_archive.dbo.ARTICLES AS Articles, ntb.dbo.STATS AS Stats " _
				& "WHERE Stats.ArticleID = Articles.RefID " _
				& "AND Stats.RecordDate >= CONVERT(DATETIME, '" & strFromDate & "', 104) AND Stats.RecordDate < CONVERT(DATETIME, '" & CDate(strToDate) + 1 & "', 104) "
		' Add criteria for articles older than x days
		if len(strArchive) > 0 then
			if CBool(strOlderThan) then
				sSqlStmt = sSqlStmt & "AND DateDiff(d, articles.creationdatetime, stats.recorddate) >= " & CLng(strArchive) & " "
			else
				sSqlStmt = sSqlStmt & "AND DateDiff(d, articles.creationdatetime, stats.recorddate) < " & CLng(strArchive) & " "
			end if
		end if
		' Include only selected maingroups
		if len(strMainGroup) > 0 then
			if Instr(1, strMainGroup, "32") > 0 then
				strMainGroup = strMainGroup & ", 8192, 16384, 32768"
			end if
			sSqlStmt = sSqlStmt & "AND Articles.MainGroup IN (" & strMainGroup & ") "
		end if
		' Include only Nyhetstjenesten and Formidlingstjenesten
		sSqlStmt = sSqlStmt & "AND Articles.ntbFolderId IN (1, 2048) "

		sSqlStmt = sSqlStmt & "AND Stats.UserID IN (" & strUser & ") " _
					& "GROUP BY NTB_ID, userid, ArticleTitle, MainGroup, ArticleId "
		
		if len(strRatingHigher) > 0 then
			iRatingHigher = CLng(strRatingHigher)
		else 
			iRatingHigher = 0
		end if
		if len(strRatingLower) > 0 then
			iRatingLower = CLng(strRatingLower)
		else 
			iRatingLower = 999999
		end if
		
		sSqlStmt = sSqlStmt & "ORDER BY NTB_ID, userid "

		rsStats.Open sSqlStmt, Application("cns")
		iUserCount = 0
		iArticleCount = 0
		strMainGroupName = ""	
		strArticleTitle = ""
		strNTBID = ""
		strTemp = ""
		' Add data for all users
		bOutput = false
		do until rsStats.EOF
			iUserCount = iUserCount + 1
			iArticleCount = iArticleCount + rsStats.Fields("NumberOfArticles")
			if len(strNTBID) = 0 then
				strArticleTitle = rsStats.Fields("ArticleTitle")
				strNTBID = rsStats.Fields("NTB_ID")
				' Prm has unique NTB_ID so we don't need to parse for rev id
				if rsStats.Fields("MainGroup") < 32 then
					iFound = instrRev(strNTBID, "_")
					if iFound > 0 then 
						strNTBID = Left(strNTBID, iFound - 1)
					end if
				end if
			end if
			iMainGroup = rsStats.Fields("MainGroup")
			strArticleTitle = rsStats.Fields("ArticleTitle")
			strNTBID = rsStats.Fields("NTB_ID")
			if rsStats.Fields("MainGroup") < 32 then
				iFound = instrRev(strNTBID, "_")
				if iFound > 0 then 
					strNTBID = Left(strNTBID, iFound - 1)
				end if
			end if
			strArticleId = rsStats.Fields("ArticleID")
			rsStats.MoveNext
			if rsStats.EOF then
				bOutput = true
			else
				strTemp = rsStats.Fields("NTB_ID")
				' Prm has unique NTB_ID so we don't need to parse for rev id
				if rsStats.Fields("MainGroup") < 32 then
					iFound = instrRev(strTemp, "_")
					if iFound > 0 then 
						strTemp = Left(strTemp, iFound - 1)
					end if
				end if
				if strTemp <> strNTBID then
					strNTBID = rsStats.Fields("NTB_ID")
					' Prm has unique NTB_ID so we don't need to parse for rev id
					if rsStats.Fields("MainGroup") < 32 then
						iFound = instrRev(strNTBID, "_")
						if iFound > 0 then 
							strNTBID = Left(strNTBID, iFound - 1)
						end if
					end if
					bOutput = true
				end if
			end if
			
			if bOutput then
				if iArticleCount > iRatingHigher AND iArticleCount < iRatingLower then
					iTotalUserCount = iTotalUserCount + iUserCount
					iTotalArticleCount = iTotalArticleCount + iArticleCount
					iTotalRowCount = iTotalRowCount + 1
					strMainGroupName = GetMainGroupName(iMainGroup)

					if len(strArticleTitle) > 60 then
						strArticleTitle = left(strArticleTitle, 57) & "..."
					end if

					htmContent = htmContent & "<TR>" _
							& "<TD align='left'><A class=x HREF='../templates/x.asp?a=" & strArticleID & "' onclick='w(" & strArticleId & ");return false;'>" & strArticleTitle & "</A></TD>" _
							& "<TD align='right'>" & iUserCount & "</TD>" _
							& "<TD align='right'>" & iArticleCount & "</TD>" _
							& "<TD align='left'>" & strMainGroupName & "</TD>" _
							& "</TR>"
				end if
				bOutput = false
				iUserCount = 0
				iArticleCount = 0
			end if
		loop
		rsStats.Close
		Set rsStats = Nothing
	end if

	iTotal = GetTotalNumberOfArticles(strFromDate, strToDate, strMainGroup)
	
	' Add total in the bottom of the table.
	htmContent = htmContent & "<TR><TD align='left'><B>Totalt " & iTotalRowCount & " av " & iTotal & " artikler</B></TD>" _
						& "<TD align='right'><B>" & iTotalUserCount & "</B></TD>" _
						& "<TD align='right'><B>" & iTotalArticleCount & "</B></TD>" 
	htmContent = htmContent & "</TR></TBODY></TABLE><P>"
						
	ReportRating = htmContent ' & "<BR>" & sSqlStmt & "<BR>" & strMainGroup & "<BR>"

End Function

Function ReportLogin(strCompany, strFromDate, strToDate)

	Dim htmContent, rsStats, rsUser, strUser, aCompany, i, sSqlStmt
	Dim iUserCount, iLoginCount, iTotalUserCount, iTotalLoginCount, iUniqueUserCount
	Dim iTotalUniqueUserCount, iTotalRowCount
	Dim strCurrentUser, strCurrentDate
	
	aCompany = Split(strCompany, ", ")

	htmContent = htmContent & "<TABLE border=0 id='statReport' class='news'>" _
				& "<THEAD><TH align='left' width=270px>Kunde</TH>" _
				& "<TH align='right' width=120px>Ant unike brukere</TH>" _
				& "<TH align='right' width=90px>Ant brukere</TH>" _
				& "<TH align='right' width=110px>Ant p�logginger</TH>" _
				& "</THEAD><TBODY>"
	' Loop through all companies
	strUser = ""
	for i = lbound(aCompany) to ubound(aCompany)
		Set rsUser = RetrieveCompanyUsers(aCompany(i), 0)
		' Create a string of all the users to include in the sql
		do until rsUser.EOF
			if len(strUser) > 0 then 
				strUser = strUser & ", "
			end if
			strUser = strUser & "'" & rsUser.Fields("GeneralInfo.user_id") & "'"
			rsUser.MoveNext
		loop
	next
				
	iTotalUserCount = 0
	iTotalLoginCount = 0
	iTotalUniqueUserCount = 0
	iTotalRowCount = 0
	
	' Loop through all companies
	for i = lbound(aCompany) to ubound(aCompany)
		Set rsUser = RetrieveCompanyUsers(aCompany(i), 0)
		strUser = ""
		' Create a string of all the users to include in the sql
		do until rsUser.EOF
			if len(strUser) > 0 then 
				strUser = strUser & ", "
			end if
			strUser = strUser & "'" & rsUser.Fields("GeneralInfo.user_id") & "'"
			rsUser.MoveNext
		loop
	
		' Get data if we have users
		if len(strUser) > 0 then
			Set rsStats = Server.CreateObject("ADODB.Recordset")
			sSqlStmt = "SELECT Count(UserID) AS LoginCount, UserID, dbo.FormatDate(Stats.RecordDate, 'dd.mm.yyy') AS LoginDate " _
						& "FROM ntb.dbo.STATS as stats " _
						& "WHERE Stats.RecordDate >= CONVERT(DATETIME, '" & strFromDate & "', 104) " _
						& "AND Stats.RecordDate < CONVERT(DATETIME, '" & CDate(strToDate) + 1 & "', 104) " _
						& "AND Stats.UserID IN (" & strUser & ") " _
						& "AND StatsTypeID = 2 " _
						& "GROUP BY UserID, dbo.FormatDate(Stats.RecordDate, 'dd.mm.yyy') " _
						& "ORDER BY UserID, dbo.FormatDate(Stats.RecordDate, 'dd.mm.yyy') "

			rsStats.Open sSqlStmt, Application("cns")
			iUserCount = 0
			iLoginCount = 0
			iUniqueUserCount = 0
			strCurrentUser = ""
			strCurrentDate = ""
			' Add data for all users
			do until rsStats.EOF
				if strCurrentUser <> rsStats.Fields("UserID") or strCurrentDate <> rsStats.Fields("LoginDate") then
					if strCurrentUser <> rsStats.Fields("UserID") then
						iUniqueUserCount = iUniqueUserCount + 1
					end if
					iUserCount = iUserCount + 1
					strCurrentUser = rsStats.Fields("UserID")
					strCurrentDate = rsStats.Fields("LoginDate")
				end if
				iLoginCount = iLoginCount + rsStats.Fields("LoginCount")
				rsStats.MoveNext
			loop
			rsStats.Close
			Set rsStats = Nothing
			if iUserCount > 0 then
				iTotalUserCount = iTotalUserCount + iUserCount
				iTotalLoginCount = iTotalLoginCount + iLoginCount
				iTotalUniqueUserCount = iTotalUniqueUserCount + iUniqueUserCount
				iTotalRowCount = iTotalRowCount + 1
				
				htmContent = htmContent & "<TR>" _
						& "<TD align='left'>" & GetCompanyName(aCompany(i)) & "</TD>" _
						& "<TD align='right'>" & CLng(iUniqueUserCount) & "</TD>" _
						& "<TD align='right'>" & CLng(iUserCount) & "</TD>" _
						& "<TD align='right'>" & CLng(iLoginCount) & "</TD>" _
						& "</TR>"
			end if
		end if
	next

	' Add total in the bottom of the table.
	htmContent = htmContent & "<TD align='left' width=250px><B>Totalt " & iTotalRowCount & " kunder</B></TD>" _
						& "<TD align='right'><B>" & iTotalUniqueUserCount & "</B></TD>" _
						& "<TD align='right'><B>" & iTotalUserCount & "</B></TD>" _
						& "<TD align='right'><B>" & iTotalLoginCount & "</B></TD>" 
	htmContent = htmContent & "</TBODY></TABLE><P>"
						
	ReportLogin = htmContent ' & "<BR>" & sSqlStmt & "<BR>"

End Function

Function GetTotalNumberOfArticles(ByVal strFromDate, ByVal strToDate, ByVal strMainGroup)

	Dim rsStats, sSqlStmt
	 
	Set rsStats = Server.CreateObject("ADODB.Recordset")

	sSqlStmt = "SELECT COUNT(REFID) AS TotalArticles " _
				& "FROM ntb_archive.dbo.ARTICLES AS Articles " _
				& "WHERE (ntbFolderID IN (1, 2048)) " _
				& "AND (CreationDateTime >= CONVERT(DATETIME, '" & strFromDate & "', 104)) " _
				& "AND (CreationDateTime < CONVERT(DATETIME, '" & CDate(strToDate) + 1 & "', 104)) "
				
	if len(strMainGroup) > 0 then
		if Instr(1, strMainGroup, "32") > 0 then
			strMainGroup = strMainGroup & ", 8192, 16384, 32768"
		end if
		sSqlStmt = sSqlStmt & "AND Articles.MainGroup IN (" & strMainGroup & ") "
	end if
	rsStats.Open sSqlStmt, Application("cns")
	
	if rsStats.BOF and rsStats.EOF then
		GetTotalNumberOfArticles = 0
	else
		GetTotalNumberOfArticles = rsStats.Fields("TotalArticles")
	end if

End Function
                      
Function GetMainGroupName(intMainGroup)

	select case CLng(intMainGroup)
		case 1:		GetMainGroupName = "Innenriks"
		case 2:		GetMainGroupName = "Utenriks"
		case 4:		GetMainGroupName = "Sport"
		case 8:		GetMainGroupName = "Kultur og underholdning"
		case 16:	GetMainGroupName = "Priv til red"
		case 32:	GetMainGroupName = "Pressemelding"
		case 8192:	GetMainGroupName = "Pressemelding"
		case 16384:	GetMainGroupName = "Pressemelding"
		case 32768:	GetMainGroupName = "Pressemelding"
		case else
			GetMainGroupName = "Ukjent"
	end select

End Function

Function RenderHelpReport()

	Dim htmContent

	htmContent = "<DIV class='adminBlueLink'><A class='admin' HREF=""User_homepage.asp"">Til portalen</A>&nbsp;&nbsp;&nbsp;<A class='admin' HREF=""ViewStats.asp"">Tilbake til s�kesiden</A></DIV><BR>" 
	
	htmContent = htmContent & "<TABLE border=0 id='HelpReport' class='news'><TR><TD width=610px>" 
	
	htmContent = htmContent & "Visning av rapportene best�r av to deler. F�rst en side med valg og deretter selve rapporten.<P>" _
				& "<B>Forklaring av valgsiden p� 1-2-3:</B><BR>" _
				& "<B>1. Velg kunde:</B><BR>" _
				& "Valg av kunder kan gj�res p� flere m�ter. Man kan selektere kunder ved � velge en eller flere kundegrupper. De kundene som tilh�rer en kundegruppe blir automatisk selektert. Eller man kan velge kundene seperat i kundelisten. For � velge flere kan man holde Ctrl og/eller Shift knappen nede samtidig som man klikker med musen p� en kunde eller kundegruppe.<BR><BR>" _
				& "For � opprette en kundegruppe velger man f�rst de kundene man �nsker i en gruppe. Deretter trykker man p� knappen 'Legg til' og gir gruppen et navn. Dersom man �nsker � endre p� gruppen senere velger man av/p� de kundene man �nsker og skriver inn det samme navnet p� gruppen.<BR><BR>" _
				& "<B>2. Velg kriterie:</B><BR>" _
				& "Velg deretter de kriteriene som er �nskelig. Rating er kun gyldig for 'Rating' rapporten, og ved 'P�logginger' s� er det kun n�dvendig � velge et tidsrom.<BR><BR>" _
				& "Dato: Velg et av tidsrommene eller spesifiser selv ved hjelp av 'Velg dato' valget.<BR><BR>" _
				& "Arkiv: Velg 'Eldre enn' dersom artikkelen m� v�re lest x antall dager etter at den ble publisert, eller 'Nyere enn' dersom den skal v�re lest innen x antall dager etter at den ble publisert.<BR><BR>" _
				& "Rating: Velg om en artikkel skal v�re lest flere enn og/eller f�rre enn x antall ganger.<BR><BR>" _
				& "Mediagruppe: Velg en eller flere mediagrupper. Hvis ingen er valgt, s� blir alle grupper valgt. Velg flere ved hjelp av Ctrl eller Shift knappen.<BR><BR>" _
				& "<B>3. Velg rapport:</B><BR>" _
				& "Ved � trykke p� en av knappene navigerer man til den valgte rapporten. Se forklaring nedenfor for hva den enkelte rapporten inneholder.<P>" _
				& "<B>Forklaring av rapportene:</B><BR>" _
				& "<B>Artikkel:</B><BR>" _
				& "Denne rapporten viser det totale antall artikler som er lest i l�pet av en periode pr kunde.<BR>" _
				& "<B>Kunde</B> = Navn p� kunden. Kun de kundene som har brukere som har lest minst en artikkel blir tatt med.<BR>" _
				& "<B>Ant unike lesere</B> = Antall unike brukere som har lest minst en artikkel i l�pet av perioden.<BR>" _
				& "<B>Ant artikler</B> = Antall artikler som er lest i l�pet av perioden. Dersom to brukere leser samme artikkel blir antallet 2. Det har ingen betydning hvor mange ganger en bruker leser samme artikkel.<BR>" _
				& "<B>Mediegruppene</B> = Inndeling av antall artikler i hver mediegruppe.<BR><BR>" _
				& "<B>Rating:</B><BR>" _
				& "Denne rapporten viser ratingen p� de artiklene som tilfredstiller kriteriene i l�pet av en periode. I tillegg viser den hvor mange artikler som er publisert i perioden enten den er lest eller ikke.<BR>" _
				& "<B>Artikkel</B> = Navn p� artikkelen.<BR>" _
				& "<B>Ant unike lesere</B> = Antall unike lesere som har lest denne artikkelen.<BR>" _
				& "<B>Ant oppslag</B> = Antall ganger artikkelen er lest. For hver gang artikkelen leses �kes antallet, uavhengig av hvilken bruker som har lest artikkelen, og om han har lest den tidligere eller ikke.<BR>" _
				& "<B>Mediagruppe</B> = Navn p� mediegruppen artikkelen tilh�rer.<BR><BR>" _
				& "<B>P�logginger:</B><BR>" _
				& "Denne rapporten viser det totale antall p�logginger som er utf�rt i l�pet av en periode pr kunde.<BR>" _
				& "<B>Kunde</B> = Navn p� kunden. Kun de kundene som har brukere som har logget p� portalen minst en gang blir tatt med.<BR>" _
				& "<B>Ant unike brukere</B> = Antall unike brukere som logget seg p� i l�pet av perioden.<BR>" _
				& "<B>Ant brukere</B> = Antall brukere som har logget seg p� i l�pet av perioden. P�loggingene blir telt opp 1 pr. dag pr. bruker. Dersom en bruker logger seg p� 10 ganger i l�pet av 4 dager blir antallet 4 for denne brukeren.<BR>" _
				& "<B>Ant p�logginger</B> = Opptelling av det totale antall p�logginger som har blitt utf�rt. Her telles ogs� autologin med dersom brukeren har aktivisert denne funksjonen.<BR>" 

	htmContent = htmContent & "</TD></TR></TABLE>" 
	RenderHelpReport = htmContent

End Function
%>
