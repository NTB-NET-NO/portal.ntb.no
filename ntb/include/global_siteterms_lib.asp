<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_siteterms_lib.asp
' Global Initialization library with Site Terms functions; used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' oGetSiteTermOptions
'
' Description:
'	Retrieves a list of values for the specified SiteTerm 
'	(sPropertyName) and an instance of SiteTerms Catalog XMLDOM
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function oGetSiteTermOptions (ByVal sPropertyName, _
							  ByRef oProfileDefsCatalog, _
							  ByRef oSiteTermsProfile)
	Const PROFILE_NAME_ATTRIBUTE = "name"
	Const PROFILE_PROPERTY_NODE = "Property"
	Const PROFILE_ATTRIBUTE_NODE = "Attribute"

    Dim listTemp
    Dim dictTemp
    Dim sSearch, sProp, sName, sValue, sXML, sReferenceString
    Dim oXMLProperty, oXMLTerms, oXMLTerm, oDocElem

	Set oGetSiteTermOptions = Nothing
	' PRE Conditions
	If (oProfileDefsCatalog Is Nothing) Then Exit Function
	If (oSiteTermsProfile Is Nothing) Then Exit Function

    sSearch = ".//" & PROFILE_PROPERTY_NODE & _
			  "[" & "@" & PROFILE_NAME_ATTRIBUTE & " = '" & sPropertyName & "']"
    Set oDocElem = oProfileDefsCatalog.documentElement
    Set oXMLProperty = oDocElem.selectSingleNode (sSearch)
	sReferenceString = sGetReferenceString (oXMLProperty)
    If (Not bIsSiteTerm(oXMLProperty, sReferenceString)) Then Exit Function

    sSearch = ".//" & PROFILE_PROPERTY_NODE & _
			  "[" & "@" & PROFILE_NAME_ATTRIBUTE & " = '" & sReferenceString & "']"
    Set oDocElem = oSiteTermsProfile.documentElement
    Set oXMLProperty = oDocElem.selectSingleNode (sSearch)
	If (oXMLProperty Is Nothing) Then Exit Function

    Set oXMLTerms = oXMLProperty.selectNodes("./" & PROFILE_ATTRIBUTE_NODE)
    If (oXMLTerms.length = 0) Then Exit Function

	Set listTemp = Server.CreateObject ("Commerce.SimpleList")
	If (listTemp Is Nothing) Then Exit Function

	For Each oXMLTerm In oXMLTerms
	    sName = oXMLTerm.getAttribute("displayName")
	    sValue = oXMLTerm.getAttribute ("value")
	    If IsNull(sValue) or IsEmpty(sValue) or (sValue = "") Then _
	       sValue = oXMLTerm.getAttribute("name")

		Set dictTemp = Server.CreateObject ("Commerce.Dictionary")
		If Not (dictTemp Is Nothing) Then
			dictTemp(sName) = sValue
			listTemp.Add dictTemp
		End If
	Next

    Set oGetSiteTermOptions = listTemp
End Function


' -----------------------------------------------------------------------------
' bIsSiteTerm
'
' Description:
'	Returns TRUE if @propType = "SITETERM"
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bIsSiteTerm(ByVal oXMLProperty, ByRef sReferenceString)
	Const SITE_TERM = "SITETERM"
	Const PROP_TYPE = "propType"

	Dim aNames

	bIsSiteTerm = False

	If (oXMLProperty.getAttribute(PROP_TYPE) <> SITE_TERM) Then Exit Function
	If IsNull(sReferenceString) Then
		Err.Raise &h80004005   ' A profile's SiteTerm property did not specify a Type Reference, which is required
	End If
	
	aNames = Split (sReferenceString, ".")
	sReferenceString = aNames (UBound(aNames))

	bIsSiteTerm = True
End Function


' -----------------------------------------------------------------------------
' sGetReferenceString
'
' Description:
'	Returns the @referenceString value for a given Property object
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetReferenceString(ByRef oXMLProperty)
	Const PROFILE_REFSTRING_ATTRIBUTE = "referenceString"

	Dim sTemp

	sGetReferenceString = ""
	If (oXMLProperty Is Nothing) Then Exit Function

	On Error Resume Next
	sTemp = oXMLProperty.getAttribute(PROFILE_REFSTRING_ATTRIBUTE)
	If (Err.Number <> 0) Then
		Err.Clear
		Exit Function
	End If

	sGetReferenceString = sTemp
End Function
</SCRIPT>