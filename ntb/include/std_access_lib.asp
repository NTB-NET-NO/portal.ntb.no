<%
' =============================================================================
' std_access_lib.asp
' Standard library with Access control functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetUserInfo
'
' Description:
'	Called from setupenv include file, this function determines:
'   m_userID -- userid
'   m_UserAccessType -- IIS_AUTH / TICKET_AUTH / GUEST_VISIT / ANON_VISIT
'   m_UserType -- AUTH_USER / GUEST_USER / ANON_USER
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
'The provided function
'Sub GetUserInfo()
	'Dim sAuthUser, rsUser

	'If dictConfig.i_FormLoginOptions = USE_IIS_AUTH Then
		'sAuthUser = Request.ServerVariables("AUTH_USER")
		'If sAuthUser <> "" Then 
			' Strip off any Domain information
			'sAuthUser = Mid(sAuthUser, InStr(1, sAuthUser, "\", vbBinaryCompare) + 1)		
			'Set rsUser = GetUserProfileByLoginName(sAuthUser)

			' Defer error handling to EnsureAccess/EnsureAuthAccess
			'If rsUser Is Nothing Then
				'm_UserID = ""
			'Else
				'm_UserID = rsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value
			'End If
			
			'm_UserType = AUTH_USER
			'm_UserAccessType = IIS_AUTH
		'Else
			' Force Authentication if the AUTH_USER Server Variable is blank
			' by sending the Response.Status of 401 Access Denied.
	
			' Finish the Page by issuing a Response.End so that a user
			' cannot cancel through the dialog box.

			'Response.Status = "401 Access Denied"
			'Response.Write mscsMessageManager.GetMessage("L_401AccessDenied_ErrorMessage", sLanguage)
			'Response.End		
		'End If
	'Else		
		'If mscsAuthMgr.IsAuthenticated() Then
			'm_UserID = mscsAuthMgr.GetUserID(AUTH_TICKET)
			'm_UserAccessType = TICKET_AUTH
			'm_UserType = AUTH_USER
		'ElseIf Not IsNull(mscsAuthMgr.GetUserID(GUEST_TICKET)) Then
			'm_UserID = mscsAuthMgr.GetUserID(GUEST_TICKET)
			'm_UserAccessType = GUEST_VISIT
			'm_UserType = GUEST_USER
		'Else
			'm_UserID = ""
			'm_UserAccessType = ANON_VISIT
			'm_UserType = ANON_USER
		'End If
	'End If
'End Sub

'Code provided from Commerce help-files to enable use of AuthFilter
Sub GetUserInfo()
    Dim sAuthUser, mscsUser
    Dim aName

    If dictConfig.i_DelegatedAdminOptions = DELEGATED_ADMIN_SUPPORTED Then
       sAuthUser = LoginName(Request.ServerVariables("LOGON_USER")) 
        End If
        Rem Integrated Windows and basic auth take precedence over HTML form auth
        If sAuthUser <> "" Then
            m_UserAccessType = IIS_AUTH ' BASIC_AUTH
            m_UserType = AUTH_USER
            Set mscsUser = GetUserProfileByLoginName(sAuthUser)
            If mscsUser Is Nothing Then
               m_UserID = ""
            Else
               m_UserID = mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value
            End If
        Else
            If mscsAuthMgr.IsAuthenticated() Then
              sAuthUser = mscsAuthMgr.GetUserID(AUTH_TICKET)
              Set mscsUser = GetUserProfileByLoginName(sAuthUser)
                If mscsUser Is Nothing Then
                   m_UserID = ""
                Else
                  m_UserID = mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value
                End If
              m_UserAccessType = TICKET_AUTH
              m_UserType = AUTH_USER
            Else 
                If Not IsNull(mscsAuthMgr.GetUserID(GUEST_TICKET)) Then
                   m_UserID = mscsAuthMgr.GetUserID(GUEST_TICKET)
                   m_UserAccessType = GUEST_VISIT
                   m_UserType = GUEST_USER
                Else
                   m_UserID = ""
                   m_UserAccessType = ANON_VISIT
                   m_UserType = ANON_USER
                End If
            End If
        End If
End Sub

'Strip away the domain-name
	Function LoginName(ByVal sUserName)
    Dim aName
    
    If (sUserName = "") Then
      Exit Function
    End if
    aName = Split (sUserName, "\")
    LoginName = aName(UBound(aName))
	End Function

' -----------------------------------------------------------------------------
' EnsureAccess
'
' Description:
'	EnsureAccess() implements page-level access control. The function grants
'	access to users who are either authenticated to IIS (using Basic/Integrated
'	Windows authentication) or have tickets. Call EnsureAccess() from within
'	Main() on every page that requires such access control. A call to
'	EnsureAccess() must appear before any other statement in Main(). 
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub EnsureAccess()
	Dim sUserID, rsUser, bCookieAllowed
	
	Select Case m_UserType
		Case ANON_USER
			If dictConfig.i_FormLoginOptions = LOGIN_OPTIONAL_ON_ENTRANCE Then
				Response.Redirect(GenerateURL(MSCSSitePages.Login, Array(), Array()))
			Else
				bCookieAllowed = IsPersistentCookieAllowed()
			
				' Create a profile for new guest user if site privacy option allows profiling.
				Set rsUser = GetNewGuestUserProfile()

				sUserID = sGetUserIDForNewGuestUser(rsUser)
				If bCookieAllowed Then
					Call mscsAuthMgr.SetProfileTicket(sUserID, WRITE_TICKET_TO_COOKIE)	
					m_iTicketLocation = 2
				Else
					Call mscsAuthMgr.SetProfileTicket(sUserID, WRITE_TICKET_TO_URL)
					m_iTicketLocation = 1
					' Must redirect to ensure the ticket appears in URL.
					Response.Redirect(GenerateURL(MSCSSitePages.Home, Array(), Array()))
				End If
				
				m_UserID = sUserID
				m_UserType = GUEST_USER
				m_UserAccessType = GUEST_VISIT
				Call EnsureAccess()
			End If
		
		Case GUEST_USER
			If IsFormLoginRequired() Then
				Response.Redirect(GenerateURL(MSCSSitePages.Login, Array(), Array()))
			End If

			Set mscsUserProfile = GetCurrentUserProfile()
			If Not mscsUserProfile Is Nothing Then
				If mscsUserProfile.Fields(GetQualifiedName(GENERAL_INFO_GROUP, PROFILE_TYPE)) = REGISTERED_PROFILE Then
					Response.Redirect(GenerateURL(MSCSSitePages.Welcome, Array(), Array()))
				End If
			End If
			
		Case AUTH_USER
			Set mscsUserProfile = EnsureUserProfile()
	End Select
End Sub


' -----------------------------------------------------------------------------
' EnsureAuthAccess
'
' Description:
'	EnsureAuthAccess() implements page-level access control. The function
'	grants access to users who are either authenticated to IIS (using Basic/
'	Integrated Windows authentication) or have an auth ticket. Call
'	EnsureAuthAccess() from within Main() on every page that requires such
'	access control. A call to EnsureAuthAccess() must appear before any other
'	statement in Main(). 
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function EnsureAuthAccess()
	If m_UserType <> AUTH_USER Then
		Response.Redirect(GenerateURL(MSCSSitePages.Login, Array(), Array()))
	End If
	
	Set mscsUserProfile = EnsureUserProfile()
End Function        


' -----------------------------------------------------------------------------
' CheckPartnerServiceAccess
'
' Description:
'	CheckPartnerServiceAccess() implements page-level access control.  It
'	grants access to users who have their PartnerDeskRoleFlags set to
'	ROLE_ADMIN, all others are redirected to an error page.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CheckPartnerServiceAccess()
	Dim iUserRole
	Dim sUserOrgID
	Dim mscsUserProfile
	
	Set mscsUserProfile = GetCurrentUserProfile()
	
	iUserRole = mscsUserProfile(USER_PARTNERDESK_ROLE).Value
	If IsNull(iUserRole) Or iUserRole <> ROLE_ADMIN then
		Response.Redirect(GenerateURL(MSCSSitePages.NoAuth, Array(), Array())) 
	End If
	
	sUserOrgID = mscsUserProfile(USER_ORGID).Value
	If IsNull(sUserOrgID) then
	    Response.Redirect(GenerateURL(MSCSSitePages.NoAccount, Array(), Array()))
	End If	
End Sub


' -----------------------------------------------------------------------------
' IsFormLoginRequired
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsFormLoginRequired()
    IsFormLoginRequired = False
    
    Select Case dictConfig.i_FormLoginOptions
        Case FORCE_LOGIN_ON_ENTRANCE
			IsFormLoginRequired = True
        Case FORCE_LOGIN_ON_PURCHASE
            If IsEntityInSet(sThisPage, MSCSPageSets.PurchasePageSet) Then
				IsFormLoginRequired = True
            End If
    End Select
End Function


' -----------------------------------------------------------------------------
' IsPersistentCookieAllowed
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsPersistentCookieAllowed()
	IsPersistentCookieAllowed = False
	
	' Test the cookie-readiness of the user agent by writing a persistent cookie to
	'	the response and reading it back on a subsequent request.
	If dictConfig.i_SiteTicketOptions = PUT_TICKET_IN_COOKIE Then
		' If cookies has not been written to response yet ...
		If IsNull(MSCSAppFrameWork.RequestNumber("cookie_test", Null)) Then
			' Write the cookie to the response.
			Call SetTestCookie("persistent_cookie")
			' Issue a request to read back the cookie.
			Call Response.Redirect(AddURLArguments(MSCSAppFramework.GetPageURL(), Array("cookie_test"), Array(1)))
		Else
			' If the request contains the cookie we set earlier ...
			If Request.Cookies("MSCS2000TestCookie") <> "" Then
				' Remove the test cookie so we don't leave a stale cookie behind.
				Response.Cookies("MSCS2000TestCookie").Expires = Now - 1
				IsPersistentCookieAllowed = True
			End If
		End If
	End If
End Function


' -----------------------------------------------------------------------------
' sGetUserIDForNewGuestUser
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetUserIDForNewGuestUser(ByVal rsUser)
	If rsUser Is Nothing Then
		sGetUserIDForNewGuestUser = MSCSGenID.GenGUIDString		
	Else
		sGetUserIDForNewGuestUser = rsUser.Fields("GeneralInfo.user_id").Value
	End If
End Function


' -----------------------------------------------------------------------------
' GetAuthManagerObject
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAuthManagerObject()
	Set GetAuthManagerObject = Server.CreateObject("Commerce.AuthManager")
    GetAuthManagerObject.Initialize(MSCSCommerceSiteName)
End Function
%>