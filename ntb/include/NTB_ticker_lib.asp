<SCRIPT LANGUAGE=VBScript RUNAT=Server>

Sub initTicker()
	Application("vt") = 2
	Application("tbuffer1") = ""
	Application("tbuffer2") = ""
	Application("tcontentURL") = "../ticker/content.asp"
	Application("showticker") = 0
	Application("lastmin") = Minute(Now)
	
	'applet tags
	Application("tmarkup") = "<applet code='headline.class' width='764' height='25'>"_
			& "<param name='applet_width' value='764'>"_
			& "<param name='applet_height' value='25'>"_
			& "<param name='read_messages' value='read_from_file'>"_
			& "<param name='text_file' value='" &Application("tcontentURL") &"'>"_
			& "<param name='copyright' value='Copyright (c) 2001 BruceM Anderson,http://appletlib.tripod.com'>"_
			& "<param name='regcode' value=''>"_				
			& "<param name='headline_width' value='106'>"_
			& "<param name='headline_color' value='003084'>"_
			& "<param name='headline_highlight_color' value='003084'>"_
			& "<param name='headline_bgcolor' value='003084'>"_
			& "<param name='message_color' value='dee9ed'>"_
			& "<param name='message_highlight_color' value='BFCCD9'>"_
			& "<param name='message_bgcolor' value='003084'>"_
			& "<param name='message_font_type' value='Arial'>"_
			& "<param name='message_font_size' value='18'>"_
			& "<param name='message_font_style' value='1'>"_
			& "<param name='message_underline' value='no'>"_
			& "<param name='message_align' value='center'>"_
			& "<param name='message_margin' value='10'>"_
			& "<param name='border_thickness' value='0'>"_
			& "<param name='border_color' value='003084'>"_
			& "<param name='pausetime' value='3000'>"_
			& "<param name='scroll_delay' value='40'>"_
			& "<param name='type_delay' value='40'>"_
			& "<param name='yoffset' value='0'>"_
			& "</applet>"

	Application("tickface1") = ""
	Application("tickface2") = ""

	call updateTickerContent()

End Sub

Sub updateTickerContent()
	Dim rs, mm, trr, msgs, tcn ' ticker rows
	
	Application("lastmin") = Minute(Now)
	
	Set rs = Server.CreateObject("ADODB.Recordset")
	Set msgs = Server.CreateObject("ADODB.Stream")
	
	rs.CursorLocation = 3
	rs.Open "getTickers", Application("cns"), 0
	trr = rs.RecordCount
	
	msgs.Mode = 3
	msgs.Type = 2
	msgs.LineSeparator = -1 'adCRLF
	msgs.Open
	'msgs.WriteText "!!LedSign ticker script",1

	if not rs.EOF then
		'update tickface
		'process returned records
		'msgs.WriteText "!!Ticker has " & CStr(trr) & " messages.",1
		
		Dim url, s, msgtext
		Dim mark
		mark = 0
		
		while not rs.EOF
			s=CStr(rs.Fields("MessageText"))

			mark = instr(1,s,"/*U:")
			if mark > 0 then
				mark = mark+4
				url = Mid(s,mark,instr(mark,s,"*/")- mark)
				url = Left (url,len(url)-7)
			end if

			mark = instr(1,s,"/*M:")
			if mark > 0 then
				mark = mark+4
				msgtext = Mid(s,mark,instr(mark,s,"*/")- mark)
				msgtext = Right(msgtext,len(msgtext)-2)
			end if

			if url <> "" then
				msgs.WriteText "Viktig melding:|" &msgtext &"|" &url &"|_ny", 1
			else
				msgs.WriteText "Viktig melding:|" &msgtext &"| | ", 1
			end if
			
			rs.MoveNext
		wend
		'msgs.WriteText "Reload",1
	end if
	rs.close
	set rs = nothing

	msgs.Position = 0
	if Application("vt") = 1 then
		Application("tbuffer2") = msgs.ReadText()
	else
		Application("tbuffer1") = msgs.ReadText()
	End if
	msgs.Close
	set msgs = Nothing

	if Application("vt") = 1 then
		if trr > 0 then
			Application("tickface2") = Application("tmarkup")
		else
			Application("tickface2") = ""
		end if
		Application("vt") = 2
	else
		if trr > 0 then
			Application("tickface1") = Application("tmarkup")
		else
			Application("tickface1") = ""
		end if
		Application("vt") = 1
	End if
	if trr > 0 then
			Application("showticker") = trr
		else
			Application("showticker") = 0
	End if
	
End sub

Function getTickerFace()
	' Returns <APPLET.... fragment surrounded by table row tags or
	' nothing if there are no pending messages
	Dim ts, te
	ts = "<TR><TD colspan=""8"" width=""100%"" align=""right"">"
	te = "</TD></TR>"
	If Application("showticker") > 0 then ' there is something to show
		if Application("vt") = 1 then
			getTickerFace = Application("tickface1")
		else
			getTickerFace = Application("tickface2")
		end if
	else
		getTickerFace = ""
	End if
End Function
</SCRIPT>
