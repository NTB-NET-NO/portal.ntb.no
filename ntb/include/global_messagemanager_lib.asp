<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_messagemanager_lib.asp
' Global Initialization library with Message Manager functions; used by
' global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetMessageManagerObject
'
' Description:
'	Read Message Manager entries from rc.xml
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetMessageManagerObject()
    Dim MsgMgr
    Dim oDOMDocument 'As DOMDocument
    Dim RCFileName
    Dim SchemaNode 'As IXMLDOMNode
    Dim LanguageNodes 'As IXMLDOMNodeList
    Dim LanguageNode 'As IXMLDOMNode
    Dim LanguageName
    Dim Locale
    Dim EntryNodes 'As IXMLDOMNodeList
    Dim EntryNode 'As IXMLDOMNode
    Dim EntryName
    Dim EntryValue
    Dim ValueNodes 'As IXMLDOMNodeList
    Dim ValueNode 'As IXMLDOMNode

    Set MsgMgr = CreateObject("Commerce.MessageManager")
    RCFileName = GetRootPath() & "rc.xml"
    Set oDOMDocument = GetXMLFromFile(RCFileName)
    Set SchemaNode = oDOMDocument.getElementsByTagName("MessageManager").Item(0)
    
    Set LanguageNodes = SchemaNode.selectNodes("Language")
    For Each LanguageNode In LanguageNodes
        LanguageName = LanguageNode.Attributes.getNamedItem("Name").Text
        Locale = LanguageNode.Attributes.getNamedItem("Locale").Text
        Call MsgMgr.AddLanguage(LanguageName, Locale)
    Next
    
    Set EntryNodes = SchemaNode.selectNodes("Entry")
    For Each EntryNode In EntryNodes
        EntryName = EntryNode.Attributes.getNamedItem("Name").Text
        
        Set ValueNodes = EntryNode.selectNodes("Value")
        For Each ValueNode In ValueNodes
            LanguageName = ValueNode.Attributes.getNamedItem("Language").Text
            EntryValue = ValueNode.Text
            Call MsgMgr.AddMessage(EntryName, EntryValue, LanguageName)
        Next
    Next
    
    MsgMgr.DefaultLanguage = SchemaNode.Attributes.getNamedItem("DefaultLanguage").Text
    Set GetMessageManagerObject = MsgMgr
End Function


' -----------------------------------------------------------------------------
' GetXMLFromFile
'
' Description:
'	GetXMLFromFile -- Simple helper function that simply loads an XML DOM from
'	a file
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Public Function GetXMLFromFile(ByVal sFileName) 'As DOMDocument
    Dim xmlDoc 'As New DOMDocument
    Set xmlDoc = CreateObject("MSXML.DOMDocument")
    
    xmlDoc.load sFileName
    
    Set GetXMLFromFile = xmlDoc
End Function
</SCRIPT>