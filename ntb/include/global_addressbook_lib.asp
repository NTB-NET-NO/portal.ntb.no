<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_addressbook_lib.asp
' Global Initialization library with Address Book functions; used by
' global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetShipToAddressFieldDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetShipToAddressFieldDefinitions()
	Dim MSCSMessageManager
	Dim sLanguage

	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

	Dim listFlds, dictFld
	
	Set listFlds = GetAddressFieldDefinitions()

	Set dictFld = GetDictionary()
	dictFld.Name = BILLING_USE
	dictFld.Label = MSCSMessageManager.GetMessage("L_BillToAddress_SameAs_ShipToAddress_HTMLText", sLanguage)
	dictFld.InputType = CHECKBOX
	dictFld.DefaultValue = "1"
	Call listFlds.Add(dictFld)

	Set GetShipToAddressFieldDefinitions = listFlds
End Function


' -----------------------------------------------------------------------------
' GetAddressFieldDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAddressFieldDefinitions()
	Dim MSCSMessageManager
	Dim sLanguage
	Dim listFlds, dictFld
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage
    
    Dim AddressLastName
    Dim AddressFirstName
    Dim AddressLine1
    Dim AddressLine2
    Dim AddressCity
    Dim AddressRegion
    Dim AddressPostalCode
    Dim AddressCountry

    Set dictFld = GetDictionary()
    dictFld.Name = FIELD_ADDRESS_LAST_NAME
    dictFld.Label = MSCSMessageManager.GetMessage("L_LastName_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadLastName_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Set AddressLastName = dictFld
    
    Set dictFld = GetDictionary()
    dictFld.Name = FIELD_ADDRESS_FIRST_NAME
    dictFld.Label = MSCSMessageManager.GetMessage("L_FirstName_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadFirstName_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Set AddressFirstName = dictFld

    Set dictFld = GetDictionary()
    dictFld.Name = FIELD_ADDRESS_ADDRESS1
    dictFld.Label = MSCSMessageManager.GetMessage("L_AddressLine1_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadAddressLine1_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Set AddressLine1 = dictFld           
    
    Set dictFld = GetDictionary()
    dictFld.Name = FIELD_ADDRESS_ADDRESS2
    dictFld.Label = MSCSMessageManager.GetMessage("L_AddressLine2_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadAddressLine2_ErrorMessage", sLanguage)
    dictFld.IsRequired = False
    Set AddressLine2 = dictFld           
    
    Set dictFld = GetDictionary()
    dictFld.Name = FIELD_ADDRESS_CITY
    dictFld.Label = MSCSMessageManager.GetMessage("L_City_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadCity_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Set AddressCity = dictFld              

    Set dictFld = GetDictionary()
    dictFld.Name = REGION_NAME
    dictFld.Label = MSCSMessageManager.GetMessage("L_Region_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadRegion_ErrorMessage", sLanguage)    
    dictFld.IsRequired = False
    Set AddressRegion = dictFld            

    Set dictFld = GetDictionary()
    dictFld.Name = POSTAL_CODE
    dictFld.Label = MSCSMessageManager.GetMessage("L_PostalCode_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadPostalCode_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Set AddressPostalCode = dictFld     

    Set dictFld = GetDictionary()
    dictFld.Name = FIELD_ADDRESS_COUNTRY_CODE
    dictFld.Label = MSCSMessageManager.GetMessage("L_Country_HTMLText", sLanguage)
    dictFld.Size = 1
    dictFld.InputType = LISTBOX
    Set dictFld.ListItems = GetCountryNamesList()
    Set dictFld.ListValues = GetCountryCodesList()    
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_BadCountry_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Set AddressCountry = dictFld          

    Set listFlds = GetSimpleList()

    ' $$INTLADDR: This illustrates how to use an alternative address format (in this case Japanese) 
    'If dictConfig.i_SiteDefaultLocale = 1041 Then
    '    Call listFlds.Add(AddressCountry)            
    '    Call listFlds.Add(AddressPostalCode)            
    '    Call listFlds.Add(AddressRegion)            
    '    Call listFlds.Add(AddressCity)            
    '    Call listFlds.Add(AddressLine1)            
    '    Call listFlds.Add(AddressLine2)            
    '    Call listFlds.Add(AddressLastName)            
    '    Call listFlds.Add(AddressFirstName)        
    'Else
        Call listFlds.Add(AddressLastName)            
        Call listFlds.Add(AddressFirstName)        
        Call listFlds.Add(AddressLine1)            
        Call listFlds.Add(AddressLine2)            
        Call listFlds.Add(AddressCity)            
        Call listFlds.Add(AddressRegion)            
        Call listFlds.Add(AddressPostalCode)            
        Call listFlds.Add(AddressCountry)            
    'End If

	Set GetAddressFieldDefinitions = listFlds
End Function


' -----------------------------------------------------------------------------
' GetAddressBookAddressFieldDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAddressBookAddressFieldDefinitions()
	Dim MSCSMessageManager
	Dim sLanguage
	Dim listFlds, dictFld
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

    Set listFlds = GetSimpleList()
    
    Set dictFld = GetDictionary()
    dictFld.Name = FIELD_ADDRESS_FRIENDLY_NAME
    dictFld.Label = MSCSMessageManager.GetMessage("L_AddressName_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_AddressName_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Call listFlds.Add(dictFld)
    
    For Each dictFld In GetAddressFieldDefinitions()
		Call listFlds.Add(dictFld)
    Next

    Set GetAddressBookAddressFieldDefinitions = listFlds
End Function
</SCRIPT>
