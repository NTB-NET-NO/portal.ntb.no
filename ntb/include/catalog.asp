<%
' =============================================================================
' Catalog.asp
' Catalog functions used by other files like catalog.asp, menu.asp, search.asp,
' etc.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Const PRODUCTLIST_PAGE_SIZE = 10 ' How many products to show in productlist results
Const CATALOG_ROOT_CATEGORY_COLS = "CategoryName,CatalogName"


' -----------------------------------------------------------------------------
' sUserCatalogsAsString
'
' Description:
'   This function retrieves the catalogs associated with a user as a comma-
'   separated list
'
' Parameters:
'	none
'
' Returns:
'	String
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sUserCatalogsAsString()
	Dim sCatalogName, sResults
	Dim rsCatalogs
	Dim bFound
	bFound = False
	
	Set rsCatalogs = mscsUserCatalogs()
	
	While Not rsCatalogs.EOF
		bFound = True
		sCatalogName = rsCatalogs(CATALOG_NAME_PROPERTY_NAME)
		sResults = sResults & sCatalogName & ","
		
		rsCatalogs.MoveNext()
	Wend
	If bFound Then sResults = Left(sResults, Len(sResults) - 1)
	Set rsCatalogs = Nothing

	sUserCatalogsAsString = sResults
End Function	


' -----------------------------------------------------------------------------
' mscsUserCatalogsetID
'
' Description:
'	This method looks up the catalogsetid for the user.
'
' Parameters:
'	none
'
' Returns:
'	CatalogSet
'
' Notes :
'	It works by first looking at the user properties... if there is no 
'	user-level default, it will look for an org-level catalogset.
'	Else, the method will return a catalogID as specified
'   in RegisteredUserDefaultCatalogSet or AnonymousUserDefaultCatalogSet
'   (This code simulates CatalogSets::mscsUserCatalogsetID w/o any object
'	creation)
' -----------------------------------------------------------------------------
Function mscsUserCatalogsetID()
    Dim rsUser, rsOrg
    Dim org_id

    Set rsUser = GetCurrentUserProfile()
    
    If Not rsUser Is Nothing Then
        mscsUserCatalogsetID = rsUser.Fields.Item(USER_CATALOGSET).Value
        If IsNull(mscsUserCatalogsetID) Then
            ' Get the user's org profile.
            org_id = rsUser.Fields.Item(USER_ORGID).Value
            
            If Not IsNull(org_id) Then
				' Set rsGetProfile's bForceDBLookUp to False to avoid a database look-up.
				Set rsOrg = rsGetProfileByKey(FIELD_ORG_ORG_ID, org_id, PROFILE_TYPE_ORG, False)
                If Not rsOrg Is Nothing Then 
                    mscsUserCatalogsetID = rsOrg.Fields.Item(ORG_CATALOGSET).Value
                End If
            End If
        End If
    Else
		mscsUserCatalogsetID = Null
    End If
    
    If IsNull(mscsUserCatalogsetID) Then
		' Get the catalog set to fall back if no catalog set was specified at user/org level.
		If m_UserType = AUTH_USER Then
			mscsUserCatalogsetID = dictConfig.s_AuthenticatedUserDefaultCatalogSet
		Else
			mscsUserCatalogsetID = dictConfig.s_AnonymousUserDefaultCatalogSet
		End If
    End If
End Function


' -----------------------------------------------------------------------------
' mscsUserCatalogs
'
' Description:
'   This function retrieves the catalogs associated with a user as a recordset.
'
' Parameters:
'	none
'
' Returns:
'	CatalogSet
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsUserCatalogs()
	Dim mscsCatalogSets
	Dim sFallBackCatalogSet
	
	Set mscsCatalogSets = Server.CreateObject("Commerce.CatalogSets")
	Call mscsCatalogSets.Initialize(dictConfig.s_CatalogConnectionString, _
	                                dictConfig.s_TransactionConfigConnectionString)
	
	' Get the catalog set to fall back if no catalog set was specified at user/org level.
	If m_UserType = AUTH_USER Then
		sFallBackCatalogSet = dictConfig.s_AuthenticatedUserDefaultCatalogSet
	Else
		sFallBackCatalogSet = dictConfig.s_AnonymousUserDefaultCatalogSet
	End If
	
	Set mscsUserCatalogs = mscsCatalogSets.GetCatalogsForUser(MSCSProfileService, _
															m_UserID, _
										 					sFallBackCatalogSet)
	Set mscsCatalogSets = Nothing
End Function


' -----------------------------------------------------------------------------
' mscsUserCatalogsFromID
'
' Description:
'   This function retrieves the catalogs associated with a user as a recordset.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsUserCatalogsFromID(ByVal CatalogSetID)
	Dim mscsCatalogSets
		
	Set mscsCatalogSets = Server.CreateObject("Commerce.CatalogSets")
	Call mscsCatalogSets.Initialize(dictConfig.s_CatalogConnectionString, _
	                                dictConfig.s_TransactionConfigConnectionString)
	
	Set mscsUserCatalogsFromID = mscsCatalogSets.GetCatalogsInCatalogSet(CatalogSetID)
	Set mscsCatalogSets = Nothing
End Function

' -----------------------------------------------------------------------------
' GetPriceAndCurrency
'
' Description:
'	Retrieves both the price and price string of the product at the cursor
'	in rsProducts
'
' Parameters:
'	rsProducts				- the Recordset with products
'	cyProductPrice			- the retrieved price of that product
'	cyCurrency				- used to return the price as a string formatted to the locale
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub GetPriceAndCurrency(ByRef rsProducts, ByRef cyProductPrice, ByRef cyCurrency)
	If (Not rsProducts.fields("i_ClassType") = cscProductFamilyClass ) Then	'	no variants found
		cyProductPrice = rsProducts.Fields(PRODUCT_PRICE_PROPERTY_NAME).Value
		cyCurrency = htmRenderCurrency(cyProductPrice)
	Else
		cyProductPrice = ""
		cyCurrency = mscsMessageManager.GetMessage("pur_VariantPrice", sLanguage)
	End If
End Sub


' -----------------------------------------------------------------------------
' mscsGetCategoryObject
'
' Description:
'   This function gets the category object for the given category name.
'   If the category name is "" (meaning root category), this function will 
'   return the Nothing object, as the catalog doesn't have an category
'   object for the root
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsGetCategoryObject(ByVal oCatalog, ByVal sCategoryName)
	Dim oCategory
	
	If sCategoryName <> "" Then
		Set oCategory = oCatalog.GetCategory(sCategoryName)
		If oCategory Is Nothing Then
			Response.Redirect(GenerateURL(MSCSSitePages.BadCatalogItem, Array(), Array()))	
		End If
	Else
		Set oCategory = Nothing
	End If
	
	Set mscsGetCategoryObject = oCategory
End Function	

' -----------------------------------------------------------------------------
' rsFreeTextSearch
'
' Description:
'	This function performs a FreeTextSearch search, using typical
'	properties to return and common properties to sort on.
'	Returns the results as a recordset, and iRecordCount gets set to
'	the number of records retrieved.
'
' Parameters:
'	sPhrase					- the freetext string to find
'	iStartingRecord			- return results starting from this record#
'	iRecordsToRetrieve		- return max. this many records
'	iRecordCount			- retrieve the actually retrieved number
'								of records into this parameter
'
' Returns:
'	Recordset with search results
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsFreeTextSearch(ByVal sPhrase, ByVal iStartingRecord, ByVal iRecordsToRetrieve, ByRef iRecordCount)
	Dim sPropertiesToReturn, sPropertiesToSortOn, rsResult, iClassType, sCatalogsToSearch
	
	' "name" and "description" are required product properties and cannot have null values.
	sPropertiesToReturn = PRODUCT_NAME_PROPERTY_NAME & "," & _
	                      PRODUCT_DESCRIPTION_PROPERTY_NAME & "," & _
	                      CATALOG_NAME_PROPERTY_NAME & "," & _
	                      CATEGORY_NAME_PROPERTY_NAME & "," & _
	                      CATALOG_PRODUCTID_PROPERTY_NAME & "," & _
	                      CATALOG_VARIANTID_PROPERTY_NAME & "," & _
	                      DEFINITION_TYPE_PROPERTY_NAME


    ' "name" is a required product property and cannot have null value.
    sPropertiesToSortOn = CATEGORY_NAME_PROPERTY_NAME & "," & _	                      
                          PRODUCT_NAME_PROPERTY_NAME
	
	' iClassType determines the type of entities to search.
	' (See FreeTextSearch documentation)
	iClassType = cscProductFamilyForVariantsClass Or _
	             cscProductClass Or _
	             cscCategoryClass

	sCatalogsToSearch = sUserCatalogsAsString()
	
	Set rsFreeTextSearch = MSCSCatalogManager.FreeTextSearch( _
	                                                    sPhrase, _
	                                                    sCatalogsToSearch, _
	                                                    iClassType, _
	                                                    sPropertiesToReturn, _
	                                                    sPropertiesToSortOn, _
	                                                    True, _
	                                                    iStartingRecord, _
	                                                    iRecordsToRetrieve, _
	                                                    iRecordCount _
	                                                )
End Function


' -----------------------------------------------------------------------------
' mscsGetProductList
'
' Description:
'   This function gets the product recordset for a given category, using the
'	given paging information.  It returns nOutRecordsTotal, nOutPagesTotal as
'	outparams.
'   If the category object is null, it uses the RootProducts for the list of
'	products.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsGetProductList(oCatalog, oCategory, nPageNumber, nOutRecordsTotal, nOutPagesTotal)
	Dim sSortColumn, nStartingRecord, nTemp
		
	sSortColumn = PRODUCTLIST_SORT_PROPERTY_NAME
	nStartingRecord = (nPageNumber - 1) * PRODUCTLIST_PAGE_SIZE + 1
	If Not oCategory Is Nothing Then
		Set mscsGetProductList = oCategory.Products(, _
													sSortColumn, _
													nStartingRecord, _
													PRODUCTLIST_PAGE_SIZE, _
													nOutRecordsTotal _
											    )
		' Determine the total number of pages
		nTemp = nOutRecordsTotal / PRODUCTLIST_PAGE_SIZE
		If (nTemp) = Fix(nTemp) Then
			nOutPagesTotal = nTemp
		Else
			nOutPagesTotal = Fix(nTemp) + 1
		End If
	
	Else
		Set mscsGetProductList = oCatalog.RootProducts
		nOutPagesTotal = 1
		nOutRecordsTotal = mscsGetProductList.RecordCount
	End If
End Function


' -----------------------------------------------------------------------------
' mscsGetSubCategoriesList
'
' Description:
'   This function gets the child categories for a Category object.
'   If the Category object is Nothing, then it gets the subcategories from
'   the root of the catalog
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsGetSubCategoriesList(ByVal oCatalog, ByVal oCategory)
	If Not oCategory Is Nothing Then
		Set mscsGetSubCategoriesList = oCategory.ChildCategories
	Else
		Set mscsGetSubCategoriesList = oCatalog.RootCategories(CATALOG_ROOT_CATEGORY_COLS)
	End If
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' EnsureUserHasRightsToCatalog
'
' Description:
'	Ensure that the user has rights to the given catalog.  Otherwise redirect to BadCatalogItem
'
' Parameters:
'	sCatalogName				- Name of the catalog to check
'	sUserID						- ID of the user, to check access rights
'
' Returns:
'
' Notes :
'   Considered a presentation function, because it may redirect
' -----------------------------------------------------------------------------
Sub EnsureUserHasRightsToCatalog(ByVal sCatalogName, ByVal sUserID)
	Dim ID
	
	If IsNull(sCatalogName) Then
		Response.Redirect(GenerateURL(MSCSSitePages.BadUrl, Array(), Array()))
	End If
	
	' Verify that sCatalogName is valid for the supplied userid
	ID = mscsUserCatalogsetID()
	If IsNull(MSCSCatalogSets.Value(ID)) Then
		Err.Raise HRESULT_E_FAIL, "", mscsMessageManager.GetMessage("L_InvalidCatalogSet_ErrorMessage", sLanguage)
	End If
	If IsNull(MSCSCatalogSets.Value(ID).Value(sCatalogName)) Then
		Response.Redirect(GenerateURL(MSCSSitePages.BadCatalogItem, Array(), Array()))
	End If
End Sub

' -----------------------------------------------------------------------------
' GetCatalogForUser
'
' Description:
'	Returns the catalog object of given catalog name, if the user
'	has rights to that catalog.  Otherwise redirect to BadCatalogItem
'
' Parameters:
'	sCatalogName				- Name of the catalog to get
'	sUserID						- ID of the user, to check access rights
'
' Returns:
'	A catalog object
' Notes :
'   Considered a presentation function, because it may redirect
' -----------------------------------------------------------------------------
Function GetCatalogForUser(ByVal sCatalogName, ByVal sUserID)
	Call EnsureUserHasRightsToCatalog(sCatalogName, sUserID)
	Set GetCatalogForUser = MSCSCatalogManager.GetCatalog(sCatalogName)
	
	If GetCatalogForUser Is Nothing Then
		Response.Redirect(GenerateURL(MSCSSitePages.BadCatalogItem, Array(), Array()))
	End If
End Function


' -----------------------------------------------------------------------------
' htmRenderCategoryPage
'
' Description:
'	Renders a category page.  A category page lists subcategories and products
'	within a category.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCategoryPage(ByVal oCatalog, ByVal sCatalogName, ByVal sCategoryName, ByVal oCategory, ByVal rsProducts, ByVal nPageNumber, ByVal nOutPagesTotal)
	Dim htmContent, urlLink
	Dim nRecordsTotal
	Dim rsChildCategories, bCategoriesFound
	Dim i

	' If the result set is larger than one page, show paging info..
	If (nOutPagesTotal > 1) Then
		htmContent = htmContent & FormatOutput(mscsMessageManager.GetMessage("L_Total_Pages_Found_HTMLText", sLanguage), Array(nPageNumber, nOutPagesTotal))
		For i = 1 to nOutPagesTotal
			urlLink = RenderCategoryUrl(sCatalogName, sCategoryName, i)
			htmContent = htmContent & RenderLink(urlLink, i, MSCSSiteStyle.Link) & NBSP
		Next
		htmContent = htmContent & CR & CR
	End If
		
	' If on page one, show the subcategories
	If nPageNumber = 1 Then
		Set rsChildCategories = mscsGetSubCategoriesList(oCatalog, oCategory)
		If Not rsChildCategories.EOF Then
			bCategoriesFound = True
			htmContent = htmContent & htmRenderCategoriesList(oCatalog, rsChildCategories, MSCSSiteStyle.Body) & CR
		End If
	End If

	' Show any products (using htmRenderProductsTable)
	If Not rsProducts.EOF Then
		htmContent = htmContent & htmRenderProductsTable( _
	                               oCatalog, _
	                               rsProducts, _
	                               sCategoryName _
	                           )				
	ElseIf Not bCategoriesFound Then
	    ' Category didn't contain any products or subcategories
	    htmContent = RenderText(mscsMessageManager.GetMessage("L_Empty_Catalog_Category_HTMLText", sLanguage), MSCSSiteStyle.Body)
	End If
	
	htmRenderCategoryPage = htmContent & CR
End Function


' -----------------------------------------------------------------------------
' htmRenderCategoriesList
'
' Description:
'   This function renders a recordset containing categories.
'   It provides links to each of the contained categories.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCategoriesList(ByVal oCatalog, ByVal rsCategories, ByVal style)
    Dim sProductCategory, sCatalogName
    Dim htmCategory
    Dim lstrTemp		
    
    If (rsCategories.EOF) Then
		lstrTemp = "" & CR
    Else
		sCatalogName = rsCategories.Fields(CATALOG_NAME_PROPERTY_NAME).Value
			
		lstrTemp = ""
		While Not rsCategories.EOF 
			sProductCategory = rsCategories.Fields(CATEGORY_NAME_PROPERTY_NAME).Value
			htmCategory = RenderCategoryLink(sCatalogName, sProductCategory, 1, style)
			lstrTemp = lstrTemp & htmCategory & CR
		    rsCategories.MoveNext
		Wend
    End If

	htmRenderCategoriesList = lstrTemp
End Function


' -----------------------------------------------------------------------------
' htmRenderProductsTable
'
' Description:
'   This function renders a recordset containing products
'   in a table format.  The table will have the following columns:
'		1) identifying property (i.e. sku),
'		2) name, 
'		3) description, and
'		4) price
'
' Parameters:
'
' Returns:
'
' Notes :
'	If the identifying property and name are the same, only 3 columns
'   are shown (no dupe)
' -----------------------------------------------------------------------------
Function htmRenderProductsTable(ByVal oCatalog, ByVal rsProducts, ByVal sCategoryName)
	Dim sCatalogName, sProductName, sProductDescription, sProductID
    Dim arrRows, arrAttLists	
    Dim cyProductPrice, cyCurrency
	Dim htmProduct, htmBody
	Dim bNameIsIdentifyingProperty
	
	sCatalogName = rsProducts.Fields(CATALOG_NAME_PROPERTY_NAME).Value

    ' Determine if the "name" property is the identifying product property
    bNameIsIdentifyingProperty = StrComp( _
											PRODUCT_NAME_PROPERTY_NAME, _
											oCatalog.IdentifyingProductProperty, _
											vbTextCompare _
										) = 0 
    
    ' Build the column headers depending on if "name" is the identifying property 
    If bNameIsIdentifyingProperty Then
        arrRows = Array( _
                                 mscsMessageManager.GetMessage("L_Product_Name_DisplayName_HTMLText", sLanguage), _
                                 mscsMessageManager.GetMessage("L_Product_Description_DisplayName_HTMLText", sLanguage), _
                                 mscsMessageManager.GetMessage("L_Product_Price_DisplayName_HTMLText", sLanguage) _
                             )
    Else 
        arrRows = Array( _
                                 oCatalog.IdentifyingProductProperty, _
                                 mscsMessageManager.GetMessage("L_Product_Name_DisplayName_HTMLText", sLanguage), _
                                 mscsMessageManager.GetMessage("L_Product_Description_DisplayName_HTMLText", sLanguage), _
                                 mscsMessageManager.GetMessage("L_Product_Price_DisplayName_HTMLText", sLanguage) _
                             )
    End If        
    
    htmBody = RenderTableHeaderRow(arrRows, Array(), MSCSSiteStyle.TRCenter)
    
	' Fill out colums for each row in the recordset
	While Not rsProducts.EOF
		' "name" and "description" are required product properties and cannot have null values.
		sProductName = rsProducts.Fields(PRODUCT_NAME_PROPERTY_NAME).Value
		sProductDescription = rsProducts.Fields(PRODUCT_DESCRIPTION_PROPERTY_NAME).Value
		
		sProductID = rsProducts.Fields(oCatalog.IdentifyingProductProperty).Value
		
		'	check for price on variant
		Call GetPriceAndCurrency(rsProducts, cyProductPrice, cyCurrency)
		
		htmProduct = RenderProductLink(sCatalogName, sCategoryName, sProductName, CStr(sProductID), MSCSSiteStyle.Body)
		                       
        ' Build the columns depending on if "name" is the identifying property 
        If bNameIsIdentifyingProperty Then
            arrRows = Array(htmProduct, sProductDescription, cyCurrency)
            arrAttLists = Array(MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDRight)
        Else
            arrRows = Array(sProductID, htmProduct, sProductDescription, cyCurrency)
            arrAttLists = Array(MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDRight)
        End If

        htmBody = htmBody & RenderTableDataRow(arrRows, arrAttLists, MSCSSiteStyle.TRMiddle)        
        rsProducts.MoveNext
	Wend

    htmRenderProductsTable = RenderTable(htmBody, MSCSSiteStyle.BorderedTable)
End Function


' -----------------------------------------------------------------------------
' htmRenderCatalogList
'
' Description:
'   This function renders a recordset of catalogs in a list format.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCatalogList(ByVal rsCatalogs, ByVal style)
	Dim sCatalogName
	Dim urlCatalog
	
	While Not rsCatalogs.EOF
		sCatalogName = rsCatalogs.Fields(CATALOG_NAME_PROPERTY_NAME).Value
		htmRenderCatalogList = htmRenderCatalogList & _
		                    RenderCatalogLink(sCatalogName, style) & _
		                    CR
		rsCatalogs.MoveNext
	Wend
End Function


' -----------------------------------------------------------------------------
' RenderSearchResults
'
' Description:
' This function renders a recordset containing search results.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderSearchResults(ByVal sPhrase, ByVal rsResult, ByVal iRecordCount, ByVal style)
	Dim htmResults	
	
	htmResults = RenderText(mscsMessageManager.GetMessage("L_SEARCH_PRODUCTS_CRITERIA_TEXT", sLanguage) & sPhrase, style) & BR
	
	If rsResult Is Nothing Then
		' None of the spec'd columns were found
		htmResults = htmResults & RenderText(mscsMessageManager.GetMessage("L_SEARCH_PRODUCTS_NOMATCH_TEXT", sLanguage), style) & CRLF
	ElseIf rsResult.EOF Then
		' Empty result set
		htmResults = htmResults & RenderText(mscsMessageManager.GetMessage("L_SEARCH_PRODUCTS_NOMATCH_TEXT", sLanguage), style) & CRLF
	Else
		htmResults = htmResults & RenderText(mscsMessageManager.GetMessage("L_SEARCH_PRODUCTS_RESULTS_TEXT", sLanguage) & CStr(iRecordCount), style) & CRLF

		While Not rsResult.EOF
            htmResults = htmResults & RenderSearchResultRow(rsResult, style) & BR
            rsResult.MoveNext
		Wend
	End If	
	RenderSearchResults = htmResults
End Function


' -----------------------------------------------------------------------------
' RenderSearchResultRow
'
' Description:
'	This function renders one row from a recordset containing search results.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderSearchResultRow(rsResult, style)
	Dim sProductID, sVariantID, sProductName, sDescription, sCategoryName, sCatalogName
	Dim iClassType, bIsProduct, htmResults

	sCatalogName = rsResult.Fields(CATALOG_NAME_PROPERTY_NAME).Value
    sCategoryName = rsResult.Fields(CATEGORY_NAME_PROPERTY_NAME).Value
    
    ' "name" and "description" are required product properties and cannot have null values.
    sProductName = rsResult.Fields(PRODUCT_NAME_PROPERTY_NAME).Value
    sDescription = rsResult.Fields(PRODUCT_DESCRIPTION_PROPERTY_NAME).Value
    
	iClassType = rsResult.Fields(DEFINITION_TYPE_PROPERTY_NAME).Value
	sProductID =  rsResult.Fields(CATALOG_PRODUCTID_PROPERTY_NAME).Value
	sVariantID =  rsResult.Fields(CATALOG_VARIANTID_PROPERTY_NAME).Value
		
	' Ensure the ProductName property is defined when rendering products
	bIsProduct = (iClassType = cscProductClass) Or _
				 (iClassType = cscProductVariantClass) Or _
				 (iClassType = cscProductFamilyClass)
			
	' Render the search result row, depending on iClassType
	If iClassType = cscProductClass Then
	    htmResults = RenderText(FormatOutput(LABEL_TEMPLATE, Array(mscsMessageManager.GetMessage("L_Product_HTMLText", sLanguage))), style) &_
					 RenderProductLink(sCatalogName, CATALOG_SEARCH_CATEGORY_NAME, sProductName, sProductID, style) & _
					 " -- " & _
					 RenderText(sDescription, style)
			    
	ElseIf iClassType = cscProductVariantClass Then
		htmResults = RenderText(FormatOutput(LABEL_TEMPLATE, Array(mscsMessageManager.GetMessage("L_Variant_HTMLText", sLanguage))), style) &_
		             RenderProductLink(sCatalogName, CATALOG_SEARCH_CATEGORY_NAME, sProductName, sProductID, style) & _
					 " -- " & _
					 RenderText(sDescription, style)			
	
	ElseIf iClassType = cscCategoryClass Then
	    htmResults = RenderText(FormatOutput(LABEL_TEMPLATE, Array(mscsMessageManager.GetMessage("L_Category_HTMLText", sLanguage))), style) &_
	                 RenderCategoryLink(sCatalogName, sCategoryName, 1, style) & _
					 " -- " & _
					 RenderText(sDescription, style)
			
	ElseIf iClassType = cscProductFamilyClass Then
		htmResults = RenderText(FormatOutput(LABEL_TEMPLATE, Array(mscsMessageManager.GetMessage("L_Family_HTMLText", sLanguage))), style) &_
		             RenderProductLink(sCatalogName, CATALOG_SEARCH_CATEGORY_NAME, sProductName, sProductID, style) & _
					 " -- " & _
					 RenderText(sDescription, style)
	Else
		' Unknown class type
	End If 
	  
	RenderSearchResultRow = htmResults
End Function


' -----------------------------------------------------------------------------
' URL Rendering Functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' RenderCategoryURL
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderCategoryURL(sCatalogName, sCategory, nPage)
	Dim sURL
	sURL = GenerateURL(MSCSSitePages.Category, Array(CATALOG_NAME_URL_KEY, CATEGORY_NAME_URL_KEY, PAGENUMBER_URL_KEY), Array(sCatalogName, sCategory, nPage))
	RenderCategoryURL = sURL
End Function 


' -----------------------------------------------------------------------------
' RenderProductURL
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderProductURL(sCatalogName, sProductCategory, sProductID)
	Dim sURL
	sURL = GenerateURL(MSCSSitePages.Product, Array(CATALOG_NAME_URL_KEY, CATEGORY_NAME_URL_KEY, PRODUCT_ID_URL_KEY), Array(sCatalogName, sProductCategory, CStr(sProductID)))
	RenderProductURL = sURL
End Function


' -----------------------------------------------------------------------------
' RenderCatalogURL
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderCatalogURL(sCatalogName)
	RenderCatalogURL = RenderCategoryURL(sCatalogName, "", 1)
End Function


' -----------------------------------------------------------------------------
' Link tag Rendering Functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' RenderCategoryLink
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderCategoryLink(sCatalogName, sCategory, nPage, style)
	Dim sURL, htmLinkText
	sURL = RenderCategoryURL(sCatalogName, sCategory, nPage)
	htmLinkText =  RenderText(sCategory, style)
	RenderCategoryLink = RenderLink(sURL, htmLinkText, MSCSSiteStyle.Link)
End Function 
	

' -----------------------------------------------------------------------------
' RenderProductLink
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderProductLink(sCatalogName, sProductCategory, sProductName, sProductID, style)
	Dim sURL, htmLinkText
	sURL = RenderProductURL(sCatalogName, sProductCategory, sProductID)
	htmLinkText = RenderText(sProductName, style)
	RenderProductLink = RenderLink(sURL, htmLinkText, MSCSSiteStyle.Link)
End Function


' -----------------------------------------------------------------------------
' RenderCatalogLink
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderCatalogLink(sCatalogName, style)
	Dim sURL, htmLinkText
	sURL = RenderCatalogURL(sCatalogName)
	htmLinkText = RenderText(sCatalogName, style)
	RenderCatalogLink = RenderLink(sURL, htmLinkText, MSCSSiteStyle.Link)
End Function	
%>
