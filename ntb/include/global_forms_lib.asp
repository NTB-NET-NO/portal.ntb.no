<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_forms_lib.asp
' Global Initialization library with Forms functions; used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' dictGetPropertySchema
'
' Description:
' Function to create a dictionary for property attributes
'     These attributes are used in the application for 
'     1)Standard Form Validation 
'     2)Rendering functions
'     3)Profile Updations
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictGetPropertySchema (ByVal oGroup, ByVal oProperty, _
								ByRef oProfileDefsCatalog, ByRef oSiteTermsProfile)
    Const PROFILE_PROPERTY_SMALLINT = 2
    Const PROFILE_PROPERTY_NUMBER   = 3
    Const PROFILE_PROPERTY_DATETIME = 7
    Const PROFILE_PROPERTY_PASSWORD = 130
    Const PROFILE_PROPERTY_STRING   = 130
    Const PROFILE_PROPERTY_GROUP    = 132
        
    Const SITE_PROPERTY_TEXTBOX       = 0    
    Const SITE_PROPERTY_LISTBOX       = 1
    Const SITE_PROPERTY_PASSWORD      = 2
    Const SITE_PROPERTY_HIDDEN        = 3
    Const SITE_PROPERTY_CHECKBOX      = 4
    Const SITE_PROPERTY_COMBOBOX      = 5
    Const SITE_PROPERTY_DATETIME      = 7
    Const SITE_PROPERTY_NUMBER        = 8
    Const SITE_PROPERTY_REFERENCE     = 9
    Const SITE_PROPERTY_SITETERM	  = 10
    Const SITE_PROPERTY_BOOLEAN		  = 11

    Const PROPERTY_DISPLAY_SIZE       = "32"
    Const PROPERTY_MIN_LENGTH         = "1"

    Dim dictProperty
    Dim oAttribute
    Dim dictAttributes

    Set dictProperty = GetDictionary()
    Set dictAttributes = GetDictionary()

    ' Add attributes exported by the profile service to the property
    '     property dictionary
    For Each oAttribute In oProperty.Properties
        ' Add the dictionary of all attributes for this property
        dictAttributes(oAttribute.Name) = oAttribute.Value
    Next
    Set dictProperty.Attributes = dictAttributes
    
    ' Add attributes used for rendering of this property
    dictProperty.Name = oProperty.Name
    dictProperty.IsGroup = False
    dictProperty.GroupName = oGroup.Name
    dictProperty.QualifiedName = oGroup.Name & "." & oProperty.Name
    dictProperty.DefinedSize = oProperty.DefinedSize
    dictProperty.Type = oProperty.Type

    ' Add attributes to be used for standard form validation routines
    ' //
    dictProperty.Label     = oProperty.Properties("DisplayName").Value
    dictProperty.Size      = PROPERTY_DISPLAY_SIZE
    dictProperty.MaxLength = CStr(oProperty.DefinedSize)
    dictProperty.MinLength = PROPERTY_MIN_LENGTH
    
    If (dictProperty.Attributes.IsJoinKey = True) Then    
        ' Treat the JOIN KEY properties as HIDDEN_FIELD in application
        dictProperty.InputType = SITE_PROPERTY_HIDDEN
        dictProperty.IsRequired = "False"
        Set dictProperty.Options = Nothing
    Else
        ' Map the profile service property types to the one used by
        '     applications and for standard form validation routines
		If (dictProperty.Type <> PROFILE_PROPERTY_GROUP) Then _
			dictProperty.IsRequired = oProperty.Properties("IsRequired").Value

        Select Case dictProperty.Type
            Case PROFILE_PROPERTY_GROUP
                dictProperty.InputType = SITE_PROPERTY_HIDDEN
            Case PROFILE_PROPERTY_NUMBER
				' Should check for "referenceString" attribute
				' The only indication of a link to a SiteTerm
                Set dictProperty.Options = oGetSiteTermOptions(oProperty.Name, _
												oProfileDefsCatalog, oSiteTermsProfile)
				If (dictProperty.Options Is Nothing) Then
					dictProperty.InputType = SITE_PROPERTY_COMBOBOX
				Else
					dictProperty.InputType = SITE_PROPERTY_SITETERM
				End If
            Case PROFILE_PROPERTY_DATETIME
                dictProperty.InputType = SITE_PROPERTY_DATETIME
			Case PROFILE_PROPERTY_SMALLINT
				dictProperty.InputType = SITE_PROPERTY_BOOLEAN
            Case Else
				' Should check for "referenceString" attribute
				' The only indications of a link to a SiteTerm
                Set dictProperty.Options = oGetSiteTermOptions(oProperty.Name, _
												oProfileDefsCatalog, oSiteTermsProfile)
                If (dictProperty.Options Is Nothing) Then
					dictProperty.InputType = SITE_PROPERTY_TEXTBOX
				Else
					dictProperty.InputType = SITE_PROPERTY_SITETERM
				End If
        End Select
    End If    

    Set dictGetPropertySchema = dictProperty
End Function


' -----------------------------------------------------------------------------
' GetFormDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'	GetMessageManagerObject() must run before this routine does
' -----------------------------------------------------------------------------
Function GetFormDefinitions()
	Dim dictForms
	
	Set dictForms = GetDictionary()
	Set dictForms.Value("Login") = GetLoginFieldDefinitions()
	Set dictForms.Value("Registration") = GetRegistrationFieldDefinitions()	
	Set dictForms.Value("BillToAddress") = GetAddressFieldDefinitions()
	Set dictForms.Value("ShipToAddress") = GetShipToAddressFieldDefinitions()
	Set dictForms.Value("AddressBookAddress") = GetAddressBookAddressFieldDefinitions()

	Set dictForms.Value("CreditCard") = GetCreditCardFieldDefinitions()
	Set dictForms.Value("PurchaseOrder") = GetPurchaseOrderFieldDefinitions()

	Set GetFormDefinitions = dictForms
End Function


' -----------------------------------------------------------------------------
' GetPurchaseOrderFieldDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetPurchaseOrderFieldDefinitions()
	Dim MSCSMessageManager
	Dim sLanguage
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

    Dim listFlds, dictFld
    
    Set listFlds = GetSimpleList()

    Set dictFld = GetDictionary()
    dictFld.Name = "po_number"
    dictFld.Label = MSCSMessageManager.GetMessage("L_PurchaseOrderNumber_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MaxLength = L_Standard_TextBox_MaxLength_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_Bad_PurchaseOrder_Number_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Call listFlds.Add(dictFld)

	' Add a billing currency field if alternative currency may be used.
	If dictConfig.i_AltCurrencyOptions = ALTCURRENCY_DISPLAY_ENABLED Then
		Call listFlds.Add(GetBillToCurrencyFieldDefinition())           		
	End If
	
    Set GetPurchaseOrderFieldDefinitions = listFlds
End Function


' -----------------------------------------------------------------------------
' GetLoginFieldDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   For best results, keep the regex pattern the same as BusinessDesk and the Service Desk
' -----------------------------------------------------------------------------
Function GetLoginFieldDefinitions()
	Dim MSCSMessageManager
	Dim sLanguage
    Dim listFlds, dictFld
	
	' Login fields size constraints
	Const L_Login_UserName_MaxLength_Number = 127
	Const L_Login_UserName_MinLength_Number = 1

	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage
    
    Set listFlds = GetSimpleList()
    
    Set dictFld = GetDictionary()
    dictFld.Name = LOGON_NAME
    dictFld.Label = MSCSMessageManager.GetMessage("L_Login_UserName_Label_HTMLText", sLanguage)
	dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MaxLength = L_Login_UserName_MaxLength_Number
    dictFld.MinLength = L_Login_UserName_MinLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = FormatOutput(MSCSMessageManager.GetMessage("L_Bad_Login_UserName_ErrorMessage", sLanguage), Array(L_Login_UserName_MinLength_Number, L_Login_UserName_MaxLength_Number))
    dictFld.IsRequired = True
    dictFld.Pattern = "^[^?*/""|:<>=+;,.\\\)(#[\]&^/]*"
    dictFld.PatternErrorMessage = MSCSMessageManager.GetMessage("L_Bad_UserName_Pattern_ErrorMessage", sLanguage)
    dictFld.RawInput = True  ' This signifies we want to get <,>, and " -- which will be rejected by Pattern
    Call listFlds.Add(dictFld)
    
    Set dictFld = GetPasswordFieldDefinition()
    dictFld.Name = LOGON_PASSWORD
    dictFld.Label = MSCSMessageManager.GetMessage("L_Login_Password_Label_HTMLText", sLanguage)
    dictFld.IsRequired = True
    Call listFlds.Add(dictFld)    

    Set GetLoginFieldDefinitions = listFlds
End Function    


' -----------------------------------------------------------------------------
' GetPasswordFieldDefinition
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   For best results, keep the regex pattern the same as BusinessDesk and the Service Desk
' -----------------------------------------------------------------------------
Function GetPasswordFieldDefinition()
	Dim MSCSMessageManager
	Dim sLanguage
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

	' Password field size constraints
	Const L_PasswordMaxLength_Number = 14
	Const L_PasswordMinLength_Number = 7

	Dim dictFld
	
    Set dictFld = GetDictionary()
    dictFld.Size = L_Standard_TextBox_Size_Number    
    dictFld.MaxLength = L_PasswordMaxLength_Number
    dictFld.MinLength = L_PasswordMinLength_Number
    dictFld.InputType = PASSWORD
    dictFld.Pattern = "^[^<>""&]*"  ' Note: when using AD, we use a more restrictive mask
    dictFld.PatternErrorMessage = MSCSMessageManager.GetMessage("L_Bad_Password_Pattern_ErrorMessage", sLanguage)
    dictFld.RawInput = True  ' This signifies we want to get <,>, and " -- which will be rejected by Pattern
    dictFld.ErrorMessage = FormatOutput(MSCSMessageManager.GetMessage("L_BadPassword_ErrorMessage", sLanguage), Array(L_PasswordMinLength_Number, L_PasswordMaxLength_Number))
    
	Set GetPasswordFieldDefinition = dictFld
End Function


' -----------------------------------------------------------------------------
' GetRegistrationFieldDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetRegistrationFieldDefinitions()
	Dim MSCSMessageManager
	Dim sLanguage
	
	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage

    Dim listFlds, dictFld
    
    Set listFlds = GetLoginFieldDefinitions()
    
    Set dictFld = GetPasswordFieldDefinition()
    dictFld.Name = CONFIRM_PASSWORD
    dictFld.Label = MSCSMessageManager.GetMessage("L_Login_ConfirmPassword_Label_HTMLText", sLanguage)
    dictFld.IsRequired = True
    Call listFlds.Add(dictFld)        

    Set GetRegistrationFieldDefinitions = listFlds
End Function
</SCRIPT>