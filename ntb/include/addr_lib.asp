<%
' =============================================================================
' Addr_lib.asp
' Library of functions that deal with AddressBook addresses
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetUserAddresses
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetUserAddresses(ByVal sUserID)
	Set GetUserAddresses = GetAddresses(sUserID)			
End Function


' -----------------------------------------------------------------------------
' GetOrganizationAddresses
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetOrganizationAddresses(ByVal sOrgID)
	Set GetOrganizationAddresses = GetAddresses(sOrgID)
End Function


' -----------------------------------------------------------------------------
' GetAddresses
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   Uses ADO to access the db directly
' -----------------------------------------------------------------------------
Function GetAddresses(ByVal sAddrParentID) 
	Dim rsAddr
	Dim sqlStmt, sSelectList, sTableSource, sSearchCondition, sOrderExpression

	sSelectList = _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_ADDRESS_ID))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_FRIENDLY_NAME))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_FIRST_NAME))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_LAST_NAME))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_ADDRESS1))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_ADDRESS2))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_CITY))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_REGION_NAME))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_POSTAL_CODE))) & ", " & _
					FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_COUNTRY_CODE)))
	
	sTableSource = PROFILE_TYPE_ADDRESS

	sSearchCondition = _
		FormatOutput( _
			KEYVALUE_TEMPLATE, _
			Array( _
				FormatOutput( _
					BRACKETED_TEMPLATE, _
					Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_ID)) _
				), _
				FormatOutput(SINGLE_QUOTE_TEMPLATE, Array(sAddrParentID)) _	
			) _
		)
			
	sOrderExpression = FormatOutput(BRACKETED_TEMPLATE, Array(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_ADDRESS_FRIENDLY_NAME)))

	sqlStmt = FormatOutput( _
			"SELECT [GeneralInfo.address_id], [GeneralInfo.address_name], [GeneralInfo.first_name], [GeneralInfo.last_name], [GeneralInfo.address_line1], [GeneralInfo.address_line2], [GeneralInfo.city], [GeneralInfo.region_name], [GeneralInfo.postal_code], [GeneralInfo.country_code] FROM Address WHERE [GeneralInfo.id]='%1' ORDER BY [GeneralInfo.address_name]", _
			Array( _
					sAddrParentID _
				 ) _
		)
			 
	Set rsAddr = Server.CreateObject("ADODB.Recordset") 
	rsAddr.Open sqlStmt, MSCSAdoConnection

	Set GetAddresses = rsAddr
End Function


' -----------------------------------------------------------------------------
' GetAddressType
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAddressType()
	' Default to shipping address if the address type is not specified.
	GetAddressType = MSCSAppFrameWork.RequestNumber(ADDRESS_TYPE, SHIPPING_ADDRESS)
	
	' Default to shipping address if the specified address type is invalid.
	If (GetAddressType <> SHIPPING_ADDRESS) And (GetAddressType <> BILLING_ADDRESS) Then
		GetAddressType = SHIPPING_ADDRESS
	End If
End Function

' -----------------------------------------------------------------------------
' IsZipCodeValid
'
' Description:
'	IsZipCodeValid checks that zip code entered conforms to one of the
'	following formats:
'	ddddd-dddd  e.g. 98052-0000
'	ddddddddd   e.g. 980520000
'	ddddd dddd  e.g. 98052 0000
'	ddddd-      e.g. 98052-
'	ddddd       e.g. 98052
'	You may add further validation logic to IsZipCodeValid to ensure zip code
'	entered is in existence or that it matches the state entered by the user.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsZipCodeValid(sZipCode)	
	Dim oRegEx

	IsZipCodeValid = False
	
	If Not IsNull(sZipCode) Then	
		Set oRegEx = Server.CreateObject("VBScript.RegExp")			
		oRegEx.Pattern = "^(\d{5})[ -]?(\d{4})$|^(\d{5})[ -]?$" 
		If oRegEx.Test(sZipCode) Then
			IsZipCodeValid = True
		End If
	End If			
End Function


' -----------------------------------------------------------------------------
' LookupRegion
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function LookupRegion(ByVal sRegion, sCountryCode)
	If IsNull(sRegion) Then
		LookupRegion = Null
	Else
		' First assume user entered a region name (as opposed to region code).
		'	Verify that the region name is a valid one.
		If mscsAppConfig.GetRegionCodeFromCountryCodeAndRegionName(sCountryCode, sRegion) <> "" Then
			LookupRegion = sRegion
		Else
			' If first assumption didn't work, then assume user entered a region code. 
			'	Call GetRegionNameFromCountryCodeAndRegionCode to obtain the region name from
			'	region code. Note that the method returns the region code supplied to the call 
			'	(instead of null) if it fails to match the region code with a region name. 
			'	Since a region name may be identical to a region code, we cannot conclude from 
			'	the return value alone that lookup has failed. Therefore we need to call
			'	mscsAppConfig.GetRegionCodeFromCountryCodeAndRegionName with the return value
			'	from the first call since this method will return "" if it fails to match the
			'	region name with a region code.
			LookUpRegion = mscsAppConfig.GetRegionNameFromCountryCodeAndRegionCode(sCountryCode, sRegion)
			
			If StrComp(LookUpRegion, sRegion, vbTextCompare) = 0 Then
				If mscsAppConfig.GetRegionCodeFromCountryCodeAndRegionName(sCountryCode, LookUpRegion) = "" Then
					LookupRegion = Null
				End If
			End If
		End If
	End If
End Function
%>
