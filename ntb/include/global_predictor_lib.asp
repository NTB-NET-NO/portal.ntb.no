<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_predictor_lib.asp
' Global Initialization library with Predictor functions; used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitPredictor
'
' Description:
'	Initialize Predictor Client Component
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitPredictor()
	Dim objPredictor
	Dim connstr_db_dw
	Const strModelName = "Transactions"

	connstr_db_dw = GetDWConnectionString()
	If connstr_db_dw = "" Then
		Set InitPredictor = Nothing
		Exit Function
	End If

	Set objPredictor = Server.CreateObject("Commerce.PredictorClient")

	' Load the model from the data warehouse
	On Error Resume Next

	objPredictor.LoadModelFromDB strModelName, connstr_db_dw
	If Err.Number <> 0 Then
		' No model found.
		Set objPredictor = Nothing
		Set InitPredictor = Nothing
		Exit Function
	End If
	
	On Error Goto 0

	' The popularity penality weights the predictions
	' against always recommending the most popular products.
	' 
	' The higher the value, the greater the penalty.
	' 0.0 = No penalty.
	' 1.0 = Maximum penalty.

	objPredictor.fpPopularityPenalty = 0.8

	If MSCSEnv = DEVELOPMENT Then
		' In development mode, you may want to set this property,
		' which causes an error to be generated if any
		' of the input attributes in the case is not recognized.  
		' 
		' By default, unknown input attributes are ignored.
		' 
		' This is useful for debugging because it help detect
		' errors in the formatting of the input case which could
		' cause the predictor to always recommend the same products.
		Rem
		' However, in production mode it is not recommended because
		' it will generate an error if, for example, a new product
		' is after after the model was last built. 
		Rem
		' objPredictor.bFailOnUnknownInputAttributes = True
	End If

	' The confidence threshold sets a lower bound on how
	' confident the predictor needs to be in order to return
	' a recommendation.
	' 0.0 = Do not restrict based on confidence.
	' 100.0 = Don't return any predictions.

	objPredictor.fpDefaultConfidence = 10

	Set InitPredictor = objPredictor		
End Function
</SCRIPT>