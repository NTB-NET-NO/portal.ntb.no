<%
' =============================================================================
' std_ordergrp_lib.asp
' Standard library with Order group functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetOrderGroup
'
' Description:
'	Returns an initialized OrderGroup object.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetOrderGroup(ByVal sOrderID)
	Dim mscsOrderGrp
	
	Set mscsOrderGrp = Server.CreateObject("Commerce.OrderGroup")
	Call mscsOrderGrp.Initialize(dictConfig.s_TransactionsConnectionString, sOrderID)
	Set GetOrderGroup = mscsOrderGrp
End Function


' -----------------------------------------------------------------------------
' GetLineItemsCount
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetLineItemsCount(ByVal mscsOrderGrp)
	Dim iCount, sOrderFormName
	
	GetLineItemsCount = 0
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		GetLineItemsCount = GetLineItemsCount + mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName).Items.Count
	Next
End Function


' -----------------------------------------------------------------------------
' LoadBasket
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function LoadBasket(ByVal sUserID)
	Dim mscsOrderGrp

	Set mscsOrderGrp = GetOrderGroup(sUserID)
	Call mscsOrderGrp.LoadBasket()
	Set LoadBasket = mscsOrderGrp
End Function


' -----------------------------------------------------------------------------
' GetAnyLineItem
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAnyLineItem(mscsOrderGrp)
	Dim sOrderFormName
	Dim mscsOrderForm
	
	For Each sOrderFormName In mscsOrderGrp.Value.OrderForms
		Set mscsOrderForm = mscsOrderGrp.Value.OrderForms(sOrderFormName)
		If mscsOrderForm.Items.Count > 0 Then
			Set GetAnyLineItem = mscsOrderForm.Items(0)
			Exit Function
		End If
	Next 
End Function


' -----------------------------------------------------------------------------
' GetAnyOrderForm
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAnyOrderForm(mscsOrderGrp)
	Dim sOrderFormName
	
	For Each sOrderFormName In mscsOrderGrp.Value.OrderForms
		Set GetAnyOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		Exit For
	Next 
End Function


' -----------------------------------------------------------------------------
' SetKeyOnOrderForms
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SetKeyOnOrderForms(ByVal mscsOrderGrp, ByVal sKey, ByVal sValue)
	Dim sOrderFormName
	Dim mscsOrderForm
	
	For Each sOrderFormName In mscsOrderGrp.Value.OrderForms
		Call mscsOrderGrp.PutOrderFormValue(sKey, sValue, sOrderFormName)
	Next 
End Sub


' -----------------------------------------------------------------------------
' GetErrorCount
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetErrorCount(ByVal mscsOrderGrp, ByVal sCollectionName)            
	Dim oOrderForm, sOrderFormName
	For Each sOrderFormName in mscsOrderGrp.Value(ORDERFORMS)
		Set oOrderForm =  mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		GetErrorCount = GetErrorCount + oOrderForm.Value(sCollectionName).Count
	Next
End Function


' -----------------------------------------------------------------------------
' RemoveEmptyOrderForms
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RemoveEmptyOrderForms(mscsOrderGrp)
	Dim sOrderFormName, mscsOrderForm	
		
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		Set mscsOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		If mscsOrderForm.Items.Count = 0 Then
			Call mscsOrderGrp.RemoveOrderForm(sOrderFormName)
		End If
	Next
End Sub


' -----------------------------------------------------------------------------
' GetOrderGroupManager
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetOrderGroupManager()
	Dim mscsOrderGrpMgr
	
	Set mscsOrderGrpMgr = Server.CreateObject("Commerce.OrderGroupManager")
	Call mscsOrderGrpMgr.Initialize(dictConfig.s_TransactionsConnectionString)
	Set GetOrderGroupManager = mscsOrderGrpMgr
End Function


' -----------------------------------------------------------------------------
' AddDiscountMessages
'
' Description:
'	This function adds a footnote symbol for each discount applied to each
'   line item.  The footnote symbol will be "D" + the campaign id
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub AddDiscountMessages(mscsOrderGrp)
	Dim oItem, oOrderForm, sOrderFormName, ciid
	For Each sOrderFormName In mscsOrderGrp.Value.Orderforms
		Set oOrderForm = mscsOrderGrp.Value.OrderForms.Value(sOrderFormName)
		For Each oItem In oOrderForm.Items
			If IsObject(oItem.discounts_applied) Then
				For Each ciid In oItem.discounts_applied
					oItem.Value("_messages") = oItem.Value("_messages") & "D" & CStr(ciid) & NBSP
				Next
			Else
				oItem.Value("_messages") = NBSP
			End If
		Next
	Next
End Sub
%>