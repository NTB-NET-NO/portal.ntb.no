var ascChrFile = "up.gif";	// Image for ascending sort
var desChrFile = "down.gif";	// Image for descending sort
var ascChr = "&uarr;";		// Symbol for ascending sort
var desChr = "&darr;";		// Symbol for descending sort
var isIE;				// True if IE
var linkEventString =			// What's insider <a> tag
	'onMouseOver=\'setCursor(this);\' ' +
	'onMouseOut=\'resetStatuswindow();\'';
var colSortTypes;
var bExcludeLastRowInSort;
// This function is used to setup sorting on a table's headers
function initTable(id, colTypes, initColumn, excludeLastRowInSort, doSortOnInit) {
	// Check whether it's viewed by IE 5.0 or greater
	if (! checkBrowser()) return;

	// Tell if the last row should be included in the sort.	
	bExcludeLastRowInSort = excludeLastRowInSort;
	
    var table = document.getElementById(id);
    var thead= table.getElementsByTagName("thead")[0];
    var cols = thead.getElementsByTagName("th");    
    if (doSortOnInit == null) {
        doSortOnInit = 0;
    }

    colSortTypes = colTypes;    // loop through each header and attach an onclick event
    for (i=0; i < cols.length; i++) {
        if (colTypes[i] != null) {
		cols[i].innerHTML = '<A ' + linkEventString + '>' + cols[i].innerHTML + '</A>';
		cols[i].onclick = function() {sortTable(this,colTypes[i])};
        } else {
		// Add events when moving over the heading.
		cols[i].innerHTML = '<A ' + linkEventString + '>' + cols[i].innerHTML + '</A>';
		// Add event when clicking on the heading.		cols[i].onclick = function() {sortTable(this)};        }
        // set sort indicator on first column        if (i==initColumn) {
            // Don't set indicator if we should sort on init.
	    if (doSortOnInit != 1) {
                setIndicator(cols[i], true);                sortedOn = initColumn;
            }
        }
    }    // Sort the table on init.
    if (doSortOnInit == 1) {
        sortTable(cols[initColumn]);
        sortTable(cols[initColumn]);
    }
}

var sortedOn = 0;
var lastSorted = "";

function sortTable(e, type) {
    // figure out which column this is in the table, and the table
    if (e.parentNode) {
	    table = e.parentNode.parentNode.parentNode;
        sortOn = e.cellIndex;
	} else if (e.parentElement) {
	    table = e.parentElement.parentElement.parentElement;
        sortOn = e.cellIndex;
	}
	
    //var table = document.getElementById(tableId);
    var tbody = table.getElementsByTagName('tbody')[0];
    var rows = tbody.getElementsByTagName('tr');

    var rowArray = new Array();
    var length;
    if (bExcludeLastRowInSort)
    	length=rows.length-1;
    else
    	length=rows.length;
    
    for (var i=0; i<length; i++) {
        rowArray[i] = rows[i].cloneNode(true);
    }
    
    type = colSortTypes[sortOn];
    if (sortOn == sortedOn) { 
        rowArray.reverse(); 
        lastSorted = (lastSorted == "asc" || lastSorted == "") ? "desc" : "asc";
    } else {
        lastSorted = "asc";
        sortedOn = sortOn;
        if (type == 'number') {
            rowArray.sort(rowCompareNumbers);
        } else if (type == "dollar") {
            rowArray.sort(rowCompareDollars);
        } else {
            rowArray.sort(rowCompare);
        }
    }
            
    var newTbody = document.createElement('tbody');
    for (var i=0, length=rowArray.length; i<length; i++) {
        newTbody.appendChild(rowArray[i]);
    }
    if (bExcludeLastRowInSort)
        newTbody.appendChild(rows[rows.length-1].cloneNode(true));
    
    table.replaceChild(newTbody, tbody);
    
    // get all the cells in the thead and set the class on the sorted column
    var thead = table.getElementsByTagName('thead')[0];
    var cells = thead.getElementsByTagName('th');
    for (var i=0; i < cells.length; i++) {
        cells[i].className = "";    
        var spans = cells[i].getElementsByTagName('img');
        for (var j=0; j < spans.length; j++) {
            cells[i].removeChild(spans[j]);
        }        // Remove old arrow if any.
	if (cells[i].innerHTML.charAt(cells[i].innerHTML.length - 1) != '>')		cells[i].innerHTML = cells[i].innerHTML.substr(0, cells[i].innerHTML.length - 1);
    }
    setIndicator(e);
}

function rowCompare(a, b) {

	// Since some cells may have tags we'll do this.
	// Should maybe be done more robust.	if (isNull(a.getElementsByTagName('td')[sortedOn].firstChild.nodeValue))		var aVal = a.getElementsByTagName('td')[sortedOn].firstChild.innerHTML.toUpperCase();
	else		var aVal = a.getElementsByTagName('td')[sortedOn].firstChild.nodeValue.toUpperCase();	if (isNull(b.getElementsByTagName('td')[sortedOn].firstChild.nodeValue))		var bVal = b.getElementsByTagName('td')[sortedOn].firstChild.innerHTML.toUpperCase();	else
		var bVal = b.getElementsByTagName('td')[sortedOn].firstChild.nodeValue.toUpperCase();
    return (aVal == bVal ? 0 : (aVal > bVal ? 1 : -1));
}

function rowCompareNumbers(a, b) {	if (isNull(a.getElementsByTagName('td')[sortedOn].firstChild.nodeValue))
		var aVal = parseInt(a.getElementsByTagName('td')[sortedOn].firstChild.innerHTML);
	else
		var aVal = parseInt(a.getElementsByTagName('td')[sortedOn].firstChild.nodeValue);
	if (isNull(b.getElementsByTagName('td')[sortedOn].firstChild.nodeValue))
		var bVal = parseInt(b.getElementsByTagName('td')[sortedOn].firstChild.innerHTML);
	else
		var bVal = parseInt(b.getElementsByTagName('td')[sortedOn].firstChild.nodeValue);

    return (aVal - bVal);
}

function rowCompareDollars(a, b) {
	if (isNull(a.getElementsByTagName('td')[sortedOn].firstChild.nodeValue))
		var aVal = parseFloat(a.getElementsByTagName('td')[sortedOn].firstChild.innerHTML.substr(1));
	else
		var aVal = parseFloat(a.getElementsByTagName('td')[sortedOn].firstChild.nodeValue.substr(1));
	if (isNull(b.getElementsByTagName('td')[sortedOn].firstChild.nodeValue))
		var bVal = parseFloat(b.getElementsByTagName('td')[sortedOn].firstChild.innerHTML.substr(1));
	else
		var bVal = parseFloat(b.getElementsByTagName('td')[sortedOn].firstChild.nodeValue.substr(1));

    return (aVal - bVal);
}

function setIndicator(e, pageLoad) {
    if (pageLoad) {
        lastSorted = "asc";
    }
    e.className = "sorted";    e.innerHTML = e.innerHTML + ((lastSorted == "asc") ? ascChr : desChr);
    var indicator = document.createElement("img");
//    indicator.setAttribute("src", lastSorted + ".gif");
//    indicator.setAttribute("width", "9");
//    indicator.setAttribute("height", "10");
    indicator.setAttribute("width", "0");
    indicator.setAttribute("height", "0");
    indicator.setAttribute("alt", (lastSorted == "asc") ? "Ascending" : "Descending");    indicator.className = "sortIndicator";    e.appendChild(indicator);
}
function setCursor(obj)
{
	var rowText, reRowText;
	
	reRowText = /(\< *[^\>]*\>|\&nbsp\;)/g;
	// Show hint text at the browser status bar
	rowText = String(obj.innerHTML);

	// Remove up/down 
	rowText = rowText.replace(/\<font id\=updown.*\<\/font\>/ig, "");
	// Remove HTML tags and &nbsp;
	rowText = rowText.replace(reRowText, "");
	// Remove carriage return, linefeed, and tab
	rowText = rowText.replace(/\r|\n|\t/g, "");
	
	// Setting window's status bar
	window.status = "Klikk for � sortere p� " + String(rowText);

	// Setting title
	obj.title = "Klikk for � sortere p� " + String(rowText);

	// Change the mouse cursor to pointer or hand
	if (isIE)
		obj.style.cursor = "hand";
	else
		obj.style.cursor = "pointer";
}

//*****************************************************************************
// Function to reset the status window
//*****************************************************************************
function resetStatuswindow()
{
	window.status = '';
}

//*****************************************************************************
// Function to check browser type/version
//*****************************************************************************
function checkBrowser()
{
	if (navigator.appName == "Microsoft Internet Explorer"
		&& parseInt(navigator.appVersion) >= 4)
	{
		isIE = true;
		return true;
	}
	// For some reason, appVersion returns 5 for Netscape 6.2 ...
	else if (navigator.appName == "Netscape"
		&& navigator.appVersion.indexOf("5.") >= 0)
	{
		isIE = false;
		return true;
	}
	else
		return false;
}

//*****************************************************************************
// Some type helping functions
//*****************************************************************************
function isAlien(a) {
   return isObject(a) && typeof a.constructor != 'function';
}
function isArray(a) {
    return isObject(a) && a.constructor == Array;
}
function isBoolean(a) {
    return typeof a == 'boolean';
}
function isEmpty(o) {
    var i, v;
    if (isObject(o)) {
        for (i in o) {
            v = o[i];
            if (isUndefined(v) && isFunction(v)) {
                return false;
            }
        }
    }
    return true;
}
function isFunction(a) {
    return typeof a == 'function';
}
function isNull(a) {
    return typeof a == 'object' && !a;
}
function isNumber(a) {
    return typeof a == 'number' && isFinite(a);
}
function isObject(a) {
    return (a && typeof a == 'object') || isFunction(a);
}
function isString(a) {
    return typeof a == 'string';
}
function isUndefined(a) {
    return typeof a == 'undefined';
} 
