<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_creditcards_lib.asp
' Global Initialization library with Creditcard functions; used by
' global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetCreditCardFieldDefinitions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCreditCardFieldDefinitions()
	Dim MSCSMessageManager
	Dim sLanguage
    Dim listFlds, dictFld
	
	' Credit card fields size constraints
	Const L_CreditCardNumber_TextBox_MaxLength_Number = 19
	Const L_CreditCardNumber_TextBox_MinLength_Number = 13	

	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage
	
    Set listFlds = GetSimpleList()

    Set dictFld = GetDictionary()
    dictFld.Name = CC_NAME
    dictFld.Label = MSCSMessageManager.GetMessage("L_CreditCardName_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MaxLength = L_CreditCardName_TextBox_MaxLength_Number
    dictFld.MinLength = L_Standard_TextBox_MinLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_Bad_CreditCard_Name_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
    Call listFlds.Add(dictFld)
    
    Set dictFld = GetDictionary()
    dictFld.Name = CC_NUMBER
    dictFld.Label = MSCSMessageManager.GetMessage("L_CreditCardNumber_HTMLText", sLanguage)
    dictFld.Size = L_Standard_TextBox_Size_Number
    dictFld.MaxLength = L_CreditCardNumber_TextBox_MaxLength_Number
    dictFld.MinLength = L_CreditCardNumber_TextBox_MinLength_Number
    dictFld.InputType = TEXTBOX
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_Bad_CreditCard_Number_ErrorMessage", sLanguage)
    dictFld.IsRequired = True   
    Call listFlds.Add(dictFld)     

    Set dictFld = GetDictionary()
    dictFld.Name = CC_TYPE
    dictFld.Label = MSCSMessageManager.GetMessage("L_CreditCardType_HTMLText", sLanguage)
    dictFld.Size = 1
    dictFld.InputType = LISTBOX
    Set dictFld.ListItems = GetCreditCardNamesList()
	Set dictFld.ListValues = GetCreditCardCodesList()    
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_Bad_CreditCard_Type_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
	Call listFlds.Add(dictFld)     

    Set dictFld = GetDictionary()
    dictFld.Name = CC_MONTH
    dictFld.Label = MSCSMessageManager.GetMessage("L_CreditCardMonth_HTMLText", sLanguage)
    dictFld.Size = 1
    dictFld.InputType = LISTBOX
    Set dictFld.ListItems = GetMonthNamesList()
    Set dictFld.ListValues = GetMonthCodesList()    
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_Bad_CreditCard_Month_ErrorMessage", sLanguage)
    dictFld.IsRequired = True
	Call listFlds.Add(dictFld)           

    Set dictFld = GetDictionary()
    dictFld.Name = CC_YEAR
    dictFld.Label = MSCSMessageManager.GetMessage("L_CreditCardYear_HTMLText", sLanguage)
    dictFld.Size = 1
    dictFld.InputType = LISTBOX
    Set dictFld.ListItems = GetYearsListItems()
    Set dictFld.ListValues = GetYearsListValues()     
    dictFld.ErrorMessage = MSCSMessageManager.GetMessage("L_Bad_CreditCard_Year_ErrorMessage", sLanguage)
    dictFld.IsRequired = True    
	Call listFlds.Add(dictFld)           
	
	' Add a billing currency field if alternative currency may be used.
	If dictConfig.i_AltCurrencyOptions = ALTCURRENCY_DISPLAY_ENABLED Then
		Call listFlds.Add(GetBillToCurrencyFieldDefinition())           
	End If
	
    Set GetCreditCardFieldDefinitions = listFlds
End Function


' -----------------------------------------------------------------------------
' GetCreditCardNamesList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCreditCardNamesList()
	Dim MSCSMessageManager
	Dim sLanguage
    Dim listItems

	Set MSCSMessageManager = Application("MSCSMessageManager")
	sLanguage = MSCSMessageManager.DefaultLanguage
    
	Set listItems = GetSimpleList()
    Call listItems.Add(MSCSMessageManager.GetMessage("L_SelectOne_Text", sLanguage))
    Call listItems.Add(MSCSMessageManager.GetMessage("L_AmericanExpress_Text", sLanguage))
    Call listItems.Add(MSCSMessageManager.GetMessage("L_Discover_Text", sLanguage))
    Call listItems.Add(MSCSMessageManager.GetMessage("L_MasterCard_Text", sLanguage))
    Call listItems.Add(MSCSMessageManager.GetMessage("L_VISA_Text", sLanguage))

    Set GetCreditCardNamesList = listItems
End Function    


' -----------------------------------------------------------------------------
' GetCreditCardCodesList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCreditCardCodesList()
    Dim listVals
    
	Set listVals = GetCreditCardNamesList()
	listVals(0) = "-1"

    Set GetCreditCardCodesList = listVals
End Function    
</SCRIPT>