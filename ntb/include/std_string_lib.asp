<%
' =============================================================================
' std_string_lib.asp
' Standard library with string handling functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' BooleanToString
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function BooleanToString(sValue)
    Select Case sValue
        Case "-1"
            BooleanToString = mscsMessageManager.GetMessage("L_STEPSEARCH_BOOLEAN_TRUE_TEXT", sLanguage)
        Case "0"
            BooleanToString = mscsMessageManager.GetMessage("L_STEPSEARCH_BOOLEAN_FALSE_TEXT", sLanguage)        
        Case Else
            BooleanToString = sValue
    End Select
End Function


' -----------------------------------------------------------------------------
' StringToBoolean
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function StringToBoolean(sValue)
    Select Case sValue
        Case mscsMessageManager.GetMessage("L_STEPSEARCH_BOOLEAN_TRUE_TEXT", sLanguage)
            StringToBoolean = 1
        Case mscsMessageManager.GetMessage("L_STEPSEARCH_BOOLEAN_FALSE_TEXT", sLanguage)
            StringToBoolean = 0
    End Select
End Function


' -----------------------------------------------------------------------------
' Space
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function Space(ByVal iSize)
	Dim iCount

	For iCount = 1 To iSize
		Space = Space & "&nbsp;"
	Next
End Function


' -----------------------------------------------------------------------------
' RemoveBlankItemsFromArray
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RemoveBlankItemsFromArray(ByRef arrItems, ByRef arrNew)
	Dim sItem
	Dim iCount

	ReDim arrNew(-1)

	For Each sItem In arrItems
		If (sItem <> "") Then
			ReDim Preserve arrNew(iCount)
			arrNew(iCount) = sItem
			iCount = iCount + 1
		End If
	Next
End Sub


' -----------------------------------------------------------------------------
' sChopOffRight
'
' Description:
'	If string sValue ends in string sToChopOffRight, chop off that right string
'
' Parameters:
'	sValue					- the string that can end in sToChopOffRight
'	sToChopOffRight			- the string to be chopped off of sValue if it's
'								there
'
' Returns:
'	String
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sChopOffRight(sValue, sToChopOffRight)
	Dim sResult
	
	If Len(sValue) = Len(sToChopOffRight) Then
		If (sValue = sToChopOffRight) Then
			sResult = ""
		End If
	ElseIf Len(sValue) > Len(sToChopOffRight) Then
		If Right(sValue, Len(sToChopOffRight)) = sToChopOffRight Then
			sResult = Left(sValue, Len(sValue) - Len(sToChopOffRight))
		Else
			sResult = sValue
		End If
	Else
		sResult = sValue
	End If
	sChopOffRight = sResult
End Function


' -----------------------------------------------------------------------------
' sChopOffLeft
'
' Description:
'	If string sValue starts with string sToChopOffLeft, chop off that left
'	string
'
' Parameters:
'	sValue					- the string that can start with sToChopOffLeft
'	sToChopOffLeft			- the string to be chopped off of sValue if it's
'								there
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sChopOffLeft(sValue, sToChopOffLeft)
	Dim sResult
	
	If Len(sValue) = Len(sToChopOffLeft) Then
		If (sValue = sToChopOffLeft) Then
			sResult = ""
		End If
	ElseIf Len(sValue) > Len(sToChopOffLeft) Then
		If Left(sValue, Len(sToChopOffLeft)) = sToChopOffLeft Then
			sResult = Right(sValue, Len(sValue) - Len(sToChopOffLeft))
		Else
			sResult = sValue
		End If
	Else
		sResult = sValue
	End If
	sChopOffLeft = sResult
End Function
%>
