<%
' =============================================================================
' html_lib.asp
' HTML Formatting Helper Functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' RenderText
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderText(ByVal str, ByVal sAttList)
	RenderText = "<FONT" & sAttList & ">" & str & "</FONT>"
End Function


' -----------------------------------------------------------------------------
' RenderTextBox
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderTextBox(ByVal sName, ByVal sValue, ByVal iSize, ByVal iMaxLength, ByVal sAttList)
	RenderTextBox = "<INPUT TYPE=""TEXT"" NAME=""" & sName & """ VALUE=""" & sValue & """ SIZE=""" & iSize & """ MAXLENGTH=""" & iMaxLength & """" & sAttList & ">"
End Function


' -----------------------------------------------------------------------------
' RenderPasswordBox
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderPasswordBox(ByVal sName, ByVal sValue, ByVal iSize, ByVal iMaxLength, ByVal sAttList)
	RenderPasswordBox = "<INPUT TYPE=""PASSWORD"" NAME=""" & sName & """ VALUE=""" & sValue & """ SIZE=""" & iSize & """ MAXLENGTH=""" & iMaxLength & """" & sAttList & ">"
End Function


' -----------------------------------------------------------------------------
' RenderLink
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderLink(ByVal url, ByVal htmText, ByVal sAttList)
	RenderLink = "<A HREF=""" & url & """" & sAttList & ">" & htmText & "</A>"
End Function 


' -----------------------------------------------------------------------------
' RenderCheckBox
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderCheckBox(ByVal sName, ByVal sValue, ByVal bChecked, ByVal sAttList)
	RenderCheckBox = "<INPUT TYPE=""CHECKBOX"" NAME=""" & sName & """ VALUE=""" & sValue & """"

	If bChecked Then
		RenderCheckBox = RenderCheckBox & " CHECKED"
	End If
	
	RenderCheckBox = RenderCheckBox & sAttList & ">"
End Function


' -----------------------------------------------------------------------------
' RenderRadioButton
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderRadioButton(ByVal sName, ByVal sValue, ByVal bChecked, ByVal sAttList)
	RenderRadioButton = "<INPUT TYPE=""RADIO"" NAME=""" & sName & """ VALUE=""" & sValue & """"

	If bChecked Then
		RenderRadioButton = RenderRadioButton & " CHECKED"
	End If
	
	RenderRadioButton = RenderRadioButton & sAttList & ">"
End Function


' -----------------------------------------------------------------------------
' RenderHiddenField
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderHiddenField(ByVal sName, ByVal sValue)
	RenderHiddenField = "<INPUT TYPE=""HIDDEN"" NAME=""" & sName & """ VALUE=""" & sValue & """>"	
End Function


' -----------------------------------------------------------------------------
' RenderSubmitButton
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderSubmitButton(ByVal sBtnName, ByVal sBtnText, ByVal sAttList)
	RenderSubmitButton = "<INPUT TYPE=""SUBMIT"" NAME=""" & sBtnName & """ VALUE=""" & sBtnText & """" & sAttList & ">"	
End Function


' -----------------------------------------------------------------------------
' RenderResetButton
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderResetButton(ByVal sBtnName, ByVal sBtnText, ByVal sAttList)
	RenderResetButton = "<INPUT TYPE=""RESET"" NAME=""" & sBtnName & """ VALUE=""" & sBtnText & """" & sAttList & ">"		
End Function


' -----------------------------------------------------------------------------
' RenderImageButton
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderImageButton(ByVal sImageName, ByVal sImageSource, ByVal sAttList)
	RenderImageButton = "<INPUT TYPE=""IMAGE"" " & "NAME=""" & sImageName & """ " & "SRC=""" & sImageSource & """" & sAttList & ">"
End Function


' -----------------------------------------------------------------------------
' RenderForm
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderForm(ByVal urlAction, ByVal htmBody, ByVal iHTTPMethod)
	Dim sHTTPMethod
	
	If iHTTPMethod = 1 Then
		sHTTPMethod = "POST"
	Else
		sHTTPMethod = "GET"
	End If
	
	RenderForm = "<FORM ACTION=""" & urlAction & """ METHOD=""" & sHTTPMethod & """>" & htmBody & "</FORM>"
End Function	


' -----------------------------------------------------------------------------
' RenderListBox
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'	This is a private function.
' -----------------------------------------------------------------------------
Function RenderListBox(ByVal iSource, ByVal sName, colItemNames, colItemValues, ByVal sSelectedOption, ByVal iSize, ByVal bMultiple, ByVal sAttList)
	Dim htmOptions, sKey, i

	Select Case iSource
		Case SIMPLELIST_COLLECTION
			If colItemNames.Count <> colItemValues.Count Then
				RenderListBox = -1
				Exit Function
			Else
				For i = 0 To colItemNames.Count - 1
					If StrComp(sSelectedOption, colItemValues(i), vbTextCompare) = 0 Then
						htmOptions = htmOptions & "<OPTION SELECTED VALUE=""" & colItemValues(i) & """>" & colItemNames(i)
					Else
						htmOptions = htmOptions & "<OPTION VALUE=""" & colItemValues(i) & """>" & colItemNames(i)
					End If
				Next
			End If
		Case DICTIONARY_COLLECTION
			For Each sKey In colItemNames
				If StrComp(sSelectedOption, colItemNames.Value(sKey), vbTextCompare) = 0 Then
					htmOptions = htmOptions & "<OPTION SELECTED VALUE=""" & colItemNames.Value(sKey) & """>" & sKey
				Else
					htmOptions = htmOptions & "<OPTION VALUE=""" & colItemNames.Value(sKey) & """>" & sKey
				End If
			Next
		Case ARRAY_COLLECTION
			If UBound(colItemNames) <> UBound(colItemValues) Then
				RenderListBoxFromCollection = -1
				Exit Function
			Else
				For i = 0 To UBound(colItemNames)
					If StrComp(sSelectedOption, colItemValues(i), vbTextCompare) = 0 Then
						htmOptions = htmOptions & "<OPTION SELECTED VALUE=""" & colItemValues(i) & """>" & colItemNames(i)
					Else
						htmOptions = htmOptions & "<OPTION VALUE=""" & colItemValues(i) & """>" & colItemNames(i)
					End If
				Next
			End If
	End Select

	RenderListBox = "<SELECT NAME=""" & sName & """ SIZE=""" & iSize & """"

	If bMultiple Then
		RenderListBox = RenderListBox & " MULTIPLE" 
	End If	

	RenderListBox = RenderListBox & sAttList & ">" & htmOptions & "</SELECT>" 
End Function


' -----------------------------------------------------------------------------
' RenderListBoxFromSimpleList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderListBoxFromSimpleList(ByVal sName, listItemNames, listItemValues, ByVal sSelectedOption, ByVal iSize, ByVal bMultiple, ByVal sAttList)
	RenderListBoxFromSimpleList = RenderListBox(SIMPLELIST_COLLECTION, sName, listItemNames, listItemValues, sSelectedOption, iSize, bMultiple, sAttList)
End Function


' -----------------------------------------------------------------------------
' RenderListBoxFromArray
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderListBoxFromArray(ByVal sName, arrItemNames, arrItemValues, ByVal sSelectedOption, ByVal iSize, ByVal bMultiple, ByVal sAttList)
	RenderListBoxFromArray = RenderListBox(ARRAY_COLLECTION, sName, arrItemNames, arrItemValues, sSelectedOption, iSize, bMultiple, sAttList)
End Function


' -----------------------------------------------------------------------------
' RenderImage
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderImage(ByVal urlSource, ByVal iWidth, ByVal iHeight, ByVal sDesc, ByVal sAttList)
	    RenderImage = "<IMG SRC=""" & urlSource & """"

		If Not IsNull(iWidth) Then
			RenderImage = RenderImage & " WIDTH=""" & iWidth & """"
		End If

		If Not IsNull(iHeight) Then
			RenderImage = RenderImage & " HEIGHT=""" & iHeight & """"
		End If
		
		RenderImage = RenderImage & " ALT=""" & sDesc & """" & sAttList & ">"
End Function


' -----------------------------------------------------------------------------
' RenderUnorderedList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderUnorderedList(ByVal arrItem, ByVal sty)
	Dim htmItems
	Dim i, iCount
	
	iCount = UBound(arrItem) + 1
	
	For i = 0 To iCount - 1
		htmItems = htmItems & "<LI>" & RenderText(arrItem(i), sty) _
			& "</LI>"
	Next
	
	RenderUnorderedList = "<UL>" & htmItems & "</UL>"
End Function 


' -----------------------------------------------------------------------------
' RenderUnorderedListFromSimpleList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderUnorderedListFromSimpleList(listItems, ByVal sAttList)
	Dim sItem, htmList
	
	For Each sItem In listItems
		htmList = htmList & "<LI" & sAttList & ">"
		htmList = htmList & sItem & "</LI>"
	Next
	
	RenderUnorderedListFromSimpleList = htmList
End Function


' -----------------------------------------------------------------------------
' RenderListFromSimpleList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderListFromSimpleList(listItems, style)
	Dim sItem, htmList
	
	For Each sItem In listItems
		htmList = htmList & RenderText(sItem, style) & BR
	Next
	
	RenderListFromSimpleList = htmList
End Function


' -----------------------------------------------------------------------------
' RenderTable
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderTable(ByVal htmRows, ByVal sAttList)
	RenderTable = "<TABLE" & sAttList & ">" & htmRows & "</TABLE>"
End Function


' -----------------------------------------------------------------------------
' RenderTableHeaderRow
'
' Description:
'	RenderTableHeaderRow renders a table header row. Null and "" are acceptable
'	values in arrData. Note that if table cells have identical alignment
'	(center-alied, for example), there is no need to align cells indivi-
'	dually. Just specify the alignment for the row (sRowAttList) and set
'	arrDataAttLists to an empty array (Array()).
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderTableHeaderRow(arrData, arrDataAttLists, ByVal sRowAttList)
	RenderTableHeaderRow = RenderTableRow(arrData, arrDataAttLists, sRowAttList, HEADER_ROW)
End Function


' -----------------------------------------------------------------------------
' RenderTableDataRow
'	RenderTableDataRow renders a table data row. Null and "" are acceptable
'	values in arrData. Note that if table cells have identical alignment
'	(center-alinged, for example), there is no need to align cells indivi-
'	dually. Just specify the alignment for the row (sRowAttList) and set
'	arrDataAttLists to an empty array (Array()).
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderTableDataRow(arrData, arrDataAttLists, ByVal sRowAttList)
	RenderTableDataRow = RenderTableRow(arrData, arrDataAttLists, sRowAttList, DATA_ROW)
End Function


' -----------------------------------------------------------------------------
' RenderTableRow
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderTableRow(arrData, arrDataAttLists, ByVal sRowAttList, ByVal iRowType)
	Dim i, bErr
	
	bErr = False
	
	If UBound(arrDataAttLists) <> -1 Then
		If UBound(arrData) <> UBound(arrDataAttLists) Then
			RenderTableRow = "-1"
			bErr = True
		End If
	End If
	
	If Not bErr Then
		RenderTableRow = "<TR" & sRowAttList & ">"
		Select Case iRowType
			Case HEADER_ROW
				If UBound(arrDataAttLists) = -1 Then
					For i = 0 To UBound(arrData)
						If (arrData(i) = "") Or IsNull(arrData(i)) Then arrData(i) = NBSP
						RenderTableRow = RenderTableRow & "<TH>" & arrData(i) & "</TH>"
					Next
				Else
					For i = 0 To UBound(arrData)
						If (arrData(i) = "") Or IsNull(arrData(i)) Then arrData(i) = NBSP
						RenderTableRow = RenderTableRow & "<TH" & arrDataAttLists(i) & ">" & arrData(i) & "</TH>"
					Next
				End If
			Case DATA_ROW
				If UBound(arrDataAttLists) = -1 Then
					For i = 0 To UBound(arrData)
						If (arrData(i) = "") Or IsNull(arrData(i)) Then arrData(i) = NBSP
						RenderTableRow = RenderTableRow & "<TD>" & arrData(i) & "</TD>"
					Next
				Else
					For i = 0 To UBound(arrData)
						If (arrData(i) = "") Or IsNull(arrData(i)) Then arrData(i) = NBSP
						RenderTableRow = RenderTableRow & "<TD" & arrDataAttLists(i) & ">" & arrData(i) & "</TD>"
					Next
				End If
		End Select
		RenderTableRow = RenderTableRow & "</TR>"
	End If
End Function


' -----------------------------------------------------------------------------
' RenderElement
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderElement(ByVal sElement, ByVal sContent, ByVal sAttList)
	RenderElement = "<" & sElement & sAttList & ">" & sContent & "</" & sElement & ">"
End Function


' -----------------------------------------------------------------------------
' GetRepeatStyle
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetRepeatStyle(ByVal iCount, ByVal sAttList)
	Dim i
	
	ReDim arrItems(iCount - 1)
	
	For i = 0 To UBound(arrItems)
		arrItems(i) = sAttList
	Next
	
	GetRepeatStyle = arrItems
End Function


' -----------------------------------------------------------------------------
' InsertLineBreaks
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InsertLineBreaks(ByVal arrLines)
	Dim i
	
	For i = LBound(arrLines) To UBound(arrLines)
		InsertLineBreaks = InsertLineBreaks & BRTag(arrLines(i))
	Next
End Function



' -----------------------------------------------------------------------------
' InsertParagraphBreaks
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InsertParagraphBreaks(ByVal arrLines)
	Dim i
	
	For i = LBound(arrLines) To UBound(arrLines)
		InsertParagraphBreaks = InsertParagraphBreaks & PTag(arrLines(i))
	Next
End Function


' -----------------------------------------------------------------------------
' InsertBlankSpace
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InsertBlankSpace(ByVal arrLines)
	Dim i
	
	For i = LBound(arrLines) To UBound(arrLines)
		InsertBlankSpace = InsertBlankSpace & " "
	Next

End Function


' -----------------------------------------------------------------------------
' Bold
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function Bold(ByVal s)
    Bold = "<B>" & s & "</B>"
End Function


' -----------------------------------------------------------------------------
' Italic
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function Italic(ByVal s)
    Italic = "<I>" & s & "</I>"
End Function


' -----------------------------------------------------------------------------
' BRTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function BRTag(s)
	BRTag = s & "<BR>"
End Function


' -----------------------------------------------------------------------------
' PTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function PTag(s)
	PTag = "<P>" & s & "</P>"
End Function


' -----------------------------------------------------------------------------
' RenderPreFormattedText
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderPreFormattedText(ByVal s, ByVal style)
	RenderPreFormattedText = Tag("PRE", s)
End Function


' -----------------------------------------------------------------------------
' Tag
'
' Description:
'	Encloses the text segment between a beginning and an end HTML tag
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function Tag(ByVal sTagName, ByVal htmText)
    Tag = "<" & UCase(sTagName) & ">" & htmText & "</" & _
        UCase(sTagName) & ">"
End Function
%>