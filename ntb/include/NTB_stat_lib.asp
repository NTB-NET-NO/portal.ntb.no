<%
'***********************************************************************************
' NTB_stat_lib.asp
'
' Support functions for statistical reports
'
' NOTES:
'		'none'
'
' CREATED BY: �yvind Andersen
' CREATED DATE: 2004.01.20
' UPDATED BY: 
' UPDATED DATE: 
' REVISION HISTORY:
'		'none'
'
'***********************************************************************************
Function AddStatGroup(strGroupName, strCompanyId)

	Dim sSqlStmt, cn, rs, lngId, lngRecordsAffected

	' Check if the group already exists	
	sSqlStmt = "SELECT * " _
			& "FROM ntb.dbo.REPORTCUSTOMERGROUP AS REPORTCUSTOMERGROUP " _
			& "WHERE Name = '" & strGroupName & "'"

	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sSqlStmt, Application("cns"), 2, 3
	
	if rs.BOF and rs.EOF then
		' Get MaxId for the insert. (Should be moved to sp when needed.)
		sSqlStmt = "SELECT Max(GroupId) AS GroupId FROM ntb.dbo.REPORTCUSTOMERGROUP"
		rs.Close
		rs.Open sSqlStmt, Application("cns") 
		if IsNull(rs.Fields("GroupId")) then
			lngId = 1
		else
			lngId = CLng(rs.Fields("GroupId")) + 1
		end if
		' Insert a new row.
		sSqlStmt = "INSERT INTO ntb.dbo.REPORTCUSTOMERGROUP VALUES(" & lngId & ", '" & strCompanyId & "', '" & strGroupName & "')"
		Set cn = CreateObject("ADODB.Connection")
		cn.Open Application("cns")
		cn.Execute sSqlStmt, lngRecordsAffected
	else
		' Update the existing row.
		rs.Fields("CompanyId") = strCompanyId
		rs.Update	
	end if
	rs.Close
	set rs = nothing

End Function

Function DeleteStatGroup(strGroupNameId)

	Dim sSqlStmt, cn, lngRecordsAffected, lngGroupId

	' Find the group id from the string. 
	' The string is build up like this. 
	' GroupId[CompanyId, CompanyId .....]
	lngGroupId = Left(strGroupNameId, instr(1, strGroupNameId, "[")-1)

	sSqlStmt = "DELETE ntb.dbo.REPORTCUSTOMERGROUP WHERE GroupId = " & lngGroupId
	Set cn = CreateObject("ADODB.Connection")
	cn.Open Application("cns")
	cn.Execute sSqlStmt, lngRecordsAffected

End Function

Function GetAllCompanyGroup()

	Dim sSqlStmt, rs
	
	sSqlStmt = "SELECT * " _
			& "FROM ntb.dbo.REPORTCUSTOMERGROUP AS REPORTCUSTOMERGROUP " _
			& "ORDER BY Name"

	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sSqlStmt, Application("cns")
	
	Set GetAllCompanyGroup = rs

End Function

' A function that return last month name based on current date
Function GetLastMonthName()

	Dim iMonth

	iMonth = Month(Date) - 1
	if iMonth = 0 then iMonth = 12
	
	GetLastMonthName = MonthName(iMonth)

End Function

' Function that returns a string with the dates of last week
Function GetLastWeekDates()

	Dim iDayOfWeek

	iDayOfWeek = DatePart("w", Date, 2, 2)
	GetLastWeekDates = CStr(Date - iDayOfWeek - 6) & "-" & CStr(Date - iDayOfWeek)
	
End Function

Function GetFirstDayOfMonth(intMonth)
	GetFirstDayOfMonth = DateSerial(Year(Date()), intMonth, 1)
End Function

Function GetLastDayOfMonth(intMonth)
	GetLastDayOfMonth = DateSerial(Year(Date()), intMonth+1, 0)
End Function

Function GetFirstDayOfLastMonthByDate(dteDate)

	Dim iMonth
	
	iMonth = Month(dteDate) - 1
	if iMonth = 0 then 
		' This is december last year, so decrement the year also.
		iMonth = 12
		GetFirstDayOfLastMonthByDate = DateSerial(Year(Date())-1, iMonth, 1)
	else
		GetFirstDayOfLastMonthByDate= GetFirstDayOfMonth(iMonth)
	end if

End Function

Function GetLastDayOfLastMonthByDate(dteDate)
	Dim iMonth

	iMonth = Month(dteDate) - 1
	if iMonth = 0 then 
		iMonth = 12
		' This is december last year, so decrement the year also.
		GetLastDayOfLastMonthByDate = DateSerial(Year(Date()) - 1, iMonth+1, 0)
	else
		GetLastDayOfLastMonthByDate = DateSerial(Year(Date()), iMonth+1, 0)
	end if

End Function

Function GetFirstDayOfYear(intYear)
	GetFirstDayOfYear = DateSerial(intYear, 1, 1)
End Function

Function GetFirstDayOfLastWeek(dteDate)

	Dim iDayOfWeek

	iDayOfWeek = DatePart("w", dteDate, 2, 2)
	GetFirstDayOfLastWeek = CDate(dteDate - iDayOfWeek - 6)

End Function

Function GetLastDayOfLastWeek(dteDate)

	Dim iDayOfWeek

	iDayOfWeek = DatePart("w", dteDate, 2, 2)
	GetLastDayOfLastWeek = CDate(dteDate - iDayOfWeek)

End Function
%>
