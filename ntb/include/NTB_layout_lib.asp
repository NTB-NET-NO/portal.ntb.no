<%
'***********************************************************************************
' NTB_layout_lib.asp
'
' HTML Formatting Helper Functions
'
' USES:
'		iframe.asp - For including iframes
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen
' CREATED DATE: 2002.04.09
' UPDATED BY: Solveig Skjermo, Andersen
' UPDATED DATE: 2002.06.14
' REVISION HISTORY:
' UPDATED 2003.03.19, By Roar Vestre, Added List "Logger"
' UPDATED 2003.06.03, By Roar Vestre, Added List "Direkte"
'
'***********************************************************************************
option explicit

' -----------------------------------------------------------------------------
' RenderTitleBar
'
' Description:	This renderfunction generates a title area displaying the page heading.
'				The title area can be displayed with or without a picture filter link.
'
' Parameters:	Title; name of the heading
'				PhotolinkOption; A parameter that is either PhotoLink or NoPhotoLink, and that
'				indicates if a photo filter link should be displayed.
'				width; default with
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderTitleBar(ByVal title, ByVal PhotoLinkOption, ByVal width)

	Dim PageID, tbstr
	PageID = "nopage"

	If Request("pageID")<>"" Then
		PageID = Request("pageID")
	End If

	tbstr = "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width="& width &" height=""50"">" _
					& "<tr height=""8"" align=""right""><td colspan=3 class='mini'>&#160;&nbsp;</td></tr>" _
					& "<tr height=""20"">" _
					& "<td class=""main_heading1"" width=""10"" height=""20"" align=""left"" bgcolor=""#003084"">" _
					& "&#160;</td>" _
					& "<td class=""main_heading1"" width=""633"" height=""20"" align=""left"" bgcolor=""#003084"">" _
					& "<p class=""main_heading1""><font color=""#FFFFFF""><span class=""main_heading1"">" & title & "</span></font></p>" _
					& "</td><td class=""main_heading1"" height=""20"" align=""right"" bgcolor=""#003084"">" _
					& "<img src=""../images/ntb_inv.gif"" border=""0"">&#160;&#160;</td></tr>"

	If (PhotoLinkOption = "PhotoLink") Then
		If Request("photoOption") <> "Yes" Then
			tbstr = tbstr & "<tr height=""10"" align=""right""><td colspan=3 class=""news""> Vis bare nyheter med <a href=""../mainpages/MainSectionPages.asp?pageID=" & PageID & "&photoOption=Yes""> bilde</a></td></tr></table>"
		Else
			tbstr = tbstr & "<tr height=""10"" align=""right""><td colspan=3 class=""news""> Vis <a href=""../mainpages/MainSectionPages.asp?pageID=" & PageID & """>alle</a> nyheter</td></tr></table>"
		End If
	Else
		tbstr = tbstr & "<tr height=""10"" align=""right""><td colspan=3 class=""news"" height=""10""> &#160;</td></tr></table>"
	End If

	RenderTitleBar = tbstr
End Function


' -----------------------------------------------------------------------------
' RenderIFrame
'
' Description:This renderfunction generates an iframe with specified content and size.
'
' Parameters: ListType: Type of news list to be displayed in the iframe. This type
'			  is used as the suffix for the corresponding iframe file
'			  SizeTypeHeight: Indicates the type of height to be used for the iframe.
'			  Should be one of the following types:
'			  height1, height2
'			  SizeTypeWidth: Indicates the type of width to be used for the iframe.
'			  Should be one of the following types:
'			  width2, width3
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderIFrame(ByVal ListType, ByVal SizeTypeWidth, ByVal SizeTypeHeight, ByVal PhotoOption, ByVal TimeOption, ByVal FrameNumber, ByVal intSakNr)

	Dim header
	header = "<div class=""main_heading2"">Oversikt</div>"

	Select Case ListType
		Case "alle_innenriks"
			'stoffgruppe Innenriks
			header = "<div class=""main_heading2"">Innenriks</div>"
		Case "alle_sport"
			'stoffgruppe Sport
			header = "<div class=""main_heading2"">Sport</div>"
		Case "alle_utenriks"
			'stoffgruppe Utenriks
			header = "<div class=""main_heading2"">Utenriks</div>"
		Case "innenriks_alle"
			'stoffgruppe Innenriks
			header = "<div class=""main_heading2"">Alle innenriksnyheter</div>"
		Case "innenriks_politikk"
			'stoffgruppe Innenriks hovedkategori Politikk
			header = "<div class=""main_heading2"">Politikk</div>"
		Case "innenriks_okonomi_naeringsliv"
			'stoffgruppe Innenriks hovedkategori "�konomi og n�ringsliv" og "Arbeidsliv"
			header = "<div class=""main_heading2"">�konomi og n�ringsliv</div>"
		Case "innenriks_krim_ulykker"
			'stoffgruppe Innenriks hovedkategori "Kriminalitet og rettsvesen" og "Ulykker og naturkatastrofer"
			header = "<div class=""main_heading2"">Krim og ulykker</div>"
		Case "utenriks_alle"
			'stoffgruppe Utenriks, except undergruppe "Bakgrunn" og "Analyse" og "Faktaboks" og "Oversikter"
			header = "<div class=""main_heading2"">Nyheter</div>"
		Case "utenriks_bakgrunn"
			'stoffgruppe Utenriks undergruppe "Bakgrunn" og "Analyse" og "Faktaboks" og "Oversikter" og "Menyer, til red"
			header = "<div class=""main_heading2"">Bakgrunn - Analyse - Fakta - Oversikt</div>"
		Case "okonomi_innenriks"
			'stoffgruppe Innenriks, category "�konomi og n�ringsliv"  and "Arbeidsliv"
			header = "<div class=""main_heading2"">Innenriks</div>"
		Case "okonomi_utenriks"
			'stoffgruppe Utenriks, category "�konomi og n�ringsliv"  and "Arbeidsliv"
			header = "<div class=""main_heading2"">Utenriks</div>"
		Case "sport_alle"
			'stoffgruppe Sport, except undergruppe "Tabeller og resultater"
			header = "<div class=""main_heading2"">Nyheter</div>"
		Case "sport_tabeller"
			'stoffgruppe Sport, with undergruppe "Tabeller og resultater"
			header = "<div class=""main_heading2"">Tabeller og resultater</div>"
		Case "kul_reportasjer"
			'stoffgruppe Kultur og underholdning, with undergruppe different from "Oversikter og notiser" and "Menyer, til red"
			header = "<div class=""main_heading2"">Reportasjer og nyheter</div>"
		Case "kul_notiser"
			'stoffgruppe Kultur og underholdning, with undergruppe "Oversikter og notiser"
			header = "<div class=""main_heading2"">Notiser</div>"
		Case "kul_showbiz"
			'stoffgruppe Kultur og underholdning, with undergruppe "Showbiz"
			header = "<div class=""main_heading2"">Showbiz</div>"
		Case "kul_menyer"
			'stoffgruppe Kultur og underholdning, with undergruppe "Menyer, til red"
			header = "<div class=""main_heading2"">Menyer</div>"
		Case "priv_menyer"
			'undergruppe "Menyer, til red" (no stoffgruppe) Helen 2103: Should not be messages with stoffgruppe Priv til red
			header = "<div class=""main_heading2"">Menyer</div>"
		Case "priv_tilred"
			'stoffgruppe Priv til red, except undergruppe "Menyer, til red" and "PRM-tjenesten" Helen 2103:Should incl undergruppe "Menyer, til red"
			header = "<div class=""main_heading2"">Priv. til red.</div>"
		Case "prm_ntbpluss"
			'stoffgruppe Formidlingstjenester, undergruppe "PRM-tjenesten"
			'header = "<table border=""0"" width=100% cellpadding=""0"" cellspacing=""0""class=""main_heading2""><tr><td>NTB Pluss    </td><td align=""left""><img src=""../images/ntbpluss.gif""></td></tr></table>"
			header = "<div class=""main_heading2"">NTB</div>"
		Case "prm_nwa"
			'stoffgruppe Formidlingstjenester, undergruppe "NWA"
			'header = "<table border=""0"" width=100% cellpadding=""0"" cellspacing=""0""class=""main_heading2""><tr><td>News Aktuell</td><td align=""left""><img src=""../images/nwa.gif""></td></tr></table>"
			header = "<div class=""main_heading2"">&nbsp;</div>"
		Case "prm_bwi"
			'stoffgruppe Formidlingstjenester, undergruppe "BWI"
			header = "<table border=""0"" width=100% cellpadding=""0"" cellspacing=""0""class=""main_heading2""><tr><td>Business Wire</td><td align=""left""><img src=""../images/bwi.gif""></td></tr></table>"
			'header = "<div class=""main_heading2"">Business Wire</div>"
		case "prm_obi"
			' Stoffgruppe Formidlingstjeneser, undergruppe "OBI"
			header = "<table border=""0"" width=100% cellpadding=""0"" cellspacing=""0""class=""main_heading2""><tr><td>Oslo B�rs</td><td align=""left"">&nbsp;</td></tr></table>"
		Case "logger"
			'Notabene folder: Interne Logger
			header = "<div class=""main_heading2"">Interne logger</div>"
		Case "direkte"
			'Notabene folder: Ut Direkte
			header = "<div class=""main_heading2"">NTB Direkte</div>"
		Case "pr_sounds"
			'stoffgruppe Formidlingstjenester, undergruppe "BWI"
			header = "<table width='95%'><tr><td rowspan=""3"" valign=""top"" style=""width:285px""><div class='main_heading2'>Siste ukes lydsaker</div></td><td><div class='hjelpetekst' align='left'>Sett mark�ren p� h�yttalersymbolet for � h�re lyden.</div></td></tr><tr><td><div class='hjelpetekst' align='left'>(Funksjonen er desverre ikke tilgjengelig i alle nettlesere.</div></td></tr><tr><td><div class='hjelpetekst' align='left'>Velg MP3, eller klikk <a href=""HelpSite.asp?anchor=SoundSearch"">her</a> for mer informasjon.)</div></td></tr></table>"
		Case "sak_alle"
			'SakID=intCaseId stoffgruppe Innenriks of Utenriks
			header = "<div class=""main_heading2"">F�lg saken</div>"
		Case "sak_bakgrunn"
			'KeyWordID=intKeyWordID undergruppe "Bakgrunn" og "Analyse" og "Faktaboks"
			header = "<div class=""main_heading2"">Bakgrunn - Analyse - Fakta</div>"
	End Select


	'logic for displaying frames for different screen resolutions
	Dim defaultHeight1, defaultHeight2, defaultHeight1s

	Dim lowRes 'parameter to indicate low or high screen resolution
	lowRes = False
	if LCase(CStr(Session("LowRes"))) = "true" then
	'a session variable is set on login indicating screen resolution
		lowRes = true
	end if


	If (lowRes = true) Then
		defaultHeight1 = 321
		defaultHeight1s = 220
		defaultHeight2 = 145
	Else
		defaultHeight1 = 473
		'height1s is used on the soundlistpage
		defaultHeight1s = 360
		defaultHeight2 = 221
	End If


	Dim frameHeight
	Dim frameWidth
	Dim sourceFile
	sourceFile = "../template/iframe.asp?listType=" & ListType & "&photoOption=" & PhotoOption  & "&SakNr=" & intSakNr

	If (SizeTypeHeight = "height1") Then
		frameHeight = defaultHeight1
	ElseIf (SizeTypeHeight = "height1s") Then
		frameHeight = defaultHeight1s
	Else
		frameHeight = defaultHeight2
	End If

	If (SizeTypeWidth = "width2") Then
		frameWidth = 291
	ElseIf (SizeTypeWidth = "width1") Then
		frameWidth = 600
	Else
		frameWidth = 188
	End If

	RenderIFrame = "<table bordercolor=""#61c7d7"" border=""1"" cellpadding=""2"" cellspacing=""3"" valign=""top"">" _
					& 	"<tr valign=""top"" align=""left"">" _
					&		"<td valign=""top"" style='border:none;'>" + header + "<IFRAME NAME=""Frame" & FrameNumber & """ FRAMEBORDER=""0"" WIDTH=""" & frameWidth & """ HEIGHT=""" & frameHeight & """ MARGINWIDTH=""0"" MARGINHEIGHT=""0"" SRC=""" & sourceFile & """></IFRAME></td>" _
					&	"</tr>" _
					& "</table>"

End Function

' -----------------------------------------------------------------------------
' TitleTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------

Function TitleTag()
	TitleTag = "<title>NTBs nyhetsportal</title>"
End Function
%>