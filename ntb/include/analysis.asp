<%
' =============================================================================
' Analysis.asp
' Analysis functions used by files like _additem, payment.asp, etc.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' Analysis_LogAddToBasket
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub Analysis_LogAddToBasket(ByVal sCatalogName, ByVal sCategoryName, ByVal sProductID, ByVal sVariantID)
	Dim sPRID
	sPRID = Analysis_ConstructPRID(sCatalogName, sCategoryName, sProductID, sVariantID)
	Call Analysis_LogEvent("BSK", "AIBSK", Array(sPRID))
End Sub


' -----------------------------------------------------------------------------
' Analysis_LogRemoveFromBasket
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub Analysis_LogRemoveFromBasket(ByVal sCatalogName, sCategoryName, ByVal sProductID, ByVal sVariantID)
	Dim sPRID
	sPRID = Analysis_ConstructPRID(sCatalogName, sCategoryName, sProductID, sVariantID)
	Call Analysis_LogEvent("BSK", "RIBSK", Array(sPRID))
End Sub


' -----------------------------------------------------------------------------
' Analysis_LogSubmitOrder
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub Analysis_LogSubmitOrder(ByVal sOrderID)
	Dim sORID
	sORID = Analysis_ConstructKeyValueString("ORID", sOrderID)
	Call Analysis_LogEvent("ORD", "SUBOR", Array(sORID))
End Sub


' -----------------------------------------------------------------------------
' Analysis_LogEvent
'
' Description:
'	This function logs a CEVT (Commerce Event) to the IIS log, of class sClass.
'   It will log any properties defined in rgKeyValueStrings for the event.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub Analysis_LogEvent(ByVal sClass,ByVal sEvent, ByVal rgKeyValueStrings)
	Dim sString, i

	sString = "&CEVT={T=" & sClass & ",EVT=" & sEvent
	
	For i=LBound(rgKeyValueStrings) to UBound(rgKeyValueStrings)
		sString = sString & "," & rgKeyValueStrings(i)
	Next
	
	sString = sString & "}"
	
	Response.AppendToLog sString
End Sub


' -----------------------------------------------------------------------------
' Analysis_ConstructKeyValueString
'
' Description:
'	This function constructs a key value string for Analysis_LogEvent.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function Analysis_ConstructKeyValueString(ByVal sKey, ByVal sValue)
	' Sanitize sValue
	sValue = Server.URLEncode(sValue)
	
	Analysis_ConstructKeyValueString = sKey & "=" & DoubleQuote(sValue)
End Function


' -----------------------------------------------------------------------------
' Analysis_ConstructPRID
'
' Description:
'	(Private)
'   This function constructs the PRID KeyValueString for AIBSK, RIBSK, AITP,
'	and RITP events
'   (Note sCategoryName is not logged, here for future reference...)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function Analysis_ConstructPRID(ByVal sCatalogName, sCategoryName, _
								ByVal sProductID, ByVal sVariantID)
	Dim sValue
	
	sValue = sCatalogName & ";" & sProductID 
		
	If Not IsNull(sVariantID) Then
		sValue = sValue & ";" &  sVariantID
	End If
	
	Analysis_ConstructPRID = Analysis_ConstructKeyValueString("PRID", sValue)
End Function	
%>
