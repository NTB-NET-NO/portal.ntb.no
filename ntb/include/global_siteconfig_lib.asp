<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_siteconfig_lib.asp
' Global Initialization library with Site Configuration functions; used by
' global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetINIString
'
' Description:
'	Returns the value of the INI key specified or "" if key or its value cannot
'	be found.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetINIString(ByVal sFileName, ByVal sKey)
	Dim oFSO, oTextStream
    Dim sLine, sValue
    
    sValue = ""

	' Open the config file for reading
	Set oFSO = GetFileSystemObject()
	
	Set oTextStream = oFSO.OpenTextFile(sFileName, ForReading, False, TristateUseDefault)
	
	sLine = oTextStream.ReadLine
	
	Do While Not oTextStream.AtEndOfStream
		sValue = GetKeyValue(sLine, sKey)
		If sValue <> "" Then
			Exit Do
		End If
		sLine = oTextStream.ReadLine
	Loop
	
	On Error Resume Next
	Call oTextStream.Close()
	Set oTextStream = Nothing
	Set oFSO = Nothing
	On Error Goto 0
	
	GetINIString = sValue
End Function


' -----------------------------------------------------------------------------
' GetINIFileName
'
' Description:
'	Returns the file name for the INI file containing site's configuration
'	information.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetINIFileName()
	GetINIFileName = GetRootPath & CONFIG_FILE
End Function


' -----------------------------------------------------------------------------
' GetRootPath
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetRootPath()
	Const WEBSITE_ROOT_DIRECTORY = "\"
	If appframework.VirtualDirectory = "" Then
		GetRootPath = Server.MapPath(WEBSITE_ROOT_DIRECTORY) & "\"
	Else
		' Commerce site is installed under a virtual directory.
		GetRootPath = Server.MapPath(WEBSITE_ROOT_DIRECTORY & appframework.VirtualDirectory) & "\"
	End If
End Function


' -----------------------------------------------------------------------------
' JoinWebFarm
'
' Description:
'	Joins the Webfarm [Different Webservers Running the same Application] if
'	not already in it.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub JoinWebFarm(ByRef iWebServerCount)
    Dim mscsSiteCfg, mscsSync
    Dim sWebServerMachine, iCurrWebServer
    Dim arrWebServerMachines, arrWebServerNames, arrWebServerInstances
    
    Set mscsSiteCfg = GetSiteConfigObject()
    sWebServerMachine = GetComputerName()
    
    arrWebServerMachines = mscsSiteCfg.MakeArrayFromString(mscsSiteCfg(MSCSCommerceAppName).Value.Fields(WEB_SERVER_MACHINE).Value)
    
	iWebServerCount = mscsSiteCfg(MSCSCommerceAppName).Value.Fields(WEB_SERVER_COUNT).Value
	
    ' Is the Web Server machine already in the Web farm?
    If Not IsEntityInArray(sWebServerMachine, arrWebServerMachines) Then
        If iWebServerCount > 0 Then
          iCurrWebServer = mscsSiteCfg(MSCSCommerceAppName).Value.Fields(CURRENT_WEB_SERVER).Value + 1
          arrWebServerNames = mscsSiteCfg.MakeArrayFromString(mscsSiteCfg(MSCSCommerceAppName).Value.Fields(WEB_SERVER_NAME).Value)
          arrWebServerInstances = mscsSiteCfg.MakeArrayFromString(mscsSiteCfg(MSCSCommerceAppName).Value.Fields(WEB_SERVER_INSTANCE).Value)
        Else
          Const L_BAD_NUMBEROFSERVERS_PROPERTYVALUE_ERRORMESSAGE = "Found an improper number of servers."
          Err.Raise vbObjectError + 2100, GetErrorSource("JoinWebFarm()"), FormatOutput(L_TermDefinition_Text, Array(L_Bad_NumberOfServers_PropertyValue_ErrorMessage, iWebServerCount))
        End If
        
        ReDim Preserve arrWebServerMachines(iWebServerCount)
        ReDim Preserve arrWebServerNames(iWebServerCount)
        ReDim Preserve arrWebServerInstances(iWebServerCount)

        arrWebServerMachines(iWebServerCount) = sWebServerMachine
        
        arrWebServerNames(iWebServerCount) = arrWebServerNames(0)
        
        arrWebServerInstances(iWebServerCount) = arrWebServerInstances(0)
		
		iWebServerCount = iWebServerCount + 1

        With mscsSiteCfg(MSCSCommerceAppName).Value
			.Fields(WEB_SERVER_MACHINE).Value = mscsSiteCfg.MakeStringFromArray(arrWebServerMachines)
			.Fields(WEB_SERVER_NAME).Value = mscsSiteCfg.MakeStringFromArray(arrWebServerNames)
			.Fields(WEB_SERVER_INSTANCE).Value = mscsSiteCfg.MakeStringFromArray(arrWebServerInstances)
			.Fields(CURRENT_WEB_SERVER).Value = iCurrWebServer
			.Fields(WEB_SERVER_COUNT).Value = iWebServerCount
		End With
        
        ' Update Admin Data with information about the new webserver added to the WebFarm
        Call mscsSiteCfg.SaveConfig()

        ' Syncs up Datawarehouse related information like LogFileDirectory , Directory index files 
        ' and the logfile type  etc so that the new webserver is in sync with the DataWarehousing 
        ' for the Application. 
        Set mscsSync = CreateObject("Commerce.PrivateDWMBToAdmin")
        Call mscsSync.Run(MSCSCommerceSiteName, MSCSCommerceAppName)
    End If
End Sub


' -----------------------------------------------------------------------------
' GetSiteConfigObject
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSiteConfigObject()
    Dim mscsSiteCfg

    Set mscsSiteCfg = CreateObject("Commerce.SiteConfig")
    Call mscsSiteCfg.Initialize(MSCSCommerceSiteName)
    Set GetSiteConfigObject = mscsSiteCfg
End Function


' -----------------------------------------------------------------------------
' GetComputerName
'
' Description:
'	Get the name of this computer.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetComputerName()
    Dim objWshShell, objEnv
    
	Set objWshShell = Server.CreateObject("Wscript.Shell")
	Set objEnv = objWshShell.Environment("Process")
	GetComputerName = objEnv("COMPUTERNAME")
End Function


' -----------------------------------------------------------------------------
' GetCommerceSiteName
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCommerceSiteName()
	GetCommerceSiteName = MSCSCommerceSiteName
End Function


' -----------------------------------------------------------------------------
' GetConfigDictionary
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetConfigDictionary()
	Dim mscsAppConfig
	
	Set mscsAppConfig = GetAppConfigObject()
	Set GetConfigDictionary = mscsAppConfig.GetOptionsDictionary("")  
End Function


' -----------------------------------------------------------------------------
' GetAppConfigObject
'
' Description:
'	Returns an initialized AppConfig object.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAppConfigObject()
	Dim mscsAppConfig, mscsSiteCfg
 	
 	' Is the AppConfig object stored in the Application object?
 	If Not IsObject(Application("MSCSAppConfig")) Then

 		' Create and initialize the AppConfig object.
 		Set mscsAppConfig = Server.CreateObject("Commerce.AppConfig")
		Call mscsAppConfig.Initialize(MSCSCommerceSiteName)	
		
		' Inject some special values into the configuration dictionary...
		Set dictConfig = mscsAppConfig.GetOptionsDictionary("")
		Set mscsSiteCfg = GetSiteConfigObject()	
		dictConfig.s_NonSecureHostName = mscsSiteCfg(Application("MSCSCommerceAppName")).Value.Fields("s_NonSecureHostName").Value
		dictConfig.s_SecureHostName = mscsSiteCfg(Application("MSCSCommerceAppName")).Value.Fields("s_SecureHostName").Value
		
		Set Application("MSCSAppConfig") = mscsAppConfig
		Set GetAppConfigObject = mscsAppConfig
	Else 
 		' Retrieve the AppConfig object from the Application object.
 		Set GetAppConfigObject = Application("MSCSAppConfig")
	End If
End Function


' -----------------------------------------------------------------------------
' GetSitePages
'
' Description:
'	This function assigns friendly names to pages in the sites
'	Friendly names are also used by PageSets and GetSecurePagesDictionary
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSitePages()
    Dim dictPages

    Set dictPages = GetDictionary()  

    ' Login pages
    dictPages.Login = "login/login.asp"    
	dictPages.GuestLogin = "login/_guest.asp" 'does not apply
	dictPages.LogOff = "login/logoff.asp"
    dictPages.NewUser = "login/newuser.asp" ' does not apply
	
	' Error pages
    dictPages.NoAuth = "error/noauth.asp"
    dictPages.BadURL = "error/badurl.asp"    
    dictPages.NoAccount = "error/profile.asp"    
    dictPages.ProfileMissing = "mainpages/login.asp"  'Send the user to login-window instead of giving an error message  
    dictPages.BadCatalogItem = "error/baditem.asp"    

	' Main pages
	dictPages.Welcome = "welcome.asp"    
    dictPages.Catalog = "default.asp"
    dictPages.Home = "default.asp" 
	dictPages.Category = "category.asp"
    dictPages.Product = "product.asp"
    dictPages.Search = "search.asp"
    dictPages.SearchableDepartments = "srchdept.asp"        
    dictPages.StepSearch = "stepsrch.asp"
    dictPages.OrderSummary = "summary.asp"        
    dictPages.Confirm = "confirm.asp"    
    
    ' Basket pages
    dictPages.Basket = "basket.asp"
    dictPages.AddItem = "_additem.asp"
    dictPages.DeleteItem = "_delitem.asp"
    dictPages.DeleteAllItems = "_delall.asp"
    dictPages.EditItemQuantities = "_editqty.asp"		 
    
	' Shipping and billing pages
	dictPages.Dispatch = "dispatch.asp"
	dictPages.CreditCard = "crdtcard.asp"
	dictPages.PurchaseOrder = "po.asp"
    dictPages.ShippingMethods = "pickship.asp"
    dictPages.SetShippingMethod = "_setship.asp"
    dictPages.AddressForm = "addrform.asp"
    dictPages.AddressBook = "addrbook.asp"
    dictPages.AddAddressToAddressBook = "newaddr.asp"
    dictPages.SetAddress = "_setadrs.asp"

	' For Partner/Customer service pages	
	Call AddPartnerAndCustomerServicePages(dictPages)
	
    Set GetSitePages = dictPages
End Function


' -----------------------------------------------------------------------------
' GetSecurePagesDictionary
'
' Description:
'	Any page requiring secure access must be on this dictionary. 
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSecurePagesDictionary()
    Dim dictPages

    Set dictPages = GetDictionary()  

    dictPages.Value(Application("MSCSSitePages").Value("Login")) = True
	dictPages.Value(Application("MSCSSitePages").Value("NewUser")) = True
	dictPages.Value(Application("MSCSSitePages").Value("Dispatch")) = True
	dictPages.Value(Application("MSCSSitePages").Value("CreditCard")) = True
	dictPages.Value(Application("MSCSSitePages").Value("PurchaseOrder")) = True
	dictPages.Value(Application("MSCSSitePages").Value("Confirm")) = True
	
    Set GetSecurePagesDictionary = dictPages
End Function


' -----------------------------------------------------------------------------
' InitPageSets
'
' Description:
'	PageSets are sets of related pages.  For instance you may have a pageset
'	comprised of the pages you wish to show an advertisement on.
'	PageSets can be used using the function IsEntityInSet
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitPageSets(MSCSSitePages)
	Dim dictSets

    Set dictSets = Server.CreateObject("Commerce.Dictionary")  
	
	dictSets.PurchasePageSet = CreateSet( _
											Array( _
													MSCSSitePages.Dispatch, _
													MSCSSitePages.CreditCard, _
													MSCSSitePages.PurchaseOrder, _
													MSCSSitePages.ShippingMethods, _
													MSCSSitePages.SetShippingMethod, _
													MSCSSitePages.AddressForm, _
													MSCSSitePages.AddressBook, _
													MSCSSitePages.AddAddressToAddressBook, _
													MSCSSitePages.SetAddress, _
													MSCSSitePages.OrderSummary, _
													MSCSSitePages.Confirm _
											) _
										)
		                            
	dictSets.ProductPageSet = CreateSet( _
	                            Array(MSCSSitePages.Product, _
	                                  MSCSSitePages.Category) _
	                           )

	dictSets.AdvertisementPageSet = CreateSet( _
									Array(MSCSSitePages.Catalog, _
										MSCSSitePages.Home, _
										MSCSSitePages.Category, _
										MSCSSitePages.Product, _
										MSCSSitePages.Search, _
										MSCSSitePages.SearchableDepartments, _
										MSCSSitePages.StepSearch, _
										MSCSSitePages.ShowBids) _
								)
	Set InitPageSets = dictSets
End Function


' -----------------------------------------------------------------------------
' GetBaseURL
'
' Description:
'   Returns the base of the urls for this application
' -----------------------------------------------------------------------------
Function GetBaseURL()
    GetBaseURL = "http://" & dictConfig.s_NonSecureHostname
    If GetCommerceSiteInstallPoint() = VIRTUAL_DIRECTORY Then
		GetBaseURL = GetBaseURL & "/" & appframework.VirtualDirectory
	End If
End Function	


' -----------------------------------------------------------------------------
' GetMachineBaseURL
'
' Description:
'   Returns the base of the urls for this application specifically for this server
' Notes :
'   If you wish to use bizdesk production refresh capabilities from a browser outside
'   the LAN, then this function must be changed so that it returns a machine-specific
'   url that works outside the lan. (ie www1.foobar.com or www2.foobar.com)
' -----------------------------------------------------------------------------
Function GetMachineBaseURL()
	Dim iPort, sPort, sWebServerMachine
	
	sWebServerMachine = GetComputerName()
	' We assume the port number is the same for all the Web servers on a Webfarm.
	iPort = iGetWebServerPort()
	
	If iPort <> 80 Then
		sPort = ":" & CStr(iPort)
	End If
    
    GetMachineBaseURL = "http://" 
    GetMachineBaseURL = GetMachineBaseURL & sWebServerMachine
    GetMachineBaseURL = GetMachineBaseURL & sPort
    
    If GetCommerceSiteInstallPoint() = VIRTUAL_DIRECTORY Then
		GetMachineBaseURL = GetMachineBaseURL & "/" & appframework.VirtualDirectory
    End If
End Function	


' -----------------------------------------------------------------------------
' iGetWebServerPort
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function iGetWebServerPort()
	Dim i, mscsSiteCfg
	Dim arrSeverBinding, sWebServerBindings, arrWebServerBindings
	
	Set mscsSiteCfg = GetSiteConfigObject()
	
	sWebServerBindings = mscsSiteCfg(MSCSCommerceAppName).Value.Fields("s_ServerBindings").Value
	arrWebServerBindings = mscsSiteCfg.MakeArrayFromString(sWebServerBindings)
	
	' We assume the port number is the same for all the Web servers on a Webfarm.
	' Server binding is returned in form of [IP number]:port number:host name
	arrSeverBinding = Split(arrWebServerBindings(0),":")
	
	For i = 0 To UBound(arrSeverBinding)
		If IsNumeric(arrSeverBinding(i)) Then 
			iGetWebServerPort = CInt(arrSeverBinding(i))
		End If
	Next
End Function


' -----------------------------------------------------------------------------
' IsActiveDirectoryPresent
'
' Description:
'	Function to check if Active Directory data store is present
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function IsActiveDirectoryPresent()
    Const sXMLTagCatalog = "Catalog"
    Const sXMLTagProfile = "Profile"
    Const g_sParentURL = "ProfileSystem.ParentDN"
    Const sXMLTagGroup = "Group"
    Const sXMLAttrName = "name"
    Const sXMLTagProperty = "Property"
    Const CURR_PROFILE = "Profile Definitions.UserObject"

    Dim bSupported
    Dim bVal
    Dim oXMLProp
    Dim oBizDataManager
    Dim oXMLDoc
    Dim aGroups
    Dim sWhere
    Dim i

    Set oBizDataManager = Server.CreateObject("Commerce.BusinessDataAdmin")
    Call oBizDataManager.Connect(dictConfig.s_BizDataStoreConnectionString)    
    Set oXMLDoc = oBizDataManager.GetProfile(CStr(CURR_PROFILE))
    
    sWhere = "./" & sXMLTagCatalog & "/" & sXMLTagProfile & "/"
    If (InStr(1, g_sParentURL, ".") > 0) Then
        aGroups = Split(g_sParentURL, ".")
        For i = LBound(aGroups) To UBound(aGroups) - 1
            sWhere = sWhere & "/" & sXMLTagGroup & "[@" & sXMLAttrName & "='" & aGroups(i) & "']"
        Next
        i = UBound(aGroups)
        sWhere = sWhere & "/" & sXMLTagProperty & "[@" & sXMLAttrName & "='" & aGroups(i) & "']"
    Else
        sWhere = sWhere & "/" & sXMLTagProperty & "[@" & sXMLAttrName & "='" & g_sParentURL & "']"
    End If
    Set oXMLProp = oXMLDoc.DocumentElement.SelectSingleNode(sWhere)
    If oXMLProp Is Nothing Then
        bSupported = False
    Else
        bSupported = True
    End If
    
    IsActiveDirectoryPresent = bSupported
End Function


' -----------------------------------------------------------------------------
' sGetActiveDirectoryDefaultNamingContext
'
' Description:
'	Function to get the active directory domain name
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetActiveDirectoryDefaultNamingContext()
    Const CURR_PROFILE = "Profile Definitions.UserObject"
    Const AD_SOURCE_TYPE = "LDAPv3"
    Const sXMLTagDataSource = "DataSource"
    Const sXMLTagSourceInfo = "SourceInfo"
    Const sXMLAttrSourceType = "sourceType"
    Const sXMLTagAttribute = "Attribute"
    Const sXMLAttrName = "name"
    Const sXMLDefaultNamingContext = "defaultNamingContext"
    
    Dim bSupported
    Dim bVal
    Dim oXMLProp
    Dim oBizDataManager
    Dim oXMLDoc
    Dim aGroups
    Dim sWhere
    Dim i

    Dim obizdata
    Dim sProfileDSN
    Dim oXMLNode    
    Dim sDefaultNamingContext
    
    Set oBizDataManager = Server.CreateObject("Commerce.BusinessDataAdmin")
    Call oBizDataManager.Connect(dictConfig.s_BizDataStoreConnectionString)
    Set oXMLDoc = oBizDataManager.GetProfile(CStr(CURR_PROFILE))
    
    Set oXMLNode = oXMLDoc.DocumentElement.SelectSingleNode( _
                                                               ".//" & _
                                                               sXMLTagDataSource & _
                                                               "[@" & _
                                                               sXMLAttrSourceType & _
                                                               "='" & _
                                                               AD_SOURCE_TYPE & _
                                                               "']//" & _
                                                               sXMLTagSourceInfo & _
                                                               "[@isDefault='1']" _
                                                           )
    
    If Not IsEmpty(oXMLNode) And Not (oXMLNode Is Nothing) Then
      sDefaultNamingContext = oXMLNode.SelectSingleNode( _
                                                           ".//" & _
                                                            sXMLTagAttribute & _
                                                            "[@" & _
                                                            sXMLAttrName & _
                                                            "='" & _
                                                            sXMLDefaultNamingContext & _
                                                            "']" _
                                                       ).GetAttribute("value")
    Else
      sDefaultNamingContext = ""
    End If
    
    sGetActiveDirectoryDefaultNamingContext = sDefaultNamingContext
End Function


' -----------------------------------------------------------------------------
' GetCommerceSiteInstallPoint
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetCommerceSiteInstallPoint()
	If appframework.VirtualDirectory = "" Then
		GetCommerceSiteInstallPoint = HOME_DIRECTORY
	Else
		GetCommerceSiteInstallPoint = VIRTUAL_DIRECTORY	
	End If
End Function


' -----------------------------------------------------------------------------
' InitSitePipelines
'
' Description:
'   Gives friendly names to the pipelines used through the site (under
'	MSCSPipelines)
'   Also enables global setting of logging
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function InitSitePipelines()
    Dim objPage, dictPipeline
    Dim MSCSEnv
    
    Set dictPipeline = GetDictionary()  

    dictPipeline.Folder	   = GetRootPath() & "pipeline\"

    ' You must grant Internet Guest Account (IUSER_machinename) 
    ' write permission on the following directory to enable logging
    dictPipeline.LogFolder = GetRootPath() & "pipeline\logfiles\"

	If MSCSEnv = DEVELOPMENT Then
		dictPipeline.LoggingEnabled  = False
		dictPipeline.LogsCycled      = False 		
	ElseIf MSCSEnv = PRODUCTION Then
		dictPipeline.LoggingEnabled  = False
		dictPipeline.LogsCycled      = False		
	End If
 
    dictPipeline.Product  = dictPipeline.Folder & "product.pcf"
    dictPipeline.Basket   = dictPipeline.Folder & "basket.pcf"
    dictPipeline.Total	  = dictPipeline.Folder & "total.pcf"
    dictPipeline.Checkout = dictPipeline.Folder & "checkout.pcf"
    dictPipeline.Fragment = dictPipeline.Folder & "fragment.pcf"
    dictPipeline.ReceivePO = dictPipeline.Folder & "recvpo.pcf"
    dictPipeline.Advertising = dictPipeline.Folder & "advertising.pcf"
    dictPipeline.Discounts   = dictPipeline.Folder & "discounts.pcf"
    dictPipeline.RecordEvent = dictPipeline.Folder & "RecordEvent.pcf"            
 
    Set InitSitePipelines = dictPipeline
End Function


' -----------------------------------------------------------------------------
' GetPaymentMethodsForSite
'
' Description:
'	This procedure must run after MessageManager is assigned
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetPaymentMethodsForSite()
	Dim mscsMessageManager, sLanguage
	
	Set mscsMessageManager = Application("MSCSMessageManager")
	sLanguage = mscsMessageManager.DefaultLanguage
	
	Dim listMethods

	Set listMethods = GetSimpleList()
	If IsBitValueSet(dictConfig.i_PaymentOptions, CREDIT_CARD_PAYMENT_OPTION) Then
		Call listMethods.Add(CREDIT_CARD_PAYMENT_METHOD)
	End If

	If IsBitValueSet(dictConfig.i_PaymentOptions, PURCHASE_ORDER_PAYMENT_OPTION) Then
		Call listMethods.Add(PURCHASE_ORDER_PAYMENT_METHOD)
	End If
	
	If listMethods.Count = 0 Then
		Err.Raise &H80004005, , mscsMessageManager.GetMessage("L_Bad_Site_PaymentOptions_ErrorMessage", sLanguage)
	End If
	
	Set GetPaymentMethodsForSite = listMethods
End Function
</SCRIPT>