<%
' =============================================================================
' std_pipeline_lib.asp
' Standard library with Pipeline functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetPipelineLogFile
'
' Description:
'	Returns logfile path with shopper id in filename
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetPipelineLogFile(ByVal s)
	GetPipelineLogFile = MSCSPipelines.LogFolder & s & m_UserID & ".log"
End Function


' -----------------------------------------------------------------------------
' RunMtsPipeline
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RunMtsPipeline(ByVal sPipeName, ByVal sPipeLogName, ByVal mscsOrderGrp)
    Dim objFSO, objFile, dictContext, dictCache, mscsUserProfile
    Dim arrSplitters
    Dim iErrorLevel
    Dim sOrderFormName
    
    ' Every pipeline may need the following contexts
    Set dictContext = GetDictionary()
    Set dictContext.MessageManager = MSCSMessageManager
    Set dictContext.DataFunctions = MSCSDataFunctions
    dictContext.Language = sLanguage
    Set dictContext.CatalogManager = Application("MSCSCatalogManager")
    Set dictContext.CacheManager = Application("MSCSCacheManager")
    Set dictContext.ProfileService = MSCSProfileService			

    ' Each specific pipeline may require additional contexts
    Select Case sPipeName
		Case MSCSPipelines.Product

        Case MSCSPipelines.Basket
			' Copy the discounts_clicked collection to each orderform
	        If IsObject(mscsOrderGrp.Value.discounts_clicked) Then
				For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
					Set mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName).Value("discounts_clicked") = _
						mscsOrderGrp.Value.discounts_clicked
				Next
			End If
	
	        dictContext.CatalogConnectionString = dictConfig.s_CatalogConnectionString

			Set mscsUserProfile = GetCurrentUserProfile()
			If Not mscsUserProfile Is Nothing Then
				Set dictContext.UserProfile = mscsUserProfile
			End If

			'You can also set dictContext.ContextProfile here if you wish to do context targetting
			Set dictContext.Evaluator = Application("MSCSExpressionEvaluator")
			dictContext.CacheName = "Discounts"
			
        Case MSCSPipelines.Total
	        dictContext.TransactionConfigConnectionString = dictConfig.s_TransactionConfigConnectionString	

        Case MSCSPipelines.Checkout
			Set dictCache = Application("MSCSCacheManager").GetCache("Discounts")
			For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
				Set mscsOrderGrp.value.Orderforms(sOrderFormName).Value("_performance") = dictCache.Value("_performance")
			Next
			Set dictContext.PageGroups = dictCache.PageGroups
			Set dictContext.dictConfig = dictConfig
    End Select 
    
	If MSCSPipelines.LoggingEnabled Then
	    If MSCSPipelines.LogsCycled Then
		    Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
		    On Error Resume Next
		    Set objFile = objFSO.GetFile(sPipeLogName)
		    If Err.number <> 0 Then
		        Err.Clear
		    Else
		        objFile.Delete		    
		    End If
		    On Error GoTo 0
	    End If
		mscsOrderGrp.LogFile = sPipeLogName
	End If

	If Not Response.IsClientConnected() Then
		Response.End
	End If
	iErrorLevel = mscsOrderGrp.RunPipe(sPipeName, g_sPipelineProgID, dictContext)
    
    RunMtsPipeline = iErrorLevel
End Function
%>
