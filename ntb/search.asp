<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' search.asp
' Perform Free Text Search
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim sPhrase, sCacheKey, sCatalogSetID
	Dim sSearchPhrase
	Dim rsSearchResults, iRecordCount
	
	Const MIN_SEARCHRESULTS = 1
	Const MAX_SEARCHRESULTS = 50
	
	Call EnsureAccess()
	sPageTitle = mscsMessageManager.GetMessage("L_Search_HTMLTitle", sLanguage)
	
	' Initialize
	sPhrase = ""
	sSearchPhrase = ""
	
	' Get search phrase
	sPhrase = LCase(GetRequestString("keyword", Null))
	If IsNull(sPhrase) Then
		sSearchPhrase = ""
	Else
		sSearchPhrase = sPhrase
	End If
	
	' If results are cached, the query doesn't need to be run
	If bFullTextResultsAreCached(sSearchPhrase, htmPageContent, sCacheKey) Then
		'
	Else
		Set rsSearchResults = rsFreeTextSearch(sSearchPhrase, MIN_SEARCHRESULTS, MAX_SEARCHRESULTS, iRecordCount)
		htmPageContent = htmRenderFullTextSearchResults(sSearchPhrase, rsSearchResults, sCacheKey, iRecordCount)
	End If
	' The result to display is now in htmPageContent.
End Sub


' -----------------------------------------------------------------------------
' bFullTextResultsAreCached
'
' Description:
'   Check out whether the search results are already cached; if they are,
'	return the result in htmCachedResult and the cache key in sCacheKey.
'	If FreeText search is not installed on the server, return error message
'	in htmCachedResult, with empty sCacheKey.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bFullTextResultsAreCached(sSearchPhrase, ByRef htmCachedResult, ByRef sCacheKey)
	Dim sCatalogSetID
	
	htmCachedResult = ""
	If dictConfig.i_IsFullTextSearchInstalled Then
		sCatalogSetID = mscsUserCatalogsetID()
		sCacheKey = sCatalogSetID & sSearchPhrase
		htmCachedResult = LookupCachedFragment("FTSearchPageCache", sCacheKey)
	Else ' FreeText Not installed!
		htmCachedResult = RenderText(mscsMessageManager.GetMessage("L_FREETEXT_NOT_INSTALLED_TEXT", sLanguage), MSCSSiteStyle.Body)
		sCacheKey = ""
	End If
	bFullTextResultsAreCached = (htmCachedResult <> "")
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderFullTextSearchResults
'
' Description:
'   Render the results
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderFullTextSearchResults(ByRef sPhrase, ByRef rsResultSet, sCacheKey, iRecordCount)
	Dim htmTitle, htmContent
	
	htmTitle = RenderText(mscsMessageManager.GetMessage("L_FREETEXT_SEARCH_RESULT_TEXT", sLanguage), _
				MSCSSiteStyle.Title) & CRLF	
	
	htmContent = RenderSearchResults(sPhrase, rsResultSet, iRecordCount, MSCSSiteStyle.Body)
	
	htmRenderFullTextSearchResults = htmTitle & htmContent
	Call CacheFragment("FTSearchPageCache", sCacheKey, htmRenderFullTextSearchResults)
End Function

%>
