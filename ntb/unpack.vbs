option explicit

const s500HANDLER = "/error/500error.asp"
const bIsBizDesk = false

const L_BeginUnpack_Text           = "Begin: unpack.vbs"
const L_InitialInfo_Text           = "  sVRoot=%1, sServerNumber=%2, sDestDir=%3, sAppName=%4"
const L_BeginGetVRoot_Text         = "  Begin: Getting oVRoot"
const L_EndGetVRoot_Text           = "  End: Getting oVRoot"
const L_BeginSiteConfigInit_Text   = "  Begin: oSiteConfig.Initialize()"
const L_EndSiteConfigInit_Text     = "  End: oSiteConfig.Initialize()"
const L_BeginAddErrorHandlers_Text = "  Begin: AddErrorHandlers"
const L_Found404_Text              = "  found 404: %1"
const L_Found401_Text              = "  found 401: %1"
const L_Found500_Text              = "  found 500.100: %1"
const L_Created500_Text            = "  created 500.100: %1"
const L_EndAddErrorHandlers_Text   = "  End: AddErrorHandlers"
const L_BeginSetACLs_Text          = "  Begin: SetACLs"
const L_EndSetACLs_Text            = "  End: SetACLs"
const L_BeginAddWidgetVRoot_Text   = "  Begin: AddWidgetVRoot"
const L_EndAddWidgetVRoot_Text     = "  End: AddWidgetVRoot"
const L_BeginSetBizDeskFlag_Text   = "  Begin: SetBizDeskFlag"
const L_EndSetBizDeskFlag_Text     = "  End: SetBizDeskFlag"
const L_BeginSetLocaleInfo_Text    = "  Begin: SetLocaleInfo"
const L_EndSetLocaleInfo_Text      = "  End: SetLocaleInfo"
const L_FailUnpack_Text            = "FAIL unpack.vbs: %1 %2"
const L_EndUnpack_Text             = "End: unpack.vbs"


sub main(byref nErrors)
    dim sServerNumber, sSiteName, sVRoot, sDestDir, sAppName, oSiteConfig
    dim webSite, oVRoot, vSubItems, arg
	
	' sServerNumber is passed as the /i: argument when we call the script    
	' sSiteName is passed as the /n: argument when we call the script
	' sVRoot is passed as the /v: argument when we call the script
	' sDestDir is passed as the /d: argument when we call the script
	' sAppName is passed as the /r: argument when we call the script
    ' >>> prepopulate with test data...
    if bIsBizDesk then 
		sServerNumber = "1"
		sSiteName = "Retail"
		sVRoot = "retailbizdesk"
		sDestDir = "D:\inetpub\wwwroot\retailbizdesk"
		sAppName = "retailbizdesk"
	else
		sServerNumber = "1"
		sSiteName = "Retail"
		sVRoot = "retail"
		sDestDir = "D:\inetpub\wwwroot\retail"
		sAppName = "retail"
	end if
	
	for each arg in WScript.Arguments
		vSubItems = split(arg, ":")
		select case vSubItems(0)
			case "/n"
				sSiteName = vSubItems(1)
				sSiteName = replace(sSiteName, """", "")
			case "/v"
				sVRoot = vSubItems(1)
				sVRoot = replace(sVRoot, """", "")
			case "/i"
				sServerNumber = vSubItems(1)
				sServerNumber = replace(sServerNumber, """", "")
			case "/d"
				sDestDir = Mid(arg, 4)
				sDestDir = replace(sDestDir, """", "")
			case "/r"
				sAppName = vSubItems(1)
				sAppName = replace(sAppName, """", "")
		end select
	next

	call WriteLogMessage(FormatOutput(L_InitialInfo_Text, Array(sVRoot, sServerNumber, sDestDir, sAppName)))
	
	WriteLogMessage(L_BeginGetVRoot_Text)
	set webSite = GetObject("IIS://localhost/w3svc/" + sServerNumber)    
    set oVRoot = webSite.GetObject("IIsWebVirtualDir", "Root")
	WriteLogMessage(L_EndGetVRoot_Text)

	WriteLogMessage(L_BeginSiteConfigInit_Text)
	set oSiteConfig = CreateObject("Commerce.SiteConfig")
	call oSiteConfig.Initialize(sSiteName)
	WriteLogMessage(L_EndSiteConfigInit_Text)
	
	'=================================================
	
	WriteLogMessage(L_BeginAddErrorHandlers_Text)
	on error resume next
		call AddErrorHandlers(sSiteName, oVRoot, sVRoot, sDestDir)
		if err.number<>0 Then
			call WriteLogMessage(FormatOutput(L_FailUnpack_Text, Array(hex(err.number), err.description)))
			nErrors = nErrors + 1
		end if
	on error goto 0
	WriteLogMessage(L_EndAddErrorHandlers_Text)
	
	WriteLogMessage(L_BeginSetACLs_Text)
	on error resume next
		call SetACLs(sDestDir)
		if err.number<>0 Then
			call WriteLogMessage(FormatOutput(L_FailUnpack_Text, Array(hex(err.number), err.description)))
			nErrors = nErrors + 1
		end if
	on error goto 0
	WriteLogMessage(L_EndSetACLs_Text)
	
	if bIsBizDesk then
		WriteLogMessage(L_BeginAddWidgetVRoot_Text)
		on error resume next
			call AddWidgetVRoot(oVRoot)
			if err.number<>0 Then
				call WriteLogMessage(FormatOutput(L_FailUnpack_Text, Array(hex(err.number), err.description)))
				nErrors = nErrors + 1
			end if
		on error goto 0
		WriteLogMessage(L_EndAddWidgetVRoot_Text)
		
		WriteLogMessage(L_BeginSetBizDeskFlag_Text)
		on error resume next		
			call SetBizDeskFlag(oSiteConfig, sVRoot)
			if err.number<>0 Then
				call WriteLogMessage(FormatOutput(L_FailUnpack_Text, Array(hex(err.number), err.description)))
				nErrors = nErrors + 1
			end if
		on error goto 0
		WriteLogMessage(L_EndSetBizDeskFlag_Text)
	end if

	WriteLogMessage(L_BeginSetLocaleInfo_Text)
	on error resume next		
		call SetLocaleInfo(oSiteConfig)
		if err.number<>0 Then
			call WriteLogMessage(FormatOutput(L_FailUnpack_Text, Array(hex(err.number), err.description)))
                        nErrors = nErrors + 1
		end if
	on error goto 0
	WriteLogMessage(L_EndSetLocaleInfo_Text)
	
	'=================================================

end sub

sub AddErrorHandlers(byVal sSiteName, byVal oVRoot, byVal sVRoot, byVal sDestDir)
	dim vDir, vList, sItem, i, vSubItems, bFound500
	
	' the following code tests whether we are unpackaging into the root of a website
    if Len(sVRoot) = 1 And sVRoot = "/" Then
        set vDir = oVRoot ' we are at the root so use the root object
    else ' we need to find the subroot in the metabase and work from there
        set vDir = oVRoot.GetObject("IIsWebVirtualDir", sVRoot)
    end if

	' HttpErrors use the form: HTTPErrorCode, HTTPErrorSubcode, FILE, Filename
	const HTTP_ERROR_CODE = 0
	const HTTP_ERROR_SUBCODE = 1
	i=0
	
	' get the list of errors
	vList = vDir.GetEx("HttpErrors") 
	bFound500 = false
	for each sItem in vList 
		vSubItems = split(sItem, ",")
		select case trim(vSubItems(HTTP_ERROR_CODE))
			case "404"
				if bIsBizDesk then
					vList(i) = "404," & vSubItems(HTTP_ERROR_SUBCODE) & ",FILE," & sDestDir & "\bdaccesserror.htm"
					call WriteLogMessage(FormatOutput(L_Found404_Text, Array(vList(i))))
				end if
			case "401"
				if bIsBizDesk then
					vList(i) = "401," & vSubItems(HTTP_ERROR_SUBCODE) & ",FILE," & sDestDir & "\bdaccesserror.htm"
					call WriteLogMessage(FormatOutput(L_Found401_Text, Array(vList(i))))
				end if
			case "500"
				if vSubItems(HTTP_ERROR_SUBCODE) = "100" then
					If sVRoot = "/" Then
						vList(i) = "500," & vSubItems(HTTP_ERROR_SUBCODE) & ",URL," & s500HANDLER
					Else
						vList(i) = "500," & vSubItems(HTTP_ERROR_SUBCODE) & ",URL,/" & sVRoot & s500HANDLER
					End If
					call WriteLogMessage(FormatOutput(L_Found500_Text, Array(vList(i))))
					bFound500 = true
				end if
		end select
		i = i + 1
	next
	if not bFound500 then
		' create 500:100 error
		redim preserve vList(i)
		if sVRoot = "/" Then
			vList(i) = "500,100,URL," & s500HANDLER
		else
			vList(i) = "500,100,URL,/" & sVRoot & s500HANDLER
		end if

		call WriteLogMessage(FormatOutput(L_Created500_Text, Array(vList(i))))
	end if
	vDir.PutEx 2, "HttpErrors", vList 
    
	' this call commits the changes
    vDir.SetInfo
end sub

sub SetACLs(byVal sDestDir)
	if bIsBizDesk then
		' Sets Acls on the Analysis Reports directory
		dim sReportsDir, oAcl, hr
		Set oAcl = CreateObject("Commerce.Acl")
		sReportsDir = sDestDir & "\analysis\Completed Reports"
		hr = oACL.SetACLs(sReportsDir, &HC0010047, &H20)
		sReportsDir = sReportsDir & "\Temp HTML Tables"

		hr = oACL.SetACLs(sReportsDir, &HC0010047, &H20)
	end if
end sub

sub AddWidgetVRoot(byVal oVRoot)
	' Adds the Widgets vroot to the IIS website
	' If the vroot already exists then we won't overwrite it
    Dim oInstaller, sPath, vDir
    
    Set oInstaller = CreateObject("WindowsInstaller.Installer")
    
    sPath = oInstaller.ProductInfo("{705CB382-C149-11D2-973D-00C04f79E4B3}", "InstallLocation")
    if sPath <> "" then
        sPath = sPath & "Widgets"

		' Attempt to get the vdir, if not found, create it
		on error resume next
		set vDir = oVRoot.GetObject("IIsWebVirtualDir", "widgets")
		if err.number<>0 then
			on error goto 0
			set vDir = oVRoot.Create("IIsWebVirtualDir", "widgets")
		else
			on error goto 0
		end if

        vDir.AuthFlags = 6
        vDir.AccessFlags = 535
        vDir.Path = sPath
        vDir.SetInfo
    end if
end sub

sub SetLocaleInfo(byVal oSiteConfig)
	dim oDataFunc, nSysLCID, sCharSet

	' get default system LCID from datafunctions object
	set oDataFunc = CreateObject("Commerce.DataFunctions")
	nSysLCID = oDataFunc.Locale
	sCharSet = sGetCharSet(nSysLCID)

	' set i_SiteDefaultLocale and i_BaseCurrencyLocale in App Default Config
	call SetAppDefaultConfigField(oSiteConfig, "i_SiteDefaultLocale",  nSysLCID)
	call SetAppDefaultConfigField(oSiteConfig, "i_BaseCurrencyLocale", nSysLCID)

	' set s_PageEncodingCharset in App Default Config
	call SetAppDefaultConfigField(oSiteConfig, "s_PageEncodingCharset", sCharSet)
end sub

function sGetCharSet(byVal nSysLCID)
	' language constants
	const LOCALE_ARABIC		= &H01
	const LOCALE_BULGARIAN	= &H02	
	const LOCALE_CATALAN	= &H03	
	const LOCALE_CHINESE	= &H04
	const LOCALE_CZECH		= &H05
	const LOCALE_DANISH		= &H06
	const LOCALE_GERMAN		= &H07
	const LOCALE_GREEK		= &H08
	const LOCALE_ENGLISH	= &H09	
	const LOCALE_SPANISH	= &H0A	
	const LOCALE_FINNISH	= &H0B	
	const LOCALE_FRENCH		= &H0C
	const LOCALE_HEBREW		= &H0D
	const LOCALE_HUNGARIAN	= &H0E
	const LOCALE_ICELANDIC	= &H0F
	const LOCALE_ITALIAN	= &H10
	const LOCALE_JAPANESE	= &H11
	const LOCALE_KOREAN		= &H12
	const LOCALE_DUTCH		= &H13
	const LOCALE_NORWEGIAN	= &H14
	const LOCALE_POLISH		= &H15
	const LOCALE_BRAZPORT	= &H16
	const LOCALE_ROMANIAN	= &H18
	const LOCALE_RUSSIAN	= &H19
	const LOCALE_SERBCROAT	= &H1A
	const LOCALE_SLOVAK		= &H1B
	const LOCALE_SWEDISH	= &H1D
	const LOCALE_THAI		= &H1E
	const LOCALE_TURKISH	= &H1F
	const LOCALE_UKRAINIAN	= &H22
	const LOCALE_ESTONIAN	= &H25
	const LOCALE_LATVIAN	= &H26
	const LOCALE_LITHUANIAN	= &H27
	const LOCALE_SLOVENIAN	= &H2A
	const LOCALE_VIETNAMESE	= &H2A
	const LOCALE_BASQUE		= &H2D
	const LOCALE_GALICIAN	= &H56

	' sublanguage constants
	const LOCALE_CHINESE_TRADITIONAL	= &H0404
	const LOCALE_CHINESE_SIMPLIFIED		= &H0804

    ' default to west-europe
	sGetCharSet	= "windows-1252"
	if not isNumeric(nSysLCID) then exit function
	nSysLCID = CLng(nSysLCID)
	'use LCID to get a default charset
	if	bIsLang(nSysLCID, LOCALE_JAPANESE)	then
		sGetCharSet	= "shift_jis"
	elseif	bIsLang(nSysLCID, LOCALE_BASQUE)	or _
			bIsLang(nSysLCID, LOCALE_BRAZPORT)	or _
			bIsLang(nSysLCID, LOCALE_CATALAN)	or _
			bIsLang(nSysLCID, LOCALE_DANISH)	or _
			bIsLang(nSysLCID, LOCALE_DUTCH)		or _
			bIsLang(nSysLCID, LOCALE_ENGLISH)	or _
			bIsLang(nSysLCID, LOCALE_FINNISH)	or _
			bIsLang(nSysLCID, LOCALE_FRENCH)	or _
			bIsLang(nSysLCID, LOCALE_GALICIAN)	or _
			bIsLang(nSysLCID, LOCALE_GERMAN)	or _
			bIsLang(nSysLCID, LOCALE_ICELANDIC)	or _
			bIsLang(nSysLCID, LOCALE_ITALIAN)	or _
			bIsLang(nSysLCID, LOCALE_NORWEGIAN)	or _
			bIsLang(nSysLCID, LOCALE_SPANISH)	or _
			bIsLang(nSysLCID, LOCALE_SWEDISH)	then
		sGetCharSet	= "windows-1252"
	elseif	bIsLang(nSysLCID, LOCALE_CHINESE) then
		if		bIsSubLang(nSysLCID, LOCALE_CHINESE_SIMPLIFIED) then
			sGetCharSet	= "gb2312"
		else	'bIsSubLang(nSysLCID, LOCALE_CHINESE_TRADITIONAL)
			sGetCharSet	= "big5"
		end if
	elseif	bIsLang(nSysLCID, LOCALE_KOREAN)	then
		sGetCharSet	= "ks_c_5601-1987"
	elseif	bIsLang(nSysLCID, LOCALE_ESTONIAN)	or _
			bIsLang(nSysLCID, LOCALE_LATVIAN)	or _
			bIsLang(nSysLCID, LOCALE_LITHUANIAN) then
		sGetCharSet	= "windows-1257"
	elseif	bIsLang(nSysLCID, LOCALE_BULGARIAN)	or _
			bIsLang(nSysLCID, LOCALE_HUNGARIAN)	or _
			bIsLang(nSysLCID, LOCALE_RUSSIAN)	or _
			bIsLang(nSysLCID, LOCALE_UKRAINIAN) then
			sGetCharSet	= "windows-1251"
	elseif	bIsLang(nSysLCID, LOCALE_SERBCROAT) or _
			bIsLang(nSysLCID, LOCALE_CZECH)		or _
			bIsLang(nSysLCID, LOCALE_POLISH)	or _
			bIsLang(nSysLCID, LOCALE_ROMANIAN)	or _
			bIsLang(nSysLCID, LOCALE_SLOVAK)	or _
			bIsLang(nSysLCID, LOCALE_SLOVENIAN)	then
		sGetCharSet	= "windows-1250"
	elseif	bIsLang(nSysLCID, LOCALE_THAI)		then
		sGetCharSet	= "windows-874"
	elseif	bIsLang(nSysLCID, LOCALE_ARABIC)	then
		sGetCharSet	= "windows-1254"
	elseif	bIsLang(nSysLCID, LOCALE_GREEK)		then
		sGetCharSet	= "windows-1253"
	elseif	bIsLang(nSysLCID, LOCALE_TURKISH)	then
		sGetCharSet	= "windows-1254"
	elseif	bIsLang(nSysLCID, LOCALE_VIETNAMESE) then
		sGetCharSet	= "windows-1258"
	elseif	bIsLang(nSysLCID, LOCALE_HEBREW)	then
		sGetCharSet	= "windows-1255"
	end if
end function

function bIsLang(byVal nSysLCID, byVal nTestLCID)
    ' test for language by ANDing sublanguage with last two bytes of system locale
	nSysLCID = (nSysLCID and 255)
	bIsLang =  cBool(((nSysLCID and nTestLCID) = nTestLCID) and ((nSysLCID and nTestLCID) = nSysLCID))
end function 

function bIsSubLang(byVal nSysLCID, byVal nTestLCID)
	' test for sublanguage by ANDing sublanguage with system locale
	bIsSubLang =  cBool(((nSysLCID and nTestLCID) = nTestLCID) and ((nSysLCID and nTestLCID) = nSysLCID))
end function 

sub SetAppDefaultConfigField(byRef oSiteConfig, byVal sPropertyName, byVal vValue)
    ' set siteconfig field value in App Default Config resource
	oSiteConfig.Fields("App Default Config").Value.Fields(sPropertyName).Value = vValue
	call oSiteConfig.SaveConfig()
end sub

sub SetBizDeskFlag(byVal oSiteConfig, byVal sVRoot)
    Dim vValue
    const ISBIZDESK = &h200

    vValue = oSiteConfig.Fields(sVRoot).Value.Fields("f_ResourceFlags").Value
	oSiteConfig.Fields(sVRoot).Value.Fields("f_ResourceFlags").Value = (vValue Or ISBIZDESK)
	oSiteConfig.SaveConfig()
end sub

Function FormatOutput(ByVal sTemplate, arrArgs)
	Dim i
	
	FormatOutput = sTemplate
	
	For i = LBound(arrArgs) To UBound(arrArgs)
		FormatOutput = Replace(FormatOutput , "%" & i + 1, arrArgs(i))
	Next
End Function


sub WriteLogMessage(byVal sMessage)
    dim oFileSystem     
    dim oTextStream     
    dim sPathName       
    
    wscript.echo sMessage

	set oFileSystem = CreateObject("Scripting.FileSystemObject")
	sPathName = oFileSystem.GetDriveName(sGetEnvironmentVariable("TEMP", "")) & "\Pup.log"
    
	on error resume next
		set oTextStream = oFileSystem.OpenTextFile(sPathName, 8, True)	'  ForAppending=8
		oTextStream.Write sMessage & chr(13) & chr(10)
		oTextStream.Close
	on error goto 0
end sub

function sGetEnvironmentVariable(byVal sVariable, byVal sArea) 
    dim oWshShell       
    dim oSysEnv
    
    if (Trim(sArea) = "") Then sArea = "PROCESS"
    
    set oWshShell = CreateObject("WScript.Shell")
    set oSysEnv = oWshShell.Environment(sArea)
    sGetEnvironmentVariable = oSysEnv(sVariable)
end function



' global code starts here...
Dim nErrors
nErrors = 0
on error resume next ' errors will bubble up to here
WriteLogMessage(L_BeginUnpack_Text)
call main(nErrors)
if err.number<>0 Then 'any other bubbled up error is handled here
	call WriteLogMessage(FormatOutput(L_FailUnpack_Text, Array(hex(err.number), err.description)))
	nErrors = nErrors + 1
end if
WriteLogMessage(L_EndUnpack_Text)
on error goto 0
if nErrors>0 Then
	err.raise -1 ' raise an error so that the executable process returns an error
end if

