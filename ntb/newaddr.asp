<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/form_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/addr_lib.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' newaddr.asp
' Adds, edits, or deletes address book addresses.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim dictFldVals, dictFldErrs, iAddrType, sAction, sAddrID
	    
	Call EnsureAuthAccess()
	
	Call SetupAddressForm(dictFldVals, dictFldErrs, iAddrType, sAction, sAddrID)
	htmPageContent = htmRenderAddressForm(dictFldVals, dictFldErrs, iAddrType, sAction, sAddrID)
End Sub


' -----------------------------------------------------------------------------
' SetupAddressForm
'
' Description:
'   Do the bulk of the work in preparing the address form for display
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SetupAddressForm(ByRef dictFldVals, ByRef dictFldErrs, ByRef iAddrType, ByRef sAction, ByRef sAddrID)
	Dim bSuccess, iErrorLevel, rsAddr, listFlds, arrParams, arrParamVals
	
	Call FetchData(iAddrType, sAction, sAddrID, rsAddr)
	Call CatchErrors(iAddrType, sAction, sAddrID, rsAddr)
	
	If sAction = "delete" Then
		Call MSCSProfileService.DeleteProfile(sAddrID, PROFILE_TYPE_ADDRESS)
		Response.Redirect(GenerateURL(MSCSSitePages.AddressBook, Array(ADDRESS_TYPE), Array(iAddrType)))
	End If
	
	' Initialize
	iErrorLevel = 0
	bSuccess = False	
	Set dictFldVals = Nothing
	Set dictFldErrs = Nothing
	
	Set listFlds = Application("MSCSForms").Value("AddressBookAddress")		
	
	If IsFormSubmitted() Then 
		Set dictFldVals = GetSubmittedFieldValues(listFlds)
		iErrorLevel = iValidateSubmittedAddressForAddressBook(listFlds, dictFldVals, dictFldErrs, sAction, sAddrID, rsAddr)
		If iErrorLevel = 0 Then
			' Validation of submitted data succeeded.
			bSuccess = True
		End If
	End If

	If bSuccess Then
		Call UpdateAddressBook(dictFldVals, sAction, sAddrID, rsAddr)

		arrParams = Array(ADDRESS_TYPE, ADDRESS_ID)
		arrParamVals = Array(iAddrType, sAddrID)
		Response.Redirect(GenerateURL(MSCSSitePages.AddressBook, arrParams, arrParamVals))
	End If

	If iErrorLevel = 0 Then
		Set dictFldVals = dictGetInitialFieldValues(listFlds, sAction, rsAddr, iAddrType)
	End If
End Sub


' -----------------------------------------------------------------------------
' FetchData
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub FetchData(ByRef iAddrType, ByRef sAction, ByRef sAddrID, ByRef rsAddr)
	iAddrType = GetAddressType()
	
	sAction = mscsAppFramework.RequestString("action", Null)
	
	' Is action verb supported?
	If (sAction <> "add") And (sAction <> "edit") And (sAction <> "delete") Then
		sAction = Null
	' Get the address_id for the address to edit or delete.
	ElseIf (sAction = "edit") or (sAction = "delete") Then
		sAddrID = mscsAppFramework.RequestString(ADDRESS_ID, Null)
		If Not IsNull(sAddrID) Then
			' Set rsGetProfile's bForceDBLookUp to True to force a database look-up.
			Set rsAddr = rsGetProfile(sAddrID, PROFILE_TYPE_ADDRESS, True)
		End If
	End If
End Sub


' -----------------------------------------------------------------------------
' CatchErrors
'
' Description:
'   If the basket is empty, you don't need to be here
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrors(ByVal iAddrType, ByVal sAction, ByVal sAddrID, ByVal rsAddr)
	Dim mscsOrderGrp
	
	' Redirect if the address book is disabled or is set to read-only.
	If dictConfig.i_AddressBookOptions = ADDRESSBOOK_DISABLED Then
		Response.Redirect(GenerateURL(MSCSSitePages.AddressForm, Array(), Array()))
	ElseIf dictConfig.i_AddressBookOptions = ADDRESSBOOK_READONLY Then
		Response.Redirect(GenerateURL(MSCSSitePages.AddressBook, Array(ADDRESS_TYPE), Array(iAddrType)))
	End If
	
	Set mscsOrderGrp = LoadBasket(m_UserID)
	
	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
	
	' Redirect if action verb is not specified or supported.
	If IsNull(sAction) Then
		Response.Redirect(GenerateURL(MSCSSitePages.AddressBook, Array(ADDRESS_TYPE), Array(iAddrType)))
	End If
	
	' To edit or delete an address, the address must exist.
	If (sAction = "edit") Or (sAction = "delete") Then
		If IsNull(sAddrID) Or (rsAddr Is Nothing) Then
			Response.Redirect(GenerateURL(MSCSSitePages.AddressBook, Array(ADDRESS_TYPE), Array(iAddrType)))
		End If
	End If
End Sub


' -----------------------------------------------------------------------------
' iValidateSubmittedAddressForAddressBook
'
' Description:
'   Insert your specific address valication logic inside this function.
'	The default implementation supports basic validation for international addresses:
'	It ensures that all required fields contain data.
'	It ensures that the region field is a required field for countries that have regions defined.
'	It accepts region name or region code as input; validates them against regions table.
'	It expands region code to region name on subsequent renderings of address form.
'	It implements Zip code validation for US addresses.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function iValidateSubmittedAddressForAddressBook(listFlds, dictFldVals, dictFldErrs, ByVal sAction, ByRef sAddrID, rsAddr)
	Dim iErrorLevel, listRegions, sRegionName, bNameChkReqd
	
	iErrorLevel = 0
	
	' Name check required to ensure the uniqueness of address name used.
	bNameChkReqd = True
	
	' Perform field-level validation.
 	Set dictFldErrs = GetFieldsErrorDictionary(listFlds, dictFldVals)

	If dictFldErrs.Count > 0 Then
		iErrorLevel = 1
	End If
	
	If Not IsNull(dictFldVals.Value(ADDRESS_NAME)) Then
		If sAction = "edit" Then
			' If user has not changed the address name, there is no need to test for uniqueness.
			If StrComp(dictFldVals.Value(ADDRESS_NAME), rsAddr.Fields(GENERAL_INFO_GROUP & "." & ADDRESS_NAME).Value, vbTextCompare) = 0 Then
				bNameChkReqd = False
			End If
		End If
		
		If bNameChkReqd Then
			' Test for uniqueness of the address name
			If Not bIsAddressNameUnique(dictFldVals.Value(ADDRESS_NAME)) Then
				dictFldErrs.Value(ADDRESS_NAME) = mscsMessageManager.GetMessage("L_Unique_FriendlyName_Required_ErrorMessage", sLanguage)
				iErrorLevel = 1
			End If
		End If
	End If
	
	' If there is a regions list for the country then region is a required field 
	'	and what user enters for region requires validation, otherwise region is
	'	an optional field and no validation is required. Note that user may enter
	'	either full region name or abbreviated region code into the region field.
	Set listRegions = MSCSAppConfig.GetRegionNamesListFromCountryCode(dictFldVals.Value(ADDRESS_COUNTRY))
	If listRegions.Count > 0 Then
		sRegionName = LookupRegion(dictFldVals.Value(REGION_NAME), dictFldVals.Value(ADDRESS_COUNTRY))
		If IsNull(sRegionName) Then
			dictFldErrs.Value(REGION_NAME) = mscsMessageManager.GetMessage("L_BadRegion_ErrorMessage", sLanguage)
			iErrorLevel = 1
		Else
			' Since user may enter region code into region name field, modify user 
			'	input to expand region code to region name.
			dictFldVals.Value(REGION_NAME) = sRegionName
		End If
	End If
		
	If StrComp(dictFldVals.Value(ADDRESS_COUNTRY), "US", vbTextCompare) = 0 Then
		If Not IsZipCodeValid(dictFldVals.Value(POSTAL_CODE)) Then
			dictFldErrs.Value(POSTAL_CODE) = mscsMessageManager.GetMessage("L_BadPostalCode_ErrorMessage", sLanguage)
			iErrorLevel = 1
		End If
	End If

	iValidateSubmittedAddressForAddressBook = iErrorLevel
End Function


' -----------------------------------------------------------------------------
' bIsAddressNameUnique
'
' Description:
'   Is the given address name unique?
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bIsAddressNameUnique(ByVal sAddrName)
	Dim rsAddresses
	
	bIsAddressNameUnique = True

	Set rsAddresses = GetUserAddresses(m_UserID)
	While Not rsAddresses.EOF 
		If StrComp(sAddrName, rsAddresses.Fields(GENERAL_INFO_GROUP & "." & ADDRESS_NAME).Value, vbTextCompare) = 0 Then
			bIsAddressNameUnique = False
			Exit Function
		End If
		rsAddresses.MoveNext
	Wend
End Function


' -----------------------------------------------------------------------------
' UpdateAddressBook
'
' Description:
'   Add address to the Addressbook Dictionary dictAddressBook
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub UpdateAddressBook(ByVal dictAddr, ByVal sAction, ByRef sAddrID, ByVal rsAddr)
    Dim sItem
    
	dictAddr.Value(MODIFIED_BY) = m_UserID
	dictAddr.Value(ADDRESS_PARENT_ID) = m_UserID

	' Set the region code. Note that region code is set to region name if there are no 
	'	regions defined for the specified country. Both region name and region code are 
	'	set to "" if region field is left blank.
	If Not IsNull(dictAddr.Value(REGION_NAME)) Then
		dictAddr.Value(REGION_CODE) = MSCSAppConfig.GetRegionCodeFromCountryCodeAndRegionName(dictAddr.Value(ADDRESS_COUNTRY), dictAddr.Value(REGION_NAME))
		If dictAddr.Value(REGION_CODE) = "" Then
			dictAddr.Value(REGION_CODE) = dictAddr.Value(REGION_NAME)
		End If
	Else
		dictAddr.Value(REGION_NAME) = ""
		dictAddr.Value(REGION_CODE) = ""
	End If
    
	If sAction = "add" Then
		sAddrID = MSCSGenID.GenGUIDString
	    Set rsAddr = MSCSProfileService.CreateProfile(sAddrID, PROFILE_TYPE_ADDRESS)
	End If
    
    For Each sItem In dictAddr
		rsAddr.Fields(GENERAL_INFO_GROUP & "." & sItem).Value = dictAddr.Value(sItem)	
    Next
    
    rsAddr.Fields(GENERAL_INFO_GROUP & "." & ADDRESS_TYPE).Value = NEUTRAL_ADDRESS
    
    Call rsAddr.Update()
End Sub


' -----------------------------------------------------------------------------
' dictGetInitialFieldValues
'
' Description:
'   Return Dictionary with default values
'
' Parameters:
'
' Returns:
'	Dictionary object
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictGetInitialFieldValues(ByVal listFlds, ByVal sAction, ByVal rsAddr, ByVal iAddrType)	
	Dim dictItem, dictFldVals, sKey, fld
	
	If sAction = "edit" Then
		Set dictFldVals = RsToDict(rsAddr("GeneralInfo").Value)
	Else
		Set dictFldVals = GetDictionary()
		dictFldVals.Value(ADDRESS_COUNTRY) = "-1"
	End If

	Set dictGetInitialFieldValues = dictFldVals
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderAddressForm
'
' Description:
'   Render the whole page (Address form)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderAddressForm(ByRef dictFldVals, ByRef dictFldErrs, ByVal iAddrType, ByVal sAction, ByVal sAddrID)
	Dim sPageTitle, arrParams, arrParamVals, urlAction
	Dim htmTitle, htmContent
	
	If sAction = "add" Then
		sPageTitle = mscsMessageManager.GetMessage("L_AddAddress_HTMLTitle", sLanguage)	
	Else
		sPageTitle = mscsMessageManager.GetMessage("L_EditAddress_HTMLTitle", sLanguage)		
	End If

	htmTitle = RenderText(sPageTitle, MSCSSiteStyle.Title)	
	
	arrParams = Array(ADDRESS_TYPE, "action", ADDRESS_ID)
	arrParamVals = Array(iAddrType, sAction, sAddrID)
	
	urlAction = GenerateURL(MSCSSitePages.AddAddressToAddressBook, arrParams, arrParamVals)
	htmContent = htmRenderFillOutForm(urlAction, "AddressBookAddress", dictFldVals, dictFldErrs)

	htmRenderAddressForm = htmTitle & CRLF & htmContent
End Function
%>
