<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' Category.asp
' Display the product list of the input catalog, category and page number.
' Use cached HTML if available.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
Dim sCatalogName, oCatalog, oCategory, sCategoryName
Dim iPageNumber, nRecordsTotal, nOutPagesTotal
Dim sCacheKey
Dim htmTitle
Dim rsProducts
	
	Call EnsureAccess()

	FetchCategoryData sPageTitle, sCatalogName, sCategoryName, iPageNumber
	Call EnsureUserHasRightsToCatalog(sCatalogName, m_UserID)
	htmPageContent = htmCachedProductList(sCatalogName, sCategoryName, iPageNumber, sCacheKey)
	If IsNull(htmPageContent) Then '<< No cached HTML found, generate it
		Set oCatalog = MSCSCatalogManager.GetCatalog(sCatalogName)
		If oCatalog Is Nothing Then
			Response.Redirect(GenerateURL(MSCSSitePages.BadCatalogItem, Array(), Array()))
		End If
		Set oCategory = mscsGetCategoryObject(oCatalog, sCategoryName)
		Set rsProducts = mscsGetProductList(oCatalog, oCategory, iPageNumber, nRecordsTotal, nOutPagesTotal)
		htmPageContent = htmCategoryHTML(oCatalog, sCatalogName, sCategoryName, oCategory, rsProducts, iPageNumber, nOutPagesTotal, sCacheKey)
		Set rsProducts = Nothing
		Set oCategory = Nothing
		Set oCatalog = Nothing
	End If
End Sub


' -----------------------------------------------------------------------------
' FetchCategoryData
'
' Description:
'   Read input parameters, fill in these variables
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub FetchCategoryData(Byref sPageTitle, Byref sCatalogName, Byref sCategoryName, Byref iPageNumber)
	sPageTitle = mscsMessageManager.GetMessage("L_Category_HTMLTitle", sLanguage)
	sCatalogName = GetRequestString(CATALOG_NAME_URL_KEY, Null)
	sCategoryName = GetRequestString(CATEGORY_NAME_URL_KEY, Null)
	iPageNumber = MSCSAppFrameWork.RequestNumber(PAGENUMBER_URL_KEY, 1)
End Sub


' -----------------------------------------------------------------------------
' htmCategoryHTML
'
' Description:
'   Generate HTML display of the given data
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmCategoryHTML(oCatalog, sCatalogName, sCategoryName, oCategory, rsProducts, iPageNumber, nOutPagesTotal, sCacheKey)
Dim htmPageContent
Dim htmTitle

	' Render the page title
	If IsNull(sCategoryName) Then
		htmTitle = RenderText(sCatalogName, MSCSSiteStyle.Title)
	Else
		htmTitle = RenderText(sCategoryName, MSCSSiteStyle.Title)
	End If
	
	htmPageContent = htmTitle & CRLF & htmRenderCategoryPage(oCatalog, sCatalogName, sCategoryName, oCategory, rsProducts, iPageNumber, nOutPagesTotal)
	
	Call CacheFragment("ProductListCache", sCacheKey, htmPageContent)
	htmCategoryHTML = htmPageContent
End Function


' -----------------------------------------------------------------------------
' htmCachedProductList
'
' Description:
'   If there's cached HTML for the list of products in the given catalog,
'   category and page number, return it; otherwise, return Null.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmCachedProductList(sCatalogName, sCategoryName, iPageNumber, ByRef sCacheKey)
	htmCachedProductList = Null
	sCacheKey = sCatalogName & sCategoryName & iPageNumber
	htmCachedProductList = LookupCachedFragment("ProductListCache", sCacheKey)
End Function

%>

