<!-- #INCLUDE FILE="include/header.asp" -->   
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' Default.asp
' Homepage
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim CatalogSetID
	Dim rsCatalogs, rsProducts
	Dim iCatalogCount, nPageNumber
	Dim sCatalogName
	Dim oCatalog, oCategory
    
	Call EnsureAccess()
	
	CatalogSetID = mscsUserCatalogsetID()
	htmPageContent = LookupCachedFragment("DefaultPageCache", CatalogSetID)
	If IsNull(htmPageContent) Then
		Call PrepareDefaultPage(CatalogSetID, rsCatalogs, iCatalogCount, sCatalogName, oCatalog, oCategory, rsProducts, nPageNumber)
		htmPageContent = htmRenderDefaultPage(CatalogSetID, rsCatalogs, iCatalogCount, sCatalogName, oCatalog, oCategory, rsProducts, nPageNumber)
	End If
End Sub


' -----------------------------------------------------------------------------
' PrepareDefaultPage
'
' Description:
'   Helper function to perform the business logic needed to read the data
'	that must be presented on the homepage, after which these values are passed
'	on to the rendering function
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareDefaultPage(ByVal CatalogSetID, ByRef rsCatalogs, ByRef iCatalogCount, ByRef sCatalogName, ByRef oCatalog, ByRef oCategory, ByRef rsProducts, ByRef nPageNumber)
	Dim sCategoryName
	Dim nRecordsTotal, nOutPagesTotal
	
	Set rsCatalogs = mscsUserCatalogsFromID(CatalogSetID)
	iCatalogCount = GetRecordCount(rsCatalogs)
	
	' Get the page info, depending on the number of catalogs in the system
	Select Case iCatalogCount
		Case 0
		Case 1
			sCatalogName = rsCatalogs.Fields(CATALOG_NAME_PROPERTY_NAME).Value
			sCategoryName = CATALOG_ROOT_CATEGORY_NAME
			Set oCatalog = MSCSCatalogManager.GetCatalog(sCatalogName) 'Catalog Name comes from trusted source (not url), otherwise we should call GetCatalogForUser or EnsureUserHasRightsToCatalog
			nPageNumber = 1
				
			Set oCategory = mscsGetCategoryObject(oCatalog, sCategoryName)
			Set rsProducts = mscsGetProductList(oCatalog, oCategory, nPageNumber, nRecordsTotal, nOutPagesTotal)
		Case Else
	End Select
End Sub


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderDefaultPage
'
' Description:
'   Render the page's main HTML
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderDefaultPage(ByVal CatalogSetID, ByVal rsCatalogs, ByVal iCatalogCount, ByVal sCatalogName, ByVal oCatalog, ByVal oCategory, ByVal rsProducts, ByVal nPageNumber)
    Dim htmTitle, htmContent, htmPageContent
    Dim nPagesTotal

	' Get the catalog fragment from the cache, or render it
    sPageTitle = mscsMessageManager.GetMessage("L_Home_HTMLTitle", sLanguage)
    
	' Render the page, depending on the number of catalogs in the system
	Select Case iCatalogCount
		Case 0
			' No catalogs available
			htmTitle = RenderText(mscsMessageManager.GetMessage("L_MENU_CATEGORIES_SECTION_HTMLTitle", sLanguage), MSCSSiteStyle.Title)
			htmContent = RenderText(mscsMessageManager.GetMessage("L_No_Catalogs_Installed_ErrorMessage", sLanguage), MSCSSiteStyle.Body)
		Case 1
			' Render the one catalog...
			htmTitle = RenderText(mscsMessageManager.GetMessage("L_MENU_CATEGORIES_SECTION_HTMLTitle", sLanguage), MSCSSiteStyle.Title)
			htmContent = htmRenderCategoryPage(oCatalog, sCatalogName, CATALOG_ROOT_CATEGORY_NAME, oCategory, rsProducts, nPageNumber, nPagesTotal)
		Case Else
			' Render a list of catalogs
			htmTitle = RenderText(mscsMessageManager.GetMessage("L_MENU_CATALOG_SECTION_HTMLTitle", sLanguage), MSCSSiteStyle.Title)
			htmContent = htmRenderCatalogList(rsCatalogs, MSCSSiteStyle.Body)
		End Select
		htmPageContent = htmTitle & CRLF & htmContent
		Call CacheFragment("DefaultPageCache", CatalogSetID, htmPageContent)
	htmRenderDefaultPage = htmPageContent
End Function

%>

