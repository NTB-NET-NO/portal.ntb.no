<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/addr_lib.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/form_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' addrbook.asp
' Display page for Address Book editing
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim iAddrType, rsAddresses, sCheckedAddrID
	
	Call EnsureAuthAccess()
	
	Call FetchData(iAddrType, rsAddresses, sCheckedAddrID)
	Call CatchErrors(iAddrType)
	
	htmPageContent = htmRenderAddressBookPage(iAddrType, rsAddresses, sCheckedAddrID)
End Sub


' -----------------------------------------------------------------------------
' FetchData
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub FetchData(ByRef iAddrType, ByRef rsAddresses, ByRef sCheckedAddrID)
	iAddrType = GetAddressType()
	
	Set rsAddresses = GetUserAddresses(m_UserID)	
	
	If Not rsAddresses.EOF Then 
		' Use the first address as the default address.
		'	Add code here to set the default address differently such as
		'	using the shipping or billing address of the basket (if any)
		'	or setting it to last address that was added or edited.
		sCheckedAddrID = rsAddresses(GENERAL_INFO_GROUP & "." & ADDRESS_ID).Value
	End If
End Sub


' -----------------------------------------------------------------------------
' CatchErrors
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrors(ByVal iAddrType)
	Dim mscsOrderGrp
	
	' Redirect if the address book is disabled or is set to read-only.
	If dictConfig.i_AddressBookOptions = ADDRESSBOOK_DISABLED Then
		Response.Redirect(GenerateURL(MSCSSitePages.AddressForm, Array(ADDRESS_TYPE), Array(iAddrType)))
	End If
	
	Set mscsOrderGrp = LoadBasket(m_UserID)
	
	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
End Sub


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderAddressBookPage
'
' Description:
'   Render page output
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderAddressBookPage(ByVal iAddrType, rsAddresses, ByVal sCheckedAddrID)
	Dim htmContent, htmLinkText, htmTitle, htmNewAddrLink
	Dim urlLink, arrParams, arrParamVals
		
	htmNewAddrLink = ""
	
	If dictConfig.i_AddressBookOptions = ADDRESSBOOK_READWRITE Then
		arrParams = Array(ADDRESS_TYPE, "action")
		arrParamVals = Array(iAddrType, "add")
	
		urlLink = GenerateURL(MSCSSitePages.AddAddressToAddressBook, arrParams, arrParamVals)
		htmLinkText = RenderText(mscsMessageManager.GetMessage("L_ADDRESS_NEW_ADDRESS_HTMLText", sLanguage), MSCSSiteStyle.Body)
		htmNewAddrLink = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
	End If

	' If address book is empty, show a message.
	If rsAddresses.EOF Then
		htmContent = RenderText(mscsMessageManager.GetMessage("L_EmptyAddressBook_ErrorMessage", sLanguage), MSCSSiteStyle.Body) & BR
		htmContent = htmContent & htmNewAddrLink
	Else
		htmContent = htmRenderAddressBook(iAddrType, rsAddresses, sCheckedAddrID)
		htmContent = htmNewAddrLink & htmContent & htmNewAddrLink		
	End if
	
	If iAddrType = SHIPPING_ADDRESS Then
		sPageTitle = mscsMessageManager.GetMessage("L_ShipToAddress_HTMLTitle", sLanguage)
	Else
		sPageTitle = mscsMessageManager.GetMessage("L_BillToAddress_HTMLTitle", sLanguage)
	End If
			
	htmTitle = RenderText(sPageTitle, MSCSSiteStyle.Title)
	htmRenderAddressBookPage = htmTitle & CRLF & htmContent
End Function


' -----------------------------------------------------------------------------
' htmRenderAddressBook
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderAddressBook(ByVal iAddrType, ByRef rsAddresses, ByVal sCheckedAddrID)
	Dim urlLink, urlAction, sBtnText, htmLinkText
	Dim htmButton, htmBody
	
	htmBody = htmRenderAddressRows(iAddrType, rsAddresses, sCheckedAddrID) & BR

	If iAddrType = SHIPPING_ADDRESS Then
		' Render checkbox authorizing use of current address for both shipping and billing purposes.
		htmBody = htmBody & RenderCheckBox(BILLING_USE, CHECKED, True, MSCSSiteStyle.CheckBox) 
		htmBody = htmBody & RenderText(mscsMessageManager.GetMessage("L_Address_BillTo_ShipTo_HTMLText", sLanguage), MSCSSiteStyle.Body) & CRLF
	End If
	
	sBtnText = mscsMessageManager.GetMessage("L_SelectAddress_Button", sLanguage)
	htmBody = htmBody & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)

	urlAction = GenerateURL(MSCSSitePages.SetAddress, Array(ADDRESS_TYPE), Array(iAddrType))
	htmRenderAddressBook = RenderForm(urlAction, htmBody, HTTP_POST)
End Function


' -----------------------------------------------------------------------------
' htmRenderAddressRows
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderAddressRows(ByVal iAddrType, ByVal rsAddresses, ByVal sCheckedAddrID)
	Dim arrDataRow, urlLink, htmLinkText, htmEditLink, htmDelLink
    Dim sAddrID, arrParams, arrParamVals, i
    
    ' Increase the number of table header columns to accomodate Edit and Delete headers.
    If dictConfig.i_AddressBookOptions = ADDRESSBOOK_READWRITE Then
		ReDim arrDataRow(11)
		arrDataRow(1) = mscsMessageManager.GetMessage("L_EditAddress_HTMLText", sLanguage)
		arrDataRow(2) = mscsMessageManager.GetMessage("L_DeleteAddress_HTMLText", sLanguage)
		i = 2
	Else
		ReDim arrDataRow(9)
		i = 0
	End If
    
	' Populate the array for the header row.
	arrDataRow(0) = ""

	' $$INTLADDR: This illustrates how to use an alternative address format (in this case Japanese) 
	'If dictConfig.i_SiteDefaultLocale = 1041 Then
	'	arrDataRow(i + 1) = mscsMessageManager.GetMessage("L_AddressName_HTMLText", sLanguage)
	'	arrDataRow(i + 2) = mscsMessageManager.GetMessage("L_Country_HTMLText", sLanguage)
	'	arrDataRow(i + 3) = mscsMessageManager.GetMessage("L_PostalCodeCondensed_HTMLText", sLanguage)
	'	arrDataRow(i + 4) = mscsMessageManager.GetMessage("L_Region_HTMLText", sLanguage) 
	'	arrDataRow(i + 5) = mscsMessageManager.GetMessage("L_City_HTMLText", sLanguage) 
	'	arrDataRow(i + 6) = mscsMessageManager.GetMessage("L_AddressLine1_HTMLText", sLanguage)
	'	arrDataRow(i + 7) = mscsMessageManager.GetMessage("L_AddressLine2Condensed_HTMLText", sLanguage)
	'	arrDataRow(i + 8) = mscsMessageManager.GetMessage("L_LastName_HTMLText", sLanguage)
	'	arrDataRow(i + 9) = mscsMessageManager.GetMessage("L_FirstName_HTMLText", sLanguage)
	'Else
		arrDataRow(i + 1) = mscsMessageManager.GetMessage("L_AddressName_HTMLText", sLanguage)
		arrDataRow(i + 2) = mscsMessageManager.GetMessage("L_LastName_HTMLText", sLanguage)
		arrDataRow(i + 3) = mscsMessageManager.GetMessage("L_FirstName_HTMLText", sLanguage)
		arrDataRow(i + 4) = mscsMessageManager.GetMessage("L_AddressLine1_HTMLText", sLanguage)
		arrDataRow(i + 5) = mscsMessageManager.GetMessage("L_AddressLine2Condensed_HTMLText", sLanguage)
		arrDataRow(i + 6) = mscsMessageManager.GetMessage("L_City_HTMLText", sLanguage) 
		arrDataRow(i + 7) = mscsMessageManager.GetMessage("L_Region_HTMLText", sLanguage) 
		arrDataRow(i + 8) = mscsMessageManager.GetMessage("L_PostalCodeCondensed_HTMLText", sLanguage)
		arrDataRow(i + 9) = mscsMessageManager.GetMessage("L_Country_HTMLText", sLanguage)
	'End If

	htmRenderAddressRows = RenderTableHeaderRow(arrDataRow, Array(), MSCSSiteStyle.TRCenter)
	
	' Generate the QueryString for edit and delete links using a placeholder.
	arrParams = Array(ADDRESS_TYPE, "action", ADDRESS_ID)
	arrParamVals = Array(iAddrType, "edit", "addrid")
	urlLink = GenerateURL(MSCSSitePages.AddAddressToAddressBook, arrParams, arrParamVals)
	htmLinkText = RenderText(mscsMessageManager.GetMessage("L_Edit_HTMLText", sLanguage), MSCSSiteStyle.Body)
	htmEditLink = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)
	
	arrParamVals = Array(iAddrType, "delete", "addrid")
	urlLink = GenerateURL(MSCSSitePages.AddAddressToAddressBook, arrParams, arrParamVals)
	htmLinkText = RenderText(mscsMessageManager.GetMessage("L_Delete_HTMLText", sLanguage), MSCSSiteStyle.Body)
	htmDelLink = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)

	While Not rsAddresses.EOF 
		sAddrID = rsAddresses(GENERAL_INFO_GROUP & "." & ADDRESS_ID).Value

		If sAddrID = sCheckedAddrID Then
		    arrDataRow(0) = RenderRadioButton(ADDRESS_ID, sAddrID, True, MSCSSiteStyle.RadioButton)
		Else
		    arrDataRow(0) = RenderRadioButton(ADDRESS_ID, sAddrID, False, MSCSSiteStyle.RadioButton)
		End If
		
		' Put the Edit and Delete links in the second and third columns, respectively.
		If dictConfig.i_AddressBookOptions = ADDRESSBOOK_READWRITE Then
			arrDataRow(1) = Replace(htmEditLink, "addrid", sAddrID)
			arrDataRow(2) = Replace(htmDelLink, "addrid", sAddrID)
		End If
		
		' $$INTLADDR: This illustrates how to use an alternative address format (in this case Japanese) 
		'If dictConfig.i_SiteDefaultLocale = 1041 Then
		'	arrDataRow(i + 1) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & ADDRESS_NAME).Value
		'	arrDataRow(i + 2) = MSCSAppConfig.GetCountryNameFromCountryCode(rsAddresses.Fields(GENERAL_INFO_GROUP & ".country_code").Value)
		'	arrDataRow(i + 3) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & POSTAL_CODE).Value
		'	arrDataRow(i + 4) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & REGION_NAME).Value
		'	arrDataRow(i + 5) = rsAddresses.Fields(GENERAL_INFO_GROUP & ".city").Value
		'	arrDataRow(i + 6) = rsAddresses.Fields(GENERAL_INFO_GROUP & ".address_line1").Value
		'	arrDataRow(i + 7) = rsAddresses.Fields(GENERAL_INFO_GROUP & ".address_line2").Value
		'	arrDataRow(i + 8) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & LAST_NAME).Value
		'	arrDataRow(i + 9) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & FIRST_NAME).Value
		'Else
			arrDataRow(i + 1) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & ADDRESS_NAME).Value
			arrDataRow(i + 2) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & LAST_NAME).Value
			arrDataRow(i + 3) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & FIRST_NAME).Value
			arrDataRow(i + 4) = rsAddresses.Fields(GENERAL_INFO_GROUP & ".address_line1").Value
			arrDataRow(i + 5) = rsAddresses.Fields(GENERAL_INFO_GROUP & ".address_line2").Value
			arrDataRow(i + 6) = rsAddresses.Fields(GENERAL_INFO_GROUP & ".city").Value
			arrDataRow(i + 7) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & REGION_NAME).Value
			arrDataRow(i + 8) = rsAddresses.Fields(GENERAL_INFO_GROUP & "." & POSTAL_CODE).Value
			arrDataRow(i + 9) = MSCSAppConfig.GetCountryNameFromCountryCode(rsAddresses.Fields(GENERAL_INFO_GROUP & ".country_code").Value)
		'End If
		
	    htmRenderAddressRows = htmRenderAddressRows & RenderTableDataRow(arrDataRow, Array(), MSCSSiteStyle.TRLeft)
	    rsAddresses.MoveNext 
	Wend

	htmRenderAddressRows = RenderTable(htmRenderAddressRows, MSCSSiteStyle.BorderedTable)
End Function
%>
