<!-- #INCLUDE FILE="include/header.asp" -->   
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/payment.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' Dispatch.asp
' Tween page that dispatches to the right payment method page
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim mscsOrderGrp, listMethods, sPaymentMethod, urlRedirect, sVerifyWithTotal
	
	Call EnsureAccess()

	sVerifyWithTotal = GetRequestString("verify_with_total", Null)

	Set mscsOrderGrp = LoadBasket(m_UserID)

	' Get all payment methods applicable to user.
	Set listMethods = GetPaymentMethodsForUser()

	If listMethods.Count > 0 Then
		sPaymentMethod = GetRequestString(PAYMENT_METHOD, Null)
		If IsNull(sPaymentMethod) Then
			sPaymentMethod = sLookUpPaymentMethod(mscsOrderGrp, listMethods)
		Else
			' Is payment method supported for user?
			If Not IsEntityInList(sPaymentMethod, listMethods) Then
				sPaymentMethod = sLookUpPaymentMethod(mscsOrderGrp, listMethods)
			End If
		End If

		urlRedirect = GenerateURL(GetPaymentPageFromPaymentMethod(sPaymentMethod), Array("verify_with_total"), Array(sVerifyWithTotal))
		Response.Redirect(urlRedirect)
	Else
		Err.Raise HRESULT_E_FAIL, "Site", mscsMessageManager.GetMessage("L_Bad_User_PaymentOptions_ErrorMessage", sLanguage)
	End If		
End Sub


' -----------------------------------------------------------------------------
' sLookUpPaymentMethod
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sLookUpPaymentMethod(mscsOrderGrp, listMethods)
	Dim sPaymentMethod
	
	If IsNull(mscsOrderGrp.Value(PAYMENT_METHOD)) Then
		sPaymentMethod = GetDefaultPaymentMethodForUser(listMethods)
	' Is payment method supported for user?
	ElseIf IsEntityInList(listMethods, mscsOrderGrp.Value(PAYMENT_METHOD)) Then
		sPaymentMethod = mscsOrderGrp.Value(PAYMENT_METHOD)
	Else
		' If it appears that the site has disabled a payment method, Discard the saved 
		' payment method and fall back to user's default payment method.
		sPaymentMethod = GetDefaultPaymentMethodForUser(listMethods)
	End If

	sLookUpPaymentMethod = sPaymentMethod
End Function
%>
