<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/analysis.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<%
' =============================================================================
' _additem.asp
' Tween page for adding item to the user's basket
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim mscsOrderGrp, mscsCatalog, dictItem
	Dim sCatalogName, sCategoryName, sProductID, sVariantID, sOrderFormToAddTo
    Dim iProductQty, iErrorLevel
    Dim arrParams, arrParamVals
    
	Call EnsureAccess()
	
	sCategoryName = GetRequestString(CATEGORY_NAME_URL_KEY, "")
    sProductID = GetRequestString(PRODUCT_ID_URL_KEY, Null)
    sVariantID = GetRequestString(VARIANT_ID_URL_KEY, Null)
    iProductQty = GetProductQuantity()
	
	' Get the catalog
	sCatalogName = GetRequestString(CATALOG_NAME_URL_KEY, Null)
	Set mscsCatalog = GetCatalogForUser(sCatalogName, m_UserID)	
	
    If IsNull(sCatalogName) Or IsNull(sProductID) Then
        Response.Redirect(GenerateURL(MSCSSitePages.BadURL, Array(), Array()))
    End If

	Set mscsOrderGrp = LoadBasket(m_UserID)

	Set dictItem = Server.CreateObject("Commerce.Dictionary") 
	dictItem.product_catalog = sCatalogName
	dictItem.product_catalog_base = mscsCatalog.BaseCatalogName
	dictItem.product_id = sProductID
	dictItem.Quantity = iProductQty
	dictItem.product_category = sCategoryName
	
	If Not IsNull(sVariantID) Then
	    dictItem.product_variant_id = sVariantID
	End If
	
	' Add the item to the appropriate OrderForm
	ApplyVendorInfo dictItem, sCatalogName
	Call mscsOrderGrp.AddItem(dictItem)		
		
	' Note: you may want to run pipeline here if the site is structured not to redirect to basket,
	' for example if you need to keep a total up-to-date, or if you want to notify users 
	' of basket warnings (e.g. product or discount no longer available) immediately

	Call mscsOrderGrp.SaveAsBasket()

	Call Analysis_LogAddToBasket( _
	                                sCatalogName, _
	                                sCategoryName, _
	                                sProductID, _
	                                sVariantID _
	                            )
	' Redirect to desired page
	Select Case (dictConfig.i_AddItemRedirectOptions)
		Case REDIRECT_TO_BASKET
			Call Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))     
		Case REDIRECT_TO_PRODUCT
			arrParams = Array(CATALOG_NAME_URL_KEY, PRODUCT_ID_URL_KEY, PRODUCT_ACTION_KEY)
			arrParamVals = Array(sCatalogName, sProductID, ADD_ACTION)
			Call Response.Redirect(GenerateURL(MSCSSitePages.Product, arrParams, arrParamVals))     
	End Select
End Sub


' -----------------------------------------------------------------------------
' ApplyVendorInfo
'
' Description:
'   This function looks up the vendor id, the vendor qualifier, and the vendor qualifier value 
'	for the specified catalog, using the CatalogToVendorAssociation API.
'   If i_BizTalkOptions is set to use BizTalk PO Xfer, it will return
'   the vendor id associated with the catalog
'
' Parameters:
'   dictItem		- the item that is being added to the basket
'	sCatalogName	- the catalog from which the item was selected
'
' Returns:
'   changes dictItem by reference
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub ApplyVendorInfo(dictItem, sCatalogName)
	Dim oCatalogToVendorAssociation, rs
	
	If dictConfig.i_BizTalkOptions = BIZTALK_PO_XFER_ENABLED Then
		Set oCatalogToVendorAssociation = Server.CreateObject("Commerce.CatalogToVendorAssociation")
		Call oCatalogToVendorAssociation.Initialize(dictConfig.s_CatalogConnectionString)
		Set rs = oCatalogToVendorAssociation.GetVendorInfoForCatalog(sCatalogName)
		
		If rs.EOF Then
			dictItem.vendorID = DEFAULT_ORDERFORM
			dictItem.vendor_qual = ""
			dictItem.vendor_qual_value = ""
		Else
			dictItem.vendorID = rs("VendorName").Value
			dictItem.vendor_qual = rs("VendorQualifier").Value
			dictItem.vendor_qual_value = rs("VendorID").Value
		End If		
	Else
		dictItem.vendorID = sCatalogName
		dictItem.vendor_qual = ""
		dictItem.vendor_qual_value = ""
	End If
End Sub

' -----------------------------------------------------------------------------
' GetProductQuantity
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetProductQuantity()
	Dim iQty

    iQty = MSCSAppFrameWork.RequestNumber(PRODUCT_QTY_URL_KEY, Null)
    
    If IsNull(iQty) Then
		GetProductQuantity = 1
	ElseIf Not IsNumberInRange(iQty, 1, 999) Then
		GetProductQuantity = 1
	Else
		GetProductQuantity = iQty
	End If
End Function

%>
