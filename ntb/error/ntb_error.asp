<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->

<%Response.Buffer=True%>
<%
'***********************************************************************************
' error/ntb_error.asp
'
' NTB General error file for Runtime errors with no need for new logon.
'
' NOTES:
'		'none'
'
' CREATED BY: Roar Vestre
' CREATED DATE: 2002.07.04
' REVISION HISTORY:
'		'none'
'
'***********************************************************************************
%>

<html>
<head>
<%=Titletag()%>
<link rel="stylesheet" type="text/css" href="../include/ntb.css">
</HEAD>
<BODY>
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td><%=RenderTitleBar("Feilmelding", "NoPhoto", 602)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<%

		CONST	L_Default_ErrorMessage = "Det er oppst�tt en feil! Feilnr.: "
		CONST	L_NewAccount_ErrorMessage = "Du er nettopp blitt registrert som ny bruker. Du m� vente i ca. 5 minutter f�r du kan endre din profil!"
		CONST	L_CreatedNewUserAccount_ErrorMessage = "Brukeren er nettop blitt registrert som ny bruker. Du m� vente i ca. 5 minutter f�r du kan endre p� brukerprofilen!"

		Dim fIsAuth
		Dim retAsp
		Dim strAuthErr, strUrl

		strAuthErr = Request.QueryString("Err")
		If Not (strAuthErr = "" Or IsNull(strAuthErr)) then
			Select Case  strAuthErr
				Case "newuser"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_NewAccount_ErrorMessage&"</div>"&"<BR>")
				Case "creatednewuser"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_CreatedNewUserAccount_ErrorMessage&"</div>"&"<BR>")
				Case Else
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_Default_ErrorMessage&strAuthErr&"</div>"&"<BR>")
			End Select
		else
			Response.Write ("<BR>"&"<div class=main_heading2>"&L_UserAuthentication_ErrorMessage&"</div>"&"<BR>")
		End if%>
		<br>
		<div class="news_heading">
		<a href="..\Mainpages\MainsectionPages.asp">G� tilbake til hovedsiden</a>
	</td>
</tr>
</table>
</BODY>
</HTML>