
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->

<%

Response.Buffer=True

'copy from authfiles/error.asp
'have to have an error-message without destroying the session

Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>

<html>
<head>
<%=Titletag()%>
<link rel="stylesheet" type="text/css" href="<%=style%>">
</HEAD>
<BODY>
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td><%=RenderTitleBar("Feilmelding", "NoPhoto", 450)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<%
		'destroy the session
		'Session.Abandon

		CONST	L_NoAccessToArticle_ErrorMessage = "Du har ikke tilgang til innholdet i denne kategorien.<br>Kontakt <a href='mailto:marked@ntb.no'>marked@ntb.no</a> - telefon 22 03 44 00, for mer informasjon."

		Dim fIsAuth
		Dim retAsp
		Dim strAuthErr, strUrl

		Response.Write ("<BR>"&"<div class=main_heading2 nowrap>"&L_NoAccessToArticle_ErrorMessage&"</div>"&"<BR>")
%>
	</td>
</tr>
</table>
</BODY>
</HTML>


<%'the old code from Microsoft
'<!-- #INCLUDE FILE="../include/header.asp" -->
'<!-- #INCLUDE FILE="../include/const.asp" -->
'<!-- #INCLUDE FILE="../include/html_lib.asp" -->
'<!-- #INCLUDE FILE="../include/catalog.asp" -->
'<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
'<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
'<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
'<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
'<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
'<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
'<!-- #INCLUDE FILE="../include/setupenv.asp" -->
'<!-- #INCLUDE FILE="../template/no_menu.asp" -->


' =============================================================================
' noauth.asp
' Error page: user isn't authorized to access.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

'Sub Main()
'	Dim htmTitle, htmContent, htmLinkText, urlLink

'	htmTitle = RenderText(mscsMessageManager.GetMessage("L_Access_Not_Authorized_HTMLTitle", sLanguage), _
'		MSCSSiteStyle.Title) & CRLF

'	htmContent = RenderText(mscsMessageManager.GetMessage("L_ERROR_NOAUTH_CAUSE_TEXT", sLanguage), _
'		MSCSSiteStyle.Warning)	& CRLF

'	urlLink = GenerateURL(MSCSSitePages.Home, Array(), Array())
'	htmLinkText = RenderText(mscsMessageManager.GetMessage("L_GoTo_HomePage_Link_HTMLText", sLanguage), MSCSSiteStyle.Body)

'	htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)
'	htmPageContent = htmTitle & htmContent
'End Sub
%>