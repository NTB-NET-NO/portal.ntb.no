<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->

<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../template/no_menu.asp" -->
<%
' =============================================================================
' profile.asp
' Error page: User's profile is missing.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================
Sub Main

End Sub

%>
<HTML>
<HEAD>
<%=Titletag()%>
<LINK href="../include/ntb.css" type="text/css" rel="stylesheet">
</HEAD>
<BODY>
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td><%=RenderTitleBar("Feilmelding", "NoPhoto", 602)%><br>
	</td>
</tr>
<tr>
	<td align="center">

		<div class="news_heading">En feil oppstod!</div>
		<br>
		<%
		If dictConfig.i_FormLoginOptions = USE_IIS_AUTH Then
			Response.Write ("<BR>"&"<div class=main_heading2>"& mscsMessageManager.GetMessage("L_Profile_Is_Missing_ErrorMessage", sLanguage)&"</div>"&"<BR>")
			Response.Write ("<BR>"&"<div class=main_heading2>"& mscsMessageManager.GetMessage("L_IISAuthLoginTip_HTMLText", sLanguage)&"</div>"&"<BR>")
		Else
			Response.Write ("<BR>"&"<div class=main_heading2>"& mscsMessageManager.GetMessage("L_Profile_Is_Missing2_ErrorMessage", sLanguage)&"</div>"&"<BR>")
		End If
		%>
		<br>
		<div class="news_heading">
		<a href="../Authfiles/login.asp">G� tilbake til siden for innlogging</a>
	</td>
</tr>
</table>
</BODY>
</HTML>