<!-- #INCLUDE FILE="../include/header.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../template/layout1.asp" -->
<%
' =============================================================================
' construc.asp
' Display the message that the site is under construction.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim htmTitle, htmContent
	
	sPageTitle = MSCSMessageManager.GetMessage("L_Site_Under_Construction_HTMLTitle", sLanguage)
	

	htmTitle = RenderText(sPageTitle, _
	    MSCSSiteStyle.Title) & CRLF
	
	htmContent = RenderText(mscsMessageManager.GetMessage("L_ERROR_UNDER_CONSTRUCTION_TEXT", sLanguage), _
	    MSCSSiteStyle.Body)

	htmPageContent = htmTitle & htmContent
End Sub
%>
