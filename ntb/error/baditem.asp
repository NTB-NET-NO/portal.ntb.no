<!-- #INCLUDE FILE="../include/header.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../template/layout1.asp" -->
<%
' =============================================================================
' baditem.asp
' Error page for basket/product details
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' https_secured   = no
' required_params = none
' optional_params = none
Sub Main()
	Dim htmTitle, htmContent
	Dim arrItem

	htmTitle = RenderText(mscsMessageManager.GetMessage("L_Bad_Catalog_Entity_ErrorMessage", sLanguage), _
		MSCSSiteStyle.Title) & CRLF
	
	arrItem = Array(mscsMessageManager.GetMessage("L_Obsolete_Catalog_Link_HTMLText", sLanguage), _
		mscsMessageManager.GetMessage("L_Obsolete_Bookmark_Entry_HTMLText", sLanguage), mscsMessageManager.GetMessage("L_Bad_Catalog_URL_HTMLText", sLanguage))
		
	htmContent = RenderText(mscsMessageManager.GetMessage("L_Catalog_Entity_Unavailable_ErrorMessage", sLanguage), _
		MSCSSiteStyle.Warning)
		
	htmContent = htmContent & RenderUnorderedList(arrItem, _
		MSCSSiteStyle.Body) 

	htmPageContent = htmTitle & htmContent
End Sub
%>







