<!-- #INCLUDE FILE="../include/header.asp" -->
<%
  Const lngMaxFormBytes = 200
  Const L_TITLE_TEXT = "The page cannot be displayed"
  Const L_ERRORINFO_TEXT = "We apologize for the inconvenience.  Please try again later.  If the problem persists, please contact our webmaster."
  Const L_ERRORTYPE_TEXT = "Error Type:"
  CONST L_BROWSERTYPE_TEXT = "Browser Type:"
  CONST L_PAGE_TEXT = "Page:"
  CONST L_POSTDATA_TEXT = "POST Data:"
  CONST L_TIME_TEXT = "Time:"
  CONST L_LINE_TEXT = "Line:"
  CONST L_FILE_TEXT = "File:"
  CONST L_MSCSENVNOTE_TEXT = "Note: Set Application(""MSCSEnv"") = DEVELOPMENT to turn on client error messages."
  CONST L_ERRORHANDLINGNOTE_TEXT = "Note: Please see error\500error.asp to further customize error messages."

  Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
  Dim strMethod, lngPos, datNow, strQueryString, strURL, bFixupGlobalAsa, oEventLog, bLogToEventLog, bShowTechnicalInfo, bDevelopmentMode

  If Response.Buffer Then
    Response.Clear
    Response.Status = "500 Internal Server Error"
    Response.ContentType = "text/html"
    Response.Expires = 0
  End If

  Set objASPError = Server.GetLastError
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<html dir=ltr>
<head>
<style>
a:link			{font:8pt/11pt verdana; color:FF0000}
a:visited		{font:8pt/11pt verdana; color:#4e4e4e}
</style>
<META NAME="ROBOTS" CONTENT="NOINDEX">
<title><% = L_TITLE_TEXT %></title>
</head>


<!--- FRIENDLY GENERIC ERROR MESSAGE GOES HERE -->

<body bgcolor="FFFFFF">
<h1><% = Application("MSCSCommerceSiteName") %></h1>
<table width="410" cellpadding="3" cellspacing="5">
  <tr>
    <td align="left" valign="middle" width="360">
	<h1 style="COLOR:000000; FONT: 13pt/15pt verdana"><% = L_TITLE_TEXT%></h1>
    </td>
  </tr>

  <tr>
    <td width="400" colspan="2">
	<font style="COLOR:000000; FONT: 8pt/11pt verdana"><% = L_ERRORINFO_TEXT %></font></td>
  </tr>
</table>

<hr>

<%

' --- Do special work the first time global.asa error is detected
If IsEmpty(Application("MSCSErrorInGlobalASA")) Then
   Application("MSCSErrorInGlobalASA") = True
   Application("GlobalAsaErrorText") = RenderDetailedInfoList()
   bFixupGlobalAsa = True
Else
   bFixupGlobalAsa = False
End If


' --- Determine if the site was in dev mode... Assume production mode if unspecified.  Also, take no dependencies on constants being available
bDevelopmentMode = Not IsEmpty(Application("MSCSEnv")) And (Application("MSCSEnv")=0)

' --- We will log to the eventlog upon first global.asa error, or for any other error
bLogToEventLog = Not bDevelopmentMode And (Not Application("MSCSErrorInGlobalASA") Or bFixupGlobalAsa = True)

' --- We will show technical info to browser in DevelopmentMode mode
bShowTechnicalInfo = bDevelopmentMode

' --- Log to the EventLog
If bLogToEventLog Then
      Dim sError
      sError = vbCR & _
               L_ERRORTYPE_TEXT & " " & objASPError.Category & vbCR & _
               L_PAGE_TEXT & " " & Request.ServerVariables("SCRIPT_NAME") & vbCR & _
               L_FILE_TEXT & " " & objASPError.File & vbCR & _
               L_LINE_TEXT & objASPError.Line & vbCR & _
               Hex(objASPError.Number) & vbCR & _
               objASPError.Description & vbCR

      If Not bShowTechnicalInfo Then ' If not showing technical info, tell them how they can see it
          sError = sError & L_MSCSENVNOTE_TEXT & vbCR
      End If

      sError = sError & L_ERRORHANDLINGNOTE_TEXT & vbCR

      On Error Resume Next
          Set oEventLog = CreateObject("Commerce.AdminEventLog")
          ' Note: you can use oEventLog.Initialize to log to a remote machine
          Call oEventLog.WriteErrorEvent(Application("MSCSCommerceSiteName"), sError)
      On Error Goto 0
End If

' --- Display Technical Error Info
If bShowTechnicalInfo Then
    If Application("MSCSErrorInGlobalASA") Then
        Response.Write Application("GlobalAsaErrorText")
    Else
        Response.Write RenderDetailedInfoList()
    End If
End If

%>

</body>
</html>


<%
' Helper Functions for detailed error messages

Function RenderDetailedInfoList()
  Dim htm

  htm = "<ul>"

  htm = htm & "<li>" & L_ERRORTYPE_TEXT & "<br>"
  htm = htm & RenderErrorType()
  htm = htm & "</li><p>"

  htm = htm & "<li>" & L_BROWSERTYPE_TEXT & "<br>"
  htm = htm & Request.ServerVariables("HTTP_USER_AGENT")
  htm = htm & "</li><p>"

  htm = htm & "<li>" & L_PAGE_TEXT & "<br>"
  htm = htm & RenderPageInfo()
  htm = htm & "</li><p>"

  If strMethod = "POST" Then
    htm = htm & "<li>" & L_POSTDATA_TEXT & "<br>"
    htm = htm & RenderPostData()
    htm = htm & "</li><p>"
  End If

  htm = htm & "<li>" & L_TIME_TEXT & "<br>"
  htm = htm & RenderTime()
  htm = htm & "</li><p>"

  htm = htm & "</ul><p>"

  RenderDetailedInfoList = htm
End Function

Function RenderErrorType()
  RenderErrorType = RenderErrorType & Server.HTMLEncode(objASPError.Category)
  If objASPError.ASPCode > "" Then RenderErrorType = RenderErrorType & Server.HTMLEncode(", " & objASPError.ASPCode)
  RenderErrorType = RenderErrorType & Server.HTMLEncode(" (0x" & Hex(objASPError.Number) & ")" ) & "<br>"

  If objASPError.ASPDescription > "" Then
		RenderErrorType = RenderErrorType & Server.HTMLEncode(objASPError.ASPDescription) & "<br>"

  elseIf (objASPError.Description > "") Then
		RenderErrorType = RenderErrorType & Server.HTMLEncode(objASPError.Description) & "<br>"
  end if

  blnErrorWritten = False

  ' Only show the Source if it is available and the request is from the same machine as IIS
  If objASPError.Source > "" Then
    strServername = LCase(Request.ServerVariables("SERVER_NAME"))
    strServerIP = Request.ServerVariables("LOCAL_ADDR")
    strRemoteIP =  Request.ServerVariables("REMOTE_ADDR")
    If (strServername = "localhost" Or strServerIP = strRemoteIP) And objASPError.File <> "?" Then
      RenderErrorType = RenderErrorType & Server.HTMLEncode(objASPError.File)
      If objASPError.Line > 0 Then RenderErrorType = RenderErrorType &  ", line " & objASPError.Line
      If objASPError.Column > 0 Then RenderErrorType = RenderErrorType &  ", column " & objASPError.Column
      RenderErrorType = RenderErrorType & "<br>"
      RenderErrorType = RenderErrorType & "<font style=""COLOR:000000; FONT: 8pt/11pt courier new""><b>"
      RenderErrorType = RenderErrorType & Server.HTMLEncode(objASPError.Source) & "<br>"
      If objASPError.Column > 0 Then RenderErrorType = RenderErrorType & String((objASPError.Column - 1), "-") & "^<br>"
      RenderErrorType = RenderErrorType &  "</b></font>"
      blnErrorWritten = True
    End If
  End If

  If Not blnErrorWritten And objASPError.File <> "?" Then
    RenderErrorType = RenderErrorType & "<b>" & Server.HTMLEncode(objASPError.File)
    If objASPError.Line > 0 Then RenderErrorType = RenderErrorType & Server.HTMLEncode(", line " & objASPError.Line)
    If objASPError.Column > 0 Then RenderErrorType = RenderErrorType & ", column " & objASPError.Column
    RenderErrorType = RenderErrorType & "</b><br>"
  End If
End Function

Function RenderPageInfo()
  strMethod = Request.ServerVariables("REQUEST_METHOD")

  RenderPageInfo = strMethod & " "

  RenderPageInfo = RenderPageInfo & Request.ServerVariables("SCRIPT_NAME")
End Function

Function RenderPostData()
    If Request.TotalBytes > lngMaxFormBytes Then
      RenderPostData = Server.HTMLEncode(Left(Request.Form, lngMaxFormBytes)) & " . . ."
    Else
      RenderPostData = Server.HTMLEncode(Request.Form)
    End If
End Function

Function RenderTime()
  datNow = Now()

  RenderTime = Server.HTMLEncode(FormatDateTime(datNow, 1) & ", " & FormatDateTime(datNow, 3))
End Function

%>