<!-- #INCLUDE FILE="../include/header.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../template/no_menu.asp" -->
<%
' =============================================================================
' badurl.asp
' Error page
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim htmTitle, htmContent, htmLinkText, urlLink
	Dim arrItem

	
	htmTitle = RenderText(mscsMessageManager.GetMessage("L_Bad_URL_Page_HTMLTitle", sLanguage), _
		MSCSSiteStyle.Title) & CRLF
	
	arrItem = Array(mscsMessageManager.GetMessage("L_ERROR_BADURL_CAUSE1_TEXT", sLanguage), _
		mscsMessageManager.GetMessage("L_ERROR_BADURL_CAUSE2_TEXT", sLanguage), mscsMessageManager.GetMessage("L_ERROR_BADURL_CAUSE3_TEXT", sLanguage))
		
	htmContent = RenderText(mscsMessageManager.GetMessage("L_ERROR_BADURL_CAUSE_TEXT", sLanguage), _
		MSCSSiteStyle.Warning)
		
	htmContent = htmContent & RenderUnorderedList(arrItem, _
		MSCSSiteStyle.Body) 
	
	urlLink = GenerateURL(MSCSSitePages.Home, Array(), Array())
	htmLinkText = RenderText(mscsMessageManager.GetMessage("L_GoTo_HomePage_Link_HTMLText", sLanguage), MSCSSiteStyle.Body)
	htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)

	htmPageContent = htmTitle & htmContent
End Sub
%>