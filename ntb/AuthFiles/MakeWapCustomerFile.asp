<%@ Language=VBScript %>
<%

dim intRowNum

MakeCustomerFile

Sub MakeCustomerFile()
	'*** Lager fullstendig Customer fil:
	dim strWapFile, objFile
	dim FSO
	dim cn, rsCustomer

	set FSO = Server.Createobject("Scripting.FileSystemObject")

	'strWapFile = Application("WAP_CustomerFile")
	strWapFile = "E:\WAP_Customer_Information.txt"

	Set objFile = FSO.CreateTextFile(strWapFile, True)
	'On Error Resume Next

	set cn = Server.Createobject("ADODB.Connection")
    cn.ConnectionTimeout = Application("Wap_Kunder_ConnectionTimeout")
    cn.CommandTimeout = Application("Wap_Kunder_CommandTimeout")
    cn.CursorLocation = adUseClient
	cn.ConnectionString = Application("wap_Kunder_ConnectionString")
	cn.Open

    set rsCustomer = Server.Createobject("ADODB.Recordset")
    rsCustomer.CursorLocation = adUseClient
	rsCustomer.CursorType = adOpenStatic
	'rsCustomer.LockType = adLockBatchOptimistic

    Set rsCustomer.ActiveConnection = cn
	rsCustomer.Open "select * from Customer"
    Set rsCustomer.ActiveConnection = Nothing
	cn.Close

	with rsCustomer
		do until .EOF
			intRowNum = intRowNum + 1
			'left(strVerdi, 4) <> "Alle"

			strSlutt = ">"
			strFullName = "<Customer=" & .Fields("Customer") & strSlutt
			strMobilphone = "<Mobilphone=" & .Fields("Mobilphone").Value & strSlutt
			if .Fields("SMS").Value = "True" then
				strblnSMS = "<SMS=Sann" & strSlutt
			else
				strblnSMS = "<SMS=Usann" & strSlutt
			end if

			if .Fields("WAP").Value = "True" then
				strblnWAP = "<WAP=Sann" & strSlutt
			else
				strblnWAP = "<WAP=Usann" & strSlutt
			end if

			strPrio13Stoffgr = "<Prio13Stoffgr=" & Trim(.Fields("Prio13Stoffgr").Value) & strSlutt
			if trim(.Fields("NTBSniffertxt").Value) <> "" then
				strblnSniffertxt = "<blnSniffertxt=Sann" & strSlutt
			else
				strblnSniffertxt = "<blnSniffertxt=Usann" & strSlutt
			end if

			if Right(.Fields("NTBSniffertxt").Value, 1) <> ";" And trim(.Fields("NTBSniffertxt").Value) <> "" then
				strNTBSniffertxt = "<NTBSniffertxt=" & .Fields("NTBSniffertxt").Value & ";" & strSlutt
			else
				strNTBSniffertxt = "<NTBSniffertxt=" & .Fields("NTBSniffertxt").Value & strSlutt
			end if

			strNTBSniffertxt = Replace(strNTBSniffertxt,"; ", ";")
			strNTBSniffertxt = Replace(strNTBSniffertxt," ;", ";")

			strNTBStoffgrupper = "<NTBStoffgrupper=" & .Fields("NTBStoffgrupper").Value & strSlutt
			strNTBOmraader = "<NTBOmraader=" & .Fields("NTBOmraader").Value & strSlutt
			strNTBFylker = "<NTBFylker=" & .Fields("NTBFylker").Value & strSlutt
			strNTBHovedKategorier = "<NTBHovedKategorier=" & .Fields("NTBHovedKategorier").Value & strSlutt
			strNTBUnderKatSport = "<NTBUnderKatSport=" & .Fields("NTBUnderKatSport").Value & strSlutt
			strNTBUnderKatOkonomi = "<NTBUnderKatOkonomi=" & .Fields("NTBUnderKatOkonomi").Value & strSlutt
			strNTBUnderKatKuriosa = "<NTBUnderKatKuriosa=" & .Fields("NTBUnderKatKuriosa").Value & strSlutt
			strNTBUnderKatKultur = "<NTBUnderKatKultur=" & .Fields("NTBUnderKatKultur").Value & strSlutt

			strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kongestoff;", "Kuriosa;")
			strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa;", "Kuriosa2;", 1, 1)
			strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa;", "")
			strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa2;", "Kuriosa;")

			strNTBOmraader = Replace(strNTBOmraader, "Norge;", "Norge2;", 1, 1)
			strNTBOmraader = Replace(strNTBOmraader, "Norge;", "")
			strNTBOmraader = Replace(strNTBOmraader, "Norge2;", "Norge;")

			strNTBHovedKategorier = Replace(strNTBHovedKategorier, "AlleHov;", "")
			strNTBOmraader = Replace(strNTBOmraader, "AlleOmr;", "")
			strNTBFylker = Replace(strNTBFylker, "AlleFyl;", "")
			strNTBUnderKatKultur = Replace(strNTBUnderKatKultur, "AlleKul;", "")
			strNTBUnderKatOkonomi = Replace(strNTBUnderKatOkonomi, "AlleOko;", "")
			strNTBUnderKatSport = Replace(strNTBUnderKatSport, "AlleSpo;", "")

			objFile.WriteLine strFullName & strMobilphone & strblnSMS & strblnWAP & strPrio13Stoffgr & strblnSniffertxt & strNTBSniffertxt & strNTBStoffgrupper & strNTBOmraader & strNTBFylker & strNTBHovedKategorier & strNTBUnderKatSport & strNTBUnderKatOkonomi & strNTBUnderKatKuriosa & strNTBUnderKatKultur
			.MoveNext
		loop
	End With

	objFile.Close
	rsCustomer.Close
	Set rsCustomer = Nothing
	set FSO = Nothing
End Sub
%>
<html>
<body>
Wap Kundefil ferdig skrevet med <%=intRowNum%> poster.
</body>
</html>

