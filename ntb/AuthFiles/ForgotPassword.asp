<%
'***********************************************************************************
' ForgotPassword.asp
'
' This file is used to send the password to a user by username
'
' USES:
'		NTB_layout_lib.asp - For rendering of titlebar
'
'
' NOTES:
'		none
'
' CREATED BY: �yvind Andersen 
' CREATED DATE: 2003.11.24
' UPDATED BY: 
' UPDATED DATE: 
' REVISION HISTORY:
'
'
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_admin_render_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/addr_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<%
Sub Main()

End Sub
%>
<HTML>
<HEAD>
<LINK href="ntb.css" type="text/css" rel="stylesheet">
<%=Titletag()%>
<SCRIPT language="javascript">
<!--
//function to detect the users screen resolution and setting it in a hidden field
function setFocus(){
	document.frmLogin.txtUsername.focus()
}
-->
</SCRIPT>
</HEAD>
<BODY onLoad="setFocus()">
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td>
		<%=RenderTitleBar("Glemt passord?", "NoPhoto", 620)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<%Response.Write(RenderForgotPassword())%>
	</td>
</tr>
</table>
</BODY>
</HTML>
