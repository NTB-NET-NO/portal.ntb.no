<%@ LANGUAGE="VBSCRIPT"%>

<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/addr_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->

<%
Sub Main ()

	' Following code is moved to Session_OnEnd in Global.asa:
	Dim oAuthManagerObject
	Set oAuthManager = GetAuthManagerObject()
	Call oAuthManager.SetAuthTicket ("", true, 0)
	Set oAuthManager = nothing

	Response.Cookies("ntbUserID") = ""
	Response.Cookies("ntbPassword") = ""
	Response.cookies("ntbLastRequestedURL") = ""
	
	Session.Abandon

	Response.Redirect "../Authfiles/Login.asp"

End Sub
%>