<%
'***********************************************************************************
' iframe.asp
'
' Calls different functions based on several parameters to retrieve the correct
' news lists. Some javascripts exists to make the news list "stand still" during
' page refresh and to open news items in a seperate window.
'
' USES:
'		NTB_cache_lib.asp - For generating content in the different news list
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen
' CREATED DATE: 2002.04.09
' UPDATED BY: Trond Orrestad, Andersen
' UPDATED DATE: 2002.04.09
' REVISION HISTORY:
' UPDATED BY: Roar Vestre, NTB
' UPDATED DATE: 2002.05.27: Added "Vis siste 24 timer"-link on top of iframe
' UPDATED 2003.03.17, By Roar Vestre, Added List "Logger"
' UPDATED 2003.05.03, By Roar Vestre, Added List "Direkte"
'
'**********************************************************************************
%>

<!-- #INCLUDE FILE="../include/NTB_cache_lib.asp" -->

<%
Dim style
'if Session("Browser") = "" then
'	Response.Redirect "../Authfiles/Login.asp"
'elseif ....
if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="<%=style%>">
</head>
<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7" onload="setScroll()" >
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="../include/overlib.js"></script>
<%
Dim PhotoOption
Dim TimePeriod
Dim ListType
Dim intSakNr

If Request.QueryString("timePeriod")<>"" Then
	If Request.QueryString("timePeriod") = "48" Then
      TimePeriod = "48"
    Else
	  TimePeriod = "24"
    End If
End If

If Request.QueryString("listType")<>"" Then
      'Response.Write ("Her kommer nyheter innen " & Request.QueryString("listType") & "!<br>")
      ListType = Request.QueryString("listType")
End If

If Request.QueryString("photoOption")<>"" Then
	If Request.QueryString("photoOption") = "Yes" Then
      PhotoOption = "Yes"
    Else
	  PhotoOption = "No"
    End If
End If

dim refresh
dim title

If Request.QueryString("refresh") <> "" Then
	refresh = Request.QueryString("refresh") * 1000
else
	refresh = 60000
end if

If Request.QueryString("title") <> "" Then
	title = Request.QueryString("title")
else
	title = ""
end if


'set refreshRate to 60 seconds as default for all lists (used in JavaScript at the bottom of the page)
Response.Write ("<script language=""JavaScript"">var refreshRate = "& refresh &"</script>")

If (title <> "") Then
	'Display custom header
	Response.Write ("<div class=""news""><b>" & title & "</b></div>")
	'Response.Write ("<br>")
End if


If (timeperiod = "48") Then
	'Show first 24 hours link if the news list displays last 48 hours -->
	Response.Write ("<div class=""news"">Vis <a href=""vg-iframe_noauth.asp?timePeriod=24&listType=" & ListType & "&photoOption=" & PhotoOption & """ target=_self> siste</a> 24 timer</div>")
End if


Select Case listType
	Case "alle_nyheter"
		'stoffgruppe Innenriks
		Response.Write(getArticleList_v2("alle_nyheter", PhotoOption, TimePeriod))
	Case "alle_innenriks"
		'stoffgruppe Innenriks
		Response.Write(getArticleList_v2("innenriks_alle", PhotoOption, TimePeriod))
	Case "alle_sport"
		'stoffgruppe Sport
		Response.Write(getArticleList_v2("sport_alle", PhotoOption, TimePeriod))
	Case "alle_utenriks"
		'stoffgruppe Utenriks
		Response.Write(getArticleList_v2("utenriks_alle", PhotoOption, TimePeriod))
	Case "innenriks_alle"
		'stoffgruppe Innenriks
		Response.Write(getArticleList_v2("innenriks_alle", PhotoOption, TimePeriod))
	Case "innenriks_politikk"
		'stoffgruppe Innenriks hovedkategori Politikk
		Response.Write(getArticleList_v2("innenriks_politikk", PhotoOption, TimePeriod))
	Case "innenriks_okonomi_naeringsliv"
		'stoffgruppe Innenriks hovedkategori "�konomi og n�ringsliv" og "Arbeidsliv"
		Response.Write(getArticleList_v2("innenriks_okonomi", PhotoOption, TimePeriod))
	Case "innenriks_krim_ulykker"
		'stoffgruppe Innenriks hovedkategori "Kriminalitet og rettsvesen" og "Ulykker og naturkatastrofer"
		Response.Write(getArticleList_v2("innenriks_krim", PhotoOption, TimePeriod))
	Case "utenriks_alle"
		'stoffgruppe Utenriks, except undergruppe "Bakgrunn" og "Analyse" og "Faktaboks" og "Oversikter"
		Response.Write(getArticleList_v2("utenriks_filtered", PhotoOption, TimePeriod))
	Case "utenriks_bakgrunn"
		'stoffgruppe Utenriks undergruppe "Bakgrunn" og "Analyse" og "Faktaboks" og "Oversikter" og "Menyer, til red"
		Response.Write(getWeeklyItemsList_v2("utenriks_bakgrunn", PhotoOption))
	Case "okonomi_innenriks"
		'stoffgruppe Innenriks, category "�konomi og n�ringsliv" and "Arbeidsliv"
		Response.Write(getArticleList_v2("innenriks_okonomi", PhotoOption, TimePeriod))
	Case "okonomi_utenriks"
		'stoffgruppe Utenriks, category "�konomi og n�ringsliv" and "Arbeidsliv"
		Response.Write(getArticleList_v2("okonomi_utenriks", PhotoOption, TimePeriod))
	Case "sport_alle"
		'stoffgruppe Sport, except undergruppe "Tabeller og resultater"
		Response.Write(getArticleList_v2("sport_filtered", PhotoOption, TimePeriod))
	Case "sport_tabeller"
		'stoffgruppe Sport, with undergruppe "Tabeller og resultater"
		Response.Write(getArticleList_v2("sport_tabeller", PhotoOption, TimePeriod))
	Case "kul_reportasjer"
		'stoffgruppe Kultur og underholdning, with undergruppe different from "Oversikter og notiser" and "Menyer, til red"
		Response.Write(getWeeklyItemsList_v2("kul_reportasjer", PhotoOption))
	Case "kul_notiser"
		'stoffgruppe Kultur og underholdning, with undergruppe "Notiser"
		Response.Write(getWeeklyItemsList_v2("kul_notiser", PhotoOption))
	Case "kul_menyer"
		'stoffgruppe Kultur og underholdning, with undergruppe "Menyer, til red"
		Response.Write(getWeeklyItemsList_v2("kul_menyer", PhotoOption))
	Case "priv_menyer"
		'undergruppe "Menyer, til red" (no stoffgruppe) Helen 2103: Should not be messages with stoffgruppe Priv til red
		Response.Write(getWeeklyItemsList_v2("priv_menyer","")) ', PhotoOption, TimePeriod))
	Case "priv_tilred"
		'stoffgruppe Priv til red, except undergruppe "Menyer, til red" and "PRM-tjenesten" Helen 2103:Should incl undergruppe "Menyer, til red"
		Response.Write(getWeeklyItemsList_v2("priv_tilred","")) ', PhotoOption, TimePeriod))
	Case "prm_ntbpluss"
		'stoffgruppe Formidlingstjenester, undergruppe "PRM-tjenesten"
		Response.Write(getWeeklyItemsList_v2("prm_ntbpluss",""))
	Case "prm_nwa"
		'stoffgruppe Formidlingstjenester, undergruppe "NWA"
		Response.Write(getWeeklyItemsList_v2("prm_nwa",""))
	Case "prm_bwi"
		'stoffgruppe Formidlingstjenester, undergruppe "BWI"
		Response.Write(getWeeklyItemsList_v2("prm_bwi",""))
	Case "direkte"
		'Direkte
		Response.Write(getLoggerList("direkte", PhotoOption, TimePeriod))
	Case "logger"
		'Logger
		Response.Write(getLoggerList("logger", PhotoOption, TimePeriod))
	Case "sak_alle"
		'stoffgruppe Alle
		intSakNr = Request.QueryString("SakNr")
		Response.Write(getKeyWordItemsList("sak_alle", PhotoOption, intSakNr))
	Case "sak_bakgrunn"
		'stoffgruppe Alle, undergruppe "Bakgrunn" og "Analyse" og "Faktaboks" og "Oversikter"
		intSakNr = Request.QueryString("SakNr")
		Response.Write(getKeyWordItemsList("sak_bakgrunn", PhotoOption, intSakNr))
	Case "pr_sounds"
		'alle med lyd
		'set refreshRate to 10 minutes for the sound list
		Response.Write ("<script language=""JavaScript"">refreshRate = 600000</script>")
		Response.Write ("<script language=""javascript"" src=""http://194.19.39.29/kunde/ntb/flashsound.js""></script>")
		Response.Write ("<table width='98%'>")
		Response.Write(getWeeklyItemsList("sounds",""))
		Response.Write ("</table>")
End Select


Response.Write ("<br>")	'add a line break before links

'Show previous 48 hours link if the news list displays last 24 hours (do not show this link for PRM mesages)-->
If (Left(listType,2) <> "pr") AND (Left(listType,3) <> "kul") AND (Left(listType,3) <> "sak" AND listType <> "utenriks_bakgrunn" AND listType <> "logger" AND listType <> "direkte") Then
	If (timeperiod <> "48") Then
		Response.Write ("<div class=""news"">Vis <a href=""vg-iframe_noauth.asp?timePeriod=48&listType=" & ListType & "&photoOption="  & PhotoOption & """ target=_self> forrige</a> 48 timer</div>")
	else
		'Show first 24 hours link if the news list displays last 48 hours -->
		Response.Write ("<div class=""news"">Vis <a href=""vg-iframe_noauth.asp?timePeriod=24&listType=" & ListType & "&photoOption=" & PhotoOption & """ target=_self> siste</a> 24 timer</div>")
	End if
End if
%>
</body>
<script language="JavaScript">

//function that scroll down the page to the same spot as the page
//was prior to refresh
function setScroll(){
	//a variable y is added to the url after the character zzz used for splitting purpose in the refreshTOHere-function
	//this y variable is fetched from the url-string and used to decide how much to scroll
	if(location.search){
		var tempArray=location.search.split("zzz");
		//lastArrayElement =tempArray.pop();
		lastArrayElement = tempArray[tempArray.length - 1];
		yLength = lastArrayElement.length
		y=lastArrayElement.slice(2);
		window.scrollTo(0,y);
	}
}

//function that adds a y-value indicating the amount of scrolling that the user has performed
function refreshToHere(){
	if(typeof(window.pageYOffset)=="number"){
		y=window.pageYOffset;
	}else{
		y=document.body.scrollTop;
	}
	location.href=location.href.replace(/\&zzz.*/,"")+"&zzzy="+y;
}

//set refresh rate
setTimeout("refreshToHere()",refreshRate);

//function to open news items in a separate window
var myWindow;
function w(aid) {
	var myURL;
	myURL = 'x.asp?a=' + aid;
	if(myWindow && !myWindow.closed) { // window is open
		myWindow.location = myURL;
		myWindow.focus();
	} else {
		leftPlacement = screen.width - 620; //ensures that the window pops up to the right independent on screen resolution
		myName = 'PopupPage';
		myOptions = 'height=600,width=600,scrollbars=yes,resizable=yes,top=80, left='+ leftPlacement;
		myWindow = window.open(myURL, myName, myOptions);
		myWindow.focus();
	}
}

//function to open sound html code in a separate window
var soundHtmlWindow;
function ws(aid, fname) {
	var myURL;
	myURL = '../template/sound_html.asp?id=' + aid + '\&fname=' + fname;
	if(soundHtmlWindow && !soundHtmlWindow.closed) { // window is open
		soundHtmlWindow.location = myURL;
		soundHtmlWindow.focus();
	} else {
		myName = 'SoundPopupPage';
		myOptions = 'height=600,width=480,scrollbars=yes,resizable=yes,top=80, left=10';
		soundHtmlWindow = window.open(myURL, myName, myOptions);
		soundHtmlWindow.focus();
	}
}
</script>
</html>