<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->

<%Response.Buffer=True%>

<%
REM sample file for using with AuthFilter
REM redirected to this file, for handling known Errors
REM Error would be added to QS parameter 'Err' and it could be used as explained
%>

<html>
<head>
<%=Titletag()%>
<link rel="stylesheet" type="text/css" href="ntb.css">
</HEAD>
<BODY>
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td><%=RenderTitleBar("Feilmelding", "NoPhoto", 602)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<%
		'destroy the session
		Session.Abandon 
        Response.Cookies("ntbPassword") = ""
        Response.Cookies("ntbUserID") = ""

		CONST	L_UnauthorizedLogon_ErrorMessage = "Ikke autorisert innlogging: Ikke tilgang til portalen.<br>Brukernavn eller passord er feil."
		CONST	L_LogonFailed_ErrorMessage = "Ikke autorisert innlogging: Innlogging avbrutt"
		CONST	L_ACLOnRessource_ErrorMessage = "Nektet tilgang til siden."
		CONST	L_FilterAuthorizationDenied_ErrorMessage = "Ikke autorisert innlogging: Nektet tilgang av Internet Information Services"
		CONST	L_ISAPIAuthFailure_ErrorMessage = "Ikke autorisert innlogging"
		CONST	L_CheckUserName_ErrorMessage = "Brukernavn er feil. Vennligst pr�v igjen."
		CONST	L_CheckUserPassword_ErrorMessage = "Brukernavn eller passord er feil. Vennligst pr�v igjen."
		CONST	L_UserAuthentication_ErrorMessage = "Feil oppstod i autentisering av brukeren"
		CONST	L_ToManySessions_ErrorMessage = "Du har for mange aktive brukersesjoner og m� logge ut f�r du kan �pne en ny sesjon."
		'CONST	L_HelpPassword_ErrorMessage = "Kontakt IT-ansvarlig i din organisasjon."
		CONST	L_HelpPassword_ErrorMessage = "Kontakt Rune Wikst�l, som er NTBs brukerst�tte for portalbrukere.<br/><br/>Telefon: 22 03 46 55 / 907 44 576. <br/>E-post: <a href='mailto:rune.wikstol@ntb.no'>rune.wikstol@ntb.no</a>"
		CONST 	L_SendEmail_ErrorMessage = "Noe feilet ved utsending av e-post.<p>Kontakt Rune Wikst�l, som er NTBs brukerst�tte for portalbrukere.<br/><br/>Telefon: 22 03 46 55 / 907 44 576. <br/>E-post: <a href='mailto:rune.wikstol@ntb.no'>rune.wikstol@ntb.no</a>"
		CONST 	L_SendSMS_ErrorMessage = "Noe feilet ved utsending av SMS.<p>Kontakt Rune Wikst�l, som er NTBs brukerst�tte for portalbrukere.<br/><br/>Telefon: 22 03 46 55 / 907 44 576. <br/>E-post: <a href='mailto:rune.wikstol@ntb.no'>rune.wikstol@ntb.no</a>"
		
		CONST	L_ClosedAccount_ErrorMessage = "Din bedrift er ikke lengre medlem av NTBs nyhetsportal."
		
		Dim fIsAuth
		Dim retAsp
		Dim strAuthErr, strUrl
	
		strAuthErr = Request.QueryString("Err")
		If Not (strAuthErr = "" Or IsNull(strAuthErr)) then
			Select Case  strAuthErr 
				Case "401.1"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_UnauthorizedLogon_ErrorMessage&"</div>"&"<BR>")
				Case "401.2"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_LogonFailed_ErrorMessage&"</div>"&"<BR>")
				Case "401.3"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_ACLOnRessource_ErrorMessage&"</div>"&"<BR>")
				Case "401.4"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_FilterAuthorizationDenied_ErrorMessage&"</div>"&"<BR>")
				Case "401.5"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_ISAPIAuthFailure_ErrorMessage&"</div>"&"<BR>")
				Case "session"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_ToManySessions_ErrorMessage&"</div>"&"<BR>")	
				Case "password"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_HelpPassword_ErrorMessage&"</div>"&"<BR>")
				Case "uautorisert"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_ACLOnRessource_ErrorMessage&"</div>"&"<BR>")	
				Case "closed"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_ClosedAccount_ErrorMessage&"</div>"&"<BR>")	
				Case "checkusername"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_CheckUserName_ErrorMessage&"</div>"&"<BR>")	
				Case "sendemail"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_SendEmail_ErrorMessage&"</div>"&"<BR>")	
				Case "sendsms"
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_SendSMS_ErrorMessage&"</div>"&"<BR>")	
				Case Else
					Response.Write ("<BR>"&"<div class=main_heading2>"&L_CheckUserPassword_ErrorMessage&"</div>"&"<BR>")
			End Select		
		else
			Response.Write ("<BR>"&"<div class=main_heading2>"&L_UserAuthentication_ErrorMessage&"</div>"&"<BR>")
		End if%>
		<br>
		<div class="news_heading">
		<a href="login.asp">G� tilbake til siden for innlogging</a>
	</td>
</tr>
</table>
</BODY>
</HTML>