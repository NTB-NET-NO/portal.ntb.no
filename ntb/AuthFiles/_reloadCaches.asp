<%@ Language=VBScript %>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Ralfs">
</HEAD>
<BODY>
Refreshing memory datastore and all list caches<br>
<%
Call oMemDb.LoadCache()
Response.Write("Memory cache now has articles from " & CStr(oMemDb.minDate) & _
"(" & CStr(oMemDb.minId) & ") to " & CStr(oMemDb.maxDate) & "(" & _
CStr(oMemDb.maxId) & ").<hr>")
Dim lc
Set lc = Application("ListCache")
lc.RefreshCache("prmsimplelists")

Application("PageCache").FlushAll

Session.Abandon
%>
</BODY>
</HTML>
