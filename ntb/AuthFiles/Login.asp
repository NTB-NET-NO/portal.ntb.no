<%
'***********************************************************************************
' Login.asp
'
' This file is used to ensure that all users of the site are logged on and have the rights
' requested
'
' USES:
'		NTB_layout_lib.asp - For rendering of titlebar
'
'
' NOTES:
'		none
'
' CREATED BY: Solveig Skjermo, Andersen
' CREATED DATE: 2002.04.16
' UPDATED BY: Solveig Skjermo, Andersen
' UPDATED DATE: 2002.07.01
' REVISION HISTORY:
' 2002.08.05 Lars Christian Hegde, NTB: Added bugfix: login form displayed in IFrames
' 2003.03.19: Roar Vestre, NTB: Added Session("OrgName") = strOrgName
' 2005.03.07: Roar Vestre: Added Session("Mobile") and Session("Email") for SmsWapEmail
' 2007.02.26: Richard Husevaag: Removed function call for request password by SMS
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/addr_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<%

Dim strSelect, strPassword, strSiteName, strUserID, strRetAsp
Dim strLinkEnding
Dim oProfile, objAuth, oOrg
Dim idOrg, blnAutologin, ntbLastRequestedURL
Dim bEmail, bSMS, strReturn
Dim strName, strCompany, strEmail, strMobile

Sub Main()
	if Request.cookies("ntbLastRequestedURL") = "" then
		Response.cookies("ntbLastRequestedURL") = Request.cookies("MSCSFirstRequestedURL")
		ntbLastRequestedURL = Request.cookies("MSCSFirstRequestedURL")
	else
		ntbLastRequestedURL = Request.cookies("ntbLastRequestedURL")
	end if
	'Response.write(Request.cookies("ntbLastRequestedURL") & "<br>")
	'Response.write(Request.Cookies("ntbPassword") & "<br>")
	'Response.write(Request.Cookies("ntbUserID"))
End Sub

'check for Submit or not
strSelect = Request.Form("realSubmit")
' strSelect = Request.Form("frmLogin")



strUserID = Request.Cookies("ntbUserID")
strPassword = Request.Cookies("ntbPassword")

'if (Session("Username") = "") and (strUserID <> "") and (Request.QueryString("autologin") = "true") then
if (Session("Username") = "") and (strUserID <> "") then
	blnAutologin = true
else
	blnAutologin = false
end if


' Check if the user has requested the password.
if strSelect = "sendpassword" then
	' Sl� opp og send passordet p� mail
	strUserID = Request.Form("txtUsername")
	' Get UserID from LogonUser
	Set oProfile = GetUserProfileByLoginName(strUserID)
	if oProfile is nothing then
		Response.Redirect "error.asp?Err=checkusername"
	end if

	bEmail = false
	bSMS = false
	if Request.Form("chkEmail") = "on" then bEmail = true
	'if Request.Form("chkSMS") = "on" then bSMS = true

	strReturn = ForgotPassword(strUserID, bEmail, bSMS)
	Response.Write "<div class='main_heading2'>" & strReturn & "</DIV><BR>"
	' Should also send SMS
	blnAutologin = false
end if

' Check if the user has requested to be registered
if strSelect = "registeruser" then

	strName = Request.Form("txtName")
	strCompany = Request.Form("txtCompany")
	strEmail = Request.Form("txtEmail")
	strMobile = Request.Form("txtMobile")

	strReturn = RegisterUser(strName, strEmail, strMobile, strCompany)
	'strReturn = "Takk! Vi vil kontakte deg med brukernavn og passord s� snart som mulig."

	Response.Write "<div class='main_heading2'>" & strReturn & "</DIV><BR>"

	blnAutologin = false
end if

'If users pressed the submit button or Coockies present
'Autologin if BrowserSession-Coockies still active
if (strSelect = "fromButton") or (blnAutologin = true) then
'if (strSelect = "fromButton") then

	'Create and initialize the AuthManager
	set objAuth = Server.CreateObject("Commerce.AuthManager")
	strSiteName = CStr(Application("MSCSCommerceSiteName"))		'Get siteName, set in Global.asa in application scope
	Call objAuth.Initialize(strSiteName)

	'get username and password
	if (strSelect = "fromButton") then
		strUserID = Request("txtUsername")
		strPassword	= Request("txtPassword")
        Response.Cookies("ntbUserID") = strUserID
        Response.Cookies("ntbPassword") = strPassword
	end if

	if strUserID = "" OR strPassword = "" then
		Response.Redirect "error.asp?Err=401.1"
	end if

	'get the profile
	Set oProfile = GetUserProfileByLoginName(strUserID)

	if oProfile is nothing then
	'if no existing profile -> send to errorpage
		Response.Redirect "error.asp"
	end if

	' set AuthTicket
	objAuth.SetAuthTicket Application("PortalDomainName") & "\" & strUserID, True, 600

	'set the end of the link to the next page (to be able to use "post" in the form)
	strLinkEnding = strLinkEnding + "&proxyuser="
    strLinkEnding = strLinkEnding + Application("PortalDomainName") & "\" & strUserID
    strLinkEnding = strLinkEnding + "&proxypwd="
    strLinkEnding = strLinkEnding + strPassword

	'get orgid and the org-profile
	idOrg = oProfile.AccountInfo.org_id
	Set oOrg = GetCompanyProfile(idOrg)

	if oOrg.GeneralInfo.account_status = 0 then
		strRetAsp = "../AuthFiles/error.asp?Err=closed"
		Response.Redirect strRetAsp
	end if

	'get role and org-name
	Dim sRole, strOrgName
	sRole = oProfile.BusinessDesk.partner_desk_role
	strOrgName = oOrg.GeneralInfo.Name

	'set session-variable
	Session("Username") = strUserID
	Session("Password") = strPassword
	Session("UID") = oProfile.GeneralInfo.user_id
	Session("LowRes") = Request.Form("hidLowRes")

	' 2005.03.07:Roar Vestre: Added for SmsWapEmail:
	Session("Mobile") = oProfile.GeneralInfo.tel_number
	Session("Email") = oProfile.GeneralInfo.email_address

	if Request.Form("bType") <> "" then
		Response.Cookies("ntbBrowser")= Request.Form("bType")
		Session("Browser")= Request.Form("bType")
	else
		Session("Browser")= Request.Cookies("ntbBrowser")
	end if

	Session("ACF")= oOrg.GeneralInfo.accesslevel
	Session("OrgName") = strOrgName
	
	

	
	'set right session role
	if sRole = "2" then
		if strOrgName = "Norsk Telegrambyr�" then
		'admin user from NTB
			Session("Role")= "Admin"
		else
		'admin user from a customer
			Session("Role") = "Super"
		end if
	else
	'normal user
		Session("Role")= "Normal"
	end if

	'check how many active windows the user already have

	'lock so multiple users do not overwrite each other's changes
	Application.lock

	Dim aryList, intX

	'check to see if the array exists yet
	if IsArray(Application("UserList")) then
		aryList = Application("UserList")

		'check to see if this user is already in the array
		for intX = 0 to UBound(aryList, 2)
		'go through the array
			if StrComp(aryList(0, intX), Session("UserName"), 1) = 0 then
			'when the user have been found

				'increment the number of sessions
				aryList(1,intX) = CInt(aryList(1,intX)) + 1

				if Cint(aryList(1,intX)) > 500 then
				'if the user have 5 active sessions already (trying to open the 6th)
				'-> deny access to portal

					'set the global variable
					Application("UserList") = aryList

					'unlock
					Application.unlock

					'send the user to error-page
					strRetAsp = "../AuthFiles/error.asp?Err=session"
					Response.Redirect strRetAsp
				end if

				Dim flag
				flag = true

				exit for
			end if
		next

		if not flag then
		'if the user doesn't exit in the array already
		'redim the array and add the user to the list
			ReDim preserve aryList(1, UBound(aryList, 2) + 1)

			'set the new user
			aryList(0, UBound(aryList, 2)) = Session("UserName")
			aryList(1, UBound(aryList, 2)) = 1
			intX = UBound(aryList, 2)

		end if

	else
	'If not create a new array and add the user to the list
		ReDim aryList(1,0)

		'set the new user
		aryList(0, UBound(aryList, 2)) = Session("UserName")
		aryList(1, UBound(aryList, 2)) = 1
		intX = UBound(aryList, 2)

	end if

	'set the global variable
	Application("UserList") = aryList
	Session("LogonNum") = aryList(1,intX)

	'unlock
	Application.unlock

	' Log that the user logged on.
	call objUsageEntry.AddEntry(Session("UID"), 0, 2) ' 2 is the statstypeid for log on

'	'check whether it is time to change password
'	if sRole <> "1" then
'	'if the user is not a normal user
'		Dim datePassword, DateExpire, DateWarning
'		datePassword = oProfile.AccountInfo.password_expire_date
'
'		'the password expire after 3 months, just change the number for other interval (months)
'		DateExpire = DateAdd("M", 3, datePassword)
'
'		if DateExpire <= Date then
'		'the password is to old
'
'			if sRole = "3" then
'			'if the user is a superuser - give message
'				'strRetAsp = objAuth.GetURL("mainpages/ChangePassword.asp?print=message&org=" &strOrgName, True, False) 'didn't work on mac
'				strRetAsp = "../mainpages/ChangePassword.asp?print=message&org=" &strOrgName
'				strRetAsp = strRetAsp & strLinkEnding
'				Response.Redirect strRetAsp
'
'			elseif sRole = "2" then
'			'give the user a opportunity to change the password
'				'strRetAsp = objAuth.GetURL("mainpages/ChangePassword.asp?print=change", True, False) 'didn't work on mac
'				strRetAsp = "../mainpages/ChangePassword.asp?print=change&org=" &strOrgName
'				strRetAsp = strRetAsp & strLinkEnding
'				Response.Redirect strRetAsp
'
'			end if
'		end if
'	end if

	if Request("chkBandWidth") <> "" then
	'go to the original requested ASP, admin, light or personalized page
	'set a session-variable??

	elseif Request("chkAdmin") <> "" then
	'send the user to the admin-site
		if sRole = "2" then
		'check whether this user is a super superuser
			if strOrgName <> "Norsk Telegrambyr�" then
			'this user is a super superuser from one of NTBs customer
				'strRetAsp = objAuth.GetURL("mainpages/Userlist.asp", True, False) 'didn't work on mac
				strRetAsp = "../mainpages/Userlist.asp?"
				strRetAsp = strRetAsp & strLinkEnding
				Response.Redirect strRetAsp
			else
			'this user is a NTB admin user
				'strRetAsp = objAuth.GetURL("mainpages/Companylist.asp", True, False) 'didn't work on mac
				strRetAsp = "../mainpages/Companylist.asp?"
				strRetAsp = strRetAsp & strLinkEnding
				Response.Redirect strRetAsp
			end if

		else
		'give an errormessage
			'strRetAsp = objAuth.GetURL("Authfiles/error.asp?Err=uautorisert", True, False) 'didn't work on mac
			strRetAsp = "../Authfiles/error.asp?Err=uautorisert"
			strRetAsp = strRetAsp & strLinkEnding
			Response.Redirect strRetAsp
		end if

	else
		' Verify the profile to check if the user must update his profile.
		if not VerifyProfile(oProfile) then
			Session("VerifyProfile") = true
			strRetAsp = "../mainpages/EditProfile.asp?huid=" & oProfile.Fields("GeneralInfo.user_id") & "" & "&hcoid=" & oProfile.Fields("AccountInfo.org_id") & "" & "?"
			strRetAsp = strRetAsp & strLinkEnding
			
			Response.Redirect strRetAsp
		else
			'send the user to his/hers personalized page
			'strRetAsp = objAuth.GetURL("mainpages/User_homepage.asp", True, False) 'didn't work on mac
			strRetAsp = "../mainpages/User_homepage.asp?"
			strRetAsp = strRetAsp & strLinkEnding

			'TEST RoV: Go to first requested URL
			if instr(1, lcase(ntbLastRequestedURL), "mainpages/mainsectionpages.asp") > 0 Then
				dim intPos
				intPos = instr(1,lcase(ntbLastRequestedURL), "/ntb/")
				ntbLastRequestedURL = ".." & mid(ntbLastRequestedURL, intPos + 4)
				'strRetAsp = ntbLastRequestedURL

				'*** For debug purpose:
				'Response.Write (ntbLastRequestedURL)
				'Response.End
				'session.Abandon
			end if
			
			
			Response.Redirect strRetAsp
		end if
	end if
else
	PrintLogin
End if

Set objAuth = Nothing
Set oProfile = Nothing
Set oOrg = Nothing



%>

<%Sub PrintLogin()%>
<HTML>
<HEAD>
<LINK href="ntb.css" type="text/css" rel="stylesheet">
<%=Titletag()%>

<script language="javascript">
<!--
//function to detect the users screen resolution and setting it in a hidden field
function getScreenRes(){
	if (navigator.appName.indexOf("Microsoft") >= 0){
	 //width = screen.width;
	 height = screen.height;
	 document.forms.frmLogin.elements.bType.value = "ie";
	}
	else if (navigator.appName.indexOf("Netscape") >= 0){
	 //width = screen.availWidth;
	 height = screen.availHeight;
	 document.forms.frmLogin.elements.bType.value = "ns";
	}
	else{
		height = 600;
		document.forms.frmLogin.elements.bType.value = "ie";
	}

	if (height < 650){
	  lowRes = "true";
	  document.forms.frmLogin.elements.hidLowRes.value = "true";
	}
	else {
	  lowRes = "false";
	  document.forms.frmLogin.elements.hidLowRes.value = "false";
	}
	document.frmLogin.txtUsername.focus()
}

// Checks if the login form i displayed in an IFrame. If so, the browser refreshes and
// displays the login form in the top frame. (Bugfix)
if ( parent.location.href != window.location.href )
	top.location.href = "../authfiles/login.asp";
//-->
</script>

</HEAD>
<BODY onLoad="getScreenRes()">
<br><br>
<table border="0" width="40%" align="center">
<tr>
	<td>
		<%=RenderTitleBar("Velkommen til NTBs nyhetsportal", "NoPhoto", 620)%><br>
	</td>
</tr>
<tr>
	<td align="center">
		<table border="0" width=300>
			<FORM NAME="frmLogin" ACTION="login.asp" METHOD="post">
			<tr>
				<td colspan="2" height="40">
					<div class="news11">Vennligst angi ditt brukernavn og passord</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="main_heading2">Brukernavn:
				</td>
				<td>
					<INPUT TYPE="text" NAME="txtUsername" class="news11" MAXLENGTH=32>
				</td>
				<script language="Javascript">

				</script>
			</tr>
			<tr>
				<td>
					<div class="main_heading2">Passord:
				</td>
				<td>
					<INPUT TYPE="password" NAME="txtPassword" class="news11" MAXLENGTH=32>
				</td>
			</tr>
			<tr><td colspan="2">&#160;</td></tr>
			<!--<tr>
				<td>
					<div class="news11">Lett versjon:
				</td>
				<td>
					<INPUT type="checkbox" name=chkBandWidth>
				</td>
			</tr></tr>-->
			<tr>
				<td>
					<div class="news11">Brukeradministrasjon:
				</td>
				<td>
					<INPUT type="checkbox" name=chkAdmin>
				</td>

			</tr>
			<tr><td colspan="2">&#160;</td></tr>


			<!--tr><td colspan="2" class="news11">
			<b>11.10.2004:</b><br><i>Vi iverksetter for tiden automatisk endring av passord. Beskjed sendes via e-post til hver enkelt bruker.
			Om n�dvendig kan gjeldende passord bestilles via <a href="ForgotPassword.asp">e-post eller SMS</a> nederst p� denne siden.</i>
			</td></tr-->
<!--
			<tr><td colspan="2" class="news" style="FONT-SIZE:small;">
				<b>10.02.2005, kl. 1300:</b><br>
				NTB-Portalen er nede p.g.a. n�dvendig vedlikehold. Forventet nedetid er ca. en og en halv time. Vi beklager!
				<br>
				<p>I mellomtiden kan NTB-nyhetene f�lges p�:
				<a href="http://www.ntb.no/nyheter">www.ntb.no/nyheter</a>

				<br>Brukernavn: <b>gjest</b>
				<br>Passord: <b>vaage18</b>
				<p>
				Vennlig hilsen
				NTB-IT
			</td></tr>
			<tr><td colspan="2" class="news" style="FONT-SIZE:small;">
				<b>27.04.2005: NYHET!</b><br>
				<p>
				N� kan du bestille skreddersydd e-postvarsling fra NTB. Klikk p� lenken VARSLING �verst til venstre i menylinja.
				</p>
			</td></tr>
-->

			<tr>
				<td colspan="2" align="center" height="60">
					<input type="hidden" name="realSubmit" value="fromButton">
					<INPUT type="hidden" name="hidLowRes">
					<INPUT type="hidden" name="bType">

					<input type="reset" class="formbutton" name="action" id=L_Reset_Button value="T�m felt" style="background:#bfccd9 none; color:#003366; width:80px">
					<input type="submit" class="formbutton" name="action" id=L_Submit_Button value="Logg inn" style="background:#bfccd9 none; color:#003366; width:80px">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<div class="news11">Glemt passordet ditt? <a href="ForgotPassword.asp">Klikk her</a>.
					<div class="news11">Ikke registrert bruker? <a href="RegisterUser.asp">Registrer deg her</a>.
				</td>
			</tr>
			</form>
		</table>
	</td>
</tr>
</table>
</BODY>
</HTML>
<%End sub%>
