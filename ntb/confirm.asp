<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' confirm.asp
' Confirm Order page for Basket
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Const ID = "ID"

Sub Main()
	Dim mscsOrderGrp, sOrderNumber, sOrderID
	
	Call EnsureAccess()
    
    Call InitializeConfirmationPage(sOrderID, mscsOrderGrp, sOrderNumber)
    
    htmPageContent = htmRenderConfirmationPage(sOrderID, mscsOrderGrp, sOrderNumber)
    
    Call TerminateConfirmationPage(mscsOrderGrp)
End Sub


' -----------------------------------------------------------------------------
' InitializeConfirmationPage
'
' Description:
'   Initialize variables; called by Main
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitializeConfirmationPage(ByRef sOrderID, ByRef mscsOrderGrp, ByRef sOrderNumber)
	sOrderID = GetRequestString(ORDER_ID, Null)
	If IsNull(sOrderID) Then
		Response.Redirect(GenerateURL(MSCSSitePages.BadURL, Array(), Array()))
	End If
	
	Set mscsOrderGrp = GetOrderGroup(m_UserID)
	Call mscsOrderGrp.LoadOrder(sOrderID)
	sOrderNumber = mscsOrderGrp.Value(ORDER_NUMBER)
End Sub


' -----------------------------------------------------------------------------
' TerminateConfirmationPage
'
' Description:
'   Clean up; called by Main
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub TerminateConfirmationPage(ByRef mscsOrderGrp)
	Set mscsOrderGrp = Nothing
End Sub


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderConfirmationPage
'
' Description:
'   Render whole Confirmation Page; called by Main
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderConfirmationPage(sOrderID, mscsOrderGrp, sOrderNumber)
	Dim htmContent, htmTitle
	Dim arrParams, arrParamVals, urlLink, htmLinkText
	
	sPageTitle = mscsMessageManager.GetMessage("L_Confirmation_HTMLTitle", sLanguage)
    htmContent = RenderText(mscsMessageManager.GetMessage("L_ConfirmationMessage_HTMLText", sLanguage), MSCSSiteStyle.Body) & CRLF
    htmContent = htmContent & RenderText(FormatOutput(LABEL_TEMPLATE, Array(mscsMessageManager.GetMessage("L_ConfirmationNumber_HTMLText", sLanguage))), MSCSSiteStyle.Body)
    
	If m_UserType = AUTH_USER Then
		arrParams = Array(ID, ORDER_NUMBER, SOURCE_PAGE)
		arrParamVals = Array(sOrderID, sOrderNumber, MSCSAppFrameWork.GetPageURL())
		urlLink = GenerateURL(MSCSSitePages.ViewUserOrder, arrParams, arrParamVals)
		htmLinkText = RenderText(sOrderNumber, MSCSSiteStyle.Body)
		htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
	Else
		htmContent = htmContent & RenderText(sOrderNumber, MSCSSiteStyle.Body) & BR
	End If
	
    htmContent = htmContent & RenderText(mscsMessageManager.GetMessage("L_ThankYou_HTMLText", sLanguage), MSCSSiteStyle.Body) & CRLF
    
    urlLink = GenerateURL(MSCSSitePages.Home, Array(), Array())
    htmLinkText = RenderText(mscsMessageManager.GetMessage("L_ToPlaceNewOrder_HTMLText", sLanguage), MSCSSiteStyle.Body)
    htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & CRLF
    
	htmContent = htmContent & RenderText(mscsMessageManager.GetMessage("L_ToEndSession_HTMLText", sLanguage), MSCSSiteStyle.Warning) & CRLF _
					& RenderText(mscsMessageManager.GetMessage("L_CloseYourBrowser_HTMLText", sLanguage), MSCSSiteStyle.Warning) & BR _
					& RenderText(mscsMessageManager.GetMessage("L_ClearBrowserHistory_HTMLText", sLanguage), MSCSSiteStyle.Warning) & CRLF _
					& RenderText(mscsMessageManager.GetMessage("L_EndOfSessionWarning_HTMLText", sLanguage), MSCSSiteStyle.Warning) & BR

	htmTitle = RenderText(mscsMessageManager.GetMessage("L_Confirmation_HTMLTitle", sLanguage), MSCSSiteStyle.Title) & CRLF

    htmRenderConfirmationPage = htmTitle & htmContent
End Function    
%>

			
