<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/addr_lib.asp" -->
<!-- #INCLUDE FILE="include/form_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' addrform.asp
' Display page for Address editing
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()    
	Dim mscsOrderGrp, listFlds, dictFldVals, dictFldErrs
	Dim bSuccess, iErrorLevel, iAddrType, arrParams, arrParamVals
	Dim htmContent, htmTitle, sTitle, urlAction, urlRedirect
	    
	Call EnsureAccess()

	Set mscsOrderGrp = LoadBasket(m_UserID)
	
	Call CatchErrors(mscsOrderGrp)
	
	' Initialize variables
	bSuccess = False	
	iErrorLevel = 0
	iAddrType = GetAddressType()
	Set dictFldVals = Nothing
	Set dictFldErrs = Nothing
	arrParams = Array(ADDRESS_TYPE)
	arrParamVals = Array(iAddrType)
	urlAction = GenerateURL(MSCSSitePages.AddressForm, arrParams, arrParamVals)

	If iAddrType = SHIPPING_ADDRESS Then
		sTitle = mscsMessageManager.GetMessage("L_ShipToAddress_HTMLTitle", sLanguage)
		sPageTitle = sTitle
		Set listFlds = Application("MSCSForms").Value("ShipToAddress")		
	Else
		sTitle = mscsMessageManager.GetMessage("L_BillToAddress_HTMLTitle", sLanguage)
		sPageTitle = sTitle
		Set listFlds = Application("MSCSForms").Value("BillToAddress")		
	End If
	
	If IsFormSubmitted() Then 
		Set dictFldVals = GetSubmittedFieldValues(listFlds)
		iErrorLevel = iValidateSubmittedAddress(listFlds, dictFldVals, dictFldErrs)
		If iErrorLevel = 0 Then		
			Rem Validation of submitted data succeeded.
			bSuccess = True
		End If
	End If

	If bSuccess Then
		If (iAddrType = SHIPPING_ADDRESS) And (dictFldVals(BILLING_USE) = "") Then
			arrParams = Array(ADDRESS_TYPE)
			arrParamVals = Array(BILLING_ADDRESS)
			urlRedirect = GenerateURL(MSCSSitePages.AddressForm, arrParams, arrParamVals)
		Else
			urlRedirect = GenerateURL(MSCSSitePages.ShippingMethods, Array(), Array())			
		End If
		
		Call SaveAddressToOrderGroup(mscsOrderGrp, dictFldVals, iAddrType)
		Response.Redirect(urlRedirect)
	End If

	If iErrorLevel = 0 Then
		Set dictFldVals = GetDefaultValuesForAddressFormFields(mscsOrderGrp, listFlds, iAddrType)
	End If

	If iAddrType = SHIPPING_ADDRESS Then	
		htmContent = htmContent & htmRenderFillOutForm(urlAction, "ShipToAddress", dictFldVals, dictFldErrs)
	Else
		htmContent = htmContent & htmRenderFillOutForm(urlAction, "BillToAddress", dictFldVals, dictFldErrs)	
	End If
	
	htmTitle = RenderText(sTitle, MSCSSiteStyle.Title)
	htmPageContent = htmTitle & CRLF & htmContent
End Sub


' -----------------------------------------------------------------------------
' CatchErrors
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrors(ByVal mscsOrderGrp)
	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
End Sub


' -----------------------------------------------------------------------------
' GetSavedAddressID
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSavedAddressID(mscsOrderGrp, ByVal iAddrType) 
	Dim dict, sAddrID
	
	If iAddrType = SHIPPING_ADDRESS Then
		Set dict = GetAnyLineItem(mscsOrderGrp)
		sAddrID = dict.Value(SHIPPING_ADDRESS_ID)
	Else
		Set dict = GetAnyOrderForm(mscsOrderGrp)
		sAddrID = dict.Value(BILLING_ADDRESS_ID)
	End If
	
	GetSavedAddressID = sAddrID
End Function


' -----------------------------------------------------------------------------
' SaveAddressToOrderGroup
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveAddressToOrderGroup(mscsOrderGrp, dictFldVals, ByVal iAddrType)
	Dim bUseForBillTo
	Dim sAddrID
	
	bUseForBillTo = False

	' Lookup any existing shipping or billing address id, else generate a new id
	sAddrID = GetSavedAddressID(mscsOrderGrp, iAddrType) 
	If IsNull(sAddrID) Then
		sAddrID = MSCSGenID.GenGUIDString
	End If
	
	If dictFldVals.Value(BILLING_USE) <> "" Then
		bUseForBillTo = True
		' Remove the key from dictionary to avoid persisting it to order.
		dictFldVals.Value(BILLING_USE) = Null
	End If
	
	' Set the region code. Note that region code is set to region name if there are no 
	'	regions defined for the specified country. Both region name and region code are 
	'	set to "" if region field is left blank.
	If Not IsNull(dictFldVals.Value(REGION_NAME)) Then
		dictFldVals.Value(REGION_CODE) = MSCSAppConfig.GetRegionCodeFromCountryCodeAndRegionName(dictFldVals.Value(ADDRESS_COUNTRY), dictFldVals.Value(REGION_NAME))
		If dictFldVals.Value(REGION_CODE) = "" Then
			dictFldVals.Value(REGION_CODE) = dictFldVals.Value(REGION_NAME)
		End If
	Else
		dictFldVals.Value(REGION_NAME) = ""
		dictFldVals.Value(REGION_CODE) = ""
	End If

	If iAddrType = SHIPPING_ADDRESS Then
		dictFldVals.Value(ADDRESS_NAME) = mscsMessageManager.GetMessage("L_GenericAddressName1_Text", sLanguage)
		Call mscsOrderGrp.SetAddress(sAddrID, dictFldVals)
		Call mscsOrderGrp.SetShippingAddress(sAddrID, True)
		If bUseForBillTo Then
			Rem The address can be used for both shipping and billing.
			Call SetKeyOnOrderForms(mscsOrderGrp, BILLING_ADDRESS_ID, sAddrID)	
		End If
	Else
		Dim sShipAddrID
		sShipAddrID = GetSavedAddressID(mscsOrderGrp, SHIPPING_ADDRESS)
		If (sAddrID = sShipAddrID) Then
			sAddrID =  MSCSGenID.GenGUIDString	'	BILLING_ADDRESS now uses its own address data
		End If
		dictFldVals.Value(ADDRESS_NAME) = mscsMessageManager.GetMessage("L_GenericAddressName2_Text", sLanguage)
		Call mscsOrderGrp.SetAddress(sAddrID, dictFldVals)		
		Call SetKeyOnOrderForms(mscsOrderGrp, BILLING_ADDRESS_ID, sAddrID)
	End If
			
	Call mscsOrderGrp.SaveAsBasket()
End Sub


' -----------------------------------------------------------------------------
' GetDefaultValuesForAddressFormFields
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetDefaultValuesForAddressFormFields(mscsOrderGrp, listFlds, ByVal iAddrType)	
	Dim dictFldVals
	Dim sAddrID
	
	Set dictFldVals = Nothing
	
	sAddrID = GetAnySavedAddressID(mscsOrderGrp, iAddrType)
	If Not IsNull(sAddrID) Then
		Set dictFldVals = mscsOrderGrp.GetAddress(sAddrID)
	End If
	
	If dictFldVals Is Nothing Then
		Set dictFldVals = GetDictionary()
		dictFldVals.Value(ADDRESS_COUNTRY) = "-1"
	End If

	Rem The billing address is the same as shipping address.
	dictFldVals.Value(BILLING_USE) = CHECKED

	Set GetDefaultValuesForAddressFormFields = dictFldVals
End Function


' -----------------------------------------------------------------------------
' GetAnySavedAddressID
'
' Description:
'   This function gets a saved address_id from the basket,
'   preferably of the requested type, else of the other type
'   For use by GetDefaultValuesForAddressFormFields
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAnySavedAddressID(mscsOrderGrp, ByVal iAddrType) 
	Dim dict, sAddrID
	
	If iAddrType = SHIPPING_ADDRESS Then
		Set dict = GetAnyLineItem(mscsOrderGrp)
		sAddrID = dict.Value(SHIPPING_ADDRESS_ID)
		If IsNull(sAddrID) Then
			Set dict = GetAnyOrderForm(mscsOrderGrp)
			sAddrID = dict.Value(BILLING_ADDRESS_ID)
		End If
	Else
		Set dict = GetAnyOrderForm(mscsOrderGrp)
		sAddrID = dict.Value(BILLING_ADDRESS_ID)
		If IsNull(sAddrID) Then
			Set dict = GetAnyLineItem(mscsOrderGrp)
			sAddrID = dict.Value(SHIPPING_ADDRESS_ID)
		End If
	End If
	
	GetAnySavedAddressID = sAddrID
End Function


' -----------------------------------------------------------------------------
' iValidateSubmittedAddress
'
' Description:
'   Insert your specific address valication logic inside this function.
'	The default implementation supports basic validation for international addresses:
'	It ensures that all required fields contain data.
'	It ensures that the region field is a required field for countries that have regions defined.
'	It accepts region name or region code as input; validates them against regions table.
'	It expands region code to region name on subsequent renderings of address form.
'	It implements Zip code validation for US addresses.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function iValidateSubmittedAddress(listFlds, dictFldVals, dictFldErrs)
	Dim iErrorLevel, listRegions, sRegionName
	
	iErrorLevel = 0
	
	Rem Perform field-level validation.
 	Set dictFldErrs = GetFieldsErrorDictionary(listFlds, dictFldVals)

	If dictFldErrs.Count > 0 Then
		iErrorLevel = 1
	End If
	
	' If there is a regions list for the country then region is a required field 
	'	and what user enters for region requires validation, otherwise region is
	'	an optional field and no validation is required. Note that user may enter
	'	either full region name or abbreviated region code into the region field.
	Set listRegions = MSCSAppConfig.GetRegionNamesListFromCountryCode(dictFldVals.Value(ADDRESS_COUNTRY))
	If listRegions.Count > 0 Then
		sRegionName = LookupRegion(dictFldVals.Value(REGION_NAME), dictFldVals.Value(ADDRESS_COUNTRY))
		If IsNull(sRegionName) Then
			dictFldErrs.Value(REGION_NAME) = mscsMessageManager.GetMessage("L_BadRegion_ErrorMessage", sLanguage)
			iErrorLevel = 1
		Else
			' Since user may enter region code into region name field, modify user 
			'	input to expand region code to region name.
			dictFldVals.Value(REGION_NAME) = sRegionName
		End If
	End If
		
	If StrComp(dictFldVals.Value(ADDRESS_COUNTRY), "US", vbTextCompare) = 0 Then
		If Not IsZipCodeValid(dictFldVals.Value(POSTAL_CODE)) Then
			dictFldErrs.Value(POSTAL_CODE) = mscsMessageManager.GetMessage("L_BadPostalCode_ErrorMessage", sLanguage)
			iErrorLevel = 1
		End If
	End If

	iValidateSubmittedAddress = iErrorLevel
End Function
%>
