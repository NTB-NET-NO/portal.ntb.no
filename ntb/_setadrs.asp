<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/addr_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<%
' =============================================================================
' _setadrs.asp
' Tween page for setting an address (shipping or billing) in the user's basket.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim mscsOrderGrp, rsAddr, dictAddr
    Dim iItemIndex, iAddressType, sAddrID

	Call EnsureAuthAccess()
	Call FetchData(mscsOrderGrp, sAddrID, rsAddr)
	Call CatchErrors(mscsOrderGrp, sAddrID, rsAddr)
	
	' Copy the address profile's GeneralInfo section into a dictionary to be added to the OrderGroup
    Set dictAddr = RsToDict(rsAddr("GeneralInfo").Value)
    
	Select Case GetAddressType()
		Case SHIPPING_ADDRESS
			Call mscsOrderGrp.SetAddress(sAddrID, dictAddr)				
			Call mscsOrderGrp.SetShippingAddress(sAddrID, True)			
			If Not IsNull(GetRequestString(BILLING_USE, Null)) Then
				' The address can be used for both shipping and billing.
				Call SetKeyOnOrderForms(mscsOrderGrp, BILLING_ADDRESS_ID, sAddrID)
			End If
		Case BILLING_ADDRESS
			Call mscsOrderGrp.SetAddress(sAddrID, dictAddr)	
            Call SetKeyOnOrderForms(mscsOrderGrp, BILLING_ADDRESS_ID, sAddrID)
	End Select

    Call mscsOrderGrp.SaveAsBasket()
    If (GetAddressType() = SHIPPING_ADDRESS) And _
		IsNull(GetRequestString(BILLING_USE, Null)) Then
			Response.Redirect( _
								 GenerateURL(MSCSSitePages.AddressBook, Array(ADDRESS_TYPE), Array(BILLING_ADDRESS)) _
							 )
	Else
		Response.Redirect(GenerateURL(MSCSSitePages.ShippingMethods, Array(), Array()))	
    End If
End Sub


' -----------------------------------------------------------------------------
' FetchData
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function FetchData(ByRef mscsOrderGrp, ByRef sAddrID, ByRef rsAddr)
    sAddrID = GetRequestString(ADDRESS_ID, Null)

	Set mscsOrderGrp = LoadBasket(m_UserID)

	' Set rsGetProfile's bForceDBLookUp to True to force a database look-up.
    Set rsAddr = rsGetProfile(sAddrID, PROFILE_TYPE_ADDRESS, True)
End Function	
	

' -----------------------------------------------------------------------------
' CatchErrors
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrors(ByVal mscsOrderGrp, ByVal sAddrID, ByVal rsAddr)
	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
	
    If IsNull(sAddrID) Or (rsAddr Is Nothing) Then
        Response.Redirect(GenerateURL(MSCSSitePages.AddressBook, Array(), Array()))
    End If
End Sub
%>
