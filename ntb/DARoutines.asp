<%
  Public Const ADSTYPE_CASE_IGNORE_STRING = 3
  Public Const ADS_ATTR_UPDATE = 2

  Public Const ADS_ACETYPE_ACCESS_ALLOWED = 0
  Public Const ADS_ACETYPE_ACCESS_DENIED = &H1 '1
  Public Const ADS_ACETYPE_SYSTEM_AUDIT = &H2              '2,
  Public Const ADS_ACETYPE_ACCESS_ALLOWED_OBJECT = &H5 '5
  Public Const ADS_ACETYPE_ACCESS_DENIED_OBJECT = &H6    '6
  Public Const ADS_ACETYPE_SYSTEM_AUDIT_OBJECT = &H7     '7
  
  Public Const ADS_FLAG_OBJECT_TYPE_PRESENT = &H1            '1,
  Public Const ADS_FLAG_INHERITED_OBJECT_TYPE_PRESENT = &H2   '2
        
  Public Const ADS_RIGHT_DELETE = &H10000   '65536
  Public Const ADS_RIGHT_READ_CONTROL = &H20000  '131072
  Public Const ADS_RIGHT_WRITE_DAC = &H40000    '262144
  Public Const ADS_RIGHT_WRITE_OWNER = &H80000    '524288
  Public Const ADS_RIGHT_SYNCHRONIZE = &H100000   '1048576
  Public Const ADS_RIGHT_STANDARD_RIGHTS_REQUIRED = &HF0000  '983040
  Public Const ADS_RIGHT_STANDARD_RIGHTS_READ = &H20000    '131072
  Public Const ADS_RIGHT_STANDARD_RIGHTS_WRITE = &H20000  ' 131072
  Public Const ADS_RIGHT_STANDARD_RIGHTS_EXECUTE = &H20000  '131072
  Public Const ADS_RIGHT_STANDARD_RIGHTS_ALL = &H1F0000    '2031616
  Public Const ADS_RIGHT_SPECIFIC_RIGHTS_ALL = &HFFFF     ' 65535 '6.5535e+4 '
  Public Const ADS_RIGHT_ACCESS_SYSTEM_SECURITY = &H1000000   ' 16777216
  Public Const ADS_RIGHT_GENERIC_READ = &H80000000      '2147483648#
  Public Const ADS_RIGHT_GENERIC_WRITE = &H40000000      '1073741824
  Public Const ADS_RIGHT_GENERIC_EXECUTE = &H20000000    '536870912
  Public Const ADS_RIGHT_GENERIC_ALL = &H10000000    '268435456
  Public Const ADS_RIGHT_DS_CREATE_CHILD = &H1     '1
  Public Const ADS_RIGHT_DS_DELETE_CHILD = &H2    '2
  
  Public Const ADS_RIGHT_ACTRL_DS_LIST = &H4     '4
  Public Const ADS_RIGHT_DS_SELF = &H8       '8
  Public Const ADS_RIGHT_DS_READ_PROP = &H10       '16
  Public Const ADS_RIGHT_DS_WRITE_PROP = &H20      '32
  Public Const ADS_RIGHT_DS_DELETE_TREE = &H40      '64
  Public Const ADS_RIGHT_DS_LIST_OBJECT = &H80      '128


  Public Const ADS_ACEFLAG_INHERIT_ACE = &H2    ' 2
  Public Const ADS_ACEFLAG_NO_PROPAGATE_INHERIT_ACE = &H4    '4
  Public Const ADS_ACEFLAG_INHERIT_ONLY_ACE = &H8    ' 8
  Public Const ADS_ACEFLAG_INHERITED_ACE = &H10    '16
  Public Const ADS_ACEFLAG_VALID_INHERIT_FLAGS = &H1F    '31
  Public Const ADS_ACEFLAG_SUCCESSFUL_ACCESS = &H40    '64
  Public Const ADS_ACEFLAG_FAILED_ACCESS = &H80  '128
  
  
  Public Const ADS_GROUP_TYPE_GLOBAL_GROUP = &H2
  Public Const ADS_GROUP_TYPE_DOMAIN_LOCAL_GROUP = &H4
  Public Const ADS_GROUP_TYPE_LOCAL_GROUP = &H4
  Public Const ADS_GROUP_TYPE_UNIVERSAL_GROUP = &H8
  Public Const ADS_GROUP_TYPE_SECURITY_ENABLED = &H80000000
  
  Public Const ADSTYPE_NT_SECURITY_DESCRIPTOR = 25
  Public Const ADSTYPE_OCTET_STRING = 8
  
  Public Const AD_ADMIN_GROUP_SUFFIX = "_ADMINGROUP"
  Public Const AD_USER_GROUP_SUFFIX = "_USERGROUP"
  Public Const AD_ADMIN_GROUP_AN_SUFFIX = "_AG"
  Public Const AD_USER_GROUP_AN_SUFFIX = "_UG"
  Public Const AD_SAM_ACCOUNT_NAME_LEN = 20
  Public Const AD_CN_LEN = 64
  Public Const AD_CHAR_MASK = "^[^?*/""|:<>=+;,\\\)(#[\]]*"


  Const sXMLTagDataSource = "DataSource"
  Const sXMLTagSourceInfo = "SourceInfo"
  const sXMLAttrSourceType = "sourceType"

  Const AD_SOURCE_TYPE = "LDAPv3"
	

Sub SetGroupPermissions(szOrgName, aprops, cn, nUserID)
	Dim sCatalogName
	Dim sProfileDSN
	Dim MSCSProfileService
	Dim szDomain
	Dim szAdmingroup
	Dim szUserGroup
	Dim oRS
	Dim bRet
	Dim szCurrentDomain
	Dim szTmp
	Dim szUser
	Dim delQuery
	Dim rs  
	
	On Error Resume Next
   	rem Get the catalog name eg. "Profile Definitions"
	sCatalogName = sGetCatalogName()
	sProfileDSN = sGetProfileConnStr()
	szUser = Request.ServerVariables("LOGON_USER")
	szTmp = Split(szUser,"\")
	szCurrentDomain = szTmp(0)
	 
	If Err.number<> 0 then
	    delQuery = GetDeleteQuery(szOrgName, aprops, cn, nUserID)
		bRet = bExecWithErrDisplay(rs, cn, delQuery, L_ReadDSNFail_ErrorMessage)
		Call SetError(L_PERMISSIONS_DialogTitle, L_ReadDSNFail_ErrorMessage, _
	              sGetUserScriptError(Err), ERROR_ICON_CRITICAL)
    	  Err.Clear
	  Exit Sub
	End If              

	szDomain = sGetDomainName() 
	rem need to get SAMAccountName for the group
	szAdmingroup = szDomain & "\" & sGetGroupSAMAccountName(g_szAdminGroup, sProfileDSN)
	szUserGroup = szDomain & "\" & sGetGroupSAMAccountName(g_szUserGroup, sProfileDSN)
	If Err.number<> 0 then
	    delQuery = GetDeleteQuery(szOrgName, aprops, cn, nUserID)
		bRet= bExecWithErrDisplay(rs, cn, delQuery, L_RetrievesAMAccountNameFail_ErrorMessage)
		Call SetError(L_PERMISSIONS_DialogTitle, L_RetrievesAMAccountNameFail_ErrorMessage, _
	              sGetUserScriptError(Err), ERROR_ICON_CRITICAL)
	    Exit Sub          
	   Err.Clear
	End If              

	Dim sSQL
	sSQL = "SELECT " & g_sProfileSystem & "." & g_sntSecurityDescriptor
	sSQL = sSQL & " FROM " & sBracket(sGetPropProfile(aprops(1))) & " WHERE " ' 
	sSQL = sSQL & "GeneralInfo.org_id = '" & nUserID & "'" 
	sSQL = sSQL &  " AND "
	sSQL = sSQL & sBracket(g_sParentURL) & " = '" & sGetParentDN(NULL) & "'"
	   
	bRet = bRecordExec(oRS, cn, sSQL)
	If Not bRet then
		delQuery = GetDeleteQuery(szOrgName, aprops, cn, nUserID)
		bRet = bExecWithErrDisplay(rs, cn, delQuery, L_SetTrusteeFail_ErrorMessage)
		Exit Sub
	End If 

 
    Dim octSecDesc, objGroupInfo
	octSecDesc = oRS(g_sProfileSystem & "." & g_sntSecurityDescriptor).value
	Set oRS = Nothing
		
	If Not IsEmpty(octSecDesc) or Not ISNull(octSecDesc) then
		Dim propVal
		Set propVal = CreateObject("PropertyValue")
			propVal.PutObjectProperty ADSTYPE_OCTET_STRING, (octSecDesc)
	
		Dim secDesc
		Dim dACL
		Set secdesc = propVal.GetObjectProperty(ADSTYPE_NT_SECURITY_DESCRIPTOR)
		Set dACL = secdesc.DiscretionaryAcl
		
		If Err.number<> 0 then
			delQuery = GetDeleteQuery(szOrgName, aprops, cn, nUserID)
			bRet = bExecWithErrDisplay(rs, cn, delQuery, L_MSCSUPSGetSecObjectFAIL_ErrorMessage)
			Call SetError(L_PERMISSIONS_DialogTitle, L_MSCSUPSGetSecObjectFAIL_ErrorMessage, _
			              sGetUserScriptError(Err), ERROR_ICON_CRITICAL)
		  Err.Clear
		  Exit Sub
		End If              

		Dim newAce
			
		Set newAce = CreateObject("AccessControlEntry")
		rem set permissions for ADMIN_GROUP
		' ACE1: on the Object (not inherited)
		' Set AceType to: ACCESS_ALLOWED_ACE_TYPE = 0
		newAce.AceType = ADS_ACETYPE_ACCESS_ALLOWED
		newAce.AccessMask = ADS_RIGHT_GENERIC_ALL
		newAce.AceFlags = ADS_ACEFLAG_INHERIT_ACE
		newAce.Trustee = szAdmingroup
		' default: newACE.AceFlags = ?
		dACL.AddAce newAce
		
		rem set permissions for USER_GROUP
		' Set AceType to: ACCESS_ALLOWED_ACE_TYPE = 0
		Set newAce = CreateObject("AccessControlEntry")
		newAce.AceType = ADS_ACETYPE_ACCESS_ALLOWED
		newAce.AccessMask = ADS_RIGHT_DS_READ_PROP 
		newAce.Trustee = szUserGroup
		newAce.AceFlags = ADS_ACEFLAG_INHERIT_ACE + ADS_ACEFLAG_INHERIT_ONLY_ACE
		dACL.AddAce newAce
		
		secdesc.DiscretionaryAcl = dACL
  
		propVal.PutObjectProperty ADSTYPE_NT_SECURITY_DESCRIPTOR, secdesc
		if LCase(szCurrentDomain) = LCase(szDomain) then 
  			octSecDesc = propVal.GetObjectProperty(ADSTYPE_OCTET_STRING)
  	    end if 
		
		If Err.number<> 0 then
		    delQuery = GetDeleteQuery(szOrgName, aprops, cn, nUserID)
			bRet = bExecWithErrDisplay(rs, cn, delQuery, L_SetTrusteeFail_ErrorMessage)
			Call SetError(L_PERMISSIONS_DialogTitle, L_SetTrusteeFail_ErrorMessage, _
			              sGetUserScriptError(Err), ERROR_ICON_CRITICAL)
		    Err.Clear
        Exit Sub
		End If
			
		Dim oRec
		Set oRec = Server.CreateObject("ADODB.Record")
		oRec.Open sSQL, cn, AD_MODE_READ_WRITE, AD_CREATE_OVERWRITE
		oRec(g_sProfileSystem & "." & g_sntSecurityDescriptor).Value = octSecDesc
		
		oRec("__Commit").Value = 1
		oRec.Fields.Update
			
		If Err.number<> 0 then
			delQuery = GetDeleteQuery(szOrgName, aprops, cn, nUserID)
			bRet = bExecWithErrDisplay(rs, cn, delQuery, L_SetTrusteeFail_ErrorMessage)
			Call SetError(L_PERMISSIONS_DialogTitle, L_UpdateSecDesc_ErrorMessage, _
			              sGetUserScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
			Set oRS = Nothing
			Exit Sub
		End If
	    	Set propVal = Nothing
			Set oRS = Nothing
		End If
		
		On Error Goto 0
		
End Sub



Function sGetGroupSAMAccountName(sGroupName,sProfileDSN)
	dim i
	dim oRS
	dim bRet
	dim cn
	dim dGroupState
	Dim oXMLGroup
	Dim sSQL
	Dim aGroupProps
	Dim aGroups
	Dim g_sGroupsAMAccountName
	g_sGroupsAMAccountName = "GeneralInfo.sAMAccountName"
	Set dGroupState = Server.CreateObject("Commerce.Dictionary")
	dGroupState.ProfileDSN = sProfileDSN
	dGroupState.ProfileName = "Profile Definitions.ADGroup"
	
	Call GetProperties(aGroupProps, aGroups, oXMLGroup, dGroupState)	
	
	sSQL = "SELECT " & g_sGroupsAMAccountName '& " AS " & aprops(1)(2)
	Dim aIDCol
	Dim nIDCol
	sSQL = sSQL & " FROM " & sBracket(sGetPropProfile(aGroupProps(1))) & " WHERE " 
	sSQL = sSQL & " GeneralInfo.GroupName = '" & sGroupName & "'"
	if g_bIsADInstallation then
		sSQL = sSQL & " AND " & sBracket(g_sParentURL) & " = '" & _
					sGetParentDN(dGroupState) & "'"
	end if
	set cn = cnGetProviderConn()
'	cn.Open sGetProviderConnStr()
	bRet = bSafeExec(oRS, cn, sSQL)
	sGetGroupSAMAccountName = ""	
	If IsObject(oRS) Then
		If Not oRS.Eof then
		 sGetGroupSAMAccountName = oRS(0)
		End If
	End If			

End Function

Function sGetDomainName()
   Dim obizdata
   Dim sProfileDSN
   Dim oXMLDoc
   set obizdata = oGetBizDataManager
   sProfileDSN = sGetProfileConnStr
   call ConnectBizDataManager(obizdata, sProfileDSN)
   set oXMLDoc=CreateObject("MSXML.DOMDocument")
   Set oXMLDoc = obizdata.GetProfile(REF_GROUP_PROFILE, CStr(Response.CharSet))
    
   sGetDomainName = oXMLdoc.DocumentElement.SelectSingleNode(".//" & sXMLTagDataSource & _
        "[@" & sXMLAttrSourceType & "='" & AD_SOURCE_TYPE & "']//" & sXMLTagSourceInfo & _
        "[@isDefault='1']").GetAttribute("domain")
   Set obizdata = Nothing  
End Function

Rem Function to add a user to an existing group
Rem Input Parameters :
Rem szGroupName : name of the group the user is added to. 
Rem             (Name is the primary key defined in the ADGroup profile )
Rem sProfileDSN : DSN to connect to the Profile store
Sub AddUserToGroup(szGroupName, szUser, sProfileDSN)
	Dim oRS
	Dim bRet
	Dim cn
	Dim dGroupState
	Dim sSQL
	Dim szParentURL
	
	Set dGroupState = Server.CreateObject("Commerce.Dictionary")
	dGroupState.ProfileDSN = sProfileDSN
	dGroupState.ProfileName = "Profile Definitions.ADGroup"
	
	sSQL = "SELECT GeneralInfo.member FROM " & GROUP_PROFILE & " WHERE "
	sSQL = sSQL & " GeneralInfo.GroupName = '" & szGroupName & "'"
	
	rem If this is called from Users page, the Organization name gets appended.
	rem ParentDN of the form OU=<Org Name>,OU=<Site Name>,OU=MSCS_40_Root
	if g_bIsADInstallation then
		szParentURL = sGetParentURL(dGroupState)
		sSQL = sSQL & " AND " & sBracket(g_sParentURL) & " = '" & _
					szParentURL & "'"
	end if
	
	set cn = cnGetProviderConn()
	bRet = bRecordExec(oRS, cn, sSQL)
	
	Dim nMemberCount
	Dim szMembers
	Dim members
	If bRet then
		members = oRs("GeneralInfo.member").Value 
		If IsNull(members) or IsEmpty(members) Then
		  nMemberCount = 0
		  ReDim members(nMemberCount)
		Else
		  rem members returned as array of variant
		  rem members = Split(members,",")
		  nMemberCount = UBound(members)
		  ReDim Preserve members(nMemberCount + 1)
		  nMemberCount = nMemberCount + 1
		End If
		Rem szUser should be a fully qualified DN
		members(nMemberCount) = szUser
		
		For i = 0 to UBound(members)
			members(i) = sQuoteArg(members(i), "String")
		Next
		szMembers = Join(members, ",")
		
		sSQL = "UPDATE " & GROUP_PROFILE & " SET GeneralInfo.member = (" & szMembers & ")"
		sSQL = sSQL & " WHERE GeneralInfo.GroupName = '" & szGroupName & "'"
		if g_bIsADInstallation then
			sSQL = sSQL & " AND " & sBracket(g_sParentURL) & " = '" & _
						szParentURL & "'"
		end if
		bRet = bSafeExec(oRS, cn, sSQL)
	End If
	
	Set cn = Nothing

End Sub

Rem Function to remove a user from an existing group
Rem Input Parameters :
Rem szGroupName : name of the group the user is to be removed from. 
Rem             (Name is the primary key defined in the ADGroup profile )
Rem szUser : Full DN of the user
Rem sProfileDSN : DSN to connect to the Profile store

Sub RemoveUserFromGroup(szGroupName, szUser, sProfileDSN)
	Dim oRS
	Dim bRet
	Dim cn
	Dim dGroupState
	Dim sSQL
	Dim szParentURL
	
	Set dGroupState = Server.CreateObject("Commerce.Dictionary")
	dGroupState.ProfileDSN = sProfileDSN
	dGroupState.ProfileName = "Profile Definitions.ADGroup"
	
	sSQL = "SELECT GeneralInfo.member FROM " & GROUP_PROFILE & " WHERE "
	sSQL = sSQL & " GeneralInfo.GroupName = '" & szGroupName & "'"
	
	rem If this is called from Users page, the Organization name gets appended.
	rem ParentDN of the form OU=<Org Name>,OU=<Site Name>,OU=MSCS_40_Root
	if g_bIsADInstallation then
		szParentURL = sGetParentURL(dGroupState)
		sSQL = sSQL & " AND " & sBracket(g_sParentURL) & " = '" & _
					szParentURL & "'"
	end if
	
	
	set cn = cnGetProviderConn()
	bRet = bRecordExec(oRS, cn, sSQL)
	
	Dim nIndex
	Dim nNewCount
	Dim nMemberCount
	Dim members
	Dim newMembers
	Dim szNewMembers
	Dim bUserMatch
	
	bUserMatch = False
	
	If bRet then
		nNewCount = 0
		If Not (IsNull(oRs("GeneralInfo.member").Value) or IsEmpty(oRs("GeneralInfo.member").Value)) Then
			members = oRs("GeneralInfo.member").Value
			nMemberCount = UBound(members)
			ReDim newMembers(nMembercount)
			For nIndex = 0 to nMemberCount
			 If LCase(members(nIndex)) = LCase(szUser) Then
					bUserMatch = True
			        if nMemberCount > 0 then 
						Redim Preserve newMembers(nMemberCount -1)
					else
						Redim newMembers(nMemberCount)
					end if 	
			 Else
					newMembers(nNewCount) = members(nIndex)
					nNewCount = nNewCount + 1
			 End If
			Next
			For i = 0 to UBound(newMembers)
				newMembers(i) = sQuoteArg(newMembers(i), "String")
			Next
			szNewMembers = Join(newMembers, ",")
			if nMemberCount = 0 and bUserMatch = True then				
				sSQL = "UPDATE " & GROUP_PROFILE & " SET GeneralInfo.member = Null"
			else
				sSQL = "UPDATE " & GROUP_PROFILE & " SET GeneralInfo.member = (" & szNewMembers & ")"
			end if	
				sSQL = sSQL & " WHERE GeneralInfo.GroupName = '" & szGroupName & "'"
			if g_bIsADInstallation then
				sSQL = sSQL & " AND " & sBracket(g_sParentURL) & " = '" & _
							szParentURL & "'"
			end if
			bRet = bSafeExec(oRS, cn, sSQL)
		End If

	End If

	Set cn = Nothing

End Sub

Function GetDeleteQuery(szOrgName, aprops, cn, nUserID)
	Dim sSQL
	sSQL = "Delete "
	sSQL = sSQL & " FROM " & sBracket(sGetPropProfile(aprops(1))) & " WHERE " ' 
	sSQL = sSQL & "GeneralInfo.org_id = '" & nUserID & "'" 
'	sSQL = sSQL &  " AND "
'	sSQL = sSQL & sBracket(g_sParentURL) & " = '" & sGetParentDN(NULL) & "'"
	GetDeleteQuery = sSQL
End Function

%>

