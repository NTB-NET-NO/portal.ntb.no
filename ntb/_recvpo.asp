<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<%
' =============================================================================
' _recvpo.asp
' This page receives PO's from BizTalk. 
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Dim m_userid
Dim MSCSPipelines, MSCSMessageManager, sLanguage, MSCSDataFunctions
Dim MSCSCommerceSiteName, MSCSAppFrameWork, dictConfig
Dim MSCSProfileService

Set MSCSPipelines = Application("MSCSPipelines")    
Set MSCSMessageManager = Application("MSCSMessageManager")
sLanguage = MSCSMessageManager.DefaultLanguage
Set MSCSDataFunctions  = Application("MSCSDataFunctions")
m_userid = "{00000000-0000-0000-0000-000000000000}"

MSCSCommerceSiteName = Application("MSCSCommerceSiteName")
Set MSCSAppFrameWork = Application("MSCSAppFrameWork")
Set dictConfig = Application("MSCSAppConfig").GetOptionsDictionary("")
Set MSCSProfileService = Application("MSCSProfileService")

Call Main()
' --- End of page ---


'$$ These functions are prime candidates for componentization
Sub Main()
	Dim objXMLTransforms, szXML, xmlSchema
	Dim mscsOrderGrp, mscsOrderForm
	Dim sFilePath, sTrackingNumber, sOrderID
	
	szXML = ParseRequestForm()
	Set objXMLTransforms = Server.CreateObject("Commerce.DictionaryXMLTransforms")
	sFilePath = Server.MapPath("\" & MSCSAppFrameWork.VirtualDirectory) & "\poschema.xml"
	Set xmlSchema = objXMLTransforms.GetXMLFromFile(sFilepath) ' Can also be read from WebDAV
	Set mscsOrderForm = objXMLTransforms.ReconstructDictionaryFromXML(szXML, xmlSchema)

	Set mscsOrderGrp = GetOrderGroup(m_userid)
	Call mscsOrderGrp.AddOrderForm(mscsOrderForm)
	Call RunOrderPipeline(mscsOrderGrp)

	sOrderID = mscsOrderGrp.SaveAsOrder(sTrackingNumber)
End Sub


' -----------------------------------------------------------------------------
' ParseRequestForm
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function ParseRequestForm()
	Dim PostedDocument
	Dim ContentType
	Dim CharSet
	Dim EntityBody
	Dim Stream
	Dim StartPos
	Dim EndPos

	ContentType = Request.ServerVariables( "CONTENT_TYPE" )

	' Determine request entity body character set (default to us-ascii)
	CharSet = "us-ascii"
	StartPos = InStr( 1, ContentType, "CharSet=""", 1)
	If (StartPos > 0 ) then
		StartPos = StartPos + Len("CharSet=""")
		EndPos = InStr( StartPos, ContentType, """",1 )
		CharSet = Mid (ContentType, StartPos, EndPos - StartPos )
	End if
  
	'
	' Check for multipart MIME message
	'
	PostedDocument = ""

	if ( ContentType = "" or Request.TotalBytes = 0) then
		'
		' Content-Type is required as well as an entity body
		'
		Response.Status = "406 Not Acceptable"
		Response.Write "Content-type or Entity body is missing" & VbCrlf
		Response.Write "Message headers follow below:"  & VbCrlf
		Response.Write Request.ServerVariables("ALL_RAW") & VbCrlf
		Response.End
	else
		if ( InStr( 1,ContentType,"multipart/" ) > 0 ) then
			'
			' MIME multipart message. Build MIME header
			'
			PostedDocument = "MIME-Version: 1.0" & vbCrLf & "Content-Type: " & ContentType & vbCrLf & vbCrLf
			PostedDocument = PostedDocument & "This is a multi-part message in MIME format." & vbCrLf 
		End if
		   
		'
		' Get the post entity body
		'
	
		EntityBody = Request.BinaryRead (Request.TotalBytes )
		   
		'
		' Convert to UNICODE
		'
		Set Stream = Server.CreateObject("AdoDB.Stream")
		Stream.Type = 1						'adTypeBinary
		stream.Open
		Stream.Write EntityBody
		Stream.Position = 0
		Stream.Type = 2						'adTypeText
		Stream.Charset = CharSet
		PostedDocument = PostedDocument & Stream.ReadText
		Stream.Close
		Set Stream = Nothing
	End if
	
	ParseRequestForm = PostedDocument
End Function


' -----------------------------------------------------------------------------
' RunOrderPipeline
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RunOrderPipeline(mscsOrderGrp)
	Dim iErrorLevel

	iErrorLevel = RunMtsPipeline(MSCSPipelines.ReceivePO, GetPipelineLogFile("ReceivePO"), mscsOrderGrp)
	If iErrorLevel<>1 Then 
		Dim sOrderFormName, sErr, sErrs, mscsOrderForm
		For Each sOrderFormName In mscsOrderGrp.value("Orderforms")
			Set mscsOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
			For Each sErr In mscsOrderForm.Value(BASKET_ERRORS)
				sErrs = sErrs & sErr & CRLF
			Next
		Next
		For Each sOrderFormName In mscsordergrp.value("Orderforms")
			Set mscsOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
			For Each sErr In mscsOrderForm.Value(PURCHASE_ERRORS)
				sErrs = sErrs & sErr & CRLF
			Next
		Next
		Response.Status = "500 " & sErrs
		Response.End
		Err.Raise HRESULT_E_FAIL,,sErrs
	End If
	
End Sub
%>  