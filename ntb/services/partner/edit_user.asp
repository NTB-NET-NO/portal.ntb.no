<!-- #INCLUDE FILE="../../include/header.asp" -->
<!-- #INCLUDE FILE="../../include/const.asp" -->
<!-- #INCLUDE FILE="../../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../../include/catalog.asp" -->
<!-- #INCLUDE FILE="../../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../../include/form_lib.asp" -->
<!-- #INCLUDE FILE="../include/initialize.asp"                          -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/lib.asp"			                        -->
<!-- #INCLUDE FILE="../include/profiles_common.asp"                     -->
<!-- #INCLUDE FILE="../include/check_profile.asp"                       -->
<!-- #INCLUDE FILE="../include/delete_profiles.asp"                     -->
<!-- #INCLUDE FILE="../include/get_profiles.asp"                        -->
<!-- #INCLUDE FILE="../include/save_all_profiles.asp"                   -->
<!-- #INCLUDE FILE="../include/save_multiple_profiles.asp"              -->
<!-- #INCLUDE FILE="../include/save_one_profile.asp"                    -->
<!-- #INCLUDE FILE="../include/edit_handlers.asp"	                    -->
<!-- #INCLUDE FILE="../include/messagebox_handlers.asp"	                -->
<!-- #INCLUDE FILE="../include/password_handlers.asp"	                -->
<!-- #INCLUDE FILE="../include/render_common.asp"	                    -->
<!-- #INCLUDE FILE="../include/render_edit_common.asp"	                -->
<!-- #INCLUDE FILE="../include/render_edit_blank.asp"	                -->
<!-- #INCLUDE FILE="../include/render_edit_single.asp"	                -->
<!-- #INCLUDE FILE="../include/render_edit_multi.asp"	                -->
<!-- #INCLUDE FILE="../include/render_messagebox.asp"	                -->
<!-- #INCLUDE FILE="../include/render_password.asp"	                    -->
<!-- #INCLUDE FILE="../include/ad_routines.asp"	                    -->
<!-- #INCLUDE FILE="../../template/layout1.asp" -->
<%
' =============================================================================
' edit_user.asp
' User management for partners.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim sAction, sSubAction, sProfileIDs
	
	Call EnsureAuthAccess()

	' Make sure user has admin priviledges
	Call CheckPartnerServiceAccess()

	Call InitializePartnerDesk(sAction, sSubAction, sProfileIDs)		
	If Not IsNull(sProfileIDs) Then Call CheckIfProfilesExist(sSubAction, sProfileIDs)
	
	' Get this page content    
    htmPageContent = htmEditPageActionHandlers(sAction, sSubAction, sProfileIDs)    
End Sub


' -----------------------------------------------------------------------------
' htmEditPageActionHandlers
'
' Description:
'	Function to call appropriate handlers for all the EDIT type pages. This
'   function can be used for all the profiles.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmEditPageActionHandlers(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim htm
    Dim iProfileCount, iStatusID
    Dim listProfileSchema
    Dim sStatusField, sProfileListPage, sSourcePage, sActionPage, sPreviousAction
    Dim bValidPostedValues, bDisplayPostedValues, bDirty

    ' Check the sAction and call appropriate action handlers
    Select Case sAction
        Case ADD_ACTION
			Call PrepareAddAction(sStatusField, iStatusID, _
					bValidPostedValues, bDisplayPostedValues, _
					listProfileSchema)
            htm = htmAddActionHandler(sAction, sSubAction, sProfileIDs, _
					sStatusField, iStatusID, _
					bValidPostedValues, bDisplayPostedValues, _
					listProfileSchema)
        
        Case COPY_ACTION
			Call PrepareCopyAction(sSubAction, sProfileIDs, iProfileCount, _
						iStatusID, sStatusField, bValidPostedValues, _
						bDisplayPostedValues, sProfileListPage, listProfileSchema)
            htm = htmCopyActionHandler(sAction, sSubAction, sProfileIDs, _
						iProfileCount, iStatusID, sStatusField, bValidPostedValues, _
						bDisplayPostedValues, sProfileListPage, listProfileSchema)
        
        Case EDIT_ACTION
			Call PrepareEditAction(sSubAction, sProfileIDs, _
					iProfileCount, listProfileSchema)
            htm = htmEditAction(sAction, sSubAction, sProfileIDs, _
					iProfileCount, listProfileSchema)
        
        Case DELETE_ACTION
			Call PrepareDeleteAction(sSubAction, sProfileIDs, sProfileListPage, _
					iProfileCount)
            htm = htmDeleteActionHandler(sAction, sSubAction, sProfileIDs, _
					sProfileListPage, iProfileCount)	
        
        Case DELETEALL_ACTION
			Call PrepareDeleteAllAction(sProfileListPage)
            Call DeleteAllActionHandler(sAction, sSubAction, sProfileIDs, sProfileListPage)
        
        Case CHANGE_ACTION
	        Call PrepareSaveChangesConfirmation(sSubAction, sPreviousAction, _
				listProfileSchema, bValidPostedValues, bDisplayPostedValues)
            htm = htmChangePasswordActionHandler(sAction, sSubAction, sProfileIDs, _
					sPreviousAction, listProfileSchema, bValidPostedValues, bDisplayPostedValues)        
        
        Case CANCEL_ACTION
			Call PrepareCloseAction(sSubAction, sProfileIDs, sActionPage, sPreviousAction, sStatusField, _
				iStatusID, bValidPostedValues, bDisplayPostedValues, _
				listProfileSchema, sSourcePage, bDirty)
            htm = htmCloseActionHandler(sAction, sSubAction, sProfileIDs, _
				sActionPage, sPreviousAction, sStatusField, _
				iStatusID, bValidPostedValues, bDisplayPostedValues, _
				listProfileSchema, sSourcePage, bDirty)        
        
        Case SAVE_ACTION
			Call PrepareSaveAction()
            htm = htmSaveActionHandler(sAction, sSubAction, sProfileIDs)
        
        Case SELECTALL_ACTION
			Call PrepareSelectAllAction(sProfileListPage)
            Call SelectAllActionHandler(sProfileListPage)
        
        Case DESELECTALL_ACTION
			Call PrepareDeselectAllAction(sProfileListPage)
            Call DeSelectAllActionHandler(sProfileListPage)
            
        Case Else
            Response.Redirect(GenerateURL(MSCSSitePages.BadAction, Array(), Array()))            
    End Select
    
    Set listProfileSchema = Nothing
    htmEditPageActionHandlers = htm
End Function
%>
