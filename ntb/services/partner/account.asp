<!-- #INCLUDE FILE="../../include/header.asp" -->
<!-- #INCLUDE FILE="../../include/const.asp" -->
<!-- #INCLUDE FILE="../../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../../include/catalog.asp" -->
<!-- #INCLUDE FILE="../../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../../include/form_lib.asp" -->
<!-- #INCLUDE FILE="../include/initialize.asp"                          -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/lib.asp"			                        -->
<!-- #INCLUDE FILE="../include/profiles_common.asp"                     -->
<!-- #INCLUDE FILE="../include/check_profile.asp"                       -->
<!-- #INCLUDE FILE="../include/get_profiles.asp"                        -->
<!-- #INCLUDE FILE="../include/save_all_profiles.asp"                   -->
<!-- #INCLUDE FILE="../include/save_multiple_profiles.asp"              -->
<!-- #INCLUDE FILE="../include/save_one_profile.asp"                    -->
<!-- #INCLUDE FILE="../include/edit_handlers.asp"	                    -->
<!-- #INCLUDE FILE="../include/messagebox_handlers.asp"	                -->
<!-- #INCLUDE FILE="../include/render_common.asp"	                    -->
<!-- #INCLUDE FILE="../include/render_edit_common.asp"	                -->
<!-- #INCLUDE FILE="../include/render_edit_blank.asp"	                -->
<!-- #INCLUDE FILE="../include/render_edit_single.asp"	                -->
<!-- #INCLUDE FILE="../include/render_edit_multi.asp"	                -->
<!-- #INCLUDE FILE="../include/render_messagebox.asp"	                -->
<!-- #INCLUDE FILE="../../template/layout1.asp" -->
<%
' =============================================================================
' account.asp
' Account management for partners.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()	
	Dim sAction, sSubAction, sPreviousAction, sActionPage, sProfileIDs
	Dim iProfileCount
	Dim listProfileSchema
	Dim bValidPostedValues, bDisplayPostedValues
	Dim sStatusField, iStatusID
	
	Call EnsureAuthAccess()

	' Make sure user has admin privileges
	Call CheckPartnerServiceAccess()

	Call InitializePartnerDesk(sAction, sSubAction, sProfileIDs)
	If IsNull(sAction) Then sAction	= EDIT_ACTION
	If IsNull(sSubAction) Then sSubAction = EDIT_ACTION
    If Not IsNull(sProfileIDs) Then Call CheckIfProfilesExist(sSubAction, sProfileIDs)
        
    ' Making sure that the current user can only see his own organizationprofile.
	'     Tweaking the organization profile ID posted to this page, will have no
	'     effect on the organization details seen by the user
	' $$ START OF CHANGE
	' $$ was: sProfileIDs = mscsUserProfile(USER_ORGID).Value
	If VarType(mscsUserProfile) <> 0 Then
		sProfileIDs = mscsUserProfile(USER_ORGID).Value
	Else
		sProfileIDs = m_UserID
	End If
	' $$ END OF CHANGE
        
    ' Get this page content
    Select Case sAction
	    Case EDIT_ACTION
		    Call PrepareEditAction(sSubAction, sProfileIDs, iProfileCount, listProfileSchema)
            htmPageContent = htmEditAction(sAction, sSubAction, sProfileIDs, iProfileCount, listProfileSchema)
        Case SAVE_ACTION
			Call PrepareSaveAction()
            htmPageContent = htmSaveActionHandler(sAction, sSubAction, sProfileIDs)
        Case SELECT_ACTION
			Call PrepareSelectActionHandlerForEdit(sAction, sSubAction, sProfileIDs, _
				sActionPage, sPreviousAction, listProfileSchema, _
				bValidPostedValues, bDisplayPostedValues)
			htmPageContent = htmSelectActionHandlerForEdit(sAction, sSubAction, sProfileIDs, _
				sActionPage, sPreviousAction, listProfileSchema, _
				bValidPostedValues, bDisplayPostedValues)      
        Case SELECTED_ACTION
			Call PrepareSelectedAction(sAction, sSubAction, sProfileIDs, _
				sStatusField, iStatusID, bValidPostedValues, bDisplayPostedValues, listProfileSchema)
	        htmPageContent = htmSelectedActionHandlerForEdit(sAction, sSubAction, sProfileIDs, _
				sStatusField, iStatusID, bValidPostedValues, bDisplayPostedValues, listProfileSchema)
        Case Else
            Response.Redirect(GenerateURL(MSCSSitePages.BadAction, Array(), Array()))
	End Select
End Sub
%>
