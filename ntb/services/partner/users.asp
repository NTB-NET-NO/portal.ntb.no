<!-- #INCLUDE FILE="../../include/header.asp" -->
<!-- #INCLUDE FILE="../../include/const.asp" -->
<!-- #INCLUDE FILE="../../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../../include/catalog.asp" -->
<!-- #INCLUDE FILE="../../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../../include/form_lib.asp" -->
<!-- #INCLUDE FILE="../include/initialize.asp"                          -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/lib.asp"			                        -->
<!-- #INCLUDE FILE="../include/get_profiles.asp"                        -->
<!-- #INCLUDE FILE="../include/profiles_common.asp"                     -->
<!-- #INCLUDE FILE="../include/list_handlers.asp"	                    -->
<!-- #INCLUDE FILE="../include/render_common.asp"	                    -->
<!-- #INCLUDE FILE="../include/render_list.asp"	                        -->
<!-- #INCLUDE FILE="../../template/layout1.asp" -->
<%
' =============================================================================
' users.asp
' User management for partners.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim sAction
	Dim sSubAction
	Dim sProfileIDs
	
	Call EnsureAuthAccess()

	' Make sure user has admin priviledges
	Call CheckPartnerServiceAccess()
	 	
	Call InitializePartnerDesk(sAction, sSubAction, sProfileIDs)
	If IsNull(sAction) Then sAction	= LIST_ACTION
	
	' Get this page content
	htmPageContent = htmListPageActionHandlers(sAction, sSubAction, sProfileIDs)
End Sub


' -----------------------------------------------------------------------------
' htmListPageActionHandlers
'
' Description:
'	LIST page Action Handlers
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmListPageActionHandlers(ByRef sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim htm
    Dim iPageNumber, iTotalPages, iTotalRecords, iRecordsDisplayed

    ' Initialize the paging variables
    Call InitializePagingVariables(sAction, iPageNumber, iTotalPages, True)

	' Add action handlers for this page
	Select Case sAction
	    Case SELECTED_ACTION
	        Call SelectedActionHandlerForList(sAction, sSubAction, sProfileIDs)
	         
	    Case LIST_ACTION, _
	         SELECT_ACTION, _
	         FILTER_ACTION, _
	         PAGING_ACTION, _
	         FIRSTPAGE_ACTION, _
	         LASTPAGE_ACTION, _
	         PREVIOUSPAGE_ACTION, _
	         NEXTPAGE_ACTION	    
	         
            htm = htmRenderProfilesList(sAction, sSubAction, iPageNumber, iTotalPages)
        Case Else
            Response.Redirect(GenerateURL(MSCSSitePages.BadAction, Array(), Array()))
    End Select   	        
    
    htmListPageActionHandlers = htm
End Function
%>
