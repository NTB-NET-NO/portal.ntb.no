<%
' =============================================================================
' order_handlers.asp
' Action handlers for Order actions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' PrepareOrderDetailsAction
'
' Description:
'	"Order details" action handler
'
' Parameters:
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'	iProfileCount				- Count of the number of profiles in the set
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareOrderDetailsAction(ByVal sSubAction, ByVal sProfileIDs, ByRef iProfileCount)
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
End Sub


' -----------------------------------------------------------------------------
' htmOrderDetailsActionHandler
'
' Description:
'	Rendering function for "Order details" action handler
'
' Parameters:
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'	iProfileCount				- Count of the number of profiles in the set
'
' Returns:
'	String with HTML
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmOrderDetailsActionHandler(ByVal sAction, ByVal sSubAction, _
	ByVal sProfileIDs, ByVal iProfileCount)
    Dim htm
        
    ' Check if at least one order profile has been selected
    If iProfileCount = 1 Then
        ' Display the order details
        htm = htmRenderOrderDetailsForIDs(sAction, sSubAction, sProfileIDs)
    Else
        ' Redirect to the corresponding list page
        Call RedirectOrderDetailsForIDs()
    End If
    
    htmOrderDetailsActionHandler = htm
End Function


' -----------------------------------------------------------------------------
' RedirectOrderDetailsForIDs
'
' Description:
'	Redirector sub for "Order details for IDs" action
'
' Parameters:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RedirectOrderDetailsForIDs()
	Dim sProfileListPage, sActionPage
	
	sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
	sActionPage = GenerateURL(sProfileListPage, Array(STATUS_FIELD_NAME, STATUS_FIELD_ID), Array(STATUS_FIELD_VALUE_ERROR, E_SELECT_ONE_ORDER))
	Response.Redirect (sActionPage)
End Sub


' -----------------------------------------------------------------------------
' htmRenderOrderDetailsForIDs
'
' Description:
'	Rendering function for "Order Details for IDs"
'
' Parameters:
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderOrderDetailsForIDs(sAction, sSubAction, sProfileIDs)
	Dim rsOrder
	
	Set rsOrder = rsGetOrderDetails(sProfileIDs)        
    htmRenderOrderDetailsForIDs = htmRenderOrderDetails(sAction, sSubAction, sProfileIDs, rsOrder)        
    Set rsOrder = Nothing
End Function


' -----------------------------------------------------------------------------
' PrepareCloseOrderDetailsAction
'
' Description:
'	Close read-only edit sheet
'
' Parameters:
'	sProfileListPage			- Page that lists the profiles
'	sSourcePage					- Page where action occurred
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareCloseOrderDetailsAction(ByRef sProfileListPage, ByRef sSourcePage)
	sProfileListPage = ""
    sSourcePage = GetRequestString(SOURCE_PAGE, Null)
    If IsNull(sSourcePage) Then
	    sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
    End If
End Sub


' -----------------------------------------------------------------------------
' CloseOrderDetailsActionHandler
'
' Description:
'	"Order Details" action handler
'
' Parameters:
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'	sSourcePage					- Page where action occurred
'	sProfileListPage			- Page that lists the profiles
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CloseOrderDetailsActionHandler(ByVal sAction, ByVal sSubAction, _
		ByVal sProfileIDs, ByVal sSourcePage, ByVal sProfileListPage)
    
    ' Redirect to either source page (if present) or the corresponding list 
    '     page for this profile
    If Not IsNull(sSourcePage) Then
        Response.Redirect (sSourcePage)
    Else
	    sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
        Response.Redirect GenerateURL(sProfileListPage, Array(), Array())
    End If
End Sub
%>