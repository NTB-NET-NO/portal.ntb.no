<%
' =============================================================================
' render_common.asp
' Common rendering routines
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' sGetPageHeading
'
' Description:
'	Function to get the page heading
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetPageHeading(ByVal sPageName)
    Dim sPageHeading

    Select Case sPageName
        ' Common pages for both partner and customer services
        Case MSCSSitePages.ChangePassword
            sPageHeading = mscsMessageManager.GetMessage("L_HEADING_CHANGE_PASSWORD_Text", sLanguage)
	    Case MSCSSitePages.HelpPage
	        sPageHeading = ""
	        
	    ' partner service pages	
	    Case MSCSSitePages.EditPartnerAccount
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_ORGANIZATION_DETAIL_Text", sLanguage)
	    Case MSCSSitePages.ListPartnerUsers
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_USER_LIST_Text", sLanguage)
	    Case MSCSSitePages.EditPartnerUser
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_USER_DETAIL_Text", sLanguage)
	    Case MSCSSitePages.ListPartnerOrders
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_ORDER_LIST_Text", sLanguage)
	    Case MSCSSitePages.ViewPartnerOrder
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_ORDER_DETAIL_Text", sLanguage)
	    ' customer service pages for requisitioners
	    Case MSCSSitePages.EditUserAccount
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_USER_DETAIL_Text", sLanguage)
	    Case MSCSSitePages.ListUserOrders
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_ORDER_LIST_Text", sLanguage)
	    Case MSCSSitePages.ViewUserOrder
	        sPageHeading = mscsMessageManager.GetMessage("L_HEADING_ORDER_DETAIL_Text", sLanguage)
        Case Else
			Err.Raise E_NOTIMPL
    End Select
    
    sGetPageHeading = sPageHeading
End Function


' -----------------------------------------------------------------------------
' rowRenderHeader
'
' Description:
'	Function to display the header information for the current page
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderHeader()
    Dim htmColumn, sBtnText, htmColumAttrib, htmRow
    Dim arrCell, arrCellAlignment

    ' sBtnText = mscsMessageManager.GetMessage("L_HELP_ACTION_BUTTON_Text", sLanguage)
    'htmColumn = RenderSubmitButton(BUTTON_SUBMIT & HELP_ACTION, sBtnText, MSCSSiteStyle.Button)
    'arrCell = Array(htmColumn)
    'arrCellAlignment = Array(ALIGN_RIGHT)
    'htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.StatusTable, False, "")

    'rowRenderHeader = htmRow
End Function


' -----------------------------------------------------------------------------
' rowRenderStatusInfo
'
' Description:
'	Function to display the status information for the current page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderStatusInfo(ByVal sStatusField, ByVal iStatusID, _
                             ByVal iProfileCount, ByVal bDisplayCount)
	' Password field size constraints
	Const L_PasswordMaxLength_Number = 14
	Const L_PasswordMinLength_Number = 7

    Dim htmColumn, htmColumAttrib, htmRow
    Dim arrCell, arrCellAlignment
    Dim url
    Dim sErrMsg
    
    If IsNull(sStatusField) Then
        sStatusField = GetRequestString(STATUS_FIELD_NAME,Null)
        iStatusID = MSCSAppFrameWork.RequestNumber(STATUS_FIELD_ID, Null)    
    End If
    
    Select Case sStatusField
        Case STATUS_FIELD_VALUE_ERROR
             Select Case iStatusID
                Case E_SELECT_ONE_RECORD
                     sErrMsg = mscsMessageManager.GetMessage("L_MSG_SELECT_ONEPROFILE_Text", sLanguage)
                Case E_NO_RECORD_SELECTED
                     sErrMsg = mscsMessageManager.GetMessage("L_MSG_SELECT_PROFILES_Text", sLanguage)
                Case E_ENTITY_RECORDS_DELETED
                     sErrMsg = mscsMessageManager.GetMessage("L_MSG_ENTITY_RECORD_NOT_DELETED_Text", sLanguage)
                Case E_USER_INPUT_ERROR
                     sErrMsg = mscsMessageManager.GetMessage("L_STATUS_MSG_InvalidForm_Text", sLanguage)
                Case E_SELECT_ONE_ORDER
                     sErrMsg = mscsMessageManager.GetMessage("L_MSG_SELECT_ONEORDER_Text", sLanguage)   
                Case E_PRIMARY_KEY_VIOLATION
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_MSG_NONPRIMARY_Text", sLanguage)
                Case E_UNIQUE_KEY_VIOLATION
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_MSG_NONUNIQUE_Text", sLanguage)
                Case E_PASSWORD_MISMATCH
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_USERPASSWORD_MISMATCH_Text", sLanguage)
                Case E_PASSWORD_EMPTY
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_USERPASSWORD_EMPTY_Text", sLanguage)
				Case E_PASSWORD_WRONG_LENGTH
					 sErrMsg = FormatOutput(MSCSMessageManager.GetMessage("L_BadPassword_ErrorMessage", sLanguage), Array(L_PasswordMinLength_Number, L_PasswordMaxLength_Number))
                Case E_LOGONNAME_LENGTH
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_LOGONNAME_INVALID_Text", sLanguage)
				Case E_LOGONNAME_BADCHARS
                     sErrMsg = mscsMessageManager.GetMessage("L_Bad_UserName_Pattern_ErrorMessage", sLanguage)
				Case E_PASSWORD_BADCHARS
                     sErrMsg = mscsMessageManager.GetMessage("L_Bad_Password_Pattern_ErrorMessage", sLanguage)
                Case E_ADPASSWORD_BADCHARS
                     sErrMsg = mscsMessageManager.GetMessage("L_Bad_ADPassword_Pattern_ErrorMessage", sLanguage)
                Case E_PASSWORDSET_FAIL
                     sErrMsg = mscsMessageManager.GetMessage("L_Invalid_Login_Password_ErrorMessage", sLanguage)
                Case E_USER_ALREADY_EXIST
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_USER_EXIST_Text", sLanguage)
                Case E_SAVING_PROFILE_PROPERTIES
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_SAVING_PROFILEPROPERTIES_Text", sLanguage)
                Case E_ADDING_USER_TO_GROUP_IN_AD
                     sErrMsg = mscsMessageManager.GetMessage("L_ERROR_ADDING_USERINAD_Text", sLanguage)
                Case E_NO_PROFILES_PERPAGE
					 sErrMsg = mscsMessageManager.GetMessage("L_MSG_FILTER_NOPROFILESPERPAGE_Text", sLanguage)
				Case E_FILTER_NO_RECORDS
					 sErrMsg = mscsMessageManager.GetMessage("L_MSG_FILTER_NORECORDS_Text", sLanguage)
             End Select
             
             htmColumn = FormatOutput(CONTROL_ERROR_TEMPLATE, Array(mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage), sErrMsg))
    
        Case STATUS_FIELD_VALUE_SUCCESS             
             Select Case iStatusID
                Case S_NEW_ENTITY_RECORD_ADDED
                     htmColumn = mscsMessageManager.GetMessage("L_MSG_PROFILE_ADDED_Text", sLanguage)
                Case S_ENTITY_RECORD_UPDATED
                     htmColumn = mscsMessageManager.GetMessage("L_MSG_PROFILE_UPDATED_Text", sLanguage)
                Case S_ENTITY_RECORDS_DELETED
                     htmColumn = mscsMessageManager.GetMessage("L_MSG_PROFILE_DELETED_Text", sLanguage)                
                Case S_EDITING_MULTIPLE_PROFILES
                     htmColumn = mscsMessageManager.GetMessage("L_MSG_MULTIPLE_EDIT_Text", sLanguage)
                Case S_ADDING_NEW_PROFILE
                     htmColumn = mscsMessageManager.GetMessage("L_MSG_ADDING_PROFILE_Text", sLanguage)
                Case S_EDITING_EXISTING_PROFILE                
                     htmColumn = mscsMessageManager.GetMessage("L_MSG_EDITING_PROFILE_Text", sLanguage)
                Case Else
             End Select

        Case Else
             If (iProfileCount > 0) And bDisplayCount Then
                htmColumn = FormatOutput(mscsMessageManager.GetMessage("L_STATUS_MSG_LISTPAGE_Text", sLanguage), _
                    Array(CStr(iProfileCount)))
             Else
                htmColumn = htmRenderBlankSpaces(4)
             End If
    End Select    

    arrCell = Array(RenderText(htmColumn, MSCSSiteStyle.StatusString))
    arrCellAlignment = Array(ALIGN_CENTER)
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.StatusTable, False, "")

    rowRenderStatusInfo = htmRow
End Function


' -----------------------------------------------------------------------------
' sReConstructPageURL
'
' Description:
'	Function to re-construct the page url and replaces the key values too.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sReConstructPageURL(ByVal arrQSKeyName, ByVal arrQSKeyValue)
    Dim i
    Dim sPageName, sQueryString, sEncodedKey, sEncodedNewValue
    Dim url
    
    ' Initialize the url with complete script name for the current page
    url = MSCSAppFrameWork.GetRequestProtocol() & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("SCRIPT_NAME")
    
    ' Get the QUERY STRING part of the URL for the current page
    sQueryString = Request.ServerVariables("QUERY_STRING")
    
    ' Loop through the QUERY STRING collection and replace the key/value pair
    '     passed to this function
    For i = LBound(arrQSKeyName) To UBound(arrQSKeyName)
        sEncodedKey = Server.URLEncode(arrQSKeyName(i))
        sEncodedNewValue = Server.URLEncode(arrQSKeyValue(i))
        sQueryString = sReplaceString(sQueryString, sEncodedKey, sEncodedNewValue)
    Next    
    
    ' Add "?" to the url if ther's some contents in the QUERY STRING
    If Len(sQueryString) > 0 Then
		url = url & "?" & sQueryString
	End If
        
    sReConstructPageURL = url
End Function


' -----------------------------------------------------------------------------
' sReplaceString
'
' Description:
'	Function to replace a value for a key in a string with a new value.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sReplaceString(ByVal sQueryString, ByVal sEncodedKey, ByVal sEncodedNewValue)
    Dim iLocationOfKey, iLocationOfAnd, iLenOfPrevValue    
    Dim url, sAppendToURL
    
    ' Check if the key exists
    iLocationOfKey = InStr(1, sQueryString, sEncodedKey, 1)    
    If iLocationOfKey > 0 Then
        ' Key is present in the string
        url = Mid(sQueryString, 1, iLocationOfKey + Len(sEncodedKey))
        iLocationOfAnd = InStr(Len(url), sQueryString, "&", 1)
        sAppendToURL = ""
        If iLocationOfAnd > 0 Then sAppendToURL = Mid(sQueryString, iLocationOfAnd, Len(sQueryString))
        url = url & sEncodedNewValue & sAppendToURL
    Else
        ' Key is not present in the string, Add it to the string
        url = sQueryString
        iLocationOfAnd = InStr(1, sQueryString, "&", 1)
        If (iLocationOfAnd > 0) OR Len(sQueryString) > 0 Then
            url = url & "&"
        End If
        url = url & sEncodedKey & "=" & sEncodedNewValue        
    End If
    
    sReplaceString = url
End Function


' -----------------------------------------------------------------------------
' colRenderSimpleSearchTab
'
' Description:
'	Function to add Search option tab (Simple) to LIST type pages
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderSimpleSearchTab(ByVal sCriteria)
    Dim url, urlItem
    Dim htmColumn
    Dim arrQSKeyName, arrQSKeyValue    
                
    ' Show "Simple" search option as a link if current user selection
    '     is "Advanced".
    If StrComp(sCriteria, SEARCH_CRITERIA_ADVANCED, vbTextCompare) = 0 Then
        arrQSKeyName = Array(QUERY_STRING_CRITERIA)
        arrQSKeyValue = Array(SEARCH_CRITERIA_SIMPLE)    
        url = sReConstructPageURL(arrQSKeyName, arrQSKeyValue)
        htmColumn = RenderLink(url, mscsMessageManager.GetMessage("L_SEARCH_CRITERIA_SIMPLE_Text", sLanguage), MSCSSiteStyle.Link)
    Else
        ' Else, by default display the Simple search option
        htmColumn = Bold(mscsMessageManager.GetMessage("L_SEARCH_CRITERIA_SIMPLE_Text", sLanguage))
    End If

    colRenderSimpleSearchTab = htmColumn
End Function


' -----------------------------------------------------------------------------
' colRenderAdvancedSearchTab
'
' Description:
'	Function to add Search option tab (Advanced) to LIST type pages
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderAdvancedSearchTab(sCriteria)
    Dim url, urlItem
    Dim htmColumn    
    Dim arrQSKeyName, arrQSKeyValue

    ' If the user's current search option is "Advanced" then display the
    '     Advanced option as BOLD label
    If StrComp(sCriteria, SEARCH_CRITERIA_ADVANCED, vbTextCompare) = 0 Then
        htmColumn = Bold(mscsMessageManager.GetMessage("L_SEARCH_CRITERIA_ADVANCED_Text", sLanguage))
    Else
        ' Else, display the "Advanced" serach option tab as a LINK
        arrQSKeyName = Array(QUERY_STRING_CRITERIA)
        arrQSKeyValue = Array(SEARCH_CRITERIA_ADVANCED)    
        url = sReConstructPageURL(arrQSKeyName, arrQSKeyValue)        
        htmColumn = RenderLink(url, mscsMessageManager.GetMessage("L_SEARCH_CRITERIA_ADVANCED_Text", sLanguage), MSCSSiteStyle.Link)
    End If
    
    colRenderAdvancedSearchTab = htmColumn
End Function


' -----------------------------------------------------------------------------
' rowRenderSearchOptionForString
'
' Description:
'	Function to add search option contents for a property of type TEXTBOX
'
' Parameters:
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'	iProfileCount				- Count of the number of profiles in the set
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderSearchOptionForString(ByVal sFieldDisplayText, _
           ByVal sFieldName, ByVal bFieldMessage, ByVal iMaxSize, _
           ByVal varFieldValue)
    Dim htmTextBox
    Dim arrCell, arrCellAlignment
    
    htmTextBox = RenderTextBox(sFieldName, varFieldValue, DEFAULT_TEXT_BOX_WIDTH, iMaxSize, MSCSSiteStyle.TextBox)
    arrCell = Array(sFieldDisplayText, htmTextBox)
    arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
    rowRenderSearchOptionForString = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")
End Function


' -----------------------------------------------------------------------------
' rowRenderSearchOptionForInteger
'
' Description:
'	Function to add search option for a property of type COMBOBOX
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderSearchOptionForInteger(ByVal sFieldName, ByVal sDisplayName, ByVal iFieldValue)
    Dim htmColumn1, htmColumn2
    Dim arrCell, arrCellAlignment

    Select Case sFieldName
        Case FIELD_USER_STATUS
             htmColumn1 = sDisplayName
             htmColumn2 = htmRenderUserStatusOptions(iFieldValue, False)
        Case FIELD_USER_PARTNERDESK_ROLE             
             htmColumn1 = sDisplayName
             htmColumn2 = htmRenderPartnerDeskRoles(iFieldValue, False)
        Case FIELD_ORDER_STATUS             
             htmColumn1 = sDisplayName
             htmColumn2 = htmRenderOrderStatusOptions(iFieldValue)
        Case FIELD_ORDER_DATE_TYPE             
             htmColumn1 = sDisplayName
             htmColumn2 = htmRenderOrderDateTypes(iFieldValue)
    End Select
    
    arrCell = Array(htmColumn1, htmColumn2)
    arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
    rowRenderSearchOptionForInteger = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")
End Function


' -----------------------------------------------------------------------------
' rowRenderSearchOptionForDate
'
' Description:
'	Function to add search option for a property of type DATETIME
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderSearchOptionForDate(ByVal oProperty, ByVal dictSearchOptions)
    Dim htmColumn, htmRowDateTime
    Dim arrCell, arrCellAlignment
                                          
    htmColumn = RenderTextBox(oProperty.Name & FIELD_START_DATE, _
                              dictSearchOptions.Value(oProperty.Name & FIELD_START_DATE), _
                              DEFAULT_TEXT_BOX_WIDTH, _
                              DEFAULT_TEXT_BOX_WIDTH, _
                              MSCSSiteStyle.TextBox)
    htmColumn = htmColumn & htmRenderBlankSpaces(4) & _
                            mscsMessageManager.GetMessage("L_FIELD_END_DATE_Text", sLanguage) & _
                            htmRenderBlankSpaces(4) & _
							RenderTextBox(oProperty.Name & FIELD_END_DATE, _
                                          dictSearchOptions.Value(oProperty.Name & FIELD_END_DATE), _
                                          DEFAULT_TEXT_BOX_WIDTH, _
                                          DEFAULT_TEXT_BOX_WIDTH, _
                                          MSCSSiteStyle.TextBox)
    arrCell = Array(oProperty.Attributes.DisplayName, htmColumn)
    arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
    htmRowDateTime = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")
    
    rowRenderSearchOptionForDate = htmRowDateTime
End Function


' -----------------------------------------------------------------------------
' htmRenderEmptyRecordSet
'
' Description:
'	Function to return HTML TABLE for empty record-set
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderEmptyRecordSet(ByRef sMsg)
    Dim arrCell, arrCellAlignment
    Dim htmRow

	arrCell = Array(sMsg)
    arrCellAlignment = Array(ALIGN_CENTER)
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")
    htmRenderEmptyRecordSet = RenderTable(htmRow, MSCSSiteStyle.ContentsBodyTable)
End Function


' -----------------------------------------------------------------------------
' tableRenderFooterInfo
'
' Description:
'	Function to display footer information for the current page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderFooterInfo(ByVal sAction, ByVal sSubAction, ByVal iPageNumber, _
           ByVal iTotalRecords, ByVal iTotalPages, ByVal bRequiresPagingCtrls, _
           ByVal bRequiresLastPageCtrl)
    Dim htmRow
    Dim arrCell(), arrCellAlignment()
    Dim sPageType, sMsg0, sMsg1

    If (iTotalRecords > RECORDS_PER_PAGE) Or bRequiresPagingCtrls Then
        ReDim arrCell(3)
        ReDim arrCellAlignment(3)
    Else
        ReDim arrCell(1)
        ReDim arrCellAlignment(1)
    End If

    sPageType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_TYPE)

    If StrComp(sPageType, PAGE_TYPE_LIST, vbTextCompare) = 0 Then
		If bRequiresPagingCtrls Then
			sMsg0 = htmRenderBlankSpaces(20)
			sMsg1 = mscsMessageManager.GetMessage("L_MSG_PROFILE_PAGING_STATUS_Text", sLanguage)
		Else
			sMsg0 = FormatOutput(mscsMessageManager.GetMessage("L_MSG_TOTAL_STATUS_Text", sLanguage), Array(CStr(iTotalRecords)))
			sMsg1 = mscsMessageManager.GetMessage("L_MSG_PAGING_STATUS_Text", sLanguage)
		End If

        arrCell(0) = Bold(sMsg0)
        arrCellAlignment(0) = ALIGN_LEFT

        If (iTotalRecords > RECORDS_PER_PAGE) Or bRequiresPagingCtrls Then
           arrCell(1) = Bold(RenderText(FormatOutput(sMsg1, Array(iPageNumber, iTotalPages)), MSCSSiteStyle.Body))
           arrCellAlignment(1) = ALIGN_CENTER

           arrCell(2) = formRenderPagingButtons(sAction, sSubAction, iPageNumber, iTotalPages, bRequiresLastPageCtrl)
           arrCellAlignment(2) = ALIGN_RIGHT
        End If    
    End If

    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.FooterTable, False, "")
    tableRenderFooterInfo = RenderTable(htmRow, MSCSSiteStyle.FooterTable)
End Function


' -----------------------------------------------------------------------------
' formRenderPagingButtons
'
' Description:
'	Function to display paging buttons
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderPagingButtons(ByVal sAction, ByVal sSubAction, _
           ByVal iPageNumber, ByVal iTotalPages, ByVal bRequiresLastPageCtrl)    
    Dim htmContent
    Dim urlItem, url, sBtnText
    Dim arrQSKeyName, arrQSKeyValue    
    
    If (StrComp(sAction, SELECT_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sSubAction, SELECT_ACTION, vbTextCompare) = 0) Then
        arrQSKeyName = Array(SUB_ACTION_TYPE)
        arrQSKeyValue = Array(SELECT_ACTION)    
    Else
        arrQSKeyName = Array()
        arrQSKeyValue = Array() 
    End If
            
    url = sReConstructPageURL(arrQSKeyName,arrQSKeyValue)
    
    htmContent = RenderHiddenField(ACTION_TYPE, PAGING_ACTION)    
    htmContent = htmContent & _
                 RenderHiddenField(TOTAL_PAGES, iTotalPages)    
    
    sBtnText = FIRSTPAGE_ACTION
    htmContent = htmContent & RenderSubmitButton(BUTTON_SUBMIT & FIRSTPAGE_ACTION, sBtnText, MSCSSiteStyle.Button)
    
    sBtnText = PREVIOUSPAGE_ACTION
    htmContent = htmContent & RenderSubmitButton(BUTTON_SUBMIT & PREVIOUSPAGE_ACTION, sBtnText, MSCSSiteStyle.Button) _
					&   RenderTextBox(PAGE_NUM_FIELD, iPageNumber, PAGING_TEXT_BOX_WIDTH, _
                                PAGING_TEXT_BOX_WIDTH, MSCSSiteStyle.TextBox)
    
    sBtnText = NEXTPAGE_ACTION
    htmContent = htmContent & RenderSubmitButton(BUTTON_SUBMIT & NEXTPAGE_ACTION, sBtnText, MSCSSiteStyle.Button)

	If bRequiresLastPageCtrl Then
			' --- TRUE only for ORDERS page
		    sBtnText = LASTPAGE_ACTION
		    htmContent = htmContent & RenderSubmitButton(BUTTON_SUBMIT & LASTPAGE_ACTION, sBtnText, MSCSSiteStyle.Button)
    End If

    formRenderPagingButtons = RenderForm(url, htmContent, HTTP_POST)
End Function


' -----------------------------------------------------------------------------
' rowRenderEditSheetBlankLine
'
' Description:
'	Function to display a blank line in the entity record details
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderEditSheetBlankLine()
    Dim arrCell, arrCellAlignment

    arrCell = Array(htmRenderBlankSpaces(4), htmRenderBlankSpaces(4), htmRenderBlankSpaces(4))
    arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT, ALIGN_LEFT)
    rowRenderEditSheetBlankLine = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")
End Function

  
' -----------------------------------------------------------------------------
' cellRenderReadOnlyProperty
'
' Description:
'	Function to display read-only property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function cellRenderReadOnlyProperty(ByVal sFieldName, ByVal sDisplayName, _
           ByVal sFieldValue, ByVal sDisplayValue)
    Dim htmRow
    Dim arrCell, arrCellAlignment
    Dim htmSpaces
	htmSpaces = htmRenderBlankSpaces(4)

    arrCell = Array(htmSpaces, sDisplayName & htmSpaces, sDisplayValue & htmSpaces)
    arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT, ALIGN_LEFT)
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")
    
    ' Add a hidden FORM element for all the read-only properties.
    htmRow = htmRow & RenderHiddenField(sFieldName, sFieldValue)
    
    cellRenderReadOnlyProperty = htmRow
End Function


' -----------------------------------------------------------------------------
' htmRenderBlankSpaces
'
' Description:
'	Function to return html blank spaces( &nbsp;)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderBlankSpaces(ByVal nSpaces)
    Dim i, htm
    
    For i = 0 To (nSpaces - 1)
        htm = htm & "&nbsp;"
    Next

    htmRenderBlankSpaces = htm
End Function


' -----------------------------------------------------------------------------
' colRenderCountryNames
'
' Description:
'	Function to display all country names
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderCountryNames(ByVal oProperty, ByVal sPropertyValue)
    Dim arrCountries
    
    arrCountries = ConvertListToArray(MSCSAppConfig.GetCountryNamesList())
    colRenderCountryNames = htmRenderCountries(oProperty, sPropertyValue, arrCountries)
End Function


' -----------------------------------------------------------------------------
' htmRenderUserStatusOptions
'
' Description:
'	Function to display user status options
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderUserStatusOptions(ByVal sFieldValue, ByVal bIsRequired)

    htmRenderUserStatusOptions = htmRenderSelectTag(FIELD_USER_STATUS)
    If Not bIsRequired Then
		htmRenderUserStatusOptions = htmRenderUserStatusOptions & MSCSAppFrameWork.Option(BLANK_OPTION, sFieldValue) & BLANK_OPTION
	End If

	htmRenderUserStatusOptions = htmRenderUserStatusOptions _
		& MSCSAppFrameWork.Option(ACCOUNT_ACTIVE, sFieldValue) _
		& mscsMessageManager.GetMessage("L_USER_STATUS_ACTIVE_Text", sLanguage) _
		& MSCSAppFrameWork.Option(ACCOUNT_INACTIVE, sFieldValue) _
		& mscsMessageManager.GetMessage("L_USER_STATUS_INACTIVE_Text", sLanguage) _
		& htmRenderSelectEndTag()
End Function


' -----------------------------------------------------------------------------
' htmRenderPartnerDeskRoles
'
' Description:
'	Function to display user i_PartnerDeskRoleFlags options
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderPartnerDeskRoles(ByVal sFieldValue, ByVal bIsRequired)

	htmRenderPartnerDeskRoles = htmRenderSelectTag(FIELD_USER_PARTNERDESK_ROLE)
    If Not bIsRequired Then
		htmRenderPartnerDeskRoles = htmRenderPartnerDeskRoles & MSCSAppFrameWork.Option(BLANK_OPTION, sFieldValue) & BLANK_OPTION
	End If

	htmRenderPartnerDeskRoles = htmRenderPartnerDeskRoles _
			& MSCSAppFrameWork.Option(ROLE_USER, sFieldValue) _
			& mscsMessageManager.GetMessage("L_USER_PDROLE_USER_Text", sLanguage) _
			& MSCSAppFrameWork.Option(ROLE_ADMIN, sFieldValue) _
			& mscsMessageManager.GetMessage("L_USER_PDROLE_ADMIN_Text", sLanguage) _
			& htmRenderSelectEndTag()
End Function


' -----------------------------------------------------------------------------
' htmRenderOrderStatusOptions
'
' Description:
'	Function to display the order status options
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderOrderStatusOptions(ByVal sFieldValue)
    htmRenderOrderStatusOptions = htmRenderSelectTag(FIELD_ORDER_STATUS) _
		& MSCSAppFrameWork.Option(BLANK_OPTION, sFieldValue) & BLANK_OPTION _
		& MSCSAppFrameWork.Option(ORDER_STATUS_NEW_ORDER, sFieldValue) _
		& mscsMessageManager.GetMessage("L_ORDER_STATUS_NEWORDER_Text", sLanguage) _
		& MSCSAppFrameWork.Option(ORDER_STATUS_APPROVAL_PENDING, sFieldValue) _
		& mscsMessageManager.GetMessage("L_ORDER_STATUS_APPROVALPENDING_Text", sLanguage) _
		& MSCSAppFrameWork.Option(ORDER_STATUS_APPROVED, sFieldValue) _
		& mscsMessageManager.GetMessage("L_ORDER_STATUS_APPROVED_Text", sLanguage) _
		& MSCSAppFrameWork.Option(ORDER_STATUS_REJECTED, sFieldValue) _
		& mscsMessageManager.GetMessage("L_ORDER_STATUS_REJECTED_Text", sLanguage) _
		& MSCSAppFrameWork.Option(ORDER_STATUS_FULLFILLED, sFieldValue) _
		& mscsMessageManager.GetMessage("L_ORDER_STATUS_FULLFILLED_Text", sLanguage) _
		& htmRenderSelectEndTag()
End Function


' -----------------------------------------------------------------------------
' htmRenderOrderDateTypes
'
' Description:
'	Function to display the order date type for both user and organization orders
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderOrderDateTypes(ByVal sFieldValue)
    htmRenderOrderDateTypes = htmRenderSelectTag(FIELD_ORDER_DATE_TYPE) _
		& MSCSAppFrameWork.Option(BLANK_OPTION, sFieldValue) & BLANK_OPTION _
		& MSCSAppFrameWork.Option(ORDER_DATE_TYPE_CREATED, sFieldValue) _
		& mscsMessageManager.GetMessage("L_ORDER_DATE_CREATED_Text", sLanguage) _
		& MSCSAppFrameWork.Option(ORDER_DATE_TYPE_MODIFIED, sFieldValue) _
		& mscsMessageManager.GetMessage("L_ORDER_DATE_MODIFIED_Text", sLanguage) _
		& htmRenderSelectEndTag()
End Function


' -----------------------------------------------------------------------------
' htmRenderSiteTerm
'
' Description:
'	Renders a list box corresponding to a SiteTerm property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderSiteTerm(ByVal oProperty, ByVal varPropertyValue)
	Dim str, sName, sValue
	Dim dictOption

	sValue = ""
	If Not IsNull(varPropertyValue) Then sValue = CStr(varPropertyValue)

    str = htmRenderSelectTag(oProperty.Name)
    If Not oProperty.IsRequired Then
		str = str & MSCSAppFrameWork.Option(BLANK_OPTION, sValue) & BLANK_OPTION
	End If

	For Each dictOption In oProperty.Options
		For Each sName In dictOption
			str = str & MSCSAppFrameWork.Option(dictOption(sName), sValue) & sName
		Next
	Next
    str = str & htmRenderSelectEndTag()

    htmRenderSiteTerm = str
End Function


' -----------------------------------------------------------------------------
' htmRenderBoolean
'
' Description:
'	Renders a check-box corresponding to a boolean property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderBoolean(ByVal sPropertyName, ByVal varPropertyValue)
	htmRenderBoolean = RenderCheckBox (sPropertyName, "-1", varPropertyValue, MSCSSiteStyle.CheckBox)
End Function


' -----------------------------------------------------------------------------
' htmRenderCountries
'
' Description:
'	Function to render all the country names in an HTML combo box
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCountries(ByVal oProperty, ByVal sPropertyValue, ByVal arrCountryNames)
    Dim i, str
    Dim sCountryCode, sCountryName
        
    str = htmRenderSelectTag(oProperty.Name)
        str = str & MSCSAppFrameWork.Option(BLANK_OPTION, sPropertyValue) & BLANK_OPTION
        For i = LBound(arrCountryNames) To UBound(arrCountryNames)
            sCountryName = arrCountryNames(i)
            sCountryCode = MSCSAppConfig.GetCountryCodeFromCountryName(sCountryName)
            str = str & MSCSAppFrameWork.Option(sCountryCode, sPropertyValue) & sCountryName
        Next
    str = str & htmRenderSelectEndTag()
    
    htmRenderCountries = str
End Function


' -----------------------------------------------------------------------------
' htmRenderSelectTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderSelectTag(ByVal sSelectName)   
    htmRenderSelectTag = "<SELECT " & "NAME='" & sSelectName & "' " & ">"
End Function


' -----------------------------------------------------------------------------
' htmRenderSelectEndTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderSelectEndTag()   
    htmRenderSelectEndTag = "</SELECT>"
End Function
%>