<%
' =============================================================================
' messagebox_handlers.asp
' Action handlers for message boxes; Save Changes Confirmation
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================


' -----------------------------------------------------------------------------
' PrepareSaveChangesConfirmation
'
' Description:
'	"Save Changes Confirmation" handler
'
' Parameters: (all OUT except first)
'	sSubAction					- Subaction called
'	sPreviousAction				- Previous action
'	listProfileSchema			- SimpleList object that holds profile schema
'	bValidPostedValues			- Are the posted values valid?
'	bDisplayPostedValues		- Must the posted values be displayed?
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareSaveChangesConfirmation(ByVal sSubAction, ByRef sPreviousAction, _
			ByRef listProfileSchema, ByRef bValidPostedValues, ByRef bDisplayPostedValues)
	
	' Initialize
	Set listProfileSchema = Nothing
	bValidPostedValues = False
	bDisplayPostedValues = False
	
    sPreviousAction = GetRequestString(PREVIOUS_ACTION, Null)
    Select Case sSubAction
        ' The user has opted to save the updated values of the profile    
        Case MSGBOX_ACTION_YES
             ' Update the User profile properties for the profiles selected and 
             '    redirect accordingly             
             ' Validate the changes made to the values
             Set listProfileSchema = listCloneProfileSchema(sThisPage)
             bValidPostedValues = ValidateFields(listProfileSchema)         
        Case MSGBOX_ACTION_NO            
             bValidPostedValues = True
             bDisplayPostedValues = True
             Set listProfileSchema = listCloneProfileSchema(sThisPage)
    End Select
End Sub


' -----------------------------------------------------------------------------
' htmSaveChangesConfirmationHandler
'
' Description:
'	Rendering function for "Save Changes Confirmation" action
'
' Parameters:
'	Identical to PrepareSaveChangesConfirmation
'
' Returns:
'	String with HTML
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmSaveChangesConfirmationHandler(ByVal sAction, ByVal sSubAction, _
			ByVal sProfileIDs, ByVal sActionPage, ByRef sPreviousAction, _
			ByRef listProfileSchema, ByRef bValidPostedValues, ByRef bDisplayPostedValues)
    Dim htm
    Dim bUpdated
    Dim iStatusID
    Dim sStatusField
    
    ' Check if the Message Box page is already displayed to user
    Select Case sSubAction
        Case MSGBOX_ACTION_YES
             If bValidPostedValues Then
                bUpdated = bSaveProfileAndRedirect(sSubAction, sProfileIDs, _
                              sStatusField, iStatusID, sActionPage)
             End If
             
             If (Not bValidPostedValues) OR (Not bUpdated) Then                
                    bDisplayPostedValues = True                    
                    sStatusField = STATUS_FIELD_VALUE_ERROR
                    iStatusID = E_USER_INPUT_ERROR                    
                    
                    htm = htmRenderEditPage(sPreviousAction, sSubAction, _
                             sProfileIDs, sStatusField, iStatusID, _
                             bValidPostedValues, bDisplayPostedValues, _
                             listProfileSchema)
             End If
             
        ' User has opted to be on the edit page     
        Case MSGBOX_ACTION_NO            
             ' Display the profile details
             htm = htmRenderEditPage(sPreviousAction, sSubAction, _
                       sProfileIDs, sStatusField, iStatusID, _
                       bValidPostedValues, bDisplayPostedValues, _
                       listProfileSchema)
        Case Else
             ' confirmation (message box) page is not yet displayed and need to be displayed
             '    for single profile editing only.
             ' Display the Message Box page for saving the changes made
             htm = htmRenderMessageBoxPage(sAction, sSubAction, sActionPage, sProfileIDs)
    End Select    

    htmSaveChangesConfirmationHandler = htm    
End Function
%>