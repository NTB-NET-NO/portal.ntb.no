<%
' =============================================================================
' check_profile.asp
' Profile Checking routines for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' bProfileHasChanged
'
' Description:
'	Function to check to see if the posted values for a profile changed from
'	what's on disk
'
' Parameters:
'	sProfileID					- ID of profile to check
'
' Returns:
'	Boolean: True (Success), False (Failure)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bProfileHasChanged(ByVal sProfileID)
    Dim bChanged, bSupported
    Dim rsProfile
    Dim oProperty
    Dim iUserClass, iPropertyValue
    Dim sProfileKey, sAccessType, sPropertyValue
    Dim frmPropertyValue
    Dim listProfileSchema
    
    bChanged = False
    iUserClass = iGetCurrentUserPropertyAccessClass()
    Set listProfileSchema = listCloneProfileSchema(sThisPage)    
    Set rsProfile = rsGetProfileDetails(sProfileID)

    ' Loop through the profile properties list and check 
    ' if posted values are equal to the existing ones.
    For Each oProperty In listProfileSchema
        If Not (oProperty.IsGroup Or (oProperty.Name = FIELD_USER_PASSWORD)) Then
            ' Check if the Property is supported by the site (based on site features)
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)

            If bSupported Then
                ' Get the current user's privileges for this property
                sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)

                ' Compare values for read-write properties only
                If StrComp(sAccessType, ACCESS_READWRITE, vbTextCompare) = 0 Then
                    Select Case oProperty.Type
                    Case DATA_TYPE_STRING
                         sPropertyValue = rsProfile(oProperty.QualifiedName).Value
                         frmPropertyValue = GetRequestString(oProperty.Name, Null)
                         If (IsNull(sPropertyValue) And (Not IsNull(frmPropertyValue))) Or _
							((Not IsNull(sPropertyValue)) And IsNull(frmPropertyValue)) Or _
							(sPropertyValue <> frmPropertyValue) Then
							 bChanged = True
                         End If

                    Case DATA_TYPE_NUMBER
                         iPropertyValue = rsProfile(oProperty.QualifiedName).Value
                         frmPropertyValue = MSCSAppFrameWork.RequestNumber(oProperty.Name, Null)
                         If iPropertyValue <> frmPropertyValue Then
                             bChanged = True
                         End If
                    End Select
                End If
            End If
        End If
    Next
    
    bProfileHasChanged = bChanged
End Function


' -----------------------------------------------------------------------------
' bIsPropertyValueAdded
'
' Description:
'	Function to determine if any properties were added
'
' Parameters:
'
' Returns:
'	Boolean: True (Success), False (Failure)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bIsPropertyValueAdded()
    Dim bAdded, bSupported
    Dim rsProfile    
    Dim oProperty
    Dim iUserClass
    Dim sProfileKey, sAccessType
    Dim frmPropertyValue    
    Dim listProfileSchema
    
    bAdded = False
    iUserClass = iGetCurrentUserPropertyAccessClass()
    Set listProfileSchema = listCloneProfileSchema(sThisPage)

    For Each oProperty In listProfileSchema
        If NOT oProperty.IsGroup Then
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)
            If bSupported Then
                sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)
                If StrComp(sAccessType, ACCESS_READWRITE,vbTextCompare) = 0 Then                
                    Select Case oProperty.Type
                        Case DATA_TYPE_STRING
                            frmPropertyValue = GetRequestString(oProperty.Name, Null)
                        Case DATA_TYPE_NUMBER
                            frmPropertyValue = MSCSAppFrameWork.RequestNumber(oProperty.Name, Null)
                    End Select
        
                    If Not IsNull(frmPropertyValue) Then
                       bAdded = True
                    End If
                End If
            End If
        End If
    Next
    
    bIsPropertyValueAdded = bAdded
End Function


' -----------------------------------------------------------------------------
' bCheckIfProfileExists
'
' Description:
'	This function ensures that a profile doesn't already exist
'
' Parameters:
'
' Returns:
'
' Notes :
'   For users, this function will return True only if the profile exists and
'   the profile being looked up is not the current user's profile
' -----------------------------------------------------------------------------
Function bCheckIfProfileExists(ByRef sStatusField, ByRef iStatusID)
    Dim id
    Dim rsProfile
    Dim po_number
    Dim logon_name        
    Dim sProfileType
    Dim bProfileExists

	' Initialize
	bProfileExists = False
	Set rsProfile = Nothing

    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    
    Select Case sProfileType
        Case PROFILE_TYPE_USER
            logon_name = GetRequestString(FIELD_USER_LOGIN_NAME, Null)
            If (logon_name <> "") Then
				If (GetRequestString(FIELD_USER_ID, Null) = m_UserID) Then
					' If a user is editing themself, get their current profile and see if they're attempting a name change
					Set rsProfile =  EnsureUserProfile()
					If rsProfile(GENERAL_INFO_GROUP)(FIELD_USER_LOGIN_NAME) = logon_name Then
						Set rsProfile = Nothing ' this signifies that there is no collision
					Else
						Set rsProfile = rsGetProfile(logon_name, PROFILE_TYPE_USER, True)
					End If
				Else
					Set rsProfile = rsGetProfile(logon_name, PROFILE_TYPE_USER, True)
				End If
            Else
				Set rsProfile = Nothing
            End If

            If rsProfile Is Nothing Then
                bProfileExists = False
            Else
				If (rsProfile(GENERAL_INFO_GROUP)(FIELD_USER_ID) = _
					GetRequestString(FIELD_USER_ID, Null)) Then
					bProfileExists = False
				Else
					bProfileExists = True
				End If
            End If

            If bProfileExists Then iStatusID = E_USER_ALREADY_EXIST

        Case PROFILE_TYPE_ORG
			bProfileExists = False

		Case Else
            Err.Raise E_NOTIMPL
    End Select

    If bProfileExists Then
        sStatusField = STATUS_FIELD_VALUE_ERROR
    Else
        sStatusField = STATUS_FIELD_VALUE_SUCCESS
    End If
    
    bCheckIfProfileExists = bProfileExists
End Function
%>