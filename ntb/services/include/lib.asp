<%
' =============================================================================
' lib.asp
' General library for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' CheckIfProfilesExist
'
' Description:
'	Sub to check if the profile IDs posted to the page are valid ones
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CheckIfProfilesExist(ByVal sSubAction, ByVal sProfileIDs)
    Dim i, iProfileCount
    Dim rsProfile    
    Dim arrProfileIDs
    
    ' Get the count of profiles
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    
    If iProfileCount = 1 Then
        ' For single profile, get the profile details
        Set rsProfile = rsGetProfileDetails(sProfileIDs)
        If rsProfile Is Nothing Then
            ' Redirect to the error page
            Response.Redirect(GenerateURL(MSCSSitePages.NoProfile, Array(), Array()))
        Else
            Set rsProfile = Nothing
        End If
        
    ElseIf (iProfileCount > 1) Then
        ' Check for multiple profiles (ex: multi-edit scenario and 
        '     NOT Select All scenario), split the string containing the IDs 
        '     into an array and check validity of each of the profiles.
        arrProfileIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)
        For i = LBound(arrProfileIDs) To UBound(arrProfileIDs)            
            Set rsProfile = rsGetProfileDetails(arrProfileIDs(i))
            If rsProfile Is Nothing Then
                ' Redirect to an error page
                Response.Redirect(GenerateURL(MSCSSitePage.NoProfile, Array(), Array()))
            End If                
        Next
        If Not (rsProfile Is Nothing) Then Set rsProfile = Nothing
    End If
End Sub


' -----------------------------------------------------------------------------
' CheckIfOrderExists
'
' Description:
'	Sub to check if an oder id (can be order_id and order_number) is a valid one.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CheckIfOrderExists(ByVal sSubAction, ByVal sProfileIDs)
    Dim rsOrder
    Dim iProfileCount
    
    ' Get order details, we don't have to worry about mutlti-edit scenario
    '     as only one order details can be viewed at one time
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)    
    If iProfileCount = 1 Then
        Set rsOrder = rsGetOrderDetails(sProfileIDs)
        If rsOrder.RecordCount = 1 Then
            Response.Redirect(GenerateURL(MSCSSitePage.NoOrder, Array(), Array()))
        End If
    End If    

    If rsOrder.RecordCount = 1 Then Set rsOrder = Nothing
End Sub


' -----------------------------------------------------------------------------
' GetSearchDictionaryForThisPage
'
' Description:
'	Function to store AND retrieve the search criteria for the current 
'   user AND for current page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetSearchDictionaryForThisPage(ByVal sAction, ByRef sStatusField, ByRef iStatusID)
    Dim org_id, user_id, page_name, filter_type
    Dim sProfileType
    Dim arrValues(1), arrColumns(1) 
    Dim bNewUserEntry
    Dim oSearchStorage, oSearchDictionary
    
    Set oSearchStorage = Server.CreateObject("Commerce.DBStorage")
    Call oSearchStorage.InitStorage( _
                                        dictConfig.s_TransactionsConnectionString, _
                                        TABLE_USER_FILTER, _
                                        TABLE_USER_FILTER_KEY1 & _
                                            " " & _
                                            TABLE_USER_FILTER_KEY2, _
                                        "Commerce.Dictionary", _
                                        "marshalled_data" _
                                     )
    
    bNewUserEntry = False
    arrColumns(0) = TABLE_USER_FILTER_KEY1
    arrColumns(1) = TABLE_USER_FILTER_KEY2
    arrValues(0) = m_UserID
    arrValues(1) = sThisPage
        
    ' DBStorage throws an error when the record is NOT found
    On Error Resume Next
		Set oSearchDictionary = oSearchStorage.LookupData _
								 ( _
									 Null, _
									 arrColumns, _
									 arrValues _
								 )    
		If Err <> 0 Or IsNull(oSearchDictionary) Then
			Err.Clear
			Set oSearchDictionary = Server.CreateObject("Commerce.Dictionary")
			bNewUserEntry = True
			oSearchDictionary.page_name = sThisPage
			oSearchDictionary.user_id = m_UserID
			oSearchDictionary.org_id = mscsUserProfile(USER_ORGID).Value
		End If
    On Error GoTo 0

    filter_type = GetRequestString(QUERY_STRING_CRITERIA, Null)
    If Not IsNull(filter_type) Then oSearchDictionary.filter_type = filter_type

    ' Update the user filter options if FILTER_ACTION button is clicked
    If StrComp(sAction, FILTER_ACTION, vbTextCompare) = 0 Then        
		' Get the profile type used in this page
		sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
        If StrComp(sProfileType, PROFILE_TYPE_ORDER, vbTextCompare) = 0 Then
			' Call function to populate order search criteria
			Call PopulateOrderSearchCriteria(oSearchDictionary)
			' Returns Err.Number <> 0 when date validation fails
		Else
			' Call function to populate profile search criteria
		    Call PopulateProfileSearchCriteria(oSearchDictionary)
			' Returns Err.Number <> 0 when date validation fails
        End If
    End If

	If (Err.Number = 0) Then
		' Input data VALIDATION might fail
		On Error Resume Next
		If bNewUserEntry Then
			oSearchDictionary.filter_type = SEARCH_CRITERIA_SIMPLE
			Call oSearchStorage.InsertData(Null, oSearchDictionary)
		Else
			Call oSearchStorage.CommitData(Null, oSearchDictionary)
		End If
	End If

	If (Err.Number <> 0) Then
		sStatusField = STATUS_FIELD_VALUE_ERROR
		iStatusID = E_USER_INPUT_ERROR
		Err.Clear
	Else
		sStatusField = STATUS_FIELD_VALUE_SUCCESS
		iStatusID = 0
	End If

    Set oSearchStorage = Nothing
    Set GetSearchDictionaryForThisPage = oSearchDictionary    
    Set oSearchDictionary = Nothing    
End Function


' -----------------------------------------------------------------------------
' PopulateOrderSearchCriteria
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PopulateOrderSearchCriteria(ByRef oSearchDictionary)
	Dim end_date, date_type, start_date
	Dim po_number, order_number, order_status_code
	Dim last_name, first_name
	Dim description
	Dim vendor_part_number
	Dim sStartDate, sEndDate

    first_name = GetRequestString(FILTER_FIELD_ORDER_USER_FIRST_NAME, Null)
    last_name = GetRequestString(FILTER_FIELD_ORDER_USER_LAST_NAME, Null)
	order_number = GetRequestString(FIELD_ORDER_NUMBER, Null)
	order_status_code = MSCSAppFrameWork.RequestNumber(FIELD_ORDER_STATUS, Null)
	po_number = GetRequestString(FILTER_FIELD_ORDER_PO_NUMBER, Null)
	vendor_part_number = GetRequestString(FILTER_FIELD_ORDER_SKU, Null)
	description = GetRequestString(FIELD_ORDER_LINEITEM_DESC, Null)
	date_type = GetRequestString(FIELD_ORDER_DATE_TYPE, Null)

	sStartDate = GetRequestString(FIELD_ORDER_START_DATE, Null)
	sEndDate = GetRequestString(FIELD_ORDER_END_DATE, Null)
	start_date = MSCSAppFrameWork.RequestDate(FIELD_ORDER_START_DATE, Null)
	end_date = MSCSAppFrameWork.RequestDate(FIELD_ORDER_END_DATE, Null)

    oSearchDictionary.first_name = first_name
    oSearchDictionary.last_name = last_name
    oSearchDictionary.order_number = order_number
    oSearchDictionary.order_status_code = order_status_code
    oSearchDictionary.po_number = po_number
    oSearchDictionary.vendor_part_number = vendor_part_number
    oSearchDictionary.description = description
    oSearchDictionary.date_type = date_type
    oSearchDictionary.start_date = start_date

	' Check if EndDate is a DATE (not a DATETIME)
	If (Hour(end_date) = 0) And _
	   (Minute(end_date) = 0) And _
	   (Second(end_date) = 0) Then
		' If TRUE then Increment EndDate w/ 1
		end_date = CStr(CDate(end_date) + 1)
	End If

	' Validate Start_Date / End_Date values
	If (bInvalidDate(sStartDate, start_date) Or bInvalidDate(sEndDate, end_date)) Then
		oSearchDictionary.start_date = sStartDate
		oSearchDictionary.end_date = sEndDate
		Err.Number = -1
	Else
		' Assign EndDate value to SearchDictionary property
		oSearchDictionary.end_date = end_date
	End If
End Sub

' -----------------------------------------------------------------------------
' bInvalidDate
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bInvalidDate(sDate, dtDate)

	If (Len(sDate) > 0) And IsNull(dtdate) Then
		bInvalidDate = True
	Else
		bInvalidDate = False
	End If
End Function

' -----------------------------------------------------------------------------
' PopulateProfileSearchCriteria
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PopulateProfileSearchCriteria(ByRef oSearchDictionary)
    Dim sName
    Dim oProperty
    Dim listProfileSchema
    Dim sStart, sEnd, sStartDate, sEndDate
    Dim dtStartDate, dtEndDate

    Set listProfileSchema = listCloneProfileSchema(sThisPage)

    For Each oProperty In listProfileSchema
        sName = oProperty.Name
        If Not oProperty.IsGroup Then
            If (StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ALL, vbTextCompare) = 0) Or _
               (StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ADVANCED, vbTextCompare) = 0) Then

                ' Check the property type
                Select Case oProperty.InputType                      
                Case TextBox
                    oSearchDictionary(sName) = GetRequestString(sName, Null)
                Case ComboBox
                    oSearchDictionary(sName) = MSCSAppFrameWork.RequestNumber(sName, Null)
                Case DateTime
                    sStart = sName & FIELD_START_DATE
					sStartDate = GetRequestString(sStart, Null)
					dtStartDate = MSCSAppFrameWork.RequestDate(sStart, Null)
                    oSearchDictionary(sStart) = dtStartDate
                    sEnd = sName & FIELD_END_DATE
					sEndDate = GetRequestString(sEnd, Null)
					dtEndDate = MSCSAppFrameWork.RequestDate(sEnd, Null)
                    oSearchDictionary(sEnd) = dtEndDate

					' Validate Start_Date / End_Date values
					If (bInvalidDate(sStartDate, dtStartDate) Or bInvalidDate(sEndDate, dtEndDate)) Then
						oSearchDictionary(sStart) = sStartDate
						oSearchDictionary(sEnd) = sEndDate
						Err.Number = -1
						Exit Sub
					End If
                End Select
            End If
        End If
    Next
    Set listProfileSchema = Nothing
End Sub


' -----------------------------------------------------------------------------
' dictGetSearchDictionaryForPage
'
' Description:
'	Function to retrieve user filter criteria for current user AND page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictGetSearchDictionaryForPage(ByVal sPageName)
    Dim arrValues(1)
    Dim arrColumns(1)
    Dim bNewUserEntry
    Dim oSearchStorage
    Dim oSearchDictionary
    
    Set oSearchStorage = Server.CreateObject("Commerce.DBStorage")
    Call oSearchStorage.InitStorage( _
										 dictConfig.s_TransactionsConnectionString, _
                                         TABLE_USER_FILTER, _
                                         TABLE_USER_FILTER_KEY1 & _
                                            " " & _
                                            TABLE_USER_FILTER_KEY2, _
                                         "Commerce.Dictionary", _
                                         "marshalled_data" _
                                     )
    
    bNewUserEntry = False
    arrColumns(0) = TABLE_USER_FILTER_KEY1
    arrColumns(1) = TABLE_USER_FILTER_KEY2
    arrValues(0) = GetAttributeValueFromProfile(mscsUserProfile, FIELD_USER_ID)
    arrValues(1) = sPageName
    
    ' DBStorage throws error when the record is NOT found
    On Error Resume Next    
		Set oSearchDictionary = oSearchStorage.LookupData(Null, arrColumns, arrValues)    
		If Err <> 0 Or _
			IsEmpty(oSearchDictionary) Or _
			IsNull(oSearchDictionary) Or _
			oSearchDictionary Is Nothing Then
			Err.Clear
			Set oSearchDictionary = Server.CreateObject("Commerce.Dictionary")
			bNewUserEntry = True
		End If
    On Error GoTo 0
    
    Set oSearchStorage = Nothing
    Set dictGetSearchDictionaryForPage = oSearchDictionary
    Set oSearchDictionary = Nothing
End Function


' -----------------------------------------------------------------------------
' bValidateLoginNameChars
'
' Description:
'    This function validates the characters in a login name.  
' Parameters:
'
' Returns:
'
' Notes :
'   Pass an unfiltered logon_name (do not filter <,>,")
'   For best results, keep this validation in sync with the BusinessDesk and login forms
' -----------------------------------------------------------------------------
Function bValidateLoginNameChars(sLogon)
	Dim logon_name, oRegEx
	Set oRegEx = New RegExp
	Const LOGONNAME_MASK = "^[^?*/""|:<>=+;,.\\\)(#[\]&^/]*"   

	bValidateLoginNameChars = bMatchesPattern(oRegEx, sLogon, LOGONNAME_MASK)
End Function

' -----------------------------------------------------------------------------
' bValidatePasswordChars
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   Pass an unfiltered password string (do not filter <,>,")
'   For best results, keep this validation in sync with the BusinessDesk and login forms
' -----------------------------------------------------------------------------
Function bValidatePasswordChars(sPassword)
	Dim logon_name, oRegEx, sMask
	Set oRegEx = New RegExp

	If Application("MSCSProfileServiceUsesActiveDirectory") Then
		sMask = "^[^?*/""|:<>=+;,.\\\)(#[\]&^/]*"
	Else
		sMask = "^[^<>""&]*"
	End if

	bValidatePasswordChars = bMatchesPattern(oRegEx, sPassword, sMask)
End Function


' -----------------------------------------------------------------------------
' sGetParentDN
'
' Description:
'	 Function to get the ParentDN related for an organization.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetParentDN()
    Dim sOrgName
    Dim sParentDN
    Dim sTempParentDN
        
    sTempParentDN = AD_CONTAINER_CS40    
    sOrgName = GetOrgAttributeFromID(FIELD_ORG_NAME, _
                                     mscsUserProfile(USER_ORGID).Value)
    sParentDN = AD_SITE_CONTAINER_PREFIX & sOrgName & "," & sTempParentDN
    
    sGetParentDN = sParentDN
End Function


' -----------------------------------------------------------------------------
' sGetUserAccessForThisProperty
'
' Description:
'	Function to return  the current user access privileges for this property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetUserAccessForThisProperty(ByVal iUserClass, ByVal oProperty)
    Dim sAccessType

    Select Case iUserClass
        Case GUEST_USER_CLASS
             sAccessType = oProperty.Attributes(CUSTOM_ATTRIBUTE_ANONYMOUS_ACCESS)
        Case REGISTERED_USER_CLASS
             sAccessType = oProperty.Attributes(CUSTOM_ATTRIBUTE_USER_ACCESS)
        Case DELEGATED_ADMIN_CLASS
             sAccessType = oProperty.Attributes(CUSTOM_ATTRIBUTE_ADMIN_ACCESS)
    End Select

    sGetUserAccessForThisProperty = sAccessType
End Function


' -----------------------------------------------------------------------------
' iGetCurrentUserPropertyAccessClass
'
' Description:
'	Function to get the current user's property access class
'
' Parameters:
'
' Returns:
'	GUEST_USER_CLASS, REGISTERED_USER_CLASS, DELEGATED_ADMIN_CLASS
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function iGetCurrentUserPropertyAccessClass()
	Dim mscsUserProfile
    Dim iUserClass
    Dim iUserRole

    Select Case m_UserAccessType
		Case GUEST_VISIT
			 iUserClass = GUEST_USER_CLASS

		Case TICKET_AUTH, IIS_AUTH
			 Set mscsUserProfile = EnsureUserProfile()
			 iUserRole = mscsUserProfile(USER_PARTNERDESK_ROLE).Value
			 If iUserRole = ROLE_ADMIN Then
			    iUserClass = DELEGATED_ADMIN_CLASS
			 Else
			    iUserClass = REGISTERED_USER_CLASS
			 End If
	End Select

    iGetCurrentUserPropertyAccessClass = iUserClass
End Function


' -----------------------------------------------------------------------------
' bCheckIfThePropertyFeatureIsSupported
'
' Description:
'	Function to check If this property's feature is enabled in the site
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bCheckIfThePropertyFeatureIsSupported(ByVal oProperty)
    Dim bEnabled
    Dim sFeatureID
    
    sFeatureID = oProperty.Attributes(CUSTOM_ATTRIBUTE_FeatureID)
    bEnabled = False
        
    Select Case sFeatureID
        Case ALL_FEATURES
             bEnabled = True
        Case DELEGATED_ADMIN_SUPPORT
             If dictConfig.i_DelegatedAdminOptions = DELEGATED_ADMIN_SUPPORTED Then
                bEnabled = True
             End If
        Case AD_SUPPORT
             If MSCSProfileServiceUsesActiveDirectory Then
                bEnabled = True
             End If         
    End Select
    
    bCheckIfThePropertyFeatureIsSupported = bEnabled
End Function


' -----------------------------------------------------------------------------
' sGetDisplayValueForProperty
'
' Description:
'	Function to get display text for properties of type NUMBER
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetDisplayValueForProperty(ByVal sPropertyName, ByVal sPropertyValue)
	Dim sDisplayText

	Select Case sPropertyName
		Case FIELD_USER_STATUS
			 sDisplayText = sGetUserStatus(sPropertyValue)
		
		Case FIELD_USER_PARTNERDESK_ROLE
			 sDisplayText = sGetUserPartnerDeskRole(sPropertyValue)
		
		Case FIELD_ORDER_STATUS_CODE
			 sDisplayText = sGetOrderStatusDetails(sPropertyValue)			
		
		Case Else
			sDisplayText = sPropertyValue
	End Select

	sGetDisplayValueForProperty = sDisplayText
End Function


' -----------------------------------------------------------------------------
' sGetUserStatus
'
' Description:
'	Function to get the user status code's description
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetUserStatus(ByVal iUserStatus)
    Dim str

    Select Case iUserStatus
        Case ACCOUNT_ACTIVE
            str = mscsMessageManager.GetMessage("L_USER_STATUS_ACTIVE_Text", sLanguage)
        Case ACCOUNT_INACTIVE
            str = mscsMessageManager.GetMessage("L_USER_STATUS_INACTIVE_Text", sLanguage)
        Case Else
            str = iUserStatus
    End Select

    sGetUserStatus = str
End Function


' -----------------------------------------------------------------------------
' sGetUserPartnerDeskRole
'
' Description:
'	Function to get the user's i_partnerdeskroleflags description
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetUserPartnerDeskRole(ByVal iPartnerDeskRole)
    Dim str

    Select Case iPartnerDeskRole
        Case ROLE_USER
            str = mscsMessageManager.GetMessage("L_USER_PDROLE_USER_Text", sLanguage)
        Case ROLE_ADMIN
            str = mscsMessageManager.GetMessage("L_USER_PDROLE_ADMIN_Text", sLanguage)
        Case Else
            str = iPartnerDeskRole
    End Select

    sGetUserPartnerDeskRole = str
End Function


' -----------------------------------------------------------------------------
' sGetOrderStatusDetails
'
' Description:
'	Function to get the order status code's description
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetOrderStatusDetails(ByVal iOrderStatus)
    Dim str

    If IsObject(iOrderStatus) Then
        str = MSCSAppConfig.DecodeStatusCode(iOrderStatus.Value)
    Else
        str = MSCSAppConfig.DecodeStatusCode(iOrderStatus)
    End If

    sGetOrderStatusDetails = str
End Function


' -----------------------------------------------------------------------------
' ReDirectToPage
'
' Description:
'	Sub to redirect to any page with URL arguments. This function will
'   also replace the values of keys passed in arrKeys with new values 
'   from arrValues in the newly constructed URL
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub ReDirectToPage(ByVal sPageName, ByVal arrKeys, ByVal arrValues)
    Dim sActionPage

    If (IsNull(arrKeys) Or IsNull(arrValues)) Then
		sActionPage = GenerateURL(sPageName, Array(), Array())
	Else
        ' Construct the URL based on current parameters
        sActionPage = GenerateURL(sPageName, arrKeys, arrValues)
    End If

    ' ReDirect to the above URL
    Response.Redirect sActionPage
End Sub


' -----------------------------------------------------------------------------
' sGetSelectActionPropertyName
'
' Description:
'	Get the property name for which Select Action button (...) is clicked
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetSelectActionPropertyName(ByVal sAction)
    Dim frmElement
    Dim sPropertyName

    sPropertyName = Null
    
    ' Check id select action button (...) is clicked
    If StrComp(sAction, SELECT_ACTION, vbTextCompare) = 0 Then
        ' Determine for which property the select action button (...) was clicked
        frmElement = BUTTON_SELECT & FIELD_ORG_CONTACT
        If Not IsNull(GetRequestString(frmElement, Null)) Then
            sPropertyName = FIELD_ORG_CONTACT
        End If
        
        frmElement = BUTTON_SELECT & FIELD_ORG_RECEIVING_MANAGER
        If Not IsNull(GetRequestString(frmElement, Null)) Then            
            sPropertyName = FIELD_ORG_RECEIVING_MANAGER
        End If
        
        frmElement = BUTTON_SELECT & FIELD_ORG_PURCHASING_MANAGER            
        If Not IsNull(GetRequestString(frmElement, Null)) Then            
            sPropertyName = FIELD_ORG_PURCHASING_MANAGER
        End If
                
        If IsNull(sPropertyName) Then
            sPropertyName = GetRequestString(SELECT_FIELD_NAME, Null)                
        End If        
    Else
        ' Check the URL for the select action property name
        sPropertyName = GetRequestString(SELECT_FIELD_NAME, Null)
    End If
    
    sGetSelectActionPropertyName = sPropertyName
End Function


' -----------------------------------------------------------------------------
' listCloneProfileSchema
'
' Description:
'	Function to get the current profile properties
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function listCloneProfileSchema(ByVal sPageName)
    Dim sProfileType
    Dim dictProfiles
    Dim listProfileSchema
    
    sProfileType = sGetThisPageProperty(sPageName, PROPERTY_PAGE_PROFILE)
    Set dictProfiles = Application("MSCSProfileProperties")
    Set listProfileSchema =  MSCSDataFunctions.CloneObject(dictProfiles(sProfileType))
    Set dictProfiles = Nothing
    Set listCloneProfileSchema = listProfileSchema
End Function


' -----------------------------------------------------------------------------
' iGetCountOfProfiles
'
' Description:
'	Function to get the count of profile id's
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function iGetCountOfProfiles(ByVal sSubAction, ByVal sProfileIDs)
    Dim iProfileIDs
    Dim arrProfileIDs
    
    ' Initialize the iProfileCount as 0
    iProfileIDs = 0
    
    ' Check if all the profiles for a particular search criteria are selected    
    If StrComp(sSubAction, SELECTALL_ACTION, vbTextCompare) = 0 Then
        ' For SelectAll action the profile count is set to -1
        iProfileIDs = -1            
    
    ' Split the sProfileIDs and get the count of the profiles selected
    ElseIf Not IsNull(sProfileIDs) Then
        arrProfileIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)
        iProfileIDs = UBound(arrProfileIDs) + 1        
    End If
    
    iGetCountOfProfiles = iProfileIDs
End Function


' -----------------------------------------------------------------------------
' sGetThisPageProperty
'
' Description:
'	Function to get the page properties
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetThisPageProperty(ByVal sPageName, ByVal sPropertyName)
    Dim dictPages, dictPageProperties
    Dim sPropertyValue

    Set dictPages = Application("MSCSPageProperties")
    Set dictPageProperties = dictPages(sPageName)
    sPropertyValue = dictPageProperties(sPropertyName)
    
    Set dictPages = Nothing
    Set dictPageProperties = Nothing
    
    sGetThisPageProperty = sPropertyValue
End Function


' -----------------------------------------------------------------------------
' InitializePagingVariables
'
' Description:
'	Function to initialize paging variables AND return the current page number
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitializePagingVariables(ByRef sAction, _
                              ByRef iPageNumber, _
                              ByRef iTotalPages, _
                              ByVal bIsProfilePage)
    iPageNumber = MSCSAppFrameWork.RequestNumber(PAGE_NUM_FIELD, Null)
    iTotalPages = MSCSAppFrameWork.RequestNumber(TOTAL_PAGES, Null)
    
    Select Case sAction
        Case FIRSTPAGE_ACTION
            iPageNumber = 1
        Case PREVIOUSPAGE_ACTION
			If iPageNumber <= 1 Then
				iPageNumber = 1
			ElseIf iPageNumber > iTotalPages + 1 Then
				iPageNumber = iTotalPages
			Else
				iPageNumber = iPageNumber - 1
            End If
        Case NEXTPAGE_ACTION
            If (iPageNumber >= iTotalPages) And (Not bIsProfilePage) Then
				iPageNumber = iTotalPages
			ElseIf iPageNumber <= 0 Then
				iPageNumber = 1
			Else
				iPageNumber = iPageNumber + 1
			End If
        Case LASTPAGE_ACTION
            If Not IsNull(iTotalPages) Then
               iPageNumber = iTotalPages
            Else
               iPageNumber = -1
            End If
        Case PAGING_ACTION
            If (iPageNumber > iTotalPages) And (Not bIsProfilePage) Then
				iPageNumber = iTotalPages
            ElseIf iPageNumber < 1 Then
				iPageNumber = 1
            End If
    End Select

    If IsNull(iPageNumber) Then iPageNumber = 1
    If IsNull(iTotalPages) Then iTotalPages = 0
End Sub


' -----------------------------------------------------------------------------
' RelocateRecordPointer
'
' Description:
'	Sub to initialize the local variables passed, which will be used in LIST
'	type page and specifically in the paging mechanism.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RelocateRecordPointer(ByRef Count, _
                          ByRef nPage, _
                          ByRef nCountTemp, _
                          ByRef bRecordCountPresent, _
                          ByVal rs, _
                          ByRef iPageNumber, _
                          ByRef iTotalRecords)
    Count = 0
    nPage = 1
    nCountTemp = 0
   
    If iTotalRecords = 0 Then
        bRecordCountPresent = False
        If iPageNumber > 1 Then
            While nPage < iPageNumber
                nCountTemp = 0
                While nCountTemp < RECORDS_PER_PAGE
                    nCountTemp = nCountTemp + 1
                    Count = Count + 1
                    rs.MoveNext
                Wend
                nPage = nPage + 1
            Wend
        End If
        iTotalRecords = Count
        nCountTemp = 0
    End If
End Sub


' -----------------------------------------------------------------------------
' SetGlobalPagingVariables
'
' Description:
'	Sub to Set the global paging variables depending upon the local variables
'   passed to the function
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SetGlobalPagingVariables(ByVal bRecordCountPresent, _
                             ByVal nCountTemp, _
                             ByRef iTotalRecords, _  
                             ByRef iPageNumber, _
                             ByRef iTotalPages, _
                             ByVal bIsProfilePage)
    ' Paging buttons handler
    If Not bRecordCountPresent Then
       iTotalRecords = iTotalRecords + nCountTemp
    End If   

    iTotalPages = iTotalRecords \ RECORDS_PER_PAGE
    If iTotalRecords > (iTotalPages * RECORDS_PER_PAGE) Then
        iTotalPages = iTotalPages + 1
    End If

    If (iPageNumber = -1) Then
		iPageNumber = iTotalPages
    End If

    If (iPageNumber > iTotalPages) Then
		If bIsProfilePage Then
			iTotalPages = iPageNumber ' = iTotalPages + 1
		Else
			iPageNumber = iTotalPages
		End If
    End If    
End Sub


' -----------------------------------------------------------------------------
' listGetValidPropertiesForMultiEdit
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function listGetValidPropertiesForMultiEdit(ByVal listProfileSchema)
    Dim sChecked
    Dim oProperty
    Dim iUserClass
    Dim bSupported
    Dim sAccessType
    Dim listValidProperties
    
    Set listValidProperties = Server.CreateObject("Commerce.SimpleList")
    iUserClass = iGetCurrentUserPropertyAccessClass()
        
    For Each oProperty In listProfileSchema
        If Not oProperty.IsGroup Then            
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)        
            If (bSupported) Then
                sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)
                
                If (StrComp(sAccessType, ACCESS_READWRITE, vbTextCompare) = 0) AND _
                   (oProperty.Attributes.IsPrimaryKey = False) AND _
                   (oProperty.Attributes.IsUniqueKey = False) AND _
                   (oProperty.Attributes.IsJoinKey = False) Then

                   sChecked = GetRequestString(oProperty.Name & FIELD_UPDATED, "")
                   If StrComp(sChecked, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
                        Call listValidProperties.Add(oProperty)
                   End If
            
                End If
            End If
        End If            
    Next    
    Set listGetValidPropertiesForMultiEdit = listValidProperties
End Function


' -----------------------------------------------------------------------------
' listReConstructPropertiesSchema
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function listReConstructPropertiesSchema(ByVal listValidProperties)
    Dim sChecked
    Dim oProperty
    Dim iUserClass
    Dim bSupported
    Dim sAccessType
    Dim listProfileSchema
    Dim listCombinedProperties

    iUserClass = iGetCurrentUserPropertyAccessClass()
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
    Set listCombinedProperties = Server.CreateObject("Commerce.SimpleList")
        
    For Each oProperty In listProfileSchema
        If oProperty.IsGroup Then
            Call listCombinedProperties.Add(oProperty)
        Else    
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)
            sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)
            sChecked = GetRequestString(oProperty.Name & FIELD_UPDATED, "")
            If (bSupported) AND _                
               (StrComp(sAccessType, ACCESS_READWRITE, vbTextCompare) = 0) AND _
               (oProperty.Attributes.IsPrimaryKey = False) AND _
               (oProperty.Attributes.IsUniqueKey = False) AND _
               (oProperty.Attributes.IsJoinKey = False) AND _
               (StrComp(sChecked, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) Then
               Call listCombinedProperties.Add(dictGetpropertyFromList(oProperty.Name, listValidProperties))
            Else
               Call listCombinedProperties.Add(oProperty)
            End If
        End If                                    
    Next
    
    Set listReConstructPropertiesSchema = listCombinedProperties
End Function


' -----------------------------------------------------------------------------
' dictGetpropertyFromList
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function dictGetpropertyFromList(ByVal sPropertyName, ByVal listValidProperties)
    Dim oProperty
    
    For Each oProperty In listValidProperties
        If StrComp(sPropertyName, oProperty.Name, vbTextCompare) = 0 Then 
			' $$ Must add stuff here               
            Exit For
        End If
    Next
    
    Set dictGetpropertyFromList = oProperty
End Function


' -----------------------------------------------------------------------------
' ValidateFields
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function ValidateFields(ByVal listCtrls)
	' Password field size constraint
	Const L_PasswordMaxLength_Number = 14
	Const L_PasswordMinLength_Number = 7

    Dim dictCtrl, oRegEx
    Dim i, iCtrlCount, iMax, iMin, iUserClass
    Dim sTmpValue, sAccessType, sAction, sNew, sCopy
    Dim bPassedValidation, bSupported

    iCtrlCount = listCtrls.Count
    bPassedValidation = True
    Set oRegEx = Server.CreateObject("VBScript.RegExp")
    iUserClass = iGetCurrentUserPropertyAccessClass()

	sNew = mscsMessageManager.GetMessage("L_ADD_ACTION_BUTTON_Text", sLanguage)
	sCopy = mscsMessageManager.GetMessage("L_COPY_ACTION_BUTTON_Text", sLanguage)
	sAction = GetRequestString(ACTION_TYPE, "")

    For i = 0 To iCtrlCount - 1
        Set dictCtrl = listCtrls(i)
        dictCtrl.fldValue = GetRequestString(dictCtrl.Name, Null)

        ' If user did not enter value in the field
        If IsNull(dictCtrl.fldValue) And (dictCtrl.IsGroup = False) Then
	        ' But he was supposed to
			If dictCtrl.IsRequired And _
			   (dictCtrl.Name <> FIELD_USER_sAMAccountName) And _
			   ((dictCtrl.Name <> FIELD_USER_PASSWORD) Or (sAction = sNew) Or (sAction = sCopy)) Then

				' Check if this property is supported by the site feature
				bSupported = bCheckIfThePropertyFeatureIsSupported(dictCtrl)
				If bSupported Then
					sAccessType = sGetUserAccessForThisProperty(iUserClass, dictCtrl)
					If (sAccessType = ACCESS_READWRITE) Then
						dictCtrl.Err = True
						bPassedValidation = False
					End If
				End If
            End If
        Else
            Select Case dictCtrl.InputType
                Case TEXTBOX, PASSWORD
					If (dictCtrl.Name = FIELD_USER_PASSWORD) Then
						iMax = L_PasswordMaxLength_Number
		                iMin = L_PasswordMinLength_Number
					Else
						iMax = CInt(dictCtrl.MaxLength)
						iMin = CInt(dictCtrl.MinLength)
					End If

                    ' If field supports entering double byte chars
                    ' If dictCtrl.DoubleByte Then
                    '    iMax = iMax * 2
                    ' End If 

                    If Len(Trim(dictCtrl.fldValue)) > iMax Then
                        dictCtrl.fldValue = Left(dictCtrl.fldValue, iMax)        
                        dictCtrl.Err = True
                        bPassedValidation = False            
                    ElseIf Len(Trim(dictCtrl.fldValue)) < iMin Then
                            dictCtrl.Err = True
                            bPassedValidation = False                        
                    ElseIf IsNull(dictCtrl.Pattern) = False Then
                        If oRegEx.Test(Trim(dictCtrl.fldValue)) = False Then 
                            dictCtrl.Err = True
                            bPassedValidation = False
                        End If
                    End If 
                Case LISTBOX
                    If dictCtrl.fldValue = mscsMessageManager.GetMessage("L_SelectOne_Text", sLanguage) Then
                        dictCtrl.Err = True
                        bPassedValidation = False                            
                    End If
                Case Else
            End Select
        End If
    Next
    ValidateFields = bPassedValidation
End Function

' -----------------------------------------------------------------------------
' TDTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function TDTag(ByVal sDataCell, ByVal sAlignment, ByVal sAttrib)
    TDTag = "<TD " & sAttrib & " ALIGN=" & _
		    DoubleQuote(sAlignment) & ">" & sDataCell & "</TD>"    
End Function


' -----------------------------------------------------------------------------
' THTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function THTag(ByVal sHeaderCell, ByVal sAlignment, ByVal sAttrib)
    THTag = "<TH " & sAttrib & " ALIGN=" & _
		    DoubleQuote(sAlignment) & ">" & sHeaderCell & "</TH>"    
End Function


' -----------------------------------------------------------------------------
' TRTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function TRTag(ByVal arrCell, ByVal arrCellAlignment, ByVal sAttList, ByVal bHeaderRow, ByVal sAttrib)
	Dim htmRow
	Dim iCount, i

	TRTag = "<TR" & sAttList & ">"
	
	For i = 0 To UBound(arrCell)
		If bHeaderRow = True Then
			TRTag = TRTag & THTag(arrCell(i), arrCellAlignment(i), sAttrib)
		Else
            Rem Investigae why Null is not passed down to TDTag
		    If IsNull(arrCell(i)) Then
		        arrCell(i) = NBSP
		    End If
			TRTag = TRTag & TDTag(arrCell(i), arrCellAlignment(i), sAttrib)
        End If
	Next
	
	TRTag = TRTag & "</TR>"
End Function
%>