<%
' =============================================================================
' save_one_profile.asp
'
' Common routines for creating and saving single profiles at a time
' (note: many of these routines are used in multi-select cases..
'  where each profile is saved one-by-one)
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================


' -----------------------------------------------------------------------------
' bInsertProfile
'
' Description:
'    Creates a profile, then calls bInsertProfileDetails to set individual properties
' Parameters:
'
' Returns:
'
' Notes :
'    Special logonname and password validation is done here before the profile is created
' -----------------------------------------------------------------------------
Function bInsertProfile(ByVal sSubAction, ByRef sStatusField, _
                        ByRef sStatusID, ByRef sProfileIDs)
    Dim oGenID
    Dim sNewGUID
    Dim bInserted
    Dim po_number
    Dim rsProfile
    Dim logon_name
    Dim sProfileType    
    
    bInserted = True
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)

    Select Case sProfileType            
        Case PROFILE_TYPE_USER
			logon_name = GetRequestString(FIELD_USER_LOGIN_NAME, Null)

            ' Add special handler for checking the logon_name size(20 chars)
            '     in Active Directory + SQL scenarion only.  We need to strip out
            '     the domain from the logon name and then
            '     check if the length is more that 20 characters. This is because 
            '     we are storing the logon_name in sam_account_name property which 
            '     accepts a sting of 20 characters max. 
            If MSCSProfileServiceUsesActiveDirectory Then
               ' Strip the domain name and company name from the logon name
               If InStr(1, logon_name, "\") Then
                    logon_name = Mid(logon_name, InStr(1, logon_name, "\") + 1)
               ElseIf InStr(1, logon_name, "@") Then
                    logon_name = Mid(logon_name, 1, InStr(1, logon_name, "@"))                    
               End If
                       
               ' Check if the logon name is more than 20 characters long
               If Len(logon_name) > 20 Then                   
                   sStatusField = STATUS_FIELD_VALUE_ERROR
                   sStatusID = E_LOGONNAME_LENGTH
                   bInsertProfile = False
                   Exit Function
               End If
            End If    

            ' Ensure that the logon_name is valid before we create the profile
            If Not bValidateLoginNameChars(Request(FIELD_USER_LOGIN_NAME)) Then
               sStatusField = STATUS_FIELD_VALUE_ERROR
               sStatusID = E_LOGONNAME_BADCHARS
               bInsertProfile = False
               Exit Function
            End If

            ' Ensure that the password is valid before we create the profile
            If Not bValidatePasswordChars(Request(FIELD_USER_PASSWORD)) Then
				sStatusField = STATUS_FIELD_VALUE_ERROR
				If MSCSProfileServiceUsesActiveDirectory Then
					sStatusID = E_ADPASSWORD_BADCHARS
				Else
					sStatusID = E_PASSWORD_BADCHARS
				End If
				bInsertProfile = False
				Exit Function
            End If

            ' Valid logon_name, create user profile            
            Set rsProfile = MSCSProfileService.CreateProfile(logon_name, PROFILE_TYPE_USER)                                                            
            rsProfile(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value = MSCSGenID.GenGUIDString
			Dim oUserProfile, sUserName
			Set oUserProfile = EnsureUserProfile()
			sUserName = oUserProfile.Fields(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_USER_LOGIN_NAME)).Value
			rsProfile(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_USER_CHANGED_BY)).Value = sUserName
			rsProfile(GetQualifiedName(GENERAL_INFO_GROUP, PROFILE_TYPE)).Value = REGISTERED_PROFILE
			rsProfile(GetQualifiedName(ACCOUNT_INFO_GROUP, ACCOUNT_STATUS)).Value = ACCOUNT_ACTIVE
			rsProfile(GetQualifiedName(ACCOUNT_INFO_GROUP, DATE_REGISTERED)).Value = Now()
			rsProfile(GetQualifiedName(PROFILE_SYSTEM_GROUP, DATE_CREATED)).Value = Now()
			rsProfile(GetQualifiedName(PROFILE_SYSTEM_GROUP, FIELD_USER_DATE_UPDATED)).Value = Now()
            sProfileIDs = rsProfile(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value            
                 
        Case Else
			Err.Raise E_NOTIMPL

    End Select

    bInsertProfile = bInsertProfileDetails(sSubAction, sStatusField, sStatusID, _
                                      rsProfile, sProfileIDs)
End Function    


' -----------------------------------------------------------------------------
' bInsertProfileDetails
'    
' Description:
'    Loops thru each property in the schema, and dispatches either bInsertProperty or
'    a custom Property save handler.  Also updates the user ACLs in AD.
'    (sibling function => bSaveProfileDetails, for updating pre-existing profiles)
' Parameters:
'
' Returns:
'
' Notes :
'    Custom property handlers also exist in functions that bInsertProperty calls
'    (i.e. SaveReadWriteProperty)... those property handlers are shared with 
'    bSaveProfileDetails, since bSaveProfileDetails also calls SaveReadWriteProperty etc
' -----------------------------------------------------------------------------
Function bInsertProfileDetails(ByVal sSubAction, Byref sStatusField, Byref sStatusID, _
                               ByVal rsProfiles, ByVal sProfileIDs)
    Dim bPaintACL
    Dim bInserted
    Dim oProperty    
    Dim listProfileSchema    
    Dim sProfileType
    Dim arrPrevRole()
    Dim arrCurrentRole()
                                                                            
	bInserted = True    
    Set listProfileSchema = listCloneProfileSchema(sThisPage)

	' Loop through list of property dictionaries for a particular profile and
	'     save the property value.
    For Each oProperty In listProfileSchema        
        ' Check if the property is not a group property
        If NOT oProperty.IsGroup Then

			' Special handler for specific properties
			Select Case oProperty.Name
			    Case FIELD_USER_PARENT_DN
			         Call SaveUserParentDNProperty( _
			                                          sSubAction, _
			                                          oProperty, _
			                                          rsProfiles _
			                                      )
                Case FIELD_USER_sAMAccountName
			        Call SaveSAMAccountNameProperty( _
			                                           sSubAction, _
			                                           oProperty, _
			                                           rsProfiles _
			                                       )
			        
			    Case FIELD_USER_UserAccountControl			
			         Call SaveUserAccountControlProperty( _
			                                                sSubAction, _
			                                                oProperty, _
			                                                sProfileIDs, _
			                                                rsProfiles _
			                                            )
	
	            Case FIELD_USER_PARTNERDESK_ROLE
	                 Call SaveUserPartnerDeskRoleProperty( _
                                                             sSubAction, _
                                                             oProperty, _
                                                             rsProfiles, _
                                                             sProfileIDs, _
                                                             arrPrevRole, _
                                                             arrCurrentRole _
                                                         ) 
			    Case FIELD_USER_DATE_REGISTERED 
			        rsProfiles(oProperty.QualifiedName).Value = Now()
			    
			    Case PROFILE_TYPE
			        rsProfiles(oProperty.QualifiedName).Value = REGISTERED_PROFILE
			    
			    ' Handler for all other properties				
			    Case Else	
			        bInserted = bInsertProperty( _
                                                   sSubAction, _
                                                   oProperty, _
                                                   rsProfiles, _
                                                   sProfileIDs _
                                               )
                    If Not bInserted Then
                        Exit For  
                    End If                                                                     
			End Select            
        End If
    Next
    
    Set oProperty = Nothing
    Set listProfileSchema = Nothing

    ' Check if all the property values were saved
    If bInserted Then
        ' Update the property values in the undelying data store
        On Error Resume Next
            Call rsProfiles.Update()

            ' Some error occurred while assigning property values
            If Err.number Then                
                Err.Clear
                On Error GoTo 0                
                sStatusField = STATUS_FIELD_VALUE_ERROR
                sStatusID = E_SAVING_PROFILE_PROPERTIES
                bInsertProfileDetails = False
                Exit Function
            End If
        On Error GoTo 0
    Else
        
        ' Some error occurred while assigning property values        
        sStatusField = STATUS_FIELD_VALUE_ERROR
        sStatusID = E_SAVING_PROFILE_PROPERTIES
        bInsertProfileDetails = False
        Exit Function
    End If

    ' For USER profiles, Update the user ACLs in AD if     
    '     1.)the update was sucessfull
    '     2.)site supports profiles in AD
    '     3.)the current profile type is PROFILE_TYPE_USER    
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    If  (bInserted) AND _
        (MSCSProfileServiceUsesActiveDirectory) AND _
        (StrComp(sProfileType, PROFILE_TYPE_USER, vbTextCompare) = 0) Then
        bPaintACL = bHandlePaintingACLsForUsersInAD( _
                                                       sSubAction, _
                                                       sProfileIDs, _
                                                       arrPrevRole, _
                                                       arrCurrentRole _
                                                   )
        If Not bPaintACL Then
            bInserted = False
            Call DeleteSingleProfile(sProfileIDs)
            sStatusField = STATUS_FIELD_VALUE_ERROR
            sStatusID = E_ADDING_USER_TO_GROUP_IN_AD
        End If            
    End If
    
    bInsertProfileDetails = bInserted
End Function


' -----------------------------------------------------------------------------
' SaveUserParentDNProperty
'
' Description:
'	Sub to save the User ParentDN property (AD specific)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveUserParentDNProperty(ByVal sSubAction, ByVal oProperty, ByVal rsProfiles)
    Dim org_name
        			     			     
	If (StrComp(sSubAction, ADD_ACTION, vbTextCompare) = 0) OR _
	   (StrComp(sSubAction, COPY_ACTION, vbTextCompare) = 0) Then
	   org_name = GetOrgAttributeFromID(FIELD_ORG_NAME, mscsUserProfile(USER_ORGID).Value)
	   rsProfiles(oProperty.QualifiedName).Value =  AD_SITE_CONTAINER_PREFIX & org_name & _
	               "," & _
	               AD_SITE_CONTAINER_PREFIX & MSCSCommerceSiteName & _
	   			   "," & AD_CONTAINER_CS40
	End If											 
End Sub


' -----------------------------------------------------------------------------
' SaveSAMAccountNameProperty
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveSAMAccountNameProperty(ByVal sSubAction, ByRef oProperty, ByRef rsProfiles)
    Dim sUserLogonName
    Dim sSAMAccountName
    Dim sChar
    Dim iLocation
            		     
    If (StrComp(sSubAction, ADD_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sSubAction, COPY_ACTION, vbTextCompare) = 0) Then
    	sUserLogonName = GetRequestString(FIELD_USER_LOGIN_NAME, Null)    	
    	sChar = "\"
    	iLocation = InStr(1, sUserLogonName, sChar)
    	If iLocation > 0 Then
    	    sSAMAccountName = Mid(sUserLogonName, iLocation + 1)
        Else    	    
            sChar = "@"
    	    iLocation = InStr(1, sUserLogonName, sChar)
            If iLocation > 0 Then
                sSAMAccountName = Mid(sUserLogonName, 1, iLocation - 1)
            Else
                sSAMAccountName = sUserLogonName
            End If                
        End If
                	    
    	rsProfiles(oProperty.QualifiedName).Value = sSAMAccountName    	
    End If
End Sub


' -----------------------------------------------------------------------------
' bInsertProperty
'
' Description:
'    Generic property insertion routine.  Checks if save of this property is
'    supported for user, and if so dispatches to Hidden/ReadOnly/ReadWrite handlers
' Parameters:
'
' Returns:
'
' Notes :
'   Sibling function to bSaveProperty
' -----------------------------------------------------------------------------
Function bInsertProperty(ByVal sSubAction, ByVal oProperty, ByVal rsProfiles, _
                         ByVal sProfileIDs)
    Dim bInserted
    Dim iUserClass
    Dim bSupported
    Dim sAccessType
    
    bInserted = True	
    iUserClass = iGetCurrentUserPropertyAccessClass()
	sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)
	bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)
	
	If bSupported Then
	    ' Property is currently supported by the site (i.e. the 
	    '     site feature to which the property belongs is currently 
	    '     supported)
	    Select Case sAccessType				    
	        ' Handle hidden properties
	        Case ACCESS_HIDDEN
	            Call InsertHiddenProperty( _ 
                                             oProperty, _
                                             rsProfiles _
                                         )
			    	            
	        ' Handle read-only properties
	        Case ACCESS_READONLY
	            Call SaveReadOnlyProperty( _
                                             sSubAction, _
                                             oProperty, _
                                             rsProfiles, _
                                             sProfileIDs _
                                         )
			    	             					    
	        ' Handle read-write properties
	        Case ACCESS_READWRITE
	            ' don't save multivalued properties
                If (oProperty.Attributes.IsMultivalued = False) Or _
					(oProperty.Name = FIELD_USER_PASSWORD) Then                
                    bInserted = bSaveReadWriteProperty( _
                                                          sSubAction, _
                                                          oProperty, _
                                                          rsProfiles, _
                                                          sProfileIDs _
                                                      )
                End If
	    End Select	
	End If
	
	bInsertProperty = bInserted
End Function


' -----------------------------------------------------------------------------
' bSaveProfileDetails
'
' Description:
'    Loops thru each property in the schema, and dispatches either bSaveProperty or
'    a custom Property save handler, as long as sAccessType<>ACCESS_HIDDEN. 
'    Also update the user ACLs in AD.
'    (sibling function => bInsertProfileDetails)
'
' Parameters:
'
' Returns:
'
' Notes :
'    Note: this function can be called with multiple profileid's speicified
'
'    Custom property handlers also exist in functions that bSaveProperty calls
'    (i.e. SaveReadWriteProperty)... those property handlers are shared with 
'    bInsertProfileDetails, since bInsertProfileDetails also calls SaveReadWriteProperty etc

' -----------------------------------------------------------------------------
Function bSaveProfileDetails(ByVal sSubAction, ByRef sProfileIDs, ByRef sStatusField, _
                             ByRef sStatusID, ByVal rsProfiles)
    Dim sChecked
    Dim bUpdated, bPaintACL, bSupported
    Dim oProperty
    Dim sAccessType, sProfileType
    Dim listProfileSchema
    Dim iUserClass, iProfileCount
    Dim arrPrevRole(), arrCurrentRole()

	bUpdated = True
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
    iUserClass = iGetCurrentUserPropertyAccessClass()
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)

	' Loop through list of property dictionaries for a particular profile and
	' save the property value.
    For Each oProperty In listProfileSchema
        ' Check if the property is not a group property
        If Not oProperty.IsGroup Then
            ' Check if this property is supported by the site feature
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)
			If bSupported Then
				' Check the user access for this property
		        sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)

                If (sAccessType <> ACCESS_HIDDEN) Then
					' Special handler for specific properties
					Select Case oProperty.Name			    
					Case FIELD_USER_UserAccountControl			
						Call SaveUserAccountControlProperty( _
			                                                sSubAction, _
			                                                oProperty, _
			                                                sProfileIDs, _
			                                                rsProfiles _
							                                )
	
					Case FIELD_USER_PARTNERDESK_ROLE
						Call SaveUserPartnerDeskRoleProperty( _
                                                             sSubAction, _
                                                             oProperty, _
                                                             rsProfiles, _
                                                             sProfileIDs, _
                                                             arrPrevRole, _
                                                             arrCurrentRole _
							                                 )
					Case FIELD_USER_LOGIN_NAME
						If Not bValidateLoginNameChars(Request(FIELD_USER_LOGIN_NAME)) Then
							sStatusField = STATUS_FIELD_VALUE_ERROR
							sStatusID = E_LOGONNAME_BADCHARS
							bSaveProfileDetails = False
							Exit Function
						Else
							If (oProperty.Attributes.IsMultivalued = False) Then
								bUpdated = bSaveProperty( _
														sSubAction, _
														oProperty, _
														rsProfiles, _
														sProfileIDs, _
														sAccessType _
														)
							End If

							If Not bUpdated Then Exit For
						End If
					' Handler for all other properties
					Case Else
						If (oProperty.Attributes.IsMultivalued = False) Then
							bUpdated = bSaveProperty( _
								                    sSubAction, _
									                oProperty, _
										            rsProfiles, _
											        sProfileIDs, _
												    sAccessType _
													)
						End If

						If Not bUpdated Then Exit For
					End Select
				End If
			End If
        End If
    Next        

    ' Check if all the property values were saved
    If bUpdated Then
        If iProfileCount = 1 Then
            ' Update the property values in the undelying data store
            On Error Resume Next
                If StrComp(sProfileType, PROFILE_TYPE_USER, vbTextCompare) = 0 And m_UserID=sProfileIDs Then
                    Call UpdateUserProfile(rsProfiles) 'Editing self, so update last-updated timestamp cookie
                Else
                    Call rsProfiles.Update()
                End If
                ' Some error occurred while assigning property values
                If Err.Number Then                
                    Err.Clear
                    On Error GoTo 0                
                    sStatusField = STATUS_FIELD_VALUE_ERROR
                    sStatusID = E_SAVING_PROFILE_PROPERTIES
                    bSaveProfileDetails = False
                    Exit Function
                End If
            On Error GoTo 0
        End If            
    Else
        
        ' Some error occurred while assigning property values        
        sStatusField = STATUS_FIELD_VALUE_ERROR
        sStatusID = E_SAVING_PROFILE_PROPERTIES
        bSaveProfileDetails = False
        Exit Function
    End If

    ' For USER profiles, Update the user ACLs in AD if     
    '     1.)the update was sucessfull
    '     2.)site supports profiles in AD
    '     3.)the current profile type is PROFILE_TYPE_USER    
    If  (bUpdated) AND _
        (MSCSProfileServiceUsesActiveDirectory) AND _
        (StrComp(sProfileType, PROFILE_TYPE_USER, vbTextCompare) = 0) Then
        ' Check if partner_desk_role is updated
        sChecked = GetRequestString(FIELD_USER_PARTNERDESK_ROLE & FIELD_UPDATED, "")
        If StrComp(sChecked, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
            bPaintACL = bHandlePaintingACLsForUsersInAD(sSubAction, _
                                                        sProfileIDs, _
                                                        arrPrevRole, _
                                                        arrCurrentRole)
            If Not bPaintACL Then
                bUpdated = False
                sStatusField = STATUS_FIELD_VALUE_ERROR
                sStatusID = E_ADDING_USER_TO_GROUP_IN_AD
            End If
        End If
    End If    
    bSaveProfileDetails = bUpdated
End Function


' -----------------------------------------------------------------------------
' bSaveProperty
'    Generic property saving routine.  Checks if save of this property is
'    supported for user, and if so dispatches to ReadOnly/ReadWrite handlers
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'    Sibling function to bInsertProperty.
'    This function is never reached with sAccessType=Hidden becuase bSaveProfileDetails blocks that.
' -----------------------------------------------------------------------------
Function bSaveProperty(ByVal sSubAction, ByVal oProperty, ByVal rsProfiles, _
                       ByVal sProfileIDs, ByVal sAccessType)
	Dim bUpdated

	bUpdated = True
			    	
    Select Case sAccessType
			    	    			    	        
        ' Handle read-only properties
        Case ACCESS_READONLY
            Call SaveReadOnlyProperty( _
	                                       sSubAction, _
                                           oProperty, _
                                           rsProfiles, _
                                           sProfileIDs _
                                      )

        ' Handle read-write properties
        Case ACCESS_READWRITE			    	        
             bUpdated = bSaveReadWriteProperty( _
	                                               sSubAction, _
                                                   oProperty, _
                                                   rsProfiles, _
                                                   sProfileIDs _
                                              )
	End Select	

	bSaveProperty = bUpdated
End Function


' -----------------------------------------------------------------------------
' SaveUserAccountControlProperty
'
' Description:
'	Sub to save the User Account Control property (AD Specific)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveUserAccountControlProperty(ByVal sSubAction, ByVal oProperty, _
                                   ByVal sProfileIDs, ByVal rsProfiles)
    Dim sChecked, sUserIDs, sAdminIDs, sCheckedRole
    Dim iProfileCount
    Dim sCheckedStatus, sActiveUserIDs
    Dim varPropertyValue
    Dim iUserAccountStatus, iUserPartnerDeskRole
        
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)    
    If (iProfileCount = 0) OR (iProfileCount = 1) Then        
        iUserAccountStatus = MSCSAppFrameWork.RequestNumber(FIELD_USER_STATUS, Null)
        iUserPartnerDeskRole = MSCSAppFrameWork.RequestNumber(FIELD_USER_PARTNERDESK_ROLE, Null)

		If Not IsNull(iUserAccountStatus) Then
			' -- The user is not a regular user editing his/her own profile
			If iUserAccountStatus = ACCOUNT_ACTIVE Then
				If iUserPartnerDeskRole = ROLE_ADMIN Then
					varPropertyValue = AD_DELEGATED_ADMIN
				Else
					varPropertyValue = AD_USER_ACTIVE
				End If
			Else
				varPropertyValue = AD_USER_INACTIVE
			End If
			rsProfiles(oProperty.QualifiedName).Value = varPropertyValue
		End If
    Else
        ' check if both account_status and partner_desk roles are getting updated
        sCheckedStatus = GetRequestString(FIELD_USER_STATUS & FIELD_UPDATED, "")
        sCheckedRole = GetRequestString(FIELD_USER_PARTNERDESK_ROLE & FIELD_UPDATED, "")
        
        iUserAccountStatus = MSCSAppFrameWork.RequestNumber(FIELD_USER_STATUS, Null)
        iUserPartnerDeskRole = MSCSAppFrameWork.RequestNumber(FIELD_USER_PARTNERDESK_ROLE, Null)
        
        ' Both account_status and partner_desk_role are getting updated
        If (StrComp(sCheckedStatus, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) AND _
           (StrComp(sCheckedRole, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) Then           
           If iUserAccountStatus = ACCOUNT_ACTIVE Then
                If iUserPartnerDeskRole = ROLE_ADMIN Then
                    varPropertyValue = AD_DELEGATED_ADMIN
                Else    
                    varPropertyValue = AD_USER_ACTIVE
                End If    	    
           Else
                varPropertyValue = AD_USER_INACTIVE
           End If
           Call SaveCommonValueForAllSelectedRecords( _
                                                        sSubAction, _
                                                        oProperty.Name, _
                                                        varPropertyValue, _
                                                        sProfileIDs _
                                                    ) 
        ' Only account_status is getting updated
        ElseIf StrComp(sCheckedStatus, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
           If iUserAccountStatus = ACCOUNT_ACTIVE Then
                sAdminIDs = sGetAdminsOnly(sProfileIDs)
                varPropertyValue = AD_DELEGATED_ADMIN                
                Call SaveCommonValueForAllSelectedRecords( _
                                                             sSubAction, _
                                                             oProperty.Name, _
                                                             varPropertyValue, _
                                                             sAdminIDs _
                                                         )
                sUserIDs = sGetUsersOnly(sProfileIDs)
                varPropertyValue = AD_USER_ACTIVE
                Call SaveCommonValueForAllSelectedRecords( _
                                                             sSubAction, _
                                                             oProperty.Name, _
                                                             varPropertyValue, _
                                                             sUserIDs _
                                                         )
           Else
                varPropertyValue = AD_USER_INACTIVE                
                Call SaveCommonValueForAllSelectedRecords( _
                                                             sSubAction, _
                                                             oProperty.Name, _
                                                             varPropertyValue, _
                                                             sProfileIDs _
                                                         )
           End If
           
        ' Only partner_desk_role is getting updated
        ElseIf StrComp(sCheckedRole, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
            ' Update the partner_desk_role only for the active users (ACCOUNT_ACTIVE)
            sActiveUserIDs = GetActiveUsersOnly(sProfileIDs)    
                    
            If iUserPartnerDeskRole = ROLE_ADMIN Then
                varPropertyValue = AD_DELEGATED_ADMIN
            Else    
                varPropertyValue = AD_USER_ACTIVE
            End If
            Call SaveCommonValueForAllSelectedRecords( _
                                                         sSubAction, _
                                                         oProperty.Name, _
                                                         varPropertyValue, _
                                                         sActiveUserIDs _
                                                     )
        End If
    End If
End Sub


' -----------------------------------------------------------------------------
' GetActiveUsersOnly
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetActiveUsersOnly(ByVal sProfileIDs)
    Dim i    
    Dim arrIDs
    Dim iIndex
    Dim arrNewIDs()
    Dim sActiveUserIDs
    Dim iUserAccountStatus
    
    iIndex = 0
    arrIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)    
    ReDim arrNewIDs(UBound(arrIDs))
    For i = LBound(arrIDs) To UBound(arrIDs)
        iUserAccountStatus = GetUserPropertyFromID(FIELD_USER_STATUS, arrIDs(i))
        If iUserAccountStatus = ACCOUNT_ACTIVE Then
            arrNewIDs(iIndex) = arrIDs(i)
            iIndex = iIndex + 1
        End If
    Next
    ReDim Preserve arrNewIDs(iIndex)

    sActiveUserIDs = Join(arrNewIDs, PROFILE_ID_SEPARATOR)

    GetActiveUsersOnly = sActiveUserIDs
End Function


' -----------------------------------------------------------------------------
' sGetAdminsOnly
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetAdminsOnly(ByVal sProfileIDs)
    Dim i    
    Dim arrIDs
    Dim iIndex
    Dim arrNewIDs()
    Dim sAdminIDs
    Dim iUserRole
    
    iIndex = 0
    arrIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)    
    ReDim arrNewIDs(UBound(arrIDs))
    For i = LBound(arrIDs) To UBound(arrIDs)
        iUserRole = GetUserPropertyFromID(FIELD_USER_PARTNERDESK_ROLE, arrIDs(i))
        If iUserRole = ROLE_ADMIN Then
            arrNewIDs(iIndex) = arrIDs(i)
            iIndex = iIndex + 1
        End If
    Next
    ReDim Preserve arrNewIDs(iIndex)

    sAdminIDs = Join(arrNewIDs, PROFILE_ID_SEPARATOR)

    sGetAdminsOnly = sAdminIDs
End Function


' -----------------------------------------------------------------------------
' sGetUsersOnly
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetUsersOnly(ByVal sProfileIDs)
    Dim i    
    Dim arrIDs
    Dim iIndex
    Dim sUserIDs
    Dim arrNewIDs()
    Dim iUserRole
    
    iIndex = 0
    arrIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)    
    ReDim arrNewIDs(UBound(arrIDs))
    For i = LBound(arrIDs) To UBound(arrIDs)
        iUserRole = GetUserPropertyFromID(FIELD_USER_PARTNERDESK_ROLE, arrIDs(i))
        If iUserRole <> ROLE_ADMIN Then
            arrNewIDs(iIndex) = arrIDs(i)
            iIndex = iIndex + 1
        End If
    Next
    ReDim Preserve arrNewIDs(iIndex)

    sUserIDs = Join(arrNewIDs, PROFILE_ID_SEPARATOR)

    sGetUsersOnly = sUserIDs
End Function


' -----------------------------------------------------------------------------
' SaveUserPartnerDeskRoleProperty
'
' Description:
'	Sub to save the User Partner Desk Role property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveUserPartnerDeskRoleProperty(ByVal sSubAction, ByVal oProperty, _
      ByVal rsProfile, ByVal sProfileIDs, ByRef arrPrevRole, ByRef arrCurrentRole) 
    Dim i
    Dim sIDs
    Dim sChecked
    Dim iProfileCount
    Dim varPropertyValue
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
            
    ' Get the property value
	varPropertyValue = MSCSAppFrameWork.RequestNumber(oProperty.Name, Null)
            
	If (iProfileCount = 0) OR (iProfileCount = 1) Then	        
	    ' Store the user previous partner_desk_role, this is required only in
	    '     the AD scenario(while painting ACLs in AD) to move the user from
	    '     ADMIN group to USER group or vice-versa	
	    If  MSCSProfileServiceUsesActiveDirectory Then
	        ReDim arrPrevRole(1)
	        ReDim arrCurrentRole(1)	            
	        arrPrevRole(0) = rsProfile(USER_PARTNERDESK_ROLE).Value
	        arrCurrentRole(0) = varPropertyValue
        End If
            
        ' Save the user partner desk role
	    rsProfile(oProperty.QualifiedName).Value = varPropertyValue
    
    Else        
        sChecked = GetRequestString(oProperty.Name & FIELD_UPDATED, "")
        If StrComp(sChecked, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
	        ' Store the user previous partner_desk_role, this is required only in
	        '     the AD scenario(while painting ACLs in AD) to move the user from
	        '     ADMIN group to USER group or vice-versa
	        If  MSCSProfileServiceUsesActiveDirectory Then
                sIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)
                ReDim arrPrevRole(UBound(sIDs))
                ReDim arrCurrentRole(UBound(sIDs))
                For i = LBound(sIDs) To UBound(sIDs)
                    arrPrevRole(i) = GetUserPropertyFromID(FIELD_USER_PARTNERDESK_ROLE, sIDs(i))
                    arrCurrentRole(i) = varPropertyValue
                Next
            End If
                            
            ' Save the partner desk role for all the selected users                
            Call SaveCommonValueForAllSelectedRecords( _
                                                         sSubAction, _
                                                         oProperty.Name, _
                                                         varPropertyValue, _
                                                         sProfileIDs _
                                                     )
         End If        
    End If    
End Sub


' -----------------------------------------------------------------------------
' InsertHiddenProperty
'
' Description:
'	Sub to save any Hidden(for the current user) property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InsertHiddenProperty(ByVal oProperty, ByVal rsProfile)
	   Select Case oProperty.Name
	        Case FIELD_USER_ORGID
	             rsProfile(oProperty.QualifiedName).Value = mscsUserProfile(USER_ORGID).Value
	   End Select
End Sub


' -----------------------------------------------------------------------------
' SaveReadOnlyProperty
'
' Description:
'	Sub to save any Read-Only property.  Generally, there is nothing to do
'       except when the property is a ChangedBy or ChangedDate field 
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveReadOnlyProperty(ByVal sSubAction, _
                         ByVal oProperty, _
                         ByVal rsProfiles, _
                         ByVal sProfileIDs)
    Dim iProfileCount
    Dim oUserProfile, sUserName
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)

	Select Case oProperty.Name
	Case FIELD_USER_CHANGED_BY
		Set oUserProfile = EnsureUserProfile()
		sUserName = oUserProfile.Fields(GetQualifiedName(GENERAL_INFO_GROUP, LOGON_NAME)).Value	
	     If (iProfileCount = 0) OR (iProfileCount = 1) Then
	        rsProfiles(oProperty.QualifiedName).Value = sUserName
	     ElseIf iProfileCount > 1 Then	        
	        Call SaveCommonValueForAllSelectedRecords(sSubAction, _
                                                      oProperty.Name, _
                                                      sUserName, _
                                                      sProfileIDs)
	     End If

	Case FIELD_ORG_CHANGED_BY
	     If (iProfileCount = 0) OR (iProfileCount = 1) Then
	        rsProfiles(oProperty.QualifiedName).Value = m_UserID
	     ElseIf iProfileCount > 1 Then	        
	        Call SaveCommonValueForAllSelectedRecords(sSubAction, _
                                                      oProperty.Name, _
                                                      m_UserID, _
                                                      sProfileIDs)
	     End If
	
	Case FIELD_USER_DATE_UPDATED, _
	     FIELD_ORG_DATE_UPDATED
	     
	     If (iProfileCount = 0) OR (iProfileCount = 1) Then
	        rsProfiles(oProperty.QualifiedName).Value = Now()
	     Else	        
	        Call SaveCommonValueForAllSelectedRecords(sSubAction, _
                                                      oProperty.Name, _
                                                      Now(), _
                                                      sProfileIDs)
	     End If
	End Select
End Sub


' -----------------------------------------------------------------------------
' bSaveReadWriteProperty
'
' Description:
'	Sub to save any Read Write property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bSaveReadWriteProperty(ByVal sSubAction, ByVal oProperty, _
                                ByVal rsProfiles, ByVal sProfileIDs)
    Dim sChecked, sCountryName
    Dim bUpdated
    Dim iProfileCount
    Dim varPropertyValue

    bUpdated = True
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)

    ' Get the property value posted depending upon type of the property                
	If oProperty.Type = DATA_TYPE_SMALLINT Then
	   ' BOOLEAN property
	   varPropertyValue = MSCSAppFrameWork.RequestNumber(oProperty.Name, Null)
	   varPropertyValue = Not(IsNull(varPropertyValue))
	ElseIf oProperty.Type = DATA_TYPE_NUMBER Then
	   varPropertyValue = MSCSAppFrameWork.RequestNumber(oProperty.Name, Null)
	ElseIf oProperty.Type = DATA_TYPE_DATETIME Then
	   varPropertyValue = MSCSAppFrameWork.RequestDate(oProperty.Name, Null)
	Else
	   varPropertyValue = GetRequestString(oProperty.Name, Null)
	End If

	' Check for PRIMARY_KEY/UNIQUE_KEY violations
	If (oProperty.Attributes.IsPrimaryKey = True) OR _
	   (oProperty.Attributes.IsUniqueKey = True) Then

	    On Error Resume Next	   
	        rsProfiles(oProperty.QualifiedName).Value = varPropertyValue
	        If Err.Number = OBJECT_ALREADY_EXIST Then
	            bUpdated = False
	        End If
	    On Error GoTo 0
	
	Else
	   ' For editing single profile only
	   If ((iProfileCount = 0) OR (iProfileCount = 1)) Then
			If (Not IsNull(varPropertyValue)) Then
				If IsNull(rsProfiles(oProperty.QualifiedName).Value) Then
					rsProfiles(oProperty.QualifiedName).Value = varPropertyValue
				ElseIf rsProfiles(oProperty.QualifiedName).Value <> varPropertyValue Then
					rsProfiles(oProperty.QualifiedName).Value = varPropertyValue
				End If
			End If
	   ElseIf iProfileCount > 1 Then            
            sChecked = GetRequestString(oProperty.Name & FIELD_UPDATED, "")
            If StrComp(sChecked, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
                Call SaveCommonValueForAllSelectedRecords( _
                                                             sSubAction, _
                                                             oProperty.Name, _
                                                             varPropertyValue, _
                                                             sProfileIDs _
                                                         )
	        End If
	   End If
	End If	
	
	bSaveReadWriteProperty = bUpdated
End Function


' -----------------------------------------------------------------------------
' bHandlePaintingACLsForUsersInAD
'
' Description:
'	Sub to handle painting of appropriate ACLs for user in AD (adding the 
'   user/s to appropriate AD Groups, i.e. ADMINGROUP or USERGROUP
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bHandlePaintingACLsForUsersInAD(ByVal sSubAction, ByVal sProfileIDs, _
            ByVal arrPrevRole(), ByVal arrCurrentRole())
    Dim i
    Dim sIDs, sUserCN
    Dim bPaintACL
    
    bPaintACL = True
    sIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)
    
    ' Loop through the user id's stored in the global variable sProfileIDs
    For i = LBound(sIDs) To UBound(sIDs)
        sUserCN =  GetUserPropertyFromID(FIELD_USER_LOGIN_NAME, sIDs(i))
        
        ' Get the values for current user (based in user id's) current and 
        '     previous, partner desk role from the global variables
        bPaintACL = bPaintACLForUser( _
                                        sUserCN, _
                                        sSubAction, _
                                        arrPrevRole(i), _
                                        arrCurrentRole(i) _
                                    )
        If Not bPaintACL Then
            Exit For
        End If
    Next
    bHandlePaintingACLsForUsersInAD = bPaintACL
End Function


' -----------------------------------------------------------------------------
' SaveProfileProperty
'
' Description:
'	Sub to save a property value in any profile
'
' Parameters:
'
' Returns:
'
' Notes :
'    Causes a rsProfile.Update and should be avoided unless saving only a handful
'    of properties in a row
' -----------------------------------------------------------------------------
Sub SaveProfileProperty(ByRef rsProfile, ByVal sPropertyName, ByVal sValue)
    Dim SubProperty
    Dim RootProperty
    Dim sFullFieldName


    For Each RootProperty In rsProfile.Fields
        If IsObject(RootProperty) Then
            For Each SubProperty In RootProperty.Value
                If SubProperty.Name = sPropertyName Then
                    sFullFieldName = RootProperty.Name & "." & sPropertyName
                    rsProfile(sFullFieldName).Value = sValue
					On Error Resume Next
                    Call rsProfile.Update
                    Exit For
                End If
            Next
        Else
            If RootProperty.Name = sPropertyName Then
                SubProperty.Value = sValue
            End If
        End If
    Next
    
    Set RootProperty = Nothing
    Set SubProperty = Nothing
End Sub


' -----------------------------------------------------------------------------
' SaveCommonValueForAllSelectedRecords
'
' Description:
'	Sub to save a common value to all the selected records
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveCommonValueForAllSelectedRecords(ByVal sSubAction, ByVal sPropertyName, _
                                         ByVal varValue, ByVal sProfileIDs)
    Dim sSelectProfileIDs    
    Dim arrIDs
    Dim i
    Dim sProfileType
    
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    
    If StrComp(sSubAction, SELECTED_ACTION, vbTextCompare) = 0 Then
        sSelectProfileIDs = GetRequestString(SELECTED_ATTRIBUTE_NAME, Null)
        arrIDs = Split(sSelectProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)
    Else
        arrIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)
    End If
    
    For i = LBound(arrIDs) To UBound(arrIDs)
        Select Case sProfileType
            Case PROFILE_TYPE_USER
                 Call SaveUserPropertyForID(sPropertyName, arrIDs(i), varValue)
            Case Else
        End Select
    Next
    
End Sub


' -----------------------------------------------------------------------------
' bSaveProfileAndRedirect
'
' Description:
'	Function to update the profile properties and redirect to a different
'   page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bSaveProfileAndRedirect(ByVal sSubAction, ByRef sProfileIDs, _
            ByRef sStatusField, ByRef iStatusID, ByVal sActionPage)
    Dim htm
    Dim bUpdated
    Dim rsProfiles
    Dim iProfileCount
    
    bUpdated = True    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    
    If iProfileCount = 1 Then
        Set rsProfiles = rsGetProfileDetails(sProfileIDs)
    Else
        rsProfiles = Null
    End If
        
    ' Save profile/s (supports multi-edit) with new values
    bUpdated = bSaveProfileDetails(sSubAction, _
                                   sProfileIDs, _
                                   sStatusField, _
                                   iStatusID, _
                                   rsProfiles)
       
    ' ReDirect to the action page passed to this function
    If bUpdated Then
        Response.Redirect (sActionPage)
    End If
    
    If Not IsNull(rsProfiles) Then
        Set rsProfiles = Nothing
    End If
    
    bSaveProfileAndRedirect = bUpdated
End Function


' -----------------------------------------------------------------------------
' UpdateUserPasswordProperty
'
' Description:
'	Sub to update the user password property depending upon
'	current user type (DELEGATED_ADMIN or USER)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub UpdateUserPasswordProperty(ByVal sAction, _
                               ByVal sSubAction, _
                               ByVal sProfileIDs, _
                               ByVal sUserOldPassword, _
                               ByVal sUserNewPassword)
    Dim objUser
    Dim iProfileCount
    Dim sStatusMessage, sProfileListPage
    Dim dictSearchOptions
    Dim oUserProfile, sUserName
    Dim varUserNewPassword

    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    If iProfileCount = 1 Then                    
        Set objUser = rsGetUserDetails(sProfileIDs)
        
        If objUser Is Nothing Then
            Err.Raise HRESULT_E_FAIL
        End If

        ' If using AD, we pass the specified oldpassword and the newpassword as a multivalue
        ' to the password property --  AD will do the checking of the oldpasword for us.
        ' If not using AD, we must manually check that the specified oldpassword equals the password on file.
        If Application("MSCSProfileServiceUsesActiveDirectory") Then
            varUserNewPassword = Array (sUserOldPassword, sUserNewPassword)
        Else
            If (sUserOldPassword<>objUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_USER_PASSWORD)).Value) Then
                Err.Raise -1 'This error is caught by calling function
            End If
            varUserNewPassword = sUserNewPassword
        End If

        objUser(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_USER_PASSWORD)).Value = varUserNewPassword        

        ' Update changedby/lastchanged
        Set oUserProfile = EnsureUserProfile()

        sUserName = oUserProfile.Fields(GetQualifiedName(GENERAL_INFO_GROUP, LOGON_NAME)).Value   
        objUser(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_USER_CHANGED_BY)).Value = sUserName
        objUser(GetQualifiedName(PROFILE_SYSTEM_GROUP, FIELD_USER_DATE_UPDATED)).Value = Now()
        objUser.Update()
    Else
        Err.Raise E_NOTIMPL
    End If
End Sub


' -----------------------------------------------------------------------------
' UpdateSelectActionPropertyName
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub UpdateSelectActionPropertyName(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim iProfileCount
    Dim sSelectFieldName, sSelectProfileID
    Dim rsProfile
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    sSelectFieldName = sGetSelectActionPropertyName(sAction)           
    sSelectProfileID = GetRequestString(SELECTED_ATTRIBUTE_NAME, Null)

    Dim oUserProfile, sLogonName
    Set oUserProfile = EnsureUserProfile()
    Set rsProfile = rsGetProfileDetails(sProfileIDs)
    sLogonName = oUserProfile.Fields(GetQualifiedName(GENERAL_INFO_GROUP, LOGON_NAME)).Value

    ' For single profile editing
    If iProfileCount = 1 Then
        Call SaveProfileProperty(rsProfile, sSelectFieldName, sSelectProfileID)
        Call SaveProfileProperty(rsProfile, FIELD_GENERIC_CHANGED_BY, sLogonName)
        Call SaveProfileProperty(rsProfile, FIELD_GENERIC_DATE_UPDATED, Now())
        Set rsProfile = Nothing
        
    ' For multi-edit   
    ElseIf iProfileCount > 1 Then
       Call SaveCommonValueForAllSelectedRecords(sSelectFieldName, sSelectProfileID)
       Call SaveCommonValueForAllSelectedRecords(FIELD_GENERIC_CHANGED_BY, sLogonName)
       Call SaveCommonValueForAllSelectedRecords(FIELD_GENERIC_DATE_UPDATED, Now())
    End If    
End Sub


' -----------------------------------------------------------------------------
' SaveUserPropertyForID
'
' Description:
'	Sub to save the User property for a user ID
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveUserPropertyForID(ByVal sAttributeName, ByVal sID, ByVal sValue)
    Dim rsUser
    Set rsUser = rsGetProfileByKey( _
                                    FIELD_USER_ID, _
                                    sID, _
                                    PROFILE_TYPE_USER, _
                                    True _
                                )
    If rsUser Is Nothing Then
        Err.Raise HRESULT_E_FAIL  
    End If
    Call SaveProfileProperty(rsUser, sAttributeName, sValue)    
End Sub
%>