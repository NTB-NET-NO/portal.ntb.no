<%
' =============================================================================
' render_orders.asp
' Rendering functions for Orders
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================


' -----------------------------------------------------------------------------
' htmRenderOrderList
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderOrderList(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
                            ByRef iPageNumber, ByRef iTotalRecords, ByRef iTotalPages)
    Dim htmRow, htmTable, htmForm, htmContents
    Dim url
    Dim rowHeader, rowStatus
    Dim formSearch, formContent
    Dim tableFooter
    Dim sStatusField, sPageHeading, sCriteria, sTargetPage
    Dim iStatusID
    Dim dictSearchOptions
    Dim rsOrders
	Dim mscsUserProfile, mscsOrderGrp

	' $$ This call should move out of this presentation function
	Call PrepareOrderList(sAction, dictSearchOptions, mscsUserProfile, _
							sStatusField, iStatusID, rsOrders, _
							iPageNumber, iTotalRecords, iTotalPages, sCriteria, _
							sTargetPage)
							
    rowHeader = rowRenderHeader()
    formSearch = formRenderOrderSearchOptions(dictSearchOptions, mscsUserProfile, sCriteria)
	If (sStatusField = STATUS_FIELD_VALUE_SUCCESS) Then
		formContent = formRenderOrdersListContents(sAction, sSubAction, _
							rsOrders, iPageNumber, iTotalRecords, iTotalPages, _
							sTargetPage)
	    sStatusField = Null
		iStatusID = Null
	Else
		formContent = htmRenderEmptyRecordset(mscsMessageManager.GetMessage("L_MSG_FILTER_NORECORDS_Text", sLanguage))
	End If

    rowStatus = rowRenderStatusInfo(sStatusField, iStatusID, iTotalRecords, True)
    tableFooter = tableRenderFooterInfo(sAction, sSubAction, iPageNumber, iTotalRecords, _
                                           iTotalPages, False, True)

    ' Construct the HTML for the current page
    htmRow = rowHeader & rowStatus
    htmTable = RenderTable(htmRow, MSCSSiteStyle.StatusTable)
    url = GenerateURL(MSCSSitePages.HelpPage, Array(SOURCE_PAGE), Array(sThisPage))
    htmForm = RenderForm(url, htmTable, HTTP_POST)
    htmContents = htmForm
   
    sPageHeading = sGetPageHeading(sThisPage)
    htmContents = htmContents _
					& RenderText(sPageHeading, MSCSSiteStyle.Title) _
					& formSearch _
					& formContent _
					& tableFooter
    
    htmContents = RenderText(htmContents, MSCSSiteStyle.Body)

    ' Release the objects assigned in the global variables
    Set dictSearchOptions = Nothing
    Set rsOrders = Nothing
    
    htmRenderOrderList = htmContents
End Function


' -----------------------------------------------------------------------------
' formRenderOrderSearchOptions
'
' Description:
'	Function to render Simple and Advanced Search options for Orders
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderOrderSearchOptions(ByVal dictSearchOptions, ByVal mscsUserProfile, sCriteria)
    Dim htmColumn, htmTable, htmContent, htmForm, htmRow
    Dim arrCell, arrCellAlignment, arrQSKeyName, arrQSKeyValue
    Dim url, urlItem
    Dim sBtnText
    Dim bAddIfNotPresent
        		        
	' Add "Search Criteria" table to the page
	arrCell = Array(colRenderSimpleSearchTab(sCriteria), colRenderAdvancedSearchTab(sCriteria))
	arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
	htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchHeaderTable, False, "")
	htmContent = RenderTable(htmRow, MSCSSiteStyle.SearchHeaderTable)

    ' Add Simple Or Advanced search options to the page
	If StrComp(sCriteria, SEARCH_CRITERIA_ADVANCED, vbTextCompare) = 0 Then
	    htmTable = rowRenderAdvancedSearchOptionsForOrders(dictSearchOptions, mscsUserProfile)
	Else
	    htmTable = rowRenderSimpleSearchOptionsForOrders(dictSearchOptions, mscsUserProfile)
	End If
	    
	' Add a HIDDEN FORM element for ACTION_TYPE, to store the value of 
	'     FILTER_ACTION. This is required to handle Return 
	'     key press by the user on the Search options.
	htmContent = htmContent & RenderHiddenField(ACTION_TYPE, FILTER_ACTION)

    ' Add a SUBMIT button (FILTER_ACTION) to the page	
	sBtnText = mscsMessageManager.GetMessage("L_FILTER_ACTION_BUTTON_Text", sLanguage)
	htmColumn = RenderSubmitButton(BUTTON_SUBMIT & FILTER_ACTION, sBtnText, MSCSSiteStyle.Button)
	
	arrCell = Array(htmColumn, Null)
	arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
	htmTable = htmTable & TRTag(arrCell, arrCellAlignment, Null, False, "")
	htmContent = htmContent & RenderTable(htmTable, MSCSSiteStyle.SearchBodyTable)    
        
    bAddIfNotPresent = False
    arrQSKeyName = Array(QUERY_STRING_CRITERIA)
    arrQSKeyValue = Array(dictSearchOptions.filter_type)    
    url = sReConstructPageURL(arrQSKeyName, arrQSKeyValue)
    
    ' Add the search options FORM to the page
	htmForm = RenderForm(url, htmContent, HTTP_POST)
		
    formRenderOrderSearchOptions = htmForm
End Function


' -----------------------------------------------------------------------------
' rowRenderSimpleSearchOptionsForOrders
'
' Description:
'	Function to add Simple search options contents ORDERS
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderSimpleSearchOptionsForOrders(ByVal dictSearchOptions, ByVal mscsUserProfile)
    Dim htmRow
    Dim sUserProperty, sSearchValue
    Dim arrCell, arrCellAlignment
    Dim iSearchValue
    
    ' ORDERS details specific to an organization (can be accessed only by 
    '     DELEGATED_ADMIN)    
    If StrComp(sThisPage, MSCSSitePages.ListPartnerOrders, vbTextCompare) = 0 Then
        sSearchValue = dictSearchOptions.Value(FILTER_FIELD_ORDER_USER_LAST_NAME)
        htmRow = htmRow & _
                 rowRenderSearchOptionForString( _
                                                   mscsMessageManager.GetMessage("L_FIELD_LAST_NAME_Text", sLanguage), _
                                                   FILTER_FIELD_ORDER_USER_LAST_NAME, _
                                                   mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                                   64, _
                                                   sSearchValue _
                                               )
		
		sSearchValue = dictSearchOptions.Value(FILTER_FIELD_ORDER_USER_FIRST_NAME)
        htmRow = htmRow & _
				 rowRenderSearchOptionForString( _
                                                   mscsMessageManager.GetMessage("L_FIELD_FIRST_NAME_Text", sLanguage), _
                                                   FILTER_FIELD_ORDER_USER_FIRST_NAME, _
                                                   mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                                   64, _
                                                   sSearchValue _
                                               )
        
    ' ORDERS details specific to a user (can be accessed by all REGISTERED_USERS)
    ElseIf StrComp(sThisPage, MSCSSitePages.ListUserOrders, vbTextCompare) = 0 Then
    
      '  sUserProperty = mscsUserProfile(USER_FIRST_NAME).Value
      '  arrCell = Array(mscsMessageManager.GetMessage("L_FIELD_FIRST_NAME_Text", sLanguage), sUserProperty)
      '  arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
      '  htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")
                 
      '  sUserProperty = mscsUserProfile(USER_LAST_NAME).Value
      '  arrCell = Array(mscsMessageManager.GetMessage("L_FIELD_LAST_NAME_Text", sLanguage), sUserProperty)
      '  arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
      '  htmRow = htmRow & _
      '           TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")
    
    End If
        
    ' ORDER details accessible for all the REGISTERED_USERS
    sSearchValue = dictSearchOptions.Value(FIELD_ORDER_NUMBER)
    htmRow = htmRow & _
             rowRenderSearchOptionForString( _
                                               mscsMessageManager.GetMessage("L_FIELD_ORDER_NUMBER_Text", sLanguage), _
                                               FIELD_ORDER_NUMBER, _
                                               mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                               32, _
                                               sSearchValue _
                                           )
    'iSearchValue = dictSearchOptions.Value(FIELD_ORDER_STATUS)
    'htmRow = htmRow & _
    '         rowRenderSearchOptionForInteger( _
    '                                            FIELD_ORDER_STATUS, _
    '                                            mscsMessageManager.GetMessage("L_FIELD_ORDER_STATUS_Text", sLanguage), _
    '                                            iSearchValue _
    '                                        )

    rowRenderSimpleSearchOptionsForOrders = htmRow
End Function


' -----------------------------------------------------------------------------
' rowRenderAdvancedSearchOptionsForOrders
'
' Description:
'	Function to add Advanced search options for ORDERS
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderAdvancedSearchOptionsForOrders(ByVal dictSearchOptions, ByVal mscsUserProfile)
    Dim htmColumn, htmRow
    Dim sUserProperty, sSearchValue
    Dim arrCell, arrCellAlignment
    Dim iSearchValue
    
    ' ORDERS details specific to an organization (can be accessed only by the 
    '     DELEGATED_ADMIN)        
    If StrComp(sThisPage, MSCSSitePages.ListPartnerOrders, vbTextCompare) = 0 Then            
        sSearchValue = dictSearchOptions.Value(FILTER_FIELD_ORDER_USER_LAST_NAME)
        htmRow = htmRow & _
                 rowRenderSearchOptionForString( _
                                                   mscsMessageManager.GetMessage("L_FIELD_LAST_NAME_Text", sLanguage), _
                                                   FILTER_FIELD_ORDER_USER_LAST_NAME, _
                                                   mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                                   64, _
                                                   sSearchValue _
                                               )
		sSearchValue = dictSearchOptions.Value(FILTER_FIELD_ORDER_USER_FIRST_NAME)
        htmRow = htmRow & _
				 rowRenderSearchOptionForString( _
                                                   mscsMessageManager.GetMessage("L_FIELD_FIRST_NAME_Text", sLanguage), _
                                                   FILTER_FIELD_ORDER_USER_FIRST_NAME, _
                                                   mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                                   64, _
                                                   sSearchValue _
                                               )
        
    ' ORDERS details specific to a user (can be accessed all REGISTERED_USERS)
    ElseIf StrComp(sThisPage, MSCSSitePages.ListUserOrders, vbTextCompare) = 0 Then
        sUserProperty = mscsUserProfile(USER_LAST_NAME).Value
        arrCell = Array(mscsMessageManager.GetMessage("L_FIELD_LAST_NAME_Text", sLanguage), sUserProperty)
        arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
        htmRow = htmRow & _
                 TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")
        
        sUserProperty = mscsUserProfile(USER_FIRST_NAME).Value
        arrCell = Array(mscsMessageManager.GetMessage("L_FIELD_FIRST_NAME_Text", sLanguage), sUserProperty)
        arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
        htmRow = htmRow & _
				 TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")
    End If        
        
    ' ORDER details accessible for all the REGISTERED_USERS
    sSearchValue = dictSearchOptions.Value(FIELD_ORDER_NUMBER)
    htmRow = htmRow & _
             rowRenderSearchOptionForString( _
                                               mscsMessageManager.GetMessage("L_FIELD_ORDER_NUMBER_Text", sLanguage), _
                                               FIELD_ORDER_NUMBER, _
                                               mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                               32, _
                                               sSearchValue _
                                           )            
    ' Display the po_number only in the case of B2B sites
    If dictConfig.i_DelegatedAdminOptions = DELEGATED_ADMIN_SUPPORTED Then
        sSearchValue = dictSearchOptions.Value(FILTER_FIELD_ORDER_PO_NUMBER)
        htmRow = htmRow & _
                 rowRenderSearchOptionForString( _
                                                   mscsMessageManager.GetMessage("L_FIELD_ORDER_PONUMBER_Text", sLanguage), _
                                                   FILTER_FIELD_ORDER_PO_NUMBER, _
                                                   mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                                   32, _
                                                   sSearchValue _
                                               )
    End If
                                                   
    sSearchValue = dictSearchOptions.Value(FILTER_FIELD_ORDER_SKU)
    htmRow = htmRow & _
             rowRenderSearchOptionForString( _
                                               mscsMessageManager.GetMessage("L_FIELD_ORDER_SKU_Text", sLanguage), _
                                               FILTER_FIELD_ORDER_SKU, _
                                               mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                               50, _
                                               sSearchValue _
                                           )
    sSearchValue = dictSearchOptions.Value(FIELD_ORDER_LINEITEM_DESC)
    htmRow = htmRow & _
             rowRenderSearchOptionForString( _
                                               mscsMessageManager.GetMessage("L_FIELD_ORDER_DESCRIPTION_Text", sLanguage), _
                                               FIELD_ORDER_LINEITEM_DESC, _
                                               mscsMessageManager.GetMessage("L_FILTER_MSG_STRING_Text", sLanguage), _
                                               255, _
                                               sSearchValue _
                                           )
    iSearchValue = dictSearchOptions.Value(FIELD_ORDER_STATUS)
    htmRow = htmRow & _
             rowRenderSearchOptionForInteger( _
                                                FIELD_ORDER_STATUS, _
                                                mscsMessageManager.GetMessage("L_FIELD_ORDER_STATUS_Text", sLanguage), _
                                                iSearchValue _
                                            )
    iSearchValue = dictSearchOptions.Value(FIELD_ORDER_DATE_TYPE)
    htmRow = htmRow & _
             rowRenderSearchOptionForInteger( _
                                                FIELD_ORDER_DATE_TYPE, _
                                                mscsMessageManager.GetMessage("L_ORDER_DATE_TYPE_Text", sLanguage), _
                                                iSearchValue _
                                            )
                                                
    htmColumn = RenderTextBox( _
                                 FIELD_ORDER_START_DATE, _
                                 GetRequestString(FIELD_ORDER_START_DATE, Null), _
                                 DEFAULT_TEXT_BOX_WIDTH, _
                                 DEFAULT_TEXT_BOX_WIDTH, _
                                 MSCSSiteStyle.TextBox _
                            )
    htmColumn = htmColumn & htmRenderBlankSpaces(4) & _
                            mscsMessageManager.GetMessage("L_FIELD_ORDER_ENDDATE_Text", sLanguage) & _
                            htmRenderBlankSpaces(4)
    htmColumn = htmColumn & RenderTextBox( _
                                             FIELD_ORDER_END_DATE, _
                                             GetRequestString(FIELD_ORDER_END_DATE, Null), _
                                             DEFAULT_TEXT_BOX_WIDTH, _
                                             DEFAULT_TEXT_BOX_WIDTH, _
                                             MSCSSiteStyle.TextBox _
                                        )
    arrCell = Array(mscsMessageManager.GetMessage("L_FIELD_START_DATE_Text", sLanguage), htmColumn)
    arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
    rowRenderAdvancedSearchOptionsForOrders = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchBodyTable, False, "")            
End Function


' -----------------------------------------------------------------------------
' formRenderOrdersListContents
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderOrdersListContents(ByVal sAction, ByVal sSubAction, ByVal rsOrders, _
                                         ByRef iPageNumber, ByRef iTotalRecords, _
                                         ByRef iTotalPages, ByVal sTargetPage)
    Dim htmTable, htmTaskButtons, htmContent
    Dim url
            
    htmTaskButtons = tableRenderOrdersListTasks(sAction)
    htmTable = tableRenderOrdersListContents(sAction, sSubAction, rsOrders, _
                                                iPageNumber, iTotalRecords, _
                                                iTotalPages)
    htmContent = htmTaskButtons & htmTable
    
    ' Wrap the contents of the page into a FORM element    
    url = GenerateURL(sTargetPage, Array(), Array())
    formRenderOrdersListContents = RenderForm(url, htmContent, HTTP_POST)
End Function


' -----------------------------------------------------------------------------
' tableRenderOrdersListTasks
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderOrdersListTasks(sAction)
    Dim htmRow, htmColumn
    Dim sBtnText
    Dim arrCell(0), arrCellAlignment(0)
    Dim url
    
    sBtnText = mscsMessageManager.GetMessage("L_ORDERDETAILS_ACTION_BUTTON_Text", sLanguage)
    htmColumn = RenderSubmitButton(BUTTON_SUBMIT & ORDERDETAILS_ACTION, sBtnText, MSCSSiteStyle.Button)

    arrCell(0) = htmColumn
    arrCellAlignment(0) = ALIGN_LEFT
	
    htmRow = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsHeaderTable, False, "")
    tableRenderOrdersListTasks = RenderTable(htmRow, MSCSSiteStyle.ListContentsHeaderTable)
End Function


' -----------------------------------------------------------------------------
' tableRenderOrdersListContents
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderOrdersListContents(ByVal sAction, ByVal sSubAction, ByVal rsOrders, _
                                          ByRef iPageNumber, ByRef iTotalRecords, _
                                          ByRef iTotalPages)
    Dim htmTable, htmRow
    Dim arrCell, arrCellAlignment
    Dim Count
    Dim i, iCount, iRecordsDisplayed
    Dim nPage, nCountTemp
    Dim sUniqueID
    Dim bRecordCountPresent
        
    bRecordCountPresent = True

    ' Check if record-set in the global variable is empty
    If IsNull(rsOrders) Then
        ' Empty record-set handler
        htmTable = htmRenderEmptyRecordSet(mscsMessageManager.GetMessage("L_MSG_FILTER_NORECORDS_Text", sLanguage))
    Else
        ' Initialize the local variable involved in paging mechanism        
        Call RelocateRecordPointer(Count, nPage, nCountTemp, bRecordCountPresent, _
                                      rsOrders, iPageNumber, iTotalRecords)

        htmRow = rowRenderListHeadingForOrders()
        
        iCount = 0
        While Not rsOrders.EOF
			' Add a hidden FORM element to store the ORDER NUMBER for the 
			' current record.
			htmRow = htmRow & RenderHiddenField( _
									SELECTED_ATTRIBUTE_NAME & _
									CStr(Count) & _
									RECORD_ID_ATTRIBUTE_NAME, _
									rsOrders(FIELD_ORDER_NUMBER).Value _
									)
			arrCell = arrPopulateOrderHeaderDetails(iCount, rsOrders)

			' Generate an HTML TR tag for this OrderHeader
			ReDim arrCellAlignment(UBound(arrCell))
			For i = LBound(arrCell) To UBound(arrCell)
				arrCellAlignment(i) = ALIGN_LEFT
			Next
			htmRow = htmRow & TRTag(arrCell, arrCellAlignment, _
								MSCSSiteStyle.ListContentsBodyTable, False, "")

			' Increment the counter which keeps track of the records displayed so far
			' on the current page                
			iRecordsDisplayed = iRecordsDisplayed + 1

            ' Increment the appropriate counters and move to the next record 
            ' in the record-set
            Count = Count + 1
            rsOrders.MoveNext
        Wend

        ' Wrap all the HTML TR's for each record in the record-set into TABLE tag
        htmTable = RenderTable(htmRow, MSCSSiteStyle.ListContentsBodyTable)

        ' Set the global paging variables depending upon the local variables
        Call SetGlobalPagingVariables(bRecordCountPresent, 0, iTotalRecords, _
                                         iPageNumber, iTotalPages, False)
    End If

    tableRenderOrdersListContents = htmTable
End Function


' -----------------------------------------------------------------------------
' rowRenderListHeadingForOrders
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderListHeadingForOrders()
    Dim htmRow
    Dim arrCell, arrCellAlignment()
    Dim i
    Dim oProperty
    
    If StrComp(sThisPage, MSCSSitePages.ListPartnerOrders, vbTextCompare) = 0 Then
        arrCell = Array( _
                           htmRenderBlankSpaces(4), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_NUMBER_Text", sLanguage), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_FIRSTNAME_Text", sLanguage), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_LASTNAME_Text", sLanguage), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_DATE_Text", sLanguage), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_STATUS_Text", sLanguage), _                                   
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_TOTALAMOUNT_Text", sLanguage) _
                       )
    ElseIf StrComp(sThisPage, MSCSSitePages.ListUserOrders, vbTextCompare) = 0 Then
        arrCell = Array( _
                           htmRenderBlankSpaces(4), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_NUMBER_Text", sLanguage), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_DATE_Text", sLanguage), _
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_STATUS_Text", sLanguage), _                                   
                           mscsMessageManager.GetMessage("L_COLUMN_ORDER_TOTALAMOUNT_Text", sLanguage) _
                       )                
    End If

    ReDim arrCellAlignment(UBound(arrCell))
    For i = LBound(arrCell) To UBound(arrCell)
        arrCellAlignment(i) = ALIGN_LEFT
    Next
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsHeaderTable, True, "")
    
    rowRenderListHeadingForOrders = htmRow
End Function


' -----------------------------------------------------------------------------
' arrPopulateOrderHeaderDetails
'
' Description:
'	Function to populate an ARRAY with an order details
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function arrPopulateOrderHeaderDetails(ByRef iCount, ByRef rsOrder)
    Dim arrCell
    
    ' ReDim the array depending upon the page name as the order information 
    '     pertaining to organization is different than that of a user
    If StrComp(sThisPage, MSCSSitePages.ListPartnerOrders, vbTextCompare) = 0 Then
       ' For orders specific to an organization (DELGATED_ADMIN access only)
       ReDim arrCell(6)
    Else
       ' For orders specific to a user (REGISTER_USER access)
       ReDim arrCell(4)
    End If
                     
    If iCount = 0 Then
	   arrCell(0) = RenderRadioButton( _
	                                     SELECTED_ATTRIBUTE_NAME, _
	                                     rsOrder(FIELD_ORDER_ID).Value, _
	                                     True, _
	                                     MSCSSiteStyle.RadioButton _
	                                  )
    Else
	   arrCell(0) = RenderRadioButton( _
	                                     SELECTED_ATTRIBUTE_NAME, _
	                                     rsOrder(FIELD_ORDER_ID).Value, _
	                                     False, _
	                                     MSCSSiteStyle.RadioButton _
	                                  )
	End If
	iCount = iCount + 1
	
    ' All the orders will have ORDER NUMBER, so populating the ARRAY with it
    arrCell(1) = rsOrder(FIELD_ORDER_NUMBER).Value

    ' Populate the ARRAY depending upon the page name as the order information 
    '     pertaining to organization is different than that of a user                     
    If StrComp(sThisPage, MSCSSitePages.ListPartnerOrders, vbTextCompare) = 0 Then
       ' For orders specific to an organization (DELGATED_ADMIN access only)
       arrCell(2) = rsOrder(FIELD_ORDER_USER_FIRST_NAME).Value
       arrCell(3) = rsOrder(FIELD_ORDER_USER_LAST_NAME).Value
       arrCell(4) = rsOrder(FIELD_ORDER_DATE_UPDATED).Value
       arrCell(5) = sGetOrderStatusDetails(rsOrder(FIELD_ORDER_STATUS_CODE).Value)
                        
       ' Add support for EURO
       arrCell(6) = htmRenderCurrency(rsOrder(FIELD_ORDER_TOTAL_TOTAL).Value)
    Else    
       ' For orders specific to a user (REGISTER_USER access)
       arrCell(2) = rsOrder(FIELD_ORDER_DATE_UPDATED).Value
       arrCell(3) = sGetOrderStatusDetails(rsOrder(FIELD_ORDER_STATUS_CODE).Value)
                        
       ' Add support for EURO
       arrCell(4) = htmRenderCurrency(rsOrder(FIELD_ORDER_TOTAL_TOTAL).Value)
    End If

    arrPopulateOrderHeaderDetails = arrCell
End Function


' -----------------------------------------------------------------------------
' htmRenderOrderDetails
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderOrderDetails(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
                               ByVal rsOrder)
    Dim rowHeader, rowStatus
    Dim formContent
    Dim htmRow, htmTable, htmForm, htmContents
    Dim url
    Dim sPageHeading
        
	' Get the HTML for the current page
    rowHeader = rowRenderHeader()
    formContent = formRenderOrderContents(sAction, sSubAction, sProfileIDs, rsOrder)    
    
    ' Construct the HTML for the current page    
    htmRow = rowHeader
    htmRow = htmRow & rowStatus
    htmTable = RenderTable(htmRow, MSCSSiteStyle.StatusTable)
    url = GenerateURL(MSCSSitePages.HelpPage, Array(SOURCE_PAGE), Array(sThisPage))
    htmForm = RenderForm(url, htmTable, HTTP_POST)
    htmContents = htmForm
    
    sPageHeading = sGetPageHeading(sThisPage)
    htmContents = htmContents & _
                  RenderText(sPageHeading, MSCSSiteStyle.Title)
    htmContents = htmContents & formContent    

    htmContents = RenderText(htmContents, MSCSSiteStyle.Body)
    
    htmRenderOrderDetails = htmContents
End Function


' -----------------------------------------------------------------------------
' formRenderOrderContents
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderOrderContents(sAction, sSubAction, sProfileIDs, rsOrder)    
    Dim htmForm, htmTable, htmTaskButtons, htmContent
    Dim url
    Dim sSourcePage, sPropertyName
       
    htmTaskButtons = tblOrderDetailsTasks(sSubAction, sProfileIDs)
    htmTable = tblRenderOrderDetailsContents(rsOrder)    
    htmContent = htmTaskButtons & htmTable
    
    sSourcePage = GetRequestString(SOURCE_PAGE, Null)
    url = GenerateURL(sThisPage, Array(), Array())
    htmContent = htmContent & RenderHiddenField(ACTION_TYPE, sAction)
    htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, sSubAction)
    htmContent = htmContent & RenderHiddenField(GUIDS, sProfileIDs)
    htmContent = htmContent & RenderHiddenField(SOURCE_PAGE, sSourcePage)
    
    ' Wrap the contents of the page into a FORM element    
    htmForm = RenderForm(url, htmContent, HTTP_POST)
    
    formRenderOrderContents = htmForm    
End Function


' -----------------------------------------------------------------------------
' tblOrderDetailsTasks
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tblOrderDetailsTasks(ByVal sSubAction, ByVal sProfileIDs)
    Dim htmTable, htmRow, htmColumn
    Dim sBtnText
    Dim arrCell(0), arrCellAlignment(0)
    Dim url    
    
    sBtnText = mscsMessageManager.GetMessage("L_CANCEL_ACTION_BUTTON_Text", sLanguage)
    htmColumn = RenderSubmitButton(BUTTON_SUBMIT & CANCEL_ACTION, sBtnText, MSCSSiteStyle.Button)

    arrCell(0) = htmColumn
    arrCellAlignment(0) = ALIGN_LEFT
          
    htmRow = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsHeaderTable, False, "")
    htmTable = RenderTable(htmRow, MSCSSiteStyle.ContentsHeaderTable)
    
    tblOrderDetailsTasks = htmTable
End Function


' -----------------------------------------------------------------------------
' tblRenderOrderDetailsContents
'
' Description:
'	Function to display Order Header and Order Line items in an Edit sheet format
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tblRenderOrderDetailsContents(ByVal rsOrder)
    Dim htmTable, htmRow
    Dim arrCell, arrCellAlignment
    Dim sCurrencyValue, sValue
	Dim OrderFormName, Item
	Dim addr_id
	Dim i
	Dim mscsRandomOrderform, mscsOrderGrp
        
    Set mscsOrderGrp = mscsGetOrderGroupWithDiscountsByID(rsOrder.Fields("ordergroup_id"))
    Set mscsRandomOrderform = GetAnyOrderForm(mscsOrderGrp)

    htmRow = cellRenderReadOnlyProperty( _
	                                      "order_number", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_NUMBER_Text", sLanguage), _
	                                      mscsOrderGrp.Value("order_number"), _
	                                      mscsOrderGrp.Value("order_number") _
	                                  )
	
	htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "order_create_date", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_DATE_Text", sLanguage), _
	                                      mscsOrderGrp.Value("order_create_date"), _
	                                      mscsOrderGrp.Value("order_create_date") _
	                                  )
	
	htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "order_status_code", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_STATUS_Text", sLanguage), _
	                                      mscsOrderGrp.Value("order_status_code"), _
	                                      sGetOrderStatusDetails(mscsOrderGrp.Value("order_status_code")) _
	                                  )
	
	htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "saved_cy_oadjust_subtotal", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_SUBTOTAL_Text", sLanguage), _
	                                      mscsOrderGrp.Value("saved_cy_oadjust_subtotal"), _
	                                      htmRenderCurrency(mscsOrderGrp.Value("saved_cy_oadjust_subtotal")) _
	                                  )

	sValue = htmRenderCurrency(mscsOrderGrp.Value("saved_cy_shipping_total"))
	If Not IsNull(mscsRandomOrderform.Value("saved_shipping_discount_description")) Then
        sValue = sValue & " (" & mscsRandomOrderform.Value("saved_shipping_discount_description") & ")"
    End If

	htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "saved_cy_shipping_total", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_SHIPTOTAL_Text", sLanguage), _
	                                      sValue, _
	                                      sValue _
	                                  )
	
	
    htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "saved_cy_tax_total", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_TAXTOTAL_Text", sLanguage), _
	                                      mscsOrderGrp.Value("saved_cy_tax_total"), _
	                                      htmRenderCurrency(mscsOrderGrp.Value("saved_cy_tax_total")) _
	                                  )
	
    htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "saved_cy_total_total", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_TOTAL_Text", sLanguage), _
	                                      mscsOrderGrp.Value("saved_cy_total_total"), _
	                                      htmRenderCurrency(mscsOrderGrp.Value("saved_cy_total_total")) _
	                                  )
	
    
	htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "payment_method", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_PAYMENTMETHOD_Text", sLanguage), _
	                                      mscsRandomOrderform.payment_method, _
	                                      sGetPaymentMethodString(mscsRandomOrderform.payment_method) _
	                                  )
	
	htmRow = htmRow  & _
			cellRenderReadOnlyProperty( _
	                                      "billing_address_id", _
	                                      mscsMessageManager.GetMessage("L_FIELD_ORDER_BILLINGADDRESS_Text", sLanguage), _
	                                      sGetAddressName(mscsOrderGrp, mscsRandomOrderform.billing_address_id), _
	                                      sGetAddressName(mscsOrderGrp, mscsRandomOrderform.billing_address_id) _
	                                  )
	
    
	htmTable = RenderTable(htmRow, MSCSSiteStyle.ContentsBodyTable)
    
	htmTable = htmTable & BR & Bold(mscsMessageManager.GetMessage("L_MSG_LINE_ITEMS_Text", sLanguage))
    
	' Display the Order Line item details
	htmRow = rowRenderLineItemHeader()
	
	For Each OrderFormName In mscsOrderGrp.Value.ORDERFORMS
	    For Each Item In mscsOrderGrp.Value.ORDERFORMS(OrderFormName).Items
				arrCell = Array( _
								   htmRenderBlankSpaces(4), _
				                   Item.Value("saved_product_name"), _
				                   Item.Value("description"), _
				                   Item.Value("product_id"), _
				                   Item.Value("product_variant_id"), _
				                   Item.Value("quantity"), _
				                   Item.Value("shipping_method_name"), _
				                   sGetAddressName(mscsOrderGrp, Item.Value("shipping_address_id")), _
				                   Item.Value("_messages"), _
				                   htmRenderCurrency(Item.Value("cy_unit_price")), _
				                   htmRenderCurrency(Item.Value("cy_lineitem_total")) _
				               )
				               
				arrCellAlignment = Array(ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT, _
										 ALIGN_LEFT _
										)
				htmRow = htmRow & _
				         TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsBodyTable, False, "")
		Next
	Next    

	htmTable = htmTable & RenderTable(htmRow, MSCSSiteStyle.ListContentsBodyTable)
	
	htmTable = htmTable & BR & Bold(mscsMessageManager.GetMessage("L_MSG_ADDRESSES_SECTION_Text", sLanguage))
	
	' ** Render the Address Names
	ReDim arrCell(mscsOrderGrp.Value("Addresses").Count - 1)
	ReDim arrCellAlignment(mscsOrderGrp.Value("Addresses").Count - 1)
	i = 0
    For Each addr_id In mscsOrderGrp.Value("Addresses")
        arrCell(i) = mscsMessageManager.GetMessage("L_AddressName_HTMLText", sLanguage) & " = " & Bold(mscsOrderGrp.Value("Addresses").Value(addr_id).address_name)
        arrCellAlignment(i) = ALIGN_LEFT
        i = i + 1
    Next
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsHeaderTable, False, "")
    
    ' ** Render the Addresses
    i = 0 
    For Each addr_id In mscsOrderGrp.Value("Addresses")
        arrCell(i) = sRenderAddress(mscsOrderGrp.Value("Addresses").Value(addr_id))
        arrCellAlignment(i) = ALIGN_LEFT
        i = i + 1
    Next
    htmRow = htmRow & TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsBodyTable, False, "")
					       	    
    htmTable = htmTable & RenderTable(htmRow, MSCSSiteStyle.ListContentsBodyTable)
	
	Set mscsOrderGrp = Nothing
	Set mscsRandomOrderform = Nothing
	tblRenderOrderDetailsContents = htmTable
End Function


' -----------------------------------------------------------------------------
' sRenderAddress
'
' Description:
'	
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sRenderAddress(dictAddress)
	Dim s

	' $$INTLADDR: This illustrates how to use an alternative address format (in this case Japanese) 
	'If dictConfig.i_SiteDefaultLocale = 1041 Then
	'	' Use Japanese Address Format
	'	s = s & mscsMessageManager.GetMessage("L_Country_HTMLText", sLanguage) & " = " & MSCSAppConfig.GetCountryNameFromCountryCode(dictAddress.country_code) & BR
	'	s = s & mscsMessageManager.GetMessage("L_PostalCode_HTMLText", sLanguage) & " = " & dictAddress.postal_code & BR
	'	If Not IsNull(dictAddress.region_code) Then s = s & mscsMessageManager.GetMessage("L_Region_HTMLText", sLanguage) & " = " & dictAddress.region_code & BR
	'	s = s & mscsMessageManager.GetMessage("L_City_HTMLText", sLanguage) & " = " & dictAddress.city & BR
	'	s = s & mscsMessageManager.GetMessage("L_AddressLine1_HTMLText", sLanguage) & " = " & dictAddress.address_line1 & BR
	'	If dictAddress.address_line2 <> "" Then s = s & mscsMessageManager.GetMessage("L_AddressLine2_HTMLText", sLanguage) & " = " & dictAddress.address_line2 & BR
	'	s = s & mscsMessageManager.GetMessage("L_LastName_HTMLText", sLanguage) & " = " & dictAddress.last_name & BR
	'	s = s & mscsMessageManager.GetMessage("L_FirstName_HTMLText", sLanguage) & " = " & dictAddress.first_name & BR
	'Else
		s = s & mscsMessageManager.GetMessage("L_LastName_HTMLText", sLanguage) & " = " & dictAddress.last_name & BR
		s = s & mscsMessageManager.GetMessage("L_FirstName_HTMLText", sLanguage) & " = " & dictAddress.first_name & BR
		s = s & mscsMessageManager.GetMessage("L_AddressLine1_HTMLText", sLanguage) & " = " & dictAddress.address_line1 & BR
		If dictAddress.address_line2 <> "" Then s = s & mscsMessageManager.GetMessage("L_AddressLine2_HTMLText", sLanguage) & " = " & dictAddress.address_line2 & BR
		s = s & mscsMessageManager.GetMessage("L_City_HTMLText", sLanguage) & " = " & dictAddress.city & BR
		If Not IsNull(dictAddress.region_code) Then s = s & mscsMessageManager.GetMessage("L_Region_HTMLText", sLanguage) & " = " & dictAddress.region_code & BR
		s = s & mscsMessageManager.GetMessage("L_PostalCode_HTMLText", sLanguage) & " = " & dictAddress.postal_code & BR
		s = s & mscsMessageManager.GetMessage("L_Country_HTMLText", sLanguage) & " = " & MSCSAppConfig.GetCountryNameFromCountryCode(dictAddress.country_code) & BR
	'End If
	sRenderAddress = s
End Function


' -----------------------------------------------------------------------------
' rowRenderLineItemHeader
'
' Description:
'	Function to display order line item header
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderLineItemHeader()
    Dim htmRow
    Dim arrCell, arrCellAlignment
    Dim i

    arrCell = Array( _
						htmRenderBlankSpaces(4), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_NAME_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_DESCRIPTION_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_PRODUCTID_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_VARIANTID_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_QUANTITY_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_SHIPMETHOD_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_SHIPADDRESS_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_MESSAGES_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_UNITPRICE_Text", sLanguage), _
						mscsMessageManager.GetMessage("L_COLUMN_LINEITEMS_LINETOTAL_Text", sLanguage) _
                   )
    
    ReDim arrCellAlignment(UBound(arrCell))
    For i = LBound(arrCell) To UBound(arrCell)
        arrCellAlignment(i) = ALIGN_LEFT
    Next
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsHeaderTable, True, "")

    rowRenderLineItemHeader = htmRow
End Function
%>