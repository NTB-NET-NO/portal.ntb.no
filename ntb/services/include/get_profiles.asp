<%
' =============================================================================
' get_profiles.asp
' Profile reading routines for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' rsGetProfilesList
'
' Description:
'	Function to get the list of users depending upon the filter criteria
'
' Parameters:
'
' Returns:
'
' Notes :
' /!\ Uses ADO to execute SQL string directly
' -----------------------------------------------------------------------------
Function rsGetProfilesList(ByVal dictSearchOptions, ByRef iTotalRecords)
    Dim oProperty
    Dim sSQLStmt, sOrderByClause, sTableName, sOrgIDWhereClause
    Dim org_name    
    Dim rsProfiles    
    Dim bFieldSelected, bFirstProperty   
    Dim listProfileSchema
    
    ' Initialize
    bFirstProperty = True
    bFieldSelected = True
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
        
    ' Determine the Profile table name and default WHERE clause for the SQL statement
    '     passed to the commerce provider to retreive profiles
    Call GetTableNameAndWhereClause(sTableName, sOrgIDWhereClause)

    ' Construct the default SELECT SQL statement
    sSQLStmt = "SELECT "
    For Each oProperty In listProfileSchema
        If Not oProperty.IsGroup Then
            If oProperty.Attributes.IsPrimaryKey Or _
               oProperty.Attributes.IsJoinKey Or _
               oProperty.Attributes.IsUniqueKey Then
               If bFirstProperty Then
                   sSQLStmt = sSQLStmt & "[" & oProperty.QualifiedName & "]"
                   bFirstProperty = False
               Else
                   sSQLStmt = sSQLStmt & ", [" & oProperty.QualifiedName & "]"
               End If
            ElseIf StrComp(oProperty.Attributes.ShowInList, "1", vbTextCompare) = 0 Then
                If bFirstProperty Then
                   bFirstProperty = False
                   sSQLStmt = sSQLStmt & "[" & oProperty.QualifiedName & "]"
                Else
                   sSQLStmt = sSQLStmt & ", [" & oProperty.QualifiedName & "]"
               End If
            End If
        End If
    Next
    Set oProperty = Nothing
    Set listProfileSchema = Nothing
    
    ' Add FROM table clause, the first WHERE clause to the SQL query, 
    ' WHERE clauses depending upon user search options
    sSQLStmt = sSQLStmt & " FROM " & sTableName & sOrgIDWhereClause & sConstructWHEREclause(bFieldSelected, dictSearchOptions)

	' Could optionally add an ORDER BY clause here
	sSQLStmt = sSQLStmt & " ORDER BY " & "[" & GetQualifiedName(GENERAL_INFO_GROUP, "logon_name") & "]"

    ' Get the profile recorset
    Set rsProfiles = Server.CreateObject("ADODB.Recordset")
    rsProfiles.Open sSQLStmt, MSCSAdoConnection

    ' Check if the recorset returned is empty
    If rsProfiles.EOF And rsProfiles.BOF Then 
        iTotalRecords = 0
    End If    
    
    Set rsGetProfilesList = rsProfiles
End Function


' -----------------------------------------------------------------------------
' rsGetProfileDetails
'
' Description:
'	Function to retrieve profile details
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetProfileDetails(ByVal sProfileIDs)    
    Dim rsProfile, sProfileType
    
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    
    Select Case sProfileType
        Case PROFILE_TYPE_USER
             Set rsProfile = rsGetUserDetails(sProfileIDs)
        Case PROFILE_TYPE_ORG
             Set rsProfile = rsGetOrgDetails(sProfileIDs)    
        Case Else
			Err.Raise E_NOTIMPL
    End Select 
    
    Set rsGetProfileDetails = rsProfile 
    Set rsProfile = Nothing
End Function


' -----------------------------------------------------------------------------
' rsGetUserDetails
'
' Description:
'	Function to get the details for a particular user
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetUserDetails(ByVal sID)
    Dim rsUser
	If sID = m_UserID Then
		Set rsUser = GetCurrentUserProfile()
	Else
		Set rsUser = rsGetProfileByKey( _
		                                  FIELD_USER_ID, _
		                                  sID, _
		                                  PROFILE_TYPE_USER, _
		                                  True _
		                              )
	End If
    Set rsGetUserDetails = rsUser    
End Function


' -----------------------------------------------------------------------------
' GetUserPropertyFromID
'
' Description:
'	Function to get a user profile's property value
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetUserPropertyFromID(ByVal sPropertyName, ByVal sID)
    Dim rsUser
    Dim varPropertyValue

    Set rsUser = rsGetUserDetails(sID)
    
    If rsUser Is Nothing Then
	    varPropertyValue = Null
    Else
        varPropertyValue = GetAttributeValueFromProfile(rsUser, sPropertyName)
    End If
    
    GetUserPropertyFromID = varPropertyValue
End Function


' -----------------------------------------------------------------------------
' sGetUserFullName
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetUserFullName(ByVal sID)
    Dim sUserFullName, sUserLastName, sUserFirstName
    
    sUserLastName = GetUserPropertyFromID(FIELD_USER_LAST_NAME, sID)
    sUserFirstName = GetUserPropertyFromID(FIELD_USER_FIRST_NAME, sID)
        
    If IsNull(sUserFirstName) Then
        If Not IsNull(sUserLastName) Then 
            sUserFullName = sUserLastName
        Else
            sUserFullName = ""
        End If                
    
    ElseIf IsNull(sUserLastName) Then
        sUserFullName = sUserFirstName
    
    Else
        sUserFullName =  sUserLastName & " " & sUserFirstName
    End If
    
    sGetUserFullName = sUserFullName    
End Function


' -----------------------------------------------------------------------------
' sGetUserLoginName
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetUserLoginName(ByVal sID)
	Dim sUserLoginName

    sUserLoginName = GetUserPropertyFromID(FIELD_USER_LOGIN_NAME, sID)
    If IsNull(sUserLoginName) Then sUserLoginName = ""

	sGetUserLoginName = sUserLoginName
End Function


' -----------------------------------------------------------------------------
' rsGetOrgDetails
'
' Description:
'	Function to get organization details
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetOrgDetails(ByVal sID)   
    Dim rsOrg
        
    Set rsOrg = rsGetProfileByKey( _
                                     FIELD_ORG_ORG_ID, _
                                     sID, _
                                     PROFILE_TYPE_ORG, _
                                     True _
                                 )
    Set rsGetOrgDetails = rsOrg
End Function


' -----------------------------------------------------------------------------
' GetOrgAttributeFromID
'
' Description:
'	Function to get an organization profile's property value
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetOrgAttributeFromID(ByVal sAttributeName, ByVal sID)
    
    Dim rsOrg
    Dim sAttributeValue
	    
    Set rsOrg = rsGetProfileByKey( _
                                     FIELD_ORG_ORG_ID, _
                                     sID, _
                                     PROFILE_TYPE_ORG, _
                                     True _
                                 )
    If rsOrg Is Nothing Then
        sAttributeValue = Null
    Else
        sAttributeValue = GetAttributeValueFromProfile(rsOrg, sAttributeName)
    End If
    
    GetOrgAttributeFromID = sAttributeValue
End Function


' -----------------------------------------------------------------------------
' GetAttributeValueFromProfile
'
' Description:
'	Function to retreive a property value from any profile
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAttributeValueFromProfile(ByVal objProfile, ByVal sPropertyName)
    Dim SubProperty
    Dim RootProperty
    Dim sFullFieldName

    For Each RootProperty In objProfile.Fields
        If (typename(RootProperty.Value) = "Fields") Then
            For Each SubProperty In RootProperty.Value
                If SubProperty.Name = sPropertyName Then
                    sFullFieldName = RootProperty.Name & "." & sPropertyName
                    GetAttributeValueFromProfile = objProfile(sFullFieldName).Value
                    Exit Function
                End If
            Next
        Else
            If RootProperty.Name = sPropertyName Then
                GetAttributeValueFromProfile = RootProperty.Value
                Exit Function
            End If
        End If
    Next
    
    Set SubProperty = Nothing
    Set RootProperty = Nothing    
End Function
%>  