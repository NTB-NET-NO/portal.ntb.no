<%
' =============================================================================
' render_edit_blank.asp
' Common rendering routines for (blank) editing pages
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' $$ This file's functions have not yet been separated for
' $$ business and presentation logic.

' -----------------------------------------------------------------------------
' htmRenderBlankEditPage
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderBlankEditPage(ByVal sAction, _
                                ByVal sSubAction, _
                                ByVal sProfileIDs, _
                                ByVal sStatusField, _
                                ByVal iStatusID, _
                                ByVal bValidPostedValues, _
                                ByVal bDisplayPostedValues, _
                                ByVal listProfileSchema)
    Dim url    
    Dim htmForm, htmTable, htmContents, htmStatusRows
    Dim rowStatus, rowHeader
    Dim formContent
    Dim sPageHeading
    Dim iProfileCount

    ' Get the HTML for the current page
    rowHeader = rowRenderHeader()
    formContent = formRenderBlankEditPageBody(sAction, _
                                              sSubAction, _
                                              sProfileIDs, _                                                 
                                              bValidPostedValues, _
                                              bDisplayPostedValues, _
                                              listProfileSchema)
                                             
    rowStatus = rowRenderStatusInfo(sStatusField, iStatusID, iProfileCount, True)
    
    ' Construct the HTML for the current page    
    htmStatusRows = rowHeader & rowStatus    
    htmTable = RenderTable(htmStatusRows, MSCSSiteStyle.StatusTable)
    url = GenerateURL(MSCSSitePages.HelpPage, Array(SOURCE_PAGE), Array(sThisPage))
    htmForm = RenderForm(url, htmTable, HTTP_POST)
    htmContents = htmForm
    
    sPageHeading = sGetPageHeading(sThisPage)
    htmContents = htmContents & _
                  RenderText(sPageHeading, MSCSSiteStyle.Title)
    htmContents = htmContents & formContent    

    htmContents = RenderText(htmContents, MSCSSiteStyle.Body)
    htmRenderBlankEditPage = htmContents
End Function


' -----------------------------------------------------------------------------
' formRenderBlankEditPageBody
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderBlankEditPageBody(ByVal sAction, _
                                     ByVal sSubAction, _
                                     ByVal sProfileIDs, _                                        
                                     ByVal bValidPostedValues, _
                                     ByVal bDisplayPostedValues, _
                                     ByVal listProfileSchema)
    Dim url
    Dim htmForm, htmTable, htmContent, htmTaskButtons
    Dim sSourcePage, sPropertyName
    
    htmTaskButtons = tableRenderEditPageTasks(sAction, sSubAction, sProfileIDs)
    htmTable = tableRenderBlankProfile( _
                                          sAction, _
                                          sSubAction, _                                          
                                          sProfileIDs, _
                                          bValidPostedValues, _
                                          bDisplayPostedValues, _
                                          listProfileSchema _
                                      )    
    htmContent = htmTaskButtons & htmTable        
    htmContent = htmContent & RenderHiddenField(ACTION_TYPE, sAction)
            
    htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, ADD_ACTION)
    
    url = GenerateURL(sThisPage, Array(), Array())
    htmForm = RenderForm(url, htmContent, HTTP_POST)
        
    formRenderBlankEditPageBody = htmForm
End Function


' -----------------------------------------------------------------------------
' tableRenderBlankProfile
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :	READONLY properties are not initially rendered for two reasons:
'
'			- in case of COPYing an existing profile those properties
'			might be irrelevant for the new one (e.g.: ChangedBy, DateLastChanged,
'			DateCreated);
'			- in case of ADDing a new profile we do not have values associated w/
'			such a property.
'
' -----------------------------------------------------------------------------
Function tableRenderBlankProfile(ByVal sAction, _
                                 ByVal sSubAction, _                                    
                                 ByVal sProfileIDs, _
                                 ByVal bValidPostedValues, _
                                 ByVal bDisplayPostedValues, _
                                 ByVal listProfileSchema)

    Dim htm, htmRow, htmGroupHeader, htmTable
    Dim bFirstInGroup, bSupported, bRenderProperty
    Dim arrCell, arrCellAlignment
    Dim rsProfile
    Dim oProperty
    Dim iUserClass
    Dim sAccessType, sCommonValue, sDisplayValue

    ' Get the property access class for the current user, 
    ' e.g. DELEGATED_ADMIN_CLASS, REGISTERED_USER_CLASS or GUEST_USER_CLASS
    iUserClass = iGetCurrentUserPropertyAccessClass()

    ' Loop through the list of properties for particular profile    
    For Each oProperty In listProfileSchema

        ' Initialize the variable to display the property
        bRenderProperty = True

        ' Check if dictionary is a group or a property
        If oProperty.IsGroup Then
            htmGroupHeader = rowRenderEditSheetBlankLine()
            htmGroupHeader = htmGroupHeader & rowRenderPropertyGroup(oProperty)
			bFirstInGroup = True
        Else
            ' Render property
            ' Check if this property is supported by the site feature
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)            

            ' Render the property only if the site supports the corresponding
            '     feature
            If bSupported Then
                ' Check the user access for this property
                sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)

                Select Case sAccessType
                Case ACCESS_READONLY
					' Render the read-only properties if sAction is not COPY_ACTION
					' Also don't render the multivalue properties
					If (StrComp(sAction, COPY_ACTION, vbTextCompare) <> 0) And _
					   (oProperty.Attributes.IsMultivalued = False) Then

						htm = rowRenderReadOnlyProperty(sAction, _
								    sSubAction, oProperty, bValidPostedValues, _
									sProfileIDs, rsProfile)
						' Avoid displaying a group header only
						' (htm = Empty when creating a New User profile)
	                    If (bFirstInGroup = True) And (htm <> "") Then
							htmRow = htmRow & htmGroupHeader
							bFirstInGroup = False
						End If

						htmRow = htmRow & htm
					End If

                Case ACCESS_READWRITE
                    ' don't render multivalued properties
                    If (oProperty.Attributes.IsMultivalued = False) Or _
						(oProperty.Name = FIELD_USER_PASSWORD) Then

	                    If bFirstInGroup = True Then
							htmRow = htmRow & htmGroupHeader
							bFirstInGroup = False
						End If

                        htmRow = htmRow & _
                                 rowRenderBlankProperty(sAction, _
                                                        sSubAction, _
                                                        oProperty, _
                                                        listProfileSchema, _
                                                        bValidPostedValues, _                                                               
                                                        sProfileIDs, _
                                                        bDisplayPostedValues)
                    End If
                End Select
            End If
        End If            
    Next    
    htmTable = RenderTable(htmRow, MSCSSiteStyle.ContentsBodyTable)
    
    Set oProperty = Nothing
    tableRenderBlankProfile = htmTable    
End Function


' -----------------------------------------------------------------------------
' rowRenderBlankProperty
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderBlankProperty(ByVal sAction, _
                                ByVal sSubAction, _
                                ByVal oProperty, _
                                ByVal listProfileSchema, _
                                ByVal bValidPostedValues, _                                   
                                ByVal sProfileIDs, _
                                ByVal bDisplayPostedValues)
    Dim i
    Dim url
    Dim htmTip, htmRow
    Dim sErrStr, sLastName, sFirstName, sStateCode, sCountryCode
    Dim sPropertyName, sDisplayValue
    Dim arrCell(3), arrCellAlignment(3)
    Dim mscsProperty
    Dim varPropertyValue    
    
    ' Every property is displayed in a different HTML TR tag and has following columns
    '     1.)Column for displaying Reqired gif Or check box(for muli-edit)
    '     2.)Column for displaying the "Display Name" of the property
    '     3.)Column for displaying the property value in either TEXTBOX, COMBOBOX or as
    '        a SUBMIT button.
    
    ' Column 1 : Display either the required gif/blank/check box for the property
    ' Add details for the property
    arrCell(0) = sRenderHTMLPreFixForProperty(sAction, sSubAction, oProperty, sProfileIDs)
    
    ' Column 2: Display the "Display Name" for the property
    arrCell(1) = oProperty.Attributes.DisplayName & "&nbsp;&nbsp;&nbsp;&nbsp;"
        
    ' Column 3: Display the property value in either TEXTBOX, COMBOBOX or as 
    '           SUBMIT button
    ' First get the value for the property
    varPropertyValue = Null
    If Not bValidPostedValues Then
        ' Get the property dictionary from the list of properties
        varPropertyValue = varGetValueFromPropertyDictionaries(oProperty, _
                                                                listProfileSchema)
        
    ' Get the value from the Request object    
    ElseIf bDisplayPostedValues Then        
        varPropertyValue = varGetValueFromRequestObject(oProperty)
    End If

    arrCell(2) = colRenderProperty(sAction, _
                                   sSubAction, _
                                   oProperty, _
                                   sProfileIDs, _                                     
                                   varPropertyValue)
    arrCell(2) = arrCell(2) & "&nbsp;&nbsp;&nbsp;&nbsp;"

    ' General the HTML TR tag for the property
    arrCellAlignment(0) = ALIGN_LEFT
    arrCellAlignment(1) = ALIGN_LEFT
    arrCellAlignment(2) = ALIGN_LEFT
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")
    
    ' Add a row before displaying the property value, for displaying TIP for the 
    '     property which failed validation
    If Not bValidPostedValues Then 
        htmTip = rowRenderPropertyTip(oProperty, listProfileSchema)    
    End If

    ' Add the Property Tip and the Property TR tags    
    htmRow = htmRow & htmTip
    
    rowRenderBlankProperty = htmRow
End Function
%>