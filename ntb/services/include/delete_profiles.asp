<%
' =============================================================================
' delete_profiles.asp
' Delete profiles code for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' DeleteAllProfiles
'
' Description:
'	Sub to delete all the selected profiles. This sub will delete all the
'   profiles returned by the search criteria in the corresponding LIST
'   page.
'
' Parameters:
'
' Returns:
'
' Notes :
' /!\ This function uses ADO to access data directly
' -----------------------------------------------------------------------------
Sub DeleteAllProfiles(ByVal dictSearchOptions)
    Dim sSQLStmt, sTableName, sOrgIDWhereClause, sWhereClauseForCurrentUser
    Dim rsProfile
    Dim bFieldSelected    
    
    bFieldSelected = True    
    
    ' Determine the Profile table name and default WHERE clause for the SQL statement
    '     passed to the commerce provider to retreive profiles
    Call GetTableNameAndWhereClause(sTableName, sOrgIDWhereClause)
    
    ' Construct the default SQL statement
    sSQLStmt = "DELETE FROM " & sTableName

    ' Add the first WHERE clause to the SQL query
    sSQLStmt = sSQLStmt & sOrgIDWhereClause
    
    ' Add the WHERE clause so that the delegated admin cannot delete 
    '     his own profile
    sWhereClauseForCurrentUser = sGetWhereClauseForCurrentUser(bFieldSelected)
    sSQLStmt = sSQLStmt & sWhereClauseForCurrentUser
    
    ' Add WHERE clause depending upon user search options
    sSQLStmt = sSQLStmt & sConstructWHEREclause(bFieldSelected, dictSearchOptions)
                    
    ' Get the profile recorset
    Set rsProfile = Server.CreateObject("ADODB.Recordset")
    rsProfile.Open sSQLStmt, MSCSAdoConnection
End Sub


' -----------------------------------------------------------------------------
' sGetWhereClauseForCurrentUser
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetWhereClauseForCurrentUser(bFieldSelected)    
    Dim sWhereClause
    Dim mscsUserProfile
    
    Set mscsUserProfile = EnsureUserProfile()
    
	sWhereClause = " AND [" & GetQualifiedName(GENERAL_INFO_GROUP, USER_ID) & "] <>" & _
                   "'" & _
                           mscsUserProfile(GetQualifiedName(GENERAL_INFO_GROUP, USER_ID)).Value & _
                   "' "
    sGetWhereClauseForCurrentUser = sWhereClause
End Function


' -----------------------------------------------------------------------------
' DeleteSelectedProfiles
'
' Description:
'	Sub to delete selected profiles. This function is used to delete one or more
'   profiles.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub DeleteSelectedProfiles(ByVal sSubAction, ByVal sProfileIDs)
    Dim i, iProfileCount
    Dim arrProfileIDs
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
        
    If iProfileCount > 1 Then
       arrProfileIDs = Split(sProfileIDs, PROFILE_ID_SEPARATOR, -1, 1)
       For i = LBound(arrProfileIDs) To UBound(arrProfileIDs)
           Call DeleteSingleProfile(arrProfileIDs(i))
       Next
    ElseIf iProfileCount = 1 Then
       Call DeleteSingleProfile(sProfileIDs)
    End If    
End Sub


' -----------------------------------------------------------------------------
' DeleteSingleProfile
'
' Description:
'	Function to delete any one profile
'
' Parameters:
'	sProfileID					- ID of profile to delete
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub DeleteSingleProfile(ByVal sProfileID)    
    Dim sProfileType
    
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    Select Case sProfileType
        Case PROFILE_TYPE_USER
             Call DeleteRegisteredUser(sProfileID)
        Case Else
			Err.Raise E_NOTIMPL
    End Select
End Sub    


' -----------------------------------------------------------------------------
' DeleteRegisteredUser
'
' Description:
'	Sub to delete a specific registered user profile
'
' Parameters:
'	sID							- ID of profile to delete
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub DeleteRegisteredUser(ByVal sID)
    Dim sCurrentUserID    
    Dim mscsUserProfile
    
    ' Prevent delegated admins from deleting their own profile
    If StrComp(sID, m_UserID, vbTextCompare) <> 0 Then 
        Call MSCSProfileService.DeleteProfileByKey( _
                                                      PROFILE_KEY_USERID, _
                                                      sID, _
                                                      PROFILE_TYPE_USER _
                                                  )
    End If
End Sub
%>