<%
' =============================================================================
' render_messagebox.asp
' Messagebox rendering functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' htmRenderMessageBoxPage
'
' Description:
'	Function to display message box page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderMessageBoxPage(ByVal sAction, ByVal sSubAction, _
                                 ByVal sActionPage, ByVal sProfileIDs)
    Dim htm
    Dim bDirty
    Dim iProfileCount
    Dim sSelectFieldName
    
    bDirty = False
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    sSelectFieldName = sGetSelectActionPropertyName(sAction)
        
    Rem Check if the user is adding a profile                  
    If iProfileCount = 0 Then                
       bDirty = IsPropertyValueAdded()                
    ElseIf iProfileCount = 1 Then
       bDirty = bProfileHasChanged(sProfileIDs)
    End If
    
    ' Redirect to the ChangePassword page, if the profile property values have
    ' not been changed and pass the source page in the URL
    If bDirty Then
         Select Case sAction
            Case CHANGE_ACTION, _
                 SELECT_ACTION
                htm = htmRenderMessageBoxForSelect( _
                                                      sAction, _
                                                      sSelectFieldName, _
                                                      sProfileIDs _
                                                  )
        End Select                                                               
    Else       
        Response.Redirect(sActionPage)
    End If
    
    htmRenderMessageBoxPage = htm
End Function


' -----------------------------------------------------------------------------
' htmRenderMessageBoxForSelect
'
' Description:
'	Function  to display message box page when "select" action is selected
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderMessageBoxForSelect(ByVal sAction, ByVal sSelectFieldName, ByVal sProfileIDs)
    Dim arrBody
    Dim arrButtons(2)
    Dim arrDestination(2)
    Dim arrSaveFormPosted(2)    
    Dim url    
    Dim htmMsgBox
       
    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SELECT_FIELD_NAME, SUB_ACTION_TYPE, PREVIOUS_ACTION, GUIDS), Array(sAction, sSelectFieldName, MSGBOX_ACTION_YES, GetRequestString(ACTION_TYPE, Null), sProfileIDs))
    arrButtons(0) = mscsMessageManager.GetMessage("L_MSGBOX_YESACTION_BUTTON_Text", sLanguage)
    arrDestination(0) = url
    arrSaveFormPosted(0) = True
    
    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE, SELECT_FIELD_NAME, PREVIOUS_ACTION, GUIDS), Array(sAction, MSGBOX_ACTION_NO, sSelectFieldName, GetRequestString(ACTION_TYPE, Null), sProfileIDs))

    arrButtons(1) = mscsMessageManager.GetMessage("L_MSGBOX_NOACTION_BUTTON_Text", sLanguage)
    arrDestination(1) = url
    arrSaveFormPosted(1) = True
    
    htmMsgBox = tableRenderMessageBoxPage( _
                                           mscsMessageManager.GetMessage("L_MSG_HEADING_SAVE_Text", sLanguage), _
                                           mscsMessageManager.GetMessage("L_MSG_MSGBOX_SELECT_Text", sLanguage), _
                                           False, _
                                           Null, _
                                           Null, _
                                           arrButtons, _
                                           arrDestination, _
                                           arrSaveFormPosted _
                                        )
     htmRenderMessageBoxForSelect = htmMsgBox
End Function


' -----------------------------------------------------------------------------
' htmRenderMessageBoxForDelete
'
' Description:
'	Function to display message box page while deleting entity record/s
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderMessageBoxForDelete()
    Dim arrBody, arrButtons(2), arrDestination(2), arrSaveFormPosted(2)
    Dim arrNames
    Dim url
    Dim htmMsgBox
    Dim bNumberList
    Dim i
    
    bNumberList = False
   
    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE), Array(DELETE_ACTION, MSGBOX_ACTION_YES))
    arrButtons(0) = mscsMessageManager.GetMessage("L_MSGBOX_YESACTION_BUTTON_Text", sLanguage)
    arrDestination(0) = url
    arrSaveFormPosted(0) = True

    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE), Array(DELETE_ACTION, MSGBOX_ACTION_NO))
    arrButtons(1) = mscsMessageManager.GetMessage("L_MSGBOX_NOACTION_BUTTON_Text", sLanguage)
    arrDestination(1) = url
    arrSaveFormPosted(1) = False
    
    htmMsgBox = tableRenderMessageBoxPage( _
                                             mscsMessageManager.GetMessage("L_MSG_TITLE_DELETE_Text", sLanguage), _
                                             mscsMessageManager.GetMessage("L_MSG_CONFIRM_DELETE_Text", sLanguage), _
                                             bNumberList, _
                                             Array(mscsMessageManager.GetMessage("L_MSG_NOTES_DELETE_Text", sLanguage)), _
                                             Null, _
                                             arrButtons, _
                                             arrDestination, _
                                             arrSaveFormPosted _
                                         )
    htmRenderMessageBoxForDelete = htmMsgBox
End Function


' -----------------------------------------------------------------------------
' htmRenderMessageBoxForDeleteAll
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderMessageBoxForDeleteAll()
    Dim arrBody, arrButtons(2), arrDestination(2), arrSaveFormPosted(2)
    Dim arrNames
    Dim url
    Dim htmMsgBox
    Dim bNumberList
    Dim i
    
    bNumberList = True

    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE), Array(DELETEALL_ACTION, MSGBOX_ACTION_YES))
    arrButtons(0) = mscsMessageManager.GetMessage("L_MSGBOX_YESACTION_BUTTON_Text", sLanguage)
    arrDestination(0) = url
    arrSaveFormPosted(0) = False

    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE), Array(DELETEALL_ACTION, MSGBOX_ACTION_NO))
    arrButtons(1) = mscsMessageManager.GetMessage("L_MSGBOX_NOACTION_BUTTON_Text", sLanguage)
    arrDestination(1) = url
    arrSaveFormPosted(1) = False
    
    htmMsgBox = tableRenderMessageBoxPage( _
                                             mscsMessageManager.GetMessage("L_MSG_TITLE_DELETE_Text", sLanguage), _
                                             mscsMessageManager.GetMessage("L_MSG_CONFIRM_DELETEALL_Text", sLanguage), _
                                             False, _
                                             Array(mscsMessageManager.GetMessage("L_MSG_NOTES_DELETE_Text", sLanguage)), _
                                             Null, _
                                             arrButtons, _
                                             arrDestination, _
                                             arrSaveFormPosted _
                                         )
    htmRenderMessageBoxForDeleteAll = htmMsgBox
End Function


' -----------------------------------------------------------------------------
' htmRenderMessageBoxForSave
'
' Description:
'	Function to display message box page with save options
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderMessageBoxForSave(ByVal sAction, ByVal sProfileIDs)
    Dim arrBody, arrButtons(2), arrDestination(2), arrSaveFormPosted(2)
    Dim url
    Dim htmMsgBox
               
    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE, PREVIOUS_ACTION, GUIDS), Array(sAction, MSGBOX_ACTION_YES, GetRequestString(ACTION_TYPE, Null), sProfileIDs))
    arrButtons(0) = mscsMessageManager.GetMessage("L_MSGBOX_YESACTION_BUTTON_Text", sLanguage)
    arrDestination(0) = url
    arrSaveFormPosted(0) = True
    

    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE, PREVIOUS_ACTION, GUIDS), Array(sAction, MSGBOX_ACTION_NO, GetRequestString(ACTION_TYPE, Null), sProfileIDs))
    arrButtons(1) = mscsMessageManager.GetMessage("L_MSGBOX_NOACTION_BUTTON_Text", sLanguage)
    arrDestination(1) = url
    arrSaveFormPosted(1) = True
    
    htmMsgBox = tableRenderMessageBoxPage( _
                                             mscsMessageManager.GetMessage("L_MSG_HEADING_SAVE_Text", sLanguage), _
                                             mscsMessageManager.GetMessage("L_MSG_MSGBOX_SAVE_Text", sLanguage), _
                                             False, _
                                             Null, _
                                             Null, _
                                             arrButtons, _
                                             arrDestination, _
                                             arrSaveFormPosted _
                                        )
    
    htmRenderMessageBoxForSave = htmMsgBox
End Function


' -----------------------------------------------------------------------------
' htmRenderMessageBoxForCancel
'
' Description:
'	Function to display message box page for CANCEL action
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderMessageBoxForCancel(ByVal sAction, ByVal sProfileIDs)
    Dim arrBody, arrButtons(2), arrDestination(2), arrSaveFormPosted(2)
    Dim url
    Dim htmMsgBox
               
    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE, PREVIOUS_ACTION, GUIDS), Array(sAction, MSGBOX_ACTION_YES, GetRequestString(ACTION_TYPE, Null), sProfileIDs))
    arrButtons(0) = mscsMessageManager.GetMessage("L_MSGBOX_YESACTION_BUTTON_Text", sLanguage)
    arrDestination(0) = url
    arrSaveFormPosted(0) = True

    url = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE, PREVIOUS_ACTION, GUIDS), Array(sAction, MSGBOX_ACTION_NO, GetRequestString(ACTION_TYPE, Null), sProfileIDs))
    arrButtons(1) = mscsMessageManager.GetMessage("L_MSGBOX_NOACTION_BUTTON_Text", sLanguage)
    arrDestination(1) = url
    arrSaveFormPosted(1) = True
    
    htmMsgBox = tableRenderMessageBoxPage( _
                                             mscsMessageManager.GetMessage("L_MSG_HEADING_SAVE_Text", sLanguage), _
                                             mscsMessageManager.GetMessage("L_MSG_MSGBOX_CANCEL_Text", sLanguage), _
                                             False, _
                                             Null, _
                                             Null, _
                                             arrButtons, _
                                             arrDestination, _
                                             arrSaveFormPosted _
                                        )
    
    htmRenderMessageBoxForCancel = htmMsgBox
End Function


' -----------------------------------------------------------------------------
' tableRenderMessageBoxPage
'
' Description:
'	Function to display message box page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderMessageBoxPage(ByVal sTitle, _
                                   ByVal sHeading, _
                                   ByVal bNumberList, _
                                   ByVal arrBody, _
                                   ByVal sFooter, _
                                   ByVal arrButtons, _
                                   ByVal arrDestination, _
                                   ByVal arrSaveFormPosted)

    Dim htmTable, htmRow, htmColumn, htmForm, htmContent
    Dim arrCell, arrCellAlignment
    Dim url
    Dim iArrIndex, iCount
    Dim sItem, sFormValue
        
    ' Display Title
    arrCell = Array(Bold(sTitle))
    arrCellAlignment = Array(ALIGN_LEFT)
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.MsgBoxTitleTable, False, "")
    
    ' Display the heading
    If Not IsNull(sHeading) Then
        arrCell = Array(sHeading)
        arrCellAlignment = Array(ALIGN_LEFT)
        htmRow = htmRow & TRTag(arrCell, arrCellAlignment, _
                          MSCSSiteStyle.MsgBoxHeadingTable, _
                          False, "")
    End If

    ' Display body for the message box
    If Not IsNull(arrBody) Then
        htmRow = htmRow & rowRenderMessageBoxPageContents(arrBody, bNumberList)        
    End If
    
    ' Display the footer
    If Not IsNull(sFooter) Then
        arrCell = Array(sFooter)
        arrCellAlignment = Array(ALIGN_LEFT)
        htmRow = htmRow & TRTag(arrCell, arrCellAlignment, _
			MSCSSiteStyle.MsgBoxFooterTable, False, "")
    End If
    htmTable = RenderTable(htmRow, MSCSSiteStyle.MsgBoxBodyTable)
    
    ' Display the buttons
    htmColumn = ""
    tableRenderMessageBoxPage = htmTable & tableRenderMessageBoxPageButtons(arrButtons, _
                           arrDestination, arrSaveFormPosted)
End Function


' -----------------------------------------------------------------------------
' rowRenderMessageBoxPageContents
'
' Description:
'	Function to display the MessageBox page's contents
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderMessageBoxPageContents(ByVal arrBody, ByVal bNumberList)
    Dim iArrIndex, iCount
    Dim htmColumn, htmRow
    Dim arrCell, arrCellAlignment
    
    iCount = 1
    For iArrIndex = LBound(arrBody) To UBound(arrBody)
         If bNumberList Then
            htmColumn = CStr(iCount)
            iCount = iCount + 1
            htmColumn = htmColumn & htmRenderBlankSpaces(4)
         End If
         htmColumn = htmColumn & arrBody(iArrIndex)
         arrCell = Array(htmColumn)
         arrCellAlignment = Array(ALIGN_LEFT)
         htmRow = TRTag(arrCell, arrCellAlignment, _
                   MSCSSiteStyle.MsgBoxBodyTable, False, "")
    Next
    
    rowRenderMessageBoxPageContents = htmRow
End Function


' -----------------------------------------------------------------------------
' tableRenderMessageBoxPageButtons
'
' Description:
'	Function to render Message box page submit buttons
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderMessageBoxPageButtons(ByVal arrButtons, _
           ByVal arrDestination, ByVal arrSaveFormPosted)
    Dim arrCell, arrCellAlignment
    Dim iArrIndex
    Dim htmContent, htmRow, htmTable
    Dim url
    Dim sItem, sBtnText, sFormValue
    
    ReDim arrCell(UBound(arrButtons))
    ReDim arrCellAlignment(UBound(arrButtons))
    
    For iArrIndex = LBound(arrButtons) To UBound(arrButtons) - 1 
        htmContent = ""
        url = arrDestination(iArrIndex)
        If arrSaveFormPosted(iArrIndex) = True Then
            For Each sItem In Request.Form
                sFormValue = GetRequestString(sItem, Null)
                htmContent = htmContent & RenderHiddenField(sItem, sFormValue)
            Next
        End If
        
        sBtnText = arrButtons(iArrIndex)
        htmContent = htmContent & RenderSubmitButton(arrButtons(iArrIndex), sBtnText, MSCSSiteStyle.Button)
        
        arrCell(iArrIndex) = RenderForm(url, htmContent, HTTP_POST)
    Next
    arrCellAlignment(0) = ALIGN_RIGHT
    arrCellAlignment(1) = ALIGN_LEFT
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.MsgBoxFooterTable, False, "")
    htmTable = RenderTable(htmRow, MSCSSiteStyle.MsgBoxFooterTable)

    tableRenderMessageBoxPageButtons = htmTable
End Function
%>