<%
' =============================================================================
' logic_orders.asp
' Business logic functions for Orders
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================


' -----------------------------------------------------------------------------
' PrepareOrderList
'
' Description:
'	Find all variables about this order list before handing them over to the
'	rendering function
'
' Parameters: (all OUT except first)
'	sAction						- Action called
'	dictSearchOptions			- Dictionary with all search options to find
'									the orders the user requested
'	mscsUserProfile				- User Profile object of current user
'	sStatusField				- $$
'	iStatusID					- $$
'	rsOrders					- Resulting recordset with orders
'	iPageNumber					- Active page number
'	iTotalRecords				- Total # of records retrieved
'	iTotalPages					- Total # of pages in recordset
'	sCriteria					- Displayable list of search criteria
'	sTargetPage					- Page that displays the results
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareOrderList(ByVal sAction, ByRef dictSearchOptions, ByRef mscsUserProfile, _
						ByRef sStatusField, _
						ByRef iStatusID, ByRef rsOrders, ByRef iPageNumber, _
						ByRef iTotalRecords, ByRef iTotalPages, ByRef sCriteria, _
						ByRef sTargetPage)
    Set dictSearchOptions = GetSearchDictionaryForThisPage(sAction, sStatusField, iStatusID)
	If (sStatusField = STATUS_FIELD_VALUE_SUCCESS) Then
		Set rsOrders = rsGetOrdersList(dictSearchOptions, iPageNumber, iTotalRecords, iTotalPages)
	End If
	
    ' Get the search criteria ("Simple" OR "Advanced")
    sCriteria = GetRequestString(QUERY_STRING_CRITERIA, Null)
	If IsNull(sCriteria) Then sCriteria = dictSearchOptions.filter_type
	
    Set mscsUserProfile = EnsureUserProfile()
        
    sTargetPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_TARGET)
End Sub


' -----------------------------------------------------------------------------
' mscsGetOrderGroupWithDiscountsByID
'
' Description:
'
' Parameters:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function mscsGetOrderGroupWithDiscountsByID(iID)
	Dim mscsOrderGrp
	
    Set mscsOrderGrp = GetOrderGroup(m_UserID)
    Call mscsOrderGrp.LoadOrder(iID)
    Call AddDiscountMessages(mscsOrderGrp)
    Set mscsGetOrderGroupWithDiscountsByID = mscsOrderGrp
End Function


' -----------------------------------------------------------------------------
' sGetAddressName
'
' Description:
'
' Parameters:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetAddressName(mscsOrderGrp, addr_id)
    sGetAddressName = mscsOrderGrp.GetAddress(addr_id).Value("address_name")
End Function


' -----------------------------------------------------------------------------
' sGetPaymentMethodString
'
' Description:
'
' Parameters: 
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetPaymentMethodString(sMethodID)
	If sMethodID = CREDIT_CARD_PAYMENT_METHOD Then
		sGetPaymentMethodString = mscsMessageManager.GetMessage("L_CreditCard_Payment_DisplayName_Text", sLanguage)
	ElseIf sMethodID = PURCHASE_ORDER_PAYMENT_METHOD Then
		sGetPaymentMethodString = mscsMessageManager.GetMessage("L_PurchaseOrder_Payment_DisplayName_Text", sLanguage)
	Else
		sGetPaymentMethodString = sMethodID
	End If
End Function


' -----------------------------------------------------------------------------
' AddDiscountMessages
'
' Description:
'	This function adds a footnote symbol for each discount applied to each
'   line item.  The footnote symbol will be "D" + the campaign id
'
' Parameters:
'	mscsOrderGrp				- Ordergroup object (i.e. basket) to add
'									Discount messages to
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub AddDiscountMessages(mscsOrderGrp)
    Dim oItem, oOrderForm, sOrderFormName, ciid
    
    For Each sOrderFormName In mscsOrderGrp.Value.ORDERFORMS
        Set oOrderForm = mscsOrderGrp.Value.ORDERFORMS.Value(sOrderFormName)
        For Each oItem In oOrderForm.Items
            If IsObject(oItem.discounts_applied) Then
                For Each ciid In oItem.discounts_applied
                    oItem.Value("_messages") = oItem.Value("_messages") & "D" & CStr(ciid) & NBSP
                Next
            Else
                oItem.Value("_messages") = NBSP
            End If
        Next
    Next
End Sub
%>
