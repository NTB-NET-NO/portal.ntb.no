<%
' =============================================================================
' password_handlers.asp
' Action handlers for password change/cancel
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' ChangeActionHandler
'
' Description:
'	"Change" action handler
'
' Parameters:
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function ChangeActionHandler(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim htm    
    Dim iStatusID
    Dim bValidPostedValues, bDisplayPostedValues
    Dim sStatusField

    sStatusField = Null
    iStatusID = Null
    bValidPostedValues = True
    bDisplayPostedValues = False
    
    htm = htmRenderChangePasswordPage( _
                                      sAction, _
                                      sSubAction, _
                                      sProfileIDs, _
                                      sStatusField, _
                                      iStatusID, _
                                      bValidPostedValues, _
                                      bDisplayPostedValues _
                                  )
    ChangeActionHandler = htm
End Function
    
    
' -----------------------------------------------------------------------------
' UpdatePasswordActionHandler
'
' Description:
'	Function to save the user/s user_security_password property
'
' Parameters:
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function UpdatePasswordActionHandler(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs)
	' Password field size constraint
	Const L_PasswordMinLength_Number = 7
    
    Dim htm
    Dim bDirty, bValidPostedValues, bDisplayPostedValues
    Dim iStatusID
    Dim sItem, sSourcePage, sActionPage
    Dim sStatusField, sUserIdUpdated, sUserOldPassword, sUserNewPassword
    Dim sUserConfirmPassword

    sUserOldPassword = GetRequestString(FIELD_USER_OLD_PASSWORD, Null)
    sUserNewPassword = GetRequestString(FIELD_USER_NEW_PASSWORD, Null)
    sUserConfirmPassword = GetRequestString(FIELD_USER_CONFIRM_PASSWORD, Null)
    sSourcePage = GetRequestString(SOURCE_PAGE, Null)

	' Check if either of the above values is null
    If Not (IsNull(sUserNewPassword) Or IsNull(sUserConfirmPassword) Or _
            IsNull(sUserOldPassword)) Then
		' Check for invalid password chars
		If bValidatePasswordChars(Request(FIELD_USER_OLD_PASSWORD)) And _
		   bValidatePasswordChars(Request(FIELD_USER_NEW_PASSWORD)) And _
		   bValidatePasswordChars(Request(FIELD_USER_CONFIRM_PASSWORD)) Then

			' Check minimum length constraint
			If (Len (sUserNewPassword) >= L_PasswordMinLength_Number) Then
				' Check if both the above values are same
				If StrComp(sUserNewPassword, sUserConfirmPassword, vbBinaryCompare) = 0 Then
					' Identical values
					On Error Resume Next
					Call UpdateUserPasswordProperty( _
													   sAction, _
													   sSubAction, _
													   sProfileIDs, _
													   sUserOldPassword, _
													   sUserNewPassword _
												   )
					If Err.Number<>0 Then
						If Err.Number = -1 Then
							Err.Clear
							sStatusField = STATUS_FIELD_VALUE_ERROR
							iStatusID = E_PASSWORDSET_FAIL
						Else
							Err.Clear
							sStatusField = STATUS_FIELD_VALUE_ERROR
							iStatusID = E_SAVING_PROFILE_PROPERTIES
						End If
						bValidPostedValues = True
						bDisplayPostedValues = True
					Else
						Response.Redirect sSourcePage
					End If
				Else
					' The new and confirm password field values don't match.
					' Set appropriate status message and display the page again.
					sStatusField = STATUS_FIELD_VALUE_ERROR
					iStatusID = E_PASSWORD_MISMATCH
					bValidPostedValues = True
					bDisplayPostedValues = True
				End If
			Else
				' The minimum length constraint is not valid.
				' Set appropriate status message and display the page again.
				sStatusField = STATUS_FIELD_VALUE_ERROR
				iStatusID = E_PASSWORD_WRONG_LENGTH
				bValidPostedValues = True
				bDisplayPostedValues = True
			End If
		Else
			' There was an invalid password character
			sStatusField = STATUS_FIELD_VALUE_ERROR
			If MSCSProfileServiceUsesActiveDirectory Then
				iStatusID = E_ADPASSWORD_BADCHARS
			Else
				iStatusID = E_PASSWORD_BADCHARS
			End If
			bValidPostedValues = True
			bDisplayPostedValues = True
		End If
    Else
        ' One of the two fields is empty.
        ' Set appropriate status message and display the page again.
        sStatusField = STATUS_FIELD_VALUE_ERROR
        iStatusID = E_PASSWORD_EMPTY
        bValidPostedValues = True
        bDisplayPostedValues = True
    End If

	UpdatePasswordActionHandler = htmRenderChangePasswordPage( _
									sAction, _
									sSubAction, _
									sProfileIDs, _
									sStatusField, _
									iStatusID, _
									bValidPostedValues, _
									bDisplayPostedValues _
								  )
End Function


' -----------------------------------------------------------------------------
' htmCancelUpdatePasswordAction
'
' Description:
'	Function to cancel the update of the user's user_security_password property
'
' Parameters:
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmCancelUpdatePasswordAction(ByRef sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim htm
    Dim sSourcePage
    
    sSourcePage = GetRequestString(SOURCE_PAGE, Null)        
    Response.Redirect (sSourcePage)
    
    htmCancelUpdatePasswordAction = htm
End Function
%>