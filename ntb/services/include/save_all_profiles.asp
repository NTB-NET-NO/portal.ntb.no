<%
' =============================================================================
' save_all_profiles.asp
'
' Common routines for saving Profiles when "Select All" is in use.
' Note: "Select All" was a feature which was punted from services profile editing,
'       these routines are for reference only
'       
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' SaveAllSelectedProfiles
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SaveAllSelectedProfiles(ByRef sStatusField, ByRef iStatusID, ByVal dictSearchOptions)
    Dim sSQLStmt, sSQLStmtForUser, sSQLStmtForAdmin, sTableName
    Dim sChecked, sAccessType, sCheckedRole, sCountryCode, sCountryName
    Dim rsProfile
    Dim oProperty
    Dim iUserClass, iUserAccountStatus
    Dim bSupported, bFieldUpdated, bFieldSelected
    Dim sCheckedStatus, sPartnerDeskRole, sOrgIDWhereClause, sSetClauseForAccountStatus
    Dim varPropertyValue
    Dim listProfileSchema
        
    bFieldUpdated = False
    bFieldSelected = True    
    iUserClass = iGetCurrentUserPropertyAccessClass()    
    
    ' Determine the Profile table name and default WHERE clause for 
    '     the SQL statement passed to the Commerce Provider to retreive 
    '     profiles
    Call GetTableNameAndWhereClause(sTableName, sOrgIDWhereClause)
    
    ' Construct the default SQL statement
    sSQLStmt = "Update " & sTableName & " Set "
    
    Set listProfileSchema = listCloneProfileSchema(sThisPage)    
    ' Add the updated property names and values to the SQL statement
    For Each oProperty In listProfileSchema
        If Not oProperty.IsGroup Then        
            varPropertyValue = Null        
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)
		    sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)		
		
		    If (bSupported) AND _
		       (StrComp(sAccessType, ACCESS_READWRITE, vbTextCompare) = 0) Then
                If oProperty.Type = DATA_TYPE_NUMBER Then
                    varPropertyValue = MSCSAppFrameWork.RequestNumber(oProperty.Name, Null)
                Else
                    varPropertyValue = GetRequestString(oProperty.Name, Null)
                End If            
            End If
            sChecked = GetRequestString(oProperty.Name & FIELD_UPDATED, "")
            If StrComp(sChecked, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
                sSQLStmt = sSQLStmt & sAddSetClause(oProperty.Name, oProperty.Type, _
                                        varPropertyValue, bFieldUpdated)
            End If
        End If
    Next
    Set oProperty = Nothing
    Set listProfileSchema = Nothing
    
    ' Add set clause for user_id_changed_by property
    sSQLStmt = sSQLStmt & sAddSetClause( _
                                           FIELD_GENERIC_CHANGED_BY, _
                                           DATA_TYPE_STRING, _
                                           m_UserID, _
                                           bFieldUpdated _
                                       )
    
    ' Add ser clause for date_updated property
    sSQLStmt = sSQLStmt & sAddSetClause( _
                                           FIELD_GENERIC_DATE_UPDATED, _
                                           DATA_TYPE_DATETIME, _
                                           Now(), _
                                           bFieldUpdated _
                                       )
                                       
    ' Special handler for user_account_control property
    sSetClauseForAccountStatus = AddSetClauseAndUpdateUserAccountControl _
                                 ( _
                                     bFieldUpdated, _
                                     bFieldSelected, _
                                     dictSearchOptions _
                                 )                                         
    ' Add the WHERE clause for user_account_status property
    sSQLStmt = sSQLStmt & sSetClauseForAccountStatus
    
    ' Add the first WHERE clause for the org_id property
    sSQLStmt = sSQLStmt & sOrgIDWhereClause
                    
    ' Add WHERE clause depending upon user search options
    sSQLStmt = sSQLStmt & sConstructWHEREclause(bFieldSelected, dictSearchOptions)

    ' Get the profile recorset
    Set rsProfile = Server.CreateObject("ADODB.Recordset")
    rsProfile.Open sSQLStmt, MSCSAdoConnection
End Sub


' -----------------------------------------------------------------------------
' AddSetClauseAndUpdateUserAccountControl
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function AddSetClauseAndUpdateUserAccountControl(ByRef bFieldUpdated, _
           ByRef bFieldSelected, ByVal dictSearchOptions)
    Dim sSQLStmt
    Dim sProfileType, sCheckedRole, sCheckedStatus
    Dim varPropertyValue
    Dim iUserAccountStatus
    
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    
    If (MSCSProfileServiceUsesActiveDirectory) AND _
       (StrComp(sProfileType, PROFILE_TYPE_USER,vbTextCompare) = 0) Then
        ' Check if we need to update the user_account_control property
        sCheckedStatus = GetRequestString(FIELD_USER_STATUS & FIELD_UPDATED, "")
        sCheckedRole = GetRequestString(FIELD_USER_PARTNERDESK_ROLE & FIELD_UPDATED, "")
        ' Check if either account_status or partner_desk_role is selected. This is because
        '     user_account_control is effected by these two properties only
        If (StrComp(sCheckedStatus, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) OR _
           (StrComp(sCheckedRole, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) Then
           ' Need to update the user_account_property
           
            ' Check if only account_status is getting updated
            If (StrComp(sCheckedStatus, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) AND _
               (StrComp(sCheckedRole, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) <> 0) Then
                
                ' Only account_status is updated, update the user_account_control for the 
                '     active user only
                iUserAccountStatus = MSCSAppFrameWork.RequestNumber(FIELD_USER_STATUS, Null)       
                If iUserAccountStatus = ACCOUNT_ACTIVE Then                    
                   Call UpdateUserAccountControlProperty(ROLE_USER, dictSearchOptions)
                   Call UpdateUserAccountControlProperty(ROLE_ADMIN, dictSearchOptions)
                End If
            Else
                ' Either both account_status and partner_desk_role is selected or only 
                '     partner_desk_role is updated
                varPropertyValue = GetUserAccountControl(sSQLStmt, bFieldSelected)
                sSQLStmt = sSQLStmt & sAddSetClause( _
                                                       FIELD_USER_UserAccountControl, _
                                                       DATA_TYPE_NUMBER, _
                                                       varPropertyValue, _
                                                       bFieldUpdated _
                                                   )
            End If                                               
        End If            
    End If 
    
    AddSetClauseAndUpdateUserAccountControl = sSQLStmt
End Function


' -----------------------------------------------------------------------------
' UpdateUserAccountControlProperty
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub UpdateUserAccountControlProperty(ByVal iPartnerDeskRole, ByVal dictSearchOptions)
    Dim rsProfile        
    Dim sSQLStmt, sTableName, sOrgIDWhereClause, sWhereClauseForRole
    Dim bFieldUpdated, bFieldSelected
    Dim varPropertyValue
    
    bFieldUpdated = False
    bFieldSelected = True    
    
    ' Determine the Profile table name and default WHERE clause for 
    '     the SQL statement passed to the commerce provider to retreive 
    '     profiles
    Call GetTableNameAndWhereClause(sTableName, sOrgIDWhereClause)
    
    ' Construct the default SQL statement
    sSQLStmt = "Update"
    ' Add FROM table clause
    sSQLStmt = sSQLStmt & " " & sTableName & " Set "
    
    If iPartnerDeskRole = ROLE_ADMIN Then        
        varPropertyValue = AD_DELEGATED_ADMIN
    Else
        varPropertyValue = AD_USER_ACTIVE
    End If    
    sSQLStmt = sSQLStmt & sAddSetClause(FIELD_USER_UserAccountControl, _
                                        DATA_TYPE_NUMBER, _
                                        varPropertyValue, _
                                        bFieldUpdated)
    ' Add the first WHERE clause to the SQL query
    sSQLStmt = sSQLStmt & sOrgIDWhereClause
    
    ' Add the WHERE clause for the partner_desk_role
    sSQLStmt = sSQLStmt & sAddWhereClauseForPartnerRole(bFieldSelected, iPartnerDeskRole)
                                                           
    ' Add WHERE clause depending upon user search options
    sSQLStmt = sSQLStmt & sConstructWHEREclause(bFieldSelected, dictSearchOptions)
    
    ' Get the profile recorset
    Set rsProfile = Server.CreateObject("ADODB.Recordset")
    rsProfile.Open sSQLStmt, MSCSAdoConnection
End Sub                                    


' -----------------------------------------------------------------------------
' GetUserAccountControl
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetUserAccountControl(ByRef sSQLStmt, ByRef bFieldSelected)
    Dim sCheckedRole, sCheckedStatus   
    Dim varPropertyValue
    Dim iUserAccountStatus
    Dim iUserPartnerDeskRole
                    
    ' Check if account_status and partner_desk_role are to be updated
    sCheckedStatus = GetRequestString(FIELD_USER_STATUS & FIELD_UPDATED, "")
    sCheckedRole = GetRequestString(FIELD_USER_PARTNERDESK_ROLE & FIELD_UPDATED, "")
    
    ' Both account_status and partner_desk_role are getting updated
    If (StrComp(sCheckedStatus, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) AND _
       (StrComp(sCheckedRole, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0) Then
       iUserAccountStatus = MSCSAppFrameWork.RequestNumber(FIELD_USER_STATUS, Null)
       iUserPartnerDeskRole = MSCSAppFrameWork.RequestNumber(FIELD_USER_PARTNERDESK_ROLE, Null)
       If iUserAccountStatus = ACCOUNT_ACTIVE Then
            If iUserPartnerDeskRole = ROLE_ADMIN Then
                varPropertyValue = AD_DELEGATED_ADMIN
            Else    
                varPropertyValue = AD_USER_ACTIVE
            End If    	    
       Else
            varPropertyValue = AD_USER_INACTIVE
       End If
        
    ' Only account_status is getting updated
    ElseIf StrComp(sCheckedStatus, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
       If iUserAccountStatus = ACCOUNT_ACTIVE Then
            varPropertyValue = AD_USER_ACTIVE
       Else
            varPropertyValue = AD_USER_INACTIVE
       End If
        
    ' Only partner_desk_role is getting updated
    ElseIf StrComp(sCheckedRole, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then
        ' Update the partner_desk_role only for the active users (ACCOUNT_ACTIVE)         
        sSQLStmt = sSQLStmt & sAddWhereClauseForActiveUsers(bFieldSelected)
        
        If iUserPartnerDeskRole = ROLE_ADMIN Then
            varPropertyValue = AD_DELEGATED_ADMIN
        Else    
            varPropertyValue = AD_USER_ACTIVE
        End If
    End If    
    GetUserAccountControl = varPropertyValue
End Function


' -----------------------------------------------------------------------------
' sAddWhereClauseForPartnerRole
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddWhereClauseForPartnerRole(ByRef bFieldSelected, ByVal iPartnerDeskRole)
    Dim sSQLStmt
    Dim sClause
                
    If bFieldSelected Then 
        sClause = "AND"
    Else
        bFieldSelected = True
        sClause = "WHERE"
    End If
    sSQLStmt = sAddSQLNumber(sClause, FIELD_USER_PARTNERDESK_ROLE, iPartnerDeskRole)
                            
    sAddWhereClauseForPartnerRole = sSQLStmt    
End Function


' -----------------------------------------------------------------------------
' sAddWhereClauseForActiveUsers
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddWhereClauseForActiveUsers(ByRef bFieldSelected)
    Dim sSQLStmt
    Dim sClause
                
    If bFieldSelected Then 
        sClause = "AND"
    Else
        bFieldSelected = True
        sClause = "WHERE"
    End If
    sSQLStmt = sAddSQLNumber(sClause, USER_STATUS, ACCOUNT_ACTIVE)
                            
    sAddWhereClauseForActiveUsers = sSQLStmt
End Function


%>