<%
' =============================================================================
' requisition.asp
' Common routines library for requesting orders and order details
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================


' -----------------------------------------------------------------------------
' rsGetOrdersList
'
' Description:
'	Function to get the orders depending on filter criteria selected by user
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetOrdersList(ByVal dictSearchOptions, ByRef iPageNumber, _
                         ByRef iTotalRecords, ByRef iTotalPages)
    Dim oReqMan, oSearchInfo, oResultsInfo
    Dim mscsUserProfile
    Dim rsOrders
    Dim sStatusCode
    
    Set mscsUserProfile = EnsureUserProfile()
    
    ' Create and initialize the objects
    Set oReqMan = Server.CreateObject("Commerce.OrderGroupManager")
    oReqMan.Initialize (dictConfig.s_TransactionsConnectionString)
    Set oSearchInfo = Server.CreateObject("Commerce.SimpleFindSearchInfo")
    Set oResultsInfo = Server.CreateObject("commerce.SimpleFindResultInfo")
    
    ' Assign appropriate values for the SearchInfo dictionary
    ' These search options are applicable for both Simple and Advanced
    '     search options.
    oSearchInfo.user_org_id = mscsUserProfile(USER_ORGID).Value    
    Select Case sThisPage
    Case MSCSSitePages.ListPartnerOrders
         If Len(dictSearchOptions.first_name) > 0 Then 
            oSearchInfo.user_firstname = dictSearchOptions.first_name
         End If
         If Len(dictSearchOptions.last_name) > 0 Then 
            oSearchInfo.user_lastname = dictSearchOptions.last_name
         End If
    Case MSCSSitePages.ListUserOrders
         oSearchInfo.user_id = m_UserID
    End Select    
    If Len(dictSearchOptions.order_number) > 0 Then _
        oSearchInfo.order_number = dictSearchOptions.order_number
    sStatusCode = dictSearchOptions.order_status_code
    Select Case sStatusCode
		Case ORDER_STATUS_NEW_ORDER, ORDER_STATUS_APPROVAL_PENDING, _
			ORDER_STATUS_APPROVED, ORDER_STATUS_REJECTED, ORDER_STATUS_FULLFILLED
				oSearchInfo.StatusFilter = dictSearchOptions.order_status_code
		Case Else
	        oSearchInfo.StatusFilter = ORDER_STATUS_NEW_ORDER Or _
                ORDER_STATUS_APPROVAL_PENDING Or _
                ORDER_STATUS_APPROVED Or _
                ORDER_STATUS_REJECTED Or _
                ORDER_STATUS_FULLFILLED
    End Select

    ' For Advanced Search options only
    If dictSearchOptions.filter_type = SEARCH_CRITERIA_ADVANCED Then
        If Len(dictSearchOptions.vendor_part_number) > 0 Then _
            oSearchInfo.product_id = dictSearchOptions.vendor_part_number
        If Len(dictSearchOptions.description) > 0 Then _
            oSearchInfo.description = dictSearchOptions.description
        If Len(dictSearchOptions.po_number) > 0 Then _
            oSearchInfo.po_number = dictSearchOptions.po_number
        If (dictSearchOptions.date_type = 1) Then _
            oSearchInfo.SearchDateTimeColumn = FIELD_ORDER_DATE_CREATED
        If (dictSearchOptions.date_type = 2) Then _
            oSearchInfo.SearchDateTimeColumn = FIELD_ORDER_DATE_UPDATED
        If Len(dictSearchOptions.start_date) > 0 Then _
            oSearchInfo.SearchDateTimeStart = dictSearchOptions.start_date
        If Len(dictSearchOptions.end_date) > 0 Then _
            oSearchInfo.SearchDateTimeEnd = dictSearchOptions.end_date
    End If
    
    ' Assign appripriate values to the ResultsInfo dictionary
    oResultsInfo.columns = "OrderGroup.ordergroup_id," & _
                           "OrderGroup.order_number," & _
                           "OrderGroup.user_first_name," & _
                           "OrderGroup.user_last_name," & _
                           "OrderGroup.user_id," & _
                           "OrderGroup.order_status_code," & _
                           "OrderGroup.d_DateLastChanged," & _
                           "OrderGroup.saved_cy_total_total"
                           '"OrderFormHeader.user_org_id," & _
                           '"OrderFormHeader.user_org_name"                           

    oResultsInfo.PageSize = RECORDS_PER_PAGE    
    ' iPageNumber = -1 indicates that the user has requested last page.
    If (iTotalPages > 0) And _
	   ((oResultsInfo.PageNumber > iTotalPages) Or (iPageNumber = -1)) Then
        oResultsInfo.PageNumber = iTotalPages
    Else
        oResultsInfo.PageNumber = iPageNumber
    End If    
    oResultsInfo.JoinOrderFormInfo = False
    oResultsInfo.JoinLineItemInfo = False

    ' Sort Orders ascendingly
    oResultsInfo.OrderGroupSortColumn = "order_number"
    oResultsInfo.SortDirection = "ASC"

    ' Call the SimpleFind() method to get the orders
    Set rsOrders = oReqMan.SimpleFind(oSearchInfo, oResultsInfo, iTotalRecords)

    Set oSearchInfo = Nothing
    Set oResultsInfo = Nothing
    Set oReqMan = Nothing
    
    Set rsGetOrdersList = rsOrders
    Set rsOrders = Nothing
End Function


' -----------------------------------------------------------------------------
' rsGetOrderDetails
'
' Description:
'	Function to get the details for a particular order
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rsGetOrderDetails(ByVal sID)    
    Dim oReqMan, oSerachInfo, oResultsInfo
    Dim columns, rsOrder
           
    ' Create and initialize the objects
    Set oReqMan = Server.CreateObject("Commerce.OrderGroupManager")
    oReqMan.Initialize (dictConfig.s_TransactionsConnectionString)
    Set oSerachInfo = Server.CreateObject("Commerce.SimpleFindSearchInfo")
    Set oResultsInfo = Server.CreateObject("commerce.SimpleFindResultInfo")

    ' Assign appropriate values for the SearchInfo dictionary  
    If IsNull(sID) Then
        oSerachInfo.order_number = GetRequestString(ORDER_NUMBER, Null)
    Else
        oSerachInfo.ordergroup_id = sID
    End If

    ' Assign appripriate values to the ResultsInfo dictionary    
    oResultsInfo.JoinOrderFormInfo = True
    oResultsInfo.JoinLineItemInfo = True
    oResultsInfo.columns =   "OrderGroup.ordergroup_id, " & _
                             "OrderGroup.order_status_code, " & _
                             "OrderGroup.d_DateCreated, " & _
                             "OrderGroup.d_DateLastChanged, " & _
                             "OrderGroup.saved_cy_total_total, " &_
                             "OrderGroup.order_number, " & _
                             "OrderGroup.user_id, " & _
                             "OrderGroup.user_first_name, " & _
                             "OrderGroup.user_last_name, " & _
                             "OrderFormHeader.user_org_id, " & _
                             "OrderFormHeader.user_org_name, " & _
                             "OrderFormHeader.po_number, " & _
                             "OrderFormHeader.orderform_id, " & _  
                             "OrderFormLineItems.product_id, " & _                           
                             "OrderFormLineItems.description, " & _
                             "OrderFormLineItems.quantity, " & _
                             "OrderFormLineItems.cy_unit_price"
    
    Set rsOrder = oReqMan.SimpleFind(oSerachInfo, oResultsInfo)
    
    Set oSerachInfo = Nothing
    Set oResultsInfo = Nothing
    Set oReqMan = Nothing    

    Set rsGetOrderDetails = rsOrder
    
    Set rsOrder = Nothing
End Function
%>
