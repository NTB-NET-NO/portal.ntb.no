<%
' =============================================================================
' ad_routines.asp
' Active Directory routines for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' bPaintACLForUser
'
' Description:
'	Function to paint ACLS for a user in Active Directory data store
'   This is achieved by adding/removing user to/from 
'   <COMPANY_NAME>_ADMINGROUP or <COMPANY_NAME>_USERGROUP
'
' Parameters:
'
' Returns:
'	Boolean: True (Success), False (Failure)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bPaintACLForUser( _
                             ByVal sUserCN, _
                             ByVal sSubAction, _
                             ByVal iUserPrevPartnerDeskRole, _
                             ByVal iUserPartnerDeskRole _
                         )
	Dim bPaintACL
	Dim sUserDN, sCatalogName, sUserOrgName
    Dim mscsUserProfile
    
    Set mscsUserProfile = EnsureUserProfile()
    bPaintACL = True
	sUserOrgName = GetOrgAttributeFromID(FIELD_ORG_NAME, mscsUserProfile(USER_ORGID).Value)
	
	' Construct the full DN for this particular user
	sUserDN = AD_COMMON_NAME_PREFIX & sUserCN & _
	          "," & _
	          AD_SITE_CONTAINER_PREFIX & sUserOrgName & _
			  "," & _
			  AD_SITE_CONTAINER_PREFIX & MSCSCommerceSiteName & _
			  "," & _
			  AD_CONTAINER_CS40 & _
			  "," & _
			  MSCSActiveDirectoryDomain
            	    		          	
	' Check if the user profile has been created
	If (StrComp(sSubAction, ADD_ACTION, vbTextCompare) = 0) OR _
	   (StrComp(sSubAction, COPY_ACTION, vbTextCompare) = 0) Then
	    If iUserPartnerDeskRole = ROLE_ADMIN Then		    		    
	        ' Add user to the  <COMPANY_NAME>_ADMINGROUP			
	    	bPaintACL = bAddUserToGroup(sUserOrgName & AD_ADMIN_GROUP_SUFFIX, sUserDN)		    
	    Else			
	        ' Add user to the <COMPANY_NAME>_USERGROUP
	    	bPaintACL = bAddUserToGroup(sUserOrgName & AD_USER_GROUP_SUFFIX, sUserDN)	
	    End If
	Else
	    ' For user that are already exisiting
	    If iUserPrevPartnerDeskRole = ROLE_ADMIN Then
	    	If iUserPartnerDeskRole = ROLE_USER Then
	    		bPaintACL = bRemoveUserFromGroup( _
	    		                                    sUserOrgName & AD_ADMIN_GROUP_SUFFIX, _
	    		                                    sUserDN _
	    		                                )
	    		If bPaintACL Then
	    		    bPaintACL = bAddUserToGroup( _
	    		                                   sUserOrgName & AD_USER_GROUP_SUFFIX, _
	    		                                   sUserDN _
	    		                               )		    		
	    	    End If
	    	End If
	    Else
	    	If iUserPartnerDeskRole = ROLE_ADMIN Then
	    	    bPaintACL = bRemoveUserFromGroup( _
	    		                                    sUserOrgName & AD_USER_GROUP_SUFFIX, _
	    		                                    sUserDN _
	    		                                )
	    		If bPaintACL Then
	    		    bPaintACL = bAddUserToGroup( _
	    		                                   sUserOrgName & AD_ADMIN_GROUP_SUFFIX, _
	    		                                   sUserDN _
	    		                               )		    		
	    	    End If
	    	End If
	    End If
	End If
    	
	bPaintACLForUser = bPaintACL
End Function	


' -----------------------------------------------------------------------------
' bAddUserToGroup
'
' Description:
'	Function to add a user to an existing Active Directory group
'
' Parameters:
'	szGroupName					- Name of the group to add user to
'	sUserDN						- DN of User to add to that group
'
' Returns:
'	Boolean: True (Success), False (Failure)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bAddUserToGroup(ByVal szGroupName, ByVal sUserDN)	
	Dim bAdded
	Dim iIndex
	Dim members
	Dim objProfile
	
	bAdded = True
	iIndex = 0
    ' Get the profile for the group
	Set objProfile = rsGetProfile(szGroupName, GROUP_PROFILE, True)
		
	' Get existing members of group
	members = objProfile("GeneralInfo.member").Value	
	' ReDim the members array and append the new value
	If Not IsNull(members) Then
	    If Not IsEmpty(members) Then
	        If UBound(members) >= 0 Then
	            ReDim Preserve members(UBound(members) + 1)
	            members(UBound(members)) = sUserDN
            End If  	            
        End If
	Else
	    ReDim members(iIndex)
	    members(iIndex) = sUserDN
	End If
	
	' Assign the new members array to the property
	objProfile("GeneralInfo.member").value = members	
	
	' Update the Active Directory group profile with new member values
	On Error Resume Next
	    Call objProfile.Update()
	    If Err.number Then
	        bAdded = False
	        Err.Clear
	    End if
	On Error GoTo 0
		
	bAddUserToGroup = bAdded
End Function


' -----------------------------------------------------------------------------
' bRemoveUserFromGroup
'
' Description:
'	Function to remove a user from an Active Directory group
'
' Parameters:
'	szGroupName					- Name of the group to add user to
'	sUserDN						- DN of User to add to that group
'
' Returns:
'	Boolean: True (Success), False (Failure)
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bRemoveUserFromGroup(ByVal szGroupName, ByVal sUserDN)
    Dim iIndex    
    Dim members
    Dim bRemoved
    Dim nNewCount    
    Dim objProfile
	Dim newMembers()	
    Dim sExistingMember	
    
    bRemoved = True
	iIndex = 0	    
    ' Get the profile for the group    
	Set objProfile = rsGetProfile(szGroupName, GROUP_PROFILE, True)
    
	' Get existing members of group
	members = objProfile("GeneralInfo.member").Value
	
    ' Populate the new array with all memeber apart from the one to be removed
	If Not IsNull(members) Then
	    If Not IsEmpty(members) Then	        		    
	        nNewCount = 0
	    	    ReDim newMembers(UBound(members))
		    For iIndex = LBound(members) to UBound(members)
		        sExistingMember = members(iIndex)
		        
		        ' Temporary work around
		        ' TASK: Delete this later
		        ' //
		        If InStr(1, sExistingMember, "\\") > 0 Then
		            sExistingMember = Replace(sExistingMember, "\\", "\")
		        End If
		        ' //
		        	        
		        If StrComp(sExistingMember, sUserDN, vbTextCompare) <> 0 Then
		    	    newMembers(nNewCount) = sExistingMember	    	    
		    	    nNewCount = nNewCount + 1
		        End If
		    Next		        
	    	    ReDim Preserve newMembers(nNewCount)
		        
            ' Assign the new members array to the property            
	        objProfile("GeneralInfo.member").value = newMembers
	        On Error Resume Next
	            Call objProfile.Update()
	            If Err.number Then
	                bRemoved = False
	                Err.Clear
	            End if
	        On Error GoTo 0
        End If
	End If
	
	bRemoveUserFromGroup = bRemoved
End Function 
%>