<%
' =============================================================================
' render_password.asp
' Password form rendering functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' htmRenderChangePasswordPage
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderChangePasswordPage(ByVal sAction, ByVal sSubAction, _
          ByVal sProfileIDs, ByVal sStatusField, ByVal iStatusID, _
          ByVal bValidPostedValues, ByVal bDisplayPostedValues)
    Dim rowHeader, rowStatus
    Dim formContent
    Dim htmRow, htmTable, htmForm, htmContents
    Dim url
    Dim iProfileCount
    Dim rsUser
    Dim sPageHeading
        
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)    
    If iProfileCount = 1 Then Set rsUser = rsGetProfileDetails(sProfileIDs)
        
    ' Get the HTML for the current page
    rowHeader = rowRenderHeader()
    formContent = formRenderChangePasswordPageContents( _
                                                          sAction, _
                                                          sSubAction, _
                                                          sProfileIDs, _
                                                          rsUser, _
                                                          bValidPostedValues, _
                                                          bDisplayPostedValues _
                                                      )
    rowStatus = rowRenderStatusInfo(sStatusField, iStatusID, iProfileCount, True)
    
    ' Construct the HTML for the current page    
    htmRow = rowHeader
    htmRow = htmRow & rowStatus
    htmTable = RenderTable(htmRow, MSCSSiteStyle.StatusTable)
    url = GenerateURL(MSCSSitePages.HelpPage, Array(SOURCE_PAGE), Array(sThisPage))
    htmForm = RenderForm(url, htmTable, HTTP_POST)
    htmContents = htmForm
    
    sPageHeading = sGetPageHeading(sThisPage)
    htmContents = htmContents & _
                  RenderText(sPageHeading, MSCSSiteStyle.Title)
    htmContents = htmContents & formContent    
    htmContents = RenderText(htmContents, MSCSSiteStyle.Body)
    
    If iProfileCount = 1 Then Set rsUser = Nothing
        
    htmRenderChangePasswordPage = htmContents
End Function


' -----------------------------------------------------------------------------
' formRenderChangePasswordPageContents
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderChangePasswordPageContents(sAction, sSubAction, _
          sProfileIDs, rsUser, bValidPostedValues, bDisplayPostedValues)
    Dim htmForm, htmTable, htmTaskButtons, htmContent
    Dim url
    Dim sSourcePage
    
    htmTaskButtons = tableChangePasswordTasks(sAction, sSubAction, sProfileIDs)
    htmTable = tableRenderChangePasswordContents( _
                                                    rsUser, _
                                                    bValidPostedValues, _
                                                    bDisplayPostedValues _
                                                )
    htmContent = htmTaskButtons & htmTable

    sSourcePage = GetRequestString(SOURCE_PAGE, Null)
    
    url = GenerateURL(sThisPage, Array(), Array())    
    htmContent = htmContent & RenderHiddenField(ACTION_TYPE, sAction) _
		& RenderHiddenField(SUB_ACTION_TYPE, sSubAction) _
		& RenderHiddenField(GUIDS, sProfileIDs)
    
    If Not IsNull(sSourcePage) Then
        htmContent = htmContent & RenderHiddenField(SOURCE_PAGE, sSourcePage)       
    End If    
    
    ' Wrap the contents of the page into a FORM element    
    htmForm = RenderForm(url, htmContent, HTTP_POST)
        
    formRenderChangePasswordPageContents = htmForm
End Function


' -----------------------------------------------------------------------------
' tableChangePasswordTasks
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableChangePasswordTasks(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim htmTable, htmRow, htmColumn
    Dim arrCell(1), arrCellAlignment(1)
    Dim url, sBtnText
    Dim iProfileCount
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    
    ' Add an empty row at the top
    htmRow = TRTag(arrCell, arrCellAlignment, Null, False, "")
    
    sBtnText = mscsMessageManager.GetMessage("L_SAVE_ACTION_BUTTON_Text", sLanguage)
    htmColumn = RenderSubmitButton(BUTTON_SUBMIT & SAVE_ACTION, sBtnText, MSCSSiteStyle.Button)

	htmColumn = htmColumn & htmRenderBlankSpaces(4)
	
	sBtnText = mscsMessageManager.GetMessage("L_CANCEL_ACTION_BUTTON_Text", sLanguage)
	htmColumn = htmColumn & RenderSubmitButton(BUTTON_SUBMIT & CANCEL_ACTION, sBtnText, MSCSSiteStyle.Button)

    arrCell(0) = htmColumn
    arrCellAlignment(0) = ALIGN_LEFT
    
    htmColumn = mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage) & _
                " = " & _
                mscsMessageManager.GetMessage("L_MSG_REQUIRED_FIELD_Text", sLanguage)
    
    arrCell(1) = htmColumn
    arrCellAlignment(1) = ALIGN_RIGHT
          
    htmRow = htmRow & TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsHeaderTable, False, "")
    htmTable = RenderTable(htmRow, MSCSSiteStyle.ContentsHeaderTable)
    
    tableChangePasswordTasks = htmTable
End Function


' -----------------------------------------------------------------------------
' tableRenderChangePasswordContents
'
' Description:
'	Function to display Password change Edit sheet.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderChangePasswordContents(ByVal rsUser, ByVal bValidPostedValues, _
          ByVal bDisplayPostedValues)
	' Password field size constraint
	Const L_PasswordMaxLength_Number = 14

    Dim htmTable, htmRow
    Dim arrCell(3), arrCellAlignment(3)
    Dim sFieldValue
    Dim url

    arrCellAlignment(0) = ALIGN_LEFT
    arrCellAlignment(1) = ALIGN_LEFT
    arrCellAlignment(2) = ALIGN_LEFT

    ' Add row for the original password
    arrCell(0) = mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage)
    arrCell(1) = mscsMessageManager.GetMessage("L_FIELD_OLD_PASSWORD_Text", sLanguage)

    If bDisplayPostedValues Then
        sFieldValue = GetRequestString(FIELD_USER_OLD_PASSWORD, Null)
    End If
    arrCell(2) = RenderPasswordBox(FIELD_USER_OLD_PASSWORD, sFieldValue, DEFAULT_TEXT_BOX_WIDTH, L_PasswordMaxLength_Number, MSCSSiteStyle.PasswordBox)
    htmRow = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")

    ' Add row for the new password
    arrCell(0) = mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage)
    arrCell(1) = mscsMessageManager.GetMessage("L_FIELD_NEW_PASSWORD_Text", sLanguage)
    
    If bDisplayPostedValues Then 
        sFieldValue = GetRequestString(FIELD_USER_NEW_PASSWORD, Null)
    End If        
    arrCell(2) = RenderPasswordBox(FIELD_USER_NEW_PASSWORD, sFieldValue, DEFAULT_TEXT_BOX_WIDTH, L_PasswordMaxLength_Number, MSCSSiteStyle.PasswordBox)
    htmRow = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")

    ' Add row for the confirm new password
    arrCell(0) = mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage)
    arrCell(1) = mscsMessageManager.GetMessage("L_FIELD_CONFIRM_PASSWORD_Text", sLanguage)

    If bDisplayPostedValues Then
        sFieldValue = GetRequestString(FIELD_USER_CONFIRM_PASSWORD, Null)
    End If
    arrCell(2) = RenderPasswordBox(FIELD_USER_CONFIRM_PASSWORD, sFieldValue, DEFAULT_TEXT_BOX_WIDTH, L_PasswordMaxLength_Number, MSCSSiteStyle.PasswordBox)
    htmRow = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")
        
    htmTable = RenderTable(htmRow, MSCSSiteStyle.ContentsBodyTable)
    
    tableRenderChangePasswordContents = htmTable
End Function
%>