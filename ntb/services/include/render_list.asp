<%
' =============================================================================
' render_list.asp
' Common list rendering functions
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' htmRenderProfilesList
'
' Description:
'	Function to display contents for any LIST type page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderProfilesList(ByRef sAction, ByVal sSubAction, ByRef iPageNumber, _
                            ByRef iTotalPages)
    Dim url
    Dim htmRow, htmForm, htmTable, htmContents    
    Dim rowHeader, rowStatus
    Dim iStatusID, iTotalRecords
    Dim formSearch, formContent
    Dim rsProfiles
    Dim tableFooter
    Dim sStatusField, sPageHeading
    Dim dictSearchOptions
	Dim bIsEmpty

    Set dictSearchOptions = GetSearchDictionaryForThisPage(sAction, sStatusField, iStatusID)
	If (sStatusField = STATUS_FIELD_VALUE_SUCCESS) Then _
		Set rsProfiles = rsGetProfilesList(dictSearchOptions, iTotalRecords)

    ' Get the HTML for the current page and assign them to appropriate
    ' global variables
    rowHeader = rowRenderHeader()
    formSearch = formRenderProfileSearchOptions(sAction, sSubAction, dictSearchOptions)
	If (sStatusField = STATUS_FIELD_VALUE_SUCCESS) Then
		bIsEmpty = False
		formContent = formRenderListPageContents(sAction, sSubAction, rsProfiles, _
					    iPageNumber, iTotalRecords, iTotalPages, bIsEmpty)
		If (True = bIsEmpty) Then
			sStatusField = STATUS_FIELD_VALUE_ERROR
			If (sAction = FILTER_ACTION) Then
				iStatusID = E_FILTER_NO_RECORDS
			ElseIf (sAction = LIST_ACTION) And (iPageNumber = 1) Then
				iStatusID = E_NO_PROFILES
			Else
				' PAGING_ACTION
				iStatusID = E_NO_PROFILES_PERPAGE
			End If
		Else
		    sStatusField = Null
			iStatusID = Null
		End If
	Else
		formContent = htmRenderEmptyRecordset(mscsMessageManager.GetMessage("L_MSG_FILTER_NORECORDS_Text", sLanguage))
	End If

    rowStatus = rowRenderStatusInfo(sStatusField, iStatusID, iTotalRecords, False)
    tableFooter = tableRenderFooterInfo(sAction, sSubAction, iPageNumber, _
                    iTotalRecords, iTotalPages, True, False)

    ' Construct the HTML for the current page
    htmRow = rowHeader & rowStatus
    htmTable = RenderTable(htmRow, MSCSSiteStyle.StatusTable)
    url = GenerateURL(MSCSSitePages.HelpPage, Array(SOURCE_PAGE), Array(sThisPage))
    htmContents = RenderForm(url, htmTable, HTTP_POST)
    
    sPageHeading = sGetPageHeading(sThisPage)
    htmContents = htmContents _
					& RenderText(sPageHeading, MSCSSiteStyle.Title) _
					& formSearch _
					& formContent _
					& tableFooter
    
    htmContents = RenderText(htmContents, MSCSSiteStyle.Body)

    ' Release the objects assigned in the global variables
    Set dictSearchOptions = Nothing
    Set rsProfiles = Nothing
    
    htmRenderProfilesList = htmContents
End Function


' -----------------------------------------------------------------------------
' formRenderProfileSearchOptions
'
' Description:
'	Function to display the filtering options for the current page If they
'	exist
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderProfileSearchOptions(ByVal sAction, ByVal sSubAction, _
										ByVal dictSearchOptions)
    Dim url
    Dim sBtnText, sCriteria
    Dim htmRow, htmForm, htmTable, htmColumn, htmContent
    Dim arrCell
    Dim urlItem
    Dim arrQSKeyName, arrQSKeyValue, arrCellAlignment
    Dim bAddIfNotPresent
        
    ' Get the search criteria ("Simple" OR "Advanced")
    sCriteria = GetRequestString(QUERY_STRING_CRITERIA, Null)
	If IsNull(sCriteria) Then sCriteria = dictSearchOptions.filter_type
		        
	' Add "Search Criteria" table to the page
	arrCell = Array(colRenderSimpleSearchTab(sCriteria), colRenderAdvancedSearchTab(sCriteria))
	arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
	htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.SearchHeaderTable, False, "")
	htmContent = RenderTable(htmRow, MSCSSiteStyle.SearchHeaderTable)

    ' Add Simple Or Advanced search options to the page
	If StrComp(sCriteria, SEARCH_CRITERIA_ADVANCED, vbTextCompare) = 0 Then
	    htmTable = rowsRenderAdvanceSearchOptionsForProfiles(dictSearchOptions)
	Else
	    htmTable = rowsRenderSimpleSearchOptionsForProfiles(dictSearchOptions)
	End If
		    
	' Add a HIDDEN FORM element for ACTION_TYPE, to store the value of 
	'     FILTER_ACTION. This is required to handle Return 
	'     key press by the user on the Search options.
	htmContent = htmContent & _
	             RenderHiddenField(ACTION_TYPE, FILTER_ACTION)

    ' Add a SUBMIT button (FILTER_ACTION) to the page	
	sBtnText = mscsMessageManager.GetMessage("L_FILTER_ACTION_BUTTON_Text", sLanguage)
	htmColumn = RenderSubmitButton(BUTTON_SUBMIT & FILTER_ACTION, sBtnText, MSCSSiteStyle.Button)  
	arrCell = Array(htmColumn, NBSP)
	arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT)
	htmTable = htmTable & TRTag(arrCell, arrCellAlignment, Null, False, "")
	htmContent = htmContent & RenderTable(htmTable, MSCSSiteStyle.SearchBodyTable)    
        
    bAddIfNotPresent = False
    htmContent = htmContent & _
                 RenderHiddenField(QUERY_STRING_CRITERIA, dictSearchOptions.filter_type)
    
    If (StrComp(sAction, SELECT_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sSubAction, SELECT_ACTION, vbTextCompare) = 0) Then
        arrQSKeyName = Array(SUB_ACTION_TYPE)
        arrQSKeyValue = Array(SELECT_ACTION)    
    Else
        arrQSKeyName = Array()
        arrQSKeyValue = Array() 
    End If        
    url = sReConstructPageURL(arrQSKeyName, arrQSKeyValue)
    
    ' Add the search options FORM to the page
	htmForm = RenderForm(url, htmContent, HTTP_POST)
		
    formRenderProfileSearchOptions = htmForm
End Function


' -----------------------------------------------------------------------------
' rowsRenderSimpleSearchOptionsForProfiles
'
' Description:
'	Function to add Simple search option contents to LIST type pages
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowsRenderSimpleSearchOptionsForProfiles(ByVal dictSearchOptions)
    Dim htmRow, htmColumn, htmRowTextBox, htmRowDateTime, htmRowComboBox
    Dim arrCell, arrCellAlignment
    Dim oProperty
    Dim sUserProperty
    Dim listProfileSchema
        
    ' Add Simple search option contents for all profiles
    ' Loop through the global dictionary for the profile and determine the properties
    '     which participate in the search operations.
    ' The "SearchOptions" attribute of each property is used to determine if it will 
    '     participate in search operation.
        
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
        
	For Each oProperty In listProfileSchema	   
	    If Not oProperty.IsGroup Then
            If StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ALL, vbTextCompare) = 0 Then
                 
                ' Depending upon the property type add appropriate HTML input tags
                Select Case oProperty.InputType
                     Case TextBox
                         htmRowTextBox = htmRowTextBox & _
                                         rowRenderSearchOptionForString _
                                         ( _
                                             oProperty.Attributes.DisplayName, _
                                             oProperty.Name, _
                                             oProperty.Tip, _
                                             oProperty.DefinedSize, _
                                             dictSearchOptions.Value(oProperty.Name) _
                                         )
                     Case ComboBox
                         htmRowComboBox = htmRowComboBox & _
                                          rowRenderSearchOptionForInteger _
                                          ( _
                                              oProperty.Name, _
                                              oProperty.Attributes.DisplayName, _
                                              dictSearchOptions.Value(oProperty.Name) _
                                          )
                             
                     Case DateTime
                         htmRowDateTime = htmRowDateTime & _
                                          rowRenderSearchOptionForDate _
                                          ( _ 
                                              oProperty, _
                                              dictSearchOptions _
                                          )
                    
                End Select
             End If
        End If
    Next
        
    htmRow = htmRowComboBox & htmRowTextBox & htmRowDateTime
    Set oProperty = Nothing
    Set listProfileSchema = Nothing
    
    rowsRenderSimpleSearchOptionsForProfiles = htmRow
End Function


' -----------------------------------------------------------------------------
' rowsRenderAdvanceSearchOptionsForProfiles
'
' Description:
'	Function to add Advanced search option contents to LIST type pages
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowsRenderAdvanceSearchOptionsForProfiles(ByVal dictSearchOptions)
    Dim htmColumn, htmRow, htmRowComboBox, htmRowTextBox, htmRowDateTime
    Dim arrCell, arrCellAlignment
    Dim oProperty
    Dim sUserProperty
    Dim listProfileSchema
    
    ' Add Simple search option contents for all profiles
    ' Loop through the global dictionary for the profile and determine the properties
    '     which participate in the search operations.
    ' The "SearchOptions" attribute of each property is used to determine if it will 
    '     participate in search operation.

    Set listProfileSchema = listCloneProfileSchema(sThisPage)
        
    For Each oProperty In listProfileSchema
        If Not oProperty.IsGroup Then
            If (StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ALL, vbTextCompare) = 0) Or _
               (StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ADVANCED, vbTextCompare) = 0) Then
                   
               ' Depending upon the property type add appropriate HTML input tags
               Select Case oProperty.InputType
                 Case COMBOBOX
                      htmRowComboBox = htmRowComboBox & _
                                       rowRenderSearchOptionForInteger _
                                       ( _
                                           oProperty.Name, _
                                           oProperty.Attributes.DisplayName, _
                                           dictSearchOptions.Value(oProperty.Name) _
                                       )

                 Case TEXTBOX
                      htmRowTextBox = htmRowTextBox & _
                                      rowRenderSearchOptionForString _
                                      ( _
                                          oProperty.Attributes.DisplayName, _
                                          oProperty.Name, _
                                          oProperty.Tip, _
                                          oProperty.DefinedSize, _
                                          dictSearchOptions.Value(oProperty.Name) _
                                      )
                             
                 Case DATETIME
                      htmRowDateTime = htmRowDateTime & _
                                       rowRenderSearchOptionForDate _
                                       ( _
                                           oProperty, _
                                           dictSearchOptions _
                                       )
                                         
               End Select
            End If
        End If
    Next
        
    htmRow = htmRowComboBox & htmRowTextBox & htmRowDateTime
    
    Set oProperty = Nothing
    Set listProfileSchema = Nothing
    
    rowsRenderAdvanceSearchOptionsForProfiles = htmRow
End Function


' -----------------------------------------------------------------------------
' tableRenderListPageTasks
'
' Description:
' Function to render task specific to LIST type pages
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderListPageTasks(ByVal sAction, ByVal sSubAction)
    Dim htmTable, htmRow, htmColumn
    Dim arrCell(0), arrCellAlignment(0)
    Dim url, sBtnText

    ' Add an empty row at the top
    htmRow = TRTag(arrCell, arrCellAlignment, Null, False, "")
    
    ' For all the profiles
    ' Check if the LIST page is displayed in the profile PICKER mode
    '      For ex :  Selecting a user for the list (user picker)
    If (StrComp(sAction, SELECT_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sSubAction, SELECT_ACTION, vbTextCompare) = 0) Then
               
        sBtnText = mscsMessageManager.GetMessage("L_SELECTED_ACTION_BUTTON_Text", sLanguage)
        htmColumn = htmColumn & RenderSubmitButton(BUTTON_SUBMIT & SELECTED_ACTION, sBtnText, MSCSSiteStyle.Button)
    Else           
        ' Add task buttons for any general LIST type page
        sBtnText = mscsMessageManager.GetMessage("L_ADD_ACTION_BUTTON_Text", sLanguage)
        htmColumn = RenderSubmitButton(BUTTON_SUBMIT & ADD_ACTION, sBtnText, MSCSSiteStyle.Button)
        
        htmColumn = htmColumn & htmRenderBlankSpaces(4)
        
        sBtnText = mscsMessageManager.GetMessage("L_COPY_ACTION_BUTTON_Text", sLanguage)
        htmColumn = htmColumn & RenderSubmitButton(BUTTON_SUBMIT & COPY_ACTION, sBtnText, MSCSSiteStyle.Button)
        
        htmColumn = htmColumn & htmRenderBlankSpaces(4)
        
        sBtnText = mscsMessageManager.GetMessage("L_DELETE_ACTION_BUTTON_Text", sLanguage)
        htmColumn = htmColumn & RenderSubmitButton(BUTTON_SUBMIT & DELETE_ACTION, sBtnText, MSCSSiteStyle.Button)
        
        htmColumn = htmColumn & htmRenderBlankSpaces(4)
        
        sBtnText = mscsMessageManager.GetMessage("L_EDIT_ACTION_BUTTON_Text", sLanguage)
        htmColumn = htmColumn & RenderSubmitButton(BUTTON_SUBMIT & EDIT_ACTION, sBtnText, MSCSSiteStyle.Button)

'		htmColumn = htmColumn & htmRenderBlankSpaces(4)
'		
'		sBtnText = mscsMessageManager.GetMessage("L_SELECTALL_ACTION_BUTTON_Text", sLanguage)
'     htmColumn = htmColumn & RenderSubmitButton(BUTTON_SUBMIT & SELECTALL_ACTION, sBtnText, MSCSSiteStyle.Button)
'		
'		htmColumn = htmColumn & htmRenderBlankSpaces(4)
'
'     sBtnText = mscsMessageManager.GetMessage("L_DESELECTALL_ACTION_BUTTON_Text", sLanguage)
'		htmColumn = htmColumn & RenderSubmitButton(BUTTON_SUBMIT & DESELECTALL_ACTION, sBtnText, MSCSSiteStyle.Button)
    End If

    arrCell(0) = htmColumn
    arrCellAlignment(0) = ALIGN_LEFT

    htmRow = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsHeaderTable, False, "")
    htmTable = RenderTable(htmRow, MSCSSiteStyle.ListContentsHeaderTable)
    
    tableRenderListPageTasks = htmTable    
End Function


' -----------------------------------------------------------------------------
' formRenderListPageContents
'
' Description:
'	Function to render LIST type page contents
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderListPageContents(ByVal sAction, _
                                    ByVal sSubAction, _
                                    ByVal rsProfiles, _
                                    ByRef iPageNumber, _
                                    ByRef iTotalRecords, _
                                    ByRef iTotalPages, _
                                    ByRef bIsEmpty)
    Dim htmForm, htmTable, htmTaskButtons, htmContent
    Dim url
    Dim sSourcePage, sPropertyName, sTargetPage
    
    sTargetPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_TARGET)    
    htmTaskButtons = tableRenderListPageTasks(sAction, sSubAction)
    
    htmTable = tableRenderMultipleRecords(sAction, _
                                          sSubAction, _
                                          rsProfiles, _
                                          iPageNumber, _
                                          iTotalRecords, _
                                          iTotalPages, _
                                          bIsEmpty)
    htmContent = htmTaskButtons & htmTable
    
    If (StrComp(sAction, SELECT_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sSubAction, SELECT_ACTION, vbTextCompare) = 0) Then    
        sSourcePage = GetRequestString(SOURCE_PAGE, Null)
        sPropertyName = sGetSelectActionPropertyName(sAction)
        url = GenerateURL(sSourcePage, Array(), Array())        
        htmContent = htmContent & RenderHiddenField(ACTION_TYPE, SELECT_ACTION) _       
			& RenderHiddenField(SELECT_FIELD_NAME, sPropertyName)
    Else        
        If Len(sSubAction) > 0 Then            
            url = GenerateURL(sTargetPage, Array(), Array())        
            htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, sSubAction)            
        Else
            url = GenerateURL(sTargetPage, Array(), Array())
        End If
    End If

    ' Wrap the contents of the page into a FORM element    
    htmForm = RenderForm(url, htmContent, HTTP_POST)
    
    formRenderListPageContents = htmForm
End Function


' -----------------------------------------------------------------------------
' tableRenderMultipleRecords
'
' Description:
'	Function to add list sheet contents to current page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderMultipleRecords(ByVal sAction, _
                                    ByVal sSubAction, _
                                    ByVal rsProfiles, _
                                    ByRef iPageNumber, _
                                    ByRef iTotalRecords, _
                                    ByRef iTotalPages, _
                                    ByRef bIsEmpty)
    
    Dim htmTable, htmRow
    Dim arrCell, arrCellAlignment
    Dim Count
    Dim nPage, nCountTemp
    Dim sUniqueID
    Dim bRecordCountPresent
    Dim i

    bRecordCountPresent = True

    ' Initialize the local variable involved in paging mechanism        
    Call RelocateRecordPointer2(Count, _
                                nPage, _
                                nCountTemp, _
                                bRecordCountPresent, _
                                rsProfiles, _
                                iPageNumber, _
                                iTotalRecords)

    ' Add headings for the profile properties shown in the LIST page
    htmRow = rowRenderListHeading()

    ' Display the Profile in the list sheet by looping through the record-set
    ' stored in the global variable
    While (Not rsProfiles.EOF) And (nCountTemp < RECORDS_PER_PAGE)
		' List handler for all the profiles                
		arrCell = arrPopulateProfileDetails(sAction, _
											sSubAction, _
											Count, _
											rsProfiles)
    
		' Generate an HTML TR tag for this Profile/OrderHeader
		ReDim arrCellAlignment(UBound(arrCell))
		For i = LBound(arrCell) To UBound(arrCell)
			arrCellAlignment(i) = ALIGN_LEFT
		Next
		htmRow = htmRow & TRTag(arrCell, arrCellAlignment, _
                              MSCSSiteStyle.ListContentsBodyTable, _
                              False, _
                              "")

        ' Increment the appropriate counters and move to the next 
        '     record in the record-set
        nCountTemp = nCountTemp + 1
        Count = Count + 1
        rsProfiles.MoveNext
    Wend

	' Check if current page is empty
	If (nCountTemp = 0) Then 
		bIsEmpty = True
		htmTable = htmRenderEmptyRecordSet(mscsMessageManager.GetMessage("L_MSG_FILTER_NORECORDS_Text",sLanguage))
	Else
	    ' Wrap all the HTML TR's for each record in the record-set into TABLE tag
	    htmTable = RenderTable(htmRow, MSCSSiteStyle.ListContentsBodyTable)
	End If
	
    ' Set the global paging variables depending upon the local variables                
    Call SetGlobalPagingVariables(bRecordCountPresent, _
                                  nCountTemp, _
                                  iTotalRecords, _  
                                  iPageNumber, _
                                  iTotalPages, _
                                  True)

    tableRenderMultipleRecords = htmTable
End Function


' -----------------------------------------------------------------------------
' arrPopulateProfileDetails
'
' Description:
'	Function to populate an ARRAY with profile properties
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function arrPopulateProfileDetails(ByVal sAction, ByVal sSubAction, _
                                   ByVal Count, ByVal rsProfile)
    Dim arrCell
    Dim oProperty
    Dim i
    Dim listProfileSchema
    Dim sProfileType
                                         
    ReDim arrCell(0)
    i = 1
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    
    ' Loop through the profile property collection and add them to the array, if
    '     1.)the Property is a JOIN KEY (GUID) then a hidden FORM element for the 
    '        property value is stored in the array
    '     2.)if their ShowInList attribute has value = "1"
            
    ReDim arrCell(listProfileSchema.Count)
    For Each oProperty In listProfileSchema
       If Not oProperty.IsGroup Then
            If (sProfileType = PROFILE_TYPE_USER And oProperty.Name = FIELD_USER_ID) Or _
			   (sProfileType <> PROFILE_TYPE_USER And oProperty.Attributes.IsJoinKey = True) Then
                arrCell(0) = htmRenderUniqueIDForProfile(sAction, _
                                                         sSubAction, _
                                                         oProperty, _
                                                         Count, _
                                                         rsProfile)
            ElseIf StrComp(oProperty.Attributes.ShowInList, "1", vbTextCompare) = 0 Then                    
                If oProperty.Type = DATA_TYPE_NUMBER Then
	        		    arrCell(i) = sGetDisplayValueForProperty _
	        		                 (oProperty.Name, rsProfile(oProperty.QualifiedName))
	        	   Else
	        	        arrCell(i) = rsProfile(oProperty.QualifiedName)
	        	   End If    
                i = i + 1
    
            End If
        End If
    Next
    ReDim Preserve arrCell(i)

    ' Relase the Property object in the oProperty variable
    Set oProperty = Nothing
    Set listProfileSchema = Nothing
    
    arrPopulateProfileDetails = arrCell
End Function


' -----------------------------------------------------------------------------
' htmRenderUniqueIDForProfile
'
' Description:
'	Function to add profile identifier (GUID) to the LIST page and 
'   to add either checkbox/radiobutton
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderUniqueIDForProfile(ByVal sAction, ByVal sSubAction, _
           ByVal oProperty, ByVal Count, ByVal rsProfile)
    Dim htm                    
    
    ' Depending upon the value in variable, sAction add
    '     1.)Radio button if action type is SELECT_ACTION
    '     2.)Check Box (checked initially) if sub action type is 
    '        SELECTALL_ACTION
    '     3.)By default add a check box (un-checked initially)
    
    If (StrComp(sAction, SELECT_ACTION, vbTextCompare) = 0)OR _
       (StrComp(sSubAction, SELECT_ACTION, vbTextCompare) = 0) Then
        htm = RenderRadioButton(SELECTED_ATTRIBUTE_NAME, _
                                rsProfile(oProperty.QualifiedName), _
                                False, _
                                MSCSSiteStyle.RadioButton)
    ElseIf StrComp(sSubAction, SELECTALL_ACTION, vbTextCompare) = 0 Then
           htm = RenderCheckBox(SELECTED_ATTRIBUTE_NAME & CStr(Count), SELECTED_ATTRIBUTE_CHECKED, True, MSCSSiteStyle.CheckBox)
    Else
           htm = RenderCheckBox(SELECTED_ATTRIBUTE_NAME & CStr(Count), SELECTED_ATTRIBUTE_CHECKED, False, MSCSSiteStyle.CheckBox)
    End If
    
    ' Add a hidden HTML FORM element to store the value for the property in 
    '     SELECTED_ATTRIBUTE_NAME FORM element
    htm = htm & _
          RenderHiddenField(SELECTED_ATTRIBUTE_NAME & CStr(Count) & _
            RECORD_ID_ATTRIBUTE_NAME, rsProfile(oProperty.QualifiedName))

    htmRenderUniqueIDForProfile = htm
End Function


' -----------------------------------------------------------------------------
' rowRenderListHeading
'
' Description:
'	Function to Add headings for the profile properties shown in LIST
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderListHeading()
    Dim htmRow
    Dim arrCell, arrCellAlignment()
    Dim i
    Dim oProperty
    Dim listProfileSchema
    
         
    ' For all other profiles, check the property attribute ShowInList to 
    '     determine if the property need to shown in the List sheet
    i = 0
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
            
    ReDim arrCell(listProfileSchema.Count + 1)
    arrCell(i) = htmRenderBlankSpaces(4)
    i = i + 1
    For Each oProperty In listProfileSchema
        If Not oProperty.IsGroup Then
            If StrComp(oProperty.Attributes.ShowInList, "1", vbTextCompare) = 0 Then
               arrCell(i) = oProperty.Attributes.DisplayName
               i = i + 1
            End If
        End If            
    Next
    ReDim Preserve arrCell(i)
            
    Set oProperty = Nothing
    Set listProfileSchema = Nothing
    
    ReDim arrCellAlignment(UBound(arrCell))
    For i = LBound(arrCell) To UBound(arrCell)
        arrCellAlignment(i) = ALIGN_LEFT
    Next
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ListContentsHeaderTable, True, "")
    
    rowRenderListHeading = htmRow
End Function


' -----------------------------------------------------------------------------
' RelocateRecordPointer2
'
' Description:
'	Sub to initialize the local variables passed, which will be used in LIST
'	type page and specifically in the paging mechanism.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RelocateRecordPointer2(ByRef Count, _
                           ByRef nPage, _
                           ByRef nCountTemp, _
                           ByRef bRecordCountPresent, _
                           ByVal rs, _
                           ByRef iPageNumber, _
                           ByRef iTotalRecords)
    Count = 0
    nPage = 1
    nCountTemp = 0

    If iTotalRecords = 0 Then
        bRecordCountPresent = False
        If iPageNumber > 1 Then
            While (nPage < iPageNumber) And (Not rs.EOF)
                nCountTemp = 0
                While (nCountTemp < RECORDS_PER_PAGE)  And (Not rs.EOF)
                    nCountTemp = nCountTemp + 1
                    Count = Count + 1
                    rs.MoveNext
                Wend
                nPage = nPage + 1
            Wend
        End If
        iTotalRecords = Count
        nCountTemp = 0
    End If
End Sub
%>