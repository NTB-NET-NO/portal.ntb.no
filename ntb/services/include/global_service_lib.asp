<SCRIPT LANGUAGE=VBScript RUNAT=Server>
' =============================================================================
' global_service_lib.asp
' Global Initialization library with Partner/Customer Service functions;
' used by global.asa
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' AddPartnerAndCustomerServicePages
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub AddPartnerAndCustomerServicePages(ByRef dictPage)
    ' Common Pages for customer and partner services
	dictPage.ChangePassword			= "services/change_password.asp"
	dictPage.HelpPage               = "services/help.asp"

	' partner service pages
	dictPage.EditPartnerAccount		= "services/partner/account.asp"	
	dictPage.ListPartnerUsers		= "services/partner/users.asp"
	dictPage.EditPartnerUser		= "services/partner/edit_user.asp"	
	dictPage.ListPartnerOrders		= "services/partner/orders.asp"
	dictPage.ViewPartnerOrder		= "services/partner/view_order.asp"
	dictPage.ListPartnerAddresses	= "services/partner/addresses.asp"
	dictPage.EditPartnerAddress		= "services/partner/edit_address.asp"
			
	' customer service pages for requisitioners
	dictPage.EditUserAccount	    = "services/customer/account.asp"	
	dictPage.ListUserOrders			= "services/customer/orders.asp"	
	dictPage.ViewUserOrder			= "services/customer/view_order.asp"
	dictPage.ListUserAddresses		= "services/customer/addresses.asp"
	dictPage.EditUserAddress		= "services/customer/edit_address.asp"
	
	
	dictPage.BadAction              = "services/error/badaction.asp"
	dictPage.NoProfile              = "services/error/noprofile.asp"
	dictPage.NoOrgProfile           = "services/error/noorgprofile.asp"
	dictPage.NoAccess               = "services/error/noaccess.asp"	
	dictPage.NoOrder                = "services/error/noorder.asp"
End Sub


' -----------------------------------------------------------------------------
' GetPartnerAndCustomerServicePageAttributes
'
' Description:
'	Function to get the properties for all partner and customer
'   service pages
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetPartnerAndCustomerServicePageAttributes(MSCSSitePages)
    Const PAGE_TYPE_LIST = "List"
    Const PAGE_TYPE_EDIT = "Edit"
    
    Const PROFILE_TYPE_ORDER	  = "Order"
        
    Dim dictPages
    Dim dictPageProperties
    
    Set dictPages = GetDictionary()
        
    Set dictPageProperties = GetDictionary()
    dictPageProperties.PageType    = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType = PROFILE_TYPE_USER
    dictPageProperties.TargetPage  = MSCSSitePages.ChangePassword    
    dictPageProperties.ListPage    = MSCSSitePages.ListPartnerUsers
    Set dictPages(MSCSSitePages.ChangePassword) = dictPageProperties
        
	Set dictPageProperties = GetDictionary()	
	dictPageProperties.PageType    = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType = Null
    dictPageProperties.TargetPage  = MSCSSitePages.HelpPage    
    Set dictPages(MSCSSitePages.HelpPage) = dictPageProperties
    
	Set dictPageProperties = GetDictionary()	
	dictPageProperties.PageType      = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType   = PROFILE_TYPE_ORG
    dictPageProperties.TargetPage    = MSCSSitePages.EditPartnerAccount    
    Set dictPages(MSCSSitePages.EditPartnerAccount) = dictPageProperties
	
	Set dictPageProperties = GetDictionary()	
	dictPageProperties.PageType    = PAGE_TYPE_LIST
    dictPageProperties.ProfileType = PROFILE_TYPE_USER
    dictPageProperties.TargetPage  = MSCSSitePages.EditPartnerUser    
    Set dictPages(MSCSSitePages.ListPartnerUsers) = dictPageProperties
	
	Set dictPageProperties = GetDictionary()	
	dictPageProperties.PageType    = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType = PROFILE_TYPE_USER
    dictPageProperties.TargetPage  = MSCSSitePages.EditPartnerUser
    dictPageProperties.ListPage    = MSCSSitePages.ListPartnerUsers    
    Set dictPages(MSCSSitePages.EditPartnerUser) = dictPageProperties
	
	Set dictPageProperties = GetDictionary()	
	dictPageProperties.PageType     = PAGE_TYPE_LIST
    dictPageProperties.ProfileType  = PROFILE_TYPE_ORDER
    dictPageProperties.TargetPage   = MSCSSitePages.ViewPartnerOrder    
    Set dictPages(MSCSSitePages.ListPartnerOrders) = dictPageProperties
	
	Set dictPageProperties = GetDictionary()	
	dictPageProperties.PageType     = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType  = PROFILE_TYPE_ORDER
    dictPageProperties.TargetPage   = MSCSSitePages.ViewPartnerOrder
    dictPageProperties.ListPage     = MSCSSitePages.ListPartnerOrders    
    Set dictPages(MSCSSitePages.ViewPartnerOrder) = dictPageProperties
    
	Set dictPageProperties = GetDictionary()
	dictPageProperties.PageType        = PAGE_TYPE_LIST
    dictPageProperties.ProfileType     = PROFILE_TYPE_ADDRESS
    dictPageProperties.TargetPage      = MSCSSitePages.EditPartnerAddress    
    Set dictPages(MSCSSitePages.ListPartnerAddresses) = dictPageProperties
	
	Set dictPageProperties = GetDictionary()
	dictPageProperties.PageType      = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType   = PROFILE_TYPE_ADDRESS
    dictPageProperties.TargetPage    = MSCSSitePages.EditPartnerAddresses
    dictPageProperties.ListPage      = MSCSSitePages.ListPartnerAddresses    
    Set dictPages(MSCSSitePages.EditPartnerAddress) = dictPageProperties
    
    Set dictPageProperties = GetDictionary()
	dictPageProperties.PageType    = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType = PROFILE_TYPE_USER
    dictPageProperties.TargetPage  = MSCSSitePages.EditUserAccount    
    Set dictPages(MSCSSitePages.EditUserAccount) = dictPageProperties
    
    Set dictPageProperties = GetDictionary()
	dictPageProperties.PageType    = PAGE_TYPE_LIST
    dictPageProperties.ProfileType = PROFILE_TYPE_ORDER
    dictPageProperties.TargetPage  = MSCSSitePages.ViewUserOrder    
    Set dictPages(MSCSSitePages.ListUserOrders) = dictPageProperties
    
    Set dictPageProperties = GetDictionary()	
	dictPageProperties.PageType     = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType  = PROFILE_TYPE_ORDER
    dictPageProperties.TargetPage   = MSCSSitePages.ViewUserOrder
    dictPageProperties.ListPage     = MSCSSitePages.ListUserOrders    
    Set dictPages(MSCSSitePages.ViewUserOrder)  = dictPageProperties
 	
	Set dictPageProperties = GetDictionary()
	dictPageProperties.PageType     = PAGE_TYPE_LIST
    dictPageProperties.ProfileType  = PROFILE_TYPE_ADDRESS
    dictPageProperties.TargetPage   = MSCSSitePages.EditUserAddress    
    Set dictPages(MSCSSitePages.ListUserAddresses) = dictPageProperties
    
    Set dictPageProperties = GetDictionary()
	dictPageProperties.PageType    = PAGE_TYPE_EDIT
    dictPageProperties.ProfileType = PROFILE_TYPE_ADDRESS
    dictPageProperties.TargetPage  = MSCSSitePages.EditUserAddress
    dictPageProperties.ListPage    = MSCSSitePages.ListUserAddresses    
    Set dictPages(MSCSSitePages.EditUserAddress)  = dictPageProperties

    Set GetPartnerAndCustomerServicePageAttributes = dictPages
End Function


' -----------------------------------------------------------------------------
' AddPartnerAndCustomerServiceStyles
'
' Description:
'	This function adds some styles used just by Partner/Customer Services
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub AddPartnerAndCustomerServiceStyles(ByRef objSiteStyle)
    objSiteStyle.StatusTable = " BGCOLOR='#FFFFFF' border='0' width='100%' cellspacing='0' cellpadding='0'"    
	
	' Search box header & body
	objSiteStyle.SearchHeaderTable = " BGCOLOR='#FFFFCC' border='1' cellspacing='5' cellpadding='0'"    
	objSiteStyle.SearchBodyTable = " BGCOLOR='#FFFFCC' width='100%'"
	
	' EditSheet grid
	objSiteStyle.ContentsHeaderTable = " BGCOLOR='#CCFFCC' border='0' width='100%' cellspacing='0' cellpadding='0'"
	' EditSheet header
	objSiteStyle.ContentsBodyTable = " BGCOLOR='#FFFFCC' border='0' width='100%' cellspacing='0' cellpadding='0'"
	' EditSheet Group Divider
	objSiteStyle.ContentsGroupTable = " BGCOLOR='#CCCCFF' border='0' width='100%' cellspacing='0' cellpadding='0'"
		
	' ListSheet grid
	objSiteStyle.ListContentsHeaderTable = " BGCOLOR='#CCCCFF' BORDER='0' CELLSPACING='0' CELLPADDING='2' WIDTH='100%'" 'For results header
	' ListSheet header
	objSiteStyle.ListContentsBodyTable = " BGCOLOR='#CCFFCC' BORDER='1' CELLSPACING='0' CELLPADDING='2' WIDTH='100%'"   'For list results and edit grids
	
	objSiteStyle.FooterTable = " BORDER='0' WIDTH='100%' CELLSPACING='0' CELLPADDING='0'"
	objSiteStyle.MsgBoxTitleTable = " BGCOLOR='#CCCCCC' border='0' width='60%' height='5%' cellspacing='0' cellpadding='0'"
	objSiteStyle.MsgBoxHeadingTable = " BGCOLOR='#CCCCCC' border='0' width='60%' heigth='5%' cellspacing='0' cellpadding='0'"
	objSiteStyle.MsgBoxBodyTable = " BGCOLOR='#CCCCCC' border='0' width='60%' heigth='15%' cellspacing='0' cellpadding='0'"
	objSiteStyle.MsgBoxFooterTable = " BGCOLOR='#CCCCCC' border='0' width='60%' heigth='5%' cellspacing='0' cellpadding='0'"			

	objSiteStyle.StatusString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"
	objSiteStyle.SearchHeaderString = " COLOR='BLACK' FACE='Verdana' SIZE='5'"
	objSiteStyle.SearchBodyString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"
	objSiteStyle.ContentsHeaderString = " COLOR='BLACK' FACE='Verdana' SIZE='5'"
	objSiteStyle.ContentsGroupString = " COLOR='BLACK' FACE='Verdana' SIZE='5'"
	objSiteStyle.ContentsBodyString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"    	
	objSiteStyle.FooterString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"
	objSiteStyle.MsgBoxTitleString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"
	objSiteStyle.MsgBoxHeadingString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"
	objSiteStyle.MsgBoxBodyString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"							
	objSiteStyle.MsgBoxFooterString = " COLOR='BLACK' FACE='Verdana' SIZE='4'"							
End Sub    
</SCRIPT>