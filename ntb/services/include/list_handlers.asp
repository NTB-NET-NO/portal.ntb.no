<%
' =============================================================================
' list_handlers.asp
' Action handlers for list type page action: "Selected"
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' SelectedActionHandlerForList
'
' Description:
'	"Selected" action handler for LIST type page    
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SelectedActionHandlerForList(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim sActionPage, sTargetpage, sSelectProperty
    Dim iProfileCount
    
    sTargetpage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_TARGET)
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    sSelectProperty = sGetSelectActionPropertyName(sAction)
    
    ' Redirect to the target page if defined or redirect to the same page
    If iProfileCount > 0 Then
        sActionPage = GenerateURL(sTargetpage, Array(ACTION_TYPE, SELECT_FIELD_NAME, GUIDS), Array(SELECTED_ACTION, sSelectProperty, sProfileIDs))
    Else
       sActionPage = GenerateURL(sThisPage, Array(), Array())
    End If
    
    Response.Redirect sActionPage
End Sub
%>