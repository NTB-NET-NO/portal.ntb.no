<%
' =============================================================================
' profiles_common.asp
' Common routines library for Profiles manipulation
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' sAddSetClause
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddSetClause(ByVal sPropertyName, _
                       ByVal iPropertyType, _
                       ByVal varPropertyValue, _
                       ByRef bFieldUpdated)
    Dim sSetClause
    
    If bFieldUpdated Then
        sSetClause = ", "
    Else
        bFieldUpdated = True
    End If
        
    If iPropertyType = DATA_TYPE_NUMBER Then
        sSetClause = sSetClause &  _
                     sPropertyName & "=" & varPropertyValue    
    Else
        sSetClause = sSetClause &  _
                     sPropertyName & "=" & "'" & varPropertyValue & "'"
    End If                     
    
    sAddSetClause = sSetClause
End Function


' -----------------------------------------------------------------------------
' GetTableNameAndWhereClause
'
' Description:
' Sub to initialize the variable with DATABASE table name and WHERE caluse
'     depending upon the profile type
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub GetTableNameAndWhereClause(ByRef sTableName, ByRef sOrgIDWhereClause)
    Dim sProfileType
    
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    Select Case sProfileType
        Case PROFILE_TYPE_USER        
             sTableName = "UserObject"
             sOrgIDWhereClause = " WHERE [" & USER_ORGID & "]=" & _
                                 "'" & _
                                    mscsUserProfile(USER_ORGID).Value & _
                                 "' "
        Case Else
			Err.Raise E_NOTIMPL
    End Select
End Sub


' -----------------------------------------------------------------------------
' sConstructWHEREclause
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sConstructWHEREclause(ByRef bFieldSelected, ByRef dictSearchOptions)
    Dim sSQLStmt
    Dim oProperty                              
    Dim listProfileSchema
    
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
    
    For Each oProperty In listProfileSchema
        If Not oProperty.IsGroup Then
            If dictSearchOptions.filter_type = SEARCH_CRITERIA_ADVANCED Then
                If (StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ALL, vbTextCompare) = 0) Or _
                   (StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ADVANCED, vbTextCompare) = 0) Then
                    sSQLStmt = sSQLStmt & _
                               sAddCriteriaToQuery(bFieldSelected, oProperty, dictSearchOptions)
                End If
            Else
                If StrComp(oProperty.Attributes.SearchOptions, SEARCH_OPTION_ALL, vbTextCompare) = 0 Then
                    sSQLStmt = sSQLStmt & _
                               sAddCriteriaToQuery(bFieldSelected, oProperty, dictSearchOptions)
                End If
            End If
        End If            
    Next
    
    Set oProperty = Nothing
    Set listProfileSchema = Nothing    
    sConstructWHEREclause = sSQLStmt
End Function


' -----------------------------------------------------------------------------
' sAddCriteriaToQuery
'
' Description:
'	Function to build the T-SQL statement
'
' Parameters:
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddCriteriaToQuery(ByRef bFieldSelected, ByVal oProperty, _
                             ByVal dictSearchOptions)
    Dim sSQLStmt
    Dim sClause
                
    If bFieldSelected Then 
       sClause = "AND"
    Else
       sClause = "WHERE"
    End If
    
    Select Case oProperty.InputType
    Case ComboBox
         If dictSearchOptions.Value(oProperty.Name) <> BLANK_OPTION Then
             bFieldSelected = True
             sSQLStmt = sAddSQLNumber( _
                                         sClause, _
                                         oProperty.QualifiedName, _
                                         dictSearchOptions.Value(oProperty.Name) _
                                     )
         End If
    Case TextBox
         If Len(dictSearchOptions.Value(oProperty.Name)) > 0 Then
             bFieldSelected = True
             sSQLStmt = sAddSQLString( _
                                         sClause, _
                                         oProperty.QualifiedName, _
                                         dictSearchOptions.Value(oProperty.Name) _
                                     )
         End If
    Case DateTime
         If Len(dictSearchOptions.Value(oProperty.Name & FIELD_START_DATE)) > 0 And _
            Len(dictSearchOptions.Value(oProperty.Name & FIELD_END_DATE)) > 0 Then

             bFieldSelected = True
             sSQLStmt = sAddStartDate( _
                                         sClause, _
                                         oProperty.QualifiedName, _
                                         dictSearchOptions.Value(oProperty.Name & FIELD_START_DATE) _
                                     ) & _
                        sAddEndDate( _
                                         "AND", _
                                         oProperty.QualifiedName, _
                                         dictSearchOptions.Value(oProperty.Name & FIELD_END_DATE) _
                                   )
    
         ElseIf Len(dictSearchOptions.Value(oProperty.Name & FIELD_START_DATE)) > 0 Then
             bFieldSelected = True
             sSQLStmt = sAddStartDate( _
                                         sClause, _
                                         oProperty.QualifiedName, _
                                         dictSearchOptions.Value(oProperty.Name & FIELD_START_DATE) _
                                     )
         ElseIf Len(dictSearchOptions.Value(oProperty.Name & FIELD_END_DATE)) > 0 Then
            sSQLStmt = sAddEndDate( _
                                         sClause, _
                                         oProperty.QualifiedName, _
                                         dictSearchOptions.Value(oProperty.Name & FIELD_END_DATE) _
                                  )
         End If
    End Select

    sAddCriteriaToQuery = sSQLStmt
End Function


' -----------------------------------------------------------------------------
' sAddSQLNumber
'
' Description:
'	Function to add AND clause for a property of type NUMBER
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddSQLNumber(ByVal sClause, ByVal sPropertyName, ByVal sPropertyValue)
    Dim str
    
    str = " " & sClause & " " & _
          "[" & _
              sPropertyName & _
          "]" & _
          " = " & _
          sPropertyValue
          
    sAddSQLNumber = str               
End Function


' -----------------------------------------------------------------------------
' sAddSQLString
'
' Description:
'	Function to add AND clause for LIKE string value
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddSQLString(ByVal sClause, ByVal sPropertyName, ByVal sPropertyValue)
    Dim str
    
    str = " " & sClause & " " & _
          "[" & _
              sPropertyName & _
          "]" & _
          " LIKE " & _
          "'%" & _
              HandleSingleQuote(sPropertyValue) & _
          "%'"
          
    sAddSQLString = str
End Function              


' -----------------------------------------------------------------------------
' HandleSingleQuote
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function HandleSingleQuote(ByVal sSQLValue)
    Dim sSQL
    If InStr(1, sSQLValue, "'") > 0 Then
        sSQL = Replace(sSQLValue, "'", "''")        
    Else
        sSQL = sSQLValue
    End If
    HandleSingleQuote = sSQL
End Function


' -----------------------------------------------------------------------------
' sAddStartDate
'
' Description:
'	Function to add Start Date clause
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddStartDate(ByVal sClause, ByVal sPropertName, ByVal sPropertyValue)
    Dim sSQLStmt

    sSQLStmt = " " & sClause & " " & _
               "[" & _
                   sPropertName & _
               "]" & _
               " >= '" & _
                   GetODBCTimeStamp(sPropertyValue) & _
               "'"

    sAddStartDate = sSQLStmt
End Function                   


' -----------------------------------------------------------------------------
' sAddEndDate
'
' Description:
'	Function to add End Date clause
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sAddEndDate(ByVal sClause, ByVal sPropertName, ByVal sPropertyValue)
    Dim sSQLStmt
    
    sSQLStmt = " " & sClause & " " & _
               "[" & _
                   sPropertName & _
               "]" & _
               " < '" & _
                   GetODBCTimeStamp(sPropertyValue + 1) & _
               "'"

    sAddEndDate = sSQLStmt
End Function


' -----------------------------------------------------------------------------
' GetODBCTimeStamp
'
' Description:
'	Used to convert a VB variant date into an ODBC timestamp format { ts 'yyyy-mm-dd hh:mm:ss' }
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetODBCTimeStamp(varTime)
    If Not IsDate(varTime) Then 
        GetODBCTimeStamp = "null"
    Else
        GetODBCTimeStamp = "{ ts '" & right("0000" & Year(varTime),4) &  "-" & right("00" & Month(varTime),2) & "-" & right("00" & Day(varTime),2) & " " & right("00" & Hour(varTime),2) &  ":" & right("00" & Minute(varTime),2) & ":" & right("00" & Second(varTime),2) & ".0' }"
    End If
End Function

%>