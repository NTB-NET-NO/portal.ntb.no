<%
' =============================================================================
' render_edit_common.asp
' Common rendering routines for editing pages
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' htmRenderEditPage
'
' Description:
'	Function to display the contents of any EDIT type page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderEditPage(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
                           ByVal sStatusField, ByVal iStatusID, _
                           ByVal bValidPostedValues, ByVal bDisplayPostedValues, _
                           ByVal listProfileSchema)
    Dim url
    Dim htmRow, htmForm, htmTable, htmContents, htmStatusRows
    Dim rsProfile
    Dim rowStatus, rowHeader
    Dim formContent
    Dim sPageHeading
    Dim iProfileCount
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    
    If (StrComp(sAction, ADD_ACTION, vbTextCompare) <> 0) Then
       If iProfileCount = 1 Then
            Set rsProfile = rsGetProfileDetails(sProfileIDs)
       End If
    End If
    
    ' Get the HTML for the current page
    rowHeader = rowRenderHeader()
    formContent = formRenderEditPageBody(sAction, _
                                         sSubAction, _
                                         sProfileIDs, _
                                         rsProfile, _
                                         bValidPostedValues, _
                                         bDisplayPostedValues, _
                                         listProfileSchema)
    rowStatus = rowRenderStatusInfo(sStatusField, iStatusID, iProfileCount, True)
    
    ' Construct the HTML for the current page    
    htmStatusRows = rowHeader & rowStatus    
    htmTable = RenderTable(htmStatusRows, MSCSSiteStyle.StatusTable)
    url = GenerateURL(MSCSSitePages.HelpPage, Array(SOURCE_PAGE), Array(sThisPage))
    htmForm = RenderForm(url, htmTable, HTTP_POST)
    htmContents = htmForm

    ' Release the objects assigned in the global variables            
    Set rsProfile = Nothing
    
    sPageHeading = sGetPageHeading(sThisPage)
    htmRenderEditPage = RenderText(htmContents _
					& RenderText(sPageHeading, MSCSSiteStyle.Title) _
					& formContent, MSCSSiteStyle.Body)

End Function
                                            

' -----------------------------------------------------------------------------
' formRenderEditPageBody
'
' Description:
'	Function to display the contents for the current page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function formRenderEditPageBody(ByVal sAction, ByVal sSubAction, _
           ByVal sProfileIDs, ByVal rsProfiles, ByVal bValidPostedValues, _
           ByVal bDisplayPostedValues, ByVal listProfileSchema)
    Dim url
    Dim htmForm, htmTable, htmContent
    Dim sSourcePage, sPropertyName
    Dim htmTaskButtons
    
    htmTaskButtons = tableRenderEditPageTasks(sAction, sSubAction, sProfileIDs)
    htmTable = tableRenderProfiles( _
                                      sAction, _
                                      sSubAction, _
                                      rsProfiles, _
                                      sProfileIDs, _
                                      bValidPostedValues, _
                                      bDisplayPostedValues, _
                                      listProfileSchema _
                                  )    
    htmContent = htmTaskButtons & htmTable
    
    url = GenerateURL(sThisPage, Array(), Array())
    htmContent = htmContent & RenderHiddenField(ACTION_TYPE, sAction)    
    htmContent = htmContent & RenderHiddenField(GUIDS, sProfileIDs)
    
    Select Case sAction        
        Case ADD_ACTION            
            htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, ADD_ACTION)
        
        Case COPY_ACTION            
            htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, COPY_ACTION)
            
        Case EDIT_ACTION            
            ' If StrComp(sSubAction, SELECTALL_ACTION, vbTextCompare) <> 0 Then
                htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, EDIT_ACTION)
            ' Else                                  
            '    htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, sSubAction)
            ' End If                                  
            
        Case SAVE_ACTION            
            htmContent = htmContent & RenderHiddenField(SUB_ACTION_TYPE, sSubAction)
    End Select
    
    htmForm = RenderForm(url, htmContent, HTTP_POST)
    formRenderEditPageBody = htmForm
End Function


' -----------------------------------------------------------------------------
' tableRenderEditPageTasks
'
' Description:
'	Function to display tasks buttons for the current page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderEditPageTasks(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs)
    Dim url, sBtnText
    Dim htmRow, htmTable, htmColumn
    Dim arrCell(1), arrCellAlignment(1)
    Dim iProfileCount
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    
    ' Add an empty row at the top
    htmRow = TRTag(arrCell, arrCellAlignment, Null, False, "")
    
	sBtnText = mscsMessageManager.GetMessage("L_SAVE_ACTION_BUTTON_Text", sLanguage)
    htmColumn = RenderSubmitButton(BUTTON_SUBMIT & SAVE_ACTION, sBtnText, MSCSSiteStyle.Button)
    
    ' Special handler for profile pages (EditPartnerAccount, EditUserAccount) as they
    '      will list only the current user's and his organization profile
    If (StrComp(sThisPage,MSCSSitePages.EditPartnerAccount,vbTextCompare) <> 0) AND _
       (StrComp(sThisPage,MSCSSitePages.EditUserAccount,vbTextCompare) <> 0) Then        
                   
        ' Add task buttons for any general EDIT type page    
		sBtnText = mscsMessageManager.GetMessage("L_CANCEL_ACTION_BUTTON_Text", sLanguage)
        htmColumn = htmColumn & _
                    htmRenderBlankSpaces(4) & _
                    RenderSubmitButton(BUTTON_SUBMIT & CANCEL_ACTION, sBtnText, MSCSSiteStyle.Button)
    End If
    arrCell(0) = htmColumn
    arrCellAlignment(0) = ALIGN_LEFT
    
    If (iProfileCount = 0) OR (iProfileCount = 1) Then       
        htmColumn = mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage) & _
                    " = " & _
                    mscsMessageManager.GetMessage("L_MSG_REQUIRED_FIELD_Text", sLanguage)
    Else
         htmColumn = RenderCheckBox("", "", True, MSCSSiteStyle.CheckBox) & _
                     " = " & _
                     mscsMessageManager.GetMessage("L_MSG_SELECT_UPDATEDFIELDS_Text", sLanguage)         
    End If
    arrCell(1) = htmColumn
    arrCellAlignment(1) = ALIGN_RIGHT    
    
          
    htmRow = htmRow & _
             TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsHeaderTable, False, "")
    htmTable = RenderTable(htmRow, MSCSSiteStyle.ContentsHeaderTable)
    
    tableRenderEditPageTasks = htmTable
End Function


' -----------------------------------------------------------------------------
' tableRenderProfiles
'
' Description:
'	Function to add edit sheet for any profile
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function tableRenderProfiles(ByVal sAction, ByVal sSubAction, ByVal rsProfile, _
           ByVal sProfileIDs, ByVal bValidPostedValues, ByVal bDisplayPostedValues, _
           ByVal listProfileSchema)
    Dim htmRow, htmGroupHeader, htmTable    
    Dim bFirstInGroup, bSupported, bRenderProperty
    Dim arrCell, arrCellAlignment
    Dim oProperty
    Dim iUserClass, iProfileCount
    Dim sAccessType, sCommonValue, sDisplayValue

    iUserClass = iGetCurrentUserPropertyAccessClass()
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
	bFirstInGroup = False

    ' Loop through the list of properties for particular profile    
    For Each oProperty In listProfileSchema
        bRenderProperty = True    

        ' Display name attribute only for property groups
        If oProperty.IsGroup Then
            htmGroupHeader = rowRenderEditSheetBlankLine()
            htmGroupHeader = htmGroupHeader & rowRenderPropertyGroup(oProperty)
			bFirstInGroup = True
        Else
            ' Check if this property is supported by the site feature
            bSupported = bCheckIfThePropertyFeatureIsSupported(oProperty)
            
            If bSupported Then                
				' Check the user access for this property
		        sAccessType = sGetUserAccessForThisProperty(iUserClass, oProperty)

                Select Case sAccessType
                Case ACCESS_HIDDEN                     
                     htmRow = htmRow & rowRenderHiddenProperty(sAction, _
										sSubAction, oProperty, rsProfile, _
                                        sProfileIDs)
                Case ACCESS_READONLY
					' Render the read-only properties if sAction is not COPY_ACTION
					' Also don't render the multivalue properties
					If (StrComp(sAction, COPY_ACTION, vbTextCompare) <> 0) AND _
					   (oProperty.Attributes.IsMultivalued = False) Then

	                    If bFirstInGroup = True Then
							htmRow = htmRow & htmGroupHeader
							bFirstInGroup = False
						End If

						htmRow = htmRow & _
									rowRenderReadOnlyProperty(sAction, _
									sSubAction, oProperty, bValidPostedValues, _
									sProfileIDs, rsProfile)
					End If                                                            
                Case ACCESS_READWRITE
					' Add special handler for all the properties
                    ' Don't render the Multivalue properties, or passwords in the multiedit case                    
                    If Not ((oProperty.Attributes.IsMultivalued = True And oProperty.Name <> FIELD_USER_PASSWORD) Or _
                       (oProperty.Name = FIELD_USER_PASSWORD And iProfileCount> 1 )) Then
	             
	                    If bFirstInGroup = True Then
							htmRow = htmRow & htmGroupHeader
							bFirstInGroup = False
						End If

                        htmRow = htmRow & _
									rowRenderProperty(sAction, sSubAction, _
									oProperty, listProfileSchema, _
									bValidPostedValues, rsProfile, _
									sProfileIDs, bDisplayPostedValues)
                    End If
                End Select
            End If
        End If            
    Next    
    htmTable = RenderTable(htmRow, MSCSSiteStyle.ContentsBodyTable)
    
    Set oProperty = Nothing    
    tableRenderProfiles = htmTable
End Function


' -----------------------------------------------------------------------------
' rowRenderPropertyGroup
'
' Description:
'	Function to add property group heading information
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderPropertyGroup(ByVal oProperty)
    Dim htmRow
    Dim arrCell, arrCellAlignment
    
    arrCell = Array(htmRenderBlankSpaces(4), Bold(oProperty.DisplayName), htmRenderBlankSpaces(4))
    arrCellAlignment = Array(ALIGN_LEFT, ALIGN_LEFT, ALIGN_LEFT)
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsGroupTable, False, "")
    
    rowRenderPropertyGroup = htmRow
End Function


' -----------------------------------------------------------------------------
' rowRenderHiddenProperty
'
' Description:
'	Function to display properties which are hidden for the current user
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderHiddenProperty(ByVal sAction, ByVal sSubAction, ByVal oProperty, _
           ByVal rsProfile, ByVal sProfileIDs)
    Dim htm
    Dim iProfileCount

    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)

    ' Handle USER_ID case
    If (oProperty.Name = FIELD_USER_ID) And (sAction <> COPY_ACTION) Then
	    ' For single profile editing	      
        If iProfileCount = 1 Then
	   	   htm = RenderHiddenField(oProperty.Name, rsProfile(oProperty.QualifiedName).Value)
        End If

	' Handle PRIMARY KEY properties   
    ElseIf (oProperty.Attributes.IsPrimaryKey = True) Then
        If iProfileCount = 1 Then
             htm = RenderHiddenField(oProperty.Name, rsProfile(oProperty.QualifiedName).Value)
	        ' For supporting multi-edit
		    ' ElseIf iProfileCount > 1 Then
			'     htm = RenderHiddenField(GUIDS, sProfileIDs)
        End If
    End If

    rowRenderHiddenProperty = htm
End Function


' -----------------------------------------------------------------------------
' rowRenderReadOnlyProperty
'
' Description:
'	Function to display ReadOnly properties
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderReadOnlyProperty(ByVal sAction, _
                                   ByVal sSubAction, _
                                   ByVal oProperty, _
                                   ByVal bValidPostedValues, _
                                   ByVal sProfileIDs, _
                                   ByVal rsProfile)
    Const SITE_PROPERTY_SITETERM = 10

    Dim htm
    Dim dictOption
    Dim iProfileCount
	Dim sName, sValue, sDisplayValue, sForeignKeyValue

    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)

	If iProfileCount = 1 Then
		If IsObject (rsProfile) Then
			sValue = rsProfile(oProperty.QualifiedName).Value
		Else
			sValue = varGetValueFromRequestObject(oProperty)
		End If
		If (oProperty.InputType = SITE_PROPERTY_SITETERM) Then
			For Each dictOption In oProperty.Options
				For Each sName In dictOption
					If (dictOption(sName) = sValue) Then
						sDisplayValue = sName
						Exit For
					End If
				Next
			Next
		Else
			sDisplayValue = sValue
		End If

		htm = cellRenderReadOnlyProperty(oProperty.Name, _
                oProperty.Attributes.DisplayName, sValue, _
                sDisplayValue)
	End If

    rowRenderReadOnlyProperty = htm
End Function


' -----------------------------------------------------------------------------
' rowRenderProperty
'
' Description:
'	Function to display all properties accessible to the current user in any
'	profile
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderProperty(ByVal sAction, _
                           ByVal sSubAction, _
                           ByVal oProperty, _
                           ByVal listProfileSchema, _
                           ByVal bValidPostedValues, _
                           ByVal rsProfile, _
                           ByVal sProfileIDs, _
                           ByVal bDisplayPostedValues)

    Dim i
    Dim url
    Dim htmTip, htmRow
    Dim sErrStr, sLastName, sFirstName, sStateCode, sCountryCode
    Dim sPropertyName, sDisplayValue
    Dim arrCell(3), arrCellAlignment(3)
    Dim mscsProperty
    Dim varPropertyValue    
    
    ' Every Property is displayed in a different HTML TR tag and has following columns
    '     1.)Column for displaying Required icon Or check box(for multi-edit)
    '     2.)Column for displaying the "Display Name" of the property
    '     3.)Column for displaying the property value in either TEXTBOX, COMBOBOX or as
    '        a SUBMIT button.
    
    ' Column 1 : Display either the required gif/blank/check box for the property
    ' Add details for the property
    arrCell(0) = sRenderHTMLPreFixForProperty(sAction, sSubAction, oProperty, sProfileIDs)    
    
    ' Column 2: Display the "Display Name" for the property
    arrCell(1) = oProperty.Attributes.DisplayName & "&nbsp;&nbsp;&nbsp;&nbsp;"
    
    ' Column 3: Display the property value in either TEXTBOX, COMBOBOX or as 
    '           SUBMIT button
    ' First get the value for the property
    varPropertyValue = varGetPropertyValue(sAction, sSubAction, oProperty, _
                         listProfileSchema, bValidPostedValues, rsProfile, _
                         sProfileIDs, bDisplayPostedValues)
    ' Display the contents of the property
    If (oProperty.Attributes.IsPrimaryKey = True) Then
        ' For PRIMARY KEY properties
        arrCell(2) = colRenderPrimaryKeyProperty(sAction, sSubAction, _
                       oProperty, sProfileIDs, varPropertyValue, _
                       bDisplayPostedValues)
        
    ElseIf (oProperty.Attributes.IsUniqueKey = True) Then
       ' For UNIQUE KEY properties
       arrCell(2) = colRenderUniqueKeyProperty(sAction, oProperty, _
                      sProfileIDs, varPropertyValue)
    Else
        ' For all other properties
        arrCell(2) = colRenderProperty(sAction, sSubAction, oProperty, _
                       sProfileIDs, varPropertyValue)
    End If
	arrCell(2) = arrCell(2) & "&nbsp;&nbsp;&nbsp;&nbsp;"

    ' General the HTML TR tag for the property
    arrCellAlignment(0) = ALIGN_LEFT
    arrCellAlignment(1) = ALIGN_LEFT
    arrCellAlignment(2) = ALIGN_LEFT
    htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")
    
    ' Add a row before displaying the property value, for displaying TIP for the 
    '     property which failed validation
    If Not bValidPostedValues Then 
        htmTip = rowRenderPropertyTip(oProperty, listProfileSchema)    
    End If

    ' Add the Property Tip and the Property TR tags    
    htmRow = htmRow & htmTip
    
    rowRenderProperty = htmRow
End Function


' -----------------------------------------------------------------------------
' sRenderHTMLPreFixForProperty
'
' Description:
'	Function for displaying "Reqired" gif Or check box(for muli-edit) before each 
'   property in the EDIT type page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sRenderHTMLPreFixForProperty(ByVal sAction, ByVal sSubAction, _
                                      ByVal oProperty, ByVal sProfileIDs)
    
    Dim url
    Dim sHTMLPreFix    
    Dim iProfileCount
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)

    If (iProfileCount = 0) OR (iProfileCount = 1) Then
        If (oProperty.Attributes.IsRequired = True) Then
            sHTMLPreFix = mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage)
        Else
            sHTMLPreFix = htmRenderBlankSpaces(4)
        End If    
    
    Else
        sHTMLPreFix = RenderCheckBox(oProperty.Name & FIELD_UPDATED, SELECTED_ATTRIBUTE_CHECKED, False, MSCSSiteStyle.CheckBox)
    End If    

    sRenderHTMLPreFixForProperty = sHTMLPreFix
End Function


' -----------------------------------------------------------------------------
' varGetPropertyValue
'
' Description:
'	Function to determine the profile property value to display
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function varGetPropertyValue(ByVal sAction, ByVal sSubAction, ByVal oProperty, _
           ByVal listProfileSchema, ByVal bValidPostedValues, ByVal rsProfile, _
           ByVal sProfileIDs, ByVal bDisplayPostedValues)
    
    Dim varPropertyValue
        
    If Not bValidPostedValues Then    
        ' Get the property dictionary from the list of properties
        varPropertyValue = varGetValueFromPropertyDictionaries(oProperty, _
                             listProfileSchema)
        
    ' Get the value from the Requset object    
    ElseIf bDisplayPostedValues Then        
        varPropertyValue = varGetValueFromRequestObject(oProperty)
        
    Else
        ' Determine the property value depending upon the action type        
        varPropertyValue = varUpdateValueForCurrentAction(sAction, _
                             sSubAction, rsProfile, oProperty, sProfileIDs)
    End If
    
    varGetPropertyValue = varPropertyValue
End Function


' -----------------------------------------------------------------------------
' varGetValueFromPropertyDictionaries
'
' Description:
'	Function to get the property value from list of property dictionaries
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function varGetValueFromPropertyDictionaries(ByVal oProperty, ByVal listProfileSchema)
    Dim i
    Dim htmRow
    Dim arrCell, arrCellAlignment
    Dim mscsProperty
    Dim varPropertyValue
                            
    ' Loop through the list of property dictionaries (for current 
    ' profile type)to get the current property dictionary
    For i = 0 To (listProfileSchema.Count - 1)
        If StrComp(listProfileSchema(i).Name, oProperty.Name, vbTextCompare) = 0 Then
            Set mscsProperty = listProfileSchema(i)
            Exit For
        End If
    Next
    varPropertyValue = mscsProperty.fldValue

    Set mscsProperty = nothing
    
    varGetValueFromPropertyDictionaries = varPropertyValue
End Function


' -----------------------------------------------------------------------------
' varGetValueFromRequestObject
'
' Description:
'	Function to get the property value from the underlying REQUEST object
'   (Part of the URL or HTML FORM)    
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function varGetValueFromRequestObject(ByVal oProperty)
    Dim varPropertyValue
    
    Select Case oProperty.Type
    Case DATA_TYPE_STRING
         varPropertyValue = GetRequestString(oProperty.Name, Null)             
    Case DATA_TYPE_NUMBER
         varPropertyValue = MSCSAppFrameWork.RequestNumber(oProperty.Name, Null)
    End Select

    varGetValueFromRequestObject = varPropertyValue
End Function


' -----------------------------------------------------------------------------
' varUpdateValueForCurrentAction
'
' Description:
'	Function to get the property value based on the current action type
'   as stored in the global variable g_sAction
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function varUpdateValueForCurrentAction(ByVal sAction, ByVal sSubAction, _
           ByVal rsProfile, ByVal oProperty, ByVal sProfileIDs)

    Dim iProfileCount
    Dim sPropertyName, sSelectProfileIDs
    Dim varPropertyValue
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
            
    ' Check the current action
    Select Case sAction
                   
        ' If adding a new profile with values from exisiting profile     
        Case COPY_ACTION
             If (oProperty.Attributes.IsPrimaryKey = True) Or _
                (oProperty.Attributes.IsJoinKey = True) Or _
                (oProperty.Attributes.IsUniqueKey = True) Then
                varPropertyValue = Null
             Else
                varPropertyValue = rsProfile(oProperty.QualifiedName).Value             
             End If
            
        ' If editing an exisiting profile     
        Case EDIT_ACTION             
             If iProfileCount > 1 Then                
                ' While editing multiple profile
                varPropertyValue = Null
                
             ElseIf (iProfileCount = 0) OR (iProfileCount = 1) Then
                ' While editing one profile             
                varPropertyValue = rsProfile(oProperty.QualifiedName).Value
             End If
             
        Case Else
            If iProfileCount = 1 Then
                If IsObject(rsProfile) Then
                    varPropertyValue = rsProfile(oProperty.QualifiedName).Value
                End If
            Else
                varPropertyValue = Null
            End If
    End Select
    
    varUpdateValueForCurrentAction = varPropertyValue
End Function


' -----------------------------------------------------------------------------
' colRenderPrimaryKeyProperty
'
' Description:
'	Function to display Primary Key property's value
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderPrimaryKeyProperty(ByVal sAction, ByVal sSubAction, _
           ByVal oProperty, ByVal sProfileIDs, ByVal varPropertyValue, _
           ByVal bDisplayPostedValues)
    Dim htm    
    Dim sProfileType
    Dim iProfileCount
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    sProfileType = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_PROFILE)
    
    ' If a new profile is getting added then display a text box
    If (StrComp(sAction, ADD_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sAction, COPY_ACTION, vbTextCompare) = 0) OR _
       (bDisplayPostedValues) Then
         htm = RenderTextBox(oProperty.Name, varPropertyValue, _
                 DEFAULT_TEXT_BOX_WIDTH, DEFAULT_TEXT_BOX_WIDTH, _
				 MSCSSiteStyle.TextBox)
    Else
         ' For single profile editing
         If (iProfileCount = 0) OR (iProfileCount = 1) Then
         
             ' Special handler for UserObject and Oganization profiles
             If (StrComp(sProfileType, PROFILE_TYPE_USER, vbTextCompare) = 0) Or _
                (StrComp(sProfileType, PROFILE_TYPE_ORG, vbTextCompare) = 0) Then
                 
                 ' For the case in which AD data store is supported, the primary key
                 '    value is read-only as its not possible to updated the cn/ou 
                 '    properties in AD.
                 If MSCSProfileServiceUsesActiveDirectory Then
                     htm = varPropertyValue & _
                           RenderHiddenField(oProperty.Name, varPropertyValue)
                 Else
                     ' For the case when AD data store in not supported
                     htm = RenderTextBox(oProperty.Name, _
                                         varPropertyValue, _
                                         DEFAULT_TEXT_BOX_WIDTH, _
                                         DEFAULT_TEXT_BOX_WIDTH, _
                                         MSCSSiteStyle.TextBox)
                 End If                                     
             Else
                 ' For all other profile types
                 htm = RenderTextBox(oProperty.Name, _
                                     varPropertyValue, _
                                     DEFAULT_TEXT_BOX_WIDTH, _
                                     DEFAULT_TEXT_BOX_WIDTH, _
                                     MSCSSiteStyle.TextBox)
             End If
         Else
             ' For multiple profile editing
             htm = mscsMessageManager.GetMessage("L_MSG_UNIQUE_PROPERTY_Text", sLanguage)
         End If
    End If

    colRenderPrimaryKeyProperty = htm
End Function


' -----------------------------------------------------------------------------
' colRenderUniqueKeyProperty
'
' Description:
'	Function to display the UNIQUE KEY properties
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderUniqueKeyProperty(ByVal sAction, ByVal sSubAction, _
           ByVal oProperty, ByVal sProfileIDs, ByVal varPropertyValue)
    Dim htm
    Dim iProfileCount
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    
    ' Display text box while adding a new profile       
    If (StrComp(sAction, ADD_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sAction, COPY_ACTION, vbTextCompare) = 0) Then
         htm = RenderTextBox(oProperty.Name, _
                             varPropertyValue, _
                             DEFAULT_TEXT_BOX_WIDTH, _
                             DEFAULT_TEXT_BOX_WIDTH, _
                             MSCSSiteStyle.TextBox)
    
    ' If updating an exisiting profile, check if more than on records are
    '     selected                            
    ElseIf (sAction = EDIT_ACTION) Then
        
         ' For single profile editing
         If (iProfileCount = 0) OR (iProfileCount = 1) Then
             htm = RenderTextBox(oProperty.Name, _
                                 varPropertyValue, _
                                 DEFAULT_TEXT_BOX_WIDTH, _
                                 DEFAULT_TEXT_BOX_WIDTH, _
                                 MSCSSiteStyle.TextBox)
         Else
             ' For multi-edit
             htm = mscsMessageManager.GetMessage("L_MSG_UNIQUE_PROPERTY_Text", sLanguage) & _
                   RenderHiddenField(oProperty.Name, mscsMessageManager.GetMessage("L_MSG_UNIQUE_PROPERTY_Text", sLanguage))    
         End If
    End If
    
    colRenderUniqueKeyProperty = htm           
End Function


' -----------------------------------------------------------------------------
' colRenderProperty
'
' Description:
'	Function to display the contents of any EDIT type page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderProperty(ByVal sAction, ByVal sSubAction, ByVal oProperty, _
           ByVal sProfileIDs, ByVal varPropertyValue)
    Const SITE_PROPERTY_SITETERM = 10
    Const SITE_PROPERTY_BOOLEAN	 = 11

    Dim htm
    Dim sCountryCode
           
    ' Check if property requires any custom handling
    Select Case oProperty.Name
        Case FIELD_USER_PASSWORD  
             htm = colRenderPasswordProperty(sAction, _
                                             sSubAction, _
                                             oProperty, _
                                             varPropertyValue)
                 
        ' Handler for properties which are of reference type (having guid of other
        '        profiles)          
        Case FIELD_ORG_CONTACT, _
             FIELD_ORG_PURCHASING_MANAGER, _
             FIELD_ORG_RECEIVING_MANAGER
             htm = colRenderForeignKeyProperty(sAction, _
                                               sSubAction, _
                                               oProperty, _
                                               sProfileIDs, _                                                  
                                               varPropertyValue)
                 
        Case FIELD_USER_STATUS
             htm = htmRenderUserStatusOptions(varPropertyValue, oProperty.IsRequired)

        Case FIELD_USER_PARTNERDESK_ROLE
             htm = htmRenderPartnerDeskRoles(varPropertyValue, True)

        Case Else
			 Select Case oProperty.InputType
			 Case SITE_PROPERTY_BOOLEAN
				Dim bValue

				If  IsNull(varPropertyValue) Then
					bValue = False
				Else
					bValue = varPropertyValue
				End If
				htm = htmRenderBoolean (oProperty.Name, bValue)

             Case SITE_PROPERTY_SITETERM
				htm = htmRenderSiteTerm (oProperty, varPropertyValue)
			 Case Else
				' Display text box for all other properties
				htm = RenderTextBox(oProperty.Name, varPropertyValue, _
                         DEFAULT_TEXT_BOX_WIDTH, DEFAULT_TEXT_BOX_WIDTH, _
                         MSCSSiteStyle.TextBox)
			 End Select
    End Select

    colRenderProperty = htm
End Function


' -----------------------------------------------------------------------------
' colRenderPasswordProperty
'
' Description:
'	Function to display user_security_password property
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderPasswordProperty(ByVal sAction, ByVal sSubAction, _
                                   ByVal oProperty, ByVal varPropertyValue)
    
    Dim htm, sBtnText

    ' Display the text box when a new profile is getting added
    If (StrComp(sAction, ADD_ACTION, vbTextCompare) = 0) OR _
       (StrComp(sAction, COPY_ACTION, vbTextCompare) = 0) Then
        htm = RenderPasswordBox(oProperty.Name, varPropertyValue, DEFAULT_TEXT_BOX_WIDTH, DEFAULT_TEXT_BOX_WIDTH, MSCSSiteStyle.PasswordBox)
    
    ' Else, display the change submit button
    ElseIf (StrComp(sAction, EDIT_ACTION, vbTextCompare) = 0) AND _
           ( _
               (StrComp(sSubAction, ADD_ACTION, vbTextCompare) = 0) OR _
               (StrComp(sSubAction, COPY_ACTION, vbTextCompare) = 0) _
           ) Then
         htm = RenderPasswordBox(oProperty.Name, varPropertyValue, DEFAULT_TEXT_BOX_WIDTH, DEFAULT_TEXT_BOX_WIDTH, MSCSSiteStyle.PasswordBox)
    Else
        sBtnText = mscsMessageManager.GetMessage("L_CHANGE_ACTION_BUTTON_Text", sLanguage)
        htm = RenderSubmitButton(BUTTON_SUBMIT & CHANGE_ACTION, sBtnText, MSCSSiteStyle.Button)
    End If

    colRenderPasswordProperty = htm
End Function


' -----------------------------------------------------------------------------
' colRenderForeignKeyProperty
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderForeignKeyProperty(ByVal sAction, ByVal sSubAction, _
           ByVal oProperty, ByVal sProfileIDs, ByVal varPropertyValue)
    Dim htm, sBtnText
    Dim sDisplayValue
    Dim iProfileCount
    Dim sSelectFieldName
    
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    sSelectFieldName = sGetSelectActionPropertyName(sAction)
    
    Select Case sAction
    ' Display Current user properties when adding a new profile
    Case ADD_ACTION
         sDisplayValue = sGetUserLoginName(m_UserID)
         htm = sDisplayValue
         htm = htm & RenderHiddenField(oProperty.Name, m_UserID)
         
    ' Display selected user properties when adding a new user by copying pre-existing profile
    Case COPY_ACTION
         If Not IsNull(varPropertyValue) Then
            sDisplayValue = sGetUserLoginName(varPropertyValue)
         End If   
         htm = sDisplayValue
         htm = htm & RenderHiddenField(oProperty.Name, varPropertyValue)
         
    ' For other action types
    Case Else
         If Not IsNull(varPropertyValue) Then
            sDisplayValue = sGetUserLoginName(varPropertyValue)
         End If
         
         sBtnText = SELECT_ACTION
         htm = sDisplayValue & RenderSubmitButton(BUTTON_SELECT & oProperty.Name, sBtnText, MSCSSiteStyle.Button)
         htm = htm & RenderHiddenField(oProperty.Name, varPropertyValue)
    End Select

    colRenderForeignKeyProperty = htm
End Function


' -----------------------------------------------------------------------------
' rowRenderPropertyTip
'
' Description:
'	Function to add an HTML TR tag for displaying property Tip
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function rowRenderPropertyTip(ByVal oProperty, ByVal listProfileSchema)
    Dim i
    Dim htmRow
    Dim arrCell(2)
    Dim mscsProperty
    Dim arrCellAlignment(2)
    
    ' Loop through the list of property dictionaries (for current 
    '     profile type)to get the current property dictionary
    For i = 0 To (listProfileSchema.Count - 1)
        If StrComp(listProfileSchema(i).Name, oProperty.Name, vbTextCompare) = 0 Then
            Set mscsProperty = listProfileSchema(i)
            Exit For
        End If
    Next    
        
    If mscsProperty.Err Then
        arrCell(0) = htmRenderBlankSpaces(4)
        arrCell(1) = htmRenderBlankSpaces(4)
        arrCell(2) = colRenderPropertyTip(mscsMessageManager.GetMessage("L_MSG_FIELD_VALIDATION_Text", sLanguage))
        arrCellAlignment(0) = ALIGN_LEFT
        arrCellAlignment(1) = ALIGN_LEFT
        arrCellAlignment(2) = ALIGN_LEFT
        htmRow = TRTag(arrCell, arrCellAlignment, MSCSSiteStyle.ContentsBodyTable, False, "")
    End If
    
    Set mscsProperty = nothing    
    
    rowRenderPropertyTip = htmRow
End Function


' -----------------------------------------------------------------------------
' colRenderPropertyTip
'
' Description:
'	Function to display tip
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function colRenderPropertyTip(ByVal sTip)
    colRenderPropertyTip = RenderText(FormatOutput(CONTROL_ERROR_TEMPLATE, Array(mscsMessageManager.GetMessage("L_Error_Emphasis_Symbol_Text", sLanguage), sTip)), MSCSSiteStyle.Warning)
End Function
%>