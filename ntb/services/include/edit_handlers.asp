<%
' =============================================================================
' edit_handlers.asp
' Add, Delete, Copy etc. action handlers for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' PrepareAddAction
'
' Description:
'	"Add" action handler, called from partner/edit_user.asp
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareAddAction(ByRef sStatusField, ByRef iStatusID, _
		ByRef bValidPostedValues, ByRef bDisplayPostedValues, _
		ByRef listProfileSchema)
    sStatusField = STATUS_FIELD_VALUE_SUCCESS
    iStatusID = S_ADDING_NEW_PROFILE
    bValidPostedValues = True
    bDisplayPostedValues = False
    
    ' Get the simple list of profile property dictionaries
    Set listProfileSchema = listCloneProfileSchema(sThisPage)    
End Sub


' -----------------------------------------------------------------------------
' htmAddActionHandler
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmAddActionHandler(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
		ByVal sStatusField, ByVal iStatusID, ByVal bValidPostedValues, _
		ByVal bDisplayPostedValues, ByVal listProfileSchema)
    
    ' Render a blank EDIT page for any profile
    htmAddActionHandler = htmRenderBlankEditPage( _
                                 sAction, _
                                 sSubAction, _
                                 sProfileIDs, _
                                 sStatusField, _
                                 iStatusID, _
                                 bValidPostedValues, _
                                 bDisplayPostedValues, _
                                 listProfileSchema)
End Function


' -----------------------------------------------------------------------------
' PrepareCopyAction
'
' Description:
'	"Copy" action handler
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareCopyAction(ByVal sSubAction, ByVal sProfileIDs, ByRef iProfileCount, _
		ByRef iStatusID, ByRef sStatusField, ByRef bValidPostedValues, _
		ByRef bDisplayPostedValues, ByRef sProfileListPage, ByRef listProfileSchema)
	' Initialize
	sProfileListPage = ""
	iStatusID = Null
	Set listProfileSchema = Nothing
	
    ' Get the count of profiles
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    
    ' Check if one profile is selected, copy action requires that the user
    ' selects only one profile in the list page
    If iProfileCount = 1 Then    
        ' Display the edit page with selected profile details        
        iStatusID = Null
        bValidPostedValues = True
        sStatusField = Null
        bDisplayPostedValues = False
        
        ' Get the simple list of profile property dictionaries
        Set listProfileSchema = listCloneProfileSchema(sThisPage)    
    Else
        ' Get the corresponding list page name for this page
        sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
	End If
End Sub


' -----------------------------------------------------------------------------
' htmCopyActionHandler
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmCopyActionHandler(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
		ByVal iProfileCount, _
		ByVal iStatusID, ByVal sStatusField, ByVal bValidPostedValues, _
		ByVal bDisplayPostedValues, ByVal sProfileListPage, ByVal listProfileSchema)
    Dim htm    
    
    If iProfileCount = 1 Then    
        ' Render edit page contents
        htm = htmRenderEditPage(sAction, _
                                sSubAction, _
                                sProfileIDs, _
                                sStatusField, _
                                iStatusID, _
                                bValidPostedValues, _
                                bDisplayPostedValues, _
                                listProfileSchema)
    Else
        Call ReDirectToPage(sProfileListPage, _
                            Array(STATUS_FIELD_NAME, STATUS_FIELD_ID), _
                            Array(STATUS_FIELD_VALUE_ERROR, E_SELECT_ONE_RECORD))
    End If
    
    htmCopyActionHandler = htm
End Function


' -----------------------------------------------------------------------------
' PrepareEditAction
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareEditAction(ByVal sSubAction, ByVal sProfileIDs, ByRef iProfileCount, _
						ByRef listProfileSchema)
    ' Check to see if user wants to edit all the profiles
    ' Get the count of profiles
    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    If iProfileCount >= 1 Then            
		' Get the simple list of profile property dictionaries
        Set listProfileSchema = listCloneProfileSchema(sThisPage)
	End If
End Sub


' -----------------------------------------------------------------------------
' htmEditAction
'
' Description:
'	"Edit" action handler display. This function supports single-edit,
'   multi-edit and SelectAll+Edit scenarios.
'	Business logic to prepare for this display is in
'	PrepareEditAction.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmEditAction(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
							ByVal iProfileCount, ByRef listProfileSchema)
    Dim htm
    Dim iStatusID
    Dim bValidPostedValues, bDisplayPostedValues
    Dim sActionPage, sStatusField, sProfileListPage
    
    If iProfileCount = -1 Then    
       ' Call the multi-edit action handler for all the profiles returned by the 
       '      search criteria
       htm = EditAllProfiles(sAction, sSubAction, sProfileIDs)
    Else
        ' Else, Check if at least one profile is selected        
        If iProfileCount > 1 Then
            ' Editing multiple profiles
            iStatusID = S_EDITING_MULTIPLE_PROFILES
            sStatusField = STATUS_FIELD_VALUE_SUCCESS
        Else
            ' Editing single profile
            sStatusField = STATUS_FIELD_VALUE_SUCCESS
            iStatusID = S_EDITING_EXISTING_PROFILE            
        End If
        
        ' Check if atleast one profile is selected
        If iProfileCount >= 1 Then            
            ' Render edit page contents for the selected profile/s
            '     This function supports multi-edit
            bValidPostedValues = True
            bDisplayPostedValues = False            
                        
            ' Render edit page contents
            htm = htmRenderEditPage(sAction, sSubAction, sProfileIDs, _
                                    sStatusField, iStatusID, bValidPostedValues, _
                                    bDisplayPostedValues, listProfileSchema)
        Else ' iProfileCount = 0
            ' Get the corresponding list page name for this page
            sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
        
            ' Else, Redirect to the list page
            Call ReDirectToPage(sProfileListPage, _
                                Array(STATUS_FIELD_NAME, STATUS_FIELD_ID), _
                                Array(STATUS_FIELD_VALUE_ERROR, E_NO_RECORD_SELECTED))
        End If
    End If
    htmEditAction = htm
End Function


' -----------------------------------------------------------------------------
' PrepareEditAllAction
'
' Description:
'	"Edit All" action handler, this function is used to edit all the 
'   profiles based on the search criteria selected by the user
'   in the corresponding list page.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareEditAllAction(ByRef iStatusID, ByRef sStatusField, ByRef bValidPostedValues, _
					ByRef bDisplayPostedValues, ByRef listProfileSchema)
    iStatusID = S_EDITING_MULTIPLE_PROFILES    
    sStatusField = STATUS_FIELD_VALUE_SUCCESS
    bValidPostedValues = True
    bDisplayPostedValues = False
    
    ' Get the simple list of profile property dictionaries
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
End Sub


' -----------------------------------------------------------------------------
' htmEditAllProfiles
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmEditAllProfiles(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
					ByVal iStatusID, ByVal sStatusField, ByVal bValidPostedValues, _
					ByVal bDisplayPostedValues, ByVal listProfileSchema)
    ' Render edit page contents
    htmEditAllProfiles = htmRenderEditPage( _
                            sAction, _
                            sSubAction, _
                            sProfileIDs, _
                            sStatusField, _
                            iStatusID, _
                            bValidPostedValues, _
                            bDisplayPostedValues, _
                            listProfileSchema)
End Function


' -----------------------------------------------------------------------------
' PrepareDeleteAction
'
' Description:
'	"Delete" action handler, this function support multi-edit but
'   will not be called for deleting all the profiles(using 
'   Select All option)
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareDeleteAction(ByVal sSubAction, ByVal sProfileIDs, ByRef sProfileListPage, _
						ByRef iProfileCount)
    ' Get the corresponding list page name for this page
    sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
    
    ' Check if confirmation (message box) page is already displayed
    Select Case sSubAction    
        ' User has opted to delete the profiles
        Case MSGBOX_ACTION_YES        
             ' Recursively delete the selected profiles
             Call DeleteSelectedProfiles(sSubAction, sProfileIDs)             
                                                                                          
        ' The user has declined to delete the profiles
        Case MSGBOX_ACTION_NO
			' No special preparation necessary             

        ' The confirmation (message box) page is not yet displayed
        Case Else        
             ' Check if the user want to delete all the profiles returned by 
             '    the search criteria or just the few selected profiles
             ' Get the count of profiles
             iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    End Select
End Sub


' -----------------------------------------------------------------------------
' htmDeleteActionHandler
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmDeleteActionHandler(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
						ByVal sProfileListPage, ByVal iProfileCount)
    Dim htm    

    Select Case sSubAction    
        ' User has opted to delete the profiles
        Case MSGBOX_ACTION_YES        
             ' Redirect to the list page
             Call ReDirectToPage(sProfileListPage, _
                                 Array(STATUS_FIELD_NAME, STATUS_FIELD_ID), _
                                 Array(STATUS_FIELD_VALUE_SUCCESS, S_ENTITY_RECORDS_DELETED))
                                                                                          
        ' The user has declined to delete the profiles, redirect to the list page
        Case MSGBOX_ACTION_NO
             Call ReDirectToPage(sProfileListPage, Null, Null)
             
        ' The confirmation (message box) page is not yet displayed
        Case Else        
             If iProfileCount = -1 Then
                ' Delete all the profiles returned by the search criteria
                htm = htm & htmRenderMessageBoxForDeleteAll()
                
             ElseIf iProfileCount > 0 Then
                    ' Delete only the profiles selected by the user 
                    htm = htm & htmRenderMessageBoxForDelete()
             Else
             
                ' The user has not selected any profiles, redirect to the list page
                Call ReDirectToPage(sProfileListPage, _
                                    Array(STATUS_FIELD_NAME, STATUS_FIELD_ID), _
                                    Array(STATUS_FIELD_VALUE_ERROR, E_NO_RECORD_SELECTED))
             End If
    End Select
    
    htmDeleteActionHandler = htm
End Function


' -----------------------------------------------------------------------------
' PrepareDeleteAllAction
'
' Description:
'	"Delete all" action handler, these subs will be called only if the user
'   has selected to deleted all the profiles returned by the search criteria.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareDeleteAllAction(ByRef sProfileListPage)
    ' Get the corresponding list page name for this page
    sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
    
    ' Get the user's search criteria for the corresponding list page
    Set dictSearchOptions = dictGetSearchDictionaryForPage(sProfileListPage)

    ' Check the user action on the DeleteAll message box page            
    Select Case sSubAction
    ' User has opted to delete all the profiles
    Case MSGBOX_ACTION_YES             
        ' Delete all the profiles returned by user's search criteria
        Call DeleteAllProfiles(dictSearchOptions)         
    Case MSGBOX_ACTION_NO
		' No special preparation to do
    End Select
    
    Set dictSearchOptions = Nothing
End Sub


' -----------------------------------------------------------------------------
' DeleteAllActionHandler
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub DeleteAllActionHandler(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
			ByVal sProfileListPage)
    ' Check the user action on the DeleteAll message box page            
    Select Case sSubAction
    ' User has opted to delete all the profiles
    Case MSGBOX_ACTION_YES             
         ' Redirect to the list page
         Call ReDirectToPage(sProfileListPage, _
                             Array(STATUS_FIELD_NAME, STATUS_FIELD_ID), _
                             Array(STATUS_FIELD_VALUE_SUCCESS, S_ENTITY_RECORDS_DELETED))
                            
    Case MSGBOX_ACTION_NO
         ' User has declined to delete the profiles, redirec to the list page
         Call ReDirectToPage(sProfileListPage, _
                             Array(ACTION_TYPE, SUB_ACTION_TYPE), _
                             Array(LIST_ACTION, SELECTALL_ACTION))
             
    End Select
End Sub


' -----------------------------------------------------------------------------
' htmChangePasswordActionHandler
'
' Description:
'	"Password change" action, this function is called by the EDIT type pages for
'   changing the user_security_password property. This function will be 
'   called before the execution is passed to change_password.asp page. The
'   functions checks if any property values in EDIT page were updated and 
'   redirect accordingly
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmChangePasswordActionHandler(ByRef sAction, ByVal sSubAction, ByVal sProfileIDs, _
									ByRef sPreviousAction, ByRef listProfileSchema, _
									ByRef bValidPostedValues, ByRef bDisplayPostedValues)
    Dim htm
    Dim bDirty, bUpdated
	Dim arrVals, arrParams
	Dim sActionPage, sSourcePage, sPropertyName
        
    ' URL to ChangePassword page that includes the source page in the URL
    sSourcePage = GenerateURL(sThisPage, Array(ACTION_TYPE, SUB_ACTION_TYPE, GUIDS), Array(EDIT_ACTION, sSubAction, sProfileIDs))
	arrParams = Array( _
	                     ACTION_TYPE, _
	                     SUB_ACTION_TYPE, _	                     
	                     SOURCE_PAGE, _
	                     GUIDS _
	                 )
	arrVals = Array( _
	                   CHANGE_ACTION, _
	                   sSubAction, _	                   
	   		           sSourcePage, _
	   		           sProfileIDs _
	   		       )
	sActionPage = GenerateURL(MSCSSitePages.ChangePassword, arrParams, arrVals)
	
	' Check if confirmation (message box) page is already displayed, since the user
    '     is leaving the editing mode to another page and some modifications may
    '     have been left un-saved.
	htmChangePasswordActionHandler = htmSaveChangesConfirmationHandler(sAction, _
            sSubAction, sProfileIDs, sActionPage, sPreviousAction, _
			listProfileSchema, bValidPostedValues, bDisplayPostedValues)
End Function


' -----------------------------------------------------------------------------
' PrepareSaveAction
'
' Description:
'	"Save" action handler, this function is used for saving all the profiles
'   and supports single, multi and edit-All scenarios
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareSaveAction()
	' No specific preparation in place here.
	' Preparations for specific subactions are called from
	' htmSaveActionHandler because that is more efficient and
	' easier to maintain; in this case, breaking up presentation
	' and business logic makes the code harder to understand
	' and maintain...
	' If your custom solution needs to prepare anything before
	' calling htmSaveActionHandler, put the code here
End Sub


' -----------------------------------------------------------------------------
' htmSaveActionHandler
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmSaveActionHandler(ByRef sAction, ByVal sSubAction, ByRef sProfileIDs)
    Dim htm
    Dim bInserted, bUpdated, bValidPostedValues, bDisplayPostedValues
    Dim sSourcePage, sStatusField
    Dim iStatusID
    Dim listProfileSchema
        
    ' Check if user is saving all the profiles returned by the
    '     search criteria in the corresponding list page
    Select Case sSubAction
		Case SELECTALL_ACTION
	        ' Call Action handler for updating all the profiles returned
		    '     by the search criteria.
		    Call PrepareSaveAllAction(sStatusField, iStatusID, _
					bValidPostedValues, bDisplayPostedValues, _
					listProfileSchema)
			htm = htmSaveAllActionHandler(sAction, sSubAction, sProfileIDs, _
					sStatusField, iStatusID, bValidPostedValues, _
					bDisplayPostedValues, listProfileSchema)
		Case ADD_ACTION, COPY_ACTION
			Call PrepareInsertProfile(sSubAction, bInserted, sProfileIDs, _
					sStatusField, iStatusID, bValidPostedValues, _
					bDisplayPostedValues, listProfileSchema)
			htm = htmInsertProfile(sAction, sSubAction, bInserted, sProfileIDs, _
					sStatusField, iStatusID, bValidPostedValues, _
					bDisplayPostedValues, listProfileSchema)
		Case EDIT_ACTION
			Call PrepareSaveProfiles(sAction, sSubAction, sProfileIDs, bUpdated, _
					sSourcePage, sStatusField, iStatusID, bValidPostedValues, _
					bDisplayPostedValues, listProfileSchema)
			htm = htmSaveProfiles(sAction, sSubAction, sProfileIDs, bUpdated, _
					sSourcePage, sStatusField, iStatusID, bValidPostedValues, _
					bDisplayPostedValues, listProfileSchema)
    End Select
    
    htmSaveActionHandler = htm
End Function


' -----------------------------------------------------------------------------
' PrepareInsertProfile
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareInsertProfile(ByVal sSubAction, ByRef bInserted, _
				ByRef sProfileIDs, ByRef sStatusField, ByRef iStatusID, _
				ByRef bValidPostedValues, ByRef bDisplayPostedValues, _
				ByRef listProfileSchema)
	Dim bProfileExists
	
	' Initialize
    bInserted = True
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
    bValidPostedValues = ValidateFields(listProfileSchema)
    bProfileExists = False
    sStatusField = ""
    iStatusID = -1
    
    ' Go
    If bValidPostedValues Then   
        bProfileExists = bCheckIfProfileExists(sStatusField, iStatusID)
        If Not bProfileExists Then            
            bInserted = bInsertProfile(sSubAction, sStatusField, _
                           iStatusID, sProfileIDs)
            If bInserted Then
                sStatusField = STATUS_FIELD_VALUE_SUCCESS
                iStatusID = S_NEW_ENTITY_RECORD_ADDED
            End If    
        Else
            bInserted = False
            bDisplayPostedValues = True    
        End If
    Else
        bInserted = False
        sStatusField = STATUS_FIELD_VALUE_ERROR
        iStatusID = E_USER_INPUT_ERROR
        bDisplayPostedValues = True
    End If
End Sub


' -----------------------------------------------------------------------------
' htmInsertProfile
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmInsertProfile(ByVal sAction, ByVal sSubAction, ByVal bInserted, _
				ByVal sProfileIDs, ByVal sStatusField, ByVal iStatusID, _
				ByVal bValidPostedValues, ByVal bDisplayPostedValues, _
				ByVal listProfileSchema)    
    Dim htm

    If bInserted Then
        bDisplayPostedValues = False
        htmInsertProfile = htmRenderEditPage(EDIT_ACTION, _
                                EDIT_ACTION, _
                                sProfileIDs, _
                                sStatusField, _
                                iStatusID, _
                                bValidPostedValues, _
                                bDisplayPostedValues, _
                                listProfileSchema)
    Else
        bDisplayPostedValues = True
        htmInsertProfile = htmRenderBlankEditPage( _
                                     ADD_ACTION, _
                                     sSubAction, _
                                     sProfileIDs, _
                                     sStatusField, _
                                     iStatusID, _
                                     bValidPostedValues, _
                                     bDisplayPostedValues, _
                                     listProfileSchema _
                                 )
    End If
End Function
    

' -----------------------------------------------------------------------------
' PrepareSaveProfiles
'
' Description:
'	Handle "Save Profiles" action, fill in variables to be used by the
'	rendering function htmSaveProfiles
'
' Parameters: (all OUT except first 3)
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'	bUpdated					- Are the profiles updated now?
'	sSourcePage					- Source page
'	sStatusField				- Status field name in profile
'	iStatusID					- Contents of status field in profile
'	bValidPostedValues			- Are all posted values valid?
'	bDisplayPostedValues		- Must these values be displayed?
'	listProfileSchema			- SimpleList object containing profile schema
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareSaveProfiles(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
				ByRef bUpdated, ByRef sSourcePage, ByRef sStatusField, _
				ByRef iStatusID, ByRef bValidPostedValues, _
				ByRef bDisplayPostedValues, ByRef listProfileSchema)
    Dim bProfileExists
    Dim iProfileCount
    Dim rsProfile
    Dim listValidProperties

	' Initialize
    bUpdated = True
    bProfileExists = False
    bValidPostedValues = False
    bDisplayPostedValues = False
    sSourcePage = GetRequestString(SOURCE_PAGE, Null)
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
    Set rsProfile = Nothing

    iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)
    ' User is editing one or more profiles
    If iProfileCount = 1 Then
        ' single-edit
		Set rsProfile = rsGetProfileDetails(sProfileIDs)
		bValidPostedValues = ValidateFields(listProfileSchema)

        If bValidPostedValues Then
			bProfileExists = bCheckIfProfileExists(sStatusField, iStatusID)
		If Not bProfileExists Then
			bUpdated = bSaveProfileDetails(sSubAction, sProfileIDs, _
                           sStatusField, iStatusID, rsProfile)
			Else
				bUpdated = False
			End If
        End If
    ElseIf iProfileCount > 1 Then
        ' Multi-edit                
        Set listValidProperties = listGetValidPropertiesForMultiEdit(listProfileSchema)
                
        ' Validate only the properties entered by the user
        bValidPostedValues = ValidateFields(listValidProperties)
        If bValidPostedValues Then                        
            bUpdated = bSaveProfileDetails(sSubAction, sProfileIDs, _
                           sStatusField, iStatusID, rsProfile)
        Else
            bUpdated = False
            ' Reconstruct the profile schema to include the properties which
            '     have failed the validation
            Set listProfileSchema = listReConstructPropertiesSchema(listValidProperties)
        End If
    End If
    If bUpdated Then
        sStatusField = STATUS_FIELD_VALUE_SUCCESS
        iStatusID = S_ENTITY_RECORD_UPDATED
        bDisplayPostedValues = False
    Else
        bDisplayPostedValues = True                
    End If
    
    ' Clean up
    Set rsProfile = Nothing
End Sub


' -----------------------------------------------------------------------------
' htmSaveProfiles
'
' Description:
'	Rendering function to display "Save Profiles" action output
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmSaveProfiles(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
				ByVal bUpdated, ByVal sSourcePage, ByVal sStatusField, _
				ByVal iStatusID, ByVal bValidPostedValues, _
				ByVal bDisplayPostedValues, ByVal listProfileSchema)
    ' Check if this page is called from some other page, this can be
    '     used from a site page where the user needs to updated his
    '     properties and he needs to be taken
    '     back to the same page once the profile is saved
    If (bUpdated) AND (Not IsNull(sSourcePage)) Then            
        Response.Redirect(sSourcePage)
    End If
                        
    ' Render edit page contents
    htmSaveProfiles = htmRenderEditPage( _
                            EDIT_ACTION, _
                            sSubAction, _
                            sProfileIDs, _
                            sStatusField, _
                            iStatusID, _
                            bValidPostedValues, _
                            bDisplayPostedValues, _
                            listProfileSchema _
                        )
End Function


' -----------------------------------------------------------------------------
' PrepareSaveAllAction
'
' Description:
'	"Save all" action handler is used to save the new property values entered
'   by the user in all profiles returned from the search criteria, in the
'   corresponding list page
'
' Parameters:
'	sStatusField				- Status field name in profile
'	iStatusID					- Contents of status field in profile
'	bValidPostedValues			- Are all posted values valid?
'	bDisplayPostedValues		- Must these values be displayed?
'	listProfileSchema			- SimpleList object containing profile schema
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareSaveAllAction(ByRef StatusField, ByRef iStatusID, _
       ByRef bValidPostedValues, ByRef bDisplayPostedValues, _
       ByRef listProfileSchema)
	Dim listValidProperties
	Dim dictSearchOptions
	
    sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
    Set dictSearchOptions = dictGetSearchDictionaryForPage(sProfileListPage)
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
    
    ' Validate only the properties involved in the save all process
    Set listValidProperties = listGetValidPropertiesForMultiEdit(listProfileSchema)
    bValidPostedValues = ValidateFields(listValidProperties)    
    If bValidPostedValues Then                        
        ' Save all the profiles with the new property values entered by the user
        Call SaveAllSelectedProfiles(sStatusField, iStatusID, dictSearchOptions)
    Else        
        ' Reconstruct the profile schema to include the properties which
        '     have failed the validation
        Set listProfileSchema = listReConstructPropertiesSchema(listValidProperties)
    End If
    
    bDisplayPostedValues = False
    Set listValidProperties = Nothing
    Set dictSearchOptions = Nothing
End Sub


' -----------------------------------------------------------------------------
' htmSaveAllActionHandler
'
' Description:
'	Rendering function to display "Save All" action output
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmSaveAllActionHandler(ByVal sAction, ByRef sSubAction, ByRef sProfileIDs, _
			ByVal StatusField, ByVal iStatusID, ByVal bValidPostedValues, _
			ByVal bDisplayPostedValues, ByVal listProfileSchema)
    ' Render edit page contents
    htmSaveAllActionHandler = htmRenderEditPage( _
                            sAction, _
                            sSubAction, _
                            sProfileIDs, _
                            sStatusField, _
                            iStatusID, _
                            bValidPostedValues, _
                            bDisplayPostedValues, _
                            listProfileSchema)
End Function


' -----------------------------------------------------------------------------
' GetStatusFieldAndID
'
' Description:
'	Helper Sub to update the Status message for save action
'
' Parameters:
' Parameters: (all OUT except first 4)
'	sSubAction					- Subaction called
'	bValidPostedValues			- Are all posted values valid?
'	bUpdated					- Are the profiles updated now?
'	bProfileExists				- Does the profile exist?
'	sStatusField				- Status field name in profile
'	iStatusID					- Contents of status field in profile
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub GetStatusFieldAndID(ByVal sSubAction, _
                        ByVal bValidPostedValues, _
                        ByVal bUpdated, _
                        ByVal bProfileExists, _
                        ByRef sStatusField, _
                        ByRef sStatusID)
    ' Check if the FORM validation went through
    If bValidPostedValues Then
                                        
        ' Valid FORM contents
        ' Check for UNIQUE_KEY Constraint
        If bUpdated Then
            ' No constraint violations occurred, reset the action type to 
            ' EDIT_ACTION and populate the status message accordingly                
            If StrComp(sSubAction, EDIT_ACTION, vbTextCompare) = 0 Then
                sStatusField = STATUS_FIELD_VALUE_SUCCESS
                sStatusID = S_ENTITY_RECORD_UPDATED
            ElseIf (StrComp(sSubAction, ADD_ACTION, vbTextCompare) = 0) OR _
                   (StrComp(sSubAction, ADD_ACTION, vbTextCompare) = 0) Then
                sStatusField = STATUS_FIELD_VALUE_SUCCESS
                sStatusID = S_NEW_ENTITY_RECORD_ADDED
            End If            
        Else            
            ' Constraint violations occurred
            If bProfileExists Then
                sStatusField = STATUS_FIELD_VALUE_ERROR
                sStatusID = E_PRIMARY_KEY_VIOLATION
            Else
                sStatusField = STATUS_FIELD_VALUE_ERROR
                sStatusID = E_UNIQUE_KEY_VIOLATION
            End If
        End If
    Else
        ' FORM validation failed
        sStatusField = STATUS_FIELD_VALUE_ERROR
        sStatusID = E_USER_INPUT_ERROR                        
    End if    
End Sub


' -----------------------------------------------------------------------------
' PrepareCloseAction
'
' Description:
'	"Cancel" action handler, fills in variables which are then passed on to the
'	rendering function
'
' Parameters: (all OUT except 1st)
'	sSubAction					- Sub action	
'	sProfileIDs					- Set of profile IDs			
'	sActionPage					- Page where Action was called
'	sPreviousAction				- What was the previous action?
'	sStatusField				- Status field name in profile
'	iStatusID					- Contents of status field in profile
'	bValidPostedValues			- Are all posted values valid?
'	bDisplayPostedValues		- Must these values be displayed?
'	listProfileSchema			- SimpleList object containing profile schema
'	sSourcePage					- Source page
'	bDirty						- Has the profile changed after the last save?
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareCloseAction(ByVal sSubAction, ByVal sProfileIDs, ByRef sActionPage, _
			ByRef sPreviousAction, ByRef sStatusField, _
			ByRef iStatusID, ByRef bValidPostedValues, ByRef bDisplayPostedValues, _
			ByRef listProfileSchema, ByRef sSourcePage, ByRef bDirty)
	Dim sProfileListPage
	Dim iProfileCount
	
    bDirty = False
    sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
    sPreviousAction = GetRequestString(PREVIOUS_ACTION, Null)
    
    ' Check if any source page was supplied.  If supplied,
    ' the cancel action will take them to this page
    sSourcePage = GetRequestString(SOURCE_PAGE, Null)
    If Not IsNull(sSourcePage) Then Exit Sub

    ' Check if confirmation (message box) page is already displayed
    Select Case sSubAction        
        Case MSGBOX_ACTION_YES
            ' User has opted to close the current page
            sActionPage = GenerateURL(sProfileListPage, Array(ACTION_TYPE), Array(LIST_ACTION))
        Case MSGBOX_ACTION_NO                        
            bValidPostedValues = True            
            bDisplayPostedValues = True
            
            ' Get the simple list of profile property dictionaries
            Set listProfileSchema = listCloneProfileSchema(sThisPage)            
            If (StrComp(sPreviousAction, ADD_ACTION, vbTextCompare) = 0) Or _
               (StrComp(sPreviousAction, COPY_ACTION, vbTextCompare) = 0) Then
               sStatusField = STATUS_FIELD_VALUE_SUCCESS
               iStatusID = S_ADDING_NEW_PROFILE
            Else
                sStatusField = STATUS_FIELD_VALUE_SUCCESS
                iStatusID = S_EDITING_EXISTING_PROFILE
           End If
		Case Else                
	         ' Get the count of profiles       
		     iProfileCount = iGetCountOfProfiles(sSubAction, sProfileIDs)         
			 If iProfileCount = 0 Then
				' Check if user has entered any property value in the EDIT page
	            bDirty = bIsPropertyValueAdded()            
			ElseIf iProfileCount = 1 Then
				' Check if user has updated any property value in the EDIT page
				bDirty = bProfileHasChanged(sProfileIDs)               
			End If
                                   
			If Not bDirty Then
				' Redirect to the list page
				sActionPage = GenerateURL(sProfileListPage, Array(ACTION_TYPE), Array(LIST_ACTION))
			End If
    End Select
End Sub


' -----------------------------------------------------------------------------
' htmCloseActionHandler
'
' Description:
'	Rendering function for "Close" action
'
' Parameters:
'	See PrepareCloseAction
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmCloseActionHandler(ByRef sAction, ByVal sSubAction, ByVal sProfileIDs, _
			ByVal sActionPage, ByVal sPreviousAction, ByVal sStatusField, _
			ByVal iStatusID, ByVal bValidPostedValues, ByVal bDisplayPostedValues, _
			ByVal listProfileSchema, ByVal sSourcePage, ByVal bDirty)
    Dim htm

    If Not IsNull(sSourcePage) Then Response.Redirect(sSourcePage)
    
    ' Check if confirmation (message box) page is already displayed
    Select Case sSubAction        
        Case MSGBOX_ACTION_YES
            ' User has opted to close the current page
            Response.Redirect (sActionPage)
        
        Case MSGBOX_ACTION_NO                        
            If (StrComp(sPreviousAction, ADD_ACTION, vbTextCompare) = 0) OR _
               (StrComp(sPreviousAction, COPY_ACTION, vbTextCompare) = 0) Then
                htm = htmRenderBlankEditPage(ADD_ACTION, _
                                             sSubAction, _
                                             sProfileIDs, _
                                             sStatusField, _
                                             iStatusID, _
                                             bValidPostedValues, _
                                             bDisplayPostedValues, _
                                             listProfileSchema)
            Else
                htm = htmRenderEditPage(EDIT_ACTION, _
                                        sSubAction, _
                                        sProfileIDs, _
                                        sStatusField, _
                                        iStatusID, _
                                        bValidPostedValues, _
                                        bDisplayPostedValues, _
                                        listProfileSchema)
           End If
           
	    Case Else                
		     ' confirmation (message box) page is not yet displayed and need to be displayed
			 '    for single profile editing only.
                                   
	         If bDirty Then
		        ' Display confirmation (message box) for saving the profile
			    htm = htm & htmRenderMessageBoxForCancel(sAction, sProfileIDs)
		     Else
				' Redirect to the list page
	            Response.Redirect (sActionPage)
		     End If
    End Select

    htmCloseActionHandler = htm
End Function
            
            
' -----------------------------------------------------------------------------
' PrepareSelectActionHandlerForEdit
'
' Description:
'	"Select" action handler, this function is called before the 
'   execution is passed on to to the LIST page (in picker mode)
'   for selecting one profile. This functions checks if the 
'   user has updated any of the property values in the EDIT page.
'
' Parameters: (all OUT except first 3)
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'	sActionPage					- The page where the action occurred
'	sPreviousAction				- The previous action
'	sSourcePage					- Source page
'	listProfileSchema			- SimpleList object containing profile schema
'	bValidPostedValues			- Are all posted values valid?
'	bDisplayPostedValues		- Must these values be displayed?
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareSelectActionHandlerForEdit(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
			ByRef sActionPage, ByRef sPreviousAction, _
			ByRef listProfileSchema, ByRef bValidPostedValues, ByRef bDisplayPostedValues)
	Dim sSelectActionProperty
	
    ' Get the property name for which the List page is getting called in
    '     picker mode
    sSelectActionProperty = sGetSelectActionPropertyName(sAction)
    
    ' The action page to redirect with complete URL
    sActionPage = GenerateURL(MSCSSitePages.ListPartnerUsers, Array(ACTION_TYPE, SELECT_FIELD_NAME, SOURCE_PAGE, GUIDS), Array(SELECT_ACTION, sSelectActionProperty, sThisPage, sProfileIDs))
    Call PrepareSaveChangesConfirmation(sSubAction, sPreviousAction, _
			listProfileSchema, bValidPostedValues, bDisplayPostedValues)
End Sub


' -----------------------------------------------------------------------------
' htmSelectActionHandlerForEdit
'
' Description:
'	Rendering function for "Select" action
'
' Parameters:
'	Identical to "PrepareSelectAction"
'
' Returns:
'	String with HTML
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmSelectActionHandlerForEdit(ByRef sAction, ByVal sSubAction, ByVal sProfileIDs, _
				ByVal sActionPage, ByVal sPreviousAction, ByVal listProfileSchema, _
				ByVal bValidPostedValues, ByVal bDisplayPostedValues)
    ' Check if confirmation (message box) page is already displayed, since the user
    '     is leaving the editing mode to another page and some modifications may
    '     have been left un-saved.
    htmSelectActionHandlerForEdit = htmSaveChangesConfirmationHandler(sAction, sSubAction, sProfileIDs, sActionPage, _
				sPreviousAction, listProfileSchema, bValidPostedValues, bDisplayPostedValues)
End Function
        
        
' -----------------------------------------------------------------------------
' PrepareSelectedAction
'
' Description:
'	"Selected" action handler, this function is called when the user has
'   returned from a LIST page (in picker mode)
'
' Parameters: (all OUT except 2nd and 3rd)
'	sAction						- Action called
'	sSubAction					- Subaction called
'	sProfileIDs					- Set of profile IDs to save
'	sStatusField				- Not acted upon (but required by the
'									rendering function; you may need to
'									change this value if you edit this
'									function for your specific business)
'	iStatusID					- Not acted upon (but required by the
'									rendering function; you may need to
'									change this value if you edit this
'									function for your specific business)
'	bValidPostedValues			- Are all posted values valid?
'	bDisplayPostedValues		- Must these values be displayed?
'	listProfileSchema			- SimpleList object containing profile schema
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareSelectedAction(ByRef sAction, ByVal sSubAction, ByVal sProfileIDs, _
			ByRef sStatusField, ByRef iStatusID, _
			ByRef bValidPostedValues, ByRef bDisplayPostedValues, _
			ByRef listProfileSchema)
    ' Update related properties if the user is returning from a picker 
    Call UpdateSelectActionPropertyName(sAction, sSubAction, sProfileIDs)
    
    ' Display the profile properties
    bValidPostedValues = True
    bDisplayPostedValues = False
    
    ' Get the simple list of profile property dictionaries
    Set listProfileSchema = listCloneProfileSchema(sThisPage)
End Sub


' -----------------------------------------------------------------------------
' htmSelectedActionHandlerForEdit
'
' Description:
'	Rendering function for "Selected" action
'
' Parameters:
'	Identical to PrepareSelectedAction
'
' Returns:
'	String with HTML
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmSelectedActionHandlerForEdit(ByVal sAction, ByVal sSubAction, ByVal sProfileIDs, _
			ByVal sStatusField, ByVal iStatusID, _
			ByVal bValidPostedValues, ByVal bDisplayPostedValues, _
			ByVal listProfileSchema)
    ' Render edit page contents
    htmSelectedActionHandlerForEdit = htmRenderEditPage(sAction, _
                            sSubAction, _
                            sProfileIDs, _
                            sStatusField, _
                            iStatusID, _
                            bValidPostedValues, _
                            bDisplayPostedValues, _
                            listProfileSchema)
End Function


' -----------------------------------------------------------------------------
' PrepareSelectAllAction
'
' Description:
'	"Select All" Action Handler
'
' Parameters: (OUT)
'	sProfileListPage			- Page name
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareSelectAllAction(ByRef sProfileListPage)
   ' Get the corresponding list page name for this page
   sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)
End Sub


' -----------------------------------------------------------------------------
' SelectAllActionHandler
'
' Description:
'	Redirecting Sub for "Select All" action
'
' Parameters:
'	Identical to PrepareSelectAllAction
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub SelectAllActionHandler(ByVal sProfileListPage)
   ' Redirect to list page with SelectAll action in the URL
   Call ReDirectToPage(sProfileListPage, _
                          Array(ACTION_TYPE, SUB_ACTION_TYPE), _
                          Array(LIST_ACTION, SELECTALL_ACTION))
End Sub


' -----------------------------------------------------------------------------
' PrepareDeselectAllAction
'
' Description:
'	"Deselect All" Action Handler
'
' Parameters: (OUT)
'	sProfileListPage			- Page name
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareDeselectAllAction(ByRef sProfileListPage)
   ' Get the corresponding list page name for this page 
   sProfileListPage = sGetThisPageProperty(sThisPage, PROPERTY_PAGE_LIST)   
End Sub


' -----------------------------------------------------------------------------
' DeSelectAllActionHandler
'
' Description:
'	Redirecting Sub for "Select All" action
'
' Parameters:
'	Identical to PrepareDeselectAllAction
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub DeSelectAllActionHandler(ByVal sProfileListPage)   
   ' Redirect to the list page with DeSelectAll action in the URL
   Call ReDirectToPage(sProfileListPage, _
                          Array(ACTION_TYPE, SUB_ACTION_TYPE), _
                          Array(LIST_ACTION, DESELECTALL_ACTION))
End Sub
%>