<%
' =============================================================================
' const.asp
' Constants for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' Users Property Access Classes
Const GUEST_USER_CLASS = 0
Const REGISTERED_USER_CLASS = 1
Const DELEGATED_ADMIN_CLASS = 4

Const SEARCH_CRITERIA_SIMPLE = "Simple"
Const SEARCH_CRITERIA_ADVANCED = "Advanced"
Const PAGE_NUM_FIELD = "iPageNumber"
Const FIELD_UPDATED = "_Updated"
Const STATUS_FIELD_NAME	= "status"
Const STATUS_FIELD_VALUE_ERROR = "error"
Const STATUS_FIELD_VALUE_SUCCESS = "success"
Const STATUS_FIELD_ID =	"status_id"

Const ACTION_TYPE =	"sAction"
Const SUB_ACTION_TYPE =	"sSubAction"
Const PREVIOUS_ACTION =	"sPreviousAction"
Const TOTAL_PAGES =	"iTotalPages"
Const SELECT_FIELD_NAME = "SelectFieldName"
Const QUERY_STRING_CRITERIA = "Criteria"
Const FIELD_START_DATE = "start_date"
Const FIELD_END_DATE = "end_date"
Const BUTTON_SUBMIT = "Submit_"
Const BUTTON_SELECT = "Select_"


' Actions in the partner/customer service pages
Const COPY_ACTION			            =	"Copy"
Const EDIT_ACTION			            =	"Edit"
Const DELETE_ACTION			            =	"Delete"
Const DELETEALL_ACTION                  =   "DeleteAll"
Const SELECTALL_ACTION		            =	"SelectAll"
Const DESELECTALL_ACTION	            =	"DeselectAll"
Const SAVE_ACTION			            =	"Save"
Const CANCEL_ACTION			            =	"Close"
Const ORDERDETAILS_ACTION	            =	"ItemDetails"
Const CHANGE_ACTION			            =	"Change"
Const FILTER_ACTION			            =	"Search"
Const HELP_ACTION                       =   "Help"
Const SELECT_ACTION			            =	"..."
Const SELECTED_ACTION		            =	"DoneSelecting"
Const FIRSTPAGE_ACTION		            =	"<<"
Const LASTPAGE_ACTION		            =	">>"
Const PREVIOUSPAGE_ACTION	            =	"<"
Const NEXTPAGE_ACTION		            =	">"
Const MSGBOX_ACTION_YES                 =   "Yes"
Const MSGBOX_ACTION_NO                  =   "No"
Const PAGING_ACTION                     =   "Paging"
Const LIST_ACTION                       =   "List"
' *** End: Query string and POST variable names ***


' Constants specific to partner and customer service pages
Const S_NEW_ENTITY_RECORD_ADDED	=	1
Const S_ENTITY_RECORD_UPDATED =	2
Const S_ENTITY_RECORDS_DELETED =	3
Const S_EDITING_MULTIPLE_PROFILES = 5
Const S_ADDING_NEW_PROFILE = 6
Const S_EDITING_EXISTING_PROFILE = 7

' Used to support multi-edit
Const PROFILE_ID_SEPARATOR              =   ";"

' Profile data types
Const DATA_TYPE_SMALLINT = 2
Const DATA_TYPE_NUMBER = 3
Const DATA_TYPE_DATETIME = 7
Const DATA_TYPE_VARIANT = 12
Const DATA_TYPE_STRING = 130

Const PROFILE_TYPE_ORDER = "Order"
Const GROUP_PROFILE = "ADGroup"

Const ORG_NAME = "GeneralInfo.name"

' Field names present on all profile recordsets
Const FIELD_GENERIC_CHANGED_BY = "user_id_changed_by"
Const FIELD_GENERIC_DATE_UPDATED = "date_last_changed"

' Active Directory constants
Const AD_USER_INACTIVE = 546
Const AD_USER_ACTIVE = 512
Const AD_DELEGATED_ADMIN = 66048
Const AD_SITE_CONTAINER_PREFIX = "OU="
Const AD_CONTAINER_CS40 = "OU=MSCS_40_Root"
Const AD_ADMIN_GROUP_SUFFIX = "_ADMINGROUP"
Const AD_USER_GROUP_SUFFIX = "_USERGROUP"
Const AD_COMMON_NAME_PREFIX = "CN="

' Filter fields
Const FILTER_FIELD_ORDER_USER_FIRST_NAME    =   "first_name"
Const FILTER_FIELD_ORDER_USER_LAST_NAME     =   "last_name"
Const FILTER_FIELD_ORDER_PO_NUMBER          =   "po_number"
Const FILTER_FIELD_ORDER_SKU                =   "vendor_part_number"

Const FIELD_ORDER_USER_FIRST_NAME           =   "user_first_name"
Const FIELD_ORDER_USER_LAST_NAME            =   "user_last_name"
Const FIELD_ORDER_ID				        =	"ordergroup_id"
Const FIELD_ORDER_NUMBER			        =	"order_number"
Const FIELD_ORDER_STATUS			        =	"order_status_code"
Const FIELD_ORDER_TOTAL				        =	"saved_cy_total_total"
Const FIELD_ORDER_SKU				        =	"product_id"
Const FIELD_ORDER_STATUS_CODE		        =	"order_status_code"
Const FIELD_ORDER_DATE_CREATED		        =	"d_DateCreated"
Const FIELD_ORDER_CREATE_DATE		        =	"order_create_date"
Const FIELD_ORDER_TOTAL_TOTAL		        =	"saved_cy_total_total"
Const FIELD_ORDER_DATE_UPDATED		        =	"d_DateLastChanged"
Const FIELD_ORDER_PONUMBER			        =	"po_number"
Const FIELD_ORDER_LINEITEM_DESC		        =	"description"
Const FIELD_ORDER_LINEITEM_QUANTITY	        =	"quantity"
Const FIELD_ORDER_LINEITEM_PRICE	        =	"cy_unit_price"
Const FIELD_ORDER_DATE_TYPE                 =   "date_type"
Const FIELD_ORDER_START_DATE                =   "start_date"
Const FIELD_ORDER_END_DATE                  =   "end_date"
Const ORDER_DATE_TYPE_CREATED	            =	1
Const ORDER_DATE_TYPE_MODIFIED	            =	2

Const TABLE_USER_FILTER = "UserFilter"
Const TABLE_USER_FILTER_KEY1 = "user_id"
Const TABLE_USER_FILTER_KEY2 = "page_name"

Const E_NO_RECORD_SELECTED = 1
Const E_ENTITY_RECORDS_DELETED = 2
Const E_SELECT_ONE_RECORD =	4
Const E_USER_INPUT_ERROR = 5
Const E_PRIMARY_KEY_VIOLATION = 6
Const E_UNIQUE_KEY_VIOLATION =   7
Const E_SELECT_ONE_ORDER            =   8
Const E_PASSWORD_MISMATCH           =   9
Const E_PASSWORD_EMPTY              =   10
Const E_LOGONNAME_LENGTH            =   11
Const E_USER_ALREADY_EXIST          =   12
Const E_SAVING_PROFILE_PROPERTIES   =   15
Const E_ADDING_USER_TO_GROUP_IN_AD  =   16
Const E_NO_PROFILES					=	17
Const E_NO_PROFILES_PERPAGE			=	18
Const E_FILTER_NO_RECORDS			=	19
Const E_PASSWORD_WRONG_LENGTH		=	20
Const E_LOGONNAME_BADCHARS		=	21
Const E_PASSWORD_BADCHARS		=	22
Const E_ADPASSWORD_BADCHARS		=	23
Const E_PASSWORDSET_FAIL		=	24
Const OBJECT_ALREADY_EXIST		    =	&HC100400B

' For order status options
'Const ORDER_STATUS_BASKET			=	1
'Const ORDER_STATUS_TEMPLATE		=	2
Const ORDER_STATUS_NEW_ORDER		=	4
Const ORDER_STATUS_APPROVAL_PENDING =	8
Const ORDER_STATUS_APPROVED			=	16
Const ORDER_STATUS_REJECTED			=	32
Const ORDER_STATUS_FULLFILLED		=	64

' Property search attribute value
Const SEARCH_OPTION_ALL = "ALL"
Const SEARCH_OPTION_ADVANCED = "ADVANCED"

Const DEFAULT_TEXT_BOX_WIDTH = "32"   
Const PAGING_TEXT_BOX_WIDTH = "3"
Const RECORDS_PER_PAGE = 10

Const START_DATE_TIME_SUFFIX = "00:00:00"
Const END_DATE_TIME_SUFFIX = "23:59:59"

' For supporting feature driven profile properties
Const CUSTOM_ATTRIBUTE_FeatureID        =	"sFeatureID"

Const ALL_FEATURES                      =   "0"
Const DELEGATED_ADMIN_SUPPORT           =   "2"
Const AD_SUPPORT                        =   "3"

' For storing the list sheet id's in all the list type partnerdesk pages
' Checkbox controls, all three build one
Const RECORD_ID_ATTRIBUTE_NAME		    =	"_ID"
Const SELECTED_ATTRIBUTE_NAME		    =	"ListItem"

' Checkbox control value 
Const SELECTED_ATTRIBUTE_CHECKED	    =	"on"


' For making the pages completely data driven, based on the profile property attributes
Const CUSTOM_ATTRIBUTE_ANONYMOUS_ACCESS		=	"sAnonymousAccess"
Const CUSTOM_ATTRIBUTE_USER_ACCESS			=	"sUserAccess"
Const CUSTOM_ATTRIBUTE_ADMIN_ACCESS         =	"sDelegatedAdminAccess"

' attribute values for above

Const ACCESS_HIDDEN		                    =	"0"
Const ACCESS_READONLY	                    =	"1"
Const ACCESS_READWRITE                      =	"2"

' Start: For page properties
Const PROPERTY_PAGE_TYPE                =   "PageType"
Const PROPERTY_PAGE_PROFILE             =   "ProfileType"
Const PROPERTY_PAGE_TARGET              =   "TargetPage"
Const PROPERTY_PAGE_LIST                =   "ListPage"
' Page types
Const PAGE_TYPE_LIST			        =	"List"
Const PROFILE_KEY_USERID = "user_id"
' Use Please Select One localizable resource instead
Const BLANK_OPTION =	" "
' End: For page properties (obsolete?)

%>
