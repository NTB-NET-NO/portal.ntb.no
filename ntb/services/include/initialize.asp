<%
' =============================================================================
' initialize.asp
' Initialization routines for /services
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' InitializePartnerDesk
'
' Description:
'	Function to initialize the variables used in all partnerdesk pages
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitializePartnerDesk(ByRef sAction, ByRef sSubAction, ByRef sProfileIDs)
    sAction = sGetAction()
    sSubAction = sGetSubAction()
    sProfileIDs = sGetSelectedIDs(sAction)
End Sub


' -----------------------------------------------------------------------------
' sGetAction
'
' Description:
'	Function to determine the action to be taken for current page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetAction()
    Dim sAction
    Dim frmItem
    Dim frmItemValue

    ' Initialize the action with null string    
    sAction = Null
    
    ' Loop through the FORM collection to find out which submit/select
    '     button is clicked.
    For Each frmItem in Request.Form
        frmItemValue = GetRequestString(frmItem, Null)
        
        If Not IsNull(frmItemValue) Then
            If InStr(1, frmItem, BUTTON_SELECT, vbTextCompare) > 0 Then
                sAction = SELECT_ACTION
                sGetAction = sAction
                Exit Function
            ElseIf InStr(1, frmItem, BUTTON_SUBMIT, vbTextCompare) > 0 Then
                sAction = Mid(frmItem, Len(BUTTON_SUBMIT) + 1)
                sGetAction = sAction
                Exit Function
            End If
        End if            
    Next
    
    ' Check for the action posted in the URL if there was no form submission
    '     This is useful in the scenarion when we are using Response.Redirect()
    '     to navigate from one page to another    
    If IsNull(sAction) Then 
        sAction = GetRequestString(ACTION_TYPE, Null)
    End If
        
    sGetAction = sAction
End Function


' -----------------------------------------------------------------------------
' sGetSubAction
'
' Description:
'	Function to get the sub action
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetSubAction()
    Dim sSubAction
    
    ' Check for the subaction in the FORM/QUERY_STRING collections
    sSubAction = GetRequestString(SUB_ACTION_TYPE, Null)
    
    sGetSubAction = sSubAction
End Function


' -----------------------------------------------------------------------------
' sGetSelectedIDs
'
' Description:
'	Function to get the profile record id's
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetSelectedIDs(ByVal sAction)
    Dim sIDs    
    
    If StrComp(sAction, SELECTED_ACTION, vbTextCompare) = 0 Then
        ' For this action the GUIDS FORM/QUERY_STRING control will have 
        '     the ID's of all the profiles selected
        sIDs = GetRequestString(GUIDS, Null)        
    ElseIf StrComp(sAction, ORDERDETAILS_ACTION, vbTextCompare) = 0 Then
        ' For this action the order id will be in SELECTED_ATTRIBUTE_NAME
        '     FORM/QUERY_STRING control
        sIDs = GetRequestString(SELECTED_ATTRIBUTE_NAME, Null)        
    Else
        ' Else, Loop through the FORM collection and get the selected
        '     profiles IDs.
        sIDs = sGetIDFromForm()
        
        ' Check the QUERY_STRING collection for the Profil IDs if none
        '     exist in FORM collection
        If IsNull(sIDs) Then 
            sIDs = GetRequestString(GUIDS, Null)
        End If        
    End If
        
    sGetSelectedIDs = sIDs
End Function


' -----------------------------------------------------------------------------
' sGetIDFromForm
'
' Description:
'	Function to loop through the FORM collection to determine the 
'   selected profiles
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetIDFromForm()
    Dim sID
    Dim frmitem
    Dim frmValue
    Dim sIDsInForm
    
    ' Initilizing the variable containing the profile IDs to Null
    sIDsInForm = Null
        
    ' Loop through the FORM's collection, each of the profiles will have
    '     either a checkbox/radiobutton control  associated with it in the LIST
    '     page. The name of the control will have a format of
    '     (SELECTED_ATTRIBUTE_NAME & Count) and a hidden FORM control 
    '     (SELECTED_ATTRIBUTE_NAME & Count & RECORD_ID_ATTRIBUTE_NAME) is used 
    '     to stores the profile IDs.
    For Each frmitem In Request.Form
        frmValue = GetRequestString(frmitem, Null)
        
        ' Check if the form element is a profile identifier checkbox/radiobutton
        '     control
        If InStr(1, frmitem, SELECTED_ATTRIBUTE_NAME) > 0 Then            
            ' Check if this profile identifier checkbox/radiobutton control is 
            '     checked
            
            If StrComp(frmValue, SELECTED_ATTRIBUTE_CHECKED, vbTextCompare) = 0 Then            
                ' Concatenate all the profile identifiers (GUIDs)into one string separated
                '     by PROFILE_ID_SEPARATOR
                sID = GetRequestString( _
                                                frmitem & RECORD_ID_ATTRIBUTE_NAME, _
                                                Null _
                                            )
                If Not IsNull(sID) Then                                            
                    If Not IsNull(sIDsInForm) Then                    
                        sIDsInForm = sIDsInForm & _
                                     PROFILE_ID_SEPARATOR & _
                                     sID
                    Else
                        sIDsInForm = sID
                    End If
                End If                    
            End If
        End If
    Next
    
    sGetIDFromForm = sIDsInForm
End Function
%>