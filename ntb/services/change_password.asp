<!-- #INCLUDE FILE="../include/header.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../include/form_lib.asp" -->
<!-- #INCLUDE FILE="include/initialize.asp"                         -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/lib.asp"			                    -->
<!-- #INCLUDE FILE="include/profiles_common.asp"                    -->
<!-- #INCLUDE FILE="include/check_profile.asp"                      -->
<!-- #INCLUDE FILE="include/get_profiles.asp"                       -->
<!-- #INCLUDE FILE="include/save_all_profiles.asp"                  -->
<!-- #INCLUDE FILE="include/save_multiple_profiles.asp"             -->
<!-- #INCLUDE FILE="include/save_one_profile.asp"                   -->
<!-- #INCLUDE FILE="include/password_handlers.asp"	                -->
<!-- #INCLUDE FILE="include/render_common.asp"	                    -->
<!-- #INCLUDE FILE="include/render_password.asp"	                -->
<!-- #INCLUDE FILE="../template/layout1.asp" -->
<%
Sub Main()
	Dim sAction
	Dim sSubAction
	Dim sProfileIDs
	
	Call EnsureAuthAccess()
  
    Call InitializePartnerDesk(sAction, sSubAction, sProfileIDs)
    If IsNull(sAction) Then sAction = CHANGE_ACTION
    
    If Not IsNull(sProfileIDs) Then
        Call CheckIfProfilesExist(sSubAction, sProfileIDs)
    Else
        Response.Redirect(GenerateURL(MSCSSitePages.NoAccount, Array(), Array()))        
    End If
    
    Rem TASK: Need to add a validation that a user can change only his
    Rem     password and a delegated admin can change password for any 
    Rem     user from the organization.   
    
    Rem Get this page content
    htmPageContent = PasswordActionHandlers(sAction, sSubAction, sProfileIDs)
End Sub

Function PasswordActionHandlers( _
                                   ByVal sAction, _
                                   ByVal sSubAction, _
                                   ByVal sProfileIDs _
                               )
    
    Dim htm
        
    Select Case sAction
    
        Case CHANGE_ACTION            
            htm = ChangeActionHandler(sAction, sSubAction, sProfileIDs)
        
        Case SAVE_ACTION
            htm = UpdatePasswordActionHandler(sAction, sSubAction, sProfileIDs)
        
        Case CANCEL_ACTION
            htm = htmCancelUpdatePasswordAction(sAction, sSubAction, sProfileIDs)
        
        Case Else
            Response.Redirect(GenerateURL(MSCSSitePages.BadAction, Array(), Array()))
    End Select

    PasswordActionHandlers = htm    
End Function
%>
