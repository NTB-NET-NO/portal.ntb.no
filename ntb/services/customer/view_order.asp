<!-- #INCLUDE FILE="../../include/header.asp" -->
<!-- #INCLUDE FILE="../../include/const.asp" -->
<!-- #INCLUDE FILE="../../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../../include/catalog.asp" -->
<!-- #INCLUDE FILE="../../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../../include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="../../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../../include/form_lib.asp" -->
<!-- #INCLUDE FILE="../include/initialize.asp"                          -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/lib.asp"			                        -->
<!-- #INCLUDE FILE="../include/get_profiles.asp"                        -->
<!-- #INCLUDE FILE="../include/profiles_common.asp"                     -->
<!-- #INCLUDE FILE="../include/order_handlers.asp"	                    -->
<!-- #INCLUDE FILE="../include/render_common.asp"	                    -->
<!-- #INCLUDE FILE="../include/logic_orders.asp"	                    -->
<!-- #INCLUDE FILE="../include/render_orders.asp"	                    -->
<!-- #INCLUDE FILE="../include/requisition.asp"			                -->
<!-- #INCLUDE FILE="../../template/layout1.asp" -->
<%
' =============================================================================
' view_order.asp
' Order view for individual customers.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim sAction, sSubAction, sProfileIDs, sSourcePage, sProfileListPage
	Dim iProfileCount
		
	Call EnsureAuthAccess()
 	
	Call InitializePartnerDesk(sAction, sSubAction, sProfileIDs)
	If IsNull(sAction) Then sAction = ORDERDETAILS_ACTION	
    If IsNull(sSubAction) Then sSubAction = ORDERDETAILS_ACTION	
    If Not IsNull(sProfileIDs) Then Call CheckIfOrderExists(sSubAction, sProfileIDs)
    
    Select Case sAction
        Case ORDERDETAILS_ACTION
			Call PrepareOrderDetailsAction(sSubAction, sProfileIDs, iProfileCount)
            htmPageContent = htmOrderDetailsActionHandler(sAction, sSubAction, _
								sProfileIDs, iProfileCount)
        
        Case CANCEL_ACTION
			Call PrepareCloseOrderDetailsAction(sProfileListPage, sSourcePage)
            Call CloseOrderDetailsActionHandler(sAction, sSubAction, sProfileIDs, _
					sSourcePage, sProfileListPage)
        Case Else
            Response.Redirect(GenerateURL(MSCSSitePages.BadAction, Array(), Array()))
    End Select
 End Sub
%>
