<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/payment.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' summary.asp
' Display page for Order Summary (basket)
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim mscsAnyOrderForm
	Dim mscsOrderGrp, mscsOrderForm, oOrderFormDisplayOrder, listMethods, dictAnyItem
	Dim bBadVerify

	Call EnsureAccess()
	
	Call InitializeSummaryPage(mscsOrderGrp, listMethods, mscsAnyOrderForm, dictAnyItem)

	' Remove any addresses from the orderform which are not referenced
	Call mscsOrderGrp.PurgeUnreferencedAddresses()

	' Add discount footnote symbols to each lineitem
	Call AddDiscountMessages(mscsOrderGrp)
	
	' If VerifyWith key exists on the order group then check for VerifyWith error.
	bBadVerify = False
	If Not IsNull(mscsOrderGrp.Value("verify_with_total")) Then
		' If the order total as last seen by the user does not match the order total just
		'	calculated, this is an error (display an error message later in the page)
		If mscsOrderGrp.Value("verify_with_total") <> CStr(mscsOrderGrp.Value(SAVED_TOTAL_TOTAL)) Then
			' Remove the VerifyWith key from order group; otherwise order summary will always show the error.
			mscsOrderGrp.Value("verify_with_total") = Null
			bBadVerify = True
		End If
	End If
	mscsOrderGrp.SaveAsBasket()

		
	htmPageContent = htmRenderSummaryPage(mscsOrderGrp, mscsAnyOrderForm, dictAnyItem, listMethods, bBadVerify)
	Call TerminateSummaryPage(mscsOrderGrp, mscsAnyOrderForm, dictAnyItem, listMethods)
End Sub


' -----------------------------------------------------------------------------
' InitializeSummaryPage
'
' Description:
'	Initialize; called by Main
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitializeSummaryPage(ByRef mscsOrderGrp, ByRef listMethods, ByRef mscsAnyOrderForm, ByRef dictAnyItem)
	Dim iErrorLevel
	
	Set mscsOrderGrp = LoadBasket(m_UserID)

	' Get all payment methods applicable to user.
	Set listMethods = GetPaymentMethodsForUser()
	
	Call CatchErrors(mscsOrderGrp, listMethods)	

	' Run the basket pipeline (pricing info, discounts, etc).
    iErrorLevel = RunMtsPipeline(MSCSPipelines.Basket, GetPipelineLogFile("Basket"), mscsOrderGrp)

    ' Check for basket errors only. Basket pipeline must not return purchase errors.
    If iErrorLevel > 1 Then 
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
    End If     
    
	' Run the total pipeline (shipping, tax, etc).
    iErrorLevel = RunMtsPipeline(MSCSPipelines.Total, GetPipelineLogFile("Total"), mscsOrderGrp)
    
    If iErrorLevel > 1 Then 
        If (GetErrorCount(mscsOrderGrp, BASKET_ERRORS) = 0) And (GetErrorCount(mscsOrderGrp, PURCHASE_ERRORS) = 0) Then
			Err.Raise vbObjectError + 2110, , mscsMessageManager.GetMessage("L_Unspecified_Pipeline_Warning_ErrorMessage", sLanguage)        
		End If
    End If     

    ' This code retrieves the billing address (which is assumed the same on each orderform)
    '	and shipping address and shipping method (which is assumed the same on each lineitem).
    '	If this assumptions do not hold true, you'll need to iterate the orderforms/lineitems.
	Set mscsAnyOrderForm = GetAnyOrderForm(mscsOrderGrp)
	Set dictAnyItem = GetAnyLineItem(mscsOrderGrp)
End Sub


' -----------------------------------------------------------------------------
' TerminateSummaryPage
'
' Description:
'	Clean up; called by Main. Not really necessary because this is ASP, and ASP
'	cleans up for you.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub TerminateSummaryPage(ByRef mscsOrderGrp, ByRef mscsAnyOrderForm, ByRef dictAnyItem, ByRef listMethods)
	On Error Resume Next
	Set mscsOrderGrp = Nothing
	Set mscsAnyOrderForm = Nothing
	Set dictAnyItem = Nothing
	Set listMethods = Nothing
End Sub


' -----------------------------------------------------------------------------
' CatchErrors
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrors(ByVal mscsOrderGrp, listMethods)
	Dim sOrderFormName, i
	Dim mscsOrderForm

	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If
	
	' There must be at least one applicable payment method.
	If listMethods.Count < 1 Then
		Err.Raise HRESULT_E_FAIL, "Site",mscsMessageManager.GetMessage("L_Bad_User_PaymentOptions_ErrorMessage", sLanguage)
	End If

	' Remove empty orderforms to prevent basket pipeline from raising pur_noitems purchase error.
	Call RemoveEmptyOrderForms(mscsOrderGrp)
	
	For Each sOrderFormName In mscsOrderGrp.Value.OrderForms
	    Set mscsOrderForm = mscsOrderGrp.Value.OrderForms(sOrderFormName)
	    For i = 0 To mscsOrderForm.Items.Count - 1	        
	        ' Is shipping_address_id set on each line-item?
	        If IsNull(mscsOrderForm.Items(i).Value(SHIPPING_ADDRESS_ID)) Then
				Response.Redirect(GenerateURL(GetAddressPage(), Array(ADDRESS_TYPE), Array(SHIPPING_ADDRESS)))
	        End If
	        
	        ' Is shipping_method_id set on each line-item?
	        If IsNull(mscsOrderForm.Items(i).Value(SHIPPING_METHOD_KEY)) Then
				Response.Redirect(GenerateURL(MSCSSitePages.ShippingMethods, Array(), Array()))
            End If
		Next
		
		' Is billing_address_id set on each orderform?
		If IsNull(mscsOrderForm.Value(BILLING_ADDRESS_ID)) Then
			Response.Redirect(GenerateURL(GetAddressPage(), Array(ADDRESS_TYPE), Array(BILLING_ADDRESS)))
		End If
	Next
End Sub


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderSummaryPage
'
' Description:
'	Render the whole summary page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderSummaryPage(mscsOrderGrp, mscsAnyOrderform, dictAnyItem, listMethods, bBadVerify)
	Dim htmContent, htmRows, htmTitle
	Dim arrHeader, arrData
	Dim oOrderFormDisplayOrder, mscsOrderForm
	Dim sOrderFormName
	
	' Display shipping method
	sPageTitle = mscsMessageManager.GetMessage("L_OrderSummary_HTMLTitle", sLanguage)
	htmContent = RenderText(mscsMessageManager.GetMessage("L_ShippingMethod_Name_DisplayName_HTMLText", sLanguage), MSCSSiteStyle.Body) 
	htmContent = htmContent & ": " & dictAnyItem.Value("shipping_method_name") & CRLF
	
	' Display billing and shipping addresses
	arrHeader = Array(_
		RenderText(mscsMessageManager.GetMessage("L_BillingAddress_Text", sLanguage), MSCSSiteStyle.Body), _
		RenderText(mscsMessageManager.GetMessage("L_ShippingAddress_Text", sLanguage), MSCSSiteStyle.Body))
	
	arrData = Array( _
		htmRenderAddress(mscsOrderGrp.GetAddress(mscsAnyOrderform.Value("billing_address_id"))), _
		htmRenderAddress(mscsOrderGrp.GetAddress(dictAnyItem.Value("shipping_address_id"))))
		
	htmRows = RenderTableHeaderRow(arrHeader, Array(), MSCSSiteStyle.TRCenter)
	htmRows = htmRows & RenderTableDataRow(arrData, Array(), MSCSSiteStyle.TRLeft)
	htmContent = htmContent & RenderTable(htmRows, MSCSSiteStyle.BasketTable) & CRLF
	
	If bBadVerify Then
		htmContent = htmContent & RenderText(mscsMessageManager.GetMessage("pur_badverify", sLanguage), MSCSSiteStyle.Warning) & BR
	End If
	
	' Render each OrderForm
	Set oOrderFormDisplayOrder = SortDictionaryKeys(mscsOrderGrp.Value(ORDERFORMS))
	For Each sOrderFormName In oOrderFormDisplayOrder
		If mscsOrderGrp.Value(ORDERFORMS).Count <> 1 Then
			htmContent = htmContent & RenderText(mscsMessageManager.GetMessage("L_OrderForm_Text", sLanguage) & sOrderFormName, MSCSSiteStyle.Body) & BR
		End If
			
		Set mscsOrderForm = mscsOrderGrp.value(ORDERFORMS).Value(sOrderFormName)
		htmContent = htmContent & htmRenderOrderForm(mscsOrderForm)
	Next
			
	If mscsOrderGrp.Value(ORDERFORMS).Count <> 1 Then
		htmContent = htmContent & RenderText(mscsMessageManager.GetMessage("L_GrandTotals_HTMLText", sLanguage), MSCSSiteStyle.Body) & BR
		htmContent = htmContent & htmRenderGrandTotals(mscsOrderGrp) & BR
	End If
		
	htmContent = htmContent & htmRenderDiscountsDescriptions(mscsOrderGrp)
	htmTitle = RenderText(mscsMessageManager.GetMessage("L_OrderSummary_HTMLTitle", sLanguage), MSCSSiteStyle.Title) & CRLF
	htmContent = htmContent & htmRenderVerifyWithForm(mscsOrderGrp, listMethods)
	
	' Clean up
	Set oOrderFormDisplayOrder = Nothing
	Set mscsOrderForm = Nothing
	
	htmRenderSummaryPage = htmTitle & htmContent
End Function


' -----------------------------------------------------------------------------
' htmRenderOrderForm
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderOrderForm(mscsOrderForm)
	Dim arrData, arrDataAttLists, i, dictItem, bDiscountApplied
	Dim arrParams, arrParamVals, htmProdCode, htmShipTotal, urlLink, sProdCode, htmLinkText
	
	' Initialize
	bDiscountApplied = False
	htmRenderOrderForm = ""
	
	If mscsOrderForm.Value(BASKET_ERRORS).Count > 0 Then
		htmRenderOrderForm = htmRenderOrderForm & RenderUnorderedList(GetArrayFromList(mscsOrderForm.Value(BASKET_ERRORS)), MSCSSiteStyle.Warning) & BR
	End If
		
	If mscsOrderForm.Value(PURCHASE_ERRORS).Count > 0 Then
		htmRenderOrderForm = htmRenderOrderForm & RenderUnorderedList(GetArrayFromList(mscsOrderForm.Value(PURCHASE_ERRORS)), MSCSSiteStyle.Warning) & BR
	End If
	
	If mscsOrderForm.Value("_winners").Count > 0 Then
		bDiscountApplied = True
		arrData = Array( _
			mscsMessageManager.GetMessage("L_ProductCode_HTMLText", sLanguage), _
			mscsMessageManager.GetMessage("L_Product_Name_DisplayName_HTMLText", sLanguage), _
			mscsMessageManager.GetMessage("L_Product_Description_DisplayName_HTMLText", sLanguage), _	    				
			mscsMessageManager.GetMessage("L_BASKET_QUANTITY_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_UNITPRICE_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_DISCOUNT_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_MESSAGES_COLUMN_TEXT", sLanguage), _	    	
			mscsMessageManager.GetMessage("L_BASKET_TOTALPRICE_COLUMN_TEXT", sLanguage))
			
		arrDataAttLists = Array( _
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDLeft, _			
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDRight)
	Else
		arrData = Array( _	
			mscsMessageManager.GetMessage("L_ProductCode_HTMLText", sLanguage), _
			mscsMessageManager.GetMessage("L_Product_Name_DisplayName_HTMLText", sLanguage), _
			mscsMessageManager.GetMessage("L_Product_Description_DisplayName_HTMLText", sLanguage), _	    				
			mscsMessageManager.GetMessage("L_BASKET_QUANTITY_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_UNITPRICE_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_TOTALPRICE_COLUMN_TEXT", sLanguage))
			
		arrDataAttLists = Array( _
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDLeft, _			
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDRight)
	End If

	htmRenderOrderForm = htmRenderOrderForm & RenderTableHeaderRow(arrData, Array(), MSCSSiteStyle.TRCenter)
	
	For i = 0 To mscsOrderForm.Items.Count - 1
		Set dictItem = mscsOrderForm.Items(i)
		
		arrParams = Array(CATALOG_NAME_URL_KEY, CATEGORY_NAME_URL_KEY, PRODUCT_ID_URL_KEY, VARIANT_ID_URL_KEY)
		arrParamVals = Array(dictItem.Value("product_catalog"), dictItem.Value("product_category"), dictItem.Value("product_id"), dictItem.Value("product_variant_id"))
		urlLink = GenerateURL(MSCSSitePages.Product, arrParams, arrParamVals)

		sProdCode = dictItem.Value("product_id")
		If Not IsNull(dictItem.Value("product_variant_id")) Then
			sProdCode = sProdCode & "-" & dictItem.Value("product_variant_id")
		End If
		
		htmLinkText = RenderText(sProdCode, MSCSSiteStyle.Body)
		htmProdCode = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)
		
		If bDiscountApplied Then
			' "name" and "description" are required product properties and cannot have null values.
			arrData = Array(_
				htmProdCode, _
				dictItem.Value("_product_name"), _								
				dictItem.Value("_product_description"), _				
				dictItem.Value("quantity"), _
				htmRenderCurrency(dictItem.Value("_cy_iadjust_currentprice")), _
				htmRenderCurrency(dictItem.Value("_cy_oadjust_discount")), _
				dictItem.Value("_messages"), _
				htmRenderCurrency(dictItem.Value("_cy_oadjust_adjustedprice")))
		Else
			' "name" and "description" are required product properties and cannot have null values.
			arrData = Array(_
				htmProdCode, _
				dictItem.Value("_product_name"), _								
				dictItem.Value("_product_description"), _				
				dictItem.Value("quantity"), _
				htmRenderCurrency(dictItem.Value("_cy_iadjust_currentprice")), _
				htmRenderCurrency(dictItem.Value("_cy_oadjust_adjustedprice")))
		End If

		htmRenderOrderForm = htmRenderOrderForm & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)
	Next
		
	If bDiscountApplied Then
		arrDataAttLists = Array(" COLSPAN='6'", MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDRight)				
	Else
		arrDataAttLists = Array(" COLSPAN='4'", MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDRight)
	End If
	
	arrData = Array(NBSP, mscsMessageManager.GetMessage("L_BASKET_SUBTOTAL_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderForm.Value(SUBTOTAL)))
	htmRenderOrderForm = htmRenderOrderForm & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	htmShipTotal = mscsMessageManager.GetMessage("L_BASKET_SHIPPING_COLUMN_TEXT", sLanguage)
	If Not IsNull(mscsOrderForm.Value("_shipping_discount_description")) Then
		htmShipTotal = htmShipTotal & " (" & mscsOrderForm.Value("_shipping_discount_description") & ")"
	End If

	arrData = Array(NBSP, htmShipTotal, _
		htmRenderCurrency(mscsOrderForm.Value(SHIPPING_TOTAL)))
	htmRenderOrderForm = htmRenderOrderForm & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	arrData = Array(NBSP, mscsMessageManager.GetMessage("L_BASKET_HANDLING_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderForm.Value(HANDLING_TOTAL)))
	htmRenderOrderForm = htmRenderOrderForm & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	arrData = Array(NBSP, mscsMessageManager.GetMessage("L_BASKET_TAX_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderForm.Value(TAX_TOTAL)))
	htmRenderOrderForm = htmRenderOrderForm & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	arrData = Array(NBSP, mscsMessageManager.GetMessage("L_BASKET_ORDERTOTAL_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderForm.Value(TOTAL_TOTAL)))
	htmRenderOrderForm = htmRenderOrderForm & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)
	
	htmRenderOrderForm = RenderTable(htmRenderOrderForm, MSCSSiteStyle.BasketTable)
	
	htmRenderOrderForm = htmRenderOrderForm & RenderText(mscsOrderForm.Value("_shipping_discount_description"), MSCSSiteStyle.Body) & BR 
End Function


' -----------------------------------------------------------------------------
' htmRenderGrandTotals
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderGrandTotals(mscsOrderGrp)
	Dim arrData, arrDataAttLists

	Call mscsOrderGrp.AggregateOrderFormValues(SHIPPING_TOTAL, SHIPPING_TOTAL)
	Call mscsOrderGrp.AggregateOrderFormValues(HANDLING_TOTAL, HANDLING_TOTAL)
	Call mscsOrderGrp.AggregateOrderFormValues(TAX_TOTAL, TAX_TOTAL) 

	arrDataAttLists = Array(MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDRight)	
	
	arrData = Array(mscsMessageManager.GetMessage("L_BASKET_SUBTOTAL_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderGrp.Value(SAVED_SUBTOTAL)))
	htmRenderGrandTotals = htmRenderGrandTotals & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	arrData = Array(mscsMessageManager.GetMessage("L_BASKET_SHIPPING_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderGrp.Value(SAVED_SHIPPING_TOTAL)))
	htmRenderGrandTotals = htmRenderGrandTotals & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	arrData = Array(mscsMessageManager.GetMessage("L_BASKET_HANDLING_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderGrp.Value(SAVED_HANDLING_TOTAL)))
	htmRenderGrandTotals = htmRenderGrandTotals & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	arrData = Array(mscsMessageManager.GetMessage("L_BASKET_TAX_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderGrp.Value(SAVED_TAX_TOTAL)))
	htmRenderGrandTotals = htmRenderGrandTotals & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)

	arrData = Array(mscsMessageManager.GetMessage("L_BASKET_ORDERTOTAL_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderGrp.Value(SAVED_TOTAL_TOTAL)))
	htmRenderGrandTotals = htmRenderGrandTotals & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)
End Function


' -----------------------------------------------------------------------------
' htmRenderVerifyWithForm
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderVerifyWithForm(mscsOrderGrp, listMethods)
	Dim urlAction, htmBody, sPaymentMethod, sBtnText
	
	' To verify that an order has not been altered between the time it was last shown to the 
	'	user and the time user completes the purchase, we store the order total shown to the 
	'	user in a hidden field and then during purchase we verify that it matches the order 
	'	total re-calculated after running the total pipeline. If there is a discrepancy we 
	'	redirect the user to the order summary with an error.
	htmBody = RenderHiddenField("verify_with_total", mscsOrderGrp.Value(SAVED_TOTAL_TOTAL))
	
	sBtnText = mscsMessageManager.GetMessage("L_CheckOut_Button", sLanguage)
	htmBody = htmBody & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)
	
	' Payment method not specifed yet.
	If IsNull(mscsOrderGrp.Value(PAYMENT_METHOD)) Then
		sPaymentMethod = GetDefaultPaymentMethodForUser(listMethods)
	' Is payment method supported for user?
	ElseIf IsEntityInList(mscsOrderGrp.Value(PAYMENT_METHOD), listMethods) Then
		sPaymentMethod = mscsOrderGrp.Value(PAYMENT_METHOD)
	Else
		' If it appears that the site has disabled a payment method, Discard the saved 
		' payment method and fall back to user's default payment method.
		sPaymentMethod = GetDefaultPaymentMethodForUser(listMethods)
	End If
	
	urlAction = GenerateURL(GetPaymentPageFromPaymentMethod(sPaymentMethod), Array(), Array())
	htmRenderVerifyWithForm = RenderForm(urlAction, htmBody, HTTP_POST)
End Function


' -----------------------------------------------------------------------------
' htmRenderAddress
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderAddress(addr_dict)
	Dim sAddr
	
    ' $$INTLADDR: This illustrates how to use an alternative address format (in this case Japanese) 
    'If dictConfig.i_SiteDefaultLocale = 1041 Then
    '    sAddr = sAddr & mscsMessageManager.GetMessage("L_Country_HTMLText", sLanguage) & ": " & MSCSAppConfig.GetCountryNameFromCountryCode(addr_dict.country_code) & BR
    '    sAddr = sAddr & mscsMessageManager.GetMessage("L_PostalCode_HTMLText", sLanguage) & ": " & addr_dict.postal_code & BR
    '    If addr_dict.region_name <> "" Then
    '	     sAddr = sAddr & mscsMessageManager.GetMessage("L_Region_HTMLText", sLanguage) & ": " & addr_dict.region_code & BR
    '    End If
    '    sAddr = sAddr & mscsMessageManager.GetMessage("L_City_HTMLText", sLanguage) & ": " & addr_dict.city & BR
    '    sAddr = sAddr & mscsMessageManager.GetMessage("L_AddressLine1_HTMLText", sLanguage) & ": " & addr_dict.address_line1 & BR
    '    If addr_dict.address_line2 <> "" Then
    '        sAddr = sAddr & mscsMessageManager.GetMessage("L_AddressLine2_HTMLText", sLanguage) & ": " & addr_dict.address_line2 & BR
    '    End If
    '    sAddr = sAddr & mscsMessageManager.GetMessage("L_LastName_HTMLText", sLanguage) & ": " & addr_dict.last_name & BR
    '    sAddr = sAddr & mscsMessageManager.GetMessage("L_FirstName_HTMLText", sLanguage) & ": " & addr_dict.first_name & BR
    'Else
        sAddr = sAddr & mscsMessageManager.GetMessage("L_LastName_HTMLText", sLanguage) & ": " & addr_dict.last_name & BR
        sAddr = sAddr & mscsMessageManager.GetMessage("L_FirstName_HTMLText", sLanguage) & ": " & addr_dict.first_name & BR
        sAddr = sAddr & mscsMessageManager.GetMessage("L_AddressLine1_HTMLText", sLanguage) & ": " & addr_dict.address_line1 & BR
    	If addr_dict.address_line2 <> "" Then
    		sAddr = sAddr & mscsMessageManager.GetMessage("L_AddressLine2_HTMLText", sLanguage) & ": " & addr_dict.address_line2 & BR
    	End If
        sAddr = sAddr & mscsMessageManager.GetMessage("L_City_HTMLText", sLanguage) & ": " & addr_dict.city & BR
        If addr_dict.region_name <> "" Then
    		sAddr = sAddr & mscsMessageManager.GetMessage("L_Region_HTMLText", sLanguage) & ": " & addr_dict.region_code & BR
    	End If
        sAddr = sAddr & mscsMessageManager.GetMessage("L_PostalCode_HTMLText", sLanguage) & ": " & addr_dict.postal_code & BR
        sAddr = sAddr & mscsMessageManager.GetMessage("L_Country_HTMLText", sLanguage) & ": " & MSCSAppConfig.GetCountryNameFromCountryCode(addr_dict.country_code)
    'End If
    htmRenderAddress = sAddr
End Function


' -----------------------------------------------------------------------------
' htmRenderDiscountsDescriptions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderDiscountsDescriptions(mscsOrderGrp)
	Dim htmContent
	Dim oCiidCollection, oContentList
	Dim sOrderFormName, oOrderForm, iItem
	Dim ciid
	Dim dictDescriptions 

	Set dictDescriptions = GetDictionary()
	
	' Get the applied discounts
	' Put the applied ciid's in a dictionary, so you only show one of each
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		Set oOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		Set oContentList = oOrderForm.Value("_discounts")
		
		For Each iItem In oOrderForm.Items
			If IsObject(iItem.Value("discounts_applied")) Then
				Set oCiidCollection = iItem.Value("discounts_applied")
				If oCiidCollection.Count > 0 Then
					For Each ciid In oCiidCollection
						dictDescriptions(ciid) = GetDiscountDescription(ciid, oContentList)
					Next
				End If
			End If
		Next
	Next
	
	If dictDescriptions.Count > 0 Then
		htmContent = Bold(mscsMessageManager.GetMessage("L_BASKET_DISCOUNTS_APPLIED_TEXT", sLanguage)) & CR
	
		For Each ciid In dictDescriptions
			htmContent = htmContent & dictDescriptions(ciid)
		Next
		
		htmRenderDiscountsDescriptions = RenderPreFormattedText(htmContent, GetDictionary())
	End If	
End Function
%>
