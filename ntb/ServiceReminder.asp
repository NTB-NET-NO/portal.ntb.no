<%@ Language=VBScript %>
<%
' =============================================================================
' ServiceReminder.asp
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Option Explicit
Response.AddHeader "pragma", "no-cache"
Response.Expires = 0
%>
<html>
<head>
</head>
<BODY BACKGROUND="sky.jpg">
<div class=Section1>
<%
Dim objAuthManager
Dim objUPM
Dim objProfile
Dim strUserID
Dim sCookieData

On Error goto 0

' Get User ID from AuthManager object
SET objAuthManager = Server.CreateObject("Commerce.AuthManager")
objAuthManager.Initialize(Application("MSCSCommerceSiteName"))
if( Err.number <> 0 ) Then
	Response.Write "<P>AuthManager Failed to initialize</P>"
End If 


If Request.Cookies("CampaignItemID") <> "" Then		' Part of a Campaign
	strUserID = objAuthManager.GetUserID(1)
	if( Err.number <> 0 ) Then
		Response.Write "<P>Failed to get UserID</P>"
	End If 
Else							' Not part of a Campaign
	' The fix for being able to send personalized direct mail outside of a campaign is 
	' to toggle the comments on the two lines below.  
	' But please ensure that the web server that generates the personalized content is placed
	' behind a firewall (since the user guid that is passed in not encrypted in this case).  
	
	Err.Raise  &H80004005, , "Error: CampaignItemID was not specified"
	' strUserID = Request.Cookies("MSCSProfile")
End If


' Get UserObject from profile Service
Set objUPM = Application("MSCSProfileService")

If (Err.Number <> 0) Then
  Response.Write "<P>Failed to create Commerce.ProfileService</P>"
End If


Set objProfile = objUPM.GetProfileByKey("User_ID", strUserID, "UserObject")

If (Err.Number <> 0) Then
  strErrMsg = "<P>User profile not found: " & strUserID & "</P>"
  Response.Write strErrMsg
End If


%>

<table border=1 cellspacing=0 cellpadding=0 bgcolor="#ffff99" style='background:
 #FFFF99;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
  <td width=655 valign=top style='width:490.9pt;border:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoPlainText><span style='font-family:Tahoma;mso-fareast-font-family:
  "MS Mincho"'>From: E-mail Notifier<o:p></o:p></span></p>
  <p class=MsoPlainText><span style='font-family:Tahoma;mso-fareast-font-family:
  "MS Mincho"'>Sent: <% = Date() %><o:p></o:p></span></p>
  <p class=MsoPlainText><span style='font-family:Tahoma;mso-fareast-font-family:
  "MS Mincho"'>To: <% = objProfile.GeneralInfo.First_Name %><o:p> </o:p><% = objProfile.GeneralInfo.Last_Name %><o:p></o:p></span></p>
  <p class=MsoPlainText><span style='font-family:Tahoma;mso-fareast-font-family:
  "MS Mincho"'>Subject: </span><b><span style='font-size:12.0pt;mso-bidi-font-size:
  10.0pt;font-family:Tahoma;mso-fareast-font-family:"MS Mincho";color:blue'>New product notice</span></b><span style='mso-fareast-font-family:
  "MS Mincho"'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoPlainText><b><span style='font-size:12.0pt;mso-bidi-font-size:10.0pt;
mso-fareast-font-family:"MS Mincho";color:blue'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></b></p>

<p class=MsoPlainText><span style='font-family:Tahoma;mso-fareast-font-family:
"MS Mincho"'>Dear <% = objProfile.GeneralInfo.First_Name %>,<o:p></o:p></span></p>

<p class=MsoPlainText><span style='font-family:Tahoma;mso-fareast-font-family:
"MS Mincho"'><span style="mso-spacerun: yes">  </span><o:p></o:p></span></p>

<p class=MsoPlainText><span style='font-family:Tahoma;mso-fareast-font-family:
"MS Mincho"'><span style="mso-spacerun: yes">  </span>We have added some exciting new products to
our Web site. <o:p></o:p> <A HREF="<% =objAuthManager.GetUrl("default.asp", False, False) %>">Come see us today!</A>.<o:p></o:p></span></p>


<p class=MsoNormal style='mso-layout-grid-align:none;text-autospace:none'><span
style='font-size:10.0pt;font-family:Arial;color:navy'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='mso-layout-grid-align:none;text-autospace:none'><span
style='font-size:10.0pt;font-family:Arial;color:navy'><span style='mso-tab-count:
1'>            </span><o:p></o:p></span></p>

<p class=MsoPlainText><span style='mso-fareast-font-family:"MS Mincho"'>____________________________________________________________<o:p></o:p></span></p>

<p class=MsoPlainText><i><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt;
font-family:Verdana;mso-fareast-font-family:"MS Mincho"'>TO UNSUBSCRIBE: You
have received this e-mail notifier as a result of your personal 
registration.<BR>To unsubscribe from this e-mail notifier, click
the link below. <o:p></o:p></span></i></p>

<p class=MsoPlainText><i><span style='font-size:8.0pt;mso-bidi-font-size:10.0pt;
font-family:Verdana;mso-fareast-font-family:"MS Mincho"'>
<A HREF="<% On Error Goto 0
            Response.Write objAuthManager.GetUrl("opt-out.asp", False, False, _
                                                  Array("rcp_email", "campitem_id", "campitem_name"), _
                                                  Array(CStr(objProfile.GeneralInfo.email_address), CStr(Request.Cookies("CampaignItemId")), CStr(Request.Cookies("CampaignItemName"))) _
                                                ) %>">Click here to opt-out of future mailings.</A><o:p></o:p></span></i></p>
</div>

</body>

</html>
