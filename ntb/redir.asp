<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<%
' =============================================================================
' Redir.asp
' Redirector tweening page.
' This file handles click-throughs and other events on
' marketing campaign items. It invokes a pipeline that
' records the events in memory and to the IIS log file.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim oPipe, sUrl, sCacheName, oContentList
	Dim dictCache, dictOrder, dictContext
	Dim Errlvl, oFactory, ciid, sEvt, count
	Dim listCount
	Dim bClickRequired, oRows, oFields            

	' Get Pipeline object
	Set oPipe = Application("CampaignsCSFEventPipe")

	' Get the Content cache that this event is associated with
	sCacheName = Request.Querystring("CacheName")

	If sCacheName <> "" Then
		' Get cache object
		Set dictCache = Application("MSCSCacheManager").GetCache(sCacheName)

		' Get name of event to record (default is CLICK)
		sEvt = Request.Querystring("evt")
		If sEvt = "" Then sEvt = "CLICK"
    
		' Uncomment the following line if you want to be able to specify
		' an event count increment other than 1 via the QueryString.
		' count = CLng(Request.Querystring("count"))
    
		' Default count is 1
		If count = 0 Then count = 1 
		ciid = CLng(Request.Querystring("ciid"))
    
		' Set up Order and Context dictionaries and execute the pipeline
		If ciid > 0 Then
			' Create dictionaries
			Set dictOrder = Server.CreateObject("Commerce.Dictionary")
			Set dictContext = Server.CreateObject("Commerce.Dictionary")

			' "_winners" is the item(s) to record the event for
			' (may be a simplelist or a scalar)
			dictOrder("_winners") = ciid

			' "_event" is the name of the event to record.  You cannot
			' record more than one event name per pipeline invocation.
			dictOrder("_event") = sEvt
        
			' Set count of number of events to record.
			' Note that this must be a simplelist, with one
			' list item (a long integer) per "_winner".
			' If the list doesn't exist, the count recorded
			' for all _winners is 1.
			If count <> 1 Then
				Set listCount = Server.CreateObject("Commerce.Simplelist")
				listcount.add(count)
				Set dictorder("_eventcount") = listCount
			End If
        
			' Create a contentlist of items from the given cache
			Set oFactory = dictCache("Factory")
			Set oContentList = oFactory.CreateNewContentList
			Set dictOrder("_content") = oContentList
        
			' Get the in-memory dictionary of campaign item performance
			Set dictOrder("_Performance") = dictCache("_Performance")
			dictContext("SiteName") = sCacheName

			' Look up page group id
			If Request.QueryString("PageGroupId") <> "" Then
				dictContext("PageGroupId") = CLng(Request.QueryString("PageGroupId"))
			Else
				dictContext("PageGroupId") = 0
			End If

			' execute the pipeline to record the event
			oPipe.orderExecute 1, dictOrder, dictContext, Errlvl
        
			' handle click-required discounts
			If LCASE(sEvt)="click" Then
				On Error Resume Next
					' Get the "click_required" boolean for the given campaign item
					Set oRows = oContentList.Search("item_id", ciid)
					If IsObject(oRows) Then
						If Not oRows.EOF Then
							bClickRequired = oRows.Fields("click_required")
							' If it's click-required, Then redirect to a page that will
							' record the item id to the Requisition for use by the OrderDiscount
							' component.
							If bClickRequired Then
								On Error Goto 0
								Call RecordClickToBasket(ciid)	'' include/record_click.asp
							End If
						End If
					End If
				On Error Goto 0            
			End If
		End If
	End If

	' redirect to the supplied URL
	sUrl = Request.Querystring("url")
	If sUrl <> "" Then 
		Response.Redirect(sUrl)
	Else 
		Response.Write(mscsMessageManager.GetMessage("L_NoRedirectionURLSpecified_ErrorMessage", sLanguage))
	End if
End Sub


' -----------------------------------------------------------------------------
' RecordClickToBasket
'
' Description:
'   Records clicks into the OrderGroup for click-required discounts
'   for later use by the OrderDiscount component.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RecordClickToBasket(ciid)
	Dim mscsOrderGrp, sOrderFormName, oListClicked
	' Get the user's orderform and the list of discounts already clicked
	
	Call EnsureAccess()
	
	Set mscsOrderGrp = LoadBasket(m_UserID)
	
	If IsObject(mscsOrderGrp.Value("discounts_clicked")) Then
		Set oListClicked = mscsOrderGrp.Value("discounts_clicked")
	Else
		Set oListClicked = Server.CreateObject("Commerce.SimpleList")
	End If
		
	' Add the discount to the list
	oListClicked.Add(ciid)
		
	' Put the list of clicked discounts into the ordergroup (for unified lookup)
	Set mscsOrderGrp.Value.discounts_clicked = oListClicked
	
	Call mscsOrderGrp.SaveAsBasket()
	Set mscsOrderGrp = Nothing
End Sub
%>
