<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="template/discount.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' basket.asp
' Display page for Basket
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' Prediction Constants -- These must match the PivotColumn for the view/table used in 
' PredictorDataTables in the Data Warehouse
Const PREDICTOR_PIVOT_COLUMN = "SKU"
Const PREDICTOR_AGGREGATE_COLUMN = "QTY"


Sub Main()
	Dim mscsOrderGrp
    Dim iErrorLevel, i, bBasketIsEmpty, bMustSaveBasket
    Dim iErrorCount
	Dim sOrderFormName
	Dim oOrderFormDisplayOrder
	
	Call EnsureAccess()
	Call InitializeBasketPage(mscsOrderGrp, bBasketIsEmpty, bMustSaveBasket, oOrderFormDisplayOrder)
	
	If Not bBasketIsEmpty Then
		' Run basket pipeline to check for errors; set oOrderFormDisplayOrder
		Call CheckBasket(mscsOrderGrp, bBasketIsEmpty, iErrorCount, bMustSaveBasket, oOrderFormDisplayOrder)
	End If
	
	If Not bBasketIsEmpty Then
		' Add discount footnote symbols to each lineitem
		Call AddDiscountMessages(mscsOrderGrp)
	End If

	If bMustSaveBasket Then
		' Save the basket (changes may have occurred when running the pipeline)
		Call mscsOrderGrp.SaveAsBasket()  
	End If
	
	Call RenderBasketPage(mscsOrderGrp, oOrderFormDisplayOrder, bBasketIsEmpty, iErrorCount)
End Sub


' -----------------------------------------------------------------------------
' InitializeBasketPage
'
' Description:
'   Helper sub to initialize variables; called by Main
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub InitializeBasketPage(ByRef objOrderGroup, ByRef bBasketIsEmpty, ByRef bMustSaveBasket, ByRef oOrderFormDisplayOrder)
	bBasketIsEmpty = True
	
	Set objOrderGroup = LoadBasket(m_UserID)

	If objOrderGroup.Value(TOTAL_LINEITEMS) > 0 Then
		bBasketIsEmpty = False
	End If
	bMustSaveBasket = False 'Assume that view basket operations don't require a SaveBasket.
                                'This may be an invalid assumption for certain pipelines.
                                'If this applies to your pcf, initialize this variable 
                                'to "Not bBasketEmpty"
	Set oOrderFormDisplayOrder = Nothing
End Sub


' -----------------------------------------------------------------------------
' CheckBasket
'
' Description:
'   Get the updated basket information
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CheckBasket(mscsOrderGrp, ByRef bBasketIsEmpty, ByRef iErrorCount, ByRef bMustSaveBasket, ByRef oOrderFormDisplayOrder)
	Dim iErrorLevel
	
	' Remove empty orderforms to prevent basket pipeline from raising pur_noitems purchase error.
	Call RemoveEmptyOrderForms(mscsOrderGrp)
		
    ' First, run the pipeline, to get the latest information.
    iErrorLevel = RunMtsPipeline(MSCSPipelines.Basket, GetPipelineLogFile("Basket"), mscsOrderGrp)
        
	' Sort the orderforms
	Set oOrderFormDisplayOrder = SortDictionaryKeys(mscsOrderGrp.Value(ORDERFORMS))
		
	If iErrorLevel > 1 Then
		' Handle basket errors only. Components in basket pipeline must not raise purchase errors.
		' Calling RemoveEmptyOrderForms() before executing the basket pipeline eliminates the 
		' possibility of pur_noitems purchase error being raised.
		iErrorCount = GetErrorCount(mscsOrderGrp, BASKET_ERRORS)
		If iErrorCount > 0 Then
			' If there is a basket error, we should save the pipeline, so that 
        	        ' the same error doesn't keep occuring.  An alternative implementation would
                	' be to prompt for user acknowledgement upon basket error
	                bMustSaveBasket = True
		ElseIf GetErrorCount(mscsOrderGrp, PURCHASE_ERRORS) > 0 Then
			Err.Raise vbObjectError + 2111, , mscsMessageManager.GetMessage("L_Bad_Pipeline_Warning_ErrorMessage", sLanguage)
		Else
	           Err.Raise vbObjectError + 2110, , mscsMessageManager.GetMessage("L_Unspecified_Pipeline_Warning_ErrorMessage", sLanguage)
		End If

		' Pipeline run may have removed the last item from the basket. Cannot learn this 
		' from mscsOrderGrp.Value(TOTAL_LINEITEMS) since pipeline run does not update it.
		If GetLineItemsCount(mscsOrderGrp) = 0 Then
			bBasketIsEmpty = True
		End If
	End If
End Sub


' -----------------------------------------------------------------------------
' CreatePOIndexes
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CreatePOIndexes(mscsOrderGrp)
	Dim i, sOrderFormName, oOrderForm, oItem
	For Each sOrderFormName In mscsOrderGrp.value(ORDERFORMS)
		Set oOrderForm = mscsOrderGrp.value(ORDERFORMS).Value(sOrderFormName)
		i=0
		For Each oItem In oOrderForm.Items
			oItem.value("_poname") = sOrderFormName
			oItem.value("_poindex") = i		
			i = i + 1
		Next
	Next
End Sub	


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' RenderBasketPage
'
' Description:
'    Render the Basket Page, including basket contents, discounts, errors
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub RenderBasketPage(mscsOrderGrp, oOrderFormDisplayOrder, bBasketIsEmpty, iErrorCount)
	Dim htmTitle, htmContent, urlLink
	Dim BasketAffinity, ProductAffinity, nDiscountsToShow

	sPageTitle = mscsMessageManager.GetMessage("L_Basket_HTMLTitle", sLanguage)

	If iErrorCount > 0 Then
		htmContent = htmContent & htmRenderBasketWarnings(mscsOrderGrp, oOrderFormDisplayOrder, BASKET_ERRORS) & CRLF
	End If
	
	If Not bBasketIsEmpty Then		
		htmContent = htmContent & htmRenderBasket(mscsOrderGrp, oOrderFormDisplayOrder)
		
		' Render discount messages
		htmContent = htmContent & htmRenderDiscountMessages(mscsOrderGrp)
		
		' Render the checkout button
		htmContent = htmContent & htmRenderCheckoutButton()
		
		' Render the predictor's list of recommendations.
		htmContent = htmContent & htmRenderPredictions(mscsOrderGrp, oOrderFormDisplayOrder)
	Else 
		htmContent = htmContent & RenderText(mscsMessageManager.GetMessage("L_BASKET_EMPTY_ORDER_TEXT", sLanguage), MSCSSiteStyle.Body) & BR
		
		Dim htmLinkText
		urlLink = GenerateURL(MSCSSitePages.Catalog, Array(), Array())
		htmLinkText = RenderText(mscsMessageManager.GetMessage("L_Browse_Our_Catalogs_HTMLText", sLanguage), MSCSSiteStyle.Body)
		htmContent = htmContent & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)
	End If

	htmTitle = RenderText(mscsMessageManager.GetMessage("L_Basket_HTMLTitle", sLanguage), MSCSSiteStyle.Title) & CRLF
	
	htmPageContent = htmTitle & htmContent

	' Populate the discount banner slot
	ProductAffinity = Null
	Set BasketAffinity = CollectBasketItems(mscsOrderGrp)
	nDiscountsToShow = 1
	htmDiscountBannerSlot = RenderDiscounts(ProductAffinity, BasketAffinity, nDiscountsToShow)
End Sub


' -----------------------------------------------------------------------------
' htmRenderBasketWarnings
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderBasketWarnings(mscsOrderGrp, oOrderFormDisplayOrder, sErrCollectionName)
	Dim sErr, sOrderFormName, mscsOrderForm, htmContent
	
	For Each sOrderFormName In oOrderFormDisplayOrder
		Set mscsOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		For Each sErr In mscsOrderForm.Value(sErrCollectionName)
			htmContent = htmContent & RenderText(sErr, MSCSSiteStyle.Warning) & BR
		Next
	Next
	
	htmRenderBasketWarnings = htmContent
End Function

	
' -----------------------------------------------------------------------------
' htmRenderBasket
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderBasket(mscsOrderGrp, oOrderFormDisplayOrder)
	Dim sOrderFormName, mscsOrderForm, dictItem, listAggregatedItems, sName, sBtnText
	Dim i, urlLink, urlAction, bDiscountApplied, sProdCode, htmLinkText
	Dim htmBasketHeaderRow, htmBasketDataRow, htmQtyCol, htmProdCode, htmRemoveCol, htmTotalRow
	Dim arrData, arrParams, arrParamVals, arrDataAttLists
	
	bDiscountApplied = False
	
	Set listAggregatedItems = Server.CreateObject("Commerce.SimpleList")
	For Each sOrderFormName in oOrderFormDisplayOrder
		Set mscsOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		If mscsOrderForm.Value("_winners").Count > 0 Then
			bDiscountApplied = True
		End If
		For Each dictItem In mscsOrderForm.Items
			Call listAggregatedItems.Add(dictItem)
		Next
	Next
	
    Call CreatePOIndexes(mscsOrderGrp)
    
	If bDiscountApplied Then
		arrData = Array( _
			mscsMessageManager.GetMessage("L_BASKET_QUANTITY_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_ProductCode_HTMLText", sLanguage), _			
			mscsMessageManager.GetMessage("L_Product_Name_DisplayName_HTMLText", sLanguage), _
			mscsMessageManager.GetMessage("L_Product_Description_DisplayName_HTMLText", sLanguage), _	    				
			mscsMessageManager.GetMessage("L_BASKET_UNITPRICE_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_DISCOUNT_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_MESSAGES_COLUMN_TEXT", sLanguage), _	    	
			mscsMessageManager.GetMessage("L_BASKET_TOTALPRICE_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_REMOVE_COLUMN_TEXT", sLanguage))
	Else
		arrData = Array( _
			mscsMessageManager.GetMessage("L_BASKET_QUANTITY_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_ProductCode_HTMLText", sLanguage), _			
			mscsMessageManager.GetMessage("L_Product_Name_DisplayName_HTMLText", sLanguage), _
			mscsMessageManager.GetMessage("L_Product_Description_DisplayName_HTMLText", sLanguage), _	    				
			mscsMessageManager.GetMessage("L_BASKET_UNITPRICE_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_TOTALPRICE_COLUMN_TEXT", sLanguage), _
			mscsMessageManager.GetMessage("L_BASKET_REMOVE_COLUMN_TEXT", sLanguage))
	End If
	
	htmRenderBasket = htmRenderBasket & RenderTableHeaderRow(arrData, Array(), MSCSSiteStyle.TRCenter)
	
	If bDiscountApplied Then
		arrDataAttLists = Array( _
			MSCSSiteStyle.TDCenter, _
			MSCSSiteStyle.TDLeft, _			
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDLeft, _			
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDCenter)
	Else
		arrDataAttLists = Array( _
			MSCSSiteStyle.TDCenter, _
			MSCSSiteStyle.TDLeft, _			
			MSCSSiteStyle.TDLeft, _
			MSCSSiteStyle.TDLeft, _			
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDRight, _
			MSCSSiteStyle.TDCenter)
	End If

	For i = 0 To listAggregatedItems.Count -1
		Set dictItem = listAggregatedItems(i)

		sName = PRODUCT_QTY_URL_KEY & "_" & dictItem.Value("_poname") & "_" & dictItem.Value("_poindex")
		htmQtyCol = RenderTextBox(sName, dictItem.quantity, 3, 3, MSCSSiteStyle.TextBox) 
			
		sBtnText = mscsMessageManager.GetMessage("L_Update_Button", sLanguage)
		htmQtyCol = htmQtyCol & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)

		arrParams = Array(CATALOG_NAME_URL_KEY, CATEGORY_NAME_URL_KEY, PRODUCT_ID_URL_KEY, VARIANT_ID_URL_KEY)
		arrParamVals = Array(dictItem.Value("product_catalog"), dictItem.Value("product_category"), dictItem.Value("product_id"), dictItem.Value("product_variant_id"))
		urlLink = GenerateURL(MSCSSitePages.Product, arrParams, arrParamVals)

		sProdCode = dictItem.Value("product_id")
		If Not IsNull(dictItem.Value("product_variant_id")) Then
			sProdCode = sProdCode & "-" & dictItem.Value("product_variant_id")
		End If
		
		htmLinkText = RenderText(sProdCode, MSCSSiteStyle.Body)
		htmProdCode = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)
			
		arrParams = Array(CATALOG_NAME_URL_KEY, CATEGORY_NAME_URL_KEY, PRODUCT_ID_URL_KEY, VARIANT_ID_URL_KEY, ORDERFORM_NAME, PO_ITEMINDEX_URL_KEY, ITEM_INDEX_URL_KEY)
		arrParamVals = Array(dictItem.Value("product_catalog"), dictItem.Value("product_category"), dictItem.Value("product_id"), dictItem.Value("product_variant_id"), dictItem.Value("_poname"), dictItem.Value("_poindex"), i)
		urlLink = GenerateURL(MSCSSitePages.DeleteItem, arrParams, arrParamVals)
		htmLinkText = RenderText(mscsMessageManager.GetMessage("L_Remove_Item_Link_HTMLText", sLanguage), MSCSSiteStyle.Body)
		htmRemoveCol = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)
			
		If bDiscountApplied Then
			' "name" and "description" are required product properties and cannot have null values.		
			arrData = Array(_
				htmQtyCol, _
				htmProdCode, _
				dictItem.Value("_product_name"), _				
				dictItem.Value("_product_description"), _
				htmRenderCurrency(dictItem.Value("_cy_iadjust_currentprice")), _
				htmRenderCurrency(dictItem.Value("_cy_oadjust_discount")), _
				dictItem.Value("_messages"), _
				htmRenderCurrency(dictItem.Value("_cy_oadjust_adjustedprice")), _
				htmRemoveCol)
		Else
			' "name" and "description" are required product properties and cannot have null values.
			arrData = Array(_
				htmQtyCol, _
				htmProdCode, _
				dictItem.Value("_product_name"), _								
				dictItem.Value("_product_description"), _				
				htmRenderCurrency(dictItem.Value("_cy_iadjust_currentprice")), _
				htmRenderCurrency(dictItem.Value("_cy_oadjust_adjustedprice")), _
				htmRemoveCol)
		End If

		htmRenderBasket = htmRenderBasket & RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)
	Next
		
	urlLink = GenerateURL(MSCSSitePages.DeleteAllItems, Array(), Array())
	htmLinkText = RenderText(mscsMessageManager.GetMessage("L_RemoveAll_HTMLText", sLanguage), MSCSSiteStyle.Body)
	arrData = Array(_
		NBSP, _
		mscsMessageManager.GetMessage("L_BASKET_SUBTOTAL_COLUMN_TEXT", sLanguage), _
		htmRenderCurrency(mscsOrderGrp.value.saved_cy_oadjust_subtotal), _
		RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link))
	
	If bDiscountApplied Then
		arrDataAttLists = Array(" COLSPAN='6'", MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDRight, MSCSSiteStyle.TDCenter)
	Else
		arrDataAttLists = Array(" COLSPAN='4'", MSCSSiteStyle.TDLeft, MSCSSiteStyle.TDRight, MSCSSiteStyle.TDCenter)	
	End If
	
	htmTotalRow = RenderTableDataRow(arrData, arrDataAttLists, MSCSSiteStyle.TRMiddle)
	
	htmRenderBasket = htmRenderBasket & htmTotalRow
	htmRenderBasket = RenderTable(htmRenderBasket, MSCSSiteStyle.BasketTable)
	urlAction = GenerateURL(MSCSSitePages.EditItemQuantities, Array(), Array())
	htmRenderBasket = RenderForm(urlAction, htmRenderBasket, HTTP_POST)
End Function


' -----------------------------------------------------------------------------
' htmRenderDiscountMessages
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderDiscountMessages(mscsOrderGrp)
	htmRenderDiscountMessages = htmRenderDiscountsApplied(mscsOrderGrp) & _
		htmRenderDiscountMessageGroup(mscsOrderGrp, "_discounts_removed", mscsMessageManager.GetMessage("L_BASKET_DISCOUNTS_REMOVED_TEXT", sLanguage)) & _
		htmRenderDiscountMessageGroup(mscsOrderGrp, "discounts_clicked", mscsMessageManager.GetMessage("L_BASKET_DISCOUNTS_CLICKED_TEXT", sLanguage))
End Function


' -----------------------------------------------------------------------------
' htmRenderDiscountsApplied
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderDiscountsApplied(mscsOrderGrp)
	Dim htmContent
	Dim oCiidCollection, oContentList
	Dim sOrderFormName, oOrderForm, iItem
	Dim sShippingDiscountDescription, listShippingDiscounts
	Dim ciid
	Dim dictDescriptions 

	Set dictDescriptions = GetDictionary()
	
	' Get the applied discounts
	' Put the applied ciid's in a dictionary, so you only show one of each
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		Set oOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		Set oContentList = oOrderForm.Value("_discounts")
		
		For Each iItem In oOrderForm.Items
			If IsObject(iItem.Value("discounts_applied")) Then
				Set oCiidCollection = iItem.Value("discounts_applied")
				If oCiidCollection.Count > 0 Then
					For Each ciid In oCiidCollection
						dictDescriptions(ciid) = GetDiscountDescription(ciid, oContentList)
					Next
				End If
			End If
		Next
	Next
	
	' Get any shipping discounts
	Set listShippingDiscounts = GetSimpleList()
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		Set oOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		sShippingDiscountDescription = oOrderForm.Value("_shipping_discount_description")
		If Not IsNull(sShippingDiscountDescription) Then
			listShippingDiscounts.Add(sShippingDiscountDescription)
		End If
	Next
	
	' Display any applied discounts
	If dictDescriptions.Count > 0 Or listShippingDiscounts.Count > 0 Then
		htmContent = Bold(mscsMessageManager.GetMessage("L_BASKET_DISCOUNTS_APPLIED_TEXT", sLanguage)) & CR
	
		For Each ciid In dictDescriptions
			htmContent = htmContent & dictDescriptions(ciid)
		Next
		
		For Each sShippingDiscountDescription In listShippingDiscounts
			htmContent = htmContent & sShippingDiscountDescription
		Next
		
		htmRenderDiscountsApplied = RenderPreFormattedText(htmContent, GetDictionary())
	End If	
End Function


' -----------------------------------------------------------------------------
' htmRenderDiscountMessageGroup
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderDiscountMessageGroup(mscsOrderGrp, sCiidCollection, sMessageGroupDescription)
	Dim htmContent
	Dim oCiidCollection, oContentList
	Dim sOrderFormName, oOrderForm
	Dim ciid
	Dim dictDescriptions 
	Set dictDescriptions = GetDictionary()
	
	' Put the applied ciid's in a dictionary, so you only show one of each
	For Each sOrderFormName In mscsOrderGrp.Value(ORDERFORMS)
		Set oOrderForm = mscsOrderGrp.Value(ORDERFORMS).Value(sOrderFormName)
		Set oContentList = oOrderForm.Value("_discounts")
		If IsObject(oOrderForm.Value(sCiidCollection)) Then
			Set oCiidCollection = oOrderForm.Value(sCiidCollection)
			If oCiidCollection.Count > 0 Then
				For Each ciid In oCiidCollection
					dictDescriptions(ciid) = GetDiscountDescription(ciid, oContentList)
				Next
			End If
		End If
	Next
	
	If dictDescriptions.Count > 0 Then
		htmContent = Bold(sMessageGroupDescription) & CR
	
		For Each ciid In dictDescriptions
			htmContent = htmContent & dictDescriptions(ciid)
		Next
		htmRenderDiscountMessageGroup = RenderPreFormattedText(htmContent, GetDictionary())
	End If	
End Function


' -----------------------------------------------------------------------------
' htmRenderCheckoutButton
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCheckoutButton()
	Dim htmBodyContent, urlAction, sBtnText
	
	If m_UserType = AUTH_USER Then
	    If dictConfig.i_AddressBookOptions <> ADDRESSBOOK_DISABLED Then
			urlAction = GenerateURL(MSCSSitePages.AddressBook, Array(), Array())
		Else
			urlAction = GenerateURL(MSCSSitePages.AddressForm, Array(), Array())
		End If
	ElseIf m_UserType = GUEST_USER Then
			urlAction = GenerateURL(MSCSSitePages.AddressForm, Array(), Array())	
	End If

	sBtnText = mscsMessageManager.GetMessage("L_CheckOut_Button", sLanguage)
	htmBodyContent = RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)
    htmRenderCheckoutButton = RenderForm(urlAction, htmBodyContent, HTTP_POST)
End Function 


' -----------------------------------------------------------------------------
' htmRenderPredictions
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderPredictions(mscsOrderGrp, oOrderFormDisplayOrder)
	Dim sOrderFormName, oOrderForm, htmContent, dItem, strPropName, varValue, lQuantity
	Dim oPredictor, slToPredict, dCase
	Dim arPredictedProps, arPredictedVals
	Dim lMaxPredictions, i, lPredictionCount
	Dim strHREF
	
	Set oPredictor = Application("MSCSPredictor")
	If oPredictor Is Nothing Then
	    ' Display this string in development mode to
	    ' allow discovery of feature even if there are no recommendations.
	    
	    If MSCSEnv = DEVELOPMENT Then
	        htmRenderPredictions = "<B>" & mscsMessageManager.GetMessage("L_RecommendationsCaption_HTMLText", sLanguage) & "</B><BR/>"
	    End If
	    Exit Function
	End If    
   	
   	' Construct the input case from the items in the basket.
   	' The constructed property name in the input dictionary
   	' is of the form: [Catalog Name].[Product ID].  This
   	' must correspond to the format used for "SKU" in the
   	' Trans_Predictor view in the data warehouse.  Thes means 
   	' that we treat all variants of a product equally 
   	' for the purposes of prediction.
   	
   	Set dCase = GetDictionary()
   	Set slToPredict = GetSimpleList()
   	
   	' Add list of properties to predict.  For product recommendations,
   	' we give the name of the PivotColumn defined in PredictorDataTables.
   	slToPredict.Add PREDICTOR_PIVOT_COLUMN
   	
	For Each sOrderFormName in oOrderFormDisplayOrder
		Set oOrderForm = mscsOrderGrp.value.OrderForms.Value(sOrderFormName)
		For Each dItem in oOrderForm.Items
		    strPropName = PREDICTOR_AGGREGATE_COLUMN & "([" & dItem.product_catalog & "].[" & dItem.product_id & "])"
		    lQuantity = CLng(dItem.quantity)
		    dCase(strPropName) = lQuantity
		Next
	Next
	
	' Call the predictor.
	lMaxPredictions = 5 'Display no more than this many predictions.
	
	oPredictor.Predict dCase, slToPredict, arPredictedProps, arPredictedVals, lMaxPredictions
	If IsArray(arPredictedProps) Then
	    htmContent = "<B>" & mscsMessageManager.GetMessage("L_RecommendationsCaption_HTMLText", sLanguage) & "</B><BR/>"
	    For i = 0 To UBound(arPredictedProps)
	        strHREF = PredictedPropToHREF(arPredictedProps(i))
	        htmContent = htmContent & strHREF & "<BR>"
	    Next
	    
	    arPredictedProps = Empty
	    arPredictedVals = Empty
	End If
	
	htmRenderPredictions = htmContent
End Function


' -----------------------------------------------------------------------------
' PredictedPropToHREF
'
' Description:
'   Construct a hyperlink into the catalog from the prediction information.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function PredictedPropToHREF(strProp)
    Dim regEx1
    Dim arProps
    Dim strCatalogName
    Dim strProductID
    Dim strProps
    Dim strHREF
    
    Set regEx1 = New RegExp
    
    ' The prediction property will return in the form:
    ' QTY([Catalog Name].[Product ID])
    
    regEx1.Pattern = PREDICTOR_AGGREGATE_COLUMN & "\(\[(.+)\]\.\[(.+)\]\)"
    strProps = regEx1.Replace(strProp, "$1;$2")
    arProps = Split(strProps, ";")
    
    strCatalogName = arProps(0)
    strProductID = arProps(1)
    
    PredictedPropToHREF = "<A HREF=" & DoubleQuote(GenerateURL(MSCSSitePages.Product, Array(), Array()) & "?" & _
	    CATALOG_NAME_URL_KEY & "=" & strCatalogName & "&" & _
		PRODUCT_ID_URL_KEY & "=" & strProductID & "&") & _
		">" & strProductID & "</A>"
		
End Function

%>
