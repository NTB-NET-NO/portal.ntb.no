<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<%
' =============================================================================
' _setship.asp
' Tween page for setting the shipping method in the user's basket.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim mscsOrderGrp, dictMethods
    Dim htmTitle, htmContent
    Dim sSelectedMethodName, sSelectedMethodID
    
	Call EnsureAccess()

	sSelectedMethodID = GetRequestString(SHIPPING_METHOD_URL_KEY, Null)
    	
	Set mscsOrderGrp = LoadBasket(m_UserID)
    Call CatchErrors(mscsOrderGrp, sSelectedMethodID)
	
	sSelectedMethodName = GetShippingMethodNameFromID(sSelectedMethodID)
	
    Call mscsOrderGrp.PutItemValue(SHIPPING_METHOD_KEY, sSelectedMethodID, True)
    Call mscsOrderGrp.PutItemValue(SHIPPING_METHOD_NAME, sSelectedMethodName, True)              
    
    Call mscsOrderGrp.SaveAsBasket()
    Response.Redirect(GenerateURL(MSCSSitePages.OrderSummary, Array(), Array())) 
End Sub


' -----------------------------------------------------------------------------
' CatchErrors
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub CatchErrors(mscsOrderGrp, ByVal sSelectedMethodID)
	If mscsOrderGrp.Value(TOTAL_LINEITEMS) = 0 Then
		Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
	End If

    If IsNull(sSelectedMethodID) Then
        Response.Redirect(GenerateURL(MSCSSitePages.ShippingMethods, Array(), Array()))
    End If
    
    If Not bIsShippingMethodSupported(sSelectedMethodID) Then
		Response.Redirect(GenerateURL(MSCSSitePages.ShippingMethods, Array(), Array()))
    End If
End Sub


' -----------------------------------------------------------------------------
' bIsShippingMethodSupported
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function bIsShippingMethodSupported(ByVal sSelectedMethodID)
	Dim dictMethods, sMethodID
	
	bIsShippingMethodSupported = False
	Set dictMethods = Application("MSCSCacheManager").GetCache("ShippingManagerCache").Cache
	
	For Each sMethodID in dictMethods
		If sMethodID = sSelectedMethodID Then
			bIsShippingMethodSupported = True
			Exit For
		End if
	Next
End Function


' -----------------------------------------------------------------------------
' GetShippingMethodNameFromID
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetShippingMethodNameFromID(ByVal sSelectedMethodID)
    Dim dictMethods, sMethodID
    
	Set dictMethods = Application("MSCSCacheManager").GetCache("ShippingManagerCache").Cache
	
	For Each sMethodID in dictMethods
		If sMethodID = sSelectedMethodID Then
			GetShippingMethodNameFromID = dictMethods.Value(sMethodID).Value(SHIPPING_METHOD_NAME)
			Exit For
		End if
	Next
End Function
%>
