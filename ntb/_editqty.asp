<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<%
' =============================================================================
' _editqty.asp
' Tween page for changing an item's quantity from the user's basket.
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
    Dim mscsOrderGrp, mscsOrderForm
    Dim sOrderFormName, i, iQty, iErrorLevel
	
	Call EnsureAccess()
    
	Set mscsOrderGrp = LoadBasket(m_UserID)
		
	For Each sOrderFormName In mscsOrderGrp.Value.OrderForms
		Set mscsOrderForm = mscsOrderGrp.Value.OrderForms.Value(sOrderFormName)
		For i = mscsOrderForm.Items.Count - 1 To 0 Step -1
		    iQty = MSCSAppFrameWork.RequestNumber(PRODUCT_QTY_URL_KEY & _
		        "_" & sOrderFormName & "_" & CStr(i), Null, 0, 999)
		    If IsNull(iQty) Then
		        Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))
		    Else
		        If iQty = 0 Then
		            Call mscsOrderGrp.RemoveItem(i, sOrderFormName) 
		        Else
		            Call mscsOrderGrp.PutItemValue( _
													  ORDERFORM_PRODUCT_QTY_KEY, _
													  iQty, _
													  True, _
													  i, _
													  sOrderFormName _
												  )
		        End if
		    End if
		Next
	Next
    
	Call mscsOrderGrp.SaveAsBasket()
	Call Response.Redirect(GenerateURL(MSCSSitePages.Basket, Array(), Array()))     
End Sub
%>
