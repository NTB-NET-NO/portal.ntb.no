<%
'***********************************************************************************
' periframe.asp
'
' This file is used to show the spesified newslist in the User_homepage.asp
'
' USES:
'		NTB_layout_lib.asp - For rendering of titlebar
'		NTB_admin_render_lib.asp - For rendering of the userlist
'		NTB_profile_lib.asp - For profilefunctions that search in the database
'
'
' NOTES:
'		none
'
' CREATED BY: Solveig Skjermo, Andersen
' CREATED DATE: 2002.04.16
' UPDATED BY: Solveig Skjermo, Andersen
' UPDATED DATE: 2002.06.28
' REVISION HISTORY:
' 2002.07.24 Roar Vestre, NTB: Bugfix to allow the use of " @ and $ in search string 
' 2002.08.07 Roar Vestre, NTB: Added "forrige 48 timer"- and "siste 24 timer" link
' 2002.09.02 Lars Christian Hegde, NTB: Bugfix: 404 error after autorefresh after 
'			 changing a text search box.
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_personal_render_lib.asp" -->

<!-- #INCLUDE FILE="../include/setupenv.asp" -->
<!-- #INCLUDE FILE="../include/const.asp" -->
<!-- #INCLUDE FILE="../include/html_lib.asp" -->
<!-- #INCLUDE FILE="../include/catalog.asp" -->
<!-- #INCLUDE FILE="../include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="../include/std_util_lib.asp" -->

<%
Dim style
'if Session("Browser") = "" then
'	Response.Redirect "../Authfiles/Login.asp"
'elseif .....
if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

'get the personal userinfo for this frame
Dim newsString, hSearch, sSearch, orgSearch, intPlace, sNewsInfo 'sJavaTextbox
Dim tmpArray, infoArray, isCatagory, newsSel

newsString = Request.QueryString ("newsSel")
hSearch = Request.Form("hidChange")
sSearch = Request.Form("txtSearch")
orgSearch = Request.Form("hidSearch")
intPlace = Request.Form("hidPlace")


%>

<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="<%=style%>" type="text/css" rel="stylesheet">

	<script language="JavaScript">

   //function that scroll down the page to the same spot as the page
   //was prior to refresh
   
   function setScroll(){
   //a variable y is added to the url after the character zzz used for splitting purpose in the refreshTOHere-function
   //this y variable is fetched from the url-string and used to decide how much to scroll
    if(location.search){
   	  var tempArray=location.search.split("zzz");
	  //lastArrayElement =tempArray.pop();
	  lastArrayElement = tempArray[tempArray.length - 1];
	  yLength = lastArrayElement.length
	  y=lastArrayElement.slice(2);
	  window.scrollTo(0,y);
    }
   }

  //function that adds a y-value indicating the amount of scrolling that the user has performed
  function refreshToHere(){
  
    if ( typeof(window.pageYOffset) == "number" ) {
	    y=window.pageYOffset;
    } else {
		y=document.body.scrollTop;
    }
    
	//Vars from form submit
	srch = '<%=sSearch%>'
	place = '<%=intPlace%>'

    if ( location.href.indexOf("newsSel") == -1 ) {
		//rebuild path from vars, needed for text searches after submit only
		loc = location.href + "?newsSel=TextSearch$" + srch + "@" + place + "&zzzy=" + y;
		//alert(loc);
		location.href = loc;
    } else {
		location.href=location.href.replace(/\&zzz.*/,"")+"&zzzy="+y;
    }
       
  }

	//function to open news items in a seperate window
	var myWindow;

	function w(aid) {
		var myURL;
		// myURL = '../mainpages/viewnewsitem.asp?aid=' + aid;
		myURL = '../template/x.asp?a=' + aid;
	  if(myWindow && !myWindow.closed) { // window is open
	    myWindow.location = myURL;
	    myWindow.focus();
	  } else {
	    leftPlacement = screen.width - 610; //ensures that the window pops up to the right independent on screen resolution
	    myName = 'PopupPage';
	    myOptions = 'height=600,width=600,scrollbars=yes,resizable=yes,top=80, left='+ leftPlacement;
	    myWindow = window.open(myURL, myName, myOptions);
	    myWindow.focus();
	  }
	}

	//command that sets the refresh rate
	setTimeout("refreshToHere()",300000); //15000

</script>
</head>
<body bgcolor="#ffffff" link="#61c7d7" vlink="#61c7d7" alink="#61c7d7" onload="setScroll()">
<%

Sub Main ()
End Sub

Dim TimePeriod
Dim amount
If Request.QueryString("timePeriod")<>"" Then
	If Request.QueryString("timePeriod") = "48" Then
      TimePeriod = "48"
      amount = 1
    Else
	  TimePeriod = "24"
	  amount = 0
    End If
End If

If (timeperiod = "48") Then
	'Show first 24 hours link if the news list displays last 48 hours -->
	'replace the " " with + signs so the QueryParser interpret the string correctly
	newsSel = Replace(newsString, " ","+")
	Response.Write ("<div class=""news"">Vis <a href=""../template/periframe.asp?timePeriod=24&newsSel=" & newsSel & """ target=_self> siste</a> 24 timer</div>")
	'Response.Write ("<br>")	'add a line break before links
End if

if (newsString <> "") AND (Left(newsString,10) = "TextSearch") then
'if the user have a searchwindow spesified

	'get the infoplace
	tmpArray = Split(newsString, "@", -1)
	intPlace = tmpArray(1)

	'get the search word
	infoArray = Split(tmpArray(0),"$",-1)

	if UBound(infoArray) < 1 then
	'no searchword saved in profile
		Response.Write RenderPerSearch("", intPlace)

	else
	'if a searchstring is present we ask Autonomy for help
		sSearch = infoArray(1)

		'Bugfix to allow the ues of " in search string (RoV):
		sSearch = Replace(sSearch, "&quot;", """")
		sSearch = Replace(sSearch, "{alfa}", "@")
		sSearch = Replace(sSearch, "{usd}", "$")

		'make the site
		Response.Write RenderPerSearch(sSearch, intPlace)

		'ask autonomy for help
		Response.Write SearchAutonomy(sSearch)
	end if

elseif hSearch = "startSearch" then
'if the user has hit the search-button or the page as refreshed

	if sSearch <> orgSearch then
	'if new search has been made -> save to profile

		'Bugfix to allow the ues of " in search string (RoV):
		dim sTempSearch
		sTempSearch = Replace(sSearch, """", "&quot;")
		sTempSearch = Replace(sTempSearch, "@", "{alfa}")
		sTempSearch = Replace(sTempSearch, "$", "{usd}")

		sNewsInfo = "TextSearch$" & sTempSearch
		Call UpdateNewsInfo(sNewsInfo, intPlace)

	end if

	'make the site
	Response.Write RenderPerSearch(sSearch, intPlace)

	'ask autonomy for help
	Dim tmp
	tmp = SearchAutonomy(sSearch)
	'Response.Write SearchAutonomy(sSearch)
	Response.Write tmp
else
	isCatagory = true
'render a categorylist-area
	Dim newsInfo, catInfo, lokGeo, wrlGeo
	Dim oList, newsList, lastid

	if newsString <> "" then
	'if there are personalized information, then display

		'replace the " " with + signs so the QueryParser interpret the string correctly
		newsString = Replace(newsString, " ","+")

		newsInfo = Split(newsString, "|", -1)
		catInfo = newsInfo(0)
		wrlGeo = newsInfo(1)
		lokGeo =newsInfo(2)

		'Get the querystring from QueryParser
		Set oList = Server.CreateObject("QueryParser.comQueryParser")
		newsList = oList.MakeFilterString(catInfo, wrlGeo, lokGeo)

		'Get the newslist
		Dim rsList
		'On Error Resume Next ' Commented out by RoV 06.08.2002
		
		'Hjelper dette?? Lch - 23/8/2002 (Ikke testet)
		'
		'if oMemDb = nothing then
		'	oMemDb = Server.CreateObject("memdb.portaldata")
		'	oMemDb.Loadcache()
		'end if 
				
		Set rsList = oMemDb.FetchForLists(newsList, 0, amount) ' 1=prev. 48h, 0=24h ' Set rsList = oMemDb.FetchForLists(newsList)
		'On Error GoTo 0 ' Commented out by RoV 06.08.2002
		if TypeName(rsList) <> "Nothing" then
			rsList.Sort = "CreationDateTime DESC"

			if not rsList.eof then
				'if there are records present in the recordset
				rsList.MoveFirst
				'lastid = rsList(0)

				'add the recordSet to the Session
				Dim RecordSetADTG
				Dim SaveStream
				Set SaveStream = Server.CreateObject("ADODB.Stream")

				rsList.Save SaveStream
				RecordSetADTG = SaveStream.Read

				'tidy up the stream now we have finished with it
				SaveStream.Close
				Set SaveStream = nothing

				'Get the html-code of the list
				Dim htmlList, htmlStream
				Set htmlList = Server.CreateObject("Formating.rsFormating")
				Set htmlStream = htmlList.FormatRecordset(rsList)
				Response.Write(htmlStream.ReadText)
			else
			'if no records present do nothing
				'lastid = 0
				Response.Write "<br><div class='hjelpetekst'>Det finnes ingen aktuelle "_
					& "nyheter innen denne spesifiseringen</div>"
			end if
		end if
		Set rsList = Nothing
	else
		Response.Write "<br><div class='hjelpetekst'>Denne ruten kan spesifiseres til "_
				& "� vise de nyhetene som <i>du</i> �nsker</div>" & newsString
	end if
end if

if isCatagory then
	If (timeperiod <> "48") Then
		Response.Write ("<br><div class=""news"">Vis <a href=""../template/periframe.asp?timePeriod=48&newsSel=" & newsString & """ target=_self> forrige</a> 48 timer</div>")
	else
		'Show first 24 hours link if the news list displays last 48 hours -->
		Response.Write ("<br><div class=""news"">Vis <a href=""../template/periframe.asp?timePeriod=24&newsSel=" & newsString & """ target=_self> siste</a> 24 timer</div>")
	End if
end if

' -----------------------------------------------------------------------------
' SearchAutonomy(ByVal strSearch)
'
' Description: Search in Autonomy and returns a list of matches
'
' Parameters:
'
' Returns:
'
' Notes :
'
' -----------------------------------------------------------------------------

Function SearchAutonomy(ByVal strSearch)

	Dim al, list
	Dim a_csn

	'use the autonomy COM-object to get list over searched item
	a_csn = application("autonomy_cns")

	Set al = Server.CreateObject("AutonomyInterface.comAutonomy")
	list = al.getSearchResultList(strSearch, a_csn, "NTB", "13", "x.asp", "2", "100")

	if list <> "" then
		SearchAutonomy = "<tr><td>" & list & "</td></tr></form></table>"
	else
		SearchAutonomy = "<br><br><div class='news11'>Dessverre ingen treff." _
						& "</td></tr></form></table>"
	end if

End Function
%>

</body>
</html>