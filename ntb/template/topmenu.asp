<%
'***********************************************************************************
'* topmenu.asp
'*
'* This file is used for displaying links on the top of the page. Most of the links
'* are internal, but some are external. The navigator also includes a search field for
'* freetext searches.
'* Also the topmenu includes an iframe, displaying either ticker or phone numbers.
'* The topmenu is included in almost every mainpage to provide the top navigator.
'*
'* USES:
'*		/template/tickeriframe.asp - used for generating the ticker or the phone
'*										numbers and emails.
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Trond Orrestad, Andersen
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Solveig Skjermo, Andersen
'* UPDATED DATE: 2002.06.04
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************

'set refresh-rate to the site
Call Response.AddHeader ("Refresh", "3600")

'check whether session is still alive
'if Session("Username") = "" then
	'Response.Redirect "../AuthFiles/Login.asp"
'end if
%>

	<table border="0" width="760" bgcolor="#003084">
    <FORM name="topnavigator" Action="searchresults.asp" Method=Post>
    <tr height="14" valign="middle">
    <!--
      <td class="top_button" width="120" height="14">
        <p class="top_button"><span class="top_button"><A href="../mainpages/Personalization_choose_window.asp" class="nav">Tilpass mine nyheter</A></span></p>
      </td>
    -->
      <td class="top_button" height="14" width="70">
        <p class="top_button"><span class="top_button"><A href="../mainpages/wapsms.asp" class="nav">Varsling</span></p></a>
      </td>
      <!--
      <td class="top_button" height="14" width="60">
        <p class="top_button"><span class="top_button"><a href="http://www.ntb.no/feature" target="_blank" class="nav">Feature</a></span></p>
      </td>
      -->
      <td class="top_button" height="14" width="100">
        <p class="top_button"><span class="top_button"><A href="../mainpages/contactList.asp" class="nav">Kontakt NTB</span></p></a>
      </td>
      <td class="top_button" height="14" width="70">
        <p class="top_button"><span class="top_button"><A href="http://www.scanpix.no" class="nav" target="_blank">Scanpix</span></p></a>
      </td>
      <td class="top_button" height="14" width="60">
        <p class="top_button"><span class="top_button"><A href="../mainpages/spraak.asp" class="nav">Spr�k</span></p></a>
      </td>
      <td class="top_button" height="14" width="60">
        <p class="top_button"><span class="top_button"><A href="../mainpages/HelpSite.asp" class="nav">Hjelp</span></p></A>
      </td>
		<td height="14" width="200" class="top_button" valign=top>
			<input class=news name="simpleFreeTextSearch" value="S�k i siste ukes nyheter" style="HEIGHT: 16px; width: 155px;" maxlength="120" onfocus="javascript:this.value=''" >
			<a href="Javascript:document.forms.topnavigator.submit();" class="nav">S�k<a>
		</td>
		<!--
		<td class="top_button" width="30">
			<a href="Javascript:document.forms.topnavigator.submit();" class="nav">S�k<a>
		</td>
		-->
		<td class="top_button" width="90">
			<a href="../mainpages/search.asp" class="nav">Avansert s�k</A>
		</td>
		<td class="top_button" width="80">
			<a href="../AuthFiles/Logout.asp" class="nav">Logg ut</A> (<%=Session("LogonNum")%>)
		</td>
	</tr>
	<tr height="25">
     <td colspan="10" height="25">
        <IFRAME NAME="tFrame" WIDTH=100% HEIGHT="25" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" scrolling="no" SRC="../template/tickeriframe.asp"></IFRAME>
  </TD></TR>
  </form>
  </table>
