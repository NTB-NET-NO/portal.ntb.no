<%@ Language=VBScript %>

<%
Dim dateArticle, intAcf

dateArticle = Request.QueryString("adate")

if dateArticle = "" then
'came from soundlist, only check access to ordinary sound
	intAcf = Clng(Session("ACF")) AND 16384 'lyd
	
	if not (intAcf > 0) then
	'send to errorpage
		Response.Redirect "../error/noauth.asp"
	end if
	
else
'came from soundsearch
	'check whether this is an archive-sound or not
	Dim dateArticleArchive
						
	'The sound are in archive if it is older than 7 days
	dateArticleArchive = DateAdd("D", 7, dateArticle)	
			
	if dateArticleArchive < Date then
	'check whether the user have access to sound-archive
		intAcf = Clng(Session("ACF")) AND 1073741824 'lydarkiv
	
		if not (intAcf > 0) then
		'send to errorpage
			Response.Redirect "../error/noauth.asp"
		end if

	else
	'check whether the user have access to sound
		intAcf = Clng(Session("ACF")) AND 16384 'lyd
	
		if not (intAcf > 0) then
		'send to errorpage
			Response.Redirect "../error/noauth.asp"
		end if
	
	end if

end if

Dim strSoundCode1
Dim strSoundCode2
Dim strSoundLink1
Dim strSoundLink2
Dim strFilename
Dim strId

strFilename = Request.QueryString("fname") 
strId = Request.QueryString("id") 

strSoundLink1 = "&lt;a href=&quot;http://194.19.39.29/kunde/ntb/asx/" & strFilename & ".asx&quot;&gt;Windows Media lyd&lt;/a&gt;"
strSoundLink2 = "&lt;a href=&quot;http://194.19.39.29/kunde/ntb/mp3/" & strFilename & ".mp3&quot;&gt;MP3 lyd;/a&gt;"

strSoundCode = "&lt;script language=&quot;javascript&quot; src=&quot;http://194.19.39.29/kunde/ntb/flashsound.js&quot;&gt;&lt;/script&gt;<br><br>" & _
				"&lt;script&gt;var lyd%s = new FlashSound();&lt;/script&gt;<br>" & _
				"&lt;a href=&quot;javascript://&quot; onmouseover=&quot;lyd%s.TGotoAndPlay('/','start')&quot; onmouseout=&quot;lyd%s.TGotoAndPlay('/','stop')&quot;&gt;<br>" & _
				"&ltimg src=&quot;http://194.19.39.29/kunde/ntb/grafikk/ntb.gif&quot; border=&quot;0&quot;&gt;&lt;/a&gt;<br>" & _
				"&lt;script language=&quot;javascript&quot;&gt;lyd%s.embedSWF(&quot;http://194.19.39.29/kunde/ntb/flash/%f.swf&quot;);&lt;/script&gt; "

strSoundCode2 = Replace(strSoundCode, "%f", strFilename)
strSoundCode = Replace(strSoundCode2, "%s", strId)

'set the right stylesheet
Dim style
if Session("Browser") = "" then
	Response.Redirect "../Authfiles/Login.asp"
elseif Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if
%>

<HTML>
<HEAD>
<link href="<%=style%>" type="text/css" rel="stylesheet">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title>NTB - HTML-kode for lyd</title>
</HEAD>
<BODY class="helpFont">
<%'style="FONT-FAMILY: Myriad, Arial, Helvetica, Geneva, Swiss, SunSans-Regular; FONT-SIZE: x-small;"%>
<h3>HTML kode for NTB-lyd</h3>
<p><%=strFilename%></p>

<p class='hjelpetekst'>Merk og kopier relevante linker eller JavaScript-kode, og lim deretter inn som ren tekst (eller som HTML i Visual Studio). <br /> Frontpage-brukere m� lime inn via Notepad for � unng� feil format p� &lt; og &gt;</p>

<p><b>Link til Windows Media-lyd:</b></p>
<p>
<%
Response.Write strSoundLink1
%>
</p>

<p><b>Link til MP3-lyd:</b></p>
<p>
<%
Response.Write strSoundLink2
%>
</p>

<p><b>JavaScript for Flash "mouseover" lyd:</b></p>
<p>
<%
Response.Write strSoundCode
%>
</p>


</BODY>
</HTML>
