<!-- Call a function to draw the nav bar or put it together here -->
<%= RenderNavbar(MSCSSiteStyle.Body) %>
<%
' =============================================================================
' navbar.inc
' #include file to display navigation bar
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' RenderNavbar
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderNavbar(ByVal style)
    Dim urlLink, htmLinkText
    
	RenderNavbar = LookupCachedFragment("StaticSectionsCache", "NavBar")
	If IsNull(RenderNavbar) Then

		urlLink = GenerateURL(MSCSSitePages.Home, Array(), Array())
		htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_Home_HTMLTitle", sLanguage), MSCSSiteStyle.Body)
		RenderNavbar = "[ " & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & " | "
		    
		urlLink = GenerateURL(MSCSSitePages.Catalog, Array(), Array())
		htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_Catalog_HTMLTitle", sLanguage), MSCSSiteStyle.Body)
		RenderNavbar = RenderNavBar & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & " | "
		
		urlLink = GenerateURL(MSCSSitePages.Basket, Array(), Array())
		htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_Basket_HTMLTitle", sLanguage), MSCSSiteStyle.Body)
		RenderNavbar = RenderNavBar & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)

		RenderNavbar = RenderNavbar & " ]"
		
		RenderNavbar = Tag("CENTER", RenderNavbar)
		Call CacheFragment("StaticSectionsCache", "NavBar", RenderNavbar)
	End If
	
End Function
%>