<HTML>
<HEAD>
	<TITLE><%= sPageTitle %></TITLE>
</HEAD>
   
<BODY>
    <CENTER><!-- #INCLUDE FILE="banner.inc" --></CENTER>
    <%= RenderText(dictConfig.s_SiteName, MSCSSiteStyle.SiteName) %><BR>
    <!-- #INCLUDE FILE="navbar.inc" -->
    <HR>
    <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3">
        <TR VALIGN="TOP">
            <TD ALIGN="LEFT" BGCOLOR='#CCCCCC'>
                <!-- #INCLUDE FILE="menu.asp" -->
            </TD><td></td>
            <TD ALIGN="LEFT"><%= htmPageContent %></TD>

        </TR>
    </TABLE>
	<CENTER><%= htmDiscountBannerSlot %></CENTER>
    <HR>
    <!--#INCLUDE FILE="footer.inc" -->
</BODY>
</HTML>
