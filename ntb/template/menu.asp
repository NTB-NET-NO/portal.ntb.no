<%
' =============================================================================
' menu.asp
' #include file to display left-hand menu bar
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Dim mscsUser
Dim CatalogSetID

Call PrepareRenderMenu(mscsUser, CatalogSetID)
Response.Write htmRenderMenu(mscsUser, CatalogSetID)


' -----------------------------------------------------------------------------
' PrepareRenderMenu
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareRenderMenu(ByRef mscsUser, ByRef CatalogSetID)
	Set mscsUser = GetCurrentUserProfile()
	CatalogSetID = mscsUserCatalogsetID()
End Sub


' -----------------------------------------------------------------------------
' htmRenderMenu
'
' Description:
'	Returns HTML but contains calls to business logic
'	functions as necessary.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderMenu(ByVal mscsUser, ByVal CatalogSetID)
	Dim htmRow
	Dim bShowPartnerServiceSection
	Dim htmCatalogSection, htmSearchSection, htmPartnerServiceSection, htmCustomerServiceSection
	Dim rsCatalogs, iCatalogCount, oCatalog, sCatalogName,  rsCategories
	Dim sAuthUser, bFormLoginSupported, sLoginName
	Dim sSearchValue
		
	' **************
	' Search Section
	' **************
	' Only cache this fragment if we're not on the search page
	If sThisPage <> MSCSSitePages.Search Then
		htmSearchSection = LookupCachedFragment("StaticSectionsCache", "SearchSection")
		If IsNull(htmSearchSection) Then
			htmSearchSection = htmRenderSearchSection(MSCSSiteStyle.Body, MSCSSiteStyle.Body, "")
			Call CacheFragment("StaticSectionsCache", "SearchSection", htmSearchSection)
		End If
	Else
		sSearchValue = GetRequestString("keyword", "")
		htmSearchSection = htmRenderSearchSection(MSCSSiteStyle.Body, MSCSSiteStyle.Body, sSearchValue)
	End If	
	 
	htmRenderMenu = htmRenderMenu & htmSearchSection
	
	' **************
	' Login Section
	' **************
	' Note: htmRenderLoginSection caches portions of itself as it can
	Call PrepareLoginSection(sAuthUser, bFormLoginSupported, mscsUser, sLoginName)	
	htmRenderMenu = htmRenderMenu & htmRenderLoginSection(sAuthUser, bFormLoginSupported, mscsUser, sLoginName)
	
	' **************
	' Catalog section
	' **************
	htmCatalogSection = LookupCachedFragment("StaticSectionsCache", CatalogSetID)
	If IsNull(htmCatalogSection) Then
		Call PrepareCatalogSection(rsCatalogs, iCatalogCount, oCatalog, sCatalogName,  rsCategories)
		htmCatalogSection = htmRenderCatalogSection(MSCSSiteStyle.Body, rsCatalogs, iCatalogCount, oCatalog, sCatalogName,  rsCategories)
		Call CacheFragment("StaticSectionsCache", CatalogSetID, htmCatalogSection)
	End If
	htmRenderMenu = htmRenderMenu & htmCatalogSection
	
    ' ************************
    ' Partner service section
    ' ************************
    ' shown to user when all following condition are present:
    ' 1) site supports delegated admin
    ' 2) user has a profile
    ' 3) user is authorized to act as administrator for other users
    ' 4) user has been assigned an organization
	bShowPartnerServiceSection = False
	If dictConfig.i_DelegatedAdminOptions = DELEGATED_ADMIN_SUPPORTED Then
		If Not mscsUser Is Nothing Then
		    If mscsUser.Fields(GetQualifiedName(BUSINESS_DESK_GROUP, FIELD_USER_PARTNERDESK_ROLE)).Value = ROLE_ADMIN Then
				If (Not mscsUser.Fields.Item(USER_ORGID).Value = "") Then
					bShowPartnerServiceSection = True
				End If
		    End If
		End If
    End If
    
    If bShowPartnerServiceSection Then
		htmPartnerServiceSection = LookupCachedFragment("StaticSectionsCache", "PartnerServiceSection")
		If IsNull(htmPartnerServiceSection) Then
			htmPartnerServiceSection = htmRenderPartnerServiceSection(MSCSSiteStyle.Body)
			Call CacheFragment("StaticSectionsCache", "PartnerServiceSection", htmPartnerServiceSection)
		End If
		htmRenderMenu = htmRenderMenu & htmPartnerServiceSection
	End If

    ' *************************
    ' Customer service section
    ' *************************
    ' shown to every user with a profile
	If Not mscsUser Is Nothing Then
		htmCustomerServiceSection = LookupCachedFragment("StaticSectionsCache", "CustomerServiceSection")
		If IsNull(htmCustomerServiceSection) Then
			htmCustomerServiceSection = htmRenderCustomerServiceSection(MSCSSiteStyle.Body)
			Call CacheFragment("StaticSectionsCache", "CustomerServiceSection", htmCustomerServiceSection)
		End If
		htmRenderMenu = htmRenderMenu & htmCustomerServiceSection
	End If

	htmRow = "<TR><TD NOWRAP>" & htmRenderMenu & "</TD></TR>"
	htmRenderMenu = RenderTable(htmRow, MSCSSiteStyle.MenuTable)

End Function


' -----------------------------------------------------------------------------
' PrepareCatalogSection
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareCatalogSection(ByRef rsCatalogs, ByRef iCatalogCount, ByRef oCatalog, ByRef sCatalogName, ByRef rsCategories)
	Set rsCatalogs = mscsUserCatalogs()
	iCatalogCount = GetRecordCount(rsCatalogs)
	If (iCatalogCount = 1) Then
		sCatalogName = rsCatalogs.Fields(CATALOG_NAME_PROPERTY_NAME).Value 
		Set oCatalog = MSCSCatalogManager.GetCatalog(sCatalogName)
		Set rsCategories = mscsGetSubCategoriesList(oCatalog, Nothing)
	End If
End Sub


' -----------------------------------------------------------------------------
' PrepareLoginSection
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub PrepareLoginSection(ByRef sAuthUser, ByRef bFormLoginSupported, ByRef mscsUser, ByRef sLoginName)
	If m_UserAccessType = IIS_AUTH Then
		sAuthUser = Request.ServerVariables("LOGON_USER")'changed AUTH_USER to LOGON_USER
		sAuthUser = Mid(sAuthUser, InStr(1, sAuthUser, "\", vbBinaryCompare) + 1)
	End If
	bFormLoginSupported = (dictConfig.i_FormLoginOptions <> FORM_LOGIN_NOT_SUPPORTED)	
	If bFormLoginSupported Then
		If m_UserType = AUTH_USER Then
			Set mscsUser = EnsureUserProfile()
			sLoginName = mscsUser.Fields(GetQualifiedName(GENERAL_INFO_GROUP, FIELD_USER_LOGIN_NAME)).Value
		End If
	End If
End Sub


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' htmRenderCatalogSection
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCatalogSection(ByVal style, ByVal rsCatalogs, ByVal iCatalogCount, ByVal oCatalog, ByVal sCatalogName, ByVal rsCategories)
	Dim htmSection
	
	If (iCatalogCount > 1) Or (iCatalogCount = 0) Then
		htmRenderCatalogSection = RenderText(MSCSMessageManager.GetMessage("L_MENU_CATALOG_SECTION_HTMLTitle", sLanguage), MSCSSiteStyle.Body) & BR
	ElseIf iCatalogCount = 1 Then
		htmRenderCatalogSection = RenderText(MSCSMessageManager.GetMessage("L_MENU_CATEGORIES_SECTION_HTMLTitle", sLanguage), MSCSSiteStyle.Body) & BR
	End If
		
	' Render the catalog section, depending on if 0, 1, or >1 catalogs installed for user/org
	If iCatalogCount > 1 Then
		htmRenderCatalogSection = htmRenderCatalogSection & htmRenderCatalogList(rsCatalogs, style) & CRLF
	ElseIf iCatalogCount = 1 Then
		' Display the root categories/products of the catalog
		htmRenderCatalogSection = htmRenderCatalogSection & htmRenderCategoriesList(oCatalog, rsCategories, style) & CRLF
	Else
        htmRenderCatalogSection = htmRenderCatalogSection & RenderText(MSCSMessageManager.GetMessage("L_Empty_Catalog_Root_HTMLText", sLanguage), style) & CRLF
	End If
End Function	


' -----------------------------------------------------------------------------
' htmRenderCustomerServiceSection
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderCustomerServiceSection(ByVal style)
	Dim urlLink, htmLinkText
	
	htmRenderCustomerServiceSection = RenderText(MSCSMessageManager.GetMessage("L_CustomerService_Section_Head_HTMLText", sLanguage), MSCSSiteStyle.Body) & BR
		
	urlLink = GenerateURL(MSCSSitePages.EditUserAccount, Array(), Array())
	htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_MENU_MANAGEMENT_USER_TEXT", sLanguage), style)
	htmRenderCustomerServiceSection = htmRenderCustomerServiceSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
	
	urlLink = GenerateURL(MSCSSitePages.ListUserOrders, Array(), Array())
	htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_MENU_MANAGEMENT_ORDERS_TEXT", sLanguage), style)
	htmRenderCustomerServiceSection = htmRenderCustomerServiceSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & CRLF
End Function	


' -----------------------------------------------------------------------------
' htmRenderPartnerServiceSection
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderPartnerServiceSection(ByVal style)
	Dim urlLink, htmLinkText

	htmRenderPartnerServiceSection = RenderText(MSCSMessageManager.GetMessage("L_PartnerService_Section_Head_HTMLText", sLanguage), MSCSSiteStyle.Body) & BR
	
	urlLink = GenerateURL(MSCSSitePages.EditPartnerAccount, Array(), Array())
	htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_MENU_MANAGEMENT_ORGANIZATION_TEXT", sLanguage), style)
	htmRenderPartnerServiceSection = htmRenderPartnerServiceSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR	

	urlLink = GenerateURL(MSCSSitePages.ListPartnerUsers, Array(), Array())
	htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_MENU_MANAGEMENT_USERS_TEXT", sLanguage), style)
	htmRenderPartnerServiceSection = htmRenderPartnerServiceSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
	    
	urlLink = GenerateURL(MSCSSitePages.ListPartnerOrders, Array(), Array())
	htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_MENU_MANAGEMENT_ORDERS_TEXT", sLanguage), style)
	htmRenderPartnerServiceSection = htmRenderPartnerServiceSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & CRLF
End Function


' -----------------------------------------------------------------------------
' htmRenderLoginSection
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderLoginSection(ByVal sAuthUser, ByVal bFormLoginSupported, ByVal mscsUser, ByVal sLoginName)
	Dim urlLink, htmCachedSection
	Dim htmLinkText
	
	If m_UserAccessType = IIS_AUTH Then
		htmRenderLoginSection = RenderText(MSCSMessageManager.GetMessage("L_Login_HTMLText", sLanguage), MSCSSiteStyle.Body) & BR
		htmRenderLoginSection = htmRenderLoginSection & RenderText(sAuthUser, MSCSSiteStyle.Body) & BR

		urlLink = GenerateURL(MSCSSitePages.LogOff, Array(), Array())
		htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_SignOff_HTMLTitle", sLanguage), MSCSSiteStyle.Body)
		htmRenderLoginSection = htmRenderLoginSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & CRLF		
	Else
		If bFormLoginSupported Then
			If m_UserType = AUTH_USER Then
				htmRenderLoginSection = _
					RenderText(MSCSMessageManager.GetMessage("L_Login_HTMLText", sLanguage), MSCSSiteStyle.Body) & BR _
						& RenderText(sLoginName, MSCSSiteStyle.Body) & BR

				htmCachedSection = LookupCachedFragment("StaticSectionsCache", "AuthdUserLinks")
				If IsNull(htmCachedSection) Then
					urlLink = GenerateURL(MSCSSitePages.Login, Array(), Array())
					htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_ChangeSignIn_HTMLText", sLanguage), MSCSSiteStyle.Body)
					htmCachedSection = RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
				
					urlLink = GenerateURL(MSCSSitePages.LogOff, Array(), Array())
					htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_SignOff_HTMLTitle", sLanguage), MSCSSiteStyle.Body)
					htmCachedSection = htmCachedSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
			
					Call CacheFragment("StaticSectionsCache", "AuthdUserLinks", htmCachedSection)
				End If 
				htmRenderLoginSection = htmRenderLoginSection & htmCachedSection & CRLF
			Else
				htmCachedSection = LookupCachedFragment("StaticSectionsCache", "NotAuthdUserLinks")
				If IsNull(htmCachedSection) Then
					htmCachedSection = RenderText(MSCSMessageManager.GetMessage("L_Login_HTMLText", sLanguage), MSCSSiteStyle.Body) & BR
					
					urlLink = GenerateURL(MSCSSitePages.Login, Array(), Array())
					htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_SignIn_HTMLTitle", sLanguage), MSCSSiteStyle.Body)
					htmCachedSection = htmCachedSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
			
					If dictConfig.i_SiteRegistrationOptions = REGISTRATION_SUPPORTED Then
						urlLink = GenerateURL(MSCSSitePages.NewUser, Array(), Array())
						htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_NewUser_HTMLText", sLanguage), MSCSSiteStyle.Body)
						htmCachedSection = htmCachedSection & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link) & BR
					End If
					Call CacheFragment("StaticSectionsCache", "NotAuthdUserLinks", htmCachedSection)
				End If
				htmRenderLoginSection = htmCachedSection & CRLF
			End If
		End If
	End If
End Function
		

' -----------------------------------------------------------------------------
' htmRenderSearchSection
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderSearchSection(ByVal dictSectionTitleStyle, ByVal dictSectionBodyStyle, sSearchValue)	
	Dim htmBody, urlAction, urlLink, htmLinkText, sBtnText
	
	htmBody = RenderText(mscsMessageManager.GetMessage("L_Search_Section_Head_HTMLText", sLanguage), dictSectionTitleStyle) & BR
	
	htmBody = htmBody & RenderTextBox("keyword", sSearchValue, 15, 60, MSCSSiteStyle.TextBox) & BR
	
	sBtnText = MSCSMessageManager.GetMessage("L_Search_Button", sLanguage)
	htmBody = htmBody & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)
	
	urlLink = GenerateURL(MSCSSitePages.SearchableDepartments, Array(), Array())
	htmLinkText = RenderText(MSCSMessageManager.GetMessage("L_Advanced_Search_Link_HTMLText", sLanguage), dictSectionBodyStyle)
	htmBody = htmBody & NBSP & RenderLink(urlLink, htmLinkText, MSCSSiteStyle.Link)
	
	urlAction = GenerateURL(MSCSSitePages.Search, Array(), Array())
	htmRenderSearchSection = RenderForm(urlAction, htmBody, HTTP_POST)
End Function
%>
