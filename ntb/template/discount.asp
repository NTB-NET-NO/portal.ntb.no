<%
' =============================================================================
' Discount.asp
' Functions for calculating and rendering discounts
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

' -----------------------------------------------------------------------------
' GetBasketDetails
'
' Description:
'	This function will get basket details as required by the BasketDetails
'	param of RenderDiscounts.  Note, the basket will be loaded, if the basket
'	is already loaded, use CollectBasketItems
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetBasketDetails()
	Dim oOrderGrp
	Dim iErrorLevel
			
	Set oOrderGrp = GetOrderGroup(m_UserID)
	Call oOrderGrp.LoadBasket()
				
	iErrorLevel = RunMtsPipeline(MSCSPipelines.Product, _
	    GetPipelineLogFile("product"), oOrderGrp)

	Set GetBasketDetails = CollectBasketItems(oOrderGrp)
	Set oOrderGrp = Nothing
End Function


' -----------------------------------------------------------------------------
' CollectBasketItems
'
' Description:
'	This function will get basket details as required by the BasketDetails
'	param of RenderDiscounts, given an in-memory ordergroup
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function CollectBasketItems(oOrderGrp)
	Dim sOrderFormName, Items, Item
	Set CollectBasketItems = Server.CreateObject("Commerce.SimpleList")
	For Each sOrderFormName In oOrderGrp.Value(ORDERFORMS)
		Set Items = oOrderGrp.Value(ORDERFORMS).Value(sOrderFormName).Items
		For Each Item In Items
			CollectBasketItems.Add Item
		Next
	Next
End Function


' -----------------------------------------------------------------------------
' GetShownProductsDetails
'
' Description:
'	This function gets called to fetch the products shown on the page (if any)
'	Discounts display can give higher weight to products which are shown on the page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetShownProductsDetails()
    Dim sCategoryName, sCatalogName, nPageNumber, sProductID

	' If the set is not empty, return its contents; otherwise, return an empty
	' simple list.
    If IsEntityInSet(sThisPage, MSCSPageSets.ProductPageSet) Then
		GetInputParameters sCatalogName, sCategoryName, nPageNumber, sProductID
		Set GetShownProductsDetails = oCachedDiscountProductInfo(sCatalogName, sCategoryName, nPageNumber, sProductID)
	Else
		Set GetShownProductsDetails = GetSimpleList()
	End If
End Function


' -----------------------------------------------------------------------------
' oCachedDiscountProductInfo
'
' Description:
'	Helper function called by GetShownProductsDetails
'	Get cached discount product info as simple list; if it isn't cached,
'	generate it and cache it.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function oCachedDiscountProductInfo(sCatalogName, sCategoryName, nPageNumber, sProductID)
	Dim sCacheKey, oCacheValue
	
	sCacheKey = sThisPage & ":" & sCatalogName & ":" & sCategoryName & ":" & sProductID & ":" & nPageNumber
	Set oCacheValue = LookupCachedObject("DiscountProductInfo", sCacheKey)
	If oCacheValue Is Nothing Then
		Set oCacheValue = listGetShownProductsInfo(sCatalogName, sCategoryName, sProductID, nPageNumber)
		Call CacheObject("DiscountProductInfo", sCacheKey, oCacheValue)
	End If
	Set oCachedDiscountProductInfo = oCacheValue
End Function


' -----------------------------------------------------------------------------
' GetInputParameters
'
' Description:
'	Helper function called by GetShownProductsDetails
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Sub GetInputParameters (ByRef sCatalogName, ByRef sCategoryName, ByRef nPageNumber, ByRef sProductID)
	sCatalogName = GetRequestString(CATALOG_NAME_URL_KEY, Null)
    sCategoryName = GetRequestString(CATEGORY_NAME_URL_KEY, "")
    nPageNumber = MSCSAppFrameWork.RequestNumber(PAGENUMBER_URL_KEY, 1)
	sProductID = GetRequestString(PRODUCT_ID_URL_KEY, "")    		
End Sub


' -----------------------------------------------------------------------------
' listGetShownProductsInfo
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function listGetShownProductsInfo(ByVal sCatalogName, ByVal sCategoryName, _
			ByVal sProductID, ByVal nPageNumber)
	Dim mscsCatalog, mscsCategory, rsProducts, rsProperties, arrParentCategories
	Dim nOutRecordsTotal, nOutPagesTotal
	Dim dictCurrentProduct
	Dim listProducts
	Dim mscsProduct
	
	' Initialize
	Set listProducts = GetSimpleList()
	Set mscsCatalog = MSCSCatalogManager.GetCatalog(sCatalogName)
    Set mscsCategory = mscsGetCategoryObject(mscsCatalog, sCategoryName)
	If mscsCategory Is Nothing Then
		arrParentCategories = Array()
	Else
		arrParentCategories = GetAncestorCategories(mscsCategory, sCategoryName)
	End If
		
	If StrComp(MSCSSitePages.Category, sThisPage, vbTextCompare) = 0 Or _
	   StrComp(MSCSSitePages.Catalog, sThisPage, vbTextCompare) = 0 Then

		Set rsProducts = mscsGetProductList(mscsCatalog, mscsCategory, nPageNumber, nOutRecordsTotal, nOutPagesTotal)
					
		While Not rsProducts.EOF
			Set dictCurrentProduct = GetDictionary()			
			dictCurrentProduct.product_catalog = sCatalogName
			dictCurrentProduct.product_id = rsProducts.Fields(mscsCatalog.IdentifyingProductProperty).Value
			dictCurrentProduct.product_category = sCategoryName
			Call CopyProductToItem(rsProducts, dictCurrentProduct, arrParentCategories)
			listProducts.Add(dictCurrentProduct)
		    rsProducts.MoveNext
		Wend 
				
	ElseIf StrComp(MSCSSitePages.Product, sThisPage, vbTextCompare) = 0 Then
		Set mscsProduct = mscsCatalog.GetProduct(sProductID)			
		Set dictCurrentProduct = GetDictionary()
	   	Set rsProperties = mscsProduct.GetProductProperties
		dictCurrentProduct.product_catalog = sCatalogName
		dictCurrentProduct.product_id = rsProperties.Fields(mscsCatalog.IdentifyingProductProperty).Value
		dictCurrentProduct.product_category = sCategoryName
		Call CopyProductToItem(rsProperties, dictCurrentProduct, arrParentCategories)
		listProducts.Add(dictCurrentProduct)
		Set dictCurrentProduct = Nothing
	End If
    			
	Set listGetShownProductsInfo = listProducts
End Function


' -----------------------------------------------------------------------------
' CopyProductToItem
'
' Description:
'	This function copies product info into an item like QCI does
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function CopyProductToItem(rsProduct, item, arrParentCategories)
	Dim i
	For Each i In rsProduct.Fields
    	If TypeName(rsProduct.Fields(i.name).Value) <> "Byte()" Then
			item.value("_product_" & i.name) = rsProduct.Fields(i.name).Value
		End If
	Next
	item.value("_product_categories") = arrParentCategories
End Function


' -----------------------------------------------------------------------------
' GetAncestorCategories
'
' Description:
'	This function returns the ancestor categories for a category in an array
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function GetAncestorCategories(ByVal mscsCategory, ByVal sCategoryName)
	Dim rs, nCount, Arr
    
	Set rs = mscsCategory.AncestorCategories
	ReDim Arr(rs.RecordCount)
    nCount = 0
    Arr(0) = sCategoryName

    While Not rs.EOF
        nCount = nCount + 1
        Arr(nCount) = rs.Fields(0).Value
        rs.MoveNext
    Wend
    
    GetAncestorCategories = Arr
End Function


' -----------------------------------------------------------------------------
' Rendering functions
' -----------------------------------------------------------------------------

' -----------------------------------------------------------------------------
' RenderDiscounts
'
' Description:
'	This functions renders discount banners.  
'	If ProductDetails and BasketDetails are supplied as not Nothing, GetContent
'	will use these simplelists to give affinity to discount which are completed
'	by items in the basket, or to items which are shown on the page
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
' $$ change to htmRenderDiscounts
Function RenderDiscounts(ProductDetails, BasketDetails, nDiscountsToShow)
	Dim oCSO, Discounts, Discount
	Dim mscsUserProfile
	
	Set oCSO = Server.CreateObject("Commerce.ContentSelector")
	
	' Set the basket and product affinity where supplied
	If Not IsNull(ProductDetails) Then Set oCSO.Products = ProductDetails
	If Not IsNull(BasketDetails) Then Set oCSO.Items = BasketDetails
	
	oCSO.Border = 1
	oCSO.TargetFrame = "_top"
	oCSO.NumRequested = nDiscountsToShow
	oCSO.Trace = False
	
	Set mscsUserProfile = GetCurrentUserProfile()
	If Not mscsUserProfile Is Nothing Then
		Set oCSO.UserProfile = mscsUserProfile
	End If

	Set Discounts = oCSO.GetContent(Application("CSFDiscountContext"))
	For Each Discount In Discounts
		RenderDiscounts = RenderDiscounts & Discount
	Next
End Function
%>

