<%@ Language=VBScript%><%
'***********************************************************************************
' x.asp
'
' This file is used for displaying all main section pages. It consists of different
' iFrames listing different news items. Based on parameters in the calling URL,
' different news lists are displayed.
'
' USES:
'		NTB_layout_lib.asp - For rendering of iframes and titlebar.
'		NTB_cache_lib.asp - For cache-functions.
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen
' CREATED DATE: 2002.04.09
' REVISION HISTORY:
' Bug fix: 2002.08.09: Roar Vestre, NTB: Check Session("UserName") for redirect to CloseWindow.asp
' 2002.08.09: Roar Vestre, NTB: Changed to new stylesheet
' 2005.02.24: Roar Vestre, Inserted "Highlite of searchWord" component
' 2007.05.14: Richard Husevaag, ABEO: Support for images from new PRS-portal
' 2007.07.12: Richard Husevaag, ABEO/Trond Hus�, NTB: Support for videos inn portal 
' 2007.08.01: Trond Hus�, NTB: Lagt tilbildelink i dokumentet. 
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_layout_lib.asp" -->
<!-- #INCLUDE FILE="../include/NTB_cache_lib.asp" -->
<%
if Session("UserName") = "" then
	'Response.Redirect "../Authfiles/CloseWindow.asp"
end if

Dim style
if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
	'style = "../include/ntb_view.css"
end if
%><html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
<link href="<%=style%>" type="text/css" rel="stylesheet">
<%=TitleTag()%>
<!--Lines included for enabling overlib, don't move down. Generates problems for NN-->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="../include/overlib.js"></script>

<script language="JavaScript">
<!--
//this Javascript is added because the same component generates search results
//for personalized page and for related news item, and for the related news items,
//the news should always be opened in the same window.
var myWindow;
myWindow = this
function w(aid)
{
	var myURL;
	myURL = '../template/x.asp?a=' + aid;
	myWindow.location = myURL;
	myWindow.focus();
}
// Function to simulate the "document.all" property on beowser that does not support it

if (!document.all)
{
	Node.prototype.__defineGetter__("all", function()
	{
		if (document.getElementsByTagName("*").length)
		{
			switch (this.nodeType)
			{
				case 9:
					return document.getElementsByTagName("*")
					break
				case 1:
					return this.getElementsByTagName("*")
					break
			}
		}
		return ""
	})
	Node.prototype.__defineSetter__("all", function() {})
}



// removing elements that is not to be printed

function removeElements(aid)
{
	// check to see if the method "window.print" is supported
	if (window.print)
	{
		document.body.className = 'printBody'
		if ( document.getElementById('vline') ) { document.getElementById('vline').className = 'printDivider'; }
		document.all.remove.style.display='none'
		document.all.removebutton.style.display='none'
		if ( document.getElementById('removepict') ) { document.all.removepict.style.display='none'; }
		window.print()
		setTimeout("displayElements();",15000)
	}
	else
	{
		var nav = navigator.userAgent.toLowerCase();
		// "window.print" is not supported. Treat opera different fom mac. (IE  works..)
		if (nav.indexOf('opera') >= 0)
		{
			window.alert("Beklager, du bruker Opera p� Macintosh og den st�tter ikke v�r utskriftsfunksjon.");
		}
		else
		{
			window.alert("Beklager, din nettleser st�tter ikke v�r utskriftsfunksjon. �pner derfor et nytt vindu hvor du kan bruke nettleserens egen funksjonalitet");
			myURL = '../template/x.asp?a=' + aid;
			leftPlacement = screen.width - 410; //ensures that the window pops up to the right independent on screen resolution
 			winOptions = 'titlebar=1,toolbar=1,location=0,menubar=0,scrollbars=1,resizable=1,width=600,height=600,top=80,left='+leftPlacement;
 			window.open(myURL, 'Utskriftsvindu', winOptions);
 		}
	}
}

// function to be called after printing, displaying the hidden elements
function displayElements()
{
	document.body.className = 'viewBody'
	if ( document.getElementById('vline') ) { document.getElementById('vline').className= 'divider'; }
	document.all.remove.style.display='';
	document.all.removebutton.style.display='';
	if ( document.getElementById('removepict') ) { document.all.removepict.style.display=''; }
}
//-->
</script>
</head>
<body class='viewBody'>

<%
Dim aid, aText, strScanpixLink
aid = Request("aid")
if aid = "" then aid = Request("a")
%>

<table border="0" width="100%" height="98%" cellspacing="0" cellpadding="3">
	<tr>
		<td rowspan="2" width="73%" valign="top">
			<table border="0" width="100%" height="100%">
			<tr>
				<td id="remove" class="news"><a href="javascript:history.back()">Tilbake</a></td>
				<td align="right" class="viewDate">ID: <%=aid%></td>
			</tr>
			<tr>
				<td valign="top" colspan="3">
					<%
					aText = getArticleText(aid)

					if aText <> "NOT" then
					'if there are a list, then display it

						' New code for Higliting search words
						' 2005.02.25, Roar Vestre
						Dim strSearchWord
						strSearchWord = Session("searchWord")
						If Request("page") = "searchresult" And strSearchWord <> "" Then
							'Response.Write("SearchWord: " & strSearchWord & "</br>") 'Debug
							Dim objUthev
							If Session("objUthevInited") = False then
								Set objUthev = Server.CreateObject("comUthevSok.UthevSok")
								Set Session("objUthev") = objUthev
								Session("objUthevInited") = -1
								'Response.Write("Debug: init object") 'Debug
							Else
								Set objUthev = Session("objUthev")
								'Response.Write("Debug: object from session") 'Debug
							End If
							aText = objUthev.DoUthevSok(aText, strSearchWord)
						End If

						Response.Write(aText)
						'Response.Write("<div class='viewDate'>ID: " & aid & "</div>")

					else
					'if there are no list show errormessage
						Response.Redirect "../error/noauth.asp?"
					end if
					%>
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="3">
				<hr size=1 noshade>
				<div class="viewHl2">Alle versjoner av denne saken</div>

				<p>
				<%= getArticleVersions(aid) %>
				</p>
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="3">&nbsp;</td>
			</tr>
			<tr id="removebutton" name="removebutton">
				<FORM METHOD="POST" ACTION="../mainpages/getarticle.asp" id=form1 name=form1>
				<INPUT type="hidden" name="aid" value="<%=CStr(aid)%>">
				<td width="33%" align="right">
					<INPUT class="formbutton" type="button" value="Skriv ut" onClick="JavaScript:removeElements(<%=aid%>);" name="print" style="background:#bfccd9 none; color:#003366; width:100px">
				</td>
				<td width="33%" align="center">
					<INPUT class="formbutton" type="submit" value="Last ned XML" name="download" style="background:#bfccd9 none; color:#003366; width:100px">
				</td>
				<td width="33%" align="left">
					<INPUT class="formbutton" type="submit" value="Last ned XTG" name="download" style="background:#bfccd9 none; color:#003366; width:100px">
				</td>
				</FORM>
			</tr>
			</table>
		</td>
		<%'get the pictures to this news item
			dim pictNodes, node, arePict, nodeClass, strPath
			dim strPict, strCaption, strHref, strDivider, strImg

			set pictNodes = getArticlePict(aid)

			'Response.Write "<td valign='top' class='divider'><table>"
			'Response.Write "<tr><td valign='top' align='center'>" & vbCrLf

			arePict = 1

			for each node in pictNodes
				if arePict = 1 then
					Response.Write "<td id='vline' valign='top' class='divider'><table>"
					Response.Write "<tr><td valign='top' align='center'>" & vbCrLf
					arePict = 2
				end if

				if not node.SelectSingleNode("media-caption") is nothing then
					strCaption = node.SelectSingleNode("media-caption").Text
				end if
				
				if not node.SelectSingleNode("media-reference/@source") is nothing then
					strPict = node.SelectSingleNode("media-reference/@source").Text
				end if
				
				
				
				nodeClass = node.getAttribute("class")
				if nodeClass = "prm" then
					' For NTB+ PRM-tjeneste
					strPath = node.SelectSingleNode("media-reference/@alternate-text").Text
					' Link til Stort bilde:
					strHref = "http://193.75.33.34/prm_vedlegg/vedlegg/" & strPath & "/" & strPict
					' Thumbnail:
					strImg = "http://193.75.33.34/prm_vedlegg/thumbnail/" & strPath & "/"  & strPict

				elseif nodeClass = "prs" then
					' For PRS-tjeneste
					strPath = node.SelectSingleNode("media-reference/@alternate-text").Text
					' Link til Stort bilde:
					strHref = strPath & "download=true&uri=" & strPict
					' Thumbnail:
					strImg = strPath & "thumbnail=70x70&uri=" & strPict

				elseif nodeClass = "kultur" then
					strImg = "http://80.91.34.200/cgi-bin/picture?/4/UNKNOWN/Q_" & strPict & ".jpg"
					strHref = "http://www.ntb.no/picture.aspx?Type=H&SpCode=" & strPict

				else
					' For Scanpix-bilder
					strImg = "http://80.91.34.200/cgi-bin/picture?/4/UNKNOWN/Q_" & strPict & ".jpg"
					strHref = "http://80.91.34.200/seno/seno.cgi?/0/UNKNOWN/Q_" & strPict
				end if

				strCaption = Replace(strCaption, Chr(10), "<br>")
				strCaption = Replace(strCaption, """", "&quot;")
				strCaption = Replace(strCaption, "'", "&#180;")
				strCaption = Replace(strCaption, "'", "&#180;")
				strCaption = strCaption & " Ref.: " & strPict
				strCaption = strCaption & "<br>(Klikk her for � se bildet hos Scanpix)"

				Response.Write "<div class='left_button'><a href=""" & strHref & """ target='scanpix' "
				Response.Write "onmouseover=""return overlib('" & strCaption & "', LEFT, CAPTION, 'Bildetekst') ;"" onmouseout=""return nd();"">" & vbCrLf

				Response.Write "<img src=""" & strImg & """ border='0' vspace='5' hspace='2' xwidth='100' />"

				Response.Write "</a></div>" & vbCrLf
			next

			strCaption = "Klikk her for � s�ke etter andre relevante bilder i Scanpix bildedatabase"

			strScanpixLink = "<tr><td valign='top' align='left' id='removepict'>"
			strScanpixLink = strScanpixLink & "<div class='news'><a target='scanpix' href='http://80.91.34.200/seno/seno.cgi?/7/PICLIB.HTML' "
			strScanpixLink = strScanpixLink & "onmouseover=""return overlib('" & strCaption & "', LEFT, CAPTION, 'Scanpix') ;"" onmouseout=""return nd();"">"
			strScanpixLink = strScanpixLink & "Bildes�k hos Scanpix</a></div>"
			strScanpixLink = strScanpixLink & "</td></tr>" & vbCrLf

			if arePict = 2 then
				Response.Write "</td></tr>"
				Response.Write strScanpixLink
			end if

			' Videolink
			' <meta name="Multimediefil" content="http://80.91.34.200/seno/video_mov/scanpixntb__klt-2007-07-12-1.mov" />
			' <meta name="NTBTjeneste" content="Nyhetstjenesten" />
			' http://80.91.34.200/seno/video_mov/hro-2007-07-31-2_01.jpg
			dim xmlVideoDoc, vxml, vxsl
			
			xmlVideoDoc = getArticleXML(aid)
			if not (xmlVideoDoc = "E_A_NOT_FOUND" OR xmlVideoDoc = "E_A_NO_ACCESS") then
				Set vxml= Server.CreateObject("MSXML2.FreeThreadedDOMDocument")
				
				vxml.async = false
				vxml.loadxml(xmlvideodoc)

				dim xmlNode
				dim videolink
				dim bildelink
				set xmlNode = vxml.SelectSingleNode("/nitf/head/meta[@name='Multimediefil']/@content")
				
				' Flytter informasjonen til en streng, slik at vi kan fikse og ordne litt med denne
				if not xmlNode is nothing then
					videolink = xmlNode.nodevalue
					bildelink = xmlNode.nodevalue
					bildelink = replace(bildelink, "scanpixntb__", "")
					bildelink = replace(bildelink, ".mov",".jpg")

					
					response.Write("<tr><td valign = 'top' align = 'left' id = 'video'><div class = 'news'><a target='scanpix' href = '"& videolink & "'><img border = 0 width='128' height='88' src = '" & bildelink & "'><br>Video hos ScanpixNTB</a></div></td></tr>" & vbcrlf)
				end if
				
			end if 
			'response.Write (vxml.value)
			
			' next
			
			
			
			
			'get the related list to this news with autonomy COM-object
			Dim al, heading, list
			Dim a_csn, a_db, a_min

			a_csn = application("autonomy_cns")
			a_db = Application("autonomy_database")
			a_min = Application("autonomy_minimum")

			'Response.Write("Doc:" & aid & " a_csn:" & a_csn)
			Set al = Server.CreateObject("AutonomyInterface.comAutonomy")
			list = al.getRelatedList(aid, a_csn, _
				a_db, a_min, "../template/x.asp")

			if list <> "" then
				if arePict = 1 then
					Response.Write "<td id='vline' valign='top' class='divider'><table>"
					Response.Write Replace(strScanpixLink, " andre ", " ")
				end if

				Response.Write "<tr><td valign='top'>"
				heading = "<div class=""main_heading2"">Relaterte nyheter</div>"
				Response.Write heading
				Response.Write list
				Response.Write "</td></tr></table></td>"
			else
				'heading = "<div class=""main_heading2"">Ingen relaterte nyheter</div>"
				'Response.Write heading
				if arePict = 2 then
					Response.Write "</table></td>"
				end if
			end if
		%>
	</tr>
</table>
</body>
</html>
