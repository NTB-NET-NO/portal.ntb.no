<% 
' =============================================================================
' banner.inc
' #include file to display advertising banner(s)
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Dim oCSO, Ads, Ad, oUserProfile
If IsEntityInSet(sThisPage, MSCSPageSets.AdvertisementPageSet) Then

	Set oCSO = Server.CreateObject("Commerce.ContentSelector")
	'oCSO.Size =  "Banner"
	oCSO.Border = 1
	oCSO.TargetFrame = "_top"
	oCSO.NumRequested = 1

	Set oUserProfile = GetCurrentUserProfile()
	If Not oUserProfile Is Nothing Then
		Set oCSO.UserProfile = oUserProfile
	End If
	Set Ads = oCSO.GetContent( Application("CSFAdvertisingContext") )
		
	For Each Ad In Ads
		Response.Write(Ad)
	Next
End If
%>

