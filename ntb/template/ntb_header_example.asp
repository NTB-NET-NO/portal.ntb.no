FILE HEADER:
<%
'***********************************************************************************
' name of file.asp
'
' Description of the file. What it does, the purpose of it, special or 
' important info etc
'
' USES:
'		name of file that it use.asp - Description of why it use it
'		ex: NTB_layout_lib.asp - For rendering of iframes and titlebar.
'
' NOTES:
'		if there are any special comments, else write 'none'
'
' CREATED BY: Trond Orrestad, Andersen 
' CREATED DATE: 2002.04.09
' UPDATED BY: Trond Orrestad, Andersen (only the last update, not for each update. to be used only after the first portal launch)
' UPDATED DATE: 2002.04.09
' REVISION HISTORY:
'		for changes after the first launch, for now write 'none'
'
'***********************************************************************************


EXAMPLE:
'***********************************************************************************
' Searchresults.asp
'
' This file is used for displaying search results. Both freetext search results and 
' category search results will be displayed. Results from the sound search will 
' also be displayed on this page.
' The different search queries are formatted and sent to a search component. This 
' component returns a RecordSet of search results. The first 20 hits are displayed
' on the page and the RecordSet is saved in a session variable to make it easy to
' display the next pages with hits.
'
' USES:
'		NTB_layout_lib.asp - For rendering of titlebar.
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen 
' CREATED DATE: 2002.04.09
' UPDATED BY: 
' UPDATED DATE: 
' REVISION HISTORY:
'		none
'
'
'***********************************************************************************


FUNCTION HEADER:
' -----------------------------------------------------------------------------
' Name of fuction
'
' Description:	Description of the function
'
' Parameters:	List of parameters with an explanation
'
' Returns:		List of what the function return with an explanation	
'				
'
' Notes :		if there are any special notes
'				
' -----------------------------------------------------------------------------


EXAMPLE:
' -----------------------------------------------------------------------------
' RenderTitleBar
'
' Description:	This renderfunction generates a title area displaying the page heading.
'				The title area can be displayed with or without a picture filter link.
'
' Parameters:	Title; name of the heading
'				PhotolinkOption; A parameter that is either PhotoLink or NoPhotoLink, and that
'				indicates if a photo filter link should be displayed.
'
' Returns:		HTML-code for drawing of a table with header text and background color
'				
'
' Notes :		none
' -----------------------------------------------------------------------------
Function RenderTitleBar(ByVal title, ByVal PhotoLinkOption) 

	Dim PageID, tbstr
	PageID = "nopage"
	
	If Request("pageID")<>"" Then
		PageID = Request("pageID") 
	End If
	
	tbstr = "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""620"" height=""50"">" _
					& "<tr height=""8"" align=""right""><td colspan=2></td></tr>" _
					& "<tr height=""20"">" _
					& "<td class=""main_heading1"" width=""10"" height=""20"" align=""left"" bgcolor=""#003084"">" _
					& "&#160;</td>" _
					& "<td class=""main_heading1"" width=""633"" height=""20"" align=""left"" bgcolor=""#003084"">" _
					&	"<p class=""main_heading1""><font color=""#FFFFFF""><span class=""main_heading1"">" & title & "</span></font></p>" _
					& "</td></tr>"
	
	If (PhotoLinkOption = "PhotoLink") Then
		If Request("photoOption") <> "Yes" Then
			tbstr = tbstr & "<tr height=""10"" align=""right""><td colspan=2 class=""news""> Vis bare nyheter med <a href=""../mainpages/MainSectionPages.asp?pageID=" & PageID & "&photoOption=Yes""> bilde</a></td></tr></table>"
		Else
			tbstr = tbstr & "<tr height=""10"" align=""right""><td colspan=2 class=""news""> Vis <a href=""../mainpages/MainSectionPages.asp?pageID=" & PageID & """>alle</a> nyheter</td></tr></table>"
		End If	
	Else
		tbstr = tbstr & "<tr height=""10"" align=""right""><td colspan=2 class=""news"" height=""10""> </td></tr></table>"
	End If	
	
	RenderTitleBar = tbstr
End Function


' -----------------------------------------------------------------------------
' RenderIFrame
'
' Description:This renderfunction generates an iframe with specified content and size.
'
' Parameters: ListType: Type of news list to be displayed in the iframe. This type
'			  is used as the suffix for the corresponding iframe file
'			  SizeTypeHeight: Indicates the type of height to be used for the iframe. 
'			  Should be one of the following types:
'			  height1, height2
'			  SizeTypeWidth: Indicates the type of width to be used for the iframe. 
'			  Should be one of the following types:
'			  width2, width3
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function RenderIFrame(ByVal ListType, ByVal SizeTypeWidth, ByVal SizeTypeHeight, ByVal PhotoOption, ByVal TimeOption, ByVal FrameNumber, ByVal intSakNr)
	
	Dim header
	header = "<div class=""main_heading2"">Oversikt</div>"

	Select Case ListType
		Case "alle_innenriks"
			'stoffgruppe Innenriks
			header = "<div class=""main_heading2"">Innenriks</div>"
		Case "alle_sport"
			'stoffgruppe Sport
			header = "<div class=""main_heading2"">Sport</div>"
		Case "alle_utenriks"
			'stoffgruppe Utenriks
			header = "<div class=""main_heading2"">Utenriks</div>"
		Case "innenriks_alle"
			'stoffgruppe Innenriks
			header = "<div class=""main_heading2"">Alle innenriksnyheter</div>"
		Case "innenriks_politikk"
			'stoffgruppe Innenriks hovedkategori Politikk
			header = "<div class=""main_heading2"">Politikk</div>"
		Case "innenriks_okonomi_naeringsliv"
			'stoffgruppe Innenriks hovedkategori "�konomi og n�ringsliv" og "Arbeidsliv"
			header = "<div class=""main_heading2"">�konomi og n�ringsliv</div>"
		Case "innenriks_krim_ulykker"
			'stoffgruppe Innenriks hovedkategori "Kriminalitet og rettsvesen" og "Ulykker og naturkatastrofer"
			header = "<div class=""main_heading2"">Krim og ulykker</div>"
		Case "utenriks_alle"
			'stoffgruppe Utenriks, except undergruppe "Bakgrunn" og "Analyse" og "Faktaboks" og "Oversikter"
			header = "<div class=""main_heading2"">Nyheter</div>"
		Case "utenriks_bakgrunn"
			'stoffgruppe Utenriks undergruppe "Bakgrunn" og "Analyse" og "Faktaboks" og "Oversikter" og "Menyer, til red"
			header = "<div class=""main_heading2"">Bakgrunn - Analyse - Fakta - Oversikt</div>"
		Case "okonomi_innenriks"
			'stoffgruppe Innenriks, category "�konomi og n�ringsliv"
			header = "<div class=""main_heading2"">Innenriks</div>"
		Case "okonomi_utenriks"
			'stoffgruppe Utenriks, category "�konomi og n�ringsliv"
			header = "<div class=""main_heading2"">Utenriks</div>"
		Case "sport_alle"
			'stoffgruppe Sport, except undergruppe "Tabeller og resultater"
			header = "<div class=""main_heading2"">Nyheter</div>"
		Case "sport_tabeller"
			'stoffgruppe Sport, with undergruppe "Tabeller og resultater"
			header = "<div class=""main_heading2"">Tabeller og resultater</div>"
		Case "kul_reportasjer"
			'stoffgruppe Kultur og underholdning, with undergruppe different from "Oversikter og notiser" and "Menyer, til red"
			header = "<div class=""main_heading2"">Reportasjer</div>"
		Case "kul_notiser"
			'stoffgruppe Kultur og underholdning, with undergruppe "Oversikter og notiser"
			header = "<div class=""main_heading2"">Notiser</div>"
		Case "kul_menyer"
			'stoffgruppe Kultur og underholdning, with undergruppe "Menyer, til red"
			header = "<div class=""main_heading2"">Menyer</div>"
		Case "priv_menyer"
			'undergruppe "Menyer, til red" (no stoffgruppe) Helen 2103: Should not be messages with stoffgruppe Priv til red
			header = "<div class=""main_heading2"">Menyer</div>"
		Case "priv_tilred"
			'stoffgruppe Priv til red, except undergruppe "Menyer, til red" and "PRM-tjenesten" Helen 2103:Should incl undergruppe "Menyer, til red"
			header = "<div class=""main_heading2"">Priv til red</div>"
		Case "prm_ntbpluss"
			'stoffgruppe Formidlingstjenester, undergruppe "PRM-tjenesten"
			header = "<table border=""0"" width=100% cellpadding=""0"" cellspacing=""0""class=""main_heading2""><tr><td>NTB Pluss</td><td align=""right""><img src=""../images/ntbpluss.gif""></td></tr></table>"
			'header = "<div class=""main_heading2"">NTB Pluss</div>"
		Case "prm_nwa"
			'stoffgruppe Formidlingstjenester, undergruppe "NWA"
			header = "<table border=""0"" width=100% cellpadding=""0"" cellspacing=""0""class=""main_heading2""><tr><td>News Aktuell </td><td align=""right""><img src=""../images/nwa.gif""></td></tr></table>"
			'header = "<div class=""main_heading2"">News Aktuell</div>"
		Case "prm_bwi"
			'stoffgruppe Formidlingstjenester, undergruppe "BWI"
			header = "<table border=""0"" width=100% cellpadding=""0"" cellspacing=""0""class=""main_heading2""><tr><td>Business Wire </td><td align=""right""><img src=""../images/bwi.gif""></td></tr></table>"
			'header = "<div class=""main_heading2"">Business Wire</div>"
		Case "pr_sounds"
			'stoffgruppe Formidlingstjenester, undergruppe "BWI"
			header = "<table width='95%'><tr><td><div class='main_heading2'>Siste ukes lydsaker</div></td><td><div class='hjelpetekst' align='right'>Sett mark�ren p� h�ytalersymbolet for � h�re lyden</div></td></tr></table>"
		Case "sak_alle"
			'SakID=intCaseId stoffgruppe Innenriks of Utenriks
			header = "<div class=""main_heading2"">F�lg saken</div>"
		Case "sak_bakgrunn"
			'KeyWordID=intKeyWordID undergruppe "Bakgrunn" og "Analyse" og "Faktaboks"
			header = "<div class=""main_heading2"">Bakgrunn - Analyse - Fakta</div>"
	End Select
	
	
	'logic for displaying frames for different screen resolutions
	Dim defaultHeight1, defaultHeight2 
	
	Dim lowRes 'this parameter should be retrieved from session object
	lowRes = False
	
	If (lowRes = true) Then
		defaultHeight1 = 321
		defaultHeight1s = 220
		defaultHeight2 = 145
	Else
		defaultHeight1 = 473
		defaultHeight1s = 380
		defaultHeight2 = 221
	End If
	  
	
	Dim frameHeight
	Dim frameWidth
	Dim sourceFile
	sourceFile = "../template/iframe.asp?listType=" & ListType & "&photoOption=" & PhotoOption  & "&SakNr=" & intSakNr
	
	If (SizeTypeHeight = "height1") Then
		frameHeight = defaultHeight1 
	ElseIf (SizeTypeHeight = "height1s") Then
		frameHeight = defaultHeight1s 
	Else
		frameHeight = defaultHeight2
	End If
	
	If (SizeTypeWidth = "width2") Then
		frameWidth = 291
	ElseIf (SizeTypeWidth = "width1") Then
		frameWidth = 600
	Else 
		frameWidth = 188
	End If
	
	RenderIFrame = "<table bordercolor=""#61c7d7"" border=""1"" cellpadding=""2"" cellspacing=""3"" valign=""top"">" _
					& 	"<tr valign=""top"" align=""left"">" _
					&		"<td valign=""top"" style='border:none;'>" + header + "<IFRAME NAME=""Frame" & FrameNumber & """ FRAMEBORDER=""0"" WIDTH=""" & frameWidth & """ HEIGHT=""" & frameHeight & """ MARGINWIDTH=""0"" MARGINHEIGHT=""0"" SRC=""" & sourceFile & """></IFRAME></td>" _
					&	"</tr>" _
					& "</table>" 

End Function

' -----------------------------------------------------------------------------
' TitleTag
'
' Description:
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------

Function TitleTag()
	TitleTag = "<title>NTBs nyhetsportal</title>"
End Function
%>