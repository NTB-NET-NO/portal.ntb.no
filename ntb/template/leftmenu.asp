<%
'***********************************************************************************
' leftmenu.asp
'
' This file is used for displaying links on the left of the page. Most of the links
' are internal, but some are external.
' The leftmenu also include logic for displaying case related thumbnails (max 3).
' The leftmenu is included in almost every mainpage to provide the left navigator.
'
' USES:
'		none
'
' NOTES:
'		none
'
' CREATED BY: Trond Orrestad, Andersen
' CREATED DATE: 2002.04.09
' UPDATED BY: Solveig Skjermo, Andersen
' UPDATED DATE: 2002.06.17
' REVISION HISTORY:
'		none
'
'**********************************************************************************
%>

<table background="../images/leftframe550x120_1.jpg" bgcolor="#003084" border="0" cellpadding="4" cellspacing="2" width="84" align="left">
	<tr><td>

		<tr>
			<td class="left_button" width="85">
			<A href="../mainpages/Personalization_choose_window.asp" class="nav">Tilpass side</A>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
			<a href="../mainpages/user_homepage.asp" class="nav">Mine nyheter</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=AlleNyheter" class="nav">Alle nyheter</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=Innenriks" class="nav">Innenriks</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=Utenriks" class="nav">Utenriks</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=Okonomi" class="nav">&Oslash;konomi</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=Sport" class="nav">Sport</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=Kultur" class="nav">Kultur og<br>underholdning</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=PrivTilRed" class="nav">Priv. til red.</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/MainSectionPages.asp?pageID=PRM" class="nav">Pressemeldinger</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/WorkList.asp" class="nav">Arbeidslister</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/nyhetskalender.asp" class="nav">Nyhetskalender</a>
			</td>
		</tr>
		<tr>
			<td class="left_button" width="81">
				<a href="../mainpages/intermedium.asp" class="nav" target="_blank">Andre kilder</a>
			</td>
		</tr>
		<!--tr>
			<td class="left_button" width="81">
				<a href="../mainpages/soundlist.asp" class="nav">Lydsaker</a>
			</td>
		</tr-->

		<%
		if Application("vtCL") = 1 then
			Response.Write Application("CaseListBuffer1")
		else
			Response.Write Application("CaseListBuffer2")
		End if

		%>

	</td></tr>

</table>
