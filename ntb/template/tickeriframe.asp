<%
'***********************************************************************************
'* tickeriframe.asp
'*
'* This file is used for displaying either ticker messages (if any) or a row with
'* phone numbers and emails for different NTB positions to be shown in the top menu.
'*
'* USES:
'*		/include/NTB_ticker_lib.asp - for logic regarding the ticker
'*
'* NOTES:
'*		none
'*
'* CREATED BY: Trond Orrestad, Andersen
'* CREATED DATE: 2002.04.09
'* UPDATED BY: Trond Orrestad, Andersen
'* UPDATED DATE: 2002.04.09
'* REVISION HISTORY:
'*		none
'*
'**********************************************************************************
%>
<!-- #INCLUDE FILE="../include/NTB_ticker_lib.asp" -->

<html>
<head>
<SCRIPT LANGUAGE="JavaScript">
function open_chat(url)
{
 leftPlacement = screen.width - 410; //ensures that the window pops up to the right independent on screen resolution
 winOptions = 'titlebar=0,toolbar=0,location=0,menubar=0,scrollbars=0,resizable=1,width=400,height=440,top=80,left='+leftPlacement;
 var win=window.open(url,'chat',winOptions);
 win.focus();
}
</SCRIPT>
<meta http-equiv="REFRESH" Content="62">
<link href="../include/ntb.css" type="text/css" rel="stylesheet">


</head>
<body bgcolor="#003084">

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="../include/overlib.js"></script>

<%
dim face
face = getTickerFace()
'show ticker bar if it is not empty
if face <> "" then
Response.Write face
else
'show phone numbers and emails
%>
<table border="0" width="774" bgcolor="#003084">
<tr height="20">
		<td colspan="8" height="20" class="contactperson_number">

			<!--p class="contactperson_number"><span class="contactperson_title"><a href="../mainpages/chat.asp?channel=vakt" target="_blank"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:vaktsjef@ntb.no" class="nav">Vaktsjef:</a></span><span class="contactperson_number"> 22 03 45 45 &nbsp; </span><span class="contactperson_title"><a href="../mainpages/chat.asp?channel=report" target="_blank"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:reportasjeleder@ntb.no" class="nav">Reportasjeleder:</a></span><span class="contactperson_number"> 22 03 45 53 &nbsp; </span><span class="contactperson_title"><a href="../mainpages/chat.asp?channel=uriks" target="_blank"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:utenriks@ntb.no" class="nav">Utenriks:</a> </span><span class="contactperson_number">22 03 45 50 &nbsp; </span><span class="contactperson_title"><a href="../mainpages/chat.asp?channel=sport" target="_blank"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:sporten@ntb.no" class="nav">Sport:</a> </span><span class="contactperson_number">22 03 45 55 &nbsp; </span><span class="contactperson_title"><a href="mailto:plussvaktsjef@ntb.no" class="nav">NTB Pluss: </a></span><span class="contactperson_number">22 03 44 05 &nbsp; </span><span class="contactperson_title"><a href="mailto:desk@scanpix.no" class="nav">Foto:</a> </span><span class="contactperson_number">22 00 32 64</span><p class="top_button"></p-->
		    <!--p class="contactperson_number"><span class="contactperson_title"><A href="Javascript:open_chat('../mainpages/chat.asp?channel=vakt')"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:vaktsjef@ntb.no" class="nav">Vaktsjef:</a></span><span class="contactperson_number"> 22 03 45 45 &nbsp; </span><span class="contactperson_title"><A href="Javascript:open_chat('../mainpages/chat.asp?channel=report')"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:reportasjeleder@ntb.no" class="nav">Reportasjeleder:</a></span><span class="contactperson_number"> 22 03 45 53 &nbsp; </span><span class="contactperson_title"><A href="Javascript:open_chat('../mainpages/chat.asp?channel=uriks')"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:utenriks@ntb.no" class="nav">Utenriks:</a> </span><span class="contactperson_number">22 03 45 50 &nbsp; </span><span class="contactperson_title"><A href="Javascript:open_chat('../mainpages/chat.asp?channel=sport')"><img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"></a>&nbsp;<a href="mailto:sporten@ntb.no" class="nav">Sport:</a> </span><span class="contactperson_number">22 03 45 55 &nbsp; </span><span class="contactperson_title"><a href="mailto:plussvaktsjef@ntb.no" class="nav">Kultur: </a></span><span class="contactperson_number">22 03 46 55&nbsp; </span><span class="contactperson_title"><a href="mailto:desk@scanpix.no" class="nav">Scanpix:</a> </span><span class="contactperson_number">22 00 32 64</span><p class="top_button"></p-->

		    <p class="contactperson_number">
		    	<span class="contactperson_title">
				<img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"
					onmouseover="return overlib('MSN Messenger: <b>vaktsjef@ntb.no</b>', ABOVE, WIDTH, 220) ;"
					onmouseout="return nd();">&nbsp;
				<a href="mailto:vaktsjef@ntb.no" class="nav">Vaktsjef:&nbsp;</a></span>
		    	<span class="contactperson_number"> 22 03 45 45 &nbsp;</span>

		    	<span class="contactperson_title">
		    	<img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"
					onmouseover="return overlib('MSN Messenger: <b>nyhetssjef@ntb.no</b>', ABOVE, WIDTH, 220) ;"
					onmouseout="return nd();">&nbsp;
		    	<a href="mailto:nyhetssjef@ntb.no" class="nav">Reportasjeleder:&nbsp;</a></span>
		    	<span class="contactperson_number">22 03 45 01 &nbsp;</span>

		    	<span class="contactperson_title">
		    	<img src="../images/chatblue2.gif" WIDTH="9" HEIGHT="11" border="0"
		    		onmouseover="return overlib('MSN Messenger: <b>uvakt@ntb.no</b>', ABOVE, WIDTH, 220) ;"
					onmouseout="return nd();">&nbsp;
		    	<a href="mailto:utenriks@ntb.no" class="nav">Utenriks:</a></span>
		    	<span class="contactperson_number">22 03 45 50 &nbsp;</span>

		    	<span class="contactperson_title">
		    	<a href="mailto:sporten@ntb.no" class="nav">Sport:&nbsp;</a></span>
		    	<span class="contactperson_number">22 03 45 55 &nbsp;</span>

		    	<span class="contactperson_title">
		    	<a href="mailto:kultur@ntb.no" class="nav">Kultur:</a></span>
		    	<span class="contactperson_number">22 03 46 55&nbsp;</span>

		    	<span class="contactperson_title">
		    	<a href="mailto:desk@scanpix.no" class="nav">Scanpix:</a></span>
		    	<span class="contactperson_number">22 00 32 64</span>

		    	<p class="top_button"></p>
		</td>
    </tr>
</table>
<%
end if
%>
</body>
</html>
