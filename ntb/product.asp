<!-- #INCLUDE FILE="include/header.asp" -->
<!-- #INCLUDE FILE="include/const.asp" -->
<!-- #INCLUDE FILE="include/html_lib.asp" -->
<!-- #INCLUDE FILE="include/catalog.asp" -->
<!-- #INCLUDE FILE="include/std_access_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cache_lib.asp" -->
<!-- #INCLUDE FILE="include/std_cookie_lib.asp" -->
<!-- #INCLUDE FILE="include/std_ordergrp_lib.asp" -->
<!-- #INCLUDE FILE="include/std_pipeline_lib.asp" -->
<!-- #INCLUDE FILE="include/std_profile_lib.asp" -->
<!-- #INCLUDE FILE="include/std_url_lib.asp" -->
<!-- #INCLUDE FILE="include/std_util_lib.asp" -->
<!-- #INCLUDE FILE="template/discount.asp" -->
<!-- #INCLUDE FILE="include/setupenv.asp" -->
<!-- #INCLUDE FILE="template/layout1.asp" -->
<%
' =============================================================================
' Product.asp
' Display page for Product Details
'
' Commerce Server 2000 Solution Sites 1.0
' -----------------------------------------------------------------------------
'  This file is part of Microsoft Commerce Server 2000
'
'  Copyright (C) 2000 Microsoft Corporation.  All rights reserved.
'
' This source code is intended only as a supplement to Microsoft
' Commerce Server 2000 and/or on-line documentation.  See these other
' materials for detailed information regarding Microsoft code samples.
'
' THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
' KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
' =============================================================================

Sub Main()
	Dim sCatalogName, sCategoryName, sProductID, sVariantID, sCacheKey, sProductName, sIdentifyingVariantProperty, sBtnText
	Dim BasketAffinity, ProductAffinity, nDiscountsToShow
	Dim mscsCatalog, mscsProduct, rsProperties, rsVariants, rsPropertyAttributes
	Dim bHasVariants
	Dim htmTitle, htmContent, htmFormContent, htmProduct, urlAction
	Dim sProductAction
	
	Call EnsureAccess()
	
	sPageTitle = mscsMessageManager.GetMessage("L_Product_HTMLTitle", sLanguage)
	
	htmPageContent = Null
	bHasVariants = False	
	
	' Get the catalog name
	sCatalogName = GetRequestString(CATALOG_NAME_URL_KEY, Null)
	Call EnsureUserHasRightsToCatalog(sCatalogName, m_UserID)
	
	' Get the product ID
	sProductID = GetRequestString(PRODUCT_ID_URL_KEY, Null)

	' Get the Product Action key
	sProductAction = GetRequestString(PRODUCT_ACTION_KEY, Null)
	
	If IsNull(sCatalogName) Or IsNull(sProductID) Then
		Response.Redirect(GenerateURL(MSCSSitePages.BadURL, Array(), Array()))		
	End If

	' Get the category name
	sCategoryName = GetRequestString(CATEGORY_NAME_URL_KEY, Null)

	'Get the variant ID
	sVariantID = GetRequestString(VARIANT_ID_URL_KEY, Null)

	' Get the catalog fragment from the cache, or render it
	sCacheKey = sCatalogName & sCategoryName & sProductID & sVariantID
	htmPageContent = LookupCachedFragment("ProductPageCache", sCacheKey)
	
	If IsNull(htmPageContent) Then
		Set mscsCatalog = MSCSCatalogManager.GetCatalog(sCatalogName)
		If mscsCatalog Is Nothing Then
			Response.Redirect(GenerateURL(MSCSSitePages.BadCatalogItem, Array(), Array()))
		End If
		
		Set mscsProduct = mscsCatalog.GetProduct(sProductID)
		If mscsProduct Is Nothing Then
			Response.Redirect(GenerateURL(MSCSSitePages.BadCatalogItem, Array(), Array()))	
		End If
		
		Set rsProperties = mscsProduct.GetProductProperties
		
		' "name" is a required product property and cannot have null value.
		sProductName = rsProperties.Fields(PRODUCT_NAME_PROPERTY_NAME).Value
		
		' Determine if the product has variants
		Set rsVariants = mscsProduct.Variants
		If Not rsVariants.EOF Then 
			bHasVariants = True
		End If

		' Render the product's built in properties (ie price)
		' (we don't do this for variants here, as they may differ per variant
		If Not bHasVariants Then
			htmProduct = htmRenderBuiltInProperties( _
													rsProperties, _
													PRODUCT_PRICE_PROPERTY_NAME, _
													MSCSSiteStyle.Body _
												) & CRLF
		End If
    
		' Render user-defined properties
		htmProduct = htmProduct & htmRenderUserDefinedProductProperties(rsProperties, MSCSSiteStyle.Body) & CRLF
		
	    ' If the product is a variant, render each of the variants (along with built-in properties for
		' each variant)

		If bHasVariants Then
			sIdentifyingVariantProperty = mscsCatalog.IdentifyingVariantProperty
			htmFormContent = RenderText(mscsMessageManager.GetMessage("L_Product_Has_Variants_HTMLText", sLanguage), MSCSSiteStyle.Body) & CR
			htmFormContent = htmFormContent & htmRenderVariantsList(sIdentifyingVariantProperty, rsVariants, sVariantID) & CRLF
		End If

		' Render the AddToBasket form	
		htmFormContent = htmFormContent & RenderHiddenField(CATALOG_NAME_URL_KEY, sCatalogName)
		htmFormContent = htmFormContent & RenderHiddenField(CATEGORY_NAME_URL_KEY, sCategoryName)
		htmFormContent = htmFormContent & RenderHiddenField(PRODUCT_ID_URL_KEY, sProductID)
		
		If Not bHasVariants And (rsProperties.Fields(PROD_CLASSTYPE_FLDNAME) = cscProductFamilyClass) Then
			htmFormContent = htmFormContent & RenderText(mscsMessageManager.GetMessage("L_ProdFamNoVariant_ErrorMessage",sLanguage), MSCSSiteStyle.Warning) & BR
		Else
			htmFormContent = htmFormContent & RenderText(mscsMessageManager.GetMessage("L_Specify_Product_Quantity_HTMLText", sLanguage), MSCSSiteStyle.Body)
			htmFormContent = htmFormContent & RenderTextBox(PRODUCT_QTY_URL_KEY, 1, 3, 3, MSCSSiteStyle.TextBox)
			
			sBtnText = mscsMessageManager.GetMessage("L_Add_To_Basket_Button", sLanguage)
			htmFormContent = htmFormContent & RenderSubmitButton(SUBMIT_BUTTON, sBtnText, MSCSSiteStyle.Button)
		End If
		
		urlAction = GenerateURL(MSCSSitePages.AddItem, Array(), Array())
		htmContent = htmProduct & RenderForm(urlAction, htmFormContent, HTTP_POST) 
			
		htmTitle = RenderText(sProductName, MSCSSiteStyle.Title)
		htmPageContent = htmTitle & CRLF & htmContent

		Call CacheFragment("ProductPageCache", sCacheKey, htmPageContent)
	End If

	' Show user message, if needed	
	Select Case (sProductAction)
		Case Null
		Case ""
		Case ADD_ACTION
			htmPageContent = htmPageContent & "<br>" & mscsMessageManager.GetMessage("pur_AddItem", sLanguage) & "<br>"
	End Select
	
	' Populate the discount banner slot
	Set ProductAffinity = GetShownProductsDetails()
	BasketAffinity = Null
	nDiscountsToShow = 1
	htmDiscountBannerSlot = RenderDiscounts(ProductAffinity, BasketAffinity, nDiscountsToShow)
End Sub


' -----------------------------------------------------------------------------
' htmRenderBuiltInProperties
'
' Description:
'   Renders the product's in built properties, such as price
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderBuiltInProperties(ByVal rsProperties, ByVal sPropertyName, ByVal style)
    Dim sText
    
    sText = mscsMessageManager.GetMessage("L_Product_Price_DisplayName_HTMLText", sLanguage) 
    sText = sText & ": " & htmRenderCurrency(rsProperties.Fields(sPropertyName).Value)
    htmRenderBuiltInProperties = RenderText(sText, style)
End Function


' -----------------------------------------------------------------------------
' htmRenderUserDefinedProductProperties
'
' Description:
'   Renders the product's user-defined properties
'   Certain properties are treated specially, such as
'		IMAGE_FILENAME_PROPERTY_NAME
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderUserDefinedProductProperties(ByVal rsProperties, ByVal style)
    Dim fldProperty
    Dim sPropertyName
    Dim htmProperty
    Dim nWidth, nHeight
    Dim bShowProperty
	
	' Iterate each property
	For Each fldProperty In rsProperties.Fields
		' Find out if the property can be shown. Do not display empty properties.
		bShowProperty = False
		If Not IsNull(fldProperty.Value) Then
			' Filter out built-in properties. Built-in properties do not have attributes.
			If Not IsNull(MSCSCatalogAttribs.Value(fldProperty.Name)) Then
				If MSCSCatalogAttribs.Value(fldProperty.Name).Value(DISPLAY_ON_SITE_ATTRIBUTE) = True Then
					bShowProperty = True
				End If
			End If
		End If
		
		If bShowProperty Then
			Select Case UCase(fldProperty.Name)
			  	Case UCase(IMAGE_FILENAME_PROPERTY_NAME)
					nWidth = PeekField(rsProperties, IMAGE_WIDTH_PROPERTY_NAME)
					nHeight = PeekField(rsProperties, IMAGE_HEIGHT_PROPERTY_NAME)
			  		htmProperty = BR & RenderImage(rsProperties.Fields(IMAGE_FILENAME_PROPERTY_NAME).Value, nWidth, nHeight, mscsMessageManager.GetMessage("L_Standard_Image_Description_Text", sLanguage), "") & CRLF
										                        
				Case UCase(PRODUCT_NAME_PROPERTY_NAME), _
				     UCase(IMAGE_WIDTH_PROPERTY_NAME), _
				     UCase(IMAGE_HEIGHT_PROPERTY_NAME)
				    htmProperty = ""  
										                            
				Case Else
				    Rem Use DisplayName attribute if it is set, otherwise use PropertyName
					sPropertyName = sGetPropertyDisplayName(fldProperty.Name)
				    htmProperty = RenderText(FormatOutput(LABEL_TEMPLATE, Array(sPropertyName)) & fldProperty.Value, MSCSSiteStyle.Body) & BR
			End Select
			htmRenderUserDefinedProductProperties = htmRenderUserDefinedProductProperties & htmProperty
		End If
	Next
End Function

' -----------------------------------------------------------------------------
' PeekField
'
' Description:
'   This function attempts to read a field off a recordset.
'   If the field doesn't exist, it returns null.
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function PeekField(ByVal rs, ByVal sFieldName)
	PeekField = Null
	On Error Resume Next
		PeekField = rs.Fields(sFieldName).Value
	On Error Goto 0
End Function


' -----------------------------------------------------------------------------
' htmRenderVariantsList
'
' Description:
'   Render all variants of the given product, with a radio button in front
'	of them so the shopper can choose which variant to buy
'
' Parameters:
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function htmRenderVariantsList(ByVal sIdentifyingVariantProperty, ByVal rsVariants, ByVal sSelectedVariantID)
    Dim listProps, arrHeaderCols, arrDataCols, arrAttLists, i, htmRows, bSelected
    
	Set listProps = listGetVariantPropertiesToShow(rsVariants.Fields)
	
	' Size arrays to accomodate the table columns. 
	ReDim arrHeaderCols(listProps.Count)
	ReDim arrDataCols(listProps.Count)
	ReDim arrAttLists(listProps.Count)
	
	' Column containg radio buttons does not require a header.
	arrHeaderCols(0) = ""
	
	' Put the display name of each user-defined property in table header row.
	For i = 0 to listProps.Count - 2
		arrHeaderCols(i + 1) = sGetPropertyDisplayName(listProps(i))
	Next
	
	' The last column is the price column.
	arrHeaderCols(listProps.Count) = mscsMessageManager.GetMessage("L_Product_Price_DisplayName_HTMLText", sLanguage)
	
	' Render the table header row, all columns within the row are center-justified.
	htmRows = RenderTableHeaderRow(arrHeaderCols, Array(), MSCSSiteStyle.TRCenter)
	
	' Set the alignment attribute for the first data column (radio buttons).
	arrAttLists(0) = MSCSSiteStyle.TDCenter
	
	' Set the alignment attribute for columns containing user-defined properties.
	'	Note that we assume a user-defined variant property has a data type of string.
	'	If you have assigned a different data type to such a property, you may
	'	need to align it differently (e.g. currency data type requires right alignment.) 
	For i = 0 to listProps.Count - 2
		arrAttLists(i + 1) = MSCSSiteStyle.TDLeft
	Next
	
	' Set the alignment attribute for the last column (price).
	arrAttLists(listProps.Count) = MSCSSiteStyle.TDRight
	
	' If default selection is not specified, select the first variant in the list.
	If IsNull(sSelectedVariantID) Then
		sSelectedVariantID = rsVariants.Fields(sIdentifyingVariantProperty).Value
	End If
	
	' Iterate thru all variants and render each as a table row.
    While Not rsVariants.EOF 
		bSelected = False
		
		If sSelectedVariantID = rsVariants.Fields(sIdentifyingVariantProperty).Value Then
			bSelected = True
		End If
		
		arrDataCols(0) = RenderRadioButton("variant_id", rsVariants.Fields(sIdentifyingVariantProperty).Value, bSelected, MSCSSiteStyle.RadioButton)
		
		' Note that we assume a user-defined variant property has a data type of string.
		'	If you have assigned a different data type to a such a property, you may
		'	need to perform additional transformation on the property value (such as calling
		'	htmRenderCurrency() for currency values) prior to displaying it.
		For i = 0 to listProps.Count - 2
			arrDataCols(i + 1) = rsVariants.Fields(listProps(i)).Value
		Next
		
		arrDataCols(listProps.Count) = htmRenderCurrency(rsVariants.Fields("cy_list_price").Value)
		
		htmRows = htmRows & RenderTableDataRow(arrDataCols, arrAttLists, MSCSSiteStyle.TRMiddle)
		rsVariants.MoveNext
	Wend
	
	htmRenderVariantsList = RenderTable(htmRows, MSCSSiteStyle.BorderedTable)
End Function


Function listGetVariantPropertiesToShow(ByVal fldsProps)
	Dim listProps, fldProp
	
	Set listProps = GetSimpleList()
	
	' Iterate thru all properties to built a list of properties to display.
	For Each fldProp In fldsProps
		' Exclude built-in properties.
		If (fldProp.Name <> "TimeStamp") And (fldProp.Name <> "cy_list_price") Then
			' Include properties that are marked for display.
			If MSCSCatalogAttribs.Value(fldProp.Name).Value("DisplayOnSite") = True Then
				listProps.Add(fldProp.Name)
			End If
		End If
	Next
	
	' Though price is a built-in property, it must be displayed.
	listProps.Add("cy_list_price")
	
	Set listGetVariantPropertiesToShow = listProps
End Function
	

' -----------------------------------------------------------------------------
' sGetPropertyDisplayName
'
' Description:
'   Gets the display name for a catalog property where available, otherwise returns the 
'   property column name
'
' Parameters:
'	sPropName			- The name of the property
'
' Returns:
'
' Notes :
'   none
' -----------------------------------------------------------------------------
Function sGetPropertyDisplayName(ByVal sPropName)
	' Use property's display name attribute if it is set
	If Not IsNull(MSCSCatalogAttribs.Value(sPropName).Value(DISPLAY_NAME_ATTRIBUTE)) Then
	    sGetPropertyDisplayName = MSCSCatalogAttribs.Value(sPropName).Value(DISPLAY_NAME_ATTRIBUTE)
	Else
	    ' Otherwise, use property name
	    sGetPropertyDisplayName = MSCSCatalogAttribs.Value(sPropName).Value(PROPERTY_NAME_ATTRIBUTE)
	End If
End Function
%>
