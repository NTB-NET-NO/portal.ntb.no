<%@ Language=VBScript %>
<%
'***********************************************************************************
' wap_kunder/wap_login.asp
'
' This file is used for login of SMS /Wap users who has no access rights 
' from portal Customers table
'
' USES:
'		none
' NOTES:
'		none
'
' CREATED BY: Roar Vestre, NTB
' CREATED DATE: 2002.08.15
' REVISION HISTORY:
'
'**********************************************************************************
option explicit

dim strMobile, strUsername
strMobile = Session("strMobile")
strUsername = Session("Username")

Function CheckError()
	dim strFeil

	strFeil = Session("strFeil")
	if strFeil <> "" then
		CheckError = "<p>" & strFeil & "</p>"
		Session("strFeil") = ""
	Else
		CheckError = ""
	end if
End Function	

%>

<html>
<head>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<title>NTB WAP kunderegister</title>

</head>
<!--
<body Background bgcolor="#ffffbf" text="#000000" link="#000000" vlink="#000000" alink="#000000" leftmargin="4">
-->
<body>

<font face="Arial"><p><em><strong>NTBs WAP og SMS side</p>

<%=CheckError%>

</strong></em>

<p><b>Logg inn: <%=strUsername%></b></p>

</font>


<form action="openbase.asp" method="post" id="form1" name="form1">
<p>
<table cellSpacing="1" cellPadding="1" border="0">

  <tr>
    <td><font face="Arial" size="2">GSM nummer: </font>  </td>
    <td><input id="Mobile" style="LEFT: 52px; TOP: 54px" name="Mobile" size="10" value="<%=strMobile%>">
		<font face="Arial" size="2"></font>
    </td>
  </tr>
  <tr>
    <td><font face="Arial" size="2">Passord: </font> </td>
    <td><input id="password" type="password" name="password" size="10">
    <font face="Arial" size="2"></font>
    </td></tr>
</table>

<input type="submit" value="Logg inn" id="submit1" name="submit1">
<!--<input type="submit" value="F� passord" id="submit2" name="submit2">-->
</form>

</body>
</html>
