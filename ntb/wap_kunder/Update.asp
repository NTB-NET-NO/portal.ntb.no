<%@ Language=VBScript %>
<%
'***********************************************************************************
' wap_kunder/Update.asp
'
' This file is used for updating SMS /Wap customer information to
' SQL-server table "wap_kunder"
'
' USES:
'		none
'
' NOTES:
'		none
'
' CREATED BY: Roar Vestre, NTB
' CREATED DATE: 2002.08.15
' REVISION HISTORY:
' Updated 2005.02.28 av Roar Vestre, lagt til felt for E-postvarsling
' Updated 2005.03.09 av Roar Vestre, Endret regler for mobilnr.
'**********************************************************************************
option explicit

dim xmlDoc
dim rsGruppeValg
dim rsCustomer
dim strNTBUnderKatKuriosa2
dim strTest, strSokeText, strTotal, strName, strVerdi, strPrio13Stoffgr2
dim strNTBStoffgrupper2, strNTBHovedKategorier2, strNTBOmraader2, strNTBFylker2
dim strKur, strNTBUnderKatOkonomi2, strNTBUnderKatKultur2, strNTBUnderKatSport2
dim bSkriv, strMobilphone, isMobileErr, strEpost, isEpostErr, bSendEpost

if Session("UserName") = "" then
	Response.Redirect "../Authfiles/Login.asp"
end if

'Check if Recordset is active
if not isObject(Session("rsCustomer")) then
	Response.Buffer = true
	Response.Clear
	Response.Redirect "openbase.asp"
end if

set rsGruppeValg = Session("rsGruppeValg")
set rsCustomer = Session("rsCustomer")

strMobilphone = (replace(Request.Form("mobilphone")," ", ""))
strEpost = Request.Form("epost")
bSendEpost = Request.Form("chk_epost") = "true"
'strMobilphone = replace(strMobilphone, "+", "00")

If len(strMobilphone) < 8 then isMobileErr = true

If (len(strEpost) < 6 or instr(2, strEpost, "@") = 0) and bSendEpost  then
	isEpostErr = true
End If

'if not isNumeric(strMobilphone) then
'	isMobileErr = true
'elseif strMobilphone < 40000000 then
'	isMobileErr = true
'end if

if isMobileErr then
	Session("UpdateErr") = "Oppgi lovlig mobilnr.!"
	rsCustomer.Fields("Mobilphone") = strMobilphone
	Response.Clear
	Response.Redirect "hovedform.asp"
end if

if isEpostErr then
	Session("UpdateErr") = "Oppgi lovlig E-postadresse!"
	rsCustomer.Fields("Epost") = strEpost
	Response.Clear
	Response.Redirect "hovedform.asp"
end if

strSokeText = trim(Request.Form("textarea1"))
rsCustomer.Fields("NTBSniffertxt") = strSokeText & ""

rsCustomer.Fields("Mobilphone") = strMobilphone & ""
rsCustomer.Fields("Epost") = strEpost & ""

rsCustomer.Fields("WAP") = Request.Form("chk_sms") = "true"
rsCustomer.Fields("SMS") = Request.Form("chk_sms") = "true"
rsCustomer.Fields("SendEpost") = bSendEpost

rsCustomer.Fields("NTBStoffgrupper") = ""
rsCustomer.Fields("NTBHovedKategorier") = ""
rsCustomer.Fields("NTBOmraader") = ""
rsCustomer.Fields("NTBFylker") = ""
rsCustomer.Fields("NTBUnderKatOkonomi") = ""
rsCustomer.Fields("NTBUnderKatKultur") = ""
rsCustomer.Fields("NTBUnderKatKuriosa") = ""
rsCustomer.Fields("NTBUnderKatSport") = ""
rsCustomer.Fields("Prio13Stoffgr") = ""

strPrio13Stoffgr2 = SkrivVerdier(9, "Prio13Stoffgr")

'NY KODE 24.06.2002 RoV:
If strPrio13Stoffgr2 <> "" Then
    strPrio13Stoffgr2 = strPrio13Stoffgr2 & "Priv-til-red;"
End If

strNTBStoffgrupper2 = SkrivVerdier(1, "NTBStoffgrupper")

if instr(1, strNTBStoffgrupper2, "Utenriks") <> 0 then
	strNTBHovedKategorier2 = SkrivVerdier(4, "NTBHovedKategorier")
	strNTBOmraader2 = SkrivVerdier(2, "NTBOmraader")
end if

if instr(1, strNTBStoffgrupper2, "Innenriks") <> 0 then
	strNTBHovedKategorier2 = SkrivVerdier(4, "NTBHovedKategorier")
	strNTBOmraader2 = SkrivVerdier(2, "NTBOmraader")
	strNTBFylker2 = SkrivVerdier(3, "NTBFylker")
end if

if instr(1, strNTBOmraader2, "Norge") <> 0 then
	strNTBFylker2 = SkrivVerdier(3, "NTBFylker")
end if

if instr(1, strNTBHovedKategorier2, "Okonomi") <> 0 then
	strNTBUnderKatOkonomi2 = SkrivVerdier(6, "NTBUnderKatOkonomi")
end if

if instr(1, strNTBHovedKategorier2, "Kultur") <> 0 then
	strNTBUnderKatKultur2 = SkrivVerdier(8, "NTBUnderKatKultur")
end if

if instr(1, strNTBStoffgrupper2, "Sport") <> 0 then
	strNTBUnderKatSport2 = SkrivVerdier(5, "NTBUnderKatSport")
end if

function SkrivVerdier(intType, strType)
	strTotal = ""
	with rsGruppeValg
		.Filter = "GruppeId=" & intType
		do until .EOF
			strName = "chk_chk" & .Fields("Verdi") & intType
			strVerdi = Request.Form(strName)
			if strVerdi <> "" then
				strTotal = strTotal & strVerdi & ";"
				bSkriv = true
			end if
			.MoveNext
		loop
	end with

	select case intType
	case 2
		if instr(1, rsCustomer.Fields("NTBStoffgrupper"), "Innenriks") <> 0 then
			'strTotal = strTotal & "Norge;"
		end if
	case 4
		if instr(1, strTotal, "Kongestoff") <> 0 then
			strKur = strKur & "Kongestoff;"
		end if
		if instr(1, strTotal, "Kuriosa") <> 0 then
			strKur = strKur & "Kjendiser;"
		end if

		if strKur <> "" then
			rsCustomer.Fields("NTBUnderKatKuriosa") = strKur
			strNTBUnderKatKuriosa2 = "<tr><td>NTBUnderKatKuriosa:</td><td> " & strKur & "</td></tr>"
			'response.write strNTBUnderKatKuriosa2
		end if
	case 9
		If strTotal <> "" Then
			strTotal = strTotal & "Priv-til-red;"
		End If
	End select

	rsCustomer.Fields(strType) = strTotal

	if bSkriv then
		SkrivVerdier = "<tr><td>" & strType & ":</td><td> " & strTotal & "</td></tr>"
	end if
	strTotal = ""
end function

if Request.Form("save") = "false" then
	Response.Clear
	Response.Redirect "hovedform.asp"
end if

rsCustomer.Fields("REMOTE_ADDR") = Request.ServerVariables("REMOTE_ADDR")
rsCustomer.Fields("AntallEndringer") = rsCustomer.Fields("AntallEndringer") + 1
rsCustomer.Fields("DatoSistEndret") = Now()

dim cn
set cn = Session("cn")
cn.open
set rsCustomer.ActiveConnection = cn

on error resume next
rsCustomer.UpdateBatch

if Err.number <> 0 then
	if Err.number = -2147217873 then
		Session("UpdateErr") =  "Mobilnummeret finnes i basen fra f�r!"
	else
		Session("UpdateErr") =  Err.number & ":" & Err.Description
	end if
	set rsCustomer.ActiveConnection = nothing
	cn.close
	Response.Clear
	Response.Redirect "hovedform.asp"
end if

on error goto 0

set rsGruppeValg = Session("rsGruppeValg")
rsCustomer.close
rsGruppeValg.Close
cn.close

set rsGruppeValg = nothing
set rsCustomer = nothing
set cn  = nothing

Session("rsGruppeValg") = ""
Session("rsCustomer") = ""
Session("cn") = ""

%>
<HTML>
<HEAD>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="../include/overlib.js"></script>
</HEAD>
<BODY>
<P><FONT face=Arial size=2><STRONG>
Endringene i din WAP/SMS-profil er n� lagret.
</STRONG></FONT></P>
</BODY>
</HTML>
