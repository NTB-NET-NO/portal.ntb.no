<%@ Language=VBScript %>
<%
'***********************************************************************************
' wap_kunder/openbase.asp
'
' This file is used for opening and verifying or adding
' SMS /Wap customer information from SQL-server table "wap_kunder"
'
' USES:
'		none
'
' NOTES:
'		none
'
' CREATED BY: Roar Vestre, NTB
' CREATED DATE: 2002.08.15
' REVISION HISTORY:
' 2005.03.10: Roar Vestre: Added Session("Mobile") and Session("Email") for SmsWapEmail
'**********************************************************************************
option explicit

Response.Buffer = true
'Response.Clear

const SMS_PATH = "D:\WAPSMS\SMS_UT\Til_TelenorOut\"
Dim rsCustomer
Dim strMobile
Dim strPassword
Dim strMessage
Dim strTest
Dim strUserName
Dim cn

Dim style
if Session("UserName") = "" then
	Response.Redirect "../Authfiles/Login.asp"
end if

if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

Call Main

Sub Main()
	Dim intACF

	if isObject(Session("rsCustomer")) then
		'Recordset allready open
		Response.Redirect "hovedform.asp"
	Else
		'Recordset not open
		strUserName = Session("Username")
		strMobile = trim(Request.Form("Mobile"))
		strPassword = trim(Request.Form("password"))

		'get session-variable for article autentication
		dim	isAutOk
		if Session("ACF") = "" then
			intACF = 0
		else
			intACF = clng(Session("ACF"))
		end if

		if (intACF AND 8192) > 0 then
			isAutOk = true
		else
			isAutOk = false
		end if

		If strMobile = "" then
		'Not called from LoginPage
			'Check is NTB user-id is registred in database:
			if OpenBase("LogonID", strUserName, isAutOk) then
				'Response.Clear
				Response.Redirect "hovedform.asp"
			Else
				Session("strFeil") = ""
				Response.Redirect "wap_login.asp"
			end if
		elseif OpenBase("Mobilphone", strMobile, false) then
		'Is called from LoginPage
			'Check if user is registred with mobile phone and password in database:
			if CheckPassword then
				Response.Clear
				Response.Redirect "hovedform.asp"
			end if
		end if
	end if

	Session("strMobile") = strMobile
	Session("strFeil") = "Feil mobilnr. eller passord.<br>Pr�v p� nytt, eller kontakt NTB for SMS/WAP-abonnement"
	Response.Redirect "wap_login.asp"

End Sub

Function CheckPassword()
	dim bLogon

	with rsCustomer
		if rsCustomer.Fields("Password") = strPassword and strPassword <> "" then
			bLogon = True

			rsCustomer.Fields("LogonID") = strUserName
			cn.Open
			set rsCustomer.ActiveConnection = cn
			rsCustomer.UpdateBatch 'adAffectCurrent '"LogonID", strUserName
			set rsCustomer.ActiveConnection = Nothing
			cn.Close
			CheckPassword = True
		else
			rsCustomer.Close
			'cn.Close
			CheckPassword = False
		end if
	end with

	strPassword = ""
End Function

Function OpenBase(strField, strFieldCheck, isNewOK)
	'--Project Data Connection
	dim strFilter
	strFilter = strField & "='" & strFieldCheck & "'"

	dim rsGruppeValg
	set cn = Server.Createobject("ADODB.Connection")
    cn.ConnectionTimeout = Application("Wap_Kunder_ConnectionTimeout")
    cn.CommandTimeout = Application("Wap_Kunder_CommandTimeout")
    cn.CursorLocation = adUseClient
	cn.ConnectionString = Application("wap_Kunder_ConnectionString")
	cn.Open 'Application("wap_Kunder_ConnectionString"), Application("Wap_Kunder_RuntimeUserName"), Application("Wap_Kunder_RuntimePassword")

    set rsCustomer = Server.Createobject("ADODB.Recordset")
    rsCustomer.CursorLocation = adUseClient
	rsCustomer.CursorType = adOpenKeyset
	rsCustomer.LockType = adLockBatchOptimistic

    Set rsCustomer.ActiveConnection = cn
	rsCustomer.Open "select * from Customer Where " & strFilter
    Set rsCustomer.ActiveConnection = Nothing

	if rsCustomer.EOF then
		if isNewOK then
			rsCustomer.AddNew
			rsCustomer.Fields(strField) = strFieldCheck
			rsCustomer.Update
		else
			OpenBase = false
			cn.Close
			exit Function
		end if
	end if

    set Session("cn") = cn

	' 2005.03.10:Roar Vestre: Added for SmsWapEmail:
	If rsCustomer.Fields("Mobilphone") & "" = "" Then
		rsCustomer.Fields("Mobilphone") = Session("Mobile")
	End If
	If rsCustomer.Fields("Epost") & "" = "" Then
		rsCustomer.Fields("Epost") = Session("Email")
	End If

	set Session("rsCustomer") = rsCustomer

    set rsGruppeValg = Server.Createobject("ADODB.Recordset")
    Set rsGruppeValg.ActiveConnection = cn
    rsGruppeValg.CursorLocation = adUseClient
    rsGruppeValg.CursorType = adOpenForwardOnly
	rsGruppeValg.Open "select * from GruppeValg order by SekvensNr, Ledetekst"
    Set rsGruppeValg.ActiveConnection = Nothing

	set Session("rsGruppeValg") = rsGruppeValg
    cn.Close
	OpenBase = True
End Function

Sub NewPassword
	if len(strMobile) <> 8 then
		Response.Buffer = true
		Response.Clear
		session("strFeil") = "Du m� oppgi et korrekt mobilnr!"
	    Response.Redirect "wap_login.asp"
	    'Response.Write "wap_login.asp?feil=Du m� oppgi et korrekt mobilnr!&mobile=" & strMobile
	end if

	Response.Expires = 0

	strPassword = FormatDateTime(Now, vbLongTime)
	strPassword = mid(strPassword, 7, 2)
	Randomize
	strPassword = FormatNumber(rnd(strPassword), 4, 0)
	strPassword = mid(strPassword, 2, 4)

	set rsCustomer = Session("rsCustomer")

	on error resume next
	rsCustomer.close
	on error goto 0

	rsCustomer.Open "Customer"

	with rsCustomer
		.filter = "Mobilphone='" & trim(strMobile) & "'"

		if not .eof then
			'Response.Write rsCustomer.Fields("Customer") & ": " & strMobile & ":" & strPassord
			rsCustomer.Fields("Password") = strPassword
			rsCustomer.Update
		else
			rsCustomer.AddNew
			rsCustomer.Fields("Mobilphone") = strMobile
			rsCustomer.Fields("Password") = strPassword
			rsCustomer.Update
			strMessage = "<p>Velkommen som ny bruker: " & strMobile & "</p>"
		end if
		'set Session("rsCustomer") = rsCustomer
	end with
	SendPassord strMobile, strPassword
End Sub

Sub SendPassord(strMobile, strPassword)
	set FSO = Server.Createobject("Scripting.FileSystemObject")
	strFile = SMS_PATH & "SM" & strMobile & "_" & strPassword & ".txt"
	set objPassFil = FSO.CreateTextFile(strFile)
	objPassFil.WriteLine "[SMS]"
	objPassFil.WriteLine "To=0047" & strMobile
	objPassFil.WriteLine "From=NTB Passord"
	objPassFil.WriteLine "Text=Passord for NTB SMS og WAP: " & strPassword
	objPassFil.close
end sub
%>
