<%@ Language=VBScript %>
<%
'***********************************************************************************
' wap_kunder/hovedform.asp
'
' This file is used for displaying adn editing SMS/ Wap information
'
' USES:
'		none
'
' NOTES:
'		none
'
' CREATED BY: Roar Vestre, NTB
' CREATED DATE: 2002.08.15
' REVISION HISTORY:
' Updated 2005.02.28 av Roar Vestre, lagt til felt for E-postvarsling
' Updated 2005.03.02 av Roar Vestre, ryddet opp i java-script, (fjernet "Alle-chekbox")
' Updated 2005.03.09 av Roar Vestre, Satt Sniff-s�k under stoffgrupper
'
'**********************************************************************************
Option Explicit
dim arrIndex
dim strForrigeGruppe
dim rsCustomer
dim rsGruppeValg
Dim bSpo, bOko, bHov, bOmr, bFyl, bKul, bKur
Dim strMobilphone, strLogonID, strEpost
Dim strWap, strSMS, strSendEpost

Dim style
if Session("Browser") = "ns" then
	style = "../include/ntb_ns.css"
else
	style = "../include/ntb.css"
end if

if isObject(rsCustomer) then
	Response.Buffer = true
	Response.Clear
	Response.Redirect "openbase.asp"
end if

set rsGruppeValg = Server.Createobject("ADODB.Recordset")
set rsCustomer = Server.Createobject("ADODB.Recordset")

'on error resume next

set rsGruppeValg = Session("rsGruppeValg")
set rsCustomer = Session("rsCustomer")
strMobilphone = rsCustomer.Fields("Mobilphone")
strLogonID = rsCustomer.Fields("LogonID")
strEpost = rsCustomer.Fields("Epost")

'on error goto 0

'if strMobilphone = "" AND strLogonID = "" then
'	Response.Buffer = true
'	Response.Clear
'	Response.Redirect "openbase.asp"
'end if


if rsCustomer.Fields("WAP") then
	strWAP = " CHECKED"
end if

if rsCustomer.Fields("SMS") then
	strSMS = " CHECKED"
end if

if rsCustomer.Fields("SendEpost") then
	strSendEpost = " CHECKED"
end if

%><HTML>
<HEAD>
<title>NTB WAP Personalisering</title>
<META http-equiv="expires" content="0">
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="<%=style%>">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="../include/overlib.js"></script>

<%
Response.Write "<script language=""javascript"">" & vbCrLf
Response.Write "<!--" & vbCrLf

%>
function HentVindu()
{
		//ResetAlle(intType);
		document.form1.save.value = false;
		document.form1.submit2.click();
}
<%

'2005.03.12: Roar Vestre: Fjernet Javascript for � sette kryss i alle kategorier ved valg av "Alle"
LagJavaAlle(2)
LagJavaAlle(3)
LagJavaAlle(4)
LagJavaAlle(5)
LagJavaAlle(6)
LagJavaAlle(8)

Sub LagJavaAlle(intType)
	Dim strCr, i, strFieldName, strFieldReset
	strCr = vbCrLf

	'2005.03.12: Roar Vestre: Fjernet Javascript for � sette kryss i alle kategorier ved valg av "Alle"
	'Response.Write "function AlleKategorier" & intType & "()" & strCr
	'Response.Write "{" & strCr
	With rsGruppeValg
		.Filter = "GruppeId=" & intType
		i = 0
		Do Until .EOF
			strFieldName = "chk_chk" & .Fields("Verdi") & intType
			if .Fields("SekvensNr") = 0 then
				strFieldReset = strFieldName
	'			Response.Write "	value = document.form1." & strFieldName & ".checked;" & strCr
	'		Else
	'			Response.Write "	document.form1." & strFieldName & ".checked = value;" & strCr
			end if
			.MoveNext
		Loop
	End With

	'select case intType
	'case 1, 2, 4
	'	Response.Write "HentVindu();" & strCr
	'end select
	'Response.Write "}"	 & strCr

	Response.Write "function ResetAlle" & intType & "()" & strCr
	Response.Write "{" & strCr
	Response.Write "	document.form1." & strFieldReset & ".checked = false;" & strCr
	Response.Write "}" & strCr
end sub

Response.Write "-->" & vbCrLf
Response.Write "</script>" & vbCrLf

%>
</HEAD>

<body Background="" bgcolor="" text=#000000 link=#000000 vlink=#000000 alink=#000000 leftmargin=10>
<FORM name=form1 METHOD=post action=Update.asp target="">

<table border="0" bgcolor_X="d4d0c8" width="100%">
	<tr>
	<td colspan=3>
	<font style="FONT-SIZE: 10pt" face=Tahoma color=#000000 size=3>
		SMS-varsling er en tilleggstjeneste som bare autoriserte brukere har tilgang til.
		Kontakt NTBs markedsavdeling for n�rmere informasjon:
		<!--<br>-->
		E-post: <a href="mailto:marked@ntb.no">marked@ntb.no</a> - Telefon: 22 03 45 09
	</font>
	</td>
	</tr>

	<tr>
	<td colspan=3>&nbsp;</td>
	</tr>

	<tr>
	<td valign="top" width='40%' onmouseover="return overlib('Jeg vil bli varslet med SMS om de viktigste hendelsene: (antallet kan variere fra ingen og opp til sju-�tte meldinger pr dag, avhengig av nyhetsbildet)', CAPTION, 'NTB-HAST');" onmouseout="return nd();">
		<%LagTabell 9, "BREAKING NEWS (NTB HAST)", "Prio13Stoffgr", 3%>
	</td>
	<td valign=top align=left width='105'>
		<font style="FONT-SIZE: 8pt" face=Tahoma color=#800000 size=2>
			<b>&nbsp;&nbsp;Mobilnr.:</b><br/><br/>
			<b>&nbsp;&nbsp;E-postadr.:</b>
		</font>
	</td>
	<td valign=top align=left>
		<font style="FONT-SIZE: 8pt" face=Tahoma color=#800000 size=2>
			<INPUT name=mobilphone id=mobilphone type="text" size="30" value=<%=strMobilphone%>><br/>
			<INPUT name=epost id=epost type="text" size="30" value=<%=strEpost%>>
			<br/><b><font color=red><%=Session("UpdateErr")%></font></b>
		</font>
	</td>

	<td valign=top align=right>
		<INPUT id=submit2 type=submit value='Lagre' name=submit2 onmouseover="return overlib('Lagre alle endringer i profilen', LEFT, CAPTION, 'Lagre');" onmouseout="return nd();">
	</td>
	</tr>
</table>

<%Session("UpdateErr") = ""%>

<!-- Table for WAP -->
<table border=1 borderColor="#61c7d7" width='99%'>
<tr><td style='border:none;'>

<table border=0 width=100%>
<tr>
<!--
<td onmouseover="return overlib('Hele artikkelen som WAP lagret for deg p� &quot;wap.ntb.no/<%=strMobilphone%>&quot; fra kategorier og s�k valgt nedenfor.', CAPTION, 'WAP');" onmouseout="return nd();">
<font style="FONT-SIZE: 8pt" face=Tahoma colorx=#800000 size=2>
<INPUT id=checkbox2 type=checkbox name=chk_wap value=true <%=strWAP%>>
Jeg �nsker meldingene lagret p� min personlige WAP-side
(SMS) Jeg �nsker i tillegg varsling om disse via SMS
</td>
-->
<td valign='top' onmouseover="return overlib('Det vil bli sendt SMS (f�rste 150 tegn av tittel og ingress) av ALLE meldinger med kategorier og s�k valgt nedenfor. (Velg med omhu, det blir lett veldig mange SMS-er om dagen!)', CAPTION, 'SMS');" onmouseout="return nd();">
<font style="FONT-SIZE: 8pt" face=Tahoma colorx=#800000 size=2>
<INPUT id=checkbox1 type=checkbox name=chk_sms value=true <%=strSMS%>>
Jeg �nsker varsling med SMS av f�lgende stoffgrupper og kategorier fra alle NTBs nyheter (velg nedenfor)
<br>Meldingene du f�r varsel om blir automatisk lagret p� din personlige wap-side, der du til enhver tid kan lese fulltekstversjonen av de 10 siste sakene i ditt personlige utvalg.
Wap-siden din finner du p�: wap.ntb.no/<%=strMobilphone%>
</td>

<td>&nbsp;</td>
<td valign='top' onmouseover="return overlib('Det vil bli sendt fulltekst nyhetsmelding som E-post i HTML-format', CAPTION, 'E-post');" onmouseout="return nd();">
<font style="FONT-SIZE: 8pt" face=Tahoma colorx=#800000 size=2>
<INPUT id=checkbox2 type=checkbox name=chk_epost value=true <%=strSendEpost%>>
Jeg �nsker tilsendt fulltekst nyhetsmelding som E-post av f�lgende stoffgrupper og kategorier fra alle NTBs nyheter (velg nedenfor)
</td>
</tr>
</font>
</table>

<table border=0 width="40%">
<tr>
<td valign="top">
<div onmouseover="return overlib('Det blir varslet om nye meldinger innenfor utvalgte kategorier. NB! Underkategorier, omr�der eller fylker m� velges for at meldingene skal bli sendt (alternativene blir tilgjengelige etterhvert som du klikker deg gjennom valgmulighetene)', CAPTION, 'Kategorier');" onmouseout="return nd();">
<%
'*** Stoffgruppe (Alle Nyheter)
	LagTabell 1, "<b>VELG STOFFGRUPPE</b>", "NTBStoffgrupper", 3
%>
</div>
</td>
</tr>
</table>

<table border=0 width="100%">
	<tr>
	<td valign="top">
		<font style="FONT-SIZE: 8pt" face=Tahoma color=#800000 size=2>
		<b>S�keord: </b>
		</font>
	    <input id=textarea1 style="width: 100%" cols=100 name=textarea1 value="<%=replace(rsCustomer.Fields("NTBSniffertxt") & "","""", "&quot;")%>" onmouseover="return overlib('Tast inn ett eller flere s�keord/begreper adskilt med semikolon. S�ket vil omfatte samtlige meldinger der dine s�keord inng�r - uavhengig av hvilke kategorier du har valgt nedenfor, men kun innenfor valgte stoffgruppe!', CAPTION, 'S�keord');" onmouseout="return nd();"></input>
	    <!--<TEXTAREA id=textarea1 style="width: 100% HEIGHT: 35px" cols=50 name=textarea1><%=rsCustomer.Fields("NTBSniffertxt")%></TEXTAREA>-->
	</td>
	</tr>
</table>

<!-- Hovedtabell: -->
<table border="0" width="100%">
<tr><td colspan=2 style='border:none;'></td></tr>
<tr><td width='25%' valign="top">

<%
'*** Velg Omr�der
If bOmr Then
	LagTabell 2, "<b>Velg omr�de(r)</b>", "NTBOmraader", 2
End If

'*** Fylker i norge
If  bFyl Then
	LagTabell 3, "Velg fylker i Norge", "NTBFylker", 2
End If

%>
</td>
<td width="25%" valign="top">
<%

'*** Hovedkategori
If bHov Then
    LagTabell 4, "<b>Velg hovedkategorier</b>", "NTBHovedKategorier", 2
End If

'*** �konomi
If bOko Then
	LagTabell 6, "Velg underkategorier: �konomi", "NTBUnderKatOkonomi", 2
End If

'*** Kultur
If bKul Then
	LagTabell 8, "Velg underkategorier: Kultur", "NTBUnderKatKultur", 2
End If

'*** Kuriosa
If bKur Then
    LagTabell 7, "Velg underkategorier: Kuriosa", "NTBUnderKatKuriosa", 2
End If

%>
</td>
<td width="33%" valign="top">
<%

'*** Sport
If bSpo Then
	LagTabell 5, "Velg underkategorier: Sport", "NTBUnderKatSport", 3
End If

%>

</td>
</TR>
</TABLE>

</td>
</TR>
</TABLE>

<table cellspacing="1" cellpadding="1">
	<tr><td height="1"></td></tr>
</table>

<table border=1 borderColor="#61c7d7" width='99%'>
<tr><td style='border:none;'>
<font style="FONT-SIZE: 10pt" face=Tahoma color=#800000 size=3>
NTB P� WAP-TELEFON OG PDA:
</font>
<br>
<font style="FONT-SIZE: 10pt" face=Tahoma color=#000000 size=3>
Brukere av NTBs portal kan lese og s�ke i NTB-nyhetene p�: wap.ntb.no/hent
Her finner du b�de den komplette tjenesten og kortversjonen "NTB Direkte", som gir en oversikt over d�gnets viktigste nyheter.
</font>
</td>
</tr>
</table>

<!--End Custom Fields--><!-- **** COMMON FIELDS SUBMITTED ON ALL FORMS **** -->

<input type=hidden value=0 name=save>

<!--
	<input type=hidden value=custom name=tab>
	<input type=hidden value=send name=command>
	<input type=hidden name=objID>
	<input type=hidden value=1 name=savecopy>
	<input type=hidden value=1 name=importance>
	<input type=hidden value=0 name=replytofolder>
-->

</FORM>

</body>
</HTML>

<%
Sub LagTabell(intType, strTittel, strNtbCol, intCol)
	Dim intAntall, i, strPopUp, i1, i2, strChecked, strFieldName

%>
  <!--<table bgcolor=d4d0c8 border=1 width='100%'>-->
  <!--<table border=1 width='100%'>-->
	<table borderColor="#61c7d7" width='100%' cellSpacing="0" cellPadding="0" border="1">

	<tr><td style='border:none;'>
		<font style="FONT-SIZE: 8pt" face="Comic Sans MS" color=#000000 size=1>
		<b>&nbsp;<%=strTittel%></b>
		</FONT>
	</td></tr>
	<tr><td style='border:none;'>

	<table border='0' width='100%' cellpadding='0' cellspacing='0'>
	<tr><td valign='top' width='50%'>

	<%
	with rsGruppeValg
		.Filter = "GruppeId=" & intType
		'.Filter = "GruppeId=" & intType & " AND SekvensNr>0"
		intAntall = .RecordCount
		i = 0
		do until .EOF

	        'Response.Write "<td valign=top width='50%'>"

	        '*** Endret 09.10.2001:
	        if .Fields("PopUp") <> "" Then
				'strPopUp = " LANGUAGE=javascript onclick='ResetAlle" & intType & "()'"
				strPopUp = " LANGUAGE=javascript onclick='HentVindu()'"
				i1 = "<b><i>"
				i2 = "</i></b>"
			elseif intType = 9 then
				strPopUp = ""
				i1 = ""
				i2 = ""
			else
				'strPopUp = ""
				strPopUp = " LANGUAGE=javascript onclick='ResetAlle" & intType & "()'"
				i1 = ""
				i2 = ""
	        end if

			if rsCustomer.fields(strNtbCol) <> "" then
				if instr(1, rsCustomer.fields(strNtbCol), .fields("Verdi")) <> 0 then
					strChecked = " CHECKED"

					if intType <> 9 then
						'Set active subcategory on these:
						select case .fields("Verdi")
						case "Sport"
							bSpo=true
						case "Okonomi"
							bOko=true
						case "Innenriks"
							bHov=true
							bOmr=true
							'bFyl=true
						case "Utenriks"
							bHov=true
							bOmr=true
						case "Norge"
							bFyl=true
						case "Kultur"
							bKul=true
						case "Kuriosa"
							'bKur=true
						end select
					end if
				else
					strChecked = ""
				end if
			else
				strChecked = ""
			end if

	        strFieldName = "chk_chk" & .Fields("Verdi") & intType
	        if .Fields("SekvensNr") = 0 then
				'2005.03.12: Roar Vestre: Fjernet Javascript for � sette kryss i alle kategorier ved valg av "Alle"
				'strPopUp = " LANGUAGE=javascript onclick='AlleKategorier" & intType & "()'"
				strPopUp = ""
			else
				'strPopUp = ""
			end if

			Response.Write "<INPUT type=checkbox value='" & .Fields("Verdi") & "' name='" & strFieldName & "'" & strPopUp & strChecked & ">"
	        Response.Write "<font style='FONT-SIZE: 8pt' face=Tahoma color=#000000 size=2>"

	        if .Fields("SekvensNr") = 0 then
		        Response.Write "<b>" & .Fields("Ledetekst") & "</b>"
	        else
		        Response.Write i1 & .Fields("Ledetekst") & i2
	        end if

	        'Response.Write "</font><INPUT type=hidden value=0 name=chk" & .Fields("Verdi") & "><td>" & vbCrLf
	        Response.Write "</font>"
			i = i + 1
			if i mod (intAntall + 0.5) / intCol = 0 then
				Response.Write "</td><td valign='top' width='50%'>" & vbCrLf
			else
		        Response.Write "<br/>" & vbCrLf
			end if

			.MoveNext
		loop
	End With%>
	</td></tr>
	</table>
  </td></tr>
  </table>
	<table cellspacing="1" cellpadding="1">
		<tr><td height="1"></td></tr>
	</table>
<%
End Sub
%>