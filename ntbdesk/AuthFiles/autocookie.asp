<%@ LANGUAGE="VBSCRIPT" %>
<%Response.Buffer=True%>

<%
REM sample file for using with AuthFilter's AutoCookie-mode
REM This file handles generating GUID and then setting the ProfileTicket
REM To be used for keeping track of Anonymous customers
%>

<%
	dim strUsername, strSiteName, strRetAsp, strPersistentCookieCheck
	dim objAuth, objGenID
	
	' we use a temp cookie 'cpcs' (check persist cookie support): you can update this temp cookie to any name you want
	strPersistentCookieCheck = Request.Cookies("cpcs")
	if (strPersistentCookieCheck = "" or IsNull(strPersistentCookieCheck)) Then		'	check persist cookie support
		set objAuth = Server.CreateObject("Commerce.AuthManager")
	
		strSiteName = CStr(Application("MSCSCommerceSiteName"))
		objAuth.Initialize(strSiteName)
	
		if (strUsername = "" or IsNull(strUsername)) then
			set objGenID = Server.CreateObject("Commerce.GenID")
			strUsername = objGenID.GenGUIDString
		End if
					
		on Error Resume next
		objAuth.SetProfileTicket strUsername, True
		Response.Cookies("cpcs") = "1"	'	set: persist-cookie support flag
		
		strRetAsp = Request.Cookies("MSCSFirstRequestedURL")
		if (strRetAsp = "" Or IsNull(strRetAsp)) then
			Response.Redirect "error.asp"
		else
			Response.Redirect strRetAsp
		end if
	else
		Response.Redirect "nocookie.asp"
	End If
%>