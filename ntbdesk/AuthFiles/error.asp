<%@ LANGUAGE="VBSCRIPT" %>
<%Response.Buffer=True%>

<%
REM sample file for using with AuthFilter
REM redirected to this file, for handling known Errors
REM Error would be added to QS parameter 'Err' and it could be used as explained
%>

<HTML>

<HEAD>
    <TITLE>Auth-User-Content</TITLE>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
</HEAD>

<BODY BGCOLOR=lightblue>
<br>
<br>
<H2 id=L_ErrorMessage_Text> An error occurred! </H2>
<br>
<%

CONST	L_UnauthorizedLogon_ErrorMessage = "Unauthorized Logon: Access Denied for the current user & most probably you have reached Max. number of Login-Retries."
CONST	L_LogonFailed_ErrorMessage = "Unauthorized: Logon Failed"
CONST	L_ACLOnRessource_ErrorMessage = "Access denied by ACL on resource"
CONST	L_FilterAuthorizationDenied_ErrorMessage = "Unauthorized: Authorization denied by filter Internet Information Services"
CONST	L_ISAPIAuthFailure_ErrorMessage = "Unauthorized: Authorization by ISAPI or CGI application/resource failed Internet Information Services"
CONST	L_CheckUserPassword_ErrorMessage = "ERROR OCCURRED in Authenticating the user (check UserID/Password)"
CONST	L_UserAuthentication_ErrorMessage = "ERROR(s) OCCURRED in Authenticating the user"
CONST	L_SelectALink_Message = "Select one of the following Links:"


	Dim fIsAuth
	Dim retAsp
	Dim strAuthErr, strUrl
	
	strAuthErr = Request.QueryString("Err")
	If Not (strAuthErr = "" Or IsNull(strAuthErr)) then
		Select Case  strAuthErr 
			Case "401.1"
				Response.Write ("<BR>"&"<H3>"&L_UnauthorizedLogon_ErrorMessage&"</H3>"&"<BR>")
			Case "401.2"
				Response.Write ("<BR>"&"<H3>"&L_LogonFailed_ErrorMessage&"</H3>"&"<BR>")
			Case "401.3"
				Response.Write ("<BR>"&"<H3>"&L_ACLOnRessource_ErrorMessage&"</H3>"&"<BR>")
			Case "401.4"
				Response.Write ("<BR>"&"<H3>"&L_FilterAuthorizationDenied_ErrorMessage&"</H3>"&"<BR>")
			Case "401.5"
				Response.Write ("<BR>"&"<H3>"&L_ISAPIAuthFailure_ErrorMessage&"</H3>"&"<BR>")
			Case Else
				Response.Write ("<BR>"&"<H3>"&L_CheckUserPassword_ErrorMessage&"</H3>"&"<BR>")
		End Select		
	else
		Response.Write ("<BR>"&"<H3>"&L_UserAuthentication_ErrorMessage&"</H3>"&"<BR>")
	End if
	'strUrl = Request.Cookies("MSCSFirstRequestedURL")
	'Response.Write ("<BR>"&"<H3>"&"requested url: " & strUrl & "</H3>" & "<BR>")

	Response.Write ("<BR>"&"<H1>"&L_SelectALink_Message&"</H1>"&"<BR>")
%>
<br>
<H1>
<A HRef="login.asp" ID=L_LoginIfRegistered_Text>Login if already registered...</A>
</H1>
<br>

</BODY>
</HTML>