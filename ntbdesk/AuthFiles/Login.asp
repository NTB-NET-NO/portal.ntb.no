<%
REM Microsoft Commerce Server 2000
REM sample login-file for using with AuthFilter
REM This file handles Login for user
%>

<%
	Dim strSelect, strPassword, strPWD, strAuthErr, strSiteName, strUserID, strRetAsp, strGUID
	Dim objAuth, objMSCSProfileService, objMSCSProfileObj

	set objAuth = Server.CreateObject("Commerce.AuthManager")
	strSiteName = CStr(Application("MSCSCommerceSiteName"))		'Get siteName, set in Global.asa in appication scope
	objAuth.Initialize(strSiteName)
	
	'check for Submit or not	
	strSelect = Request.QueryString("realSubmit")
	
	'If users pressed the submit button
	if strSelect = "fromButton" then
		strUserID	= Request.QueryString("txtUsername")		' Get UserName from QueryString if this is GET request, this could be POST request also
		strPassword	= Request.QueryString("txtPassword")		' Get Password from QueryString if this is GET request, this could be POST request also
		
		if (strUserID = "") or (strPassword = "") Or IsNull(strUserID) Or IsNull(strPassword)  then
			Response.Redirect "Login.asp"
		end If
		
		' Get User-password: comement-out following line if you support ProfileSvc
		'strPWD = GetCurrentUserPassword(strUserID)
		
		' if profileSvc is not used for BlankSite:
		strPWD = strPassword	' remove this line if you have read the password from UserProfileSvc or some other obj/src, in clear text
		
		if (strPWD = strPassword) then	' if passwords are equal, not necessary in Windows-Auth-mode
			objAuth.SetAuthTicket strUserID, True, 90				' set AuthTicket
				
			strRetAsp = Request.Cookies("MSCSFirstRequestedURL")	' First requested URL (even if there is no QueryString this URL ciontains '?' at the end
			strRetAsp = strRetAsp + "&proxyuser="					' QS-separator '?' is added by filter, in case of no Querystring
			strRetAsp = strRetAsp + strUserID						' userID submitted : "DomainName\LoginID"
			strRetAsp = strRetAsp + "&proxypwd="					' UPDATE_NEEDED for password (may need to change it to: 'strPwd')
			strRetAsp = strRetAsp + strPassword
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' Distributed-Denial-Of-Service Attack (DDoS)
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' this is to avoid DDos Attacks with known User login ID
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' Set objGenID =  Server.CreateObject("Commerce.GenID") '$PERF: store one in Application scope in GLOBAL.ASA, Application("MSCSAuthGenID")
			' strGUID = objGenID.GenGUIDString
			' 
			' objAuth.SetProperty 2, "guid", strGUID ' after setting Ticket
			' strRetAsp = strRetAsp + "&guid="
			' strRetAsp = strRetAsp + strGUID
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				
			' Go to the Original requested ASP which is stored in cookie "MSCSFirstRequestedURL" Or Default page
			if ((strRetAsp = "") Or IsNUll(strRetAsp)) then
				strRetAsp = strSiteName & "/default.asp"
				Response.Redirect strRetAsp
			end if			
			Response.Redirect strRetAsp
		else
			Response.Redirect "Login.asp"	' Incorrect password & redirect back to Login page
		end if
	else
		' $WEB_FARM scenario: Logging onto a new server in WebFarm Or FT/FailOver scenario
		if objAuth.IsAuthenticated(30) Then		' for Web-Farm scenario <valid-Auth-Ticket Exist, but not cached in Filter>
			Dim strProfileUserID				' in case, if you are using UserProfileSvc

			strUserID = objAuth.GetUserID(2)	' Get LoginID <only in case of AD-Site>, from AuthTicket
			if (strUserID = "") or (IsNull(strUserID)) then
				Response.Redirect "Login.asp"
			end If

			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' $PASSWORD: start 
			'	To get Clear-Text-Password:
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
			' Get User-password: comement-out following line if you support ProfileSvc
			'strPassword = GetCurrentUserPassword(strUserID)

			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' End Of getting Clear-Text password
			' $PASSWORD: End
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
			strRetAsp = Request.Cookies("MSCSFirstRequestedURL") ' get the requested URL
			strRetAsp = strRetAsp + "&proxyuser="
			strRetAsp = strRetAsp + strUserID
			strRetAsp = strRetAsp + "&proxypwd="
			strRetAsp = strRetAsp + strPassword
			
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' Distributed-Denial-Of-Service Attack (DDoS)
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' this is to avoid DDos Attacks with known User login ID
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' strGUID = objAuth.GetProperty(2, "guid")	' if this exists, you need to pass this also on Query string
			' 
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' If Not IsNull(strGUID) Then
			' 	strRetAsp = strRetAsp + "&guid="
			' 	strRetAsp = strRetAsp + strGUID
			' End If
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

			Response.Redirect strRetAsp
		Else	' $FIRST_TIME_LOGIN: First time logging on to the site/web-farm scenario
			PrintLogin
		End If	
	End if
	
	Set objAuth = Nothing
%>

<%
' GetCurrentUserPassword -- wrapper function for getting a user profile/pwd...
Function GetCurrentUserPassword(ByVal strUserID)
	Dim strPWD
	Dim objMSCSProfileService, objMSCSProfileObj
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' $PASSWORD: start 
	'	To get Clear-Text-Password:
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'get the Login name from Domain\LoginName format: in case of Windows-Auth mode
	' strProfileUser = split(strUserID, "\", -1, 1)
	' strProfileUserID = strProfileUser(1)

	' Get Profile Service stored in Application-Scope
	Set objMSCSProfileService = Application("MSCSProfileService")

	' Get UserProfileObj for the user already Logged in (webFarm)
	Set objMSCSProfileObj = objMSCSProfileService.GetProfile(strUserID, "UserObject")	' GetUserProfileByLoginName(strUsername)
	if (objMSCSProfileObj is nothing) then
		Response.Redirect "Login.asp"
	end if

	' if password-available: in clear-text
	strPWD = objMSCSProfileObj.Fields.Item("GeneralInfo").Value.Item("user_security_password") ' objMSCSProfileObj.Fields("GeneralInfo.user_security_password").Value
	Set objMSCSProfileObj = Nothing
	GetCurrentUserPassword = strPWD
End Function
%>

<%Sub PrintLogin() %>
<HTML>
<HEAD>
<TITLE>Login</TITLE>
</HEAD>
<BODY>
<FORM NAME="frmLogin" ACTION="Login.asp" METHOD="GET">
<br>
<br>
<br>
<H2 ID=L_LoginForm_HTMLText>CS2K-LoginForm</H2><ID Id=L_EnterCredential_ErrorMessage>
To access authenticated content, please enter your UserID & Password</ID>
<br>
<br>
<br>

<H3 ID=L_UserName_HTMLText>Username:<INPUT TYPE="text" NAME="txtUsername" SIZE=32 MAXLENGTH=32><br><ID ID=L_UserPassword_HTMLText>
Password :</ID><INPUT TYPE="password" NAME="txtPassword" SIZE=32 MAXLENGTH=32></H3><br>
<br>

<INPUT type=HIDDEN name="realSubmit" value="fromButton">
<p align="left">
	<input type="submit" name="action" id=L_Submit_Button value="Submit">	
	<input type="reset" name="action" id=L_Reset_Button value="Reset"> 
</p>
</FORM>

<H4>
<br>
<br>
<%
REM SOLUTION SITES: Retail
REM 	need to add own registration file under '\AuthFiles\' sub-Dir Or Copy ..\Retail\login\newuser.asp to '\AuthFiles\newuser.asp'
REM 	in global.asa update: 	dictPages.NewUser = "AuthFiles/newuser.asp"
REM		You can update this to POST, instead of default GET
%>
<A HRef="newuser.asp" ID=L_RegisterIf_HTMLText>Register if you are a new user	(solution sites: need to add own registration file under '\AuthFiles\' sub-Dir Or Copy ..\Retail\login\newuser.asp & update NewUser-File in Global.asa)</A>
<br>
</H4>

</BODY>
</HTML>
<%end sub%>