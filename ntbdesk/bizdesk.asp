<!--#INCLUDE FILE='include/BDHeader.asp' -->
<%
dim g_MSCSEnv, g_MSCSErrorInGlobalASA, VERBOSE_OUTPUT, SHOW_OBJECT_ERRORS, _
	FORCE_HTA_ONLY, ALLOW_CONTEXT_MENUS, g_sConfigXML

Const L_NavigationTreeWidth_Number = 164	'starting width of left nav tree frame
Const PRODUCTION  = 1
Const DEVELOPMENT = 0

g_MSCSEnv = Application("MSCSEnv")
' -- turn off errors if in production mode
if g_MSCSEnv = PRODUCTION then on error resume next
g_MSCSErrorInGlobalASA = Application("MSCSErrorInGlobalASA")
%>
<HTML>
<HEAD>
	<STYLE>
		BODY
		{
			MARGIN-TOP: 10px;
			MARGIN-LEFT: 10px;
			BACKGROUND-COLOR: white;
			cursor: default
		}
		TD
		{
			VERTICAL-ALIGN: top
		}
		#bdicon
		{
			BACKGROUND-COLOR: black
		}
		#bderror
		{
			FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 12pt;
			FONT-WEIGHT: bold;
			COLOR: red
		}
	</STYLE>
</HEAD>
<!--#INCLUDE FILE='include/BizDeskObjects.asp' -->
<%
' -- error occurred in global.asa application_onstart execution
if g_MSCSErrorInGlobalASA then AbortWithTerminalError(L_ErrorsInAppLoad_ErrorMessage)
%>
<!--#INCLUDE FILE='include/BizDeskPaths.asp' -->
<!--#INCLUDE FILE='include/BizDeskStrings.asp' -->
<!--#INCLUDE FILE='include/ASPUtil.asp' -->
<%
' -- BizDesk object error output control from application
VERBOSE_OUTPUT		= CInt(Application("VERBOSE_OUTPUT"))
SHOW_OBJECT_ERRORS	= CInt(Application("SHOW_OBJECT_ERRORS"))
FORCE_HTA_ONLY		= CInt(Application("FORCE_HTA_ONLY"))
ALLOW_CONTEXT_MENUS	= CInt(Application("ALLOW_CONTEXT_MENUS"))
' -- load objects onto Application if necessary
g_sConfigXML = g_sBDConfigPath & "bizdesk.xml"

' -- if nav dictionary and actions dictionary not in application then load them from XML config
if isEmpty(Application("MSCSNav")) or isEmpty(Application("MSCSActionContainer")) then
	LoadObjects()
end if
' -- if still not in application then display error:
if isEmpty(Application("MSCSNav")) or isEmpty(Application("MSCSActionContainer")) then
	AbortWithTerminalError(L_UnableToLoadBizDeskXML_ErrorMessage)
end if

dim aUserAgent, sUserAgent
aUserAgent = split(Request.ServerVariables("HTTP_USER_AGENT"), ";")	'e.g., Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)
sUserAgent = trim(aUserAgent(1))
' aUserAgent(1) equals something like "MSIE 5.5"
if inStr(aUserAgent(1), "MSIE") = 0 then
	AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
else
	aUserAgent = split(sUserAgent, " ")	'separate MSIE from version
	sUserAgent = trim(aUserAgent(1))
	aUserAgent = split(sUserAgent, ".")	'separate version major from minor
	'aUserAgent(0) is the major version and aUserAgent(1) is the minor version
	if aUserAgent(0) < 5 then
		AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
	elseif aUserAgent(0) = 5 then
		if not isNumeric(aUserAgent(1)) then
			'just in case there is a 5.5b or something
			AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
		elseif aUserAgent(1) < 5 then
			AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
		end if
	end if
end if

sub AbortWithTerminalError(sErrorMsg)
%>
	<BODY>
		<TABLE>
		<TR>
			<TD ID='bdicon'><IMG SRC='assets/bdlogo.gif' HEIGHT='55' WIDTH='164' BORDER='0'></TD>
			<TD>&nbsp</TD>
			<TD ID="bderror"><%= sErrorMsg %></TD>
		</TR>
		</TABLE>
	</BODY>
	</HTML>
<%
	Response.end
end sub
%>
<SCRIPT LANGUAGE='VBScript'>
<!--
	Option Explicit

	dim g_elSelection, g_elMenuDown, winHelpWindow, g_dTemp, _
		g_aPageStack(3), g_elFrameset, g_elFrames, g_bEdit, g_bNestedEdit, _
		g_bCloseAfterSave, g_fnCallback, g_bInApplication, g_nNavWidth

	<% if g_MSCSEnv = PRODUCTION then %>on error resume next<% end if %>

	' -- error codes
	const ERR_OBJ_DOSNT_SUPPORT_METHOD = 438
	' -- BizDesk keycodes for filtering:
	const L_KeyCodeShiftedCopy_Number	= 67	'shifted key code for copy: C
	const L_KeyCodeCopy_Number			= 86	'key code for cut: c
	const L_KeyCodeShiftedUndo_Number	= 87	'shifted key code for undo: U
	const L_KeyCodeUndo_Number			= 98	'key code for undo: u
	const L_KeyCodeShiftedPaste_Number	= 88	'shifted key code for paste: V
	const L_KeyCodePaste_Number			= 99	'key code for paste: v
	const L_KeyCodeShiftedCut_Number	= 118	'shifted key code for cut: X
	const L_KeyCodeCut_Number			= 120	'key code for cut: x
	const KEYCODE_ESC		= 27
	const KEYCODE_BACKSPACE	= 8
	const KEYCODE_ENTER		= 13
	const KEYCODE_LEFT		= 37
	const KEYCODE_UP		= 38
	const KEYCODE_RIGHT		= 39
	const KEYCODE_DOWN		= 40
	const KEYCODE_TAB		= 9
	const KEYCODE_F1 = 112
	const KEYCODE_F2 = 113
	const KEYCODE_F3 = 114
	const KEYCODE_F4 = 115
	const KEYCODE_F5 = 116
	const KEYCODE_F6 = 117
	const KEYCODE_F7 = 118
	const KEYCODE_F8 = 119
	const KEYCODE_F9 = 120
	const KEYCODE_F10 = 121
	const KEYCODE_F11 = 122
	const KEYCODE_F12 = 123

	'initialize page stack
	g_aPageStack(0) = "*"
	g_aPageStack(1) = "0"
	g_aPageStack(2) = "0"
	g_aPageStack(3) = "0"

	g_nNavWidth = <%= L_NavigationTreeWidth_Number %>	'starting width of left nav tree frame

	set g_elSelection = nothing		'currently selected menu item
	set g_elMenuDown = nothing		'current menu displayed

	g_bEdit = false					'when set to true then display as edit page
	g_bNestedEdit = false			'when set to true then nested edit pages displayed
	g_bCloseAfterSave = false		'when set to true then need to close as long as no save errors
	g_fnCallback = null				'callback function to execute when close nested edit page

	' -- dictionary for temporary client storage
	set g_dTemp = CreateObject("Scripting.Dictionary")

	' -- test for HTA application object
	g_bInApplication = cbool(LCase(Right(top.location, 3)) = "hta")
	if not g_bInApplication and <%= CInt(FORCE_HTA_ONLY) %> then
		' -- not running in HTA mode - show error
		document.write "<BODY><TABLE><TR>"
		document.write "	<TD ID='bdicon'><IMG SRC='assets/bdlogo.gif' HEIGHT='55' WIDTH='164' BORDER='0'></TD>"
		document.write "	<TD>&nbsp</TD>"
		document.write "	<TD ID='bderror'><%= L_MustInstallClient_ErrorMessage %></TD>"
		document.write "</TR></TABLE></BODY>"
		document.close()
	end if

	' -- set title and display frames or error
	top.document.title = "<%= sFormatString(L_BizDeskMicrosoftCommerceServer_HTMLTitle, array(g_MSCSSiteName, UCase(Request.serverVariables("SERVER_NAME")), Request.serverVariables("SERVER_PORT"))) %>"

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' task button routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse move handler for task button menu items
	sub menu_onMouseMove(elSource)
		if inStr(elSource.parentElement.className, "bdtaskmenu") then
			RemoveSelection()
			SetSelection(elSource)
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse over handler for task buttons
	sub task_onMouseOver(oWindow, evt)
		dim elSource, elMenu, elMenuItem
		set elSource = evt.srcElement
		if elSource.tagName <> "BUTTON" then set elSource = elSource.parentElement
		elSource.className = elSource.className & " bdtaskover"
		set elMenu = oWindow.document.all(elSource.id & "menu")
		if not g_elMenuDown is nothing and not g_elMenuDown is elMenu then
			CloseMenu(oWindow)
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse out handler for task buttons
	sub task_onMouseOut(oWindow, elSource)
		dim elMenu
		if elSource.tagName <> "BUTTON" then set elSource = elSource.parentElement
		set elMenu = oWindow.document.all(elSource.id & "menu")
		if elMenu is nothing or not g_elMenuDown is elMenu then
			elSource.className = replace(elSource.className, " bdtaskover", "")
			elSource.className = replace(elSource.className, " bdtaskpress", "")
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse down handler for task buttons
	sub task_onMouseDown(oWindow, elSource)
		dim elMenu
		if elSource.tagName <> "BUTTON" then set elSource = elSource.parentElement
		elSource.className = replace(elSource.className, " bdtaskover", "")
		elSource.className = elSource.className & " bdtaskpress"
		set elMenu = oWindow.document.all(elSource.id & "menu")
		if elSource.getAttribute("type") = "menu" and not elMenu is nothing then
			OpenMenu oWindow, elSource, elMenu
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	selects a task button menu item
	sub SetSelection(elNewSelection)
		if inStr(elNewSelection.className, "bdtaskdisabled") = 0 then
			set g_elSelection = elNewSelection
			g_elSelection.className = g_elSelection.className & " selected"
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	unselects a task button menu item
	sub removeSelection()
		if not g_elSelection is nothing then
			g_elSelection.className = replace(g_elSelection.className, " selected", "")
			set g_elSelection = nothing
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	closes open task button menu
	sub OpenMenu(oWindow, elSource, elMenu)
		dim elMenuItem
		if not g_elMenuDown is nothing then CloseMenu(oWindow)
		if not oWindow.document.all("bdfindbycontent") is nothing then
			oWindow.document.all("bdfindbycontent").style.zIndex = -1
		end if
		elMenu.style.left = (elSource.offsetLeft + 3) & "px"
		elMenu.style.display = "block"
		set g_elMenuDown = elMenu
		for each elMenuItem in elMenu.children
			if inStr(elMenuItem.className, "bdtaskdisabled") = 0 then
				SetSelection(elMenuItem)
				elMenuItem.focus()
				exit for
			end if
		next
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	closes open task button menu
	sub CloseMenu(oWindow)
		dim elBtn, elTaskItem
		on error resume next
		if not g_elMenuDown is nothing then
			if not oWindow.document.all("bdfindbycontent") is nothing then
				oWindow.document.all("bdfindbycontent").style.zIndex = 0
			end if
			set elTaskItem = g_elSelection
			removeSelection()
			set elBtn = oWindow.document.all(left(g_elMenuDown.id, len(g_elMenuDown.id) - 4))
			if not elBtn is nothing then
				elBtn.className = replace(elBtn.className, " bdtaskover", "")
				elBtn.className = replace(elBtn.className, " bdtaskpress", "")
			end if
			g_elMenuDown.style.display = "none"
			set g_elMenuDown = nothing
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	key down handler for help task button to treat enter like click
	sub task_onKeyDown(oWindow, evt)
		dim elSource, elMenuItem
		set elSource = evt.srcElement
		if elSource.tagName <> "BUTTON" then set elSource = elSource.parentElement
		if evt.keyCode = KEYCODE_ENTER then
			' -- enter same as click item
			if elSource.getAttribute("type") = "menu" then
				OpenMenu oWindow, elSource, oWindow.document.all(elSource.id & "menu")
			else
				elSource.click()
			end if
			evt.returnValue = false
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	key down handler for help task menu to handle enter and arrows
	sub menu_onKeyDown(oWindow, evt)
		dim elSource
		set elSource = evt.srcElement
		if elSource.parentElement.getAttribute("type") <> "menu" then exit sub
		select case evt.keyCode
			case KEYCODE_ENTER
				' -- enter same as click item
				elSource.click()
				evt.returnValue = false
			case KEYCODE_TAB
				' -- leave menu
				elSource.parentElement.previousSibling.focus()
				CloseMenu(oWindow)
			case KEYCODE_UP
				' -- up sets previous sibling
				do while not elSource.previousSibling is nothing
					set elSource = elSource.previousSibling
					if inStr(elSource.className, "bdtaskdisabled") = 0 then
						removeSelection()
						SetSelection(elSource)
						elSource.focus()
						exit do
					end if
				loop
				evt.returnValue = false
			case KEYCODE_DOWN
				' -- down sets next sibling
				do while not elSource.nextSibling is nothing
					set elSource = elSource.nextSibling
					if inStr(elSource.className, "bdtaskdisabled") = 0 then
						removeSelection()
						SetSelection(elSource)
						elSource.focus()
						exit do
					end if
				loop
				evt.returnValue = false
			case KEYCODE_ESC
				CloseMenu(oWindow)
				evt.returnValue = false
		end select
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' framework task routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes a task button's postto or goto
	sub DoTask(oWindow, elSource)
		dim sForm
		set g_elMenuDown = nothing
		sForm = elSource.getAttribute("form")
		if not isNull(sForm) then
			PosttoAction oWindow, elSource.id, oWindow.document.all(sForm)
		else
			GotoAction oWindow, elSource.id
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes a task button's goto
	sub GotoAction(oWindow, sHREF)
		set g_elMenuDown = nothing
		oWindow.enableAllTasks(false)
		if g_bNestedEdit then
			CloseEditPage()
		else
			oWindow.document.location.replace(sHREF)
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes a task button's postto
	sub PosttoAction(oWindow, sHREF, elForm)
		dim sOnTask
		set g_elMenuDown = nothing
		oWindow.enableAllTasks(false)
		elForm.action = sHREF
		elForm.method = "POST"
		elForm.target = ""
		sOnTask = elForm.getAttribute("ONTASK")
		if not isNull(sOnTask) then 
			oWindow.executeTask(sOnTask)
		else
			if g_bNestedEdit then
				CloseEditPage()
			else
				elForm.submit()
			end if
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	sets edit mode display for title and status bars and
	'						hides left navigation tree menu
	sub SetEditMode(bSetEdit)
		dim frameSet, leftFrame
		if g_bNestedEdit then exit sub
		set frameSet = document.all.tags("FRAMESET")(0)
		set leftFrame = document.all.tags("FRAME")(0)
		g_bEdit = bSetEdit
		if g_bEdit then
			if window.frames(0).document.body.clientWidth > 5 then
				g_nNavWidth = window.frames(0).document.body.clientWidth
			end if
			frameSet.cols = "0,*,0,0,0"
			leftFrame.noresize = true
			leftFrame.tabindex = -1
		else
			frameSet.cols = g_nNavWidth & ",*,0,0,0"
			leftFrame.noresize = false
			leftFrame.tabindex = 0
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	OpenEditPage(sPage, fnCallback)
	'		opens a new nested edit page from the current edit page; when the nested 
	'		edit page is closed then the callback function will be called
	'
	'	Arguments:	sPage		String. URL of page to open.
	'				fnCallback	Function name string. Name of function to call when
	'							nested edit page closes.
	'
	sub OpenEditPage(sPage, fnCallback)
		dim i
		if not g_bEdit then exit sub
		for i = LBOUND(g_aPageStack) to UBOUND(g_aPageStack)
			if g_aPageStack(i) = "*" and i < UBOUND(g_aPageStack) then
				g_aPageStack(i) = "0"
				g_elFrames(i + 1).tabindex = -1
				g_aPageStack(i + 1) = "*"
				g_elFrames((i + 1) + 1).tabindex = 0
				g_elFrameset.cols = "0," & join(g_aPageStack, ",")
				g_elFrames((i + 1) + 1).src = sPage
				g_bNestedEdit = true
				if isNull(fnCallback) or fnCallback = "" then
					g_fnCallback = null
				else
					g_fnCallback = "window.frames(" & (i + 1) & ")." & fnCallback
				end if
				exit for
			end if
		next
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	closes a nested edit page and returns to the previous 
	'						edit page then executes the callback function
	sub CloseEditPage()
		dim i
		if not g_bEdit then exit sub
		for i = LBOUND(g_aPageStack) to UBOUND(g_aPageStack)
			if g_aPageStack(i) = "*" and i > LBOUND(g_aPageStack) then
				g_aPageStack(i) = "0"
				g_elFrames(i + 1).tabindex = -1
				g_aPageStack(i - 1) = "*"
				g_elFrames(i).tabindex = 0
				g_elFrameset.cols = "0," & join(g_aPageStack, ",")
				g_elFrames(i + 1).src = ""
				g_bNestedEdit = cBool(i - 1 > 0)
				on error resume next
				window.execScript "ResetParentEditTasks(bdbody" & i & ")", "VBScript"
				' -- execute callback function
				if not isNull(g_fnCallback) then execute(g_fnCallback)
				on error goto 0
				exit for
			end if
		next
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	clears the navigation tree selection when welcome displays
	sub ResetParentEditTasks(oWindow)
		dim elBtnBack, elBtnSave, elBtnSaveBack, elBtnSaveNew

		oWindow.focus()
		set elBtnBack = oWindow.elGetTaskBtn("back")
		set elBtnSave = oWindow.elGetTaskBtn("save")
		set elBtnSaveBack = oWindow.elGetTaskBtn("saveback")
		set elBtnSaveNew = oWindow.elGetTaskBtn("savenew")
		if not elBtnBack is nothing then oWindow.EnableTask("back")
		if oWindow.g_bDirty then
			if not elBtnSave is nothing then oWindow.EnableTask("save")
			if not elBtnSaveBack is nothing then oWindow.EnableTask("saveback")
			if not elBtnSaveNew is nothing then oWindow.EnableTask("savenew")
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	clears the navigation tree selection when welcome displays
	sub ClearNavSelection()
		on error resume next
		window.frames(0).ClearSelection()
		on error goto 0
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	click handler for help task button
	sub OpenHelp(strHelpFile)
		if typename(winHelpWindow) = "HTMLWindow2" then winHelpWindow.close
		set winHelpWindow = window.open("<%= g_sBizDeskRoot %>docs/default.asp?helptopic=" & strHelpFile, "winHelpWindow", _
						 "height=500,width=700,status=no,toolbar=yes,menubar:no,resizable=yes")
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' client state routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetTemp(sName, oValue)
	'		saves a temporary value in a scripting dictionary in the parent frame
	'
	'	Arguments:	sName	String. Name to use as lookup key for value.
	'				oValue	Variant or Object. Value or reference to store.
	'
	sub SetTemp(sName, oValue)
		if isObject(oValue) then
			set g_dTemp(sName) = oValue
		else
			g_dTemp(sName) = oValue
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	set oValue = GetTemp(sName)
	'		OR
	'	sValue = GetTemp(sName)
	'		retrieves a temporary value from a scripting dictionary in the parent frame
	'
	'	Arguments:	sName	String. Name to use as lookup key for value.
	'
	'	Returns:			Variant or Object. Value or reference retrieved.
	'
	function GetTemp(sName)
		if isObject(g_dTemp(sName)) then
			set GetTemp = g_dTemp(sName)
		else
			GetTemp = g_dTemp(sName)
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ClearTemp(sName)
	'		removes a temporary value from a scripting dictionary in the parent frame
	'
	'	Arguments:	sName	String. Name to use as lookup key for value.
	'
	sub ClearTemp(sName)
		g_dTemp(sName) = null
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' date conversion routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nDay = nGetDay(sValue)
	'		returns integer for day in formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Day value or -1 if not found
	'
	function nGetDay(sValue)
		dim nDayIndex
		nDayIndex = <%= Application("MSCSDayIndex") %>
		nGetDay = nGetDatePart(sValue, nDayIndex)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nMonth = nGetMonth(sValue)
	'		returns integer for m onthin formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Month value or -1 if not found
	'
	function nGetMonth(sValue)
		dim nMonthIndex
		nMonthIndex = <%= Application("MSCSMonthIndex") %>
		nGetMonth = nGetDatePart(sValue, nMonthIndex)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nYear = nGetYear(sValue)
	'		returns integer for year in formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Year value or -1 if not found
	'
	function nGetYear(sValue)
		dim nYearIndex, nYear
		nYearIndex = <%= Application("MSCSYearIndex") %>
		nYear = nGetDatePart(sValue, nYearIndex)
		if nYear > 3000 then
			nGetYear = -1
		else
			nGetYear = nYear
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	gets date part based on index and separator
	function nGetDatePart(sValue, nPartIndex)
		dim sSeparator, aDate
		sSeparator = "<%= Application("MSCSDateSeparator") %>"
		nGetDatePart = -1
		if not isNull(sValue) then
			aDate = split(sValue, sSeparator)
			if Ubound(aDate) = 2 then
				if aDate(nPartIndex) <> "" and isNumeric(aDate(nPartIndex)) then
					nGetDatePart = aDate(nPartIndex)
				end if
			end if
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	dtDate = dtGetDate(sValue)
	'		returns date variant for formatted value or current date if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Date. Date for value or today's date if not found
	'
	function dtGetDate(sValue)
		dim nYear, nMonth, nDay
		nYear = nGetYear(sValue)
		nMonth = nGetMonth(sValue)
		nDay = nGetDay(sValue)
		dtGetDate = date()
		if nYear > -1 and nMonth > -1 and nDay > -1 then
			dtGetDate = DateSerial(nYear, nMonth, nDay)
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	sDateText = sGetFormattedDate(nYear, nMonth, nDay)
	'		returns string date for day, month, and year in site's default locale
	'
	'	Arguments:	nYear	Integer. integer year
	'				nMonth	Integer. integer month
	'				nDay	Integer. integer day
	'
	'	Returns:			String. Date for day, month, and year in site's default locale
	'
	function sGetFormattedDate(nYear, nMonth, nDay)
		dim sMSCSDateFormat
		sMSCSDateFormat = "<%= Application("MSCSDateFormat") %>"
		sGetFormattedDate = ""
		if nYear > -1 and nMonth > -1 and nDay > -1 then
			sMSCSDateFormat = replace(sMSCSDateFormat, "yyyy", nYear)
			sMSCSDateFormat = replace(sMSCSDateFormat, "mm", nMonth)
			sMSCSDateFormat = replace(sMSCSDateFormat, "dd", nDay)
			sGetFormattedDate = sMSCSDateFormat
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	bValue = bIsDate(sValue)
	'		returns true if string is valid date formatted in site's default locale
	'
	'	Arguments:	sValue	String. string value to validate
	'
	'	Returns:			Boolean. true if valid date, false if not
	'
	function bIsDate(sValue)
		dim aMonthDays, nYear, nMonth, nDay
		' -- array of muber of days in month for day range checking
		aMonthDays = Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
		' -- assume ok unless find different
		bIsDate = true
		' -- get year, month and day from value if possible (-1 if invalid)
		nYear = nGetYear(sValue)
		nMonth = nGetMonth(sValue)
		nDay = nGetDay(sValue)
		if not (nYear > -1 and nMonth > -1 and nDay > -1) then
			' -- year, month, or day not valid
			bIsDate = false
		else
			' -- cast as integer
			nYear = CInt(nYear)
			nMonth = CInt(nMonth)
			nDay = CInt(nDay)

			if nYear < 50 then
				' -- assume 0-49 is 2000-2049
				nYear = nYear + 2000
			elseif nYear < 100 then
				' -- assume 50-99 is 1950-1999
				nYear = nYear + 1900
			end if

			' -- set number of days in Feb for leap years
			If nYear Mod 4 = 0 Then
				If nYear Mod 100 = 0 Then
					If nYear Mod 400 = 0 Then
						' -- century only a leap year if divisible by 400
						aMonthDays(1) = 29
					End If
				Else
					' -- non-century divisible by 4 is always a leap year
					aMonthDays(1) = 29
				End If
			End If

			if nMonth < 1 or nMonth > 12 then
				' -- month outside acceptable range
				bIsDate = false
			elseif nDay < 1 or nDay > aMonthDays(nMonth - 1) then
				' -- day outside acceptable range
				bIsDate = false
			end if
		end if
	end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' event handler routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	context menu handler to turn off context menus
	sub document_onContextMenu()
		' -- disallow default context menu unless in entry fields
		if not (window.event.srcElement.tagName = "INPUT" or window.event.srcElement.tagName = "TEXTAREA") then
			window.event.returnValue = <%= CInt(ALLOW_CONTEXT_MENUS) %>
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	key down handler to turn off all but editing keys
	sub document_onKeyDown()
		dim evt, elSource
		set evt = window.event
		set elSource = evt.srcElement
		if evt.ctrlKey then
			select case evt.keyCode
				case	KEYCODE_LEFT, KEYCODE_UP, KEYCODE_RIGHT, KEYCODE_DOWN, _
						L_KeyCodeShiftedCopy_Number, L_KeyCodeCopy_Number, _
						L_KeyCodeShiftedUndo_Number, L_KeyCodeUndo_Number, _
						L_KeyCodeShiftedPaste_Number, L_KeyCodePaste_Number, _
						L_KeyCodeShiftedCut_Number, L_KeyCodeCut_Number
					' -- allow these keys
				case else
					' -- disallow all other control key combos
					evt.returnValue = false
			end select
		elseif evt.keyCode = KEYCODE_BACKSPACE and _
			not (elSource.tagName = "INPUT" or elSource.tagName = "TEXTAREA") then
			evt.returnValue = false
		end if
		select case evt.keyCode
			case KEYCODE_F1
				' -- open help
				OpenHelp ""
				' -- disallow all FN keys
				evt.returnValue = false
				evt.cancelBubble = true
			case KEYCODE_F2, KEYCODE_F3, KEYCODE_F4, KEYCODE_F5, KEYCODE_F6, _
				KEYCODE_F7, KEYCODE_F8, KEYCODE_F9, KEYCODE_F10, KEYCODE_F11, KEYCODE_F12
				' -- disallow all FN keys
				evt.returnValue = false
				evt.cancelBubble = true
		end select
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	drag start handler to turn off dragging
	sub document_onDragStart()
		' -- disable dragging
		window.event.returnValue = false
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	select start handler to turn off selection except in inputs
	sub document_onSelectStart()
		' -- disallow selection of text on the page except in edit boxes
		Dim elSource
		set elSource = window.event.srcElement
		if elSource.tagName = "TEXTAREA" or _
			elSource.tagName = "INPUT" then
			if (elSource.readonly or elSource.disabled) then window.event.returnValue = false
		else
			window.event.returnValue = false
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	sets focus on first right frame
	sub window_onFocus()
		on error resume next
		bdbody1.focus()
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	window load handler to set globals
	sub window_onLoad()
		set g_elFrameset = document.all.tags("FRAMESET")
		if g_elFrameset.length > 0 then
			set g_elFrameset = g_elFrameset(0)
			set g_elFrames = document.all.tags("FRAME")
		end if
	end sub
'-->
</SCRIPT>
<FRAMESET FRAMEBORDER='0' FRAMESPACING='1' COLS='<%= L_NavigationTreeWidth_Number %>,*,0,0,0'>
	<FRAME ID='bdnav'   SRC='navtree.asp' FRAMEBORDER='0' FRAMESPACING='0' SCROLLING='no'   APPLICATION='yes'></FRAME>
	<FRAME ID='bdbody1' SRC='welcome.asp' FRAMEBORDER='0' FRAMESPACING='0' SCROLLING='auto' APPLICATION='yes' ACCESSKEY='0'></FRAME>
	<FRAME ID='bdbody2' TABINDEX='-1'     FRAMEBORDER='0' FRAMESPACING='0' SCROLLING='auto' APPLICATION='yes' ACCESSKEY='0' NORESIZE></FRAME>
	<FRAME ID='bdbody3' TABINDEX='-1'     FRAMEBORDER='0' FRAMESPACING='0' SCROLLING='auto' APPLICATION='yes' ACCESSKEY='0' NORESIZE></FRAME>
	<FRAME ID='bdbody4' TABINDEX='-1'     FRAMEBORDER='0' FRAMESPACING='0' SCROLLING='auto' APPLICATION='yes' ACCESSKEY='0' NORESIZE></FRAME>
</FRAMESET>
</HTML>
