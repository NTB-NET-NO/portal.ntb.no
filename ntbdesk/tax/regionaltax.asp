<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../Include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='./Include/taxStrings.asp' -->
<!--#INCLUDE FILE='./Include/tax_util.asp' -->

<% Call Main() %>

<HTML>
<HEAD>

<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
.dtTableStyle DIV.dtContainer
{
    HEIGHT: 300px;
}
</STYLE>
<SCRIPT LANGUAGE ='VBSCRIPT'>
			
	function OnChange()
		taxform.dt.value = window.event.XMLlist.xml
	end function
		
	function CountryChange()
		dim efRegion, efCountry
		set efRegion = dtTax.field("region_code")
		set efCountry = dtTax.field("country_code")
		if efCountry.value <> "" then
			efRegion.reload(efCountry.value)
		end if
	end function
		
</SCRIPT>
</HEAD>

<BODY SCROLL='NO'>

<% InsertEditTaskBar L_Tax_StaticText, g_sStatustext %>

	<form id="taxform" method="post">
		<input type="hidden" name="type" id="type" value="save">
		<input type="hidden" name="dt" id="dt">
	</form>
	
	<xml ID="dtTaxMeta">
		<%= g_xmlTaxMeta %>
	</xml>

	<xml id="dtTaxData">
		<%= g_xmlTaxData %>
	</xml>
	
<%
	If bIsdt Then	
%>
	<xml id="emptyData">
		<document>
			<record/>
		</document>
	</xml>

	<xml id="esMeta">
		<editsheet>
		    <global expanded="yes">
		        <name><%= L_Tax_Text%></name>
				<key><%= L_Tax_KeyName%></key>
		    </global>
		    <template register="dtTax">
				<![CDATA[<DIV ID="dtTax" CLASS="dynamicTable"
										 MetaXML="dtTaxMeta" 
										 DataXML="dtTaxData" 
										 LANGUAGE="VBScript"
										 ONCHANGE="OnChange()">
										 <%= L_LoadingProperties_Text%></DIV>]]>
			</template>
		</editsheet>
	</xml>
<div ID="bdcontentarea">
	<DIV ID="esMaster" CLASS="editSheet"
		 MetaXML="esMeta"
		 DataXML="emptyData"
		 LANGUAGE="VBScript"
		 ONCHANGE="setDirty('<%= L_SaveConfirmationDialog_Text%>')"
		 ONREQUIRE="setRequired('')"
		 ONVALID="setValid('')">
		 <%= L_LoadingProperties_Text%>
	</DIV>	
</div>

<% 
	End If
%>
 
</BODY>
</HTML>