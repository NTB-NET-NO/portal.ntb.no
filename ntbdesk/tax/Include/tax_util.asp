<%	
	' Global variables
	Dim g_sErrorTitle, g_sErrorMessage	' Error variables
	Dim g_MSCSTransactionConfigConnStr, g_oMarshaler
	Dim g_sXMLCountryCodeList, g_sXMLRegionCodeList, g_xmlMetaDoc, g_xmlTaxMeta, g_xmlTaxData 
	Dim g_sType, g_sStatusText, bIsdt
	
	' The error handling implemented here allows errors to bubble immediately upon error
	' Before performing operations, SetErrorContext is called to provide friendly contextual errors
	' for the attempted operations
	
	Sub Main
		On Error Resume Next
		' Set global xml variables to avoid empty strings if error occurs
		Call ProcessList
		If Err = 0 Or g_sType = "save" Then
			bIsdt = True
		End If
		Call SetErr			
	End Sub
	
	Sub ProcessList()
		Dim sXML, xmlDoc, xmlRecord, xmlNode		

		bIsdt = False
		g_sType = Request.Form("type")
		' If there is data, be sure to preserve it
		If g_sType = "save" Then
			sXML = Request.Form("dt")
			g_xmlTaxData = sXML
		Else
			g_xmlTaxData = "<document/>"
		End If
		'Get database connection
		Call SetErrorContext(L_GetSiteConfig_DialogTitle, L_SiteConfig_ErrorMessage)
		g_MSCSTransactionConfigConnStr= GetSiteConfigField("transaction config", "connstr_db_TransactionConfig")   
		Set g_oConn = oGetADOConnection(g_MSCSTransactionConfigConnStr)
		' No need to process further since there is no database connection!
		If g_oConn.State = 0 Then		' 0 = connection is closed 
			Err.Raise -34001	' Control cannot load without access to the Region table for select lists
		End If
		
		' Set up the country and region lists; create XML meta 
		Call SetErrorContext(L_GetData_DialogTitle, L_OptionList_ErrorMessage)
		Call BuildCountryAndRegionCodesList
		Set xmlDoc = server.CreateObject("MSXML.DOMDocument")
		xmlDoc.async = false
		xmlDoc.loadXML(GetXMLTaxMeta)
		Set g_xmlMetaDoc = xmlDoc.DocumentElement	' Variable used by GetXMLTaxMeta function
		g_xmlTaxMeta = xmlDoc.xml
		
		' Get data for dynamic table and/or save changes to database
		If g_sType = "save" Then
			If sXML <> "" Then
				Call SetErrorContext(L_SaveData_DialogTitle, L_Save_ErrorMessage)
				xmlDoc.loadXML(sXML)
				If xmlDoc.childNodes.length = 1 and _
					xmlDoc.selectNodes("*[@state='new']/*[. != '']").length = 0 Then
					'convert from site locale to server locale
					For Each xmlNode In xmlDoc.selectNodes("//rate")
						xmlNode.text = g_MSCSDataFunctions.ConvertFloatString(xmlNode.text, g_DBDefaultLocale)
					Next
					sXML = xmlDoc.xml	' Make sure data exists to avoid attempting to save empty recordset to database
				End If
				Call SaveTaxDataChanges(sXML)				
				g_sStatustext = L_Save_StatusBar
				Call SetErrorContext(L_GetData_DialogTitle, L_LoadData_ErrorMessage)
				g_xmlTaxData = GetXMLTaxData()
			End If
		Else	' Loading list for the first time
			Call SetErrorContext(L_GetData_DialogTitle, L_LoadData_ErrorMessage)
			g_xmlTaxData = GetXMLTaxData()			
		End If
		If g_xmlTaxData = "" Then
			g_xmlTaxData = "<document/>"
		End If
	End Sub
	
	Function GetXMLTaxMeta()
		Dim sBuffer
		sBuffer = "<dynamictable>"
		sBuffer = sBuffer & "<global uniquekey='no' required='no' keycol='country_code' sortbykey='yes'>"
		sBuffer = sBuffer & "</global>"
		sBuffer = sBuffer & "<fields>"
		sBuffer = sBuffer & g_sXMLCountryCodeList	' Country options list
		sBuffer = sBuffer & g_sXMLRegionCodeList	' Region options list
		sBuffer = sBuffer & "<numeric id='rate' width='35' required='yes' subtype='float' min='0' max='1000'>"
		sBuffer = sBuffer & "<format>" & g_sMSCSNumberFormat  & "</format>"
		sBuffer = sBuffer & "<name>" & L_TaxRate_Text & "</name>"
		sBuffer = sBuffer & "<prompt>" & L_TypeTaxRate_Text & "</prompt>"
		sBuffer = sBuffer & "<error>" & L_TaxRate_ErrorMessage & "</error>"
		sBuffer = sBuffer & "</numeric>"
		sBuffer = sBuffer & "</fields>"
		GetXMLTaxMeta = sBuffer & "</dynamictable>"
	End Function

	Sub BuildCountryAndRegionCodesList()
		Dim sSQLCountry, sSQLRegion, rsCountry, rsRegion
		Dim sCountryCode, sCountryDisplay, sRegionCode, sRegionDisplay
		Dim sCountryBuffer, sRegionBuffer, sOption, sOptions
		Dim fldCountryName, fldCountryDisplay, fldRegionName, fldRegionGroup, fldRegionDisplay
		
		' Get recordsets of countries and regions
		sSQLCountry = "SELECT u_Name, u_DisplayName FROM Region WHERE i_Type = 1 ORDER BY u_Name"
		Set rsCountry = GetRecordset(sSQLCountry)
		sSQLRegion = "SELECT u_name, u_DisplayName, u_Group FROM Region WHERE i_Type = 2 ORDER BY u_Group, u_DisplayName"
		Set rsRegion = GetRecordset(sSQLRegion)
				
		If (rsCountry.EOF AND rsCountry.BOF) Then
			Call SetErrorContext(L_GetData_DialogTitle, L_OnLoadError_StaticText)
			Err.Raise -34000		' Jump into error handler since there is no data in the region table
		End If
		' Create options lists
		sCountryBuffer = sCountryBuffer & "<select id='country_code' width='35' onchange='CountryChange'>"
		sCountryBuffer = sCountryBuffer & "<name>" & L_Country_Text & "</name>"
		sCountryBuffer = sCountryBuffer & "<prompt>" & L_SelectCountry_Text & "</prompt>"		
		sRegionBuffer = sRegionBuffer & "<select id='region_code' width='35'>"
		sRegionBuffer = sRegionBuffer & "<name>" & L_State_Text & "</name>"
		sRegionBuffer = sRegionBuffer & "<prompt>" & L_SelectState_Text & "</prompt>"
		sCountryBuffer = sCountryBuffer & "<select id='country_code'>"		
		Set fldCountryName = rsCountry("u_Name")
		Set fldCountryDisplay = rsCountry("u_DisplayName")
		Set fldRegionName = rsRegion("u_name")
		Set fldRegionGroup = rsRegion("u_Group")
		Set fldRegionDisplay = rsRegion("u_DisplayName")
		rsCountry.MoveFirst
		Do Until rsCountry.EOF
			sCountryBuffer = sCountryBuffer & "<option value='" & fldCountryName & "'>" & setText(fldCountryDisplay.value) & "</option>"
			' If there are any records, begin option groups
			sRegionBuffer = sRegionBuffer & "<select id='" & fldCountryName & "'>"
			sRegionBuffer = sRegionBuffer & "<option value='@'>" & L_AllStates_ComboBox & "</option>"
			If Not rsRegion.EOF Then
				sOptions = ""
				Do Until fldRegionGroup <> fldCountryName
					sOption = "<option value='" & fldRegionName & "'>" & setText(fldRegionDisplay.value) & "</option>"
					sOptions = sOptions & sOption
					rsRegion.MoveNext
					If rsRegion.EOF Then Exit Do
				Loop
				sRegionBuffer = sRegionBuffer & sOptions
			End If
			sRegionBuffer = sRegionBuffer & "</select>"
			rsCountry.MoveNext
		Loop
		sCountryBuffer = sCountryBuffer & "</select></select>"
		sRegionBuffer = sRegionBuffer & "</select>"

		' Put xml into global variables for GetXMLTaxMeta()
		g_sXMLCountryCodeList = sCountryBuffer
		g_sXMLRegionCodeList = sRegionBuffer
	End Sub	
	
	Function GetRecordset(sSQL)
		Dim rs		
		Set rs = Server.CreateObject ("ADODB.Recordset")
		With rs
			.CursorLocation = AD_USE_CLIENT
			.Open sSQL, g_oConn, AD_OPEN_STATIC, AD_LOCK_PESSIMISTIC
		End With
		Set GetRecordset = rs
	End Function

	Function GetXMLTaxData()
		Dim rsTaxData, xmlRecord, xmlOption, xmlNewNode, xmlData
		
		' Create dynamic table marshaler object
		Set g_oMarshaler = Server.Createobject("commerce.dyntablemarshaler")				
		' Get the recordset
		With g_oMarshaler
			.Initialize g_MSCSTransactionConfigConnStr
			Set rsTaxData = .GetAsRecordSet("RegionalTax", Array(),Array(), _
											Array("country_code", "region_code", "rate"), _
											"ORDER BY country_code, region_code")
		End With
			
		' Create xml from recordset
		Set xmlData = xmlGetXMLFromRSEx(rsTaxData, -1, -1, -1, null)
		'Set display names for regions based on country code (otherwise the Listsheet will just
		'get the first match which might not be in the right country list)
		For Each xmlRecord In xmlData.childNodes
			If xmlRecord.haschildNodes Then
				Set xmlOption = g_xmlMetadoc.selectSingleNode("fields/select/select[@id $ieq$ '" _
								& xmlRecord.selectSingleNode("country_code").text & "']/option[@value $ieq$ '" _
								& xmlRecord.selectSingleNode("region_code").text & "']")
				If Not xmlOption Is Nothing Then
					Set xmlNewNode = xmlData.ownerDocument.createElement("region_code_displayvalue")
					xmlNewNode.text = xmlOption.text
					xmlRecord.appendChild(xmlNewNode)
				End If
			End If
		Next
		GetXMLTaxData = xmlData.xml
	End Function
	
	Sub SaveTaxDataChanges(sXML)
		Dim xml		
		' Create dynamic table marshaler object
		Set g_oMarshaler = Server.Createobject("commerce.dyntablemarshaler")
		With g_oMarshaler
			.Initialize g_MSCSTransactionConfigConnStr
			.SaveXMLToTable sXML, "RegionalTax", Array(),Array()
		End With
		Set g_oMarshaler = Nothing		
	End Sub
	
	Sub SetErrorContext(sTitle, sMessage)
		g_sErrorTitle = sTitle
		g_sErrorMessage = sMessage
	End Sub
	
	Function setText(sValue)
		If IsNull(sValue) Then 
			sValue = ""
		Else			
			setText = replace(replace(replace(replace(sValue, "&", "&amp;"), "<", "&lt;"), ">", "&gt;"), vbCrLf, "&#13;")
		End If
	End Function
	
	Sub SetErr()
		' This function is called from Main but only triggers an error message if an error has occurred
		If Err <> 0 Then
			If Err <> -34001 Then
				Call setError(g_sErrorTitle, g_sErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
				Err.Clear
			End If
			If g_sType = "save" Then g_sStatustext = L_ErrorSave_StatusBar
		End If
	End Sub
%>