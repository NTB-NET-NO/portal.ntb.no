<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

Const L_OnLoadError_StaticText = "There are no codes in the Region table. (Note: you can add codes to this table using the Data Codes module.)"

Const L_Tax_Text = "&lt;U&gt;T&lt;/U&gt;ax"
Const L_Tax_KeyName = "t"
Const L_Save_StatusBar = "Tax Rates Saved"
Const L_ErrorSave_StatusBar = "An error occurred while saving the Tax rates."
Const L_Tax_StaticText = "Tax"

'Heading Constants
Const L_Country_Text = "Country/Region"
Const L_State_Text = "State/Province"
Const L_TaxRate_Text = "Tax rate (%)"

'Prompt Constants
Const L_SelectCountry_Text = "Select a country/region"
Const L_SelectState_Text = "Select a state/province"
Const L_TypeTaxRate_Text = "Type a tax rate"
Const L_AllStates_ComboBox = "All States/Provinces"

'Error Handling
Const L_TaxRate_ErrorMessage = "Tax Rate must be a positive decimal number between 0 and 1000 inclusive."
Const L_SaveConfirmationDialog_Text = "Save tax rates before exiting?"
Const L_Required_Text = "Cannot save until all required fields have values."
Const L_Valid_Text = "Cannot save until all invalid fields are corrected."
Const L_LoadingProperties_Text = "Loading properties, please wait..."
Const L_GetRegions_DialogTitle = "Get States/Provinces"
Const L_GetCountries_DialogTitle = "Get Countries/Regions"
Const L_GetData_DialogTitle = "Get Data"
Const L_SaveData_DialogTitle = "Save Tax Data"
Const L_Save_ErrorMessage = "An error occurred while saving Tax data."
Const L_GetSiteConfig_DialogTitle = "Get SiteConfig"
Const L_SiteConfig_ErrorMessage = "An error occurred while retrieving the connection string."
Const L_OptionList_ErrorMessage = "Country/Region and State/Province lists could not be loaded."
Const L_LoadData_ErrorMessage = "An error occurred while loading tax data."
%>
	
	
