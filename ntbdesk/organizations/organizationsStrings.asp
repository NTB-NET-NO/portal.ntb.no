<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

const g_sDictName = "OrgState"
const g_sDELIMITER = ";"
const g_iListPageSize = 20
const g_sListPage = "orgs.asp"
const g_sEditPage = "org_edit.asp"
'Localization Strings
const L_PageTitleOrganization_Text = "Organization Management"
const L_SearchCategoryLabel_Text = "Find by:"
const L_FirstNameInput_Text = "First Name:"
const L_LastNameInput_Text = "Last Name:"
const L_PerformSearchButton_Text = "Find Now"
const L_ListUserTableHeaderLastName_Text = "Last" 'ListView Table header for Last Name column
const L_ListUserTableHeaderFirstName_Text = "First" 'ListView Table header for First Name column
const L_ListUserTableHeaderCompany_Text = "Company" 'ListView Table header for Company column
const L_ListUserTableHeaderEmail_Text = "Email" 'ListView Table header for Company column

%>
