<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='constants.asp' -->
<!--#INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE='UOResources.asp' -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='../common/XMLroutines.asp' -->
<!--#INCLUDE FILE='../common/UserOrgListFns.asp' -->

<%

Dim g_sEntity, g_sKey, g_sStatusText, g_ColumnSize, g_rsQuery, g_sQuery, g_sName
Dim g_sFindBy, g_sFindByText
'boolean variable - set to true for AD Installations. This helps to modify queries
'submitted to provider to contain ParentDN.
Dim g_bisADInstallation

'call the Main subroutine
call Main

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Main
sub Main
	Dim aIDCol
	Dim oXMLProfileDoc
	Dim aprops, agroups
	Dim oXMLSearchColumns
	Dim dState, dUMState, key
	Dim rsOrgs
	Dim sitetermURL , profileSRC ,exprarch ,typeops

	rem make sure that if User clicked on "Manage Users for org" earlier
	rem all associated values are cleared from the Session variable
	if len(trim(Session("UMOrgRestrict")))>0 then
	    Session("UMOrgRestrict") = ""
	    Session("UMOrgID") = ""
		set dUMState = Session("UMState")
		for each key in dUMState
		    dUMState.Value(key) = NULL
		next
		set dUMState = nothing
		Session("UMState") = ""	
	end if
	
	Session("ORGHITS") = 0
	rem Create state dictionary for "Organization" profile
	rem this loads up all properties into an array
	
	set dState = dGetUMStateDictionary(g_sUMState)
	rem store it in a variable SABE !!why not use g_sUMState variable ??
	g_sEntity = g_sUMState
	
	rem get the array of properties from Session variable
	aprops = Session(g_sPropsLabel)
	
	rem get the array of groups for Org profile from Session variable
	agroups = Session(g_sGroupsLabel)

	rem this is an array containing Identity key(JOIN) column info
	rem aIDCol(1) = proper name of column e.g [GeneralInfo.user_id]
	rem aIDCol(2) = position of PK column in aprops array
'	aIDCol = Session(g_sIDColLabel)
	
	redim aIDCol(2)
    aIDCol(2) = nGetMatchingEntry(XML_ATTR_ORGID, aprops)
    aIDCol(1) = sGetPropName(aprops(aIDCol(2)))
	
	
	rem get the name used to refer each row. 
	g_sName = Session(g_sNameLabel)
	g_sKey = sGetIDName(aIDCol)
	
	set oXMLProfileDoc = Session(g_sProfileLabel)
	
	rem clear status text
	g_sStatusText = ""
	'SABE
	g_ColumnSize = 20 Rem in percentage
	
	'if Trim(dState.Value("findby")) = "name" then Session("DecrementFlag") = 1

	g_bIsADInstallation = bIsAdInstallation(oXMLProfileDoc)
    
	call ProcessForm(dState)
   

	call DetermineAction(dState, aprops)
	g_sQuery = dState.Value("query")
	
	rem SABE check if really necessary 
	call ClearUsers(dState)
	
	rem store the findby
	if IsNull(dState.Value("findby")) then
	    g_sFindBy = ""
	else
	    g_sFindBy = Trim(dState.Value("findby"))
	    g_sFindByText = Trim(dState.Value("FindByText"))
	end if

	If Session("ErrorMessage") = "" Then 
		Set rsOrgs = rsGetListSheetRecordSet(dState)
	End If
	
	sitetermURL= sRelURL2AbsURL("../common/QBSiteTermsTransform.asp") & "?LoadCatalogSets=" & true  
    profileSRC = sRelURL2AbsURL("../common/QBProfileTransform.asp") & "?CURR_PROFILE=" & CURR_PROFILE 
    exprarch = sRelURL2AbsURL("../exprarch")
    typeops = sRelURL2AbsURL("../organizations/TypesOpsUO.xml")
%>
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<LINK REL='stylesheet' TYPE='text/css' HREF='/widgets/exprbldrhtc/exprbldr.css'>

<!--#INCLUDE FILE="ListPageRoutines.asp" -->

</HEAD>
<BODY SCROLL=no>
<%
	InsertTaskBar L_PageTitleOrganization_Text, g_sStatusText
%>

<!-- FIND BY STARTS HERE - add your group ids to the option value -->

<DIV ID="bdfindbycontent" CLASS='findByContent'>
<FORM ACTION='SearchPage.asp' ID='searchform' NAME='searchform'>
<%= sGetSearchXML(oXMLProfileDoc, oXMLSearchColumns, dState) %>

<!-- FIND BY ENDS HERE - just add DIVs for your own groups as below -->
<DIV ID="divfbGeneral" STYLE="display:none">
	<TABLE>
		<TR>
			<TD WIDTH='100'>
				<SPAN ID='findbylabel' TITLE='<%=L_FindLabel_Text %>'><%= L_SearchCategoryLabel_Text %></SPAN>
			</TD>
			<TD WIDTH='200'>
			
			<DIV ID='selfindby' CLASS='editField' 
					METAXML='fbMeta' DATAXML='fbData'><%= L_Loading_Text %></DIV>									
			</TD>
			<TD ALIGN='RIGHT'>
				<BUTTON CLASS='bdbutton' LANGUAGE='VBScript' ONCLICK='reloadListSheet()' 
						ID='btnfindby' NAME='btnfindby' TITLE='<%= L_FindButton_Text %>'><%=L_FindButton_Text %></BUTTON>
			</TD>
		</TR>	
	</TABLE>	
</DIV>	


<%= sPrintSearchForms(oXMLSearchColumns) %> 


<XML ID="xmlisConfig">
  <EBCONFIG>
    <DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
	<DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
	<CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
	<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
	<HELPTOPIC><%= sRelURL2AbsURL("../docs/default.asp?helptopic=cs_bd_usersgrps_pqvv.htm")%></HELPTOPIC>
    <EXPR-ASP><%= exprarch %></EXPR-ASP>
    <!-- The presence of the DEBUG node tells CB to output debug info. -->
    <DEBUG />

    <!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
    <PROFILE-LIST>
    <PROFILE SRC="<%=profileSRC%>"></PROFILE>
    </PROFILE-LIST>
    <SITE-TERMS URL="<%=sitetermURL%>" />
    <MESSAGE><%= L_SearchOrg_Text %></MESSAGE>
    <FILTER-PROPERTIES>(not(@isSearchable) || @isSearchable != '0') $and$ (not(@isActive) || @isActive!= '0') $and$ (not(@isMultiValued) || @isMultiValued = '0') $and$ (@propType != 'PASSWORD') $and$ (@propType != 'BINARY')  $and$ (@propType != 'IMAGE')</FILTER-PROPERTIES>
    <TYPESOPS><%= typeops %></TYPESOPS>
  </EBCONFIG>
</XML>

<DIV ID="divQueryBldr" CLASS="clsQueryBldr"
    STYLE="behavior:url(/widgets/exprbldrhtc/QueryBuilder.htc); width:95%; display:none; background-color:transparent; border-style:none" XMLCfgID="xmlisConfig" OnREADY="OnQBReady()">
      <H3 ALIGN="center"><%= L_LoadingQB_Text %></H3>
       <%= L_CurrentQuery_Text %>
</DIV>
<INPUT TYPE='hidden' NAME='AdvQuery' ID='AdvQuery' VALUE=''>


</DIV>

</FORM>


<!-- FIND BY DIVS ENDS HERE -->
<div ID="bdcontentarea">


<FORM ACTION='' METHOD='POST' ID='addform' NAME='addform' >
    <INPUT TYPE='hidden' ID='type' NAME='type' VALUE='add'>
    <INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>' VALUE='-1'>
</FORM>

<FORM ACTION='' METHOD='POST' ID='copyform' NAME='copyform' >
		<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='copy'>
    <INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>'>
    <INPUT TYPE='hidden' ID='friendlyname' NAME='friendlyname' VALUE=''>
</FORM>
<FORM ACTION='ListPaging.asp' ID='newpageform' NAME='newpageform'>
	<INPUT TYPE='hidden' ID='query' NAME='query' VALUE='<%= g_sQuery%>'>
</FORM>
<FORM ACTION='' METHOD='POST' ID='deleteform' NAME='deleteform' ONTASK='ShowDelDialog "<%= g_sEntity %>", deleteform.elements.<%= g_sKey %>.value, deleteform.elements.friendlyname.value'>
    <INPUT TYPE='hidden' ID='type' NAME='type' VALUE='delete'>
    <INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>' VALUE='' >
    <INPUT TYPE='hidden' ID='friendlyname' NAME='friendlyname' VALUE=''>
    <INPUT TYPE='hidden' ID='numitems' NAME='numitems' VALUE='0'>
    <INPUT TYPE='hidden' ID='SelType' NAME='SelType' VALUE='' >
    <INPUT TYPE='hidden' ID='Query' NAME='Query' VALUE='' >
</FORM>

<%
'    Response.Write "<br>SQL = " & g_sQuery & "<br>"
    Call PrintListSheet(dState, aprops, rsOrgs)
    Set rsOrgs = Nothing
    If	Session("MSCSSiteTermVer") <> Application("MSCSSiteTermVer") Then
	'	Session("MSCSSiteTermVer") = Application("MSCSSiteTermVer")
	    Call GetNewSiteTermXML(True)
	End IF	
%>

<div ID="filtertext" class="filtertext" style="MARGIN-TOP: 20px"><%= L_BDSSFilter_Text & " " & L_UseWildCardSearch_Text  %></div>

    <DIV ID='lsProfiles' CLASS='listSheet' STYLE="MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px; HEIGHT: 80%"
        DataXML='profilesList'
        MetaXML='profilesMeta'
        LANGUAGE='VBScript'
        OnRowSelect='OnSelectRow()'
        OnRowUnselect='OnRowUnselect()'
        OnAllRowsSelect='OnSelectAllRows()'
        OnAllRowsUnselect='OnUnSelectAllRows()'><%= L_LoadingList_Text %></DIV>


<FORM ACTION='' METHOD='POST' ID='selectform'>
    <INPUT TYPE='hidden' ID='type' NAME='type' VALUE='open'>
    <INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>' VALUE=''>
    <INPUT TYPE='hidden' ID='friendlyname' NAME='friendlyname' VALUE='' >
    <INPUT TYPE='hidden' ID='numitems' NAME='numitems' VALUE='0'>
    <INPUT TYPE='hidden' ID='lock' NAME='lock' VALUE='false'>
    <INPUT TYPE='hidden' ID='sortcol' NAME='sortcol' VALUE='<%= g_sName %>'>
    <INPUT TYPE='hidden' ID='sortdir' NAME='sortdir' >
    <INPUT TYPE='hidden' ID='newpage' NAME='newpage'>
    <INPUT TYPE='hidden' ID='SelType' NAME='SelType' VALUE='' >
    <INPUT TYPE='hidden' ID='Query' NAME='Query' VALUE='' >

</FORM>

</div>
</BODY>
</HTML>
<%End Sub%>