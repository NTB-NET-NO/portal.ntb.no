<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='constants.asp' -->
<!--#INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE='UOResources.asp' -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='../common/XMLroutines.asp' -->
<!--#INCLUDE FILE='../common/UserOrgEditFns.asp' -->
<!--#INCLUDE FILE='../common/DARoutines.asp' -->

<%
Dim g_bisADInstallation
Dim g_sKey, g_sKeyValue, g_sType, g_sStatus, g_sEntity, g_sReadOnly
Dim aprops, agroups
Dim g_sTitleText
Dim g_sPrevValue ,g_bBulkUpdate
Dim g_bCopy
Dim bInsertError
Dim bUpdateError
Dim g_szAdminGroup
Dim g_szUserGroup
Dim delQuery

g_bCopy = False
bInsertError = False
bUpdateError = False

call main


Function CreateGroups(dState, sOrgName)
	Dim dGroupState,oXMLGroup
	Dim agroups, aprops
	Dim sSQL, dValues
	Dim bOK, cn ,rs
	
    REM -- Stop

	Set dGroupState = Server.CreateObject("Commerce.Dictionary")
	dGroupState.ProfileDSN = dState("ProfileDSN")
	dGroupState.ProfileName = "Profile Definitions.ADGroup"
	
	Call GetProperties(aprops, agroups, oXMLGroup, dGroupState)

	Set dValues = Server.CreateObject("Commerce.Dictionary")
	dValues.GroupName = sOrgName & AD_ADMIN_GROUP_SUFFIX
	g_szAdminGroup = dValues.GroupName
	REM -- g_szAdminGroup = Left(dValues.GroupName,20)
	REM -- dValues.SAMAccountName = g_szAdminGroup
	dValues.SAMAccountName = Left(sOrgName, AD_SAM_ACCOUNT_NAME_LEN - Len(AD_ADMIN_GROUP_AN_SUFFIX)) & AD_ADMIN_GROUP_AN_SUFFIX
	REM -- dValues.SAMAccountName = sOrgName & "A"
	dValues.GroupType = -2147483646
	REM -- Set the ProfileID as a Guid
	
	dValues.ProfileID = sGetGUID()
	dValues.ParentDN = sGetGroupParentURL(sorgName,dState)
	sSQL = InsertGroupQuery(aprops, dValues)
	
	Set cn = cnGetproviderconnection(dState)
	
	On Error Resume Next
	bOK = bExecWithErrDisplay(rs,cn,sSQL, _
									sFormatString(L_CreateGroup_ErrorMessage,Array(dValues.GroupName)))
	If bOK then 
		dValues.GroupName = sOrgName & AD_USER_GROUP_SUFFIX
		g_szUserGroup = dValues.GroupName
		REM -- g_szUserGroup = Left(dValues.GroupName,20)
		REM -- dValues.SAMAccountName = g_szUserGroup
		dValues.SAMAccountName = Left(sOrgName, AD_SAM_ACCOUNT_NAME_LEN - Len(AD_USER_GROUP_AN_SUFFIX)) & AD_USER_GROUP_AN_SUFFIX	
		dValues.GroupType = -2147483646
		dValues.ProfileID = sGetGUID()
		dValues.ParentDN = sGetGroupParentURL(sorgName,dState)
		sSQL = InsertGroupQuery(aprops, dValues)
		bOK = bExecWithErrDisplay(rs,cn,sSQL, _
										 sFormatString(L_CreateGroup_ErrorMessage,Array(dValues.GroupName)))
	end if
	
	CreateGroups = bOK
	Set dValues = Nothing
	Set dGroupState = Nothing
	Set cn = Nothing
	
End Function

Function sGetGroupParentURL(sorgName,dState)
	Dim sBaseDN
	sBaseDN = sGetParentURL(dState)
	If sBaseDN="" Then
		sGetGroupParentURL = ""
	Else
		sGetGroupParentURL = "OU=" & sOrgname & "," & sBaseDN
	End If	
End Function



Function SetUserGroupPermissions (szObjectContainer , szGroup)
	Dim oOpenDSObject , oObject
	Dim secDescr , dACL
	Dim newAce

	Set oOpenDsObject = GetObject("LDAP:")
	Set oObject = oOpenDSObject.OpenDSObject(szObjectContainer, g_szAdmin, g_szPassword, 1)
	  	
	Set secDescr = oObject1.Get("nTSecurityDescriptor")
	Set dACL = secDescr.DiscretionaryACL
	  
	Set newAce = CreateObject("AccessControlEntry")

	 REM -- ACE1: on the Object (not inherited)
	 REM -- Set AceType to: ACCESS_ALLOWED_ACE_TYPE = 0
	 newAce.AceType = ADS_ACETYPE_ACCESS_ALLOWED
	 REM --newACE.AccessMask = -1 ' all rights
	 REM --newAce.AccessMask = ADS_RIGHT_DELETE + ADS_RIGHT_READ_CONTROL + ADS_RIGHT_WRITE_DAC + ADS_RIGHT_WRITE_OWNER + ADS_RIGHT_DS_CREATE_CHILD + ADS_RIGHT_DS_DELETE_CHILD + ADS_RIGHT_ACTRL_DS_LIST + ADS_RIGHT_DS_SELF + ADS_RIGHT_DS_READ_PROP + ADS_RIGHT_DS_WRITE_PROP + ADS_RIGHT_DS_DELETE_TREE + ADS_RIGHT_DS_LIST_OBJECT
	 newAce.AccessMask = ADS_RIGHT_DS_READ_PROP 
	 newAce.Trustee = szgroup
	 newAce.AceFlags = ADS_ACEFLAG_INHERIT_ACE + ADS_ACEFLAG_INHERIT_ONLY_ACE
	 REM -- default: newACE.AceFlags = ?

	 dACL.AddAce newAce
  
	 secDescr.DiscretionaryACL = dACL
	 oObject.Put "nTSecurityDescriptor", secDescr
	 oObject.SetInfo

End Function

Function InsertGroupQuery(aprops, dValues)
	Dim sSQL, sName , sFullName
	Dim params, values , svalue , iType
	Dim nIndex, bneedComma
	
	bneedComma = False
	sSQL = "INSERT INTO " & sBracket(sGetPropProfile(aprops(1)))
	params = " (" 
	values = " VALUES("
	for nIndex= 1 to Ubound(aprops)
		sName = sGetPropName(aprops(nIndex))
	    sFullName = sBracket(sGetPropFullname(aprops(nIndex)))
	    svalue = dValues(sName)
	    iType = sGetPropType(aprops(nIndex))
			if len(sValue) > 0 and LCase(sValue)<> "null" then
				if bneedcomma then
					params = params & ", " & sFullName
					values = values & ", " & sQuoteArg(sValue, itype)
				else
					params = params & sFullName
					values = values & sQuoteArg(sValue, itype)
					bneedcomma = true
				end if
			end if
	 next	
	params = params & ")"
	values = values & ")"
	
	InsertGroupQuery = sSQL & params & values
End function

Function sGetParentURL(dState)
	REM --Function to determine where to create a user for AD
	REM --Need to get a base URL. Should be a Site property (currently isn't so will need to be 
	REM --hard coded.
	REM --Then for users, need to get the org name (query time) and the user name.
	REM --will add to base url ";ou={orgName};cn={userName}"
	dim sOrgName, bExit, sBaseURL
	bExit = False
	sOrgName = trim(Request.Form("Name"))
	if len(sOrgname) <= 0 then
		bExit = True
	end if
	sBaseURL = sGetBaseURL()
	if len(trim(sBaseURL)) = 0 then
		bExit = True
	end if
	if bExit then
		sGetParentURL = ""
	else
		sGetParentURL = sBaseURL REM --& ";ou=" & sOrgName
	end if
End Function

  
    
' -----------------------------------------------------------------------
' --Main begins here
' -----------------------------------------------------------------------

Sub Main
    Dim cn, oRSQuery, sQuery, oXMLDoc, aIDCol, bIDInt, g_sFriendly, nitems, aGUIDs
    Dim bIsInsert, iIDtype, nIndex, sWhere, dState, dSelect, user
    Dim sHTMLCase, nBrowse, nID
    Dim aUserIds, aNames
    Dim sBulkWhereClause
    REM -- Error flag to check if an error occured executing the query.
    Dim bRet
    Dim sXML
    Dim arrType
   
            
    bRet = True
    bIsInsert = False
    
	Set g_sPrevValue = Server.CreateObject("Commerce.Dictionary")
    set dState = dGetUMStateDictionary("Organization")
    set cn = cnGetProviderConnection(dState)
    set dSelect = dState.Value("Selected")
    

	REM --To allow dynamic Profile changing, need to refresh Profile everytime
    call GetProperties(aprops, agroups,oXmlDoc, dState)
'   call DetermineIDColumn(oXMLDoc, aprops, aIDCol)
    set Session(g_sProfileLabel) = oXMLDoc
    g_bIsADInstallation = bIsAdInstallation(oXMLDoc)
    
   	redim aIDCol(2)
    aIDCol(2) = nGetMatchingEntry(XML_ATTR_ORGID, aprops)
    aIDCol(1) = sGetPropName(aprops(aIDCol(2)))

    Session(g_sPropsLabel) = aprops
    Session(g_sGroupsLabel) = agroups
    Session(g_sIDColLabel) = aIDCol
    nID = nGetIDColNo(aIDCol) 
    nBrowse = nGetNumBrowses(oXMLDoc, sHTMLCase)

    bIDInt = bIsIDInt(aprops, aIDCol)
    if len(trim(Request.Form("Name"))) > 0 then
        g_sFriendly = Request.Form("Name")
    else
        g_sFriendly = Request.Form("friendlyname")
    end if
    
    arrType = Split(CStr(Request.Form("type")),",")
    g_sType = arrType(0)
    
    g_sEntity = L_OrgEntity_Text
    g_sKey = sGetIDName(aIDCol)
    g_sKeyValue = Request.form(g_sKey)
    
    nitems = dSelect.count
    if nItems = 0 and Not IsEmpty(Request.Form("numitems")) then
      nitems = CInt(Request.Form("numitems"))
      REM --Unpack GUIDs of selected organizations, put into dictionary for safe keeping on server
			if len(nitems) > 0 then
			  if nitems = 1 then
			    dSelect.Value(g_sKeyValue) = g_sFriendly
			  elseif nitems > 1 then
			    aUserIDs = split(g_sKeyValue,g_sDELIMITER)
			    aNames = split(g_sFriendly, g_sDELIMITER)
			    for nIndex= LBound(aUserIDs) to UBound(aUserIDs)
			        dSelect.Value(aUserIDs(nIndex)) = aNames(nIndex)
			    next
			  end if
			end if
	 end if		
    If strcomp(Request.Form("SelType"),"All",1) = 0 Then
			nitems = -1
			g_bBulkUpdate = True
			sBulkWhereClause = sGetWhereClause(dState.Value("Query"))
    Elseif strcomp(Request.Form("bBulkUpdate"),"True",1) = 0 then
			g_bBulkUpdate = True
			sBulkWhereClause = Request.Form("sBulkWhereClause")
    End If	

    REM -- if pfid was empty set key value to -1 (for add)
    if g_sKeyValue = "" or g_sKeyValue = "-1" then g_sKeyValue = "-1"
    REM -- if type is empty or keyvalue is -1 the set type to add
    g_sStatus = ""

	REM --If come here from edit page where user has hit save button
    If strcomp(g_sType, "save", 1) = 0 or strcomp(g_sType, "savenew", 1) = 0 Then
				REM -- determine the type of variable ID is
        iIDtype = sGetPropType(aprops(nID))

        bIsInsert = (strcomp(Request.Form("ISINSERT"), "True",1) = 0)

    	If  bIsInsert Then
    		REM -- If we need to insert a new org, make up the Insert query and execute it. 
				
    		call MakeInsertQuery(aprops, sQuery, aIDCol, g_sKeyValue, bIDInt, dState)
    		bRet = ExecCmd(sQuery, cn, _
    							sFormatString(L_InsertEntity_ErrorMessage,Array(g_sEntity)))
    		sQuery = ""
    		REM -- For delegated admin
    		If g_bIsAdInstallation and bRet then
    			REM -- Add Code here to add groups for the created org.
    			
    			bRet = CreateGroups(dState,g_sFriendly)
    		
    			REM -- Add code here to set permissions on this org
    			If bRet then
    				Call SetGroupPermissions(g_sFriendly, aprops, cn, g_sKeyValue)
    			End If	
    		End If
    		If Not bRet then
    		   	g_sStatus =  sFormatString(L_CouldNotAdd_StatusBar, Array(g_sEntity, g_sFriendly))
				g_sType = "add"
				bInsertError = True
				nitems = -1
				If g_bIsAdInstallation Then
					delQuery = GetDeleteQuery(g_sFriendly, aprops, cn, g_sKeyValue)
					bRet = ExecCmd(delQuery,cn, sFormatString(L_InsertEntity_ErrorMessage,Array(g_sEntity)))
				End If
			Else
    			REM -- Display appropriate status message.
    			g_sStatus = sFormatString(L_Add_StatusBar, Array(g_sEntity, g_sFriendly))
    			REM --adding new org to the selected dictionary
    			Call ClearUsers(dState)
    			dSelect.Value(g_sKeyValue) = g_sFriendly
    		End If	
    	Else
			REM -- If we're not inserting, we're saving changes to existing org(s). Call UPDATE

					REM --aGUIDs is an array that will hold the guids for all orgs we are currently editing.
			If nitems > 0 Then
				Redim aGUIDs(nitems-1)
				nIndex= 0
				For Each user IN dSelect
					aGUIDs(nIndex) = user
					nIndex= nIndex + 1 
				Next
            End if  
                
            If (nitems = 1) then
            REM -- There is only one Org. Get it's name for display
                if len(trim(Request.Form("Name"))) > 0 then
                    dSelect.Value(aGUIDs(0)) = trim(Request.Form("Name"))
                end if
                g_sFriendly = dSelect.Value(aGUIDs(0))
                REM -- Form Update Query and execute query
                call MakeUpdateQuery(aprops, sQuery, aIDCol, dState)
                sWhere = " WHERE " & sBracket(sGetPropFullName(aprops(nID))) & " = " & sQuoteArg(aGUIDs(0), iIDType)
                bRet = ExecCMD(sQuery & sWHERE, cn, _
										 sFormatString(L_InsertEntity_ErrorMessage,Array(g_sFriendly)))
                REM -- Display status
                If Not bRet then
    					g_sStatus = sFormatString(L_CouldNotSave_StatusBar, Array(g_sEntity, g_sFriendly))
    					bUpdateError = True 
    			Else
    					g_sStatus = sFormatString(L_Save_StatusBar, Array(g_sEntity, g_sFriendly))
    			End If	
            Else
					
				On Error Resume Next
				REM -- Build query from the Expression builder output.
				Call MakeMultipleUpdateQuery(aprops, sQuery)
				If g_bBulkUpdate Then
					If len(sBulkWhereClause) > 0 then
						sWhere = " WHERE " & sBulkWhereClause
					End If	
					bRet = ExecCMD(sQuery & sWHERE, cn, _
							sFormatString(L_BulkUpdateEntity_ErrorMessage,Array(g_sEntity)))
					Else
					Dim strGUIDS
					For nIndex= 0 to nitems-1
						aGUIDs(nIndex) = sQuoteArg(aGUIDs(nIndex), iIDType)
					Next
					strGUIDS = Join(aGUIDs, ",")
			        REM -- Build an IN Clause here
					sWhere = " WHERE " & sBracket(sGetPropFullName(aprops(nID))) & " IN (" & strGUIDS & " )"
					bRet = ExecCMD(sQuery & sWHERE, cn, _
										sFormatString(L_UpdateNEntity_ErrorMessage,Array(nitems, g_sEntity)))
  				End If
  				If Err.number <> 0 or Not bRet then
  					g_sStatus = sFormatString(L_CouldNotSave_StatusBar, Array(nitems, g_sEntity))	
  					Err.Clear
  				Else
					If nitems = -1 Then
						g_sStatus = sFormatString(L_SaveAll_StatusBar, Array(g_sEntity))
					Else	
						g_sStatus = sFormatString(L_Save_StatusBar, Array(nitems, g_sEntity))
					End If	
  				End If
	  			On Error Goto 0	
			End if
        End If
    End If  
  
  Select case (Lcase(g_sType))
  	case "save", "open"
  	
  		bIsInsert = False
  		Select case (nitems)
  			case "0", "1"
  				call FormQuery(sQuery, aprops, g_sKeyValue, nID,dState)
  				bRet = ExecQuery(oRSQuery, cn, sQuery, L_OrgFetchFail_ErrorMessage)
  				If Not bRet Then
    				REM -- display a blank edit sheet. Failed to retrieve user.
  					sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
  				  				    
  				Else
  					REM -- display the user
  					sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 1, "", 0)
  					Session("OrgXML") = sXML
  					Set Session("UserRS")= oRSQuery
  					'Count to know the number of consecutive hits on this profile .
					'If greater than 1 then we  do a kind of state cahching 
					Session("ORGHITS") = Session("ORGHITS") + 1
  				End If	
				set oRSQuery = Nothing
  			case else
  			 			   
  				call ShowQueryBuilder()
  		End Select
  case "savenew"
   		If Not bRet then
			call FormQuery(sQuery, aprops, g_sKeyValue, nID, dState)
			bRet = ExecQuery(oRSQuery, cn, sQuery, L_OrgFetchFail_ErrorMessage)
			If Not bRet Then
  				REM -- display a blank edit sheet. Failed to save.
  			    sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
  			Else
  				REM -- display user with error.
  			    sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 1, "", 0)
  			    Session("OrgXML") = sXML
  			    Set Session("UserRS")= oRSQuery
  			End If  
  			set oRSQuery = Nothing
  		Else 
  			 		REM -- show blank edit page
  			 		bIsInsert = True
  			 		g_sTitleText = sFormatString(L_New_Text, Array(g_sEntity))
  			 		sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
  		End If
  case "add"
  
  		Select case (nitems)
  			case "1"
  			    
  				If Not bRet then
  					sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
  				Else	
  					sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, nitems, "", 0)
  					Session("OrgXML") = sXML
  					Set Session("UserRS")= oRSQuery
  				End If	
  			case else
  			    
  				sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, nitems, "", 0)
  		End Select 
  case "copy"
  
  	g_bCopy = True
    call FormQuery(sQuery, aprops, g_sKeyValue, nID,dState)
    bRet = ExecQuery(oRSQuery, cn, sQuery, L_CopyOrgFail_ErrorMessage)
	If Not bRet Then
  		REM -- display a blank edit sheet. Failed to retrieve organization.
  		sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
  	Else	
		sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, nitems, "", 0)
		Session("OrgXML") = sXML
		Set Session("UserRS")= oRSQuery
		'Count to know the number of consecutive hits on this profile .
		'If greater than 1 then we  do a kind of state cahching 
		Session("ORGHITS") = Session("ORGHITS") + 1
    End If
    Set oRSQuery = Nothing
            
End Select
    
Set cn = Nothing


%>

<HTML>
<HEAD>
    <TITLE> BizDesk </TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<!-- #INCLUDE FILE="EditPageRoutines.asp" -->

</HEAD>
<BODY SCROLL='no'>
<%
	
    if g_sType = "add" then
	'	g_sTitleText = sFormatString(L_New_Text, Array(g_sEntity))
	    g_sTitleText = L_NewOrg_Text
    elseif g_sType = "savenew" then
		If Not bRet then
			 g_sTitleText = sFormatString(L_OpenOrganization_Text, Array(g_sfriendly))
		Else
			g_sTitleText = L_NewOrg_Text
		End If	
    elseif g_sType = "copy" then REM -- g_sType = "copy"
		g_sTitleText = sFormatString(L_CopyOrg_Text, Array( g_sfriendly))
		
    else REM -- g_sType = "open"
   
		if (nitems > 1) then

		'	  g_sTitleText = sFormatString(L_OpenMultiple_Text, Array(nitems, g_sEntity))
			  g_sTitleText = sFormatString(L_OpenOrganizations_Text, Array(nitems))
		'	  g_sTitleText = L_UpdateOrg_Text & g_sTitleText
		elseif nitems = -1 then 
			  g_sTitleText = L_OpenAllOrganizations_Text
		else
			  g_sTitleText = sFormatString(L_OpenOrganization_Text, Array(g_sfriendly))
		'	  g_sTitleText = L_UpdateOrg_Text & g_sTitleText
		
		end if
    end if
   
%>
<%
    InsertEditTaskBar g_sTitleText, g_sStatus
%>

<DIV ID='editSheetContainer' CLASS='editPageContainer' onChange='EnableSave()'>
<FORM ID='profilesform' ACTION='' METHOD='post' ONTASK='OnSave()'>
<INPUT TYPE='hidden' NAME='type' VALUE='save'>
<INPUT TYPE='hidden' NAME='numitems' VALUE="<%=nitems%>" >
<INPUT TYPE='hidden' NAME='bBulkUpdate' VALUE="<%= g_bBulkUpdate %>">
<INPUT TYPE='hidden' NAME='sBulkWhereClause' VALUE="<%= sBulkWhereClause %>">
<INPUT TYPE='hidden' NAME='AdvQuery' VALUE="">

<!-- putting edit sheets within this container allows for scroling without 
		losing the task buttons off the top of the screen -->
<%
REM --then you can either set g_rsQuery to a recordset or set g_sQuery to a SQL query string

REM --g_sReadOnly is used to set the key fields to readonly when opening existing item
REM --g_sReadOnly = "no"
REM --if g_sType <> "add" then g_sReadOnly = "yes"

REM --display profile groups

If bInsertError or bUpdateError Then 
    'Response.Write Session("OrgXML")
    
    if Not IsEmpty(Session("UserUpdateRS")) or Not IsObject(Session("UserUpdateRS")) then 
        if Session("ORGHITS") >= 1  then 	sXML = sXMLMakeEditSheets(Session("UserUpdateRS"), agroups, aprops, 1, "", 0)
	else
		sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
	end if    
    Response.Write sXML
Else
    Response.Write sXML
End IF

%>

	<!-- product groups meta data -->
	
	
<INPUT TYPE='hidden' NAME='OrgGUID' VALUE='<%=g_sKeyValue%>'>
<INPUT TYPE="HIDDEN" NAME="ISINSERT" ID="ISINSERT" VALUE="<%= bIsInsert%>">
</FORM>

<%    
if (strcomp(g_sType , "add", 1) <> 0) and _
	(strcomp(g_sType , "savenew", 1) <> 0) and _
	nItems <= 1 and _
	Not g_bBulkUpdate then %>


<xml id='nothing'>
    <record></record>
</xml>

<% end if %>
</DIV> <!-- End of Edit whole sheet -->

<FORM ID="closeform" ACTION METHOD="post" ONTASK="OnClose">
</FORM>
<FORM ID="addform" ACTION METHOD="post" ONTASK='OnSaveNew()' >
</FORM>

</BODY>
</HTML>
<%
end sub
%>


<% sub ShowQueryBuilder()
		
		Dim sitetermURL, profileSRC, exprarch, typeops
		sitetermURL= sRelURL2AbsURL("../common/QBSiteTermsTransform.asp") & "?LoadCatalogSets=" & True  
		profileSRC = sRelURL2AbsURL("../common/QBProfileTransform.asp") & "?CURR_PROFILE=" & CURR_PROFILE 
		exprarch = sRelURL2AbsURL("../exprarch")
		typeops = sRelURL2AbsURL("../organizations/TypesOpsUpdate.xml")
%>

   <XML ID="xmlisConfig">
		<EBCONFIG>
		<DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
		<CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
		<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
		<DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
		<HELPTOPIC><%= sRelURL2AbsURL("../docs/default.asp?helptopic=cs_bd_usersgrps_xapq.htm")%></HELPTOPIC>
		<EXPR-ASP><%=exprarch%></EXPR-ASP>
		<!-- The presence of the DEBUG node tells CB to output debug info. -->
		<DEBUG />
		<!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
		<PROFILE-LIST>
		<PROFILE SRC="<%=profileSRC	%>"></PROFILE>
		</PROFILE-LIST>
		<SITE-TERMS URL="<%=sitetermURL%>" />
		<MESSAGE><%= L_UpdateOrgValues_Text %></MESSAGE>
		<FILTER-PROPERTIES>not(@keyDef) $and$ (not(@isUpdatable) || @isUpdatable != '0') $and$ (not(@isReadOnly) || @isReadOnly != '1') $and$ (not(@isActive) || @isActive!= '0') $and$ (not(@isMultiValued) || @isMultiValued = '0') $and$ (@propType != 'BINARY')  $and$ (@propType != 'IMAGE')</FILTER-PROPERTIES>
		<TYPESOPS><%=typeops%></TYPESOPS>
		</EBCONFIG>
	</XML>
	
    <DIV ID="divQueryBldr" CLASS="clsQueryBldr"
      STYLE="behavior:url(/widgets/exprbldrhtc/QueryBuilder.htc); width:100%;" XMLCfgID="xmlisConfig" ONREADY="OnQBReady()" onQueryChange="EnableTaskMultiEdit()">
        <H3 ALIGN="center"><%= L_LoadingQB_Text %></H3>
        <%= L_CurrentQuery_Text %>
    </DIV>
	
<% End Sub %>
