<!--#INCLUDE FILE = '../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE = '../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE = 'constants.asp' -->
<!--#INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE = 'UOResources.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE = "../common/XMLRoutines.asp" -->
<!--#INCLUDE FILE = "../common/UserOrgListFns.asp" -->

<%
Dim iCurpage , iPrevPage
Dim sortCol , sortDir
Dim g_bIsADInstallation
Dim g_dRequest
Dim g_MSCSDefaultLocale, g_MSCSCurrencyLocale, g_sMSCSDateFormat, g_sMSCSWeekStartDay, g_sMSCSCurrencyFormat, g_sMSCSNumberFormat

' -- get locale info for Datafunctions and formats for widgets
g_MSCSDefaultLocale		= Application("MSCSDefaultLocale")
g_MSCSCurrencyLocale	= Application("MSCSCurrencyLocale")
g_sMSCSDateFormat		= Application("MSCSDateFormat")
g_sMSCSWeekStartDay		= Application("MSCSWeekStartDay")
g_sMSCSCurrencyFormat	= Application("MSCSCurrencyFormat")
g_sMSCSNumberFormat		= Application("MSCSNumberFormat")

set g_dRequest = dGetRequestXMLAsDict()

Rem Get the passed in values from the listsheet post
iCurPage = g_dRequest("page")
'iPrevPage = Session("PrevPage")
'Session("PrevPage") = iCurPage
sortCol = g_dRequest("sortcol")
sortDir = g_dRequest("sortdir")

Call Search()

Sub Search()
	Dim sFindBy
	Dim dState, aIDCol , query
	DIm rs,cn,bNoQuery,bOK
	Dim aprops
	Dim oXMLProfileDoc
	Set dState = dGetUMStateDictionary(g_sUMState)
	aprops = Session(g_sPropsLabel)
	set oXMLProfileDoc = Session(g_sProfileLabel)

	if IsEmpty(Session("ADInstallation")) then 
		g_bIsADInstallation = bIsAdInstallation(oXMLProfileDoc)
		Session("ADInstallation") = g_bIsADInstallation
	else
		g_bIsADInstallation =Session("ADInstallation") 
	end if	
	
	rem if (strcomp(g_dRequest("findbytype"), "advanced", 1) = 0) then
	if (strcomp(g_dRequest("selfindby"), "QueryBldr", 1) = 0) then
      dim oxml, sStr
      set oXML = Server.CreateObject("MSXML.DomDocument")
      sStr = g_dRequest("AdvQuery")
      oxml.loadxml("<XML>" & sStr & "</XML>")
      set Session(g_sAdvQueryLabel) = oXML'.DocumentElement.XML
  end if
 
	dState.Value("ChangeOrder") = False
	if (len(sortcol) > 0) then
	  if strcomp(sortcol, dState.Value("SortColumn"), 0) = 0 then
	      if (dState.Value("SortDirection") = "asc") then
	          dState.Value("SortDirection") = "desc"
	      else
	          dState.Value("SortDirection") = "asc"
	      end if
	  end if
	  dState.Value("SortColumn") = sortcol
	  if NOT isNull(sortdir) then
	      if len(sortdir) > 0 then
	          dState.Value("SortDirection") = sortdir
	      end if
	  end if
	  dState.Value("action") = "query"
	  dState.Value("ChangeOrder") = True
	end if
  
  rem sFindBy = trim(g_dRequest("findbytype"))
  sFindBy = trim(g_dRequest("selfindby"))
  If Left(sFindBy,2) = "fb" Then
		sFindBy = Right(sFindBy, Len(sFindBy)-2)
	End If	
	if len(sFindBy) > 0 then
	  dState.Value("action") = "query"
	  dState.Value("FindBy") = sFindBy
	  dState.Value("FindByText") = Trim(g_dRequest("fb" & sFindBy))
	  dState.Value("ChangeSearch") = True
	  	  
	  'If you run a new query, deselect all selected users
		rem ??? ClearUsers(dState)
	end if
	
	Call NewQuery(dState,aprops)

    set cn = cnGetProviderConnection(dState)

	if bIsDictEmpty(dState.Value("query")) then
	  bNoQuery = True
	else
	  query = dState.Value("query")

	  if Instr(1,LCase(query),"generalinfo.name") = 0 or Instr(1,LCase(query),"generalinfo.org_id")=0 then  
	      Call ThrowWarningDialog(L_OrgProfileKeyMisplaced_Errormessage)
	      Response.End
	  else
		  query = query & " ORDER by [GeneralInfo.Name] ASC"
		  dState.Value("query") = query
	  end if	  
	end if
    
	if NOT bNoQuery then 
		bOK = bSafeExec(rs, cn, Query)
		If Not bOK Then
		   ' Response.Write getErrorXML(Session("ErrorMessage"))
		    Call ThrowWarningDialog(Session("ErrorMessage") &  L_RefreshConnection_Text)
		Else
		   	Call WriteOutRS2XML(rs, (iCurPage -1) * g_iListPageSize )
		End If	
	else
		Call WriteBlankXML()
	end if
	Set rs = Nothing
	Set cn = Nothing	 	
End Sub

Sub WriteOutRS2XML(rsQuery, nStartRecord)
	Dim xmlOutPut
	Dim xmlNode
	Dim xmlEmptyRec
	Dim nrec
	
	Set xmlOutPut = xmlGetXMLFromRSEx(rsQuery, (iCurPage -1) * g_iListPageSize, g_iListPageSize, TOO_BIG_TO_COUNT, NULL)
	
	If Not xmlOutput Is Nothing then
		'filter out the sitename so that it doesn't get listed out as an organization
		Call xmlFilterSitename(xmlOutput)
		nrec = xmlOutPut.GetAttribute("recordcount")
		if nrec > 0 and Session("ADInstallation") = True then 
		 xmlOutPut.SetAttribute "recordcount" , nrec - 1 
		end if  
		Response.Write xmlOutPut.xml
	End if
end Sub


Sub WriteBlankXML()
Dim sXML
	sXML= "<document><record/></document>"
	Response.Write sXML
	
End Sub

Function getErrorXML(sErrorText)
  getErrorXML = "<document>"
  getErrorXML = getErrorXML & "<ERROR ID='0x" & hex(Err.number) & "' SOURCE='" & sErrorText & " - " & Err.source & "'>" & _
			"<![CDATA[" & Session("ErrorMessage") & "]]></ERROR>"
  getErrorXML = getErrorXML & "</document>"
  
  Err.Clear
End function

%>