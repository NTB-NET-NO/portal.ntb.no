<%
'Constants for labeling Session variables, so that OM and OM won't interfere with one another

const g_sDictName     = "OrgState"
const g_sDELIMITER    = ";"
const g_iListPageSize = 20
const g_sListPage     = "orgs.asp"
const g_sEditPage     = "org_edit.asp"


const g_sEntityLabel		= "OMEntity"
const g_sProfileLabel	= "OMProfile"
const g_sKeyLabel			= "OMKey"
const g_sNameLabel		= "OMName"
const g_sPropsLabel		= "OMProps"
const g_sGroupsLabel		= "OMGroups"
const g_sIDColLabel		= "OMIDCol"
const g_sStateLabel		= "OMState"
const g_sGUIDListLabel	= "OMGUIDList"
const g_sAdvQueryLabel	= "OMAdvQuery"
const g_sUMState			= "Organization"

const XML_ATTR_ORGID		= "org_id"
const XML_ATTR_USERID	= "user_id"
const XML_ATTR_ORG_FRIENDLYNAME = "name"

const CURR_PROFILE		= "Profile Definitions.Organization"
const ORG_PROFILE			= "Organization"
const USER_PROFILE		= "UserObject"
const GROUP_PROFILE		= "ADGroup"
const REF_GROUP_PROFILE = "Profile Definitions.ADGroup"
const REF_USER_PROFILE	= "Profile Definitions.UserObject"
const REF_ORG_PROFILE	= "Profile Definitions.Organization"

%>