<SCRIPT LANGUAGE='VBScript'>
<!--

  Const L_NOTSAVED_TEXT = "Changes made, but not saved"
  Const L_PROMPTSAVE_TEXT = "Do you want to exit without saving changes?"
  Dim g_bDirty
  g_bDirty = False

sub OnChange()
	if esUser0.required then
	   DisableTask "save"
	else
	   EnableTask "save"
	   EnableTask "savenew"
	   EnableTask "saveback"
	   SetStatusText L_NOTSAVED_TEXT
	end if
	g_bDirty = true
 end sub

sub window_onload()
 dim sType
 sType = "<%=g_sType%>"
 if (sType ="add") OR (sType= "copy") OR (sType= "savenew") then
     esUser0.focus()
     profilesform.ISINSERT.value="True"
 end if
 
      
 DisableTask "save"
 DisableTask "savenew"
 DisableTask "saveback"
 g_bDirty = False
end sub
   
sub OnSaveNew()
  profilesform.type.value = "savenew"
  ShowWaitDisplay()
  profilesform.submit
End Sub


Function Submit_UpdateQuery()
  dim sStr
  sStr = divQueryBldr.GetExprBody()
  if (len(Trim(sStr)) = 0) OR IsNull(sStr) then
    MsgBox "<%= L_ConstructUpdateQuery_Text %>",, sFormatString("<%= L_PROFILE_DialogTitle %>",Array("<%=g_sUMState %>"))
    Submit_UpdateQuery = False
    EnableTask "back"
  else
    profilesform.AdvQuery.value = sStr
    Submit_UpdateQuery = True
    'profilesform.submit()
  end if
end Function

sub EnableTaskMultiEdit()
  dim sStr
  sStr = divQueryBldr.GetExprBody()
  if (len(Trim(sStr)) = 0) OR (strcomp(Trim(sStr), "null", 1) = 0) then
	DisableTask "save"
	DisableTask "saveback"
  else
	EnableTask "save"
	EnableTask "saveback"
  end if			
End Sub

sub OnQBReady()
	divQueryBldr.Activate(TRUE)
End Sub
  
  function bAllRequiredFilledIn(byRef sMessage)
		dim sFieldStr
    dim oXMLFields, oXML, oXMLField, oXMLTest, oXMLEl, sName, sStr
    dim bDone, sSep
    bDone = True
    sFieldStr = ""
    sSep = ""
    sMessage =""
			
    sStr = ".//field[@required='yes' $and$ $not$ @hide]"
    set oxmLFields = UserMeta0.DocumentElement.SelectNodes(sStr)
    for each oXMLField in oXMLFields
      sName = oXMLField.GetAttribute("id")
      set oTest = document.all(sName)
      if NOT oTest is Nothing then
        if len(oTest.Value) > 0 then
           bDone = bDone AND True
        else
          bDone = False
          sFieldStr = sFieldStr & sSep & sName
          sSep = ", "
        end if
      end if
    next
    If sMessage ="" Then 
			sMessage = sFormatString("<%= L_EnterRequiredField_Text %>", Array(sFieldStr))
		End If	
    bAllRequiredFilledIn = bDone
  End Function

  sub OnSave()
	Dim bSave
	bSave = True
	If strcomp(profilesform.bBulkUpdate.value, "True", 1) = 0 or CInt(profilesform.numitems.value) > 1 then
	  bSave = Submit_UpdateQuery()
	End If
	If bSave then 
		ShowWaitDisplay()
		profilesform.submit
	End If
  End Sub

  sub CallExtDialog(ByVal sProfileName, ByRef sVal)
      Dim sResponse, elOrgID, sOrgID
      Dim sProfile
      
      set elOrgID = esUser0.field("<%= XML_ATTR_ORGID%>")
	  sOrgID = elOrgID.value
		if len(trim(sOrgID)) > 0 or _
			(Mid(sProfileName, Instr(1, sProfileName, ".")+1) = "<%= ORG_PROFILE %>") then
				
			sProfile = Mid(sProfileName, Instr(1, sProfileName, ".") + 1)  
			sVal = window.showModalDialog("dlg_ObjManager.asp?Profile=" & sProfileName & _ 
		        "&OrgID=" & sOrgID & "&CurrentSel=" & sVal, _
		        sProfile, "status:no;help:no")
		else        
		  sVal = ""
		   alert "<%= L_SaveOrganization_Text %>"
		end if
      
  end sub
    
	sub onBrowse()
    dim sName, sVal
    dim sNameOrig
    dim retArgs
   
    sName = window.event.srcElement.id
    nlen = InStr(1, sName, "_browse")
	 sNameOrig = Left(sName, nlen - 1)
    
    sVal = esUser0.field(sNameOrig).value
    SELECT case (lcase(sNameOrig))
			<%= sHTMLCase %>
'sHTMLCase contains Case Statements like the following.
'     case "defaultblanketponumber"
'				call CallExtDialog("Profile Definitions.BlanketPOs", sVal)
'     case "defaultcostcenternumber"
'				call CallExtDialog("CostCenter")
		   case else
		End Select
		if (len(trim(sVal)) > 0) AND (lcase(sVal) <> "nullchoice") then
			retArgs = split(sVal , "|||")
			esUser0.field(sNameOrig).value = retArgs(0) rem the underlying value
			esUser0.field(sName).value = retArgs(1) rem the text value
		elseif lcase(sVal) = "nullchoice" then
		  document.all(window.event.srcElement.id).value = ""		
		end if
	end sub
    

sub OnClose ()
	closeform.submit()
end sub

 -->
 </SCRIPT>