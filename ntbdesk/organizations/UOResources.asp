<%
'LOC: This file	contains all localizable strings for the Organizations module


const L_PageTitleOrganization_Text			= "Organizations"
const L_ErrorMessageTitle_Text				= "Organization"

Rem Error Messages

const L_DELETE_DialogTitle			= "Are you sure you want to Delete the following Organization(s)?"
const L_QUERY_DialogTitle			= "Execute Query"
const L_GROUPS_DialogTitle			= "Create Groups"
const L_PERMISSIONS_DialogTitle		= "Set Group Permissions"
const L_OPEN_DialogTitle			= "Open Organization"
const L_DeleteOperation_ErrorMessage = "Could not delete organization."
const L_OrgContainsUsers_ErrorMessage				= " Cannot delete organization.This organization contains users underneath it. Please delete the users first."
const L_Delete_ErrorMessage							= "An error occurred while deleting one or more organization records."
const L_NoSelection_ErrorMessage					= "The selection could not be processed."
const L_ExecFail_ErrorMessage						= "An error occurred while executing the query."
const L_SearchFail_ErrorMessage						= "An error occurred while executing the search query."
const L_RetrievesAMAccountNameFail_ErrorMessage		= "An error occurred while retrieving the sAMAccountName for the Group."
const L_ReadDSNFail_ErrorMessage					= "An error occurred while retrieving the connection information from the application configuration."
const L_MSCSUPSInitializeFail_ErrorMessage			= "An error occurred while initializing the profile service object."
const L_MSCSUPSGetProfileFail_ErrorMessage			= "An error occurred while retrieving the profile."
const L_MSCSUPSGetSecObjectFail_ErrorMessage		= "An error occurred while retrieving the security descriptor object."
const L_SetTrusteeFail_ErrorMessage					= "An error occurred while adding the ACL. May have insufficient permissions on AD domain."
const L_UpdateSecDesc_ErrorMessage					= "An error occurred while adding the ACL. Update Security Descriptor object failed."
const L_MSCSUPSUpdateFail_ErrorMessage				= "An error occurred while updating the profile."
const L_OrgFetchFail_ErrorMessage					= "An error occurred while retrieving this organization."
const L_CopyOrgFail_ErrorMessage					= "An error occurred while copying the organization properties."
const L_SaveOrgFail_ErrorMessage					= "An error occurred while saving the organization record."



const L_SelectAll_Text			= "All organizations selected"
const L_NoSelections_Text		= "No organizations selected"
const L_OneSelection_Text		= "One organization  selected [%1]"
const L_MultiSelection_Text		= "Update %1 selected organizations"
const L_UpdateOrg_Text			= "Update " 

const L_ChooseOrg_Text	        = "Choose the Organization this user belongs to:"
const L_ChooseUser_Text         = "Choose an existing user:"


const L_SearchOrg_Text			= "Search for organizations based on:"
const L_UpdateOrgValues_Text	= "Click NEW to create Update Queries"
const L_SaveOrganization_Text	= "You need to save this organization first."
const L_ConfirmDelete_Text		= "Delete %1?"
const L_ConfirmDeleteAllOrgs_Text =	 "Delete All Organizations?"

const L_UsersGroup_Text	 = "Users"
const L_ManageUsers_Text = "Manage Users"

%>
