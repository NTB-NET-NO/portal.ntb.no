
<SCRIPT LANGUAGE='VBScript'>
<!--
Dim    oSelection, oSortCol, bSortAscending
Dim g_bQBReady

g_bQBReady = False
bSortAscending = true	

Sub OnSelectAllRows()
  ' enable  d(elete) button on the task bar
  On Error Resume Next
	EnableTask "open"
	DisableTask "copy"
		
    ' build query here ???
	selectform.elements.friendlyname.value = ""
	deleteform.elements.friendlyname.value = ""
	copyform.elements.friendlyname.value = ""
	selectform.elements.<%= g_sKey %>.value = ""
	deleteform.elements.<%= g_sKey %>.value = ""
	copyform.elements.<%= g_sKey %>.value = ""
	  
	If <%= g_bIsAdInstallation %> Then
	    deleteform.elements.SelType.value = ""
	    deleteform.elements.Query.value = ""
		DisableTask "delete"
	Else
	    deleteform.elements.SelType.value = "All"
		deleteform.elements.Query.value =  "<%= g_sQuery%>"
		EnableTask "delete"
	End If	
		selectform.elements.SelType.value = "All"
		selectform.elements.Query.value =  "<%= g_sQuery%>"
		selectform.elements.numitems.value = 0 
		deleteform.elements.numitems.value = 0 

		setStatusText "<%= L_SelectAll_Text %>"
	On Error Goto 0	
End sub
    
Sub OnUnSelectAllRows()
   ' disable  d(elete) button on the task bar
	DisableTask "delete"
	DisableTask "open"
	DisableTask "copy"
    On Error Resume Next		
    selectform.elements.friendlyname.value = ""
    deleteform.elements.friendlyname.value = ""
    copyform.elements.friendlyname.value = ""
    selectform.elements.<%= g_sKey %>.value = ""
    deleteform.elements.<%= g_sKey %>.value = ""
    copyform.elements.<%= g_sKey %>.value = ""
	  
  ' clear the value for the item key in the selection form
	deleteform.elements.SelType.value = ""
	deleteform.elements.Query.value =  ""
	selectform.elements.SelType.value = ""
	selectform.elements.Query.value =  ""
	selectform.elements.numitems.value = 0
	deleteform.elements.numitems.value = 0
	' display the status text
    setStatusText "<%= L_NoSelections_Text %>"
    On Error Goto 0
End sub  	
	
	
Sub OnRowUnselect()
	Dim nitems, sRest, sName, ipos, oSelection, sleft, sright
	Set oSelection = window.event.XMLrecord
	On Error Resume Next	
	deleteform.elements.SelType.value = ""
	deleteform.elements.Query.value =  ""
	selectform.elements.SelType.value = ""
	selectform.elements.Query.value =  ""
	nitems = selectform.elements.numitems.value
	If nitems > 0 Then nitems = nitems-1
	selectform.elements.numitems.value = nitems
	deleteform.elements.numitems.value = nitems 
		
	sKeyValue = trim(oSelection.selectSingleNode("<%= g_sKey %>").text)
	sName = trim(oSelection.selectSingleNode("<%= g_sName %>").text)
    If nitems = 1 then
		EnableTask "copy"
	Else
		DisableTask "copy"
	End If
	If nitems = 0 Then 
	    selectform.elements.friendlyname.value = ""
	    deleteform.elements.friendlyname.value = ""
	    copyform.elements.friendlyname.value = ""
	    selectform.elements.<%= g_sKey %>.value = ""
	    deleteform.elements.<%= g_sKey %>.value = ""
	    copyform.elements.<%= g_sKey %>.value = ""
	' disable o(pen) and d(elete) buttons on the task bar
	    DisableTask "open"
	    DisableTask "delete"
	Else
	    sRest = trim(selectform.elements.<%= g_sKey %>.value)
	    sRest = sDeleteNameFromField(sKeyValue, sRest)
	    selectform.elements.<%= g_sKey %>.value = sRest
	    deleteform.elements.<%= g_sKey %>.value = sRest
	    copyform.elements.<%= g_sKey %>.value = sRest

	    sRest = trim(selectform.elements.friendlyname.value)
	    sRest = sDeleteNameFromField(sName, sRest)
	    selectform.elements.friendlyname.value = sRest
	    deleteform.elements.friendlyname.value = sRest
	    copyform.elements.friendlyname.value = sRest                    
	End If
		' display the status text
	Select case(nitems)
	   case (0) setStatusText "<%= L_NoSelections_Text %>"
	   case (1) setStatusText sFormatString("<%= L_OneSelection_Text %>", Array(sRest))
	   case else setStatusText sFormatString("<%= L_MultiSelection_Text %>", Array(nitems))
	End select
	On Error Goto 0
End sub

Function sDeleteNameFromField(sName, sField)
    Dim ipos, sleft, sright
    ipos = Instr(1, sField, sName)
    If (ipos > 1) Then
        sleft = Left(sField,ipos-2)
        sright = Mid(sField, ipos+len(sName))
        sDeleteNameFromField = sleft&sright
    Else
        sDeleteNameFromField = Mid(sField, 2+len(sName))
    End if
End Function

Function sAddName2Field(sName, sField)
   If (len(sField) > 0) then
      sAddName2Field = sField & ";" & sName
   Else
      sAddName2Field = sName
   End if
End Function

Sub OnSelectRow()
    Dim oSelection, sKeyValue, sName, nitems, sRest
    set oSelection = window.event.XMLrecord
    On Error Resume Next
    ' enable o(pen) and d(elete) buttons on the task bar
    EnableTask "open"
    EnableTask "delete"

    ' get the key value
    ' NOTE:
    '    oSelection.firstChild.nodeName                would return the first column name
    '    oSelection.childNodes(0).nodeName            is the same as oSelection.firstChild.nodeName
    '    oSelection.firstChild.text                    would return the first column value (contained text)
    '    oSelection.selectSingleNode("./name").text    would return the value for the column named "name"
    sKeyValue = trim(oSelection.selectSingleNode("./<%= g_sKey %>").text)
    sName = trim(oSelection.selectSingleNode("./<%= g_sName %>").text)
	
	deleteform.elements.SelType.value = ""
	deleteform.elements.Query.value =  ""
	selectform.elements.SelType.value = ""
	selectform.elements.Query.value =  ""

    nitems = selectform.elements.numitems.value
    nitems = nitems+1
    If nitems = 1 Then
		EnableTask "copy"
	Else
		DisableTask "copy"
	End If
				
    selectform.elements.numitems.value = nitems 
    deleteform.elements.numitems.value = nitems 

    ' add the value for the item key to the selection form
    sRest = trim(selectform.elements.<%= g_sKey %>.value)
    selectform.elements.<%= g_sKey %>.value = sAddName2Field(sKeyValue, sRest)
    deleteform.elements.<%= g_sKey %>.value = selectform.elements.<%= g_sKey %>.value
	copyform.elements.<%= g_sKey %>.value = sKeyValue
		
    sRest = trim(selectform.elements.friendlyname.value)
    selectform.elements.friendlyname.value = sAddName2Field(sName, sRest)
    deleteform.elements.friendlyname.value = selectform.elements.friendlyname.value
    copyform.elements.friendlyname.value = sName
		
    ' display the status text
    If (nitems = 1) Then
        setStatusText  sFormatString("<%= L_OneSelection_Text %>", Array(sName))
    Else
        setStatusText sFormatString("<%= L_MultiSelection_Text %>", Array(nitems))
    End if        
    On Error Goto 0
End sub
    
Sub ShowAddDialog(sEntity, sBasedOn)
	Dim sResponse
	'alert ("In ShoqAdd")
	If sBasedOn = "" Then
		addform.elements.<%= g_sKey %>.value = -1
		addform.submit()
		Exit Sub
	End If
	sResponse = CInt(window.showModalDialog("dlg_AddEntity.asp", _
	sEntity & "|" & sBasedOn, "dialogHeight:180px;dialogWidth:400px;status:no;help:no"))
	'alert(sResponse)
	Select case sResponse
		case "-1"
			window.event.returnValue = false
			EnableTask "new"
			EnableTask "open"
			EnableTask "delete"
		case "0"
			addform.elements.<%= g_sKey %>.value = -1
			addform.submit()
		case else
			addform.elements.<%= g_sKey %>.value = selectform.elements.<%= g_sKey %>.value
			addform.submit()
	End select
End sub
    
Sub ShowDelDialog(sEntity, sBasedOn, sFriendlyName)
      Dim iRetVal
      Dim sMsgString 
	  
	  If Len(sFriendlyName) <> 0 Then
		sMsgString = Replace("<%= L_ConfirmDelete_Text %>", "%1", sFriendlyName)
	  Else
		sMsgString = "<%= L_ConfirmDeleteAllOrgs_Text  %>"
	  End if	
      iRetVal = MsgBox(sMsgString,vbYesNo,"<%= L_DELETE_DialogTitle %>")
      rem showErrorDlg(strErrorTitle, strFriendlyError, strErrorDetails, nErrorType
      If (IRetVal = vbYes) Then
		  ShowWaitDisplay()
          deleteform.submit()
      Else
			EnableTask "delete"
			EnableTask "new"
			EnableTask "find"
			EnableTask "open"
      End if
End sub
	
Sub window_OnLoad()
  ' disable o(pen) and d(elete) buttons on the task bar
  EnableTask "new"
  Enabletask "find"
  DisplayGroup
	rem call hideFindBy when the page loads
  rem hideFindBy()
End sub

Sub DisplayGroup()
	Dim i, str, obj, sOpt
	rem sOpt = document.all.selfindby.value
	sOpt = selfindby.value
	Select case (Lcase(sOpt))
		case "querybldr"
			HideAllDivs()
			setFindByHeight(250)
			If g_bQbReady Then
				divfbGeneral.style.display = "block"
				'taCurrQuery.style.display = ""
				divQueryBldr.style.display = "block"
				divQueryBldr.SetExprBody("")
				divQueryBldr.Activate(TRUE)
			Else
				alert "<%= L_QueryBuilder_ErrorMessage %>"
			End if
		case else
			HideAllDivs()
			setFindByHeight(150)
			divQueryBldr.Deactivate()
			searchform.AdvQuery.value = ""
			divfbGeneral.style.display = "block"
			rem divQueryBldr.style.display = "none"
			If sOpt <> "" Then 
				str = "DIVfb" & sOpt
				document.all(str).style.display = "block"
			End If	
	End select
	showfindby()
End sub

Sub HideAllDivs()
	Dim node
	For each node in searchform.children
		if (Lcase(node.tagname) = "div") then
			node.style.display = "none"
		end if	
    Next
End Sub

Sub reloadListSheet()
	Dim bReload, sOpt
	sOpt = selfindby.value
	Select case sOpt
		case "QueryBldr"
			bReload = Submit_AdvQuery()
		case else
			bReload = find_form()
	End select
	If bReload Then
		OnUnSelectAllRows()
		lsProfiles.page = 1
		lsProfiles.reload("search")
	End If	
End sub

Function Find_Form()
	Dim  sOpt, sSearchText
	sOpt = selfindby.value
	If Len(Trim(sOpt)) = 0 Then
		alert "<%= L_SelectFindOption_Text %>"
		Find_Form = false
		Exit Function
	End If
	sSearchText = document.all("fb" & sOpt).value
	If (len(sSearchText) > 0 ) Then
	' AddStateTags(oCurrent)
		Find_Form = true
	Else
		alert "<%= L_EnterFindOption_Text %>"
		Find_Form = false
	End If
End Function

Function Submit_AdvQuery()
	Dim oCurrent, oXmL, oXMLHTTP, sStr
	sStr = divQueryBldr.GetExprBody()
	If (len(Trim(sStr)) = 0) OR IsNull(sStr) Then
		alert "<%= L_ConstructSearchQuery_Text %>"
		Submit_AdvQuery = False
	Else
		searchform.AdvQuery.value = sStr
		rem BUG oCurrent.submit()
		Submit_AdvQuery = True
	End If
End Function

Sub OnNewSort(oEvent)
	Dim oForm, child
	selectform.sortcol.value = oEvent.sortcol
	If (len(selectform.sortcol.value) > 0) Then
		selectform.sortdir.value = oEvent.sortdir
	End If
	selectform.action = "<%=g_sListpage%>"
	selectform.submit()
end sub

Sub OnNewPage(oEvent)
   Dim oForm, block
   block = selectform.lock.value
   If Not block Then
	   selectform.lock.value ="true"
       selectform.type.value="New Page"
       selectform.newpage.value = oEvent.page
       selectform.action = "<%=g_sListpage%>"
       selectform.submit()
   End If
End Sub

Function bAlreadyHaveQB()
   Dim elOpt
   For Each elOpt In searchform.findby.options
		If (strcomp(elOpt.Value, "QueryBldr", 1) = 0) Then
		  bAlreadyHaveQB = True
		  Exit Function
		End if
   Next
   bAlreadyHaveQB = False
End Function


Sub OnQBReady()
	Dim elOption
    If (g_bQbReady = False) Then
        g_bQbReady = True
        If strcomp(selfindby.value, "QueryBldr", 1) = 0 Then
            call DisplayGroup
        End if
    End if
End Sub


-->
</SCRIPT>