<!--#INCLUDE FILE ="../include/BDHeader.asp" -->
<!--#INCLUDE FILE ='constants.asp' -->
<!--#INCLUDE FILE ="../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE ='UOResources.asp' -->
<!--#INCLUDE FILE ='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE ='../common/XMLroutines.asp' -->
<!--#INCLUDE FILE ='../common/UserOrgEditFns.asp' --> 
<!--#INCLUDE FILE ="../common/Aux_Common.asp" -->

<%
Dim g_bIsAdInstallation
Dim g_sKey
Dim g_sName


Call Main


Sub MakeInserts(oXMLRecs, oXMLProfile, dState, sOrgID, oGen)
    dim i, oXMLRec, sProfName, sQuery, oXMLKey, oXMLKeys, oXMLProp, oXMLProps, oXMLBase
    dim sProps, sVals, sProp, sVal, sType, rs, cn, sSep, bdum
    set oXMLBase = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProfile)
    sProfName = oXMLBase.GetAttribute(sXMLAttrName)
    sQuery = "INSERT INTO " & sBracket(sProfName)
	'Separate the Properties in this profile into the unique keys and ordinary properties
    set oXMLKeys = oXMLBase.SelectNodes(".//" & sXMLTagProperty & _
        "[@" & sXMLAttrKeyDef & "!='']")
    set oXMLProps = oXMLBase.SelectNodes(".//" & sXMLTagProperty & _
        "[$not$ @" & sXMLAttrKeyDef & " ]")
	'Loop over all the records
    dim icnt
    icnt = 0
    set cn = cnGetProviderConnection(dState)
    for each oXMLRec in oXMLRecs
        icnt = icnt + 1
        oXMLRec.SelectSingleNode("./org_id").text = sOrgID
        sProps = "("
        sSep = ""
        sVals = "("
        call FormInsQuery(oXMLRec, oXMLKeys, oXMLProps, oGen, sProps, sVals, sSep,dState)

        if (len(sSep) > 0) then
				sQuery = sQuery & sProps & ") VALUES " & sVals & ")"
            bdum = bExecWithErrDisplay(rs, cn, sQuery, _
								sFormatString(L_InsertEntity_ErrorMessage,Array(sProfName)))
        end if
    next
    set cn = Nothing
End Sub

Sub FormInsQuery(ByVal oXMLRec, ByVal oXMLKeys, ByVal oXMLProps, oGen, byRef sProps, byRef sVals, byRef sSep, ByVal dState)
    dim sVal, sType, oXMLKey, oXMLProp
        for each oXMLKey in oXMLKeys
            sVal = oXMLRec.SelectSingleNode("./" & oXMLKey.GetAttribute(sXMLAttrName)).Text
            sType = oXMLKey.GetAttribute(sXMLAttrTypeDefName)
            if len(Trim(sVal)) = 0 then
                if lcase(sType) = "string" then
                    sVal = sBracketGuid(oGen.GenGUIDString())
                end if
            end if
            if len(Trim(sVal)) > 0 then
                sProps = sProps & sSep & sGetFullPathName(oXMLKey, True)
                sVals = sVals & sSep & sQuoteArg(sVal, sType)
                sSep = ", "
            end if
        next
        for each oXMLProp in oXMLProps
            sVal = oXMLRec.SelectSingleNode("./" & oXMLProp.GetAttribute(sXMLAttrName)).Text
            sType = oXMLProp.GetAttribute(sXMLAttrTypeDefName)
            If g_bIsADInstallation and sGetFullPathName(oXMLProp, False) = g_sParentURL Then
							sVal = sGetParentDN(dState)
            End If
            If len(Trim(sVal)) > 0 then
                sProps = sProps & sSep & sGetFullPathName(oXMLProp, True)
                sVals = sVals & sSep & sQuoteArg(sVal, sType)
                sSep = ", "
            end if
        next
End Sub

Sub MakeDeletes(oXMLRecs, oXMLProfile, dState)
    dim bdum, sQuery, oXMLPK, oXMLBase, sProfName, rs, cn, sPKName, sPKCol
    dim sVal, sType, oXMLRec
    set oXMLBase = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProfile)
    sProfName = oXMLBase.GetAttribute(sXMLAttrName)
    set oXMLPK = oXMLBase.SelectSingleNode(".//"& sXMLTagProperty & _
    "[@" & sXMLAttrKeyDef & "='PRIMARY'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']")
    sPKCol = oXMLPK.GetAttribute(sXMLAttrName)
    sType = oXMLPK.GetAttribute(sXMLAttrTypeDefName)
    sPKName = sGetFullPathName(oXMLPK, TRUE)
    sQuery = "DELETE FROM " & sProfName & " WHERE " & sPKNAME & " = "
    set cn = cnGetProviderConnection(dState)
    for each oXMLRec in oXMLRecs
        sVal = oXMLRec.SelectSingleNode("./" & sPKCol).Text
        sQuery = sQuery & sQuoteArg(sVal, sType)
        If g_bIsADInstallation Then
					sQuery = sQuery & " AND " & sBracket(g_sParentURL) & " = '" & _
									 sGetParentDN(dState) & "'"
        End If
        bdum = bExecWithErrDisplay(rs, cn, sQuery, _
						sFormatString(L_DeleteEntity_ErrorMessage,Array(sProfName)))
    next
    set cn = Nothing
End Sub

Sub MakeUpdates(oXMLRecs, oXMLProfile, dState, sOrgID, oGen)
    dim i, oXMLRec, sProfName, sQuery, oXMLPK, oXMLJK, oXMLProp, oXMLProps, oXMLBase
    dim sProps, sVals, sProp, sVal, sType, rs, cn, sSep, bdum, sWhere, sPK, sPKType
    dim icnt, sPKName, sInsQuery
    dim sSel, oXMLKeys, sJK, sOldJK, sJKType, sJKName, sDel


    set oXMLBase = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProfile)
    sProfName = oXMLBase.GetAttribute(sXMLAttrName)
    sInsQuery = "INSERT INTO "  & sBracket(sProfName) & " " 
    sQuery = "UPDATE " & sBracket(sProfName) & " SET " 
	'Separate the Properties in this profile into the unique keys and ordinary properties
    set oXMLPK = oXMLBase.SelectSingleNode(".//" & sXMLTagProperty & _
        "[@" & sXMLAttrKeyDef & "='PRIMARY'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']")
    sPKName = sGetFUllPathName(oXMLPK, True)
    sWhere = " WHERE " & sPKName & " = "
    sPK = oXMLPK.GetAttribute(sXMLAttrName)
    sPKType = oXMLPK.GetAttribute(sXMLAttrTypeDefName)
    set oXMLJK = oXMLBase.SelectSingleNode(".//" & sXMLTagProperty & _
       "[@" & sXMLAttrKeyDef & "='JOIN'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']")
    sJK = oXMLJK.GetAttribute(sXMLAttrName)
    sJKType = oXMLJK.GetAttribute(sXMLAttrTypeDefName)
    sJKName = sGetFUllPathName(oXMLJK, True)

    set oXMLProps = oXMLBase.SelectNodes(".//" & sXMLTagProperty & _
        "[$not$ @" & sXMLAttrKeyDef & " ]")
	'Loop over all the records
    set cn = cnGetProviderConnection(dState)
    for each oXMLRec in oXMLRecs
	'Right now there is no way to keep the user from changing the Primary Key value in the ListEditor
	'Widget. However doing an update where the PK is changed is not allowed in AD and some DBs, though
	'SQL Server does allow it.
	'To be consistent, we should do a query to see if a Record with this PK value exists
	'If it does, do the update.
	'If not, Delete the old one, and insert another one.
        sSel = "SELECT " & sPKName & " FROM " & sBracket(sProfname) & " WHERE " & sPKName & _
            " = " & sQuoteArg(oXMLRec.SelectSingleNode("./" & sPK).Text, sPKType) & _
            " ORDER BY " & sPKName

        bdum = bSafeExec(rs, cn, sSel)
        if rs.EOF then
	'There is no record in the DB with this PK Value. So we have to insert it

            set rs = Nothing

            oXMLRec.SelectSingleNode("./org_id").text = sOrgID
	'Delete any existing records with Old Join Key value 
            sOldJK = oXMLRec.SelectSingleNode("./" & sJK).text
            sDel = "DELETE FROM " & sBracket(sProfName) & " WHERE " & _
                sJKName & " = " & sQuoteArg(sOldJK, sJKType)
            bdum = bSafeExec(rs, cn, sDel)
	'Empty out the JK value if it is hidden so that the insert will generate a new value for it
            if (oXMLJK.GetAttribute(sXMLAttrisActive) = "0") then
                oXMLRec.SelectSingleNode("./" & sJK).text = ""
            end if
	'Form Insert statement and execute it
            set oXMLKeys = oXMLBase.SelectNodes(".//" & sXMLTagProperty & _
                "[@" & sXMLAttrKeyDef & "!='']")
            sProps = "("
            sSep = ""
            sVals = "("
            call FormInsQuery(oXMLRec, oXMLKeys, oXMLProps, oGen, sProps, sVals, sSep,dState)
            if (len(sSep) > 0) then
                bdum = bSafeExec(rs, cn, sInsQuery & sProps & ") VALUES " & sVals & ")")
            end if
        else

	'If a record is there with the same value of PK, it's okay to update it.
	'
	'INSERT statement is of form INSERT INTO [Profile] (Property1, Property2, ..., PropertyN)
	'VALUES(Value1, Value2, ..., ValueN)
	'Need to make sure that both the Join Key and the Primary Key Properties have values
	'If one of them is not set, is of string type, and isActive = 0 then we place a GUID in it.
            for each oXMLProp in oXMLProps
                sVal = oXMLRec.SelectSingleNode("./" & oXMLProp.GetAttribute(sXMLAttrName)).Text
                sType = oXMLProp.GetAttribute(sXMLAttrTypeDefName)
								If g_bIsADInstallation and sGetFullPathName(oXMLProp, False) = g_sParentURL Then
									sVal = sGetParentDN(dState)
								End If
                if len(Trim(sVal)) > 0 then
                    sVals = sVals & sSep & sGetFullPathName(oXMLProp, True)
                    sVals = sVals & " = " & sQuoteArg(sVal, sType)
                    sSep = ", "
                end if
            next
            if (len(sSep) > 0) then
                sVal = oXMLRec.SelectSingleNode("./" & sPK).Text
                bdum = bSafeExec(rs, cn, sQuery & sVals & sWhere & sQuoteArg(sVal, sPKType) )
            end if
        end if
    next
    set cn = Nothing
End Sub

Function bGetMultiValued(oXML)
	Dim attr
	bGetMultiValued = False
	attr = sGetAttribute(oXML,sXMLAttrIsMultiValued)
	If attr = "1" Then
		bGetMultiValued = True
	End If
End Function

Function rsMakeQuery(sOrgID, sRef, dState, oXML)
	'Gets all the information needed later about CostCenters and puts it into a XMLDomDocument
    dim sQuery, oXMLPropList, oXMLProp, sPKName, sPKStr, oXMLPK, bNeedComma
    dim sOrgIDName, sOrgStr, oXMLOrgID, sProfileName, cn, sPropName, bMultiValued
     
	'Find the primary Key for this profile and put its name in sPKName, for OrderBy Clause needed
	'by Provider
    sPKStr = ".//" & sXMLTagProperty & "[@" & sXMLAttrKeyDef & "='PRIMARY'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']"
    sOrgStr = ".//" & sXMLTagProperty & "[@" & sXMLAttrName & "='" & XML_ATTR_ORGID & "']"
    set oXMLPK = oXML.DocumentElement.SelectSingleNode(sPKStr)
    sPKName = sGetFullPathName(oXMLPK, True)
    set oXMLOrgID = oXML.DocumentElement.SelectSingleNode(sOrgStr)
    sOrgIDName = sGetFullPathName(oXMLOrgID, True)
    sQuery = "SELECT "
    bNeedComma = False
	'Loop over all the properties in the Profile
    set oXMLPropList = oXML.DocumentElement.SelectNodes(".//" & sXMLTagProperty)
    for each oXMLProp in oXMLPropList
        sPropName = sGetFullPathName(oXMLProp, True)
        bMultiValued = bGetMultiValued(oXMLProp)
        If Not bMultiValued Then
					if True = bNeedComma then
					    sQuery = sQuery & ", " & sPropName
					else
					    bNeedComma = True
					    sQuery = sQuery & sPropName
					end if
				End If	
    next
    'Extract the Profile Name from a string of form "CatName.ProfileName" Should work also for
    'string of form "ProfileName"
    sProfileName = Right(sRef, len(sRef) - Instr(1, sRef, "."))
    sQuery = sQuery & " FROM " & sBracket(sProfileName) & " WHERE "
    sQuery = sQuery  & sOrgIDName & " = '" & sOrgID & "'"
	'sQuery = sQuery & " ORDER BY " & sPKName
	  if  g_bIsAdInstallation then 
			sQuery = sQuery & " AND " & sBracket(g_sParentURL) & " = '" & _
					sGetParentDN(dState) & "'"
		end if
		set cn = cnGetProviderConnection(dState)
    set rsMakeQuery = cn.Execute(sQuery)
	set cn = nothing
	set oXMLPropList = nothing
	set oXMLProp = nothing
	set oXMLPK = nothing
	set oXMLOrgID = nothing
End Function

Function sSafeNull(Value)
    if IsNull(Value) then
        sSafeNull = ""
    else
        sSafeNull = Value
    end if
End Function


Function oXMLSetupLEMeta()
    dim oXML, oXMLEl
    set oXML = oXMLMakeDocumentElement("listeditor")
    set oXMLEl = oXML.CreateElement("columns")
    oXML.DocumentElement.appendchild oXMLEl
    set oXMLEl = oXML.CreateElement("fields")
    oXML.DocumentElement.appendchild oXMLEl
    set oXMLSetupLEMeta = OXML
End Function


Sub AddXMLColumn(oXMLCols, sColName, bKey)
    dim oXMLCol
    set oXMLCol = oXMLCols.CreateElement("COLUMN")
    oXMLCol.SetAttribute "name", sColName
    if bkey then oXMLCol.SetAttribute "key", "yes"
    oXMLCols.DocumentElement.appendChild oXMLCol
End Sub

Sub MakeListEditorMeta(byVal dState, byVal sRef, byVal oXMLProfile, byRef oXMLDisplayCols)
    dim oXMLPropList, oXMLProp, oXMLMeta
    set oXMLDisplayCols = oXMLWhichColsToDisplay(sRef)
    set oXMLMeta = oXMLSetupLEMeta
    set oXMLPropList = oXMLProfile.DocumentElement.SelectNodes(".//" & sXMLTagProperty)
    for each oXMLProp in oXMLPropList
        call MakeLEField(oXMLMeta, oXMLProp, oXMLDisplayCols)
    next
'    oXMLMeta.Save("C:\LEMeta.xml")
    Response.Write "<XML id='LEMeta'>"
    Response.Write oXMLMeta.DocumentElement.xml
    Response.Write "</XML>"
End Sub

Function bGetBooleanAttr(oXMLProp, sAttrName)
    dim sVal
    sVal = sGetAttribute(oXMLProp, sAttrName)
    if len(Trim(sVal)) > 0 then
        if sVal = "0" then
            bGetBooleanAttr = False
        else
            bGetBooleanAttr = True
        end if
    else
        bGetBooleanAttr = False
    end if
End Function

'Default value for IsRequired = false
Function bGetBooleanAttrIsRequired(oXMLProp)
    dim sVal
    sVal = sGetAttribute(oXMLProp, sXMLAttrIsRequired)
    bGetBooleanAttrIsRequired = False
    If len(Trim(sVal)) > 0 then
      if sVal = "1" then _
        bGetBooleanAttrIsRequired = True
    Else
       bGetBooleanAttrIsRequired = False
    End If   

End Function

'Default value for IsActive = true
Function bGetBooleanAttrIsActive(oXMLProp)
    dim sVal
    sVal = sGetAttribute(oXMLProp, sXMLAttrIsActive)
     bGetBooleanAttrIsActive = True
    If len(Trim(sVal)) > 0 then
      if sVal = "0" then _
        bGetBooleanAttrIsActive = False
    Else
       bGetBooleanAttrIsActive = True
    End If   

End Function

'Default value for IsReadOnly = false
Function bGetBooleanAttrIsReadOnly(oXMLProp)
    dim sVal
    sVal = sGetAttribute(oXMLProp, sXMLAttrIsReadOnly)
    bGetBooleanAttrIsReadOnly = False
    If len(Trim(sVal)) > 0 then
      if sVal = "1" then _
        bGetBooleanAttrIsReadOnly = True
    Else
       bGetBooleanAttrIsReadOnly = False
    End If   

End Function

Function bDisplayColumn(sPropName, oXMLCols, bKey)

    dim oXMLCol, oXMLAt
    bKey = False
    set oXMLCol = oXMLCols.DocumentElement.SelectSingleNode("./COLUMN[@name='" & sPropName&"']")
    if oXMLCol is Nothing then
        bDisplayColumn = False
    else
        set oXMLAt = oXMLCol.attributes.GetnamedItem("key")
        if NOT (oXMLAt is Nothing) then
            bKey = True
            set oXMLAt = nothing
        end if
        bDisplayColumn = True
        set oXMLCol = nothing
    end if
End Function


Sub MakeLEField(oXMLMeta, oXMLProp, oXMLCols)
    
    const nwidth = 50 'In Percentage
    const nMaxLen = 50
    dim oXMLFields, oXMLColumns, oXMLColumn, oXMLField, oXMLEl, oXMLText
    dim bRequired, bHide, sType, sSubType, sDataType, bDisplay, sName, sPropName
    dim sHide, sReq, bKey, bReadOnly, sReadOnly
    
    bRequired = bGetBooleanAttrIsRequired(oXMLProp)
    bHide = NOT bGetBooleanAttrIsActive(oXMLProp)
    bReadOnly = bGetBooleanAttrIsReadOnly(oXMLProp)
    sDataType = Lcase(sGetAttribute(oXMLProp, sXMLAttrTypeDefName))
    sName = sGetAttribute(oXMLProp, sXMLAttrDisplayName)
    sPropName = sGetAttribute(oXMLProp, sXMLAttrName)
    bDisplay = bDisplayColumn(sPropName, oXMLCols, bKey)
 
    select case(sDataType)
        case "number"
            sType = "numeric"
            sSubType = "integer"
		  case "datetime", "date"
				sType = "date"   
        case else
            sType = "text"
            sSubType = "short"
    End Select

    if TRue = bHide then
        sHide = "yes"
    else
        sHide = "no"
    end if
    if  TRue = bRequired then
        sReq = "yes"
    else
        sreq = "no"
    end if
    if True = bReadOnly then
        sReadOnly = "yes"
    else
        sReadOnly = "no"
    end if
    set oXMLFields = oXMLMeta.DocumentElement.SelectSingleNode(".//fields")
    set oXMLField = oXMLMeta.CreateElement(sType)
    if (True = bDisplay) then
        set oXMLColumns = oXMLMeta.DocumentElement.SelectSingleNode(".//columns")
        set oXMLColumn = oXMLMeta.CreateElement("column")
        oXMLColumn.SetAttribute "id", sPropName
        if bKey then  oXMLColumn.SetAttribute "key", "yes"
        oXMLColumn.SetAttribute "hide", sHide
        oXMLColumn.SetAttribute "width", nwidth
        oXMLColumn.SetAttribute "readonly", sReadOnly
        set oXMLText = oXMLMeta.CreateTextNode(sName)
        oXMLColumn.appendchild oXMLText
        oXMLColumns.appendchild oXMLColumn
    end if
        oXMLField.SetAttribute "id", sPropName
        oXMLField.SetAttribute "hide", sHide
        oXMLField.SetAttribute "readonly", sReadOnly
        oXMLField.SetAttribute "required", sReq
        oXMLField.SetAttribute "maxlen", nMaxLen
        oXMLField.SetAttribute "type", sType
        oXMLField.SetAttribute "subtype", sSubType
'        if (bEnum) then
'            oXMLField.appendChild oXMLEnum.DocumentElement
'        end if
            
        call MakeLEFieldSubNode(oXMLMeta, oXMLField, "name", sName, False)
        call MakeLEFieldSubNode(oXMLMeta, oXMLField, "tooltip", "tooltip for " & sName, bHide)
        call MakeLEFieldSubNode(oXMLMeta, oXMLField, "charmask", "", bHide)
        call MakeLEFieldSubNode(oXMLMeta, oXMLField, "error", "error for " & sName, bHide)
        oXMLFields.appendchild oXMLField
End Sub

Sub MakeLEFieldSubNode(oXML, oXMLField, sNodeName, sStr, bHide)
    dim oXMLNode, oXMLText
    set oXMLNode = oXML.CreateElement(sNodeName)
    oXMLNode.SetAttribute "localize", "yes"
    if (bHide = False) then
        set oXMLText = oXML.CreateTextNode(sStr)
    else
        set oXMLText = oXML.CreateTextNode("")
    end if
    oXMLNode.appendchild oXMLText
    oXMLField.appendchild oXMLNode
End Sub


sub MakeListeditorData(byRef rs, byRef oXML)
'    dim oXML
    Response.Write "<XML id='LEData'>" 
    if rs.EOF then
        set oXML = oXMLMakeDocumentElement("document")
        oXML.DocumentElement.AppendChild oXML.CreateElement("record")
        Response.Write oXML.DocumentElement.xml
    else
        'set oXML = oXMLRS2XML3(rs)
        set oXML = xmlGetXMLFromRSEx(rs, 0, g_iListPageSize, 0, NULL)
        Response.Write oXML.DocumentElement.xml
    end if
    
    Response.Write "</XML>"
End Sub

Public Function oXMLRS2XML3(rsQuery)
	dim xmlDoc, xmlNode, fdField, xmlChildNode, bIsNewRecord, sValue, i, avalues
	dim dtmp, ddate, dtime
	set xmlDoc = CreateObject("MSXML.DOMDocument")
	set xmlNode = xmlDoc.createNode(1,"document","")
	set xmlDoc.documentElement = xmlNode
    
    do while NOT rsQuery.EOF
	    set xmlNode = xmlDoc.createNode(1, "record", "")
	    xmlDoc.documentElement.appendChild xmlNode
    	for each fdfield in rsQuery.fields
		    set xmlChildNode = xmlDoc.createNode(1, sStripGroup(fdField.name), "")
		    sValue = fdField.value
		    if not isNull(sValue) then
		    	xmlChildNode.text = sValue
		    end if
		    xmlNode.appendChild xmlChildNode
		 next
		 rsQuery.MoveNext
    loop
	set oXMLRS2XML3 = xmlDoc
end Function


Sub MakeSelectBox(byVal oXMLData, byVal oXMLProfile, byRef oXMLDisplayCols, byVal sCurrSel)

    dim oXMLRecs, oXMLRec, sHTML, sID, sName, sIDVal
    sHTML = ""'"<SELECT ID='ChooseEntity'>"
    sID = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty & "[@" & _
        sXMLAttrKeyDef & "='JOIN'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']").GetAttribute(sXMLAttrName)
    sName = oXMLDisplayCols.DocumentElement.SelectSingleNode(".//" & sXMLTagColumn & _
        "[@isFriendlyName='1']").GetAttribute(sXMLAttrName)
    '"PONumber"
    
    set oXMLRecs = oXMLData.DocumentElement.SelectNodes(".//record[" &_
        sID & " $and$ " & sName & "]")
    if oXMLRecs.Length > 0 then
        For each oXMLRec in oXMLRecs
            sIDVal = oXMLRec.SelectSingleNode("./"&sID).text
            if sIDVal = sCurrSel then
                sHTML = sHTML & vbcR & "<OPTION VALUE='" & sIDVal & "' SELECTED>" &_
                     oXMLRec.SelectSingleNode("./" & sName).text & "</OPTION>"
            else
                sHTML = sHTML & vbcR & "<OPTION VALUE='" & sIDVal & "'>" &_
                     oXMLRec.SelectSingleNode("./" & sName).text & "</OPTION>"
            end if
        next
        Response.Write vbCR & sHTML
    end if
End Sub


Sub WriteXMLMetaForUserOrg(sJK, sFriendly)
	
	Dim oXMLEL
	dim oXMLCols
	Dim oXML
	dim sXML
	Dim oXMLOps
	Dim oXMLOP
	set oXML = Server.CreateObject("MSXML.DOMDocument")
    
    sXML = "<xml id='profilesMeta'>" & vbCR
	set oXML = oXMLMakeDocumentElement("listsheet")
				
	set oXMLEL = oXML.CreateElement("global")
	oXMLEl.SetAttribute "pagesize", g_iListPageSize
	oXMLEl.SetAttribute "sort", "yes"
	oXMLEl.SetAttribute "selection", "single"
	oXML.DocumentElement.appendChild oXMLEl
				
	set oXMLCols = oXML.CreateElement("columns")
	oXML.DocumentElement.appendchild oXMLCols

	dim nSize

	set oXMLEl = oXML.CreateElement("column")
	rem The Join Key is always hidden, so size can be 0
	rem and key attribute is set to true
	oXMLEl.SetAttribute "id", sJK
	oXMLEl.SetAttribute "width", 0
	oXMLEl.SetAttribute "hide", "yes"
	oXMLEl.SetAttribute "key", "yes"
	oXMLEl.text = sJK
	oXMLCols.appendChild oXMLEl
	rem The Friendly name is displayed and the only column
	rem so size can be 100 ( this is in percentage )
	set oXMLEl = oXML.CreateElement("column")
	oXMLEl.SetAttribute "id", sFriendly
	oXMLEl.SetAttribute "width", 100
	'Displays Display Name instead of the friendly name .
	if sFriendly = "logon_name" then 
	    oXMLEl.text = L_UserOrgLogonName_Text
	else    
		oXMLEl.text = sFriendly
	end if
			
	oXMLCols.appendChild oXMLEl

			 
	Set oXMLOPs = oXML.CreateElement("operations")
	oXML.DocumentElement.appendchild oXMLOPs
	Dim arrOperations, arrForms, i
	arrOperations = Array("newpage", "sort")
	arrForms = Array("newpageform", "newpageform")
	For i=0 to Ubound(arrOperations)
		Set oXMLOP = oXML.CreateElement(arrOperations(i))
		oXMLOP.SetAttribute "formid", arrForms(i)
		oXMLOPs.appendchild oXMLOP
	Next
				
	Response.Write sXML & oXML.Documentelement.xml & "</xml>" & vbCR
End Sub

Sub Main

    Dim sForm, sOrgID, sEntityID, sRef, sPropName, sPageName
    Dim oXMLProfile, dEditState, rs, dState, fdTemp, oXML, oXMLInsRecs, oXMLUpdRecs, oXMLDelRecs
    Dim oXMLRec, oGen, oXMLData, oXMLMeta, oXMLDisplayCols, sCurrSel, bUserOrOrg
    Dim sSearch, sFriendly
    Dim rsAux
    Dim xmlOutput
	'If this is coming straight from the Editsheet page, then arguments have been sent through 
	'Request.QueryString
	'
	'Need to test to see if the profile is either the UserProfile or Org Profile. If it is
	'the page needs to act a little differently than "usual":
	'   1) No listeditor. Having a Listedit page for Users and Orgs would be too confusing and
	'      complicated because it duplicates the functionality of these modules.
	'   2) The Select box isn't filled in with all the users (for the current org) or Orgs 
	'      in the DB. Instead, you have a text box to enter an initial string that limits the
	'      users you see in the select.
	'
	
    bUserOrOrg = False
    if (Request.QueryString.Count > 0) then
        sRef = Request.QueryString("Profile")
        if (sRef = REF_USER_PROFILE) OR _
            (sRef = REF_ORG_PROFILE) then
            bUserOrOrg = True
            sSearch = ""
            sFriendly = sGetFriendlyName(sRef)
        end if
        sOrgID = Request.QueryString("OrgID")
        sCurrSel = Request.QueryString("CurrentSel")
	'Need to remove this debug code eventually
        set oXMLProfile = oXMLGetAuxProfile(sRef)
        
        if IsEmpty(Session("ADInstallation")) then 
		   g_bIsADInstallation = bIsAdInstallation(oXMLProfile)
		   Session("ADInstallation") = g_bIsADInstallation
		else
		   g_bIsADInstallation = Session("ADInstallation")
        end if  

        set dState = dGetUMStateDictionary(g_sUMState)
	'If we have the Form collection populated, then this page has submitted a form to itself
	'
    elseif Request.Form.Count > 0 then
	'Get Profile and OrgID from form

        sRef = Request.Form("elProfile")
        sOrgID = Request.Form("elOrgID")
        sCurrSel = Request.Form("elCurrSel")
        set oXMLProfile = oXMLGetAuxProfile(sRef)
        g_bIsAdInstallation = bIsAdInstallation(oXMLProfile)
        set dState = dGetUMStateDictionary(g_sUMState)
        if (sRef = REF_USER_PROFILE ) OR _
            (sRef = REF_ORG_PROFILE ) then
            bUserOrOrg = True
            sSearch = Request.Form("SearchString")
            sFriendly = sGetFriendlyName(sRef)
        else
	'This next block of code is all for setting up the list editor widget or handling the XML
	'it produced previously, so it shouldn't be run for User or Org profiles.        
        
    'Get the last XML structure that the ListEditor OnChange event fired before the submission
            set oXML = Server.CreateObject("MSXML.DomDocument")
            
            oXML.LoadXML(Request.Form("elRecords"))
    'Separate the records into those that will be Inserts, those that will be updates, and
    'those that will be deletes from the database
            set oGen = Server.CreateObject("Commerce.GenID")
            set oXMLInsRecs = oXML.DocumentElement.SelectNodes(".//record[@state='new']")
            set oXMLUpdRecs = oXML.DocumentElement.SelectNodes(".//record[@state='changed']")
            set oXMLDelRecs = oXML.DocumentElement.SelectNodes(".//record[@state='deleted']")
            if oXMLInsRecs.length > 0 then
                call MakeInserts(oXMLInsRecs, oXMLProfile, dState, sOrgID, oGen)
            end if
            if oXMLDelRecs.length > 0 Then
                call MakeDeletes(oXMLDelRecs, oXMLProfile, dState)
            end if        
            if oXMLUpdRecs.length > 0 Then
                call MakeUpdates(oXMLUpdRecs, oXMLProfile, dState, sOrgID, oGen)
            end if
            set oGen = Nothing
        end if
    else
	'This page is not designed to be called without forms or Querystring arguments
        Response.Write L_WrongInvoke_ErrorMessage
        Response.End
    end if
    sPageName = Request.ServerVariables("SCRIPT_NAME")
    If (Instr(1, sPagename, "?") > 0) Then
        sPageName = Left(sPagename, Instr(1, sPagename, "?") -1)
    End If
    
    
    If sRef = REF_USER_PROFILE Then 
	    g_sKey = XML_ATTR_USERID
	ElseIf sRef = REF_ORG_PROFILE Then
	    g_sKey = XML_ATTR_ORGID
    Else
        g_sKey = sGetJoinKeyName(oXMLProfile)
   	End If 
	    
   g_sName = sFriendly
   
   If bUserOrOrg then
      If Len(sSearch) > 0 then 
		  Session("UserSearchString") = sSearch
		  Set rsAux = rsMakeUserOrgList(sOrgID, sRef, dState, oXMLProfile, sSearch, sCurrSel, sFriendly)
      Else
          if Len(Session("UserSearchString")) > 0 then 
	    	  Set rsAux = rsMakeUserOrgList(sOrgID, sRef, dState, oXMLProfile, Session("UserSearchString"), sCurrSel, sFriendly) 
          else
              Set rsAux = Nothing
          end if   
      End IF		  
   Else 
	      Set rsAux = rsMakeQuery(sOrgID, sRef, dState, oXMLProfile)        
   End If
   
%>
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
<!--	
	    .visibleSection {display: block}
	    .hiddenSection {display: none}
	    .dlgwidth {width: 600px }
	    .txtwidth {width: 450px } 
	BODY
	{
		PADDING: 15px;
		MARGIN: 0;
	}
-->	
</STYLE>

<SCRIPT LANGUAGE='VBScript'>
<!--

	sub window_onload()
	<%
		dim dBDError, slBDError
		if not isEmpty(Session("MSCSBDError")) then
			set slBDError = Session("MSCSBDError")
			for each dBDError in slBDError
	%>
						showErrorDlg "<%= dBDError("errortitle") %>", "<%= dBDError("friendlyerror") %>", "<%= dBDError("errordetails") %>", <%= dBDError("errortype") %>
	<%		next
			Session("MSCSBDError") = empty
		end if
	%>
	     LEformManage.btnSearch.disabled = "False"
	end sub

    Sub ShowXML()
        dim sVal, oXMl, oAll
        set oXML = window.event.XMLList
        LEFormManage.elRecords.value = oXML.xml
'        sVal = oXML.xml
        if oXML.hasChildNodes then
            LEFormManage.btnManage.disabled = "False"
        else
            LEFormManage.btnManage.disabled = "True"
        end if
        
    End Sub
    
    Sub ManageEntities()
        LEformManage.submit()
    End Sub
    
    Sub SelectEntity()
        dim elBase
        If IsObject(ChooseEntity) then
			set elBase = ChooseEntity
			window.parent.ReturnValue = elBase.options(elBase.selectedIndex).value & "|||" & elBase.options(elBase.selectedIndex).text
		Else
			window.parent.ReturnValue = elKeyValue.value & "|||" & elName.value
		End If
		window.parent.close
    End Sub
    
    Sub CancelEntity()
        window.parent.close
    End Sub
    
    sub changesearch()
        if len(Trim(LEFormManage.SearchString.value)) > 0 then
            LEformManage.submit()
        end if        
    End sub
    
    sub CheckButton()
        if len(Trim(LEFormManage.SearchString.value)) > 0 then
            LEformManage.btnSearch.disabled = "False"
        else
            LEformManage.btnSearch.disabled = "True"
        end if        
    End sub
    
    sub OnUnselectRow()
		elKeyValue.value = ""
		elName.value = ""
	end sub

	sub OnSelectRow()
		dim oSelection, sKeyValue, sName
		set oSelection = window.event.XMLrecord
		sKeyValue = trim(oSelection.selectSingleNode("./<%= g_sKey %>").text)
		sName = trim(oSelection.selectSingleNode("./<%= g_sName %>").text)
		'ChooseEntity.innerHTML = "<OPTION VALUE='" & sKeyValue & "' SELECTED>" & sName & "</OPTION>"
		elKeyValue.value = sKeyValue
		elName.value = sName
	end sub    
-->
</SCRIPT>
</HEAD>
<BODY SCROLL='NO'>
<%
    if NOT bUserOrOrg then
        call MakeListEditorMeta(dState, sRef, oXMLProfile, oXMLDisplayCols)
        call MakeListeditorData(rsAux, oXMLData)
   end if
%>

<P>&nbsp;</P>

<TABLE>
<TR WIDTH="100%">
  <%if sRef = REF_ORG_PROFILE then %>
	 <TD> <%= L_ChooseOrg_Text %> </TD>
	<%elseif sRef = REF_USER_PROFILE then %>
	 <TD> <%= L_ChooseUser_Text %> </TD>
	<%end if %>
</TR>
<TR WIDTH="70%">
<TD WIDTH="70%">
<%
    if bUserOrOrg then
       ' call MakeUserOrgSelectBox(sOrgID, sRef, dState, oXMLProfile, sSearch, sCurrSel, sFriendly)
       Call WriteXMLMetaForUserOrg(g_sKey, g_sName)
       If Not rsAux Is Nothing then
       	Set xmlOutput = xmlGetXMLFromRSEx(rsAux, 0, g_iListPageSize, 0, Null)
			if sRef = REF_ORG_PROFILE then _
				Call xmlFilterSitename(xmlOutput)

%>
		<xml id='profilesList'>
			<% 
			   Response.Write xmlOutput.xml
			%>
		</xml>
	
<%
		Else
			Call WriteBlankList()
		End If
%>				
    <DIV ID=lsProfiles CLASS='listSheet'
        DataXML='profilesList'
        MetaXML='profilesMeta'
        LANGUAGE='VBScript'
        OnRowSelect='OnSelectRow()'
        OnRowUnselect='OnUnselectRow()'><%= L_LoadingList_Text %></DIV>
	
	<INPUT TYPE=HIDDEN Name="elKeyValue" ID="elKeyValue">
	<INPUT TYPE=HIDDEN Name="elName" ID="elName">

<%
    else
%>

<SELECT ID='ChooseEntity'>
    <OPTION VALUE='NullChoice'></OPTION>
<%
        call MakeSelectBox(oXMLData, oXMLProfile, oXMLDisplayCols, sCurrSel)
%>
</SELECT> 
<% end if
	Set rsAux = Nothing
%>
&nbsp;&nbsp;</TD>
<TD WIDTH="30%">
    <Table>
	<TR>
		<TD>
		  <BUTTON ID='btnSelect' NAME='btnSelect' ONCLICK='SelectEntity()' STYLE="Width:80px"><%= L_SelectItem_Text %></BUTTON>
		</TD>
	</TR>
	<TR>
		<TD height="1px">
		</TD>
	</TR>
	<TR>
		<TD>
		  <BUTTON ID='btnCancel' NAME='btnCancel' ONCLICK='CancelEntity()' STYLE="Width:80px"><%= L_Cancel_Text %></BUTTON>
		</TD>
	</TR>
	</Table>
</TD>
</TABLE>
    <FORM ID='LEFormManage' METHOD='POST' ACTION='<%=sPageName%>' TARGET='_self'>
        <INPUT TYPE="HIDDEN" Name="elOrgID" ID="elOrgID" VALUE="<%=sOrgID%>">
        <INPUT TYPE="HIDDEN" Name="elProfile" ID="elProfile" VALUE="<%=sRef%>">
        <INPUT  NAME='elCurrSel' ID='elCurrSel' TYPE='HIDDEN' VALUE="<%=sCurrSel%>">
        <INPUT  NAME='elRecords' ID='elRecords' TYPE='HIDDEN' >
        <INPUT  NAME='elSelected' ID='elSelected' TYPE='HIDDEN' >
<%
'GR6/24/99 gary, This is pretty well setup for allowing one to enter a partial name in the text box
'below, hit the button and have the page submit to itself and have the page reconstruct the 
'select box with those orgs/users whose names start witht the string entered. But our provider
'chokes on the query for users so I have made all this invisible for now and have all orgs or all users
'for the current org in the select box.
 if bUserOrOrg then 
%>
        <TABLE ID='TABLE2'>
        <TR>
        <TD><%= L_UserOrgLogonName_Text%></TD>
        <TD>
        <INPUT  NAME='SearchString' ID='SearchString' TYPE='TEXT' VALUE="<%=Session("UserSearchString")%>" ONCHANGE='CheckButton'>
        </TD>
        <TD>
        <BUTTON ID='btnSearch' Name='btnSearch' ONCLICK='ChangeSearch()' DISABLED="False"><%= L_Search_Text %></BUTTON>
        </TD>
        </TABLE>
<%else%>
 
        <DIV ID='LEManage' CLASS='listEditor' MetaXML='LEMeta' DataXML='LEData' OnChange='ShowXML'>
        </DIV>
        <P></P>
        <CENTER>
        <BUTTON ID='btnManage' Name='btnManage' ONCLICK='ManageEntities()' DISABLED="False"><%= L_SubmitChanges_Text %></BUTTON>
        </CENTER>
<%end if%>
    </FORM>
	<FORM ACTION="Aux_Response.asp" ID="newpageform" NAME="newpageform">
        <INPUT TYPE="HIDDEN" Name="elOrgID" ID="elOrgID" VALUE="<%=sOrgID%>">
        <INPUT TYPE="HIDDEN" Name="elProfile" ID="elProfile" VALUE="<%=sRef%>">
        <INPUT NAME='elCurrSel' ID='elCurrSel' TYPE='HIDDEN' VALUE="<%=sCurrSel%>">
	</FORM>
<div ID="filtertext" class="filtertext" style="MARGIN-LEFT: 10px"><%= L_BDSSFilter_Text & " " & L_UseWildCardSearch_Text  %></div>
</BODY>

</HTML>
<%
End Sub
%>

