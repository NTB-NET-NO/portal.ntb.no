<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../common/UOCommonStrings.asp" -->
<%
 const L_Select_DialogTitle = "Selection Dialog"
 const L_CustomSelect_DialogTitle = "Select %1"
 dim args
 args = "Profile=" & Request.QueryString("Profile") & "&OrgID=" & _
        Request.QueryString("OrgID") & "&CurrentSel=" & _
        Request.QueryString("CurrentSel")
        
%>
<HTML>
<HEAD>
<HEAD>
<TITLE></TITLE>
<STYLE>
	HTML
	{
	    WIDTH: 520px;
	    HEIGHT: 450px;
	}
</STYLE>

	<SCRIPT LANGUAGE="VBScript">
	<!--
		Dim dTitle
		
		dTitle = window.dialogArguments
		if dTitle <> "" and dTitle = "UserObject" then
		 	document.title = sFormatString("<%= L_CustomSelect_DialogTitle %>", Array("<%= L_UserEntity_Text %>"))
		else
			document.title = "<%= L_Select_DialogTitle %>"
		end if
		Sub window_onload()
			If dTitle <> "UserObject" and dTitle <> "Organization" then
				window.dialogHeight = "600px"
			End If	
		End Sub 

		Function sFormatString(sFormat, aArgs)
			dim nArg
			for nArg = LBound(aArgs) to UBound(aArgs)
				sFormat = Replace(sFormat, "%" & nArg + 1, aArgs(nArg))
			next
			sFormatString = sFormat
		End Function

		-->
	</SCRIPT>
</HEAD>

<BODY SCROLL=no >
<% 
	Dim nDlgHeight
	Dim nDlgWidth
	If Request.QueryString("Profile") = "Profile Definitions.UserObject" or _
		Request.QueryString("Profile") = "Profile Definitions.Organization" then 
		nDlgHeight	= 450
		nDlgWidth	= 520
	else 
		nDlgHeight	= 600
		nDlgWidth	= 600
	end if
%>
<IFRAME ID='IFrame1' ALIGN='LEFT' FRAMEBORDER='0' HEIGHT='<%= nDlgHeight %>' WIDTH='<%= nDlgWidth %>'
SCROLLING='NO' SRC="AuxListEdit.asp?<%=args%>" STYLE="overflow:auto"></IFRAME>


</BODY>

</HTML>