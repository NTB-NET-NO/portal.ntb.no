<!--#INCLUDE FILE='include/BDHeader.asp' -->
<!--#INCLUDE FILE='include/BizDeskPaths.asp' -->
<!--#INCLUDE FILE='include/BizDeskStrings.asp' -->
<!--#INCLUDE FILE='include/ASPUtil.asp' -->
<HTML>
<HEAD>
	<LINK REL='stylesheet' TYPE='text/css' HREF='include/bizdesk.css' ID='mainstylesheet'>
	<STYLE>
		BODY
		{
			MARGIN-TOP: 10px;
			BACKGROUND-COLOR: threedface;
			BORDER-TOP: gray 1px solid
		}
	</STYLE>
	<SCRIPT LANGUAGE=VBScript>
	<!--
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		' event handler routines:
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	context menu handler to turn off context menus
		sub document_onContextMenu()
			' -- disallow default context menu unless in entry fields
			if not (window.event.srcElement.tagName = "INPUT" or window.event.srcElement.tagName = "TEXTAREA") then
				window.event.returnValue = false
			end if
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	select start handler to turn off selection except in inputs
		sub document_onSelectStart()
			' -- disallow selection of text on the page except in edit boxes
			Dim elSource
			set elSource = window.event.srcElement
			if elSource.tagName = "TEXTAREA" or _
				elSource.tagName = "INPUT" then
				if (elSource.readonly or elSource.disabled) then window.event.returnValue = false
			else
				window.event.returnValue = false
			end if
		end sub

		sub showSecurity()
			if typename(winHelpWindow) = "HTMLWindow2" then winHelpWindow.close
			set winHelpWindow = window.open("docs/default.asp?helptopic=CS_BD_Troubleshoot_byzi.htm", "winHelpWindow", _
							 "height=500,width=700,status=no,toolbar=yes,menubar:no,resizable=yes")
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY>
	<TABLE>
	<TR>
		<TD STYLE='FONT-WEIGHT: bold;TEXT-ALIGN:center'>
			If you have trouble with setup <a HREF="javascript:showSecurity()" STYLE="COLOR: blue">click here</a>
		</TD>
	</TR>
	<TR>
		<TD>
<%
	if g_MSCSSiteName <> "" and not Application("MSCSErrorInGlobalASA") then
		dim sStartURL
		if UCase(Request.ServerVariables("HTTPS")) = "ON" then
			sStartURL = "https://"
		else
			sStartURL = "http://"
		end if
		sStartURL = sStartURL & Request.ServerVariables("SERVER_NAME") & ":" & Request.ServerVariables("SERVER_PORT") & g_sBizDeskRoot & "bizdesk.asp"
%>
			<OBJECT ID='RunSetup' CLASSID='CLSID:E9472078-EBA7-4885-8768-80ACF6F94553'
					CODEBASE='ClientSetup.CAB#version=-1,-1,-1,-1' 
					HEIGHT='180' WIDTH='320' VIEWASTEXT>
				<PARAM NAME='ServerName' VALUE="<%= UCase(Request.ServerVariables("SERVER_NAME")) %>">
				<PARAM NAME='ServerPort' VALUE="<%= Request.ServerVariables("SERVER_PORT") %>">
				<PARAM NAME='SiteName' VALUE="<%= g_MSCSSiteName %>">
				<PARAM NAME='StartURL' VALUE="<%= sStartURL %>">
			</OBJECT>
<%
	else
%>			<SPAN STYLE='FONT-FAMILY: verdana, arial, helvetica, sans-serif; FONT-SIZE: 12pt; FONT-WEIGHT: bold; COLOR: red'><%= L_ErrorsInAppLoad_ErrorMessage %></SPAN>
<%
	end if
%>		</TD>
	</TR>
	</TABLE>
</BODY>
</HTML>
