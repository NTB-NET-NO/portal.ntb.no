<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../../include/HTTPXMLUtil.asp' -->
<%
' ####################################################################
'
'   response_PropertyDefinitions.ASP
'
'	Paging for Property Definitions
'
' ####################################################################

	'------ CONST Declarations
	Const PAGE_SIZE = 20
	Const SEPARATOR1 = "|||"
	Const PAGE_TYPE = "RESPONSE"

	'----- PUBLIC Declarations
	Public g_nPage
	Public g_nItems			' A count of items returned from catalog object
	Public g_sAction
	Public g_oCatMgr		' The Catalog Manager Object
	Public g_dRequest
	Public g_rsProperties	' Catalog Property Definitions  
	Public g_aProperties()  ' An array to hold the list of properties

	Set g_dRequest = dGetRequestXMLAsDict()

	On Error Resume Next
	Perform_ServerSide_Processing
%>
<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<%= sGetXML %>
<%

' ####################################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	If (g_oCatMgr Is Nothing) Then Exit Sub

	InitializeGlobalVariables
	If (Err.Number <> 0) Then Exit Sub

	Perform_Processing_BasedOnAction
End Sub

' ######################################################################
'
Sub InitializeGlobalVariables ()

	On Error Resume Next

	ReDim g_aProperties(-1)

	g_sAction = g_dRequest("type")
	g_nPage = g_dRequest("page_id")

	If (True = IsEmpty(g_nPage)) Then g_nPage = 1
	Set g_rsProperties = g_oCatMgr.Properties
End Sub

' ######################################################################
'
Sub Perform_Processing_BasedOnAction ()

	Dim sTempFilter
	Dim sSearchType
	Dim sSearchValue
	Dim sSearchOn

	Select Case g_sAction
	Case "delete"
		DeletePropertyDefinitions g_dRequest("ch_name")

	Case "newSearch"
		sSearchOn = g_dRequest("searchon")
		If IsNull(sSearchOn) Or (sSearchOn = "off") Then _
			Session("FindPropertyDefinition") = Empty

		sSearchType = g_dRequest("selfindby")
		Select Case sSearchType
		Case "Name"
			sSearchValue = g_dRequest("txtSearchName")
		Case "DataType"
			sSearchValue = g_dRequest("txtSearchType")
		Case "AssignAll"
			sSearchValue = g_dRequest("txtSearchAssignAll")
		Case "All"
			sSearchType = ""
			Session("FindPropertyDefinition") = Empty
		End Select

		If (sSearchType <> "") Then
			sTempFilter = Session("FindPropertyDefinition")
			'Add the new filter to filter set
			If (sTempFilter <> "") Then
				sTempFilter = sTempFilter & SEPARATOR1 & sSearchType & SEPARATOR1 & sSearchValue
			Else
				sTempFilter = sSearchType & SEPARATOR1 & sSearchValue
			End If

			Session("FindPropertyDefinition") = sTempFilter
		End If
	End Select

	Call RetrieveCharArray (g_aProperties)
	Call RunFilters (g_aProperties)

	g_nItems = UBound(g_aProperties) + 1
End Sub

' ###################################################################
' Sub: RetrieveCharArray
'	Retrieves a list of characteristics (properties) 
'   from the catalog manager object
'
' Input:
'	aProperties - dynamic array to fill (should be empty)
'
Sub RetrieveCharArray (ByRef aProperties)

	Dim sPropertyName

	On Error Resume Next

	g_rsProperties.MoveFirst
	Do Until g_rsProperties.EOF
		sPropertyName = g_rsProperties.Fields("PropertyName")
		AddArray aProperties, sPropertyName

		g_rsProperties.MoveNext
	Loop
End Sub

' ###############################################################
' Sub: AddArray
'	This subroutine adds a string selection to a string array
'
' Input:
'	aArray - the array
'	sString - the string
'
Sub AddArray (ByRef aArray, ByRef sString)

	Dim iNewUBound

	iNewUBound = UBound(aArray) + 1
	ReDim Preserve aArray(iNewUBound)

	aArray(iNewUBound) = sString
End Sub

' ###############################################################
'
Function getPropertyAttribute (ByRef sPropertyName, ByRef sAttribute)

	On Error Resume Next

	g_rsProperties.MoveFirst
	Do Until g_rsProperties.EOF
		If (g_rsProperties ("PropertyName").Value = sPropertyName) Then _
			Exit Do

		g_rsProperties.MoveNext
	Loop

	getPropertyAttribute = -1
	If (g_rsProperties.EOF = False) Then
		getPropertyAttribute = g_rsProperties.Fields(sAttribute)
	End If
End Function

' ###############################################################
' Sub: RunFilters
'	Runs searches
'
' Input:
'	aFields - array to search
'
Sub RunFilters (ByRef aFields)

	Dim iCnt
	Dim aFilters

	On Error Resume Next

	aFilters = Split(Session("FindPropertyDefinition"), SEPARATOR1)
	For iCnt = 0 to UBound(aFilters) Step 2
		'Check the filter type
		Select Case aFilters(iCnt)
		Case "Name"
			' Name: Compare name of each item in char array to name from filter
			FilterByName aFields, aFilters(iCnt + 1)

		Case "DataType"
			' Type: Check types of characteristics
			FilterByCharAttribute aFields, aFilters(iCnt + 1), aFilters(iCnt)

		Case "AssignAll"
			' AssignAll: Check to see if assigned all or not assigned all
			FilterByCharAttribute aFields, aFilters(iCnt + 1), aFilters(iCnt)
		End Select
	Next
End Sub

' ###############################################################
' Sub: FilterByName
'	compares array values to a name and resizes array
'
' Input:
'	aNames - array of all property definitions
'	sName  - property name to look for
'
Sub FilterByName (ByRef aNames, ByRef sName)

	Dim iCnt
	Dim sItem
	Dim aTempNames ()

	ReDim aTempNames(-1)
	For Each sItem In aNames
		If (InStr(sItem, sName) > 0) Then
			AddArray aTempNames, sItem
		End If
	Next

	ReDim aNames(UBound(aTempNames))
	For iCnt = 0 To UBound(aTempNames)
		aNames(iCnt) = aTempNames(iCnt)
	Next
End Sub

' ##############################################################
' Sub: FilterByCharAttribute
'	checks an array of characteristics versus a char type and resizes
'	array
'
' Input:
'	aChars:	array of all property definitions
'	sValue: value to look for
'	sAttribute: attribute to search
'
Sub FilterByCharAttribute (ByRef aChars, ByRef sValue, ByRef sAttribute)

	Dim iCnt
	Dim sItem
	Dim aTempChars ()

	ReDim aTempChars(-1)
	For Each sItem In aChars
		If (CStr(getPropertyAttribute(sItem, sAttribute)) = sValue) Then
			AddArray aTempChars, sItem
		End If
	Next

	ReDim aChars(UBound(aTempChars))
	For iCnt = 0 To UBound(aTempChars)
		aChars(iCnt) = aTempChars(iCnt)
	Next
End Sub

' ##########################################################
'
Function sGetXML ()

	Dim iIndex

	Dim sType
	Dim sProperty

	If (g_oCatMgr Is Nothing) Then
		sGetXML = setXMLError (L_BadConnectionString_ErrorMessage, "")
		Exit Function
	End If

	iIndex = PAGE_SIZE * (g_nPage - 1)

	While (iIndex < g_nItems) And (iIndex < PAGE_SIZE * g_nPage)
		sProperty = g_aProperties(iIndex)
		sType = sEnumType(getPropertyAttribute(sProperty, "DataType"))
		sGetXML = sGetXML & "<record><name>" & Server.HTMLEncode(sProperty) & "</name><type>" & sType & "</type></record>"

		iIndex = iIndex + 1
	Wend

	If (Err.Number = 0) Then
		If (sGetXML = "") Then sGetXML = "<record />"
		sGetXML = "<document recordcount='" & g_nItems & "'>" & sGetXML & "</document>"
	Else
		sGetXML = setXMLError (L_ErrorsInPage_ErrorMessage, sGetScriptError(Err))
	End If

	Set g_oCatMgr = Nothing
End Function

' ###############################################################
' Sub: InitializeCatalogObject
'	Create and initialize a global catalog object
'
' Input:
'
Sub InitializeCatalogObject ()

	Dim sConnString

	On Error Resume Next
	Set g_oCatMgr = Nothing

	If (True = IsObject(Session("CatalogManager"))) Then
		If Not (Session("CatalogManager") Is Nothing) Then
			Dim rs
			Set g_oCatMgr = Session("CatalogManager")
			Set rs = g_oCatMgr.Catalogs
			If (Err.Number <> 0) Then
				Set g_oCatMgr = Nothing
				Set Session("CatalogManager") = Nothing
				Err.Clear
				Exit Sub
			End If
		End If
	End If

	If (g_oCatMgr Is Nothing) Then
		sConnString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")

		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (sConnString, True)
		If (Err.Number <> 0) Then
			Set g_oCatMgr = Nothing
			Err.Clear
		Else
			Set Session("CatalogManager") = g_oCatMgr
		End If
	End If
End Sub

' #################################################################
' Function: sEnumType
'	Converts CatalogDataTypeEnum values to their string representation
'
' Input: 
'	iDataType - one of the CatalogDataTypeEnum values
'
Function sEnumType (ByVal iDataType)

    Select Case iDataType
	Case 0
		sEnumType = L_Number_Text
	Case 2
		sEnumType = L_Float_Text
	Case 5
		sEnumType = L_String_Text
	Case 6
		sEnumType = L_DateTime_Text
	Case 7
		sEnumType = L_Currency_Text
	Case 8
		sEnumType = L_FileName_Text
	Case 9
		sEnumType = L_Enumerated_Text
	End Select
End Function

'' -------------------------------------------------------------------
''
Function setXMLError (ByRef sSource, ByRef sText)

	setXMLError = "<document recordcount='0'>" & "<ERROR ID='1' SOURCE='" & Server.HTMLEncode(sSource) & "'>" & Server.HTMLEncode(sText) & "</ERROR></document>"
	Err.Clear
End Function

%>