<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
' #########################################################################
'
'   list_CategoryDefinitions.ASP
'
'	Item Select Page for Category Definitions
'
' #########################################################################

	On Error Resume Next

	' ------------------------
	Const PAGE_TYPE = "LIST"
%>
<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

public g_aSelected ()	' An array to hold selected items
public g_iSelectedCount

sub window_onLoad ()

	redim g_aSelected(-1)

	if GetTemp("bCatImporting") then
		bImportToggle = true
		nImportCount = 0
		oImportTimer = window.setInterval("ImportProgress()", BLINK_SPEED * 1000)
		call ImportProgress ()
	end if

' Look for bad connection string
<% If (Not g_oCatMgr Is Nothing) Then %>
	EnableTask "view"
	EnableTask "new"
	EnableAllTaskMenuItems "view", True
<% End If %>
end sub

sub OnSelectRow ()

	dim oSelection

	g_iSelectedCount = g_iSelectedCount + 1
	' enable o(pen) and d(elete) buttons on the task bar
	if (g_iSelectedCount = 1) then
		EnableTask "copy"
		EnableTask "open"
	else
		DisableTask "copy"
		DisableTask "open"
	end if
	EnableTask "delete"

    set oSelection = window.event.XMLrecord
	AddArray g_aSelected, oSelection.selectSingleNode("./name").text

    call setStatusText (sStatusBarText)
end sub

sub OnDeselectRow ()

	dim oSelection

	g_iSelectedCount = g_iSelectedCount - 1
	select case g_iSelectedCount
	case 0
		DisableTask "copy"
		DisableTask "delete"
		DisableTask "open"
	case 1
		EnableTask "copy"
		EnableTask "open"
	end select

    set oSelection = window.event.XMLrecord
	DeleteArray g_aSelected,oSelection.selectSingleNode("./name").text

	if (g_iSelectedCount = 0) then
    	call setStatusText ("")
	else
	    call setStatusText (sStatusBarText)
	end if
end sub

sub OnSelectAll ()

	dim nItems

	nItems = <%= g_iDefCnt %>
	if (nItems = 1) then
		SelectAllToArray g_aSelected
	else'if(nItems > 1) then
		redim g_aSelected(0)
		g_aSelected(0) = ALL_DEFINITIONS
	end if
	g_iSelectedCount = nItems

	'enable o(pen) and d(elete) buttons on the task bar
	if (g_iSelectedCount > 0) then 
		EnableTask "delete"

		if (g_iSelectedCount = 1) then
			EnableTask "copy"
			EnableTask "open"
		else
			DisableTask "copy"
			DisableTask "open"
		end if
	end if

	' display the status text
    call setStatusText (sStatusBarText)
end sub

sub OnDeselectAll ()

	' disable o(pen) and d(elete) buttons on the task bar
	DisableTask "open"
	DisableTask "delete"

	g_iSelectedCount = 0
	DeselectAllToArray g_aSelected

	' display status text
    setStatusText ""
end sub

sub OnCopy ()

	with copyform
		.name.value = g_aSelected(0)
		.submit()
	end with
end sub

function OnDelete ()

	dim sMsg
	dim iBtnClicked

	if (g_iSelectedCount = 1) then
		sMsg = L_DeleteCategoryDefinition_Text
	else
		sMsg = sFormatString (L_DeleteDefinitions_HTMLText, Array(g_iSelectedCount))
	end if

	iBtnClicked = MsgBox (sMsg, 36, L_ConfirmDeleteCategDefs_Text)
	if (iBtnClicked = vbYes) then
		with deleteform
			.name.value = sConvertArrayToString(g_aSelected)
			.submit()
		end with
	else
		EnableTask "view"
		EnableTask "new"
		EnableTask "delete"

		if (g_iSelectedCount = 1) then
			EnableTask "copy"
			EnableTask "open"
		end if
	end if
end function

function OnOpen ()

	with selectform
		.name.value = g_aSelected(0)
		.submit()
	end with
end function

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) And (Not g_oCatMgr Is Nothing) Then
		Call setError ("", L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertTaskBar L_CategoryDefinitions_Text, g_sStatusText
%>

<xml id='lsData'>
	<document>
		<%= g_sXML %>
	</document>
</xml>

<xml id='lsMeta'>
	<listsheet>
	    <global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_iDefCnt %>'
	            selection='multi' emptyprompt='no' sort='no' />
	    <columns>
	        <column id='name' sortdir='asc' width='100'><%= L_Name_Text %></column>
			<subobject type='list'>
				<column id='property' sortdir='asc' width='100'><%= L_Property_Text %></column>
			</subobject>
	    </columns>
   	    <operations>
			<newpage formid='newpageform'/>
		</operations>
	</listsheet>
</xml>

<FORM ACTION="" METHOD="POST" ID="catSelectForm">
	<INPUT TYPE="Hidden" NAME="page_id"  VALUE="<%= g_nPage %>">
	<DIV ID="bdcontentarea" CLASS="listSheetContainer">

		<DIV ID='cgListSheet'
			CLASS='listSheet' STYLE="MARGIN:20px; HEIGHT: 80%"
			DataXML='lsData'
			MetaXML='lsMeta'
			LANGUAGE='VBScript'
			OnRowSelect='OnSelectRow'
			OnRowUnselect='OnDeselectRow'
			OnAllRowsSelect='OnSelectAll'
			OnAllRowsUnselect='OnDeselectAll'
			OnNewPage='OnNewPage("catSelectForm")'><%= L_LoadingWidget_Text %>
		</DIV>
	</DIV>
</FORM>

<FORM ID="newpageform" ACTION='response_CategoryDefinitions.asp'></FORM>

<FORM ACTION="" METHOD="POST" ID="addform">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
</FORM>
<FORM ACTION="" METHOD="POST" ID="copyform" ONTASK="OnCopy">
	<INPUT TYPE="Hidden" NAME="type" VALUE="copy">
	<INPUT TYPE="Hidden" NAME="name">
</FORM>
<FORM ACTION="" METHOD="POST" ID="selectform" ONTASK="OnOpen">
	<INPUT TYPE="Hidden" NAME="type" VALUE="open">
	<INPUT TYPE="Hidden" NAME="name">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="deleteform" ONTASK="OnDelete">
	<INPUT TYPE="Hidden" NAME="type" VALUE="delete">
	<INPUT TYPE="Hidden" NAME="name">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
</FORM>

</BODY>
</HTML>
<%
	'' -----------------
	ReleaseGlobalObjects
	'' -----------------
%>

<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'----- PUBLIC Declarations
Public g_sXML
Public g_sAction
Public g_sStatusText	' Text to be displayed in the Status Bar

Public g_nPage
Public g_iDefCnt		' Count of definitions returned

Public g_rsDefinitions	' An ADORS that will hold the list of definitions

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables

	Perform_Processing_BasedOnAction
End Sub

' ##########################################################
'
Sub InitializeGlobalVariables ()

	g_iDefCnt = 0

	g_nPage = Request.Form("page_id")
	If (True = IsEmpty(g_nPage)) Then g_nPage = 1

	g_sAction = Request.Form("type")
End Sub

' ##########################################################
'
Sub Perform_Processing_BasedOnAction ()

	On Error Resume Next

	Select Case g_sAction
		Case "delete"
			Set g_rsDefinitions = g_oCatMgr.CategoryDefinitions
			DeleteDefinitions Request.Form("name")
	End Select

	Set g_rsDefinitions = g_oCatMgr.CategoryDefinitions

	If (Err.Number <> 0) Then Exit Sub

	GetMasterDetailXML
End Sub

' ##########################################################
'
Sub GetMasterDetailXML ()

	Dim rsProperties

	Dim sPropertyName
	Dim sDefinitionName

	On Error Resume Next
	While (Not g_rsDefinitions.EOF) And (g_iDefCnt < PAGE_SIZE)
		sDefinitionName = g_rsDefinitions.Fields("DefinitionName")
		g_sXML = g_sXML & "<record><name>" & Server.HTMLEncode(sDefinitionName) & "</name>"

		Set rsProperties = g_oCatMgr.GetDefinitionProperties (sDefinitionName)
		While Not rsProperties.EOF
			sPropertyName = rsProperties.Fields("PropertyName")
			g_sXML = g_sXML & "<record><property>" & Server.HTMLEncode(sPropertyName) & "</property></record>"

			rsProperties.MoveNext
		Wend
		ReleaseRecordset rsProperties

		g_sXML = g_sXML & "</record>"
		g_iDefCnt = g_iDefCnt + 1
		g_rsDefinitions.MoveNext
	Wend

	If (g_iDefCnt = 0) Then
		g_sXML = "<record/>"
	Else
		While (Not g_rsDefinitions.EOF)
			g_iDefCnt = g_iDefCnt + 1
			g_rsDefinitions.MoveNext
		Wend
	End If
End Sub

' ##############################################################
'
Sub ReleaseGlobalObjects ()

	ReleaseRecordset g_rsDefinitions
	Set g_oCatMgr = Nothing
End Sub

%>