<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
'' --------------------------------------------------------------------
''
''	edit_ProductDefinition.ASP
''
''	Product Definitions - Item Properties Page.
''
'' --------------------------------------------------------------------

	On Error Resume Next

	Const PAGE_TYPE = "EDIT"
	' ------------------------------
	Public g_aAssChar()
	Public g_aAvailChar()
	Public g_aAssCharVariant()
	Public g_aAvailCharVariant()
%>
<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">
option explicit

public g_oSelect

public g_aAddChar()				' An array to hold added properties
public g_aRemoveChar()			' An array to hold removed properties
public g_aAddCharVariant()		' An array to hold added properties
public g_aRemoveCharVariant()	' An array to hold removed properties

sub window_onLoad ()

	redim g_aAddChar(-1)
	redim g_aRemoveChar(-1)
	redim g_aAddCharVariant(-1)
	redim g_aRemoveCharVariant(-1)

	esControl.focus
end sub

sub onChange ()

	setStatusText ""
	setDirty (L_SaveConfirmationDialog_Text)
end sub

sub SwitchFocus (ByRef oSelectTo, ByRef oSelectFrom, _
  		         ByRef oButtonTo, ByRef oButtonFrom)
	dim bDisabled

	if (oSelectTo.selectedIndex >= 0) then
		bDisabled = False

		if (oSelectFrom.selectedIndex >= 0) then
			call ClearFocus (oSelectFrom, oButtonFrom)
		end if
	else
		' Selection disabled (using CTRL key)
		bDisabled = True
	end if

	oButtonTo.disabled = bDisabled	
end sub

sub ClearFocus (ByRef oSelect, ByRef oButton)

	if (oSelect.selectedIndex >= 0) then
		oSelect.selectedIndex = -1

		oButton.disabled = True
	end if
end sub

sub MoveSelectedItems (ByRef oSelectFrom, ByRef oSelectTo, _
					   ByRef aFrom, ByRef aTo, _
					   ByRef oSelect, ByRef oAddBtn, _
					   ByVal sOp)
	dim iIndex
	dim oButton

	dim sText
	dim sValue
	dim sClass
	dim elOption

	set oButton = window.event.srcElement

	for each elOption in oSelectFrom.options
		if elOption.selected then
			sValue = elOption.value
			sText  = elOption.text
			sClass = elOption.className
			iIndex = elOption.index
			if not bRemovedFromArray(aFrom, sValue) then AddArray aTo, sValue

			call AddNewOption (oSelectTo, sValue, sText, sClass)
			oSelectFrom.remove(iIndex)
			oButton.disabled = True
			oSelectFrom.focus

			select case sOp
			case "Add"
				oSelect.remove(iIndex)
				if (oSelect.selectedIndex = -1) then oAddBtn.disabled = True
			case "Remove"
				call AddNewOption (oSelect, sValue, sText, sClass)
			end select
		end if
	next

	onChange
end sub

sub AddNewOption (ByRef oSelect, ByRef sValue, ByRef sText, ByRef sClass)

	dim elOption

	set elOption = document.createElement("OPTION")
	elOption.value = sValue
	elOption.text = sText
	elOption.className = sClass
	oSelect.options.add(elOption)

	set elOption = Nothing
end sub

sub ptForm_onSubmit ()

	dim elOption

	' Check if the event was triggered by pressing ENTER
	' in this particular case w/ only one INPUT control
	if (window.event.srcElement.tagName = "FORM") then
		' Then ignore it!
		window.event.returnValue = False
		exit sub
	end if

	if (0 = InStr (ptForm.action, "select")) then
		' SAVE operation
		for each elOption in ptForm.ass_char.options
			elOption.selected = True
		next
		for each elOption in ptForm.ass_char_variant.options
			elOption.selected = True
		next
		with ptForm.all
			.added_chars.value = sConvertArrayToString (g_aAddChar)
			.removed_chars.value = sConvertArrayToString (g_aRemoveChar)
			.added_chars_variant.value = sConvertArrayToString (g_aAddCharVariant)
			.removed_chars_variant.value = sConvertArrayToString (g_aRemoveCharVariant)
		end with
	end if

	ptForm.submit
end sub

sub ptForm_onSaveNew ()

	with ptForm.type
		select case .value
		case "submitNew"
			.value = "createNew"
		case else
			.value = "saveNew"
		end select
	end with

	ptForm_onSubmit
end sub

sub ass_char_onChange (ByRef oSelectTo, ByRef oSelectFrom, ByRef oButtonTo, ByRef oButtonFrom, _
					   ByRef oButtonCopy, ByRef oButtonEdit, ByRef oButtonMoveUp, ByRef oButtonMoveDown, ByRef sID)
	dim bValue

	SwitchFocus oSelectTo, oSelectFrom, oButtonTo, oButtonFrom
	DisableCopyEdit oButtonCopy, oButtonEdit, False
	if (nSelections (oSelectTo) <> 1) then
		' Multiple Selections with Shift/Ctrl keys
		bValue = True
	else
		bValue = False
	end if

	DisableCopyEdit oButtonCopy, oButtonEdit, bValue
	DisableMoveUpDown oButtonMoveUp, oButtonMoveDown, sID, bValue
end sub

sub avail_char_onChange (ByRef oSelectTo, ByRef oSelectFrom, ByRef oButtonTo, ByRef oButtonFrom, _
						 ByRef oButtonCopy, ByRef oButtonEdit, _
						 ByRef oButtonMoveUp, ByRef oButtonMoveDown)

	SwitchFocus oSelectTo, oSelectFrom, oButtonTo, oButtonFrom
	DisableCopyEdit oButtonCopy, oButtonEdit, True
	DisableMoveUpDown oButtonMoveUp, oButtonMoveDown, "", True
end sub

sub remove_char_onClick2 (ByRef oSelectFrom, ByRef oSelectTo, _
						  aAddChar, aRemoveChar, ByRef oSelectTo2, _
						  ByRef oButtonCopy, ByRef oButtonEdit, _
						  ByRef oButtonMoveUp, ByRef oButtonMoveDown, ByRef sID)

	MoveSelectedItems oSelectFrom, oSelectTo, aAddChar, aRemoveChar, oSelectTo2, Nothing, "Remove"
	DisableCopyEdit oButtonCopy, oButtonEdit, True
	DisableMoveUpDown oButtonMoveUp, oButtonMoveDown, sID, True
end sub

sub DisableCopyEdit (ByRef oButtonCopy, ByRef oButtonEdit, ByVal bValue)

	oButtonCopy.disabled = bValue
	oButtonEdit.disabled = bValue
end sub

sub OnNewPropertyDef (ByRef oSelectAss)

	dim sType

	set g_oSelect = oSelectAss

	sType = window.showModalDialog("dlg_AddPropertyDefinition.asp", , _
				"dialogHeight:270px;dialogWidth:350px;status:no;help:no")
	if (sType <> "") then
		call setEditPageParameters (PROPERTY_DEF_PARAMETERS, _
									Array("type", "ch_type"), _
									Array("add", sType))
		call openEditPage("<%= g_sBizDeskRoot %>catalogs/designer/edit_PropertyDefinition.asp", "OnCallback")
	end if
end sub

sub OnCopyPropertyDef (ByRef oSelectAss)

	set g_oSelect = oSelectAss

	call setEditPageParameters (PROPERTY_DEF_PARAMETERS, _
								Array("type", "ch_name"), _
								Array("copy", sCurrentProperty(oSelectAss)))
	call openEditPage("<%= g_sBizDeskRoot %>catalogs/designer/edit_PropertyDefinition.asp", "OnCallback")
end sub

sub OnEditPropertyDef (ByRef oSelectAss)

	call setEditPageParameters (PROPERTY_DEF_PARAMETERS, _
								Array("type", "ch_name"), _
								Array("open", sCurrentProperty(oSelectAss)))
	call openEditPage("<%= g_sBizDeskRoot %>catalogs/designer/edit_PropertyDefinition.asp", "")
end sub

function sCurrentProperty (ByRef oSelectAss)

	with oSelectAss
		sCurrentProperty = .options(.selectedIndex).value
	end with
end function

sub OnCallback ()

	dim iIndex

	dim sNewPropertyDef
	dim aNewPropertyDef

	dim sNewPropertyType
	dim aNewPropertyType

	sNewPropertyDef = getTemp ("sNewPropertyDef")
	sNewPropertyType = getTemp ("sNewPropertyType")

	if (sNewPropertyDef <> "") then
		call clearTemp ("sNewPropertyDef")
		call clearTemp ("sNewPropertyType")

		aNewPropertyDef  = Split (sNewPropertyDef, SEPARATOR1)
		aNewPropertyType = Split (sNewPropertyType, SEPARATOR1)

		for iIndex=0 to UBound(aNewPropertyDef)
			call AddOption (g_oSelect, aNewPropertyDef(iIndex), aNewPropertyType(iIndex))
		next
	end if
end sub

sub AddOption (ByRef oSelect, ByRef sNewPropertyDef, ByRef sAssignAll)

	dim elOption

	set elOption = document.createElement("OPTION")
	elOption.value = sNewPropertyDef
	elOption.text = sNewPropertyDef
	if (sAssignAll = "1") then elOption.className = "clsOPTIONRequired"
	oSelect.options.add(elOption)

	if (g_oSelect.id = "ass_char") then
		AddArray g_aAddChar, sNewPropertyDef
	else
		AddArray g_aAddCharVariant, sNewPropertyDef
	end if

	onChange
end sub

</SCRIPT>

<STYLE>

.clsOPTIONRequired
{
	BACKGROUND-COLOR: #ffff99! important
}

</STYLE>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertEditTaskBar g_sHeader, g_sStatusText
%>

<xml id='esData'>
	<DOCUMENT>
		<RECORD>
			<name><%= Server.HTMLEncode(g_sPtName) %></name>
			<req_properties>
<%= L_ListPrice_Text %>
			</req_properties>
		</RECORD>
	</DOCUMENT>
</xml>

<xml id='esMeta'>
	<editsheets>
		<editsheet>
			<global expanded='yes'>
		        <name><%= L_ProductDefProperties_Text %></name>
		        <key><%= L_ProductDefProperties_Accelerator %></key>
			</global>
			<fields>
			    <text id='name' required='yes'
			           maxlen='128' subtype='short'>
			        <name><%= L_Name_Text %></name>
			        <tooltip><%= L_Name_Tooltip %></tooltip>
			    </text>
		    </fields>
		</editsheet>
		<editsheet>
		    <global expanded='yes'>
		        <name><%= L_ProductProperties_Text %></name>
				<key><%= L_ProductProperties_Accelerator %></key>
		    </global>
		    <template register='ass_char'><![CDATA[<BR>
				<TABLE>
					<TR>
						<TD><SPAN TITLE="<%= L_AvailableProperties_Tooltip %>"><%= L_AvailableProperties_Text %></SPAN></TD>
						<TD>&nbsp;</TD>
						<TD><SPAN TITLE="<%= L_AssignedProperties_Tooltip %>"><%= L_AssignedProperties_Text %></SPAN></TD>
					</TR>
					<TR>
						<TD STYLE="width:33%">
							<SELECT SIZE="15" NAME="avail_char" ID="avail_char" STYLE="width:100%" MULTIPLE
							 LANGUAGE="VBScript" ONCHANGE="avail_char_onChange ptForm.avail_char, ptForm.ass_char, ptForm.add_char, ptForm.remove_char, ptForm.copy_char, ptForm.edit_char, ptForm.move_up, ptForm.move_down">
<%= sPropertyArrayAsOptions (g_aAvailChar) %>
							</SELECT>
						</TD>
						<TD VALIGN="CENTER" ALIGN="CENTER">
							<BUTTON NAME="add_char" DISABLED ID="add_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="MoveSelectedItems ptForm.avail_char, ptForm.ass_char, g_aRemoveChar, g_aAddChar, ptForm.avail_char_variant, ptForm.add_char_variant, 'Add'"><%= L_Add_Button %></BUTTON>
							<BR>
							<BUTTON NAME="remove_char" DISABLED ID="remove_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="remove_char_onClick2 ptForm.ass_char, ptForm.avail_char, g_aAddChar, g_aRemoveChar, ptForm.avail_char_variant, ptForm.copy_char, ptForm.edit_char, ptForm.move_up, ptForm.move_down, 'ass_char'"><%= L_Remove_Button %></BUTTON>
						</TD>
						<TD STYLE="width:33%">
							<SELECT SIZE="15" NAME="ass_char" ID="ass_char" STYLE="width:100%" MULTIPLE
							 LANGUAGE="VBScript" ONCHANGE="ass_char_OnChange ptForm.ass_char, ptForm.avail_char, ptForm.remove_char, ptForm.add_char, ptForm.copy_char, ptForm.edit_char, ptForm.move_up, ptForm.move_down, 'ass_char'">
<%= sPropertyArrayAsOptions (g_aAssChar) %>
							</SELECT>
						</TD>
						<TD VALIGN="CENTER" ALIGN="CENTER">
							<BUTTON NAME="new_char" ID="new_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnNewPropertyDef ptForm('ass_char')"><%= L_New_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="copy_char" ID="copy_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnCopyPropertyDef ptForm('ass_char')"><%= L_Copy_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="edit_char" ID="edit_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnEditPropertyDef ptForm('ass_char')"><%= L_Edit_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="move_up" ID="move_up" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="onMove 'ass_char', -1, ptForm.move_up, ptForm.move_down"><%= L_MoveUp_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="move_down" ID="move_down" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="onMove 'ass_char', 1, ptForm.move_up, ptForm.move_down"><%= L_MoveDown_Button %></BUTTON>
						</TD>
					</TR>
				</TABLE><BR>
		    ]]></template>
		</editsheet>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_ProductVariantProperties_Text %></name>
				<key><%= L_ProductVariantProperties_Accelerator %></key>
		    </global>
		    <template register='ass_char_variant'><![CDATA[<BR>
				<TABLE>
					<TR>
						<TD><SPAN TITLE="<%= L_AvailableProperties_Tooltip %>"><%= L_AvailableProperties_Text %></SPAN></TD>
						<TD>&nbsp;</TD>
						<TD><SPAN TITLE="<%= L_AssignedProperties_Tooltip %>"><%= L_AssignedProperties_Text %></SPAN></TD>
					</TR>
					<TR>
						<TD STYLE="width:33%">
							<SELECT SIZE="15" NAME="avail_char_variant" ID="avail_char_variant" STYLE="width:100%" MULTIPLE
								 LANGUAGE="VBScript" ONCHANGE="avail_char_onChange ptForm.avail_char_variant, ptForm.ass_char_variant, ptForm.add_char_variant, ptForm.remove_char_variant, ptForm.copy_charvar, ptForm.edit_charvar, ptForm.move_up2, ptForm.move_down2">
<%= sPropertyArrayAsOptions (g_aAvailCharVariant) %>
							</SELECT>
						</TD>
						<TD VALIGN="CENTER" ALIGN="CENTER">
							<BUTTON NAME="add_char_variant" DISABLED ID="add_char_variant" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="MoveSelectedItems ptForm.avail_char_variant, ptForm.ass_char_variant, g_aRemoveCharVariant, g_aAddCharVariant, ptForm.avail_char, ptForm.add_char, 'Add'"><%= L_Add_Button %></BUTTON>
							<BR>
							<BUTTON NAME="remove_char_variant" DISABLED ID="remove_char_variant" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="remove_char_onClick2 ptForm.ass_char_variant, ptForm.avail_char_variant, g_aAddCharVariant, g_aRemoveCharVariant, ptForm.avail_char, ptForm.copy_charvar, ptForm.edit_charvar, ptForm.move_up2, ptForm.move_down2, 'ass_char_variant'"><%= L_Remove_Button %></BUTTON>
						</TD>
						<TD STYLE="width:33%">
							<SELECT SIZE="15" NAME="ass_char_variant" ID="ass_char_variant" STYLE="width:100%" MULTIPLE 
								 LANGUAGE="VBScript" ONCHANGE="ass_char_onChange ptForm.ass_char_variant, ptForm.avail_char_variant, ptForm.remove_char_variant, ptForm.add_char_variant, ptForm.copy_charvar, ptForm.edit_charvar, ptForm.move_up2, ptForm.move_down2, 'ass_char_variant'">
<%= sPropertyArrayAsOptions (g_aAssCharVariant) %>
							</SELECT>
						</TD>
						<TD VALIGN="CENTER" ALIGN="CENTER">
							<BUTTON NAME="new_charvar" ID="new_charvar" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnNewPropertyDef ptForm.ass_char_variant"><%= L_New_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="copy_charvar" ID="copy_charvar" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnCopyPropertyDef ptForm.ass_char_variant"><%= L_Copy_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="edit_charvar" ID="edit_charvar" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnEditPropertyDef ptForm.ass_char_variant"><%= L_Edit_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="move_up2" ID="move_up2" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="onMove 'ass_char_variant', -1, ptForm.move_up2, ptForm.move_down2"><%= L_MoveUp_Button %></BUTTON>
							<BR>
							<BUTTON DISABLED NAME="move_down2" ID="move_down2" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="onMove 'ass_char_variant', 1, ptForm.move_up2, ptForm.move_down2"><%= L_MoveDown_Button %></BUTTON>
						</TD>
					</TR>
				</TABLE><BR>
		    ]]></template>
		</editsheet>
	</editsheets>
</xml>

<DIV ID='bdcontentarea' CLASS='editSheetContainer'>

	<FORM ACTION="" METHOD="POST" NAME='ptForm' ID="ptForm" ONTASK="ptForm_onSubmit()">
		<INPUT TYPE="Hidden" NAME="type" VALUE="<%= g_sActionType %>">
		<INPUT TYPE="Hidden" NAME="old_name" VALUE="<%= g_sOldName %>" ID="old_name">
		<INPUT TYPE="Hidden" NAME="added_chars" ID="added_chars">
		<INPUT TYPE="Hidden" NAME="removed_chars" ID="removed_chars">
		<INPUT TYPE="Hidden" NAME="added_chars_variant" ID="added_chars_variant">
		<INPUT TYPE="Hidden" NAME="removed_chars_variant" ID="removed_chars_variant">
		<DIV ID='esControl' 
			CLASS='editSheet' 
			ONCHANGE = 'onChange'
			ONREQUIRE ='setRequired(L_Required_Text)'
			ONVALID = 'setValid(L_Valid_Text)'
			MetaXML='esMeta' 
			DataXML='esData'><%= L_LoadingPropertyGroups_Text %>
		</DIV>
	</FORM>
</DIV>


<FORM ACTION="" METHOD="POST" NAME='closeForm' ID="closeForm">
	<INPUT TYPE="Hidden" NAME="type" VALUE="close">
</FORM>
<FORM ACTION="" METHOD="POST" NAME="ptForm2" ID="ptForm2" ONTASK="ptForm_onSaveNew">
</FORM>

</BODY>
</HTML>


<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

' ----- PUBLIC Declarations
Public g_sAction			' String representation of action to take on page
Public g_sHeader			' Tab header
Public g_sPtName			' Product Type Name
Public g_sOldName
Public g_sActionType
Public g_sStatusText		' Text to be displayed in the Status Bar 

Public g_aAddChar
Public g_aRemoveChar
Public g_aAddCharVariant
Public g_aRemoveCharVariant ' To hold changed properties

Public g_rsProperties

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables

	Perform_Processing_BasedOnAction

	Configure_AvailableAndAssigned_PropertiesArrays
End Sub

' ################################################################
'
Sub InitializeGlobalVariables ()

	Dim sItem

	g_sAction = Request.Form("type")
	g_sPtName = Request.Form("pt_name")

	ReDim g_aAssChar(-1)
	ReDim g_aAvailChar(-1)
	ReDim g_aAssCharVariant(-1)
	ReDim g_aAvailCharVariant(-1)

	If (g_sAction = "submitNew") Or _
	   (g_sAction = "createNew") Or _
	   (g_sAction = "submitEdit") Then

		SplitToDynamicArray Request.Form("ass_char"), ",", g_aAssChar
		SplitToDynamicArray Request.Form("ass_char_variant"), ",", g_aAssCharVariant
	End If

	Set g_rsProperties = g_oCatMgr.Properties
	' Retrieve list of available properties
	RetrieveCharArray g_aAvailChar
	For Each sItem In g_aAvailChar
		AddArray g_aAvailCharVariant, sItem
	Next
End Sub

' ################################################################
'
Sub Perform_Processing_BasedOnAction ()

	Select Case g_sAction
	Case "submitNew"
		Process_SubmitNEW

	Case "createNew"
		Process_CreateNEW

	Case "saveNew"
		Process_SaveNEW

	Case "submitEdit"
		Process_SubmitEDIT

	Case "add"
		Process_ADD

	Case "open"
		Process_OPEN

	End Select
End Sub

' ################################################################
'
Sub Process_SubmitNEW ()

	Dim bAssignAll
	Dim sPropertyName
	Dim sCreationError
	Dim sCreationErrorVariant

	'Capture user-entered data
	g_sPtName = Request.Form("name")
	g_sOldName = g_sPtName

	' Check for properties labeled as "assign to all defs"
	For Each sPropertyName In g_aAvailChar
		bAssignAll = getPropertyAttribute (sPropertyName, "AssignAll")
		If (bAssignAll = True) Then
			If Not (bInArray(g_aAssChar, sPropertyName) Or _
					bInArray(g_aAssCharVariant, sPropertyName)) Then

				AddArray g_aAssChar, sPropertyName
			End If
		End If
	Next

	'Create the new definition
	sCreationError = sCreateNewDefinition(g_sPtName, PRODUCT_DEFINITION, g_aAssChar)
	sCreationErrorVariant = sCreateNewDefinition(g_sPtName, VARIANT_DEFINITION, g_aAssCharVariant)
	If (sCreationError <> "") Or (sCreationErrorVariant <> "") Then
		g_sActionType = g_sAction
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sPtName))
		g_sHeader = sFormatString (L_ProductDefinition_HTMLText, Array(L_NewProduct_Text))
	Else
		g_sActionType = "submitEdit"
		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sPtName))
		g_sHeader = sFormatString (L_ProductDefinition_HTMLText, Array(g_sPtName))
	End If
End Sub

' ################################################################
'
Sub Process_CreateNEW ()

	Process_SubmitNEW

	If (g_sActionType = "submitEdit") Then
		ReDim g_aAssChar(-1)
		ReDim g_aAssCharVariant(-1)
		g_sActionType = "submitNew"

		Process_ADD
	End If
End Sub

' ################################################################
'
Sub Process_SaveNEW ()

	Process_SubmitEDIT

	ReDim g_aAssChar(-1)
	ReDim g_aAssCharVariant(-1)
	g_sActionType = "submitNew"

	Process_ADD
End Sub

' ################################################################
'
Sub Process_SubmitEDIT ()

	Dim bFlag
	Dim sItem
	Dim rsProperties

	On Error Resume Next

	g_sActionType = "submitEdit"
	g_sOldName = Request.Form("old_name")
	'Capture user-entered data
	g_sPtName = Request.Form("name")
	g_sHeader = sFormatString (L_ProductDefinition_HTMLText, Array(g_sPtName))

	g_aAddChar = Split(Request.Form("added_chars"), SEPARATOR1)
	g_aRemoveChar = Split(Request.Form("removed_chars"), SEPARATOR1)
	g_aAddCharVariant = Split(Request.Form("added_chars_variant"), SEPARATOR1)
	g_aRemoveCharVariant = Split(Request.Form("removed_chars_variant"), SEPARATOR1)

	For Each sItem In g_aRemoveChar
		bFlag = False
		If (False = getPropertyAttribute (sItem, "AssignAll")) Then
			bFlag = True
		Else
			If bInArray (g_aAssCharVariant, sItem) Then
				bFlag = True
			Else
				AddArray g_aAssChar, sItem
			End If
		End If

		If bFlag Then
			g_oCatMgr.RemoveDefinitionProperty g_sOldName, sItem
			If (Err.Number <> 0) Then Exit For
		End If
	Next

	If (Err.Number = 0) Then
		For Each sItem In g_aRemoveCharVariant
			g_oCatMgr.RemoveDefinitionProperty g_sOldName, sItem
			If (Err.Number <> 0) Then Exit For

			If (True = getPropertyAttribute (sItem, "AssignAll")) And _
				(False = bInArray (g_aAssChar, sItem)) Then

				AddArray g_aAddChar, sItem
				AddArray g_aAssChar, sItem
			End If
		Next

		If (Err.Number = 0) Then
			For Each sItem In g_aAddChar
				g_oCatMgr.AddDefinitionProperty g_sOldName, sItem
				'' Property already exists.
				'' Property created through a nested EditPage
				If (Hex(Err.Number) = &H889800D) Then Err.Clear
			Next

			For Each sItem In g_aAddCharVariant
				g_oCatMgr.AddDefinitionVariantProperty g_sOldName, sItem
			Next

			If (g_sOldName <> g_sPtName) Then
				Call g_oCatMgr.RenameDefinition(g_sOldName, g_sPtName)
				g_sOldName = g_sPtName
			End If
		End If
	End If

	If (Err.Number = 0) Then
		Set rsProperties = g_oCatMgr.GetDefinitionProperties (g_sPtName)
		ReorderPropertiesInDefinition rsProperties, g_aAssChar
		ReorderPropertiesInDefinition rsProperties, g_aAssCharVariant
		g_oCatMgr.SetDefinitionProperties g_sPtName, rsProperties
		ReleaseRecordset rsProperties

		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sPtName))
	Else
		ReDim g_aAssChar(-1)
		ReDim g_aAssCharVariant(-1)
		GetDefinitionProperties
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sPtName))
	End If
End Sub

' ################################################################
'
Sub Process_ADD ()

	If	(g_sPtName <> "") And _
		(g_sAction <> "saveNew") And _
		(g_sAction <> "createNew") Then
		''--- COPY
		GetDefinitionProperties
	Else
		Dim sProperty
		Dim bAssignAll

		For Each sProperty In g_aAvailChar
			bAssignAll = getPropertyAttribute(sProperty, "AssignAll")
			If (True = bAssignAll) Then	AddArray g_aAssChar, sProperty
		Next
	End If

	g_sPtName = ""
	g_sActionType = "submitNew"
	g_sHeader = sFormatString (L_ProductDefinition_HTMLText, Array(L_NewProduct_Text))
End Sub

' ################################################################
'
Sub GetDefinitionProperties ()

	Dim sPropertyName
	Dim rsProperties

	Set rsProperties = g_oCatMgr.GetDefinitionProperties(g_sPtName)
	While Not rsProperties.EOF
		sPropertyName = rsProperties.Fields("PropertyName")
		If (rsProperties.Fields("PropertyType") = 0) Then
				' = 0 Regular Property
			AddArray g_aAssChar, sPropertyName
		Else	' = 1 Variant Property
			AddArray g_aAssCharVariant, sPropertyName
		End If

		rsProperties.MoveNext
	Wend

	ReleaseRecordset rsProperties
End Sub

' ################################################################
'
Sub Process_OPEN ()

	GetDefinitionProperties

	g_sOldName = g_sPtName
	g_sActionType = "submitEdit"
	g_sHeader = sFormatString (L_ProductDefinition_HTMLText, Array(g_sPtName))
End Sub

' ################################################################
'
Sub Configure_AvailableAndAssigned_PropertiesArrays ()

	Dim sPropertyName

	'Remove assigned properties from available properties
	For Each sPropertyName In g_aAssChar
		DeleteArray g_aAvailChar, sPropertyName
		DeleteArray g_aAvailCharVariant, sPropertyName
	Next
	'Remove assigned variant properties from available variant properties
	For Each sPropertyName In g_aAssCharVariant
		DeleteArray g_aAvailChar, sPropertyName
		DeleteArray g_aAvailCharVariant, sPropertyName
	Next
End Sub

%>