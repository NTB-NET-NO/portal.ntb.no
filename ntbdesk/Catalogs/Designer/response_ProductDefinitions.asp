<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../../include/HTTPXMLUtil.asp' -->
<%
' ####################################################################
'
'   response_ProductDefinitions.ASP
'
'	Paging for Product Definitions
'
' ####################################################################

	'------------- CONST Declarations
	Const PAGE_SIZE = 20
	Const PAGE_TYPE = "RESPONSE"

	'------------- PUBLIC Declarations
	Public g_oCatMgr
	Public g_dRequest

	On Error Resume Next

	Set g_dRequest = dGetRequestXMLAsDict()
%>
<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<%= sGetXML %>
<%

' ####################################################################
'
Function sGetXML ()

	Dim iCnt
	Dim nPage
	Dim iLast
	Dim iFirst

	Dim rsProperties
	Dim rsDefinitions

	Dim sPropertyName
	Dim sDefinitionName

	'----- Retrieve Product Definitions
	InitializeCatalogObject
	If (g_oCatMgr Is Nothing) Then
		sGetXML = setXMLError (L_BadConnectionString_ErrorMessage, "")
		Exit Function
	End If

	On Error Resume Next	

	Set rsDefinitions = g_oCatMgr.ProductDefinitions
	nPage = g_dRequest("page")
	iFirst = PAGE_SIZE * (nPage - 1)
	iLast = iFirst + PAGE_SIZE
	iCnt = iFirst

	'----- Generate XML - START
	rsDefinitions.Move iFirst

	Do Until rsDefinitions.EOF Or (iCnt >= iLast)
		sDefinitionName = rsDefinitions.Fields("DefinitionName")
		sGetXML = sGetXML & "<record><name>" & Server.HTMLEncode(sDefinitionName) & "</name>"

		Set rsProperties = g_oCatMgr.GetDefinitionProperties (sDefinitionName)
		If (Err.Number = 0) Then
			Do Until rsProperties.EOF
				sPropertyName = rsProperties.Fields("PropertyName")
				sGetXML = sGetXML & "<record><property>" & Server.HTMLEncode(sPropertyName) & "</property></record>"

				rsProperties.MoveNext
			Loop
			rsProperties.Close
			Set rsProperties = Nothing
		Else
			Err.Clear
		End If

		sGetXML = sGetXML & "</record>"
		iCnt = iCnt + 1
		rsDefinitions.MoveNext
	Loop

	Do Until rsDefinitions.EOF
		iCnt = iCnt + 1
		rsDefinitions.MoveNext
	Loop

	If (Err.Number = 0) Then
		sGetXML = "<document recordcount='" & iCnt & "'>" & sGetXML & "</document>"
	Else
		sGetXML = setXMLError (L_ErrorsInPage_ErrorMessage, sGetScriptError(Err))
	End If
	'----- Generate XML - END

	rsDefinitions.Close
	Set rsDefinitions = Nothing
	Set g_oCatMgr = Nothing
End Function

' ###############################################################
' Sub: InitializeCatalogObject
'	Create and initialize a global catalog object
'
' Input:
'
Sub InitializeCatalogObject ()

	Dim sConnString

	On Error Resume Next
	Set g_oCatMgr = Nothing

	If (True = IsObject(Session("CatalogManager"))) Then
		If Not (Session("CatalogManager") Is Nothing) Then
			Dim rs
			Set g_oCatMgr = Session("CatalogManager")
			Set rs = g_oCatMgr.Catalogs
			If (Err.Number <> 0) Then
				Set g_oCatMgr = Nothing
				Set Session("CatalogManager") = Nothing
				Err.Clear
				Exit Sub
			End If
		End If
	End If

	If (g_oCatMgr Is Nothing) Then
		sConnString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")

		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (sConnString, True)
		If (Err.Number <> 0) Then
			Set g_oCatMgr = Nothing
			Err.Clear
		Else
			Set Session("CatalogManager") = g_oCatMgr	
		End If
	End If
End Sub

'' -------------------------------------------------------------------
''
Function setXMLError (ByRef sSource, ByRef sText)

	setXMLError = "<document recordcount='0'>" & "<ERROR ID='1' SOURCE='" & Server.HTMLEncode(sSource) & "'>" & Server.HTMLEncode(sText) & "</ERROR></document>"
	Err.Clear
End Function

%>