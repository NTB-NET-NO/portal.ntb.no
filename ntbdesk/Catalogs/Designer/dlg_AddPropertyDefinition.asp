<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="catDesignerSTRINGS.asp" -->
<%
Const PAGE_TYPE = "DIALOG"
%>

<HTML>
<HEAD>
<TITLE><%= L_NewPropertyDefinition_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
<!--
BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
-->
</STYLE>
<SCRIPT LANGUAGE='VBScript'>

option explicit

'----- event handlers
sub bdcancel_OnClick ()

	window.returnValue = ""
	window.close
end sub

sub bdcontinue_OnClick ()

	window.returnValue = adddlgform.bdnewtype.value
	window.close
end sub

</SCRIPT>
</HEAD>

<BODY LANGUAGE='VBScript' ONLOAD='bdcontinue.focus' ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<FORM ACTION='' METHOD='POST' ID='adddlgform'>
	<LABEL FOR='bdnewtype'>Type:</LABEL><BR><BR>
	<TABLE>
		<TR>
			<TD WIDTH="30px"></TD>
			<TD>
				<SELECT NAME="bdnewtype" ID="bdnewtype" SIZE="7">
					<OPTION VALUE="<%= L_String_Text %>" SELECTED><%= L_String_Text %></OPTION>
					<OPTION VALUE="<%= L_Number_Text %>"><%= L_Number_Text %></OPTION>
					<OPTION VALUE="<%= L_Float_Text %>"><%= L_Float_Text %></OPTION>
					<OPTION VALUE="<%= L_Currency_Text %>"><%= L_Currency_Text %></OPTION>
					<OPTION VALUE="<%= L_DateTime_Text %>"><%= L_DateTime_Text %></OPTION>
					<OPTION VALUE="<%= L_Enumerated_Text %>"><%= L_Enumerated_Text %></OPTION>
					<OPTION VALUE="<%= L_FileName_Text %>"><%= L_FileName_Text %></OPTION>
				</SELECT><BR>
			</TD>
		<TR>
	</TABLE>
</FORM>

<TABLE>
	<TR>
		<TD WIDTH="90px"></TD>
		<TD><BUTTON ID='bdcontinue'	CLASS='bdbutton' STYLE='WIDTH:75px'><%= L_Continue_Button %></BUTTON>
			<BUTTON ID='bdcancel' CLASS='bdbutton' STYLE='WIDTH:75px'><%= L_Cancel_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>