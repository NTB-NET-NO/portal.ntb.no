<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
' ###################################################################
'
'	edit_CategoryDefinition.ASP
'
'	View Page for Category Definitions.
'
' ###################################################################

	Const PAGE_TYPE = "EDIT"
	' ------------------------------
	Public g_aAssChar()
	Public g_aAvailChar()
%>
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

public g_oSelect

public g_aAddChar()		' An array to hold added properties
public g_aRemoveChar()	' An array to hold removed properties

sub window_onLoad ()

	redim g_aAddChar(-1)
	redim g_aRemoveChar(-1)

	esControl.focus
end sub

sub onChange ()

	setStatusText ""
	setDirty (L_SaveConfirmationDialog_Text)
end sub

sub SwitchFocus (ByRef oSelectTo, ByRef oSelectFrom, _
  		         ByRef oButtonTo, ByRef oButtonFrom)
	dim bDisabled

	if (oSelectTo.selectedIndex >= 0) then
		bDisabled = False
		if (oSelectFrom.selectedIndex >= 0) then
			call ClearFocus (oSelectFrom, oButtonFrom)
		end if
	else
		' Selection disabled (using CTRL key)
		bDisabled = True
	end if

	oButtonTo.disabled = bDisabled
end sub

sub ClearFocus (ByRef oSelect, ByRef oButton)

	if (oSelect.selectedIndex >= 0) then
		oSelect.selectedIndex = -1
		oButton.disabled = true
	end if
end sub

sub MoveSelectedItems (ByRef oSelectFrom, ByRef oSelectTo, _
					   ByRef aFrom, ByRef aTo)
	dim oButton
	dim newOption

	dim sValue
	dim sOption

	set oButton = window.event.srcElement

	for each sOption in oSelectFrom.options
		if sOption.selected then
			sValue = sOption.value
			if not bRemovedFromArray(aFrom, sValue) then AddArray aTo, sValue

			set newOption = document.createElement("OPTION")
			newOption.value = sValue
			newOption.text = sOption.text
			oSelectFrom.remove(sOption.index)
			oSelectTo.options.add(newOption)
			oButton.disabled = True
			oSelectFrom.focus
			set newOption = Nothing
		end if
	next

	onChange
end sub

sub cgForm_onSubmit ()

	dim sOption

	for each sOption in cgForm.ass_char.options
		sOption.selected = true
	next
	with cgForm
		.added_chars.value = sConvertArrayToString (g_aAddChar)
		.removed_chars.value = sConvertArrayToString (g_aRemoveChar)

		.submit()
	end with
end sub

sub cgForm_onSaveNew ()

	with cgForm.type
		select case .value
		case "submitNew"
			.value = "createNew"
		case else
			.value = "saveNew"
		end select
	end with

	cgForm_onSubmit
end sub

sub ass_char_onChange (ByRef oSelectTo, ByRef oSelectFrom, ByRef oButtonTo, ByRef oButtonFrom, _
					   ByRef oButtonCopy, ByRef oButtonEdit, ByRef oButtonMoveUp, ByRef oButtonMoveDown, ByRef sID)
	dim bValue

	SwitchFocus oSelectTo, oSelectFrom, oButtonTo, oButtonFrom
	DisableCopyEdit oButtonCopy, oButtonEdit, False
	if (nSelections (oSelectTo) <> 1) then
		' Multiple Selections with Shift/Ctrl keys
		bValue = True
	else
		bValue = False
	end if

	DisableCopyEdit oButtonCopy, oButtonEdit, bValue
	DisableMoveUpDown oButtonMoveUp, oButtonMoveDown, sID, bValue
end sub

sub avail_char_onChange (ByRef oSelectTo, ByRef oSelectFrom, ByRef oButtonTo, ByRef oButtonFrom, _
						 ByRef oButtonCopy, ByRef oButtonEdit, _
						 ByRef oButtonMoveUp, ByRef oButtonMoveDown)

	SwitchFocus oSelectTo, oSelectFrom, oButtonTo, oButtonFrom
	DisableCopyEdit oButtonCopy, oButtonEdit, True
	DisableMoveUpDown oButtonMoveUp, oButtonMoveDown, "", True
end sub

sub remove_char_onClick (ByRef oSelectFrom, ByRef oSelectTo, aAddChar, aRemoveChar, _
						 ByRef oButtonCopy, ByRef oButtonEdit, _
						 ByRef oButtonMoveUp, ByRef oButtonMoveDown, ByRef sID)

	MoveSelectedItems oSelectFrom, oSelectTo, aAddChar, aRemoveChar
	DisableCopyEdit oButtonCopy, oButtonEdit, True
	DisableMoveUpDown oButtonMoveUp, oButtonMoveDown, sID, True
end sub

sub DisableCopyEdit (ByRef oButtonCopy, ByRef oButtonEdit, ByVal bValue)

	oButtonCopy.disabled = bValue
	oButtonEdit.disabled = bValue
end sub

sub OnNewPropertyDef (ByRef oSelectAss)

	dim sType

	set g_oSelect = oSelectAss

	sType = window.showModalDialog("dlg_AddPropertyDefinition.asp", , _
				"dialogHeight:270px;dialogWidth:350px;status:no;help:no")
	if (sType <> "") then
		call setEditPageParameters (PROPERTY_DEF_PARAMETERS, _
									Array("type", "ch_type"), _
									Array("add", sType))
		call openEditPage("<%= g_sBizDeskRoot %>catalogs/designer/edit_PropertyDefinition.asp", "OnCallback")
	end if
end sub

sub OnCopyPropertyDef (ByRef oSelectAss)

	set g_oSelect = oSelectAss

	call setEditPageParameters (PROPERTY_DEF_PARAMETERS, _
								Array("type", "ch_name"), _
								Array("copy", sCurrentProperty(oSelectAss)))
	call openEditPage("<%= g_sBizDeskRoot %>catalogs/designer/edit_PropertyDefinition.asp", "OnCallback")
end sub

sub OnEditPropertyDef (ByRef oSelectAss)

	call setEditPageParameters (PROPERTY_DEF_PARAMETERS, _
								Array("type", "ch_name"), _
								Array("open", sCurrentProperty(oSelectAss)))
	call openEditPage("<%= g_sBizDeskRoot %>catalogs/designer/edit_PropertyDefinition.asp", "onEditPropertyDefCallback")
end sub

sub onEditPropertyDefCallback ()

end sub

function sCurrentProperty (ByRef oSelectAss)

	with oSelectAss
		sCurrentProperty = .options(.selectedIndex).value
	end with
end function

sub OnCallback ()

	dim sNewPropertyDef
	dim aNewPropertyDef

	sNewPropertyDef = getTemp ("sNewPropertyDef")
	if (sNewPropertyDef <> "") then
		call clearTemp ("sNewPropertyDef")

		aNewPropertyDef = Split (sNewPropertyDef, SEPARATOR1)

		for each sNewPropertyDef in aNewPropertyDef
			call AddOption (g_oSelect, sNewPropertyDef)
		next
	end if
end sub

sub AddOption (ByRef oSelect, ByRef sNewPropertyDef)

	dim newOption

	set newOption = document.createElement("OPTION")
	newOption.value = sNewPropertyDef
	newOption.text = sNewPropertyDef
	oSelect.options.add(newOption)

	if (g_oSelect.id = "ass_char") then
		AddArray g_aAddChar, sNewPropertyDef
	else
		AddArray g_aAddCharVariant, sNewPropertyDef
	end if

	onChange
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) Then
		Call setError ("", L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertEditTaskBar g_sHeader, g_sStatusText
%>

<xml id='esData'>
	<DOCUMENT>
		<RECORD>
			<name><%= Server.HTMLEncode(g_sCgName) %></name>
		</RECORD>
	</DOCUMENT>
</xml>

<xml id='esMeta'>
	<editsheets>
		<editsheet>
		    <global expanded='yes' title='yes'>
		        <name><%= L_CategoryDefProperties_Text %></name>
		        <key><%= L_CategoryDefProperties_Accelerator %></key>
			</global>
			<fields>
			    <text id='name' required='yes' maxlen='128' subtype='short'>
			        <name><%= L_Name_Text %></name>
			        <tooltip><%= L_Name_Tooltip %></tooltip>
			    </text>
		    </fields>
		</editsheet>
		<editsheet>
		    <global expanded='yes'>
		        <name><%= L_CategoryProperties_Text %></name>
				<key><%= L_CategoryProperties_Accelerator %></key>
		    </global>
		    <template register="ass_char, avail_char"><![CDATA[
				<BR>
				<TABLE>
				<TR>
					<TD><SPAN TITLE="<%= L_AvailableProperties_Tooltip %>"><%= L_AvailableProperties_Text %></SPAN></TD>
					<TD>&nbsp;</TD>
					<TD><SPAN TITLE="<%= L_AssignedProperties_Tooltip %>"><%= L_AssignedProperties_Text %></SPAN></TD>
				</TR>
				<TR>
					<TD STYLE="width:33%">
						<SELECT SIZE="18" NAME="avail_char" ID="avail_char" STYLE="width:100%" MULTIPLE
						 LANGUAGE="VBScript" ONCHANGE="avail_char_onChange cgForm.avail_char, cgForm.ass_char, cgForm.add_char, cgForm.remove_char, cgForm.copy_char, cgForm.edit_char, cgForm.move_up, cgForm.move_down">
<%= sPropertyArrayAsOptions(g_aAvailChar) %>
						</SELECT>
					</TD>
					<TD VALIGN="CENTER" ALIGN="CENTER">
						<BUTTON NAME="add_char" DISABLED ID="add_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="MoveSelectedItems cgForm.avail_char, cgForm.ass_char, g_aRemoveChar, g_aAddChar"><%= L_Add_Button %></BUTTON>
						<BR>
						<BUTTON NAME="remove_char" DISABLED ID="remove_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="remove_char_onClick cgForm.ass_char, cgForm.avail_char, g_aAddChar, g_aRemoveChar, cgForm.copy_char, cgForm.edit_char, cgForm.move_up, cgForm.move_down, 'ass_char'"><%= L_Remove_Button %></BUTTON>
					</TD>
					<TD STYLE="width:33%">
						<SELECT SIZE="18" NAME="ass_char" ID="ass_char" STYLE="width:100%" MULTIPLE
						 LANGUAGE="VBScript" ONCHANGE="ass_char_onChange cgForm.ass_char, cgForm.avail_char, cgForm.remove_char, cgForm.add_char, cgForm.copy_char, cgForm.edit_char, cgForm.move_up, cgForm.move_down, 'ass_char'">
<%= sPropertyArrayAsOptions(g_aAssChar) %>
						</SELECT>
					</TD>
					<TD VALIGN="CENTER" ALIGN="CENTER">
						<BUTTON NAME="new_char" ID="new_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnNewPropertyDef cgForm.ass_char"><%= L_New_Button %></BUTTON>
						<BR>
						<BUTTON DISABLED NAME="copy_char" ID="copy_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnCopyPropertyDef cgForm.ass_char"><%= L_Copy_Button %></BUTTON>
						<BR>
						<BUTTON DISABLED NAME="edit_char" ID="edit_char" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="OnEditPropertyDef cgForm.ass_char"><%= L_Edit_Button %></BUTTON>
						<BR>
						<BUTTON DISABLED NAME="move_up" ID="move_up" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="onMove 'ass_char', -1, cgForm.move_up, cgForm.move_down"><%= L_MoveUp_Button %></BUTTON>
						<BR>
						<BUTTON DISABLED NAME="move_down" ID="move_down" STYLE="width:90px" LANGUAGE="VBScript" ONCLICK="onMove 'ass_char', 1, cgForm.move_up, cgForm.move_down"><%= L_MoveDown_Button %></BUTTON>
					</TD>
				</TR>
				</TABLE><BR>
		    ]]></template>
		</editsheet>
	</editsheets>
</xml>

<DIV ID='bdcontentarea' CLASS='editSheetContainer'>

	<FORM ACTION="" METHOD="POST" NAME='cgForm' ID="cgForm" ONTASK="cgForm_onSubmit">
		<INPUT TYPE="Hidden" NAME="type" VALUE="<%= g_sActionType %>" ID="type">
		<INPUT TYPE="Hidden" NAME="old_name" VALUE="<%= g_sOldName %>" ID="old_name">
		<INPUT TYPE="Hidden" NAME="added_chars" ID="added_chars">
		<INPUT TYPE="Hidden" NAME="removed_chars" ID="removed_chars">
		<DIV ID='esControl'
			CLASS='editSheet'
			MetaXML='esMeta'
			DataXML='esData'
			ONCHANGE = 'onChange'
			ONREQUIRE ='setRequired(L_Required_Text)'
			ONVALID = 'setValid(L_Valid_Text)'><%= L_LoadingPropertyGroups_Text %>
		</DIV>
	</FORM>
</DIV>

<FORM ACTION="" METHOD="POST" NAME="closeForm" ID="closeForm">
	<INPUT TYPE="Hidden" NAME="type" VALUE="close">
</FORM>
<FORM ACTION="" METHOD="POST" NAME="cgForm2" ID="cgForm2" ONTASK="cgForm_onSaveNew">
</FORM>

</BODY>
</HTML>
<%
'	####################
	ReleaseGlobalObjects
'	####################
%>


<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

' ----- PUBLIC Declarations
Public g_sAction	' String representation of action to take on page
Public g_sActionType' Type to pass through

Public g_sHeader	' Tab header
Public g_sStatusText' To be displayed in the Status Bar

Public g_sCgName	' Category Definition Name
Public g_sOldName

' To hold data on property definitions
Public g_aAddChar
Public g_aRemoveChar

Public g_rsProperties	' Catalog Property Definitions

' ################################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables

	Perform_Processing_BasedOnAction

	Configure_AvailableProperties_Array
End Sub

' ################################################################
'
Sub InitializeGlobalVariables ()

	g_sAction = Request.Form("type")

	ReDim g_aAssChar(-1)
	ReDim g_aAvailChar(-1)

	Set g_rsProperties = g_oCatMgr.Properties
	' Retrieve list of available properties
	RetrieveCharArray g_aAvailChar
End Sub

' ################################################################
'
Sub Perform_Processing_BasedOnAction ()

	Select Case g_sAction
	Case "submitNew"
		Process_SubmitNEW

	Case "createNew"
		Process_CreateNEW

	Case "saveNew"
		Process_SaveNEW

	Case "submitEdit"
		Process_SubmitEDIT

	Case "add", "copy"
		Process_ADD

	Case "open"
		Process_OPEN

	End Select
End Sub

' ################################################################
'
Sub Process_SubmitNEW ()

	Dim sItem
	Dim sCreationError

	'Capture user-entered data
	g_sCgName  = Trim (Request.Form("name"))
	g_sOldName = g_sCgName
	SplitToDynamicArray Request.Form("ass_char"), ",", g_aAssChar

	'Create the new definition
	sCreationError = sCreateNewDefinition(g_sCgName, CATEGORY_DEFINITION, g_aAssChar)
	If (sCreationError <> "") Then
		g_sActionType = "submitNew"
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sCgName))
		g_sHeader = sFormatString (L_CategoryDefinition_HTMLText, Array(L_NewCategory_Text))
	Else
		g_sActionType = "submitEdit"
		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sCgName))
		g_sHeader = sFormatString (L_CategoryDefinition_HTMLText, Array(g_sCgName))
	End If
End Sub

' ################################################################
'
Sub Process_CreateNEW ()

	Process_SubmitNEW

	If (g_sActionType = "submitEdit") Then
		ReDim g_aAssChar(-1)
		g_sAction = "add"

		Process_ADD
	End If
End Sub

' ################################################################
'
Sub Process_SaveNEW ()

	Process_SubmitEDIT

	If (g_sActionType = "submitEdit") Then
		ReDim g_aAssChar(-1)
		g_sAction = "add"

		Process_ADD
	End If
End Sub

' ################################################################
'
Sub Process_SubmitEDIT ()

	Dim rsProperties
	Dim sPropertyName

	g_sActionType = "submitEdit"
	g_sOldName = Request.Form("old_name")
	'Capture user-entered data
	g_sCgName = Trim (Request.Form("name"))
	SplitToDynamicArray Request.Form("ass_char"), ",", g_aAssChar
	g_aAddChar = Split (Request.Form("added_chars"), SEPARATOR1)
	g_aRemoveChar = Split(Request.Form("removed_chars"), SEPARATOR1)

	For Each sPropertyName In g_aAddChar
		If (sPropertyName <> "") Then g_oCatMgr.AddDefinitionProperty g_sOldName, sPropertyName
	Next

	For Each sPropertyName In g_aRemoveChar
		If (sPropertyName <> "") Then g_oCatMgr.RemoveDefinitionProperty g_sOldName, sPropertyName
	Next

	If (g_sOldName <> g_sCgName) Then
		Call g_oCatMgr.RenameDefinition(g_sOldName, g_sCgName)
		g_sOldName = g_sCgName
	End If

	Set rsProperties = g_oCatMgr.GetDefinitionProperties (g_sCgName)
	ReorderPropertiesInDefinition rsProperties, g_aAssChar
	g_oCatMgr.SetDefinitionProperties g_sCgName, rsProperties
	ReleaseRecordset rsProperties

	g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sCgName))
	g_sHeader = sFormatString (L_CategoryDefinition_HTMLText, Array(g_sCgName))
End Sub

' ################################################################
'
Sub Process_ADD ()

	If (g_sAction = "copy") Then
		g_sCgName = Request.Form("name")
		getDefinitionProperties g_sCgName, g_aAssChar
	End If

	g_sCgName = ""
	g_sActionType = "submitNew"
	g_sHeader = sFormatString (L_CategoryDefinition_HTMLText, Array(L_NewCategory_Text))
End Sub

' ################################################################
'
Sub Process_OPEN ()

	g_sCgName = Request.Form("name")
	getDefinitionProperties g_sCgName, g_aAssChar

	g_sOldName = g_sCgName
	g_sActionType = "submitEdit"
	g_sHeader = sFormatString (L_CategoryDefinition_HTMLText, Array(g_sCgName))
End Sub

' ################################################################
'
Sub Configure_AvailableProperties_Array ()

	Dim sPropertyName

	'Remove assigned properties from available properties
	For Each sPropertyName In g_aAssChar
		DeleteArray g_aAvailChar, sPropertyName
	Next
End Sub

' ################################################################
'
Sub ReleaseGlobalObjects ()

	Set g_oCatMgr = Nothing
End Sub

%>