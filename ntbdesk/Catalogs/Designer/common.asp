<SCRIPT LANGUAGE="VBScript">
'import display constants
const BLINK_SPEED = 3
const SECONDS_TO_CHECK = 45
'import display vars
public bImportToggle
public oImportTimer
public nImportCount

const SEPARATOR1 = "|||"
const SEPARATOR2 = "$!$"
' -----------------------------------------------------
const ALL_DEFINITIONS = "All Definitions"
const PROPERTY_DEF_PARAMETERS = "PropertyDefParameters"

' ------ CATALOG DataType Enum
const INTEGER_TYPE = 0
const BIGINTEGER_TYPE = 1
const FLOAT_TYPE = 2
const DOUBLE_TYPE = 3
const BOOLEAN_TYPE = 4
const STRING_TYPE = 5
const DATETIME_TYPE = 6
const CURRENCY_TYPE = 7
const FILEPATH_TYPE = 8
const ENUMERATION_TYPE = 9
' ------------------------
const DATE_TYPE = 10
const TIME_TYPE = 11

' ###############################################################
' Sub: AddArray
'	This subroutine adds a string selection to a string array
'
' Input: 
'	aArray - the array
'	sString - the string
'
sub AddArray (ByRef aArray, sString)

	dim iNewUBound

	iNewUBound = UBound(aArray) + 1
	redim preserve aArray(iNewUBound)

	aArray(iNewUBound) = sString
end sub

' ###############################################################
' Sub: DeleteArray
'	This subroutine deletes a string selection from a string array 
'
' Input: 
'	aArray - the array
'	sString - the string
'
sub DeleteArray (ByRef aArray, ByRef sString)

	dim i
	dim j
	dim iUBound

	iUBound = UBound(aArray)
	for i = 0 to iUBound
		if (aArray(i) = sString) then
			for j = i to iUBound - 1
				aArray(j) = aArray(j+1)
			next

			redim preserve aArray(iUBound - 1)
			exit for
		end if
	next
end sub

' ###############################################################
'
sub SplitToDynamicArray (ByRef sText, ByRef sDelimiter, ByRef aDynamic)

	dim aTemp
	dim sItem

	aTemp = Split(sText, sDelimiter)
	for each sItem in aTemp
		AddArray aDynamic, Trim(sItem)
	next
end sub

' ###############################################################
' Function: bRemovedFromArray
'	This function deletes a string selection from a string array if it
'	exists.
'
' Input: 
'	aArray - the array
'	sString - the string
'
' Returns:
'	true if item was in array, false if not
'
function bRemovedFromArray (ByRef aArray, ByRef sString)

	dim iTemp

	bRemovedFromArray = False

	for iTemp = 0 To UBound(aArray)
		if (aArray(iTemp) = sString) then
			bRemovedFromArray = True

			if (iTemp < UBound(aArray)) then aArray(iTemp) = aArray(UBound(aArray))
			redim preserve aArray(UBound(aArray) - 1)
			exit for
		end if
	next
end function

' ###############################################################
' Sub: SelectAllToArray
'	This subroutine copies all the "name" attributes into the "aArray"
'
' Input:
'	aArray - the array
'
sub SelectAllToArray (ByRef aArray)

	dim xml
	dim oName
	dim listNames

	redim aArray(-1)

	set xml = lsData.XMLdocument.documentElement
	set listNames = xml.selectNodes ("//name")
	for each oName in listNames
		AddArray aArray, oName.text
	next
end sub

' ###############################################################
' Sub: DeselectAllToArray
'	This subroutine deletes all items in the array passed in and sets
'	array(0) to an empty string
'
' Input:
'	aArray - the array
'
sub DeselectAllToArray (ByRef aArray)

	redim aArray(-1)
end sub

' ###############################################################
' Function: sConvertArrayToString
'	This function converts an array into a triple-pipe-delimited string
' 	("|||") for passing between pages
'
' Input:
'	aArray - the array to load
'
'
function sConvertArrayToString (ByRef aArray)

	dim sItem

	for each sItem in aArray
		sConvertArrayToString = sConvertArrayToString & sItem + SEPARATOR1
	next

	if (sConvertArrayToString <> "") then sConvertArrayToString = Left(sConvertArrayToString, Len(sConvertArrayToString) - Len(SEPARATOR1))
end function

' ###############################################################
'
sub OnNewPage (ByRef sForm)

	document.all.item(sForm).page_id.value = window.event.page
end sub

' ###############################################################
'
sub DisableMoveUpDown (ByRef oButtonMoveUp, ByRef oButtonMoveDown, ByRef sID, ByVal bValue)

	dim bDisableUp
	dim bDisableDown

	if (True = bValue) then
		oButtonMoveUp.disabled = True
		oButtonMoveDown.disabled = True
	else
		with document.all(sID)
			if (.selectedIndex > 0) then
				bDisableUp = False
			else
				bDisableUp = True
			end if
			if (.selectedIndex < .options.length - 1) then
				bDisableDown = False
			else
				bDisableDown = True
			end if
			oButtonMoveUp.disabled = bDisableUp
			oButtonMoveDown.disabled = bDisableDown
		end with
	end if
end sub

' ###############################################################
'
function nSelections (ByRef oSelect)

	dim oOption

	for each oOption in oSelect.options
		if (True = oOption.selected) then nSelections = nSelections + 1
	next
end function

' ###############################################################
'
sub onMove (ByRef sID, ByVal iDirection, ByRef oBtnUp, oBtnDown)

	dim iIndex
	dim oFrom

	with document.all(sID)
		set oFrom = .options(.selectedIndex)
		iIndex = .selectedIndex + iDirection

		.options.remove .selectedIndex
		.options.add oFrom, iIndex
		.selectedIndex = iIndex

		if (.selectedIndex = 0) then
			oBtnUp.disabled = True
			oBtnDown.disabled = False
		elseif(.selectedIndex = .options.length - 1) then
			oBtnUp.disabled = False
			oBtnDown.disabled = True
		else
			oBtnUp.disabled = False
			oBtnDown.disabled = False
		end if
	end with

	onChange
end sub

sub setEditPageParameters (ByRef sDictName, ByRef aKeys, ByRef aValues)

	dim iIndex
	dim dParams
	dim dServerState

	set dParams = CreateObject ("Scripting.Dictionary")
	for iIndex=0 to UBound(aKeys)
		dParams.Add aKeys(iIndex), aValues(iIndex)
	next
	set dServerState = dSetServerState (sDictName, dParams)

	set dParams = Nothing
	set dServerState = Nothing
end sub

'' -------------------------------------------------------------------
''
''	Function:		ImportProgress
''
''	Description:	
''					
''	Arguments:		sDictName - 
''					aKeys-
''					aValues - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub ImportProgress ()
	if bImportToggle then
		call SetStatusText ("<%= L_CatalogImportInProgress_StatusBar %>")
	else
		call ClearStatusText ()
	end if
	nImportCount = nImportCount + BLINK_SPEED
	if nImportCount >= SECONDS_TO_CHECK then call CheckImportProgress ()
end sub

'' -------------------------------------------------------------------
''
''	Function:		CheckImportProgress
''
''	Description:	
''					
''	Arguments:		sDictName - 
''					aKeys-
''					aValues - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub CheckImportProgress ()
	dim nImportOps, oXMLHTTPObj, bAsync
	'create XMLHTTP object and query page that executes the following query and returns the result:
	'  SELECT Count(StatusID) AS ImportOps FROM CatalogStatus WHERE EndDate = NULL OR Status <> 0
On Error Resume Next
	set oXMLHTTPObj = CreateObject("MSXML2.XMLHTTP.2.6")
	bAsync = False
	'response page just returns an integer - greater than zero means import in progress
	oXMLHTTPObj.open "POST", "Response_Import.asp", bAsync
	oXMLHTTPObj.send 
	nImportOps = CInt(oXMLHTTPObj.responseText)
	if (Err.Number <> 0) then nImportOps = -1

	if nImportOps > 0 then
		'an import is in progress so continue counting
		nImportCount = 0
	else
		window.clearInterval(oImportTimer)
		call SetTemp("bCatImporting", false)
		dim sStatus
		if nImportOps = -1 then
			sStatus = "<%= L_CatalogImportErrors_StatusBar %>"
		else
			sStatus = "<%= L_CatalogImportComplete_StatusBar %>"
		end if
		call SetStatusText (sStatus)
	end if
end sub

'' -------------------------------------------------------------------
''
''	Function:		window_onunload
''
''	Description:	
''					
''	Arguments:		sDictName - 
''					aKeys-
''					aValues - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub window_onUnload ()
	if not isEmpty(oImportTimer) then window.clearInterval(oImportTimer)
end sub

'' -------------------------------------------------------------------
''
function sStatusBarText ()

	if (g_iSelectedCount = 1) then
		sStatusBarText = sFormatString (L_DefinitionSelected_StatusBar, Array(g_aSelected(0)))
	else
		sStatusBarText = sFormatString (L_DefinitionsSelected_StatusBar, Array(g_iSelectedCount))
	end if
end function

</SCRIPT>

<%
' ----- CONST Values
Const PRODUCT_DEFINITION = 0
Const CATEGORY_DEFINITION = 1
Const VARIANT_DEFINITION = 2 ' PRODUCT with VARIANTs
' ------ Range of SQL Data Types
Const MIN_INT = -2147483648
Const MAX_INT =  2147483647
Const MIN_FLOAT = -1.79E+38
Const MAX_FLOAT =  1.79E+38
Const MIN_MONEY = -922337203685477
Const MAX_MONEY =  922337203685477

' ------ CATALOG DataType Enum
Const INTEGER_TYPE = 0
Const BIGINTEGER_TYPE = 1
Const FLOAT_TYPE = 2
Const DOUBLE_TYPE = 3
Const BOOLEAN_TYPE = 4
Const STRING_TYPE = 5
Const DATETIME_TYPE = 6
Const CURRENCY_TYPE = 7
Const FILEPATH_TYPE = 8
Const ENUMERATION_TYPE = 9

Const DATE_TYPE = 10
Const TIME_TYPE = 11

Const SEPARATOR1 = "|||"
Const SEPARATOR2 = "$!$"

Const ERR_PROPERTY_ALREADY_MODIFIED = -2003304442

Const PAGE_SIZE = 20
' -----------------------------------------------------
Const ALL_DEFINITIONS = "All Definitions"
Const PROPERTY_DEF_PARAMETERS = "PropertyDefParameters"

'----- PUBLIC Declarations
Public g_oCatMgr		' The Catalog Manager Object
Public g_sConnString	' Catalogs Connection String
Public g_sThousands		' Thousands Separator

' ###############################################################
' Sub: InitializeCatalogObject
'	Create and initialize a global catalog object
'
' Input:
'
Sub InitializeCatalogObject ()

	Const myLOCALE_STHOUSAND = 15

	On Error Resume Next
	Set g_oCatMgr = Nothing
	g_sThousands = g_MSCSDataFunctions.GetLocaleInfo(myLOCALE_STHOUSAND, Application("MSCSCurrencyLocale"))

	If (True = IsObject(Session("CatalogManager"))) Then
		If Not (Session("CatalogManager") Is Nothing) Then
			Dim rs
			Set g_oCatMgr = Session("CatalogManager")
			Set rs = g_oCatMgr.Catalogs
			If (Err.Number <> 0) Then
				Set g_oCatMgr = Nothing
				Set Session("CatalogManager") = Nothing
				Call setError (L_Critical_DialogTitle, L_BadConnectionString_ErrorMessage, _
							   sGetScriptError(Err), ERROR_ICON_CRITICAL)
				Err.Clear
				Exit Sub
			End If
		End If
	End If

	If (g_oCatMgr Is Nothing) Then
		g_sConnString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")

		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (g_sConnString, True)
		If (Err.Number <> 0) Then
			Set g_oCatMgr = Nothing
			Call setError (L_Critical_DialogTitle, L_BadConnectionString_ErrorMessage, _
						   sGetScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
		Else
			Set Session("CatalogManager") = g_oCatMgr
		End If
	End If
End Sub

' ###############################################################
' Function: bInArray
'	Checks if a string is in an array
'
' Input:
'	aArray - the array
'	sString - the string
'
' Returns:
'	true if item was in array, false if not
'
Function bInArray (ByRef aArray, sString)

	Dim sItem

	bInArray = False

	For Each sItem In aArray
		If (sItem = sString) Then
			bInArray = True
			Exit For
		End If
	Next
End Function

' ###############################################################
' Sub: AddArray
'	This subroutine adds a string selection to a string array
'
' Input:
'	aArray - the array
'	sString - the string
'
Sub AddArray (ByRef aArray, ByRef sString)

	Dim iNewUBound

	iNewUBound = UBound(aArray) + 1
	ReDim Preserve aArray(iNewUBound)

	aArray(iNewUBound) = sString
End Sub

' ###############################################################
' Sub: DeleteArray
'	This subroutine deletes a string selection from a string array
'
' Input:
'	aArray - the array
'	sString - the string
'
Sub DeleteArray (ByRef aArray, ByRef sString)

	Dim i
	Dim j
	Dim iUBound

	iUBound = UBound(aArray)
	For i = 0 To iUBound
		If (aArray(i) = sString) Then
			For j = i To iUBound - 1
				aArray(j) = aArray(j+1)
			Next

			Redim Preserve aArray(iUBound - 1)
			Exit For
		End If
	Next
End Sub

' #####################################################################
' Function: sCreateNewDefinition
'	This function creates a new definition
'
' Input:
'	sName - the name of the new definition
'	iType - type of definition
'	aProperties - fields to be associated with definition
'
' Returns:
'	string error message if error
'
Function sCreateNewDefinition (ByRef sDefinitionName, ByVal iType, ByRef aProperties)

	Dim sPropertyName

	If (Trim(sDefinitionName) = "") Then
		sCreateNewDefinition = L_EmptyDefinitionName_ErrorMessage
		Exit Function
	End If

	On Error Resume Next
	' Create Definition Based on Type
	Select Case iType
	Case CATEGORY_DEFINITION
		g_oCatMgr.CreateCategoryDefinition sDefinitionName
	Case PRODUCT_DEFINITION
		g_oCatMgr.CreateProductDefinition sDefinitionName
	Case VARIANT_DEFINITION
	End Select
	' Add Properties To Definition
	If (Err.Number = 0) Then
		For Each sPropertyName In aProperties
			If (iType = VARIANT_DEFINITION) Then
				g_oCatMgr.AddDefinitionVariantProperty sDefinitionName, sPropertyName
			Else
				g_oCatMgr.AddDefinitionProperty sDefinitionName, sPropertyName
			End If
		Next
	End If
	If (Err.Number <> 0) Then
		sCreateNewDefinition = Err.Description
		Call setError (L_CreateDefinition_DialogTitle, sFormatString (L_ErrorCreatingDefinition_ErrorMessage, Array(sDefinitionName)), sGetScriptError(Err), ERROR_ICON_INFORMATION)
		Err.Clear
	End If
End Function

' ###############################################################
' Sub: RetrieveCharArray
'	Retrieves a list of characteristics (properties)
'   from the catalog manager object
'
' Input:
'	aProperties - dynamic array to fill (should be empty)
'
Sub RetrieveCharArray (ByRef aProperties)

	Dim sPropertyName

	If g_rsProperties.EOF Then Exit Sub
	g_rsProperties.MoveFirst

	Do Until g_rsProperties.EOF
		sPropertyName = g_rsProperties.Fields("PropertyName")
		AddArray aProperties, sPropertyName

		g_rsProperties.MoveNext
	Loop
End Sub

' #####################################################################
' Sub: sPropertyArrayAsOptions
'	Returns an array as a series of option elements
'
' Input:
'	aArray - array to convert
'
Function sPropertyArrayAsOptions (ByRef aArray)

	Dim bAssignAll
	Dim sPropertyName

	For Each sPropertyName In aArray
		sPropertyArrayAsOptions = sPropertyArrayAsOptions & _
					"<OPTION VALUE='" & sPropertyName & "'"
		bAssignAll = getPropertyAttribute (sPropertyName, "AssignAll")
		If (bAssignAll = True) Then _
			sPropertyArrayAsOptions = sPropertyArrayAsOptions & " CLASS='clsOPTIONRequired'"

		sPropertyArrayAsOptions = sPropertyArrayAsOptions & ">" & sPropertyName & "</OPTION>"
	Next
End Function

' #####################################################################
' Sub: DeleteDefinitions
'	Deletes definitions from the catalog object
'
' Input:
'	sDeleteString - string of definition names to delete
'
Sub DeleteDefinitions (ByRef sDefinitions)

	Dim rs
	Dim nDeleted
	Dim aDefinitions

	Dim sDefinition
	Dim sDefinitionName

	aDefinitions = Split(sDefinitions, SEPARATOR1)
	If (aDefinitions(0) = ALL_DEFINITIONS) Then
		' Delete All items
		Do Until g_rsDefinitions.EOF
			sDefinitionName = g_rsDefinitions("DefinitionName")
			DeleteDefinition sDefinitionName, sDefinition, nDeleted
			g_rsDefinitions.MoveNext
		Loop
	Else
		' Delete selected items
		For Each sDefinitionName In aDefinitions
			DeleteDefinition sDefinitionName, sDefinition, nDeleted
		Next
	End If

	Select Case nDeleted
	Case 0
		g_sStatusText = L_NoDefinitionsDeleted_StatusBar
	Case 1
		g_sStatusText = sFormatString (L_Deleted_StatusBar, Array(sDefinition))
	Case Else
		g_sStatusText = sFormatString (L_DefinitionsDeleted_StatusBar, Array(nDeleted))
	End Select
End Sub

' #################################################################
'
Sub DeleteDefinition (ByRef sDefinitionName, ByRef sDefinition, ByRef nDeleted)

	On Error Resume Next
	g_oCatMgr.DeleteDefinition sDefinitionName
	If (Err.Number = 0) Then
		nDeleted = nDeleted + 1
		sDefinition = sDefinitionName
	Else
		Call setError (L_DeleteDefinition_DialogTitle, sFormatString (L_ErrorDeletingDefinition_ErrorMessage, Array(sDefinitionName)), sGetScriptError(Err), ERROR_ICON_INFORMATION)
		Err.Clear
	End If
End Sub

' #################################################################
'
Function sGetOnOff (ByRef sValue)

	If (sValue = "on") Or (sValue = True) Or (sValue = "1") Then
		sGetOnOff = "1" 'On
	Else'If (sValue = "off") Or (sValue = False) Then
		sGetOnOff = "0"
	End If
End Function

' #################################################################
'
Function sConvertFromBoolean (ByVal bValue)

	If (bValue = True) Then
		sConvertFromBoolean = "1"
	Else
		sConvertFromBoolean = "0"
	End If
End Function

' #################################################################
'
Function bConvertToBoolean (ByRef sValue)

	If (sValue = "1") Then
		bConvertToBoolean = True
	Else
		bConvertToBoolean = False
	End If
End Function

' #################################################################
'
Sub ReleaseRecordset (ByRef rs)

	If (True = IsObject (rs)) Then
		If (Not rs Is Nothing) Then
			rs.Close
			Set rs = Nothing
		End If
	End If
End Sub

' ###############################################################
'
Sub SplitToDynamicArray (ByRef sText, ByRef sDelimiter, ByRef aDynamic)

	Dim aTemp
	Dim sItem

	aTemp = Split(sText, sDelimiter)
	For Each sItem In aTemp
		AddArray aDynamic, Trim(sItem)
	Next
End Sub

' ###############################################################
'
'	[in]:	sPropertyName
'	[in]:	sAttribute
'
'	[out]:	returns VARIANT
'
Function getPropertyAttribute (ByRef sPropertyName, ByRef sAttribute)

	On Error Resume Next

	g_rsProperties.MoveFirst
	Do Until g_rsProperties.EOF
		If (g_rsProperties ("PropertyName").Value = sPropertyName) Then _
			Exit Do

		g_rsProperties.MoveNext
	Loop

	getPropertyAttribute = -1
	If (g_rsProperties.EOF = False) Then
		getPropertyAttribute = g_rsProperties.Fields(sAttribute)
	End If
End Function

' ###############################################################
'
'	[in]:	sPropertyName
'
'	[out]:	aProperties - DYNAMIC ARRAY
'
Sub getDefinitionProperties (ByRef sPropertyName, ByRef aProperties)

	Dim rsProperties

	Set rsProperties = g_oCatMgr.GetDefinitionProperties(sPropertyName)
	While Not rsProperties.EOF
		AddArray aProperties, rsProperties.Fields("PropertyName").Value
		rsProperties.MoveNext
	Wend

	ReleaseRecordset rsProperties
End Sub

' #################################################################
'
Sub ReorderPropertiesInDefinition (ByRef rsProperties, ByRef aProperties)

	Dim iIndex

	If (True = IsEmpty(rsProperties)) Then Exit Sub

	For iIndex = 0 To UBound(aProperties)
		Do Until rsProperties.EOF
			If (rsProperties.Fields("PropertyName").Value = aProperties(iIndex)) Then
				rsProperties.Fields("PropertyOrder").Value = iIndex
				Exit Do
			End If

			rsProperties.MoveNext
		Loop

		rsProperties.MoveFirst
	Next
End Sub

' #################################################################
' Function: iEnumType
'	Converts field types to CatalogDataTypeEnum
'
' Input: 
'	sDisplayString - string display of field type
'
Function iEnumType (ByRef sDisplayString)

    Select Case sDisplayString
	Case L_Number_Text
		iEnumType = 0
	Case L_Float_Text
		iEnumType = 2
	Case L_String_Text
		iEnumType = 5
	Case L_DateTime_Text
		iEnumType = 6
	Case L_Currency_Text
		iEnumType = 7
	Case L_FileName_Text
		iEnumType = 8
	Case L_Enumerated_Text
		iEnumType = 9
	End Select
End Function

' #################################################################
' Function: sEnumType
'	Converts CatalogDataTypeEnum values to their string representation
'
' Input: 
'	iDataType - one of the CatalogDataTypeEnum values
'
Function sEnumType (ByVal iDataType)

    Select Case iDataType
	Case 0
		sEnumType = L_Number_Text
	Case 2
		sEnumType = L_Float_Text
	Case 5
		sEnumType = L_String_Text
	Case 6
		sEnumType = L_DateTime_Text
	Case 7
		sEnumType = L_Currency_Text
	Case 8
		sEnumType = L_FileName_Text
	Case 9
		sEnumType = L_Enumerated_Text
	End Select
End Function

'' -------------------------------------------------------------------
''
Function setXMLError (ByRef sSource, ByRef sText)

	setXMLError = "<document recordcount='0'>" & "<ERROR ID='1' SOURCE='" & Server.HTMLEncode(sSource) & "'>" & Server.HTMLEncode(sText) & "</ERROR></document>"
	Err.Clear
End Function

%>