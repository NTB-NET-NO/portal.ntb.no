<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../../include/ASPUtil.asp" -->
<%
	Const PAGE_TYPE = "DIALOG"
	''---------------------------
	Perform_ServerSide_Processing
%>
<!--#INCLUDE FILE="catDesignerSTRINGS.asp" -->
<!--#INCLUDE FILE="common.asp" -->

<HTML>
<HEAD>
<TITLE><%= L_EnumeratedValues_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
<!--
BODY
{
	PADDING:15px;
	MARGIN:0;
}
-->
</STYLE>

<SCRIPT LANGUAGE='VBScript'>

option explicit

public g_aOptions()

'----- event handlers
sub window_onLoad ()

	dim iLen
	dim sOptions

	redim g_aOptions(-1)

	sOptions = "<%= Request.QueryString("options") %>"
	iLen = Len(sOptions)
	if (iLen > 0) then
		sOptions = Right(sOptions, iLen - Len(SEPARATOR1))
		SplitToDynamicArray sOptions, SEPARATOR1, g_aOptions
	end if
end sub

sub newValue_onKeyUp ()

	dim bDisabled

	if (newValue.value <> "") then
		bDisabled = False
	else
		bDisabled = True
	end if

	btnAdd.disabled = bDisabled
end sub

sub btnAdd_onClick ()

	dim sValue
	dim bWarning
	dim newOption

	bWarning = False
	sValue = Trim (newValue.value)
	if (sValue <> "") then
		if bIsInList (values, sValue) then
			bWarning = True
			MsgBox L_ValueMustBeUnique_Text, 48, L_Warning_Text
		else
			set newOption = document.createElement("OPTION")
			newOption.value = sValue
			newOption.text = sValue
			values.options.add(newOption)

			AddArray g_aOptions, sValue
			set newOption = Nothing
		end if
	end if

	if not bWarning then
		newValue.value = ""
		btnAdd.disabled = True
	end if
	newValue.focus
end sub

sub btnDelete_onClick ()

	dim iIndex
	dim sValue

	with values.options
		iIndex = .selectedIndex
		sValue = values.options(iIndex).text
		DeleteArray g_aOptions, sValue
		.remove iIndex

		if (iIndex < .length) then
			.selectedIndex = iIndex
		else
			.selectedIndex = .length - 1
		end if

		if (.length = 0) then btnDelete.disabled = True
	end with
end  sub

sub values_onChange ()

	dim bDisabled

	if (values.selectedIndex >= 0) then
		bDisabled = False
	else
		bDisabled = True
	end if

	btnDelete.disabled = bDisabled
end sub

sub btnCancel_onClick ()

	window.returnValue = ""
	window.close
end sub

sub btnOK_onClick ()
	dim sOptions, sValue

	window.returnValue = SEPARATOR1 & Join(g_aOptions, SEPARATOR1)
	window.close
end sub

function bIsInList (ByRef elList, ByRef sValue)

	dim elItem

	for each elItem in elList.options
		if (UCase (elItem.text) = UCase (sValue)) then
			bIsInList = True
			exit function
		end if
	next

	bIsInList = False
end function

</SCRIPT>
</HEAD>

<BODY LANGUAGE='VBScript' ONLOAD='btnOK.focus' ONKEYUP="if window.event.keyCode = 27 then btnCancel.click">

<TABLE>
	<TR>
		<TD>
		<LABEL FOR='values'><%= L_Values_Text %></LABEL><BR>
		<SELECT SIZE="10" ID="values" STYLE="width:300px">
<%= g_sOptions %>
		</SELECT><BR>
		</TD>
	</TR>
	<TR>
		<TD>
		<BUTTON ID='btnDelete' CLASS='bdbutton' STYLE='<%= L_DialogButton1_Style %>' DISABLED><%= L_Delete2_Button %></BUTTON>
		<BR><BR><BR>
		</TD>
	</TR>
	<TR>
		<TD>
		<LABEL FOR='newValue'><%= L_NewValue_Text %></LABEL><BR>
		<INPUT TYPE="text" ID="newValue" STYLE="width:212px" MAXLENGTH="3600">&nbsp;<BUTTON ID="btnAdd" STYLE="<%= L_DialogButton1_Style %>" DISABLED><%= L_Add2_Button %></BUTTON>
		<BR><BR>
		</TD>
	</TR>
	<TR>
		<TD ALIGN="RIGHT">
			<BUTTON ID='btnOK' CLASS='bdbutton' STYLE='<%= L_DialogButton2_Style %>'><%= L_OK_Button %></BUTTON><BUTTON ID='btnCancel' CLASS='bdbutton' STYLE='<%= L_DialogButton2_Style %>'><%= L_Cancel_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>

<%

' ------ PUBLIC Declarations
Public g_sOptions

' #######################################################################
'
Sub Perform_ServerSide_Processing ()

	Dim sOption
	Dim aOptions

	g_sOptions = Request.QueryString("options")
	aOptions = Split (g_sOptions, SEPARATOR1)
	g_sOptions = ""

	For Each sOption In aOptions
		If (sOption <> "") Then g_sOptions = g_sOptions & "<option value=" & sOption & ">" & sOption & "</option>"
	Next
End Sub

%>