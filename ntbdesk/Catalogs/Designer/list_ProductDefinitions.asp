<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
' #########################################################################
'
'   list_ProductDefinitions.ASP
'
'	Item Select Page for Product Definitions
'
' #########################################################################

	On Error Resume Next

	' ------------------------
	Const PAGE_TYPE = "LIST"
%>
<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

public g_iSelectedCount

public g_aSelected () ' an array to hold selected items
redim  g_aSelected (-1)

sub window_onLoad ()

	dim sAction
	dim sResponse

	if GetTemp("bCatImporting") then
		bImportToggle = true
		nImportCount = 0
		oImportTimer = window.setInterval("ImportProgress()", BLINK_SPEED * 1000)
		call ImportProgress ()
	end if

' Look for Bad Connection String
<% If (Not g_oCatMgr Is Nothing) Then %>
	EnableTask "view"
	EnableTask "new"
	EnableAllTaskMenuItems "view", True

	sAction = "<%= g_sAction %>"
	' F O R C E   D E L E T E
	if (sAction = "forceDelete") then
		' Inform user about Force Update action
		sResponse = MsgBox (L_ForceDelete_Text, vbYesNo + vbQuestion, L_BizDesk_Text)
		if (sResponse = vbYes) then
			dim elDelete

			g_iSelectedCount = -1
			set elDelete = elGetTaskBtn ("delete")
			EnableTask "delete"
			elDelete.click()
		end if
	end if
<% End If %>
end sub

sub OnSelectRow ()

	dim oSelection

	g_iSelectedCount = g_iSelectedCount + 1
	' enable o(pen) and d(elete) buttons on the task bar
	if (g_iSelectedCount = 1) then
		EnableTask "copy"
		EnableTask "open"
	else
		DisableTask "copy"
		DisableTask "open"
	end if
	EnableTask "delete"

    set oSelection = window.event.XMLrecord
	AddArray g_aSelected, oSelection.selectSingleNode("./name").text
    call setStatusText (sStatusBarText)
end sub

sub OnDeselectRow ()

	dim oSelection

	g_iSelectedCount = g_iSelectedCount - 1
	select case g_iSelectedCount
	case 0
		DisableTask "copy"
		DisableTask "delete"
		DisableTask "open"
	case 1
		EnableTask "copy"
		EnableTask "open"
	end select

    set oSelection = window.event.XMLrecord
	DeleteArray g_aSelected, oSelection.selectSingleNode("./name").text

	if (g_iSelectedCount = 0) then
		call setStatusText ("")
	else
		call setStatusText (sStatusBarText)
	end if
end sub

sub OnSelectAll ()

	dim nItems

	nItems = <%= g_iDefCnt %>
	if (nItems = 1) then
		SelectAllToArray g_aSelected
	else'if(nItems > 1) then
		redim g_aSelected(0)
		g_aSelected(0) = ALL_DEFINITIONS
	end if
	g_iSelectedCount = nItems

	'enable o(pen) and d(elete) buttons on the task bar
	EnableTask "delete"
	if (g_iSelectedCount = 1) then
		EnableTask "copy"
		EnableTask "open"
	else
		DisableTask "copy"
		DisableTask "open"
	end if

	' display the status text
    call setStatusText (sStatusBarText)
end sub

sub OnDeselectAll ()

	'disable o(pen) and d(elete) buttons on the task bar
	DisableTask "open"
	DisableTask "delete"

	g_iSelectedCount = 0
	DeselectAllToArray g_aSelected

	' display status text
    setStatusText ""
end sub

sub OnCopy ()

	with copyform
		.pt_name.value = g_aSelected(0)
		.submit()
	end with
end sub

sub OnOpen ()

	with selectform
		.pt_name.value = g_aSelected(0)
		.submit()
	end with
end sub

sub OnDelete ()

	dim sMsg
	dim iBtnClicked

	if (g_iSelectedCount = -1) then
		' Continue FORCE Delete
		with deleteform
			.type.value = "deleteF"
			.submit()
			exit sub
		end with
	elseif (g_iSelectedCount = 1) then
		sMsg = L_DeleteProductDefinition_Text
	else
		sMsg = sFormatString (L_DeleteDefinitions_HTMLText, Array(g_iSelectedCount))
	end if

	iBtnClicked = MsgBox (sMsg, 36, L_ConfirmDeleteProdDefs_Text)
	if (iBtnClicked = vbYes) then
		with deleteform
			.pt_name.value = sConvertArrayToString(g_aSelected)
			.submit()
		end with
	else
		EnableTask "view"
		EnableTask "new"
		EnableTask "delete"

		if (g_iSelectedCount = 1) then
			EnableTask "copy"
			EnableTask "open"
		end if
	end if
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) And (Not g_oCatMgr Is Nothing) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertTaskBar L_ProductDefinitions_Text, g_sStatusText
%>

<xml id='lsData'>
	<document>
		<%= g_sXML %>
	</document>
</xml>

<xml id='lsMeta'>
	<listsheet>
	    <global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_iDefCnt %>'
	            selection='multi' emptyprompt='no' sort='no' />
	    <columns>
	        <column id='name' sortdir='asc' width='100'><%= L_Name_Text %></column>
		    <subobject type='list'>
			    <column id='property' sortdir='asc' width='100'><%= L_Property_Text %></column>
		    </subobject>
	    </columns>
   	    <operations>
			<newpage formid='newpageform'/>
		</operations>
	</listsheet>
</xml>

<FORM ACTION="" METHOD="POST" ID="PtSelectForm">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">

	<DIV ID="bdcontentarea" CLASS="listSheetContainer">
		<DIV ID='PtListSheet'
			CLASS='listSheet' STYLE="MARGIN:20px; HEIGHT: 80%"
			DataXML='lsData'
			MetaXML='lsMeta'
			LANGUAGE='VBScript'
			OnRowSelect='OnSelectRow'
			OnRowUnselect='OnDeselectRow'
			OnAllRowsSelect='OnSelectAll'
			OnAllRowsUnselect='OnDeselectAll'
			OnNewPage='OnNewPage("PtSelectForm")'><%= L_LoadingWidget_Text %>
		</DIV>
	</DIV>
</FORM>

<FORM ID="newpageform" ACTION='response_ProductDefinitions.asp'></FORM>

<FORM ACTION="edit_ProductDefinition.asp" METHOD="POST" ID="addform">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
</FORM>
<FORM ACTION="" METHOD="POST" ID="copyform" ONTASK="OnCopy">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="pt_name">
</FORM>
<FORM ACTION="" METHOD="POST" ID="selectform" ONTASK="OnOpen">
	<INPUT TYPE="Hidden" NAME="type" VALUE="open">
	<INPUT TYPE="Hidden" NAME="pt_name">
</FORM>
<FORM ACTION="" METHOD="POST" ID="deleteform" ONTASK="OnDelete">
	<INPUT TYPE="Hidden" NAME="type" VALUE="delete">
	<INPUT TYPE="Hidden" NAME="pt_name" VALUE="<%= g_sDefinitions %>">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
</FORM>

</BODY>
</HTML>
<%
	'' -----------------
	ReleaseGlobalObjects
	'' -----------------
%>

<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'----- PUBLIC Declarations
Public g_sXML
Public g_sAction
Public g_sStatusText	' Text to be displayed in the Status Bar
Public g_sDefinitions	' Definitions to be deleted

Public g_nPage
Public g_iDefCnt		' Count of definitions returned

Public g_rsDefinitions	' An ADO RS that will hold the list of definitions

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables

	Perform_Processing_BasedOnAction
End Sub

' ##########################################################
'
Sub InitializeGlobalVariables ()

	g_iDefCnt = 0

	g_nPage = Request.Form("page_id")
	If (True = IsEmpty(g_nPage)) Then g_nPage = 1

	g_sAction = Request.Form("type")
End Sub

' ##########################################################
'
Sub Perform_Processing_BasedOnAction ()

	On Error Resume Next
	Select Case g_sAction
	Case "delete", "deleteF"
		g_sDefinitions = Request.Form("pt_name")
		DeleteProductDefinitions
	End Select

	Set g_rsDefinitions = g_oCatMgr.ProductDefinitions
	If (Err.Number <> 0) Then Exit Sub

	GetMasterDetailXML
End Sub

' ##########################################################
'
Sub GetMasterDetailXML ()

	Dim iLast
	Dim rsProperties

	Dim sPropertyName
	Dim sDefinitionName

	iLast = PAGE_SIZE

	On Error Resume Next
	While (Not g_rsDefinitions.EOF) And (g_iDefCnt < iLast)
		sDefinitionName = g_rsDefinitions.Fields("DefinitionName")
		g_sXML = g_sXML & "<record><name>" & Server.HTMLEncode(sDefinitionName) & "</name>"

		Set rsProperties = g_oCatMgr.GetDefinitionProperties (sDefinitionName)
		If (Err.Number = 0) Then
			While Not rsProperties.EOF
				sPropertyName = rsProperties.Fields("PropertyName")
				g_sXML = g_sXML & "<record><property>" & Server.HTMLEncode(sPropertyName) & "</property></record>"

				rsProperties.MoveNext
			Wend
			ReleaseRecordset rsProperties
		Else
			Call setError ("", L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
		End If

		g_sXML = g_sXML & "</record>"
		g_iDefCnt = g_iDefCnt + 1
		g_rsDefinitions.MoveNext
	Wend

	If (g_iDefCnt = 0) Then
		g_sXML = "<record/>"
	Else
		While (Not g_rsDefinitions.EOF)
			g_iDefCnt = g_iDefCnt + 1
			g_rsDefinitions.MoveNext
		Wend
	End If
End Sub

' #####################################################################
'
Sub DeleteProductDefinitions ()

	Dim rs
	Dim nDeleted
	Dim aDefinitions
	Dim bForceDelete

	Dim sDefinition
	Dim sDefinitionName

	aDefinitions = Split(g_sDefinitions, SEPARATOR1)
	g_sDefinitions = ""

	If (g_sAction = "deleteF") Then
		bForceDelete = True
	Else
		bForceDelete = False
	End If

	If (aDefinitions(0) = ALL_DEFINITIONS) Then
		' Delete All items
		Set g_rsDefinitions = g_oCatMgr.ProductDefinitions
		Do Until g_rsDefinitions.EOF
			sDefinitionName = g_rsDefinitions("DefinitionName")
			DeleteProductDefinition sDefinitionName, sDefinition, nDeleted, bForceDelete
			g_rsDefinitions.MoveNext
		Loop
		ReleaseRecordset g_rsDefinitions
	Else
		' Delete selected items
		For Each sDefinitionName In aDefinitions
			DeleteProductDefinition sDefinitionName, sDefinition, nDeleted, bForceDelete
		Next
	End If

	If (g_sAction = "forceDelete") Then _
		g_sDefinitions = Right(g_sDefinitions, Len(g_sDefinitions) - Len(SEPARATOR1))

	Select Case nDeleted
	Case 0
		g_sStatusText = L_NoDefinitionsDeleted_StatusBar
	Case 1
		g_sStatusText = sFormatString (L_Deleted_StatusBar, Array(sDefinition))
	Case Else
		g_sStatusText = sFormatString (L_DefinitionsDeleted_StatusBar, Array(nDeleted))
	End Select
End Sub

' ##############################################################
'
Sub DeleteProductDefinition (ByRef sDefinitionName, ByRef sDefinition, _
							 ByRef nDeleted, ByVal bForceDelete)

	On Error Resume Next
	g_oCatMgr.DeleteDefinition sDefinitionName, bForceDelete
	If (Err.Number = 0) Then
		nDeleted = nDeleted + 1
		sDefinition = sDefinitionName
	Else
		If (Err.Number = -2003304436) Then
			g_sAction = "forceDelete"
			g_sDefinitions = g_sDefinitions & SEPARATOR1 & sDefinitionName
		End If
		Err.Clear
	End If
End Sub

' ##############################################################
'
Sub ReleaseGlobalObjects ()

	ReleaseRecordset g_rsDefinitions
	Set g_oCatMgr = Nothing
End Sub

%>