<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
' ######################################################################
'
'	edit_PropertyDefinition.asp
'
'	Item View Page for Property Definitions
'
' ######################################################################

	Const PAGE_TYPE = "EDIT"

	On Error Resume Next
%>
<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>
<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

public g_sAction
public g_bIsNested

sub window_onLoad ()

	dim sType
	dim sResponse

	g_bIsNested = <%= g_bIsNested %>
	g_sAction = "<%= g_sAction %>"

	sType = chForm.type.value

	' F O R C E   U P D A T E
	if (g_sAction = "forceUpdate") then
		' Inform user about Force Update action
		sResponse = MsgBox (L_ForceUpdate_Text, vbYesNo + vbQuestion, L_BizDesk_Text)
		if (sResponse = vbYes) then
			' Continue Force Update
			with chForm
				.type.value = sType & "F" ' submitEditF, saveNewF
				.submit()
			end with
		end if
	end if

	' N E S T E D   P A G E
	if	((sType = "saveNew") or _
		(sType = "createNew")) and (g_sAction <> "add") then

		hideStatusAnimation

		sResponse = window.showModalDialog("dlg_AddPropertyDefinition.asp", , _
			"dialogHeight:270px;dialogWidth:350px;status:no;help:no")
		if (sResponse = "") then
			chForm.type.value = "submitEdit"
		else
			with addform
				.ch_type.value = sResponse
				.submit()
			end with
		end if
	end if

	if (True = g_bIsNested) then
		call setTemp ("sNewPropertyDef", "<%= g_sNewDefs %>")
		call setTemp ("sNewPropertyType", "<%= g_sNewTypes %>")
	end if

	esControl.focus()
end sub

sub onChange ()

	setStatusText ""
	setDirty (L_SaveConfirmationDialog_Text)
end sub

sub chForm_onSubmit ()

	dim xMin
	dim xMax
	dim xDef

	dim bValid

	' validate form based on characteristic type
	bValid = True
	select case chForm("ch_Type").value
	case L_Number_Text
		xMin = CLng(esControl.field("ch_number_min").value)
		xMax = CLng(esControl.field("ch_number_max").value)
		xDef = esControl.field("ch_default").value

		bValid =  bAreValidMinMaxDef (xMin, xMax, xDef, DOUBLE_TYPE)

	case L_Float_Text
		xMin = CDbl(esControl.field("ch_float_min").value)
		xMax = CDbl(esControl.field("ch_float_max").value)
		xDef = esControl.field("ch_default").value

		bValid = bAreValidMinMaxDef (xMin, xMax, xDef, STRING_TYPE)

	case L_Currency_Text
		xMin = esControl.field("ch_money_min").value
		xMin = Replace (xMin, "$", " ")
		xMin = CDbl (xMin)

		xMax = esControl.field("ch_money_max").value
		xMax = Replace (xMax, "$", " ")
		xMax = CDbl (xMax)

		xDef = esControl.field("ch_default").value
		xDef = Replace (xDef, "$", " ")

		bValid = bAreValidMinMaxDef (xMin, xMax, xDef, STRING_TYPE)

	case L_String_Text
		xMin = CInt(esControl.field("ch_string_minlength").value)
		xMax = CInt(esControl.field("ch_string_maxlength").value)
		xDef = Len (esControl.field("ch_default").value)

		bValid = False
		if (xMin > xMax) then
			MsgBox L_MinLengthShouldBe_Text, 48, L_Warning_Text
		elseif (xDef > xMax) then
			MsgBox L_LengthOfDefaultValueIsGreaterThanMax_Text, 48, L_Warning_Text
		else
			bValid = True
		end if

	case L_DateTime_Text
		xMin = dtGetDate(esControl.field("ch_date_min").value)
		xMax = dtGetDate(esControl.field("ch_date_max").value)
		xDef = esControl.field("ch_default").value
		if (xDef <> "") then xDef = dtGetDate(xDef)

		bValid = False
		if (xMin > xMax) then
			MsgBox L_EarliestDateShouldBeLessThanLatestDate_Text, 48, L_Warning_Text
		elseif (xDef <> "") then
			if (xDef < xMin)  then
				MsgBox L_DefaultValueShouldBeGreaterThanEarliestDate_Text, 48, L_Warning_Text
			elseif (xDef > xMax) then
				MsgBox L_DefaultValueShouldBeLessThanLatestDate_Text, 48, L_Warning_Text
			else
				bValid = True
			end if
		else
			bValid = True
		end if
	end select

	' submit form if valid
	if (True = bValid) then
		chForm.submit()
	else
		EnableTask "back"
		g_bDirty = False
		esControl.resetDirty()
	end if
end sub

sub chForm_onSaveNew ()

	dim sType

	with chForm.type
		sType = .value
		select case .value
		case "submitNew"
			.value = "createNew"
		case else
			.value = "saveNew"
		end select

		chForm_onSubmit
		'' reaching this statement means that validation of fields failed
		'' in chForm_onSubmit. Reverse .type to previous value
		.value = sType
	end with
end sub

function bAreValidMinMaxDef (xMin, xMax, ByRef sDef, ByVal iType)

	dim xDef

	bAreValidMinMaxDef = true
	if (Len(sDef) > 0) then
		select case iType
		case 3 ' AD_INTEGER
			xDef = CLng(sDef)
		case 5 ' AD_DOUBLE
			xDef = CDbl(sDef)
		end select

		if (xMin <= xMax) then
			if (xMin > xDef) or (xDef > xMax) then
				bAreValidMinMaxDef = false
				MsgBox L_DefaultValueShouldBe_Text, 48, L_Warning_Text
			end if
		else
			bAreValidMinMaxDef = false
			MsgBox L_MinValueShouldBe_Text, 48, L_Warning_Text
		end if
	else
		if (xMin > xMax) then
			MsgBox L_MinValueShouldBe_Text, 48, L_Warning_Text
			bAreValidMinMaxDef = false
		end if
	end if
end function

sub onBrowse ()

	dim sItem
	dim sOptions
	dim sDefault
	dim sResponse

	dim xml
	dim xmlDoc
	dim xmlItem
	dim xmlEnumNode
	dim xmlMenuitem
	dim xmlMenuitems

	dim aTempArray

	set xml = esMeta.XMLdocument
	set xmlDoc = xml.documentElement
	set xmlEnumNode = xmlDoc.selectSingleNode ("//select[@id $ieq$ 'ch_default']")
	set xmlEnumNode = xmlEnumNode.selectSingleNode ("./select[@id $ieq$ 'ch_default']")
	set xmlMenuitems = xmlEnumNode.selectNodes ("./option")

	for each xmlItem in xmlMenuitems
		if (xmlItem.text <> "") then sOptions = sOptions & SEPARATOR1 & xmlItem.text
	next

	sResponse = window.showModalDialog("dlg_EnumPropertyDefinition.asp?options=" & sOptions, , _
									   "dialogHeight:360px;dialogWidth:360px;status:no;help:no")
	if (sResponse <> "") then
		sResponse = Right(sResponse, Len(sResponse) - Len(SEPARATOR1))
		sDefault = esControl.field("ch_default").value
		chForm.properties.value = sResponse

		' Remove the existing OPTION elements
		for each xmlItem in xmlMenuitems
			call xmlEnumNode.removeChild(xmlItem)
		next

		if (sResponse <> "") then
			aTempArray = Split(sResponse, SEPARATOR1)
			' Add the new OPTION elements
			for each sItem in aTempArray
				set xmlMenuitem = xmlEnumNode.appendChild(xml.createElement("option"))
				xmlMenuitem.setAttribute "value", sItem
				xmlMenuitem.text = sItem
			next
		end if

		esControl.field("ch_default").reload("ch_default")
		esControl.field("ch_default").value = sDefault
	end if
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If
	InsertEditTaskBar g_sHeader, g_sStatusText

	If IsNull(g_sDisplayName) Then g_sDisplayName = ""
%>

<xml id='esMeta'>
<editsheets>
<editsheet>
	<global expanded='no' title='no'>
	</global>
	<fields>
	    <text id='name' required='yes' readonly='<% If (g_sAction = "add") Or (g_sAction = "copy") Then %>no<% Else %>yes<% End If %>'
	          maxlen='128' subtype='short'>
	        <name><%= L_Name_Text %></name>
	        <tooltip><%= L_Name_Tooltip %></tooltip>
	        <charmask><%= Server.HTMLEncode("^[^\d/<>&()'][^/<>&()']*$") %></charmask>
	        <error><%= L_NameValidation_ErrorMessage %></error>
	    </text>
	    <boolean id='ch_display_on_site'>
	        <name><%= L_DisplayOnSite_Text %></name>
	        <tooltip><%= L_DisplayOnSite_Tooltip %></tooltip>
	    </boolean>
	    <text id='ch_display_name' maxlen='50' subtype='short'>
	        <name><%= L_DisplayName_Text %></name>
	        <tooltip><%= L_DisplayName_Tooltip %></tooltip>
	        <charmask>^(\s*\S+\s*)*$</charmask>
	    </text>
<% If (g_sPropertyType = L_String_Text) Or _
	  (g_sPropertyType = L_Enumerated_Text) Then %>
	    <boolean id='ch_ft_searchable'>
	        <name><%= L_FreeTextSearchable_Text %></name>
	        <tooltip><%= L_FreeTextSearchable_Tooltip %></tooltip>
	    </boolean>
<% End If %>
<% If (g_sPropertyType <> L_FileName_Text) Then %>
	    <boolean id='ch_spec_searchable'>
	        <name><%= L_SpecSearchable_Text %></name>
	        <tooltip><%= L_SpecSearchable_Tooltip %></tooltip>
	    </boolean>
<% End If %>

<%
Dim sMin, sMax

Select Case g_sPropertyType
Case L_String_Text
%>
	    <numeric id='ch_string_minlength' required='yes'
	          min='0' max='3600' subtype='integer'>
	        <name><%= L_MinLength_Text %></name>
	        <tooltip><%= L_MinLength_Tooltip %></tooltip>
	        <format><%= g_sMSCSNumberFormat %></format>
	        <error><%= L_MinimumLength_ErrorMessage %></error>
	    </numeric>
	    <numeric id='ch_string_maxlength' required='yes'
	           min='0' max='3600' subtype='integer'>
	        <name><%= L_MaxLength_Text %></name>
	        <tooltip><%= L_MaxLength_Tooltip %></tooltip>
			<format><%= g_sMSCSNumberFormat %></format>
	        <error><%= L_MaximumLength_ErrorMessage %></error>
	    </numeric>
	    <text id='ch_default' maxlen='3600' subtype='short'>
	        <name><%= L_Default_Text %></name>
	        <tooltip><%= L_Default_Tooltip %></tooltip>
		</text>
<%
Case L_Number_Text
	sMin = stringValue (MIN_INT, INTEGER_TYPE)
	sMax = stringValue (MAX_INT, INTEGER_TYPE)
%>
	    <numeric id='ch_number_min' required='yes'
	        min='<%= sMin %>' max='<%= sMax %>' subtype='integer'>
	        <name><%= L_MinValue_Text %></name>
	        <tooltip><%= L_MinValue_Tooltip %></tooltip>
	        <format><%= g_sMSCSNumberFormat %></format>
	    </numeric>
	    <numeric id='ch_number_max' required='yes'
	        min='<%= sMin %>' max='<%= sMax %>' subtype='integer'>
	        <name><%= L_MaxValue_Text %></name>
	        <tooltip><%= L_MaxValue_Tooltip %></tooltip>
	        <format><%= g_sMSCSNumberFormat %></format>
	    </numeric>
	    <numeric id='ch_default'
			min='<%= sMin %>' max='<%= sMax %>' subtype='integer'>
	        <name><%= L_Default_Text %></name>
	        <tooltip><%= L_Default_Tooltip %></tooltip>
	        <format><%= g_sMSCSNumberFormat %></format>
		</numeric>
<%
Case L_Float_Text
	sMin = stringValue (MIN_FLOAT, FLOAT_TYPE)
	sMax = stringValue (MAX_FLOAT, FLOAT_TYPE)
%>
	    <numeric id='ch_float_min' required='yes'
	        min='<%= sMin %>' max='<%= sMax %>' subtype='float'>
	        <name><%= L_MinValue_Text %></name>
	        <tooltip><%= L_MinValue_Tooltip %></tooltip>
			<format><%= g_sMSCSNumberFormat %></format>
	    </numeric>
	    <numeric id='ch_float_max' required='yes'
	        min='<%= sMin %>' max='<%= sMax %>' subtype='float'>
	        <name><%= L_MaxValue_Text %></name>
	        <tooltip><%= L_MaxValue_Tooltip %></tooltip>
			<format><%= g_sMSCSNumberFormat %></format>
	    </numeric>
	    <numeric id='ch_default'
	        min='<%= sMin %>' max='<%= sMax %>' subtype='float'>
	        <name><%= L_Default_Text %></name>
	        <tooltip><%= L_Default_Tooltip %></tooltip>
	        <format><%= g_sMSCSNumberFormat %></format>
		</numeric>
<%
Case L_Currency_Text
	sMin = stringValue (MIN_MONEY, CURRENCY_TYPE)
	sMax = stringValue (MAX_MONEY, CURRENCY_TYPE)
%>
	    <numeric id='ch_money_min' required='yes'
	        min='<%= sMin %>' max='<%= sMax %>' subtype='currency'>
	        <name><%= L_MinValue_Text %></name>
	        <tooltip><%= L_MinValue_Tooltip %></tooltip>
			<format><%= g_sMSCSCurrencyFormat %></format>
	    </numeric>
	    <numeric id='ch_money_max' required='yes'
			min='<%= sMin %>' max='<%= sMax %>' subtype='currency'>
	        <name><%= L_MaxValue_Text %></name>
	        <tooltip><%= L_MaxValue_Tooltip %></tooltip>
			<format><%= g_sMSCSCurrencyFormat %></format>
	    </numeric>
	    <numeric id='ch_default' required='no'
	        min='<%= sMin %>' max='<%= sMax %>' subtype='currency'>
	        <name><%= L_Default_Text %></name>
	        <tooltip><%= L_Default_Tooltip %></tooltip>
			<format><%= g_sMSCSCurrencyFormat %></format>
		</numeric>
<%
Case L_DateTime_Text
%>
	    <date id='ch_date_min' required='yes' firstday='<%= g_sMSCSWeekStartDay %>'>
	        <name><%= L_EarliestDate_Text %></name>
	        <tooltip><%= L_EarliestDate_Tooltip %></tooltip>
			<format><%= g_sMSCSDateFormat %></format>
	    </date>
	    <time id='ch_time_min' required='yes'>
			<name><%= L_EarliestTime_Text %></name>
			<tooltip><%= L_EarliestTime_Tooltip %></tooltip>
		</time>
		<date id='ch_date_max' required='yes' firstday='<%= g_sMSCSWeekStartDay %>'>
			<name><%= L_LatestDate_Text %></name>
			<tooltip><%= L_LatestDate_Tooltip %></tooltip>
			<format><%= g_sMSCSDateFormat %></format>
	    </date>
		<time id='ch_time_max' required='yes'>
			<name><%= L_LatestTime_Text %></name>
			<tooltip><%= L_LatestTime_Tooltip %></tooltip>
		</time>
		<date id='ch_default' firstday='<%= g_sMSCSWeekStartDay %>'>
			<name><%= L_DefaultDate_Text %></name>
			<tooltip><%= L_DefaultDate_Tooltip %></tooltip>
			<format><%= g_sMSCSDateFormat %></format>
		</date>
<%
	Case L_Enumerated_Text
%>
	    <select id='ch_default' required='yes' onbrowse='OnBrowse'>
			<name><%= L_Default_Text %></name>
			<tooltip><%= L_Default_Tooltip %></tooltip>
			<select id='ch_default'>
<%= sEnumOptions %>
			</select>
	    </select>
<%
End Select
%>
	    <boolean id='ch_assign_all'>
			<name><%= L_AssignToAll_Text %></name>
			<tooltip><%= L_AssignToAll_Tooltip %></tooltip>
		</boolean>
	    <boolean id='ch_export'>
			<name><%= L_ExportToDW_Text %></name>
			<tooltip><%= L_ExportToDW_Tooltip %></tooltip>
		</boolean>
	    <boolean id='ch_display_in_list'>
			<name><%= L_DisplayInProductsList_Text %></name>
			<tooltip><%= L_DisplayInProductsList_Tooltip %></tooltip>
		</boolean>
	</fields>
</editsheet>
</editsheets>
</xml>

<xml id='esData'>
	<?xml version="1.0" ?>
	<DOCUMENT>
		<RECORD>
			<name><%= Server.HTMLEncode(g_sPropertyName) %></name>
			<ch_display_on_site><%= g_sDisplayOnSite %></ch_display_on_site>
			<ch_display_name><%= Server.HTMLEncode(g_sDisplayName) %></ch_display_name>
			<ch_ft_searchable><%= g_sFTSearchable %></ch_ft_searchable>
			<ch_spec_searchable><%= g_sSpecSearchable %></ch_spec_searchable>
<%= sTypeSpecificData %>
			<ch_assign_all><%= g_sAssignAll %></ch_assign_all>
			<ch_export><%= g_sExport %></ch_export>
			<ch_display_in_list><%= g_sDisplayInList %></ch_display_in_list>
		</RECORD>
	</DOCUMENT>
</xml>


<DIV ID='bdcontentarea' CLASS='editSheetContainer'>
	<FORM ACTION="" METHOD="POST" NAME='chForm' ID="chForm" ONTASK="chForm_onSubmit">
		<INPUT TYPE="Hidden" NAME="type" ID="type" VALUE="<%= g_sActionType %>">
		<INPUT TYPE="Hidden" NAME="ch_type" ID="ch_type" VALUE="<%= g_sPropertyType %>">
		<INPUT TYPE="Hidden" NAME="is_nested" ID="is_nested" VALUE="<%= g_bIsNested %>">
		<INPUT TYPE="Hidden" NAME="new_defs" ID="new_defs" VALUE="<%= g_sNewDefs %>">
		<INPUT TYPE="Hidden" NAME="new_types" ID="new_types" VALUE="<%= g_sNewTypes %>">
		<INPUT TYPE="Hidden" NAME="properties" ID="properties" VALUE="<%= g_sProperties %>">
		<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
		<DIV ID='esControl' 
			CLASS='editSheet'
			ONCHANGE = 'onChange'
			ONREQUIRE ='setRequired(L_Required_Text)'
			ONVALID = 'setValid(L_Valid_Text)'
			MetaXML='esMeta'
			DataXML='esData'><%= L_LoadingPropertyGroups_Text %>
		</DIV>
	</FORM>
</DIV>

<FORM ACTION="" METHOD="POST" NAME='closeForm' ID="closeForm">
	<INPUT TYPE="Hidden" NAME="type" VALUE="close">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
</FORM>
<FORM ACTION="" METHOD="POST" NAME='chForm2' ID="chForm2" ONTASK="chForm_onSaveNew">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="is_nested" ID="is_nested" VALUE="<%= g_bIsNested %>">
</FORM>
<FORM ACTION="" METHOD="POST" NAME='addform' ID="addform">
	<INPUT TYPE="Hidden" NAME="is_nested" ID="is_nested" VALUE="<%= g_bIsNested %>">
	<INPUT TYPE="Hidden" NAME="new_defs" ID="new_defs" VALUE="<%= g_sNewDefs %>">
	<INPUT TYPE="Hidden" NAME="new_types" ID="new_types" VALUE="<%= g_sNewTypes %>">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type">
</FORM>

</BODY>
</HTML>
<%
	ReleaseGlobalObjects
%>


<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

' ----- PUBLIC Declarations
Public g_sExport
Public g_sHeader
Public g_sAction			' String representation of action to take on page
Public g_sNewDefs
Public g_sNewTypes
Public g_sDefault
Public g_sMinDate
Public g_sMinTime
Public g_sMaxDate
Public g_sMaxTime
Public g_sMinValue
Public g_sMaxValue
Public g_sAssignAll
Public g_sProperties
Public g_sStatusText		' Text to be displayed in the Status Bar
Public g_sActionType		' Type to pass through
Public g_sDisplayName
Public g_sPropertyName
Public g_sPropertyType
Public g_sFTSearchable
Public g_sSearchableBy
Public g_sDisplayOnSite
Public g_sDisplayInList
Public g_sSpecSearchable

Public g_nPage				' Page #

Public g_bIsNested
Public g_aNewVariant		' Array containing the new list of enum properties

Public g_rsAttributes		' ADO RS containing property attributes
Public g_rsCurrentVariant	' ADO RS containing the current list of enum values

' ################################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables

	Perform_Processing_BasedOnAction
End Sub

' ################################################################
'
Sub InitializeGlobalVariables ()

	g_sAction = Request.Form("type")
	g_nPage = Request.Form("page_id")

	If (g_sAction = "") Then
		Dim dServerState
		' Nested Page
		Set dServerState = Session (PROPERTY_DEF_PARAMETERS)
			g_bIsNested = True
			g_sAction = dServerState("type")
			g_sPropertyName = dServerState("ch_name")
			g_sPropertyType = dServerState("ch_type")
		Set Session (PROPERTY_DEF_PARAMETERS) = Nothing
		Session (PROPERTY_DEF_PARAMETERS) = Empty

		If IsNull(g_sPropertyType)Then g_sPropertyType = sEnumType(getPropertyType(g_sPropertyName))
	Else
		g_bIsNested = CBool(Request.Form("is_nested"))
		g_sPropertyName = Request.Form("ch_name")
		g_sPropertyType = Request.Form("ch_type")
		g_sNewDefs = Request.Form("new_defs")
		g_sNewTypes = Request.Form("new_types")
	End If

	If (g_sAction <> "open") And (g_sAction <> "copy") Then Exit Sub

	Set g_rsAttributes = g_oCatMgr.GetPropertyAttributes (g_sPropertyName)
	' If OPEN or COPY, figure the type of property to edit/copy
	Select Case g_sPropertyType
	Case L_String_Text
		g_sMinValue = stringValue(g_rsAttributes.Fields("MinLength").Value, INTEGER_TYPE)
		g_sMaxValue = stringValue(g_rsAttributes.Fields("MaxLength").Value, INTEGER_TYPE)
		g_sDefault  = g_rsAttributes.Fields("u_DefaultValue")

	Case L_Number_Text
		g_sMinValue = stringValue(g_rsAttributes.Fields("i_MinValue").Value, INTEGER_TYPE)
		g_sMaxValue = stringValue(g_rsAttributes.Fields("i_MaxValue").Value, INTEGER_TYPE)
		g_sDefault  = stringValue(g_rsAttributes.Fields("i_DefaultValue").Value, INTEGER_TYPE)

	Case L_Float_Text
		g_sMinValue = stringValue(g_rsAttributes.Fields("fp_MinValue").Value, FLOAT_TYPE)
		g_sMaxValue = stringValue(g_rsAttributes.Fields("fp_MaxValue").Value, FLOAT_TYPE)
		g_sDefault  = stringValue(g_rsAttributes.Fields("fp_DefaultValue").Value, FLOAT_TYPE)

	Case L_Currency_Text
		g_sMinValue = stringValue(g_rsAttributes.Fields("cy_MinValue").Value, CURRENCY_TYPE)
		g_sMaxValue = stringValue(g_rsAttributes.Fields("cy_MaxValue").Value, CURRENCY_TYPE)
		g_sDefault  = stringValue(g_rsAttributes.Fields("cy_DefaultValue").Value, CURRENCY_TYPE)

	Case L_DateTime_Text
		g_sMinValue = stringValue(g_rsAttributes.Fields("dt_MinValue").Value, DATETIME_TYPE)
		g_sMinDate = stringValue(g_sMinValue, DATE_TYPE)
		g_sMinTime = stringValue(g_sMinValue, TIME_TYPE)

		g_sMaxValue = stringValue(g_rsAttributes.Fields("dt_MaxValue").Value, DATETIME_TYPE)
		g_sMaxDate = stringValue(g_sMaxValue, DATE_TYPE)
		g_sMaxTime = stringValue(g_sMaxValue, TIME_TYPE)

		g_sDefault = stringValue(g_rsAttributes.Fields("dt_DefaultValue").Value, DATETIME_TYPE)

	Case L_Enumerated_Text
		g_sDefault = g_rsAttributes.Fields("u_DefaultValue")
		Set g_rsCurrentVariant = g_oCatMgr.GetPropertyValues(g_sPropertyName)

	Case L_FileName_Text
	End Select
	' Common Attributes
	g_sFTSearchable = sConvertFromBoolean (g_rsAttributes.Fields("IsFreeTextSearchable").Value)
	g_sSpecSearchable = sConvertFromBoolean (g_rsAttributes.Fields("IncludeInSpecSearch").Value)
	g_sExport = sConvertFromBoolean (g_rsAttributes.Fields("ExportToDW").Value)
	g_sDisplayInList = sConvertFromBoolean (g_rsAttributes.Fields("DisplayInProductsList").Value)
	g_sDisplayOnSite = sConvertFromBoolean(g_rsAttributes.Fields("DisplayOnSite").Value)
	g_sAssignAll = sConvertFromBoolean(g_rsAttributes.Fields("AssignAll").Value)
End Sub

' ################################################################
'
Sub Perform_Processing_BasedOnAction ()

	Select Case g_sAction
	Case "submitNew", "createNew"
		Process_SubmitNEW

	Case "submitEdit", "saveNew", "submitEditF", "saveNewF"
		Process_SubmitEDIT

	Case "add", "copy"
		Process_ADD

	Case "open"
		Process_OPEN

	End Select
End Sub

' ################################################################
'
Sub Process_SubmitNEW ()

	Dim sCreationError

	' Capture user-entered data
	GetInputData

	' ----- Create the New Property Definition
	sCreationError = sCreatePropertyDefinition()
	If (sCreationError <> "") Then
		g_sActionType = g_sAction
		g_sAction = "add"	' To avoid disabling the NAME
		g_sStatusText = sCreationError
		g_sHeader = sFormatString (L_PropertyDefinition_HTMLText, Array(L_NewProperty_Text, g_sPropertyType))
	Else
		On Error Resume Next
		'Check for multiple-choiced property
		If (g_sPropertyType = L_Enumerated_Text) Then
			' Modify property values set
			AddVariants
		End If

		If (g_sAction = "submitNew") Then
			g_sActionType = "submitEdit"
		Else'If(g_sAction= "createNew") Then
			g_sActionType = "createNew"
		End If
		'Assign to all existing product types if so indicated
		If (g_sAssignAll = "1") Then AddPropertyToAllProductDefinitions 

		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sPropertyName))
		g_sHeader = sFormatString (L_PropertyDefinition_HTMLText, Array(g_sPropertyName, g_sPropertyType))

		If	(True = g_bIsNested)Then
			If (g_sNewDefs <> "") Then
				g_sNewDefs = g_sNewDefs & SEPARATOR1
				g_sNewTypes = g_sNewTypes & SEPARATOR1
			End If

			g_sNewDefs = g_sNewDefs & g_sPropertyName
			g_sNewTypes = g_sNewTypes & g_sAssignAll
		End If
	End If
End Sub

' ################################################################
'
Sub Process_SubmitEDIT

	Dim sStatus

	If (g_sAction = "submitEdit") Or (g_sAction = "submitEditF") Then
		g_sActionType = "submitEdit"
	Else
		g_sActionType = "saveNew"
	End If

	'Capture user-entered data
	GetInputData

	On Error Resume Next
	'Check for multiple-choiced property
	If (g_sPropertyType = L_Enumerated_Text) Then
		' Modify property values set
		UpdateVariants
	End If

	If (Err.Number = 0) Then
		'Get Attributes RECORDSET
		Set g_rsAttributes = g_oCatMgr.GetPropertyAttributes(g_sPropertyName)
		'Process type-specific user-entered data
		SetDefinitionAttributes
	End If

	If (Err.Number = 0) Then
		sStatus = L_Saved_StatusBar
	Else
		sStatus = L_ErrorSaving_StatusBar
	End If
	g_sStatusText = sFormatString (sStatus, Array(g_sPropertyName))
	g_sHeader = sFormatString (L_PropertyDefinition_HTMLText, Array(g_sPropertyName, g_sPropertyType))
End Sub

' ################################################################
'
Sub Process_ADD ()

	g_sPropertyName = ""
	g_sActionType = "submitNew"
	g_sHeader = sFormatString (L_PropertyDefinition_HTMLText, Array(L_NewProperty_Text, g_sPropertyType))
	If (g_sAction = "copy") Then Exit Sub

	Select Case g_sPropertyType
	Case L_Number_Text, L_Float_Text, L_DateTime_Text, L_Currency_Text
		Select Case g_sPropertyType
		Case L_Number_Text
			g_sMinValue = stringValue(MIN_INT, INTEGER_TYPE)
			g_sMaxValue = stringValue(MAX_INT, INTEGER_TYPE)
		Case L_Float_Text
			g_sMinValue = stringValue(MIN_FLOAT, FLOAT_TYPE)
			g_sMaxValue = stringValue(MAX_FLOAT, FLOAT_TYPE)
		Case  L_Currency_Text
			g_sMinValue = stringValue(MIN_MONEY, CURRENCY_TYPE)
			g_sMaxValue = stringValue(MAX_MONEY, CURRENCY_TYPE)
		End Select
		g_sFTSearchable = "0"

	Case Else
		Select Case g_sPropertyType
		Case L_String_Text
			g_sMinValue = "0"
			g_sMaxValue = "3600"
		End Select

		g_sFTSearchable = "1" 'Yes
	End Select

	g_sExport = "0" 'No
	g_sDisplayInList = "0" 'No
	g_sSpecSearchable = "1" 'Yes
End Sub

' ################################################################
'
Sub Process_OPEN ()

	g_sActionType = "submitEdit"
	g_sDisplayName = g_rsAttributes.Fields("DisplayName")

	g_sHeader = sFormatString (L_PropertyDefinition_HTMLText, Array(g_sPropertyName, g_sPropertyType))
End Sub

' #####################################################################
'
Sub GetInputData ()

	g_sPropertyName = Request.Form("name")
	g_sDisplayName = Request.Form("ch_display_name")
	g_sDefault = Request.Form ("ch_default")

	g_sDisplayOnSite = sGetOnOff (Request.Form("ch_display_on_site"))
	g_sSpecSearchable = sGetOnOff(Request.Form("ch_spec_searchable"))
	g_sFTSearchable = sGetOnOff(Request.Form("ch_ft_searchable"))
	g_sAssignAll = sGetOnOff(Request.Form("ch_assign_all"))
	g_sExport = sGetOnOff(Request.Form("ch_export"))
	g_sDisplayInList = sGetOnOff(Request.Form("ch_display_in_list"))

	'Capture type-specific user-entered data
	Select Case g_sPropertyType
	Case L_String_Text
		g_sMinValue = Request.Form("ch_string_minlength")
		g_sMaxValue = Request.Form("ch_string_maxlength")

	Case L_Number_Text
		g_sMinValue = Request.Form("ch_number_min")
		g_sMaxValue = Request.Form("ch_number_max")

	Case L_Float_Text
		g_sMinValue = Request.Form("ch_float_min")
		g_sMaxValue = Request.Form("ch_float_max")

	Case L_Currency_Text
		g_sMinValue = Request.Form("ch_money_min")
		g_sMaxValue = Request.Form("ch_money_max")

	Case L_DateTime_Text
		g_sMinDate = Request.Form("ch_date_min")
		g_sMinTime = Request.Form("ch_time_min")
		g_sMinValue = g_sMinDate & " " & g_sMinTime

		g_sMaxDate = Request.Form("ch_date_max")
		g_sMaxTime = Request.Form("ch_time_max")
		g_sMaxValue = g_sMaxDate & " " & g_sMaxTime

	Case L_Enumerated_Text
		g_aNewVariant = Split (Request.Form("properties"), SEPARATOR1)

		If (g_sAction = "submitNew") Then
			g_sMaxValue = "40"
		Else'If (g_sAction = "submitEdit") Then
			Set g_rsCurrentVariant = g_oCatMgr.GetPropertyValues(g_sPropertyName)
		End If

	Case L_FileName_Text
		If (g_sAction = "submitNew") Then
			g_sMaxValue = "256"
		End If
	End Select
End Sub

' #####################################################################
'
Sub SetDefinitionAttributes ()

	Dim bForceUpdate

	If (True = g_rsAttributes.EOF) Then Exit Sub
	On Error Resume Next

	g_rsAttributes.Fields("DisplayName") = g_sDisplayName
	g_rsAttributes.Fields("DisplayOnSite") = bConvertToBoolean(g_sDisplayOnSite)
	g_rsAttributes.Fields("AssignAll") = bConvertToBoolean(g_sAssignAll)
	g_rsAttributes.Fields("IsFreeTextSearchable") = bConvertToBoolean(g_sFTSearchable)
	g_rsAttributes.Fields("IncludeInSpecSearch") = bConvertToBoolean(g_sSpecSearchable)
	g_rsAttributes.Fields("ExportToDW") = bConvertToBoolean(g_sExport)
	g_rsAttributes.Fields("DisplayInProductsList") = bConvertToBoolean(g_sDisplayInList)

	Select Case g_sPropertyType
	Case L_String_Text
		g_rsAttributes.Fields ("MinLength") = variantValue(g_sMinValue, INTEGER_TYPE)
		g_rsAttributes.Fields ("MaxLength") = variantValue(g_sMaxValue, INTEGER_TYPE)
		g_rsAttributes.Fields ("u_DefaultValue") = g_sDefault

	Case L_Number_Text
		g_rsAttributes.Fields ("i_MinValue") = variantValue(g_sMinValue, INTEGER_TYPE)
		g_rsAttributes.Fields ("i_MaxValue") = variantValue(g_sMaxValue, INTEGER_TYPE)
		g_rsAttributes.Fields ("i_DefaultValue") = variantValue(g_sDefault, INTEGER_TYPE)

	Case L_Float_Text
		g_rsAttributes.Fields ("fp_MinValue") = variantValue(g_sMinValue, FLOAT_TYPE)
		g_rsAttributes.Fields ("fp_MaxValue") = variantValue(g_sMaxValue, FLOAT_TYPE)
		g_rsAttributes.Fields ("fp_DefaultValue") = variantValue(g_sDefault, FLOAT_TYPE)

	Case L_Currency_Text
		g_rsAttributes.Fields ("cy_MinValue") = variantValue(g_sMinValue, CURRENCY_TYPE)
		g_rsAttributes.Fields ("cy_MaxValue") = variantValue(g_sMaxValue, CURRENCY_TYPE)
		g_rsAttributes.Fields ("cy_DefaultValue") = variantValue(g_sDefault, CURRENCY_TYPE)

	Case L_DateTime_Text
		g_rsAttributes.Fields ("dt_MinValue") = variantValue(g_sMinValue, DATETIME_TYPE)
		g_rsAttributes.Fields ("dt_MaxValue") = variantValue(g_sMaxValue, DATETIME_TYPE)
		g_rsAttributes.Fields ("dt_DefaultValue") = variantValue(g_sDefault, DATETIME_TYPE)

	Case L_Enumerated_Text
		g_rsAttributes.Fields ("u_DefaultValue") = g_sDefault

	Case L_FileName_Text
	End Select

	If (Err.Number = 0) Then 
		If (g_sAction = "submitEditF") Or (g_sAction = "saveNewF") Then
			bForceUpdate = True
		Else
			bForceUpdate = False
		End If

		g_oCatMgr.SetPropertyAttributes g_rsAttributes, bForceUpdate
		If (Err.Number <> 0) Then CheckForPropertyAlreadyModified
	End If
End Sub

' #################################################################
' Function: sCreatePropertyDefinition
'
' Input:
'	oCat - catalog object to add characteristic for
'
' Returns:
'	string error message if error
'
Function sCreatePropertyDefinition ()

	Dim iPropertyType

	iPropertyType = iEnumType (g_sPropertyType)
	On Error Resume Next
	If (g_sPropertyType = L_String_Text) Then
		Set g_rsAttributes = g_oCatMgr.CreateProperty (g_sPropertyName, iPropertyType, variantValue (g_sMaxValue, INTEGER_TYPE))
	Else
		Set g_rsAttributes = g_oCatMgr.CreateProperty (g_sPropertyName, iPropertyType)
	End If

	If (Err.Number <> 0) Then
		sCreatePropertyDefinition = L_PropertyAlreadyExists_StatusBar
		Call setError (L_CreateDefinition_DialogTitle, sFormatString (L_DefinitionNameAlreadyUsed_ErrorMessage, Array(g_sPropertyName)), sGetScriptError(Err), ERROR_ICON_INFORMATION)
		Err.Clear
	Else
		SetDefinitionAttributes
	End If
End Function

' ##################################################################
'
Sub AddPropertyToAllProductDefinitions ()

	Dim rsDefinitions	' an ADO RS that will hold the list of definitions

	On Error Resume Next

	Set rsDefinitions = g_oCatMgr.ProductDefinitions
	Do Until rsDefinitions.EOF
		g_oCatMgr.AddDefinitionProperty _
			rsDefinitions.Fields("DefinitionName").Value, g_sPropertyName

		rsDefinitions.MoveNext
	Loop

	ReleaseRecordset rsDefinitions
End Sub

' #####################################################################
'
Sub AddVariants ()
	Dim sEnumValue

	For Each sEnumValue In g_aNewVariant
		g_oCatMgr.AddPropertyValue g_sPropertyName, sEnumValue
	Next
End Sub

' #################################################################
'
Sub UpdateVariants ()

	While Not g_rsCurrentVariant.EOF
		g_oCatMgr.RemovePropertyValue g_sPropertyName, g_rsCurrentVariant.Fields("Value").Value
		g_rsCurrentVariant.MoveNext
	Wend

	AddVariants
End Sub

' #################################################################
'
Function sEnumOptions ()

	sEnumOptions = sOptionsFromArray (g_aNewVariant)
	If (sEnumOptions = "") Then
		sEnumOptions = sOptionsFromRecordset (g_rsCurrentVariant)
	End If

	If (sEnumOptions = "") Then
		sEnumOptions = "<option />"
	End If
End Function

' ##################################################################
'
Function sOptionsFromArray (ByRef aItems)

	Dim iLen
	Dim sItem

	sOptionsFromArray = ""
	If (True = IsArray(aItems)) Then
		If (UBound(aItems) >= 0) Then
			For Each sItem In aItems
				sOptionsFromArray = sOptionsFromArray & _
					"<option value='" & sItem & "'>" & sItem & "</option>"

				g_sProperties = g_sProperties & SEPARATOR1 & sItem
			Next

			RemovePrefixFromProperties
		End If
	End If
End Function

' ##################################################################
'
Function sOptionsFromRecordset (ByRef rsItems)

	Dim iLen
	Dim sValue

	sOptionsFromRecordset = ""
	If (False = IsObject(rsItems)) Then Exit Function

	While Not rsItems.EOF
		sValue = rsItems.Fields("Value")
		sOptionsFromRecordset = sOptionsFromRecordset & _
			"<option value='" & sValue & "'>" & sValue & "</option>"

		g_sProperties = g_sProperties & SEPARATOR1 & sValue
		rsItems.MoveNext
	Wend

	RemovePrefixFromProperties
End Function

' ##################################################################
'
Function sTypeSpecificData ()

	Select Case g_sPropertyType
	Case L_String_Text
		sTypeSpecificData =	"<ch_string_minlength>" & g_sMinValue & "</ch_string_minlength>" & _
							"<ch_string_maxlength>" & g_sMaxValue & "</ch_string_maxlength>" & _
							"<ch_default>" & g_sDefault & "</ch_default>"
	Case L_Number_Text
		sTypeSpecificData =	"<ch_number_min>" & g_sMinValue & "</ch_number_min>" & _
							"<ch_number_max>" & g_sMaxValue & "</ch_number_max>" & _
							"<ch_default>" & g_sDefault & "</ch_default>"
	Case L_Float_Text
		sTypeSpecificData = "<ch_float_min>" & g_sMinValue & "</ch_float_min>" & _
							"<ch_float_max>" & g_sMaxValue & "</ch_float_max>" & _
							"<ch_default>" & g_sDefault & "</ch_default>"
	Case L_Currency_Text
		sTypeSpecificData =	"<ch_money_min>" & g_sMinValue & "</ch_money_min>" & _
							"<ch_money_max>" & g_sMaxValue & "</ch_money_max>" & _
							"<ch_default>" & g_sDefault & "</ch_default>"
	Case L_DateTime_Text
		sTypeSpecificData =	"<ch_date_min>" & g_sMinDate & "</ch_date_min>" & _
							"<ch_time_min>" & g_sMinTime & "</ch_time_min>" & _
							"<ch_date_max>" & g_sMaxDate & "</ch_date_max>" & _
							"<ch_time_max>" & g_sMaxTime & "</ch_time_max>" & _
							"<ch_default>" & g_sDefault & "</ch_default>"
	Case L_Enumerated_Text 
		sTypeSpecificData =	"<ch_default>" & g_sDefault & "</ch_default>"

	Case L_FileName_Text
	End Select
End Function

' #################################################################
'
Sub CheckForPropertyAlreadyModified ()

	If (Err.Number = ERR_PROPERTY_ALREADY_MODIFIED) Then
		g_sAction = "forceUpdate"
		Err.Clear
	End If
End Sub

' #################################################################
'
Sub RemovePrefixFromProperties ()

	Dim iLength

	iLength = Len(g_sProperties)
	If (iLength > 0) Then
		g_sProperties = Right(g_sProperties, iLength - Len(SEPARATOR1))
	End If
End Sub

' #################################################################
'
Function stringValue (ByRef varValue, ByVal iType)

	If (True = IsNull(varValue)) Then
		stringValue = ""
		Exit Function
	End If

	Select Case iType
	Case STRING_TYPE, ENUMERATION_TYPE, FILEPATH_TYPE
		stringValue = varValue
	Case INTEGER_TYPE
		stringValue = replace(g_MSCSDataFunctions.Number (varValue), g_sThousands, "")
	Case FLOAT_TYPE
		stringValue = replace(g_MSCSDataFunctions.Float (cStr(varValue), g_DBDefaultLocale, True, True), g_sThousands, "")
	Case CURRENCY_TYPE
		stringValue = replace(replace(g_MSCSDataFunctions.LocalizeCurrency(varValue, g_DBCurrencyLocale, " "), g_sThousands, ""), " ", "")
	Case DATETIME_TYPE
		stringValue = g_MSCSDataFunctions.DateTime (varValue)
	Case DATE_TYPE
		stringValue = g_MSCSDataFunctions.Date (varValue)
	Case TIME_TYPE
		stringValue = g_MSCSDataFunctions.Time (varValue)
	End Select
End Function

' #################################################################
'
Function variantValue (ByRef sString, ByVal iType)

	If ("" = sString) Then 
		variantValue = Null
		Exit Function
	End If

	Select Case iType
	Case INTEGER_TYPE
		variantValue = g_MSCSDataFunctions.ConvertNumberString (sString)
	Case FLOAT_TYPE
		variantValue = g_MSCSDataFunctions.ConvertFloatString (sString)
	Case CURRENCY_TYPE
		variantValue = g_MSCSDataFunctions.ConvertStringToCurrency (sString, g_MSCSCurrencyLocale)
	Case DATETIME_TYPE
		variantValue = g_MSCSDataFunctions.ConvertDateTimeString (sString)
	Case DATE_TYPE
		variantValue = g_MSCSDataFunctions.ConvertDateString (sString)
	Case TIME_TYPE
		variantValue = g_MSCSDataFunctions.ConvertTimeString (sString)
	Case Else
		variantValue = sValue
	End Select
End Function

' #################################################################
'
Sub ReleaseGlobalObjects ()

	Set g_rsAttributes = Nothing
	' Should be ReleaseRecordset g_rsAttributes
	ReleaseRecordset g_rsCurrentVariant

	Set g_oCatMgr = Nothing
End Sub

' ###############################################################
'
'	[in]:	sPropertyName
'
'	[out]:	returns VARIANT
'
Function getPropertyType (ByRef sPropertyName)

	Dim rsAttributes

	Set rsAttributes = g_oCatMgr.GetPropertyAttributes (sPropertyName)
	getPropertyType = rsAttributes.Fields("DataType")

	rsAttributes.Close
	Set rsAttributes = Nothing
End Function

%>