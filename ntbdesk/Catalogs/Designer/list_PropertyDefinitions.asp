<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
'' ----------------------------------------------------------------------
''
''   list_PropertyDefinitions.ASP
''
''	Property Definitions - Item Select Page
''
'' ----------------------------------------------------------------------

	On Error Resume Next

	Const PAGE_TYPE = "LIST"
	' ------------------------
	Public g_aProperties()	  ' an array that will hold the list of characteristics
%>

<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catDesignerSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->

<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
	DIV.esContainer, .esEditgroup DIV.esContainer
	{
	    BACKGROUND-COLOR: transparent;
	}
</STYLE>

<SCRIPT LANGUAGE="VBScript">

option explicit

public g_aSelected ()		' An array to hold selected items
public g_iSelectedCount

sub window_onLoad ()

	redim g_aSelected (-1)

	if GetTemp("bCatImporting") then
		bImportToggle = true
		nImportCount = 0
		oImportTimer = window.setInterval("ImportProgress()", BLINK_SPEED * 1000)
		call ImportProgress ()
	end if

' Look for bad connection string
<% If (Not g_oCatMgr Is Nothing) Then %>
	EnableTask "view"
	EnableTask "new"
	EnableTask "find"

	EnableAllTaskMenuItems "view", True
	EnableAllTaskMenuItems "new", True
<% End If %>
end sub

sub onSelectRow ()

	dim oSelection

	g_iSelectedCount = g_iSelectedCount + 1
	if (g_iSelectedCount = 1) then
		EnableTask "copy"
		EnableTask "open"
	else
		DisableTask "copy"
		DisableTask "open"
	end if
	EnableTask "delete"

    set oSelection = window.event.XMLrecord
	AddArray g_aSelected, oSelection.selectSingleNode("./name").text
	selectform.elements.ch_type.value = oSelection.selectSingleNode("./type").text

    call setStatusText (sStatusBarText)
end sub

sub onDeselectRow ()

	dim oSelection

	g_iSelectedCount = g_iSelectedCount - 1
	select case g_iSelectedCount
	case 0
		DisableTask "copy"
		DisableTask "delete"		
		DisableTask "open"
	case 1
		EnableTask "copy"
		EnableTask "open"
	end select

    set oSelection = window.event.XMLrecord
	DeleteArray g_aSelected,oSelection.selectSingleNode("./name").text

	if (g_iSelectedCount = 0) then
    	call setStatusText ("")
	else
		call setStatusText (sStatusBarText)
	end if
end sub

sub onSelectAll ()

	dim nItems

	nItems = <%= g_nItems %>
	if (nItems = 1) then
		SelectAllToArray g_aSelected
	else'if(nItems > 1) then
		redim g_aSelected(0)
		g_aSelected(0) = ALL_DEFINITIONS
	end if
	g_iSelectedCount = nItems

	' enable o(pen) and d(elete) buttons on the task bar
	EnableTask "delete"
	if (g_iSelectedCount = 1) then
		EnableTask "copy"
		EnableTask "open"
	else
		DisableTask "copy"
		DisableTask "open"
	end if

	' display the status text
    call setStatusText (sStatusBarText)
end sub

sub onDeselectAll ()

	' disable o(pen) and d(elete) buttons on the task bar
	DisableTask "open"
	DisableTask "delete"

	g_iSelectedCount = 0
	DeselectAllToArray g_aSelected

	' display status text
    setStatusText ""
end sub

sub onCopy ()

	with copyform
		.ch_name.value = g_aSelected(0)
		.ch_type.value = selectform.ch_type.value
		.submit()
	end with
end sub

function onDelete ()

	dim sMsg
	dim iBtnClicked

	if (g_iSelectedCount = 1) then
		sMsg = L_DeletePropertyDefinition_Text
	else
		sMsg = sFormatString (L_DeleteDefinitions_HTMLText, Array(g_iSelectedCount))
	end if

	iBtnClicked = MsgBox (sMsg, 36, L_ConfirmDeletePropDefs_Text)
	if (iBtnClicked = vbYes) then
		with deleteform
			.ch_name.value = sConvertArrayToString(g_aSelected)
			.submit()
		end with
	else
		EnableTask "view"
		EnableTask "new"
		EnableTask "delete"
		EnableTask "find"

		if (g_iSelectedCount = 1) then
			EnableTask "copy"
			EnableTask "open"
		end if
	end if
end function

sub onOpen ()

	with selectform
		.ch_name.value = g_aSelected(0)
		.submit()
	end with
end sub

sub findby_onChange ()

	dim sDisplay
	dim bDisabled

	with document.all
		.txtSearchName.style.display = "none"
		.txtSearchType.style.display = "none"
		.txtSearchAssignAll.style.display = "none"

		sDisplay = "block"
		bDisabled = False
		select case window.event.srcElement.value
		case "Name"
			.txtSearchName.style.display = "block"
		case "DataType"
			.txtSearchType.style.display = "block"
		case "AssignAll"
			.txtSearchAssignAll.style.display = "block"
		case "All"
			sDisplay = "none"
			bDisabled = True
			esFind.all.searchon.checked = False
		end select

		.criterialabel.style.display = sDisplay
		esFind.all.searchon.disabled = bDisabled
	end with
end sub

sub onNewPage ()

	dim nPage
	
	nPage = window.event.page
	UpdatePageNumber nPage
	findForm.type.value = "newPage"
end sub

sub OnCheck ()

	with findForm.searchon
		if (True = .checked) then
			.value = "on"
		else
			.value = "off"
		end if
	end with
end sub

sub onFind ()

	UpdatePageNumber 1
	findForm.type.value = "newSearch"

	with chListSheet
		.page = 1
		call .reload ("find")
	end with
end sub

sub UpdatePageNumber (ByVal nPage)

	findForm.page_id.value = nPage
	copyform.page_id.value = nPage
	selectform.page_id.value = nPage
	deleteform.page_id.value = nPage

	add_string_form.page_id.value = nPage
	add_number_form.page_id.value = nPage
	add_float_form.page_id.value = nPage
	add_money_form.page_id.value = nPage
	add_date_form.page_id.value = nPage
	add_enum_form.page_id.value = nPage
	add_filename_form.page_id.value = nPage
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) And (Not g_oCatMgr Is Nothing) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertTaskBar L_PropertyDefinitions_Text, g_sStatusText
%>

<xml id='lsData'>
<document>
	<%= sMasterXML() %>
</document>
</xml>

<xml id='lsMeta'>
	<listsheet>
	    <global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_nItems %>'
	        selection='multi' sort='no' />
	    <columns>
	        <column id='name' sortdir='asc' width='50'><%= L_Name_Text %></column>
	        <column id='type' width='50'><%= L_Type_Text %></column>
	    </columns>
   	    <operations>
			<newpage formid='findForm'/>
			<find formid='findForm'/>
		</operations>
	</listsheet>
</xml>

<%
	Dim sName
	Dim sType
	Dim sFindBy
	Dim sAssigned

	Dim nFilters

	Call GetFiltersInfo (nFilters, sName, sType, sFindBy, sAssigned)
%>
<xml id='fbData'>
	<document>
		<record>
			<selfindby><%= sFindBy %></selfindby>
			<txtSearchName><%= sName %></txtSearchName>
			<txtSearchType><%= sType %></txtSearchType>
			<txtSearchAssignAll><%= sAssigned %></txtSearchAssignAll>
		</record>
	</document>
</xml>

<xml id="fbmeta">
<editsheet>
    <global title='no'/>
	<fields>
		<select id='selfindby' onchange='findby_onChange'>
			<select ID='selfindby'>
				<option value='Name'><%= L_Name_Text %></option>
				<option value='DataType'><%= L_Type_Text %></option>
				<option value='AssignAll'><%= L_AssignedToAll_Text %></option>
				<option value='All'><%= L_AllDefinitions_Text %></option>
			</select>
		</select>
		<text id='txtSearchName' subtype='short' maxlen='70'/>
		<select id='txtSearchType'>
			<select id='txtSearchType'>
				<option value='5'><%= L_String_Text %></option>
				<option value='0'><%= L_Number_Text %></option>
				<option value='2'><%= L_Float_Text %></option>
				<option value='7'><%= L_Currency_Text %></option>
				<option value='6'><%= L_DateTime_Text %></option>
				<option value='9'><%= L_Enumerated_Text %></option>
				<option value='8'><%= L_FileName_Text %></option>
			</select>
		</select>
		<select id='txtSearchAssignAll'>
			<select id='txtSearchAssignAll'>
				<option value='<%= cStr(True) %>'><%= L_Yes_Text %></option>
				<option value='<%= cStr(False) %>'><%= L_No_Text %></option>
			</select>
		</select>
	</fields>
    <template fields='selfindby txtSearchName txtSearchType txtSearchAssignAll'><![CDATA[
		<TABLE>
			<TR>
				<TD><LABEL FOR='selfindby'><%= L_LookFor_Text %></LABEL></TD>
				<TD WIDTH='300px'>
					<DIV ID='selfindby'><%= L_LoadingWidget_Text %></DIV>
				</TD>
				<TD><BUTTON ID="btnfindby" CLASS="bdbutton" STYLE="width:<%= L_FindByButton_Style %>" LANGUAGE="VBScript" ONCLICK="onFind()"><%= L_FindNow_Text %></BUTTON></TD>
			</TR>
			<TR>
				<TD><SPAN ID='criterialabel'><%= L_Criteria_Text %></SPAN></TD>
				<TD>
					<DIV ID='txtSearchName' STYLE='width:300px'
						 <% If (sFindBy <> "Name") Then %> STYLE='display:none'<% End If %>><%= L_LoadingWidget_Text %></DIV>
					<DIV ID='txtSearchType' STYLE='width:300px'
						 <% If (sFindBy <> "DataType") Then %> STYLE='display:none'<% End If %>><%= L_LoadingWidget_Text %></DIV>
					<DIV ID='txtSearchAssignAll' STYLE='width:300px'
						 <% If (sFindBy <> "AssignAll") Then %> STYLE='display:none'<% End If %>><%= L_LoadingWidget_Text %></DIV>
				</TD>
				<TD>&nbsp;</TD>
			</TR>
			<TR>
				<TD></TD>
				<TD STYLE="padding:0px">
					<INPUT TYPE='checkbox' ID='searchon' NAME='searchon'
						LANGUAGE="VBScript" ONCLICK='OnCheck()'
						<% If (nFilters > 1) Then %> CHECKED VALUE="on"<% Else %> VALUE="off" <% End If %>>
					<LABEL FOR='searchon'><%= L_SearchOnlyTheListBelow_Text %></LABEL>
				</TD>
				<TD></TD>
			</TR>
		</TABLE>
    ]]></template>
</editsheet>
</xml>

<FORM ACTION="response_PropertyDefinitions.asp" ID="findForm">
	<INPUT TYPE="Hidden" NAME="type">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<DIV ID="bdfindbycontent" CLASS="findByContent">
		<DIV ID="esFind" class="editSheet"
			METAXML='fbMeta' DATAXML='fbData'></DIV>
	</DIV>
</FORM>

<DIV ID="bdcontentarea" CLASS="listSheetContainer">
	<DIV ID="filtertext" STYLE="MARGIN-TOP:20px; MARGIN-LEFT:40px;"><%= L_BDSSFilter_Text %></DIV>
	<BR>
	<DIV ID='chListSheet'
		CLASS='listSheet' STYLE="MARGIN:20px; HEIGHT: 80%"
		DataXML='lsData'
		MetaXML='lsMeta'
		LANGUAGE='VBScript'
		OnRowSelect='onSelectRow'
		OnRowUnselect='onDeselectRow'
		OnAllRowsSelect='onSelectAll'
		OnAllRowsUnselect='onDeselectAll'
		OnNewPage='onNewPage'><%= L_LoadingWidget_Text %>
	</DIV>
</DIV>

<FORM ACTION="" METHOD="POST" ID="add_string_form">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type" VALUE="<%= L_String_Text %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="add_number_form">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type" VALUE="<%= L_Number_Text %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="add_float_form">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type" VALUE="<%= L_Float_Text %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="add_money_form">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type" VALUE="<%= L_Currency_Text %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="add_date_form">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type" VALUE="<%= L_DateTime_Text %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="add_enum_form">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type" VALUE="<%= L_Enumerated_Text %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="add_filename_form">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="ch_type" VALUE="<%= L_FileName_Text %>">
</FORM>

<FORM ACTION="" METHOD="POST" ID="copyform" ONTASK="OnCopy">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
	<INPUT TYPE="Hidden" NAME="type" VALUE="copy">
	<INPUT TYPE="Hidden" NAME="ch_name">
	<INPUT TYPE="Hidden" NAME="ch_type">
</FORM>
<FORM ACTION="" METHOD="POST" ID="selectform" ONTASK="OnOpen">
	<INPUT TYPE="Hidden" NAME="type" VALUE="open">
	<INPUT TYPE="Hidden" NAME="ch_name">
	<INPUT TYPE="Hidden" NAME="ch_type">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
</FORM>
<FORM ACTION="" METHOD="POST" ID="deleteform" ONTASK="OnDelete">
	<INPUT TYPE="Hidden" NAME="type" VALUE="delete">
	<INPUT TYPE="Hidden" NAME="ch_name">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nPage %>">
</FORM>

</BODY>
</HTML>
<%
	'' -----------------
	ReleaseGlobalObjects
	'' -----------------
%>


<%
'	#########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
'	#########################################################

'----- PUBLIC Declarations
Public g_sAction
Public g_sStatusText  ' To be displayed in the Status Bar
		              ' at the top of the window (default is Null)
Public g_nPage		  ' page #
Public g_nItems		  ' total number of items

Public g_rsProperties ' PropertyDefinitions used by the current Catalog

' ###############################################################
'
Sub Perform_ServerSide_Processing ()

	g_nItems = 0
	InitializeCatalogObject
	If (g_oCatMgr Is Nothing) Then Exit Sub

	InitializeGlobalVariables
	Perform_Processing_BasedOnAction
End Sub

' ###############################################################
'
Sub InitializeGlobalVariables ()

	ReDim g_aProperties (-1)
	g_sAction = Request.Form("type")
	g_nPage = Request.Form("page_id")

	If (True = IsEmpty(g_nPage)) Then g_nPage = 1
End Sub

' ###############################################################
'
Sub Perform_Processing_BasedOnAction ()

	On Error Resume Next

	Select Case g_sAction
	Case "delete"
		DeletePropertyDefinitions Request.Form("ch_name")
	End Select

	Set g_rsProperties = g_oCatMgr.Properties
	If (Err.Number <> 0) Then Exit Sub

	RetrieveCharArray g_aProperties
	RunFilters g_aProperties

	g_nItems = UBound(g_aProperties) + 1
End Sub

' ###############################################################
' Sub: RunFilters
'	Runs searches
'
' Input:
'	aFields - array to search
'
Sub RunFilters (ByRef aFields)

	Dim iCnt
	Dim aFilters
	Dim sTempFilter

	sTempFilter = Session("FindPropertyDefinition")
	aFilters = Split(sTempFilter, SEPARATOR1)

	For iCnt = 0 To UBound(aFilters) Step 2
		'Check the filter type
		Select Case aFilters(iCnt)
		Case "Name"
			' Name : Compare name of each item in char array to name from filter
			FilterByName aFields, aFilters(iCnt + 1)
		Case "DataType"
			' Type : Check types of characteristics
			FilterByCharAttribute aFields, aFilters(iCnt + 1), aFilters(iCnt)
		Case "AssignAll"
			' AssignAll : Check to see if assigned all or not assigned all
			FilterByCharAttribute aFields, aFilters(iCnt + 1), aFilters(iCnt)
		End Select
	Next
End Sub

' ###############################################################
' Sub: FilterByName
'	compares array values to a name and resizes array
'
' Input:
'	aNames - array of all property definitions
'	sName  - property name to look for
'
Sub FilterByName (ByRef aNames, ByRef sName)

	Dim iCnt
	Dim sItem
	Dim aTempNames ()

	ReDim aTempNames(-1)
	For Each sItem In aNames
		If (InStr(sItem, sName) > 0) Then
			AddArray aTempNames, sItem
		End If
	Next

	ReDim aNames(UBound(aTempNames))
	For iCnt = 0 To UBound(aTempNames)
		aNames(iCnt) = aTempNames(iCnt)
	Next
End Sub

' ##############################################################
' Sub: FilterByCharAttribute
'	checks an array of characteristics versus a char type and resizes
'	array
'
' Input:
'	aChars:	array of all property definitions
'	sValue: value to look for
'	sAttribute: attribute to search
'
Sub FilterByCharAttribute (ByRef aChars, ByRef sValue, ByRef sAttribute)

	Dim iCnt
	Dim sItem
	Dim aTempChars ()

	ReDim aTempChars(-1)
	For Each sItem In aChars
		If (CStr(getPropertyAttribute(sItem, sAttribute)) = sValue) Then
			AddArray aTempChars, sItem
		End If
	Next

	ReDim aChars(UBound(aTempChars))
	For iCnt = 0 To UBound(aTempChars)
		aChars(iCnt) = aTempChars(iCnt)
	Next
End Sub

' ##############################################################
'
Function sMasterXML ()

	If (g_nItems > 0) Then
		Dim sType
		Dim iIndex
		Dim sProperty

		iIndex = PAGE_SIZE * (g_nPage - 1)

		While (iIndex < g_nItems) And (iIndex < PAGE_SIZE * g_nPage)
			sProperty = g_aProperties(iIndex)
			sType = sEnumType(getPropertyAttribute (sProperty, "DataType"))
			sMasterXML = sMasterXML & "<record><name>" & Server.HTMLEncode(sProperty) & "</name><type>" & sType & "</type></record>"

			iIndex = iIndex + 1
		Wend
	Else
		sMasterXML = "<record/>"
	End If
End Function

' ###############################################################
' Sub: DeletePropertyDefinitions
'	Deletes property definitions
'	from the catalog manager object
'
' Input:
'	sDeleteString - string of characteristic names to delete
'
Sub DeletePropertyDefinitions (ByRef sDeleteString)

	Dim iCnt
	Dim iDeleted

	Dim aDelete
	Dim sDefinitionName

	On Error Resume Next

	iDeleted = 0
	aDelete = Split(sDeleteString, SEPARATOR1)

	If (aDelete(0) = ALL_DEFINITIONS) Then
		ReDim aDelete(-1)
		' Retrieve list of available properties
		Set g_rsProperties = g_oCatMgr.Properties
		RetrieveCharArray aDelete
	End If

	On Error Resume Next
	For iCnt = 0 To UBound(aDelete)
		sDefinitionName = aDelete(iCnt)
		g_oCatMgr.DeleteProperty(sDefinitionName)

		If (Err.Number = 0) Then
			iDeleted = iDeleted + 1
		Else
			Call setError (L_DeleteDefinition_DialogTitle, sFormatString (L_PropertyDefinitionInUse_ErrorMessage, Array(sDefinitionName)), sGetScriptError(Err), ERROR_ICON_INFORMATION)
			Err.Clear
		End If
	Next

	If (iDeleted = 0) Then
		g_sStatusText = L_NoDefinitionsDeleted_StatusBar
	ElseIf (iDeleted = 1) Then
		g_sStatusText = sFormatString (L_Deleted_StatusBar, Array(aDelete(0)))
	ElseIf (iDeleted > 1) Then
		g_sStatusText = sFormatString (L_DefinitionsDeleted_StatusBar, Array(iDeleted))
	End If
End Sub

Sub GetFiltersInfo ( _
		ByRef nFilters, _
		ByRef sName, _
		ByRef sType, _
		ByRef sFindBy, _
		ByRef sAssigned _
				   )
	Dim iCnt
	Dim sValue
	Dim aFilters

	aFilters = Split (Session("FindPropertyDefinition"), SEPARATOR1)
	nFilters = UBound (aFilters)

	If (nFilters = -1) Then
		sFindBy = "Name"
		sName = ""
	Else
		sFindBy = aFilters(nFilters - 1)
	End If

	For iCnt = 0 To nFilters Step 2

		sValue = aFilters(iCnt + 1)
		Select Case aFilters(iCnt)
		Case "Name"
			sName = sValue
		Case "DataType"
			sType = sValue
		Case "AssignAll"
			sAssigned = sValue
		End Select
	Next
End Sub

' ##############################################################
'
Sub ReleaseGlobalObjects ()

	Set g_oCatMgr = Nothing
End Sub

%>