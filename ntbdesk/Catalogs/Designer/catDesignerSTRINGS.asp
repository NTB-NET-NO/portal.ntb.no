<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

' P-r-o-p-e-r-t-y   T-y-p-e-s  (T-e-x-t)
Const L_String_Text	= "Text"
Const L_Number_Text = "Number"
Const L_Float_Text = "Decimal"
Const L_FileName_Text = "File Name"
Const L_Currency_Text = "Money/Currency"
Const L_Enumerated_Text = "Multiple Choice"
Const L_DateTime_Text = "Date, Time, Date-Time"

' E-r-r-o-r  M-e-s-s-a-g-e
Const L_ErrorsInPage_ErrorMessage = "<BR>An error occurred while loading."
Const L_BadConnectionString_ErrorMessage = "The catalog system could not be initialized.<BR>Please contact your system administrator."

%>

<% If (PAGE_TYPE <> "RESPONSE") Then %>

<%
' T-e-x-t
Const L_No_Text = "No"
Const L_Yes_Text = "Yes"
Const L_NewProduct_Text = "New"
Const L_NewCategory_Text = "New"
Const L_NewProperty_Text = "New"
Const L_Type_Text = "Type"
Const L_Values_Text = "Values:"
Const L_FindNow_Text = "Find Now"
Const L_LookFor_Text = "Look for:"
Const L_Property_Text = "Property"
Const L_Criteria_Text = "Criteria:"
Const L_ListPrice_Text = "List Price"
Const L_NewValue_Text = "New value:"
Const L_AllDefinitions_Text = "All Definitions"
Const L_AssignedProperties_Text = "Assigned properties:"
Const L_AssignedProperties_Tooltip = "Assigned properties"
Const L_AvailableProperties_Text = "Available properties:"
Const L_AvailableProperties_Tooltip = "Available properties"
Const L_AssignedToAll_Text = "Assigned to All Product Types"
Const L_LoadingWidget_Text = "Loading widget, please wait..."
Const L_SearchOnlyTheListBelow_Text = "Search only the list below."
Const L_ProductDefinitions_Text = "Catalog Designer: Product Definitions"
Const L_CategoryDefinitions_Text = "Catalog Designer: Category Definitions"
Const L_PropertyDefinitions_Text = "Catalog Designer: Property Definitions"
Const L_LoadingPropertyGroups_Text = "Loading property groups, please wait..."

' H-T-M-L  T-e-x-t
Const L_ProductDefinition_HTMLText = "Product Definition: %1"
Const L_CategoryDefinition_HTMLText = "Category Definition: %1"
Const L_PropertyDefinition_HTMLText = "Property Definition: %1 (%2)"

' D-i-a-l-o-g  T-i-t-l-e
Const L_Critical_DialogTitle = "Critical"
Const L_DeleteDefinition_DialogTitle = "Delete Definition"
Const L_CreateDefinition_DialogTitle = "Create Definition"
Const L_EnumeratedValues_DialogTitle = "Multiple Choice Values"
Const L_NewPropertyDefinition_DialogTitle = "New Property Definition"

' S-t-a-t-u-s  B-a-r
Const L_Saved_StatusBar = "%1 saved"
Const L_Deleted_StatusBar = "%1 deleted"
Const L_ErrorSaving_StatusBar = "Error saving %1"
Const L_DefinitionsDeleted_StatusBar = "%1 definitions deleted"
Const L_NoDefinitionsDeleted_StatusBar = "No definitions deleted"
Const L_PropertyAlreadyExists_StatusBar = "Error: Property name already exists"
Const L_CatalogImportInProgress_StatusBar = "Catalog import in progress..."
Const L_CatalogImportComplete_StatusBar = "Catalog import complete."
Const L_CatalogImportErrors_StatusBar = "Errors importing catalog."

' B-u-t-t-o-n
Const L_OK_Button = "OK"
Const L_New_Button = "New..."
Const L_Copy_Button = "Copy..."
Const L_Edit_Button = "Edit..."
Const L_Cancel_Button = "Cancel"
Const L_MoveUp_Button = "Move Up"
Const L_Add2_Button = "Add"
Const L_Add_Button = "Add&nbsp;&gt;"
Const L_Continue_Button = "Continue"
Const L_MoveDown_Button = "Move Down"
Const L_Delete2_Button = "Delete"
Const L_Remove_Button = "&lt;&nbsp;Remove"

' T-e-x-t  &  A-c-c-e-l-e-r-a-t-o-r
Const L_CategoryDefProperties_Text = "&lt;U&gt;C&lt;/U&gt;ategory Definition Properties"
Const L_CategoryDefProperties_Accelerator = "c"
Const L_CategoryProperties_Text = "Category &lt;U&gt;P&lt;/U&gt;roperties"
Const L_CategoryProperties_Accelerator = "p"
Const L_ProductDefProperties_Text = "&lt;U&gt;P&lt;/U&gt;roduct Definition Properties"
Const L_ProductDefProperties_Accelerator = "p"
Const L_ProductProperties_Text = "P&lt;U&gt;r&lt;/U&gt;oduct Properties"
Const L_ProductProperties_Accelerator = "r"
Const L_ProductVariantProperties_Text = "Product &lt;U&gt;V&lt;/U&gt;ariant Properties"
Const L_ProductVariantProperties_Accelerator = "v"

' T-e-x-t  &  T-o-o-l-t-i-p
Const L_Name_Text = "Name"
Const L_Name_Tooltip = "Name"
Const L_DisplayOnSite_Text = "Display on site:"
Const L_DisplayOnSite_Tooltip = "Display on site"
Const L_DisplayName_Text = "Display name:"
Const L_DisplayName_Tooltip = "Display name"
Const L_FreeTextSearchable_Text = "Free text searchable:"
Const L_FreeTextSearchable_Tooltip = "Free text searchable"
Const L_SpecSearchable_Text = "Specification searchable:"
Const L_SpecSearchable_Tooltip = "Specification searchable"
Const L_MinLength_Text = "Minimum length:"
Const L_MinLength_Tooltip = "Minimum length"
Const L_MaxLength_Text = "Maximum length:"
Const L_MaxLength_Tooltip = "Maximum length"
Const L_Default_Text = "Default value:"
Const L_Default_Tooltip = "Default value"
Const L_DefaultDate_Text = "Default date:"
Const L_DefaultDate_Tooltip = "Default date"
Const L_MinValue_Text = "Minimum value:"
Const L_MinValue_Tooltip = "Minimum value"
Const L_MaxValue_Text = "Maximum value:"
Const L_MaxValue_Tooltip = "Maximum value"
Const L_EarliestDate_Text = "Earliest date:"
Const L_EarliestDate_Tooltip = "Earliest date"
Const L_EarliestTime_Text = "Earliest time:"
Const L_EarliestTime_Tooltip = "Earliest time"
Const L_LatestDate_Text = "Latest date:"
Const L_LatestDate_Tooltip = "Latest date"
Const L_LatestTime_Text = "Latest time:"
Const L_LatestTime_Tooltip = "Latest time"
Const L_AssignToAll_Text = "Assign to all product types:"
Const L_AssignToAll_Tooltip = "Assign to all product types"
Const L_ExportToDW_Text = "Export to Data Warehouse:"
Const L_ExportToDW_Tooltip = "Export to data warehouse"
Const L_DisplayInProductsList_Text = "Display in products list:"
Const L_DisplayInProductsList_Tooltip = "Display in products list"

' E-r-r-o-r  M-e-s-s-a-g-e
Const L_EmptyDefinitionName_ErrorMessage = "Error: Empty Definition Name"
Const L_ErrorCreatingDefinition_ErrorMessage = "<BR>Error creating definition <B>%1</B>."
Const L_ErrorDeletingDefinition_ErrorMessage = "<BR>Error deleting definition <B>%1</B>."
Const L_DefinitionNameAlreadyUsed_ErrorMessage = "The name <B>%1</B> is already used by another definition."
Const L_PropertyDefinitionInUse_ErrorMessage = "<B>%1</B> definition is currently used by another product or category definition. It cannot be deleted."
Const L_NameValidation_ErrorMessage = "The text contains characters that are not valid. It cannot contain '&lt;', '&gt;', '&amp;' '/', '(', ')' or an apostrophe and the first character cannot be a digit."
Const L_MaximumLength_ErrorMessage = "Maximum length should be an integer value in between 0 and 3600."
Const L_MinimumLength_ErrorMessage = "Minimum length should be an integer value in between 0 and 3600."

' S-t-y-l-e
Const L_FindByButton_Style = "75px"
Const L_DialogButton1_Style = "width:6em"
Const L_DialogButton2_Style = "width:6em"

%>

<SCRIPT LANGUAGE="VBScript">

' P-r-o-p-e-r-t-y  T-y-p-e-s (T-e-x-t)
Const L_String_Text	= "Text"
Const L_Number_Text = "Number"
Const L_Float_Text = "Decimal"
Const L_FileName_Text = "File Name"
Const L_Currency_Text = "Money/Currency"
Const L_Enumerated_Text = "Multiple Choice"
Const L_DateTime_Text = "Date, Time, Date-Time"

' T-e-x-t
Const L_Warning_Text = "Warning"
Const L_BizDesk_Text = "BizDesk"
Const L_SaveConfirmationDialog_Text = "Save definition before exiting?"
Const L_ConfirmDeleteProdDefs_Text = "Confirm Product Definitions Delete"
Const L_ConfirmDeletePropDefs_Text = "Confirm Property Definitions Delete"
Const L_ConfirmDeleteCategDefs_Text = "Confirm Category Definitions Delete"
Const L_MinValueShouldBe_Text = "Minimum Value should be less than the Maximum Value."
Const L_MinLengthShouldBe_Text = "Minimum Length should be less than the Maximum Length."
Const L_DeleteProductDefinition_Text = "Are you sure you want to delete this product definition?"
Const L_DeletePropertyDefinition_Text = "Are you sure you want to delete this property definition?"
Const L_DeleteCategoryDefinition_Text = "Are you sure you want to delete this category definition?"
Const L_Valid_Text = "Cannot save until all required fields have values and invalid fields are corrected."
Const L_Required_Text = "Cannot save until all required fields have values and invalid fields are corrected."
Const L_LengthOfDefaultValueIsGreaterThanMax_Text = "Length of Default Value is greater than Maximum Length."
Const L_ForceDelete_Text = "Products based on these definitions still exist. Do you want to continue the delete?"
Const L_DefaultValueShouldBe_Text = "Default Value should be less than the Maximum and greater than the Minimum value."
Const L_ForceUpdate_Text = "The record you are saving has already been modified by another user. Do you want to continue the save?"
Const L_EarliestDateShouldBeLessThanLatestDate_Text = "Earliest Date should be less or equal than Latest Date."
Const L_DefaultValueShouldBeGreaterThanEarliestDate_Text = "Default Value should be greater or equal than Earliest Date."
Const L_DefaultValueShouldBeLessThanLatestDate_Text = "Default Value should be less or equal than Latest Date."
Const L_ValueMustBeUnique_Text = "The value must be unique."

' H-T-M-L  T-e-x-t
Const L_DeleteDefinitions_HTMLText = "Are you sure you want to delete these %1 definitions?"

' S-t-a-t-u-s  B-a-r
Const L_DefinitionSelected_StatusBar = "1 item selected [%1]"
Const L_DefinitionsSelected_StatusBar = "%1 items selected"

</SCRIPT>

<% End If %>