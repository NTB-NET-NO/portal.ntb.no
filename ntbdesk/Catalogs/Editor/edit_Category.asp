<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
'' --------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright (c) 1996-2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			edit_Category.asp
''
''	Description:	Category Editor (Open page).
''
'' --------------------------------------------------------------------

	Const PAGE_TYPE = "EDIT"
	' --------------------------
	Public g_aCgProperties()	' To hold fields associated with category
	Public g_aParentCategories()' Parent categories

	On Error Resume Next
%>

<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->

<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

' Scripts for manipulating product categories
public g_nSelections
public g_aParentCategories()		' added parent categories
public g_aAssignedCategories()		' currently assigned categories

sub window_OnLoad ()

	dim sAction
	dim sParentCategories
	dim sAssignedCategories

	redim g_aParentCategories(-1)
	redim g_aAssignedCategories(-1)

	sAction = "<%= g_sAction %>"
	sAssignedCategories = "<%= g_sAssignedCategories %>"
	sParentCategories = "<%= Join (g_aParentCategories, SEPARATOR1) %>"

	SplitToDynamicArray sParentCategories, SEPARATOR1, g_aParentCategories
	SplitToDynamicArray sAssignedCategories, SEPARATOR1, g_aAssignedCategories

	esControl.focus

	' F O R C E   U P D A T E
	CheckForceUpdate sAction, nhForm
end sub

</SCRIPT>

<SCRIPT LANGUAGE="VBScript" DEFER>

public bFlag
bFlag = False

sub onTVItemSelect ()

	dim sName
	dim bUnselectAll

	setDirty(L_SaveCategory_Text)

	if bFlag then
		bFlag = False
		exit sub
	end if

	sName = sGetCategoryFromEvent()
	if (True = bIsDescendant(sName)) then
		MsgBox L_IsDescendant_Text, 48
		treeview.selectNode(sName)
	else
		bUnselectAll = False
		if (sName = L_NoParentCategory_Text) then
			bUnselectAll = True
		elseif (UBound(g_aParentCategories) = -1) then
			bUnselectAll = True
		elseif((UBound(g_aParentCategories) = 0) and (g_aParentCategories(0) = L_NoParentCategory_Text)) then
			bUnselectAll = True
		end if
		
		if bUnselectAll then
			bFlag = True
			treeView.unselectAll
			treeView.selectNode(sName)
			ReDim g_aParentCategories(-1)
		end if

		AddArray g_aParentCategories, sName
	end if
end sub

sub onTVItemUnselect ()

	dim sName

	sName = sGetCategoryFromEvent()
	DeleteArray g_aParentCategories, sName

	if (UBound(g_aParentCategories) = -1) then
		bFlag = True
		AddArray g_aParentCategories, L_NoParentCategory_Text
		treeView.selectNode (L_NoParentCategory_Text)
	else
		setDirty(L_SaveCategory_Text)
	end if
end sub

sub onTVItemOpen ()

	tvform.category.value = sGetCategoryFromEvent
end sub

function bIsDescendant (ByRef sParentUID)

	dim Opt

	bIsDescendant = True

	for each Opt in ass_child_category.children
		if (True = treeView.areRelated(Opt.value, sParentUID)) then exit function
	next

	bIsDescendant = False
end function

function bIsAncestor (ByRef sChildUID)

	dim sParentUID

	bIsAncestor = True

	for each sParentUID in g_aParentCategories
		if (True = treeView.areRelated(sChildUID, sParentUID)) then exit function
	next

	bIsAncestor = False
end function

function sGetCategoryName (ByRef xmlNode)

	sGetCategoryName = xmlNode.getAttribute("caption")
	if (sGetCategoryName = "") then sGetCategoryName = xmlNode.text
end function

sub onSelect ()
	dim sID
	dim elOptions

	sID = window.event.srcElement.id
	if sID = "avail_child_category" then
		dim sChildName

		sChildName = avail_child_category.value
		if (True = bIsAncestor (sChildName)) then
			avail_child_category.unselect(sChildName)
			MsgBox L_IsAncestor_Text, 48
			exit sub
		end if
		nhForm.add_child_category.disabled = false
	else
		nhForm.remove_child_category.disabled = false
		g_nSelections = g_nSelections + 1
		if (g_nSelections > 1) then
			nhForm.move_up.disabled = true
			nhForm.move_down.disabled = true
			exit sub
		else'(g_nSelections = 1) then
			nhForm.move_up.disabled = false
			nhForm.move_down.disabled = false
			set elOptions = ass_child_category.children
			if ass_child_category.selected is elOptions(0) then nhForm.move_up.disabled = true
			if ass_child_category.selected is elOptions(elOptions.length - 1) then nhForm.move_down.disabled = true
		end if
	end if
end sub

sub onUnselect ()
	dim sID
	dim elSrc, elOptions

	set elSrc = window.event.srcElement
	sID = elSrc.id

	if sID = "avail_child_category" then
		if (elSrc.selected is Nothing) then _
			nhForm.add_child_category.disabled = true
	else
		g_nSelections = g_nSelections - 1
		if (g_nSelections = 1) then
			nhForm.move_up.disabled = false
			nhForm.move_down.disabled = false
			set elOptions = ass_child_category.children
			if ass_child_category.selected is elOptions(0) then nhForm.move_up.disabled = true
			if ass_child_category.selected is elOptions(elOptions.length - 1) then nhForm.move_down.disabled = true
		elseif (g_nSelections < 1) then
			nhForm.move_up.disabled = true
			nhForm.move_down.disabled = true
			nhForm.remove_child_category.disabled = true
		end if
	end if
end sub

sub onAdd ()

	ass_child_category.add(avail_child_category.remove)
	nhForm.add_child_category.disabled = true
	if (g_nSelections = 1) then nhForm.move_down.disabled = true
end sub

sub onRemove ()

	g_nSelections = 0
	avail_child_category.add(ass_child_category.remove())
	nhForm.remove_child_category.disabled = true
	nhForm.move_up.disabled = true
	nhForm.move_down.disabled = true
end sub

sub onMoveUp ()

	ass_child_category.moveUp
end sub

sub onMoveDown ()

	ass_child_category.moveDown
end sub

sub nhForm_onSubmit ()
	dim sValue
	dim elOption

	dim aAddChildCategories()
	redim aAddChildCategories(-1)

	for each elOption in ass_child_category.all.tags("DIV")
		sValue = elOption.value
		if not bRemovedFromArray (g_aAssignedCategories, sValue) then _
			call AddArray (aAddChildCategories, sValue)
	next

	with nhForm
		.parent_categories.value = sConvertArrayToString (g_aParentCategories)
		.added_child_categories.value = sConvertArrayToString(aAddChildCategories)
		.removed_child_categories.value = sConvertArrayToString(g_aAssignedCategories)

		.submit()
	end with
end sub

sub showChanged2 ()

	document.all.relationship_xml.value = window.event.XMLlist.xml
end sub

sub onSaveNew ()

	dim sResponse

	sResponse = window.showModalDialog("dlg_AddCategory.asp", , _
		"dialogHeight:270px;dialogWidth:350px;status:no;help:no")
	if (sResponse <> "") then
		nhForm.nh_type.value = sResponse

		select case nhForm.type.value
		case "add", "submitNew"
			nhForm.type.value = "createNew"
		case else
			nhForm.type.value = "saveNew"
		end select
	end if

	nhForm_onSubmit
end sub

' ##############################################################
' ONBROWSE Event Handler for Relationships DYNAMIC TABLE element
' (Invokes PRODUCT PICKER dialog)
'
sub onBrowse ()

	dim sResponse

	call setEditPageParameters(CATALOG_PARAMETERS, _
							   Array ("ct_name"), Array ("<%= replace(g_sCtName, """", """""") %>"))
	sResponse = window.showModalDialog ("dlg_ProductPicker.asp?mode=CATALOGS", , _
		"dialogHeight:545px;dialogWidth:700px;status:no;help:no")
	dClearServerState (CATALOG_PARAMETERS)

	if (sResponse <> "") then
		dim sType
		dim aParams

		aParams = Split (sResponse, SEPARATOR1)
		if (aParams(1) = "CategoryName") then
			sType = L_Category_Text
		else
			sType = L_Product_Text
		end if

		with DynamicTable
			.field("rel_item").value = aParams(2)
			.field("rel_descr").value = sType
		end with
	end if
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertEditTaskBar g_sHeader, g_sStatusText
%>

<xml id='esData'>
	<DOCUMENT>
		<RECORD>
			<cg_name><%= Server.HTMLEncode(g_sCgName) %></cg_name>
<%= sGetCategoryDefinitionXML %>
			<price><%= g_sPrice %></price>
			<cg_searchable><%= g_sIsSearchable %></cg_searchable>
		</RECORD>
	</DOCUMENT>
</xml>

<xml id='esMeta'>
	<editsheets>
		<editsheet>
		    <global expanded='yes' downlevel='no'>
				<name><%= L_CategoryProperties_Text %></name>
				<key><%= L_CategoryProperties_Tooltip %></key>
			</global>
			<fields>
			    <text id='cg_name' required='yes' readonly='<% If (g_sActionType = "submitNew") Then %>no<% Else %>yes<% End If %>' 
			           maxlen='128' subtype='short'>
			        <charmask><%= Server.HTMLEncode("^[^<>]*$") %></charmask>
			        <name><%= L_Name_Text %></name>
			        <tooltip><%= L_Name_Tooltip %></tooltip>
   			        <error><%= sFormatString (L_TextField2_ErrorMessage, Array(1, 128)) %></error>
			    </text>
<%
Dim iHoldType
Dim iMinLength
Dim iMaxLength

Dim sRows
Dim sValue

Dim sMin
Dim sMax
Dim sItem
Dim sItemID

For Each sItem In g_aCgProperties
	iHoldType = getPropertyAttribute(sItem, "DataType")
	sItemID = sReplaceSpaces(sItem)

    Select Case iHoldType
	Case STRING_TYPE
		iMInLength = getPropertyAttribute(sItem, "MinLength")
		iMaxLength = getPropertyAttribute(sItem, "MaxLength")
%>
				<text id='<%= sItemID %>' required='no' readonly='no' 
					minlen='<%= iMinLength %>'
					maxlen='<%= iMaxLength %>'
					subtype='<% If (iMaxLength > 80) Then %>short<% Else %>long<% End If %>'>
			        <name><%= sColon(sItem) %></name>
			        <tooltip><%= sItem %></tooltip>
			        <prompt><%= L_EnterHTMLText_Text %></prompt>
			        <charmask>^(\s*\S*\s*)*$</charmask>
   			        <error><%= sFormatString (L_TextField1_ErrorMessage, Array(iMinLength, iMaxLength)) %></error>
			    </text>
<%
	Case FILEPATH_TYPE
			sValue = g_oDict(sItem)
			If (True = bFileContainsImage(sValue)) Then _
				sRows = sRows & sConvertPropertyToTableRow(sItem, sValue)
%>
				<text id='<%= sItemID %>' required='no' readonly='no' 
			           maxlen='256' subtype='short'>
			        <name><%= sColon(sItem) %></name>
			        <tooltip><%= sItem %></tooltip>
					<prompt><%= L_EnterURL_Text %></prompt>
			    </text>
<%	Case INTEGER_TYPE
		sMin = stringValue(getPropertyAttribute(sItem, "i_MinValue"), INTEGER_TYPE)
		sMax = stringValue(getPropertyAttribute(sItem, "i_MaxValue"), INTEGER_TYPE)
%>
				<numeric id='<%= sItemID %>' required='no' readonly='no' 
			           min='<%= sMin %>' max='<%= sMax %>' subtype='integer'>
			        <name><%= sColon(sItem) %></name>
					<tooltip><%= sItem %></tooltip>
			        <format><%= g_sMSCSNumberFormat %></format>
			        <error></error>
			    </numeric>
<%	Case FLOAT_TYPE
		sMin = stringValue(getPropertyAttribute(sItem, "fp_MinValue"), FLOAT_TYPE)
		sMax = stringValue(getPropertyAttribute(sItem, "fp_MaxValue"), FLOAT_TYPE)
%>
				<numeric id='<%= sItemID %>' required='no' readonly='no' 
			           min='<%= sMin %>' max='<%= sMax %>' subtype='float'>
			        <name><%= sColon(sItem) %></name>
			        <tooltip><%= sItem %></tooltip>
			        <format><%= g_sMSCSNumberFormat %></format>
			        <error></error>
			    </numeric>
<%	Case CURRENCY_TYPE
		sMin = stringValue(getPropertyAttribute(sItem, "cy_MinValue"), CURRENCY_TYPE)
		sMax = stringValue(getPropertyAttribute(sItem, "cy_MinValue"), CURRENCY_TYPE)
%>
				<numeric id='<%= sItemID %>' required='no' readonly='no' 
			           min='<%= sMin %>' max='<%= sMax %>' subtype='currency'>
			        <name><%= sColon(sItem) %></name>
			        <tooltip><%= sItem %></tooltip>
			        <format><%= g_sMSCSCurrencyFormat %></format>
			    </numeric>
<%	Case DATETIME_TYPE
		sMin = stringValue(getPropertyAttribute(sItem, "dt_MinValue"), DATE_TYPE)
		sMax = stringValue(getPropertyAttribute(sItem, "dt_MaxValue"), DATE_TYPE)
%>
				<date id='<%= sItemID %>' required='no' readonly='no' firstday='<%= g_sMSCSWeekStartDay %>'
					min='<%= sMin %>' max='<%= sMax %>'>
			        <name><%= sColon(sItem) %></name>
			        <tooltip><%= sItem %></tooltip>
			        <format><%= g_sMSCSDateFormat %></format>
			        <error><%= sFormatString (L_DateField_ErrorMessage, Array (sMin, sMax)) %></error>
			    </date>
<%	Case ENUMERATION_TYPE %>
			    <select id='<%= sItemID %>' required='no' readonly='no'>
			        <name><%= sColon(sItem) %></name>
			        <tooltip><%= sItem %></tooltip>
				    <select id='<%= sItemID %>'>
<%= sGetEnumOptions (sItem) %>
				    </select>
			    </select>
<%
	End Select
Next
%>
				<numeric id='price' required='no' readonly='no' 
			           min='<%= stringValue(MIN_MONEY, CURRENCY_TYPE) %>'
			           max='<%= stringValue(MAX_MONEY, CURRENCY_TYPE) %>'
			           subtype='currency'>
			        <name><%= L_Price_Text %></name>
			        <tooltip><%= L_Price_Tooltip %></tooltip>
			        <format><%= g_sMSCSCurrencyFormat %></format>
			    </numeric>
			    <boolean id='cg_searchable' readonly='no'>
			        <name><%= L_Searchable_Text %></name>
			        <tooltip><%= L_Searchable_Tooltip %></tooltip>
			    </boolean>
			</fields>
		</editsheet>
		<editsheet>
		    <global expanded='no' downlevel='no'>
		        <name><%= L_AssignedCategories_Text %></name>
		        <key><%= L_AssignedCategories_Accelerator %></key>
			</global>
			<template register='ass_child_category'><![CDATA[<BR>
			<TABLE>
				<TR>
					<TD><%= L_ParentCategory_Text %></TD>
					<TD STYLE='width:20px'></TD>
					<TD><%= L_AvailChildCat_Text %></TD>
					<TD>&nbsp;</TD>
					<TD><%= L_AssChildCat_Text %></TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD VALIGN="TOP" STYLE="height:100%; width:25%">
						<DIV STYLE="width:100%; height:100%; overflow:auto; background:white; border:thin inset silver">
							<DIV ID='treeView'
								CLASS='treeView '
								DataXML='tvData'
								LANGUAGE='VBScript'
								OnItemSelect='onTVItemSelect'
								OnItemUnselect='onTVItemUnselect'
								OnItemOpen='onTVItemOpen'><%= L_LoadingWidget_Text %></DIV>
						</DIV>
					</TD>
					<TD></TD>
					<TD VALIGN="TOP" STYLE="width:25%">
						<div ID="avail_child_category" selection='multi'
							CLASS="listBox" STYLE="WIDTH: 100%; HEIGHT: 180px"
							LANGUAGE="vbscript"
							ONSELECT="onSelect"
							ONUNSELECT="onUnselect">
							<%= sGetAvailableChildOptions %>
						</div>
					</TD>
					<TD ALIGN="CENTER">
						<BUTTON ID="add_child_category" CLASS="bdbutton" NAME="add_child_category" DISABLED ONCLICK="onAdd()" STYLE="<%= L_Button_Style %>"><%= L_Add_Button %></BUTTON>
						<BR>
						<BUTTON ID="remove_child_category" CLASS="bdbutton" NAME="remove_child_category" DISABLED ONCLICK="onRemove()" STYLE="<%= L_Button_Style %>"><%= L_Remove_Button %></BUTTON>
					</TD>
					<TD VALIGN="TOP" STYLE="width:25%">
						<div ID="ass_child_category" selection='multi'
							CLASS="listBox" STYLE="WIDTH: 100%; HEIGHT: 180px"
							LANGUAGE="vbscript"
							ONSELECT="onSelect"
							ONUNSELECT="onUnselect">
							<%= g_sAssignedChildOptions %>
						</div>
					</TD>
					<TD ALIGN="CENTER">
						<BUTTON ID="move_up" CLASS="bdbutton" NAME="move_up" DISABLED ONCLICK="onMoveUp()" STYLE="<%= L_Button_Style %>"><%= L_MoveUp_Button %></BUTTON>
						<BR>
						<BUTTON ID="move_down" CLASS="bdbutton"  NAME="move_down" DISABLED ONCLICK="onMoveDown()" STYLE="<%= L_Button_Style %>"><%= L_MoveDown_Button %></BUTTON>
					</TD>
				</TR>
			</TABLE>
		    ]]></template>
		</editsheet>
		<editsheet>
		    <global expanded='no' downlevel='no'>
		        <name><%= L_CategoryRelationships_Text %></name>
		        <key><%= L_CategoryRelationships_Tooltip %></key>
			</global>
			<template register='DynamicTable'><![CDATA[
				<DIV ID='DynamicTable' 
					CLASS='dynamicTable'
					MetaXML='dtMeta'
					DataXML='dtData'
					LANGUAGE='VBScript'
					ONCHANGE='showChanged2()'><%= L_LoadingPropertyGroup_Text %>
				</DIV>
		    ]]></template>
		</editsheet>
<% If (sRows <> "") Then %>
		<editsheet>
		    <global expanded='no' downlevel='no'>
				<name><%= L_Images_Text %></name>
				<key><%= L_Images_Accelerator %></key>
			</global>
			<template><![CDATA[
				<TABLE STYLE="width:98%; table-layout:fixed; overflow-x:hidden">
					<COL WIDTH=30%><COL WIDTH=70%>
<%= sRows %>
				</TABLE>
		    ]]></template>
		</editsheet>
<% End If %>
	</editsheets>
</xml>

<xml id='dtMeta'>
	<dynamictable>
	    <global required='no' keycol='rel_name' uniquekey='no' sortbykey='yes' />
	    <fields>
			<text id='rel_name_old' hide='yes' subtype='short'>
				<name><%= L_ID_Text %></name>
			</text>
			<text id='rel_item_old' hide='yes' subtype='short'>
				<name><%= L_ID_Text %></name>
			</text>
			<text id='rel_descr_old' hide='yes' subtype='short'>
				<name><%= L_ID_Text %></name>
			</text>
			<text id='rel_name' width='33' required='yes' subtype='short' maxlen='128'>
				<name><%= L_Relationship_Text %></name>
				<prompt><%= L_Relationship2_Text %></prompt>
			    <error><%= sFormatString(L_TextField3_ErrorMessage, Array(1, 128)) %></error>
			</text>
			<text id='rel_item' width='33' required='yes' onbrowse='onBrowse()' browsereadonly='yes'>
				<name><%= L_Item_Text %></name>
				<prompt><%= L_Item2_Text %></prompt>
			    <error><%= L_Item_ErrorMessage %></error>
			</text>
			<text id='rel_descr' width='33' readonly='yes' subtype='short' maxlen='256'>
				<name><%= L_ItemType_Text %></name>
				<prompt><%= L_ItemType_Text %></prompt>
			</text>
	    </fields>
	</dynamictable>
</xml>

<xml id='dtData'>
	<records>
<%= sGetDTData (g_oCategory) %>
	</records>
</xml>

<xml id='tvData'>
	<document skip='yes' selection='multi'>
<%
	GetTVData
%>
<%= g_sTVData %>
		<operations hidden='yes'>
			<expand formid='tvform' />
		</operations>
	</document>
</xml>

<DIV ID='bdcontentarea' CLASS='editSheetContainer'>
	<FORM ACTION="" METHOD="POST" NAME="nhForm" ID="nhForm" ONTASK="nhForm_onSubmit">
		<INPUT TYPE="Hidden" NAME="type" VALUE="<%= g_sActionType %>" ID="type">
		<INPUT TYPE="Hidden" NAME="nh_type" ID="nh_type">
		<INPUT TYPE="Hidden" NAME="prev_name" ID="prev_name" VALUE="<%= g_sPrevCgName %>">
		<INPUT TYPE="Hidden" NAME="ct_name" VALUE="<%= g_sCtName %>" ID="ct_name">
		<INPUT TYPE="Hidden" NAME="parent_categories" ID="parent_categories">
		<INPUT TYPE="Hidden" NAME="added_child_categories" ID="added_child_categories">
		<INPUT TYPE="Hidden" NAME="removed_child_categories" ID="removed_child_categories">
		<INPUT TYPE="Hidden" NAME="relationship_xml" ID="relationship_xml">
		<INPUT TYPE="Hidden" NAME="cg_type" ID="cg_type" VALUE="<%= g_sCgType %>">
		<DIV ID='esControl' 
			CLASS='editSheet' 
			ONCHANGE='setDirty(L_SaveCategory_Text)'
			ONREQUIRE ='setRequired(L_Required_Text)'
			ONVALID = 'setValid(L_Valid_Text)'
			MetaXML='esMeta'
			DataXML='esData'><%= L_LoadingPropertyGroups_Text %>
		</DIV>
	</FORM>
</DIV>

<FORM ID="tvform" NAME="tvform" ACTION="response_Categories.asp">
	<INPUT TYPE="Hidden" NAME="type" VALUE="expand">
	<INPUT TYPE="Hidden" NAME="method" VALUE="2">
	<INPUT TYPE="Hidden" NAME="category">
	<INPUT TYPE="Hidden" NAME="edit_category" VALUE="<%= g_sCgName %>">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sCtName %>">
</FORM>

<FORM ACTION="" METHOD="POST" NAME='closeForm' ID="closeForm">
	<INPUT TYPE="Hidden" NAME="type" VALUE="close">
</FORM>

<FORM ACTION="" METHOD="POST" NAME="nhForm2" ID="nhForm2" ONTASK="onSaveNew">
</FORM>

</BODY>
</HTML>
<%
	ReleaseGlobalObjects
%>


<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

' ----- PUBLIC Declarations
Public g_sAction		' string representation of action to take on page
Public g_sActionType	' type to pass through

Public g_sHeader		' Tab header
Public g_sStatusText	' To be displayed in the Status Bar

Public g_sCgName		' Category Name
Public g_sPrevCgName	' Previous Category Name
Public g_sCgType		' Category Definition

Public g_sCtName		' Catalog to add this category to
Public g_sPrice
Public g_sIsSearchable
Public g_sAssignedCategories
Public g_sAssignedChildOptions

Public g_aAddedChildCategories
Public g_aRemovedChildCategories

Public g_sProductID
Public g_sTVData

Public g_oCategory	' To hold category object
Public g_oDict		' g_oDict will hold values of properties associated
					' with the category definition for this category

Public g_rsProperties	' Property Definitions used by the Current Catalog

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeGlobalVariables_Part1
		InitializeCatalogObject
		Set g_oCat = g_oCatMgr.GetCatalog(g_sCtName)
		Set g_rsProperties = g_oCatMgr.Properties
	InitializeGlobalVariables_Part2

	Perform_Processing_BasedOnAction
	g_sAssignedChildOptions = sGetAssignedChildOptions()
End Sub

' ##########################################################
'
Sub InitializeGlobalVariables_Part1 ()

	Dim sAddedChildCategories
	Dim sRemovedChildCategories

	ReDim g_aParentCategories(-1)

	g_sAction = Request.Form("type")
	If (g_sAction = "") Then
		Dim dServerState
		' It's a Nested Page
		Set dServerState = Session (CATEGORY_PARAMETERS)
			g_sAction = dServerState("type")
			g_sCgName = sNullToEmptyString(dServerState("cg_name"))
			g_sCtName = sNullToEmptyString(dServerState("ct_name"))
			g_sCgType = sNullToEmptyString(dServerState("cg_type"))
		Set Session (CATEGORY_PARAMETERS) = Nothing
		Session (CATEGORY_PARAMETERS) = Empty

		g_sPrevCgName = g_sCgName
	Else
		If (g_sAction = "open") Or (g_sAction = "add") Then Exit Sub

		g_sCgName = Request.Form("cg_name")
		g_sPrevCgName = Request.Form("prev_name")
		If (g_sPrevCgName = "") Then g_sPrevCgName = g_sCgName
		g_sCgType = Request.Form("cg_type")
		g_sCtName = Request.Form("ct_name")
		SplitToDynamicArray Request.Form ("parent_categories"), SEPARATOR1, g_aParentCategories

		g_sIsSearchable = Request.Form("cg_searchable")
		g_sPrice = Request.Form("price")

		sAddedChildCategories = Request.Form("added_child_categories")
		sRemovedChildCategories = Request.Form("removed_child_categories")
		If (sAddedChildCategories <> "") Then g_aAddedChildCategories = Split(sAddedChildCategories, SEPARATOR1)
		If (sRemovedChildCategories <> "") Then g_aRemovedChildCategories = Split(sRemovedChildCategories, SEPARATOR1)
	End If
End Sub

' ##########################################################
'
Sub InitializeGlobalVariables_Part2 ()

	g_sProductID = g_oCat.IdentifyingProductProperty

	If (g_sAction <> "add") And (g_sAction <> "submitNew") And (g_sAction <> "createNew") Then
		Set g_oCategory = g_oCat.GetCategory(g_sPrevCgName)
	Else
		Set g_oCategory = Nothing
	End If
End Sub

' ################################################################
'
Sub Perform_Processing_BasedOnAction ()

	'' Special case when editing a category starting from product page
	'' Category type/definition might not be known
	If (g_sAction <> "open") Then _
		InitializeGlobalVariables

	Select Case g_sAction
	Case "submitNew"
		Process_SubmitNEW

	Case "createNew"
		Process_CreateNEW

	Case "saveNew"
		Process_SaveNEW

	Case "submitEdit", "submitEditF"
		Process_SubmitEDIT

	Case "add"
		Process_ADD

	Case "open"
		Process_OPEN
	End Select
End Sub

' ################################################################
'
Sub InitializeGlobalVariables ()

	Set g_oDict = Nothing
	Set g_oDict = Server.CreateObject("Commerce.Dictionary")

	GetDefinitionProperties g_sCgType, g_aCgProperties
	If (g_sAction = "open") Then Exit Sub

	For Each sItem In g_aCgProperties
		If (g_sAction = "add") Then
			g_oDict(sItem) = ""
		Else
			g_oDict(sItem) = Request.Form(sReplaceSpaces(sItem))
		End If
	Next
End Sub

' ################################################################
'
Sub GetDefinitionProperties (ByRef sDefName, ByRef aProperties)

	Dim rsProperties
	ReDim aProperties(-1)

	Set rsProperties = g_oCatMgr.GetDefinitionProperties (sDefName)
	While (Not rsProperties.EOF)
		AddArray aProperties, rsProperties("PropertyName")
		rsProperties.MoveNext
	Wend

	ReleaseRecordset rsProperties
End Sub

' ################################################################
'
Sub Process_SubmitNEW ()

	Dim rsProperties
	Dim sRelationshipXML
	Dim oRelationshipXML

	g_sActionType = "submitEdit"

	CreateCategory
	If (g_oCategory Is Nothing) Then
		g_sActionType = "submitNew"
		g_sStatusText = L_CategoryNameAlreadyExists_Text
		g_sHeader = sFormatString (L_Category_HTMLText, Array(g_sCtName, L_New_HTMLText, g_sCgType))
		Exit Sub
	End If

	' Create category relationships
	sRelationshipXML = Request.Form("relationship_xml")
	If (sRelationshipXML <> "") Then
		Set oRelationshipXML = oCreateXMLDocument(sRelationshipXML)
		If IsObject(oRelationshipXML) Then
			ProcessRelationshipXML oRelationshipXML, g_oCategory
		End If
	End If

	On Error Resume Next
	' Add child categories
	If IsArray(g_aAddedChildCategories) Then
		For Each sItem In g_aAddedChildCategories
			If (sItem <> "") Then g_oCategory.AddChildCategory(sItem)
		Next
	End If

	g_sHeader = sFormatString (L_Category_HTMLText, Array(g_sCtName, g_sCgName, g_sCgType))
	g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sCgName))
End Sub

' ################################################################
'
Sub Process_CreateNEW ()

	Process_SubmitNEW
	Process_ADD
End Sub

' ################################################################
'
Sub Process_SaveNEW ()

	Process_SubmitEDIT
	Process_ADD
End Sub

' ################################################################
'
Sub Process_SubmitEDIT ()

	Dim sRelationshipXML
	Dim oRelationshipXML

	g_sActionType = "submitEdit"

	UpdateCategory
	' Edit Relationships
	sRelationshipXML = Request.Form("relationship_xml")
	If (sRelationshipXML <> "") Then
		Set oRelationshipXML = oCreateXMLDocument(sRelationshipXML)
		If IsObject(oRelationshipXML) Then
			ProcessRelationshipXML oRelationshipXML, g_oCategory
		End If
	End If

	' Replace parent category
	ReplaceParentCategory

	' Remove child categories
	If IsArray(g_aRemovedChildCategories) Then
		For Each sItem In g_aRemovedChildCategories
			If (sItem <> "") Then g_oCategory.RemoveChildCategory(sItem)
		Next
	End If

	' Add child categories
	If IsArray(g_aAddedChildCategories) Then
		For Each sItem In g_aAddedChildCategories
			If (sItem <> "") Then g_oCategory.AddChildCategory(sItem)
		Next
	End If

	g_sHeader = sFormatString(L_Category_HTMLText, Array(g_sCtName, g_sCgName, g_sCgType))
	g_sStatusText = sFormatString(L_Saved_StatusBar, Array(g_sCgName))
End Sub

' ################################################################
'
Sub Process_ADD ()

	Dim rsProperties

	g_sPrice = ""
	g_sCgName = ""
	g_sPrevCgName = ""

	g_sIsSearchable = "0"
	g_sActionType = "submitNew"
	g_sHeader = sFormatString(L_Category_HTMLText, Array(g_sCtName, L_New_HTMLText, g_sCgType))

	If (g_sAction = "saveNew") Or (g_sAction = "createNew") Then
		g_sAction = "add"
		g_sCgType = Request.Form("nh_type")
		InitializeGlobalVariables
	End If
End Sub

' ################################################################
'
Sub Process_OPEN ()

	Dim rsParents
	Dim rsProperties

	g_sActionType = "submitEdit"

	If (Not g_oCategory Is Nothing) Then
		' Retrieve Parent Categories
		Set rsParents = g_oCategory.ParentCategories
		Do Until rsParents.EOF
			AddArray g_aParentCategories, rsParents("Name").Value
			rsParents.MoveNext
		Loop
		ReleaseRecordset rsParents
		' Retrieve Category Properties
		Set rsProperties = g_oCategory.GetCategoryProperties
		If (Not rsProperties.EOF) Then
			g_sCgType = rsProperties("DefinitionName")
			g_sIsSearchable = sConvertFromBoolean(rsProperties("IsSearchable").Value)
			g_sPrice = stringValue (rsProperties("cy_list_price").Value, CURRENCY_TYPE)

			InitializeGlobalVariables

			For Each sItem In g_aCgProperties
				g_oDict(sItem) = stringValue (rsProperties(sItem).Value, getPropertyAttribute(sItem, "DataType"))
			Next
		End If
		ReleaseRecordset rsProperties
	End If

	g_sHeader = sFormatString (L_Category_HTMLText, Array(g_sCtName, g_sCgName, g_sCgType))
End Sub

' ################################################################
'
Sub ReplaceParentCategory ()

	Const ERR_ALREADY_ADDED = &H8898002D

	Dim rsParents
	Dim sParentName

	' Remove existing parent categories
	Set rsParents = g_oCategory.ParentCategories
	While Not rsParents.EOF
		g_oCategory.RemoveParentCategory rsParents("Name").Value
		rsParents.MoveNext
	Wend
	ReleaseRecordset rsParents
	If (UBound(g_aParentCategories) = -1) Then Exit Sub
	If (g_aParentCategories(0) = L_NoParentCategory_Text) Then Exit Sub

	On Error Resume Next
	' Add new parent categories
	For Each sParentName In g_aParentCategories
		g_oCategory.AddParentCategory(sParentName)
		' Multiple selections of the same category
		If (Err.Number = ERR_ALREADY_ADDED) Then Err.Clear
	Next
End Sub

' ################################################################
' Sub: CreateCategory
'	This function creates a new category
'
Sub CreateCategory ()

	Dim sItem
	Dim sParentName
	Dim rsProperties

	On Error Resume Next
	Set g_oCategory = g_oCat.CreateCategory (g_sCgType, g_sCgName, , _
											 CBool(g_sIsSearchable))

	If (Not g_oCategory Is Nothing) Then
		For Each sParentName In g_aParentCategories
			g_oCategory.AddParentCategory sParentName
		Next

		Set rsProperties = g_oCategory.GetCategoryProperties
		If (Not rsProperties.EOF) Then
			rsProperties("cy_list_price") = CCur(g_sPrice)

			For Each sItem In g_oDict
				rsProperties(sItem) = variantValue(g_oDict(sItem), sItem)
			Next

			g_oCategory.SetCategoryProperties rsProperties
		End If			

		ReleaseRecordset rsProperties
	End If
End Sub

' ################################################################
' Sub: UpdateCategory
'	updates a category
'
' Returns:
'	string error message if error
'
Sub UpdateCategory ()

	Dim sItem
	Dim bForceUpdate
	Dim rsProperties

	Set rsProperties = g_oCategory.GetCategoryProperties
	If (Not rsProperties.EOF) Then
		rsProperties("CategoryName") = g_sCgName
		rsProperties("cy_list_price") = variantValue (g_sPrice, CURRENCY_TYPE)
		rsProperties("IsSearchable")  = variantValue (g_sIsSearchable, BOOLEAN_TYPE)

		For Each sItem In g_oDict
			rsProperties(sItem) = variantValue(g_oDict(sItem), sItem)
		Next

		If (g_sAction = "submitEditF") Then
			bForceUpdate = True
		Else
			bForceUpdate = False
		End If

		g_oCategory.SetCategoryProperties rsProperties, bForceUpdate
		CheckForPropertyAlreadyModified
	End If

	'ReleaseRecordset rsProperties
	Set rsProperties = Nothing
End Sub

' ################################################################
'
Function sGetCategoryDefinitionXML ()

	Dim sItem
	Dim sValue
	Dim sItemID

	' generate product-type characteristics XML
	For Each sItem In g_aCgProperties
		sItemID = sReplaceSpaces(sItem)
		sValue = replace(replace(replace(g_oDict(sItem), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
		sGetCategoryDefinitionXML = sGetCategoryDefinitionXML &  _
				"<" & sItemID & ">" & sValue & "</" & sItemID & ">"
	Next
End Function

' ################################################################
'
Function sGetAvailableChildOptions ()

	Dim rsChildren
	Dim rsAncestors
	Dim rsCategories

	Dim aNonChildren()
	Dim sCategoryName

	GetCategories rsCategories
	ReDim aNonChildren(-1)

	If (Not g_oCategory Is Nothing) Then
		Set rsChildren = g_oCategory.ChildCategories
		Set rsAncestors = g_oCategory.AncestorCategories

		Call GetRSToArray (rsChildren, "CategoryName", aNonChildren)
		Call GetRSToArray (rsAncestors, "Name", aNonChildren)

		ReleaseRecordset rsChildren
		ReleaseRecordset rsAncestors
	End If

	Do Until rsCategories.EOF
		sCategoryName = rsCategories("CategoryName")
		If bIsValid (sCategoryName, aNonChildren) Then
			sGetAvailableChildOptions = sGetAvailableChildOptions & _
					"<DIV VALUE='" & Server.HTMLEncode(sCategoryName) & "'>" & sCategoryName & "</DIV>" & vbCR
		End If

		rsCategories.MoveNext
	Loop

	ReleaseRecordset rsCategories
End Function

' ################################################################
'
Function sGetAssignedChildOptions ()

	Dim iLength
	Dim rsChildren
	Dim sChildName

	If (g_oCategory Is Nothing) Then Exit Function

	Set rsChildren = g_oCategory.ChildCategories
	While Not rsChildren.EOF
		sChildName = Server.HTMLEncode(rsChildren("CategoryName"))
		sGetAssignedChildOptions = sGetAssignedChildOptions & _
					"<DIV VALUE='" & sChildName & "'>" & sChildName & "</DIV>" & vbCR

		g_sAssignedCategories = g_sAssignedCategories & _
								SEPARATOR1 & sChildName
		rsChildren.MoveNext
	Wend

	iLength = Len(g_sAssignedCategories)
	If (iLength > 0) Then _
		g_sAssignedCategories = Right(g_sAssignedCategories, iLength - Len(SEPARATOR1))

	ReleaseRecordset rsChildren
End Function

' ################################################################
'
Sub GetCategories (ByRef rsCategories)

	Dim sPhrase

	sPhrase ="(i_ClassType = " & CATEGORY_CLASS & ") AND (CategoryName != '" & replace(g_sCgName, "'", "''") & "')"
    Set rsCategories = g_oCatMgr.Query (sPhrase, g_sCtName, , "CategoryName", "CategoryName", -1)
End Sub

' ################################################################
'
Sub GetRSToArray (ByRef rs, ByRef sField, ByRef aNonChildren)

	Do Until rs.EOF
		AddArray aNonChildren, rs(sField).Value
		rs.MoveNext
	Loop
End Sub

' ################################################################
'
Function bIsValid (ByRef sCategoryName, ByRef aNonChildren)

	Dim iIndex
	Dim iUBound

	iUBound = UBound(aNonChildren)
	If (iUBound = -1) Then
		bIsValid = True
		Exit Function
	End If

	bIsValid = False
	For iIndex = 0 To iUBound
		If (aNonChildren(iIndex) = sCategoryName) Then
			aNonChildren(iIndex) = aNonChildren(iUBound)
			ReDim Preserve aNonChildren (iUBound - 1)
			Exit Function
		End If
	Next

	bIsValid = True
End Function

' ################################################################
'
Sub GetTVData ()

	Dim bSelect
	Dim rsCategories

	bSelect = False
	g_sTVData = "<category"
	If (UBound(g_aParentCategories) = -1) Then
		bSelect = True
	ElseIf(g_aParentCategories(0) = L_NoParentCategory_Text) Then
		bSelect = True
	End If
	If bSelect Then	g_sTVData = g_sTVData & " selected='yes'"

	g_sTVData = g_sTVData & ">" & L_NoParentCategory_Text &"</category>"

	Set rsCategories = g_oCat.RootCategories("CategoryName")
	While Not rsCategories.EOF
		GetCategoryForViewCategoryPage rsCategories("CategoryName").Value, g_sCgName
		rsCategories.MoveNext
	Wend

	ReleaseRecordset rsCategories
End Sub

Sub ReleaseGlobalObjects ()

	ReleaseRecordset g_rsProperties

	Set g_oCat = Nothing
	Set g_oDict = Nothing
	Set g_oCatMgr = Nothing
	Set g_oCategory = Nothing
End Sub

%>