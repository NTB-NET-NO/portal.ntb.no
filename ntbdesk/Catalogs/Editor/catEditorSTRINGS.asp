<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version


Const L_ErrorsInPage_ErrorMessage = "<BR>An error occurred while loading."
Const L_BadConnectionString_ErrorMessage = "The catalog system could not be initialized.<BR>Please contact your system administrator."

%>

<% If (PAGE_TYPE <> "RESPONSE") Then %>

<%

' H-T-M-L  T-e-x-t
Const L_New_HTMLText = "new"
Const L_Catalogs_HTMLText = "Catalogs"
Const L_Categories_HTMLText = "Categories:"
Const L_CategoryPricing_HTMLText = "Category Pricing:"
Const L_Catalog_HTMLText = "Catalog - %1"
Const L_CatalogNew_HTMLText = "Catalog - New"
Const L_Product_HTMLText = "Catalog Editor - %1 - %2 (%3)"
Const L_Category_HTMLText = "Catalog Editor - %1 - %2 (%3)"
Const L_CustomCatalog_HTMLText = "Custom Catalog - %1"
Const L_CustomCatalogNew_HTMLText = "Custom Catalog - New"

' S-t-a-t-u-s  B-a-r
Const L_Saved_StatusBar = "%1 saved"
Const L_Deleted_StatusBar = "%1 deleted"
Const L_ErrorSaving_StatusBar = "Error saving %1"
Const L_ErrorDeleting_StatusBar = "Error deleting %1"
Const L_CatalogImportInProgress_StatusBar = "Catalog import in progress..."
Const L_CatalogImportComplete_StatusBar = "Catalog import complete."
Const L_CatalogImportErrors_StatusBar = "Errors importing catalog."

' D-i-a-l-o-g   T-i-t-l-e
Const L_Critical_DialogTitle = "Critical"
Const L_NewProduct_DialogTitle = "New Product"
Const L_NewCategory_DialogTitle = "New Category"
Const L_SelectVendor_DialogTitle = "Select Vendor"
Const L_ProductPicker_DialogTitle = "Product Picker"
Const L_CreateCatalog_DialogTitle = "Create Catalog"
Const L_DeleteCatalog_DialogTitle = "Delete Catalog"
Const L_NewCustomCatalog_DialogTitle = "New Custom Catalog"
Const L_ExportXMLCatalog_DialogTitle = "Export XML Catalog"
Const L_ExportCSVCatalog_DialogTitle = "Export CSV Catalog"
Const L_ImportXMLCatalog_DialogTitle = "Import XML Catalog"
Const L_ImportCSVCatalog_DialogTitle = "Import CSV Catalog"
Const L_CreateCustomCatalog_DialogTitle = "Create Custom Catalog"

' B-u-t-t-o-n
Const L_OK_Button = "OK"
Const L_Help_Button = "Help"
Const L_Clear_Button = "Clear"
Const L_Edit_Button = "Edit..."
Const L_Cancel_Button = "Cancel"
Const L_Delete_Button = "Delete"
Const L_Report_Button = "Report"
Const L_MoveUp_Button = "Move Up"
Const L_FindNow_Button = "Find Now"
Const L_Continue_Button = "Continue"
Const L_Add_Button = "Add&nbsp;&gt;"
Const L_NewProduct_Button = "New..."
Const L_NewCategory_Button = "New..."
Const L_MoveDown_Button = "Move Down"
Const L_SelectAll_Button = "Select All"
Const L_SetPricing_Button = "Set Pricing"
Const L_Remove_Button = "&lt;&nbsp;Remove"
Const L_DeselectAll_Button = "Deselect All"
Const L_ClearPricing_Button = "Clear Pricing"

' T-e-x-t
Const L_No_Text = "No"
Const L_Yes_Text = "Yes"

Const L_Float_Text = "Float"
Const L_String_Text	= "String"
Const L_Number_Text = "Number"
Const L_FileName_Text = "File Name"
Const L_Enumerated_Text = "Enumerated"
Const L_Currency_Text = "Money/Currency"
Const L_DateTime_Text = "Date, Time, Date-Time"

Const L_Value_Text = "Value:"
Const L_FindBy_Text = "Find by:"
Const L_Product_Text = "Product"
Const L_Category_Text = "Category"
Const L_Catalog_Text = "Catalog: "
Const L_SetPrice_Text = "Set price"
Const L_Properties_Text = "Properties"
Const L_CatalogNew_Text = "Catalog: new"
Const L_AllProducts_Text = "All Products"
Const L_PricingMethod_Text = "Pricing Method:"
Const L_DefinitionName_Text = "Definition Name"
Const L_FixedAdjustment_Text = "Fixed adjustment"
Const L_UseCustomPricing_Text = "Use custom pricing"
Const L_CustomCatalogNew_Text = "Custom Catalog: new"
Const L_EnterURL_Text = "enter URL"
Const L_EnterHTMLText_Text = "enter HTML text"
Const L_PercentageAdjustment_Text = "Percentage adjustment"

Const L_LoadingQB_Text = "Loading the Query Builder..."
Const L_LoadingWidget_Text = "loading widget, please wait..."
Const L_LoadingPropertyGroup_Text = "Loading Property Group..."
Const L_LoadingPropertyGroups_Text = "Loading Property Groups..."

Const L_None_Text = "<None>"
Const L_NoVendors_Text = "No Vendors"
Const L_NoParentCategory_Text = "(None)"
Const L_NoItemsFound_Text = "No Items Found"
Const L_NoneAvailable_Text = "None available"
Const L_NoProductsDeleted_Text = "No products deleted"
Const L_NoCatalogs_Text = "No Catalogs Exist, Please Create a Catalog."

Const L_SelectProductType_Text = "Select a product definition type:"
Const L_SelectCategoryType_Text = "Select a category definition type:"

Const L_AddToCatalog_Text = "Add to catalog:"
Const L_ParentCategory_Text = "Parent category:"
Const L_AssChildCat_Text = "Assigned child categories:"
Const L_AvailChildCat_Text = "Available child categories:"

Const L_ID_Text = "ID"
Const L_Item_Text = "Item"
Const L_Colon_Text = "%1:"
Const L_Currency2_Text = "Currency:"
Const L_ItemType_Text = "Item Type"
Const L_Keywords_Text = "Keywords:"
Const L_ListPrice_Text = "List Price"
Const L_Selection_Text = "Selection:"
Const L_FileToBeImp_Text = "File name:"
Const L_PriceID_Text = "Price column:"
Const L_ExportFile_Text = "File name:"
Const L_Categories2_Text = "Categories"
Const L_CatalogName_Text = "Catalog name:"
Const L_ByCatalog_Text = "Browse catalog"
Const L_ByKeyword_Text = "Keyword search"
Const L_Relationship_Text = "Relationship"
Const L_SelectVendor_Text = "Select a vendor:"
Const L_ColumnID_Text = "Product identifier:"
Const L_SearchMethod_Text = "Search method:"
Const L_Item2_Text = "Select the related item"
Const L_ByProperties_Text = "Search by properties"
Const L_KeywordsPrompt_Text = "Enter keywords to use in search"
Const L_Relationship2_Text = "enter text"
Const L_AssignedCategories2_Text = "Assigned categories:"
Const L_SelectBaseCatalog_Text = "Select a base catalog:"
Const L_AvailableCategories_Text = "Available categories:"
Const L_ProductIDAlreadyExists_Text = "ProductID already exists"
Const L_CategoryNameAlreadyExists_Text = "Category Name already exists"
Const L_DeleteAnyExistingCatalogData_Text = "Delete existing data in this catalog:"
Const L_DeleteAnyExistingDataInCatalog_Text = "Delete any existing data in this catalog:"

Const L_EnterPriceColumnName_Text = "enter name of price column"
Const L_EnterCatalogName_Text = "enter name of catalog to create"
Const L_EnterIdentifierName_Text = "enter name of identifier column"
Const L_EnterFilename_Text = "enter name of file"
Const L_EnterKeywords_Text = "Enter keywords or click &lt;Find Now&gt; to show all products."

Const L_Products2_Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Products in category: "
Const L_Products3_Text = "Products:"

' T-e-x-t  &  T-o-o-l-t-i-p
Const L_Name_Text = "Name:"
Const L_Name_Tooltip = "name"
Const L_BaseCtName_Text = "Base Catalog:"
Const L_BaseCtName_Tooltip = "Base Catalog"
Const L_StartDate_Text = "Start date:"
Const L_StartDate_Tooltip = "start date"
Const L_EndDate_Text = "End date:"
Const L_EndDate_Tooltip = "end date"
Const L_Currency_Tooltip = "currency"
Const L_Locale_Text = "Locale"
Const L_Locale_Tooltip = "locale"
Const L_UnitOfMeasure_Text = "Unit of weight measure:"
Const L_UnitOfMeasure_Tooltip = "weight unit"
Const L_UseSpecProperty_Text = "Use specific property"
Const L_UseSpecProperty_Tooltip = "use of specific property"
Const L_ProductID_Text = "Product unique ID:"
Const L_ProductID2_Text = "Select a property."
Const L_ProductID_Tooltip = "list of all STRING and NUMERIC properties"
Const L_VariantID_Text = "Product variant unique ID:"
Const L_VariantID_Tooltip = "list of all STRING and NUMERIC properties"
Const L_VariantPrice_Text = "Variant price"
Const L_VariantPrice_Tooltip = "Variant price"
Const L_CategoryProperties_Text = "&lt;U&gt;C&lt;/U&gt;ategory Properties"
Const L_CategoryProperties_Tooltip = "c"
Const L_Price_Text = "Price:"
Const L_Price_Tooltip = "price"
Const L_DefaultPrice_Text = "Default price:"
Const L_DefaultPrice_Tooltip = "default price"
Const L_VendorID_Text = "Vendor ID:"
Const L_VendorID_Tooltip = " vendor ID"
Const L_PricingCategory_Text = "Pricing category:"
Const L_PricingCategory_Tooltip = "pricing category"
Const L_Updated_Text = "Updated"
Const L_Updated_Tooltip = "updated"
Const L_UpdatedBy_Text = "Updated by"
Const L_UpdatedBy_Tooltip = "updated by"
Const L_CategoryRelationships_Text = "Category &lt;U&gt;R&lt;/U&gt;elationships"
Const L_CategoryRelationships_Tooltip = "r"
Const L_Searchable_Text = "Searchable:"
Const L_Searchable_Tooltip = "Searchable"

' T-e-x-t  &  A-c-c-e-l-e-r-a-t-o-r
Const L_AssignedCategories_Text = "&lt;U&gt;A&lt;/U&gt;ssigned Categories"
Const L_AssignedCategories_Accelerator = "a"
Const L_ProductProperties_Text = "&lt;U&gt;P&lt;/U&gt;roduct Properties"
Const L_ProductProperties_Accelerator = "p"
Const L_ProductVariants_Text = "Product &lt;U&gt;V&lt;/U&gt;ariants"
Const L_ProductVariants_Accelerator  = "v"
Const L_FamilyRelationships_Text = "Product Family &lt;U&gt;R&lt;/U&gt;elationships"
Const L_FamilyRelationships_Accelerator = "r"
Const L_ProductRelationships_Text = "Product &lt;U&gt;R&lt;/U&gt;elationships"
Const L_ProductRelationships_Accelerator = "r"
Const L_Categories_Text = "C&lt;U&gt;a&lt;/U&gt;tegories"
Const L_Categories_Accelerator = "a"
Const L_Products_Text = "&lt;U&gt;P&lt;/U&gt;roducts"
Const L_Products_Accelerator = "p"
Const L_CustomPricing_Text = "Custom &lt;U&gt;P&lt;/U&gt;ricing"
Const L_CustomPricing_Accelerator = "p"
Const L_CatalogProperties_Text = "&lt;U&gt;C&lt;/U&gt;atalog Properties"
Const L_CatalogProperties_Accelerator = "c"
Const L_CustomCatalogProperties_Text = "&lt;U&gt;C&lt;/U&gt;ustom Catalog Properties"
Const L_CustomCatalogProperties_Accelerator = "c"
Const L_Images_Text = "&lt;U&gt;I&lt;/U&gt;mages"
Const L_Images_Accelerator = "i"

' E-r-r-o-r   M-e-s-s-a-g-e
Const L_Item_ErrorMessage = "You must select a valid item."
Const L_Relationship_ErrorMessage = "You must select a relationship type."
Const L_ErrorCreatingCatalog_ErrorMessage = "An error occurred while creating Catalog <B>%1</B>."
Const L_ErrorDeletingCatalog_ErrorMessage = "An error occurred while deleting Catalog  <B>%1</B>."
Const L_ErrorCreatingCustomCatalog_ErrorMessage = "An error occurred while creating Custom Catalog <B>%1</B>."
Const L_ErrorImportingCatalog_ErrorMessage = "An error occurred while importing Catalog <B>%1</B>."
Const L_ErrorExportingCatalog_ErrorMessage = "An error occurred while exporting Catalog <B>%1</B>."
Const L_ErrorSavingProductProperties_ErrorMessage = "An error occurred while saving Product Properties."
Const L_TextField1_ErrorMessage = "Validation error in text.  The number of characters should be in the range  [%1, %2]."
Const L_TextField2_ErrorMessage = "Validation error in text.  The text cannot contain '&lt;' or '&gt;', and the number of characters should be in the range  [%1, %2]."
Const L_TextField3_ErrorMessage = "Validation error in text.  The text cannot contain '&lt;', '&gt;' or '&amp;', and the number of characters should be in the range  [%1, %2]."
Const L_DateField_ErrorMessage = "The date must be in the following format mm/dd/yyyy. The values should be between  %1 and %2."
Const L_ValidationErrorInCatalogName_ErrorMessage = "The text contains characters that are not valid.  The text cannot contain '&lt;', '&gt;', '[', ']' or apostrophe."
Const L_ErrorUpdatingPricingCategory_ErrorMessage = "An error occured while updating pricing category."

' M-e-s-s-a-g-e
Const L_Note_Message = "Note: "
Const L_ExportCSV_Message = "The Exported CSV file will not contain category structure information in the product catalog."
Const L_StartImport_Message = "Catalog Import process has been started successfully."
Const L_StartExport_Message = "Catalog Export process has been started successfully."

' S-t-y-l-e
Const L_Button_Style = "width:7em"
Const L_DialogButton_Style = "width:6em"
Const L_PricingButton_Style = "width:8em"

%>


<SCRIPT LANGUAGE="VBScript">

' S-t-a-t-u-s  B-a-r
Const L_Selected_StatusBar = "%1 selected"

' D-i-a-l-o-g   T-i-t-l-e
Const L_Error_DialogTitle = "Error"
Const L_DeleteProduct_DialogTitle = "Confirm Delete Product"
Const L_DeleteProducts_DialogTitle = "Confirm Delete Products"
Const L_DeleteCategory_DialogTitle = "Confirm Delete Category"

' T-e-x-t
Const L_None_Text = "<None>"
Const L_NoParentCategory_Text = "(None)"

Const L_Float_Text = "Float"
Const L_String_Text	= "String"
Const L_Number_Text = "Number"
Const L_FileName_Text = "File Name"
Const L_Enumerated_Text = "Enumerated"
Const L_Currency_Text = "Money/Currency"
Const L_DateTime_Text = "Date, Time, Date-Time"

Const L_BizDesk_Text = "BizDesk"
Const L_Product_Text = "Product"
Const L_Category_Text = "Category"
Const L_Delete_Text = "Delete %1?"
Const L_ConfirmDelete_Text = "Delete?"
Const L_NoneAvailable_Text = "None available"
Const L_BizDeskWarning_Text = "BizDesk Warning"
Const L_SaveCatalog_Text = "Save catalog before exiting?"
Const L_SaveProduct_Text = "Save product before exiting?"
Const L_SaveCategory_Text = "Save category before exiting?"
Const L_DeleteProduct_Text = "Are you sure you want to delete this product?"
Const L_DeleteProducts_Text = "Are you sure you want to delete these %1 products?"
Const L_DeleteCategory_Text = "Are you sure you want to delete this category?"
Const L_IsAncestor_Text = "The selected category is an ancestor of the current category."
Const L_IsDescendant_Text = "The selected category is a descendant of the current category."
Const L_EnterKeywords_Text = "Enter keywords or click <Find Now> to show all products."
Const L_Valid_Text = "Cannot save until all required fields have values and invalid fields are corrected."
Const L_Required_Text = "Cannot save until all required fields have values and invalid fields are corrected."
Const L_ForceUpdate_Text = "The record you are saving has already been modified by another user. Do you want to continue the save?"

' M-e-s-s-a-g-e
Const L_RefreshingCatalogs_Message = "Refreshing Catalogs"
Const L_PositiveValue_Message = "Value must be a positive number."
Const L_UniqueVariantID_Message = "The %1 value has to be unique."
Const L_RefreshingCatalogsWait_Message = "Refreshing Catalogs. Please wait ..."
Const L_AlreadyAssigned_Message = "Product is already assigned to this category!"
Const L_RefreshingCatalogCaches_Message = "Refreshing Catalog Caches. Please wait ..."
Const L_LocalPath_Message = "Please provide a local path for the catalog to be imported."

' E-r-r-o-r  M-e-s-s-a-g-e
Const L_QueryBuilder_ErrorMessage = "Unable to load Query Builder."
Const L_ErrorRefreshingCatalogCache_ErrorMessage = "An error occurred while refreshing the catalog cache for:<BR><BR><B>%1</B>."
Const L_ErrorRefreshingCatalogs_ErrorMessage = "Error Refreshing Catalogs"

</SCRIPT>

<% End If %>