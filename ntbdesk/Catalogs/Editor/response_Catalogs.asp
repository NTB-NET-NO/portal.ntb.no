<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../../include/HTTPXMLUtil.asp' -->
<%
'' --------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright (c) 1996-2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			response_Catalogs.asp
''
''	Description:	Response page for Catalogs list.
''
'' --------------------------------------------------------------------

	Const PAGE_TYPE = "RESPONSE"
	' -------------------------------
	On Error Resume Next

'' --------------------------------------------------------------------
''
''	CONST Declarations
''
'' --------------------------------------------------------------------
	Const PAGE_SIZE = 20

'' --------------------------------------------------------------------
''
''	PUBLIC Declarations
''
'' --------------------------------------------------------------------
	Public g_oCatMgr	' CatalogManager object
	Public g_dRequest	' Request object

	Public g_nPage		' Page Number
	Public g_nCatalogs	' Count of catalog records returned

	Set g_dRequest = dGetRequestXMLAsDict()
%>

<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->

<%= sGetXML %>
<%

'' -------------------------------------------------------------------
''
''	Function:		sGetXML
''
''	Description:	Returns XML data corresponding to action in List
''					Catalogs page.
''
''	Arguments:		none
''
''	Returns:		XML/String
''
'' -------------------------------------------------------------------
Function sGetXML ()

	Dim i
	Dim iLast
	Dim sCatalogName

	Dim rsCatalogs		' the recordset to hold the list of catalogs
	Dim rsCustomCatalogs' the recordset to hold the list of all custom catalogs

	On Error Resume Next
	'----- Generate XML - START
	InitializeCatalogObject

	If (Not g_oCatMgr Is Nothing) Then
		g_nPage   = CInt (g_dRequest("page"))
		iLast = PAGE_SIZE * g_nPage
		g_nCatalogs = iLast - PAGE_SIZE
		'----- Search for Catalogs
		Set rsCatalogs = g_oCatMgr.Catalogs
		'' --- Skip first g_nCatalogs
		Do Until (rsCatalogs.EOF = True) Or (i = g_nCatalogs)
			If (Not rsCatalogs.Fields("CustomCatalog")) Then i = i + 1
			rsCatalogs.MoveNext
		Loop

		Set rsCustomCatalogs = g_oCatMgr.CustomCatalogs
		Do Until (rsCatalogs.EOF = True) Or (g_nCatalogs >= iLast)
			If (Not rsCatalogs.Fields("CustomCatalog")) Then
				sCatalogName = rsCatalogs("CatalogName")
				sGetXML = sGetXML & _
							"<record>" & _
								"<name>" & Server.HTMLEncode(sCatalogName) & "</name>" & _
								"<node_type>catalog</node_type>" & _
								sGetDetailXML(sCatalogName, rsCustomCatalogs) & _
							"</record>"

				g_nCatalogs = g_nCatalogs + 1
			End If

			rsCatalogs.MoveNext
		Loop
		Do Until rsCatalogs.EOF
			If (Not rsCatalogs.Fields("CustomCatalog")) Then g_nCatalogs = g_nCatalogs + 1
			rsCatalogs.MoveNext
		Loop

		rsCatalogs.Close : rsCustomCatalogs.Close
		Set rsCatalogs = Nothing : Set rsCustomCatalogs = Nothing
		Set g_oCatMgr = Nothing

		If (Err.Number = 0) Then
			sGetXML = "<document recordcount='" & g_nCatalogs & "'>" & sGetXML & "</document>"
		Else
			sGetXML = setXMLError (L_ErrorsInPage_ErrorMessage, sGetScriptError(Err))
		End If
	Else
		sGetXML = setXMLError (L_BadConnectionString_ErrorMessage, "")
	End If
	'----- Generate XML - STOP
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetDetailXML
''
''	Description:	Returns the list of custom catalogs belonging to a
''					base catalog as XML data.
''
''	Arguments:		sCatalogName - name of current Catalog,
''					rsCustomCatalogs - ADO recordset containing the list 
''									   the list of all Custom Catalogs 
''									   in Catalog System.
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function sGetDetailXML (ByRef sCatalogName, ByRef rsCustomCatalogs)

	On Error Resume Next

	If (True = rsCustomCatalogs.EOF) Then Exit Function
	While Not rsCustomCatalogs.EOF
		If (rsCustomCatalogs("CatalogName") = sCatalogName) Then
			sGetDetailXML = sGetDetailXML & _
				"<record><name>" & Server.HTMLEncode(rsCustomCatalogs("CustomCatalogName").Value) & "</name>" & _
				"<node_type>custom_catalog</node_type></record>"
		End If

		rsCustomCatalogs.MoveNext
	Wend

	rsCustomCatalogs.MoveFirst
End Function

'' -------------------------------------------------------------------
''
''	Function:		InitializeCatalogObject
''
''	Description:	Creates and initializes a global Catalog 
''					Manager Object.
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeCatalogObject ()

	Dim sConnString

	On Error Resume Next
	Set g_oCatMgr = Nothing

	If (True = IsObject(Session("CatalogManager"))) Then
		If Not (Session("CatalogManager") Is Nothing) Then
			Dim rs
			Set g_oCatMgr = Session("CatalogManager")
			Set rs = g_oCatMgr.Catalogs
			If (Err.Number <> 0) Then
				Set g_oCatMgr = Nothing
				Set Session("CatalogManager") = Nothing
				Err.Clear
				Exit Sub
			End If
		End If
	End If

	If (g_oCatMgr Is Nothing) Then
		sConnString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")

		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (sConnString, True)
		If (Err.Number <> 0) Then
			Set g_oCatMgr = Nothing
			Err.Clear
		Else
			Set Session("CatalogManager") = g_oCatMgr
		End If
	End If
End Sub

'' -------------------------------------------------------------------
''
Function setXMLError (ByRef sSource, ByRef sText)

	setXMLError = "<document recordcount='0'>" & "<ERROR ID='1' SOURCE='" & Server.HTMLEncode(sSource) & "'>" & Server.HTMLEncode(sText) & "</ERROR></document>"
	Err.Clear
End Function

%>