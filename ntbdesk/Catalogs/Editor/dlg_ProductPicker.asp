<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
'' -------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright (c) 1996-2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			dlg_ProductPicker.ASP
''
''	Description:	Product Picker dialog. 
''					Used to select a product/category in:
''
''						- Category/Product Editors,
''						- Marketing/Auctions Areas.
''
'' -------------------------------------------------------------------

	Const PAGE_TYPE = "DIALOG"
	'----------------------------
	Public g_sSelected
	Public g_aOptionInfo()

	Public g_aProperties()		' Property names for which DisplayInProductsList = TRUE
	Public g_aPropertiesTypes() ' Corresponding Property Types
%>
<!--#INCLUDE FILE='../../include/DialogUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<TITLE><%= L_ProductPicker_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
	BODY
	{
		PADDING:15px;
		MARGIN:0;
	}
	BUTTON, INPUT
	{
		WIDTH: 8em
	}
</STYLE>
<SCRIPT LANGUAGE="VBScript">

	option explicit

	public g_sCtName
	public g_sProductID
	public g_sVariantID
	public g_sSelectionType
	public g_sProductIDType

	public g_oCategory
	public g_oSelection			' Current selection in searchByCategory
	public g_oSelection2		' Current selection in searchByProperties

	public g_nPageID
	public g_nSearchPageID

	public g_aOptionInfo

	sub window_onLoad ()

		g_sCtName = "<%= g_sCtName %>"
		g_sProductID = "<%= sReplaceSpaces (g_sProductID) %>"
		g_sVariantID = "<%= sReplaceSpaces (g_sVariantID) %>"
		g_sProductIDType = "<%= g_sProductIDType %>"
		g_aOptionInfo = Split (info.value, SEPARATOR2)

		g_nPageID = 1
		g_nSearchPageID = 1
		set g_oCategory = Nothing
		set g_oSelection = Nothing
		set g_oSelection2 = Nothing
	end sub

	' #############################################################
	' TreeView Event Handlers
	'
	sub onTVItemSelect ()

		dim sCategoryName
		
		sCategoryName = sGetCategoryFromEvent()
		products_header.innerHTML = "<%= L_Products2_Text %>" & "<B>" & sCategoryName & "</B>"
		SetCurrentSelection sCategoryName, "category"
	end sub

	sub onTVItemUnselect ()

		ClearCurrentSelection
	end sub

	sub onTVItemOpen ()

		with tvform
			.type.value = "expand"
			.category.value = sGetCategoryFromEvent
		end with
	end sub

	sub SetCurrentSelection (ByRef sText, ByRef sType)

		dim sPrevSelection

		g_sSelectionType = sType
		with document.all
			sPrevSelection = .sel.value
			.sel.value = sText
		end with

		if (g_sSelectionType = "category") and (sPrevSelection <> sText) then
			set g_oCategory = window.event.XMLItem

			with newpageform
				.category.value = sText
				.type.value = "newPage"
				.page_id.value = 1
			end with

			g_nPageID = 1
			PrListSheet.reload "newcategory"
		end if
	end sub

	sub ClearCurrentSelection ()

		document.all("sel").value = sGetCurrentCategoryName
		if (not g_oCategory is Nothing) then g_sSelectionType = "category"
	end sub

	' #############################################################
	' ONCHANGE Event Handler for SEARCH_METHOD element
	'
	sub onChangeSearchMethod ()

		select case	search_method.value
		case "bycatalog"
			tblFindBy.style.display = "none"
			tblBrowseCatalog.style.display = "inline"
		case "bykeywords"
			tblBrowseCatalog.style.display = "none"
			tblFindBy.style.display = "inline"
			divSearchByProperties.style.display = "none"
			divSearchByKey.style.display = "inline"
		case "byproperties"
			tblBrowseCatalog.style.display = "none"
			tblFindBy.style.display = "inline"
			divSearchByKey.style.display = "none"
			divSearchByProperties.style.display = "inline"
		end select
	end sub

	' #############################################################
	' ONERROR Event Handler for QueryBuilder instance
	'
	sub onErrorQB ()

		alert L_QueryBuilder_ErrorMessage

		call divQueryBldr.setExprBody (Null)
		divQueryBldr.deactivate
	end sub

	' #############################################################
	' ONCLICK Event Handler for FindNow Button
	'
	sub onFindNow ()

		with newpageform
			.type.value = "newSearch"
			.page_id.value = 1
			.category.value = ""
			if (search_method.value = "bykeywords") then
				.keywords.value = keywords.value
				.newSearchXML.value = ""
			else'if (search_method.value = "byproperties") then
				.keywords.value = ""
				.newSearchXML.value = divQueryBldr.getExprBody()
			end if
		end with

		PrSearchListSheet.reload "search"
	end sub

	' #############################################################
	' Event Handlers for Products ListSheet
	'
	sub onRowSelect ()

		' Unselect any selection based on PrSearchListSheet
		PrSearchListSheet.unselectAllRows
		set g_oSelection2 = Nothing
		' setup global selection
		set g_oSelection = window.event.XMLrecord
		with g_oSelection
			SetCurrentSelection .selectSingleNode("./" & g_sProductID).text, "product"
		end with
	end sub

	sub onRowUnselect ()

		ClearCurrentSelection
	end sub

	sub onNewPage ()

		dim nPage

		nPage = window.event.page
		newpageform.page_id.value = nPage
		newpageform.type.value = "newPage"
		g_nPageID = nPage

		if (g_sSelectionType = "product") then
			ClearCurrentSelection
		end if
	end sub

	' #############################################################
	' Event Handlers for SEARCH Products ListSheet
	'
	sub onRowSelect2 ()

		dim sProdName

		' unselect any selection based on PrListSheet
		PrListSheet.unselectAllRows
		set g_oSelection = Nothing
		' setup the global selection
		set g_oSelection2 = window.event.XMLrecord
		sProdName = g_oSelection2.selectSingleNode("./" & g_sProductID).text
		document.all("sel").value = sProdName
	end sub

	sub onRowUnselect2 ()

		dim sProdName

		set g_oSelection2 = Nothing
		document.all("sel").value = ""
	end sub

	sub onNewPage2 ()

		dim nPage

		nPage = window.event.page
		g_nSearchPageID = nPage

		with newpageform
			.page_id.value = nPage
			.type.value = "newPage"
			.category.value = ""
		end with

		if (True = IsObject (g_oSelection2)) then
			set g_oSelection2 = Nothing
		end if
	end sub

	' #############################################################
	' ONCHANGE Event Handler for SELECT CATALOG element
	' (disabled if ProductPicker is invoked with a specific Catalog Name)
	'
	sub onChangeCatalog ()

		g_sCtName = catalogname.selected.value
		call getCatalogInfo ()

		' reload Category TreeView
		with tvform
			.category.value = ""
			.type.value = "newpage"
			.catalog.value = g_sCtName
		end with
		call CatTreeView.reload ("newpage")

		' reload Products LisSheets
		with newpageform
			.type.value = "newpage"
			.page_id.value = 1
			.catalog.value = g_sCtName
			.category.value = ""
			.keywords.value = ""
			.newSearchXML.value = ""
		end with

		call PrListSheet.reload ("newpage")
		call PrSearchListSheet.reload ("newpage")

		with document.all
			' clean current global values
			.sel.value = ""
			call .divQueryBldr.setExprBody(Null)
			call .divQueryBldr.Deactivate
			call .divQueryBldr.Activate(True)
		end with

		g_nPageID = 1
		g_nSearchPageID = 1
		g_sSelectionType = ""
		set g_oCategory= Nothing
		set g_oSelection = Nothing
		set g_oSelection2 = Nothing
	end sub

	sub getCatalogInfo ()
		
		dim aInfo
		dim iIndex

		for iIndex = 0 to UBound(g_aOptionInfo)		
			aInfo = Split (g_aOptionInfo(iIndex), SEPARATOR1)
			if (g_sCtName = aInfo(0)) then exit for
		next		

		g_sProductID = aInfo(1)
		g_sVariantID = aInfo(2)
		g_sProductIDType = aInfo(3)
	end sub
	' #############################################################
	' ONCLICK Event Handlers for OK/Cancel/HELP buttons
	'
	sub onOK ()

		dim sMode
		dim sMethod
		dim sClassType
		dim sPropertyName
		dim sPropertyValue
		dim sPropertyName2
		dim sPropertyValue2

		if (sel.value = "") then
			onCancel
			exit sub
		end if

		' retrieve current search_method name
		sMethod = search_method.value

		sMode = "<%= g_sMode %>"
		select case	sMethod
		case "bycatalog"
			select case g_sSelectionType
			case "category"
				sPropertyName = "CategoryName"
				sPropertyValue = sel.value

			case "product"
				call GetCurrentProduct(sPropertyName, sPropertyValue, _
									   g_sProductID, g_oSelection)
				if (sMode = "AUCTIONS") then GetAuctionsParams sPropertyName2, sPropertyValue2, sClassType, g_oSelection
			end select

		case "bykeywords", "byproperties"
			call GetCurrentProduct(sPropertyName, sPropertyValue, _
								   g_sProductID, g_oSelection2)
			if (sMode = "AUCTIONS") then GetAuctionsParams sPropertyName2, sPropertyValue2, sClassType, g_oSelection2
		end select

		select case sMode
		case "CATALOGS"
			' C A T A L O G S
			window.returnValue = g_sCtName & SEPARATOR1 & _
							 sPropertyName & SEPARATOR1 & _
							 sPropertyValue
		case "CAMPAIGNS"
			' C A M P A I G N S
			window.returnValue = g_sCtName & "." & sPropertyName & " = " & sPropertyValue & _
								 SEPARATOR1 & g_sProductIDType
		case "AUCTIONS"
			' A U C T I O N S
			window.returnValue = g_sCtName & SEPARATOR1 & _
							 sPropertyName & SEPARATOR1 & sPropertyValue & SEPARATOR1 & _
							 sPropertyName2 & SEPARATOR1 & sPropertyValue2 & SEPARATOR1 & _
							 sClassType
		end select

		window.close
	end sub

	' #############################################################
	'
	function GetCurrentProduct (ByRef sPropertyName, ByRef sPropertyValue, _
								ByRef sProperty, ByRef oSelection)

		sPropertyName = sProperty
		If Not (oSelection Is Nothing) Then _
			sPropertyValue = oSelection.selectSingleNode ("./" & sProperty).text
	end function

	' #############################################################
	'
	sub GetAuctionsParams (ByRef sPropertyName2, ByRef sPropertyValue2, _
						   ByRef sClassType, ByRef oSelection)

		call GetCurrentProduct (sPropertyName2, sClassType, _
								"i_ClassType", oSelection)
		call GetCurrentProduct (sPropertyName2, sPropertyValue2, _
								g_sVariantID, oSelection)
	end sub

	sub onCancel ()

		window.returnValue = ""
		window.close
	end sub

</SCRIPT>
</HEAD>

<BODY LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then btnCancel.click">

<xml id='tvData'>
	<document skip='yes'>
<%= g_sTVData %>
	<operations hidden='yes'>
		<newpage formid='tvform' />
		<expand formid='tvform' />
	</operations>
	</document>
</xml>

<xml id='lsData'>
	<%= EMPTY_DOCUMENT_XML %>
</xml>

<xml id='lsMeta'>
	<listsheet>
	    <global selectall='no' curpage='<%= g_nPageID %>' pagesize='<%= PAGE_SIZE %>'
			recordcount='<%= g_nItems1 %>' selection='single' sort='no'/>
	    <columns>
<%= g_sMeta %>
	    </columns>
   	    <operations>
			<newpage formid='newpageform' />
			<newcategory formid='newpageform' />
			<newproduct formid='newpageform' />
			<delete formid='newpageform' />
		</operations>
	</listsheet>
</xml>

<xml id='lsSearchData'>
	<%= EMPTY_DOCUMENT_XML %>
</xml>

<xml id='lsSearchMeta'>
	<listsheet>
	    <global selectall='no' curpage='<%= g_nSearchPageID %>' pagesize='<%= PAGE_SIZE %>' 
			recordcount='<%= g_nItems2 %>' selection='single' sort='no'/>
	    <columns>
<%= g_sMeta %>
	    </columns>
   	    <operations>
			<newpage formid='newpageform' />
			<newproduct formid='newpageform' />
			<search formid='newpageform' />
			<delete  formid='newpageform' />
		</operations>
	</listsheet>
</xml>

<XML ID="xmlisConfig">
	<EBCONFIG>
		<ADVANCED />
		<DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
		<CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
		<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
		<DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
		<EXPR-ASP>../../exprarch</EXPR-ASP>
		<HELPTOPIC><%=sRelURL2AbsURL("../../docs/default.asp?helptopic=cs_bd_campaigns_ZHMX.htm")%></HELPTOPIC>
		<SITE-TERMS URL='QBEnumeratedValues.asp' />
		<TYPESOPS>QBTypesOps.xml</TYPESOPS>
		<!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
		<PROFILE-LIST>
			<PROFILE SRC='QBProperties.asp' />
		</PROFILE-LIST>
  </EBCONFIG>
</XML>

<xml id='efMeta'>
	<editfield>
		<select id="search_method" default="bycatalog" onchange="onChangeSearchMethod()">
			<select id="search_method">
				<option value="bycatalog"><%= L_ByCatalog_Text %></option>
				<option value="bykeywords"><%= L_ByKeyword_Text %></option>
				<option value="byproperties"><%= L_ByProperties_Text %></option>
			</select>
		</select>
		<select id="catalogname" onchange="onChangeCatalog()" 
			 <% If (g_sMode = "CATALOGS") Then %>disabled='yes'<% End If %>>
			<select id="catalogname">
				<%= sCatalogsAsOptions(g_sCtName) %>
			</select>
		</select>
		<text id="keywords" maxlen="100">
			<prompt><%= L_KeywordsPrompt_Text %></prompt>
		</text>
		<text id="sel" disabled="yes"/>
	</editfield>
</xml>
<xml id='efData'>
	<document>
		<record>
			<catalogname><%= g_sSelected %></catalogname>
			<keywords><%= g_sKeywords %></keywords>
			<sel><%= g_sSelection %></sel>
		</record>
	</document>
</xml>

<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0" FRAME="0">
	<TR>
	<TD>
		<LABEL FOR="search_method"><B><%= L_SearchMethod_Text %></B></LABEL>
	</TD>
	<TD>
		<span STYLE="width:170px">	
			<DIV ID='search_method' CLASS='editField'
				MetaXML='efMeta'
				DataXML='efData'></DIV></span>
	</TD>
	<TD STYLE="width:100px"></TD>
	<TD>
		<LABEL FOR="catalog"><B><%= L_Catalog_Text %></B></LABEL>
	</TD>
	<TD>
		<span STYLE="width:170px">	
			<DIV ID='catalogname' CLASS='editField'
				MetaXML='efMeta'
				DataXML='efData'></DIV></span>
		<INPUT TYPE="HIDDEN" NAME="info" VALUE="<%= Join (g_aOptionInfo, SEPARATOR2) %>"></INPUT>
	</TD>
	</TR>
</TABLE>
<HR STYLE="width:100%; height:3px">
<TABLE ID="tblBrowseCatalog" STYLE="height:71%" CELLSPACING="0" CELLPADDING="0" BORDER="0" FRAME="0">
	<TR CELLSPACING="0" CELLPADDING="0" BORDER="0">
		<TD VALIGN="TOP"><%= L_Categories_HTMLText %></TD>
		<TD VALIGN="TOP" ID="products_header"><%= L_Products2_Text %></TD>
	</TR>
	<TR>
		<TD VALIGN="TOP" STYLE="height:100%">
			<DIV STYLE='height:305px; width:200px; overflow:auto; background:white; border:thin inset silver'>
				<DIV ID='CatTreeView'
					CLASS='treeView '
					DataXML='tvData'
					LANGUAGE='VBScript'
					OnItemSelect='onTVItemSelect'
					OnItemUnselect='onTVItemUnselect'
					OnItemOpen='onTVItemOpen'><%= L_LoadingWidget_Text %>
				</DIV>
			</DIV>
		</TD>
		<TD VALIGN="TOP">
			<DIV ID='PrListSheet'
				STYLE='height:282px; width:395px'
				CLASS='listSheet'
				DataXML='lsData'
				MetaXML='lsMeta'
				LANGUAGE='VBScript'
				OnRowSelect='onRowSelect'
				OnRowUnselect='onRowUnselect'
				OnNewPage='onNewPage'><%= L_LoadingWidget_Text %>
			</DIV>
		</TD>
	</TR>
</TABLE>
<TABLE ID="tblFindBy" STYLE="display:none; height:71%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
	<TR>
		<TD VALIGN="TOP">
			<DIV ID="divSearchByKey" STYLE="height:145px;margin-left:15px">
				<%= L_Keywords_Text %><BR>
				<span STYLE="width:509px">	
					<DIV ID='keywords' CLASS='editField'
						MetaXML='efMeta'
						DataXML='efData'></DIV></span>
			</DIV>
			<DIV ID="divSearchByProperties" STYLE="height:145px">
				<TABLE CELLSPACING="0" CELLPADDING="0">
					<TR>
					<TD STYLE="width:4px"></TD>
					<TD>
						<DIV ID="divQueryBldr" NAME="divQueryBldr" CLASS="clsQueryBldr" STYLE="width:476px;height:100px"
							STYLE="behavior:url(/widgets/exprbldrhtc/QueryBuilder.htc);" 
							XMLCfgID="xmlisConfig" AutoActivate='true' ONERROR="onErrorQB()"><%= L_LoadingQB_Text %>
						</DIV>
					</TD>
				</TABLE>
			</DIV>
		</TD>
		<TD ID="find_btn" VALIGN="TOP" STYLE='PADDING-TOP:10px;'>
			<BUTTON ID="find" CLASS="bdbutton" ONCLICK="onFindNow()" <% If (g_sCtName = "") Then %>DISABLED<% End If %>><%= L_FindNow_Button %></BUTTON>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN="2">
			<DIV STYLE="margin-left:20px"><%= L_Products3_Text %></DIV>
			<DIV ID='PrSearchListSheet' STYLE="width:605px; height:125px"
				CLASS='listSheet'
				DataXML='lsSearchData'
				MetaXML='lsSearchMeta'
				LANGUAGE='VBScript'
				OnRowSelect='onRowSelect2'
				OnRowUnselect='onRowUnselect2'
				OnNewPage='onNewPage2'><%= L_LoadingWidget_Text %>
			</DIV>
		</TD>
	</TR>
</TABLE>

<HR STYLE="width:100%; height:3px"><BR>

<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0">
	<TR>
		<TD>
			<LABEL FOR="sel" STYLE="font-size:10pt"><%= L_Selection_Text %></LABEL>
		</TD>
		<TD>
			<span STYLE="width:560px">	
				<DIV ID='sel' CLASS='editField'
					MetaXML='efMeta'
					DataXML='efData'></DIV></span>
		</TD>
	</TR>
</TABLE>
<BR>
<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0">
	<TD STYLE="width:100%"></TD>
	<TD>
		<BUTTON CLASS="bdbutton" ID="btnOK" ONCLICK="onOK()" <% If (g_sCtName = "") Then %>DISABLED<% End If %>><%= L_OK_Button %></BUTTON>
		<BUTTON CLASS="bdbutton" ID="btnCancel" ONCLICK="onCancel()"><%= L_Cancel_Button %></BUTTON>
		<BUTTON CLASS="bdbutton" ID="btnHelp" ONCLICK="openHelp 'cs_ft_catalogs_OOAT.htm'"><%= L_Help_Button %></BUTTON>
	</TD>
</TABLE>

<FORM ID="newpageform" ACTION='response_Products.asp'>
	<INPUT TYPE="Hidden" NAME="mode" VALUE="<%= g_sMode %>">
	<INPUT TYPE="Hidden" NAME="page_id">
	<INPUT TYPE="Hidden" NAME="type">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sCtName %>">
	<INPUT TYPE="Hidden" NAME="category">
	<INPUT TYPE="Hidden" NAME="keywords">
	<INPUT TYPE="Hidden" NAME="newSearchXML">
	<INPUT TYPE="Hidden" NAME="products">
</FORM>

<FORM ID="tvform" NAME="tvform" ACTION="response_Categories.asp">
	<INPUT TYPE="Hidden" NAME="type">
	<INPUT TYPE="Hidden" NAME="method" VALUE="0">
	<INPUT TYPE="Hidden" NAME="category">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sCtName %>">
</FORM>

</BODY>
</HTML>
<%
	ReleaseGlobalObjects
%>

<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

' ----- PUBLIC Declarations
Public g_sXML
Public g_sMode			' Operating mode: Catalogs|Campaigns|Auctions
Public g_sMeta
Public g_sMethod
Public g_sHeader		' Tab header
Public g_sAction		' String representation of action to take on page
Public g_sTVData
Public g_sCtName		' Catalog Name
Public g_sCategory
Public g_sKeywords
Public g_sSelection
Public g_sFilterXML
Public g_sProductID
Public g_sVariantID
Public g_sActionType	' Type to pass through
Public g_sStatusText	' To be displayed in the Status Bar
Public g_sProperties
Public g_sCategoryName
Public g_sProductIDType

Public g_rsCatalogs		' List of catalogs
Public g_rsProducts		' List of products
Public g_rsProperties	' Property Definitions used by Current Catalog

Public g_nPageID
Public g_nSearchPageID

Public g_nItems1
Public g_nItems2

'' -------------------------------------------------------------------
''
''	Function:		Perform_ServerSide_Processing
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeGlobalVariables
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeGlobalVariables ()

	g_sMode = UCase(Request.QueryString ("mode"))
	If (g_sMode = "CATALOGS") Then
		Dim dServerState
		Set dServerState = Session (CATALOG_PARAMETERS)
			g_sCtName = sNullToEmptyString(dServerState("ct_name"))
		Set dServerState = Nothing
	End If

	On Error Resume Next
	' Retrieve the list of CATALOGS
	' assuming that there is at least one CATALOG available
	Set g_rsCatalogs = g_oCatMgr.Catalogs

	' If ProductPicker invoked from
	' Auctions or Marketing areas
	If (g_sCtName = "") Then
		' Set first CATALOG in the list
		' as currently selected
		If (Not g_rsCatalogs.EOF) Then _
			g_sCtName = g_rsCatalogs.Fields("CatalogName")
	End If

	Set g_oCat = g_oCatMgr.GetCatalog(g_sCtName)
	Set g_rsProperties = g_oCatMgr.Properties (g_sCtName)
	g_sProductID = g_oCat.IdentifyingProductProperty
	g_sVariantID = g_oCat.IdentifyingVariantProperty

	ReDim g_aProperties(-1)
	ReDim g_aPropertiesTypes(-1)
	ReDim g_aOptionInfo(-1)

	GetPropertiesToDisplayInList
	GetListSheetMeta
	GetTVData

	ReleaseRecordset g_rsProperties
	Set g_rsProperties = g_oCatMgr.Properties
	'
	' getPropertyAttribute should be called only after retieving
	' the "g_rsProperties" recordset
	'
	g_sProductIDType = sEnumType(getPropertyAttribute(g_sProductID, "DataType"))
End Sub

'' -------------------------------------------------------------------
''
''	Function:		sCatalogsAsOptions
''
''	Description:	
''
''	Arguments:		sCatalog - CatalogName (current selection)
''
''	Returns:		string (concatenated <OPTION> elements)
''
'' -------------------------------------------------------------------
Function sCatalogsAsOptions (ByRef sCatalog)

	Dim sUID
	Dim sVID
	Dim sName
	Dim sUIDType

	While Not g_rsCatalogs.EOF
		sName = g_rsCatalogs("CatalogName")
		If (sName = sCatalog) Then g_sSelected = sName

		sUID = stringValue (g_rsCatalogs("ProductID").Value, STRING_TYPE)
		sVID = stringValue (g_rsCatalogs("VariantID").Value, STRING_TYPE)
		sUIDType = sEnumType(getPropertyAttribute(sUID, "DataType"))
		sUID = sReplaceSpaces (sUID)
		sVID = sReplaceSpaces (sVID)

		AddArray g_aOptionInfo, _
				 sName & SEPARATOR1 & sUID & SEPARATOR1 & sVID & SEPARATOR1 & sUIDType
		sCatalogsAsOptions = sCatalogsAsOptions & _
							 "<option value=""" & sName & """>" & _
							 Server.HTMLEncode(sName) & "</option>"

		g_rsCatalogs.MoveNext
	Wend
End Function

'' -------------------------------------------------------------------
''
''	Function:		sEnumType
''
''	Description:	Converts CatalogDataTypeEnum values to their 
''					STRING representation
''
''	Arguments:		iDataType - one of the CatalogDataTypeEnum values
''					
''
''	Returns:		
''
'' -------------------------------------------------------------------
Function sEnumType (ByVal iDataType)

    Select Case iDataType
	Case 0
		sEnumType = "number"
	Case 2
		sEnumType = "float"
	Case 5
		sEnumType = "string"
	Case 6
		sEnumType = "datetime"
	Case 7
		sEnumType = "currency"
	Case 8
		sEnumType = "filename"
	Case 9
		sEnumType = "enumeration"
	End Select
End Function

'' -------------------------------------------------------------------
''
''	Function:		ReleaseGlobalObjects
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ReleaseGlobalObjects ()

	ReleaseRecordset g_rsProducts
	ReleaseRecordset g_rsCatalogs
	ReleaseRecordset g_rsProperties

	Set g_oCat = Nothing
	Set g_oCatMgr = Nothing
End Sub

%>