<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="catEditorSTRINGS.asp" -->
<!--#INCLUDE FILE="../../include/DialogUtil.asp" -->

<%
	Const PAGE_TYPE = "DIALOG"
	Public g_sExportType	' XML | CSV
	' -----------------------------------------
	g_sExportType = Request.QueryString("type")
%>

<HTML>
<HEAD>
<TITLE>
	<% If (g_sExportType = "XML") Then %>
		<%= L_ExportXMLCatalog_DialogTitle %>
	<% Else %>
		<%= L_ExportCSVCatalog_DialogTitle %>
	<% End If %>
</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
	BODY
	{
		PADDING:15px;
		MARGIN:0;
	}
	BUTTON, INPUT
	{
		WIDTH: 6em
	}
</STYLE>

<SCRIPT LANGUAGE='VBScript'>

	option explicit


	sub bdcancel_OnClick()

		window.returnValue = ""
		window.close
	end sub

	sub bdOK_OnClick()

		window.returnValue = file.value
		window.close
	end sub
</SCRIPT>
</HEAD>

<BODY LANGUAGE='VBScript' ONLOAD='file.focus' ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<% If (g_sExportType = "CSV") Then %>
	<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0" FRAME="0">
		<TR VALIGN="TOP">
			<TD>
				<%= L_Note_Message %>
			</TD>
			<TD>
				<%= L_ExportCSV_Message %><BR><BR>
			</TD>
		</TR>
	</TABLE>
<% Else %>
	<BR>
<% End If %>

<xml id='efMeta'>
	<editfield>
	    <text id='file' maxlen='255' subtype='short'>
			<prompt><%= L_EnterFilename_Text %></prompt>
	    </text>
	</editfield>
</xml>
<xml id='efData'>
	<document/>
</xml>

	<TABLE>
		<TR>
			<TD STYLE="width:100px">
				<LABEL FOR="file"><%= L_ExportFile_Text %></LABEL>
			</TD>
			<TD>
			<div STYLE="width:250px">	
				<DIV ID='file' CLASS='editField'
					MetaXML='efMeta'
					DataXML='efData'></DIV></div>
			</TD>
		</TR>
	</TABLE><BR>

	<TABLE WIDTH="100%">
		<TR CELLSPACING="0" CELLPADDING="0" BORDER="0">
		<TD WIDTH="100%"></TD>
			<TD>
				<BUTTON ID='bdOK' CLASS='bdbutton' STYLE='<%= L_DialogButton_Style %>'><%= L_OK_Button %></BUTTON>
			</TD>
			<TD>
				<BUTTON ID='bdcancel' CLASS='bdbutton' STYLE='<%= L_DialogButton_Style %>'><%= L_Cancel_Button %></BUTTON>
			</TD>
			<TD>
				<BUTTON ID='bdhelp' CLASS='bdbutton' STYLE='<%= L_DialogButton_Style %>'
					LANGUAGE='VBScript'
					ONCLICK='openHelp "cs_bd_catalogs_WREJ.htm"'><%= L_Help_Button %></BUTTON>
			</TD>
		</TR>
	</TABLE>

</BODY>
</HTML>

