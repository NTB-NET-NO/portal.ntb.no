<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="catEditorSTRINGS.asp" -->
<!--#INCLUDE FILE="common.asp" -->

<%
	Const PAGE_TYPE = "DIALOG"
'	#############################
	Perform_ServerSide_Processing
'	#############################	
%>
<HTML>
<HEAD>

<TITLE><%= L_NewProduct_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
	BODY
	{
		PADDING:15px;
		MARGIN:0;
	}
	BUTTON, INPUT
	{
		WIDTH: 6em
	}
</STYLE>

<SCRIPT LANGUAGE='VBScript'>

	option explicit

	sub bdcancel_OnClick ()
		window.returnValue = ""
		window.close
	end sub

	sub bdok_OnClick ()
		window.close
	end sub

	sub onSelect()
		window.returnValue = definitions.text
		bdok.disabled = False 
		bdok.focus
	end sub

	sub onUnselect()
		window.returnValue = ""
		bdok.disabled = true 
	end sub

	sub window_onLoad()
		window.returnValue = ""
		<% If (True = g_bDisabled) Then %>bdcancel.focus()<% Else %>bdok.focus()<% End If %>
	end sub

</SCRIPT>
</HEAD>

<BODY LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<TABLE>
	<TR>
		<TD STYLE="width:10px">
		<TD>
			<LABEL FOR='definitions'><%= L_SelectProductType_Text %></LABEL><BR><BR>
			<div ID="definitions"
				CLASS="listBox<% If (True = g_bDisabled) Then %> lbDisabled<%End If%>" STYLE="WIDTH: 258px; HEIGHT: 100px"
				LANGUAGE="vbscript"
				ONSELECT="onSelect"
				ONUNSELECT="onUnselect">
				<%= g_sProductDefinitions %>
			</div>
		</TD>
	</TR>
</TABLE>
<BR>
<TABLE>
	<TR>
		<TD STYLE='width:230px'></TD>
		<TD>
			<BUTTON ID='bdok' CLASS='bdbutton' <% If (True = g_bDisabled) Then %>DISABLED<% End If %>><%= L_OK_Button %></BUTTON>&nbsp;
			<BUTTON ID='bdcancel' CLASS='bdbutton'><%= L_Cancel_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>


<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

' ------ PUBLIC Declarations
Public g_bDisabled
Public g_sProductDefinitions

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	Dim rsDefinitions

	InitializeCatalogObject
	Set rsDefinitions = g_oCatMgr.ProductDefinitions

	If (True = rsDefinitions.EOF) Then
		g_bDisabled = True
		g_sProductDefinitions = "<DIV VALUE=''>" & L_NoneAvailable_Text & "</DIV>" & vbCR
	Else
		g_bDisabled = False
		While Not rsDefinitions.EOF
			g_sProductDefinitions = g_sProductDefinitions & _
					"<DIV VALUE=''>" & rsDefinitions("DefinitionName") & "</DIV>" & vbCR

			rsDefinitions.MoveNext
		Wend
	End If

	Set g_oCatMgr = Nothing
End Sub

%>