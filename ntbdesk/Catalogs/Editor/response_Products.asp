<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../../include/HTTPXMLUtil.asp' -->
<%
'' -------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright (c) 1996-2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			response_Products.ASP
''
''	Description:	Products Response page.
''
'' -------------------------------------------------------------------

	Const PAGE_TYPE = "RESPONSE"
	' ---------------------------
	Public g_dRequest
	Public g_DBDefaultLocale
	Public g_DBCurrencyLocale

	Public g_aProperties ()		' Property names for which DisplayInProductsList = TRUE
	Public g_aPropertiesTypes ()' Corresponding Property Types

	g_DBDefaultLocale = Application("MSCSDefaultLocale")
	g_DBCurrencyLocale = Application("MSCSCurrencyLocale")
	Set g_dRequest = dGetRequestXMLAsDict()

	On Error Resume Next
	Perform_ServerSide_Processing
	If (Err.Number <> 0) Then
		g_sXML = setXMLError (L_ErrorsInPage_ErrorMessage, sGetScriptError(Err))
	End If
%>

<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->

<%= g_sXML %>

<%

'' -------------------------------------------------------------------
''
''	PUBLIC Declarations
''
'' -------------------------------------------------------------------

Public g_sXML
Public g_sMode
Public g_sAction
Public g_sCtName
Public g_sCategory
Public g_sKeywords
Public g_sFilterXML
Public g_sProductID			' Catalog Products unique identifier
Public g_sVariantID			' Catalog Variants unique identifier
Public g_sStatusText		' Text to be displayed in the Status Bar
Public g_sCtNameInitString

Public g_rsProducts			' Retrieve products data using Catalog Query Method
Public g_rsProperties		' Property Definitions used by the Current Catalog

Public g_nPage				' Products Page

'' -------------------------------------------------------------------
''
''	Function:		Perform_ServerSide_Processing
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	If (g_oCatMgr Is Nothing) Then
		g_sXML = setXMLError (L_BadConnectionString_ErrorMessage, "")
		Exit Sub
	End If

	InitializeGlobalVariables
	If (Err.Number <> 0) Then Exit Sub

	Perform_Processing_BasedOnAction
	ReleaseGlobalObjects
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeGlobalVariables
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeGlobalVariables ()

	On Error Resume Next

	g_sMode = g_dRequest("mode")
	g_sAction = g_dRequest("type")
	g_sCtName = g_dRequest("catalog")
	g_sCategory = g_dRequest("category")
	g_sKeywords = g_dRequest("keywords")
	g_sFilterXML = g_dRequest("newSearchXML")
	g_sXML = ""

	Select Case g_sAction
	Case "delete"
		g_nPage = 1
	Case Else
		g_nPage = CInt(g_dRequest("page_id"))
	End Select

	Set g_oCat = g_oCatMgr.GetCatalog(g_sCtName)
	Set g_rsProperties = g_oCatMgr.Properties (g_sCtName)
	If (Not g_oCat Is Nothing) Then
		g_sProductID = g_oCat.IdentifyingProductProperty
		g_sVariantID = g_oCat.IdentifyingVariantProperty
	End If

	ReDim g_aProperties(-1)
	ReDim g_aPropertiesTypes(-1)
	GetPropertiesToDisplayInList
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Perform_Processing_BasedOnAction
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_Processing_BasedOnAction ()

	Dim nItems

	If (g_sAction = "delete") Then
		Process_Delete
	End If

	If (g_sCategory <> "") Then
		Call RetrievePageBasedOn_Category (g_nPage, nItems)
	Else
		Call RetrievePageBasedOn_Properties (g_nPage, nItems)
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_Delete	
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_Delete ()

	Dim sProdName
	Dim aProducts

	aProducts = Split(g_dRequest("products"), SEPARATOR1)

	On Error Resume Next
	For Each sProdName In aProducts
		g_oCat.DeleteProduct sProdName
	Next
End Sub

'' -------------------------------------------------------------------
''
''	Function:		
''
''	Description:	
''					
''	Arguments:		
''					
''	Returns:		
''
'' -------------------------------------------------------------------
Sub ReleaseGlobalObjects()

	ReleaseRecordset g_rsProducts
	ReleaseRecordset g_rsProperties

	Set g_oCatMgr = Nothing
	Set g_oCat = Nothing
End Sub

%>