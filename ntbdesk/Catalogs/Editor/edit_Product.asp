<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
'' --------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright (c) 1996-2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			edit_Product.asp
''
''	Description:	Product Editor (Open page).
''
'' --------------------------------------------------------------------

	Const PAGE_TYPE = "EDIT"
	' -------------------------------
	Public g_aIDs()					' Array of IDs containing ProductID/VariantID
	Public g_aPrChar()				' To hold an array of product properties
	Public g_aPvChar()
	Public g_aVariants()			' To hold product variant IDs

	On Error Resume Next
%>

<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->

<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../../include/bizdesk.css' ID='mainstylesheet'>

<SCRIPT LANGUAGE="VBScript">

option explicit

const STATE_VALID = 0
const STATE_INVALID = 1

public sItemName			' Values of a selected XML node
public g_sCtName			' Catalog Name
public g_sPriceCatName		' Pricing category
public g_xmlNodeRecord		' XML Node corresponding to Variant
							' currently being edited in ListEditor

public g_aAddChar()			' An array to hold added characteristics
public g_aRemoveChar()		' An array to hold removed characteristics

public g_nSelections		' # of selected (assigned) categories

sub window_onLoad ()

	dim sAction

	sAction = "<%= g_sAction %>"
	' F O R C E   U P D A T E
	CheckForceUpdate sAction, prForm

	redim g_aAddChar(-1)
	redim g_aRemoveChar(-1)
	set g_xmlNodeRecord = Nothing

	g_nSelections = 0
	g_sCtName =  "<%= g_sCtName %>"
	g_sPriceCatName = prForm("priceCatName").value

	esControl.focus
end sub

</SCRIPT>

<SCRIPT LANGUAGE="VBScript" DEFER>

sub prForm_onSubmit ()

	with prForm
		.added_categories.value = sConvertArrayToString(g_aAddChar)
		.removed_categories.value = sConvertArrayToString(g_aRemoveChar)
		.priceCatName.value = g_sPriceCatName

		if not (document.all("ListEditor1") is Nothing) then
			.family_xml.value = ListEditor1.xmlList.xml
		end if

		if (.type.value  = "") then .type.value = "submitEdit"
		.submit
	end with
end sub

sub saveXMLData ()

	set g_xmlNodeRecord = Nothing
	document.all.family_xml.value = window.event.XMLlist.xml
end sub

sub saveXMLData2 ()

	document.all.relationship_xml.value = window.event.XMLlist.xml
end sub

sub onChangeVariantID ()

	dim xmlNode
	dim xmlNodeRecord
	dim sVariantIDField
	dim sVariantIDValue
	dim sPattern

	sVariantIDField = "<%= sReplaceSpaces(g_sVariantID2) %>"
	sVariantIDValue = ListEditor1.field(sVariantIDField).value
	if (g_xmlNodeRecord is Nothing) then exit sub
	if (sVariantIDValue = "") then exit sub

	sPattern = "//" & sVariantIDField & "[text() = '" & sVariantIDValue & "']"
	set xmlNode = ListEditor1.XMLList.selectSingleNode (sPattern)
	if (xmlNode is Nothing) then exit sub
	set xmlNodeRecord = xmlNode.parentNode
	if (xmlNodeRecord is g_xmlNodeRecord) then exit sub

	MsgBox sFormatString(L_UniqueVariantID_Message, Array("<%= g_sVariantID %>")), 48, L_BizDeskWarning_Text
	ListEditor1.field(sVariantIDField).value = ""
end sub

sub onNewVariant ()
	set g_xmlNodeRecord = window.event.XMLrecord
	ListEditor1.field("variant_price").value = price.value
end sub

sub onEditVariant ()
	set g_xmlNodeRecord = window.event.XMLrecord
end sub

sub onCancelListEditor ()
	set g_xmlNodeRecord = Nothing
end sub

' Scripts for manipulating product categories

sub onTVItemSelect ()

	with window.event.XMLitem
		sItemName = sGetCategoryFromEvent()
	end with

	with document.all
		.add_category.disabled = False
		.remove_category.disabled = True
		.set_category.disabled = True
		.edit_category.disabled = True
		ass_category.unselectAll
	end with
end sub

sub onTVItemUnselect ()

	sItemName = ""
	document.all.add_category.disabled = True
end sub

sub onTVItemOpen ()

	tvform.type.value = "expand"
	tvform.category.value = sGetCategoryFromEvent
end sub

sub onAddCategory ()

	dim bAlreadyAssigned

	dim sText
	dim elOption

	bAlreadyAssigned = False
	sText = sItemName
	for each elOption in ass_category.children
		if (elOption.value = sText) then
			bAlreadyAssigned = True
			alert L_AlreadyAssigned_Message
			exit for
		end if
	next
	if (not bAlreadyAssigned) then
		if not bRemovedFromArray(g_aRemoveChar, sText) then AddArray g_aAddChar, sText
		ass_category.addItem sText, sItemName
		setDirty (L_SaveProduct_Text)
	end if

	treeView.unselectAll
	document.all.add_category.disabled = True
end sub

sub onRemoveCategory ()

	dim sText
	dim elOption

	g_nSelections = 0
	for each elOption in ass_category.children
		if inStr(elOption.className, "selected") then
			sText = elOption.value

			' manipulate add/remove arrays
			if not bRemovedFromArray(g_aAddChar, sText) then AddArray g_aRemoveChar, sText

			' remove from assigned characteristics
			ass_category.remove()
		end if
	next

	with document.all
		' disable remove/edit/set buttons
		.remove_category.disabled = True
		.edit_category.disabled = True
		.set_category.disabled = True
	end with
	setDirty (L_SaveProduct_Text)
end sub

sub onSelect ()

	dim bDisabled

	g_nSelections = g_nSelections + 1
	with document.all
		if (g_nSelections = 1) then
			bDisabled = False
			call .treeView.unselectAll()
			.add_category.disabled = True
		else
			bDisabled = True
		end if

		.remove_category.disabled = False
		.edit_category.disabled = bDisabled
		.set_category.disabled = bDisabled
	end with
end sub

sub onUnSelect ()

	dim bDisabled

	g_nSelections = g_nSelections - 1
	with document.all
		if (g_nSelections = 1) then
			bDisabled = False
		else
			bDisabled = True
			if (g_nSelections = 0) then _
				.remove_category.disabled = True
		end if

		.edit_category.disabled = bDisabled
		.set_category.disabled = bDisabled
	end with
end sub

sub set_category_onClick ()

	with ass_category
		document.all("price_category").value = .text
		g_sPriceCatName = .text
	end with
	document.all.clear_category.disabled = False
	setDirty (L_SaveProduct_Text)
end sub

sub clear_category_onClick ()

	g_sPriceCatName = ""

	with document.all
		.price_category.value = ""
		.clear_category.disabled = True
	end with
	setDirty (L_SaveProduct_Text)
end sub

sub onNewCategory ()

	dim sType

	sType = window.showModalDialog("dlg_AddCategory.asp", , _
				"dialogHeight:250px;dialogWidth:340px;status:no;help:no")
	if (sType <> "") then
		call setEditPageParameters (CATEGORY_PARAMETERS, _
									Array("type", "cg_type", "ct_name"), _
									Array("add", sType, g_sCtName))
		call openEditPage("<%= g_sBizDeskRoot %>catalogs/editor/edit_Category.asp", "onNewCategoryCallback")
	end if
end sub

sub onNewCategoryCallback ()

	tvform.type.value = ""
	document.all("treeView").reload "newpage"
end sub

sub onEditCategory ()

	dim sName

	sName = ass_category.text

	call setEditPageParameters (CATEGORY_PARAMETERS, _
								Array("type", "cg_name", "ct_name"), _
								Array("open", sName, g_sCtName))
	call openEditPage("<%= g_sBizDeskRoot %>catalogs/editor/edit_Category.asp", "onEditCategoryCallback")
end sub

sub onEditCategoryCallback ()

	tvform.type.value = ""
	document.all("treeView").reload "newpage"
end sub

sub onSaveNew ()

	dim sResponse

	sResponse = window.showModalDialog("dlg_AddProduct.asp", , _
		"dialogHeight:270px;dialogWidth:350px;status:no;help:no")
	if (sResponse = "") then
		EnableTask "close"
		EnableTask "save"
		EnableTask "savenew"
	else
		select case prForm.type.value
		case "add", "submitNew"
			prForm.type.value = "createNew"
		case else
			prForm.type.value = "saveNew"
		end select

		prForm.pr_newtype.value = sResponse
		prForm_onSubmit
	end if
end sub

' ##############################################################
' ONBROWSE Event Handler for Relationships DYNAMIC TABLE element
' (Invokes PRODUCT PICKER dialog)
'
sub onBrowse ()

	dim sResponse

	call setEditPageParameters(CATALOG_PARAMETERS, _
							   Array ("ct_name"), Array ("<%= replace(g_sCtName, """", """""") %>"))
	sResponse = window.showModalDialog ("dlg_ProductPicker.asp?mode=CATALOGS", , _
										"dialogHeight:545px;dialogWidth:700px;status:no;help:no")
	dClearServerState (CATALOG_PARAMETERS)

	if (sResponse <> "") then
		dim sType
		dim aParams

		aParams = Split (sResponse, SEPARATOR1)
		if (aParams(1) = "CategoryName") then
			sType = L_Category_Text
		else
			sType = L_Product_Text
		end if

		with DynamicTable
			.field("rel_item").value = aParams(2)
			.field("rel_descr").value = sType
		end with
	end if
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
	End If

	InsertEditTaskBar g_sHeader, g_sStatusText
%>

<xml id='esData'>
	<DOCUMENT>
		<RECORD>
<%= sGetESData %>
			<price><%= g_sPrice %></price>
		</RECORD>
	</DOCUMENT>
</xml>

<xml id='esMeta'>
<editsheets>
	<editsheet>
		<global expanded='yes' downlevel='no'>
			<name><%= L_ProductProperties_Text %></name>
			<key><%= L_ProductProperties_Accelerator %></key>
		</global>
		<fields>
<%

Dim sMin
Dim sMax, sMaxMoney
Dim sRows
Dim sValue
Dim sProperty
Dim sEncodedProperty

Dim iMinLength
Dim iMaxLength
Dim iPropertyType

sMaxMoney = stringValue (MAX_MONEY, CURRENCY_TYPE)

sProperty = g_sProductID
sEncodedProperty = sReplaceSpaces (sProperty)
iPropertyType = getPropertyAttribute (sProperty, "DataType")

Select Case iPropertyType
Case STRING_TYPE
	iMinLength = getPropertyAttribute (sProperty, "MinLength")
	iMaxLength = getPropertyAttribute (sProperty, "MaxLength")
%>
		<text id='<%= sEncodedProperty %>' required='yes' readonly='<% If (g_sActionType = "submitNew") Then %>no<% Else %>yes<% End If %>'
			minlen='<%= iMinLength %>' maxlen='<%= iMaxLength %>'
			subtype='<% If (iMaxLength > 80) Then %>long<% Else %>short<% End If %>'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<error><%= sFormatString(L_TextField3_ErrorMessage, Array(iMinLength, iMaxLength)) %></error>
		</text>
<%
Case INTEGER_TYPE
	sMin = stringValue (getPropertyAttribute (sProperty, "i_MinValue"), INTEGER_TYPE)
	sMax = stringValue (getPropertyAttribute (sProperty, "i_MaxValue"), INTEGER_TYPE)
%>
		<numeric id='<%= sEncodedProperty %>' required='yes' readonly='<% If (g_sActionType = "submitNew") Then %>no<% Else %>yes<% End If %>'
				min='<%= sMin %>' max='<%= sMax %>' subtype='integer'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<format><%= g_sMSCSNumberFormat %></format>
		</numeric>
<%
End Select

For Each sProperty In g_aPrChar
	If (sProperty <> g_sProductID) Then
		sEncodedProperty = sReplaceSpaces(sProperty)
		iPropertyType = getPropertyAttribute(sProperty, "DataType")

		Select Case iPropertyType
		Case STRING_TYPE
			iMinLength = getPropertyAttribute (sProperty, "MinLength")
			iMaxLength = getPropertyAttribute (sProperty, "MaxLength")
%>
		<text id='<%= sEncodedProperty %>' required='no' readonly='no'
				minlen='<%= iMinLength %>' maxlen='<%= iMaxLength %>'
				subtype='<% If (iMaxLength > 80) Then %>long<% Else %>short<% End If %>'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<prompt><%= L_EnterHTMLText_Text %></prompt>
			<charmask>^(\s*\S*\s*)*$</charmask>
			<error><%= sFormatString(L_TextField1_ErrorMessage, Array(iMinLength, iMaxLength)) %></error>
		</text>
<%
		Case FILEPATH_TYPE
			sValue = g_oDict(sProperty)
			If (True = bFileContainsImage(sValue)) Then _
				sRows = sRows & sConvertPropertyToTableRow(sProperty, sValue)
%>
		<text id='<%= sEncodedProperty %>' required='no' readonly='no'
				maxlen='256' subtype='short'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<prompt><%= L_EnterURL_Text %></prompt>
		</text>
<%		Case INTEGER_TYPE
			sMin = stringValue (getPropertyAttribute (sProperty, "i_MinValue"), INTEGER_TYPE)
			sMax = stringValue (getPropertyAttribute (sProperty, "i_MaxValue"), INTEGER_TYPE)
%>
		<numeric id='<%= sEncodedProperty %>' required='no' readonly='no'
			min='<%= sMin %>' max='<%= sMax %>' subtype='integer'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<format><%= g_sMSCSNumberFormat %></format>
		</numeric>
<%		Case FLOAT_TYPE
			sMin = stringValue (getPropertyAttribute (sProperty, "fp_MinValue"), FLOAT_TYPE)
			sMax = stringValue (getPropertyAttribute (sProperty, "fp_MaxValue"), FLOAT_TYPE)
%>
		<numeric id='<%= sEncodedProperty %>' required='no' readonly='no'
			min='<%= sMin %>' max='<%= sMax %>' subtype='float'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<format><%= g_sMSCSNumberFormat %></format>
		</numeric>
<%		Case CURRENCY_TYPE
			sMin = stringValue (getPropertyAttribute (sProperty, "cy_MinValue"), CURRENCY_TYPE)
			sMax = stringValue (getPropertyAttribute (sProperty, "cy_MaxValue"), CURRENCY_TYPE)
%>
		<numeric id='<%= sEncodedProperty %>' required='no' readonly='no' 
			min='<%= sMin %>' max='<%= sMax %>' subtype='currency'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<format><%= g_sMSCSCurrencyFormat %></format>
		</numeric>
<%		Case DATETIME_TYPE
			sMin = stringValue(getPropertyAttribute(sProperty, "dt_MinValue"), DATE_TYPE)
			sMax = stringValue(getPropertyAttribute(sProperty, "dt_MaxValue"), DATE_TYPE)
%>
		<date id='<%= sEncodedProperty %>' required='no' readonly='no' firstday='<%= g_sMSCSWeekStartDay %>'
			min='<%= sMin %>' max='<%= sMax %>'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<format><%= g_sMSCSDateFormat %></format>
			<error><%= sFormatString (L_DateField_ErrorMessage, Array (sMin, sMax)) %></error>
		</date>
<%		Case ENUMERATION_TYPE %>
		<select id='<%= sEncodedProperty %>' readonly='no'>
			<name><%= sColon(sProperty) %></name>
			<tooltip><%= sProperty %></tooltip>
			<select id='<%= sEncodedProperty %>'>
<%= sGetEnumOptions (sProperty) %>
			</select>
		</select>
<%
		End Select
	End If
Next
%>
		<numeric id='price' <% If (Not g_bFamily) Then %>required='yes'<% End If %>
			min='0' max='<%= sMaxMoney %>' subtype='currency'>
			<name><% If g_bFamily Then %><%= L_DefaultPrice_Text %><% Else %><%= L_Price_Text %><% End If %></name>
			<tooltip><% If g_bFamily Then %><%= L_DefaultPrice_Tooltip %><% Else %><%= L_Price_Tooltip %><% End If %></tooltip>
			<format><%= g_sMSCSCurrencyFormat %></format>
		</numeric>
		</fields>
	</editsheet>
<% If g_bFamily Then %> 
		<editsheet>
		    <global expanded='no' downlevel='no'>
		        <name><%= L_ProductVariants_Text %></name>
		        <key><%= L_ProductVariants_Accelerator %></key>
			</global>
			<template register="ListEditor1"><![CDATA[
			<DIV ID='ListEditor1' CLASS='listEditor'
				STYLE='PADDING: 10px;'
				DataXML='leData'
				MetaXML='leMeta'
				LANGUAGE='VBScript'
				ONCHANGE='saveXMLData'
				ONEDIT='onEditVariant'
				ONNEW='onNewVariant'
				ONCANCEL='onCancelListEditor'><%= L_LoadingWidget_Text %></DIV>
		    ]]></template>
		</editsheet>
<% End If %>
		<editsheet>
		    <global expanded='no' downlevel='no'>
		        <name><%= L_AssignedCategories_Text %></name>
		        <key><%= L_AssignedCategories_Accelerator %></key>
			</global>
			<template><![CDATA[<BR>
			<TABLE>
				<TR>
					<TD><%= L_AvailableCategories_Text %></TD>
					<TD>&nbsp;</TD>
					<TD><%= L_AssignedCategories2_Text %></TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD VALIGN="TOP" STYLE="height:100%; width:30%">
						<DIV STYLE="height:100%; width:100%; overflow:auto; background:white; border:thin inset silver">
							<DIV ID='treeView'
								CLASS='treeView '
								DataXML='tvData'
								LANGUAGE='VBScript'
								OnItemSelect='onTVItemSelect'
								OnItemUnselect='onTVItemUnselect'
								OnItemOpen='onTVItemOpen'><%= L_LoadingWidget_Text %></DIV>
						</DIV>
					</TD>
					<TD VALIGN="CENTER" ALIGN="CENTER">
						<BUTTON CLASS="bdbutton" NAME="add_category" DISABLED ID="add_category" STYLE="<%= L_Button_Style %>" ONCLICK="onAddCategory()"><%= L_Add_Button %></BUTTON><BR>
						<BUTTON CLASS="bdbutton" NAME="remove_category" DISABLED ID="remove_category" STYLE="<%= L_Button_Style %>" ONCLICK="onRemoveCategory()"><%= L_Remove_Button %></BUTTON><BR><BR><BR><BR>
					</TD>
					<TD VALIGN="TOP" STYLE="width:30%">
						<div ID="ass_category" selection='multi'
							CLASS="listBox" STYLE="WIDTH: 100%; HEIGHT: 180px"
							LANGUAGE=VBScript
							ONSELECT="onSelect"
							ONUNSELECT="onUnSelect">
							<%= sGetAssignedCategories %>
						</div>
					</TD>
					<TD VALIGN="TOP">
						<BUTTON CLASS="bdbutton" NAME="new_category" ID="new_category" STYLE="<%= L_Button_Style %>" ONCLICK="onNewCategory()"><%= L_NewCategory_Button %></BUTTON><BR>
						<BUTTON CLASS="bdbutton" NAME="edit_category" DISABLED ID="edit_category" STYLE="<%= L_Button_Style %>" ONCLICK="onEditCategory()"><%= L_Edit_Button %></BUTTON><BR><BR><BR><BR><BR><BR><BR>
						<TABLE>
							<TD>
								<LABEL FOR='price_category'><%= L_PricingCategory_Text %></LABEL><BR>
								<INPUT TYPE='text' ID='price_category' READONLY='true' NAME='price_category' MAXLENGTH='64' STYLE='width:194px; color:black; background-color:#dddddd;' TITLE='<%= L_PricingCategory_Tooltip %>' VALUE='<%= g_sPriceCatName %>'><BR>
							</TD>
						</TABLE>
						<BUTTON CLASS="bdbutton" DISABLED NAME="set_category" ID="set_category" STYLE="<%= L_PricingButton_Style %>"><%= L_SetPricing_Button %></BUTTON>
						<BUTTON CLASS="bdbutton" <%If (g_sPriceCatName = "") Then%>DISABLED<%End If%> NAME="clear_category" ID="clear_category" STYLE="<%= L_PricingButton_Style %>"><%= L_ClearPricing_Button %></BUTTON>
					</TD>
				</TR>
			</TABLE><BR>
		    ]]></template>
		</editsheet>
		<editsheet>
<% If g_bFamily Then %>
		    <global expanded='no' downlevel='no'>
				<name><%= L_FamilyRelationships_Text %></name>
				<key><%= L_FamilyRelationships_Accelerator %></key>
			</global>
<% Else  %>
		    <global expanded='yes' downlevel='no'>
				<name><%= L_ProductRelationships_Text %></name>
				<key><%= L_ProductRelationships_Accelerator %></key>
			</global>
<% End If %>
			<template register='DynamicTable'><![CDATA[
				<DIV ID='DynamicTable'
					CLASS='dynamicTable'
					MetaXML='dtMeta'
					DataXML='dtData'
					LANGUAGE='VBScript'
					ONCHANGE='saveXMLData2()'><%= L_LoadingPropertyGroup_Text %>
				</DIV>
		    ]]></template>
		</editsheet>
<% If (sRows <> "") Then %>
		<editsheet>
		    <global expanded='no' downlevel='no'>
				<name><%= L_Images_Text %></name>
				<key><%= L_Images_Accelerator %></key>
			</global>
			<template><![CDATA[
				<TABLE STYLE="width:98%; table-layout:fixed; overflow-x:hidden">
					<COL WIDTH=30%><COL WIDTH=70%>
<%= sRows %>
				</TABLE>
		    ]]></template>
		</editsheet>
<% End If %>
	</editsheets>
</xml>

<%
' dynamic table xml for product relationships
%>
<xml id='dtMeta'>
	<dynamictable>
	    <global keycol='rel_name' uniquekey='yes' sortbykey='no' />
	    <fields>
			<text id='rel_name_old' hide='yes' subtype='short'>
				<name><%= L_ID_Text %></name>
			</text>
			<text id='rel_item_old' hide='yes' subtype='short'>
				<name><%= L_ID_Text %></name>
			</text>
			<text id='rel_descr_old' hide='yes' subtype='short'>
				<name><%= L_ID_Text %></name>
			</text>
			<text id='rel_name' width='33' subtype='short' required='yes' maxlen='128'>
				<name><%= L_Relationship_Text %></name>
				<prompt><%= L_Relationship2_Text %></prompt>
			    <error><%= sFormatString(L_TextField3_ErrorMessage, Array(1, 128)) %></error>
			</text>
			<text id='rel_item' width='33' required='yes' onbrowse='onBrowse()' browsereadonly='yes'>
				<name><%= L_Item_Text %></name>
				<prompt><%= L_Item2_Text %></prompt>
			    <error><%= L_Item_ErrorMessage %></error>
			</text>
			<text id='rel_descr' width='33' readonly='yes' subtype='short' maxlen='256'>
				<name><%= L_ItemType_Text %></name>
				<prompt><%= L_ItemType_Text %></prompt>
			</text>
	    </fields>
	</dynamictable>
</xml>

<xml id='dtData'>
	<records>
<%= sGetDTData (g_oProduct) %>
	</records>
</xml>

<xml id='tvData'>
	<document skip='yes'>
<%= g_sTVData %>
	<operations hidden='yes'>
		<newpage formid='tvform' />
		<expand formid='tvform' />
	</operations>
	</document>
</xml>

<%
If (True = g_bFamily) Then
%>
	<xml id='leMeta'>
		<listeditor>
		    <columns>
				<column id='variant_id' hide='yes'></column>
<%= sGetVariantIDs %>
		    </columns>
			<fields>
				<text id='variant_id' required='no' readonly='yes' 
					hide='yes' subtype='short'>
			    </text>
<%
	Dim sOnChange

	For Each sProperty In g_aPvChar
		sEncodedProperty = sReplaceSpaces(sProperty)
		If (sProperty = "variant_id2") Then sProperty = g_sVariantID
		sValue = sColon(sProperty)
		iPropertyType = getPropertyAttribute(sProperty, "DataType")

		If (sProperty = g_sVariantID) Then
			sOnChange = " onchange='onChangeVariantID'"
		Else
			sOnChange = ""
		End If

		Select Case iPropertyType
		Case STRING_TYPE
			iMinLength = getPropertyAttribute(sProperty, "MinLength")
			iMaxLength = getPropertyAttribute(sProperty, "MaxLength")
%>
				<text id='<%= sEncodedProperty %>' required='yes' readonly='no'
					minlen='<%= iMinLength %>'
					maxlen='<%= iMaxLength %>'
					subtype='short'<%= sOnChange%>>
			        <name><%= sValue %></name>
			        <tooltip><%= sProperty %></tooltip>
			        <error><%= sFormatString (L_TextField3_ErrorMessage, Array(iMinLength, iMaxLength)) %></error>
			    </text>
<%		Case FILEPATH_TYPE %>
				<text id='<%= sEncodedProperty %>' required='yes' readonly='no'
			           maxlen='256' subtype='short'>
			        <name><%= sValue %></name>
			        <tooltip><%= sProperty %></tooltip>
					<prompt><%= L_EnterURL_Text %></prompt>
			    </text>
<%		Case INTEGER_TYPE
			sMin = stringValue (getPropertyAttribute(sProperty, "i_MinValue"), INTEGER_TYPE)
			sMax = stringValue (getPropertyAttribute(sProperty, "i_MaxValue"), INTEGER_TYPE)
%>
				<numeric id='<%= sEncodedProperty %>' required='yes' readonly='no'
					min='<%= sMin %>' max='<%= sMax %>' subtype='integer'<%= sOnChange%>>
					<name><%= sValue %></name>
					<tooltip><%= sProperty %></tooltip>
					<format><%= g_sMSCSNumberFormat %></format>
				</numeric>
<%		Case FLOAT_TYPE, DOUBLE_TYPE
			sMin = stringValue (getPropertyAttribute(sProperty, "fp_MinValue"), FLOAT_TYPE)
			sMax = stringValue (getPropertyAttribute(sProperty, "fp_MaxValue"), FLOAT_TYPE)
%>
				<numeric id='<%= sEncodedProperty %>' required='yes' readonly='no' 
					min='<%= sMin %>' max='<%= sMax %>' subtype='float'>
					<name><%= sValue %></name>
					<tooltip><%= sProperty %></tooltip>
					<format><%= g_sMSCSNumberFormat %></format>
				</numeric>
<%		Case CURRENCY_TYPE
			sMin = stringValue (getPropertyAttribute(sProperty, "cy_MinValue"), CURRENCY_TYPE)
			sMax = stringValue (getPropertyAttribute(sProperty, "cy_MaxValue"), CURRENCY_TYPE)
%>
				<numeric id='<%= sEncodedProperty %>' required='yes' readonly='no'
					min='<%= sMin %>' max='<%= sMax %>' subtype='currency'>
			        <name><%= sValue %></name>
			        <tooltip><%= sProperty %></tooltip>
			        <format><%= g_sMSCSCurrencyFormat %></format>
			    </numeric>
<%		Case DATETIME_TYPE
			sMin = stringValue(getPropertyAttribute(sProperty, "dt_MinValue"), DATE_TYPE)
			sMax = stringValue(getPropertyAttribute(sProperty, "dt_MaxValue"), DATE_TYPE)
%>
				<date id='<%= sEncodedProperty %>' required='no' readonly='no' firstday='<%= g_sMSCSWeekStartDay %>'
					min='<%= sMin %>' max='<%= sMax %>'>
				    <name><%= sValue %></name>
				    <tooltip><%= sProperty %></tooltip>
				    <format><%= g_sMSCSDateFormat %></format>
				</date>
<%		Case ENUMERATION_TYPE %>
				<select id='<%= sEncodedProperty %>' readonly='no'>
					<name><%= sValue %></name>
					<tooltip><%= sProperty %></tooltip>
					<select id='<%= sEncodedProperty %>'>
<%= sGetEnumOptions (sProperty) %>
				    </select>
			    </select>
<%
		End Select
	Next
%>
				<numeric id='variant_price' required='yes' readonly='no'
					min='0' max='<%= sMaxMoney %>' subtype='currency'>
			        <name><%= sColon(L_VariantPrice_Text) %></name>
			        <tooltip><%= L_VariantPrice_Tooltip %></tooltip>
			        <format><%= g_sMSCSCurrencyFormat %></format>
			    </numeric>
			</fields>
		</listeditor>
	</xml>

	<xml id='leData'>
		<document>
<%=	g_sXMLVariants %>
		</document>
	</xml>
<%
End If
%>

<DIV ID='bdcontentarea' CLASS='editSheetContainer'>
	<FORM ACTION="" METHOD="POST" NAME="prForm" ID="prForm" ONTASK="prForm_onSubmit">
		<INPUT TYPE="Hidden" NAME="type" VALUE="<%= g_sActionType %>" ID="type">
		<INPUT TYPE="Hidden" NAME="pr_id" VALUE="<%= Server.HTMLEncode(g_sPrID) %>" ID="pr_id">
		<INPUT TYPE="Hidden" NAME="ct_name" VALUE="<%= g_sCtName %>" ID="ct_name">
		<INPUT TYPE="Hidden" NAME="pr_newtype" ID="pr_newtype">
		<INPUT TYPE="Hidden" NAME="family_xml" ID="family_xml">
		<INPUT TYPE="Hidden" NAME="relationship_xml" ID="relationship_xml">
		<INPUT TYPE="Hidden" NAME="added_categories" ID="added_categories">
		<INPUT TYPE="Hidden" NAME="removed_categories" ID="removed_categories">
		<INPUT TYPE="Hidden" NAME="priceCatName" ID="priceCatName" VALUE="<%= g_sPriceCatName %>">
		<INPUT TYPE="Hidden" NAME="pr_type" ID="pr_type" VALUE="<%= g_sPrType %>">
		<DIV ID='esControl' 
			CLASS='editSheet' 
			ONCHANGE='setDirty (L_SaveProduct_Text)'
			ONREQUIRE ='setRequired(L_Required_Text)'
			ONVALID = 'setValid(L_Valid_Text)'
			MetaXML='esMeta' 
			DataXML='esData'><%= L_LoadingPropertyGroups_Text %>
		</DIV>
	</FORM>
</DIV>

<FORM ACTION="" METHOD="POST" NAME="prForm2" ID="prForm2" ONTASK="onSaveNew">
</FORM>

<FORM ID="tvform" NAME="tvform" ACTION="response_Categories.asp">
	<INPUT TYPE="Hidden" NAME="type">
	<INPUT TYPE="Hidden" NAME="method" VALUE="0">
	<INPUT TYPE="Hidden" NAME="category">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sCtName %>">
</FORM>

<FORM ACTION="" METHOD="POST" NAME='closeForm' ID="closeForm">
	<INPUT TYPE="Hidden" NAME="type" VALUE="close">
</FORM>

</BODY>
</HTML>
<%
	ReleaseGlobalObjects
%>

<%
' ##########################################################
'
'	SERVER SIDE -
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'' -------------------------------------------------------------------
''
''	PUBLIC Declarations
''
'' -------------------------------------------------------------------

Public g_sPrID			' Product Id
Public g_sPrice			' Product's Price
Public g_sPrType		' Product Type
Public g_sCtName		' catalog to add this product to
Public g_bFamily		' Flag to distinguish between a product with and one without variants
Public g_sAction		' string representation of action to take on page
Public g_sHeader		' Tab header
Public g_sTVData
Public g_sProductID		' Products unique identifier
Public g_sVariantID		' Variants unique identifier
Public g_sActionType	' type to pass through
Public g_sStatusText	' To be displayed in the Status Bar
Public g_sVariantID2
Public g_sXMLVariants
Public g_sPriceCatName	' Product Pricing Category
Public g_sImageProperties

Public g_oDict
Public g_oDictVar
Public g_oProduct		' To instantiate product object

Public g_dServerState

Public g_aAddedCategories
Public g_aRemovedCategories
Public g_aAssignedCategories

Public g_rsProperties	' Property Definitions used by the Current Catalog

'' -------------------------------------------------------------------
''
''	Function:		Perform_ServerSide_Processing
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_ServerSide_Processing ()

	InitializeGlobalVariables_Part1
		InitializeCatalogObject
		Set g_oCat = g_oCatMgr.GetCatalog(g_sCtName)
		Set g_rsProperties = g_oCatMgr.Properties
	InitializeGlobalVariables_Part2

	Perform_Processing_BasedOnAction
	If (g_bFamily = True) Then GetXMLVariants
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeGlobalVariables_Part1
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeGlobalVariables_Part1 ()

	g_sAction = Request.Form("type")
	If (g_sAction = "") Then
		' It's a Nested Page
		Set g_dServerState = Session(PRODUCT_PARAMETERS)
		g_sAction = g_dServerState("type")
		g_sCtName = sNullToEmptyString(g_dServerState("ct_name"))
		g_sPrType = sNullToEmptyString(g_dServerState("pr_type"))
	Else
		g_sCtName = Request.Form("ct_name")
		g_sPrType = Request.Form("pr_type")
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeGlobalVariables_Part2
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeGlobalVariables_Part2 ()

	Dim sValue
	Dim sProperty

	Dim rsProperties

	g_sProductID = g_oCat.IdentifyingProductProperty
	g_sVariantID = g_oCat.IdentifyingVariantProperty
	If (g_sProductID = g_sVariantID) Then
		g_sVariantID2 = "variant_id2"
	Else
		g_sVariantID2 = g_sVariantID
	End If
	g_sPrID = Trim (Request.Form(sReplaceSpaces(g_sProductID)))

	Select Case g_sAction
	Case "add", "copy"
	Case "open"
		' Check if it a Nested Page
		If (g_sPrID = "") Then _
			g_sPrID = g_dServerState("pr_id")
	Case Else
	End Select

	g_sPriceCatName = Request.Form("priceCatName")
	g_aAddedCategories = Split(Request.Form("added_categories"), SEPARATOR1)
	g_aRemovedCategories = Split(Request.Form("removed_categories"), SEPARATOR1)

	GetTVData
	InitializeGlobalVariables
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeGlobalVariables
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeGlobalVariables ()

	Dim bTemp
	Dim sValue
	Dim sProperty

	Dim rsProperties

	ReDim g_aPrChar(-1)
	ReDim g_aPvChar (-1)
	ReDim g_aVariants(-1)
	ReDim g_aImageProperties(-1)
	g_bFamily = False

	Set g_oProduct = Nothing : Set g_oDict = Nothing : Set g_oDictVar = Nothing
	' g_oDict will hold values of properties associated with this product 
	' g_oDictVar will hold values related to the variant part of the definition
	Set g_oDict = Server.CreateObject("Commerce.Dictionary")
	Set g_oDictVar = Server.CreateObject("Commerce.Dictionary")

	g_oDict(g_sProductID) = g_sPrID
	AddArray g_aPvChar, g_sVariantID2

	' Check if Product Definition includes Variant Properties
	Set rsProperties = g_oCatMgr.GetDefinitionProperties(g_sPrType)
	Do Until rsProperties.EOF
		If (rsProperties("PropertyType").Value = VARIANT_PROPERTY) Then
			g_bFamily = True
			Exit Do
		End If

		rsProperties.MoveNext
	Loop
	rsProperties.MoveFirst

	Do Until rsProperties.EOF
		sProperty = rsProperties("PropertyName")
		If (rsProperties("PropertyType") = REGULAR_PROPERTY) Then
			If (sProperty <> g_sProductID) Then
				bTemp = False
				If (sProperty <> g_sVariantID) Then
					bTemp = True
				ElseIf (Not g_bFamily) Then
					bTemp = True
				End If

				If bTemp Then
					If (g_sAction = "add") Then
						g_oDict(sProperty) = ""
					Else
						g_oDict(sProperty) = Request.Form (sReplaceSpaces(sProperty))
					End If

					AddArray g_aPrChar, sProperty
				End If
			End If
		Else' VARIANT_PROPERTY
			If (sProperty <> g_sProductID) And (sProperty <> g_sVariantID) Then
				If (g_sAction = "add") Then
					g_oDictVar(sProperty) = ""
				Else
					g_oDictVar(sProperty) = Request.Form(sReplaceSpaces(sProperty))
				End If

				AddArray g_aPvChar, sProperty
			End If
		End If

		rsProperties.MoveNext
	Loop

	ReleaseRecordset rsProperties
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Perform_Processing_BasedOnAction
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_Processing_BasedOnAction ()

	Select Case g_sAction
	Case "submitNew"
		Process_SubmitNEW

	Case "createNew"
		Process_CreateNEW

	Case "saveNew"
		Process_SaveNEW

	Case "submitEdit", "submitEditF"
		Process_SubmitEDIT

	Case "add"
		Process_ADD

	Case "open"
		Process_OPEN

	End Select
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_SubmitNEW
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_SubmitNEW ()

	Dim oCategory
	Dim oRecordNode
	Dim oVariantXML
	Dim oRelationshipXML

	Dim sProperty
	Dim sCategory
	Dim sVariantXML
	Dim sRelationshipXML

	g_sActionType = "submitEdit"
	'Capture user-entered data
	g_sPrice = Request.Form("price")

	'Create a new product
	CreateNewProduct

	If (g_oProduct Is Nothing) Then
		g_sActionType = "submitNew"
		g_sStatusText = L_ProductIDAlreadyExists_Text
		g_sHeader = sFormatString (L_Product_HTMLText, Array(g_sCtName, L_New_HTMLText, g_sPrType))
		Exit Sub
	End If

	If (True = g_bFamily) Then
		' Capture variant values from xml and process
		sVariantXML = Request.Form("family_xml")
		Set oVariantXML = oCreateXMLDocument(sVariantXML)

		If IsObject(oVariantXML) Then
			If oVariantXML.documentElement.hasChildNodes Then
				For Each oRecordNode In oVariantXML.documentElement.childNodes
					If (oRecordNode.getAttribute("state") = "new") Then
						For Each sProperty In g_aPvChar
							g_oDictVar(sProperty) = oRecordNode.selectSingleNode("./" & sReplaceSpaces(sProperty)).text
						Next

						CreateProductVariant variantValue(g_oDictVar(g_sVariantID2), g_sVariantID), _
											 variantValue(oRecordNode.selectSingleNode("./variant_price").text, CURRENCY_TYPE)
					End If
				Next
			End If
		End If

		Set oVariantXML = Nothing
	End If

	' Set Pricing Category
	UpdatePricingCategory
	' Create product relationships
	sRelationshipXML = Request.Form("relationship_xml")
	If (sRelationshipXML <> "") Then
		Set oRelationshipXML = oCreateXMLDocument(sRelationshipXML)
		If IsObject(oRelationshipXML) Then
			ProcessRelationshipXML oRelationshipXML, g_oProduct
		End If
	End If

	' Place product in appropriate categories
	If IsArray(g_aAddedCategories) Then
		On Error Resume Next
		For Each sCategory In g_aAddedCategories
			If (sCategory <> "") Then
				Set oCategory = g_oCat.GetCategory(sCategory)
					If (Not oCategory Is Nothing) Then oCategory.AddProduct g_sPrID
				Set oCategory = Nothing
			End If
		Next
		On Error GoTo 0
	End If

	' Set display variables
	g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sPrID))
	g_sHeader = sFormatString (L_Product_HTMLText, Array(g_sCtName, g_sPrID, g_sPrType))
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_CreateNEW
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_CreateNEW ()

	Process_SubmitNEW
	Process_ADD
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_SaveNEW
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_SaveNEW ()

	Process_SubmitEDIT
	Process_ADD
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_SubmitEDIT
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_SubmitEDIT ()

	Dim varID
	Dim varPrice
	Dim varProductID

	Dim sItem
	Dim sVariantXML
	Dim sRelationshipXML

	Dim oCategory
	Dim oVariantXML
	Dim oRecordNode
	Dim oRelationshipXML

	g_sActionType = "submitEdit"
	'Capture user-entered data
	g_sPrID = g_oDict(g_sProductID)
	g_sPrice = Request.Form("price")
	g_sHeader = sFormatString (L_Product_HTMLText, Array (g_sCtName, g_sPrID, g_sPrType))

	UpdateProduct
	If (g_oProduct Is Nothing) Then
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sPrID))
		Exit Sub
	End If
	UpdatePricingCategory

	' Update Product Relationships
	sRelationshipXML = Request.Form("relationship_xml")
	If (sRelationshipXML <> "") Then
		Set oRelationshipXML = oCreateXMLDocument(sRelationshipXML)
		If IsObject(oRelationshipXML) Then
			ProcessRelationshipXML oRelationshipXML, g_oProduct
		End If
		Set oRelationshipXML = Nothing
	End If

	' Remove product from categories
	If IsArray(g_aRemovedCategories) Then
		For Each sItem In g_aRemovedCategories
			If (sItem <> "") Then
				Set oCategory = g_oCat.GetCategory(sItem)
				If (Not oCategory Is Nothing) Then
					oCategory.RemoveProduct g_sPrID
					Set oCategory = Nothing
				End If
			End If
		Next
	End If

	' Place product in appropriate categories
	If IsArray(g_aAddedCategories) Then
		For Each sItem In g_aAddedCategories
			If (sItem <> "") Then
				Set oCategory = g_oCat.GetCategory(sItem)
				If (Not oCategory Is Nothing) Then
					oCategory.AddProduct g_sPrID
					Set oCategory = Nothing
				End If
			End If
		Next
	End If
	' Set Status Text
	g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sPrID))
	If (False = g_bFamily) Then Exit Sub

	' If Product is a Product Family Then
	'	Capture variant values from XML data and process
	sVariantXML = Request.Form("family_xml")
	Set oVariantXML = oCreateXMLDocument(sVariantXML)
	If (True = IsObject(oVariantXML)) Then
		If oVariantXML.documentElement.hasChildNodes Then
			For Each oRecordNode In oVariantXML.documentElement.childNodes
				If oRecordNode.hasChildNodes Then
					With oRecordNode
						varID = variantValue(.selectSingleNode("./variant_id").text, g_sVariantID)
						varProductID = variantValue(.selectSingleNode("./" & sReplaceSpaces(g_sVariantID2)).text, g_sVariantID)
						varPrice = variantValue(.selectSingleNode("./variant_price").text, "cy_list_price")

						If (True = IsNull(.getAttribute("state"))) Then
							AddVariantToVariantArray varID, g_aVariants
						Else
							Select Case .getAttribute("state")
							Case "new"
								varID = varProductID
								For Each sItem In g_aPvChar
									g_oDictVar(sItem) = .selectSingleNode("./" & sReplaceSpaces(sItem)).text
								Next
								CreateProductVariant varID, varPrice

							Case "changed"
								For Each sItem In g_aPvChar
									g_oDictVar(sItem) = .selectSingleNode("./" & sReplaceSpaces(sItem)).text
								Next
								UpdateProductVariant varID, varProductID, varPrice

							Case "deleted"
								DeleteProductVariant varID
							End Select
						End If
					End With
				End If
			Next

			Set oVariantXML = Nothing
		End If
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_ADD
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_ADD ()

	Dim iType

	Dim sValue
	Dim sProperty
	Dim sAttribute

	g_sPrID = ""
	g_sPrice = ""
	g_sPriceCatName = ""
	g_sActionType = "submitNew"

	If (g_sAction = "saveNew") Or (g_sAction = "createNew") Then
		g_sAction = "add"
		g_sPrType = Request.Form("pr_newtype")
		InitializeGlobalVariables
	End If
	g_sHeader = sFormatString (L_Product_HTMLText, Array(g_sCtName, L_New_HTMLText, g_sPrType))

	For Each sProperty In g_aPrChar
		iType = getPropertyAttribute(sProperty, "DataType")

		Select Case iType
		Case STRING_TYPE
			sAttribute = "u_DefaultValue"
		Case INTEGER_TYPE
			sAttribute = "i_DefaultValue"
		Case FLOAT_TYPE
			sAttribute = "fp_DefaultValue"
		Case CURRENCY_TYPE
			sAttribute = "cy_DefaultValue"
		Case DATETIME_TYPE
			sAttribute = "dt_DefaultValue"
		Case ENUMERATION_TYPE
			sAttribute = "u_DefaultValue"
		Case Else
			sAttribute = "u_DefaultValue"
		End Select
		sValue = stringValue (getPropertyAttribute (sProperty, sAttribute), iType)

		If (True = IsNull (sValue)) Then
			sValue = ""
		End If
		g_oDict(sProperty) = sValue
	Next
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_OPEN
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_OPEN ()

	Dim rs
	Dim sItem

	Set g_oProduct = g_oCat.GetProduct (g_sPrID)
	If (Not g_oProduct Is Nothing) Then
		Set rs = g_oProduct.GetProductProperties
		g_sPrice = stringValue (rs("cy_list_price").Value, CURRENCY_TYPE)
		g_oDict(g_sProductID) = stringValue (rs(g_sProductID).Value, getPropertyAttribute(g_sProductID, "DataType"))
		g_sPriceCatName = g_oProduct.PricingCategory

		For Each sItem In g_aPrChar
			If (True = IsNull (rs(sItem).Value)) Then
				g_oDict(sItem) = ""
			Else
				g_oDict(sItem) = stringValue (rs(sItem).Value, getPropertyAttribute (sItem, "DataType"))
			End If
		Next

		ReleaseRecordset rs
	End If

	g_sHeader = sFormatString (L_Product_HTMLText, Array(g_sCtName, g_sPrID, g_sPrType))
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetXMLVariants	
''
''	Description:	Gets XML Data corresponding to ListEditor instance
''					in Product Editor.
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetXMLVariants ()

	Dim rs
	Dim sItem
	Dim sItemID

	If (g_sAction = "add") Or (g_oProduct Is Nothing) Then
		g_sXMLVariants = "<record/>"
		Exit Sub
	End If

	Set rs = g_oProduct.Variants
	While Not rs.EOF
		g_sXMLVariants = g_sXMLVariants & _
							"<record><variant_id>" & rs(g_sVariantID) & "</variant_id>"
		For Each sItem In g_aPvChar
			sItemID = sReplaceSpaces(sItem)

			If (sItem = "variant_id2") Then
				g_sXMLVariants = g_sXMLVariants & "<" & sItemID & ">" & rs(g_sVariantID) & "</" & sItemID & ">"
			Else
				g_sXMLVariants = g_sXMLVariants & "<" & sItemID & ">" & rs(sItem) & "</" & sItemID & ">"			
			End If
		Next
		g_sXMLVariants = g_sXMLVariants & _
							"<variant_price>" & rs("cy_list_price") & "</variant_price>" & _
							"</record>"
		rs.MoveNext
	Wend
	ReleaseRecordset rs

	If (g_sXMLVariants = "") Then
		g_sXMLVariants = "<record/>"
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		sGetESData
''
''	Description:	Generate product-type characteristics XML Data
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function sGetESData ()

	Dim sItem
	Dim sValue
	Dim sItemID

	sItem = g_sProductID
	sItemID = sReplaceSpaces(sItem)
	sGetESData = sGetESData &  "<" & sItemID & ">" & g_oDict(sItem) & "</" & sItemID & ">"

	For Each sItem In g_aPrChar
		sItemID = sReplaceSpaces(sItem)
		sValue = replace(replace(replace(g_oDict(sItem), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
		sGetESData = sGetESData &  "<" & sItemID & ">" & sValue & "</" & sItemID & ">"
	Next
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetAssignedCategories
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function sGetAssignedCategories ()

	Dim rsParents
	Dim oCategory
	Dim sCategory

	If (Not g_oProduct Is Nothing) Then
		Set rsParents = g_oProduct.ParentCategories

		While Not rsParents.EOF
 			sCategory = rsParents("Name")
			sGetAssignedCategories = sGetAssignedCategories & _
					"<DIV VALUE='" & Server.HTMLEncode(sCategory) & "'>" & _
					sCategory & "</DIV>" & vbCR

			rsParents.MoveNext
		Wend

		ReleaseRecordset rsParents
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetVariantIDs
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		string
''
'' -------------------------------------------------------------------
Function sGetVariantIDs ()

	Dim iSize

	Dim sItem
	Dim sItemID
	Dim sColumnName

	iSize = 100 / (UBound(g_aPvChar) + 2)
	For Each sItem In g_aPvChar
		sItemID = sReplaceSpaces(sItem)

		If (sItem = "variant_id2") Then
			sColumnName = g_sVariantID
		Else
			sColumnName = sItem
		End If

		sGetVariantIDs = sGetVariantIDs & "<column id='" & sItemID & "' width='" & iSize & "'>" & sColumnName & "</column>"
	Next

	sGetVariantIDs = sGetVariantIDs & "<column id='variant_price' width='" & iSize & "'>" & L_VariantPrice_Text & "</column>"
End Function

'' ###################################################################
''
''	Product Create/Update procedures.
''
'' ###################################################################

'' -------------------------------------------------------------------
''
''	Function:		CreateNewProduct
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub CreateNewProduct ()

	Dim sProperty
	Dim rsProperties

	On Error Resume Next

	' Price is a REQUIRED field for the API
	If (g_sPrice = "") Then g_sPrice = "0"
	Set g_oProduct = g_oCat.CreateProduct(g_sPrType, _
										  variantValue(g_sPrID, g_sProductID), _
										  variantValue(g_sPrice, CURRENCY_TYPE))
	If (g_oProduct Is Nothing) Then Exit Sub

	Set rsProperties = g_oProduct.GetProductProperties
	If (Not rsProperties.EOF) Then
		For Each sProperty In g_oDict
			rsProperties(sProperty) = variantValue(g_oDict(sProperty), sProperty)
		Next

		g_oProduct.SetProductProperties rsProperties
		CheckForErrorSavingProperties
	Else
		rsProperties.Close
	End If

	Set rsProperties = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		UpdateProduct
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub UpdateProduct ()

	Dim sItem
	Dim bForceUpdate
	Dim rsProperties

	On Error Resume Next

	Set g_oProduct = g_oCat.GetProduct(g_sPrID)
	If (g_oProduct Is Nothing) Then Exit Sub

	Set rsProperties = g_oProduct.GetProductProperties
	If (Not rsProperties.EOF) Then
		' Price is a REQUIRED field for the API
		If (g_sPrice = "") Then g_sPrice = "0"
		rsProperties("cy_list_price") = variantValue(g_sPrice, CURRENCY_TYPE)

		For Each sItem In g_oDict
			rsProperties(sItem) = variantValue(g_oDict(sItem), sItem)
		Next
	End If

	' Update Product
	If (g_sAction = "submitEditF") Then
		bForceUpdate = True
	Else
		bForceUpdate = False
	End If
	g_oProduct.SetProductProperties rsProperties, bForceUpdate
	CheckForPropertyAlreadyModified
	CheckForErrorSavingProperties

	Set rsProperties = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		CheckForErrorSavingProperties
''
''	Description:	
''
'' -------------------------------------------------------------------
Sub CheckForErrorSavingProperties ()

	If (Err.Number <> 0) Then
		Call setError (L_Critical_DialogTitle, L_ErrorSavingProductProperties_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		CreateProductVariant
''
''	Description:	
''
''	Arguments:		variantID -
''					cyPrice -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub CreateProductVariant (ByRef variantID, ByVal cyPrice)

	Dim sProperty
	Dim rsProperties

	' Create the New Product
	On Error Resume Next
	Set rsProperties = g_oProduct.CreateVariant (variantID)
	If (Not rsProperties.EOF) Then
		' Assign values
		If Not IsNull(cyPrice) Then _
			rsProperties("cy_list_price") = cyPrice

		For Each sProperty In g_oDictVar
			If (sProperty <> "variant_id2") Then _
				rsProperties(sProperty) = variantValue(g_oDictVar(sProperty), sProperty)
		Next

		g_oProduct.SetVariantProperties(rsProperties)
		AddVariantToVariantArray variantID, g_aVariants
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		UpdateProductVariant
''
''	Description:	
''
''	Arguments:		variantID -
''					variantNewID -
''					cyPrice -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub UpdateProductVariant (ByRef variantID, ByRef variantNewID, ByVal cyPrice)

	Dim sProperty
	Dim bForceUpdate
	Dim rsProperties

	' If VariantID has been modified Then
	If (variantID <> variantNewID) Then
		DeleteProductVariant variantID
		CreateProductVariant variantNewID, cyPrice
		Exit Sub
	End If
	' Else
	On Error Resume Next
	Set rsProperties = g_oProduct.GetVariantProperties(variantNewID)
	If (Not rsProperties.EOF) Then
		rsProperties("cy_list_price") = cyPrice

		For Each sProperty In g_oDictVar
			If (sProperty <> "variant_id2") Then _
				rsProperties(sProperty) = variantValue (g_oDictVar(sProperty), sProperty)
		Next

		If (g_sAction = "submitEditF") Then
			bForceUpdate = True
		Else
			bForceUpdate = False
		End If

		g_oProduct.SetVariantProperties rsProperties
		CheckForPropertyAlreadyModified

		AddVariantToVariantArray variantID, g_aVariants
	End If

	Set rsProperties = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		DeleteProductVariant
''
''	Description:	
''
''	Arguments:		variantID
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub DeleteProductVariant (ByRef variantID)

	On Error Resume Next
	g_oProduct.DeleteVariant variantID
End Sub

' #####################################################################
'
'' -------------------------------------------------------------------
''
''	Function:		AddVariantToVariantArray
''
''	Description:	
''
''	Arguments:		varID - 
''					aVariants -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub AddVariantToVariantArray (ByRef varID, ByRef aVariants)

	Dim iUBound

	If IsArray(aVariants) Then
		iUBound = UBound(aVariants) + 1
	Else
		iUBound = 0
	End If

	ReDim Preserve  aVariants(iUBound)
	aVariants(iUBound) = varID
End Sub

'' -------------------------------------------------------------------
''
''	Function:		UpdatePricingCategory
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub UpdatePricingCategory ()

	Dim rsProperties

	On Error Resume Next
	g_oProduct.PricingCategory = g_sPriceCatName
	If (Err.Number <> 0) Then
		g_sPriceCatName = ""
		Call setError (L_Critical_DialogTitle, L_ErrorUpdatingPricingCategory_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	Set rsProperties = g_oProduct.GetProductProperties
	If (Not rsProperties.EOF) Then
		g_sPrice = stringValue (rsProperties("cy_list_price").Value, CURRENCY_TYPE)
	End If
	ReleaseRecordset rsProperties
End Sub

'' -------------------------------------------------------------------
''
''	Function:		ReleaseGlobalObjects
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ReleaseGlobalObjects ()

	If Not IsEmpty(Session(PRODUCT_PARAMETERS)) Then
		Set Session(PRODUCT_PARAMETERS) = Nothing
		Session(PRODUCT_PARAMETERS) = Empty
	End If

	ReleaseRecordset g_rsProperties

	Set g_oCat = Nothing
	Set g_oCatMgr = Nothing
End Sub

%>