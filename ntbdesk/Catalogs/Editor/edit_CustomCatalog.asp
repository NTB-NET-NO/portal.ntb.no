<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
' ######################################################################
'
'	edit_CustomCatalog.ASP
'
'	Item View Page for Custom Catalogs
'
' ######################################################################

	On Error Resume Next
	' --------------------------------------
	Const PAGE_TYPE = "EDIT"
%>
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

public g_bFlag			' Semaphore
public g_sItemID		' For holding value of a selected xml attribute node
public g_oXMLNodePricing

sub window_onLoad ()

	dim sAction

	g_sItemID = ""
	set g_oXMLNodePricing = Nothing

	sAction = "<%= g_sAction %>"
	' F O R C E   U P D A T E
	CheckForceUpdate sAction, ctForm

	esControl.focus
end sub

sub onChange ()

	setStatusText ""
	setDirty (L_SaveCatalog_Text)
end sub

sub ctForm_onSubmit ()

	if not (g_oXMLNodePricing is Nothing) then
		RemoveQueryIDAttributes
		ctForm("tvXML").value = g_oXMLNodePricing.ownerDocument.xml
	else
		ctForm("tvXML").value = tvData.XMLDocument.xml
	end if

	ctForm.Submit
end sub

sub RemoveQueryIDAttributes ()

	dim oXMLNode
	dim oXMLList
	dim oXMLDocument

	set oXMLDocument = g_oXMLNodePricing.ownerDocument
	set oXMLList = oXMLDocument.selectNodes ("//")

	for each oXMLNode in oXMLList
		RemoveQueryID oXMLNode
	next

	RemoveQueryID oXMLDocument
end sub

sub RemoveQueryID (ByRef oNode)

	dim oAttributes

	set oAttributes = oNode.attributes
	if not (oAttributes is Nothing) then
		oAttributes.removeNamedItem ("queryid")
		set oAttributes = Nothing
	end if
end sub

sub onTVItemSelect ()

	dim sID
	dim sValue

	dim iType

	dim oXMLNode
	dim oXMLNodePricing

	set oXMLNode = window.event.XMLitem
	if not(oXMLNode is Nothing) then
		sID = oXMLNode.getAttribute("caption")
		if (g_sItemID <> sID) then
			g_sItemID = sID
			set oXMLNodePricing = oXMLNode.selectSingleNode("./pricing")
			set g_oXMLNodePricing = oXMLNodePricing

			iType = CInt(oXMLNodePricing.getAttribute("type"))
			sValue = oXMLNodePricing.text

			SetCategoryPricingPane iType, sValue
		end if
	end if
end sub

sub onTVItemUnselect ()

	DisableCategoryPricingPane True
	ctForm.custom_pricing.disabled = True

	g_sItemID = ""
end sub

sub onTVItemOpen ()

	tvform.category.value = sGetCategoryFromEvent
end sub

sub SetCategoryPricingPane (ByVal iType, ByRef sValue)

	if (iType = 0) then	' = cscNoCustomPrice
		SetCategoryPricingValues False, iType, sValue
	else
		SetCategoryPricingValues True, iType, sValue
	end if
end sub

sub SetCategoryPricingValues (ByVal bChecked, ByVal iType, ByRef sValue)

	dim bDisabled

	g_bFlag = True

	bDisabled = not bChecked
	ctForm.custom_pricing.disabled = False
	ctForm.custom_pricing.checked = bChecked
	'disable first so change event does not fire
	pr_method.disabled = True
	pr_method.value = iType
	pr_method.disabled = bDisabled
	pr_value.disabled = bDisabled
	pr_value.value = sValue

	g_bFlag = False
end sub

sub pr_onChange ()

	if g_bFlag then exit sub
	pricing_onChange()
end sub

sub pricing_onChange ()

	dim dValue
	dim sValue
	dim iMethod

	if g_bFlag then exit sub
	g_bFlag = True

	iMethod = CInt(pr_method.value)
	sValue = pr_value.value
	if ((iMethod = 1) or (iMethod = 3)) and (sValue <> "") then
		dValue = CDbl (sValue)
		if (dValue < 0) then
			alert L_PositiveValue_Message
			pr_value.focus()
			exit sub
		end if
	end if

	if (ctForm.custom_pricing.checked = True) then
		pr_method.disabled = False
		if (iMethod = 0) then
			pr_value.disabled = True
			pr_value.value = "0.00"
			SetXMLNode "0", "0"
		else
			pr_value.disabled = False
			SetXMLNode iMethod, sValue
			DisableCategoryPricingPane False
		end if
	else
		SetXMLNode "0", "0"
		DisableCategoryPricingPane True
	end if
	onChange

	g_bFlag = False
end sub

sub SetXMLNode (ByRef sType, ByRef sValue)

	g_oXMLNodePricing.setAttribute "modified", "yes"

	g_oXMLNodePricing.setAttribute "type", sType
	g_oXMLNodePricing.text = sValue
end sub

sub DisableCategoryPricingPane (ByVal bDisabled)

	g_bFlag = True

	pr_method.disabled = bDisabled
	if (True = bDisabled) then
		pr_value.disabled = True
		ctForm.custom_pricing.checked = False
		pr_method.value = 0
		pr_value.value = "0"
	end if

	g_bFlag = False
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertEditTaskBar g_sHeader, g_sStatusText
%>

<xml id='esData'>
	<DOCUMENT>
		<RECORD>
			<ct_name><%= Server.HTMLEncode(g_sCtName) %></ct_name>
			<base><%= Server.HTMLEncode(g_sBaseCtName) %></base>
			<currency><%= g_sCurrency %></currency>
		</RECORD>
	</DOCUMENT>
</xml>

<xml id='tvData'>
	<document skip='yes'>
		<%= g_sTVData %>
		<operations hidden='yes'>
			<expand formid='tvform' />
		</operations>
	</document>
</xml>

<xml id='esMeta'>
	<editsheets>
		<editsheet>
		    <global expanded='yes'>
		        <name><%= L_CustomCatalogProperties_Text %></name>
		        <key><%= L_CustomCatalogProperties_Accelerator %></key>
			</global>
			<fields>
			    <text id='ct_name' required='yes' <% If (g_sActionType <> "submitNew") Then %>readonly='yes'<% End If %>
			           maxlen='128' subtype='short'>
			        <name><%= L_Name_Text %></name>
			        <tooltip><%= L_Name_Tooltip %></tooltip>
			        <charmask><%= Server.HTMLEncode("^[^<>\[\]]*$") %></charmask>
					<error><%= L_ValidationErrorInCatalogName_ErrorMessage %></error>
			    </text>
			    <text id='base' readonly='yes' maxlen='100' subtype='short'>
			        <name><%= L_BaseCtName_Text %></name>
			        <tooltip><%= L_BaseCtName_Tooltip %></tooltip>
			        <charmask>^.*$</charmask>
			    </text>
			    <text id='currency' readonly='yes' maxlen='100' subtype='short'>
			        <name><%= L_Currency2_Text %></name>
			        <tooltip><%= L_Currency_Tooltip %></tooltip>
			    </text>
		    </fields>
		</editsheet>
		<editsheet>
		    <global expanded='no'>
		        <name><%= L_CustomPricing_Text %></name>
		        <key><%= L_CustomPricing_Accelerator %></key>
			</global>
		    <fields>
				<select id="pr_method" disabled='yes' onchange="pr_onChange()">
					<select id="pr_method">
						<option value="<%= cscNoCustomPrice %>"></option>
						<option value="<%= cscPercentageMultiplier %>"><%= L_PercentageAdjustment_Text %></option>
						<option value="<%= cscAddFixedAmount %>"><%= L_FixedAdjustment_Text %></option>
						<option value="<%= cscReplacePrice %>"><%= L_SetPrice_Text %></option>
					</select>
				</select>
				 <numeric id='pr_value' subtype='float' disabled='yes' onchange="pr_onChange()">
			        <format><%= g_sMSCSNumberFormat %></format>
				 </numeric>
		    </fields>
			<template><![CDATA[<BR>
			<TABLE>
				<TR>
					<TD><%= L_Categories_HTMLText %></TD>
					<TD>&nbsp;</TD>
					<TD><%= L_CategoryPricing_HTMLText %></TD>
				</TR>
				<TR>
					<TD>
						<DIV STYLE='width:220px; height:161px; overflow:auto; background:white; border:thin inset silver'>
							<DIV ID='treeView'
								CLASS='treeView '
								DataXML='tvData'
								LANGUAGE='VBScript'
								OnItemSelect='onTVItemSelect'
								OnItemUnselect='onTVItemUnselect'
								OnItemOpen='onTVItemOpen'><%= L_LoadingWidget_Text %></DIV>
						</DIV>
					</TD>
					<TD>&nbsp;</TD>
					<TD VALIGN="TOP">
						<FIELDSET STYLE="height:160px; padding:10px">
						<INPUT TYPE="checkbox" ID="custom_pricing" DISABLED ONCLICK="pr_onChange()" STYLE="background-color:transparent"><LABEL FOR="custom_pricing"><%= L_UseCustomPricing_Text %></LABEL><BR><BR>
						<%= L_PricingMethod_Text %><BR>
						<div STYLE="width:200px">	
							<DIV ID='pr_method' CLASS='editField'
								MetaXML='esMeta'
								DataXML='esData'></DIV></div>
						<BR><BR>
						<%= L_Value_Text %><BR>
						<div STYLE="width:200px">
							<DIV ID='pr_value' CLASS='editField'
								MetaXML='esMeta'
								DataXML='esData'></DIV></div>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE><BR><BR>
		    ]]></template>
		</editsheet>
	</editsheets>
</xml>

<DIV ID='bdcontentarea' CLASS='editSheetContainer'>

	<FORM ACTION="" METHOD="POST" NAME='ctForm' ID="ctForm" ONTASK="ctForm_onSubmit">
		<INPUT TYPE="Hidden" NAME="type" VALUE="<%= g_sActionType %>" ID="type">
		<INPUT TYPE="Hidden" NAME="old_name" VALUE="<%= g_sOldCtName %>" ID="old_name">
		<INPUT TYPE="Hidden" NAME="tvXML" ID="tvXML">
		<DIV ID='esControl'
			CLASS='editSheet'
			ONCHANGE='onChange'
			ONREQUIRE ='setRequired(L_Required_Text)'
			ONVALID = 'setValid(L_Valid_Text)'
			MetaXML='esMeta' 
			DataXML='esData'><%= L_LoadingPropertyGroups_Text %>
		</DIV>
	</FORM>
</DIV>

<FORM ID="tvform" NAME="tvform" ACTION="response_Categories.asp">
	<INPUT TYPE="Hidden" NAME="type" VALUE="expand">
	<INPUT TYPE="Hidden" NAME="method" VALUE="1">
	<INPUT TYPE="Hidden" NAME="custom_catalog" VALUE="<%= g_sCtName %>">
	<INPUT TYPE="Hidden" NAME="category">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sBaseCtName %>">
</FORM>

<FORM ACTION="" METHOD="POST" NAME='closeForm' ID="closeForm">
	<INPUT TYPE="Hidden" NAME="type" VALUE="close">
</FORM>

</BODY>
</HTML>


<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'------ CONST Declarations
Const cscNoCustomPrice = 0
Const cscPercentageMultiplier = 1
Const cscAddFixedAmount = 2
Const cscReplacePrice = 3

' ----- PUBLIC Declarations
Public g_sHeader
Public g_sAction		' String representation of action to take on page
Public g_sTVData
Public g_sCtName
Public g_sCurrency
Public g_sActionType	' Type to pass through
Public g_sStatusText	' To be displayed in the Status Bar
Public g_sOldCtName		' Catalog Name
Public g_sBaseCtName

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables
	Perform_Processing_BasedOnAction

	GetTVData
End Sub

' ##########################################################
'
Sub InitializeGlobalVariables ()

	g_sAction = Request.Form("type")
	g_sCtName = Request.Form("ct_name")
	g_sBaseCtName = Request.Form("base")
	Set g_oCat = g_oCatMgr.GetCatalog(g_sBaseCtName)

	Select Case g_sAction
	Case "add", "open"
		g_sCurrency = sGetCurrency

	Case "submitNew", "submitEdit"
		g_sTVData = Request.Form("tvXML")

		'Capture user-entered data
		g_sCurrency = Request.Form("currency")
	End Select
End Sub

' ##########################################################
'
Function sGetCurrency ()

	Dim rsAttributes

	Set rsAttributes = g_oCat.GetCatalogAttributes
	If (Not rsAttributes.EOF) Then _
		sGetCurrency = rsAttributes("Currency")

	ReleaseRecordset rsAttributes
End Function

' ################################################################
'
Sub Perform_Processing_BasedOnAction ()

	Select Case g_sAction
	Case "submitNew"
		Process_SubmitNEW

	Case "submitEdit", "submitEditF"
		Process_SubmitEDIT

	Case "add"
		Process_ADD

	Case "open"
		Process_OPEN

	End Select
End Sub

' ################################################################
'
Sub Process_SubmitNEW ()

	Dim sCreationError

	g_sOldCtName = g_sCtName
	'Create the new definition
	sCreationError = sCreateNewCustomCatalog
	If (sCreationError <> "") Then
		g_sActionType = "submitNew"
		g_sStatusText = L_CustomCatalogNew_HTMLText
		g_sHeader = sFormatString (L_CustomCatalog_HTMLText, Array(L_New_HTMLText))
	Else
		g_sActionType = "submitEdit"
		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sCtName))
		g_sHeader = sFormatString(L_CustomCatalog_HTMLText, Array(g_sCtName))
	End If
End Sub

' ################################################################
'
Sub Process_SubmitEDIT ()

	Dim sUpdateError

	g_sActionType = "submitEdit"
	g_sOldCtName = Request.Form("old_name")
	'Update the definition
	sUpdateError = sUpdateCustomCatalog
	If (sUpdateError <> "") Then
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sCtName))
	Else
		g_sOldCtName = g_sCtName
		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sCtName))
	End If

	g_sHeader = sFormatString (L_CustomCatalog_HTMLText, Array(g_sCtName))
End Sub

' ################################################################
'
Sub Process_ADD ()

	Dim iType
	Dim aFields
	Dim sField

	g_sActionType = "submitNew"

	g_sCtName = ""
	g_sOldCtName = ""
	g_sHeader = sFormatString (L_CustomCatalog_HTMLText, Array(L_New_HTMLText))
End Sub

' ################################################################
'
Sub Process_OPEN ()

	g_sActionType = "submitEdit"

	g_sOldCtName = g_sCtName
	g_sHeader = sFormatString (L_CustomCatalog_HTMLText, Array(g_sCtName))
End Sub

' ##################################################################
' Function: sCreateNewCustomCatalog
'	This function creates a new custom catalog
'
' Returns:
'	string error message if error
'
Function sCreateNewCustomCatalog ()

	Dim rsAttributes

	On Error Resume Next
	Set rsAttributes = g_oCat.CreateCustomCatalog (g_sCtName)
	ReleaseRecordset rsAttributes
	If (Err.Number <> 0) Then
		sCreateNewCustomCatalog = Err.Description
		Call setError (L_CreateCustomCatalog_DialogTitle, sFormatString(L_ErrorCreatingCustomCatalog_ErrorMessage, Array(g_sCtName)), sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	Else
		SetCategoryCustomPrice
	End If
End Function

' ##################################################################
' Function: sUpdateCustomCatalog
'	This function updates a custom catalog
'
' Returns:
'	string error message if error
'
Function sUpdateCustomCatalog ()

	Dim rsAttributes
	Dim bForceUpdate

	If (g_sOldCtName <> g_sCtName) Then
		Set rsAttributes = g_oCat.GetCustomCatalogAttributes(g_sOldCtName)
		If (Not rsAttributes.EOF) Or (Err.Number > 0) Then
			rsAttributes("CustomCatalogName") = g_sCtName

			If (g_sAction = "submitEditF") Then
				bForceUpdate = True
			Else
				bForceUpdate = False
			End If

			On Error Resume Next
			g_oCat.SetCustomCatalogAttributes rsAttributes, bForceUpdate
			CheckForPropertyAlreadyModified
		Else
			rsAttributes.Close
		End If

		Set rsAttributes = Nothing
	End If

	If (Err.Number = 0) Then
		SetCategoryCustomPrice
		CheckForPropertyAlreadyModified
	Else
		sUpdateCustomCatalog = Err.Description
	End If
End Function

' ################################################################
'
Sub SetCategoryCustomPrice ()

	Dim oXML
	Dim oXMLNode
	Dim oPricing
	Dim oXMLNodeList

	Dim fpPrice
	Dim sModified
	Dim iCustomPrice
	Dim sCategoryName

	Set oXML = Server.CreateObject("MSXML.DOMDocument")
	oXML.async = False
	oXML.loadXML g_sTVData
	Set oXMLNode = oXML.documentElement

	If Not (oXMLNode Is Nothing) Then
		Set oXMLNodeList = oXMLNode.selectNodes ("//category")
		For Each oXMLNode In oXMLNodeList
			Set oPricing = oXMLNode.selectSingleNode("./pricing")
			sModified = oPricing.getAttribute("modified")
			If (sModified = "yes") Then
				sCategoryName = oXMLNode.getAttribute("caption")
				iCustomPrice = CInt(oPricing.getAttribute("type"))
				fpPrice = variantValue(oPricing.text, FLOAT_TYPE)
				If (fpPrice = 0) or IsNull (fpPrice) Then
					iCustomPrice = 0
					fpPrice = 0
				End If

				Call g_oCat.SetCategoryCustomPrice (g_sCtName, sCategoryName, iCustomPrice, fpPrice)
			End If
		Next
	End If

	Set oXML = Nothing
End Sub

' ################################################################
'
Sub GetTVData ()

	Dim rsCategories

	g_sTVData = ""

	Set rsCategories = g_oCat.RootCategories("CategoryName")
	Do Until rsCategories.EOF
		GetCategoryForViewCustomCatalogPage rsCategories("CategoryName").Value
		rsCategories.MoveNext
	Loop

	ReleaseRecordset rsCategories
End Sub

%>