<!--#INCLUDE FILE="../../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<%
' ####################################################################
'   QBProperties.asp
'
'   Description:
'		This page returns he PRODUCT CATALOG profile 
' ####################################################################

	On Error Resume Next

	' ------ CATALOG DataType Enum
	Const INTEGER_TYPE = 0
	Const BIGINTEGER_TYPE = 1
	Const FLOAT_TYPE = 2
	Const DOUBLE_TYPE = 3
	Const BOOLEAN_TYPE = 4
	Const STRING_TYPE = 5
	Const DATETIME_TYPE = 6
	Const CURRENCY_TYPE = 7
	Const FILEPATH_TYPE = 8
	Const ENUMERATION_TYPE = 9

	Const PAGE_TYPE = "RESPONSE"

	' ------ PUBLIC Declarations
	Public g_oCatMgr		' The Catalog Manager Object
	Public g_rsProperties	' List of all Catalog PROPERTIES
%>
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->

<Document>
  <Catalog displayName="ProductCatalogXML" name="ProductCatalogXML">
    <Profile name="productProduct" displayName="<%= L_Product_Text %>">
<%= sGetCatalogProfileXML() %>
    </Profile>
  </Catalog>
</Document>

<%
' ##################################################################
' Sub: InitializeCatalogObject
'	Create and initialize a global Catalog Manager Object
'
Sub InitializeCatalogObject ()

	On Error Resume Next
	Set g_oCatMgr = Nothing

	If (True = IsObject(Session("CatalogManager"))) Then
		Set g_oCatMgr = Session("CatalogManager")
	End If

	If (g_oCatMgr Is Nothing) Then
		Dim sCatalogInitString

		sCatalogInitString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")
		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (sCatalogInitString, True)
		Set Session("CatalogManager") = g_oCatMgr
	End If
End Sub

' #####################################################################
' Function: sConvertToQBTypes
'	Converts from product catalog data types to query builder data types
'
' Input:
'	iType
'
' Returns:
'	String query builder type
'
Function sConvertToQBTypes (ByVal iType)

    Select Case iType
	Case STRING_TYPE
		sConvertToQBTypes = "string"
	Case INTEGER_TYPE
		sConvertToQBTypes = "NUMBER"
	Case FLOAT_TYPE
		sConvertToQBTypes = "float"
	Case CURRENCY_TYPE
		sConvertToQBTypes = "currency"
	Case DATETIME_TYPE
		sConvertToQBTypes = "date"
	Case ENUMERATION_TYPE
		sConvertToQBTypes = "siteterm"
	Case Else
		sConvertToQBTypes = "string"
	End Select
End Function

' #####################################################################
'
Function sGetCatalogProfileXML ()

	Dim sTxt
	Dim sDisplayName
	Dim sPropertyName
	Dim sPropertyType
	Dim fdPropertyName
	Dim fdPropertyType

	InitializeCatalogObject
	Set g_rsProperties = g_oCatMgr.Properties
	Set fdPropertyName = g_rsProperties("PropertyName")
	Set fdPropertyType = g_rsProperties("DataType")
	''--- Add List Price as first Property
	sTxt = "<Property name=""cy_list_price"" displayName=""" & L_ListPrice_Text & """ propType=""currency"" />"

	While Not g_rsProperties.EOF
		sPropertyName = Server.HTMLEncode (fdPropertyName.Value)
		sPropertyType = sConvertToQBTypes (fdPropertyType.Value)

        sTxt = sTxt & "<Property name=""" & sPropertyName & """ displayName=""" & sPropertyName & _
				""" propType=""" & sPropertyType & """"

		If (sPropertyType = "siteterm") Then
			sTxt = sTxt & " referenceString=""" & "MSCSEnumValues." & sPropertyName & """"
		End If
		sTxt = sTxt & " />"

		g_rsProperties.MoveNext
	Wend

	g_rsProperties.Close
	Set g_rsProperties = Nothing
	Set g_oCatMgr = Nothing

	sGetCatalogProfileXML = sTxt
End Function

%>