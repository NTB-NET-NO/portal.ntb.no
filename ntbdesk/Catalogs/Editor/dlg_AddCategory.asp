<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="catEditorSTRINGS.asp" -->
<!--#INCLUDE FILE="common.asp" -->

<%
	Const PAGE_TYPE = "DIALOG"
'	#############################
	Perform_ServerSide_Processing
'	#############################	
%>
<HTML>
<HEAD>

<TITLE><%= L_NewCategory_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
	BODY
	{
		PADDING:15px;
		MARGIN:0;
	}
	BUTTON, INPUT
	{
		WIDTH: 6em
	}
</STYLE>

<SCRIPT LANGUAGE='VBScript'>

	option explicit

	sub bdcancel_OnClick ()
		window.returnValue = ""
		window.close
	end sub

	sub bdcontinue_OnClick ()
		window.close
	end sub

	sub onSelect()
		window.returnValue = definitions.text
		bdcontinue.disabled = False 
		bdcontinue.focus
	end sub

	sub onUnselect()
		window.returnValue = ""
		bdcontinue.disabled = true 
	end sub

	sub window_onLoad()
		window.returnValue = ""
		bdcancel.focus
	end sub

</SCRIPT>
</HEAD>

<BODY LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<TABLE>
	<TR>
		<TD STYLE="width:10px">
		<TD>
			<LABEL FOR='definitions'><%= L_SelectCategoryType_Text %></LABEL><BR><BR>
			<div ID="definitions"
				CLASS="listBox" STYLE="WIDTH: 258px; HEIGHT: 100px"
				LANGUAGE="vbscript"
				ONSELECT="onSelect"
				ONUNSELECT="onUnselect">
				<%= g_sCategoryDefinitions %>
			</div>
		</TD>
	</TR>
</TABLE>
<BR>
<TABLE>
	<TR>
		<TD STYLE='width:230px'></TD>
		<TD><BUTTON ID='bdcontinue' CLASS='bdbutton' DISABLED><%= L_Continue_Button %></BUTTON>
			<BUTTON ID='bdcancel' CLASS='bdbutton'><%= L_Cancel_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>

<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

' ------ PUBLIC Declarations
Public g_sCategoryDefinitions

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	Dim rsDefinitions

	InitializeCatalogObject
	Set rsDefinitions = g_oCatMgr.CategoryDefinitions

	If (True = rsDefinitions.EOF) Then
		g_sCategoryDefinitions = "<DIV VALUE=''>" & L_NoneAvailable_Text & "</DIV>" & vbCR
	Else
		While Not rsDefinitions.EOF
			g_sCategoryDefinitions = g_sCategoryDefinitions & _
				"<DIV VALUE=''>" & rsDefinitions("DefinitionName") & "</DIV>" & vbCR

			rsDefinitions.MoveNext
		Wend
	End If

	Set g_oCatMgr = Nothing
End Sub

%>