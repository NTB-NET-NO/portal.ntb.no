<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<% 
' #####################################################################
'
'   list_Catalogs.ASP      
'
'	Item Select Page for Catalogs
'
' #####################################################################

	Const PAGE_TYPE = "LIST"

	On Error Resume Next
%>
<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->
<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

' ----- CONST Declarations
const SELECT_NO = 0
const SELECT_CATALOG = 1
const SELECT_CUSTCATALOG = 2

' ----- PUBLIC Declarations
public g_bLocked
public g_oSelection
public g_iSelectionType		' SELECT_NO|SELECT_CATALOG|SELECT_CUSTCATALOG
public g_sSelectedCatalog

sub window_onLoad ()

	g_bLocked = False
	g_iSelectionType = SELECT_NO

<% If g_bImporting Then %>
	call SetTemp("bCatImporting", true)
<% End If %>
	if GetTemp("bCatImporting") then
		bImportToggle = true
		nImportCount = 0
		oImportTimer = window.setInterval("ImportProgress()", BLINK_SPEED * 1000)
		call ImportProgress ()
	end if

' Look for bad connection string
<% If (Not g_oCatMgr Is Nothing) Then %>
	EnableAllTaskMenuItems "new", True
	EnableAllTaskMenuItems "import", True
	EnableTaskButtons
<% Else %>
	DisableTask "new"
<% End If %>
end sub

sub onSelectRow ()

	if (g_bLocked = True) then exit sub

	if (g_iSelectionType = SELECT_NO) then
		EnableTask "open"
		EnableTask "delete"
	end if

    set g_oSelection = window.event.XMLrecord
	' enable o(pen) and d(elete) buttons on the task bar
	g_sSelectedCatalog = g_oSelection.selectSingleNode("./name").text

	if (g_oSelection.selectSingleNode("./node_type").text = "custom_catalog") then
		g_iSelectionType = SELECT_CUSTCATALOG
	else
		g_iSelectionType = SELECT_CATALOG

		EnableTask "export"
		EnableAllTaskMenuItems "export", True
		EnableTask "send"
	end if

    setStatusText sFormatString (L_Selected_StatusBar, Array(g_sSelectedCatalog))
end sub

sub onDeselectRow ()

	DisableTask "open"
	DisableTask "delete"
	DisableTask "send"

	if (g_iSelectionType <> SELECT_CUSTCATALOG) then _
		DisableTask "export"

	g_sSelectedCatalog = ""
	g_iSelectionType = SELECT_NO
	setStatusText ""
end sub

sub onOpen ()

	DisableTaskButtons
	selectform.ct_name.value = g_sSelectedCatalog

	if (g_iSelectionType = SELECT_CUSTCATALOG) then

		selectform.base.value = g_oSelection.selectSingleNode("../name").text
		selectform.action = "edit_CustomCatalog.asp"
	end if

	selectform.submit
end sub

sub onDelete ()

	dim iBtnClicked
	dim sBaseCatalog

	iBtnClicked = MsgBox (sFormatString(L_Delete_Text, Array(g_sSelectedCatalog)), vbYesNo, L_ConfirmDelete_Text)
	if (iBtnClicked = vbYes) then
		g_bLocked = True

		if (g_iSelectionType = SELECT_CATALOG) then
			deleteform.ct_name.value = g_sSelectedCatalog
		else
			sBaseCatalog = g_oSelection.selectSingleNode("../name").text
			deleteform.ct_name.value = sBaseCatalog & SEPARATOR1 & g_sSelectedCatalog
		end if
		deleteform.submit
	else
		EnableTaskButtons
	end if
end sub

sub EnableTaskButtons ()

	EnableTask "new"
	EnableTask "import"
	EnableTask "refresh"
	if (g_iSelectionType <> SELECT_NO) then
		EnableTask "open"
		EnableTask "delete"

		if (g_iSelectionType = SELECT_CATALOG) then
			EnableTask "export"
			EnableTask "send"
		end if
	end if
end sub

sub DisableTaskButtons ()

	DisableTask "new"
	DisableTask "import"
	DisableTask "refresh"
	DisableTask "open"
	DisableTask "delete"
	DisableTask "export"
	DisableTask "send"
end sub

sub onImportXML ()

	dim aParams
	dim sResponse

	sResponse = window.showModalDialog("dlg_ImportXMLCatalog.asp", , _
				"dialogHeight:180px;dialogWidth:420px;status:no;help:no")
	if (sResponse <> "") then
		aParams = Split (sResponse, SEPARATOR1)

		with import_xml_form
			.file_name.value = aParams(0)
			.blow_away.value = aParams(1)

			.submit()
		end with
	else
		EnableTaskButtons
	end if
end sub

sub onImportCSV ()

	dim aParams
	dim sResponse

	sResponse = window.showModalDialog("dlg_ImportCSVCatalog.asp", , _
				"dialogHeight:270px;dialogWidth:420px;status:no;help:no")
	if (sResponse <> "") then
		aParams = Split (sResponse, SEPARATOR1)

		with import_csv_form
			.file_name.value = aParams(0)
			.ct_name.value = aParams(1)
			.product_id.value = aParams(2)
			.price_id.value = aParams(3)
			.blow_away.value = aParams(4)

			.submit()
		end with
	else
		EnableTaskButtons
	end if
end sub

sub onExportXML ()

	onExport ("XML")
end sub

sub onExportCSV ()

	onExport ("CSV")
end sub

sub onExport (ByRef sType)

	dim sHeight
	dim sResponse
	dim sExportForm

	select case sType
	case "XML"
		sHeight = "150"
	case "CSV"
		sHeight = "180"
	end select

	sExportForm = "export_" & sType & "_form"
	sResponse = window.showModalDialog("dlg_ExportCatalog.asp?type=" & sType, , _
				"dialogHeight:" & sHeight & "px;dialogWidth:420px;status:no;help:no")
	if (sResponse <> "") then
		with document.all(sExportForm)
			.ct_name.value = g_sSelectedCatalog
			.file_name.value = sResponse

			.submit()
		end with
	else
		EnableTaskButtons
	end if
end sub

sub onSend ()

	dim aResponse
	dim sResponse

	sResponse =  window.showModalDialog("dlg_VendorPicker.asp?mode=send", , _
										"dialogHeight:250px;dialogWidth:340px;status:no;help:no")
	if (sResponse = "") then
		EnableTaskButtons
		exit sub
	end if
	aResponse = Split (sResponse, SEPARATOR1)
	if (aResponse(0) = L_None_Text) then
		EnableTaskButtons
		exit sub
	end if

	with sendform
		.ct_name.value = g_sSelectedCatalog
		.vendor_name.value = aResponse(0)
		.vendor_id.value = aResponse(1)
		.vendor_value.value = aResponse(2)
		.submit()
	end with
end sub

sub onNewCustomCatalog ()

	dim sResponse

	sResponse = window.showModalDialog("dlg_AddCatalog.asp", , _
				"dialogHeight:220px;dialogWidth:420px;status:no;help:no")
	if (sResponse = "") then
		EnableTaskButtons
	else
		with addcustform
			.base.value = sResponse
			.submit()
		end with
	end if
end sub

sub onRefresh ()

	setStatusText L_RefreshingCatalogsWait_Message
	SetWaitDisplayText(L_RefreshingCatalogs_Message)
	ShowWaitDisplay()
	call setTimeout ("Refresh1()", 1)
end sub

sub Refresh1()

	dim sErrMessage

	' perform server-side refresh tasks
	call RefreshCatalogs(sErrMessage)
	if sErrMessage <> "" then _
		showErrorDlg L_Error_DialogTitle, _
					 L_ErrorRefreshingCatalogs_ErrorMessage, _
					 sErrMessage, _
					 ERROR_ICON_ALERT

	setStatusText L_RefreshingCatalogCaches_Message
	ShowWaitDisplay()
	call setTimeout ("Refresh2()", 1)
end sub

sub Refresh2 ()

	dim sURL
	dim sErrMessage

	dim aApplicationURLs

	sURL = "<%= sApplicationURLs () %>"
	aApplicationURLs = Split (sURL, SEPARATOR1)

	for each sURL in aApplicationURLs
		call RefreshCatalogCache (sURL, sErrMessage)
		if sErrMessage <> "" then _
			showErrorDlg L_Error_DialogTitle, _
						 sFormatString (L_ErrorRefreshingCatalogCache_ErrorMessage, Array(sURL)), _
						 sErrMessage, _
						 ERROR_ICON_ALERT
	next

	EnableAllTaskMenuItems "new", True
	EnableAllTaskMenuItems "import", True
	EnableTaskButtons
	setStatusText ""
end sub

sub RefreshCatalogs (ByRef sErrMessage)

	dim sURL
	dim bAsync
	dim oHTTPRequest

	bAsync = False
	sURL = "refresh_Catalogs.asp"

' To prevent troubles due to cancellation of Refresh operation
' through IE Intranet Security <> Low
On Error Resume Next

	set oHTTPRequest = CreateObject ("MSXML2.XMLHTTP.2.6")
	oHTTPRequest.open "POST", sURL, bAsync
	oHTTPRequest.send ""

	sErrMessage = oHTTPRequest.responseText
end sub

sub RefreshCatalogCache (ByRef sApplicationName, ByRef sErrMessage)

	dim sURL
	dim bAsync
	dim oHTTPRequest

	bAsync = False
	sURL = sApplicationName & "/BDRefresh.asp?cache_name=CatalogCache"

' To prevent troubles due to cancellation of Refresh operation
' through IE Intranet Security <> Low
On Error Resume Next
	set oHTTPRequest = CreateObject ("MSXML2.XMLHTTP.2.6")
	oHTTPRequest.open "POST", sURL, bAsync
	oHTTPRequest.send ""
	if (err.number <> 0) then
		showErrorDlg L_Error_DialogTitle, _
					 sFormatString (L_ErrorRefreshingCatalogCache_ErrorMessage, Array(sApplicationName)), _
					 sGetScriptError (Err), _
					 ERROR_ICON_ALERT
		err.clear
	end if

	sErrMessage = oHTTPRequest.responseText
end sub

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) And (Not g_oCatMgr Is Nothing) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertTaskBar L_Catalogs_HTMLText, g_sStatusText
%>

<xml id='lsData'>
	<document>
		<%= g_sMasterXML %>
	</document>
</xml>

<xml id='lsMeta'>
	<listsheet>
	    <global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_nCatalogs %>'
            selection='single' emptyprompt='no' sort='no' />
	    <columns>
	        <column id='name' id2='name' sortdir='asc' width='100'><%= L_Name_Text %></column>
	        <column id='node_type' id2='node_type' hide='yes'></column>
	    </columns>
   	    <operations>
			<newpage formid='newpageform'/>
		</operations>
	</listsheet>
</xml>

<DIV ID='bdcontentarea' CLASS='listSheetContainer'>
	<INPUT TYPE="Hidden" NAME="page_id">

	<DIV ID='ctListSheet'
		CLASS='listSheet' STYLE="MARGIN:20px; HEIGHT:80%"
		DataXML='lsData'
		MetaXML='lsMeta'
		LANGUAGE='VBScript'
		OnRowSelect='onSelectRow'
		OnRowUnselect='onDeselectRow'><%= L_LoadingWidget_Text %>
	</DIV>
</DIV>

<FORM ID="newpageform" ACTION='response_Catalogs.asp'>
	<INPUT TYPE="Hidden" NAME="type" VALUE="newpage">
</FORM>
<FORM ACTION="" METHOD="POST" ID="addform">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
</FORM>
<FORM ACTION="" METHOD="POST" ID="addcustform" ONTASK="onNewCustomCatalog">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="base">
</FORM>
<FORM ACTION="" METHOD="POST" ID="selectform" ONTASK='onOpen'>
	<INPUT TYPE="Hidden" NAME="type" VALUE="open">
	<INPUT TYPE="Hidden" NAME="ct_name">
	<INPUT TYPE="Hidden" NAME="base">
	<INPUT TYPE="Hidden" NAME="page_id1" VALUE="1">
	<INPUT TYPE="Hidden" NAME="page_id2" VALUE="1">
</FORM>
<FORM ACTION="" METHOD="POST" ID="deleteform" ONTASK='onDelete'>
	<INPUT TYPE="Hidden" NAME="type" VALUE="delete">
	<INPUT TYPE="Hidden" NAME="ct_name">
</FORM>
<FORM ACTION="" METHOD="POST" ID="import_xml_form" ONTASK="onImportXML">
	<INPUT TYPE="Hidden" NAME="type" VALUE="import_xml">
	<INPUT TYPE="Hidden" NAME="file_name">
	<INPUT TYPE="Hidden" NAME="blow_away">
</FORM>
<FORM ACTION="" METHOD="POST" ID="import_csv_form" ONTASK="onImportCSV">
	<INPUT TYPE="Hidden" NAME="type" VALUE="import_csv">
	<INPUT TYPE="Hidden" NAME="file_name">
	<INPUT TYPE="Hidden" NAME="ct_name">
	<INPUT TYPE="Hidden" NAME="product_id">
	<INPUT TYPE="Hidden" NAME="price_id">
	<INPUT TYPE="Hidden" NAME="blow_away">
</FORM>
<FORM ACTION="" METHOD="POST" ID="export_xml_form" ONTASK="onExportXML">
	<INPUT TYPE="Hidden" NAME="type" VALUE="export_xml">
	<INPUT TYPE="Hidden" NAME="ct_name">
	<INPUT TYPE="Hidden" NAME="file_name">
</FORM>
<FORM ACTION="" METHOD="POST" ID="export_csv_form" ONTASK="onExportCSV">
	<INPUT TYPE="Hidden" NAME="type" VALUE="export_csv">
	<INPUT TYPE="Hidden" NAME="ct_name">
	<INPUT TYPE="Hidden" NAME="file_name">
</FORM>
<FORM ACTION="" METHOD="POST" ID="sendform" ONTASK="onSend">
	<INPUT TYPE="Hidden" NAME="type" VALUE="send">
	<INPUT TYPE="Hidden" NAME="ct_name">
	<INPUT TYPE="Hidden" NAME="vendor_name">
	<INPUT TYPE="Hidden" NAME="vendor_id">
	<INPUT TYPE="Hidden" NAME="vendor_value">
</FORM>
<FORM ACTION="" METHOD="POST" ID="refreshform" ONTASK="onRefresh">
	<INPUT TYPE="Hidden" NAME="type" VALUE="refresh">
</FORM>

</BODY>
</HTML>
<%
	'' -----------------
	ReleaseGlobalObjects
	'' -----------------
%>


<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'----- CONST  Declarations
Const CATALOG_CACHE_TABLE = "CatalogCache_Virtual_Directory"

'----- PUBLIC Declarations
Public g_sAction
Public g_sMasterXML
Public g_sStatusText ' To be displayed in the Status Bar

Public g_rsCatalogs		' the recordset to hold the list of catalogs

Public g_nPage
Public g_nCatalogs	' total number of catalogs

public g_bImporting

' ###########################################################
'
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables
	If Err.Number <> 0 Then Exit Sub

	Perform_Processing_BasedOnAction
End Sub

' ############################################################
'
Sub InitializeGlobalVariables ()

	g_sAction = Request.Form("type")
	g_nPage   = Request.Form("page_id")
	If IsEmpty(g_nPage) Then g_nPage = 1
	g_bImporting = false
End Sub

' ############################################################
'
Sub Perform_Processing_BasedOnAction ()

	On Error Resume Next

	Select Case g_sAction
	Case "delete"
		DeleteCatalog Request.Form("ct_name")

	Case "refresh"
		Process_REFRESH

	Case "import_xml"
		Process_ImportXML

	Case "import_csv"
		Process_ImportCSV

	Case "export_xml"
		Process_EXPORT "XML"

	Case "export_csv"
		Process_EXPORT "CSV"

	Case "send"
		Process_SEND
	End Select

	Set g_rsCatalogs = g_oCatMgr.Catalogs
	GetMasterDetailXML
End Sub

' ############################################################
'
Function sApplicationURLs ()

	Dim aURLs()
	Dim sSQLQuery
	Dim sConnectionString

	Dim rsApplications

	If (Err.Number <> 0) Then Exit Function

	sSQLQuery = "SELECT "
	sSQLQuery = sSQLQuery & "u_vdir_url AS URL"
	sSQLQuery = sSQLQuery & " FROM " & CATALOG_CACHE_TABLE

	sConnectionString = GetSiteConfigField("transaction config", "connstr_db_TransactionConfig")
	Set rsApplications = rsGetRecordset (sConnectionString, _
										 sSQLQuery, _
										 AD_OPEN_FORWARD_ONLY, _
										 AD_LOCK_READ_ONLY)

	ReDim aURLs(-1)
	Do Until rsApplications.EOF
		Call AddArray (aURLs, rsApplications("URL").Value)
		rsApplications.MoveNext
	Loop
	ReleaseRecordset rsApplications

	' PRE: sApplicationURLs = ""
	If (UBound(aURLs) <> -1) Then _
		sApplicationURLs = Join (aURLs, SEPARATOR1)
End Function

' ###############################################################
' Sub: DeleteCatalog
'	Deletes a catalog or custom catalog
'
' Input:
'	sCatalogs - catalog to delete / parent + custom catalog
'
Sub DeleteCatalog (ByRef sCatalogs)

	Dim oCatalog
	Dim oCatalogSets

	Dim sCatalog
	Dim aCatalogs
	Dim mscsTransactionConfigConnStr

	aCatalogs = Split (sCatalogs, SEPARATOR1)
	sCatalog = aCatalogs(0)

	On Error Resume Next
	If (UBound(aCatalogs) = 0) Then
		g_oCatMgr.DeleteCatalog sCatalog
		If (Err.Number <> 0) Then
			Call setError (L_DeleteCatalog_DialogTitle, sFormatString(L_ErrorDeletingCatalog_ErrorMessage, Array (sCatalog)), sGetScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
			g_sStatusText = sFormatString (L_ErrorDeleting_StatusBar, Array(sCatalog))
			Exit Sub
		Else
			DeleteCatalogVendor sCatalog
			Set oCatalogSets = Server.CreateObject ("Commerce.CatalogSets")
				If IsEmpty (g_sConnString) Then _
					g_sConnString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")
				mscsTransactionConfigConnStr = GetSiteConfigField("Transaction Config", "connstr_db_TransactionConfig")

				oCatalogSets.Initialize g_sConnString, mscsTransactionConfigConnStr
				oCatalogSets.RemoveCatalogFromCatalogSets sCatalog
			Set oCatalogSets = Nothing
		End If
	Else'If(UBound(aCatalogs) = 2) Then
		Set oCatalog = g_oCatMgr.GetCatalog(sCatalog)
		If (oCatalog Is Nothing) Then Exit Sub

		sCatalog = aCatalogs(1)
		oCatalog.DeleteCustomCatalog sCatalog
		Set oCatalog = Nothing
	End If

	g_sStatusText = sFormatString (L_Deleted_StatusBar, Array(sCatalog))
End Sub

' ###############################################################
'
Sub Process_ImportXML ()

	Dim sFilePath
	Dim bUpdateOnly

	sFilePath = sGetFilePath()
	bUpdateOnly = Not CBool(Request.Form("blow_away"))

	On Error Resume Next
	g_oCatMgr.ImportXML sFilePath, bUpdateOnly
	If (Err.Number <> 0) Then
		Call setError (L_ImportXMLCatalog_DialogTitle, sFormatString (L_ErrorImportingCatalog_ErrorMessage, Array (sFilePath)), sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	Else
		Call setError (L_ImportXMLCatalog_DialogTitle, L_StartImport_Message, "", ERROR_ICON_INFORMATION)
		g_bImporting = true
	End If
End Sub

' ###############################################################
'
Sub Process_ImportCSV ()

	Dim sCatalog
	Dim sPriceID
	Dim sFilePath
	Dim sProductID

	Dim bUpdateOnly

	sFilePath = sGetFilePath()
	sCatalog = Request.Form("ct_name")
	sProductID = Request.Form("product_id")
	sPriceID = Request.Form("price_id")

	bUpdateOnly = Not CBool(Request.Form("blow_away"))

	On Error Resume Next
	g_oCatMgr.ImportCSV sFilePath, sProductID, sPriceID, sCatalog, bUpdateOnly
	If (Err.Number <> 0) Then
		Call setError (L_ImportCSVCatalog_DialogTitle, sFormatString (L_ErrorImportingCatalog_ErrorMessage, Array (sFilePath)), sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	Else
		Call setError (L_ImportCSVCatalog_DialogTitle, L_StartImport_Message, "", ERROR_ICON_INFORMATION)
		g_bImporting = true
	End If
End Sub

' ###############################################################
'
Sub Process_EXPORT(ByRef sExportType)

	Dim sTitle
	Dim sMethod
	Dim sCatalog
	Dim sFilePath

	sCatalog = Request.Form("ct_name")
	sFilePath = sGetFilePath()
On Error Resume Next
	' ----- Distinguish between the two types of EXPORT
	Select Case sExportType
	Case "CSV"
		' ----- EXPORT CSV File
		sTitle = L_ExportCSVCatalog_DialogTitle
		g_oCatMgr.ExportCSV sFilePath, sCatalog
	Case "XML"
		' ----- EXPORT XML File
		sTitle = L_ExportXMLCatalog_DialogTitle
		g_oCatMgr.ExportXML sFilePath, sCatalog
	End Select

	If (Err.Number <> 0) Then
		Call setError (sTitle, sFormatString (L_ErrorExportingCatalog_ErrorMessage, Array (sCatalog)), sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	Else
		Call setError (sTitle, L_StartExport_Message, "", ERROR_ICON_INFORMATION)
	End If
End Sub

' ###############################################################
'
Sub Process_SEND ()

	Dim sResult
	Dim sFilePath
	Dim sVendorName
	Dim sCatalogName
	Dim sBTOrderDocType
	Dim sBTSubmitTypeQueue
	Dim sBTSourceQualifierID
	Dim sBTSourceQualifierValue
	Dim sDestinationQualifierID
	Dim sDestinationQualifierValue

	Dim oFileSystem
	Dim oXMLDocument
	Dim oInterchange
	Dim oCatalogVendor

	Dim rsVendorInfo

	On Error Resume Next
	Set oInterchange = Server.CreateObject ("BizTalk.Interchange")
	If (oInterchange Is Nothing) Then Exit Sub

	sFilePath = "catalog" & CStr(Date) & ".xml"
	sFilePath = Server.MapPath (g_sBizDeskRoot &  Replace (sFilePath, "/", ""))
	sCatalogName = Request.Form("ct_name")

	Call g_oCatMgr.ExportXML (sFilePath, sCatalogName, True)

	Set oXMLDocument = Server.CreateObject("Microsoft.XMLDOM")
	oXMLDocument.async = False
	oXMLDocument.validateOnParse = False
	oXMLDocument.resolveExternals = False
	Call oXMLDocument.Load (sFilePath)

	sBTOrderDocType		 = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkCatalogDocType")
	sBTSubmitTypeQueue	 = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkSubmitTypeQueue")
	sBTSourceQualifierID = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkSourceQualifierID")
	sBTSourceQualifierValue = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkSourceQualifierValue")

	sDestinationQualifierID = Request.Form("vendor_id")
	sDestinationQualifierValue = Request.Form("vendor_value")

	sResult = oInterchange.Submit ( _
									 sBTSubmitTypeQueue, _
									 oXMLDocument.xml, _
									 sBTOrderDocType, _
									 sBTSourceQualifierID, _
									 sBTSourceQualifierValue, _
									 sDestinationQualifierID, _
									 sDestinationQualifierValue _
								  )
	Set oXMLDocument = Nothing
	Set oInterchange = Nothing
	'
	' Delete XML (file) representation of the catalog
	'
	Set oFileSystem = Server.CreateObject ("Scripting.FileSystemObject")
		Call oFileSystem.DeleteFile (sFilePath)
	Set oFileSystem = Nothing
End Sub

' ###############################################################
'
Sub GetMasterDetailXML ()

	Dim i
	Dim iLast
	Dim sCatalogName
	Dim rsCustomCatalogs

	If (g_rsCatalogs.EOF) Then
		g_sMasterXML = "<record/>"
		Exit Sub
	End If

	iLast = PAGE_SIZE * g_nPage
	g_nCatalogs = iLast - PAGE_SIZE
	'' --- Skip first g_nCatalogs
	Do Until (g_rsCatalogs.EOF = True) Or (i = g_nCatalogs)
		If (Not g_rsCatalogs.Fields("CustomCatalog")) Then i = i + 1
		g_rsCatalogs.MoveNext
	Loop

	Set rsCustomCatalogs = g_oCatMgr.CustomCatalogs
	Do Until (g_rsCatalogs.EOF = True) Or (g_nCatalogs >= iLast)
		If (Not g_rsCatalogs.Fields("CustomCatalog")) Then
			sCatalogName = g_rsCatalogs.Fields("CatalogName")
			g_sMasterXML = g_sMasterXML & _
							"<record>" & _
							"<name>" & Server.HTMLEncode(sCatalogName) & "</name>" & _
							"<node_type>catalog</node_type>" & _
							sGetDetailXML (sCatalogName, rsCustomCatalogs) & _
							"</record>"
			g_nCatalogs = g_nCatalogs + 1
		End If

		g_rsCatalogs.MoveNext
	Loop
	ReleaseRecordset rsCustomCatalogs
	Do Until g_rsCatalogs.EOF
		If (Not g_rsCatalogs.Fields("CustomCatalog")) Then g_nCatalogs = g_nCatalogs + 1
		g_rsCatalogs.MoveNext
	Loop

	If (g_nCatalogs = 0) Then g_sMasterXML = "<record/>"
End Sub

' ###############################################################
'
Function sGetDetailXML (ByRef sCatalogName, ByRef rsCustomCatalogs)

	If (True = rsCustomCatalogs.EOF) Then Exit Function

	While Not rsCustomCatalogs.EOF
		If (rsCustomCatalogs("CatalogName") = sCatalogName) Then
			sGetDetailXML = sGetDetailXML & _
				"<record><name>" & Server.HTMLEncode(rsCustomCatalogs("CustomCatalogName").Value) & "</name>" & _
				"<node_type>custom_catalog</node_type></record>"
		End If

		rsCustomCatalogs.MoveNext
	Wend

	rsCustomCatalogs.MoveFirst
End Function

' ###############################################################
'
Sub ReleaseGlobalObjects ()

	ReleaseRecordset g_rsCatalogs
	Set g_oCatMgr = Nothing
End Sub

' ###############################################################
'
Function sGetFilePath ()

	Const SHARE_NAME = "CommerceCatalogs"

	sGetFilePath = Request.Form("file_name")
	If (0 = InStr (sGetFilePath, "\")) And (0 = InStr(sGetFilePath, ":")) Then
		sGetFilePath = "\\" & Request.ServerVariables("SERVER_NAME") & "\" & SHARE_NAME & "\" & sGetFilePath
	End If
End Function

%>