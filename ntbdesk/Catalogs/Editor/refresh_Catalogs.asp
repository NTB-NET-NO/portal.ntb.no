<!--#INCLUDE FILE='../../include/BDHeader.asp' -->
<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<%
'' --------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright (c) 1996-2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			refresh_Catalogs.asp
''
''	Description:	Refresh page for Catalogs.
''
'' --------------------------------------------------------------------

	Server.ScriptTimeout = 60*60*2

	Public g_oCatMgr	' CatalogManager object
	Public g_rsCatalogs ' Catalogs Recordset

	On Error Resume Next
	InitializeCatalogObject
	If Not (g_oCatMgr Is Nothing) Then Process_REFRESH
%>
<%

'' -------------------------------------------------------------------
''
''	Procedure:		IntializeCatalogObject
''
''	Description:	Creates and initializes an instance of a
''					CatalogManager object.
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeCatalogObject ()

	Dim sConnString

	On Error Resume Next
	Set g_oCatMgr = Nothing

	If (True = IsObject(Session("CatalogManager"))) Then
		If Not (Session("CatalogManager") Is Nothing) Then
			Dim rs
			Set g_oCatMgr = Session("CatalogManager")
			Set rs = g_oCatMgr.Catalogs
			If (Err.Number <> 0) Then
				Set g_oCatMgr = Nothing
				Set Session("CatalogManager") = Nothing
				Err.Clear
				Exit Sub
			End If
		End If
	End If

	If (g_oCatMgr Is Nothing) Then
		sConnString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")

		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (sConnString, True)
		If (Err.Number <> 0) Then
			Set g_oCatMgr = Nothing
			Err.Clear
		Else
			Set Session("CatalogManager") = g_oCatMgr	
		End If
	End If
End Sub

' -----------------------------------------------------------
'
Sub Process_REFRESH ()

	' Regenerate FREE TEXT SEARCH Indexes
	RegenerateAllFreeTextSearchIndexes
	If (Err.Number <> 0) Then Exit Sub

	' Publish all Custom Catalogs (templates)
	PublishAllCustomCatalogs
End Sub

' ---------------------------------------------------------------
'
Sub RegenerateAllFreeTextSearchIndexes ()

	Dim rs
	Dim oCatalog

	On Error Resume Next

	Set g_rsCatalogs = g_oCatMgr.Catalogs
	Do Until g_rsCatalogs.EOF
		Set oCatalog = g_oCatMgr.GetCatalog(g_rsCatalogs("CatalogName").Value)
		If (Not oCatalog Is Nothing) Then
			Set rs = oCatalog.GetCatalogAttributes
			If Not rs("CustomCatalog").Value Then
				' 0 = Full Rebuild
				oCatalog.RegenerateFreeTextSearchIndex(True)
				Set oCatalog = Nothing
				If (Err.number <> 0) Then
					Response.Write sGetScriptError (Err)
					Err.Clear
				End If
			End If
			ReleaseRecordset rs
		End If

		g_rsCatalogs.MoveNext
	Loop

	ReleaseRecordset g_rsCatalogs
End Sub

' ---------------------------------------------------------------
'
Sub PublishAllCustomCatalogs ()

	Dim oCatalog
	Dim rsCustomCatalogs

	On Error Resume Next

	Set rsCustomCatalogs = g_oCatMgr.CustomCatalogs
	Do Until rsCustomCatalogs.EOF
		Set oCatalog = g_oCatMgr.GetCatalog(rsCustomCatalogs("CatalogName").Value)
		If (Not oCatalog Is Nothing) Then
			oCatalog.GenerateCustomCatalog rsCustomCatalogs("CustomCatalogName").Value
			Set oCatalog = Nothing
			If (Err.number <> 0) Then
				Response.Write sGetScriptError (Err)
				Err.Clear
			End If
		End If

		rsCustomCatalogs.MoveNext
	Loop
End Sub

'' -------------------------------------------------------------------
''
''	Function:		ReleaseRecordset
''
''	Description:	
''
''	Arguments:		rs -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ReleaseRecordset (ByRef rs)

	If (True = IsObject (rs)) Then
		If (Not rs Is Nothing) Then
			rs.Close
			Set rs = Nothing
		End If
	End If
End Sub

%>