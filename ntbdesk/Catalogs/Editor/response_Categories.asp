<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../../include/HTTPXMLUtil.asp' -->
<%
'' -------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright 1996 - 2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			response_Categories.asp
''
''	Description:	Category Editor (View Properties page).
''
'' -------------------------------------------------------------------

	Const PAGE_TYPE = "RESPONSE"
	' ---------------------------
	Public g_dRequest
	Public g_DBDefaultLocale
	Public g_DBCurrencyLocale

	Public g_aParentCategories ()

	g_DBDefaultLocale = Application("MSCSDefaultLocale")
	g_DBCurrencyLocale = Application("MSCSCurrencyLocale")
	Set g_dRequest = dGetRequestXMLAsDict()

	On Error Resume Next
	Perform_ServerSide_Processing
	If (Err.Number <> 0) Then
		g_sXML = setXMLError2 (L_ErrorsInPage_ErrorMessage, sGetScriptError(Err))
	End If
%>

<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->

<%= g_sXML %>

<%

'' -------------------------------------------------------------------
''
''	PUBLIC Declarations
''
'' -------------------------------------------------------------------

Public g_sXML
Public g_sAction
Public g_sTVData
Public g_sCtName
Public g_sCgName
Public g_sProductID

'' -------------------------------------------------------------------
''
''	Function:		Perform_ServerSide_Processing
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_ServerSide_Processing ()

	InitializeGlobalVariables
	If (g_oCat Is Nothing) Then Exit Sub

	Perform_Processing_BasedOnAction
	ReleaseGlobalObjects
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeGlobalVariables
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeGlobalVariables ()

	ReDim  g_aParentCategories (-1)

	g_sCtName = g_dRequest("catalog")
	g_sAction = g_dRequest("type")
	g_sCgName = g_dRequest("category")

	InitializeSingleCatalog g_sCtName
	If (g_oCatMgr Is Nothing) Or (g_oCat Is Nothing) Then
		g_sXML = setXMLError2 (L_BadConnectionString_ErrorMessage, "")
		Exit Sub
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Perform_Processing_BasedOnAction
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_Processing_BasedOnAction ()

	On Error Resume Next

	If (g_sAction = "delete") Then g_oCat.DeleteCategory (g_sCgName)
	g_sProductID = g_oCat.IdentifyingProductProperty
	If (Err.Number <> 0) Then Exit Sub

	g_sXML = sGetResponseCategoriesXML
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeSingleCatalog
''
''	Description:	
''
''	Arguments:		sCatalogName - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeSingleCatalog (ByRef sCatalogName)

	On Error Resume Next
	Set g_oCat = Nothing
	InitializeCatalogObject
	If (g_oCatMgr Is Nothing) Then Exit Sub

	Set g_oCat = g_oCatMgr.GetCatalog (sCatalogName)
End Sub

'' -------------------------------------------------------------------
''
''	Function:		sGetResponseCategoriesXML
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function sGetResponseCategoriesXML ()

	sGetResponseCategoriesXML = "<document skip='yes'>"

	sGetResponseCategoriesXML = sGetResponseCategoriesXML & sGetTVData

	sGetResponseCategoriesXML = sGetResponseCategoriesXML & "<operations hidden='yes'><newpage formid='tvform' /><expand formid='tvform' /></operations></document>"
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetTVData
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function sGetTVData ()

	Dim iMethod

	Dim oCategory
	Dim rsParents
	Dim rsCategories

	'--  Based on source of call build the right method
	'--  of category retrieval
	'--  iMethod = 
	'-- 		0: called from view_Catalog or view_product pages
	'-- 		1: called from view_CustomCatalog page
	'-- 		2: called from view_Category page
	iMethod = CInt(g_dRequest("method"))

	'-- Retrieve Set of Categories based on Action
	If (g_sAction = "expand") Then
		'-- Child Categories of a Current Category
		Set oCategory = g_oCat.GetCategory (g_sCgName)
		If (oCategory Is Nothing) Then Exit Function
		Set rsCategories = oCategory.ChildCategories
		Set oCategory = Nothing

		If (iMethod = 1) Then		' View Custom Catalog
			g_sCtName = g_dRequest("custom_catalog")
		ElseIf (iMethod = 2) Then	' View Category
			g_sCgName = g_dRequest ("edit_category")

			If (g_sCgName <> "") Then
				Set oCategory = g_oCat.GetCategory (g_sCgName)
				If Not (oCategory Is Nothing) Then
					'-- Get ParentCategories
					On Error Resume Next
					Set rsParents = oCategory.ParentCategories
					Set oCategory = Nothing

					Do Until rsParents.EOF
						AddArray g_aParentCategories, rsParents("Name").Value
						rsParents.MoveNext
					Loop
				End If
			End If
		End If
	Else
		'-- Root Categories for the Current Catalog
		Set rsCategories = g_oCat.RootCategories("CategoryName")
	End If

	'-- Concatenate XML text generated for these categories
	Select Case iMethod
	Case 0
		Do Until rsCategories.EOF
			GetCategory rsCategories("CategoryName").Value
			rsCategories.MoveNext
		Loop

	Case 1
		Do Until rsCategories.EOF
			GetCategoryForViewCustomCatalogPage rsCategories("CategoryName").Value
			rsCategories.MoveNext
		Loop

	Case 2
		Do Until rsCategories.EOF
			GetCategoryForViewCategoryPage rsCategories("CategoryName").Value, g_sCgName
			rsCategories.MoveNext
		Loop
	End Select

	ReleaseRecordset rsCategories
	sGetTVData = g_sTVData
End Function

'' -------------------------------------------------------------------
''
''	Function:		ReleaseGlobalObjects
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ReleaseGlobalObjects ()

	Set g_oCat = Nothing
	Set g_oCatMgr = Nothing
End Sub

%>