<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<%
'' -------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright 1996 - 2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			edit_Catalog.asp
''
''	Description:	Catalog Editor (Open page).
''
'' -------------------------------------------------------------------

	Const PAGE_TYPE = "EDIT"
	' ---------------------------
	Public g_aProperties ()		' Property names for which DisplayInProductsList = TRUE
	Public g_aPropertiesTypes ()' Corresponding Property Types

	On Error Resume Next
%>

<!--#INCLUDE FILE='../../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='catEditorSTRINGS.asp' -->
<!--#INCLUDE FILE='common.asp' -->

<%
'	#############################
	Perform_ServerSide_Processing
'	#############################
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

option explicit

const READYSTATE_COMPLETE = 4
const URL_EDIT_CATEGORY_PAGE = "<%= g_sBizDeskRoot %>catalogs/editor/edit_Category.asp"
const URL_EDIT_PRODUCT_PAGE  = "<%= g_sBizDeskRoot %>catalogs/editor/edit_Product.asp"

public g_sCtName
public g_sFilterXML
public g_sProductID

public g_oCategory
public g_oSelection
public g_oSelection2		' Current selection in EditSheet group

public g_nPageID
public g_nSearchPageID

public g_aSelected()
public g_aSelected2()		' Currently selected names in ListSheets

sub window_onLoad ()

	dim sAction

	redim g_aSelected(-1)
	redim g_aSelected2(-1)

	'' Prevent race-condition.
	if (not IsObject(g_oCategory)) then	set g_oCategory = Nothing

	g_sProductID = "<%= sReplaceSpaces(g_sProductID) %>"
	g_sCtName =  "<%= g_sCtName %>"
	g_nPageID = <%= g_nPageID %>
	g_nSearchPageID = <%= g_nSearchPageID %>
	g_sFilterXML = "<%= g_sFilterXML %>"

	SetInitialFocus

	sAction = "<%= g_sAction %>"
	' F O R C E   U P D A T E
	CheckForceUpdate sAction, ctForm
	esControl.focus
end sub

sub SetInitialFocus ()

	if (tvData.XMLDocument.readyState <> READYSTATE_COMPLETE) then
		window.setTimeout "SetInitialFocus()", 100
	else
		SetInitialCategoryObj ("<%= g_sCategory2 %>")
	end if
end sub

sub SetInitialCategoryObj (ByRef sCategoryName)

	dim oXML
	dim sName
	dim sPattern

	if (sCategoryName = "") then exit sub

	sName = replace(sCategoryName, "'", "\'")
	set oXML = tvData.XMLDocument.documentElement

	sPattern = "//category[@caption $ieq$ '" & sName & "']"
	set g_oCategory = oXML.selectSingleNode(sPattern)

	if (g_oCategory is Nothing) then
		sPattern = "//category[text() $ieq$ '" & sName & "']"
		set g_oCategory = oXML.selectSingleNode(sPattern)
	end if
end sub

</SCRIPT>

<SCRIPT LANGUAGE="VBScript" DEFER>

sub onChange ()

	setStatusText ""
	setDirty (L_SaveCatalog_Text)
end sub

sub ctForm_onSubmit ()

	with ctForm
		.page_id1.value = g_nPageID
		.page_id2.value = g_nSearchPageID

		if (True = IsObject (g_oCategory)) then
			if (not g_oCategory is Nothing) then
				.category.value = sGetCurrentCategoryName
			end if
		end if
		if (search_method.value = "bykeywords") then
			.filterXML.value = ""
		else										' search by properties
			keywords.value = ""
			.filterXML.value = document.all("divQueryBldr").getExprBody()
		end if

		.submit()
	end with
end sub

' #############################################################
' ONCHANGE Event Handler for SEARCH_METHOD element
'
sub onChangeSearchMethod ()

	with document.all
		select case	search_method.value
		case "bykeywords"
			.divQueryBldr.style.display = "none"
			.divSearchByKey.style.display = "inline"
		case "byproperties"
			.divSearchByKey.style.display = "none"
			.divQueryBldr.style.display = "inline"
		end select
	end with
end sub

' #############################################################
' TreeView Event Handlers
'
sub onTVItemSelect ()

	SetCurrentSelection sGetCategoryFromEvent
end sub

sub onTVItemUnselect ()

	Set g_oCategory = Nothing
	DisableButtons "edit_category", "delete_category", True
end sub

sub onTVItemOpen ()

	with tvform
		.type.value = "expand"
		.category.value = sGetCategoryFromEvent
	end with
end sub

sub SetCurrentSelection (ByRef sText)

	' disable all products selections/buttons
	DisableButtons "edit_product", "delete_products", True
	Set g_oSelection = Nothing
	ReDim g_aSelected(-1)
	' enable Edit/Delete category
	DisableButtons "edit_category", "delete_category", False
	document.all("sel").innerHTML = "<%= L_Products2_Text %>" & "<B>" & sText & "</B>"

	g_nPageID = 1
	set g_oCategory = window.event.XMLItem

	with newpageform
		.category.value = sText
		.type.value = "newPage"
		.page_id.value = 1
	end with

	PrListSheet.reload "newcategory"
end sub

sub DisableButtons (ByRef sEditBtn, ByRef sDeleteBtn, ByVal bDisabled)

	ctForm(sEditBtn).disabled = bDisabled
	ctForm(sDeleteBtn).disabled = bDisabled
end sub

' #############################################################
' ONCLICK Event Handlers for buttons in the CATEGORIES group
'

sub onEditCategory ()

	dim sName

	sName = sGetCurrentCategoryName()
	newpageform("category").value = sName

	call setEditPageParameters(CATEGORY_PARAMETERS, _
							   Array ("type", "cg_name", "cg_type", "ct_name"), _
							   Array ("open", sName, g_oCategory.getAttribute ("type"), g_sCtName))
	call openEditPage (URL_EDIT_CATEGORY_PAGE, "onEditCategoryCallback")
end sub

sub onDeleteCategory ()

	if (vbNo = iConfirmDeleteCategory ()) then exit sub
	DisableButtons "edit_category", "delete_category", True

	with tvform
		.type.value = "delete"
		.category.value = sGetCurrentCategoryName()
	end with
	CatTreeView.reload "newpage"

	set g_oCategory = Nothing

	with newpageform
		.type.value = "newpage"
		.page_id.value = 1
		.category.value = "*****"
	end with

	PrListSheet.reload "newcategory"
	document.all("sel").innerHTML = "<%= L_Products2_Text %>"
end sub

sub onNewCategory ()

	dim sType

	sType = window.showModalDialog("dlg_AddCategory.asp", , _
				"dialogHeight:250px;dialogWidth:340px;status:no;help:no")
	if (sType <> "") and (sType <> L_NoneAvailable_Text) then
		call setEditPageParameters(CATEGORY_PARAMETERS, _
								   Array ("type", "cg_type", "ct_name"), _
								   Array ("add", sType, g_sCtName))
		call openEditPage(URL_EDIT_CATEGORY_PAGE, "onNewCategoryCallback")
	end if
end sub

sub onEditProduct ()

	dim sID
	dim sQuery

	sID = g_aSelected(0)
	sQuery = "//record[" & g_sProductID & "!text() $eq$ """ & replace(sID, """", "\""") & """]"
	set g_oSelection = lsData.XMLdocument.documentElement.selectSingleNode (sQuery)

	call setEditPageParameters(PRODUCT_PARAMETERS, _
							   Array ("type", "pr_id", "pr_type", "ct_name"), _
							   Array ("open", sID, g_oSelection.selectSingleNode("./" & "DefinitionName").text, g_sCtName))
	call openEditPage (URL_EDIT_PRODUCT_PAGE, "onEditProductCallback")
end sub

sub onEditProductCallback ()

	redim g_aSelected(-1)
	DisableButtons "edit_product", "delete_products", True

	PrListSheet.reload "newcategory"
end sub

sub onDeleteProducts ()

	dim nProducts

	nProducts = UBound (g_aSelected) + 1
	if (vbNo = iConfirmDeleteProducts (nProducts)) then exit sub

	DisableButtons "edit_product", "delete_products", True
	with newpageform
		.type.value = "delete"
		.products.value = sConvertArrayToString(g_aSelected)
	end with

	redim g_aSelected(-1)
	PrListSheet.reload "delete"
end sub

sub onNewCategoryCallback ()

	dim sCategoryName

	if (not g_oCategory is Nothing) then _
		sCategoryName = sGetCurrentCategoryName()

	with tvform
		.type.value = "newpage"
		.category.value = sCategoryName
	end with

	CatTreeView.reload "newpage"
	SetInitialCategoryObj sCategoryName
end sub

sub onEditCategoryCallback ()

	DisableButtons "edit_category", "delete_category", True

	with tvform
		.type.value = "newpage"
		.category.value = newpageform("category").value
	end with
	CatTreeView.reload "newpage"
end sub

sub onSelectAll ()

	dim xml
	dim xmlDoc

	dim oList
	dim oItem

	redim g_aSelected(-1)
	set xml = lsData.XMLdocument
	set xmlDoc = xml.documentElement
	set oList = xmlDoc.selectNodes ("//" & g_sProductID)

	for each oItem in oList
		AddArray g_aSelected, oItem.text
	next

	if (UBound(g_aSelected) = 0) then
		set g_oSelection = xmlDoc.selectSingleNode ("//record[" & g_sProductID & "!text() $eq$ """ & replace(oList.item(0).text, """", "\""") & """]")
		DisableButtons "edit_product", "delete_products", False
	else'if (UBound(g_aSelected) > 0)
		set g_oSelection = Nothing
		document.all("delete_products").disabled = False
	end if
end sub

sub onDeselectAll ()

	redim g_aSelected (-1)
	DisableButtons "edit_product", "delete_products", True
end sub

sub onRowSelect ()

	dim sProductID

	set g_oSelection = window.event.XMLrecord
	sProductID = g_oSelection.selectSingleNode("./" & g_sProductID).text
	AddArray g_aSelected, sProductID

	if (UBound(g_aSelected) = 0) then
		DisableButtons "edit_product", "delete_products", False
	else'UBound(g_aSelected) > 0
		ctForm("edit_product").disabled = True
	end if
end sub

sub onRowUnselect ()

	dim sProductID

	sProductID = window.event.XMLrecord.selectSingleNode("./" & g_sProductID).text
	DeleteArray g_aSelected, sProductID

	if (UBound(g_aSelected) = -1) then
		DisableButtons "edit_product", "delete_products", True
	elseif (UBound(g_aSelected) = 0) then
		DisableButtons "edit_product", "delete_products", False
	else
		ctForm("edit_product").disabled = True
	end if
end sub

sub onNewPage ()

	dim nPage

	nPage = window.event.page
	g_nPageID = nPage

	with newpageform
		.page_id.value = nPage
		.type.value = "newPage"
	end with
end sub

' #############################################################
' ONCLICK Event Handlers for elements in the PRODUCTS group

sub onFindNow ()

	onDeselectAll2

	with newpageform2
		.type.value = "newSearch"
		.page_id.value = 1
		if (search_method.value = "bykeywords") then
			.keywords.value = keywords.value
			.newSearchXML.value = ""
		else
			.keywords.value = ""
			.newSearchXML.value = divQueryBldr.getExprBody()
		end if
	end with

	PrSearchListSheet.reload "search"
end sub

sub onNewProduct ()

	dim sType

	sType = window.showModalDialog("dlg_AddProduct.asp", , _
				"dialogHeight:250px;dialogWidth:340px;status:no;help:no")

	if (sType <> "") then
		call setEditPageParameters(PRODUCT_PARAMETERS, _
								   Array ("type", "pr_type", "ct_name"), _
								   Array ("add", sType, g_sCtName))
		call openEditPage (URL_EDIT_PRODUCT_PAGE, "onNewProductCallback")
	end if
end sub

sub onEditProduct2 ()

	dim sID
	dim sQuery

	sID = g_aSelected2(0)
	sQuery = "//record[" & g_sProductID & "!text() $eq$ """ & replace(sID, """", "\""") & """]"
	set g_oSelection2 = lsSearchData.XMLdocument.documentElement.selectSingleNode (sQuery)

	call setEditPageParameters(PRODUCT_PARAMETERS, _
							   Array ("type", "pr_id", "pr_type", "ct_name"), _
							   Array ("open", sID, g_oSelection2.selectSingleNode("./DefinitionName").text, g_sCtName))
	call openEditPage (URL_EDIT_PRODUCT_PAGE, "onEditProductCallback2")
end sub

sub onEditProductCallback2 ()

	redim g_aSelected2(-1)
	DisableButtons "edit_product2", "delete_products2", True

	PrSearchListSheet.reload "search"
end sub

sub onDeleteProducts2 ()

	dim nProducts

	nProducts = UBound (g_aSelected2) + 1
	if (vbNo = iConfirmDeleteProducts (nProducts)) then exit sub

	DisableButtons "edit_product2", "delete_products2", True
	with newpageform2
		.type.value = "delete"
		.products.value = sConvertArrayToString(g_aSelected2)
	end with

	redim g_aSelected2(-1)
	PrSearchListSheet.reload "delete"
end sub

sub onRowSelect2 ()

	dim sProdName

	set g_oSelection2 = window.event.XMLrecord
	sProdName = g_oSelection2.selectSingleNode("./" & g_sProductID).text
	AddArray g_aSelected2, sProdName

	ToggleButtons
end sub

sub onRowUnselect2 ()

	dim sProdName

	sProdName = window.event.XMLrecord.selectSingleNode("./" & g_sProductID).text
	DeleteArray g_aSelected2, sProdName

	ToggleButtons	
end sub

sub onSelectAll2 ()

	dim xml
	dim xmlDoc

	dim oList
	dim oItem

	redim g_aSelected2 (-1)

	set xml = lsSearchData.XMLdocument
	set xmlDoc = xml.documentElement
	set oList = xmlDoc.selectNodes ("//" & g_sProductID)
	for each oItem in oList
		AddArray g_aSelected2, oItem.text
	next

	ToggleButtons
end sub

sub ToggleButtons ()

	dim iUBound

	iUBound = UBound (g_aSelected2)
	if (iUBound = -1) then
		DisableButtons "edit_product2", "delete_products2", True
	elseif (iUBound = 0) then
		DisableButtons "edit_product2", "delete_products2", False
	else'if(iUBound > 0) then
		with ctForm
			.edit_product2.disabled = True
			.delete_products2.disabled = False
		end with
	end If
end sub

sub onDeselectAll2 ()

	redim g_aSelected2 (-1)
	set g_oSelection2 = Nothing

	DisableButtons "edit_product2", "delete_products2", True
end sub

sub onNewPage2 ()

	dim nPage

	nPage = window.event.page
	newpageform2.page_id.value = nPage
	g_nSearchPageID = nPage

	if (True = IsObject (g_oSelection2)) then
		g_oSelection2 = ""
		redim g_aSelected2 (-1)
		DisableButtons "edit_product2", "delete_products2", True
	end if
end sub

sub onNewProductCallback ()

	g_nSearchPageID = 1
	newpageform2.page_id.value = 1

	with PrSearchListSheet
		.page = 1
		.reload "newproduct"
	end with

	if (True = IsObject (g_oSelection2)) then
		g_oSelection2 = ""
		redim g_aSelected2 (-1)
		DisableButtons "edit_product2", "delete_products2", True
	end if
end sub

sub onReadyQB ()

	if (g_sFilterXML <> "") then
		call document.all("divQueryBldr").SetExprBody(g_sFilterXML)
	end if
end sub

sub onErrorQB ()

	alert L_QueryBuilder_ErrorMessage

	with document.all
		call .divQueryBldr.setExprBodyXML (Nothing)
		.divQueryBldr.Deactivate
	end with
end sub

sub onBrowseVendors ()

	dim aResponse
	dim sResponse

	sResponse =  window.showModalDialog("dlg_VendorPicker.asp?mode=assign", , _
										"dialogHeight:250px;dialogWidth:340px;status:no;help:no")
	if(sResponse = "") then exit sub

	aResponse = Split (sResponse, SEPARATOR1)
	if (aResponse(0) = L_None_Text) then aResponse(0) = ""

	vendor.value = aResponse(0)
	with ctForm
		.vendor_qualifier.value = aResponse(1)
		.qualifier_value.value = aResponse(2)
	end with
end sub

function iConfirmDeleteItems (ByVal nItems, _
			ByRef sSingleMsg, ByRef sSingleTitle, _
			ByRef sMultiMsg, ByRef sMultiTitle)

	dim sMsg
	dim sTitle

	if (nItems = 1) then
		sMsg = sSingleMsg
		sTitle = sSingleTitle
	else
		sMsg = sFormatString (sMultiMsg, Array(nItems))
		sTitle = sMultiTitle
	end if

	iConfirmDeleteItems = MsgBox (sMsg, vbYesNo, sTitle)
end function

function iConfirmDeleteProducts (ByVal nProducts)

	iConfirmDeleteProducts = iConfirmDeleteItems (nProducts, _
			L_DeleteProduct_Text, L_DeleteProduct_DialogTitle, _
			L_DeleteProducts_Text, L_DeleteProducts_DialogTitle)
end function

function iConfirmDeleteCategory ()

	iConfirmDeleteCategory = iConfirmDeleteItems (1, _
			L_DeleteCategory_Text, L_DeleteCategory_DialogTitle, _
			"", "")
end function

</SCRIPT>
</HEAD>

<BODY>
<%
	' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
	' (for those errors not treated in place)
	If (Err.Number <> 0) And (Not g_oCatMgr Is Nothing) Then
		Call setError (L_Critical_DialogTitle, L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If

	InsertEditTaskBar g_sHeader, g_sStatusText
%>

<xml id='tvData'>
	<document skip='yes'>
<%= g_sTVData %>
		<operations hidden='yes'>
			<newpage formid='tvform' />
			<expand formid='tvform' />
		</operations>
	</document>
</xml>

<xml id='lsData'>
	<%= sGetListData %>
</xml>

<xml id='lsMeta'>
	<listsheet>
	    <global selectall='no' curpage='<%= g_nPageID %>' pagesize='<%= PAGE_SIZE %>'
			recordcount='<%= g_nItems1 %>' selection='multi' sort='no'/>
	    <columns>
<%= g_sMeta %>
	    </columns>
   	    <operations>
			<newpage formid='newpageform' />
			<newcategory formid='newpageform' />
			<newproduct formid='newpageform' />
			<delete formid='newpageform' />
		</operations>
	</listsheet>
</xml>

<XML ID="xmlisConfig">
	<EBCONFIG>
		<ADVANCED />
		<DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
		<CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
		<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
		<DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
		<HELPTOPIC><%= sRelURL2AbsURL("../../docs/default.asp?helptopic=cs_bd_catalogs_DICD.htm")%></HELPTOPIC>
		<EXPR-ASP>../../exprarch</EXPR-ASP>
		<SITE-TERMS URL='QBEnumeratedValues.asp' />
		<TYPESOPS>QBTypesOps.xml</TYPESOPS>
		<!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
		<PROFILE-LIST>
			<PROFILE SRC='QBProperties.asp' />
		</PROFILE-LIST>
  </EBCONFIG>
</XML>

<xml id='lsSearchData'>
	<record/>
</xml>
<% '= sGetSearchListData %>

<xml id='lsSearchMeta'>
	<listsheet>
	    <global selectall='no' curpage='<%= g_nSearchPageID %>' pagesize='<%= PAGE_SIZE %>' 
			recordcount='<%= g_nItems2 %>' selection='multi' sort='no'/>
	    <columns>
<%= g_sMeta %>
	    </columns>
   	    <operations>
			<newpage formid='newpageform2' />
			<newproduct formid='newpageform2' />
			<search formid='newpageform2' />
			<delete  formid='newpageform2' />
		</operations>
	</listsheet>
</xml>

<xml id='esData'>
	<DOCUMENT>
		<RECORD>
			<name><%= Server.HTMLEncode(g_sCtName) %></name>
			<start_date><%= g_sStartDate %></start_date>
			<end_date><%= g_sEndDate %></end_date>
			<currency><%= g_sCurrency %></currency>
			<weight_unit><%= g_sWeightUnit %></weight_unit>
			<product_id><%= g_sProductID %></product_id>
			<variant_id><%= g_sVariantID %></variant_id>
<% If (ENABLED = g_iBizTalkOptions) Then %>
			<vendor><%= g_sVendorName %></vendor>
<% End If %>
		</RECORD>
	</DOCUMENT>
</xml>

<xml id='esMeta'>
	<editsheets>
		<editsheet>
		    <global expanded='yes'>
		        <name><%= L_CatalogProperties_Text %></name>
		        <key><%= L_CatalogProperties_Accelerator %></key>
			</global>
			<fields>
			    <text id='name' required='yes' maxlen='100' subtype='short' readonly='<% If (g_sAction = "add") Then %>no<% Else %>yes<% End If %>'>
			        <name><%= L_Name_Text %></name>
			        <tooltip><%= L_Name_Tooltip %></tooltip>
			        <charmask><%= Server.HTMLEncode("^[^<>\[\]']*$") %></charmask>
			        <error><%= L_ValidationErrorInCatalogName_ErrorMessage %></error>
			    </text>
			    <date id='start_date' firstday='<%= g_sMSCSWeekStartDay %>'>
			        <name><%= L_StartDate_Text %></name>
			        <tooltip><%= L_StartDate_Tooltip %></tooltip>
			        <format><%= g_sMSCSDateFormat %></format>
			    </date>
			    <date id='end_date' firstday='<%= g_sMSCSWeekStartDay %>'>
			        <name><%= L_EndDate_Text %></name>
			        <tooltip><%= L_EndDate_Tooltip %></tooltip>
			        <format><%= g_sMSCSDateFormat %></format>
				</date>
			    <text id='currency' required='yes' maxlen='3' subtype='short'>
			        <name><%= L_Currency2_Text %></name>
			        <tooltip><%= L_Currency_Tooltip %></tooltip>
			    </text>
			    <text id='weight_unit' readonly='yes' maxlen='100' subtype='short'>
			        <name><%= L_UnitOfMeasure_Text %></name>
			        <tooltip><%= L_UnitOfMeasure_Tooltip %></tooltip>
			    </text>
			    <select id='product_id' <% If (g_sAction = "add") Then %>readonly='no' required='yes'<% Else %>readonly='yes'<%End If%> >
			        <name><%= L_ProductID_Text %></name>
			        <tooltip><%= L_ProductID_Tooltip %></tooltip>
			        <prompt><%= L_ProductID2_Text %></prompt>
			        <select id='product_id'>
						<%= g_sProperties %>
			        </select>
			    </select>
			    <select id='variant_id' <% If (g_sAction = "add") Then %>readonly='no' required='yes'<% Else %>readonly='yes'<%End If%>>
			        <name><%= L_VariantID_Text %></name>
			        <tooltip><%= L_VariantID_Tooltip %></tooltip>
			        <prompt><%= L_ProductID2_Text %></prompt>
			        <select id='variant_id'>
						<%= g_sProperties %>
			        </select>
			    </select>
<% If (ENABLED = g_iBizTalkOptions) Then %>
				<text id='vendor' maxlen='100' subtype='short' browsereadonly='yes' onbrowse='onBrowseVendors'>
			        <name><%= L_VendorID_Text %></name>
			        <tooltip><%= L_VendorID_Tooltip %></tooltip>
			    </text>
<% End If %>
		    </fields>
		</editsheet>
		<editsheet>
		    <global expanded='no'>
		        <name><%= L_Categories_Text %></name>
		        <key><%= L_Categories_Accelerator %></key>
			</global>
			<template><![CDATA[
			<TABLE><BR>
				<TR>
					<TD><%= L_Categories_HTMLText %></TD>
					<TD><DIV ID='sel'><%= L_Products2_Text & "<B>" & g_sCategory & "</B>" %></DIV></TD>
				</TR>
				<TR HEIGHT="280px">
					<TD VALIGN="TOP">
						<DIV STYLE='width:310px; height:252px; overflow:auto; background:white; border:thin inset silver'>
							<DIV ID='CatTreeView'
								CLASS='treeView '
								DataXML='tvData'
								LANGUAGE='VBScript'
								OnItemSelect='onTVItemSelect'
								OnItemUnselect='onTVItemUnselect'
								OnItemOpen='onTVItemOpen'><%= L_LoadingWidget_Text %>
							</DIV>
						</DIV>
					</TD>
					<TD WIDTH="400px" VALIGN="TOP">
						<DIV ID='PrListSheet'
							CLASS='listSheet'
							DataXML='lsData'
							MetaXML='lsMeta'
							LANGUAGE='VBScript'
							OnRowSelect='onRowSelect'
							OnRowUnselect='onRowUnselect'
							OnAllRowsSelect='onSelectAll'
							OnAllRowsUnselect='onDeselectAll'
							OnNewPage='onNewPage'><%= L_LoadingWidget_Text %>
						</DIV>
					</TD>
				</TR>
			</TABLE>
			<TABLE>
				<TR>
					<TD>
						<BUTTON ID="edit_category" CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onEditCategory()" <% If (g_sCategory2 = "") Then %>DISABLED<% End If %>><%= L_Edit_Button %></BUTTON>
						<BUTTON ID="delete_category" CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onDeleteCategory()" <% If (g_sCategory2 = "") Then %>DISABLED<% End If %>><%= L_Delete_Button %></BUTTON>
						<BUTTON ID="new_category" CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onNewCategory()" <% If (g_sAction = "add") Then %>DISABLED<% End If %>><%= L_NewCategory_Button %></BUTTON>
					</TD>
					<TD STYLE="width:65px"></TD>
					<TD>
						<BUTTON ID="edit_product" CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onEditProduct()" DISABLED><%= L_Edit_Button %></BUTTON>
						<BUTTON ID="delete_products" CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onDeleteProducts()" DISABLED><%= L_Delete_Button %></BUTTON>
					</TD>
				</TR>
			</TABLE>
		    ]]></template>
		</editsheet>
		<editsheet>
		    <global expanded='no'>
		        <name><%= L_Products_Text %></name>
		        <key><%= L_Products_Accelerator %></key>
			</global>
		    <fields>
				<select id="search_method" onchange="onChangeSearchMethod()" default='<%= g_sMethod %>'>
					<select id="search_method">
						<option value="bykeywords"><%= L_ByKeyword_Text %></option>
						<option value="byproperties"><%= L_ByProperties_Text %></option>
					</select>
				</select>
			    <text id='keywords' maxlen='100' subtype='short'>
			        <prompt><%= g_sKeywords %></prompt>
			    </text>
		    </fields>
			<template><![CDATA[<BR>
			<LABEL FOR="search_method"><B><%= L_SearchMethod_Text %></B></LABEL>
			<div STYLE="width:170px">	
				<DIV ID='search_method' CLASS='editField'
					MetaXML='esMeta'
					DataXML='esData'></DIV></div>
			<BR><BR>

			<TABLE>
				<TR>
					<TD STYLE="width:8px"></TD>
					<TD>
						<DIV ID="divSearchByKey" <% If (g_sMethod = "byproperties") Then %>STYLE="display:none"<% End If %>>
							<%= L_Keywords_Text %><BR>
							<div STYLE="width:560px">	
								<DIV ID='keywords' CLASS='editField'
									MetaXML='esMeta'
									DataXML='esData'></DIV></div>
						</DIV>
						<DIV ID="divQueryBldr" NAME="divQueryBldr" CLASS="clsQueryBldr" STYLE="width:560px; height:150px;<% If (g_sMethod = "bykeywords") Then %> display:none<% End If %>"
							STYLE="behavior:url(/widgets/exprbldrhtc/QueryBuilder.htc); width:100%" 
							XMLCfgID="xmlisConfig" AutoActivate='true' ONREADY="onReadyQB()" ONERROR="onErrorQB()"><%= L_LoadingQB_Text %>
						</DIV>
					</TD>
					<TD VALIGN="TOP">
						<BR><BUTTON ID="find" CLASS="bdbutton" STYLE="<%= L_Button_Style %>" ONCLICK="onFindNow()" <% If (g_sAction = "add") Then %>DISABLED<% End If %>><%= L_FindNow_Button %></BUTTON>
					</TD>
				</TR>
			</TABLE>
			<BR>
			<TABLE>
				<TR>
					<TD>
						<INPUT ID="align2" TYPE="text" STYLE="width:20px; visibility:hidden"><%= L_Products3_Text %><BR>
						<DIV ID='PrSearchListSheet' STYLE="width:562px"
							CLASS='listSheet'
							DataXML='lsSearchData'
							MetaXML='lsSearchMeta'
							LANGUAGE='VBScript'
							OnRowSelect='onRowSelect2'
							OnRowUnselect='onRowUnselect2'
							OnAllRowsSelect='onSelectAll2'
							OnAllRowsUnselect='onDeselectAll2'
							OnNewPage='onNewPage2'><%= L_LoadingWidget_Text %>
						</DIV>
					</TD>
					<TD VALIGN="TOP">
						<INPUT ID="align3" TYPE="text" STYLE="width:20px; height:45px; visibility:hidden"></INPUT><BR>
						<BUTTON ID="new_product" CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onNewProduct()" <% If (g_sAction = "add") Then %>DISABLED<% End If %>><%= L_NewProduct_Button %></BUTTON><BR>
						<BUTTON ID="edit_product2"  CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onEditProduct2()" DISABLED><%= L_Edit_Button %></BUTTON><BR>
						<BUTTON ID="delete_products2" CLASS="bdbutton" STYLE='<%= L_Button_Style %>' ONCLICK="onDeleteProducts2()" DISABLED><%= L_Delete_Button %></BUTTON><BR>
					</TD>
				</TR>
			</TABLE>
		    ]]></template>
		</editsheet>
	</editsheets>
</xml>

<DIV ID='bdcontentarea' CLASS='editSheetContainer'>

	<FORM ACTION="" METHOD="POST" NAME='ctForm' ID="ctForm" ONTASK="ctForm_onSubmit">
		<INPUT TYPE="Hidden" NAME="type" VALUE="<%= g_sActionType %>" ID="type">
		<INPUT TYPE="Hidden" NAME="old_vendorname" VALUE="<%= g_sOldVendorName %>" ID="old_vendorname">
		<INPUT TYPE="Hidden" NAME="vendor_qualifier" ID="vendor_qualifier">
		<INPUT TYPE="Hidden" NAME="qualifier_value" ID="qualifier_value">
		<INPUT TYPE="Hidden" NAME="page_id1" ID="page_id1">
		<INPUT TYPE="Hidden" NAME="page_id2" ID="page_id2">
		<INPUT TYPE="Hidden" NAME="filterXML" ID="filterXML">
		<INPUT TYPE="Hidden" NAME="category" ID="category">
		<DIV ID='esControl' 
			CLASS='editSheet' 
			MetaXML='esMeta' 
			DataXML='esData'
			ONCHANGE='onChange'
			ONREQUIRE ='setRequired(L_Required_Text)'
			ONVALID = 'setValid(L_Valid_Text)'><%= L_LoadingPropertyGroups_Text %>
		</DIV>
	</FORM>
</DIV>

<FORM ID="newpageform" ACTION='response_Products.asp'>
	<INPUT TYPE="Hidden" NAME="mode" VALUE="CATALOGS">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE=<%= g_nPageID %>>
	<INPUT TYPE="Hidden" NAME="type">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sCtName %>">
	<INPUT TYPE="Hidden" NAME="category">
	<INPUT TYPE="Hidden" NAME="products">
</FORM>

<FORM ID="newpageform2" ACTION='response_Products.asp'>
	<INPUT TYPE="Hidden" NAME="mode" VALUE="CATALOGS">
	<INPUT TYPE="Hidden" NAME="page_id" VALUE="<%= g_nSearchPageID %>">
	<INPUT TYPE="Hidden" NAME="type">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sCtName %>">
	<INPUT TYPE="Hidden" NAME="keywords">
	<INPUT TYPE="Hidden" NAME="newSearchXML">
	<INPUT TYPE="Hidden" NAME="products">
</FORM>

<FORM ID="newproductform" NAME="newproductform" ACTION="edit_Product.asp">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="pr_type">
	<INPUT TYPE="Hidden" NAME="ct_name" VALUE="<%= g_sCtName %>">
</FORM>

<FORM ID="openproductform" NAME="openproductform" ACTION="edit_Product.asp">
	<INPUT TYPE="Hidden" NAME="type" VALUE="open">
	<INPUT TYPE="Hidden" NAME="pr_id">
	<INPUT TYPE="Hidden" NAME="pr_type">
	<INPUT TYPE="Hidden" NAME="ct_name" VALUE="<%= g_sCtName %>">
</FORM>

<FORM ID="newcategoryform" NAME="newcategoryform" ACTION="cg_view.asp">
	<INPUT TYPE="Hidden" NAME="type" VALUE="add">
	<INPUT TYPE="Hidden" NAME="cg_type">
	<INPUT TYPE="Hidden" NAME="ct_name" VALUE="<%= g_sCtName %>">
</FORM>

<FORM ID="opencategoryform" NAME="opencategoryform" ACTION="cg_view.asp">
	<INPUT TYPE="Hidden" NAME="type" VALUE="open">
	<INPUT TYPE="Hidden" NAME="cg_name">
	<INPUT TYPE="Hidden" NAME="cg_type">
	<INPUT TYPE="Hidden" NAME="ct_name" VALUE="<%= g_sCtName %>">
</FORM>

<FORM ID="tvform" NAME="tvform" ACTION="response_Categories.asp">
	<INPUT TYPE="Hidden" NAME="type">
	<INPUT TYPE="Hidden" NAME="method" VALUE="0">
	<INPUT TYPE="Hidden" NAME="category">
	<INPUT TYPE="Hidden" NAME="catalog" VALUE="<%= g_sCtName %>">
</FORM>

<FORM ACTION="" METHOD="POST" NAME='closeForm' ID="closeForm">
	<INPUT TYPE="Hidden" NAME="type" VALUE="close">
</FORM>

</BODY>
</HTML>
<%
	ReleaseGlobalObjects
%>

<%

' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'' -------------------------------------------------------------------
''
''	Global CONST Declarations
''
'' -------------------------------------------------------------------

Const ENABLED = 1
Const g_sMode = "CATALOGS"

'' -------------------------------------------------------------------
''
''	PUBLIC Declarations
''
'' -------------------------------------------------------------------

Public g_sXML
Public g_sMeta
Public g_sCtName		' Catalog Name
Public g_sAction		' String representation of action to take on page
Public g_sHeader		' Tab header
Public g_sMethod
Public g_sTVData
Public g_sEndDate
Public g_sCurrency
Public g_sKeywords
Public g_sCategory
Public g_sCategory2
Public g_sFilterXML
Public g_sStartDate
Public g_sProductID
Public g_sVariantID
Public g_sProperties
Public g_sActionType	' Type to pass through
Public g_sStatusText	' To be displayed in the Status Bar
Public g_sWeightUnit
Public g_sVendorName
Public g_sOldVendorName

Public g_rsCatalogs		' List of catalogs
Public g_rsProducts		' List of products
Public g_rsProperties	' Property Definitions used by the Current Catalog

Public g_nItems1
Public g_nItems2
Public g_nPageID
Public g_nSearchPageID
Public g_iBizTalkOptions

'' -------------------------------------------------------------------
''
''	Function:		Perform_ServerSide_Processing
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_ServerSide_Processing ()

	InitializeCatalogObject
	InitializeGlobalVariables
	If (Err.Number <> 0) Then
		Set g_oCat = Nothing
		g_sActionType = g_sAction
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sCtName))
		If (g_sAction = "submitNew") Then
			g_sHeader = L_CatalogNew_HTMLText
		Else
			g_sHeader = sFormatString (L_Catalog_HTMLText, Array(g_sCtName))
		End If

		Exit Sub
	End If

	Perform_Processing_BasedOnAction
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InitializeGlobalVariables
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeGlobalVariables ()

	Dim iType
	Dim iMaxLength
	Dim sPropertyName

On Error Resume Next
	g_iBizTalkOptions = iGetBizTalkOptions()
	' T E M PO R A R Y
	g_iBizTalkOptions = 1

	g_sVendorName = Request.Form("vendor")
	g_sOldVendorName = Request.Form("old_vendorname")

	g_sAction = Request.Form("type")
	g_sCtName = Request.Form("ct_name")
	g_sCategory = Request.Form("category")
	g_sCategory2 = g_sCategory

	g_sKeywords = Request.Form("keywords")
	If (g_sKeywords = "") Then g_sKeywords = L_EnterKeywords_Text
	g_sFilterXML = Request.Form("filterXML")
	g_nPageID = CInt(Request.Form("page_id1"))
	g_nSearchPageID = CInt(Request.Form("page_id2"))
	g_sMethod = Request.Form("search_method")
	If (g_sMethod = "") Then
		g_sMethod = "bykeywords"
	End If

	Select Case g_sAction
	Case "submitNew", "submitEdit"
		'Capture user-entered data
		g_sCtName = Request.Form("name")
		g_sStartDate = Request.Form("start_date")
		g_sEndDate = Request.Form("end_date")
		g_sCurrency = Request.Form("currency")
		g_sProductID = Request.Form("product_id")
		g_sVariantID = Request.Form("variant_id")
		g_sWeightUnit = Request.Form("weight_unit")

	Case "open"
		Set g_rsCatalogs = g_oCatMgr.Catalogs
		If Err.Number <> 0 Then Exit Sub

		Do Until g_rsCatalogs.EOF
			If (g_rsCatalogs("CatalogName") = g_sCtName) Then
				g_sStartDate = g_rsCatalogs("StartDate")
				g_sEndDate = g_rsCatalogs("EndDate")
				g_sCurrency = g_rsCatalogs("Currency")
				g_sProductID = g_rsCatalogs("ProductID")
				g_sVariantID = g_rsCatalogs("VariantID")
				g_sWeightUnit = g_rsCatalogs("WeightMeasure")
				Exit Do
			End If

			g_rsCatalogs.MoveNext
		Loop
	End Select

	Set g_rsProperties = g_oCatMgr.Properties
	If Err.Number <> 0 Then Exit Sub

	While Not g_rsProperties.EOF
		sPropertyName = g_rsProperties.Fields("PropertyName")
		iType = g_rsProperties.Fields("DataType")
		iMaxLength = g_rsProperties.Fields("MaxLength")
		If (iType = INTEGER_TYPE) Or (iType = BIGINTEGER_TYPE) Or _
		   ((iType = STRING_TYPE) And (iMaxLength < 900)) Then
			g_sProperties = g_sProperties & "<option value=""" & sPropertyName & """>" & Server.HTMLEncode(sPropertyName) & "</option>"
		End If

		g_rsProperties.MoveNext
	Wend

	If (g_sAction <> "submitNew") And (g_sAction <> "add") Then
		ReleaseRecordset g_rsProperties
		Set g_oCat = g_oCatMgr.GetCatalog (g_sCtName)
		Set g_rsProperties = g_oCatMgr.Properties (g_sCtName)

		GetTVData
		If (g_sCategory <> "") Then InsertSelected
	End If

	ReDim g_aProperties(-1)
	ReDim g_aPropertiesTypes(-1)
	GetPropertiesToDisplayInList
	GetListSheetMeta
End Sub

'' -------------------------------------------------------------------
''
''	Function:		InsertSelected	
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InsertSelected ()

	Dim iPos

	iPos = InStr (g_sTVData, ">" & g_sCategory)

	If (iPos = 0) Then _
		iPos = InStr (g_sTVData, "caption=" & Chr(34) & g_sCategory & Chr(34))

	If (iPos > 0) Then
		g_sTVData = Left (g_sTVData, iPos - 1) & " selected='yes' " & Right (g_sTVData, Len(g_sTVData) - iPos + 1)
	Else
		g_sCategory2 = ""
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		iGetBizTalkOptions 
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function iGetBizTalkOptions ()

	iGetBizTalkOptions = 0
	If (g_MSCSAppConfig Is Nothing) Then Exit Function

	iGetBizTalkOptions = g_MSCSAppConfig.GetOptionsDictionary("").Value("i_BizTalkOptions")
	If (True = IsNull(iGetBizTalkOptions)) Then iGetBizTalkOptions = 0
End Function

'' -------------------------------------------------------------------
''
''	Function:		Perform_Processing_BasedOnAction
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Perform_Processing_BasedOnAction ()

	Select Case g_sAction
	Case "submitNew"
		Process_SubmitNEW

	Case "submitEdit", "submitEditF"
		Process_SubmitEDIT

	Case "add"
		Process_ADD

	Case "open"
		Process_OPEN

	End Select
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_SubmitNEW
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_SubmitNEW ()

	Dim bCatalogCreated

	bCatalogCreated = bCreateCatalog()

	If (False = bCatalogCreated) Then
		g_sAction = "add"
		g_sActionType = "submitNew"
		g_sHeader = L_CatalogNew_HTMLText
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sCtName))
	Else
		SetVendor

		g_sActionType = "submitEdit"
		g_sHeader = sFormatString (L_Catalog_HTMLText, Array(g_sCtName))
		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sCtName))
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_SubmitEDIT
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_SubmitEDIT ()

	Dim sUpdateError

	g_sActionType = "submitEdit"
	'Update the definition
	sUpdateError = sUpdateCatalog()
	If (sUpdateError <> "") Then
		g_sStatusText = sFormatString (L_ErrorSaving_StatusBar, Array(g_sCtName))
	Else
		SetVendor

		g_sStatusText = sFormatString (L_Saved_StatusBar, Array(g_sCtName))
	End If

	g_sHeader = sFormatString (L_Catalog_HTMLText, Array(g_sCtName))
End Sub

'' -------------------------------------------------------------------
''
''	Function:		Process_ADD
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_ADD ()

	g_sActionType = "submitNew"
	g_sHeader = L_CatalogNew_Text

	g_sCtName = ""
	g_sProductID = ""
	g_sVariantID = ""
	g_sStartDate = stringValue(Date, DATETIME_TYPE)
	g_sEndDate   = stringValue(DateAdd("m", 3, Date), DATETIME_TYPE)

	'----- Get Currency/LCID/Unit of Weight Measure
	g_sCurrency = GetSiteConfigField ("App Default Config", "s_BaseCurrencyCode")
	If (g_sCurrency = "") Then g_sCurrency = "USD"
	g_sWeightUnit = GetSiteConfigField ("App Default Config", "s_WeightMeasure")
	If (g_sWeightUnit = "") Then g_sWeightUnit = "lbs"
End Sub

'' -------------------------------------------------------------------
''
''	Function:	Process_OPEN	
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub Process_OPEN ()

	GetVendor

	g_sActionType = "submitEdit"
	g_sHeader = sFormatString (L_Catalog_HTMLText, Array(g_sCtName))
End Sub

'' -------------------------------------------------------------------
''
''	Function:		bCreateCatalog
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		boolean (True - SUCCESS, False - FAILURE)
''
'' -------------------------------------------------------------------
Function bCreateCatalog ()

	On Error Resume Next
	Set g_oCat = g_oCatMgr.CreateCatalog(g_sCtName, CStr(g_sProductID), CStr(g_sVariantID), , _
										 CStr(g_sCurrency), CStr(g_sWeightUnit), _
				 						 variantValue(g_sStartDate, DATETIME_TYPE), _
				 						 variantValue(g_sEndDate, DATETIME_TYPE))
	bCreateCatalog = True
	If (Err.Number <> 0) Then
		bCreateCatalog = False
		Call setError (L_CreateCatalog_DialogTitle, sFormatString(L_ErrorCreatingCatalog_ErrorMessage, Array(g_sCtName)), sGetScriptError(Err), ERROR_ICON_CRITICAL)
		Err.Clear
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		sUpdateCatalog
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		string (error message if error)
''
'' -------------------------------------------------------------------
Function sUpdateCatalog ()

	Dim rsCatalog
	Dim bForceUpdate

	Set rsCatalog = g_oCat.GetCatalogAttributes
	rsCatalog.Fields("StartDate") = variantValue(g_sStartDate, DATETIME_TYPE)
	rsCatalog.Fields("EndDate") = variantValue(g_sEndDate, DATETIME_TYPE)
	rsCatalog.Fields("CatalogName") = g_sCtName
	rsCatalog.Fields("Currency") = g_sCurrency

	If (g_sAction = "submitEditF") Then
		bForceUpdate = True
	Else
		bForceUpdate = False
	End If
	g_oCat.SetCatalogAttributes rsCatalog, bForceUpdate
	CheckForPropertyAlreadyModified

	Set rsCatalog = Nothing
End Function

'' -------------------------------------------------------------------
''
''	Function:		ReleaseGlobalObjects
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ReleaseGlobalObjects ()

	ReleaseRecordset g_rsProducts
	ReleaseRecordset g_rsCatalogs
	ReleaseRecordset g_rsProperties

	Set g_oCat = Nothing
	Set g_oCatMgr = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetVendor
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetVendor ()

	Dim rsVendorInfo
	Dim oCatalogVendor

	On Error Resume Next
	GetCatalogVendorObject oCatalogVendor
	If (oCatalogVendor Is Nothing) Then Exit Sub

	Set rsVendorInfo = oCatalogVendor.GetVendorInfoForCatalog (g_sCtName)
	If (Not rsVendorInfo.EOF) Then _
		g_sVendorName = rsVendorInfo.Fields("VendorName")

	g_sOldVendorName = g_sVendorName
	Set oCatalogVendor = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		SetVendor
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub SetVendor ()

	Dim oCatalogVendor

	If (g_sOldVendorName = g_sVendorName) Then Exit Sub
	GetCatalogVendorObject oCatalogVendor
	If (oCatalogVendor Is Nothing) Then Exit Sub

	If (g_sVendorName = "") Then
		Call DeleteCatalogVendor (g_sCtName)
	Else
		Call oCatalogVendor.SpecifyVendorForCatalog (g_sCtName, _
												Request.Form("vendor_qualifier"), _
												Request.Form("qualifier_value"), _
												g_sVendorName)
	End If

	g_sOldVendorName = g_sVendorName
	Set oCatalogVendor = Nothing
End Sub

%>