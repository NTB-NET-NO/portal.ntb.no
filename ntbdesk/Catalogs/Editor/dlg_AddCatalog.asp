<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="catEditorSTRINGS.asp" -->
<!--#INCLUDE FILE="common.asp" -->

<%
	Const PAGE_TYPE = "DIALOG"
' ###############################
	Perform_ServerSide_Processing
' ###############################
%>

<HTML>
<HEAD>
<TITLE><%= L_NewCustomCatalog_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
<!--
BODY
{
	PADDING:15px;
	MARGIN:0;
}
-->
</STYLE>

<SCRIPT LANGUAGE='VBScript'>

option explicit

sub bdcancel_OnClick()

	window.returnValue = ""
	window.close
end sub

sub bdok_OnClick()

	window.returnValue = catalog.value
	window.close
end sub

sub window_onLoad()
<%
If (g_sCatalogs = "") Then
%>
	bdcancel.focus
<%
Else
%>
	bdok.focus
<%
End If
%>
end sub

</SCRIPT>
</HEAD>

<BODY SCROLL="no" LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<% If (g_sCatalogs <> "") Then %>
<xml id='efMeta'>
	<editfield>
		<select id="catalog" onchange="pricing_onChange()">
			<select id="pr_method">
				<%= g_sCatalogs %>
			</select>
		</select>
	</editfield>
</xml>
<xml id='efData'>
	<document/>
</xml>

<LABEL FOR="catalog"><%= L_SelectBaseCatalog_Text %></LABEL><BR><BR>
<div STYLE="width:300px">	
	<DIV ID='catalog' CLASS='editField'
		MetaXML='efMeta'
		DataXML='efData'></DIV></div>
<% Else %>
<%= L_NoCatalogs_Text %>
<% End If %><BR><BR><BR>

<TABLE>
	<TR>
		<TD STYLE='width:230px'></TD>
		<TD><BUTTON ID='bdok' CLASS='bdbutton' STYLE='<%= L_DialogButton_Style %>' <% If (g_sCatalogs = "") Then %>DISABLED<% End If %>><%= L_OK_Button %></BUTTON>
			<BUTTON ID='bdcancel' CLASS='bdbutton' STYLE='<%= L_DialogButton_Style %>'><%= L_Cancel_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>

<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'----- PUBLIC Declarations
Public g_sCatalogs

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	g_sCatalogs = sCatalogsAsOptions()
End Sub

' ##########################################################
'
Function sCatalogsAsOptions ()

	Dim sCatName	
	Dim rsCatalogs

	InitializeCatalogObject

	Set rsCatalogs = g_oCatMgr.Catalogs
	While Not rsCatalogs.EOF
		sCatName = rsCatalogs("CatalogName").Value
		If (rsCatalogs("CustomCatalog") = False) Then
			sCatalogsAsOptions = sCatalogsAsOptions & "<option value='" & replace(sCatName, "'", "&apos;") & "'>" & Server.HTMLEncode(sCatName) & "</option>" & vbCr
		End If

		rsCatalogs.MoveNext
	Wend

	rsCatalogs.Close
	Set rsCatalogs = Nothing
	Set g_oCatMgr = Nothing
End Function

%>