<% If (PAGE_TYPE <> "RESPONSE") Then %>

<SCRIPT LANGUAGE="VBScript">
'import display constants
const BLINK_SPEED = 3
const SECONDS_TO_CHECK = 45
'import display vars
public bImportToggle
public oImportTimer
public nImportCount

'' -------------------------------------------------------------------
''
''	Function:		AddArray
''
''	Description:	This subroutine adds a string selection 
''					to a string array.
''
''	Arguments:		aArray - 
''					sString -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub AddArray (ByRef aArray, ByRef sString)

	dim iNewUBound

	iNewUBound = UBound(aArray) + 1
	redim preserve aArray(iNewUBound)

	aArray(iNewUBound) = sString
end sub

'' -------------------------------------------------------------------
''
''	Function:		SplitToDynamicArray
''
''	Description:	
''					
''	Arguments:		sText -
''					sDelimiter -
''					aDynamic -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub SplitToDynamicArray (ByRef sText, ByRef sDelimiter, ByRef aDynamic)

	dim aTemp
	dim sItem

	aTemp = Split(sText, sDelimiter)
	for each sItem in aTemp
		AddArray aDynamic, Trim(sItem)
	next
end sub

'' -------------------------------------------------------------------
''
''	Function:		CheckForceUpdate
''
''	Description:	
''
''	Arguments:		sAction -
''					oForm - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub CheckForceUpdate (ByRef sAction, ByRef oForm)

	dim sResponse

	if (sAction = "forceUpdate") then
		' Inform user about Force Update action
		sResponse = MsgBox (L_ForceUpdate_Text, vbYesNo + vbQuestion, L_BizDesk_Text)
		if (sResponse = vbYes) then
			' Continue Force Update
			with oForm
				.type.value = .type.value & "F" ' submitEditF, saveNewF
				.submit
			end with
		end if
	end if
end sub

</SCRIPT>

<SCRIPT LANGUAGE="VBScript" DEFER>

'' -------------------------------------------------------------------
''
''	Microsoft Commerce Server 2000
''
''	Copyright 1996 - 2000 Microsoft Corporation. All Rights Reserved.
''
''	File:			common.asp
''
''	Description:	This file contains procedures used 
''					across Catalog Editor pages.
''
'' -------------------------------------------------------------------

'' ###################################################################
''
''	CLIENT-SIDE Functionality
''
'' ###################################################################

'' -------------------------------------------------------------------
''
''	Global CONST Declarations
''
'' -------------------------------------------------------------------
const SEPARATOR1 = "|||"
const SEPARATOR2 = "!!!"
' ----------------------------------------------
const CATALOG_PARAMETERS = "CatalogParameters"
const PRODUCT_PARAMETERS = "ProductParameters"
const CATEGORY_PARAMETERS = "CategoryParameters"

'' -------------------------------------------------------------------
''
''	Function:		DeleteArray
''
''	Description:	This procedure deletes a string selection 
''					from a string array.
''
''	Arguments:		aArray - 
''					sString -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub DeleteArray (ByRef aArray, ByRef sString)

	dim iTemp
	dim iUBound

	dim bFound

	bFound = False
	iUBound = UBound(aArray)

	for iTemp = 0 to iUBound
		if (aArray(iTemp) = sString) then
			aArray(iTemp) = aArray(iUBound)
			bFound = True
		end if
	next

	if (True = bFound) then redim preserve aArray(iUBound - 1)
end sub

'' -------------------------------------------------------------------
''
''	Function:		bRemovedFromArray
''
''	Description:	This function deletes a string selection 
''					from a string array if it exists.
''
''	Arguments:		aArray -
''					sString -
''
''	Returns:		True or False
''
'' -------------------------------------------------------------------
function bRemovedFromArray (ByRef aArray, sString)

	dim iTemp
	dim bDeleted

	bDeleted = False

	for iTemp = LBound(aArray) to UBound(aArray)
		if (Trim(aArray(iTemp)) = Trim(sString)) then
			bDeleted = True
			if (iTemp < UBound(aArray)) then aArray(iTemp) = aArray(UBound(aArray))
			redim preserve aArray(UBound(aArray) - 1)
			exit for
		end if
	next

	bRemovedFromArray = bDeleted
end function

' #####################################################################
' Function: 
'	This function converts an array into a triple-pipe-delimited string
' 	("|||") for passing between pages
'
' Input:
'	aArray - the array to load
'
'
'' -------------------------------------------------------------------
''
''	Function:		sConvertArrayToString
''
''	Description:	This function converts an array into a 
''					triple-pipe-delimited string ("|||") for passing 
''					between pages.
''
''	Arguments:		aArray
''
''	Returns:		string
''
'' -------------------------------------------------------------------
function sConvertArrayToString (ByRef aArray)

	dim iLen
	dim sItem
	dim sString
	
	if IsArray(aArray) then
		for each sItem in aArray
			sString = sString & sItem + SEPARATOR1
		next

		iLen = Len(sString)
		if (iLen >= 3) then
			sString = Left (sString, iLen - Len(SEPARATOR1))
		end if
	end if

	sConvertArrayToString = sString
end function

'' -------------------------------------------------------------------
''
''	Function:		sGetCurrentCategoryName
''
''	Description:	Gets Name of currently selected category.
''
''	Arguments:		none
''					
''	Returns:		string (CategoryName)
''
'' -------------------------------------------------------------------
function sGetCurrentCategoryName ()

	if (g_oCategory is Nothing) then
		sGetCurrentCategoryName = ""
	else
		sGetCurrentCategoryName = g_oCategory.getAttribute("caption")
		if (True = IsNull(sGetCurrentCategoryName)) then
			sGetCurrentCategoryName = g_oCategory.text
		end if
	end if
end function

'' -------------------------------------------------------------------
''
''	Function:		sGetCategoryFromEvent
''
''	Description:	Retrieves CategoryName corresponding to the most 
''					recent event triggered by a TreeView instance
''					
''	Arguments:		none
''
''	Returns:		string (CategotyName)
''
'' -------------------------------------------------------------------
function sGetCategoryFromEvent ()

	sGetCategoryFromEvent = window.event.XMLitem.getAttribute("caption")
	if (True = IsNull(sGetCategoryFromEvent)) then _
		sGetCategoryFromEvent = window.event.XMLitem.text
end function

'' -------------------------------------------------------------------
''
''	Function:		setEditPageParameters
''
''	Description:	
''					
''	Arguments:		sDictName - 
''					aKeys-
''					aValues - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub setEditPageParameters (ByRef sDictName, ByRef aKeys, ByRef aValues)

	dim iIndex
	dim dParams
	dim dServerState

	set dParams = CreateObject ("Scripting.Dictionary")
	for iIndex=0 to UBound(aKeys)
		dParams.Add aKeys(iIndex), aValues(iIndex)
	next
	set dServerState = dSetServerState (sDictName, dParams)

	set dParams = Nothing
	set dServerState = Nothing
end sub

'' -------------------------------------------------------------------
''
''	Function:		ImportProgress
''
''	Description:	
''					
''	Arguments:		sDictName - 
''					aKeys-
''					aValues - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ImportProgress ()
	if bImportToggle then
		Call SetStatusText ("<%= L_CatalogImportInProgress_StatusBar %>")
	else
		Call ClearStatusText ()
	end if
	nImportCount = nImportCount + BLINK_SPEED
	if nImportCount >= SECONDS_TO_CHECK then call CheckImportProgress ()
End Sub

'' -------------------------------------------------------------------
''
''	Function:		CheckImportProgress
''
''	Description:	
''					
''	Arguments:		sDictName - 
''					aKeys-
''					aValues - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub CheckImportProgress ()
	dim nImportOps, oXMLHTTPObj, bAsync
	'create XMLHTTP object and query page that executes the following query and returns the result:
	'  SELECT Count(StatusID) AS ImportOps FROM CatalogStatus WHERE EndDate = NULL OR Status <> 0
On Error Resume Next
	set oXMLHTTPObj = CreateObject("MSXML2.XMLHTTP.2.6")
	bAsync = False
	'response page just returns an integer - greater than zero means import in progress
	oXMLHTTPObj.open "POST", "Response_Import.asp", bAsync
	oXMLHTTPObj.send 
	nImportOps = cInt(oXMLHTTPObj.responseText)
	if (Err.Number <> 0) then nImportOps = -1

	if nImportOps > 0 then
		'an import is in progress so continue counting
		nImportCount = 0
	else
		window.clearInterval(oImportTimer)
		call SetTemp("bCatImporting", False)
		dim sStatus
		if nImportOps = -1 then
			sStatus = "<%= L_CatalogImportErrors_StatusBar %>"
		else
			sStatus = "<%= L_CatalogImportComplete_StatusBar %>"
		end if
		call SetStatusText (sStatus)
	end if
end sub

'' -------------------------------------------------------------------
''
''	Function:		window_onunload
''
''	Description:	
''					
''	Arguments:		sDictName - 
''					aKeys-
''					aValues - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
sub window_onunload ()
	if not isEmpty(oImportTimer) then window.clearInterval(oImportTimer)
end sub

</SCRIPT>

<% End If %>

<%

'' ###################################################################
''
''	SERVER-SIDE Functionality
''
'' ###################################################################

'' -------------------------------------------------------------------
''
''	Global CONST Declarations
''
'' -------------------------------------------------------------------

Const CATEGORY_CLASS = 1
Const VARIANT_CLASS = 2
Const PRODUCT_CLASS = 4
Const FAMILY_CLASS = 8
Const PRODUCTFAMILY_FOR_VARIANTS_CLASS = 16
Const VARIANTFORFAMILY_CLASS = 32
' ------ PROPERTY Types
Const REGULAR_PROPERTY = 0
Const VARIANT_PROPERTY = 1
' ------ CATALOG DataType Enum
Const INTEGER_TYPE = 0
Const BIGINTEGER_TYPE = 1
Const FLOAT_TYPE = 2
Const DOUBLE_TYPE = 3
Const BOOLEAN_TYPE = 4
Const STRING_TYPE = 5
Const DATETIME_TYPE = 6
Const CURRENCY_TYPE = 7
Const FILEPATH_TYPE = 8
Const ENUMERATION_TYPE = 9

Const DATE_TYPE = 10
Const TIME_TYPE = 11
' ------ Range of SQL Data Types
Const MIN_INT = -2147483648
Const MAX_INT =  2147483647
Const MIN_FLOAT = -1.79E+38
Const MAX_FLOAT =  1.79E+38
Const MIN_MONEY = -922337203685477.5808
Const MAX_MONEY =  922337203685477.5807

' ------ 
Const ERR_PROPERTY_ALREADY_MODIFIED = 6
' ------
Const EMPTY_XML			 = "<document recordcount='0'><record/></document>"
Const EMPTY_DOCUMENT_XML = "<document recordcount='0'><record/></document>"
' ------ String Separators
Const SEPARATOR1 = "|||"
Const SEPARATOR2 = "!!!"
' ------ # of Lines in a LISTSHEET widget instance
Const PAGE_SIZE = 20
' ----------------------------------------------
Const DEFINITION_NAME = "DefinitionName"
Const CATALOG_PARAMETERS = "CatalogParameters"
Const PRODUCT_PARAMETERS = "ProductParameters"
Const CATEGORY_PARAMETERS = "CategoryParameters"

'' -------------------------------------------------------------------
''
''	PUBLIC Declarations
''
'' -------------------------------------------------------------------

Public g_oCat			' the Current Catalog Object
Public g_oCatMgr		' the Catalog Manager Object

Public g_sConnString	' Connection String for the Current Catalog
Public g_sThousands		' Thousands Separator

'' -------------------------------------------------------------------
''
''	Function:		InitializeCatalogObject
''
''	Description:	Creates and initializes a global Catalog 
''					Manager Object.
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub InitializeCatalogObject ()

	Const myLOCALE_STHOUSAND = 15

	On Error Resume Next
	Set g_oCatMgr = Nothing
	g_sThousands = g_MSCSDataFunctions.GetLocaleInfo(myLOCALE_STHOUSAND, Application("MSCSCurrencyLocale"))

	If (True = IsObject(Session("CatalogManager"))) Then
		If Not (Session("CatalogManager") Is Nothing) Then
			Dim rs
			Set g_oCatMgr = Session("CatalogManager")
			Set rs = g_oCatMgr.Catalogs
			If (Err.Number <> 0) Then
				Set g_oCatMgr = Nothing
				Set Session("CatalogManager") = Nothing
				If (PAGE_TYPE <> "RESPONSE") Then _
					Call setError (L_Critical_DialogTitle, L_BadConnectionString_ErrorMessage, _
								   sGetScriptError(Err), ERROR_ICON_CRITICAL)
				Err.Clear
				Exit Sub
			End If
		End If
	End If

	If (g_oCatMgr Is Nothing) Then
		g_sConnString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")

		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (g_sConnString, True)
		If (Err.Number <> 0) Then
			Set g_oCatMgr = Nothing
			If (PAGE_TYPE <> "RESPONSE") Then _
				Call setError (L_Critical_DialogTitle, L_BadConnectionString_ErrorMessage, _
							   sGetScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
		Else
			Set Session("CatalogManager") = g_oCatMgr	
		End If
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		AddArray
''
''	Description:	This subroutine adds a string selection 
''					to a string array.
''
''	Arguments:		aArray - 
''					sString -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub AddArray (ByRef aArray, ByRef sString)

	Dim iNewUBound

	iNewUBound = UBound(aArray) + 1
	ReDim Preserve aArray(iNewUBound)

	aArray(iNewUBound) = sString
End Sub

'' -------------------------------------------------------------------
''
''	Function:		SplitToDynamicArray
''
''	Description:	
''					
''	Arguments:		sText - 
''					sDelimiter - 
''					aDynamic -
''
''	Returns:		
''
'' -------------------------------------------------------------------
Sub SplitToDynamicArray (ByRef sText, ByRef sDelimiter, ByRef aDynamic)

	Dim aTemp
	Dim sItem

	aTemp = Split(sText, sDelimiter)
	For Each sItem In aTemp
		AddArray aDynamic, Trim(sItem)
	Next
End Sub

'' -------------------------------------------------------------------
''
''	Function:		sReplaceSpaces
''
''	Description:	This function replaces spaces with a substitute 
''					character for display as XML IDs.
''					
''	Arguments:		sString - 
''					
''
''	Returns:		string (with substituted spaces)
''
'' -------------------------------------------------------------------
Function sReplaceSpaces (ByRef sString)

	sReplaceSpaces = Replace(sString, " ", "_zvbca_")
End Function

'' -------------------------------------------------------------------
''
''	Function:		sConvertArrayToString
''
''	Description:	This function converts an array into a 
''					triple-pipe-delimited string ("|||") for passing 
''					between pages.
''
''	Arguments:		aArray - 
''					
''	Returns:		string
''
'' -------------------------------------------------------------------
Function sConvertArrayToString (ByRef aArray)

	sConvertArrayToString = Join (aArray, SEPARATOR1)
End Function

'' -------------------------------------------------------------------
''
''	Function:		oCreateXMLDocument
''
''	Description:	This function instantiates an XML document 
''					passed in data.
''
''	Arguments:		sXML - XML to instantiate
''					
''
''	Returns:		object (XMLNode)
''
'' -------------------------------------------------------------------
Function oCreateXMLDocument (ByRef sXML)

	Dim oXmlDoc

	Set oXmlDoc = Server.CreateObject("Microsoft.XMLDOM")
	oXmlDoc.loadXML(sXML)

	Set oCreateXMLDocument = oXmlDoc
End Function

'' -------------------------------------------------------------------
''
''	Function:		ProcessRelationshipXML
''
''	Description:	Processes relationships XML corresponding to 
''					a Product or Category
''
''	Arguments:		oXMLDoc -
''					oItem - the category or product 
''							to add/delete relationships for
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ProcessRelationshipXML (ByRef oXMLdoc, ByRef oItem)

	Dim sState
	Dim sRelatedID
	Dim sRelationshipName
	Dim sRelationshipDescr

	Dim oRecordNode

	On Error Resume Next
	If oXMLdoc.documentElement.hasChildNodes Then
		For Each oRecordNode In oXMLdoc.documentElement.selectNodes("//record[@state $ieq$ 'deleted']")
			sRelatedID = oRecordNode.selectSingleNode("./rel_item_old").text
			sRelationshipName = oRecordNode.selectSingleNode("./rel_name_old").text
			sRelationshipDescr = oRecordNode.selectSingleNode("./rel_descr_old").text

			RemoveRelationship oItem, sRelatedID, sRelationshipName, sRelationshipDescr
		Next

		For Each oRecordNode In oXMLdoc.documentElement.selectNodes("//record[@state $ine$ 'deleted']")
			sState = "": sState = oRecordNode.getAttribute("state")

			Select Case sState
			Case "new"
				sRelatedID = oRecordNode.selectSingleNode("./rel_item").text
				sRelationshipName = oRecordNode.selectSingleNode("./rel_name").text
				sRelationshipDescr = oRecordNode.selectSingleNode("./rel_descr").text

				AddRelationship oItem, sRelatedID, sRelationshipName, sRelationshipDescr

			Case "changed"
				sRelatedID = oRecordNode.selectSingleNode("./rel_item_old").text
				sRelationshipName = oRecordNode.selectSingleNode("./rel_name_old").text
				sRelationshipDescr = oRecordNode.selectSingleNode("./rel_descr_old").text

				RemoveRelationship oItem, sRelatedID, sRelationshipName, sRelationshipDescr
		
				sRelatedID = oRecordNode.selectSingleNode("./rel_item").text
				sRelationshipName = oRecordNode.selectSingleNode("./rel_name").text
				sRelationshipDescr = oRecordNode.selectSingleNode("./rel_descr").text

				AddRelationship oItem, sRelatedID, sRelationshipName, sRelationshipDescr
			End Select
		Next
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		RemoveRelationship
''
''	Description:	
''
''	Arguments:		oItem -
''					sRelatedID - 
''					sRelationshipName - 
''					sRelationshipDescr -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub RemoveRelationship (ByRef oItem, ByRef sRelatedID, _
						ByRef sRelationshipName, ByRef sRelationshipDescr)

	If (sRelationshipDescr = L_Product_Text) Then
		oItem.RemoveRelationshipToProduct sRelatedID, sRelationshipName
	Else'If(sRelationshipType = L_Category_Text) Then
		oItem.RemoveRelationshipToCategory sRelatedID, sRelationshipName
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		AddRelationship
''
''	Description:	
''					
''	Arguments:		oItem -
''					sRelatedID - 
''					sRelationshipName - 
''					sRelationshipDescr -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub AddRelationship (ByRef oItem, ByRef sRelatedID, _
					 ByRef sRelationshipName, ByRef sRelationshipDescr)

	If (sRelationshipDescr = L_Product_Text) Then
		oItem.AddRelationshipToProduct sRelatedID, _
			sRelationshipName, sRelationshipDescr
	Else'If(sRelationshipType = L_Category_Text) Then
		oItem.AddRelationshipToCategory sRelatedID, _
			sRelationshipName, sRelationshipDescr
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		sGetOnOff
''
''	Description:	
''
''	Arguments:		sValue -
''					
''	Returns:		
''
'' -------------------------------------------------------------------
Function sGetOnOff (ByRef sValue)

	If (sValue = "on") Then
		sGetOnOff = "1" 'On
	Else
		sGetOnOff = "0" 'Off
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		sConvertFromBoolean
''
''	Description:	
''
''	Arguments:		bValue
''					
''	Returns:		string ("0" or "1")
''
'' -------------------------------------------------------------------
Function sConvertFromBoolean (ByVal bValue)

	If (bValue = True) Then
		sConvertFromBoolean = "1"
	Else
		sConvertFromBoolean = "0"
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		getPropertyAttribute
''
''	Description:	
''					
''	Arguments:		sPropertyName -
''					sAttribute -
''
''	Returns:		
''
'' -------------------------------------------------------------------
Function getPropertyAttribute (ByRef sPropertyName, ByRef sAttribute)

	On Error Resume Next

	g_rsProperties.MoveFirst
	Do Until g_rsProperties.EOF
		If (g_rsProperties ("PropertyName").Value = sPropertyName) Then _
			Exit Do

		g_rsProperties.MoveNext
	Loop

	getPropertyAttribute = -1
	If (g_rsProperties.EOF = False) Then
		getPropertyAttribute = g_rsProperties.Fields(sAttribute)
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetEnumOptions
''
''	Description:	
''
''	Arguments:		sProperty -
''
''	Returns:		string (concatenated <OPTION> elements)
''
'' -------------------------------------------------------------------
Function sGetEnumOptions (ByRef sProperty)

	Dim sValue
	Dim rsValues

	Set rsValues = g_oCatMgr.GetPropertyValues(sProperty)
	Do Until rsValues.EOF
		sValue = rsValues("Value")
		sGetEnumOptions = sGetEnumOptions & "<option value='" & sValue & "'>" & sValue & "</option>"

		rsValues.MoveNext
	Loop

	ReleaseRecordset rsValues
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetListData
''
''	Description:	
''					
''	Arguments:		none
''					
''	Returns:		string (XML Data corresponding to a ListSheet widget)
''
'' -------------------------------------------------------------------
Function sGetListData ()

	sGetListData = EMPTY_DOCUMENT_XML
	If (False = IsObject(g_oCat)) Then Exit Function
	If (g_oCat Is Nothing) Then Exit Function

	Call RetrievePageBasedOn_Category (g_nPageID, g_nItems1)
	sGetListData = g_sXML
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetSearchListData
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		string (XML Data corresponding to a ListSheet widget)
''
'' -------------------------------------------------------------------
Function sGetSearchListData ()

	sGetSearchListData = EMPTY_DOCUMENT_XML
	If (False = IsObject(g_oCat)) Then Exit Function
	If (g_oCat Is Nothing) Then Exit Function

	Call RetrievePageBasedOn_Properties (g_nSearchPageID, g_nItems2)
	sGetSearchListData = g_sXML
End Function

'' -------------------------------------------------------------------
''
''	Function:		RetrievePageBasedOn_Category
''
''	Description:	
''
''	Arguments:		nPageID -
''					nItems - 
''
''	Returns:		
''
'' -------------------------------------------------------------------
Sub RetrievePageBasedOn_Category (ByRef nPageID, ByRef nItems)

	Dim oCategory
	Dim iClassType
	Dim iStartRecord

	If (g_sCategory = "") Or (nPageID = 0) Then
		g_sXML = g_sXML & EMPTY_DOCUMENT_XML
		Exit Sub
	End If

	Set oCategory = g_oCat.GetCategory(g_sCategory)
	If (oCategory Is Nothing) Then
		g_sXML = g_sXML & EMPTY_DOCUMENT_XML
		Exit Sub
	End If

	iStartRecord = PAGE_SIZE * (nPageID - 1) + 1

	If (g_sMode = "AUCTIONS") Then
		iClassType = PRODUCT_CLASS + VARIANTFORFAMILY_CLASS
	Else
		iClassType = PRODUCT_CLASS + FAMILY_CLASS
	End If

	Set g_rsProducts = oCategory.DescendantProducts(iClassType, iStartRecord, PAGE_SIZE, nItems)
	Set oCategory = Nothing

	g_sXML = g_sXML & sGetXML (nItems)
End Sub

'' -------------------------------------------------------------------
''
''	Function:		RetrievePageBasedOn_Properties
''
''	Description:	
''
''	Arguments:		nPageID - 
''					nItems -
''
''	Returns:		
''
'' -------------------------------------------------------------------
Sub	RetrievePageBasedOn_Properties (ByRef nPageID, ByRef nItems)

	Dim sPhrase
	Dim sFields

	Dim iClassType
	Dim iStartRecord

	Dim oFilterXML
	dim sProperty

	'---- START retrieving the current page
	If (nPageID = 0) Then nPageID = 1
	iStartRecord = PAGE_SIZE * (nPageID - 1) + 1

	For Each sProperty In g_aProperties
		sFields = sFields & "[" & sProperty &  "], "
	next
	If (g_sMode = "AUCTIONS") Then
		sFields = sFields & "i_ClassType"
	else
		'remove trailing ", "
		sFields = Left(sFields, len(sFields) - 2)
	end if

	On Error Resume Next

	If (g_sKeywords = "") Then
		If (g_sFilterXML <> "") And (g_sFilterXML <> "null") Then
			Set oFilterXML = oCreateXMLDocument(g_sFilterXML)
			If (True = IsObject (oFilterXML)) Then _
				AddFilterXMLToProductQuery oFilterXML.documentElement, "", sPhrase
		End If
		If (sPhrase = "") Then
			sPhrase = "(i_ClassType > 0)"
		Else
			sPhrase = "(i_ClassType > 0) AND (" &  sPhrase & ")"
		End If

		If (g_sMode = "AUCTIONS") Then
			iClassType = PRODUCT_CLASS + VARIANT_CLASS
		Else
			iClassType = PRODUCT_CLASS + FAMILY_CLASS + PRODUCTFAMILY_FOR_VARIANTS_CLASS
		End If
		' QUERY for Property Values
		Set g_rsProducts = g_oCatMgr.Query (sPhrase, g_sCtName, iClassType, sFields, "[" & g_sProductID & "]", -1, iStartRecord, PAGE_SIZE, nItems)
	Else'If(g_sKeywords <> "") Then
		sPhrase = g_sKeywords

		If (g_sMode = "AUCTIONS") Then
			iClassType = PRODUCT_CLASS + VARIANTFORFAMILY_CLASS
		Else
			iClassType = PRODUCT_CLASS + FAMILY_CLASS + PRODUCTFAMILY_FOR_VARIANTS_CLASS
		End If
		' Perform FREE-TEXT Search
		Set g_rsProducts = g_oCatMgr.FreeTextSearch (sPhrase, g_sCtName, iClassType, sFields, "[" & g_sProductID & "]", -1, iStartRecord, PAGE_SIZE, nItems)
	End If

	g_sXML = sGetXML (nItems)
End Sub

'' -------------------------------------------------------------------
''
''	Function:		AddFilterXMLToProductQuery
''
''	Description:	
''
''	Arguments:		oFilterXML - 
''					sMetaOperator - 
''					sPhrase -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub AddFilterXMLToProductQuery (ByRef oFilterXML, _
								ByRef sMetaOperator, _
								ByRef sPhrase)
	Dim sTerm
	Dim oRecordNode ' to hold XML from widgets

	If Not oFilterXML.hasChildNodes Then Exit Sub

	If oFilterXML.nodeName = "TERM" Then
		For Each oRecordNode In oFilterXML.childNodes
			AddFilterXMLToProductQuery oRecordNode, UCase (oFilterXML.getAttribute("TYPE")), sTerm
		Next
	ElseIf oFilterXML.nodeName = "CLAUSE" Then
		GetClause oFilterXML, sTerm
	End If

	If (sPhrase = "") Then
		sPhrase = sTerm
	Else
		sPhrase = sPhrase & sMetaOperator & sTerm
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetClause
''
''	Description:	
''
''	Arguments:		oRecordNode -
''					sClause -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetClause (ByRef oRecordNode, ByRef sClause)

	Dim iType
	Dim sType
	Dim sValue
	Dim oValue
	Dim sProperty
	Dim sOperator

	' Capture the property to search
	With oRecordNode.selectSingleNode("./Property")
		sProperty = .getAttribute("ID")
		sType = .getAttribute("TYPE")
	End With
	sProperty = Right(sProperty, Len(sProperty) - InStr(sProperty, "."))
	sProperty = Right(sProperty, Len(sProperty) - InStr(sProperty, "."))

	' Capture the operator to use
	sOperator = oRecordNode.getAttribute("OPER")
	' Capture the value to search for
	Set oValue = oRecordNode.selectSingleNode("./IMMED-VAL")
	If IsObject (oValue) Then
		If Not (oValue Is Nothing) Then sValue = oValue.text
	End If

	Select Case sOperator
	Case "contains", "not-contains"
		sValue = "%" & sValue & "%"
	Case "begins-with", "not-begins-with"
		sValue = sValue & "%"
	End Select

	If (sProperty = "cy_list_price") Then
		iType = CURRENCY_TYPE
	Else
		iType = getPropertyAttribute(sProperty, "DataType")
	End If
	sValue = sConvertToEnglishLocale (sValue, iType)

	Select Case sType
	Case "STRING", "SITETERM", "DATE"
		If (sOperator <> "is-defined") And _
		   (sOperator <> "not-defined") Then sValue = "'" & sValue & "'"
	End Select

	sOperator = sConvertQBOperator(sOperator)
	sClause = "([" & sProperty & "] " & sOperator & " " & sValue & ")"
End Sub

'' -------------------------------------------------------------------
''
''	Function:		sConvertToEnglishLocale
''
Function sConvertToEnglishLocale (ByRef sText, ByVal iType)

	Const ENGLISH_LOCALE = 1033
	Const myLOCALE_STHOUSAND = 15

	Dim sValue
	Dim varValue
	Dim sThousands

	sValue = sText
	sThousands = g_MSCSDataFunctions.GetLocaleInfo(myLOCALE_STHOUSAND, ENGLISH_LOCALE)

	If (sValue <> "") Then
		Select Case iType
		Case INTEGER_TYPE
			varValue = g_MSCSDataFunctions.ConvertNumberString (sValue, g_DBDefaultLocale)
			If Not IsNull (varValue) Then _
				sValue = replace(g_MSCSDataFunctions.Number (varValue, ENGLISH_LOCALE), sThousands, "")
		Case FLOAT_TYPE
			varValue = g_MSCSDataFunctions.ConvertFloatString (sValue, g_DBDefaultLocale)
			If Not IsNull (varValue) Then _
				sValue = replace(g_MSCSDataFunctions.Float (CStr(varValue), ENGLISH_LOCALE, True, True), sThousands, "")
		Case CURRENCY_TYPE
			varValue = g_MSCSDataFunctions.ConvertStringToCurrency (sValue, g_DBCurrencyLocale)
			If Not IsNull (varValue) Then _
				sValue = replace(replace(g_MSCSDataFunctions.LocalizeCurrency(varValue, ENGLISH_LOCALE, " "), sThousands, ""), " ", "")
		End Select
	End If

	sConvertToEnglishLocale = sValue
End Function 

'' -------------------------------------------------------------------
''
''	Function:		sConvertQBOperator
''
''	Description:	
''
''	Arguments:		sOperator - 
''					
''	Returns:		string (relational operator in Transact SQL format)
''
'' -------------------------------------------------------------------
Function sConvertQBOperator (ByRef sOperator)

	Select Case sOperator
	Case "contains", "begins-with"
		sConvertQBOperator = "like"
	Case "not-contains", "not-begins-with"
		sConvertQBOperator = "not like"
	Case "equal"
		sConvertQBOperator = "="
	Case "not-equal"
		sConvertQBOperator = "<>"
	Case "greater-than","after"
		sConvertQBOperator = ">"
	Case "less-than","before"
		sConvertQBOperator = "<"
	Case "at-least","on-or-after"
		sConvertQBOperator = ">="
	Case "at-most","on-or-before"
		sConvertQBOperator = "<="
	Case "is-defined"
		sConvertQBOperator = "IS NOT NULL"
	Case "not-defined"
		sConvertQBOperator = "IS NULL"
	Case Else
		sConvertQBOperator = "="		
	End Select
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetXML
''
''	Description:	
''
''	Arguments:		none
''					
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function sGetXML (ByRef nItems)

	'----- Generate XML - START
	sGetXML = EMPTY_DOCUMENT_XML
	If IsEmpty(g_rsProducts) Then Exit Function
	If (g_rsProducts Is Nothing) Then Exit Function
	If (g_rsProducts.EOF = True) Then Exit Function
	sGetXML = ""

	While (Not g_rsProducts.EOF)
		sGetXML = sGetXML & sGetRecordXML
		g_rsProducts.MoveNext
	Wend

	sGetXML = "<document recordcount='" & nItems & "'>" & sGetXML & "</document>"
	'----- Generate XML - STOP
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetRecordXML
''
''	Description:	
''
''	Arguments:		
''
''	Returns:		
''
'' -------------------------------------------------------------------
Function sGetRecordXML ()

	Dim iIndex

	Dim sValue
	Dim sProperty
	Dim sEncodedProperty

	sGetRecordXML = "<record>"
	For iIndex = 0 To UBound(g_aProperties) 
		sProperty = g_aProperties(iIndex)
		sEncodedProperty = sReplaceSpaces(sProperty)
		sValue = stringValue (g_rsProducts(sProperty).Value, g_aPropertiesTypes(iIndex))
		If (Not IsNull(sValue)) Then sValue = Server.HTMLEncode(sValue)

		sGetRecordXML = sGetRecordXML &	"<" & sEncodedProperty & ">" & sValue & "</" & sEncodedProperty & ">"
	Next

	If (g_sMode = "AUCTIONS") Then
		sGetRecordXML = sGetRecordXML & "<i_ClassType>" & g_rsProducts("i_ClassType") & "</i_ClassType>"
	End If

	sGetRecordXML = sGetRecordXML & "</record>"
End Function

'' -------------------------------------------------------------------
''
''	Function:		GetTVData
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetTVData ()

	Dim rsCategories

	Set rsCategories = g_oCat.RootCategories("CategoryName")
	While Not rsCategories.EOF
		'GetCategoryTree rsCategories("CategoryName").Value
		GetCategory rsCategories("CategoryName").Value
		rsCategories.MoveNext
	Wend

	ReleaseRecordset rsCategories
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetCategory
''
''	Description:	
''					
''	Arguments:		sCategoryName -
''					
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetCategory (ByRef sCategoryName)

	Dim rs
	Dim sType
	Dim oCategory

	Set oCategory = g_oCat.GetCategory(sCategoryName)
	If (oCategory Is Nothing) Then Exit Sub

On Error Resume Next

	Set rs = oCategory.GetCategoryProperties
	If (True = rs.EOF) Then 
		ReleaseRecordset rs
		Exit Sub
	Else
		sType = rs("DefinitionName")
	End If
	ReleaseRecordset rs

	g_sTVData = g_sTVData & "<category caption=""" & Server.HTMLEncode(sCategoryName) & _
								""" closed=""yes"" type=""" & Server.HTMLEncode(sType) & """>"

	Set rs = oCategory.ChildCategories
	If Not rs.EOF Then _
		g_sTVData = g_sTVData & "<expand/>"
	ReleaseRecordset rs

	g_sTVData = g_sTVData & "</category>"
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetCategoryForViewCategoryPage
''
''	Description:	
''
''	Arguments:		sCategoryName
''
''	Returns:		
''
'' -------------------------------------------------------------------
Sub GetCategoryForViewCategoryPage (ByRef sCategoryName, ByRef sCurrentCategory)

	Dim bIsParent
	Dim nChildren
	Dim oCategory
	Dim rsChildren

	' If the Category Is the Current One
	If (sCategoryName = g_sCgName) Then Exit Sub

	Set oCategory = g_oCat.GetCategory (sCategoryName)
		If (oCategory Is Nothing) Then Exit Sub
		' Get the Child Categories
		Set rsChildren = oCategory.ChildCategories
	Set oCategory = Nothing

	bIsParent = bCheckIsParent(sCategoryName)
	' check child categories for particular cases
	If (Not rsChildren.EOF) Then
		rsChildren.MoveNext
		If (True = rsChildren.EOF) Then
			' there is only one child category
			rsChildren.MoveFirst
			If (rsChildren("CategoryName") <> sCurrentCategory) Then nChildren = 1
		Else
			nChildren = 1
		End If
	End If

	If (nChildren = 1) Then
		g_sTVData = g_sTVData & "<category closed='yes' caption=" & Chr(34) & Server.HTMLEncode(sCategoryName) & Chr(34)
		If (True = bIsParent) Then g_sTVData = g_sTVData & " selected='yes'"
		g_sTVData = g_sTVData & "><expand/>"
	Else'If(nChildren = 0) Then
		g_sTVData = g_sTVData & "<category"
		If (True = bIsParent) Then g_sTVData = g_sTVData & " selected='yes'"
		g_sTVData = g_sTVData & ">" & Server.HTMLEncode(sCategoryName)
	End If
	ReleaseRecordset rsChildren

	g_sTVData = g_sTVData & "</category>"
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetCategoryForViewCustomCatalogPage
''
''	Description:	
''
''	Arguments:		sCategoryName -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetCategoryForViewCustomCatalogPage (ByRef sCategoryName)

	Dim rs
	Dim oCategory
	Dim iCustomPrice
	Dim fpPriceAmount

On Error Resume Next
	iCustomPrice = 0
	fpPriceAmount = 0
	Set oCategory = g_oCat.GetCategory(sCategoryName)
	If (oCategory Is Nothing) Then Exit Sub

	If (g_sCtName <> "") Then _
		Call g_oCat.GetCategoryCustomPrice (g_sCtName, sCategoryName, iCustomPrice, fpPriceAmount)
Err.Clear	' API returns an error for those categories w/o price info

	g_sTVData = g_sTVData & "<category caption='" & Server.HTMLEncode(sCategoryName) & "' closed='yes'>"

	Set rs = oCategory.ChildCategories
	If Not rs.EOF Then _
		g_sTVData = g_sTVData & "<expand/>"
	ReleaseRecordset rs

	g_sTVData = g_sTVData & "<pricing modified='no' type='" & CStr(iCustomPrice) & "' hidden='yes'>" & _
							stringValue(fpPriceAmount, FLOAT_TYPE) & "</pricing>"
	
	g_sTVData = g_sTVData & "</category>"
	Set oCategory = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		bCheckIsParent
''
''	Description:	
''
''	Arguments:		sCategoryName - 
''
''	Returns:		boolean
''
'' -------------------------------------------------------------------
Function bCheckIsParent (ByRef sCategoryName)

	Dim sName

	bCheckIsParent = False
	For Each sName In g_aParentCategories
		If (sName = sCategoryName) Then
			bCheckIsParent = True
			Exit For
		End If
	Next
End Function

'' -------------------------------------------------------------------
''
''	Function:		GetCategoryTree
''
''	Description:	
''
''	Arguments:		sCategoryName -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetCategoryTree (ByRef sCategoryName)

	Dim rs
	Dim sType
	Dim oCategory

	Set oCategory = g_oCat.GetCategory(sCategoryName)
	If (oCategory Is Nothing) Then Exit Sub

On Error Resume Next

	Set rs = oCategory.GetCategoryProperties
	If (True = rs.EOF) Then 
		ReleaseRecordset rs
		Exit Sub
	Else
		sType = rs("DefinitionName")
	End If
	ReleaseRecordset rs

	Set rs = oCategory.ChildCategories
	If (Not rs.EOF) Then
		g_sTVData = g_sTVData & "<category caption=" & Chr(34) & sCategoryName & Chr(34) & _
							" type=" & Chr(34) & sType & Chr(34) & ">"
		While Not rs.EOF
			GetCategoryTree rs("CategoryName").Value
			rs.MoveNext
		Wend
	Else
		g_sTVData = g_sTVData & "<category type=" & Chr(34) & sType & Chr(34) & ">" & sCategoryName
	End If
	ReleaseRecordset rs

	g_sTVData = g_sTVData & "</category>"
End Sub

'' -------------------------------------------------------------------
''
''	Function:		sGetDTData
''
''	Description:	
''
''	Arguments:		oItem - could be an IProduct or ICategory object
''					
''	Returns:		string (XML Data corresponding to 
''					Product/Category relationships Dynamic Table).
''
'' -------------------------------------------------------------------
Function sGetDTData (ByRef oItem)

	Dim rsRelationships

	If (oItem Is Nothing) Then
		sGetDTData = "<record />"
		Exit Function
	End If
	' get Related Products
	Set rsRelationships = oItem.RelatedProducts
		sGetDTData = sGetDTDataBasedOnRecordset (rsRelationships, g_sProductID, L_Product_Text)
	ReleaseRecordset rsRelationships
	' get Related Categories
	Set rsRelationships = oItem.RelatedCategories
		sGetDTData = sGetDTData & sGetDTDataBasedOnRecordset (rsRelationships, "CategoryName", L_Category_Text)
	ReleaseRecordset rsRelationships

	If (sGetDTData = "") Then
		sGetDTData = "<record />"
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		sGetDTDataBasedOnRecordset
''
''	Description:	
''					
''	Arguments:		rs -
''					sID -
''					sDescription - 
''
''	Returns:		string (XML Data corresponding to 
''					Product/Category relationships Dynamic Table).
''
'' -------------------------------------------------------------------
Function sGetDTDataBasedOnRecordset (ByRef rs, ByRef sID, ByRef sDescription)

	Dim sItemID
	Dim sRelName

	While Not rs.EOF
		sItemID = Server.HTMLEncode(rs(sID))
		sRelName = rs("RelationshipName")

		sGetDTDataBasedOnRecordset = sGetDTDataBasedOnRecordset & _
			"<record>" & _
			"<rel_name_old>" & sRelName & "</rel_name_old>" & _
			"<rel_item_old>" & sItemID & "</rel_item_old>" & _
			"<rel_descr_old>" & sDescription & "</rel_descr_old>" & _
			"<rel_name>" & sRelName & "</rel_name>" & _
			"<rel_item>" & sItemID & "</rel_item>" & _
			"<rel_descr>" & sDescription & "</rel_descr>" & _
			"</record>" & vbCrLf

		rs.MoveNext
	Wend
End Function

'' -------------------------------------------------------------------
''
''	Function:		CheckForPropertyAlreadyModified
''
''	Description:	
''
''	Arguments:		none
''					
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub CheckForPropertyAlreadyModified ()

	If (Err.Number = ERR_PROPERTY_ALREADY_MODIFIED) Then
		g_sAction = "forceUpdate"
		Err.Clear
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetPropertiesToDisplayInList
''
''	Description:	
''
''	Arguments:		none
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetPropertiesToDisplayInList ()

	Dim sProperty

	If (g_sProductID <> "") Then 
		AddArray g_aProperties, g_sProductID
		AddArray g_aPropertiesTypes, getPropertyAttribute(g_sProductID, "DataType")
	End If
	If (g_sVariantID <> "") And (g_sVariantID <> g_sProductID) Then 
		AddArray g_aProperties, g_sVariantID
		AddArray g_aPropertiesTypes, getPropertyAttribute(g_sVariantID, "DataType")
	End If
	AddArray g_aProperties, DEFINITION_NAME
	AddArray g_aPropertiesTypes, STRING_TYPE

	On Error Resume Next
	g_rsProperties.MoveFirst
	Do Until g_rsProperties.EOF
		sProperty = g_rsProperties.Fields("PropertyName")

		If (sProperty <> g_sProductID) And (sProperty <> g_sVariantID) Then
			If (True = g_rsProperties.Fields("DisplayInProductsList")) Then
				AddArray g_aProperties, sProperty
				AddArray g_aPropertiesTypes, g_rsProperties.Fields("DataType").Value
			End If
		End If

		g_rsProperties.MoveNext
	Loop
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetListSheetMeta
''
''	Description:	
''					
''	Arguments:		none
''					
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetListSheetMeta ()

	Dim iWidth
	Dim sProperty

	iWidth = CInt(100 / (UBound(g_aProperties) + 1))

	For Each sProperty In g_aProperties
		g_sMeta = g_sMeta & "<column id='" & sReplaceSpaces(sProperty) & "' width='" & iWidth & "'"
		Select Case getPropertyAttribute(sProperty, "DataType")
		Case FLOAT_TYPE, CURRENCY_TYPE
			g_sMeta = g_sMeta & " type='float'"
		End Select
		If (sProperty = DEFINITION_NAME) Then sProperty = L_DefinitionName_Text
		g_sMeta = g_sMeta & ">" & Server.HTMLEncode(sProperty) & "</column>"
	Next
End Sub

'' -------------------------------------------------------------------
''
''	Function:		GetCatalogVendorObject
''
''	Description:	
''
''	Arguments:		oCtVendor -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub GetCatalogVendorObject (ByRef oCtVendor)

	Dim sConnectionString

	On Error Resume Next
	Set oCtVendor = Server.CreateObject ("Commerce.CatalogToVendorAssociation")
	If g_sConnString <> "" Then
		sConnectionString = g_sConnString
	Else
		sConnectionString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")
	End If
	oCtVendor.Initialize sConnectionString

	If (Err.Number <> 0) Then Set oCtVendor = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		DeleteCatalogVendor
''
''	Description:	
''					
''	Arguments:		sCatalogName - 
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub DeleteCatalogVendor (ByRef sCatalogName)

	Dim oCtVendor

	GetCatalogVendorObject oCtVendor
	If (oCtVendor Is Nothing) Then Exit Sub

	oCtVendor.UnspecifyVendorForCatalog (sCatalogName)
	Set oCtVendor = Nothing
End Sub

'' -------------------------------------------------------------------
''
''	Function:		variantValue
''
''	Description:	Casts sValue to the corresponding varProperty Type
''
''	Arguments:		sValue - 
''					varProperty -
''
''	Returns:		variant
''
'' -------------------------------------------------------------------
Function variantValue (ByRef sValue, ByVal varProperty)

	Dim iType

	If (sValue = "") Then
		variantValue = Null
		Exit Function
	End If

	If (True = IsNumeric(varProperty)) Then
		iType = varProperty
	Else
		iType = getPropertyAttribute(varProperty, "DataType")
	End If

	Select Case iType
	Case INTEGER_TYPE
		variantValue = g_MSCSDataFunctions.ConvertNumberString (sValue)
	Case FLOAT_TYPE
		variantValue = g_MSCSDataFunctions.ConvertFloatString (sValue)
	Case CURRENCY_TYPE
		variantValue = g_MSCSDataFunctions.ConvertStringToCurrency (sValue, g_MSCSCurrencyLocale)
	Case DATETIME_TYPE
		variantValue = g_MSCSDataFunctions.ConvertDateTimeString (sValue)
	Case BOOLEAN_TYPE
		variantValue = CBool(sValue)
	Case Else
		variantValue = sValue
	End Select
End Function

'' -------------------------------------------------------------------
''
''	Function:		stringValue
''
''	Description:	Casts varValue to STRING based on iType.
''
''	Arguments:		varValue -
''					iType -
''
''	Returns:		string (representation of varValue)
''
'' -------------------------------------------------------------------
Function stringValue (ByRef varValue, ByVal iType)

	If (True = IsNull(varValue)) Then
		stringValue = ""
		Exit Function
	End If

	Select Case iType
	Case STRING_TYPE, ENUMERATION_TYPE, FILEPATH_TYPE
		stringValue = varValue
	Case INTEGER_TYPE
		stringValue = replace(g_MSCSDataFunctions.Number (varValue), g_sThousands, "")
	Case FLOAT_TYPE
		stringValue = replace(g_MSCSDataFunctions.Float (cStr(varValue), g_DBDefaultLocale, True, True), g_sThousands, "")
	Case CURRENCY_TYPE
		stringValue = replace(replace(g_MSCSDataFunctions.LocalizeCurrency(varValue, g_DBCurrencyLocale, " "), g_sThousands, ""), " ", "")
	Case DATETIME_TYPE
		stringValue = g_MSCSDataFunctions.DateTime (varValue)
	Case DATE_TYPE
		stringValue = g_MSCSDataFunctions.Date (varValue)
	Case TIME_TYPE
		stringValue = g_MSCSDataFunctions.Time (varValue)
	End Select
End Function

'' -------------------------------------------------------------------
''
''	Function:		ReleaseRecordset
''
''	Description:	
''
''	Arguments:		rs -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Sub ReleaseRecordset (ByRef rs)

	If (True = IsObject (rs)) Then
		If (Not rs Is Nothing) Then
			rs.Close
			Set rs = Nothing
		End If
	End If
End Sub

'' -------------------------------------------------------------------
''
''	Function:		ReleaseRecordset
''
''	Description:	
''
''	Arguments:		rs -
''
''	Returns:		nothing
''
'' -------------------------------------------------------------------
Function sNullToEmptyString (ByVal sText)

	If IsNull(sText) Then
		sNullToEmptyString = ""
	Else
		sNullToEmptyString = sText
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		bFileContainsImage
''
''	Description:	returns TRUE if sFilePath contains .GIF or .JPG
''
''	Arguments:		sFilePath - STRING
''
''	Returns:		BOOL value
''
'' -------------------------------------------------------------------
Function bFileContainsImage (ByRef sFilePath)

	Const GIF = ".GIF"
	Const JPG = ".JPG"

	Dim sPostfix

	sPostfix = UCase (Right (sFilePath, 4))
	If (sPostfix = GIF) Or (sPostfix = JPG) Then
		bFileContainsImage = True
	Else
		bFileContainsImage = False
	End If
End Function

'' -------------------------------------------------------------------
''
''	Function:		sConvertPropertyToTableRow
''
''	Description:	converts the Property Name and Value for a FileName 
''					property into a <TR> element w/ a certain structure
''
''	Arguments:		sProperty - STRING
''					sValue - STRING
''
''	Returns:		STRING value
''
'' -------------------------------------------------------------------
Function sConvertPropertyToTableRow (ByRef sProperty, ByRef sValue)

	Dim sRow

	sRow = "<TR WIDTH='100%'>" & _
		   "<TD>" & sProperty & "</TD>" & _
		   "<TD STYLE='padding-bottom:5px; border-bottom:3px groove gray'><IMG SRC=""" & sValue & """ ALT=""" & sValue & """></TD></TR>"

	sConvertPropertyToTableRow = sRow
End Function


'' -------------------------------------------------------------------
''
Function sColon (ByRef sText)

	sColon = sFormatString (L_Colon_Text, Array(sText))
End Function

'' -------------------------------------------------------------------
''
Function setXMLError (ByRef sSource, ByRef sText)

	setXMLError = "<document recordcount='0'>" & "<ERROR ID='1' SOURCE='" & Server.HTMLEncode(sSource) & "'>" & Server.HTMLEncode(sText) & "</ERROR></document>"
	Err.Clear
End Function

'' -------------------------------------------------------------------
''
Function setXMLError2 (ByRef sSource, ByRef sText)

	setXMLError2 = "<document skip='yes'>" & "<ERROR ID='1' SOURCE='" & Server.HTMLEncode(sSource) & "'>" & Server.HTMLEncode(sText) & "</ERROR></document>"
	Err.Clear
End Function

%>