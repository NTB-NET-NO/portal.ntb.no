<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="catEditorSTRINGS.asp" -->
<!--#INCLUDE FILE="../../include/DialogUtil.asp" -->
<%
	Const PAGE_TYPE = "DIALOG"
%>

<HTML>
<HEAD>
<TITLE><%= L_ImportCSVCatalog_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
	BODY
	{
		PADDING:15px;
		MARGIN:0;
	}
	BUTTON, INPUT
	{
		WIDTH: 6em
	}
</STYLE>

<SCRIPT LANGUAGE='VBScript'>

option explicit


' ----- CONST Declarations
const SEPARATOR1 = "|||"

sub bdcancel_onClick()

	window.returnValue = ""
	window.close
end sub

sub bdok_onClick()

	dim sFilePath
	dim sImportType

	sFilePath = file.value
	if  (catalog.value = "") or (sFilePath = "") or _
		(column_id.value = "") or (price_id.value = "") then

		window.returnValue 	= ""
	else
		if bIsUNCPath (sFilePath) then
			MsgBox L_LocalPath_Message, vbOKOnly, L_BizDeskWarning_Text
			exit sub
		else
			window.returnValue = sFilePath &  SEPARATOR1 & _
								 catalog.value & SEPARATOR1 & _
								 column_id.value &  SEPARATOR1 & _
								 price_id.value & SEPARATOR1 & _
								 cInt(blow_away.checked)
		end if
	end if

	window.close
end sub

function bIsUNCPath (ByRef sFilePath)

	const DOUBLE_SLASH = "//"
	const DOUBLE_BACKSLASH = "\\"

	dim sPrefix

	sPrefix = Left (sFilePath, 2)
	if (StrComp (sPrefix, DOUBLE_SLASH) = 0) or _
	   (StrComp (sPrefix, DOUBLE_BACKSLASH) = 0) then
		bIsUNCPath = True
	else
		bIsUNCPath = False
	end if
end function

</SCRIPT>
</HEAD>

<BODY LANGUAGE='VBScript' ONLOAD='file.focus' ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<xml id='efMeta'>
	<editfield>
	    <text id='file' maxlen='255' subtype='short'>
			<prompt><%= sESCText(L_EnterFilename_Text) %></prompt>
	    </text>
	    <text id='catalog' maxlen='255' subtype='short'>
			<prompt><%= sESCText(L_EnterCatalogName_Text) %></prompt>
	    </text>
	    <text id='column_id' maxlen='255' subtype='short'>
			<prompt><%= sESCText(L_EnterIdentifierName_Text) %></prompt>
	    </text>
	    <text id='price_id' maxlen='255' subtype='short'>
			<prompt><%= sESCText(L_EnterPriceColumnName_Text) %></prompt>
	    </text>
	</editfield>
</xml>
<xml id='efData'>
	<document/>
</xml>

<TABLE>
	<TR>
		<TD STYLE="width:100px">
			<LABEL FOR="file"><%= L_FileToBeImp_Text %></LABEL>
		</TD>
		<TD>
		<div STYLE="width:220px">	
			<DIV ID='file' CLASS='editField'
				MetaXML='efMeta'
				DataXML='efData'></DIV></div>
		</TD>
	</TR>
	<TR>
		<TD STYLE="width:100px">
			<LABEL FOR="catalog"><%= L_CatalogName_Text %></LABEL>
		</TD>
		<TD>
		<div STYLE="width:220px">	
			<DIV ID='catalog' CLASS='editField'
				MetaXML='efMeta'
				DataXML='efData'></DIV></div>
		</TD>
	</TR>
	<TR>
		<TD STYLE="width:100px">
			<LABEL FOR="column_id"><%= L_ColumnID_Text %></LABEL>
		</TD>
		<TD>
		<div STYLE="width:220px">	
			<DIV ID='column_id' CLASS='editField'
				MetaXML='efMeta'
				DataXML='efData'></DIV></div>
		</TD>
	</TR>
	<TR>
		<TD STYLE="width:100px">
			<LABEL FOR="price_id"><%= L_PriceID_Text %></LABEL>
		</TD>
		<TD>
		<div STYLE="width:220px">	
			<DIV ID='price_id' CLASS='editField'
				MetaXML='efMeta'
				DataXML='efData'></DIV></div>
		</TD>
	</TR>
</TABLE>
<TABLE>
	<TR>
 
		<TD><LABEL FOR="blow_away"><%= L_DeleteAnyExistingDataInCatalog_Text %></LABEL>
		</TD>
		<TD><INPUT TYPE="checkbox" ID="blow_away" STYLE='BACKGROUND-COLOR: transparent'></INPUT>
		</TD>
	</TR>
</TABLE>
<BR><BR>
<TABLE WIDTH="100%">
	<TR>
		<TD WIDTH="100%"></TD>
		<TD>
			<BUTTON ID='bdok' CLASS='bdbutton'><%= L_OK_Button %></BUTTON>
		</TD>
		<TD>
			<BUTTON ID='bdcancel' CLASS='bdbutton'><%= L_Cancel_Button %></BUTTON>
		</TD>
		<TD>
			<BUTTON ID='bdhelp' CLASS='bdbutton'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "cs_bd_catalogs_KQGT.htm"'><%= L_Help_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>