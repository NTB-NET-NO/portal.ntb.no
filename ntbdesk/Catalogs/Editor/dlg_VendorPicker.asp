<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="catEditorSTRINGS.asp" -->
<!--#INCLUDE FILE="common.asp" -->

<%
	Const PAGE_TYPE = "DIALOG"
' ###############################
	Perform_ServerSide_Processing
' ###############################
%>

<HTML>
<HEAD>
<TITLE><%= L_SelectVendor_DialogTitle %></TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="../../include/bizdesk.css" ID="mainstylesheet">
<STYLE>
BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
BUTTON, INPUT
{
	WIDTH: 6em
}
</STYLE>

<SCRIPT LANGUAGE='VBScript'>

option explicit

sub bdcancel_onClick()
	window.returnValue = ""
	window.close
end sub

sub bdok_onClick()
	window.close
end sub

sub onSelect()
	dim elSelection
	set elSelection = vendor.selected
	window.returnValue = elSelection.innerText & SEPARATOR1 & _
						 elSelection.qualifier & SEPARATOR1 & _
						 elSelection.value
	bdok.disabled = False
	bdok.focus
end sub

sub onUnselect()
	window.returnValue = ""
	bdok.disabled = true 
end sub

sub window_onLoad()
	window.returnValue = ""
	bdcancel.focus
end sub

</SCRIPT>
</HEAD>

<BODY LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">
<LABEL FOR="vendor"><%= L_SelectVendor_Text %></LABEL><BR><BR>
<div ID="vendor"
	CLASS="listBox" STYLE="WIDTH: 300px; HEIGHT: 100px"
	LANGUAGE="vbscript"
	ONSELECT="onSelect"
	ONUNSELECT="onUnselect">
	<%= g_sVendors %>
</div>
<BR><BR>

<TABLE>
	<TR>
		<TD STYLE='width:230px'></TD>
		<TD><BUTTON ID='bdok' CLASS='bdbutton' <% If (g_nVendors = 0) Then %>DISABLED<% End If %>><%= L_OK_Button %></BUTTON>
			<BUTTON ID='bdcancel' CLASS='bdbutton'><%= L_Cancel_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>

<%
' ##########################################################
'
'	SERVER SIDE - 
'
'			Functionality, CONSTants And Global Declarations
'
' ##########################################################

'----- PUBLIC Declarations
Public g_sMode
Public g_sVendors
Public g_nVendors

' ##########################################################
'
Sub Perform_ServerSide_Processing ()

	g_sMode = Request.QueryString("mode")
	g_sVendors = sVendorsAsOptions()
End Sub

' ##########################################################
'
Function sVendorsAsOptions ()

	Const MODE_SEND = "send"		' called from a SEND Catalog operation
	Const MODE_ASSIGN = "assign"	' called from Catalog Editor when 
									' asssigning a new Vendor
	Dim rsVendors
	Dim oCatalogVendor

	Dim sSourceAliasValue
	Dim sInputDocumentName
	Dim sSourceAliasQualifier

	On Error Resume Next

	sVendorsAsOptions = "<DIV QUALIFIER='' VALUE=''>" & Server.HTMLEncode(L_None_Text) & "</DIV>" & vbCR

	GetCatalogVendorObject oCatalogVendor
	If (oCatalogVendor Is Nothing) Then Exit Function
	If (g_MSCSAppConfig Is Nothing) Then Exit Function

	Select Case g_sMode
	Case MODE_SEND
		sInputDocumentName = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkCatalogDocType")
	Case MODE_ASSIGN
		sInputDocumentName = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkOrderDocType")
	End Select
	sSourceAliasQualifier = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkSourceQualifierID")
	sSourceAliasValue	  = g_MSCSAppConfig.GetOptionsDictionary("").Value("s_BizTalkSourceQualifierValue")

	Set rsVendors = oCatalogVendor.GetVendorList(sSourceAliasQualifier, sSourceAliasValue, sInputDocumentName)
	Set oCatalogVendor = Nothing
	If Not IsObject (rsVendors) Then Exit Function

	Do Until rsVendors.EOF
		sVendorsAsOptions = sVendorsAsOptions & _
							"<DIV QUALIFIER='" & rsVendors("qualifier") & "' VALUE='" & rsVendors("value") & "'>" & rsVendors("orgname") & "</DIV>" & vbCR
		g_nVendors = g_nVendors + 1
		rsVendors.MoveNext
	Loop
	ReleaseRecordset rsVendors
End Function

%>