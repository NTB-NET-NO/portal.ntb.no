<!--#INCLUDE FILE="../../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<%
'' -------------------------------------------------------------------
''  QBEnumeratedValues.asp
''
''  Description:
''		This page returns he EnumeratedValues corresponding to 
''		a CATALOG profile.
'' -------------------------------------------------------------------
	On Error Resume Next

	' ------ CATALOG DataType Enum
	Const INTEGER_TYPE = 0
	Const BIGINTEGER_TYPE = 1
	Const FLOAT_TYPE = 2
	Const DOUBLE_TYPE = 3
	Const BOOLEAN_TYPE = 4
	Const STRING_TYPE = 5
	Const DATETIME_TYPE = 6
	Const CURRENCY_TYPE = 7
	Const FILEPATH_TYPE = 8
	Const ENUMERATION_TYPE = 9

	'' ------ PUBLIC Declarations
	Public g_oCatMgr		' The Catalog Manager Object
	Public g_rsProperties	' List of all Catalog PROPERTIES

	'' ------ Server-Side Processing
	InitializeCatalogObject
	Set g_rsProperties = g_oCatMgr.Properties
%>
<Document>
	<Catalog displayName="MSCSCatalog" name="MSCSCatalog">
		<Profile name="MSCSCatalogProperties" displayName="MSCSCatalogProperties">
			<Group name="MSCSEnumValues" displayName="MSCSEnumValues">
<%= sGetEnumPropertiesAndValues %>
			</Group>
		</Profile>
	</Catalog>
</Document>
<%

'' ------------------------------------------------------------------
'' Sub: InitializeCatalogObject
''
''		Create and initialize a global Catalog Manager Object
''
Sub InitializeCatalogObject ()

	On Error Resume Next
	Set g_oCatMgr = Nothing

	If (True = IsObject(Session("CatalogManager"))) Then
		Set g_oCatMgr = Session("CatalogManager")
	End If

	If (g_oCatMgr Is Nothing) Then
		Dim sCatalogInitString

		sCatalogInitString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")
		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (sCatalogInitString, True)
		Set Session("CatalogManager") = g_oCatMgr
	End If
End Sub

'' ------------------------------------------------------------------
''
Sub ReleaseRecordset (ByRef rs)

	If (True = IsObject (rs)) Then
		If (Not rs Is Nothing) Then
			rs.Close
			Set rs = Nothing
		End If
	End If
End Sub

'' ------------------------------------------------------------------
''
Function sGetEnumPropertiesAndValues ()

	Dim sTemp
	Dim sValue
	Dim sDisplayName
	Dim sPropertyName
	Dim sPropertyType

	Dim iValue
	Dim rsPropertyValues

	Do Until g_rsProperties.EOF
		sPropertyName = g_rsProperties("PropertyName")
		sPropertyType = g_rsProperties("DataType")

		If (sPropertyType = ENUMERATION_TYPE) Then

			sTemp = sTemp & "<Property propType=""STRING"" name=""" & sPropertyName & """ displayName=""" & sPropertyName & """>"

			iValue = 0
			Set rsPropertyValues = g_oCatMgr.GetPropertyValues (sPropertyName)
			Do Until rsPropertyValues.EOF
				sValue = rsPropertyValues("Value")
				sTemp = sTemp & "<Attribute name=""" & sValue & """ displayName=""" & sValue & """ value=""" & iValue & """ />"

				iValue = iValue + 1
				rsPropertyValues.MoveNext
			Loop
			ReleaseRecordset rsPropertyValues

			sTemp = sTemp & "</Property>"
		End If

		g_rsProperties.MoveNext
	Loop

	sGetEnumPropertiesAndValues = sTemp
	ReleaseRecordset g_rsProperties
	Set g_oCatMgr = Nothing
End Function

%>