<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='ExprUtil.asp' -->

<%
On Error Resume Next

' Create the Expression Store object and connect to the database.

Dim objExprStore
Set objExprStore = Server.CreateObject("Commerce.ExpressionStore")
Call objExprStore.Connect(GetBizDataStoreDSN())
If Err.number <> 0 Then
  Call ReportObjError()
End If

' Retrieve the expression from the store.
Dim iExprID
iExprID = Request.QueryString("ExprID")
If iExprID = "" Then
  ReportASPError(L_ExprMissingExpr_ErrorMessage)
End If

Dim rsExpr
Set rsExpr = objExprStore.GetExpression(iExprID)
If Err.number <> 0 Then
  Call ReportObjError()
End If

' Now replace expression values with those passed in.
Dim oXMLDoc
Dim oXMLNode, oXMLValue
Dim listXMLNodes, listXMLValues
Dim objParseError
Dim rsDeps

Set oXMLDoc = Server.CreateObject("Microsoft.XMLDOM")
oXMLDoc.load Request
If oXMLDoc.parseError.errorCode <> 0 Then
  Set objParseError = oXMLDoc.parseError
  Call ReportASPError("0x" & HEX(objParseError.errorCode) & ":  " & _
    objParseError.reason & " (" & objParseError.url & ")")
End If

' NAME (required)
Set oXMLNode = oXMLDoc.selectSingleNode("//NAME")
If oXMLNode Is Nothing Then
  Call ReportASPError(L_ExprMissingName_ErrorMessage)
Else
  rsExpr.Fields("ExprName").Value = oXMLNode.text
  Call oXMLNode.parentNode.removeChild(oXMLNode)
End If

' CATEGORY (optional)
Set oXMLNode = oXMLDoc.selectSingleNode("//CATEGORY")
If oXMLNode Is Nothing Then
  rsExpr.Fields("Category").Value = Null
Else
  rsExpr.Fields("Category").Value = oXMLNode.text
  Call oXMLNode.parentNode.removeChild(oXMLNode)
End If

' DESCRIPTION (optional)
Set oXMLNode = oXMLDoc.selectSingleNode("//DESC")
If oXMLNode Is Nothing Then
  rsExpr.Fields("ExprDesc").Value = Null
Else
  rsExpr.Fields("ExprDesc").Value = oXMLNode.text
  Call oXMLNode.parentNode.removeChild(oXMLNode)
End If

' EXPRESSION DEPENDENCIES (optional)
Set rsDeps = rsExpr.Fields("rsExprDeps").Value
While Not rsDeps.EOF
  rsDeps.Delete()
  rsDeps.MoveNext
  If Err.number <> 0 Then
    Call ReportObjError()  
  End If
WEnd

Set listXMLNodes = oXMLDoc.selectNodes("//EXPR-REF")
For Each oXMLNode In listXMLNodes
  rsDeps.AddNew
  rsDeps.Fields("ExprDep").Value = oXMLNode.getAttribute("ID")
  If Err.number <> 0 Then
    Call ReportObjError()
  End If
Next


' PROFILE DEPENDENCIES (required)
Set rsDeps = rsExpr.Fields("rsProfDeps").Value
While Not rsDeps.EOF
  rsDeps.Delete()
  rsDeps.MoveNext
  If Err.number <> 0 Then
    Call ReportObjError()
  End If
WEnd

Set listXMLNodes = oXMLDoc.selectNodes("//PROFDEP")
If listXMLNodes.length > 0 Then
  For Each oXMLNode In listXMLNodes
      rsDeps.AddNew
      rsDeps.Fields("ProfDep").Value = oXMLNode.text
      If Err.number <> 0 Then
        Call ReportObjError()
      End If
      Call oXMLNode.parentNode.removeChild(oXMLNode)
    Next
Else
  Call ReportASPError(L_ExprMissingProf_ErrorMessage)
End If

' EXPRESSION BODY (required)
' First localize any necessary immediate values
Dim sValue, sThousands
const LOCALE_STHOUSAND = 15
const US_ENGLISH = 1033
Dim MSCSDefaultLocale, MSCSCurrencyLocale
MSCSDefaultLocale		= Application("MSCSDefaultLocale")
MSCSCurrencyLocale	= Application("MSCSCurrencyLocale")
sThousands = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_STHOUSAND, MSCSDefaultLocale)


'DATE
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='date']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to US_English
      sValue = g_MSCSDataFunctions.ConvertDateString(oXMLValue.text, MSCSDefaultLocale)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidDate_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = g_MSCSDataFunctions.Date(sValue, US_ENGLISH)
      End If
    Next
  Else
    'Single value - convert value to US_English
    sValue = g_MSCSDataFunctions.ConvertDateString(oXMLNode.text, MSCSDefaultLocale)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidDate_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = g_MSCSDataFunctions.Date(sValue, US_ENGLISH)
    End If
  End If
Next

'TIME
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='time']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to US_English
      sValue = g_MSCSDataFunctions.ConvertTimeString(oXMLValue.text, MSCSDefaultLocale)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidTime_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = g_MSCSDataFunctions.Time(sValue, US_ENGLISH)
      End If
    Next
  Else
    'Single value - convert value to US_English
    sValue = g_MSCSDataFunctions.ConvertTimeString(oXMLNode.text, MSCSDefaultLocale)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidTime_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = g_MSCSDataFunctions.Time(sValue, US_ENGLISH)
    End If
  End If
Next

'DATETIME
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='datetime']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to US_English
      sValue = g_MSCSDataFunctions.ConvertDateTimeString(oXMLValue.text, MSCSDefaultLocale)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidDateTime_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = g_MSCSDataFunctions.DateTime(sValue, US_ENGLISH)
      End If
    Next
  Else
    'Single value - convert value to US_English
    sValue = g_MSCSDataFunctions.ConvertDateTimeString(oXMLNode.text, MSCSDefaultLocale)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidDateTime_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = g_MSCSDataFunctions.DateTime(sValue, US_ENGLISH)
    End If
  End If
Next

'NUMBER
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='number']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to US_English
      sValue = g_MSCSDataFunctions.ConvertNumberString(oXMLValue.text, MSCSDefaultLocale)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidNumber_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), US_ENGLISH, true, true), sThousands, ""))
      End If
    Next
  Else
    'Single value - convert value to US_English
    sValue = g_MSCSDataFunctions.ConvertNumberString(oXMLNode.text, MSCSDefaultLocale)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidNumber_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), US_ENGLISH, true, true), sThousands, ""))
    End If
  End If
Next

'FLOAT
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='float']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to US_English
      sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLValue.text, MSCSDefaultLocale)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidFloat_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), US_ENGLISH, true, true), sThousands, ""))
      End If
    Next
  Else
    'Single value - convert value to US_English
    sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLNode.text, MSCSDefaultLocale)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidFloat_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), US_ENGLISH, true, true), sThousands, ""))
    End If
  End If
Next

'CURRENCY
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='currency']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to US_English
      sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLValue.text, MSCSCurrencyLocale)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidCurrency_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), US_ENGLISH, true, true), sThousands, ""))
      End If
    Next
  Else
    'Single value - convert value to US_English
    sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLNode.text, MSCSCurrencyLocale)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidCurrency_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), US_ENGLISH, true, true), sThousands, ""))
    End If
  End If
Next

' NOTE: After removing all the other nodes, what is left
'  in the XML document should be the expression body
rsExpr.Fields("ExprBody").Value = oXMLDoc.xml

' Now, save the expression.
objExprStore.SaveExpression(rsExpr)
If Err.number <> 0 Then
  Call ReportObjError()
End If

%>
<EXPRINFO ID="<%=rsExpr.Fields("ExprID").Value%>">
  <NAME><%=rsExpr.Fields("ExprName").Value%></NAME>
  <DESC><%=rsExpr.Fields("ExprDesc").Value%></DESC>
</EXPRINFO>
