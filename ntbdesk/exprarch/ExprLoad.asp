<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='ExprUtil.asp' -->

<%
ON ERROR RESUME NEXT

' Create the Expression Store object and connect to the database.

DIM objExprStore
SET objExprStore = Server.CreateObject("Commerce.ExpressionStore")
CALL objExprStore.Connect(GetBizDataStoreDSN())

' Make sure the user specified an expression ID.

DIM intExprID
intExprID = Request.Form("ExprID")
IF intExprID = 0 THEN
    CALL ReportASPError(L_ExprMissingExpr_ErrorMessage)
END IF

' Get the specified expression.

DIM rsExpr
SET rsExpr = objExprStore.GetExpression(intExprID)
IF Err.number <> 0 THEN
  CALL ReportObjError()
END IF

IF rsExpr.BOF AND rsExpr.EOF THEN
    CALL ReportASPError(L_ExprInvalidExpr_ErrorMessage)
END IF

%>
<EXPRESSION>
  <ID><%=intExprID%></ID>
  <NAME><%=rsExpr.Fields("ExprName")%></NAME>
<%
IF rsExpr.Fields("Category") <> "" THEN
%>
  <CATEGORY><%=rsExpr.Fields("Category")%></CATEGORY>
<%
END IF

IF rsExpr.Fields("ExprDesc") <> "" THEN
%>
  <DESC><%=rsExpr.Fields("ExprDesc")%></DESC>
<%
END IF

' EXPRESSION BODY
Dim oXMLDoc
Dim oXMLNode, oXMLValue
Dim listXMLNodes, listXMLValues
Dim objParseError

Set oXMLDoc = Server.CreateObject("Microsoft.XMLDOM")
oXMLDoc.loadXML rsExpr.Fields("ExprBody")
If oXMLDoc.parseError.errorCode <> 0 Then
  Set objParseError = oXMLDoc.parseError
  Call ReportASPError("0x" & HEX(objParseError.errorCode) & ":  " & _
    objParseError.reason & " (" & objParseError.url & ")")
End If

' First localize any necessary immediate values
Dim sValue, sThousands
const LOCALE_STHOUSAND = 15
const US_ENGLISH = 1033
Dim MSCSDefaultLocale, MSCSCurrencyLocale
MSCSDefaultLocale		= Application("MSCSDefaultLocale")
MSCSCurrencyLocale	= Application("MSCSCurrencyLocale")
sThousands = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_STHOUSAND, MSCSDefaultLocale)

'DATE
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='date']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to current locale
      sValue = g_MSCSDataFunctions.ConvertDateString(oXMLValue.text, US_ENGLISH)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidDate_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = g_MSCSDataFunctions.Date(sValue, MSCSDefaultLocale)
      End If
    Next
  Else
    'Single value - convert value to current locale
    sValue = g_MSCSDataFunctions.ConvertDateString(oXMLNode.text, US_ENGLISH)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidDate_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = g_MSCSDataFunctions.Date(sValue, MSCSDefaultLocale)
    End If
  End If
Next

'TIME
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='time']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to current locale
      sValue = g_MSCSDataFunctions.ConvertTimeString(oXMLValue.text, US_ENGLISH)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidTime_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = g_MSCSDataFunctions.Time(sValue, MSCSDefaultLocale)
      End If
    Next
  Else
    'Single value - convert value to current locale
    sValue = g_MSCSDataFunctions.ConvertTimeString(oXMLNode.text, US_ENGLISH)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidTime_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = g_MSCSDataFunctions.Time(sValue, MSCSDefaultLocale)
    End If
  End If
Next

'DATETIME
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='datetime']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to current locale
      sValue = g_MSCSDataFunctions.ConvertDateTimeString(oXMLValue.text, US_ENGLISH)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidDateTime_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = g_MSCSDataFunctions.DateTime(sValue, MSCSDefaultLocale)
      End If
    Next
  Else
    'Single value - convert value to current locale
    sValue = g_MSCSDataFunctions.ConvertDateTimeString(oXMLNode.text, US_ENGLISH)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidDateTime_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = g_MSCSDataFunctions.DateTime(sValue, MSCSDefaultLocale)
    End If
  End If
Next

'NUMBER
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='number']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to current locale
      sValue = g_MSCSDataFunctions.ConvertNumberString(oXMLValue.text, US_ENGLISH)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidNumber_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), MSCSDefaultLocale, true, true), sThousands, ""))
      End If
    Next
  Else
    'Single value - convert value to current locale
    sValue = g_MSCSDataFunctions.ConvertNumberString(oXMLNode.text, US_ENGLISH)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidNumber_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), MSCSDefaultLocale, true, true), sThousands, ""))
    End If
  End If
Next

'FLOAT
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='float']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to current locale
      sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLValue.text, US_ENGLISH)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidFloat_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), MSCSDefaultLocale, true, true), sThousands, ""))
      End If
    Next
  Else
    'Single value - convert value to current locale
    sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLNode.text, US_ENGLISH)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidFloat_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = trim(replace(g_MSCSDataFunctions.Float(cStr(sValue), MSCSDefaultLocale, true, true), sThousands, ""))
    End If
  End If
Next

'CURRENCY
Set listXMLNodes = oXMLDoc.selectNodes("//IMMED-VAL[@TYPE='currency']")
For Each oXMLNode In listXMLNodes
  If oXMLNode.getAttribute("MULTIVAL") = "True" Then
    'Multiple values
    Set listXMLValues = oXMLNode.selectNodes("VALUE")
    For Each oXMLValue In listXMLValues
      'Convert values to current locale
      sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLValue.text, US_ENGLISH)
      If IsNull(sValue) Then
        Call ReportASPError(L_ExprInvalidCurrency_ErrorMessage & "'" & oXMLValue.text & "'")
      Else
        oXMLValue.text = trim(replace(g_MSCSDataFunctions.float(sValue, MSCSCurrencyLocale, true, true), sThousands, ""))
      End If
    Next
  Else
    'Single value - convert value to current locale
    sValue = g_MSCSDataFunctions.ConvertFloatString(oXMLNode.text, US_ENGLISH)
    If IsNull(sValue) Then
      Call ReportASPError(L_ExprInvalidCurrency_ErrorMessage & "'" & oXMLNode.text & "'")
    Else
      oXMLNode.text = trim(replace(g_MSCSDataFunctions.float(sValue, MSCSCurrencyLocale, true, true), sThousands, ""))
    End If
  End If
Next

%>
  <EXPRBODY><%=oXMLDoc.xml%></EXPRBODY>
<%

' Profile-dependency list:

%>
  <PROFILEDEPS>
<%
DIM rsProfDeps
SET rsProfDeps = rsExpr.Fields("rsProfDeps").Value
WHILE NOT rsProfDeps.EOF
%>
    <PROFILE><%=rsProfDeps.Fields("ProfDep")%></PROFILE>
<%
    CALL rsProfDeps.MoveNext()
WEND
%>
  </PROFILEDEPS>
<%

' Expression dependency list:

%>
  <EXPRDEPS>
<%
DIM rsExprDeps
DIM rsExprDepInfo
SET rsExprDeps = rsExpr.Fields("rsExprDeps").Value
WHILE NOT rsExprDeps.EOF

  SET rsExprDepInfo = objExprStore.GetExpression(rsExprDeps.Fields("ExprDep"))
  IF Err.number <> 0 THEN
    CALL ReportObjError()
  END IF

  IF NOT (rsExprDepInfo.BOF AND rsExprDepInfo.EOF) THEN
%>
    <EXPRINFO ID="<%=rsExprDepInfo.Fields("ExprID")%>">
      <NAME><%=rsExprDepInfo.Fields("ExprName")%></NAME>
      <DESC><%=rsExprDepInfo.Fields("ExprDesc")%>"</DESC>
    </EXPRINFO>
<%
  END IF

  CALL rsExprDeps.MoveNext()
WEND
%>
  </EXPRDEPS>
</EXPRESSION>
