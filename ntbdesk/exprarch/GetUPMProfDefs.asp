<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<%
'============================================================================
'   GetProfDefs.asp
'
'   Description:
'       This ASP script takes a number of POSTed arguments and returns the
'       XML form of those profiles' definitions.
'    
'   Author:  donniep
'============================================================================
%>

<!--#INCLUDE FILE='ExprUtil.asp' -->

<%
' Create a BDAO.

DIM objBDAO
SET objBDAO = Server.CreateObject("Commerce.BusinessDataAdmin")
CALL objBDAO.Connect(GetBizDataStoreDSN())
IF Err.Number <> 0 THEN
  CALL ReportASPError("0x" & HEX(Err.Number) & ":  " & Err.Description)
END IF

' Prepare the XML document to return.
DIM strProfName
DIM xmlProfDefs

IF Request.QueryString("Profile").Count = 0 THEN
  ReportASPError L_ExprMissingProf_ErrorMessage
ELSE
  strProfName = Request.QueryString("Profile")
  SET xmlProfDefs = objBDAO.GetProfile(CStr(strProfName))
  Response.Write xmlProfDefs.xml
END IF
%>
