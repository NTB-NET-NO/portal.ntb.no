<%

Const L_ExprMissingName_ErrorMessage = "You must enter a name."
Const L_ExprMissingBody_ErrorMessage = "You must enter a body for the expression."
Const L_ExprMissingProf_ErrorMessage = "Missing required Profile/ProfDep argument(s)."
Const L_ExprMissingExpr_ErrorMessage = "You must enter an ExprID."
Const L_ExprInvalidExpr_ErrorMessage = "Could not find the specified expression."
Const L_ExprInvalidDate_ErrorMessage = "The format of the Date value is not valid: "
Const L_ExprInvalidDateTime_ErrorMessage = "The format of the Date/Time value is not valid: "
Const L_ExprInvalidTime_ErrorMessage = "The format of the time value is not valid: "
Const L_ExprInvalidCurrency_ErrorMessage = "The format of the currency value is not valid: "
Const L_ExprInvalidNumber_ErrorMessage = "The format of the number value is not valid: "
Const L_ExprInvalidFloat_ErrorMessage = "The format of the decimal value is not valid: "

' This function retrieves the DSN for the BizDataStore from the site-config
' objects.
FUNCTION GetBizDataStoreDSN()
  GetBizDataStoreDSN = g_MSCSConfiguration.Fields("Biz Data Service").Value.Fields("s_BizDataStoreConnectionString").Value
END FUNCTION

SUB ReportObjError()
  Response.Write "<ASP-ERROR>0x" & HEX(Err.number) & ":  " & _
    Err.description & "</ASP-ERROR>"
  Response.End
END SUB

SUB ReportASPError(strMsg)
  Response.Write "<ASP-ERROR>" & strMsg & "</ASP-ERROR>"
  Response.End
END SUB

%>