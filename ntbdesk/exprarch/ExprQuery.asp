<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='ExprUtil.asp' -->

<%
On Error Resume Next

' Create the Expression Store object and connect to the database.

Dim objExprStore
Set objExprStore = Server.CreateObject("Commerce.ExpressionStore")
Call objExprStore.Connect(GetBizDataStoreDSN())
If Err.number <> 0 Then
  Call ReportObjError()
End If

' Get the query parameters.
Dim oXMLDoc
Dim oXMLNode, listXMLNodes
Dim objParseError
Dim rsDeps

Set oXMLDoc = Server.CreateObject("Microsoft.XMLDOM")
oXMLDoc.load Request
If oXMLDoc.parseError.errorCode <> 0 Then
  Set objParseError = oXMLDoc.parseError
  Call ReportASPError("0x" & HEX(objParseError.errorCode) & ":  " & _
    objParseError.reason & " (" & objParseError.url & ")")
End If

' NAME
Dim strName
Set oXMLNode = oXMLDoc.selectSingleNode("//NAME")
If Not oXMLNode Is Nothing Then
  strName = oXMLNode.text
End If

' CATEGORY
Dim rgstrCategory()
Set listXMLNodes = oXMLDoc.selectNodes("//CATEGORY")
If listXMLNodes.length > 0 Then
  Redim rgstrCategory(listXMLNodes.length - 1)
  Dim i
  i = 0
  For Each oXMLNode In listXMLNodes
    rgstrCategory(i) = oXMLNode.text
    i = i + 1
  Next
End If

' PROFILE
Dim rgstrProfiles()
Set listXMLNodes = oXMLDoc.selectNodes("//PROFILE")
If listXMLNodes.length > 0 Then
  Redim rgstrProfiles(listXMLNodes.length - 1)
  i = 0
  For Each oXMLNode In listXMLNodes
    rgstrProfiles(i) = oXMLNode.text
    i = i + 1
  Next
End If

' Perform the query.

Dim rsExprInfo
Set rsExprInfo = objExprStore.Query(strName, rgstrCategory, rgstrProfiles)
If Err.number <> 0 Then
  Call ReportObjError()
End If

%>
<EXPRINFOLIST>
<%

While (Not rsExprInfo.EOF)
%>
  <EXPRINFO ID="<%=rsExprInfo.Fields("ExprID")%>">
    <NAME><%=rsExprInfo.Fields("ExprName")%></NAME>
    <DESC><%=rsExprInfo.Fields("ExprDesc")%></DESC>
  </EXPRINFO>
<%
  Call rsExprInfo.MoveNext()
WEnd
%>
</EXPRINFOLIST>
