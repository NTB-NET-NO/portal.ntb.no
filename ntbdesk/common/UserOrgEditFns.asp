<%
'Functions and routines used in User and Org Edit pages
Dim g_nGroupCount
Sub FormQuery(sSQL, aprops, nUserID, nIDCol,dState)
	Dim i
  
	sSQL = "SELECT " & sBracket(sGetPropFullName(aprops(1))) '& " AS " & aprops(1)(2)
	For i = 2 to Ubound(aprops)
    'If (sIsPropMultiValued(aprops(i)) = False) And bIsPropMappedToData(aprops(i)) Then 
    If bIsPropMappedToData(aprops(i)) Then 
		  sSQL = sSQL & ", " & sBracket(sGetPropFullName(aprops(i)))  '& " AS " & aprops(i)(2)
		End If
	Next 

	sSQL = sSQL & " FROM " & sBracket(sGetPropProfile(aprops(1))) & " WHERE " ' 
	sSQL = sSQL & sBracket(sGetPropFullName(aprops(nIDCol))) & " = " & _
	    sQuoteArg(nUserID, sGetPropType(aprops(nIdCol)))
	    ' hack 07/13/99 - add ProfileSystem.ParentDN to every query
	If sGetPropFullName(aprops(nIDCol)) <> g_sParentURL And g_bIsADInstallation Then 
		sSQL = sSQL & " And " & sBracket(g_sParentURL) & " = '" & _
					sGetParentDN(dState) & "'"
	End If	
				
End sub

Sub FormCopyQuery(sSQL, aprops, nUserID, nIDCol,dState)
    
	Dim i, sType
	sSQL = "SELECT " & sBracket(sGetPropFullName(aprops(1))) '& " AS " & aprops(1)(2)
	
	For i = 2 to Ubound(aprops)
		sType = sGetPropType(aprops(i))
    'If ( (sIsPropMultiValued(aprops(i)) = False) And _
			if	( bIsPropUniqueKey(aprops(i)) = False And _
				bIsPropReadOnly(aprops(i)) = False And _
				bIsPropJoinKey(aprops(i)) = False And _ 
				bIsPropUpdatable(aprops(i)) = TRUE And _
				bIsPropMappedToData(aprops(i)) = TRUE _
				(LCase(sType) <> "password") ) Then 
		  sSQL = sSQL & ", " & sBracket(sGetPropFullName(aprops(i)))  '& " AS " & aprops(i)(2)
		End If
	Next 
	
   	sSQL = sSQL & " FROM " & sBracket(sGetPropProfile(aprops(1))) & " WHERE " ' 
	sSQL = sSQL & sBracket(sGetPropFullName(aprops(nIDCol))) & " = " & _
	    sQuoteArg(nUserID, sGetPropType(aprops(nIdCol)))
	    ' hack 07/13/99 - add ProfileSystem.ParentDN to every query
	If sGetPropFullName(aprops(nIDCol)) <> g_sParentURL And g_bIsADInstallation Then 
		sSQL = sSQL & " And " & sBracket(g_sParentURL) & " = '" & _
					sGetParentDN(dState) & "'"
	End If				
End sub

Sub RS2CopyRS(ByRef rsQuery, ByVal aprops)
  Dim fdField, bMakeBlank, bIsNewRecord, sValue, i, avalues
  Dim sType
  
  For i = 1 to Ubound(aprops)
		avalues = aprops(i)
		sType = sGetPropType(avalues)
		If bIsPropUniqueKey(avalues) OR _
			bIsPropReadOnly(avalues) OR _
			bIsPropJoinKey(avalues) OR _ 
			(bIsPropUpdatable(avalues) = FALSE) OR _
		  (LCase(sType) <> "password") Then 
				bMakeBlank = True
		Else 
			bMakeBlank = False
		End If	
		'Weed out multivalued properties For now
		'If sIsPropMultiValued(avalues) = False And _
		 
		if	bIsPropMappedToData(avalues) Then 
			If Not rsQuery.EOF Then 
			 If bMakeBlank Then  _
				rsQuery.fields.item(nGetPropMVOffset(aprops(i))-1).value = ""
			End If
		End If
  Next 
  rsQuery.Update
End Sub

Function sMVPropValue(sVal, sType)
    Dim arrVal
    sVal = Replace(sVal,MV_DELIM_ESCAPE, chr(10)+ chr(13))
    arrVal = Split(sVal, MV_DELIM)
    For i= 0 to Ubound(arrVal)
	arrVal(i) = sQuoteArg(Replace(arrVal(i),chr(10)+chr(13),MV_DELIM), sType)
    Next 
    sMVPropValue = "(" & Join(arrVal,",") & ")"
End Function

Function ExecQuery(oRS, cn, sSQL, sErrMsg)
	ExecQuery = bExecWithErrDisplay(ors, cn, sSQL, sErrMsg)
End Function

Sub MakeUpdateQuery(aprops, sSQL, aIDCol, dState)
	Dim fitem, i, bneedcomma, sName, iType, sValue, sFullName, stmp
	Dim bMultiValued
	Dim sLongName
	Dim oRS
	
	On Error Resume Next
	Dim bUpdate , sParams , UserAccountControlValue, iAccountStatus, iPartnerDeskRoleFlags
	Dim arrType
	Set oRS = oMakeUpdateRS()
    
	If Not IsEmpty(Session("PrevValue")) Then  Set g_sPrevValue = Session("PrevValue")
	bneedcomma = False
	sSQL = "UPDATE " & sBracket(sGetPropProfile(aprops(1))) & " Set "
	
	For i = 1 to Ubound(aprops)
		sname = sGetPropName(aprops(i))
		sLongName = sGetPropFullName(aprops(i))
		sFullName = sBracket(sLongName)
		'added a check For ParentDN as it is an inactive field but required in the query
		'AD does Not allow updates to reSet the PK Column, so don't put that in a Set clause
		'Also If the field is hidden, it could Not be updated. Some of these hidden fields (instanceClass)
		'in AD cause the query to fail If you reSet them, so exclude them too.
		If (i <> nGetIDColNo(aIDCol)) And  ((True = bIsPropActive(aprops(i))) OR (strcomp(sFullName, sBracket(g_sParentURL), 0) = 0)) Then 
		'Changed to allow page to Set container appropriately in AD installations
			If g_bIsADInstallation And (strcomp(sFullName, sBracket(g_sParentURL), 0) = 0) Then 
				  sValue = sGetParentURL(dState)
			Else 
				  sValue = trim(Request.Form(sName))
			 	    
				If LCase(sName) = "type" Then 
					arrType = Split(CStr(Request.Form("type")),",")
					sValue = trim(arrType(1))
				End If	

			End If
			 
			itype = sGetPropType(aprops(i))
			
			If sName = "date_last_changed" Then sValue = g_MSCSDataFunctions.datetime( Now(), g_MSCSDefaultLocale )
            If sName = "user_id_changed_by" Then sValue = Request.ServerVariables("LOGON_USER") 
			
			If bIsDateType(itype) Then 
				sValue = sFormatAsDateTime(sValue)
			ElseIf bIsFloatType(itype) Then 
				sValue = sFormatAsFloat(sValue)
			ElseIf bIsIntType(itype) Then 
				sValue = sFormatAsInt(sValue)
			ElseIf bIsCurrencyType(itype) Then 
				sValue = sFormatAsCurrency(sValue)
			End If
			
				
		'	If strcomp(itype,"Password",1) = 0 And g_sType = "save" And Instr(1,sValue,"*") > 0 Then
			If strcomp(itype,"Password",1) = 0 And g_sType = "save" And sValue = passwdguid Then  
				sValue = ""
				sValue = g_sPrevValue(sName)
			Else 
				'Check For password length And issue alert If less than MIN_PASSWORD_LEN 	
			End If
			'If strcomp(itype,"Password",1) = 0 And len(sValue) = 0 Then  sValue = g_sPrevValue(sName)
			bMultiValued = sIsPropMultiValued(aprops(i)) 
			If bMultiValued And Not IsNull(g_sPrevValue(sName)) Then 
				g_sPrevValue(sName) = Join(g_sPrevValue(sName) , MV_DELIM)
			End If
		
			If g_bIsADInstallation And (strcomp(sFullName, sBracket(g_sParentURL), 0) <> 0) Then 
				If (strcomp(sFullName, sBracket(g_sAccountOrgId), 0) = 0) Then  sValue = g_sPrevValue(sName)
				If (strcomp(sFullName, sBracket(g_sAccountStatus), 0) = 0) Then iAccountStatus = sValue
				If (strcomp(sFullName, sBracket(g_sPartnerDeskRoleFlags), 0) = 0) Then  g_iPartnerDeskRoleFlags = sValue
			End If
		
			If ((g_sPrevValue(sName) <> sValue)) Then 
			  	oRS.Fields(sLongName).Value = sValue    
				If Len(sValue)=0 Then  
					sValue = "NULL"
				End If	
				'AD Fails when trying update "" !!
				If g_bIsADInstallation And sValue = "" Then  sValue= " "
			
				If bMultiValued Then 
					sValue = sMVPropValue(sValue, itype)
				Else 
					sValue = sQuoteArg(sValue, itype)
				End If	
				
			      
				If bneedcomma Then 
			  		sParams = sParams & ", " & sFullName & " = " & sValue 
				Else 
			  		sParams = sParams & sFullName & " = " & sValue
			  		bneedcomma = true
				End If
		   'ElseIf (IsNull((g_sPrevValue(sName))) And Len(sValue)>0 And (LCase(sValue) <> "null" )) Then 
		    ElseIf (IsNull(g_sPrevValue(sName)) And Len(sValue)>0) Then 
		  		oRS.Fields(sLongName).Value = sValue
		  		If bMultiValued Then 
					sValue = sMVPropValue(sValue, itype)
				Else 
					sValue = sQuoteArg(sValue, itype)
				End If	
				
				If bneedcomma Then 
			  		sParams = sParams & ", " & sFullName & " = " & sValue 
				Else 
			  		sParams = sParams & sFullName & " = " & sValue
			  		bneedcomma = true
				End If
			End If  
		End If
	Next
	 
	If g_bIsADInstallation And sGetPropProfile(aprops(1))= USER_PROFILE Then 
		If iAccountStatus = "1" Then 
			If g_iPartnerDeskRoleFlags = g_PartnerDeskAdmin Then  
				UserAccountcontrolValue = USERACCOUNT_ADMIN
			Else 
				UserAccountcontrolValue = USERACCOUNT_NORMAL
			End If	
		Else 
			UserAccountcontrolValue = USERACCOUNT_INACTIVE
		End If	
		If bneedcomma Then 
			sParams = sParams & ", " & sBracket(g_sUserAccountControl) & " = " & UserAccountcontrolValue
			oRS.Fields(g_sUserAccountControl).Value = UserAccountcontrolValue
		Else 
			sParams = sParams & sBracket(g_sUserAccountControl) & " = " &  UserAccountcontrolValue
			oRS.Fields(g_sUserAccountControl).Value = UserAccountcontrolValue
			bneedcomma = true
		End If
	End If
	If Len(Trim(sParams)) = 0 Then  
		sSQL = ""
	Else 
		sSQL= sSQL & sParams
	End If
		
	If Not oRS is Nothing Then
		If Not IsEmpty(oRS) Then Set Session("UserUpdateRS") = oRS
	End If	
	On Error GoTo 0
End sub

Sub MakeInsertQuery(aprops, sSQL, aIDCol, sKey, bIDInt, dState)
        
	Dim fitem, i, bneedcomma, params, values, sName, itype, sValue, sFullName, nID, sID
	Dim UserAccountControlValue , iAccountStatus
	Dim sPrimaryKeyValue
	Dim bMultiValued
	Dim oRS
	Dim sLongName
	Dim arrType
	
	On Error Resume Next
	bneedcomma = False
	
	Set oRS = oMakeUpdateRS()
	sSQL = "INSERT INTO " & sBracket(sGetPropProfile(aprops(1)))
	params = " (" 
	values = " VALUES("
	nID = nGetIDColNo(aIDCol)
	sID = sGetIDName(aIDCol)
    'First Determine If the Primary key column is hidden. If so, we have to enter a value (likely
    'a GUID or a SQL Identity Column. aprops(ID #)(13) is True If the Column isn't hidden (user had to
    'enter value. 
    
    If bIsPropActive(aprops(nID)) Then 
        'Not hidden, take value user gave us. Wrap quotes around user's entry on form If appropriate
	    params = params & sBracket(sGetPropFullName(aprops(nID)))
	    values = values & sQuoteArg(Request.Form(sID), sGetPropType(aprops(nID)))
	    
		If LCase(sName) = "type" Then 
			arrType = Split(CStr(Request.Form("type")),",")
			sValue = trim(arrType(1))
		End If	
		
	    if NOT oRS is Nothing then oRS.Fields(sGetPropFullName(aprops(nID))).Value = Request.Form(sID)
        bneedcomma = true
	Else 
    'Hidden. If it's Not an integer, we'll assume it's a GUID And generate one. 
    'Otherwise, let the datastore handle it's generation And leave it out of query all together.
    	If Not (bIDInt) Then 
    	    params = params & sBracket(sGetPropFullName(aprops(nID)))
	        sKey = sGetGUID()
	        values = values & "'" & sKey & "'"
	        bneedcomma = true
	        if NOT oRS is Nothing then oRS.Fields(sGetPropFullName(aprops(nID))).Value = sKey
	        'sKey is the GUID for the new user being created. Cache it for Address Profile Use (Copy User / New User)
	        'If its "open" for an existing User we need to cache it from somewhere else.
	        'Session("UserGuid") = sKey
	     End If
	End If
	
	For i = 1 to Ubound(aprops)
		If bIsPropPrimaryKey(aprops(i)) Then 
			sName = sGetPropName(aprops(i))
			If LCase(sName) = "logon_name" Then
			     If Instr(1,Request.Form(sName),".user_security_password=") > 0 Then
			     'Specific handling For Bug 38731 sGetGUID()
			     'If xml corruption then mask logon_name with Guid.
					sPrimaryKeyValue = sGetGUID()
					sPrimaryKeyValue = sGetSamAccountNameFormat(sPrimaryKeyValue)
				 Else
					sPrimaryKeyValue = trim(Request.Form(sName))
					sPrimaryKeyValue = sGetSamAccountNameFormat(sPrimaryKeyValue)
				 End IF	
			End If	 
			Exit for
	    End If
	Next 
    
	
	
	For i = 1 to Ubound(aprops)
	'this loop is only For non-Primary Key properties. The Primary Key was treated above.
	'the nt_security_desc should be avoided from consideration
   
   		If (i <> nID) Then 
	        sName = sGetPropName(aprops(i))
	        sLongName = sGetPropFullname(aprops(i))
	        sFullName = sBracket(sLongName)
    
        'Added Omar . Have to remove binary values from the query For now as provider 
        'does Not support it.
         
         If("nt_security_descriptor" <> sName ) And ("object_category" <> sName) And ("object_class" <> sName) And ("user_account_control" <> sName) Then 
            If g_bIsADInstallation And (strcomp(sFullName, sBracket(g_sParentURL), 0) = 0) Then 
				sValue = sGetParentURL(dState)
			Else 
				sValue = trim(Request.Form(sName))
			End If
                           
			If g_bIsADInstallation And (strcomp(sFullName, sBracket(g_sAccountStatus), 0) = 0) Then  _
				iAccountStatus = sValue 
			If g_bIsADInstallation And (strcomp(sFullName, sBracket(g_sPartnerDeskRoleFlags), 0) = 0) Then  _
				g_iPartnerDeskRoleFlags = sValue
			If g_bIsADInstallation And (strcomp(sFullName, sBracket(g_sSAMAccountName), 0) = 0) Then  _
				sValue = sPrimaryKeyValue 

'We need to generate GUIDs in the UserID For new Users And the OrgID For new Orgs, but
'These are Not the primary key in the XML. So If we come across these properties 
'(And If we are here, they are Not the Primary Key) put a guid in them.
'
               
        If len(sValue) = 0 And _
            ( (sGetPropName(aprops(i)) = XML_ATTR_USERID And sGetPropProfile(aprops(i))= USER_PROFILE And bisPropActive(aprops(i))=False) OR _
            (sGetPropName(aprops(i)) = XML_ATTR_ORGID And sGetPropProfile(aprops(i))= ORG_PROFILE And bIsPropActive(aprops(i))=False) ) Then 
            sValue = sGetGUID()
        End If
	 
	    If sName = "date_last_changed" Then sValue = g_MSCSDataFunctions.datetime( Now(), g_MSCSDefaultLocale )
        If sName = "user_id_changed_by" Then sValue = Request.ServerVariables("LOGON_USER") 
        
	    iType = sGetPropType(aprops(i))
    
		If bIsDateType(itype) Then 
			sValue = sFormatAsDateTime(sValue)
		ElseIf bIsFloatType(itype) Then 
			sValue = sFormatAsFloat(sValue)
		ElseIf bIsIntType(itype) Then 
			sValue = sFormatAsInt(sValue)
		ElseIf bIsCurrencyType(itype) Then 
			sValue = sFormatAsCurrency(sValue)
		End If
			        
		If LCase(sName) = "type" Then 
			arrType = Split(CStr(Request.Form("type")),",")
			sValue = trim(arrType(1))
		End If	
        
		'SB added to avoid passing null For all the select type fields
	    If len(sValue) > 0 And LCase(sValue)<> "null" Then 
			 If sIsPropMultivalued(aprops(i)) Then 
				sValue = sMVPropValue(sValue, itype)
				If NOT oRS is Nothing then oRS.Fields(sLongName).Value = sValue			
			 Else 
				If NOT oRS is Nothing then oRS.Fields(sLongName).Value = sValue
				sValue =sQuoteArg(sValue, itype)
			 End If
	          
	         If bneedcomma Then 
		    	params = params & ", " & sFullName
		    	values = values & ", " & sValue
		     Else 
		    	params = params & sFullName
		    	values = values & sValue
		    	bneedcomma = true
		     End If
		End If
	  End If
	 End If  'to remove nt_security_descriptor	
	Next 
	If g_bIsADInstallation And sGetPropProfile(aprops(1))= USER_PROFILE Then 
		If iAccountStatus = "1" Then 
			If g_iPartnerDeskRoleFlags = g_PartnerDeskAdmin Then  
				UserAccountcontrolValue = USERACCOUNT_ADMIN
			Else 
				UserAccountcontrolValue = USERACCOUNT_NORMAL
			End If	
		Else 
			UserAccountcontrolValue = USERACCOUNT_INACTIVE
		End If	
		If bneedcomma Then 
			params = params & ", " & sBracket(g_sUserAccountControl)
			values = values & ", " & UserAccountcontrolValue
			if NOT oRS is Nothing then oRS.Fields(g_sUserAccountControl).Value = UserAccountcontrolValue
			
		Else 
			params = params & sBracket(g_sUserAccountControl)
			values = values & UserAccountcontrolValue
			if NOT oRS is Nothing then oRS.Fields(g_sUserAccountControl).Value = UserAccountcontrolValue
			bneedcomma = true
		End If
	End If	
	params = params & ")"
	values = values & ")"
	sSQL = sSQL & params & values
    		
	If Not oRS is Nothing Then
		If Not IsEmpty(oRS) Then Set Session("UserUpdateRS") = oRS
	End If	
	On Error Goto 0
End sub

Function ExecCMD(sSQL, cn, sErrMsg)
	Dim rs
	ExecCMD = bExecWithErrDisplay(rs, cn, sSQL, sErrMsg)
End Function

Sub MakeMultipleUpdateQuery(aprops, sQuery)
	Dim oEfqb
	Dim oxml, sStr

	Set oXML = Server.CreateObject("MSXML.DomDocument")
	sStr = Request.Form("AdvQuery")
	oxml.loadxml("<xml>" & sStr & "</xml>")
	Session("UPDATE") = True
	Call RemovePropertyNames(oXML)
	Call ConvertXMLNameToGuid(oXML)
	Call ConvertXMLDateTimeValues(oXML)
	Call ConvertXMLSiteTermTypes(oXML)
	'Set oEfqb = Server.CreateObject("commerce.ExprFltrQueryBldr")
	'sQuery = oEfqb.ConvertExprToSqlFltrStr(oXML.DocumentElement.FirstChild.xml)
	sQuery = sGetUpdateClauseFromXML(oXML)
	sQuery = "UPDATE " & sBracket(sGetPropProfile(aprops(1))) & " Set " & sQuery
End Sub

Function sGetUpdateClauseFromXML(oXML)
	Dim xmlNodes
	Dim xmlNode
	Dim xmlProp
	Dim xmlImmedVal
	Dim sSQL
	Dim sSep
	Dim sMultiValued
	Dim sProp, sValue, sType
	sSQL = ""
	sSep = ""
	
    Set xmlNodes = oXML.selectNodes(".//CLAUSE")
    For Each xmlNode In xmlNodes
        Set xmlProp = xmlNode.selectSingleNode(".//Property")
        sProp = sGetAttribute(xmlProp,"ID")
        sMultiValued = sGetAttribute(xmlProp,"MULTIVAL")
        Set xmlImmedVal = xmlNode.selectSingleNode(".//IMMED-VAL")
        If Instr(1,Lcase(sProp),"accountinfo.org_id") > 0 And Len(xmlImmedVal.text) = 0 Then
			Call  abort(L_InvalidOrgName_ErrorMessage)
        End If   
                
        sType = sGetAttribute(xmlImmedVal, "TYPE")
        If strcomp(sMultiValued,"TRUE",1) = 0 Then  
			sValue = sMVPropValue(xmlImmedVal.text, sType)
		Else 
			sValue = sQuoteArg(xmlImmedVal.text, sType)
		End If	
        sSQL = sSQL & sSep & sProp & " = " & sValue
        sSep = ","
    Next 
    sGetUpdateClauseFromXML = sSQL
End Function


Sub ListUpdateForSelectALL(cnProv, dState, aprops, aIDCol, sUpdateSQL)
	
	Dim cn, rs, sTable, sListGUID, sGUIDQuery
	Dim iCol, iType, sIDName
	
	sTable = dState.Value("ListTable")
	sListGUID = dState.Value("ListGUID")
    
	Set cn = Server.CreateObject("ADODB.Connection")
	cn.Open sGetListDSN
	
	icol = aIDCol(2)
    itype = sGetPropType(aprops(icol))'Was (4)
    sIDName = sBracket( sGetPropFullName(aprops(nGetIDColNo(aIDCol))) )

	sGUIDQuery = "SELECT rcp_GUID FROM " & sTable 
	Set rs = cn.Execute(sGUIDQuery)
	
	
	Do while Not rs.Eof
		Call ChunkUpdateOfList(cnProv, rs, sUpdateSQL, sIDName, iType)
	Loop	
	
	rs.Close
	Set rs = nothing
	cn.Close
	Set cn = nothing
	
End Sub

Sub ChunkUpdateOfList(cnProv, rs, sUpdateSQL, sIDName, iType)
	
	Dim aGUIDS
	Dim i, sSQL
	Dim nGUIDSOnPage
	Dim bRet
	Dim sGuid
	
	ReDim aGUIDS(MAX_CLAUSE_VALUES)
	
	For i = 0 to MAX_CLAUSE_VALUES - 1
		If Not rs.Eof Then 
			aGUIDS(i) = rs.Fields("rcp_GUID").Value
			rs.MoveNext 
		Else 
			Exit For
		End If	
	Next 
	nGUIDSOnPage = i
	
	sSQL = " WHERE " & sIDName & " IN "
		
	If nGUIDSOnPage > 0 Then 
		ReDim Preserve aGUIDs(nGUIDsOnPage -1)
		For i = 0 to nGUIDsOnPage - 1
			aGUIDs(i) = sQuoteArg(aGUIDs(i), iType)
		Next 
		sGuid = Join(aGUIDs , ",")
		sSQL = sSQL & " ( " & sGuid & " )"
	Else 
		sSQL = sSQL & "('')"
	End If	
	sSQL = sUpdateSQL & sSQL
	bRet = ExecCMD(sSQL, cnProv, _
						sFormatString(L_BulkUpdateEntity_ErrorMessage,Array(g_sEntity)))
	
End Sub

Function sGetSamAccountNameFormat(sValue)
    Dim nLen
    nLen = InStr(1, sValue, "@")
    If nLen > 0 Then 
        sGetSamAccountNameFormat = Left(sValue, nLen - 1)
    End If
    nLen = InStr(1, sValue, "\")
    If nLen > 0 Then 
        sGetSamAccountNameFormat = Mid(sValue, nLen + 1, Len(sValue) - nLen)
    End If
    If Len(Trim(sGetSamAccountNameFormat)) = 0 Then 
        sGetSamAccountNameFormat = sValue
    End If
End Function


'--------------------------------------------------
'New functions that use XML instead of strings For building up XML EditSheet Fields


Function sXMLMakeEditSheets(oRS, agroups, aprops, nitems, GUIDs, nIDCol)
    Dim sXMLMeta, sXML, ngroups, nprops,  ii, sDIV
    Dim nStart, nURNs, oXMLMeta, oXMLData
    Dim sDataName, sMetaName,oXMLRec
    Dim sXMLReturn
    Dim sXMLGroup
    nURNs = 1
    Set oXMLMeta = oXMLMakeDocumentElement("editsheets")
    Set oXMLData = oXMLMakeDocumentElement("document")'Server.CreateObject("MSXML.DomDocument")
    Set oXMLRec = oXMLData.CreateElement("record")
    oXMLData.DocumentElement.appendChild oXMLRec
    sMetaName = "UserMeta0"
    sDataName = "User0"
    sXMLMeta = "<xml id='"& sMetaName & "'>" &vbCR
    sXML = "<xml id='" & sDataName & "'>" & vbCR
    ngroups = UBound(agroups)
	nStart = 0
	g_nGroupCount = -1
	 
	Call MakeEditSheetGroup(sXMLGroup,oXMLMeta,,oXMLData,oXMLRec,oRS, nStart,ngroups,agroups, aprops, nitems, GUIDs, nIDCol, nURNs)
'   sXMLMeta = sXMLMeta & vbCR & "</fields>" & vbCR & "</editsheet>"
    sXMLMeta = sXMLMeta & vbCR & oXMLMeta.DocumentElement.xml & vbCR & "</xml>"
    sXML = sXML & vbCR & oXMLData.DocumentElement.xml & vbCR & "</xml>"
    sXMLReturn = "<SCRIPT LANGUAGE='VBScript'><!-- " & vbCr & "const NUM_URNS_ON_PAGE = " & nURNs & vbCr & "'--></SCRIPT>" & vbCR
    sXMLReturn = sXMLReturn & sXMLGroup
    sXMLReturn = sXMLReturn & sXMLMeta & vbCR & sXML & vbCR
    sDIV = "<DIV ID='esUser0' CLASS='editSheet' MetaXML='" & sMetaName
    sDIV = sDIV & "' DataXML='"& sDataName 
    sDIV = sDIV & "' ONCHANGE ='OnChange' "
    sDIV = sDIV & "  ONREQUIRE='setRequired(""" &  L_Required_Text 
	sDIV = sDIV & """)' ONVALID='setValid(""" & L_Valid_Text & """)' >" & L_LoadingProperties_Text  & "</DIV>"
    sXMLReturn = sXMLReturn & sDIV
    sXMLMakeEditSheets = sXMLReturn
End Function

Function sMakeXML(i,name ,oXMLES)
	Dim oXMLESG
	Dim sMetaName,sXMLESG
    Set oXMLESG = Server.CreateObject("MSXML.DomDocument")
    
    oXMLESG.appendChild oXMLES
    sMetaName = name
    sXMLESG = "<xml id='"& sMetaName & "'>" &vbCR
	sXMLESG = sXMLESG & vbCR & oXMLES.xml & vbCR & "</xml>"
	sMakeXML = sXMLESG
		
End Function

Sub MakeEditSheetGroup(sXMLGroup, oXMLMeta, oXMLParent, oXMLData,oXMLRec, oRS, nStart, ngroups,agroups, aprops, nitems, GUIDs, nIDCol, nURNs)
    Dim oXMLES, oXMLFields, oXMLMeta2, oXMLColl, oXMLField, oXMLSubData, bNeedTemplate
	Dim oXMLEl, oXMLGlobal, oXMLCData,oXMLESG
    Dim oXMLItem,nSubGroups, nfirst, nlast
    Dim i,sESGName,oXMLTemplate,dataValue
    Dim oXMLTemplateGlobal
    
    Dim populateXMLMethod
    
    populateXMLMethod = nitems
    If nitems = -1 Then	nitems = 1 
    
    For i = nStart to ngroups
		g_nGroupCount = g_nGroupCount + 1
		i = g_nGroupCount
	
		rem build the global tag - expanded/ title
		Set oXMLGlobal = oXMLMeta.CreateElement("global")
		If i = 1 Then 
			oXMLGlobal.SetAttribute "expanded", "yes"
		ElseIf i = 0 Then 
			oXMLGlobal.SetAttribute "title", "no"
		Else   
			oXMLGlobal.SetAttribute "expanded", "no"
		End If
		 	
		nSubGroups = agroups(i)(1)
		
		If nSubGroups > 0 Then 
			sESGName = "esSubGroupMeta" & i
			Set oXMLES = oXMLMeta.CreateElement("editsheet")
			Set oXMLESG = oXMLMeta.CreateElement("editsheets")
			
			Set oXMLTemplateGlobal = oXMLGlobal.cloneNode(True)
			oXMLGlobal.SetAttribute "title", "no"
			Set oXMLTemplate =oXMLMeta.CreateElement("template")
			oXMLTemplate.SetAttribute "register","esSubGroup" & i 
			dataValue ="<DIV ID='esSubGroup" & i & "' CLASS='editSheet'"
			dataValue = dataValue & "MetaXML='" & sESGName & "' "
			dataValue = dataValue & "DataXML='User0'>"
			dataValue = dataValue & "<H3 ALIGN='center'>" & L_LoadingPropGroups_Text & "></H3></DIV>"
			Set oXMLCData = oXMLMeta.CreateCDATASection(dataValue)
			oXMLTemplate.appendChild oXMLCData
								
		Else 
			Set oXMLES = oXMLMeta.CreateElement("editsheet")
		End If
			
		oXMLES.appendchild oXMLGlobal
        
        'Only For groups..
        If i<>0 Then 
			Set OXMLEL = oXMLMeta.CreateElement("name")
			Set oXMLCData = oXMLMeta.CreateCDATASection(agroups(i)(0))
			oXMLEl.AppendChild oXMLCData
			oXMLGlobal.appendChild oXMLEl
		End If	
        
        Set OXMLEL = oXMLMeta.CreateElement("key")
        oXMLGlobal.appendChild oXMLEl
        
        Set OXMLFields = oXMLMeta.CreateElement("fields")
        oXMLES.appendchild oXMLFields
        
		nSubGroups = agroups(i)(1)
        nfirst = agroups(i)(4) 'Starting element in aprops array
        nlast = nfirst + agroups(i)(5) - 1 'Ending element in aprops array

        Dim bHasOneField
        bHasOneField = False
        Set oXMLMeta2 = oXMLCreateXMLFieldMeta(nfirst, nlast, aprops, bNeedTemplate, bHasOneField, (nitems > 1))
        For each oXMLField in oXMLMeta2.DocumentElement.childnodes
            oXMLFields.appendchild oXMLField
        Next 
        
        Select case(populateXMLMethod)
			case -1 Set oXMLSubData = oXMLRequestXML2(aprops, nfirst, nlast) 'restore user entries
            case 0 Set oXMLSubData = oXMLBlankList(aprops, nfirst, nlast) 'Add user
            case 1 Set oXMLSubData = oXMLRS2XML2(oRS, nfirst, nlast, aprops)'Edit
            case Else Set oXMLSubData = oXMLMultiList(aprops, nfirst, nlast, GUIDs, nIDCol) 'Multiedit user
        End Select
        If g_bCopy Then  Set oXMLSubData = oXMLRS2CopyXML(oRS, nfirst, nlast, aprops)
        For each oXMLItem in oXMLSubData.DocumentElement.FirstChild.childNodes
            oXMLRec.appendchild oXMLItem
        Next 
        If bHasOneField Or nSubGroups > 0 Then 
					If nSubGroups > 0 Then 
						oXMLESG.appendChild oXMLES
						Call MakeEditSheetGroup(sXMLGroup, oXMLMeta,oXMLESG,oXMLData,oXMLRec, oRS, i + 1,i + nSubgroups,agroups, aprops, nitems, GUIDs, nIDCol, nURNs )
						
						Dim oXMLESChild						
						Set oXMLESChild = oXMLMeta.CreateElement("editsheet")
						oXMLESChild.appendchild oXMLTemplateGlobal
						 
						Set OXMLEL = oXMLMeta.CreateElement("name")
						oXMLEL.SetAttribute "localize", "yes"
						Set oXMLCData = oXMLMeta.CreateCDATASection(agroups(i)(0))
						OXMLEL.AppendChild oXMLCData
						oXMLTemplateGlobal.appendChild OXMLEL
							
						Set OXMLEL = oXMLMeta.CreateElement("key")
						oXMLEL.SetAttribute "localize", "yes"
						oXMLTemplateGlobal.appendChild OXMLEL
						
						oXMLESChild.appendChild oXMLTemplate
						
						If IsObject(oXMLParent) Then
							sXMLGroup = sXMLGroup & sMakeXML(i,sESGName,oXMLESG)
							oXMLParent.appendChild oXMLESChild
						Else
							sXMLGroup = sXMLGroup & sMakeXML(i,sESGName,oXMLESG)
							oXMLMeta.documentelement.appendchild oXMLESChild
						End If
						
						i = i + nSubgroups
					Else
						If IsObject(oXMLParent) Then 
							oXMLParent.AppendChild oXMLES
						Else						
							oXMLMeta.documentelement.appendchild oXMLES
						End If
					End If
        End If        
   Next 
End Sub

Sub MakeEditTemplate(oXML, nfirst, nlast, aprops)
    Dim avalues, i, bDidTemplate, bDoTemplate, sFieldDivName
    Dim sCatName, bReference, bOrg, bSiteTerm, bUser, bUserOrOrg, oXMLTemp
    Dim sHTML, sName, SID, oXMLGlobal, oXMLEl, iend, oXMLES, oXMLCData, sRef
   'This has some redundant processing unfortunately
    For i = nfirst to nlast
        bReference = False
        bOrg = False
        bUser = False
        bSiteTerm = False
        bUserOrOrg = False
        avalues = aprops(i)
        sName = sGetPropName(avalues)
        sRef = sGetPropRefString(avalues)
        If len(trim(sRef)) > 0 Then 
            iEnd = Instr(1, sRef, ".")
            If iEnd > 0 Then 
                sCatName = Mid(sRef, 1, iend-1)
            Else 
                sCatName = sRef
            End If
            If (strcomp(sCatName, "Site Terms", 1) = 0) Then 
                bSiteTerm = True
                bReference = False
            Else 
                bSiteTerm = False
                bReference = True
            End If
            If (bReference) Then 
                If strcomp(sRef, REF_USER_PROFILE, 0) = 0 Then 
                    bUser = True
                End If
                If strcomp(sRef, REF_ORG_PROFILE, 0) = 0 Then 
                    bOrg = True
                End If
                bUserOrOrg = bOrg OR bUser
            End If
        End If
        bDoTemplate = bReference And Not bUserOROrg
       'Is this a property with a Profile reference that is Not to a user or org?
       'If so we need to Set up a subwindow with a button in it to go to another page.

        If bDoTemplate Then 
            Set oXMLES = oXML.CreateElement("editsheet")
            Set oXMLGlobal = oXML.CreateElement("global")
            oXMLGlobal.SetAttribute "expanded", "no"
            oXMLES.appendchild oXMLGlobal
        
            Set OXMLEL = oXML.CreateElement("name")
            oXMLEL.SetAttribute "localize", "yes"
            Set oXMLCData = oXML.CreateCDATASection(sName)
            oXMLEl.AppendChild oXMLCData
            oXMLGlobal.appendChild oXMLEl
        
            Set OXMLEL = oXML.CreateElement("key")
            oXMLEL.SetAttribute "localize", "yes"
            'oXMLEl.text = sName
            oXMLGlobal.appendChild oXMLEl

            Set oXMLTemp = oXML.CreateElement("template")
            sHTML = "<DIV>"
            sHTML = sHTML & "<BUTTON ID='btn" & sName & "' Name='btn" & sName & "' CLASS='bdbutton' LANGUAGE='VBScript'"
            sHTML = sHTML & " ONCLICK=""SeparatePage window.event.srcElement,'" & sRef & "' "">Choose " & vbCR & sName & "</BUTTON>"
		   'sHTML = sHTML & "<INPUT TYPE='SUBMIT' Name='" & sName & "' ID='"& sName & "'"
           'sHTML = sHTML & " VALUE='Choose " & sName & "' ONCLICK='SeparatePage()'>"
            sHTML = sHTML & "</DIV>"
            Set oXMLCDATA = oXML.CreateCDATASection(sHTML)
            oXMLTemp.appendchild oXMLCDATA
            oXMLES.appendchild oXMLTemp
            oXML.documentelement.appendchild oXMLES
        End If
    Next 
End Sub

Function oXMLGetCatalogSets()
	Dim oXMLDoc, oXMLElement
	Dim i
	Dim oXMLChild
	Dim oXMLText
	Dim sValue, sName
	Dim oRS
	Set oXMLDoc = Server.CreateObject("MSXML.DOMDocument")
	Set oXMLElement = oXMLDoc.CreateElement("select")
	Set oXMLDoc.DocumentElement = oXMLElement
	
	rem To create blank entries
	sValue = ""
	sName = ""
	Set oXMLChild = oXMLDoc.CreateElement("option")
	oXMLChild.SetAttribute "value", sValue
	Set oXMLText = oXMLDoc.CreateTextNode(sName)
	oXMLChild.AppendChild oXMLText
	oXMLDoc.DocumentElement.appendChild oXMLChild
	rem Get the catalogsets. Tables required - catalogset_catalogs And catalogset_info
	Set oRS = rsGetCatalogSets()
	If Not Typename(oRS) = "Nothing" Then 
		Do While Not oRS.EOF 
		  sName = oRS("catalogSetName").value
		  sValue = oRS("catalogSetId").value
		  Set oXMLChild = oXMLDoc.CreateElement("option")
		  oXMLChild.SetAttribute "value", sValue
		  Set oXMLText = oXMLDoc.CreateTextNode(sName)
		  oXMLChild.AppendChild oXMLText
		  oXMLDoc.DocumentElement.AppendChild oXMLChild
		  oRs.MoveNext 
		Loop
	End If
	Set oXMLGetCatalogSets = oXMLDoc
	
End Function

Function rsGetCatalogSets()
	Dim objCatalogSets
	Dim mscsCatalogConnStr,mscsTransactionConfigConnStr	
		
   'Keep this in the Session And refresh with publish profiles 
	On Error Resume Next 'Application("MSCSCatalogSets")

	If Not IsObject(Session("MSCSCatalogSets")) or Session("MSCSCatalogSets")= Nothing  or Session("MSCSCatalogVer") <> Application("MSCSCatalogVer") Then 
		mscsTransactionConfigConnStr = GetSiteConfigField("Transaction Config", "connstr_db_TransactionConfig")   
		mscsCatalogConnStr = GetSiteConfigField("product catalog", "connstr_db_Catalog")
		Set objCatalogSets = Server.CreateObject("Commerce.CatalogSets")
		objCatalogSets.Initialize mscsCatalogConnStr, mscsTransactionConfigConnStr
					
		Set rsGetCatalogSets = objCatalogSets.GetCatalogSets
		If Err.number <> 0 Then 
			Set rsGetCatalogSets = Nothing
		End If
		Set Session("MSCSCatalogSets") = rsGetCatalogSets
		Session("MSCSCatalogVer") = Application("MSCSCatalogVer")
			
		Set objCatalogSets = Nothing
	Else 
	    Set rsGetCatalogSets = Session("MSCSCatalogSets")
	    rsGetCatalogSets.MoveFirst
	End If     
		
	On Error Goto 0
		
End Function		

'Sub oXMLMakeBlankEnum(oXMLDoc, name)
 '   Dim oXMLElement, oXMLChild
  '  Set oXMLDoc = Server.CreateObject("MSXML.DOMDocument")
'    Set oXMLElement = oXMLDoc.CreateElement("select")
 '   oXMLElement.SetAttribute "id", name
  '  Set oXMLDoc.DocumentElement = oXMLElement
'    Set oXMLChild = oXMLDoc.CreateElement("option")
 '   oXMLChild.text = ""
  '  oXMLDoc.DocumentElement.appendChild oXMLChild
'End sub

Sub oXMLMakeBlankEnum(oXMLDoc, name)
    
    Dim oXMLElement, oXMLChild
    Dim sValue
    Dim oXMLText
    sValue = ""
    Set oXMLDoc = Server.CreateObject("MSXML.DOMDocument")
    Set oXMLElement = oXMLDoc.CreateElement("select")
    oXMLElement.SetAttribute "id", name
    Set oXMLDoc.DocumentElement = oXMLElement
    Set oXMLChild = oXMLDoc.CreateElement("option")
    oXMLChild.SetAttribute "value", sValue
    set oXMLText = oXMLDoc.CreateTextNode("")
	oXMLChild.AppendChild oXMLText
     ' oXMLChild.text = ""
    oXMLDoc.DocumentElement.appendChild oXMLChild
    
End sub

 Function oXMLCreateXMLFieldMeta(nfirst, nlast, aprops, bNeedTemplate, bHasOneField, bMulti)
    Dim sXML, avalues, i, brequired, sEnum, sTemp, bTemp, oXMLClone
    Dim oXML, oXMLIn, oXMLRefField
    Set oXML = oXMLMakeDocumentElement("document")
    const date_End = "_date"
    const time_End = "_time"
    const itime = 5
    const idate = 6
    sXML = ""
    'Debug here for diamond on the Subgroup metas
    For i = nfirst to nlast
        avalues = aprops(i)
        'Weed out multivalued properties For now And properties Not mapped to data columns
        'If sIsPropMultiValued(avalues) = False And bIsPropMappedToData(aValues) Then 
			If bIsPropMappedToData(aValues) Then 
            Set oXMLin =  oXMLGetFieldXML(avalues, bTemp, bMulti)
            oXML.DocumentElement.appendchild oXMLIn.DocumentElement
             
            	If bIsReference(aValues) And Not bIsCatalogRef(aValues) And LCase(aValues(4)) <> "profile" Then 
					rem this is required because the GetFieldXML routine appends 
					rem _browse to the field created.
					Set oXMLin = oXMLGetCloneField(aValues)
					oXML.DocumentElement.appendchild oXMLIn.DocumentElement
				End If
            bNeedTemplate = bNeedTemplate OR bTemp
            bHasOneField = bHasOneField OR bisPropActive(avalues)
        
        End If
    Next
    Set oXMLCreateXMLFieldMeta = oXML
End Function

Function oXMLGetCloneField(aValues)
    Dim sName, sID
    Dim oXMLField
    Dim oXMLEL
    
    sName = sGetDisplayName(avalues) 'sGetPropName(avalues)
    sID = sGetPropName(avalues) 'sName 'avalues(9)
    
    Set oXMLField = oXMLMakeDocumentElement("text")
    oXMLField.DocumentElement.Setattribute "id", sID
    oXMLField.DocumentElement.Setattribute "subtype", "short"
	oXMLField.DocumentElement.Setattribute "hide", "yes"
	 
   
    Set oXMLEL = OXMLField.CreateElement("name")
    oXMLEl.text = sName
    oXMLField.DocumentElement.appendchild oXMLEl
    
    Set oXMLEL = OXMLField.CreateElement("tooltip")
    oXMLField.DocumentElement.appendchild oXMLEl
	 
	Set oXMLGetCloneField = oXMLField

End Function


 Function bIsSiteTerm(ByVal aValues)
    Dim sCatName
    bIsSiteTerm = False
   'sCatName = sExtractCatalog(sGetPropRefString(avalues))
    sCatName = sExtractCatalog(avalues(5))
   'If (strcomp(sGetPropType(avalues), "SITETERM", 1) = 0) Then 
    If (strcomp(avalues(4), "SITETERM", 1) = 0) Then 
			bIsSiteTerm = True
    End If
    If (strcomp(sCatName, "SiteTerms", 1) = 0) or _
		 (strcomp(sCatName, "Site Terms", 1) = 0) Then 
			bIsSiteTerm = True
    End If
End Function


 Function bIsReference(ByVal aValues)
    Dim sCatName
    'Temp Hack to remove Reference equals True For user_id_changed_by.
    '-----------------------------------------------------------------
   ' If (sGetPropName(avalues) = "user_id_changed_by") Then 
    If (avalues(2) = "user_id_changed_by") Then 
		bIsReference = False
		Exit Function
	End If	
    '------------------------------------------------------------------
   ' sCatName = sExtractCatalog(sGetPropRefString(avalues))
    sCatName = sExtractCatalog(avalues(5))
    bIsReference = False
   ' If (strcomp(sGetPropType(avalues), "PROFILE", 1) = 0) Then 
    If (strcomp(avalues(4), "PROFILE", 1) = 0) Then 
			bIsReference = True
    End If
	 If (strcomp(sCatName, "Profile Definitions", 1) = 0) or (strcomp(sCatName, CATALOG_PROFILE, 1) = 0)  Then 
			bIsReference = True
    End If
End Function

Function bIsCatalogRef(ByVal aValues)
	Dim arrCatProfile
	Dim sProfileName
	'sProfileName = sGetPropRefString(avalues)
	sProfileName = avalues(5) 
    bIsCatalogRef = False
    If Len(sProfileName) > 0 Then 
		arrCatProfile = Split(sProfileName,".")
		If strcomp(arrCatProfile(0), CATALOG_PROFILE, 0) = 0 Then 
			bIsCatalogRef = True
		End If
   End If
End Function

 Function bIsUserRef(ByVal aValues)
    'If strcomp(sGetPropRefString(avalues), REF_USER_PROFILE, 0) = 0 Then 
    If strcomp(avalues(5), REF_USER_PROFILE, 0) = 0 Then 
        bIsUserRef = True
    Else 
        bIsUserRef = False
    End If
End Function

 Function bIsOrgRef(ByVal aValues)
    'If strcomp(sGetPropRefString(avalues), REF_ORG_PROFILE, 0) = 0 Then 
    If strcomp(avalues(5), REF_ORG_PROFILE, 0) = 0 Then 
        bIsOrgRef = True
    Else 
        bIsOrgRef = False
    End If
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLGetFieldXML(ByVal avalues, ByRef bNeedTemplate, ByRef bMulti)
'
'	avalues -  array containing property attributes
'	bNeedTemplate  - boolean value to check If template is required
'	bMulti   -  boolean value indicating If it's a multivalue property
'
 Function oXMLGetFieldXML(ByVal avalues, ByRef bNeedTemplate, ByRef bMulti)
    Dim sXML, bRequired, sName, sID, iType, sEnum, bUnique, bReadOnly, bActive, sType
    Dim bSiteTerm, bReference, sCatName, iend, bUserOrOrg, bOrg, bUser
    Dim bPrimaryOrJoinKey
    Dim oXMLField, oXMLEl,oXMLCMask, oXMLEl2, oXMLErr, sTextCharMask
    Dim oXMLFormat
    Dim oXMLBrowseEl
    Dim sCharMask , sError, sTooltip
    Dim bCatalogRef
    Dim bBrowseField
	Dim sReadOnly
	Dim nMaxLen
	Dim nMinLen
	Dim oXMLPrompt
	
	bBrowseField = False
   
	'Set up some initial values For variables
    bReference = False
    bSiteTerm = False
    buserororg = False
    borg = False
    bUser = False
    bCatalogRef = bIsCatalogRef(aValues)
    bNeedTemplate = False
    'bRequired = bIsPropRequired(avalues)
    bRequired = avalues(3)
    bPrimaryOrJoinKey = bIsPropJoinKey(avalues) Or bIsPropUniqueKey(avalues)
    'iType = sGetPropType(avalues)
    iType = avalues(4)
    sType = lcase(iType) ' avalues(4) = iype
    'sName = sGetDisplayName(avalues) 'sGetPropName(avalues)
    sName = avalues(12) 
    'sID = sGetPropName(avalues) 'sName 'avalues(9)
    sID = avalues(2) 'sName 'avalues(9)
    bUnique = bIsPropUniqueKey(avalues)
    'bReadOnly = bIsPropReadOnly(avalues)
    bReadOnly = avalues(11)
    'bActive = bIsPropActive(avalues)
    bActive = avalues(13)
    sToolTip = ""
    sError = ""
	'Test to see If this field is a regular field, a site term, or a reference
	'based on contents of "referenceString" attribute
		
	'If len(trim(sGetPropRefString(avalues))) > 0 Then 
    If len(trim(avalues(5))) > 0 Then 
	'For Alpha, Only type=siteterm qualifies as siteterm.
        bSiteTerm = bIsSiteTerm(aValues)
        bReference = bIsReference(aValues)
        If (bReference) Then 
            bUser = bIsUserRef(aValues)
            bOrg = bIsOrgRef(aValues)
            bUserOrOrg = bUser OR bOrg
        End If
    End If
    
    If bPrimaryOrJoinKey And g_bIsADInstallation And (g_sType = "open" or g_sType = "save") Then 
		bReadOnly = True
	ElseIf bIsPropJoinKey(avalues) And g_sType <> "add" And g_sType <> "savenew" And g_sType <> "copy" Then 
		bReadOnly = True
'	ElseIf Not bIsPropUpdatable(aValues) And (g_sType = "open" or g_sType = "save") Then 
	ElseIf Not aValues(10) And (g_sType = "open" or g_sType = "save") Then 
		bReadOnly = True
    End If
   
    Select case sType
		case "bool"
			Set oXMLField = oXMLMakeDocumentElement("boolean")
        case "wstr", "string", "reference", "binary" , "longtext", "image"
            If bSiteTerm Then  'OR (bReference And bUserOrOrg)
					Set oXMLField = oXMLMakeDocumentElement("select")
					sToolTip = L_SiteTerm_Tooltip
            ElseIf bReference And bCatalogRef Then 
                    
					Set oXMLField = oXMLMakeDocumentElement("select")
					sToolTip = L_SiteTerm_Tooltip
            ElseIf bReference Then 
                    
            		Set oXMLField = oXMLMakeDocumentElement("text")
					oXMLField.DocumentElement.Setattribute "subtype", "short"
					oXMLField.DocumentElement.Setattribute "onbrowse", "onBrowse"
					If Not bReadOnly Then 
						oXMLField.DocumentElement.Setattribute "browsereadonly", "yes"
					End If
					bBrowseField = True
					sTooltip = L_Ref_Tooltip
	        
            Else 
                
				Set oXMLField = oXMLMakeDocumentElement("text")
                oXMLField.DocumentElement.Setattribute "subtype", "short"
                
                If sID = "user_id_changed_by" Then 
                    oXMLField.DocumentElement.Setattribute "readonly", "yes"
                    oXMLField.DocumentElement.Setattribute "default", Request.ServerVariables("LOGON_USER")
                    sTooltip = L_ChangedBy_Tooltip
                End If    
                'check If field is Primary key or JoinKey
                'check If Password
               If bPrimaryOrJoinKey Then 
					sCharMask = AD_CHAR_MASK
					rem sCharMask = ""
					sError = L_TextPK_ErrorMessage
				    sTooltip = L_TextPK_Tooltip
  			   Else 
					sCharMask = ""'"[^*""&']*"
					'sError = L_Text_ErrorMessage
					'sToolTip = L_Text_Tooltip
               End If
            End If
            
            If IsEmpty(nMaxLen) And g_bIsADInstallation And ( sID = "name" or sID = "logon_name")  Then  
                If Len(AD_ADMIN_GROUP_SUFFIX) > Len(AD_USER_GROUP_SUFFIX) Then 
					nMaxLen = AD_CN_LEN - Len(AD_ADMIN_GROUP_SUFFIX)
                Else 
	                nMaxLen = AD_CN_LEN - Len(AD_USER_GROUP_SUFFIX)
                End If
                If sID = "logon_name" Then  nMaxLen = AD_SAM_ACCOUNT_NAME_LEN
                
                sCharMask = AD_CHAR_MASK
                sError = L_TextPK_ErrorMessage
				'64 is the maxlen For a cn in AD  -11 chars For AD_ADMIN_GROUP_SUFFIX
			End If     
        
        case "siteterm"
            Set oXMLField = oXMLMakeDocumentElement("select")
		'	oXMLField.DocumentElement.Setattribute "default", 1                
			sToolTip = L_SiteTerm_Tooltip
        case "profile"
     
	        Set oXMLField = oXMLMakeDocumentElement("text")
            oXMLField.DocumentElement.Setattribute "subtype", "short"

			sTooltip = L_Ref_Tooltip
        
        case "number", "long" 
            If bSiteTerm Then 
                Set oXMLField = oXMLMakeDocumentElement("select")
                sToolTip = L_SiteTerm_Tooltip
            ElseIf bReference Then  
               Set oXMLField = oXMLMakeDocumentElement("text")
               oXMLField.DocumentElement.Setattribute "subtype", "short"
               oXMLField.DocumentElement.Setattribute "onbrowse", "onBrowse"
               sTooltip = L_Ref_Tooltip
					If Not bReadOnly Then 
						oXMLField.DocumentElement.Setattribute "browsereadonly", "yes"
					End If
               bBrowseField = True
            Else 
               Set oXMLField = oXMLMakeDocumentElement("numeric")
               oXMLField.DocumentElement.Setattribute "subtype", "integer"
				Set oXMLFormat = OXMLField.CreateElement("format")
				oXMLField.DocumentElement.appendchild oXMLFormat
				oXMLFormat.text = g_sMSCSNumberFormat
               'sError = L_Num_ErrorMessage
               'sTooltip = L_Num_Tooltip
            End If
        case "datetime" , "date"
            Set oXMLField = oXMLMakeDocumentElement("date")
			If Not (g_sType = "open" or g_sType = "save" ) Then 
				oXMLField.DocumentElement.Setattribute "default", g_MSCSDataFunctions.date( Now(), g_MSCSDefaultLocale )
			End If	
			Set oXMLFormat = OXMLField.CreateElement("format")
			oXMLField.DocumentElement.appendchild oXMLFormat
			oXMLFormat.text = g_sMSCSDateFormat
            'sError = L_Date_ErrorMessage
            sTooltip = L_Date_Tooltip
	    case "time"
			Set oXMLField = oXMLMakeDocumentElement("time")
            'sError = L_Time_ErrorMessage
            sTooltip = L_Time_Tooltip
        case "currency"
			Set oXMLField = oXMLMakeDocumentElement("numeric")
            oXMLField.DocumentElement.Setattribute "subtype", "currency"
			Set oXMLFormat = OXMLField.CreateElement("format")
			oXMLField.DocumentElement.appendchild oXMLFormat
			oXMLFormat.text = g_sMSCSCurrencyFormat
            'sError = L_Curr_ErrorMessage
            sTooltip = L_Curr_Tooltip
        case "float"
			Set oXMLField = oXMLMakeDocumentElement("numeric")
            oXMLField.DocumentElement.Setattribute "subtype", "float"
			Set oXMLFormat = OXMLField.CreateElement("format")
			oXMLField.DocumentElement.appendchild oXMLFormat
			oXMLFormat.text = g_sMSCSNumberFormat
		case "password"
             
           	Set oXMLField = oXMLMakeDocumentElement("text")
			oXMLField.DocumentElement.Setattribute "subtype", "password"
			
			If g_sType = "open" or g_sType = "save" Then   '
				oXMLField.DocumentElement.Setattribute "default", passwdguid
			End If 	
				
			nMaxLen = MAX_PASSWORD_LENGTH
			nMinLen = MIN_PASSWORD_LENGTH
			Set oXMLPrompt = OXMLField.CreateElement("prompt")
			oXMLPrompt.text = PASSWORD_MASK
			oXMLField.DocumentElement.appendchild oXMLPrompt
			rem sCharMask = "[^?*/\\""|:<>=+;,#]*"
			If g_bIsADInstallation Then  sCharMask = AD_CHAR_MASK
			sError = L_TextPwd_ErrorMessage
			sTooltip = L_TextPwd_Tooltip
	    case Else  abort(L_UnknownType_ErrorMessage & sType) 
  
    End Select

    If bBrowseField And Not bCatalogRef Then 
		sID = sID & "_browse"
		oXMLField.DocumentElement.Setattribute "id", sID
    Else 
        oXMLField.DocumentElement.Setattribute "id", sID
    End If
    
    If (brequired) And bActive And Not bMulti Then 
        oXMLField.DocumentElement.Setattribute "required", "yes"
    End If
    If bReadOnly Then 
		oXMLField.DocumentElement.Setattribute "readonly", "yes"
    End If

    
    Set oXMLCMask = OXMLField.CreateElement("charmask")
    oXMLCMask.SetAttribute "localize", "no"
    oXMLCMask.text = sCharMask
    oXMLField.DocumentElement.appendchild oXMLCMask

	If Len(sError) > 0 Then 
		Set oXMLErr = OXMLField.CreateElement("error")
		oXMLField.DocumentElement.appendchild oXMLErr
		oXMLErr.SetAttribute "localize", "yes"
		oXMLErr.text = sError
	End If	
    
    If Len(sTooltip) > 0 Then 
		Dim oXMLTool
		Set oXMLTool = OXMLField.CreateElement("tooltip")
		oXMLTool.SetAttribute "localize", "yes"
		oXMLTool.text = sTooltip
		oXMLField.DocumentElement.appendchild oXMLTool
    End If
    
    If Not (bActive) Then 
        oXMLField.DocumentElement.Setattribute "hide", "yes"
    ElseIf bReference And Not bUserOrOrg and Not sType = "profile" Then 
        bNeedTemplate = True
    End If
    oXMLField.DocumentElement.Setattribute "error", "yes"
   
    If IsEmpty(nMaxLen)  Then     nMaxLen = 255
	
	
    oXMLField.DocumentElement.Setattribute "maxlen", nMaxLen
    
    If Not IsEmpty(nMinLen) Then  
        oXMLField.DocumentElement.Setattribute "minlen", nMinLen
    End If
      
    Set oXMLEL = OXMLField.CreateElement("name")
    oXMLEl.text = sName & ":"
    rem oXMLEl.SetAttribute "localize", "yes"
    oXMLField.DocumentElement.appendchild oXMLEl
         

    Dim oXMLEn
  
    If oXMLField.DocumentElement.nodeName = "select" Then 
      
		If bCatalogRef Then 
			Set oXMLEn = oXMLGetCatalogSets()
		Else 
		'	Set oXMLEn = oXMLGetSiteTerms(sGetPropRefString(avalues), sGetPropName(avalues))
		   	Set oXMLEn = oXMLGetSiteTerms(avalues(5), avalues(2))
        End If
        oXMLField.DocumentElement.AppendChild oXMLEn.DocumentElement
        If Not (g_sType = "open" or g_sType = "save" ) Then 
       		If sID = "user_type" or sID = "partner_desk_role" or sID = "account_status" Then 
				oXMLField.DocumentElement.Setattribute "default",1
			End If
		End If 	
    End If
    Set oXMLGetFieldXML = oXMLField

End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function xmlGetAuxFriendlyDisplay(avalues, ByVal sFldName, ByVal sFldVal)
'
'	avalues -  array containing property attributes
'	sFldName  -  name of field
'	sFldVal   -  value of the field (ID as it's the JK value)
'	creates an extra field with browse appended to the fldName for
'   friendly display
'
Function xmlGetAuxFriendlyDisplay(avalues, ByVal sFldName, ByVal sFldVal)
	Dim sRef
	Dim sProfile, nLen
	Dim oXMLProfile, oXMLDisplayCols
	Dim sID, sName
	Dim cn
	Dim sSQL
	Dim tempState
	Dim sDisplayValue
	Dim rs
	Dim bRet
	Dim xmlChildNode
	Dim xmlDoc
	Dim sGrp 
	
	
	sGrp = g_sGeneralInfo & "."
	  
	sFldName = sStripGroup(sFldName)
	Set xmlDoc = CreateObject("MSXML.DOMDocument")
	Set xmlGetAuxFriendlyDisplay = xmlDoc
	sRef = sGetPropRefString(avalues)
	nLen = InStr(1, sRef, ".") + 1
    sProfile = Mid(sRef, nLen, Len(sRef))
    
	If sExtractCatalog(sRef) = sGetCatalogName() Then 
		Set oXMLProfile = oXMLGetAuxProfile(sRef)
		Set oXMLDisplayCols = oXMLWhichColsToDisplay(sRef)
		
		If sProfile = ORG_PROFILE Then 
		   sID = XML_ATTR_ORGID
		ElseIf sProfile = USER_PROFILE Then
		   sID = XML_ATTR_USERID
		Else   
		  sID = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty & "[@" & _
		  sXMLAttrKeyDef & "='JOIN'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']").GetAttribute(sXMLAttrName)   
		End If   
		   
		sName = oXMLDisplayCols.DocumentElement.SelectSingleNode(".//" & sXMLTagColumn & _
		    "[@isFriendlyName='1']").GetAttribute(sXMLAttrName)
		sSQL = "SELECT " & sGrp & sID & "," & sGrp & sName & " FROM " & sProfile & _
				 " WHERE " & sGrp & sID & " = '" & sFldVal & "'" 
		Set tempState = CreateObject("Commerce.Dictionary")		 
		tempState.ProfileDSN = sGetProfileDSN()
		Set cn = cnGetProviderConn()
		'cn.Open sGetProviderConnStr()
		bRet = bExecWithErrDisplay(rs, cn, sSQL, L_AuxDisplay_ErrorMessage)
		If bRet Then 
			If Not rs.Eof Then 
				sDisplayValue = rs(1)
				Set xmlChildNode = xmlDoc.createNode(1, sFldName & "_browse" , "")
				xmlChildNode.Text = sDisplayValue
				Set xmlGetAuxFriendlyDisplay = xmlChildNode
			End If
			Set rs = Nothing	
		End If		

		Set cn = Nothing
		Set tempState = Nothing
	End If
	
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLRS2XML2(rsQuery, nfirst, nlast, aprops)
'
'	rsQuery -  recordset
'	nfirst  -  index of first property in aprops array
'	nlast   -  index of last property in aprops array
'	aprops  - properites array of array
'	returns XMLDataisland
'
Function oXMLRS2XML2(rsQuery, nfirst, nlast, aprops)
    Dim xmlDoc, xmlNode, fdField, xmlChildNode, bIsNewRecord, sValue, i, avalues
    Dim dtmp, ddate, dtime
    Dim nType
    Dim xmlAuxNode
    Dim sRef,nLen,sProfile
    
    Set xmlDoc = CreateObject("MSXML.DOMDocument")
    Set xmlNode = xmlDoc.createNode(1,"document","")
    Set xmlDoc.documentElement = xmlNode
    Set xmlNode = xmlDoc.createNode(1, "record", "")
    xmlDoc.documentElement.appendChild xmlNode
    For i = nfirst to nlast
        avalues = aprops(i)
        'Weed out multivalued properties For now And properties Not mapped to data columns
        'If sIsPropMultiValued(avalues) = False And bIsPropMappedToData(avalues) Then 
        
        If bIsPropMappedToData(avalues) Then 
			If Not rsQuery.EOF Then 
				Set fdfield = rsQuery.fields.item(nGetPropMVOffset(aprops(i))-1)
			
				If sIsPropMultiValued(avalues) = True And IsArray(fdField.value) = True And Not IsNull(fdField.value) and sStripGroup(fdField.name) <> "user_security_password" Then 
					sValue = Join(fdField.value, MV_DELIM)
				Else 
					sValue = fdField.value
				End If	
				If sStripGroup(fdField.name) = "user_id_changed_by" Then sValue = Request.ServerVariables("LOGON_USER") 
				' CHECK Handle AD_DB_FILETIME, AD_FILETIME

			   		Select case fdField.Type
						case AD_DB_DATE,AD_DB_TIMESTAMP,AD_FILETIME,AD_DB_FILETIME
							If LCase(sGetPropType(avalues)) = "time" Then 
								nType = AD_DB_TIME
							ElseIf sStripGroup(fdField.name)="date_last_changed" or sStripGroup(fdField.name)="date_created" Then
								nType = AD_DB_TIMESTAMP
							Else
								nType = AD_DATE
							End If	
						case Else 
								nType =	fdField.Type
					End select
				Call AddFormattedNode(xmlNode, sStripGroup(fdField.name), sValue, nType)
				
				If bIsReference(avalues) And Not isNull(sValue) Then
				    sRef = sGetPropRefString(avalues)
	                nLen = InStr(1, sRef, ".") + 1
                    sProfile = Mid(sRef, nLen, Len(sRef)) 	
                    
                    If sProfile = ORG_PROFILE or sProfile = USER_PROFILE Then 
		   					If Len(sValue) > 0 Then  
								Set xmlAuxNode = xmlGetAuxFriendlyDisplay(avalues, fdField.name, sValue)
								If xmlAuxNode.childNodes.Length > 0 Then  _
								xmlNode.appendChild xmlAuxNode
							End If	
					End If		
				End If

            End If
        End If
    Next 
    Set oXMLRS2XML2 = xmlDoc
    Call SetPreviousFieldValue(rsQuery, nfirst, nlast, aprops)
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLRequestXML2(aprops, nfirst, nlast)
'
'	aprops  - properites array of array
'	nfirst  -  index of first property in aprops array
'	nlast   -  index of last property in aprops array
'	returns XMLDataisland
'
Function oXMLRequestXML2(aprops, nfirst, nlast)
    Dim xmlDoc, xmlNode, fdField, xmlChildNode, bIsNewRecord, sValue, i, avalues
    Dim dtmp, ddate, dtime
    Dim nType
    Dim xmlAuxNode
    Dim sName				  
    Dim sRef,nLen, sProfile
    
    Set xmlDoc = CreateObject("MSXML.DOMDocument")
    Set xmlNode = xmlDoc.createNode(1,"document","")
    Set xmlDoc.documentElement = xmlNode
    Set xmlNode = xmlDoc.createNode(1, "record", "")
    xmlDoc.documentElement.appendChild xmlNode
   
    avalues = aprops(0)
    For i = 1 to Ubound(aprops)
        avalues = aprops(i)
        'Weed out multivalued properties For now And properties Not mapped to data columns
        'If sIsPropMultiValued(avalues) = False And bIsPropMappedToData(avalues) Then 
        
        If bIsPropMappedToData(avalues) Then 
			sName = sGetPropName(aprops(i))
			sValue = trim(Request.Form(sName))
				
			If sIsPropMultiValued(avalues) = True And IsArray(sValue) = True And Not IsNull(sValue) and sStripGroup(sName) <> "user_security_password" Then 
				sValue = Join(sValue, MV_DELIM)
			End If	
		
			If sStripGroup(sName) = "user_id_changed_by" Then 
				sValue = Request.ServerVariables("LOGON_USER") 
			End If
			
		
'			' CHECK Handle AD_DB_FILETIME, AD_FILETIME
'		  	select case fdField.Type
'				case AD_DB_FILETIME, AD_FILETIME
'					nType = AD_DB_TIMESTAMP
'				case Else 
'					nType =	fdField.Type
'			End select
 			Call AddFormattedNode(xmlNode, sName, sValue, nType)
				
			If bIsReference(avalues) And Not isNull(sValue) Then 
				sRef = sGetPropRefString(avalues)
	            nLen = InStr(1, sRef, ".") + 1
                sProfile = Mid(sRef, nLen, Len(sRef)) 	
                
                'Filter added as we now support only editing of User/Org Profile
                'We may need to remove once we have the Generic Profile Editor
                If sProfile = ORG_PROFILE or sProfile = USER_PROFILE Then 
					If Len(sValue) > 0 Then  
						Set xmlAuxNode = xmlGetAuxFriendlyDisplay(avalues, sName, sValue)
						If xmlAuxNode.childNodes.Length > 0 Then  _
							xmlNode.appendChild xmlAuxNode
					End If	
				End If	
			End If
			
        End If
    Next 
    Set oXMLRequestXML2 = xmlDoc
    ' Call SetPreviousFieldValue(rsQuery, nfirst, nlast, aprops)
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLRS2CopyXML(rsQuery, nfirst, nlast, aprops)
'
'	rsQuery -  recordset
'	nfirst  -  index of first property in aprops array
'	nlast   -  index of last property in aprops array
'	aprops  - properites array of array
'	returns XMLData islAnd with copied values For the edit sheet
'	copies only values which are Not unique And are Not references.
'
Function oXMLRS2CopyXML(rsQuery, nfirst, nlast, aprops)
    Dim xmlDoc, xmlNode, fdField, xmlChildNode, bIsNewRecord, sValue, i, avalues
    Dim dtmp, ddate, dtime , sType , bMakeBlank
    Dim nType
    Dim xmlAuxNode
    Dim sRef,nLen,sProfile
    
    Set xmlDoc = CreateObject("MSXML.DOMDocument")
    Set xmlNode = xmlDoc.createNode(1,"document","")
    Set xmlDoc.documentElement = xmlNode
    Set xmlNode = xmlDoc.createNode(1, "record", "")
    xmlDoc.documentElement.appendChild xmlNode
    
    For i = nfirst to nlast
        avalues = aprops(i)
        'Weed out multivalued properties For now
        'If sIsPropMultiValued(avalues) = False And bIsPropMappedToData(avalues) Then 
        If bIsPropMappedToData(avalues) Then 
        	sType = sGetPropType(avalues)
				If bIsPropUniqueKey(avalues) OR _
				bIsPropReadOnly(avalues) OR _
				bIsPropJoinKey(avalues) OR _ 
				(bIsPropUpdatable(avalues) = FALSE) OR _
				(LCase(sType) = "password") Then 

				bMakeBlank = True
			Else 
				bMakeBlank = False
			End If
			
			If sGetPropFullName(aValues) = g_sAccountOrgId Then  bMakeBlank = False
			Rem It doesn't make sense to copy an organization dependent props to another
			Rem For eg. Purchasing Manager or DefaultBlanketPO.
			If sGetPropProfile(aprops(1))= ORG_PROFILE	 Then 
				'If bIsReference(avalues) Then  bMakeBlank = True 
				'Commented.  Was This by design ..? Raid 21429
			End If
	         
			If Not rsQuery.EOF Then 
				Set fdfield = rsQuery.fields.item(nGetPropMVOffset(avalues)-1)
				If sIsPropMultiValued(avalues) = True And IsArray(fdField.value)= True And Not IsNull(fdField.value) Then 
					sValue = Join(fdField.value,MV_DELIM)
				Else 
					sValue = fdField.value
				End If
				
				If bMakeBlank Then 
					sValue = Null 
				End If
				' CHECK Handle AD_DB_FILETIME, AD_FILETIME
				Select case fdField.Type
					case AD_DB_DATE,AD_DB_TIMESTAMP,AD_FILETIME,AD_DB_FILETIME
					    If LCase(sGetPropType(avalues)) = "time" Then 
							nType = AD_DB_TIME
						ElseIf sStripGroup(fdField.name)="date_last_changed" or sStripGroup(fdField.name)="date_created" Then
						    nType = AD_DB_TIMESTAMP
						Else
						    nType = AD_DATE
						End If	
					case Else 
						nType =	fdField.Type
				End select
				Call AddFormattedNode(xmlNode, sStripGroup(fdField.name), sValue, nType)
				
				If sGetPropFullName(aValues) = g_sAccountOrgId  And Not isNull(sValue) Then 
					sRef = sGetPropRefString(avalues)
	                nLen = InStr(1, sRef, ".") + 1
                    sProfile = Mid(sRef, nLen, Len(sRef)) 	
                    
                    If sProfile = ORG_PROFILE or sProfile = USER_PROFILE Then 	
						If Len(sValue) > 0 Then  
							Set xmlAuxNode = xmlGetAuxFriendlyDisplay(avalues, fdField.name, sValue)
							If xmlAuxNode.childNodes.Length > 0 Then  _
								xmlNode.appendChild xmlAuxNode
						End If	
					End If
				End If	
            End If
        End If
    Next 
    Set oXMLRS2CopyXML = xmlDoc
    
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub PackPreviousValues(sKey,sValue)
'
'	rsQuery -  recordset
'	nfirst  -  index of first property in aprops array
'	nlast   -  index of last property in aprops array
'	aprops  - properites array of array
'	To store the prev values to compare And make update query.
'
Sub SetPreviousFieldValue(rsQuery, nfirst, nlast, aprops)
	Dim fdField,i,sValue
	
	Dim nPropIndex
    For i = nfirst to nlast
	   If Not rsQuery.EOF Then 
			nPropIndex = nGetPropMVOffset(aprops(i))-1
			If nPropIndex <> -1 Then  
				Set fdField = rsQuery.fields.item(nPropIndex)
				sValue = fdField.value
				If Not isNull(sValue) Then  _
					g_sPrevValue(sStripGroup(fdField.Name)) = sValue
				End If		
		 End If	 
	Next 
	Set Session("PrevValue") = g_sPrevValue
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub PackPreviousValues(sKey,sValue)
'
'	sKey -  string of keys separated by delimiter
'	sValue -  string of values separated by delimiter
'
Sub PackPreviousValues(sKey,sValue)
	Dim item,sSep
	sSep =""
	For each item in g_sPrevValue
		sKey = skey & sSep & item
		sValue = sValue & sSep & g_sPrevValue(item)
		sSep =";"
	Next 
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub UnPackPreviousValues(sKey,sValue)
'
'	sKey -  string of keys separated by delimiter
'	sKey -  string of values separated by delimiter
'
Sub UnPackPreviousValues(sKey,sValue)
	Dim arrKeys,arrValues,i
	sSep = ";"
	arrKeys = Split(sKey,sSep)
	arrValues =Split(sValue,sSep)
	For i = 0 to UBound(arrKeys)
		g_sPrevValue(arrKeys(i)) = arrValues(i)
	Next 
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLMultiList(aprops, nfirst, nlast, sGUIDs, nIDCol)
'
'	aprops -  array of arrays containing properties And attributes
'	nFirst -  integer. index of first property
'	nLast  -   integer. index of last property
'  sGUIDs - list of guids
'  nIDCol - identity column index
'	 should be deprecated
'
 Function oXMLMultiList(aprops, nfirst, nlast, sGUIDs, nIDCol)
    Dim avalues, xmlDoc, xmlNode, fdField, xmlChildNode, bIsNewRecord, sValue, i, icnt, bOrgManage
    bOrgManage = False
    If len(trim(Session("UMOrgRestrict")))> 0 Then  
        bOrgManage = True
    End If

    Set xmlDoc = CreateObject("MSXML.DOMDocument")
    Set xmlNode = xmlDoc.createNode(1,"document","")
    Set xmlDoc.documentElement = xmlNode
    Set xmlNode = xmlDoc.createNode(1, "record", "")
    xmlDoc.documentElement.appendChild xmlNode
    For i = nfirst to nlast
        avalues = aprops(i)
        'Weed out multivalued properties For now
        If sisPropMultiValued(avalues) = False And bIsPropMappedToData(avalues) Then 
            Set xmlChildNode = xmlDoc.createNode(1, sGetPropName(avalues), "")
            If (nIDCol = i) Then 
                xmlChildNode.text = sGUIDs
            End If
            If (bOrgManage And (strcomp(sGetPropName(avalues), XML_ATTR_ORGID, 1) = 0)) Then 
                XMLChildNode.text = Session("UMOrgID")
            End If
            xmlNode.appendChild xmlChildNode
        End If
    Next 
    Set oXMLMultiList = xmlDoc
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLBlankList(aprops, nfirst, nlast)
'
'	aprops -  array of arrays containing properties And attributes
'	nFirst -  integer. index of first property
'	nLast -   integer. index of last property
'	create an editsheet with blank values. If property is Org_Id And 
'  function has been called thru "Manage users" workflow Then  fill the 
'	value of organization ID
 Function oXMLBlankList(aprops, nfirst, nlast)
    Dim avalues, xmlDoc, xmlNode, fdField, xmlChildNode, bIsNewRecord, sValue, i
    Dim bOrgManage
    Set xmlDoc = CreateObject("MSXML.DOMDocument")
    Set xmlNode = xmlDoc.createNode(1,"document","")
    Set xmlDoc.documentElement = xmlNode
    bOrgManage = False
    If len(trim(Session("UMOrgRestrict")))> 0 Then  
        bOrgManage = True
    End If
    Set xmlNode = xmlDoc.createNode(1, "record", "")
    xmlDoc.documentElement.appendChild xmlNode
    For i = nfirst to nlast
        avalues = aprops(i)
        'Weed out multivalued properties For now
        'If sIsPropMultiValued(avalues) = False And bIsPropMappedToData(avalues) Then 
        If bIsPropMappedToData(avalues) Then 
            Set xmlChildNode = xmlDoc.createNode(1, sGetPropName(avalues), "")
            If (bOrgManage And (strcomp(sGetPropName(avalues), XML_ATTR_ORGID, 1) = 0)) Then 
                XMLChildNode.text = Session("UMOrgID")
            End If
            xmlNode.appendChild xmlChildNode
        End If
    Next 
    Set oXMLBlankList = xmlDoc
End Function


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sGetWhereClause(sQuery)
'
'	oXMLProfile -  XMLDocument containing profile
'	returns - sHTML. builds HTML case statements to call from client script
'
Function nGetNumBrowses(ByVal oXMLProfile, ByRef sHTML)
    Dim oXMLRefs, oXMLRef, nCount, sCatName, sRef
    nCount = 0
    sHTML = ""
    
    Set oXMLRefs = oXMLProfile.DocumentElement.SelectNodes(".//" & sXMLTagProperty & _
        "[@" & sXMLAttrReference & " > '' ]")' && @" & sXMLAttrTypeDefname & "$ieq$ 'string']")
    For each oXMLRef in oXMLRefs
        sRef = sGetAttribute(oXMLRef,sXMLAttrReference)	'oXMLRef.GetAttribute(sXMLAttrReference)
        sCatName = sExtractCatalog(sRef)
        If (sCatname <> "Site Terms") And (sCatname <> "SiteTerms") Then  
            nCount = nCount + 1
            sHTML = sHTML & "case """ & lcase(sGetAttribute(oXMLRef,sXMLAttrName)) & _
                """" & vbCR
            sHTML = sHTML & "    call CallExtDialog("""& sRef & """, sVal)" & vbCR
        End If
    Next 
    nGetNumBrowses = nCount
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sGetWhereClause(sQuery)
'
'	sQuery -  string. Provider query 
'	returns - string. Only the where part of the query For bulk operations
'
Function sGetWhereClause(sQuery)
   Dim sWhere, nFound
   Rem Get the where clause from Query
   nFound = InStr(1, sQuery, "WHERE", 1)
   If nFound <> 0 Then 
      nFound = Len(sQuery) - (nFound + 5)
   End If
   sWhere = Right(sQuery, nFound)
   Rem Strip the Order By clause from query
   nFound = InStr(1, sWhere, "ORDER BY", 1)
   If nFound <> 0 Then 
      sWhere = Left(sWhere, nFound - 1)
   End If
   sGetWhereClause = sWhere
End Function

Function oMakeUpdateRS()
	Dim oUpdateRS
	Dim oUserRS
	Dim fld
	Dim oDict
	Dim fdField
	Set oUpdateRS = Server.CreateObject("ADODB.Recordset")
'	Set oUpdateRS = Session("UserUpdateRS")
    
    if IsEmpty( Session("UserRS")) or Not IsObject( Session("UserRS")) then 
        Set oMakeUpdateRS = Nothing
        Exit Function
    end if    

	Set oUserRS = Session("UserRS")
'	oUserRS.MoveFirst 
    
	For each fdField in oUserRS.Fields
		oUpdateRS.Fields.Append fdField.Name ,fdField.Type,fdField.DefinedSize, 32  
	Next 
	
'	oUserRS.MoveFirst
	Set oUserRS = Session("UserRS")
    oUpdateRS.Open
	oUpdateRS.AddNew
	
	For each fdField in oUserRS.Fields
		oUpdateRS.Fields(fdField.Name).Value = fdField.value
	Next     
	Set oMakeUpdateRS = oUpdateRS
End Function

%>
