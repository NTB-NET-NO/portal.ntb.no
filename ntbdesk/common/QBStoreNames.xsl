<xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">

  <xsl:template match="@*">
    <xsl:copy><xsl:value-of /></xsl:copy>
  </xsl:template>

  <xsl:template match="/">
    <xsl:pi name="xml">version="1.0"</xsl:pi>
    <xsl:apply-templates />
  </xsl:template>
  
  <xsl:template match="Document">
    <Document>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Document>
  </xsl:template>

	<xsl:template match="Catalog">
		<Catalog>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</Catalog>
	</xsl:template>

	<xsl:template match="Profile">
		<Profile>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</Profile>
	</xsl:template>

  <xsl:template match="Group">
    <Group>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Group>
  </xsl:template>

  <xsl:template match="Property">
    <Property>
      <xsl:attribute name="dataSource"><xsl:eval>GetDSN</xsl:eval></xsl:attribute>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Property>
  </xsl:template>

  <xsl:script language="VBScript">
    function GetDSN
        dim aNames
        dim sRef1
        On Error Resume Next 
        sRef1 = me.SelectSingleNode("DataRef").getAttribute("idref")
        if Not IsEmpty(sRef1) then 
          aNames = Split(sRef1, ".")
          GetDSN = aNames(0)
        else
          GetDSN = ""
        end if 	
        On Error Goto 0
    end function
  </xsl:script>
  
</xsl:stylesheet>

