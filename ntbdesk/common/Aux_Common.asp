<%

'Makes the select box when this page is used to select a user or organization
Function rsMakeUserOrgList(sOrgID, sRef, dState, oXMLProfile, sSearch, sCurrSel, sFriendly)
   
    dim sProfile, sQuery, oXMLJK, oXMLFriendly, sJK, sFullFriend, oXMLOrgID
    dim sJKShort
    dim sUpperSearch, sLastChar, sWHERE, nlen
    dim cn, rs, SIDVal, sXML, bRet
    dim sOrder
    dim sSortCol
    dim sSortDir
 
    set oXMLJK = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty &_
        "[@" & sXMLAttrKeyDef & "='PRIMARY'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']")
    'sJK = oXMLJK.GetAttribute(sXMLAttrRelationKey)
    'sJK = sGetFullPathName(oXMLJK, True)
    if sRef = REF_USER_PROFILE  then 
		sJK = "[GeneralInfo.user_id]"
	else
	   	sJK = "[GeneralInfo.org_id]"
	end if 
	   	
    sJKShort = oXMLJK.GetAttribute(sXMLAttrName)
    set oXMLFriendly = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty &_
        "[@" & sXMLAttrName & "='" & sFriendly & "']")
    sFullFriend = sGetFullPathName(oXMLFriendly, True)
    sProfile = sBracket(Mid(sRef, InStr(1, sRef, ".")+1))
    sQuery = "SELECT " & sJK & ", " & sFullFriend & " FROM " & sProfile
    sSearch = Trim(sSearch)
    sWhere = ""

    nlen = len(sSearch)
    if nlen > 0 then
    
        if sSearch = "*" then
            sWhere = " WHERE " & sFullFriend & " LIKE '%'"
        elseif Instr(1,sSearch,"*") > 0 and nlen > 1 then
            sSearch = trim(replace(sSearch,"*","%"))
            sWhere = " WHERE " & sFullFriend & " LIKE '%" & sSearch & "%' "
        else
            sWhere = " WHERE " & sFullFriend & " LIKE '%" & sSearch & "%' "
        end if
    
    end if
    
    sSortCol = Request.Form("column")
	sSortDir = Request.Form("direction")
	If Not IsEmpty(sSortCol) and len(sSortCol) > 0 then
		sOrder = "ORDER BY " & sSortCol & " " & sSortDir
	Else
		sOrder = "ORDER BY " & sFullFriend & " " & "ASC"
	End If
		
    if sRef = REF_USER_PROFILE then
        set oXMLOrgID = oXMLProfile.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty &_
            "[@" & sXMLAttrName & "='" & XML_ATTR_ORGID & "']")
        if len(sWhere) > 0 then
            sWhere = sWhere & " AND " & sGetFullPathName(oXMLOrgID, True) &_
                " = '" & sOrgID & "'"
        else
            sWhere = " WHERE " & sGetFullPathName(oXMLOrgID, True) &_
                " = '" & sOrgID & "'"
        end if
        set oXMLOrgID = nothing
    end if
    'hack to put ParentDN into query
    if  g_bIsAdInstallation then
		if len(Trim(sWhere)) > 0 then 
			sWhere = sWhere & " AND " & sBracket(g_sParentURL) & " = '" & _
					sGetParentDN(dState) & "'"
		else
			sWhere = " WHERE " & sBracket(g_sParentURL) & " = '" & _
					sGetParentDN(dState) & "'"
		end if			
	end if			
	set cn = cnGetProviderConnection(dState)
	
	sQuery = sQuery & sWhere & sOrder	
	Session("AuxListQuery")= sQuery
	    
    bRet = bExecWithErrDisplay(rs, cn, sQuery, _
										sFormatString(L_RetrieveEntity_ErrorMessage, Array(sProfile)))

    If bRet then
		Set rsMakeUserOrgList = rs
	Else
		Set rsMakeUserOrgList = Nothing
	End If	
    set rs = nothing 
    set cn = nothing
    set oXMLJK = nothing
    set oXMLOrgID = nothing
    set oXMLFriendly = nothing

end Function

%>