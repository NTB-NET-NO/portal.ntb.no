<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->

<%
	
	Dim bizData
	Dim sXML
	Dim sXML2
	Dim oXMLProfileNode
	Dim oXMLCatalog
	Dim xslProf
	Dim oXMLSiteTerm
	Dim bCatalogSets
	'Performance Fix Keep sXML in Application
	'Check for MSCSProfileSchemaChange and if True Go Thru All steps 
	'else Use Application Steps.
 
	if (Session("MSCSSiteTermVer") <> Application("MSCSSiteTermVer"))  or IsEmpty(Session("SITETERMS_XML"))  then 
		Set bizData = dGetBizDataObject()
		set oXMLSiteTerm = bizdata.GetProfile("Site Terms.MSCommerce", CStr(Response.CharSet))
		'set oXMLSiteTerm = bizdata.GetProfile("Site Terms.MSCommerce")
			
		Set xslProf = CreateObject("MSXML.DOMDocument")
		xslProf.load(Server.Mappath("QBSiteTerms.xsl"))
			
		sXML = oXMLSiteTerm.TransformNode(xslProf)
	
		Set oXMLSiteTerm = CreateObject("MSXML.DOMDocument")
		oXMLSiteTerm.loadxml sXML
			
		If IsEmpty(Request.QueryString("LoadCatalogSets")) Then
			bCatalogSets = False
		Else	
			bCatalogSets = Request.QueryString("LoadCatalogSets")
		End If	
	
	
		If bCatalogSets Then
			Set oXMLProfileNode = oXMLSiteTerm.SelectSingleNode(".//Profile") 
			Set oXMLCatalog = oXMLGetCatalogSets()
			Set oXMLCatalog = oXMLCatalog.SelectSingleNode(".//Group")
			oXMLProfileNode.AppendChild oXMLCatalog
		End if
	
		Session("SITETERMS_XML") = oXMLSiteTerm.xml
		sXML2 = oXMLSiteTerm.xml
		
		Session("MSCSSiteTermVer") = Application("MSCSSiteTermVer")
		
	Else
	    sXML2 = Session("SITETERMS_XML")
	End If
		'Response.write oXMLSiteTerm.xml
	'Clean Up	
	Set bizData = Nothing
	Set oXMLSiteTerm = Nothing
	Set xslProf = Nothing
	Set oXMLCatalog = Nothing
	Set oXMLProfileNode = Nothing
	
	Response.write Replace(sXML2,"<?xml version=""1.0""?>", "")

%>

<%
Function dGetBizDataObject()
   Dim obizData
   Dim sBizDataDSN
   On Error Resume Next 	
   Set obizData = Server.CreateObject("Commerce.BusinessDataAdmin")
   If Err.number = 0 Then
        
        sBizDataDSN = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")
		  'sBizDataDSN = g_MSCSConfiguration.Fields(CStr("Biz Data Service")).Value.Fields(CStr("s_DSN")).Value
        If Err.number = 0 Then
            obizData.Connect CStr(sBizDataDSN)
        End If

   End If
   Set dGetBizDataObject = obizData
   On Error GoTo 0

End Function

Function oXMLGetCatalogSets()
	Dim oXMLDoc, oXMLElement
	Dim oXMLProperty
	Dim i
	Dim oXMLChild
	Dim oXMLText
	Dim sValue, sName
	Dim oRS
	set oXMLDoc = Server.CreateObject("MSXML.DOMDocument")
	set oXMLElement = oXMLDoc.CreateElement("Group")
	oXMLElement.SetAttribute "name", "Catalog"
	set oXMLDoc.DocumentElement = oXMLElement
	
	rem To create blank entries
	set oXMLProperty = oXMLDoc.CreateElement("Property")
	oXMLProperty.SetAttribute "value", sValue
	oXMLProperty.SetAttribute "name", "CustomCatalogSet"

	oXMLDoc.DocumentElement.appendChild oXMLProperty
	rem Get the catalogsets. Tables required - catalogset_catalogs and catalogset_info
	Set oRS = rsGetCatalogSets()
	If Not Typename(oRS) = "Nothing" then
		Do While Not oRS.EOF 
		  sName = oRS("catalogSetName").value
		  sValue = oRS("catalogSetId").value
		  set oXMLChild = oXMLDoc.CreateElement("Attribute")
		  oXMLChild.SetAttribute "name", sValue
		  oXMLChild.SetAttribute "displayName", sName
		  oXMLProperty.AppendChild oXMLChild
		  oRs.MoveNext
		Loop
	End If
	Set oXMLGetCatalogSets = oXMLDoc
	
End Function


Function rsGetCatalogSets()
	Dim objCatalogSets
	dim mscsCatalogConnStr,mscsTransactionConfigConnStr	
		
   'Keep this in the Session and refresh with publish profiles 
	On Error Resume Next 'Application("MSCSCatalogSets")

	if Not IsObject(Session("MSCSCatalogSets")) or Session("MSCSCatalogSets")= Nothing  or Session("MSCSCatalogVer") <> Application("MSCSCatalogVer") then
		mscsTransactionConfigConnStr = GetSiteConfigField("Transaction Config", "connstr_db_TransactionConfig")   
		mscsCatalogConnStr = GetSiteConfigField("product catalog", "connstr_db_Catalog")
		set objCatalogSets = Server.CreateObject("Commerce.CatalogSets")
		objCatalogSets.Initialize mscsCatalogConnStr, mscsTransactionConfigConnStr
			
		set rsGetCatalogSets = objCatalogSets.GetCatalogSets
		If Err.number <> 0 then
			Set rsGetCatalogSets = Nothing
		End If
		Set Session("MSCSCatalogSets") = rsGetCatalogSets
		Session("MSCSCatalogVer") = Application("MSCSCatalogVer")
			
		Set objCatalogSets = Nothing
	else
	    Set rsGetCatalogSets = Session("MSCSCatalogSets")
	    rsGetCatalogSets.MoveFirst
	end if     
		
	On Error Goto 0
		
End Function		


%>