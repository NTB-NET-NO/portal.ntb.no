<%
'LOC: This file	contains all common localizable strings for the Users & Organizations module

rem Localizable resource IDs for XMLRoutines.asp
const L_BadConnection_ErrorMessage			= "A connection with the data store could not be established. Please contact your administrator."
const L_NoDisplayColumn_ErrorMessage		= " - Display column is not in Profile, removing..."
const L_SearchProfile_ErrorMessage			= "<br> Error: A search or navigate command in XML returned NULL.<br>"
const L_WrongInvoke_ErrorMessage			= "You have accessed this page incorrectly. It should only be invoked from the user and organization edit pages."
const L_NoPKColumn_ErrorMessage				= "The primary key column could not be found. Exiting."
const L_NoProperty_Errormessage				= " - property could not be found. Exiting."
const L_NoSearchColumns_ErrorMessage		= "No search columns found for this profile."
const L_CatalogNotFound_ErrorMessage		= " - Catalog not found."
const L_ProfileNotFound_ErrorMessage		= " - Profile not found in catalog."
const L_AttributeLoad_ErrorMessage			= "Could not load profile attributes."
const L_GroupPropsNotFound_ErrorMessage	    = "No group properties for this profile found in the catalog."
const L_NoPropsinGroup_ErrorMessage			= "No properties found in group number - " 
const L_XMLLoadFile_ErrorMessage			= "The XML file did not load. "
const L_LoadProfile_ErrorMessage			= "An error occurred while loading the profile. "
const L_LoadSiteTermCatalog_ErrorMessage	= "An error occurred while loading the Site Term catalog. "
const L_UnknownType_ErrorMessage			= "An unknown type was encountered. "
const L_NoUniqueGUID_ErrorMessage			= "The unique identifier for this item, which is supposed to be a GUID, is null or blank. Processing cannot continue. "
const L_SiteDN_ErrorMessage					= "Error Getting SiteDN in sGetBaseURL. Stopping."
const L_BizDataInstantiation_ErrorMessage   = "The BizDataManager object could not be instantiated." 
const L_NoDataStoreConn_ErrorMessage		= "Could not connect to data store for User Profile. Please contact your database administrator." 
const L_WrongGTDirection_Errormessage		= " - > Direction  not supported. "
const L_WrongLTDirection_Errormessage		= " - < Direction not supported. "	
const L_WrongRevDirection_Errormessage		= " - Rev Direction not supported. "	
const L_UserProfileKeyMisplaced_Errormessage= "The keys [User ID] and [Logon Name] of the [UserObject] Profile has changed. Please restore or you will no longer be able to edit users."
const L_OrgProfileKeyMisplaced_Errormessage = "The Keys [Organization ID] and [Name] of the [Organization] Profile has changed. Please restore or you will no longer be able to edit organizations."
const L_InvalidDateTime_Text                = "Invalid value for DateTime "

Rem Status Messages
const L_CountDeleted_Text					= "%1 %2s - Deleted" 
const L_EntityDeleted_Text					= "%1 %2 - Deleted"
const L_AllDeleted_Text						= "%1 - All Selections Deleted"

Const L_Required_Text						= "All required fields need to be filled."
Const L_Valid_Text							= "All fields should have valid data."
const L_Search_Text							= "Find Now"
const L_SubmitChanges_Text					= "Submit Changes"
const L_SelectItem_Text						= "OK"
const L_Cancel_Text							= "Cancel"


const L_Add_StatusBar						= "%1 %2 added"
const L_CouldNotAdd_StatusBar				= "%1 '%2' could not be added"
const L_CouldNotSave_StatusBar				= "changes to %1 '%2' could not be saved"
const L_Save_StatusBar						= "changes to %1 '%2' saved"
const L_SaveAll_StatusBar					= "changes to all selected %1s saved"
const L_New_Text							= "New %1"
const L_Open_Text							= "%1: %2"
const L_OpenMultiple_Text					= "%1 %2s:"
const L_Copy_Text							= "New %1: Copy %2 properties"
const L_UserEntity_Text						= "User"
const L_OrgEntity_Text						= "Organization"
'Fix grammar issues in other languages :Orgs
const L_OpenOrganizations_Text              = "Update Organizations: %1 Organizations"
const L_OpenOrganization_Text               = "Update Organization: %1 Organization"
const L_OpenAllOrganizations_Text           = "Update All Organizations"

const L_OpenUsers_Text			            = "Update Users: %1 Users"
const L_OpenUser_Text                       = "Update User: %1 User"
const L_OpenAllUsers_Text                   = "Update All Users"

const L_NewUser_Text			            = "New User"
const L_NewOrg_Text                         = "New Organization"

const L_CopyOrg_Text						= "New Organization: Copy %1"
const L_CopyUser_Text						= "New User: Copy %1"

const L_OrgsCountDeleted_Text				= "Organizations Deleted: %1 Organizations"
const L_UsersCountDeleted_Text				= "Users Deleted: %1 Users"
const L_OrgEntityDeleted_Text				= "Organization Deleted: %1 Organization"
const L_UserEntityDeleted_Text				= "User Deleted: %1 User"
'Fix grammar issues in other languages :Orgs

const L_LoadingQB_Text						= "Loading the Query Builder..."
const L_CurrentQuery_Text					= "Current Query XML:"
const L_LoadingList_Text					= "Loading list, please wait......"
const L_LoadingProperties_Text				= "Loading properties, please wait..."
const L_Loading_Text						= "Loading..."
const L_LoadingPropGroups_Text				= "Loading Property Groups..."

const L_ConstructSearchQuery_Text			= "You must construct a search query before submitting."
const L_ConstructUpdateQuery_Text			= "You must construct an update query before submitting."
const L_EnterRequiredField_Text				= "You have to fill out the following required field(s) before saving: %1"
const L_EnterValidFieldData_Text            = "The value '%1' is invalid. Please update before saving."
const L_EnterValidField_Text                = "This value is invalid. Please update before saving."

const L_QueryBuilder_ErrorMessage			= "Query Builder is not done activating. Please select another option"
const L_UsersListSelect_ErrorMessage        = "Please Select a User List"
const L_EnterFindOption_Text				= "Please enter a value to search and submit again."
const L_SelectFindOption_Text				= "Please select a search option and try again."

const L_NoMorePages_ErrorMessage			= "There are no more pages to display."
const L_NoPage_ErrorMessage					= "Cannot move to the requested page. There are no records on that page."

const L_AuxDisplay_ErrorMessage				= "Failed to read auxiliary profile for friendly value."
const L_InsertEntity_ErrorMessage			= "Failed to insert %1."
const L_DeleteEntity_ErrorMessage			= "Failed to delete %1."
const L_RetrieveEntity_ErrorMessage			= "Failed to retrieve %1(s)."
const L_CreateGroup_ErrorMessage			= "Failed to create group %1."
const L_UpdateEntity_ErrorMessage			= "Update %1 failed."
const L_BulkUpdateEntity_ErrorMessage		= "Bulk update of %1s failed."
const L_UpdateNEntity_ErrorMessage			= "Update of %1 %2s failed."
const L_OrgUserCheck_ErrorMessage			= "Failed to check if organization contained users."
const L_Duplicate_ErrorMessage				= "Entry with this name already exists. Retry with a different name."
const L_Connection_ErrorMessage				= "A connection to the provider could not be established.  Contact your administrator."
const L_Update_Text							= "Update "

const L_PROFILE_DialogTitle					= "%1 Error"
const L_UseWildCardSearch_Text              = "<b>Wildcard(*)</b> Search is Enabled"
const L_SearchCategoryLabel_Text			= "Find by:"
const L_FirstNameInput_Text					= "First Name:"
const L_LastNameInput_Text					= "Last Name:"
const L_FindButton_Text						= "Find Now"
const L_FindLabel_Text						= "Find by"
const L_ListUserTableHeaderLastName_Text	= "Last" 'ListView Table header for Last Name column
const L_ListUserTableHeaderFirstName_Text	= "First" 'ListView Table header for First Name column
const L_ListUserTableHeaderCompany_Text		= "Company" 'ListView Table header for Company column
const L_ListUserTableHeaderEmail_Text		= "Email" 'ListView Table header for Company column
const L_Save_Button							= "Save"
const L_UserLists_Text						= "User lists"
const L_UserListName_Text                   = "List Name"
const L_FindProperty_Text					= "Find by property"

const L_TextPK_ErrorMessage					= "Validation error in text (cannot contain any of / "" # [ ] : | < > + = ; , ? * ( \ ). " 
const L_Text_ErrorMessage					= "A text field cannot contain any of the following characters: ""&'" 
const L_Num_ErrorMessage					= "An integer can only be comprised of the digits 0-9."
const L_Date_ErrorMessage					= "This field should contain a valid date." 
const L_Time_ErrorMessage					= "This field should contain a valid time." 
const L_Curr_ErrorMessage					= "This field should contain a valid currency." 
const L_TextPwd_ErrorMessage				= "Password field should contain 7 to 14 alpha/numeric characters." 
const L_UnsupportedFeature_ErrorMessage     = "Search/Update tokens spans Multiple Data Stores.This is not supported.Please refer to help."
const L_Date_Tooltip						= "This field should contain a valid date and/or time." 
const L_Num_Tooltip							= "This field should contain a valid integer." 
const L_Time_Tooltip						= "This field should contain a valid time." 
const L_Curr_Tooltip						= "This field should contain a valid currency."
const L_Text_Tooltip						= "This field should contain a valid string." 
const L_TextPK_Tooltip						= "This is a key field and should contain a valid string." 
const L_ChangedBy_Tooltip					= "Last changed or updated by"
const L_TextPwd_Tooltip						= "Password must contain at least 7 alpha/numeric characters." 
const L_Ref_Tooltip							= "This field is a reference to another profile. Browse to select a value." 
const L_SiteTerm_Tooltip					= "This field contains a siteterm. Pick a value from the dropdown." 
const L_Abort_HTMLTitle						= "Critical Error! Operation Aborted."
const L_InvalidOrgName_ErrorMessage			= "Invalid Organization ID. This organization does not exist."

'User/Org Dialog Messages
const L_UserOrgLogonName_Text				= "Logon name"
const L_UserOrgName_Text					= "Name"
const L_SaveUserFirst_Text					= "Save the User First" 
const L_RefreshConnection_Text              = "To Refresh Data Store Connections click on the [Publish Profiles] link."


%>