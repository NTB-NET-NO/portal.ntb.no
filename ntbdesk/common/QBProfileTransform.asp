<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->

<%

	Dim bizData
	Dim xmlObj
	Dim sXML1
	Dim sXML2 
	Dim bRefresh
	Dim currProfile, currCatalog
	Dim xmlProfile, xmlCatalog
	Dim xslProf
	Dim rgPath
	Dim xmlNode, listNodes
	Dim dictDataSources
	Dim sName, sDisplayName
	
	const REF_USER_PROFILE = "Profile Definitions.UserObject"
    const REF_ORG_PROFILE = "Profile Definitions.Organization"

	bRefresh = False
	'Performance Fix Keep sXML in Application
	'Check for MSCSProfileSchemaChange and if True Go Thru All steps 
	'else Use Application Steps.
	
	currProfile = Request.QueryString("CURR_PROFILE")
	'currProfile = "Profile Definitions.UserObject"
	if currProfile = REF_USER_PROFILE  and IsEmpty(Session("USER_XML"))  then
		bRefresh = True
	elseif currProfile = REF_ORG_PROFILE  and IsEmpty(Session("ORG_XML"))  then
	   	bRefresh = True
	end if   			

	if (Session("MSCSUserXmlVer") <> Application("MSCSUserXmlVer")) or bRefresh then 
		
		Set bizData = dGetBizDataObject()
		Set xmlProfile = bizdata.GetProfile(CStr(currProfile), CStr(Response.CharSet))
	    'set xmlProfile = bizdata.GetProfile(CStr(currProfile))

    ' Convert the XML to add in data source names		
		Set xslProf = CreateObject("MSXML.DOMDocument")
		xslProf.load(Server.MapPath("QBStoreNames.xsl"))
		Set xmlObj = CreateObject("MSXML.DOMDocument")
		xmlObj.loadXML xmlProfile.transformNode(xslProf)
				
		' Now convert the data source names to display names
    Set dictDataSources = CreateObject("Scripting.Dictionary")
		rgPath = Split(currProfile, ".")
		currCatalog = rgPath(0)
		Set xmlCatalog = bizdata.GetCatalogs()
		Set listNodes = xmlCatalog.selectNodes("//Catalog[@name='" & currCatalog & "']/DataSource")
		For Each xmlNode In listNodes
		  ' Add the name/display name pair into a dictionary
		  dictDataSources.Add xmlNode.getAttribute("name"), _
		    xmlNode.getAttribute("displayName") 
		Next		
		Set listNodes = xmlObj.selectNodes("//Property[@dataSource]")
    For Each xmlNode In listNodes
      ' Replace the name with a display name from the dictionary
      xmlNode.setAttribute "dataSource", _
        dictDataSources.Item(xmlNode.getAttribute("dataSource"))      
    Next
	
    ' Convert the XML again to concatenate the display names
		xslProf.load(Server.MapPath("QBStoreNames2.xsl"))
		sXML1 = xmlObj.transformNode(xslProf)
	
		Set xmlObj = CreateObject("MSXML.DOMDocument")

		'------------------------------------------------
		'	Dim oDocRoot ,gc_sDocRoot ,gc_sProfileURI , oPI
		'	gc_sProfileURI = "schemas-microsoft-com:bizdata-profile-schema"
		'	gc_sDocRoot = "Document"
		'	Set oPI = xmlObj.createProcessingInstruction("xml", _
		'             " version=""1.0"" encoding=""" & Response.CharSet & """")
		'    xmlObj.appendChild oPI
    
		'	Set oDocRoot = xmlObj.createNode(1, gc_sDocRoot, gc_sProfileURI)
		'   xmlObj.appendChild oDocRoot
		'------------------------------------------------
	
		xmlObj.loadXML sXML1
		Set xslProf = Nothing
		Set xslProf = CreateObject("MSXML.DOMDocument")
		xslProf.load(Server.MapPath("QBProfiles.xsl"))
		sXML2 = xmlObj.transformNode(xslProf)
		if currProfile = REF_ORG_PROFILE then 
			Session("ORG_XML") = sXML2
		else
		    Session("USER_XML") = sXML2
		end if    	
		
	'	Session("MSCSUserXmlVer") = Application("MSCSUserXmlVer")
				
	Else
	    if currProfile = REF_ORG_PROFILE then 
			 sXML2= Session("ORG_XML") 
		else
		     sXML2= Session("USER_XML") 
		end if    	
	End If

	'Cleanup 
	Set xslProf = Nothing
	Set xmlObj = Nothing
	Set bizData = Nothing
	Set xmlProfile = Nothing
    'Response.Write sXML	
	Response.write Replace(sXML2,"<?xml version=""1.0""?>", "")

%>

<%

Function dGetBizDataObject()
	Dim obizData
	Dim sBizDataDSN
	On Error Resume Next
    Set obizData = Server.CreateObject("Commerce.BusinessDataAdmin")
    If Err.number = 0 Then
        
        sBizDataDSN = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")
		  'sBizDataDSN = g_MSCSConfiguration.Fields(CStr("Biz Data Service")).Value.Fields(CStr("s_DSN")).Value
        If Err.number = 0 Then
            obizData.Connect CStr(sBizDataDSN)
        End If

    End If
    Set dGetBizDataObject = obizData
    On Error Goto 0

End Function

%>