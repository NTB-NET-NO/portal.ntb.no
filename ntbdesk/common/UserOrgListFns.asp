<%
'Functions and routines used in User and Org List pages


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sub RS2XMLPage(rsQuery, dState)
'	
'	rsQuery - recordset
'	dState - state Dictionary
'	Routine for turning recordset into XML for list sheet. All fields in recordset are  
'	assumed to be valid columns in the listsheet (no checking done).
'	This particular routine uses the g_iListPageSize global variable to determine
'	the maximum number of records in the record set to put on
'	the page. If it hits EOF, it exits gracefully.
'	Routine uses response.write to write out XML at end.
'
Sub RS2XMLPage(rsQuery, dState)
	Dim xmlDoc, xmlNode, fdField, xmlChildNode, bIsNewRecord, sValue, iRecCount
	Dim aIDCol, sSortCol
	Set xmlDoc = CreateObject("MSXML.DOMDocument")
	Set xmlNode = xmlDoc.createNode(1,"document","")
	Set xmlDoc.documentElement = xmlNode
    
	bIsNewRecord = False
	If rsQuery.EOF Then
		bIsNewRecord = True
	Else
		bIsNewRecord = False
	End If
	If bIsNewRecord Then
		Set xmlNode = xmlDoc.createNode(1, "record", "")
		xmlDoc.documentElement.appendChild xmlNode
		For Each fdField In rsQuery.fields
			Set xmlChildNode = xmlDoc.createNode(1, sStripGroup(fdField.name), "")
			xmlNode.appendChild xmlChildNode
		Next
	Else
        iRecCount = g_iListPageSize
'	    iRecCount = rsQuery.PageSize
		 If (iRecCount >= 1) Then    
		'rsQuery.Save "c:\foo.xml", AD_PERSIST_XML
		    sSortCol = dState.Value("SortColumn")
		    aIDCol = Session(g_sIDColLabel)
		    
		    If Not IsNull(aIDCol) then
			    dState.Value("PageFirstGUID") = rsQuery(aIDCol(1))
			End If
			If Not IsNull(sSortCol) then   
				dState.Value("PageFirstValue")= rsQuery(sSortCol)
			End If   
            '****Need to do something about nulls
		    Do Until (rsQuery.EOF OR (0 = iRecCount)) 
		      If Not (rsQuery(1).Value = Application("MSCSSiteName")) Then
		    		Set xmlNode = xmlDoc.createNode(1, "record", "")
		    		xmlDoc.documentElement.appendChild xmlNode
		    		For Each fdField in rsQuery.fields
		    			Set xmlChildNode = xmlDoc.createNode(1, sStripGroup(fdField.name), "")
		    			sValue = fdField.value
		    			If Not isNull(sValue) Then
		    				xmlChildNode.text = sValue
		    			End If
		    			xmlNode.appendChild xmlChildNode
		    		Next
					iRecCount = iRecCount - 1
				End If      
		    	rsQuery.moveNext
		    Loop
		    'if only sitename exists in the recordset.
		    If iRecCount = g_iListPageSize Then 
					Set xmlNode = xmlDoc.createNode(1, "record", "")
					xmlDoc.documentElement.appendChild xmlNode
					For Each fdField In rsQuery.fields
						Set xmlChildNode = xmlDoc.createNode(1, sStripGroup(fdField.name), "")
						xmlNode.appendChild xmlChildNode
					Next
				End If
		Else
		    Do Until rsQuery.EOF
				If Not (rsQuery(1).Value = Application("MSCSSiteName")) Then
		    		Set xmlNode = xmlDoc.createNode(1, "record", "")
		    		xmlDoc.documentElement.appendChild xmlNode
		    		For Each fdField In rsQuery.fields
		    			Set xmlChildNode = xmlDoc.createNode(1, sStripGroup(fdField.name), "")
		    			sValue = fdField.value
		    			If Not isNull(sValue) Then
		    				xmlChildNode.text = sValue
		    			End If
		    			xmlNode.appendChild xmlChildNode
		    		Next
		    	End If
		    	rsQuery.moveNext
		    Loop
		End If
	End If

	response.write xmlDoc.xml

	Set xmlNode = Nothing
	Set xmlChildNode = Nothing
	Set xmlDoc = Nothing
End Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub WriteXMLMeta2(aprops, byVal dState, nRecs)
'	
'	aprops - array of arrays containing properties and attributes
'	dState - state Dictionary
'	nRecs  - records displayed
'	
'	create and write out the Meta XML for list sheet. Uses oXMLGetDisplayColumns 
'	function to determine which columns in profile are displayed in the list sheet.
'
Sub WriteXMLMeta2(aprops, byVal dState, nRecs)
	Dim sXML, oXMLcolumns, oXMLcol, stmp, ikeyvalue, nPageSize, iCurPage, sSortCol, sSortDir
	Dim oXML, oXMLEl, oXMLRec, oXMLCols, aIDCol, nColNo
	Dim oXMLOPs , oXMLOP
	aIDCOl = Session(g_sIDColLabel)
	Set oXML = Server.CreateObject("MSXML.DOMDocument")
			
	nPageSize = g_iListPageSize
	sSortCol = dState.Value("SortColumn")
	sSortDir = lcase(dState.Value("SortDirection"))
	Set oXMLcolumns = oXMLGetDisplayColumns   
	'Reads XML in columns.xml to determine what to display. Assumes first column is the identity
	'column for the table and that it is hidden. Also doesn't have a where clause at the moment. 
	sXML = "<xml id='profilesMeta'>" & vbCR
	Set oXML = oXMLMakeDocumentElement("listsheet")
			
	Set oXMLEL = oXML.CreateElement("global")
	oXMLEl.SetAttribute "curpage", iCurPage
	oXMLEl.SetAttribute "pagesize", nPageSize
	oXMLEl.SetAttribute "sort", "yes"
	oXMLEl.SetAttribute "selection", "multi"
	oXMLEl.SetAttribute "selectionbuttons", "yes"
	oXML.DocumentElement.appendChild oXMLEl
	If oxmLcolumns.length = 2 Then oXMLEl.SetAttribute "sort", "no"
			
	Set oXMLCols = oXML.CreateElement("columns")
	oXML.DocumentElement.appendchild oXMLCols
	If (nRecs > 1) Then
		oXMLEl.SetAttribute "recordcount", nrecs
	End If

	Dim nSize
	For Each oXMLcol In oxmLcolumns
	    stmp = oXMLcol.attributes.GetNamedItem(sXMLAttrName).value
	    nSize = oXMLcol.attributes.GetNamedItem("size").value
	    nColNo = nGetMatchingEntry(sTmp, aprops)
	    Set oXMLEl = oXML.CreateElement("column")
	    oXMLEl.SetAttribute "id", stmp
	    oXMLEl.SetAttribute "width", nSize
	   'If this column isn't active, then hide it
	    If False = bIsPropActive(aprops(nColNo)) Then
			oXMLEl.SetAttribute "hide", "yes"
	    End If
	    If (strcomp(stmp, sGetIDName(aIDCol))= 0) Then
	  	  oXMLEl.SetAttribute "key", "yes"
	    Else
	       If (strcomp(stmp, sSortCol, 1) = 0) Then
	  		 	oXMLEl.SetAttribute "sortdir", sSortDir
	       End If
	  End If
		oXMLEl.text = oXMLcol.attributes.GetNamedItem(sXMLAttrDisplayName).value
		oXMLCols.appendChild oXMLEl
	Next
			 
	Set oXMLOPs = oXML.CreateElement("operations")
	oXML.DocumentElement.appendchild oXMLOPs
	Dim arrOperations, arrForms, i
	arrOperations = Array("newpage", "sort", "search")
	arrForms = Array("newpage", "search", "search")
	For i=0 to Ubound(arrOperations)
		Set oXMLOP = oXML.CreateElement(arrOperations(i))
		oXMLOP.SetAttribute "formid", arrForms(i) & "form"
		oXMLOPs.appendchild oXMLOP
	Next
				
	Response.Write sXML & oXML.Documentelement.xml & "</xml>" & vbCR
End Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sGetSearchXML(oXMLProfileDoc, oXMLSearchColumns, dState)
'	
'	oXMLProfileDoc - XMLDocument containing the Profile
'	oXMLSearchColumns -  XMLDocument with all the searchable properties in the 
'							Profile XML to build the findby dropdown and associated DIV
'	dState - state Dictionary
'	
'  build the metadata for the findby area. gets all the options from the 
'	searchable properties XML. Add a UserList option if profile is user
'
Function sGetSearchXML(oXMLProfileDoc, oXMLSearchColumns, dState)
    Dim sHTML, sSearchString, sColName, oXMLCol, sMatch, sPropName
    Dim sHTML1, sHTML2, sHTMLData
    Dim sType, bIsMultiValued, bIsPassword
    Dim sRef,sMultiVal
    Dim sCatName, bIsSiteTerm
        
    bIsPassword = False    
    bIsMultiValued = False
    
    sSearchString = "//" & sXMLTagPROPERTY & "[@" & sXMLAttrIsSearchable & "!=0]"
    sHTML = ""
    Set oXMLSearchColumns = oXMLProfileDoc.DocumentElement.SelectNodes(sSearchString)
    'Response.Write oXMLSearchColumns.xml
	Call ValidNode(oXMLSearchColumns, L_NoSearchColumns_ErrorMessage)
	sMatch = trim(g_sFindby)

	If (len(sMatch) = 0) Then
	    sMatch = "Name"
	End If
	sHTML1 = "<select id='selfindby' onchange='DisplayGroup()'>" & vbCR
	sHTML1 = SHTML1 & "<select id='selfindby'>"
	sHTMLData = "<xml id='fbData'><document><record>"
	sHTMLData = sHTMLData & "<selfindby>" & g_sFindBy & "</selfindby>"
	
	For Each oXMLCol In oXMLSearchColumns
	
		sColName = oXMLCol.attributes.GetNamedItem("displayName").value
		sPropName = oXMLCol.attributes.GetNamedItem(sXMLAttrName).value
		sType = oXMLCol.attributes.GetNamedItem(sXMLAttrTypeDefName).value
        
        If Not oXMLCol.attributes.GetNamedItem("isMultiValued") Is Nothing then
			sMultiVal = oXMLCol.attributes.GetNamedItem("isMultiValued").Value
			If sMultiVal = "1" Then 
				bIsMultiValued = True
			Else
				bIsMultiValued = False
			End If	
		End If	
		
		If LCase(sType)= "password" Then
			bIsPassword = True 
        End If		 
		If Not oXMLCol.attributes.GetNamedItem(sXMLAttrReference) Is Nothing then
			sRef = oXMLCol.attributes.GetNamedItem(sXMLAttrReference).value
		Else
			sRef = ""
		End If	
		sCatName = sExtractCatalog(sRef)
		bIsSiteTerm = False
		If (strcomp(sType, "SITETERM", 1) = 0) Then
			bIsSiteTerm = True
		End IF
		If (strcomp(sCatName, "SiteTerms", 1) = 0) Or (strcomp(sCatName, "Site Terms", 1) = 0) Then
			bIsSiteTerm = True
		End If
		
		If Not bIsPassword and Not bIsMultiValued Then
			sHTML1 = SHTML1 & "<option value='" & sPropName & "'>" & sColName & "</option>" & vbCR
			If bIsSiteTerm Then
				sHTML2 = sHTML2 & "<select id = 'fb" & sPropName & "' >" & vbCR
				sHTML2 = sHTML2 & oXMLGetSiteTerms(sRef, sPropName).xml & "</select>" & vbCR
			Else
		   
				Select Case LCase(sType)
			   		Case "number", "long"
						sHTML2 = sHTML2 & "<numeric id = 'fb" & sPropName & "' subtype='integer'><format>" & g_sMSCSNumberFormat & "</format><error>" & L_EnterValidField_Text & "</error></numeric>" & vbCR
					Case "float"
						sHTML2 = sHTML2 & "<numeric id = 'fb" & sPropName & "' subtype='float'><format>" & g_sMSCSNumberFormat & "</format><error>" & L_EnterValidField_Text & "</error></numeric>" & vbCR
					Case "currency"
						sHTML2 = sHTML2 & "<numeric id = 'fb" & sPropName & "' subtype='float'><format>" & g_sMSCSCurrencyFormat & "</format><error>" & L_EnterValidField_Text & "</error></numeric>" & vbCR							
					Case "datetime" , "date"
						sHTML2 = sHTML2 & "<date id = 'fb" & sPropName & "' firstday='" & g_sMSCSWeekStartDay & "'><format>" & g_sMSCSDateFormat & "</format><error>" & L_EnterValidField_Text & "</error></date>" & vbCR
					Case "time"
						sHTML2 = sHTML2 & "<time id = 'fb" & sPropName & "' ><error>" & L_EnterValidField_Text & "</error></time>" & vbCR
					Case Else
						sHTML2 = sHTML2 & "<text id = 'fb" & sPropName & "' subtype='short' maxlen='70'><error>" & L_EnterValidField_Text & "</error></text>" & vbCR
				End Select
			End If	
			If sPropName = g_sFindBy Then
				sHTMLData = sHTMLData & "<fb" & sPropName & ">" & g_sFindByText & "</fb" & sPropName & ">" & vbCR
			Else
				sHTMLData = sHTMLData & "<fb" & sPropName & "></fb" & sPropName & ">" & vbCR
			End If		
		End If		
		bIsMultiValued = False
		bIsPassword = False
	Next
	sHTML1 = SHTML1 & "<option value='QueryBldr'>" & L_FindProperty_Text & "</option>" & vbCR
	If g_sUMState = "User" Then
		sHTML1 = SHTML1 & "<option value='Lists'>" & L_UserLists_Text & "</option>" & vbCR
	End If
	sHTML1 = sHTML1 & "</select>" & vbCR & "</select>" 
	sHTMLData = sHTMLData & "</record></document></xml>"
	sHTML = "<xml id='fbMeta'>" & vbCR
	sHTML = sHTML & "<fields>" & vbCR
	sHTML = sHTML & sHTML1 & vbCR
	sHTML = sHTML & sHTML2 & vbCR & "</fields>" & vbCR & "</xml>" & vbCR & sHTMLData 
	sGetSearchXML = sHTML
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sPrintSearchForms(oXMLSearchColumns)
'
'	oXMLSearchColumns -  XMLDocument with all the searchable properties in the Profile
'	XML to build the findby dropdown and associated DIV
'
Function sPrintSearchForms(oXMLSearchColumns)
    Dim oXMLCol, sColName, sHTML, icnt, sform, sinputname, sSelf, sDisplay
    Dim sFindByText
    sHTML = ""
    sSelf = Request.ServerVariables("SCRIPT_NAME")
    icnt = 0
    rem sHTML = "<TABLE>" & vbCR
    For each oXMLCol in oXMLSearchColumns
        icnt = icnt + 1
        sform = "form" & icnt
        sColName = oXMLCol.attributes.GetNamedItem(sXMLAttrName).value
        sDisplay = oXMLCol.attributes.GetNamedItem("displayName").value
        sInputName = "fb" & sColName & "Label"
        sHTML = sHTML & "<DIV ID='divfb" & sColName & "' STYLE='DISPLAY: none' >" & vbCR
        sHTML = sHTML & "<TABLE>" & vbCR
        sHTML = sHTML & "<TR>" & vbCR & "<TD WIDTH='100'>" & vbCR
        sHTML = sHTML & "<SPAN ID='" & sInputName & "' TITLE='" & sDisplay & "'>" & sDisplay & ":</SPAN>" & vbCR
        'sHTML = sHTML & sDisplay & ":" & vbCR
        sHTML = sHTML & "</TD>" & vbCR
        sHTML = sHTML & "<TD WIDTH='200'>" & vbCR
        sHTML = SHTML & "<DIV ID='fb" & sColName & "' CLASS='editField' METAXML='fbMeta' DATAXML='fbData' >"
        sHTML = sHTML & vbCR & "<%= L_LoadingList_Text %" & "></DIV>" & vbCR
        sHTML = sHTML & "</TD>" & vbCR & "</TR>" & vbCR
        sHTML = sHTML & "</TABLE>" & vbCR
        sHTML = sHTML & "</DIV>"
    Next
    rem sHTML = "</TABLE>"
    sPrintSearchForms = sHTML
End Function

'Empty Routine called in DetermineAction.
Sub NoChange(dState, aprops)
end Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub NewQuery(dState, aprops)
'
'	dState -  Dictionary. contains the state info
'  aprops -  array of array of property and attributes
'	To form a query, need to have the following:
'	1) The SELECT Columns and Table (form this string once per session and save in Dictionary)
'	2) The Main Where clause (WHERE X LIKE Y)
'	3) The Order BY or sorting clause
'	4) The Sort direction (i.e., ASC or DESC)
Sub NewQuery(dState, aprops)
    Dim sSQL, sSELECT, sWHERE, sORDER 
    If bIsDictEmpty(dState.Value("SELECTCols")) Then
'Need to form this string if not stored in dictionary already
        sSELECT = sFormSelectClause(dState,aprops)
        dState.Value("SELECTCols") = sSELECT
    Else
        sSELECT = dState.Value("SELECTCols")
    End If
    If (bIsDictEmpty(dState.Value("WHEREClause")) OR dState.Value("ChangeSearch")) Then
        sWHERE = sFormWhereClause(dState)
        dState.Value("WHEREClause") = sWHERE
    Else
        sWHERE = dState.Value("WHEREClause")
    End If
    If (bIsDictEmpty(dState.Value("ORDERClause")) OR dState.Value("ChangeOrder")) Then
        sORDER = sFormOrderClause(dState)
        dState.Value("ORDERClause") = sORDER
    Else
        sORDER = dState.Value("ORDERClause")
    End If
    dState.Value("Query") = sSELECT & sWHERE & sORDER
	'Reset some variables    
    dState.Value("ChangeOrder") = False        
    dState.Value("ChangeSearch") = False
	 'When we change the query, we are exiting List mode (if we were in it)
    dState.Value("GUIDCount")  = 0
    dState.Value("GUIDListString") = ""
    If NOT bIsDictEmpty(dState.Value("GUIDList")) Then
        Set dState.Value("GUIDList") = Nothing
    End If
    dState.Value("GUIDPointer") = 0
    dState.Value("UMListMode") = False
End Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sGetWhereClause(sQuery)
'
'	sQuery -  string. Provider query 
'	returns - string. Only the where part of the query for bulk operations
'
Function sGetWhereClause(sQuery)
  Dim sWhere, nFound
  Rem Get the where clause from Query
  nFound = InStr(1, sQuery, "WHERE", 1)
  If nFound <> 0 Then
    nFound = Len(sQuery) - (nFound + 5)
  End If
  sWhere = Right(sQuery, nFound)
  Rem Strip the Order By clause from query
  nFound = InStr(1, sWhere, "ORDER BY", 1)
  If nFound <> 0 Then
    sWhere = Left(sWhere, nFound - 1)
  End If
  sGetWhereClause = sWhere
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function ProcessForm(dState)
'
'	dState -  Dictionary
'
Sub ProcessForm(dState)
	Dim dSelect, aUserIds , aNames ,i,  nitems
	Dim sOrderBy, sOrderDir, sFindBy
   
	If Strcomp(Request.Form("type"), "delete", 1) = 0 then
		dState.Value("action") = "delete"
	ELse
		dState.Value("action") = ""
	End If
		
	'Unpack GUIDs of selected organizations, put into dictionary for safe keeping on server
	Set dSelect = dState("Selected")
	nitems = trim(Request.Form("numitems"))
	If len(nitems) > 0 Then
	  If nitems = 1 Then
	    dSelect.Value(Request.Form(Session(g_sIDColLabel)(1))) = Request.Form("friendlyname")
	  ElseIf nitems > 1 Then
	    aUserIDs = split(Request.Form(Session(g_sIDColLabel)(1)),";")
	    aNames = split(Request.Form("friendlyname"), ";")
	    For i = LBound(aUserIDs) to UBound(aUserIDs)
	        dSelect.Value(aUserIDs(i)) = aNames(i)
	    Next
	  End If
	End If

End Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub DetermineAction(dState, aprops)
'
'	dState -  Dictionary. contains the state info
'  aprops -  array of array of property and attributes
'	
'	checks Request.Form and Sessionfor changes in state
'	Basically an Action Handler.
'	Need to see if
'	1) List of user GUIDs passed in
'	2) Query Change
'   a) FindBy Column
'   b) FindBy Text
'   c) Sort Column
'   d) Sort Direction
'	4) Delete User
'
Sub DetermineAction(dState, aprops)
'Routine that checks Request.Form and Sessionfor changes in state
   Dim aGUIDList, nGUIDs, dGUID, i, sAction, sGUIDList, sListID, sdropQuery
   Dim LM, sTempName, cn
   dState.Value("NoChange") = False
   
   If (len(trim(Request.Form("UMGUIDList"))) > 0) Then
		Call MakeTempListTable(dState)
   Else
		If (len(trim(Request.Form("OMUserList")))>0) Then
			Session("UMOrgRestrict") = "Yes"
			Session("UMOrgID") = Request.Form("OrgID")
			Session("UMOrgName") = Request.Form("OrgName")
			dState.Value("Action") = "query"
			dState.Value("FindBy") = "logon_name"
			dState.Value("FindByText") = "*"
			dState.Value("ChangeSearch") = TRUE
		End If
      sAction = dState.Value("Action")
      Select Case LCase(sAction)
			Case "delete"
			   Call DeleteUsers(dState, aprops)
			Case "query"
			   Call NewQuery(dState, aprops)
			Case Else
			 'Redirect from the EditPage or unanticipated request
		    dState.Value("NoChange") = True
		    Call NoChange(dState, aprops)
      End Select
    End If
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub MakeTempListTable(dState)
'
'	dState -  Dictionary. contains the state info
'	
'	Creates the a temporary table in database with the list of users exported 
'	from the list manager list. This table is then read to create the query
'
Sub MakeTempListTable(dState)
	Dim sTempName
	Dim sDropQuery
	Dim cn
	Dim sListID
	Dim LM
	
	dState.Value("UMListMode") = True
	sTempName = sGetTempTableName
	' drop temp table by this name if it already exists
	sDropQuery = "if exists (select * from sysobjects where id = object_id(N'[dbo].[" & sTempName & "]')"
	sDropQuery = sDropQuery & "and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].["
	sDropQuery = sDropQuery  & sTempName & " ] GO"
	
	Set cn = Server.CreateObject("ADODB.Connection")
	On Error Resume Next
	'Open connection to database to store temporary table
	cn.Open(sGetTempDSN)
	If Err.number <> 0 Then
		Response.Write L_BadConnection_ErrorMessage
	End If
	Err.Clear
	cn.Execute(sDropQuery)
	cn.Close
	Set cn = Nothing
	On error Goto 0
	If Not IsEmpty(g_dRequest) Then 
		sListID = Trim(g_dRequest("UMGUIDList"))
	Else
	 	sListID = Trim(Request.Form("UMGUIDList"))
	End If 	
	
	If Len(sListID) <> 0 then 
		Set LM = Server.CreateObject("Commerce.ListManager")
	'	LM.Initialize sGetListDSN, sGetMasterListTableName
		LM.Initialize sGetListDSN
		LM.ExportToSQL sListID, sGetTempDSN, sTempName, True, 0, False
		dState.Value("ListGUID") = sListID
		dState.Value("ListTable") = sGetTempTableName
		dState.Value("GUIDPointer") = 1
		dState.Value("ListNoSelect") = False
		Set LM = Nothing	
	Else
	    dState.Value("ListNoSelect") = True 
	    Set LM = Nothing
	End If	
	
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub MakeLMListQuery(dState, aprops, aIDCol)
'
'	dState -  Dictionary. contains the state info
'  aprops -  array of array of property and attributes
'  aIDCol -  array containing the identity(Join) key info
'	
'	Creates the ListSheet widget when UserManager is used to display the users
'	in a ListManager User List. This routine has to be a little different 
'	from the main ListSheet routine because the List just gives back a series of
'	User GUIDs. Routine writes out XML for the ListSheet Meta and Data Islands.
'
Sub MakeLMListQuery(dState, aprops, aIDCol, iCurPage)
    Dim sSQL
    Dim aGUIDs
    Dim sGuid
    Dim i
    Dim iType
    Dim iCol
    Dim nGUIDsOnPage, sIDName
 
	Call GetUserGUIDsFromList(dState, aGUIDs, nGUIDsonPage, iCurPage)
    icol = aIDCol(2)
    
    itype = sGetPropType(aprops(icol))'Was (4)
    sIDName = sBracket( sGetPropFullName(aprops(nGetIDColNo(aIDCol))) )
    sSQL = sFormSelectClause(dState, aprops)
    sSQL = sSQL & " WHERE " & sIDName & " IN "
    
		
	If nGUIDSOnPage > 0 Then
		Redim Preserve aGUIDs(nGUIDsOnPage -1)
		For i = 0 to nGUIDsOnPage - 1
			aGUIDs(i) = sQuoteArg(aGUIDs(i), iType)
		Next
		sGuid = Join(aGUIDs , ",")
		sSQL  = sSQL  & " ( " & sGuid & " )"
	Else
		sSQL  = sSQL  & "('')"
	End If
	
	If Not IsEmpty(dState.Value("SortColumn")) And Not IsNull(dState.Value("SortColumn")) Then
	   sSQL = sSQL & " ORDER BY " & sBracket(sGetProviderColumnName(dState.Value("SortColumn")))
	   If Not IsEmpty(dState.Value("SortDirection")) And Not IsNull(dState.Value("SortDirection")) Then
		   sSQL = sSQL & " " & dState.Value("SortDirection")
	   End If
	End If    	   
	   
	dState.Value("Query") = sSQL
	
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub GetUserGUIDsFromList(dState, aGUIDs, nGUIDsOnPage)
'
'	dState -  Dictionary. contains the state info
'  aGUIDs -  array of GUIDS
'  nGUIDsOnPage -  no. of GUIDs returned.
'	
Sub GetUserGUIDsFromList(dState, aGUIDs, nGUIDsOnPage, iCurPage)
	Dim cn, rs, sTable, sListGUID, sGUIDQuery, i
	Dim nRecordCount
	Dim nStartRecord
	
	sTable = dState.Value("ListTable")
	sListGUID = dState.Value("ListGUID")
    On Error Resume Next
	Set cn = Server.CreateObject("ADODB.Connection")
	cn.Open sGetListDSN
	
	sGUIDQuery = "SELECT rcp_GUID FROM " & sTable 
	Set rs = cn.Execute(sGUIDQuery)
	
	nRecordCount = 0
	Do While NOT rs.EOF
		nRecordCount = nRecordCount + 1
		rs.MoveNext
	Loop
	
	nStartRecord = (iCurPage - 1) * g_iListpageSize 
	
	Redim aGUIDs(g_iListPageSize)
	
	nGUIDsOnPage = 0
	
	If nRecordCount > 0 then
		If nStartRecord > 0 then
			rs.MoveFirst
			rs.Move nStartRecord
		Else	
			rs.MoveFirst
		End If
	End If		

	For i = 0 to g_iListPageSize - 1
		If not rs.Eof Then
		    If Not IsEmpty(rs.Fields("rcp_GUID").Value) And Not IsNull(rs.Fields("rcp_GUID").Value) then
				aGUIDS(i) = Replace(rs.Fields("rcp_GUID").Value," ","")
			End If	
			'Replace added to remove leading/trailing  blank chars .
			rs.MoveNext
		Else
			Exit For
		End If	
	Next
	
	nGUIDsOnPage = i
		
	rs.Close
	Set rs = Nothing
	cn.Close
	Set cn = Nothing
	On Error Goto 0 

End Sub



Function sGetTempDSN
	sGetTempDSN = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
	'"DSN=commerce;DATABASE=commerce;UID=SA;PWD=;"
End Function

Function sGetMasterListTableName
	sGetMasterListTableName = "lm_master"
End Function

Function sGetTempTableName
	Dim sName, dState, sGUID, sStr
	If len(trim(Session("ListTempTableName"))) = 0 Then
		Set dState = dGetUMStateDictionary("User")
		sGUID = sGetGUID()
		sStr = Mid(sGUID,2, 8)
		sName = "UMlm_temp_" & sStr
		Session("ListTempTableName") = sName
	Else
		sName = Session("ListTempTableName")
	End If
	sGetTempTableName = sName
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub PrintListSheet(dState, aprops)
'
'	dState -  Dictionary. contains the state info
'  aprops -  array of array of property and attributes

Sub PrintListSheet(dState, aprops, rs)
	Dim xmlOutput 
	Dim nrec
	
		
	call WriteXMLMeta2(aprops, dState, 0)
	
	If Session("ErrorMessage") <> "" Then
		Call WriteBlankList()
		Session("ErrorMessage") = ""
	 '  Call ThrowWarningDialog()
	    Exit Sub
	End If    
	    
	If rs Is Nothing then
		Call WriteBlankList()
    Else
		Response.Write "<xml id='profilesList'>"
		
		'call RS2XMLPage(rs, dState)
		Set xmlOutPut = xmlGetXMLFromRSEx(rs, 0, g_iListPageSize, TOO_BIG_TO_COUNT, NULL)
		
		
	   ' filter out the sitename from the list of organizations. (AD returns site container too!!)
		Call xmlFilterSitename(xmlOutput)
		
		nrec = xmlOutPut.GetAttribute("recordcount")
		If nrec > 0 and Session("ADInstallation") = True Then 
		 xmlOutPut.SetAttribute "recordcount" , nrec - 1 
		End If  
		
		Response.Write xmlOutput.xml							
		Response.Write "</xml>"
	End If  

end Sub



'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function rsGetListSheetRecordSet(dState)
'
'
'	dState -  Dictionary. contains the state info
'	returns Recordset from query.
'

Function rsGetListSheetRecordSet(dState)
	Dim rs, cnProv
	Dim bOK
	
	If bIsDictEmpty(dState.Value("query")) Then
		Set rsGetListSheetRecordSet = Nothing
	Else
	    Set cnProv = cnGetProviderConnection(dState)
		
		bOK = bExecWithErrDisplay(rs, cnProv, _
											dState.Value("query"), _
											L_SearchFail_ErrorMessage)
		If bOk then
			Set rsGetListSheetRecordSet = rs
		Else
			Set rsGetListSheetRecordSet = Nothing
		End If		
		Set rs = Nothing
		Set cnProv = Nothing    
	End If  
	
End Function



'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub DeleteUsers(dState, aprops)
'
'	dState -  Dictionary. contains the state info
'   aprops -  array of array of property and attributes
'	Routine that takes a Commerce Dictionary of user Guids (dSelect, in dState
'	commerce dictionary), and deletes users by constructing an IN clause for 
'	multiple selections.Also deletes all selected users if select all is pressed.
'
Sub DeleteUsers(dState, aprops)
    
	Dim cn, sSQL 
	Dim sName, iType, aIDCol, nID
	Dim dSelect, entity, arrEntity, szEntities
	Dim szEntityAdminGroupNames
	Dim szEntityUserGroupNames
	Dim szEntityName, sStatus
	Dim sValue
	Dim sWhere
	Dim sProfile
	Dim i
	Dim sSQL1, sSQL2
	Dim sDelGroupSQL
	Dim rs
	Dim bRet
	
	On Error Resume Next
	set dSelect = dState.Value("Selected")
	aIDCol = Session(g_sIDColLabel)

    sProfile = sGetPropProfile(aprops(1))
    If Request.Form("SelType") = "All"  Then
       If dState.Value("UMListMode") = True Then 
			sSQL = "DELETE FROM " & sBracket(sProfile) & " WHERE " 
	   Else
	        If g_bisADInstallation Then
				sSQL = "DELETE FROM " & sBracket(sProfile) & " WHERE " 
			Else	
				sSQL = "DELETE FROM " & sBracket(sProfile) 
			End If	
	   End If 		
		
	Else   
	   sSQL = "DELETE FROM " & sBracket(sProfile) & " WHERE "  
	End If   
	
	nID = nGetIDColNo(aIDCol)
	sName = sBracket(sGetPropFullName(aprops(nID)))
	iType = sGetPropType(aprops(nID))'(4)

    If sProfile = USER_PROFILE And LCase(sName) <> "[generalinfo.user_id]" Then
		Call SetError(L_Delete_ErrorMessage, L_UserProfileKeyMisplaced_Errormessage, _
									sGetUserScriptError(Err), ERROR_ICON_ALERT)
									
        Exit Sub									
	ElseIf sProfile = ORG_PROFILE And LCase(sName) <> "[generalinfo.org_id]" Then
		Call SetError(L_DeleteOperation_ErrorMessage, L_OrgProfileKeyMisplaced_Errormessage, _
									sGetUserScriptError(Err), ERROR_ICON_ALERT)
		Exit Sub							
    End If
    
	Set cn = cnGetProviderConnection(dState)
    If Request.Form("SelType") = "All" Then
		rem get whereclause of select all query
		If Not IsEmpty(dState.Value("query")) Then
			sWhere = sGetWhereClause(dState.Value("query"))
			If Len(sWhere) <> 0 And Not g_bisADInstallation Then
				sSQL = sSQL & " WHERE " & sWhere
			Else
			    sSQL = sSQl & sWhere
			End If	
		Else
		rem if there is no where clause don't execute query
			sSQL = ""
		End If	
		
		sStatus = sFormatString(L_AllDeleted_Text, Array(dState.EntityName))
	 Else
		If dSelect.Count > 1 then
			'make In clause with guids
			sSQL = sSQL & sName & " IN "
			i = 0
			Redim arrEntity(dSelect.Count -1)
			For Each entity In dSelect
				arrEntity(i) =  sQuoteArg(entity, iType)
				i = i + 1
			Next
			szEntities = Join(arrEntity, ",")
			sSQL = sSQL & "( " & szEntities & " )"
									
			If g_bIsADInstallation And sProfile = ORG_PROFILE Then

				For Each entity In dSelect
					sSQL1 = "SELECT " & g_sAccountOrgId & " FROM " & sBracket(USER_PROFILE) & " WHERE " _
					    & g_sAccountOrgId & " = " & sQuoteArg(entity, iType)
					bRet = bExecWithErrDisplay(rs, cn, sSQL1, L_OrgUserCheck_ErrorMessage)
					If bRet Then
						If rs.eof Then
							szEntityAdminGroupNames =  szEntityAdminGroupNames & _
															sQuoteArg(dSelect(entity) & "_ADMINGROUP", "STRING") & ","
							szEntityUserGroupNames = szEntityUserGroupNames & _ 
															sQuoteArg(dSelect(entity) & "_USERGROUP", "STRING") & ","
						Else
						
							Call SetError(L_DELETE_DialogTitle, L_OrgContainsUsers_ErrorMessage, _
										sGetUserScriptError(Err), ERROR_ICON_ALERT)
							Set cn = Nothing
							Set rs = Nothing
							Exit Sub
						End If										
					End If	
					Set rs = Nothing									
				Next
				
				sDelGroupSQL = "DELETE FROM " & sBracket(GROUP_PROFILE) & " WHERE [GeneralInfo.GroupName] IN "
				
				sSQL1 = ""
				If Len(szEntityAdminGroupNames) > 0 Then
					szEntityAdminGroupNames = Left(szEntityAdminGroupNames, len(szEntityAdminGroupNames) -1)
					sSQL1 = sDelGroupSQL & "( " & szEntityAdminGroupNames & " )"
					szEntityUserGroupNames = Left(szEntityUserGroupNames, len(szEntityUserGroupNames) -1)
					sSQL2 = sDelGroupSQL & "( " & szEntityUserGroupNames & " )"
				End If	
			
			End If	
			rem status message
			
			if sProfile = ORG_PROFILE then 
				sStatus = sFormatString(L_OrgsCountDeleted_Text, Array(dSelect.Count))
			else
				sStatus = sFormatString(L_UsersCountDeleted_Text, Array(dSelect.Count))
			end if
				
		ElseIf dSelect.Count = 1 then
			' For single selction get correct value
			For Each entity In dSelect
				sValue =  sQuoteArg(entity, iType)
				szEntityName = dSelect(entity)
			Next	
			sSQL = sSQl & sName & " = " & sValue
			If g_bIsADInstallation and sProfile = ORG_PROFILE then
				sSQL1 = "SELECT " & g_sAccountOrgId & " FROM " & sBracket(USER_PROFILE) & " WHERE " _
				    & g_sAccountOrgId & " = " & sValue
				bRet = bExecWithErrDisplay(rs, cn, sSQL1, L_OrgUserCheck_ErrorMessage)
				sSQL1 = ""
				If bRet then
					If rs.eof then
						sDelGroupSQL = "DELETE FROM " & sBracket(GROUP_PROFILE) & " WHERE [GeneralInfo.GroupName] = "
						sSQL1 = sDelGroupSQL & sQuoteArg(szEntityName & "_ADMINGROUP", "STRING")
						sSQL2 = sDelGroupSQL & sQuoteArg(szEntityName & "_USERGROUP", "STRING")
					Else
					    Call SetError(L_DELETE_DialogTitle, L_OrgContainsUsers_ErrorMessage, _
									sGetUserScriptError(Err), ERROR_ICON_ALERT)
						Set cn = Nothing
						Set rs = Nothing
						Exit Sub
					End If										
				End If	
			End If
			If IsNull(szEntityName) then szEntityName = ""
			rem status message
			
			if sProfile = ORG_PROFILE then 
				sStatus = sFormatString(L_OrgEntityDeleted_Text, Array(szEntityName))
			else
				sStatus = sFormatString(L_UserEntityDeleted_Text, Array(szEntityName))
			end if
		'	sStatus = sFormatString(L_EntityDeleted_Text, Array(dState.EntityName, szEntityName))
		Else
			rem if there are no selsctions don't execute query
			sSQL = ""	
		End If	
	End If
	
		
	If Trim(sSQL) <> "" Then
		If sSQL1 <> "" Then
			bRet = bSafeExec(rs, cn, sSQL1)
			bRet = bSafeExec(rs, cn, sSQL2)
		    If Session("ErrorMessage") <> ""  and bRet = False then 
				Call SetError(L_DeleteOperation_ErrorMessage, L_RefreshConnection_Text, _
									Session("ErrorMessage"), ERROR_ICON_ALERT)
				Set cn = Nothing
				Set rs = Nothing
				Exit Sub
			End If				
		End If	
		bRet = bSafeExec(rs, cn, sSQL)
		If Session("ErrorMessage") <> ""  and bRet = False then 
			Call SetError(L_DeleteOperation_Error, L_RefreshConnection_Text, _
									Session("ErrorMessage"), ERROR_ICON_ALERT)
			Set cn = Nothing
			Set rs = Nothing
			Exit Sub
		End If				
		
	Else
		call setError(L_DELETE_DialogTitle, L_NoSelection_ErrorMessage, _
										0 , ERROR_ICON_ALERT)
		sStatus = ""						
	End If									
    
	If Err.Number <> 0 then 
		call setError(L_DELETE_DialogTitle, L_Delete_ErrorMessage, _
										sGetUserScriptError(Err), ERROR_ICON_ALERT)
	Else
		 g_sStatusText = sStatus
	End If
		
   
   Set cn = Nothing
   On Error Goto 0

End Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLGetDisplayColumns
'
'	returns an XMLDocument containing the columns to be displayed in the list sheet
'	It reads an external XML file on the server, columns.xml, to 
'	determine the columns to display for ListSheet view of users and orgs.
'
Function oXMLGetDisplayColumns
    Dim oXMLDoc, oXMLcolumns, oXMLstart, oXMLProf, oXMLCol, sColName, oXMLMatch
    Dim sProfName
    Set oXMLDoc=Server.CreateObject("MSXML.DOMDocument")
    oXMLDoc.Load(Server.MapPath("columns.xml"))
    Set oXMLProf = Session(g_sProfileLabel)
    sProfName = oXMLProf.DocumentElement.SelectSingleNode(".//" & _
        sXMLTagProfile).GetAttribute(sXMLAttrName)
    Set oXMLstart = oXMLDoc.DocumentElement.SelectSingleNode("./" & sXMLTagProfile &_
        "[@" & sXMLAttrName & "='" & sProfName & "']/" & sXMLTagCOLUMNS)
    Set oxmLcolumns = oXMLstart.SelectNodes("./" & sXMLTagCOLUMN)
    For Each oXMLCol In oXMLColumns
        sColName = oXMLCol.attributes.GetnamedItem("name").NodeValue
        Set oXMLMatch = oXMLProf.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty & "[@" & sXMLAttrName & _
            "='" & sColName & "']")
        If oXMLMatch is Nothing Then
            Response.Write sColname & L_NoDisplayColumn_ErrorMessage
            oXMLStart.RemoveChild oXMLCol
            Set oXMLMatch = Nothing
        End If
    Next
    Set oXMLGetDisplayColumns = oXMLstart.SelectNodes("./" & sXMLTagCOLUMN)
    Set oXMLDoc = Nothing
    Set oXMLstart = Nothing
End Function


%>