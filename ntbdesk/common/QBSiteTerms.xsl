<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">

  <xsl:template match="@*">
    <xsl:copy><xsl:value-of /></xsl:copy>
  </xsl:template>

  <xsl:template match="/">
    <xsl:pi name="xml">version="1.0"</xsl:pi>
    <xsl:apply-templates />
  </xsl:template>
  
  <xsl:template match="Document">
    <Document>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Document>
  </xsl:template>

  <xsl:template match="Catalog">
    <Catalog>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Catalog>
  </xsl:template>

  <xsl:template match="Profile">
		<Profile>
		    <xsl:apply-templates select="@*" />
		    <xsl:apply-templates />
		</Profile>
  </xsl:template>

  <xsl:template match="Group">
    <Group>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Group>
  </xsl:template>
  
  <xsl:template match="Property">
    <Property>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Property>
  </xsl:template>


  <xsl:template match="Attribute">
    <Attribute>
		<xsl:choose> 
			<xsl:when test=".[@value]">
			  <xsl:attribute name="name"><xsl:value-of select="@value" /></xsl:attribute>
			  <xsl:apply-templates select="@*[(nodeName() != 'name')]" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="@*" />
			</xsl:otherwise>
		</xsl:choose>
      <xsl:apply-templates />
    </Attribute>
  </xsl:template>


</xsl:stylesheet>
