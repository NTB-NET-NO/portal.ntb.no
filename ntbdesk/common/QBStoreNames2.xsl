<xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">

  <xsl:template match="@*">
    <xsl:copy><xsl:value-of /></xsl:copy>
  </xsl:template>

  <xsl:template match="/">
    <xsl:pi name="xml">version="1.0"</xsl:pi>
    <xsl:apply-templates />
  </xsl:template>
  
  <xsl:template match="Document">
    <Document>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Document>
  </xsl:template>

	<xsl:template match="Catalog">
		<Catalog>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</Catalog>
	</xsl:template>

	<xsl:template match="Profile">
		<Profile>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</Profile>
	</xsl:template>

  <xsl:template match="Group">
    <Group>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Group>
  </xsl:template>

  <xsl:template match="Property">
    <Property>
		<xsl:choose> 
			<xsl:when test=".[@propType $or$ @displayName]">
			  <xsl:attribute name="displayName"><xsl:eval>GetDSN</xsl:eval></xsl:attribute>
			  <xsl:apply-templates select="@*[(nodeName() != 'displayName')]" />
			</xsl:when>
			<xsl:otherwise>
        <xsl:apply-templates select="@*" />
			</xsl:otherwise>
		</xsl:choose>
      <xsl:apply-templates />
    </Property>
  </xsl:template>

  <xsl:script language="VBScript">
    function GetDSN
        dim sRef1
        dim sRef2
        sRef1 = me.getAttribute("dataSource")
        if Not sRef1 = "" then 
          sRef2 = me.getAttribute("displayName")
          GetDSN = sRef2 + " [" + sRef1 + "]"
        else
          GetDSN = me.getAttribute("displayName")
        end if 	
    end function
  </xsl:script>
  
</xsl:stylesheet>

