<%
											        

rem -- Define constants For the XML Tgs
const sXMLTagCatalog = "Catalog"
const sXMLTagProfile = "Profile"
const sXMLTagColumn = "COLUMN"
const sXMLTagColumns = "COLUMNS"
const sXMLTagSource = "DataSource"
const sXMLTagTable = "DataObject"
const sXMLTagAttribute = "Attribute"
const sXMLTagProperty = "Property"
const sXMLTagGroup = "Group"
const sXMLTagQBProperty = "Property"
const sXMLAttrIsGroup = "isGroup"
const sXMLAttrCatalogID = "catalogID"
const sXMLAttrProfileID = "profileID"
const sXMLAttrName = "name"
const sXMLAttrDisplayName = "displayName"
const sXMLAttrValue = "value"
const sXMLAttrDataType = "dataType"
const sXMLAttrReference = "referenceString"
const sXMLAttrConnectionString = "connectionString"
const sXMLAttrIsRequired = "isRequired"
const sXMLAttrIsSearchable = "isSearchable"
const sXMLAttrRef = "ref"
const sXMLAttrIsActive = "isActive"
const sXMLAttrIsReadOnly = "isReadOnly"
const sXMLAttrTypeDefName = "propType"
const sXMLAttrVirtualGroup = "virtualGroup"
const sXMLAttrIsMultiValued = "isMultiValued"
const sXMLAttrKeyDef = "keyDef"
const sXMLAttrIsUpdatable = "isUpdatable"
const sXMLTagDataRef = "DataRef"
const sXMLDefaultNamingContext = "defaultNamingContext"
const sXMLDefaultParentURL = "DefaultParentURL"
rem-- constant For Root container in AD
'const sXMLMSCSROOT = "OU=MSCS_40_Root"


rem -- constants For some of the fields which are compared against
const g_sParentURLProp = "ParentDN"
const g_sParentURL = "ProfileSystem.ParentDN"
const g_sAccountOrgId = "AccountInfo.org_id"
const g_sUserAccountControl = "ProfileSystem.user_account_control"
const g_sAccountStatus = "AccountInfo.account_status"
const g_sPartnerDeskRoleFlags = "BusinessDesk.partner_desk_role"
const g_sSAMAccountName = "ProfileSystem.sam_account_name"
const g_sntSecurityDescriptor = "nt_security_descriptor"
const g_sProfileSystem = "ProfileSystem"
const g_sGeneralInfo   = "GeneralInfo"
const USERACCOUNT_ADMIN = 66048 '0x10200
const USERACCOUNT_NORMAL = 512 '0x200
const USERACCOUNT_INACTIVE = 546 '0x222
const passwdguid = "{2AC34396D1E9}"
const g_PartnerDeskAdmin = "2"
const CATALOG_PROFILE = "Catalog"
const AD_PROFILE = "Profile Definitions.ADGroup"

const MAX_CLAUSE_VALUES = 32
const MAX_PASSWORD_LENGTH = 14
const MIN_PASSWORD_LENGTH = 7
const PASSWORD_MASK = "********"

const MV_DELIM = ";"
const MV_DELIM_ESCAPE = "\;"


'-----------------------------------
'Accessor Functions For aprops
'-----------------------------------

Function bIsActive(aPropElement)
    bIsActive = aPropElement(13)
End Function


'Function to determine If the current profile is stored in Active Directory or not
Function bIsADInstallation(oXMLProf)
	Rem This function check If the ParentDN property is present in any of the groups.
	Rem This assumes that the ParentDN is defined only For profiles residing in AD.

	Dim bVal, oXMLProp, aGroups, sWhere, i
	swhere = "./" & sXMLTagCatalog & "/" & sXMLTagProfile & "/"
	If (Instr(1, g_sParentURL, ".") > 0) Then 
		aGroups = split(g_sParentURL, ".")
		For i = lBound(aGroups) to Ubound(aGroups)-1
			swhere = swhere & "/" & sXMLTagGroup & "[@" & sXMLAttrName & "='" & aGroups(i) & "']"
		next
		i = Ubound(aGroups)
		swhere = swhere & "/" & sXMLTagProperty &"[@" & sXMLAttrName & "='" & aGroups(i) & "']"
	Else
		swhere = swhere & "/" & sXMLTagProperty &"[@" & sXMLAttrName & "='" & g_sParentURL & "']"
	End if
	set oXMLProp = oXMLProf.DocumentElement.SelectSingleNode(swhere)
	If oXMLProp is Nothing Then
		bIsAdInstallation = False
	Else
		bIsAdInstallation = True
	End if
End Function

'Function sBracketGUID(sGUID)
'Input: sGUID a string containing a GUID
'Returns: sGUID wrapped by braces "{}"
'Function to take a GUID string in sGUID and make sure that it is enclosed by curly braces, i.e., "{GUID}". The
'function looks For delimiting braces in the string already, removes them If they are there, and adds
'braces to the string. The returned value is the brace enclosed GUID.
Function sBracketGUID(sGUID)
	Dim sNoBrack, sTmp, ist, iend
	If (IsNull(sGUID)) or (len(trim(sGUID)) = 0) Then
	    abort (L_NoUniqueGUID_ErrorMessage)
	End if
	ist = Instr(1, sGUID, "{")
	If ist <= 0 Then
		ist = 1
	Else 
		ist = ist+1
	End if
	iEnd = Instr(ist, sGUID, "}")
	If iEnd <= 0 Then
		iEnd = len(sGUID)
	Else
		iEnd = iEnd - 1
	End if
	sNoBrack = Mid(sGUID, ist, iEnd - ist+1)
	sBracketGUID = "{" & sNoBrack & "}"
End Function

'Function oXMLMakeDocumentElement(sName)
'Input: sName - string
'Returns: XMLDOMDocument object with DocumentElement child with name sName
'Function that takes the name of the first child Node in an XML structure and creates
'a DOMDocument and a child element with this name. Function returns the DOMDocument object.
'Implemented mainly For convenience and to save repeated code.
Function oXMLMakeDocumentElement(sName)
    Dim oXML, oXMLEl
    set oXML = Server.CreateObject("MSXML.DomDocument")
    set oXMLEl = oXML.CreateElement(sName)
    set oXML.DocumentElement = oXMLEl
    set oXMLMakeDocumentElement = oXML
End Function



'Function to determine where to create a user For AD
'Need to get a base URL. 
Function sGetParentDN(dState)
	Dim bExit, sBaseURL
	bExit = False
	sBaseURL = sGetBaseURL()
	If len(trim(sBaseURL)) = 0 Then
		bExit = True
	End if
	If bExit Then
		sGetParentDN = ""
	Else
		sGetParentDN = sBaseURL 
	End if
End Function


'Function sGetBaseURL()
'Function to get the base AD container (For site) under which users, orgs, etc will be created
'in AD installations of CS40. 
'This URL is a RelativeDN eg. OU=TestSite,OU=MSCS_40_Root. The provider adds the defaultNamingContext 
'to it to build the fully qualified DN.
Function sGetBaseURL()
	Dim oXMLNode,xmlProfile
	Dim oXMLdoc
    Dim bizData
    On Error Resume next
    
	If IsEmpty(Session("BaseURL")) then
	    Session("ListPageCall")=True
		Set bizData = dGetBizDataObject()
		set xmlProfile = bizdata.GetProfile(AD_PROFILE)

	    Set oXMLNode = xmlProfile.SelectSingleNode(".//SourceInfo//Attribute/")
		    If Not IsEmpty(oXMLNode) and Not (oXMLNode Is Nothing) then
				sGetBaseURL = oXMLNode.SelectSingleNode(".//" & sXMLTagAttribute & _
								"[@" & sXMLAttrName & "='" & sXMLDefaultParentURL & "']").GetAttribute("value")
				Session("BaseURL") =sGetBaseURL								
		    Else
				sGetBaseURL = ""
			End If		
			
	Else
			sGetBaseURL = Session("BaseURL")
	End If

	If Err.number <> 0 Then
		abort (L_SiteDN_ErrorMessage)
	End if

  On Error Goto 0
	
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	ClearUsers(dState)
'	This routines clears the Selected dictionary
'	dState	- dictionary of Users selected from listsheet
'	
Sub ClearUsers(dState)
    Dim dSelect, user
    set dSelect = dState.Value("Selected")
    For each user in dSelect
        dSelect.Value(user) = NULL
    next
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function dCreateUMStateDictionary(sLabel)
'
'	returns: a user or Org state dictionary object.
'	creates the User or Org state dictionary
'	
'	sLabel	- string specifying If the User or Organization state is retrieved
'
Function dCreateUMStateDictionary(sLabel)
	Dim dState, oXMLProfile, aprops, agroups, aIDCol
	   
	set dState = Server.CreateObject("Commerce.Dictionary")
    
   rem store connection strings in state
	call GetSiteConnData(dState)
	rem set the Current profile name "Profile Definitions.UserObject"/..
	rem and entity name "User"
	dState.ProfileName = CURR_PROFILE
	dState.EntityName  = g_sUMState
	
	rem create a dictionary to store the selected entities
   set dState.Value("Selected") = Server.CreateObject("Commerce.Dictionary")
   
   rem get the properties and attributes into an array
   Call GetProperties(aprops, agroups, oXMLProfile, dState)
	
	rem get the Join key column info into an array - aIDCol
   'call DetermineIDColumn(oXMLProfile, aprops, aIDCol)
   reDim aIDCol(2)
   If sLabel <> "User" Then 
		aIDCol(2) = nGetMatchingEntry(XML_ATTR_ORGID, aprops)
		aIDCol(1) = sGetPropName(aprops(aIDCol(2)))
   Else		
        aIDCol(2) = nGetMatchingEntry(XML_ATTR_USERID, aprops)
		aIDCol(1) = sGetPropName(aprops(aIDCol(2)))
   End if	 
   
   rem store the profile XMLDoc object For further use
   set Session(g_sProfileLabel) = oXMLProfile

   rem If the primary key is not a hidden field (IsActive = 1) Then set this
   rem as friendly name, Else read the columns.xml file For the friendly name
   rem - that's a minor workaround. should keep columns.xml in sync!
	If (True = bIsPropActive(aprops(nGetIDColNo(aIDCol)))) Then
		Session(g_sNameLabel) = sGetIDName(aIDCol)
	Else
		Session(g_sNameLabel) = sGetFriendlyName(dState.ProfileName) '"Name"'aprops(i)(2)
	End if
	
	Session(g_sPropsLabel) = aprops
	Session(g_sGroupsLabel) = agroups
	Session(g_sIDColLabel) = aIDCol 
	Set dCreateUMStateDictionary = dState
	Set oXMLProfile = Nothing
	
End Function

Function sGetPrimaryKeyName
    Dim oXMLProf, oXMLPK, sStr
    set oXMLProf = Session(g_sProfileLabel)
    set oXMLPK = oXMLProf.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty & "[@" & _
			sXMLAttrKeyDef & "='PRIMARY'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']")
    call validNode(oXMLPK, L_NoPKColumn_ErrorMessage)  
    sGetPrimaryKeyName = oXMLPK.attributes.GetNamedItem(sXMLAttrName).NodeValue
End Function

Function sGetJoinKeyName(oXMLProf)
    Dim oXMLJK, sStr
    set oXMLJK = oXMLProf.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty & "[@" & _
			sXMLAttrKeyDef & "='JOIN'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']")
    call validNode(oXMLJK, L_NoPKColumn_ErrorMessage)  
    sGetJoinKeyName = oXMLJK.attributes.GetNamedItem(sXMLAttrName).NodeValue
End Function

Function sGetFriendlyName(sRef)
    Dim oXMLColumns, oXMLConfigFile, sProfile, sFriendly
    set oXMLConfigFile = Server.CreateObject("MSXML.DomDocument")
    oXMLConfigFile.Load(Server.MapPath("columns.xml"))
	'Assume Reference name is of Form "Catalog.Profile", so to get Profile name, take everything 
	'to the right of the first period you find (If there is any). If no period, takes whole string.
    sProfile = Mid(sRef, InStr(1, sRef, ".")+1)
   
    sGetFriendlyName = oXMLConfigFile.DocumentElement.SelectSingleNode(".//" & sXMLTagProfile &_
        "[@" & sXMLAttrName & "='" & sProfile & "']//" & sXMLTagColumn &_
        "[@isFriendlyName='1']").GetAttribute(sXMLAttrName)
    set oXMLConfigFile = nothing
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function dGetUMStateDictionary(sLabel)
'
'	returns: a user or Org state dictionary object.
'	look in Session variable to see If its created. 
'	If session variable is empty, create the dictionary, initialize it, and 
'	put it in the session variable.
'	
'	sLabel	- string specifying If the User or Organization state is retrieved
'
Function dGetUMStateDictionary(sLabel)
	If NOT IsObject(Session(g_sStateLabel)) or NOT Application("MSCSProfileSchemaVer") = Session("MSCSProfileSchemaVer") Then
        Set Session(g_sStateLabel) = dCreateUMStateDictionary(sLabel)
    End if
    set dGetUMStateDictionary = Session(g_sStateLabel)
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function dGetBizDataObject()
'
'	returns: BizDataObject.
Function dGetBizDataObject()
    Dim obizdata,sProfileDSN
	sProfileDSN = sGetProfileConnStr()
	set obizdata = oGetBizDataManager
	call ConnectBizDataManager(obizdata, sProfileDSN)
	Set dGetBizDataObject = obizdata
	Set obizdata = Nothing
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sGetSiteName
'
'	returns: string.
'	get the current site name from Application variable.
'
Function sGetSiteName
    sGetSiteName = Application("MSCSSiteName")
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub GetSiteConnData(dState)
'	
'  dState - State CommerceDictionary
'	initialize site-wide connection data For State dictionary -
'	ProfileDSN and ProviderDSN
'
Sub GetSiteConnData(dState)
    
	If (isNull(dState.Value("ProfileDSN"))) Then
	    dState.Value("ProfileDSN") = sGetProfileConnStr()
	End if
	If (isNull(dState.Value("ProviderDSN"))) Then 
	  dState.Value("ProviderDSN") = sGetProviderConnStr()
	End if

End Sub



'Function bValueChanged(ByRef sOld, ByVal sNew)
'Input: sOld, sNew -- both strings
'Output: True or False Boolean
'Function that compares two string values (a current value of a variable and an old value) and determines
'whether the two values are the same or not. Returns True If the values are different, false If they
'are the same
Function bValueChanged(ByRef sOld, ByVal sNew)
    Dim bVal
    bVal = False
    If (len(sNew) > 0) Then
        If (strcomp(sOld, sNew, 0) <> 0) Then 
            bVal = True
            sOld = sNew
        End if
    End if
    bValueChanged = bVal
End Function


'Function sGreaterThan(sDir)
'Input: sDir -- string, should be one of two values (ASC or DESC)
'Ouptut: String 
'Function For determining operator in SQL query For paging. Motivation For function is that we are 
'going forward one page and we have the value For the last entry in the sort column of the last page.
'So our new sql will be of the form "SELECT {} WHERE {SORTCOLUMN} >= {VALUE}". But there's a minor hitch.
'The above statement is only true For ASC sorts. If it is a DESC sort, Then we need the operator
'to be <=. (I.e., If we are paging forward in a DESC ordered recordset we are going to lesser and
'lesser values of the sort column (not greater as in ASC sort). So this function takes the sort
'direction (ASC or DESC as string) as a parameter and gives us the correct operator.
Function sGreaterThan(sDir)
    Dim sComp
    sComp = Lcase(Trim(sDir))
    select case sComp
        case "asc" sGreaterThan = " >= "
        case "desc" sGreaterThan = " <= "
        case Else
            call abort(sComp & L_WrongGTDirection_Errormessage) 
    End select
End Function


'Function sLessThan(sDir)
'Input: sDir -- string, should be one of two values (ASC or DESC)
'Ouptut: String 
'Similar to sGreaterThan function above but For <= operator. Motivation is paging backwards, need all records
'whose sort column value is less than or equal to first value on last page (in ASC sorted recordset).
Function sLessThan(sDir)
    Dim sComp
    sComp = Lcase(Trim(sDir))
    select case sComp
        case "desc" sLessThan = " >= "
        case "asc" sLessThan = " <= "
        case Else
            call abort(sComp & L_WrongLTDirection_Errormessage) 
    End select
End Function


'Function sReverseDir(sDir)
'Input: sDir -- string, should be one of two values (ASC or DESC)
'Ouptut: String (DESC or ASC)
'Function For reversing the order of a SQL query.
'Takes a string ASC or DESC and returns DESC or ASC respectively.
'Stops If sDIr is not equal to ASC or DESC.
Function sReverseDir(sDir)
    Dim sComp
    sComp = Lcase(Trim(sDir))
    select case sComp
        case "asc" sReverseDir = " desc "
        case "desc" sReverseDir = " asc "
        case Else
            call abort(sComp & L_WrongRevDirection_Errormessage) 
    End select
End Function



'Function bIsDictEmpty(sEntry)
'Input: sEntry - Commerce Dictionary entry
'Output: Boolean (True or False)
'Function For determining whether a Commerce Dictionary entry has been set or not. Returns
'true If the entry is null or contains a string whose trimmed length is 0.
Function bIsDictEmpty(sEntry)
    Dim bVal
'Function to determine If a dictionary entry has not been set or If it contains a blank string
    bVal = True
    If NOT ISNULL(sEntry) Then
        If (TypeName(sEntry) = "String") Then
            If len(trim(sEntry)) > 0 Then
                bVal = False
            End if
        Else
            bVal = false
        End if
    End if
    bIsDictEmpty = bVal
End Function


'Function sFormOrderClause(dState)
'Input: dState - Commerce Dictionary For State
'Output: String
'Function that generates the ORDER BY clause For listview with the appropriate Sort Column
'and sort direction as saved in state variables.
'Function returns a string with the SQL clause.
Function sFormOrderClause(dState)
    Dim sOrder, sOrderBy
    If bIsDictEmpty(dState.Value("SortColumn")) Then
        'sOrderBy = sGetOrderByKey
        'dState.Value("SortDirection") = " asc "
        'dState.Value("SortColumn") = sOrderBy
        'sOrder = " ORDER BY " & sBracket(sGetProviderColumnName(sOrderBy))
    Else
    
       if LCase(dState.Value("SortColumn"))="list_name" then dState.Value("SortColumn")="Logon_Name" 
        sOrder = " ORDER BY " & sBracket(sGetProviderColumnName(dState.Value("SortColumn"))) & " " & dState.Value("SortDirection")
    End if
    sFormOrderClause = sOrder
End Function


'Sub RemovePropertyNames(oXML)
'Routine to Parse XML from Query Builder and convert it to a more useful form For this page.
'In particular, Profile Properties are contained in <PROPERTY> tags in XML returned from QB and
'have the property name in the ID attribute. Property name is of form "Profile.Group1.Group2.{...}.Name"
'Need to remove profile name and enclose in brackets For use with ADO provider.
Sub RemovePropertyNames(oXML)
    Dim oXMLProps, sID, sIDNew, oXMLProp
    set oXMLProps = oXML.DocumentElement.SelectNodes(".//" & sXMLTagQBProperty)
    For each oXMLProp in oXMLProps
        sID = oXMLProp.GetAttribute("ID")
        sIDNew = sBracket(Mid(sID, Instr(1,sID, ".") +1))
        oXMLProp.SetAttribute "ID", sIDNew
    next
End Sub

'Sub ConvertXMLDateTimeValues(oXML)
'Formats xml for DateTime
Sub ConvertXMLDateTimeValues(oXML)
	Dim xmlNodes
	Dim xmlNode
	Dim sDateTime
	Dim attr
    Set xmlNodes = oXML.selectNodes(".//IMMED-VAL[@TYPE = 'datetime' or @TYPE = 'time']")
    For Each xmlNode In xmlNodes
        attr = xmlNode.GetAttribute("TYPE")
        sDateTime = xmlNode.text 
        If attr = "time" Then 
			xmlNode.text = sFormatAsTime(xmlNode.text)
		Else
			xmlNode.text = sFormatAsDateTime(xmlNode.text)
		End If	
        If Len(xmlNode.text) = 0  Then 
            If Session("UPDATE") = True Then 
				Call Abort(L_InvalidDateTime_Text & sDateTime)
			Else	
				Call ThrowWarningDialog(L_InvalidDateTime_Text & sDateTime)
				Response.End 
            End If
        End If    
    Next
End Sub


'Sub ConvertXMLNameToGuid(oXML)
'Formats xml for DateTime
Sub ConvertXMLNameToGuid(oXML)
	Dim xmlNodes
	Dim xmlNode
	Dim oXMLProps, sID, oXMLProp
	Dim sText , iItemCount
	Dim rs, cn, sSQL
	On Error Resume Next
	iItemCount = 0
	Set xmlNodes = oXML.selectNodes(".//IMMED-VAL")
    Set oXMLProps = oXML.DocumentElement.SelectNodes(".//" & sXMLTagQBProperty)
    For each oXMLProp in oXMLProps
        sID = oXMLProp.GetAttribute("ID")
        Set xmlNode = xmlNodes.Item(iItemCount)
        sText = xmlNode.text 
        If Instr(1,LCase(sID),"accountinfo.org_id") > 0 Then
            sSQL = "Select [GeneralInfo.org_id] from [Organization] where [GeneralInfo.name] = " & sQuoteArg(sText,"TEXT")  
            If g_bIsADInstallation Then 
				sSQL = sSQL & " AND " & sBracket(g_sParentURL) & " = '" & _
				sGetParentDN(dState) & "'"
			End If	
			Set cn = cnGetProviderConn()
			Set rs = cn.Execute(sSQL)
			If Not rs Is Nothing and Not rs.eof Then
				xmlNode.text = rs("GeneralInfo.org_id").value
			Else
				xmlNode.text = ""
			    If Session("UPDATE") = True Then 
					Call Abort(L_InvalidOrgName_ErrorMessage)
				Else	
					Call ThrowWarningDialog(L_InvalidOrgName_ErrorMessage)
					Response.End 
				End If
			End If                     
        End If
        iItemCount = iItemCount + 1
    Next
    On Error Goto 0   
End Sub

'Sub ConvertXMLSiteTermTypes(oXML)
'Formats xml for Siteterms. Called from whereclause formation procedure
Sub ConvertXMLSiteTermTypes(oXML)
	Dim xmlNodes
	Dim xmlNode
	Dim xmlPropNode
	Dim xmlPropNodes
	Dim xmlSubNode
	Dim iTemCount
	Dim sID
	
    Set xmlNodes = oXML.selectNodes(".//IMMED-VAL[@TYPE = 'siteterm']")
    Set xmlPropNodes = oXML.selectNodes(".//Property[@TYPE = 'SITETERM']")
    iTemCount = 0

    'Siteterms can now only be mapped to String Fields
    For Each xmlNode In xmlNodes 
       	If IsNumeric(xmlNode.text) Then
		    Set xmlSubNode = xmlPropNodes.Item(iTemCount)
		    sID = xmlSubNode.GetAttribute("ID")
		    If Instr(1,sID,"user_type") > 0 Or Instr(1,sID,"partner_desk_role") > 0 Or Instr(1,sID,"account_status") > 0  Then
				Call xmlNode.setAttribute("TYPE","number")	
			End If    
			iTemCount = iTemCount + 1	
		End If	
    Next
End Sub

'Function sFormWhereClause(dState)
'Input: dState - Commerce Dictionary For State
'Output: String
'Function that forms the SQL WHERE clause For the main listview query based
'on the query information the user entered or on the current page state information
'stored in the dState commerce dictionary.
'Returns a string with the SQL WHERE clause
Function sFormWhereClause(dState)
    Dim sText, oXML, sWhere, sFindBy, imatch, aprops, sFindCol
    Dim oEfqb 'For exprfltrQueryBldr
    Dim nFindString
    Dim sFindByType
    sFindBy = lcase(dState.Value("FindBy"))
    Session("UPDATE") = False
    If (lcase(sFindBy) = "querybldr") Then
       set oXML = Session(g_sAdvQueryLabel)
       Call RemovePropertyNames(oXML)
       Call ConvertXMLNameToGuid(oXML)
	   Call ConvertXMLDateTimeValues(oXML)
	   Call ConvertXMLSiteTermTypes(oXML)
	   Set oEfqb = Server.CreateObject("commerce.ExprFltrQueryBldr")
	   If Not oXML.DocumentElement.FirstChild is Nothing Then 
			sWhere = oEfqb.ConvertExprToSqlFltrStr(oXML.DocumentElement.FirstChild.xml)
	   End If		
    Else
			aprops = Session(g_sPropsLabel)
			sText = dState.Value("FindByText")
			sFindCol = sGetProviderColumnName(sFindBy)
			sFindByType = sGetProviderColumnType(sFindBy)

			If (len(Trim(sText)) > 0) Then
				'Profile is at present being treated as String. We need to remove this once 
				'We have the Generic Editor
				If UCase(sFindByType) = "STRING" or UCase(sFindByType) = "PROFILE" Then
					If strcomp(Trim(stext),"*", 1) = 0 Then
						sWhere = "" 'returns all records
					ElseIf InStr(1,Trim(sText),"*") > 0 Then
						sText = Replace(sText, "*","%")
						sWhere =  sBracket(sFindCol) & " LIKE " & sQuoteArg(sText, sFindByType)
					Else
						sWhere =  sBracket(sFindCol) & " = " & sQuoteArg(sText, sFindByType)
					End if
				ElseIf UCase(sFindByType) = "DATETIME" Then
					sWhere = sFormatAsDateExpr(sBracket(sFindCol), sText)
				Else
					sWhere =  sBracket(sFindCol) & " = " & sQuoteArg(sText, sFindByType)
				End If	
			End if
    End if
    
    If (len(trim(Session("UMOrgRestrict"))) > 0) Then
			If len(Trim(sWhere)) <> 0 Then sWhere = sWhere  & " AND " 
				sWhere = sWhere  & sBracket(sGetProviderColumnName(XML_ATTR_ORGID)) & " = '" & Session("UMOrgID") & "' "
			If g_bIsADInstallation Then 
				If len(Trim(sWhere)) <> 0 Then sWhere = sWhere  & " AND "
				sWhere = sWhere & sBracket(g_sParentURL) & " = '" & _
				sGetParentDN(dState) & "'"
			End if	
    Else
			nFindString = InStr(1,swhere,g_sParentURl,1)
			If nFindString = 0 and Not IsNull(nFindString) and g_bIsADInstallation Then 
				If len(Trim(sWhere)) <> 0 Then sWhere = sWhere  & " AND "
				sWhere = sWhere & sBracket(g_sParentURL) & " = '" & _
								sGetParentDN(dState) & "'"
			End if	
    End if
    
    If Len(Trim(sWhere)) <> 0 Then
			sFormWhereClause = " WHERE " & sWhere
    Else
			sFormWhereClause = ""
    End If
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sFormatAsDateExpr(sCol, dtVal)
'
'	returns: an expression to search For date values
'	Build a clause similar to scol >= {d 'y/m/d'} and scol <= {d 'y/m/d'}
'	 
'		
'	sCol	- column name
'	dtVal - Date value to be searched for
'
Function sFormatAsDateExpr(sCol, dtVal)
	Dim syear, smonth, sday
	Dim dtBegin
	Dim dtEnd
	
	If NOT bValidString(dtVal) Then
		sFormatAsDateExpr = ""
		Exit Function
	End if	
	If NOT isDate(dtVal) Then
		sFormatAsDateExpr = ""
		Exit Function
	End if	
	syear = year(CDate(dtVal))
	smonth = month(CDate(dtVal))
	sday = day(CDate(dtVal))
	dtBegin = syear & "/" & smonth & "/" & sday
	
	dtEnd = DateAdd("d", 1, dtVal)
	syear = year(CDate(dtEnd))
	smonth = month(CDate(dtEnd))
	sday = day(CDate(dtEnd))
	dtEnd = syear & "/" & smonth & "/" & sday
	sFormatAsDateExpr = sCol & " >= {d '" & dtBegin & "'}" & _
								" AND " & sCol & " < {d '" & dtEnd & "'}"
	
End Function



'Function sFormSelectClause(dState, aprops)
'Input: dState - Commerce Dictionary For State
'       aprops - array containing key attribute values For PROPERTYs in the XML PROFILE
'Output: String
'Function that forms the "SELECT A, B, C, FROM X" clause of the main listview
'query based on the display column information in the columns.xml configuration
'file (accessed in oXMLGetDisplayColumns function) and the Profile XML.
'Function returns a string with the SELECT Clause
Function sFormSelectClause(dState, aprops)
'Form the main select clause For UM List display. Should only do this once per session, after which
'result is cached, because it should not change. User can stop and restart browser If need be.
    Dim oXMLCols, oXMLCol, stmp, sSep, sSelect, imatch, sColName, sPropName
    sSep = ""
    sSelect = "SELECT "
    set oXMLCols = oXMLGetDisplayColumns
    For each oXMLcol in oxmLcols
        sPropName = oXMLcol.attributes.GetNamedItem("name").value
        sColName = sGetProviderColumnName(sPropName)
        sSelect = sSelect & sSEP & sBracket(sColName)
        sSep = ", "
    next
    dState.Value("Table") = sGetPropProfile(aprops(1))
    sFormSelectClause = sSelect & " FROM " & sBracket(sGetPropProfile(aprops(1)))
   
End Function        

'Function bValidString(str)
'Input: str -- string
'Returns: True or False
'Checks to see If a String is null or empty
'Returns true If neither of these conditions are true, false If either is true.
Function bValidString(str)
	Dim bval
	bval = False
	If NOT IsNull(str) Then
		If len(str) > 0 Then
			bval = true
		End if
	End if
	bValidString = bval
End Function

'Function sFormatAsDateTime(sStr)
'Input: sStr -- a string that may or may not be a date.
'Function to format dates For the ADO Provider
'Takes a string as input. If the string is not a date, function returns
'empty string.
'If string is a date, function separates it into its individual date and time
'components and reconstructs it the way the ADO provider expects:
'"year/month/day hour:minute:second"
Function sFormatAsDateTime(sStr)
	Dim syear, smonth, sday, shour, sminute, ssec, dtTime, dtDate, sVal
	If NOT bValidString(sStr) Then
		sFormatAsDateTime = ""
		Exit Function
	End if	
	If NOT isDate(sStr) Then
		sFormatAsDateTime = ""
		Exit Function
	End if	
	syear = year(CDate(sStr))
	smonth = month(CDate(sStr))
	sday = day(CDate(sStr))
	shour = hour(CDate(sStr))
	sminute = minute(CDate(sStr))
	ssec = second(CDate(sStr))
	sFormatAsDateTime = syear & "/" & smonth & "/" & sday & " " & shour & ":" & sminute & ":" & sSec	               
End Function



'Private Function bIsIDInt(aprops, aIDCol)
'Inputs: aprops -- an array containing key values of attributes of all the PROPERTYs For current
'           PROFILE XML
'       aIDCOl -- array containing the name and element # (in aprops array) of the Identity key for
'           this PROFILE
'Returns: True or False
'Determines whether the ID Column (whose number in aprops array and name are in
'aIDCol) is an integer or not.
Private Function bIsIDInt(aprops, aIDCol)
    Dim sType
    sType = sGetPropType(aprops(nGetIDColNo(aIDCol)))
    bIsIDInt= bIsIntType(stype)
End Function

'Function bExecWithErrDisplay(rs, cn, sQuery, sErrMsg)
'Inputs: rs - recordset
'cn - connection
'sQuery - SQL Query string
'Returns: True If no error occurred during query, false otherwise
'
'Main function For executing queries through the ADO provider. Traps any errors
'that occur during execution of the query. If an error occurs, it prints out
'relevant information through the response object along with the query that
'caused the problem.
Function bExecWithErrDisplay(rs, cn, sQuery, sErrMsg)
'Try to trap bad query errors and not have them crash the page
	Dim  bRet
	bRet = bExecWithErrOrAbortDisplay(rs, cn, sQuery, sErrMsg)
	bExecWithErrDisplay = bRet
End Function



'Function bExecWithErrOrAbortDisplay(rs, cn, sQuery, sErrMsg)
'Inputs: rs - recordset
'cn - connection
'sQuery - SQL Query string
'sErrMsg - Error Message
'Returns: True If no error occurred during query, false otherwise
'
'Main function For executing queries through the ADO provider. Traps any errors
'that occur during execution of the query. If an error occurs, it prints out
'relevant information through the response object along with the query that
'caused the problem.
Function bExecWithErrOrAbortDisplay(rs, cn, sQuery, sErrMsg)
    'Try to trap bad query errors and not have them crash the page
	Dim bErr
	Dim sErrNum
	Dim sErrDetail
	Dim conErr
	Dim iCount
	Dim pos
	bErr = True
	iCount = 0
	On Error resume next
	Err.Clear

	Set rs = cn.Execute(sQuery)
	conErr = cn.Errors.Count  
	If Err.number <> 0 or conErr > 0   Then
		sErrNum = Hex(Err.number)
		sErrDetail = ""
		If (sErrNum = "80040E14") or (sErrNum = "80071392") or (sErrNum = "C1003C0F")  Then
		        sErrDetail = L_Duplicate_ErrorMessage
		End if
		' ************************
		' Since the provider does not give back unique 
		' errors in the err.number field, we have to trap for specific errors in the 
		' description field in the err object.  Jayram says this will not change in this
		' release but may change at another time.  so I've trapped the connection failure 
		' error code and the inserting duplicate user failure.  other failures will just return
		' the entire err.description field.
		' ************************
		pos = Instr(Err.Description, "1056949233")
		If pos > 0 Then
			' Duplicate insertion error
			sErrDetail = L_Duplicate_ErrorMessage
		End If
		
		pos = Instr(Err.Description, "2147467259")
		If pos > 0 Then
			' connection error
			sErrDetail = L_Connection_ErrorMessage
		End If 
		
		If sErrDetail = "" Then
		Do While iCount < conErr 
		   sErrDetail = sErrDetail & " " & cn.errors(iCount).Description
		   'sErrDetail = Err.Description
		   iCount = iCount + 1
		Loop   
		End If
		
	    Session("ClearQuery") = 1
		bErr = False
		Call SetError(sFormatString(L_PROFILE_DialogTitle, Array(L_ErrorMessageTitle_Text)), _ 
		     			sErrMsg, _
						sErrDetail, ERROR_ICON_CRITICAL)
		Err.Clear
	
	
	End if
	On error goto 0
	bExecWithErrOrAbortDisplay = bErr
End Function

'Function bSafeExec(rs, cn, sQuery)
'Inputs: rs - recordset
'cn - connection
'sQuery - SQL Query string
'Returns: True If no error occurred during query, false otherwise
'
'Main function For executing queries through the ADO provider. Traps any errors
'that occur during execution of the query. If an error occurs, it prints out
'relevant information through the response object along with the query that
'caused the problem.
Function bSafeExec(rs, cn, sQuery)
'Try to trap bad query errors and not have them crash the page
  	Dim bErr
	Dim conErr
	Dim sErrNum 
	Dim sErrMsg
	bErr = True
	On Error resume next
	Err.Clear
	
	set rs = cn.Execute(sQuery)
	conErr = cn.Errors.Count
	
	If Err.number <> 0 or conErr > 0  Then
		sErrNum = Hex(Err.number)
		sErrMsg = Err.description 
			If sErrNum = "C1003C0C" Then 
				sErrMsg = L_UnsupportedFeature_ErrorMessage
				Session("ErrorMessage") = sErrMsg
			Else
				sErrMsg = sErrMsg & L_BadConnection_ErrorMessage
				Session("ErrorMessage") = sErrMsg
			End If  
		bErr = False
	Else
		Session("ErrorMessage") = ""	    
   	End If
	
	bSafeExec = bErr
	On Error Goto 0

End Function


Function bRecordExec(rs, cn, sQuery)
	Dim bErr
	Dim conErr
 	Set rs = Server.CreateObject("ADODB.Record")
	bErr = True
	On Error Resume Next
	Err.Clear
	rs.Open sQuery, cn
	
	conErr = cn.Errors.Count
	If Err.number <> 0  Then
	    	Call SetError(sFormatString(L_PROFILE_DialogTitle,  Array(L_ErrorMessageTitle_Text)), _ 
							Err.description , _
							sGetUserScriptError(Err), ERROR_ICON_CRITICAL)
			bErr = False				
		
	End If
	
	bRecordExec = bErr
'	bRecordExec = False
	On Error Goto 0

End Function


'-----------------------------------------------------------------------------
'Function sGetProfileDSN(dState)
'Input: dState -- State Commerce Dictionary
'Returns: Connection string For Biz Data Service
Function sGetProfileDSN()
	sGetProfileDSN = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")
End Function

'-----------------------------------------------------------------------------
'Function sGetProfileConnStr()
'Input: None
'Returns: Connection string For Biz Data Service
Function sGetProfileConnStr()
	sGetProfileConnStr = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")
End Function


'Wrapper function. Don't know where we're supposed to get this from, but don't want to 
'completely hard code it at present. Leaves us freedom to query some object For this name.
Private Function sGetCatalogName
    sGetCatalogName = "Profile Definitions"
End Function 


'Function sGetProviderDSN(dState)
'Input: dState -- Commerce Dictionary For State
'Returns: connection string For OLEDB Provider
Function sGetProviderDSN(dState)
	'Function that parses Biz Data Service DSN to get server name, database, uid, password and
	'uses it to create connection string For ADO provider.
	'Connection String For BizDataService will be of form:
	'Provider=SQLOLEDB;Server=A-GREGGR1B;Database=commerce3;uid=sa;pwd=
	'After parseing and turns it into form:
	'"URL=mscop://InProcConnect/server=a-greggr1b:catalog=Profile Definitions:database=commerce3:user=sa:password=:"
	Dim sBDAODSN, sServerName, sCatalogName, sDatabase, sUser, sPwd, sDSN
	sBDAODSN = dState("ProfileDSN")
	sServerName = sGetDSNArg(sBDAODSN, "Server=")
	sUser = sGetDSNArg(sBDAODSN, "uid=")
	sDatabase = sGetDSNArg(sBDAODSN, "database=")
	spwd = sGetDSNArg(sBDAODSN, "pwd=")
	sCatalogName = sGetCatalogName

	sDSN = "provider=commerce.dso.1;data source=mscop://INPROCCONNECT/Server=" & sServerName & ":Catalog=" & sCatalogName
	sDSN = sDSN & ":Database=" & sDatabase & ":User=" & sUser & ":Password=" & sPWD & ":"

	sGetProviderDSN = sDSN
End Function

'Function sGetProviderConnStr()
'Input: None
'Returns: connection string For OLEDB Provider
Function sGetProviderConnStr()
    Dim sProvConnStr
    sProvConnStr = GetSiteConfigField("Biz Data Service", "s_CommerceProviderConnectionString")
	sGetProviderConnStr = sProvConnStr
End Function



'Function sGetDSNArg(sDSN, sArg)
'Inputs: sDSN, sArg -- strings
'Returns: value For Argument sArg in Provider string sDSN
'Function used by sGetProviderDSN to extract arguments and values from the Biz Data
'Service DSN. Expects sDSN to be a string of form 
'"Provider=SQLOLEDB;Server=A-GREGGR1B;Database=commerce3;uid=sa;pwd="
'When argument is passed in sArg, function returns the argument value
'If argument is not found in sDSN, returns empty string.
'Ex: For above string (as sDSN), If sArg="uid", function will return "sa".
'Search For argument is case-insensitive.
Function sGetDSNArg(sDSN, sArg)
	Dim ist, iend, sDelim, nDSNLen, nArgLen
	sDelim = ";"
	nArglen = len(sArg)
	nDSNLen = len(sDSN)
	ist = Instr(1, sDSN, sArg,1)
	If ist > 0 Then
		ist = ist + nArgLen
		iEnd = Instr(ist, sDSN, sDelim)
		If iEnd > 0 Then
			sGetDSNArg = Mid(sDSN, ist, iend-ist)
		Else
			sGetDSNArg = Mid(sDSN, ist, nDSNLen - ist + 1)
		End if
	Else
		sGetDSNArg = ""
	End if
End Function

'Function cnGetProviderConnection(dState)
'Input: dState - Commerce Dictionary For state
'Returns: ADO Connection to Provider
'Wrapper function For getting connection to ADO provider.
'Originally designed For creating the connection once and storing it in a Session
'variable, but ADO provider only allows one successful query If one does this.
'So only way around this was to create a new connection each time and destroy it
'after every query.

Function cnGetProviderConnection(dState)
    Dim cn
    Dim bflag
    
    If Session("MSCSProfileSchemaVer")= Application("MSCSProfileSchemaVer") Then 
      bflag = False
    Else
      bflag = True
    End If   
    
    On Error Resume Next
	Err.Clear
	
	If IsEmpty(Session("MSCSProviderConn")) or bflag = True or IsEmpty(bflag)   Then 
     
		set cn = Server.CreateObject("ADODB.Connection")
		If (cn.State = AD_STATE_CLOSED) Then
			
			If IsEmpty(Session("ProviderConnStr")) Then
				cn.Open dState.Value("ProviderDSN")
				Session("ProviderConnStr")= dState.Value("ProviderDSN")
			Else
				cn.Open CStr(Session("ProviderConnStr"))
			End if
			If Err.number <> 0 Then
			    
			rem Call ShowErrDlg?
			    call Abort(L_BadConnection_ErrorMessage) 
			'	Response.Write L_BadConnection_ErrorMessage
			'	Response.End
			End If
			End if
		set cnGetProviderConnection = cn
		set Session("MSCSProviderConn") = cn
		
		If bflag = True or IsEmpty(bflag) Then 
			Session("MSCSProfileSchemaVer")= Application("MSCSProfileSchemaVer")
		End If 	
	Else
	    set cnGetProviderConnection = Session("MSCSProviderConn")
	End If    
	Err.Clear
	On Error Goto 0	

		
End Function



'Function cnGetProviderConnection()
'Input: None
'Returns: ADO  Connection to Provider
'Wrapper function For getting connection to ADO provider.

Function cnGetProviderConn()
    Dim cn
    Dim bflag
    Dim connStr
    
    If Session("MSCSProfileSchemaVer")= Application("MSCSProfileSchemaVer") Then 
      bflag = False
    Else
      bflag = True
    End If   
  ' If bflag = True Then Schema has changed and the connection needs to be reloaded.
    
    On Error Resume Next
	Err.Clear
	
	If IsEmpty(Session("MSCSProviderConn")) or bflag = True or IsEmpty(bflag)   Then 
     
		set cn = Server.CreateObject("ADODB.Connection")
		If (cn.State = AD_STATE_CLOSED) Then
			
			If IsEmpty(Session("ProviderConnStr")) Then
			    connStr =  sGetProviderConnStr()
				cn.Open connStr
				Session("ProviderConnStr")= connStr
			Else
				cn.Open CStr(Session("ProviderConnStr"))
			End if
			If Err.number <> 0 Then
			    
			rem Call ShowErrDlg?
			    call Abort(L_BadConnection_ErrorMessage) 
			'	Response.Write L_BadConnection_ErrorMessage
			'	Response.End
			End If
			End if
		set cnGetProviderConn = cn
		set Session("MSCSProviderConn") = cn
		
		If bflag = True or IsEmpty(bflag) Then 
			Session("MSCSProfileSchemaVer")= Application("MSCSProfileSchemaVer")
		End If 	
	Else
	    set cnGetProviderConn = Session("MSCSProviderConn")
	End If    
	Err.Clear
	On Error Goto 0	

		
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function sGetAttribute(oXMLNode, sAttrName)
'
'	returns:	attribute value
'
'	oXMLNode		-XMLNode Element.
'	sAttrName - string.
'
 Function sGetAttribute(oXMLNode, sAttrName)
	Dim oXMLAttributes, oXMLTemp, sRetval
	sRetval = ""
	If NOT (oXMLnode IS Nothing) Then
		set oXMLAttributes = oXMLnode.attributes
		If (oXMLAttributes.length > 0) Then
			set oXMLTemp = oXMLAttributes.getnameditem(sAttrName)		
			If NOT (oXMLTemp IS Nothing) Then
				sRetVal = CStr(oXMLTemp.nodevalue)
			End if
		End if
	End if
	sGetAttribute = sRetval
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub ValidNode(oXML, sMessage)
'
'	Purpose: check to see If an XMLNode or object returned from a search is an 
'				actual node.If the node is valid (i.e., it is a valid XML object) routine 
'				does nothing. Otherwise calls abort (basically response.end), indicates
'				an XML parsing error,and displays the message. In most cases, If the node 
'				is not returned from a search,this error would keep the pages from working,
'				so may as well shut them down immediately.
'				Forestalls an XMLDOM error If processing were done on a nonexistent node.
'	oXML		- XMLNode (could also be another XML object)
'	sMessage - Error Message to display
'
Sub ValidNode(oXML, sMessage)
	If (oXML is Nothing) or (IsEmpty(oXML)) Then
	    'Omar Work on Errors... here to throw the abort.asp
		Response.Write L_SearchProfile_ErrorMessage
		call abort(sMessage)
	End if
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub Abort(message)
'	Uses Response to display message on page and Then calls Response.End
'	
'	sMessage - Error Message to display
'
Sub Abort(message)
    Response.Redirect "abort.asp?AbortMsg=" & message
    Response.End
End Sub



'------------------------
'Accessor Functions For Properties contained in the aprops array
'--------------------------

Function sGetIDName(aIDCol)
     sGetIDName = aIDCol(1)
End Function

Function nGetIDColNo(aIDCol)
    nGetIDColNo = aIDCol(2)
End Function

Function nGetPropGroupNo(aValues)
    nGetPropGroupNo = aValues(0)
End Function

Function sGetPropID(aValues)
    sGetPropID = aValues(1)
End Function

Function sGetPropName(aValues)
    sGetPropName = aValues(2)
End Function

Function sGetDisplayName(aValues)
    sGetDisplayName = aValues(12)
End Function

Function bIsPropRequired(aValues)
    bIsPropRequired = aValues(3)
End Function

Function sGetPropRefString(aValues)
    sGetPropRefString = aValues(5)
End Function

Function sIsPropMultiValued(aValues)
    sIsPropMultiValued = aValues(6)
End Function

Function nGetPropMVOffset(aValues)
    nGetPropMVOffset = aValues(7)
End Function

Function sGetPropProfile(aValues)
    sGetPropProfile = aValues(8)
End Function

Function bIsPropJoinKey(aValues)
  If aValues(9) = "JOIN" or aValues(9) = "PRIMARYJOIN" Then
		bIsPropJoinKey =1
	Else
		bIsPropJoinKey =0
	End If	
End Function

Function bIsPropPrimaryKey(aValues)
  If aValues(9) = "PRIMARY" or aValues(9) = "PRIMARYJOIN" Then
		bIsPropPrimaryKey =1
	Else
		bIsPropPrimaryKey =0
	End If	
End Function


Function bIsPropUniqueKey(aValues)
  If Len(Trim(aValues(9))) > 0 Then
		bIsPropUniqueKey = 1
	Else
		bIsPropUniqueKey = 0	
	End If	
End Function


Function bIsPropReadOnly(aValues)
    bIsPropReadOnly = aValues(11)
End Function

Function bIsPropUpdatable(aValues)
    bIsPropUpdatable = aValues(10)
End Function

Function bIsPropActive(aValues)
    bIsPropActive = aValues(13)
End Function

Function sGetPropType(aValues)
    sGetPropType = aValues(4)'also aValues(16)
End Function

Function sGetPropFullName(aValues)
    sGetPropFullName = aValues(14)
End Function

Function bIsPropMappedToData(aValues)
    bIsPropMappedToData = aValues(1)
End Function



'-----------------------------------------

'Function sGetFullPathName(oXMLProp, bBrackets)
'Input:
'   oXMLProp - XMLNode structure For PROPERTY tag
'   bBrackets - whether to enclose returned full path in "[]"
'Returns the fully qualified path For a property from the xml For the provider/profile
'service. Example For Profile=UserObject, Group=GeneralInfo, Property=UserID,
'would return "GeneralInfo.UserID" If bBrackets is false
'"[GeneralInfo.UserID]" If bBrackets is true
Function sGetFullPathName(oXMLProp, bBrackets)
	Dim sFullName, sName, oXMLParent, oXMLAttr
	sFullName = oXMLProp.attributes.GetnamedItem(sXMLAttrName).Nodevalue
	set oXMLParent = oXMLProp.parentNode
	do while (strcomp(oXMLParent.NodeName, sXMLTagProfile, 0) <> 0)
	    set oXMLAttr = oXMLParent.attributes.GetNamedItem(sXMLAttrVirtualGroup)
	    If (strcomp(oXMLParent.NodeName, sXMLTagGroup, 0) = 0) AND _
	        (IsNull(oXMLAttr) OR (oXMLAttr is nothing)) Then 
		'Line above excludes the top "virtual" group, If present, as ADO Provider will not work If this name is
		'included in the path
		    sName = oXMLParent.attributes.GetnamedItem(sXMLAttrName).Nodevalue
		    sFullName = sName & "." & sFullName
		End if
	    set oXMLParent = oXMLParent.ParentNode
	loop
	If bBrackets Then
		sGetFullPathName = "[" & sFullName & "]"
	Else
		sGetFullPathName = sFullName
	End if
End Function

'Function sGetParentProfileName(oXMLProp)
'Input: oXMLProp - XMLNode For a PROPERTY tag.
'Returns name of parent PROFILE tag.
Function sGetParentProfileName(oXMLProp)
	Dim oXMLParent
	set oXMLParent = oXMLProp.parentNode
' loop over the parents until a PROFILE tag is found
	do while (strcomp(oXMLParent.NodeName, sXMLTagProfile, 0) <> 0)
		set oXMLParent = oXMLParent.ParentNode
	loop
'GR6/1/99 need better error handling in case XML is messed up. This would loop until crash If the
'PROFILE tag were missing or misspelled.
'return the name attribute value of the PROFILE tag.
	sGetParentProfileName = oXMLParent.attributes.GetNamedItem(sXMLAttrName).NodeValue
End Function

'Sub GetGroupAttr(oXMLGroup, ipropcnt, agroup)
'Inputs:
'   oXMLGroup - XMLNode For the GROUP tag
'   ipropcnt - the number of properties in this group
'Output:
'   agroup - an array of arrays of key group attribute values
'   agroup(i)(0) display name of group
'   agroup(i)(1) no. of subroups
'   agroup(i)(2) is the name of the group
'   agroup(i)(3) tells whether the group is required
'   agroup(i)(4) is a counter of properties in the group
'   agroup(i)(5) is the number of childnodes For the group
'
Sub GetGroupAttr(oXMLGroup, ipropcnt, agroup, nSubgroups)
	Dim agrptmp(10)
	Dim itmp
	Dim oXMLProps , str
	
	str ="./" & sXMLTagProperty
	'agrptmp(1) = oXMLgroup.attributes.GetNamedItem(sXMLAttrID).value
	agrptmp(1) = nSubgroups
	agrptmp(0) = sGetAttribute(oXMLgroup,sXMLAttrDisplayName)
	agrptmp(2) = sGetAttribute(oXMLgroup,sXMLAttrName)
	itmp = sGetAttribute(oXMLgroup,sXMLAttrIsRequired)
	agrptmp(3) = False
	If Len(Trim(itmp)) > 0 Then
		If itmp = 1 Then _
		agrptmp(3) = True
	End if
	agrptmp(4) = ipropcnt+1
	Set oXMLProps = oXMLGroup.SelectNodes(str)
	agrptmp(5) = oXMLProps.length
	agroup = agrptmp
End Sub



'Private Function oGetBizDataManager
'Wrapper Function. Hides details of how we get the BizDataManager. So now If we want to
'we can put logic here that stores it somewhere and retrieves it in between page redirection
'Returns BizDataAdmin object.
Private Function oGetBizDataManager
    Dim otmp
    On Error Resume Next
    Err.Clear
    set otmp = Server.CreateObject("Commerce.BusinessDataAdmin")
    If Err.number <> 0 OR (otmp is nothing) Then
        abort (L_BizDataInstantiation_ErrorMessage)
    End if
    Err.Clear
    On Error Goto 0
    set oGetBizDataManager = otmp
    Set otmp = Nothing
End Function


'Sub ConnectBizDataManager(oBDManager, sProfileDSN)
'Wrapper routine For connecting the BizDataAdmin object to the BizDataStore
'Inputs:
'   oBDManager - BizDataAdmin Object
'   sProfileDSN - Connection string to the current site's Biz Data Store
'
Sub ConnectBizDataManager(oBDManager, sProfileDSN)
    On Error Resume Next
    Err.Clear
    oBDManager.Connect CStr(sProfileDSN)
    If Err.number <> 0 Then
        call DiscardBizDataManger(oBDManager)
    
        If Session("ListPageCall")=True Then 
            Call ThrowWarningDialog(L_NoDataStoreConn_ErrorMessage & L_RefreshConnection_Text)
            Response.End
        Else    
            abort (L_NoDataStoreConn_ErrorMessage & L_RefreshConnection_Text)
        End If    
    End if
    Err.Clear
    On Error Goto 0
End Sub

'Sub DiscardBizDataManager(oBDManager)
'Wrapper routine For getting rid of BizDataAdmin object
'Input oBDManager - BizDataAdmin object
Sub DiscardBizDataManager(oBDManager)
    If NOT (nothing is oBDManager) Then
        set oBDManager = Nothing
     End if
End Sub

Function oXMLGetAuxProfile(sProf)
    Dim bdao
    set bdao = dGetBizDataObject()
    set oXMLGetAuxProfile = bdao.GetProfile(Cstr(sProf), CStr(Response.CharSet))
    set bdao = Nothing 
    
End Function


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Function oXMLWhichColsToDisplay(sRef)

Function oXMLWhichColsToDisplay(sRef)
    Dim oXMLCols, oXMLCol, oXMLConfigFile, sProfile
    set oXMLConfigFile = Server.CreateObject("MSXML.DomDocument")
    oXMLConfigFile.Load(Server.MapPath("columns.xml"))
    set oXMLCols = Server.CreateObject("MSXML.DomDocument")
	'Assume Reference name is of Form "Catalog.Profile", so to get Profile name, take everything 
	'to the right of the first period you find (If there is any). If no period, takes whole string.
    sProfile = Mid(sRef, InStr(1, sRef, ".")+1)
    set oXMLCols.DocumentElement = oXMLConfigFile.DocumentElement.SelectSingleNode(".//" & _
        sXMLTagProfile & "[@" & sXMLAttrName & "='" & sProfile & _
        "']/COLUMNS").CloneNode(True)
    set oXMLConfigFile = Nothing
    set oXMLWhichColsToDisplay = oXMLCols
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sub GetProfileObject(oXMLDoc, dState)
'
'	load the PROFILE XML structure into oXMLDoc. get the value of ProfileDSN from
'	the state dictionary,dState
'	
'	oXMLDoc	- DOMDocument object containing PROFILE XML structure
'	dState	- Commerce Dictionary
'
Sub GetProfileObject(oXMLDoc, dState)
   
   Dim obizdata, sProfileDSN, sProfileName
   	    
   sProfileDSN =  dState.Value("ProfileDSN")
   sProfileName = dState.Value("ProfileName")
	
	If (Session("MSCSUserXmlVer") <> Application("MSCSUserXmlVer")) or Not IsObject(Session(sProfileName)) Then
	    On Error Resume Next
	    Err.Clear	    
		set obizdata = oGetBizDataManager
		call ConnectBizDataManager(obizdata, sProfileDSN)
			
		set oXMLdoc = obizdata.GetProfile(Cstr(sProfileName), Response.charset)
		If Err.number <> 0 Then
			abort(L_LoadProfile_ErrorMessage & Err.source & " " & Hex(Err.Number) & ": " & Err.description )
		End If 
		    
		Call ValidNode(oXMLDoc, L_XMLLoadFile_ErrorMessage) 
		
		Set Session(sProfileName)= oXMLdoc
	'	Session("MSCSUserXmlVer")= Application("MSCSUserXmlVer")
		Set obizdata = Nothing
		On Error Goto 0
	 Else
        Set oXMLdoc = Session(sProfileName)
	 End if	
		
	
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sub GetProperties(aprops, agroups, oXMLDoc, dState)
'
'	fill aprops/agroups arrays with property/group attributes in an array of 
'	arrays. load the PROFILE XML structure into oXMLDoc 
'	
'	aprops	- ByRef array of arrays For PROPERTY tag attribute values
'	agroups	- ByRef array of arrays For GROUP tag attribute values
'	oXMLDoc	- ByRef DOMDocument object containing PROFILE XML structure
'	dState	- Commerce Dictionary
'
sub GetProperties(aprops, agroups, oXMLDoc, dState)

	Dim oXMLCatalog, oXMLprofile, sProfileName
	Dim oXMLGroupCont, sCatName
	Dim ntotprops,igroupno,ipropcnt,iMVCnt
	
	rem first create an XMLDOM object and load the Current Profile schema in it
	Session("ListPageCall")=False
	Call GetProfileObject(oXMLDoc, dState)
	
	rem now separate out the of the XML structure starting with the 
	rem catalog tag and put it into oXMLCatalog
	sCatName = sGetCatalogName
	call GetCatalog(oXMLDoc, oXMLCatalog, sCatName)

	rem get the profile tag with the desired name in this catalog
	Call GetProfile(oXMLCatalog,oXMLProfile, sProfileName)

	rem Get all the property groups
	rem initialize the counts For groups and properties
	ntotprops = 0
	igroupno = 0
	ipropcnt = 0
	iMVCnt = 0
	Call GetBaseProps(oXMLProfile,sProfileName, agroups, aprops, ntotprops, igroupno,ipropcnt,iMVCnt)
	Call GetGroups(oXMLProfile,sProfileName, agroups, aprops,ntotprops,igroupno,ipropcnt,iMVCnt)

End Sub

'Sub GetBaseProps(oXMLTop, sProfileName, agroups, aprops, ntotprops, igroupno, ipropcnt, iMVCnt)
'One of the main XML Profile processing routines
'This routine goes through the profile and locates all the GROUPs below the profile
'For each group, calls the routine ProcessGroup to get at Properties
'***Important Right now routine assumes Properties will come directly below GROUPs,
'Routine will break For nesting of Groups
'Inputs:
'   oXMLTop - XML PROFILE node
'   icatalogid - CatalogID that the groups should have
'   iprofileid - ID For the Profile the Groups should belong to
'   agroups - Array of properties For the groups found
'   aprops - array of arrays of properties For the PROPERTY tags of each GROUP
Sub GetBaseProps(oXMLTop, sProfileName, agroups, aprops, ntotprops, igroupno, ipropcnt, iMVCnt)
	Dim oXMLProperties, str, sWhere, oXMLgroup, strProp
	
	'Locate all PROPERTY nodes of oXMLTop that are not virutal.	
	str = "./" & sXMLTagProperty
    
	set oXMLProperties = oXMLTop.SelectNodes(str)
	Call ValidNode(oXMLProperties, L_NoPropsinGroup_ErrorMessage)
	
	ntotprops = ntotprops + oXMLProperties.length
	If IsArray(aprops) Then
		ReDim Preserve aprops(ntotprops)
	Else
		ReDim aprops(ntotprops)	
	End If
	
	ReDim aGroups(1)
	Dim agroupsTmp(5)
	agroupsTmp(1) = 0 'no. of subGroups
    agroupsTmp(4) = 1'Starting element in aprops array
    agroupsTmp(5) = ntotprops 'Ending element in aprops array
    aGroups(0) = agroupsTmp
	If Not oXMLProperties Is Nothing Then
		Call GetGroupProperties(oXMLProperties, ipropcnt, igroupno, aprops,iMVCnt)
	End If	

End Sub

'Sub GetGroups(oXMLTop, iCatalogid, iProfileid, agroups, aprops)
'One of the main XML Profile processing routines
'This routine goes through the profile and locates all the GROUPs below the profile
'For each group, calls the routine ProcessGroup to get at Properties
'***Important Right now routine assumes Properties will come directly below GROUPs,
'Routine will break For nesting of Groups
'Inputs:
'   oXMLTop - XML PROFILE node
'   icatalogid - CatalogID that the groups should have
'   iprofileid - ID For the Profile the Groups should belong to
'   agroups - Array of properties For the groups found
'   aprops - array of arrays of properties For the PROPERTY tags of each GROUP
Sub GetGroups(oXMLTop, sProfileName, agroups, aprops, ntotprops, igroupno, ipropcnt, iMVCnt)
	Dim oXMLgroups, str, sWhere, oXMLgroup, strProp
	Dim oXMLSubGroups, oXMLProperties
	
    'Locate all children GROUP nodes of oXMLTop that are not virutal.	
	str = "./" &sXMLTagGROUP
    
	set oXMLgroups = oXMLTop.SelectNodes(str)
	Call ValidNode(oXMLgroups, L_GroupPropsNotFound_ErrorMessage)
	If igroupno = 0 Then
		ReDim Preserve agroups(oXMLgroups.length)
	Else
		ReDim Preserve agroups(UBound(agroups) + oXMLgroups.length)
	End If	
	strProp = "./" & sXMLTagProperty
	For each oXMLgroup in oXMLgroups
		set oXMLProperties = oXMLgroup.SelectNodes(strprop)
		ntotprops = ntotprops + oXMLProperties.length
	Next
	If IsArray(aprops) Then
		ReDim Preserve aprops(ntotprops)
	Else
		ReDim aprops(ntotprops)	
	End If	
    
	For each oXMLgroup in oXMLgroups
		igroupno = igroupno + 1
	    set oXMLSubgroups = oXMLgroup.SelectNodes(str)
	    
        call GetGroupAttr(oXMLgroup, ipropcnt, agroups(igroupno), oXMLSubgroups.length)
        set oXMLProperties = oXMLgroup.SelectNodes(strprop)
        call ValidNode(oXMLProperties, L_NoPropsinGroup_ErrorMessage & igroupno)
        call GetGroupProperties(oXMLProperties, ipropcnt, igroupno, aprops,iMVCnt)
		If oXMLSubgroups.length > 0 Then
			call GetGroups(oXMLGroup,sProfileName,agroups,aprops,ntotprops,igroupno,ipropcnt,iMVCnt)
		End If	
	Next
End Sub

'Sub GetGroupProperties(oXMLgroup, ipropcnt, igroupno, aprops, catalogid, profileid)
'Inputs:
'   oXMLGroup - XMLNode containing current group
'   ipropcnt - number of PROPERTY child tags in current group
'   igroupno - # of current group
'   catalogid - catalogid For current catalog, used as XSL search condition to make sure PROPERTIES
'               are of correct CATALOG
'   profileid - id For current profile, used as XSL search condition to make sure PROPERTIES
'               are of correct PROFILE
'Output:
'   aprops - array of arrays of PROPERTY attribute values
'   Routine to loop over all PROPERTIES For the current GROUP, extract key attribute values and place
'these values in the aprops array of arrays.
'The array of values For each aprops(i) is originally created in a temp array (aproptmp) For each 
'PROPERTY. Then at the End of the loop over properties, it is assigned to aprop(i), creating the
'array of arrays.
Sub GetGroupProperties(oXMLproplist, ipropcnt, igroupno, aprops, iMVCnt)
	Dim str, oXMLprop, aproptmp(15), itmp, oXMLAttr, oXMLAttrs
	Dim sAName, sAVal
'   Basically, aprops(i) is an array of property values For the ith column (PROPERTY tag in order that they
'   occur in the XML Profile) to be displayed
'	aprops(i)(0) - groupno. property belongs to
'	aprops(i)(1)- IsMapped to datacolumn
'	aprops(i)(2) - propertyName
'	aprops(i)(3) - IsRequired
'	aprops(i)(4) - Type of property
'	aprops(i)(5) - Attribute reference string
'	aprops(i)(6) - IsMultivalued
'	aprops(i)(7) - count of properties actually being displayed
'	aprops(i)(8) - parent ProfileName
'   aprops(i)(9) - keyDef value
'	aprops(i)(10) - IsUpdateable
'	aprops(i)(11) - IsReadOnly
'   aprops(i)(12) - Display name
'	aprops(i)(13) - IsActive
'   aprops(i)(14) - contains the full path name of the property in the XML. For example, For UserID in
'						 the UserObject Profile, the path is "GeneralInfo.UserID"
'	aprops(i)(15) - 
	Dim oXMLDataRef

	For each oXMLprop in oXMLproplist
		ipropcnt = ipropcnt + 1
		aproptmp(0) = igroupno

		rem To check If the property is mapped to a data column
		str = "./" & sXMLTagDataRef
		Set oXMLDataRef = oXMLprop.SelectNodes(str)
		If oXMLDataRef.length > 0 Then
			aproptmp(1) = True
		Else
			aproptmp(1) = False
		End If	
		rem Property Name
		aproptmp(2) = sGetAttribute(oXMLprop,sXMLAttrName)
		
		rem IsRequired
		itmp = sGetAttribute(oXMLprop,sXMLAttrIsRequired)
		aproptmp(3) = False
		If itmp = "1" Then
			aproptmp(3) = True
		End if
		
		rem Property type
		aproptmp(4) = sGetAttribute(oXMLprop,sXMLAttrTypeDefName)
		rem Attribute reference string
		aproptmp(5) = sGetAttribute(oXMLprop,sXMLAttrReference)
		
		rem IsMultiValued
      itmp = sGetAttribute(oXMLProp, sXMLAttrIsMultiValued)
      aproptmp(6) = False
      If (itmp = "1") Then
          aproptmp(6) = True
      End if
      'If aproptmp(6) or aproptmp(1) = False Then
      If aproptmp(1) = False Then
          iMVCnt = iMVCnt + 1
      End if
      rem count of properties actually being displayed
      aproptmp(7) = ipropcnt - iMVCnt
		rem parent ProfileName
		aproptmp(8) = sGetParentProfileName(oXMLProp)
		rem KeyDef Value	
		aproptmp(9) = sGetAttribute(oXMLProp,sXMLAttrKeyDef)
		rem IsUpdateable
		aproptmp(10) = True
		itmp = sGetAttribute(oXMLprop,sXMLAttrIsUpdatable)
		If (itmp = "0") Then aproptmp(10) = False
      rem IsReadOnly 
      itmp = sGetAttribute(oXMLprop,sXMLAttrIsReadOnly)
      aproptmp(11) = False
      If (itmp = "1") Then aproptmp(11) = True
		rem DisplayName	
		aproptmp(12) = sGetAttribute(oXMLprop,sXMLAttrDisplayName)
		rem IsActive
		aproptmp(13) = sGetAttribute(oXMLprop,sXMLAttrIsActive)
		If (aproptmp(13) = "0") Then
		   aproptmp(13) = False
		Else
			aproptmp(13) = True
		End If 
		'Gets the full path of the PROPERTY tag without enclosing square brackets. This is the name that needs
		'to be passed to the ADO provider For each property.        
		aproptmp(14) = sGetFullPathName(oXMLProp, False)
		aprops(ipropcnt) = aproptmp
	Next
End Sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub GetCatalog(oXML, oXMLcatalog,sCatName)
'
'	returns: ADO connection object
'
'	oXML		-DOMDocument. contains the entire ProfileXML structure
'	sCatName - string. catalog name being queried for
'	oXMLcatalog -DOMDocument. output contains catalog node.
'
Sub GetCatalog(oXML, oXMLcatalog,sCatName)
	set oXMLcatalog = oXML.DocumentElement.SelectSingleNode("./" & sXMLTagCatalog & _
	    "[@" & sXMLAttrName & "='" & sCatName & "']")
	Call ValidNode(oXMLcatalog, sCatName & L_CatalogNotFound_ErrorMessage)
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	Sub GetProfile(oXMLcatalog, oXMLprofile, sName)
'
'	returns: ADO connection object
'
'	oXML		-DOMDocument. contains the entire ProfileXML structure
'	sName		- string. Name of profile
'	oXMLprofile -DOMDocument. XMLNode For the profile
'
Sub GetProfile(oXMLcatalog, oXMLprofile, sName)
     
	Dim str, bNoName
	Dim oXMLattr
	str = "./" & sXMLTagProfile
	bNoName = NOT (len(trim(sName)) > 0)
	'add clause to XSL pattern only If name is non-blank
	If (NOT bNoName) Then
		str = str & "[@" & sXMLAttrName & "='"&name&"']"
	End if
	set oXMLprofile = oXMLcatalog.SelectSingleNode(str)
	Call ValidNode(oXMLprofile, sName & L_ProfileNotFound_ErrorMessage )
	set oXMLattr = oXMLprofile.attributes
	Call ValidNode(oXMLattr, L_AttributeLoad_ErrorMessage)
	If (bNoName) Then
		sName = oXMLattr.GetNamedItem(sXMLAttrName).value
	End if
	
End Sub

'Sub DetermineIDColumn(oXMLProfileDoc, aprops, aIDCol)
'Inputs:
'   oXMLProfileDoc - XMLNode For Profile
'   aprops - array of arrays For PROPERTY tag attribute values
'   aIDCol - array containing the name of the ID Column (aIDCol(1)) and its position (aIDCol(2)) in the aprops array. I.e., if
'       aIDCol(2) = j, Then aprops(j) contains the values For the ID Column, aIDCOl(1) gives the name of the 
'       property that is the ID. 
Sub DetermineIDColumn(oXMLProfileDoc, aprops, aIDCol)
    Dim oXMLDoc, oXMLstart, oXMLColumn, sName, oXMLPK
    reDim aIDCol(2)
    set oXMLPK = oXMLProfileDoc.DocumentElement.SelectSingleNode(".//" & sXMLTagProperty & "[@" & _
     sXMLAttrKeyDef & "='JOIN'" & " $or$ @" & sXMLAttrKeyDef & "='PRIMARYJOIN']")
    call validNode(oXMLPK, L_NoPKColumn_ErrorMessage)  
    sName = oXMLPK.attributes.GetNamedItem(sXMLAttrName).value
    aIDCol(2) = nGetMatchingEntry(sName, aprops)
    aIDCol(1) = sGetPropName(aprops(aIDCol(2)))
    set oXMLPK = nothing
    set oXMLDoc = nothing
    set oXMLstart = nothing
End Sub

'Function bIsIntType(str)
'For determining If a PROPERTY is an integer.
'Input: str -- string containing the data type of the property
'Returns: True If str is "NUMBER", False otherwise
Function bIsIntType(str)
    If (strcomp(str, "NUMBER", 1)  = 0) Then
		bIsIntType = True
	Else
		bIsIntType = False
	End if
End Function

'Function bIsDateType(str)
'For determining If a PROPERTY is a date.
'Input: str -- string containing the data type of the property
'Returns: True If str is "DATETIME" or "DATE", False otherwise
Function bIsDateType(str)
    If (strcomp(Ucase(str), "DATETIME", 1) = 0) Or _
	   (strcomp(Ucase(str), "DATE", 1) = 0) Then
		bIsDateType = True
	Else
		bIsDateType = False
	End if
End Function

Function bIsTimeType(str)
    If (strcomp(Ucase(str), "TIME", 1) = 0) Then
		bIsTimeType = True
	Else
		bIsTimeType = False
	End if
End Function


'Function bIsCurrencyType(str)
'For determining If a PROPERTY is a date.
'Input: str -- string containing the data type of the property
'Returns: True If str is "CURRENCY", False otherwise
Function bIsCurrencyType(str)
    If (strcomp(str, "CURRENCY", 1)  = 0) Then
		bIsCurrencyType = True
	Else
		bIsCurrencyType = False
	End if
End Function

'Function bIsBoolType(str)
'For determining If a PROPERTY is a Bool.
'Input: str -- string containing the data type of the property
'Returns: True If str is "BOOL", False otherwise
Function bIsBoolType(str)
    If (strcomp(LCase(str), "bool", 1)  = 0) Then
		bIsBoolType = True
	Else
		bIsBoolType = False
	End if
End Function

'Function bIsFloatType(str)
'For determining If a PROPERTY is a Float.
'Input: str -- string containing the data type of the property
'Returns: True If str is "FLOAT", False otherwise
Function bIsFloatType(str)
    If (strcomp(str, "FLOAT", 1)  = 0) Then
		bIsFloatType = True
	Else
		bIsFloatType = False
	End if
End Function

'Function sQuoteArg(sval, stype)
'Function to hide logic about whether an argument in a SQL query should be quoted.
'Inputs: 
'   sval - value of column in SQL query
'   stype - data type of values in column
'Returns: sval If argument is number

Function sQuoteArg(sval, stype)
	If strComp(sval,"Null",1) = 0 Then
		sQuoteArg = CStr(sVal)
    ElseIf bIsIntType(stype) or  bIsBoolType(sType) Then
		sQuoteArg = CStr(sval)
	ElseIf bIsCurrencyType(sType) Then 	
	    sQuoteArg = sFormatAsCurrency(sval)
	ElseIf  bIsFloatType(sType) Then 	
	    sQuoteArg = sFormatAsFloat(sval)    
    ElseIf bIsDateType(sType) Then
		sQuoteArg = "'" & sFormatAsDateTime(sVal) & "'"
	ElseIf bIsTimeType(sType) Then 	
		sQuoteArg="'" & sFormatAsTime(sVal) & "'"
    Else
        sQuoteArg = "'" & BuildQuotedString(sval) & "'"
    End if
End Function


Function BuildQuotedString(str)
  Dim nIndex, nLen, sLeft, sRight, i
  Dim arrQuotes
  arrQuotes = Array("'")', """")
  For i = 0 To UBound(arrQuotes)
    nIndex = InStr(1, str, arrQuotes(i))
    While nIndex
      nLen = Len(str)
      sLeft = Left(str, nIndex)
      sRight = Right(str, nLen - nIndex)
      str = sLeft + arrQuotes(i) + sRight
      nIndex = InStr(nIndex + 2, str, arrQuotes(i))
    Wend
  Next
  BuildQuotedString = str
End Function

Function sFormatAsTime(sVal)
    Dim str
    str = "{t '" & sVal & ":00'}"
	sFormatAsTime = str
End Function

Function sFormatAsFloat(nNumber)
    Dim sDecSep
    sDecSep = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_SDECIMAL, g_DBDefaultLocale)
	sFormatAsFloat = CStr(g_MSCSDataFunctions.ConvertFloatString(CStr(nNumber), g_MSCSDefaultLocale))
	sFormatAsFloat= Replace(sFormatAsFloat,sDecSep,".")
End Function

Function sFormatAsInt(nNumber)
	sFormatAsInt = CStr(g_MSCSDataFunctions.ConvertNumberString(CStr(nNumber), g_MSCSDefaultLocale))
End Function

Function sFormatAsCurrency(nCurrency)
	Dim sDecSep
    sDecSep = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_SDECIMAL, g_DBDefaultLocale)
	sFormatAsCurrency = CStr(g_MSCSDataFunctions.ConvertFloatString(CStr(nCurrency), g_MSCSCurrencyLocale))
	sFormatAsCurrency= Replace(sFormatAsCurrency,sDecSep,".")
End Function

'Function nGetMatchingEntry(sStr, aprops)
'Inputs:
'   sStr - string containing name of a PROPERTY tag in PROFILE
'   aprops - array of arrays of PROPERTY attribute values
'Returns:
'   number of aprops array with name matching sStr
'Searches all entries in aprops array until it finds an entry with a name matching sStr.
'If function returns nmatch, means that aprops(nmatch) has values For PROPERTY whose name is sStr.
Function nGetMatchingEntry(sStr, aprops)
    Dim i, nmatch
    nmatch = 0
    For i = 1 to Ubound(aprops)
'GGR 4/19 changed aprops(i)(9) to aprops(i)(2) to allow user to specify profile names not db columns
        If (strcomp(sStr, sGetPropName(aprops(i)), 1) = 0) Then
            nmatch = i
            Exit For
        End if
    Next
    nGetMatchingEntry = nmatch
End Function



'Function sGetProviderColumnName(sPropName)
'Input: sPropName -- name of property
'Gets full path name For property with name sPropName from aprops array. Used in making SQL queries for
'the ADO Provider.
Function sGetProviderColumnName(ByVal sPropName)
    Dim aprops, sColName, i, bMatch
    aprops = Session(g_sPropsLabel)
    For i = 1 to Ubound(aprops)
        If (strcomp(sPropName, sGetPropName(aprops(i)), 1) = 0) Then
            sGetProviderColumnName = sGetPropFullName(aprops(i))
            Exit Function
        End if
    Next
    abort(sPropName & L_NoProperty_Errormessage)
    sGetProviderColumnName = NULL    
End Function

Function sGetProviderColumnType(ByVal sPropName)
    Dim aprops, sColName, i, bMatch
    aprops = Session(g_sPropsLabel)
    For i = 1 to Ubound(aprops)
        If (strcomp(sPropName, sGetPropName(aprops(i)), 1) = 0) Then
            sGetProviderColumnType = sGetPropType(aprops(i))
            Exit Function
        End if
    Next
    abort(sPropName & L_NoProperty_Errormessage)
    sGetProviderColumnType = NULL
End Function


'##############################################################
' NON XML functions
' Should consider moving to separate file
'##############################################################


Function sGetListDSN
	sGetListDSN = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
	'"DSN=commerce;DATABASE=commerce;UID=SA;PWD=;"
End Function

Function xmlFilterSitename(xmlOutput)
	
	Dim xmlNode
	Dim xmlEmptyRec
	
	Set xmlNode = xmlOutput.SelectSingleNode(".//record[" & _
							XML_ATTR_ORG_FRIENDLYNAME & " = '" & Application("MSCSSiteName") & "']" )
	If Not xmlNode Is Nothing and IsObject(xmlNode) Then
		xmlOutput.RemoveChild(xmlNode)
		If xmlOutput.ChildNodes.length = 0 Then
			Set xmlEmptyRec = xmlNode.cloneNode(False)
			xmlOutput.AppendChild(xmlEmptyRec)
		End if	
	End If

End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sub WriteBlankList()
'	
'	Subroutine that creates a blank list sheet widget. The columns For the 
'	list sheet are retrieved through the oXMLGetDisplayColumns function which 
'	reads a file "Columns.xml" in the users or organizations directory  
'	(i.e., this file is an external XML configuration file that tells the UI which 
'	columns of the user or org profile to display in the list.
'
sub WriteBlankList()
    Dim sXML, oXML, oXMLRec
    set oXML = oXMLMakeDocumentElement("document")
    set oXMLRec = oXML.CreateElement("record")
    oXML.DocumentElement.appendchild oXMLRec  
    sXML = "<xml id='profilesList'>" & vbCR
    Response.Write sXML & oXML.DocumentElement.xml & "</xml>" & vbCR
End sub



'-----------------------------
'Function sBracket(sStr)
'Function For wrapping brackets around a string, need to do For ADO provider PROPERTY names.
'Input:
'   sStr -- string to be enclosed in brackets
'Returns sStr enclosed by brackets.
Function sBracket(sStr)
	sBracket = "[" & sStr & "]"
End Function

'Function sUnBracket(sStr)
'Function to remove either a terminal or beginning bracket or both from a string.
'Used in storing GUIDs. Since they're stored as strings, they either always need to have enclosing brackets
'or need always not to have them or the comparisons will fail. So, choice was to always have brackets.
'This function is used to strip away any existing brackets on a GUID string. Then unbracketed GUID is 
'bracketed to make sure stored GUIDs always have brackets.
'Function created to resolve problems with import of GUIDs from ListManager lists, which did not seem to
'have the brackets around GUIDs.
Function sUnBracket(sStr)
	Dim ist, iend, sRes
	ist = Instr(1, sStr, "[")
	If (ist > 0) Then
		iEnd = Instr(1, sStr, "]")
		If iEnd > 0 Then
			sUnBracket = Mid(sStr, ist, iend-ist-1)
		Else
			sUnBracket = Right(sStr, len(sStr)-ist)
		End if
	Else
		sUnBracket = sStr
	End if
End Function

'Function sGetGUID()
'Function For generating a GUID (with enclosing curly brakcets.
Function sGetGUID()
	Dim oGenID
	If NOT IsObject(Session("GenID")) Then
		set oGenID = Server.CreateObject("Commerce.GenID")
		set Session("GenID") = oGenID
	Else
		set oGenID = Session("GenID")
	End if
	sGetGUID = sBracketGUID(oGenID.GenGUIDString())
End Function

Function sExtractCatalog(byVal sIn)
        Dim iend
        iEnd = Instr(1, sIn, ".")
        If iEnd > 0 Then
            sExtractCatalog = Mid(sIn, 1, iend-1)
        Else 
            sExtractCatalog = sIn
        End if
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	oXMLGetSiteTerms(sFullname, sPropName)
'		GetsSiteTerms if property is a SiteTerm .
'       Called from UserOrgEditFns.asp . Required for making the META for EditSheet
'       
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Function oXMLGetSiteTerms(sFullname, sPropName)
    Dim aNames, sSearch, iend, sGroups, sProp
    Dim bizdata, oXMLST, oXMLCatalog, oXMLTerms, sName, sValue, oXMLTerm
    Dim sXML, i, oXMLDoc, oXMLNode, oXMLChild, oXMLElement, oXMLText, bInvalid, sIDStr
    sIDstr = "@" & sXMLAttrName & "='"
    bInvalid = false
    
    sGroups = ""
	 
	 Rem Get the siteTerm. 
    If len(sFullName) > 0 Then
      aNames = split(sFullName, ".")
      iEnd = UBound(aNames)
    Else
		bInvalid = True
	 End if	

    If NOT bInvalid Then
        sSearch = ".//" & sXMLTagProperty & "[" & sIDStr & aNames(iend) & "']"
        On Error Resume Next
        Err.Clear        
        
        If (Session("MSCSSiteTermVer") <> Application("MSCSSiteTermVer"))  Then 
			set bizdata = dGetBizDataObject()
            set oXMLCatalog = bizdata.GetProfile("Site Terms.MSCommerce", CStr(Response.CharSet))
            set Session("SITETERMS_CAT") = oXMLCatalog
       '    Session("MSCSSiteTermVer") = Application("MSCSSiteTermVer")
            
        Else
           If Not IsObject(Session("SITETERMS_CAT")) Then 
				set bizdata = dGetBizDataObject()
				set oXMLCatalog = bizdata.GetProfile("Site Terms.MSCommerce", CStr(Response.CharSet))
				set Session("SITETERMS_CAT") = oXMLCatalog
		   Else	
				set oXMLCatalog = Session("SITETERMS_CAT")
		   End if
           
        End if
            
        If Err.number <> 0 Then
           abort( L_LoadSiteTermCatalog_ErrorMessage & Err.source & " " & Err.Number & ": " & Err.description )
        End If 
        On Error Goto 0
        
        set oXMLST = oXMLCatalog.DocumentElement.SelectSingleNode(sSearch)
        If NOT oXMLST is nothing Then
            set oXMLTerms = oXMLST.SelectNodes("./" & sXMLTagATTRIBUTE)
            If oXMLTerms.length > 0 Then
                set oXMLDoc = Server.CreateObject("MSXML.DOMDocument")
                set oXMLElement = oXMLDoc.CreateElement("select")
                set oXMLDoc.DocumentElement = oXMLElement
          
					'To create blank entry
					If sPropName <> "account_status" and sPropName <> "partner_desk_role" Then
						sValue =""
						sName =""
						set oXMLChild = oXMLDoc.CreateElement("option")
						oXMLChild.SetAttribute "value", sValue
						set oXMLText = oXMLDoc.CreateTextNode(sName)
						oXMLChild.AppendChild oXMLText
			  			oXMLDoc.DocumentElement.appendChild oXMLChild
			  		End If 	
			    	For each oXMLTerm in oXMLTerms
					    sName = oXMLTerm.attributes.GetNamedItem("displayName").value
					    sValue = sGetAttribute(oXMLTerm, "value")
					    If IsNull(sValue) or IsEmpty(sValue) or sValue = "" Then
					       sValue = oXMLTerm.attributes.GetNamedItem("name").value
					    End If  
					    set oXMLChild = oXMLDoc.CreateElement("option")
					    oXMLChild.SetAttribute "value", sValue
					    set oXMLText = oXMLDoc.CreateTextNode(sName)
					    oXMLChild.AppendChild oXMLText
					    oXMLDoc.DocumentElement.AppendChild oXMLChild
					'        i = i + 1
					next
						Else
                call oXMLMakeBlankEnum(oXMLDoc, sPropName)
            End if
        Else
            call oXMLMakeBlankEnum(oXMLDoc, sPropName)
        End if
    Else
        call oXMLMakeBlankEnum(oXMLDoc, sPropName)
    End if
    
    set oXMLGetSiteTerms = oXMLDoc
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sStripGroup(sStr)
'		Removes Group Qualifications from Properties
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Function sStripGroup(sStr)
    Dim atmp
    atmp = Split(sStr,".")
  
    If UBound(atmp) > 0 Then
		sStripGroup = atmp(UBound(atmp))
	Else
		sStripGroup = atmp(0)
	End If 	
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	 oXMLMakeBlankEnum(oXMLDoc, name)
'		Makes a Blank option in a dropdown 
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sub oXMLMakeBlankEnum(oXMLDoc, name)
    Dim oXMLElement, oXMLChild
    Dim sValue
    Dim oXMLText
    sValue = ""
    Set oXMLDoc = Server.CreateObject("MSXML.DOMDocument")
    Set oXMLElement = oXMLDoc.CreateElement("select")
    oXMLElement.SetAttribute "id", name
    Set oXMLDoc.DocumentElement = oXMLElement
    Set oXMLChild = oXMLDoc.CreateElement("option")
    oXMLChild.SetAttribute "value", sValue
    set oXMLText = oXMLDoc.CreateTextNode("")
	oXMLChild.AppendChild oXMLText
     ' oXMLChild.text = ""
    oXMLDoc.DocumentElement.appendChild oXMLChild
    
End sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	ThrowWarningDialog(warnstr)
'		Throws a warning dialogue in a page that requires posting to a ListSheet 
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Sub ThrowWarningDialog(warnstr)
   Dim xmlDoc
   Dim xmlOutPut
   'When SQlServer restarts the cache needs to refreshed .
 ' Call RefreshSessionCache()
   
   Set xmlDoc = Server.CreateObject("MSXML.DOMDocument")
   Set xmlOutPut = xmlDoc.createElement("document")
   Set xmlDoc.documentElement = xmlOutPut
   xmlOutPut.setAttribute "recordcount", -1 
   'xmlOutput.LoadXML "<document recordcount="-1"/>"
  ' Call AddWarningNode(xmlOutPut.ownerDocument, Session("ErrorMessage") &  L_RefreshConnection_Text )
   Call AddWarningNode(xmlOutPut.ownerDocument, warnstr )
   Response.Write xmlOutPut.xml
End Sub   

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	RefreshSessionCache()
'		Cleans Session cache
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sub RefreshSessionCache()
    Session("MSCSProfileSchemaVer")= ""
    Session("MSCSCatalogVer") = ""
    Session("MSCSUserXmlVer") = ""
    Session("MSCSSiteTermVer")= ""
    Set Session("MSCSProviderConn")     = Nothing		
    Set Session("MSCSCatalogSets")      = Nothing
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	AddErrorNode(xmlDOMDoc, sID, sSource)
'		adds an error node to the xml document
'
'	Arguments:	xmlDOMDoc	xml document to append error to
'				sID			string error id
'				sSource		string error message
'				sDetail		string error details
'
'	Returns:	xml document object with <document> documentElement
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub AddErrorNode(byRef xmlDOMDoc, byVal sID, byVal sSource, byVal sDetail)
	dim xmlDoc, xmlErrorNode
	set xmlDoc = xmlDOMDoc.documentElement
	set xmlErrorNode = xmlDoc.appendChild(xmlDOMDoc.createElement("ERROR"))
	xmlErrorNode.setAttribute "id", sID
	xmlErrorNode.setAttribute "source", sSource
	xmlErrorNode.text = sDetail
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	AddWarningNode(xmlDOMDoc, sMessage)
'		adds an warning node to the xml document
'
'	Arguments:	xmlDOMDoc	xml document to append warning to
'				sMessage	string warning message
'
'	Returns:	xml document object with <document> documentElement
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub AddWarningNode(byRef xmlDOMDoc, byVal sMessage)
	dim xmlDoc, xmlWarningNode
	set xmlDoc = xmlDOMDoc.documentElement
	set xmlWarningNode = xmlDoc.appendChild(xmlDOMDoc.createElement("WARNING"))
	xmlWarningNode.text = sMessage
end sub


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sGetUserScriptError(oErr)
'		returns error string populated from error object
'
'	Arguments:	oErr	Object. VBScript Err object
'
'	Returns:			String. error string with object properties
'						displayed in an HTML table
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function sGetUserScriptError(oErr)
	dim sText
	dim sErrNum
	sErrNum = Hex(oErr.number)
	sText = ""
	sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
	sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_ScriptErrorTitle_ErrorMessage & "</TH></TR>"
	'Special check introduced to check duplicates in User or Org and return a friendlier message to user
	if (sErrNum = "80040E14") or (sErrNum = "80071392") then
		sText = sText & "<TR><TH>" & L_ScriptErrorNumber_ErrorMessage & "</TH><TD>0x" &		sErrNum & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorDescription_ErrorMessage & "</TH><TD>" &	oErr.description & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorSource_ErrorMessage & "</TH><TD>" &		oErr.source & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorHelpFile_ErrorMessage & "</TH><TD>" &		oErr.helpFile & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorHelpContext_ErrorMessage & "</TH><TD>" &	oErr.helpContext & "</TD></TR>"
	else
		sText = sText & "<TR><TH>" & L_ScriptErrorNumber_ErrorMessage & "</TH><TD>0x" &		sErrNum & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorDescription_ErrorMessage & "</TH><TD>" &	oErr.description & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorSource_ErrorMessage & "</TH><TD>" &		oErr.source & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorHelpFile_ErrorMessage & "</TH><TD>" &		oErr.helpFile & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorHelpContext_ErrorMessage & "</TH><TD>" &	oErr.helpContext & "</TD></TR>"
	end if
	
	sText = sText & "</TABLE>"
	sGetUserScriptError = sText
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	GetUserListRS(sortdir)
'		returns new siteterm XML from Profile in Session("SITETERMS_XML")
'
'	Arguments: sortdir.	Indicates the direction 'ASC' or 'DESC' to sort the 
'						the UserLists Recordset
'
'	Returns:			Recordset of UserLists
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Function GetUserListRS(sortdir)
	Dim lm, rs, olistRS
	
	On Error Resume Next
	Set lm = Server.CreateObject("Commerce.ListManager")
	If Err.number <> 0 Then
		Session("UMLMProblem") = True
		Session("UMLMProblemText") = L_LMInstance_ErrorMessage
	End if
    
'	lm.Initialize sGetListDSN, sGetMasterListTableName
    lm.Initialize sGetListDSN
    
	If Err.number <> 0 Then
		If (Session("UMLMProblem")<> True) Then
			Session("UMLMProblem") = True
			Session("UMLMProblemText") = sFormatString(L_InvalidListDN_ErrorMessage, Array(sGetListDSN))
		End If
		
	Else
		Session("UMLMProblem") = False
		Set rs = lm.GetLists()
		Set olistRS = Server.CreateObject("ADODB.Recordset")
	
		olistRS.Fields.Append "List_ID" ,rs("List_ID").Type, rs("List_ID").DefinedSize, 32
		olistRS.Fields.Append "List_Name" ,rs("List_Name").Type, rs("List_Name").DefinedSize, 32    
		olistRS.CursorLocation = 3
		olistRS.Open
	
		If rs.EOF then
			REM --	Response.Write "No Lists"
			Set GetUserListRS = Nothing
			Set rs = Nothing
			Set LM = Nothing
		
		Else

			Do While NOT rs.EOF
				'filter out only the User Lists for display that have not failed 
				'and are not hidden..
				If (rs("list_flags") AND 4) AND (rs("list_status") <> 3 ) AND (rs("list_size") <> 0) Then
			'	If (rs("list_flags") AND 4) AND (rs("list_status") <> 3 )  Then
				  If NOT (rs("list_flags") AND 1 ) Then  
					olistRS.AddNew
					olistRS.Fields("List_ID").Value = rs("List_ID")
					olistRS.Fields("List_Name").Value = rs("List_Name")
				  End If 
				End If	
				rs.movenext
			Loop
		End If
	
	End If
	olistRS.MoveFirst 
	If Len(sortdir) > 0 Then  olistRS.Sort = "List_Name " & sortdir 
	 
'   Set Session("LISTRS")= olistRS	
    Set GetUserListRS = olistRS
    Set rs = Nothing
	Set LM = Nothing
	On Error Goto 0
End Function


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	GetNewSiteTermXML(bCatalogSet)
'		returns new siteterm XML from Profile in Session("SITETERMS_XML")
'
'	Arguments:	bCatalogSet	 Flag indicating treatment of catalogsets as SiteTerms
'
'	Returns:			Refreshed Session("SITETERMS_XML")
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Sub GetNewSiteTermXML(bCatalogSet)
    Dim bizData
	Dim sXML
	Dim sXML2
	Dim oXMLProfileNode
	Dim oXMLCatalog
	Dim xslProf
	Dim oXMLSiteTerm
	Dim bCatalogSets
    
    On Error Resume Next
	Set bizData = dGetBizDataObject()
	set oXMLSiteTerm = bizdata.GetProfile("Site Terms.MSCommerce", CStr(Response.CharSet))
		'set oXMLSiteTerm = bizdata.GetProfile("Site Terms.MSCommerce")
			
	Set xslProf = CreateObject("MSXML.DOMDocument")
	xslProf.load(Server.Mappath("../common/QBSiteTerms.xsl"))
	
	sXML = oXMLSiteTerm.TransformNode(xslProf)

	Set oXMLSiteTerm = CreateObject("MSXML.DOMDocument")
	oXMLSiteTerm.loadxml sXML
			
	If IsEmpty(bCatalogSet) Then
		bCatalogSets = False
	Else	
		bCatalogSets = bCatalogSet
	End If	
	
	
	If bCatalogSets Then
		Set oXMLProfileNode = oXMLSiteTerm.SelectSingleNode(".//Profile") 
		Set oXMLCatalog = oXMLGetCatalogSets2()
		Set oXMLCatalog = oXMLCatalog.SelectSingleNode(".//Group")
		oXMLProfileNode.AppendChild oXMLCatalog
	End if
	
	Session("SITETERMS_XML") = oXMLSiteTerm.xml
	sXML2 = oXMLSiteTerm.xml
		
	Session("MSCSSiteTermVer") = Application("MSCSSiteTermVer")
   
	Set bizData = Nothing
	Set oXMLSiteTerm = Nothing
	Set xslProf = Nothing
	Set oXMLCatalog = Nothing
	Set oXMLProfileNode = Nothing
	On Error Goto 0

End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	oXMLGetCatalogSets2()
'		returns CatalogSets as XMLDomDocument
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Function oXMLGetCatalogSets2()
	Dim oXMLDoc, oXMLElement
	Dim oXMLProperty
	Dim i
	Dim oXMLChild
	Dim oXMLText
	Dim sValue, sName
	Dim oRS
	set oXMLDoc = Server.CreateObject("MSXML.DOMDocument")
	set oXMLElement = oXMLDoc.CreateElement("Group")
	oXMLElement.SetAttribute "name", "Catalog"
	set oXMLDoc.DocumentElement = oXMLElement
	
	rem To create blank entries
	set oXMLProperty = oXMLDoc.CreateElement("Property")
	oXMLProperty.SetAttribute "value", sValue
	oXMLProperty.SetAttribute "name", "CustomCatalogSet"

	oXMLDoc.DocumentElement.appendChild oXMLProperty
	rem Get the catalogsets. Tables required - catalogset_catalogs and catalogset_info
	Set oRS = rsGetCatalogSets2()
	If Not Typename(oRS) = "Nothing" then
		Do While Not oRS.EOF 
		  sName = oRS("catalogSetName").value
		  sValue = oRS("catalogSetId").value
		  set oXMLChild = oXMLDoc.CreateElement("Attribute")
		  oXMLChild.SetAttribute "name", sValue
		  oXMLChild.SetAttribute "displayName", sName
		  oXMLProperty.AppendChild oXMLChild
		  oRs.MoveNext
		Loop
	End If
	Set oXMLGetCatalogSets2 = oXMLDoc
	
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	 rsGetCatalogSets2()
'		returns CatalogSets as ADODB Recordset
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Function rsGetCatalogSets2()
	Dim objCatalogSets
	dim mscsCatalogConnStr,mscsTransactionConfigConnStr	
		
   'Keep this in the Session and refresh with publish profiles 
	On Error Resume Next 'Application("MSCSCatalogSets")

	if Not IsObject(Session("MSCSCatalogSets")) or Session("MSCSCatalogSets")= Nothing  or Session("MSCSCatalogVer") <> Application("MSCSCatalogVer") then
		mscsTransactionConfigConnStr = GetSiteConfigField("Transaction Config", "connstr_db_TransactionConfig")   
		mscsCatalogConnStr = GetSiteConfigField("product catalog", "connstr_db_Catalog")
		set objCatalogSets = Server.CreateObject("Commerce.CatalogSets")
		objCatalogSets.Initialize mscsCatalogConnStr, mscsTransactionConfigConnStr
			
		set rsGetCatalogSets2 = objCatalogSets.GetCatalogSets
		If Err.number <> 0 then
			Set rsGetCatalogSets2 = Nothing
		End If
		Set Session("MSCSCatalogSets") = rsGetCatalogSets2
		Session("MSCSCatalogVer") = Application("MSCSCatalogVer")
			
		Set objCatalogSets = Nothing
	else
	    Set rsGetCatalogSets2 = Session("MSCSCatalogSets")
	    rsGetCatalogSets.MoveFirst
	end if     
		
	On Error Goto 0
		
End Function		


%>