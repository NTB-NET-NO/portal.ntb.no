<xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">

  <xsl:template match="@*">
    <xsl:copy><xsl:value-of /></xsl:copy>
  </xsl:template>

  <xsl:template match="/">
    <xsl:pi name="xml">version="1.0"</xsl:pi>
    <xsl:apply-templates />
  </xsl:template>
  
  <xsl:template match="Document">
    <Document>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Document>
  </xsl:template>

	<xsl:template match="Catalog">
		<Catalog>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</Catalog>
	</xsl:template>

	<xsl:template match="Profile">
		<Profile>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</Profile>
	</xsl:template>

  <xsl:template match="Group">
    <Group>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates />
    </Group>
  </xsl:template>

  <xsl:template match="Property">
    <Property>
		<xsl:choose> 
			 <xsl:when test=".[@propType $ieq$ 'SITETERM' $or$ @referenceString ]">
			  <xsl:attribute name="propType"><xsl:eval>GetPropType</xsl:eval></xsl:attribute> 
			  <xsl:attribute name="referenceString"><xsl:eval>RemoveCatalogRef(me.getAttribute("referenceString"))</xsl:eval></xsl:attribute>
			  <xsl:apply-templates select="@*[(nodeName() != 'propType') $and$
														 (nodeName() != 'referenceString')]" />
			</xsl:when>
			<xsl:otherwise>
                <xsl:apply-templates select="@*" />
			</xsl:otherwise>
		</xsl:choose>
      <xsl:apply-templates />
    </Property>
  </xsl:template>

  <xsl:script language="VBScript">
    function RemoveCatalogRef(sRef)
		dim aNames
		dim iend
		dim nLen
		if Len(sRef) > 0 then
		    aNames = Split(sRef, ".")
		    iend = UBound(aNames)
		end if
		if iend >= 2 then
		    nLen = InStr(1, sRef, ".")
		    RemoveCatalogRef = Mid(sRef, nLen + 1, Len(sRef))
		else
		    RemoveCatalogRef = sRef
		end if       
    end function
     
    function GetPropType   
       Dim sRef
       Dim sRef1
       Dim sType
       On Error Resume Next
       sRef1 = me.getAttribute("name")
       sRef = me.getAttribute("referenceString")
       sType = me.getAttribute("propType")
       if Len(sRef) > 0 then
			if sRef = "Catalog.CustomCatalogSet" or sRef1 = "partner_desk_role"  or sRef1 = "user_type" or sRef1 = "account_status" then
		'	if sRef = "Catalog.CustomCatalogSet"  then  
		 		GetPropType = "SITETERM"
			elseif LCase(sType)="profile" then
		'       To remove for Generic Profiles
			    GetPropType = "STRING"
			else
				GetPropType = sType
				'GetPropType = "SITETERM"
			end if 	
	   end if	
	   On Error GoTo 0
    end function       
  </xsl:script>
  
</xsl:stylesheet>

