<%
	' Global variables
	Dim g_MSCSCatalogConnStr, g_MSCSTransactionConfigConnStr, g_oCatalogSets
	Dim g_sStatusText
	
	Sub InitializeCatalogSetObject		
		On Error Resume Next
		g_MSCSTransactionConfigConnStr= GetSiteConfigField("Transaction Config", "connstr_db_TransactionConfig")
		g_MSCSCatalogConnStr = GetSiteConfigField("product catalog", "connstr_db_Catalog")
		If Err <> 0 Then 
			Call setErr(L_InitializeCatalogSet_DialogTitle, L_SiteConfig_ErrorMessage)
			Err.Clear
		End If
		Set g_oCatalogSets = Server.CreateObject("Commerce.CatalogSets")
		If Err <> 0 Then 
			Call setErr(L_InitializeCatalogSet_DialogTitle, sGetErrorById("Cant_create_object"))
			Err.Clear
		End If
		g_oCatalogSets.Initialize g_MSCSCatalogConnStr,g_MSCSTransactionConfigConnStr
		If Err <> 0 Then 
			Call setErr(L_InitializeCatalogSet_DialogTitle, sGetErrorById("Cant_connect_db"))
			Err.Clear
		End If
	End Sub
	
	Sub SetErr(sTitle, sMessage)
		Call setError(sTitle, sMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
	End Sub

%>
