<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

'PageTitles
Const L_CatalogSets_PageTitle = "Catalog Sets"
Const L_CatalogSetsNew_PageTitle = "New Catalog Set"
Const L_CatalogSetsOpen_PageTitle = "Catalog Sets - %1"

'Dialog and message box strings
Const L_InitializeCatalogSet_DialogTitle = "Initialize CatalogSet"
Const L_SiteConfig_ErrorMessage = "Error Getting Field From SiteConfig"
Const L_GetCatalogSets_DialogTitle = "Get Catalog Sets"
Const L_DeleteCatalogSet_DialogTitle = "Delete"
Const L_Delete_ErrorMessage = "Error Deleting Catalog Set"
Const L_GetCatalogSetInfo_DialogTitle = "Get Catalog Set Info"
Const L_GetAvailableCatalogs_DialogTitle = "Get Available Catalogs"
Const L_GetAssignedCatalogs_DialogTitle = "Get Assigned Catalogs"
Const L_SaveCatalogSet_DialogTitle = "Save Catalog Set"
Const L_SaveCatalogSet_ErrorMessage = "Error saving Catalog Set"
Const L_DeleteMsgBox_Text = "Delete Catalog Set %1?"	

'ListSheet strings
Const L_Name_Text = "Name"
Const L_Description_Text = "Description"
Const L_ItemSelected_StatusBar = "1 Item Selected [ %1 ]"
Const L_NoItemSelected_StatusBar = "0 Items Selected"
Const L_Delete_StatusBar ="Catalog Set Deleted: %1"
Const L_ErrorDelete_StatusBar = "Error Deleting CatalogSet"
Const L_LoadingList_Text = "Loading list, please wait..."

'Editsheet strings
Const L_CatalogSetName_Text = "Name:"
Const L_CatatlogSetDescription_Text = "Description:"

Const L_EnterCatalogSetName_Text = "Enter Catalog Set Name"
Const L_EnterCatatlogSetDescription_Text = "Enter Catalog Set Description"

Const L_CatalogSetName_ToolTip = "Catalog Set Name"
Const L_CatatlogSetDescription_ToolTip = "Catalog Set Description"
Const L_WildCardDescription_ToolTip = "Add all catalogs to this set (including new catalogs as they are created)"

Const L_LoadingProperties_Text = "Loading properties, please wait..."
Const L_Loading_Text = "Loading..."
Const L_SaveConfirmationDialog_Text = "Save Catalog Set before exiting?"
Const L_Required_Text = "Cannot save until all required fields have values."
Const L_Valid_Text = "Cannot save until all required fields have values and invalid fields are corrected."

Const L_AvailableCatalogs_Text = "Available catalogs:"
Const L_AssignedCatalogs_Text = "Assigned catalogs:"
Const L_Add_Button = "Add ->"
Const L_Add_Style = "WIDTH:7em"
Const L_Remove_Button = "<- Remove"
Const L_Remove_Style = "WIDTH:7em"
Const L_WildCardDescription_Text = "Add all catalogs"

Const L_Open_StatusBar = "[%1] Open"
Const L_Save_StatusBar = "Catalog Set Saved"
Const L_ErrorSave_StatusBar = "Error Saving Catalog Set"
Const L_ErrorDuplicate_StatusBar = "Error: Duplicate Catalog Set"

Const L_CatalogSetName_ErrorMessage = "Catalog Set name must be less than 255 characters and cannot contain '&lt;', '&gt;', or '&amp;'"
Const L_CatalogSetDescription_ErrorMessage = "Catalog Set description must be less than 255 characters and cannot contain '&lt;', '&gt;', or '&amp;'"
%>