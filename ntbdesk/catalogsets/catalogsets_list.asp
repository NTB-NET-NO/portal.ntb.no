<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="./include/catalogsets_strings.asp"-->
<!--#INCLUDE FILE="./include/catalogsets_util.asp"-->
<%	
	'Global variables
	Dim g_xmlCatalogSetListData
	
	'Local variables
	Dim sType, sID, nResult, rs, xmlData		

	Call InitializeCatalogSetObject
	On Error Resume Next
	sType = Request.Form("type")
	If sType = "delete" Then
		sID = Request.Form("CatalogSetID")
		g_oCatalogSets.DeleteCatalogSet sID
		If Err = 0 Then
			g_sStatusText = sFormatString(L_Delete_StatusBar, array(Request.Form("CatalogSetName")))
		Else
			Call setErr(L_DeleteCatalogSet_DialogTitle, L_Delete_ErrorMessage)
			g_sStatusText = L_ErrorDelete_StatusBar
		End If
	End If
	Set rs = g_oCatalogSets.GetCatalogSets
	If Err = 0 Then
		Set xmlData = xmlGetXMLFromRSEx(rs, -1, -1, -1, null)
	Else
		Call setErr(L_GetCatalogSets_DialogTitle, sGetErrorById("Bad_XML_data "))
		Err.Clear
	End If
	g_xmlCatalogSetListData = xmlData.xml
	Set rs = Nothing
	Set g_oCatalogSets = Nothing
	Set xmlData = Nothing
%>


<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<SCRIPT LANGUAGE='VBScript'>

	OPTION EXPLICIT	
		
    Sub OnSelectRow()    		
		dim oSelection
		dim sCatalogSetId, sCatalogSetName		
		'handle the task buttons        
        EnableTask "delete"
        EnableTask "open"        			
		' get XML node for the selected row
		set oSelection = window.event.XMLrecord        	
		' get the key value
		sCatalogSetId = oSelection.selectSingleNode("./CatalogSetID").text
        sCatalogSetName = oSelection.selectSingleNode("./CatalogSetName").text	    
	    ' add values to selectform
	    with selectform
			.CatalogSetId.value = sCatalogSetId
			.CatalogSetName.value = sCatalogSetName
		end with
	    ' display the status text
		setStatusText sFormatString("<%= L_ItemSelected_StatusBar%>",Array(sCatalogSetName))		
	End Sub
	   
    Sub OnUnSelectRow()
		'handle the task uttons
		DisableTask "open"
		Disabletask "delete"
	    'clear values
	    with selectform
			.CatalogSetId.value = ""
			.CatalogSetName.value = ""
		end with
		setStatusText "<%= L_NoItemSelected_StatusBar%>"		
	End Sub
	
	Sub OnTask()
		dim sTitle, sText, sResponse
		if window.event.srcElement Is elGetTaskBtn("delete") then
			sTitle = "<%= L_DeleteCatalogSet_DialogTitle%>"
			sText = sFormatString("<%= L_DeleteMsgBox_Text%>", array(selectform.CatalogSetName.value))
			sResponse = MsgBox(sText, vbOkCancel, sTitle)		
			if sResponse = vbOk then
				selectform.type.value = "delete"
				selectform.submit()
			else
				EnableTask "delete"
				EnableTask "new"
				Enabletask "open"
			end if
		else
			if Window.event.srcElement Is elGetTaskBtn("new") then
				selectform.type.value = "new"
				selectform.CatalogSetId.value = ""
				selectform.CatalogSetName.value = ""
			elseif Window.event.srcElement Is elGetTaskBtn("open") then
				selectform.type.value = "open"
			end if
			selectform.submit()
		end if		
	End Sub

</SCRIPT>
</HEAD>
<BODY SCROLL='NO'>

	<% InsertTaskBar L_CatalogSets_PageTitle, g_sStatustext %>

	<DIV ID='bdcontentarea'>
	
		<FORM ID='selectform' METHOD='POST' ONTASK="OnTask()">
			<INPUT TYPE='hidden' ID='CatalogSetId' NAME='CatalogSetId'>
			<INPUT TYPE='hidden' ID='CatalogSetName' NAME='CatalogSetName'>
			<INPUT TYPE='hidden' ID='type' NAME='type'>
		</FORM>
		
	<!-- order list data and meta -->

	<xml id='lsMeta'>
		<listsheet>
			<global sort='no' selection='single'/>
			<columns>
				<column id='CatalogSetID' hide='yes'></column>
			    <column id='CatalogSetName' width='50'><%= L_Name_Text%></column>
			    <column id='CatalogSetDescription'  width='50'><%= L_Description_Text%></column>
			 </columns>
		</listsheet>
	</xml>
	
	<xml id='lsData'>
		<%= g_xmlCatalogSetListData%>
	</xml>
		
	<DIV ID='lsCatalogSets' CLASS='listSheet' STYLE="MARGIN:20px; TOP:20px; HEIGHT:90%"
		 DataXML='lsData'
		 MetaXML='lsMeta'
		 LANGUAGE='VBScript'
		 OnRowSelect='OnSelectRow()'
		 OnRowUnselect='OnUnSelectRow()'>
		 <%= L_LoadingList_Text%>
	</DIV>

</DIV>
</BODY>
</HTML>	
