<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="./include/catalogsets_strings.asp"-->
<!--#INCLUDE FILE="./include/catalogsets_util.asp"-->
	
<%
	' Global variables
	Dim g_sType, g_sCatalogSetId, g_sCatalogSetName, g_sCatalogSetDescription, g_bWildCard, g_xmlCatalogs
	Dim g_xmlCatalogSetEditData, g_sAvailList, g_sAssignList, g_sDisabled
	
	Call ProcessCatalogSetEditForm()
	
	Sub ProcessCatalogSetEditForm()		
		g_sDisabled = ""
		' Assign values to global variables
		g_sType = Request.Form("type")
		If g_sType = "new" Then
			g_sCatalogSetId = "-1"
		Else	' open
			g_sCatalogSetId = Request.Form("CatalogSetID")
		End If
		g_sCatalogSetName = Request.Form("CatalogSetName")
		g_sCatalogSetDescription = Request.Form("CatalogSetDescription")
		g_bWildCard = Request.Form("bWildCard")
		'If IsEmpty(g_bWildCard) Then g_bWildCard = "False"
		g_xmlCatalogs = Request.Form("xmlCatalogs")
		
		Call InitializeCatalogSetObject

		'Process form based on task
		Select Case g_sType
			Case "new", "open"
				g_sStatusText = ""
			Case "save", "savenew", "saveback"
				Call SaveCatalogSet
		End Select
		If g_sType = "savenew" Then g_sCatalogSetId = "-1"
		g_xmlCatalogSetEditData = GetXMLCatalogSetEditData
		g_sAvailList = GetCatalogs("avail")
		g_sAssignList = GetCatalogs("assign")
		Set g_oCatalogSets = Nothing
	End Sub

	Function SaveCatalogSet()
	' Create catalog recordset based on checkbox value; use catalogset object to insert of update table	
		Dim rs, bCatalogs
		
		On Error Resume Next
		Select Case g_bWildCard
			Case 0	' Box is unchecked; pass catalog rs to object
				bCatalogs = False
				Set rs = GetCatalogsRS
				If g_sCatalogSetID = "-1" Then	'insert
					g_sCatalogSetID = g_oCatalogSets.CreateCatalogSet(g_sCatalogSetName, g_sCatalogSetDescription, _
																	  bCatalogs, rs)
				Else	'update
					g_oCatalogSets.UpdateCatalogSet CStr(g_sCatalogSetID), g_sCatalogSetName, _
														 g_sCatalogSetDescription, _
														 bCatalogs, rs
				End If
			Case 1	' Box is checked; no catalog rs passed to object since all catalogs are included	
				bCatalogs = True
				If g_sCatalogSetID = "-1" Then	'insert
					g_sCatalogSetID = g_oCatalogSets.CreateCatalogSet(g_sCatalogSetName, g_sCatalogSetDescription, _
																	  bCatalogs)
				Else	'update
					g_oCatalogSets.UpdateCatalogSet CStr(g_sCatalogSetID), g_sCatalogSetName, _
														 g_sCatalogSetDescription, bCatalogs
				End If
		End Select
		Set rs = Nothing
		
		' Set status text to inform user of results of operation
		If err = 0 Then
			If g_sType = "save" or g_sType = "saveback" Then
				g_sStatusText = L_Save_StatusBar
			Else	' savenew
				g_sType = "savenew"
				g_sStatusText = L_New_StatusBar
			End If
			Application.Lock()
			Application("MSCSCatalogVer") = Application("MSCSCatalogVer") + Application("MSCSVerIncrement")
			Application.Unlock()
		Else
			Call setErr(L_SaveCatalogSet_DialogTitle, L_SaveCatalogSet_ErrorMessage)			
			If g_sCatalogSetID = "-1" Then	' Error saving new data
				g_sStatusText = L_ErrorDuplicate_StatusBar
				g_sType = "err"
			Else
				g_sStatusText = L_ErrorSave_StatusBar
			End If
		End If
	End Function
	
	Function GetCatalogsRS
	' Takes xml from listbox and generates a disconnected recordset to pass to catalogsets object
		Dim oXMLCatalogs, oXMLNode, oXMLNodeList, rs	
		
		On Error Resume Next
		' Create disconnected recordset
		Set rs = Server.CreateObject("ADODB.RECORDSET")
		With rs
			.CursorLocation = AD_USE_CLIENT
			.Fields.Append "CatalogName", AD_BSTR, 255, AD_FLD_UPDATABLE
			.Open , , AD_OPEN_STATIC, AD_LOCK_PESSIMISTIC
		End With
		' Get catalogs xml from listbox; create document and node list of items
		Set oXMLCatalogs = Server.CreateObject("MSXML.DOMDocument")
		oXMLCatalogs.async = False
		oXMLCatalogs.loadXML g_xmlCatalogs
		Set oXMLNode = oXMLCatalogs.documentElement
		If oXMLNode.childNodes.Length <> 0 Then
			Set oXMLNodeList = oXMLNode.childNodes
			'  Populate rs with xml items from list
			For Each oXMLNode in oXMLNodeList
				rs.AddNew
				rs.Fields("CatalogName") = oXMLNode.Text
			Next
		End If
		If Not (rs.EOF And rs.BOF) Then
			rs.MoveFirst
		End If
		Set GetCatalogsRS = rs
		Set oXMLCatalogs = Nothing
	End Function
	
	Function GetCatalogs(sListbox)
	'Fill the list boxes with names from CatalogSet object
		Dim rs, sBuffer	
		On Error Resume Next		
		' Build recordsets
		Select Case sListbox
			Case "avail"
				If g_sCatalogSetId = "-1" Then	'adding a new set
					Set rs = g_oCatalogSets.GetCatalogs
				Else	' opening an existing set
					Set rs = g_oCatalogSets.GetCatalogsNotInCatalogSet(CStr(g_sCatalogSetId))
				End If		
				If Err <> 0 Then
					Call setErr(L_GetAvailableCatalogs_DialogTitle, sGetErrorById("Bad_XML_data "))
					Set rs = Nothing
					Err.Clear
					Exit Function
				End If
			Case "assign"
				If g_sCatalogSetId <> "-1" Then	'opening an existing set so get matching catalogs
					Set rs = g_oCatalogSets.GetCatalogsInCatalogSet(CStr(g_sCatalogSetId))
					If g_sType = "savenew" Then
						g_sCatalogSetId = "-1"
					End If
					If err <> 0 Then
						Call setErr(L_GetAssignedCatalogs_DialogTitle, sGetErrorById("Bad_XML_data "))
						Set rs = Nothing
						Err.Clear
						Exit Function
					End If
				End If
		End Select
		
		' Write data to list boxes	
		If rs.BOF And rs.EOF Then	'no data
			sBuffer = ""
		Else
			If Not rs.EOF Then
				rs.MoveFirst
				sBuffer = ""
				Do While Not rs.EOF
					sBuffer = sBuffer & "<DIV VALUE='" & rs("CatalogName") & "'>" & rs("CatalogName") & "</DIV>"
					rs.MoveNext
				Loop
			End If 
		End If
		GetCatalogs = sBuffer
		Set rs = Nothing
	End Function
	
	Function GetXMLCatalogSetEditData()
		Dim sBuffer, rs, xmlData		
		Select Case g_sType
			Case "new", "savenew", "err"
				sBuffer = "<document>"
				sBuffer = sBuffer & "<record>"
				sBuffer = sBuffer & "<CatalogSetName/>"
				sbuffer = sBuffer & "<CatalogSetDescription/>"
				sBuffer = sBuffer & "<bWildCard/>"
				sBuffer = sBuffer & "</record> "
				sBuffer = sBuffer & "</document>"
				GetXMLCatalogSetEditData = sBuffer
			Case "open", "save", "saveback"
				Set rs = g_oCatalogSets.GetCatalogSetInfo(CStr(g_sCatalogSetID))
				If err = 0 Then
					Set xmlData = xmlGetXMLFromRSEx(rs, -1, -1, -1, null)
					if xmlData.selectSingleNode("//bWildCard").text = "1" then
						g_sDisabled = " lbDisabled"
					end if
					GetXMLCatalogSetEditData = xmlData.xml
					Set xmlData = Nothing
				Else
					Call setErr(L_GetCatalogSetInfo_DialogTitle, sGetErrorById("Bad_XML_data "))
				End If
				Set rs = Nothing
		End Select
	End Function
%>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<SCRIPT LANGUAGE='VBSCRIPT'>
	
	OPTION EXPLICIT	
			
	sub onEvent(btnClicked)
		dim evt, bSelectionPresent, elOption
		set evt = window.event
		if evt.type = "select" then
			btnClicked.disabled = false
		else
			bSelectionPresent = false
			for each elOption in window.event.srcElement.children
				if inStr(elOption.className, "selected") then
					bSelectionPresent = true
				end if
			next
			if not bSelectionPresent then btnClicked.disabled = true
		end if
	end sub

	sub onClickAdd()
		listBoxRight.add(listBoxLeft.remove())
		saveform.btnAdd.disabled = true
	end sub

	sub onClickRemove()
		listBoxLeft.add(listBoxRight.remove())
		saveform.btnRemove.disabled = true
	end sub
	
	Sub onCheck()
	' Called when bWildCard checkbox is checked or unchecked.
		if bWildCard.value = 1 then
		'if bWildCard.value = 1 then	'checked - move all catalogs to listBoxRight
			listBoxLeft.selectall()
			listBoxRight.add(listBoxLeft.remove())
			listBoxRight.disabled = true
			listBoxLeft.disabled = true
			saveform.btnAdd.disabled = true
			saveform.btnRemove.disabled = true
		else						'unchecked - move all catalogs to listBoxLeft
			listBoxRight.disabled = false
			listBoxLeft.disabled = false
			listBoxRight.selectall()
			listBoxLeft.add(listBoxRight.remove())
			saveform.btnRemove.disabled = true
			saveform.btnAdd.disabled = true
		end if
	End Sub
	
	Sub onSave()
	' If bWildCard is not checked, selects all elements in listBoxRight
		dim xmlNodes, aCatalogs
		if bWildCard.value = 0 then
			listBoxRight.selectall()
			Set xmlNodes = listBoxRight.remove()
			listBoxRight.add(xmlNodes)
			saveform.xmlCatalogs.value = xmlNodes.xml
		end if
		if window.event.srcElement Is elGetTaskBtn("save") then
			saveform.type.value = "save"
		elseif window.event.srcElement Is elGetTaskBtn("savenew") then
			saveform.type.value = "savenew"
		elseif window.event.srcElement Is elGetTaskBtn("saveback") then
			saveform.type.value = "saveback"
		end if
		saveform.submit()
	End Sub
	
	sub window_onload()
		on error resume next
		esSet.focus()
	End Sub
</SCRIPT>
</HEAD>
<BODY SCROLL='NO'>

<%
if g_sType = "new" or g_sType = "savenew" then
	InsertEditTaskBar L_CatalogSetsNew_PageTitle, g_sStatusText
else
	InsertEditTaskBar sFormatString(L_CatalogSetsOpen_PageTitle, Array(g_sCatalogSetName)), g_sStatusText
end if
%>
		
<xml id='esSetMeta'>
	<editsheet>
		<global title='no'/>
		<fields>
			<text id='CatalogSetName' required='yes' maxlen='128'>
				<name><%= L_CatalogSetName_Text %></name>
				<tooltip><%= L_CatalogSetName_ToolTip %></tooltip>
				<error><%= L_CatalogSetName_ErrorMessage %></error>
				<prompt><%= L_EnterCatalogSetName_Text %></prompt>
			</text>
			<text id='CatalogSetDescription' maxlen='255'>
				<name><%= L_CatatlogSetDescription_Text %></name>
				<tooltip><%= L_CatatlogSetDescription_ToolTip %></tooltip>
				<error><%= L_CatalogSetDescription_ErrorMessage %></error>
				<prompt><%= L_EnterCatatlogSetDescription_Text %></prompt>
			</text>
		</fields>
	</editsheet>
</xml>
		
<xml id='esSetData'>
	<%= g_xmlCatalogSetEditData %>
</xml>		
					
<xml id='esCatalogsMeta'>
	<editsheet>
		<global title='no'/>
		<fields>
			<boolean id='bWildCard' default='0'>
				<label><%= L_WildCardDescription_Text %></label>
			</boolean>
		</fields>
		<template register='esSet bWildCard'>
		<![CDATA[
			<DIV ID='esSet' CLASS='editSheet' 
				 STYLE='WIDTH:700px'
				 MetaXML='esSetMeta'
				 DataXML='esSetData'>
				 <%= L_LoadingProperties_Text %>
			</DIV>

			<TABLE border=0>
			<TR>
				<TD>&nbsp;</TD>
				<TD ID='availlabel'><%= L_AvailableCatalogs_Text %></TD>
				<TD>&nbsp;</TD>
				<TD ID='assignlabel'><%= L_AssignedCatalogs_Text %></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD>
					<DIV ID='listBoxLeft' STYLE="WIDTH:265px" selection='multi'
						CLASS='listBox<%= g_sDisabled %>'
						LANGUAGE='VBScript'
						ONSELECT='onEvent(btnAdd)'
						ONUNSELECT='onEvent(btnAdd)'
						ONCHANGE='setDirty("")'>
						<%= g_sAvailList %>
					</DIV>
				</TD>
				<TD>
					<BUTTON ID='btnAdd' CLASS='bdbutton' STYLE=<%= L_Add_Style %> DISABLED
						LANGUAGE='VBScript'
						ONCLICK='onClickAdd'><%= L_Add_Button %></BUTTON>
					<BR>
					<BUTTON ID='btnRemove' CLASS='bdbutton' STYLE=<%= L_Remove_Style %> DISABLED
						LANGUAGE='VBScript'
						ONCLICK='onClickRemove'><%= L_Remove_Button %></BUTTON>
				</TD>
				<TD>
					<DIV ID='listBoxRight' STYLE="WIDTH:265px" selection='multi'
						CLASS='listBox<%= g_sDisabled %>'
						LANGUAGE='VBScript'
						ONSELECT='onEvent(btnRemove)'
						ONUNSELECT='onEvent(btnRemove)'
						ONCHANGE='setDirty("")'>
						<%= g_sAssignList %>
					</DIV>
				</TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD>
					<DIV ID='bWildCard' CLASS='editField' 
						 STYLE='WIDTH:165px; BORDER: none; BACKGROUND-COLOR: transparent'
						 MetaXML='esCatalogsMeta'
						 DataXML='esSetData'
						 OnChange='onCheck()'
						 onMouseOver='bWildCard.title = "<%= L_WildCardDescription_ToolTip %>"'>
						 <%= L_LoadingProperties_Text%>
					</DIV>
				</TD>
			</TR>
		</TABLE>
		]]></template>
	</editsheet>
</xml>
	
	<DIV ID='editSheetContainer' CLASS='editPageContainer'>
		<FORM ID='saveform' ACTION METHOD='POST' ONTASK='onSave'>
			<INPUT TYPE='hidden' ID='type' NAME='type'>
			<INPUT TYPE='hidden' ID='CatalogSetID' NAME='CatalogSetID' VALUE='<%= g_sCatalogSetId%>'>
			<INPUT TYPE='hidden' ID='xmlCatalogs' NAME='xmlCatalogs'>
			<DIV ID='esCatalogSets' 
				 CLASS='editSheet' 
				 MetaXML='esCatalogsMeta'
				 DataXML='esSetData'
				 ONREQUIRE='setRequired("")'
				 ONVALID='setValid("")'
				 ONCHANGE='setDirty("<%= L_SaveConfirmationDialog_Text%>")'>
				 <%= L_LoadingProperties_Text%>
			</DIV>
		</FORM>
	</DIV>
</BODY>
</HTML>
