<%
'LOC: This file	contains all localizable strings for Users module

const L_PageTitleRegisteredUsers_Text	= "Users"
const L_ErrorMessageTitle_Text			= "User"

rem Error Messages

const L_DELETE_DialogTitle				= "Are you sure you want to Delete the following User(s)?"
const L_OPEN_DialogTitle				= " Open User"
const L_Delete_ErrorMessage			    = "An error occurred while deleting one or more user records."
const L_NoSelection_ErrorMessage		= "The selection could not be processed."
const L_ExecFail_ErrorMessage			= "An error occurred while executing the query."
const L_SearchFail_ErrorMessage		    = "An error occurred while executing the search query."
const L_CopyUserFail_ErrorMessage	    = "An error occurred while copying user properties."
const L_NoUser_ErrorMessage		        = "The user record could not be opened. Either the record was not created or it has been deleted."


const L_SelectOrganization_Text	        = "You need to select a valid organization for this user first."
const L_ConfirmDelete_Text			    = "Delete %1?"
const L_ConfirmDeleteAllUsers_Text      = "Delete All Users?"
const L_ForOrg_Text					    = " for Organization %1 "


const L_SelectAll_Text					= "All users selected"
const L_NoSelections_Text				= "No users selected"
rem L_OneSelection_Text					= "One user  selected [username] for organization orgname"
const L_OneSelection_Text				= "One user  selected [%1] %2"
const L_MultiSelection_Text				= "%1 users selected %2"

const L_ChooseOrg_Text					= "Choose the Organization this user belongs to:"
const L_ChooseUser_Text					= "Choose an existing user:"

const L_LMInstance_ErrorMessage			= "The List Manager object could not be instantiated."
const L_InvalidListDN_ErrorMessage		= "Your list DSN  [%1] is not valid."


const L_SearchUser_Text					= "Look for:"
const L_UpdateUserValues_Text			= "Click NEW To Form Update Queries"

const L_LogonName_ErrorMessage			= "Logon Name exceeds size. Please enter a shorter one."

const L_AddMemberFail_ErrorMessage		= "An error occurred while adding the User to the group."
const L_RemoveMemberFail_ErrorMessage	= "An error occurred while removing the User from the group."



%>
