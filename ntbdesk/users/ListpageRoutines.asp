<SCRIPT LANGUAGE='VBScript'>
<!--

Dim oSelection, oSortCol, bSortAscending
bSortAscending = true
Dim g_bQBReady
g_bQBReady = False

Sub OnUnselectRow()
	Dim nitems, sRest, sName, ipos, oSelection, sleft, sright, str
	On Error Resume Next
	str = " <%=sExtraStat%>"
	Set oSelection = window.event.XMLrecord
	nitems = selectform.elements.numitems.value
	If nitems > 0 Then nitems = nitems-1
	selectform.elements.numitems.value = nitems
	deleteform.elements.numitems.value = nitems 

	sKeyValue = trim(oSelection.selectSingleNode("<%= g_sKey %>").text)
	sName = trim(oSelection.selectSingleNode("<%= g_sName %>").text)
    If nitems = 1 Then
		EnableTask "copy"
	Else
		DisableTask "copy"
	End If
	If nitems = 0 Then
		selectform.elements.friendlyname.value = ""
		deleteform.elements.friendlyname.value = ""
		copyform.elements.friendlyname.value = ""
		selectform.elements.<%= g_sKey %>.value = ""
		deleteform.elements.<%= g_sKey %>.value = ""
		copyform.elements.<%= g_sKey %>.value = ""
		' disable o(pen) and d(elete) buttons on the task bar
		DisableTask "open"
		DisableTask "delete"
	Else
		sRest = trim(selectform.elements.<%= g_sKey %>.value)
		selectform.elements.<%= g_sKey %>.value = sDeleteNameFromField(sKeyValue, sRest)
		deleteform.elements.<%= g_sKey %>.value = selectform.elements.<%= g_sKey %>.value
		copyform.elements.<%= g_sKey %>.value = selectform.elements.<%= g_sKey %>.value
		
		sRest = trim(selectform.elements.friendlyname.value)
		sRest = sDeleteNameFromField(sName, sRest)
		selectform.elements.friendlyname.value = sRest
		deleteform.elements.friendlyname.value = sRest
		copyform.elements.friendlyname.value = sRest                    
	End if
	' display the status text

	Select case(nitems)
		case (0)  setStatusText "<%= L_NoSelections_Text %>"
		case (1)  setStatusText sFormatString("<%= L_OneSelection_Text %>", Array(sRest, str))
		case else setStatusText sFormatString("<%= L_MultiSelection_Text %>", Array(nitems, str))
	End Select
	On Error Goto 0
End sub

Sub OnSelectRow()
	Dim oSelection, sKeyValue, sName, nitems, sRest, str
	str = " <%=sExtraStat%>"
	On Error Resume Next
	Set oSelection = window.event.XMLrecord
	' enable o(pen) and d(elete) buttons on the task bar
	EnableTask "open"
	'disabling delete until Provider supports it.
	EnableTask "delete"
	
	sKeyValue = trim(oSelection.selectSingleNode("./<%= g_sKey %>").text)
	sName = trim(oSelection.selectSingleNode("./<%= g_sName %>").text)
	
	nitems = selectform.elements.numitems.value

	nitems = nitems+1
    If nitems = 1 then
		EnableTask "copy"
	Else
		DisableTask "copy"
	End If
	selectform.elements.numitems.value = nitems 
	deleteform.elements.numitems.value = nitems 
	' add the value for the item key to the selection form
	sRest = trim(selectform.elements.<%= g_sKey %>.value)
	selectform.elements.<%= g_sKey %>.value = sAddName2Field(sKeyValue, sRest)
	deleteform.elements.<%= g_sKey %>.value = selectform.elements.<%= g_sKey %>.value
	copyform.elements.<%= g_sKey %>.value = sKeyValue
	
	if sName="" then sName=" "
	sRest = selectform.elements.friendlyname.value
	selectform.elements.friendlyname.value = sAddName2Field(sName, sRest)
	deleteform.elements.friendlyname.value = selectform.elements.friendlyname.value
	copyform.elements.friendlyname.value = sName
	
	            
	' display the status text
	If (nitems = 1) then
		setStatusText sFormatString("<%= L_OneSelection_Text %>", Array(sName, str))
	Else
		setStatusText sFormatString("<%= L_MultiSelection_Text %>", Array(nitems, str))
	End if        
	
	On Error Goto 0
End sub

Sub OnSelectAllRows()
  ' enable  d(elete) button on the task bar
    On Error Resume Next
	EnableTask "delete"
	EnableTask "open"
	DisableTask "copy"
  ' build query here ???
	selectform.elements.friendlyname.value = ""
	deleteform.elements.friendlyname.value = ""
	copyform.elements.friendlyname.value = ""
	selectform.elements.<%= g_sKey %>.value = ""
	deleteform.elements.<%= g_sKey %>.value = ""
	copyform.elements.<%= g_sKey %>.value = ""

	deleteform.elements.SelType.value = "All"
	deleteform.elements.Query.value =  "<%= g_sQuery%>"
	selectform.elements.SelType.value = "All"
	selectform.elements.Query.value =  "<%= g_sQuery%>"
	selectform.elements.numitems.value = 0
	deleteform.elements.numitems.value = 0
	setStatusText "<%= L_SelectAll_Text %>"
	On Error Goto 0
End sub
    
Sub OnUnSelectAllRows()
	' disable  d(elete) button on the task bar
	DisableTask "delete"
	DisableTask "open"
	Disabletask "copy"
	
	selectform.elements.friendlyname.value = ""
	deleteform.elements.friendlyname.value = ""
	copyform.elements.friendlyname.value = ""
	selectform.elements.<%= g_sKey %>.value = ""
	deleteform.elements.<%= g_sKey %>.value = ""
	copyform.elements.<%= g_sKey %>.value = ""

	' clear the value for the item key in the selection form
	deleteform.elements.SelType.value = ""
	deleteform.elements.Query.value =  ""
	selectform.elements.SelType.value = ""
	selectform.elements.Query.value =  ""
	selectform.elements.numitems.value = 0 
	deleteform.elements.numitems.value = 0
	
	' display the status text
  setStatusText "<%= L_NoSelections_Text %>"
End sub  
	
Sub OnListSelect()
	Dim oSelection, sListID, oForm
	On Error resume Next
	Set oSelection = window.event.XMLrecord
	sListID = trim(oSelection.selectSingleNode("List_ID").text)
	searchform.UMGUIDList.Value = sListID
	On Error Goto 0
	rem searchform.ListsButton.disabled = false
End sub

Sub OnListUnSelect()
	Dim oSelection, sListID
	searchform.UMGUIDList.Value = ""
	rem searchform.ListsButton.disabled = true
End sub

Function sDeleteNameFromField(sName, sField)
	Dim ipos, sleft, sright
	On Error Resume Next
	ipos = Instr(1, sField, sName)
	If (ipos > 1) Then
		sleft = Left(sField,ipos-2)
		sright = Mid(sField, ipos+len(sName))
		sDeleteNameFromField = sleft&sright
	Else
		sDeleteNameFromField = Mid(sField, 2+len(sName))
	End if
	On Error Goto 0
End Function

Function sAddName2Field(sName, sField)
	If (len(sField) > 0) Then
		sAddName2Field = sField & ";" & sName
	Else
		sAddName2Field = sName
	End if
End Function



Sub ShowAddDialog(sEntity, sBasedOn)
	Dim sResponse
	If sBasedOn = "" Then
		addform.elements.<%= g_sKey %>.value = -1
		addform.submit()
		Exit sub
	End if
	sResponse = CInt(window.showModalDialog("dlg_AddEntity.asp", _
	sEntity & "|" & sBasedOn, "dialogHeight:180px;dialogWidth:400px;status:no;help:no"))
	Select case sResponse
		case "-1"
			window.event.returnValue = false
			EnableTask "new"
			EnableTask "open"
			EnableTask "delete"
		case "0"
			addform.elements.<%= g_sKey %>.value = -1
			addform.submit()
		case else
			addform.elements.<%= g_sKey %>.value = selectform.elements.<%= g_sKey %>.value
			addform.submit()
	End select
End sub
    
Sub ShowDelDialog(sEntity, sBasedOn, sFriendlyName)
	Dim iRetVal
	Dim sMsgString 
	If Len(sFriendlyName) <> 0 Then
		sMsgString = Replace("<%= L_ConfirmDelete_Text %>", "%1", sFriendlyName)
	Else
		sMsgString = "<%= L_ConfirmDeleteAllUsers_Text  %>"
	End if	
    iRetVal = MsgBox(sMsgString,vbYesNo,"<%= L_DELETE_DialogTitle %>")
	If (IRetVal = vbYes) Then
		ShowWaitDisplay()
		deleteform.submit()
	Else
		EnableTask "delete"
		EnableTask "open"
		EnableTask "new"
		EnableTask "find"
	End If	
End sub
   
Function ListSubmit()
	Dim oForm
	rem CHECK!!!!
    rem	set oForm = displayarea.all("ListsForm")
    rem	oForm.Action = "registered.asp"
    rem	oForm.submit
    ListSubmit = True
End Function

Function Find_Form()
	Dim  sOpt, sSearchText
	sOpt = selfindby.value
	If Len(Trim(sOpt)) = 0 Then
		alert "<%= L_SelectFindOption_Text %>"
		Find_Form = false
		Exit Function
	End if
	If strcomp(LCase(sOpt), LCase("Lists"), 1) <> 0 then
		sSearchText = document.all("fb" & sOpt).value
	Else
		sSearchText = searchform("UMGUIDList").value
	End if
	If (len(sSearchText) > 0 ) Then
	    If document.all("fb" & sOpt).valid Then
	        Find_Form = true
	    Else
		    alert sFormatString("<%= L_EnterValidFieldData_Text %>", Array(sSearchText)) 
	    End if
	Else
		alert "<%= L_EnterFindOption_Text %>"
		Find_Form = false
	End if
End Function

Function Submit_AdvQuery()
	Dim oCurrent, oXmL, oXMLHTTP, sStr
    
	sStr = divQueryBldr.GetExprBody()
	If (len(Trim(sStr)) = 0) OR IsNull(sStr) Then
		alert "<%= L_ConstructSearchQuery_Text %>"
		Submit_AdvQuery = False
	Else
		searchform.AdvQuery.value = sStr
		rem BUG oCurrent.submit()
		Submit_AdvQuery = True
	End if
End Function


Sub OnNewSort(oEvent)
	Dim oForm, child
	selectform.sortcol.value = oEvent.sortcol
	If (len(selectform.sortcol.value) > 0) Then
		selectform.sortdir.value = oEvent.sortdir
	End if
	selectform.action = "<%=g_sListPage%>"
	selectform.submit()
End sub

Sub OnNewPage(oEvent)
	dim oForm, block
	block = selectform.lock.value
	If not block Then
		selectform.lock.value ="true"
		selectform.type.value="New Page"
		selectform.newpage.value = oEvent.page
		selectform.action = "<%=g_sListPage%>"
		selectform.submit()
	End if
End Sub

Function bAlreadyHaveQB()
	Dim elOpt
	For each elOpt in searchform.selfindby.options
		If (strcomp(elOpt.Value, "QueryBldr", 1) = 0) Then
			bAlreadyHaveQB = True
			Exit function
		End if
	Next
	bAlreadyHaveQB = False
End Function

Sub window_OnLoad()
	EnableTask "new"
	EnableTask "find"
	DisplayGroup
	rem call hideFindBy when the page loads
	rem hideFindBy()
End Sub

Sub DisplayGroup()
	Dim i, str, obj, sOpt
	On Error Resume Next
	sOpt = selfindby.value
	Select case (Lcase(sOpt))
		case "querybldr"
			HideAllDivs()
			setFindByHeight(250)
			If g_bQbReady Then
				divfbGeneral.style.display = "block"
				'taCurrQuery.style.display = ""
				divQueryBldr.style.display = "block"
				divQueryBldr.SetExprBody("")
				divQueryBldr.Activate(TRUE)
			Else
				alert "<%= L_QueryBuilder_ErrorMessage %>"
			End if
		case "lists"
			HideAllDivs()
			setFindByHeight(300)
			divfbGeneral.style.display = "block"
			str = "divLists"
			document.all(str).style.display = "block"
			
		case "empty"
			HideAllDivs()
			setFindByHeight(150)
			divfbGeneral.style.display = "block"
		case else
		  HideAllDivs()
			setFindByHeight(120)
			divQueryBldr.Deactivate()
			divfbGeneral.style.display = "block"
			If sOpt <> "" Then
				str = "DIVfb" & sOpt
				document.all(str).style.display = "block"
			End if	
	End select
	showfindby()
	On Error GoTo 0
End Sub

Sub HideAllDivs()
	Dim node
	For each node in searchform.children
		If (Lcase(node.tagname) = "div") Then
			node.style.display = "none"
		End if	
    Next
End Sub

Sub reloadListSheet()
	Dim bReload, sOpt
	sOpt = selfindby.value
	On Error Resume Next
	Select case LCase(sOpt)
		case "querybldr"
			bReload = Submit_AdvQuery()
		case "lists"
			bReload = ListSubmit()
		case else
			bReload = find_form()
	End select
	
	If bReload Then
		OnUnSelectAllRows()
		lsProfiles.page = 1
		lsProfiles.reload("search")
	End if	
    On Error Goto 0
End Sub

Sub OnQBReady()
    Dim elOption
    Dim selfindval
    On Error Resume Next
    If (g_bQbReady = False) Then
        g_bQbReady = True
        selfindval = selfindby.value
        If strcomp(selfindval, "QueryBldr", 1) = 0 Then
            call DisplayGroup
        End if
    End if
    On Error GoTo 0 
End Sub

 -->
 </SCRIPT>