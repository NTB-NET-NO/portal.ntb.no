<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!-- #INCLUDE FILE='constants.asp' -->
<!-- #INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE='UOResources.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!-- #INCLUDE FILE = "../common/XMLRoutines.asp" -->
<!-- #INCLUDE FILE = "../common/UserOrgListFns.asp" -->

<%
Dim iCurpage , iPrevPage
Dim sortCol , sortDir
Dim g_bIsADInstallation
Dim g_dRequest
Dim g_MSCSDefaultLocale, g_MSCSCurrencyLocale, g_sMSCSDateFormat, g_sMSCSWeekStartDay, g_sMSCSCurrencyFormat, g_sMSCSNumberFormat

' -- get locale info for Datafunctions and formats for widgets
g_MSCSDefaultLocale		= Application("MSCSDefaultLocale")
g_MSCSCurrencyLocale	= Application("MSCSCurrencyLocale")
g_sMSCSDateFormat		= Application("MSCSDateFormat")
g_sMSCSWeekStartDay		= Application("MSCSWeekStartDay")
g_sMSCSCurrencyFormat	= Application("MSCSCurrencyFormat")
g_sMSCSNumberFormat		= Application("MSCSNumberFormat")

set g_dRequest = dGetRequestXMLAsDict()

Rem Get the passed in values from the listsheet post
iCurPage = g_dRequest("page")
'iPrevPage = Session("PrevPage")
'Session("PrevPage") = iCurPage
sortCol = g_dRequest("column")
sortDir = g_dRequest("direction")
Call Search()

Sub Search()
	Dim sFindBy
	Dim dState, aIDCol , query
	DIm rs,cn,bNoQuery,bOK
	Dim aprops
	Dim oXMLProfileDoc
	Dim sTempName
	Dim sDropQuery
	Dim sListID
	Dim LM
	Dim bskipflag
	bskipflag = false

	Set dState = dGetUMStateDictionary(g_sUMState)
	aprops = Session(g_sPropsLabel)
	set oXMLProfileDoc = Session(g_sProfileLabel)
	g_bIsADInstallation = bIsAdInstallation(oXMLProfileDoc)
	
	rem if (strcomp(g_dRequest("findbytype"), "advanced", 1) = 0) then
	if (strcomp(g_dRequest("selfindby"), "QueryBldr", 1) = 0) then
      dim oxml, sStr
      set oXML = Server.CreateObject("MSXML.DomDocument")
      sStr = g_dRequest("AdvQuery")
      oxml.loadxml("<XML>" & sStr & "</XML>")
      set Session(g_sAdvQueryLabel) = oXML'.DocumentElement.XML
	end if
	dState.Value("ChangeOrder") = False
	if (len(sortcol) > 0) then
	  if strcomp(sortcol, dState.Value("SortColumn"), 0) = 0 then
	      if (dState.Value("SortDirection") = "asc") then
	          dState.Value("SortDirection") = "desc"
	      else
	          dState.Value("SortDirection") = "asc"
	      end if
	  end if
	  if LCase(sortcol) = "list_name" then 
	      bskipflag = True
	  else    
			dState.Value("SortColumn") = sortcol
	  end if		
	  if NOT isNull(sortdir) then
	      if len(sortdir) > 0 then
	          dState.Value("SortDirection") = sortdir
	      end if
	  end if
	  dState.Value("action") = "query"
	  dState.Value("ChangeOrder") = True
	end if

	sFindBy = trim(g_dRequest("selfindby"))
	if lcase(sFindBy) = "lists" and Not bskipflag then
			'the page has been sent the guid of a list of user guids. Need to use ListManager to get a 
		'copy of the list in our own private DB. Then do queries against it to get the user guids out.
		Call MakeTempListTable(dState)
	elseif len(sFindBy) > 0 then
		dState.Value("UMListMode") = False
		dState.Value("action") = "query"
		dState.Value("FindBy") = sFindBy
		dState.Value("FindByText") = Trim(g_dRequest("fb" & sFindBy))
		dState.Value("ChangeSearch") = True
	end if

	if dState.Value("ListNoSelect") = True then 
	   Call ThrowWarningDialog(L_UsersListSelect_ErrorMessage)
	   dState.Value("ListNoSelect") = False
	   Exit Sub
	end if   
	
	if dState.Value("UMListMode") and Not bskipflag then
	'	Call DetermineIDColumn(oXMLProfileDoc, aprops, aIDCol)
		redim aIDCol(2)
        aIDCol(2) = nGetMatchingEntry(XML_ATTR_USERID, aprops)
        aIDCol(1) = sGetPropName(aprops(aIDCol(2)))
		Call MakeLMListQuery(dState, aprops, aIDCol, iCurPage)
	elseif Not bskipflag then
		Call NewQuery(dState,aprops)
	End If	
	
	set cn = cnGetProviderConnection(dState)

	if bIsDictEmpty(dState.Value("query")) then
	  bNoQuery = True
	else
	  query = dState.Value("query")
	  Session("UMQuery") = query
	end if
	
	if NOT bNoQuery and Not bskipflag then
		bOK = bSafeExec(rs, cn, Query)
		If Not bOK then
		'	Response.Write getErrorXML(Session("ErrorMessage"))
			Call ThrowWarningDialog(Session("ErrorMessage") &  L_RefreshConnection_Text)
		Else
			Call WriteOutRS2XML(rs, (iCurPage -1) * g_iListPageSize)

			'Response.Write xmlGetXMLFromRSEx(rs, (iCurPage -1) * g_iListPageSize, g_iListPageSize, 0, NULL).xml
		End If	
	elseif bskipflag then
	   'Session("UListSORTORDER") = sortdir
	    Set rs = GetUserListRS(dState.Value("SortDirection"))
	    Call WriteOutRS2XML(rs, (iCurPage -1) * g_iListPageSize)
	  
	else
		Call WriteBlankXML()
	end if

  Set rs = Nothing
  Set cn = Nothing	 	
End Sub

Sub WriteOutRS2XML(rsQuery, nStartRecord)
	Dim xmlOutPut
	Set xmlOutPut = xmlGetXMLFromRSEx(rsQuery, (iCurPage -1) * g_iListPageSize, g_iListPageSize, TOO_BIG_TO_COUNT, NULL)
	If IsObject(xmlOutPut) then
		Response.Write xmlOutPut.xml
	End if
end Sub


Sub WriteBlankXML()
	Dim sXML
	sXML= "<document><record/></document>"
	Response.Write sXML
	
End Sub

Function getErrorXML(sErrorText)
  
  getErrorXML = "<document>"
  getErrorXML = getErrorXML & "<ERROR ID='0x" & hex(Err.number) & "' SOURCE='" & sErrorText & " - " & Err.source & "'>" & _
			"<![CDATA[" & Session("ErrorMessage") & "]]></ERROR>"
  getErrorXML = getErrorXML & "</document>"
  
  Err.Clear
  
End function

%>