<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='constants.asp' -->
<!--#INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE='UOResources.asp' -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='../common/XMLroutines.asp' -->
<!--#INCLUDE FILE='../common/UserOrgListFns.asp' -->

<%

Dim g_bIsADInstallation
Dim g_sEntity, g_sKey, g_sStatusText, g_ColumnSize, g_rsQuery, g_sQuery, g_sName
Dim g_sFindBy, g_sFindBytext

call Main

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sub GetAvailableLists(sHTMLLists)

'
'	sHTMLLists - returns HTML with all lists available
'
sub GetAvailableLists(sHTMLLists)
	dim lm, rs, oXML, oXMLEl, oXMLOps, oXMLCols, oXML2, oXMLRec ,olistRS ,rcount
	set oXML = Server.CreateObject("MSXML.DOMDocument")
	set oXML2 = Server.CreateObject("MSXML.DOMDocument")

	REM --Set up List meta
	set oXMLEl = oXML.CreateElement("listsheet")
	set oXML.DocumentElement = oXMLEl

	set oXMLEl = oXML.CreateElement("global")
	oXMLEl.SetAttribute "selection", "single"
	'oXMLEl.SetAttribute "headers", "no"
	'oXMLEl.SetAttribute "pagecontrols", "no"
	oXML.DocumentElement.appendChild oXMLEl
	
	set oXMLCols = oXML.CreateElement("columns")
	oXML.DocumentElement.appendchild oXMLCols
	
	set oXMLEl = oXML.CreateElement("column")
	oXMLEl.SetAttribute "id", "List_ID"
	oXMLEl.SetAttribute "hide", "yes"
	oXMLEl.SetAttribute "key", "yes"
	oXMLEl.text = "ID"
	oXMLCols.appendChild oXMLEl

	set oXMLEl = oXML.CreateElement("column")
	oXMLEl.SetAttribute "id", "List_Name"
	oXMLEl.SetAttribute "width", "200"
	oXMLEl.text = L_UserListName_Text
	oXMLCols.appendChild oXMLEl


	set oXMLOps = oXML.CreateElement("operations")
	oXML.DocumentElement.appendchild oXMLOps

	set oXMLEl = oXML.CreateElement("newpage")
	oXMLEl.SetAttribute "formid", "newpageform2"
	oXMLOps.appendChild oXMLEl
	set oXMLEl = oXML.CreateElement("sort")
	oXMLEl.SetAttribute "formid", "searchform"
	oXMLOps.appendChild oXMLEl
	set oXMLEl = oXML.CreateElement("search")
	oXMLEl.SetAttribute "formid", "searchform"
	oXMLOps.appendChild oXMLEl
    

REM --set up XML for list data
	set oXMLEl = oXML2.CreateElement("document")
	set oXML2.DocumentElement = oXMLEl
	On Error Resume Next
	Err.Clear
	rcount = 0
	set LM = Server.CreateObject("Commerce.ListManager")
	if Err.number <> 0 then
		Session("UMLMProblem") = True
		Session("UMLMProblemText") = L_LMInstance_ErrorMessage
	end if

'	lm.Initialize sGetListDSN, sGetMasterListTableName
    lm.Initialize sGetListDSN
    
	if Err.number <> 0 then
		if (Session("UMLMProblem")<> True) then
			Session("UMLMProblem") = True
			Session("UMLMProblemText") = sFormatString(L_InvalidListDN_ErrorMessage, Array(sGetListDSN))
		end if
		set oXMLRec = oXML2.CreateElement("record")
		oXMLRec.text = ""
		oXML2.documentelement.appendchild oXMLRec
		Err.Clear
		On error goto 0
	else
		Session("UMLMProblem") = False
		On error goto 0
		set rs = lm.GetLists()
		Set olistRS = Server.CreateObject("ADODB.Recordset")
	
		olistRS.Fields.Append "List_ID" ,rs("List_ID").Type, rs("List_ID").DefinedSize, 32
		olistRS.Fields.Append "List_Name" ,rs("List_Name").Type, rs("List_Name").DefinedSize, 32    
		olistRS.Open
	
		if rs.EOF then
		REM --	Response.Write "No Lists"
				set oXMLRec = oXML2.CreateElement("record")
				oXMLRec.text = ""
				oXML2.documentelement.appendchild oXMLRec
		Else

			Do while NOT rs.EOF and  rcount < 20
				'filter out only the User Lists for display that have not failed 
				'and are not hidden..Also filter out empty lists.
				If (rs("list_flags") AND 4) AND (rs("list_status") <> 3 ) AND (rs("list_size") <> 0) Then
		'		If (rs("list_flags") AND 4) AND (rs("list_status") <> 3 )  Then
				  If NOT (rs("list_flags") AND 1 ) Then  
					set oXMLRec = oXML2.CreateElement("record")
					
					set oXMLEl = oXML2.CreateElement("List_ID")
					oXMLEl.text = rs("List_ID")
					oXMLRec.appendchild oXMLEl

					set oXMLEl = oXML2.CreateElement("List_Name")
					oXMLEl.text = rs("List_Name")
					oXMLRec.appendchild oXMLEl
						
					oXML2.documentelement.appendchild oXMLRec
					olistRS.AddNew
					olistRS.Fields("List_ID").Value = rs("List_ID")
					olistRS.Fields("List_Name").Value = rs("List_Name")
					rcount = rcount + 1
				  End If 
				End If	
				rs.movenext
			Loop
		End If
	End If
    If rcount >= 1 Then olistRS.MoveFirst 
 '   Set Session("LISTRS")= olistRS	
    Set rs = Nothing
	set LM = nothing
	sHTMLLists = "<xml id='lsListofListsMeta'>" & oXML.documentelement.xml & "</xml>"
	sHTMLLists = sHTMLLists & "<xml id='lsListofListsData'>" & oXML2.documentelement.xml & "</xml>"
	
end sub


'***********************************************
'Main begins here.
'***********************************************
sub Main
	Dim aIDCol
	dim oXMLDoc, oXMLProfileDoc
	dim aprops, agroups
	dim rsUsers, oXMLSearchColumns
	dim nitems, shtmlLists
	dim dState, sSQL, sExtraStat
	Dim dSelect, auserIds , aNames, i
	Dim sitetermURL , profileSRC ,exprarch ,typeops
    
	set dState = dGetUMStateDictionary(g_sUMState)
	g_sEntity = Session(g_sEntityLabel)
	aprops = Session(g_sPropsLabel)
	agroups = Session(g_sGroupsLabel)
	aIDCol = Session(g_sIDColLabel)
	g_sName = Session(g_sNameLabel)
	g_sKey = sGetIDName(aIDCol)
	set oXMLProfileDoc = Session(g_sProfileLabel)
	
	call GetAvailableLists(shtmlLists)
	g_sStatusText = ""
	g_ColumnSize = 20 REM -- in percentage


    Session("USERHITS") = 0
	' added to get ParentDN into Query
	g_bIsADInstallation = bIsAdInstallation(oXMLProfileDoc)

	'Call to get the selected the dictionary and set the action value 
	' if it's a delete
	Call ProcessForm(dState)
    
	call DetermineAction(dState, aprops)

	g_sQuery = dState.Value("query")

	if strcomp("yes", Session("UMOrgRestrict"), 1) = 0 then
		sExtraStat = sFormatString(L_ForOrg_Text, Array(Session("UMOrgName")))
	else
		sExtraStat = ""
	end if
	
	REM -- clear users from the selected dictionary.
	call ClearUsers(dState)

	if IsNull(dState.Value("findby")) then
	    g_sFindBy = ""
	else
	    g_sFindBy = Trim(dState.Value("findby"))
	    g_sFindByText = Trim(dState.Value("FindByText"))
	end if
	
	If Session("ErrorMessage") = "" Then
		Set rsUsers = rsGetListSheetRecordSet(dState)
	End If

   
    sitetermURL= sRelURL2AbsURL("../common/QBSiteTermsTransform.asp") & "?LoadCatalogSets=" & true  
    profileSRC = sRelURL2AbsURL("../common/QBProfileTransform.asp") & "?CURR_PROFILE=" & CURR_PROFILE 
    exprarch = sRelURL2AbsURL("../exprarch")
    typeops = sRelURL2AbsURL("../users/TypesOpsUO.xml")

%>
<HTML>
<HEAD>
    <TITLE> BizDesk </TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<LINK REL='stylesheet' TYPE='text/css' HREF='/widgets/exprbldrhtc/exprbldr.css'>

<!--#INCLUDE FILE="ListpageRoutines.asp" -->



</HEAD>
<BODY SCROLL=no>
<%

g_sStatusText = g_sStatusText & sExtraStat
InsertTaskBar L_PageTitleRegisteredUsers_Text, g_sStatusText

%>

<!-- FIND BY STARTS HERE - add your group ids to the option value -->
<DIV ID="bdfindbycontent" CLASS='findByContent'>
<FORM ACTION='SearchPage.asp' ID='searchform' NAME='searchform'>
<%= sGetSearchXML(oXMLProfileDoc, oXMLSearchColumns, dState) %>

<!-- FIND BY ENDS HERE - just add DIVs for your own groups as below -->
<DIV ID="divfbGeneral" STYLE="display:none">
	<TABLE>
		<TR>
			<TD WIDTH='100'>
				<SPAN ID='findbylabel' TITLE='<%=L_FindLabel_Text %>'><%= L_SearchCategoryLabel_Text %></SPAN>
			</TD>
			<TD WIDTH='200'>
			
			<DIV ID='selfindby' CLASS='editField' 
					METAXML='fbMeta' DATAXML='fbData'><%= L_Loading_Text %></DIV>									
			</TD>
			<TD ALIGN='RIGHT'>
				<BUTTON CLASS='bdbutton' LANGUAGE='VBScript' ONCLICK='reloadListSheet()' 
						ID='btnfindby' NAME='btnfindby' TITLE='<%= L_FindButton_Text %>'><%=L_FindButton_Text %></BUTTON>
			</TD>
		
		</TR>	
	</TABLE>	
</DIV>	

<%=sPrintSearchForms(oXMLSearchColumns)%>

<XML ID="xmlisConfig">
  <EBCONFIG>
    <DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
    <CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
	<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
    <DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
		<HELPTOPIC><%= sRelURL2AbsURL("../docs/default.asp?helptopic=cs_bd_usersgrps_xapq.htm")%></HELPTOPIC>
    <EXPR-ASP><%=exprarch%></EXPR-ASP>
    <!-- The presence of the DEBUG node tells CB to output debug info. -->
    <DEBUG />
    <!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
    <PROFILE-LIST>
    <PROFILE SRC="<%=profileSRC%>"></PROFILE>
    </PROFILE-LIST>
    <SITE-TERMS URL="<%=sitetermURL%>" />
    <MESSAGE><%= L_SearchUser_Text %></MESSAGE>
    <FILTER-PROPERTIES>(not(@isSearchable) || @isSearchable != '0') $and$ (not(@isActive) || @isActive!= '0')  $and$ (not(@isMultiValued) || @isMultiValued = '0') $and$ (@propType != 'PASSWORD') $and$ (@propType != 'BINARY')  $and$ (@propType != 'IMAGE')</FILTER-PROPERTIES>
    <TYPESOPS><%=typeops%></TYPESOPS>
  </EBCONFIG>
</XML>

<DIV ID="divQueryBldr" CLASS="clsQueryBldr"
  STYLE="behavior:url(/widgets/exprbldrhtc/QueryBuilder.htc); width:95%; display:none; border-style:none; background-color:transparent" XMLCfgID="xmlisConfig" onready="OnQBReady()" fontsize=6 >
    <H3 ALIGN="center"><%= L_LoadingQB_Text %></H3>
    <% = L_CurrentQuery_Text %>
</DIV> 
 <INPUT TYPE='hidden' NAME='AdvQuery' ID='AdvQuery' VALUE=''>
    
<DIV ID="divLists" STYLE="display:none" >
	<% if True = Session("UMLMProblem") then %>
	<%=Session("UMLMProblemText")%>
	<%end if%>
		<INPUT TYPE='hidden' NAME='UMGUIDList'>
	  <DIV ID='lsListofLists' CLASS='listSheet'
		DataXML='lsListofListsData'
		MetaXML='lsListofListsMeta'
		LANGUAGE='VBScript'
		OnRowSelect='OnListSelect()'
		OnRowUnselect='OnListUnselect()'><%= L_LoadingList_Text %></DIV>
		
</DIV>

<DIV ID='displayarea'>

</DIV>
<!--XML for List of available lists -->
<%=sHTMLLists%>


</FORM>

</DIV>

<!-- FIND BY DIVS ENDS HERE -->

<DIV ID="bdcontentarea">


<!-- for Add Dialog ONTASK='ShowAddDialog "<%= g_sEntity %>", selectform.elements.friendlyname.value' -->

<FORM ACTION='' METHOD='POST' ID='addform' NAME='addform' >
    <INPUT TYPE='hidden' ID='type' NAME='type' VALUE='add'>
    <INPUT TYPE='hidden' ID="<%= g_sKey %>" NAME="<%= g_sKey %>" VALUE='-1'>
</FORM>
<FORM ACTION='' METHOD='POST' ID='copyform' NAME='copyform' >
		<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='copy'>
    <INPUT TYPE='hidden' ID="<%= g_sKey %>" NAME="<%= g_sKey %>">
    <INPUT TYPE='hidden' ID='friendlyname' NAME='friendlyname'>
</FORM>
<FORM ACTION='' METHOD='POST' ID='deleteform' NAME='deleteform' ONTASK='ShowDelDialog "<%= g_sEntity %>", deleteform.elements.<%= g_sKey %>.value, deleteform.elements.friendlyname.value'>
    <INPUT TYPE='hidden' ID='type' NAME='type' VALUE='delete'>
    <INPUT TYPE='hidden' ID="<%= g_sKey %>" NAME="<%= g_sKey %>">
    <INPUT TYPE='hidden' ID='friendlyname' NAME='friendlyname'>
    <INPUT TYPE='hidden' ID='numitems' NAME='numitems' VALUE='0'>
    <INPUT TYPE='hidden' ID='SelType' NAME='SelType'>
    <INPUT TYPE='hidden' ID='Query' NAME='Query'>
</FORM>

<FORM ACTION="ListPaging.asp" ID="newpageform" NAME="newpageform" >
	<INPUT TYPE="hidden" ID="query" NAME="query" VALUE="<%= g_sQuery %>">


</FORM>

<FORM ACTION="UserListPaging.asp" ID="newpageform2" NAME="newpageform2" >
	 <INPUT TYPE='hidden' ID='ListofList' NAME='ListofList' VALUE="true">

</FORM>

<%
    call PrintListSheet(dState, aprops, rsUsers)
    
    Set rsUsers = Nothing
    If	Session("MSCSSiteTermVer") <> Application("MSCSSiteTermVer") Then
	'	Session("MSCSSiteTermVer") = Application("MSCSSiteTermVer")
	    Call GetNewSiteTermXML(True)
	End IF	
%>

<div ID="filtertext" class="filtertext" style="MARGIN-TOP: 20px"><%= L_BDSSFilter_Text & " " & L_UseWildCardSearch_Text  %></div>

    <DIV ID=lsProfiles CLASS='listSheet' STYLE="MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px; HEIGHT: 80%"
        DataXML='profilesList'
        MetaXML='profilesMeta'
        LANGUAGE='VBScript'
        OnRowSelect='OnSelectRow()'
        OnRowUnselect='OnUnselectRow()'
        OnAllRowsSelect='OnSelectAllRows()'
        OnAllRowsUnselect='OnUnSelectAllRows()'><%= L_LoadingList_Text %></DIV>


<FORM ACTION='' METHOD='POST' ID='selectform'>
    <INPUT TYPE='hidden' ID='type' NAME='type' VALUE='open'>
    <INPUT TYPE='hidden' ID="<%= g_sKey %>" NAME="<%= g_sKey %>">
    <INPUT TYPE='hidden' ID='friendlyname' NAME='friendlyname'>
    <INPUT TYPE='hidden' ID='numitems' NAME='numitems' VALUE='0'>
    <INPUT TYPE='hidden' ID='lock' NAME='lock' VALUE='false'>
    <INPUT TYPE='hidden' ID='sortcol' NAME='sortcol' >
    <INPUT TYPE='hidden' ID='sortdir' NAME='sortdir' >
    <INPUT TYPE='hidden' ID='newpage' NAME='newpage'>
    <INPUT TYPE='hidden' ID='SelType' NAME='SelType'>
    <INPUT TYPE='hidden' ID='Query' NAME='Query'>
</FORM>
</div>
</BODY>
</HTML>
<%End Sub%>

