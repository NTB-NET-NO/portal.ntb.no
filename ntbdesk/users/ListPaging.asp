<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='constants.asp' -->
<!--#INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE='UOResources.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE = "../include/ASPUtil.asp" -->
<!--#INCLUDE FILE = "../include/HTTPXMLUtil.asp" -->
<!--#INCLUDE FILE = "../common/XMLRoutines.asp" -->
<!--#INCLUDE FILE = "../common/UserOrgListFns.asp" -->

<%
Dim iCurpage , iPrevPage
Dim sortCol , sortDir, g_dForm

Set g_dForm = dGetRequestXMLAsDict()

iCurPage = g_dForm("page")
iPrevPage = g_dForm("PrevPage")
'Session("PrevPage") = iCurPage
sortCol = g_dForm("column")
sortDir = g_dForm("direction")

Call PageChange()

Sub PageChange()
	Dim dState, aIDCol , query
	Dim rs,cn,bNoQuery,bRet
	Dim aprops
    
	Set dState = dGetUMStateDictionary(g_sUMState)
	aIDCol = Session(g_sIDColLabel)
	aprops = Session(g_sPropsLabel)
	'check aIDCol
	redim aIDCol(2)
    aIDCol(2) = nGetMatchingEntry(XML_ATTR_USERID, aprops)
    aIDCol(1) = sGetPropName(aprops(aIDCol(2)))
	
	set cn = cnGetProviderConnection(dState)

	if bIsDictEmpty(dState.Value("query")) then
	  bNoQuery = True
	else
	  query = dState.Value("query")
	  Session("UMQuery") = query
	end if
	
	if dState.Value("UMListMode") then
		Call MakeLMListQuery(dState, aprops, aIDCol, iCurPage)
		query = dState.Value("query")
	end if
	
	if NOT bNoQuery then 
		bRet = bSafeExec(rs, cn, Query)
		If Not bRet Then
			Response.Write getErrorXML(L_SearchFail_ErrorMessage)
		Else
			if dState.Value("UMListMode") then
				Call WriteOutRS2XML(rs, 0)
			'   Call WriteOutRS2XML(rs.Clone, 0)
			else
				Call WriteOutRS2XML(rs, (iCurPage -1) * g_iListPageSize)
				'Response.Write xmlGetXMLFromRSEx(rs, (iCurPage -1) * g_iListPageSize, g_iListPageSize, 5000, NULL).xml
			end if
		End If	
	else
		Call WriteBlankXML()
	end if
	Set rs = Nothing
	Set cn = Nothing	 	
End Sub

Sub WriteOutRS2XML(rsQuery, nStartRecord)
	Dim xmlOutPut
	Dim val
	Dim xmlDoc
	Set xmlOutPut = xmlGetXMLFromRSEx(rsQuery, nStartRecord, g_iListPageSize, TOO_BIG_TO_COUNT, NULL)
	If IsObject(xmlOutPut) then
	  
		If xmlOutput.childNodes.Length = 0 then
		    if (iCurpage - iPrevPage) = 1 then
				Call AddWarningNode(xmlOutput.ownerDocument, L_NoMorePages_ErrorMessage)
			else
				Call AddWarningNode(xmlOutput.ownerDocument, L_NoPage_ErrorMessage)
			end if	
		ElseIf xmlOutput.childNodes.Length = 1 then 
	       ' Set xmlOutput = CreateObject("MSXML.DOMDocument")	
	       ' set xmlDocNode = xmlOutput.createElement("document")	
	        val = xmlOutPut.childNodes.item(0).text 
	        if Len(val) = 0 then 
	            
	            set xmlDoc = Server.CreateObject("MSXML.DOMDocument")
				set xmlOutPut = xmlDoc.createElement("document")
				set xmlDoc.documentElement = xmlOutPut
                xmlOutPut.setAttribute "recordcount", -1 
				'xmlOutput.LoadXML "<document recordcount="-1"/>"
				Call AddWarningNode(xmlOutPut.ownerDocument, L_NoMorePages_ErrorMessage)
			End if
				
		End If
	    Response.Write xmlOutPut.xml
	End if
end Sub

Sub WriteBlankXML()
	Dim sXML
	sXML= "<document><record/></document>"
	Response.Write sXML
End Sub

function getErrorXML(sErrorText)
  
  getErrorXML = "<document>"
  getErrorXML = getErrorXML & "<ERROR ID='0x" & hex(Err.number) & "' SOURCE='" & sErrorText & " - " & Err.source & "'>" & _
			"<![CDATA[" & Err.description & "]]></ERROR>"
  getErrorXML = getErrorXML & "</document>"
  
  Err.Clear
  
End function

%>