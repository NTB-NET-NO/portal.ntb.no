<%
'Constants for labelling Session variables, so that OM and OM won't interfere with one another
const g_iListPageSize = 20
const g_sListPage     = "registered.asp"
const g_sEditPage     = "reg_edit.asp"
const g_sDELIMITER    = ";"

const g_sEntityLabel = "UMEntity"
const g_sProfileLabel = "UMProfile"
const g_sKeyLabel = "UMKey"
const g_sNameLabel = "UMName"
const g_sPropsLabel = "UMProps"
const g_sGroupsLabel = "UMGroups"
const g_sIDColLabel = "UMIDCol"
const g_sStateLabel = "UMState"
const g_sGUIDListLabel = "UMGUIDList"
const g_sAdvQueryLabel = "UMAdvQuery"
const g_sUMState = "User"

const XML_ATTR_ORGID = "org_id"
const XML_ATTR_USERID = "user_id"
const XML_ATTR_ORG_FRIENDLYNAME = "name"

const CURR_PROFILE = "Profile Definitions.UserObject"
const ORG_PROFILE = "Organization"
const USER_PROFILE = "UserObject"
const GROUP_PROFILE = "ADGroup"
const ADDRESS_PROFILE = "Address"
const REF_GROUP_PROFILE = "Profile Definitions.ADGroup"
const REF_USER_PROFILE = "Profile Definitions.UserObject"
const REF_ORG_PROFILE = "Profile Definitions.Organization"

%>