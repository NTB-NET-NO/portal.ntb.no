<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<%
Const sfldSAMAccountName = "SAMAccountName"
Dim g_bisADInstallation
	
%>
<!--#INCLUDE FILE='constants.asp' -->
<!--#INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE='UOResources.asp' -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='../common/XMLroutines.asp' -->
<!--#INCLUDE FILE='../common/UserOrgEditFns.asp' -->
<!--#INCLUDE FILE='../common/DARoutines.asp' -->

<%

	Dim g_sKey, g_sKeyValue, g_sType, g_sStatus, g_sEntity, g_sReadOnly
	Dim g_sTitleText
	Dim aprops, agroups
	Dim g_sPrevValue , g_bBulkUpdate, g_bCopy
	Dim g_iPartnerDeskRoleFlags
	Dim g_sDefaultNamingContext
	Dim sXML
	Dim bInsertError
	Dim bUpdateError
	
	g_bCopy = False
	bInsertError = False
        bUpdateError = False
    
   	REM -- type is either "add", "save", "copy", "savenew" or "open" (default="open")

	Dim cn, oRSQuery, sQuery, oXMLDoc, aIDCol, bIDInt, g_sFriendly, nitems, aGUIDs
	Dim bIsInsert, iIDtype, i, sWhere, dState, form, dSelect, user, sHTMLCase, nBrowse
	Dim nID
	Dim sBulkWhereClause
	Dim bRet
	Dim g_sOrgName
	Dim sListTable
	Dim arrType
	Dim sitetermURL , profileSRC ,exprarch ,typeops
	bRet = True
	REM -- Get the state Information for the User into dictionary. The Profile for which 
	REM -- state is retrieved is passed in as a parameter

	Set dState = dGetUMStateDictionary("User")
	Set cn = cnGetProviderConnection(dState)
	Set dSelect = dState.Value("Selected")
    
	REM -- stores the previous values. Used for building the Update clause
	REM -- The update clause would update only the changed values
	Set g_sPrevValue = Server.CreateObject("Commerce.Dictionary")
	

    
	REM -- aprops array contains the properties and attributes for the profile loaded
	REM -- oXMLDoc contains the profile
	call GetProperties(aprops, agroups, oXmlDoc, dState)
    
	REM -- Stores the Primary key column in aIDCol
'	call DetermineIDColumn(oXMLDoc, aprops, aIDCol)
   	reDim aIDCol(2)
    aIDCol(2) = nGetMatchingEntry(XML_ATTR_USERID, aprops)
	aIDCol(1) = sGetPropName(aprops(aIDCol(2)))

	Set Session(g_sProfileLabel) = oXMLDoc
	REM -- check if it is AD installation
	g_bIsADInstallation = bIsAdInstallation(oXMLDoc)
		
	REM -- store the property array
	Session(g_sPropsLabel) = aprops
	Session(g_sGroupsLabel) = agroups
	Session(g_sIDColLabel) = aIDCol 
	nID = nGetIDColNo(aIDCol)
	nBrowse = nGetNumBrowses(oXMLDoc, sHTMLCase)
	bIDInt = bIsIDInt(aprops, aIDCol)
	REM -- have to get the friendlyname field here
    
	bIsInsert = False
	If len(trim(Request.Form("logon_name"))) > 0 then
	    g_sFriendly = Request.Form("logon_name")
	Else
	    g_sFriendly = Request.Form("friendlyname")
	End If
	
	arrType = Split(CStr(Request.Form("type")),",")
    g_sType = arrType(0)
	g_sEntity = L_UserEntity_Text
	g_sKey = sGetIDName(aIDCol)

	g_sKeyValue = Request.form(g_sKey)
	
	Dim aUserIDs, aNames 
	nitems = dSelect.count
	If nItems = 0 and Not IsEmpty(Request.Form("numitems")) then
		nitems = CInt(Request.Form("numitems"))
		REM --Unpack GUIDs of selected organizations, put into dictionary for safe keeping on server
		If len(nitems) > 0 then
		  If nitems = 1 then
		    dSelect.Value(g_sKeyValue) = g_sFriendly
		  ElseIf nitems > 1 Then
		    aUserIDs = split(g_sKeyValue, g_sDELIMITER)
		    aNames = split(g_sFriendly, g_sDELIMITER)
		    For i = LBound(aUserIDs) To UBound(aUserIDs)
		        dSelect.Value(aUserIDs(i)) = aNames(i)
		    Next
		  End if
		End if
	End If
	If strcomp(Request.Form("SelType") ,"All",1) =0 Then
		nitems = -1
		g_bBulkUpdate = True
		sBulkWhereClause = sGetWhereClause(dState.Value("query"))
		sListTable = dState.Value("ListTable")
	Elseif strcomp(Request.Form("bBulkUpdate"), "True", 1) = 0 then
		g_bBulkUpdate = True
		sBulkWhereClause = Request.Form("sBulkWhereClause")
		sListTable = Request.Form("sListTable")
		dState.Value("ListTable") = sListTable
	End If	
	REM -- if pfid was empty set key value to -1 (for add)
	If g_sKeyValue = "" Or g_sKeyValue = "-1" Then g_sKeyValue = "-1"
	g_sStatus = ""
	If strcomp(g_sType, "save", 1) = 0 Or strcomp(g_sType, "savenew", 1) = 0 Then
	    iIDtype = sGetPropType(aprops(nID))
	    bIsInsert = (strcomp(Request.Form("ISINSERT"), "True",1) = 0)
		If  bIsInsert Then
		    
			Call MakeInsertQuery(aprops, sQuery, aIDCol, g_sKeyValue, bIDInt, dState)
			bRet = ExecCmd(sQuery, cn, _
								sFormatString(L_InsertEntity_ErrorMessage,Array(g_sEntity)))
			sQuery = ""
			If Not bRet Then
				g_sStatus =  sFormatString(L_CouldNotAdd_StatusBar, Array(g_sEntity, g_sFriendly))
				g_sType = "add"
				bInsertError = True 
				nitems = -1
			Else
				g_sStatus =  sFormatString(L_Add_StatusBar, Array(g_sEntity, g_sFriendly))
				REM --new user added to the Selected dictionary
				Call ClearUsers(dState)
				dSelect.Value(g_sKeyValue) = g_sFriendly
				REM -- add user to an existing group.
				
				g_sDefaultNamingContext = sDefaultNamingContext(oXMLDoc)
				
	    		Call AddUser(g_sFriendly, g_sOrgName)
	    	End If
		Else
	REM -- THIS IS WHERE CODE TO REALLY SAVE THE ITEM BEGINS
	  	  If nitems > 0 Then
			  ReDim aGUIDs(nitems-1)
			  i = 0
			  For Each user In dSelect
	            aGUIDs(i) = user
	            i = i+1
	          Next
	      End If  

	       If (nitems = 1) then
	        REM -- There is only one Org. Get it's name for display
	            If len(trim(Request.Form("Name"))) > 0 Then
	                dSelect.Value(aGUIDs(0)) = trim(Request.Form("Name"))
	            End If
	            g_sFriendly = dSelect.Value(aGUIDs(0))
	            REM -- Form Update Query and execute query
	            Call MakeUpdateQuery(aprops, sQuery, aIDCol, dState)
	            sWhere = " WHERE " & sBracket(sGetPropFullName(aprops(nID))) & " = " & sQuoteArg(aGUIDs(0), iIDType)
	            bRet = ExecCMD(sQuery & sWHERE, cn, _
										sFormatString(L_UpdateEntity_ErrorMessage,Array(g_sFriendly)))
										
	            REM -- Display status
	            If Not bRet Then
	                    bUpdateError = True 
	  					g_sStatus = sFormatString(L_CouldNotSave_StatusBar, Array(g_sEntity, g_sFriendly))
	  				'	Call RefreshSessionCache()
	            Else    
	                    g_sDefaultNamingContext = sDefaultNamingContext(oXMLDoc)
						Call AddUser(g_sFriendly, g_sOrgName)
						g_sStatus = sFormatString(L_Save_StatusBar, Array(g_sEntity, g_sFriendly))
				End If	
	       Else
	  					REM -- Build query from the Expression builder output.
	  					On error Resume next
	  					Call MakeMultipleUpdateQuery(aprops, sQuery)
	  					If g_bBulkUpdate Then
	  						REM -- If the select ALl is lists, do a separate processing by chunking the list
	  						REM -- otherwise form an appropriate bulk update query.
	  						REM -- Check the dState/ Form variable to see if this is a ListMode.
	  						Select Case dState.Value("UMListMode")
	  							Case true
	  								Call ListUpdateForSelectALL(cn, dState, aprops, aIDCol, sQuery)
	  							Case else
	  								If len(sBulkWhereClause) > 0 Then
	  									sWhere = " WHERE " & sBulkWhereClause
	  								End If	
	  								bRet = ExecCMD(sQuery & sWHERE, cn, _
	  													sFormatString(L_BulkUpdateEntity_ErrorMessage,Array(g_sEntity)))
	  						End Select							
	  					Else
	  						Dim strGUIDS
	  						For i = 0 to nitems-1
	  							aGUIDs(i) = sQuoteArg(aGUIDs(i), iIDType)
	  						Next
	  						strGUIDS = Join(aGUIDs, ",")
	  						REM -- TODO Build an IN Clause here
	  						REM --sWhere = " WHERE " & sBracket(sGetPropFullName(aprops(nID))) & " = " & sQuoteArg(aGUIDs(i), iIDType)
	  						sWhere = " WHERE " & sBracket(sGetPropFullName(aprops(nID))) & " IN (" & strGUIDS & " )"
	  						bRet = ExecCMD(sQuery & sWHERE, cn, _
	  											sFormatString(L_UpdateNEntity_ErrorMessage,Array(nitems, g_sEntity)))
	  	  				End If
	  	  			If Err.number <> 0 or Not bRet Then
	  	  				g_sStatus = sFormatString(L_CouldNotSave_StatusBar, Array(nitems, g_sEntity))	
	  	  				Err.Clear
	  	  			Else
	  	  				If nitems = -1 Then
	  	  					g_sStatus = sFormatString(L_SaveAll_StatusBar, Array(g_sEntity))
	  	  				Else	
	  	  					g_sStatus = sFormatString(L_Save_StatusBar, Array(nitems, g_sEntity))
	  	  				End If	
	  	  			End If
	  	  			On Error Goto 0	
					End if
		End If
	End If
  
   
   	Select Case (Lcase(g_sType))
	  Case "save", "open"
	    
	    bIsInsert = False
	    Select Case (nitems)
	      Case "0", "1"
	        Call FormQuery(sQuery, aprops, g_sKeyValue, nID, dState)
	        If bRet = False And Lcase(g_sType) = "save" Then 
	           bRet = False
	        Else   
			   bRet = ExecQuery(oRSQuery, cn, sQuery, L_NoUser_ErrorMessage)
			   On Error Resume Next
			   If Lcase(g_sType) = "open" Then 
			      Session("USERHITS") = Session("USERHITS") + 1	    
			      Session("UserGuid") = oRSQuery("GeneralInfo.User_id").Value
			      Session("UserName") = oRSQuery("GeneralInfo.Logon_name").Value
			      Set Session("UserRS") = oRSQuery
			   End if   
			   If Err.number <> 0 Then 
					On Error Goto 0 
					abort(L_UserProfileKeyMisplaced_Errormessage)
			   End If
			   On Error Goto 0 
			End If   
					If Not bRet Then
	  					REM -- display a blank edit sheet. Failed to retrieve user.
	  					If Lcase(g_sType) = "open" Then 
	  					   	sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
	  					Else	
	  						sXML = Session("UserXML")
	  					End If 	
	  				Else
	  					REM -- display the user
	  					sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 1, "", 0)
	  					Session("UserXML") = sXML
	  				End If	
	        Set oRSQuery = Nothing
	        
	      Case Else
			sitetermURL= sRelURL2AbsURL("../common/QBSiteTermsTransform.asp") & "?LoadCatalogSets=" & true  
			profileSRC = sRelURL2AbsURL("../common/QBProfileTransform.asp") & "?CURR_PROFILE=" & CURR_PROFILE 
			exprarch = sRelURL2AbsURL("../exprarch")
			typeops = sRelURL2AbsURL("../users/TypesOpsUpdate.xml")
	  		Call ShowQueryBuilder()
	        REM --call sXMLMakeEditSheets(oRSQuery, agroups, aprops, nitems, g_sKeyValue, nID)
	    End Select
	  	    
	  Case "savenew"
	        
	        Session("UserGuid") = ""
			Session("UserName") = ""
	  		If not bRet then
	  			 '   call FormQuery(sQuery, aprops, g_sKeyValue, nID, dState)
				 '   bRet = ExecQuery(oRSQuery, cn, sQuery, L_NoUser_ErrorMessage)
				 '	If Not bRet Then
	  			 	   REM -- display a blank edit sheet. Failed to open.
	  			 '	   sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
	  			 '	Else
	  			 	   REM -- display user with error
	  			 '	   sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 1, "", 0)
	  			 '	End If 
	  		Else
	  			 	REM -- show blank edit page
	  			 	bIsInsert = True
	  			 	g_sTitleText = sFormatString(L_New_Text, Array(g_sEntity))
	  			 	sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
	  			 	Session("UserXML") = sXML
	  		End If
	  		Set oRSQuery = Nothing	
	  Case "add"
	    Select case (nitems)
	  	Case "1"
	  	    Session("UserGuid") = ""
			Session("UserName") = ""
	  		If Not bRet then
	  			sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
	  		Else	
	  			sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, nitems, "", 0)
	  		End If	
	    Case else
	            sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, nitems, "", 0)
	    End Select
	    Set oRSQuery = Nothing
	  Case "copy"
	    
	  	g_bCopy = True
	  	Session("UserGuid") = ""
		Session("UserName") = ""
	    Call FormQuery(sQuery, aprops, g_sKeyValue, nID, dState)
	    bRet = ExecQuery(oRSQuery, cn, sQuery, L_CopyUserFail_ErrorMessage)
	    If Not bRet Then
	  			REM -- display a blank edit sheet. Failed to retrieve organization.
	  			sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
	  	Else	
	     	    sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, nitems, "", 0)
	     	    Session("UserXML") = sXML
	     	    Set Session("UserRS") = oRSQuery
	     	    Session("USERHITS") = Session("USERHITS") + 1	    
	    End If
	    Set oRSQuery = Nothing

	End Select
	Set cn = Nothing
	
	
 %>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<!-- #INCLUDE FILE="EditpageRoutines.asp" -->

</HEAD>
<BODY SCROLL='no'>
<%
    If g_sType = "add" Then
		'g_sTitleText = sFormatString(L_New_Text, Array(g_sEntity))
		 g_sTitleText = L_NewUser_Text
    Elseif g_sType = "savenew" Then
		If Not bRet then
			g_sTitleText = sFormatString(L_OpenUser_Text, Array(g_sfriendly)) 
		Else
			g_sTitleText = L_NewUser_Text
		End If	
    Elseif g_sType = "copy" Then 
		REM -- g_sType = "copy"
		g_sTitleText = sFormatString(L_Copy_Text, Array(g_sEntity, g_sfriendly))
		g_sTitleText = sFormatString(L_CopyUser_Text, Array( g_sfriendly))
    Else 
		REM -- g_sType = "open"

    If (nitems > 1) Then
          g_sTitleText = sFormatString(L_OpenUsers_Text, Array(nitems))
    ElseIf  nitems = -1 then 
        g_sTitleText = L_OpenAllUsers_Text
    Else
        g_sTitleText = sFormatString(L_OpenUser_Text, Array(g_sfriendly))       
    End If
          
        
    End If
%>

<%

    InsertEditTaskBar g_sTitleText, g_sStatus
%>
<!-- putting edit sheets within this container allows for scroling without 
		losing the task buttons off the top of the screen 
-->

<DIV ID='editSheetContainer' CLASS='editPageContainer' onChange='OnChange()'>

<FORM ID='profilesform' ACTION='' METHOD='post' ONTASK='OnSave()'>
<INPUT TYPE='hidden' NAME='type' VALUE='save'>
<INPUT TYPE='hidden' NAME='friendlyname' VALUE="<%= g_sFriendly%>">
<INPUT TYPE='hidden' NAME='numitems' VALUE="<%=nitems%>">
<INPUT TYPE='hidden' NAME='bBulkUpdate' VALUE="<%=g_bBulkUpdate %>">
<INPUT TYPE='hidden' NAME='sBulkWhereClause' VALUE="<%=sBulkWhereClause %>">
<INPUT TYPE='hidden' NAME='sListTable' VALUE="<%= sListTable %>">
<INPUT TYPE='hidden' NAME='AdvQuery'>
<%
REM --then you can either set g_rsQuery to a recordset or set g_sQuery to a SQL query string

REM --g_sReadOnly is used to set the key fields to readonly when opening existing item
REM --g_sReadOnly = "no"
REM --if g_sType <> "add" then g_sReadOnly = "yes"
REM --display Profiles groups
If bInsertError or bUpdateError Then 
'    Response.Write Session("UserXML")
	if Not IsEmpty(Session("UserUpdateRS")) or Not IsObject(Session("UserUpdateRS")) then 
	    if Session("USERHITS") >= 1  then  sXML = sXMLMakeEditSheets(Session("UserUpdateRS"), agroups, aprops, 1, "", 0)
    else
		sXML = sXMLMakeEditSheets(oRSQuery, agroups, aprops, 0, "", 0)
    end if   
	Response.Write sXML
Else
    Response.Write sXML
End IF

 
%>

<INPUT TYPE="HIDDEN" NAME="ISINSERT" ID="ISINSERT" VALUE="<%= bIsInsert%>">
</FORM>

</DIV>

<FORM ID="closeform" ACTION METHOD="post" ONTASK="OnClose">
</FORM>
<FORM ID="addform" ACTION METHOD="post" ONTASK='OnSaveNew()' >
</FORM>

</BODY>
</HTML>



<% sub ShowQueryBuilder()%>
   <XML ID="xmlisConfig">
		<EBCONFIG>
			<DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
			<CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
			<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
			<DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
		    <HELPTOPIC><%= sRelURL2AbsURL("../docs/default.asp?helptopic=cs_bd_usersgrps_xapq.htm")%></HELPTOPIC>
			<EXPR-ASP><%=exprarch%></EXPR-ASP>
			<!-- The presence of the DEBUG node tells CB to output debug info. -->
			<DEBUG />
			<!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
			<PROFILE-LIST>
			<PROFILE SRC="<%=profileSRC%>"></PROFILE>
			</PROFILE-LIST>
			<SITE-TERMS URL="<%=sitetermURL%>" />
			<MESSAGE><%= L_UpdateUserValues_Text %></MESSAGE>
			<FILTER-PROPERTIES>not(@keyDef) $and$ (not(@isUpdatable) || @isUpdatable != '0') $and$ (not(@isReadOnly) || @isReadOnly != '1') $and$ (not(@isActive) || @isActive!= '0') $and$ (not(@isMultiValued) || @isMultiValued = '0') $and$ (@propType != 'BINARY')  $and$ (@propType != 'IMAGE')</FILTER-PROPERTIES>
			<TYPESOPS><%=typeops%></TYPESOPS>
		</EBCONFIG>
	</XML>
	
	<DIV ID="divQueryBldr" CLASS="clsQueryBldr"
      STYLE="behavior:url(/widgets/exprbldrhtc/QueryBuilder.htc); width:100%;" XMLCfgID="xmlisConfig" ONREADY="OnQBReady()" onQueryChange="EnableTaskMultiEdit()">
        <H3 ALIGN="center"><%= L_LoadingQB_Text %></H3>
        <%= L_CurrentQuery_Text %>
    </DIV>
	
<% End Sub %>

<%
REM -- Get the container (RDN path) under which the user is added
Function sGetParentURL(dState)
	Dim rs, sOrgID, sOrgName, sUserName, sSQL, bExit, sBaseURL
	Dim bRet
	Dim arrGuid
	Dim iDim

	
	bExit = False
	sBaseURL = sGetBaseURL()
	sOrgID = Request.Form(XML_ATTR_ORGID)
	
	REM --added ParentDN clause
	if (len(trim(sOrgID)) > 0) then
		sSQL = "SELECT [GeneralInfo.Name] FROM [Organization] WHERE [GeneralInfo.org_id] = "
		sSQL = sSQL & "'" & sBracketGUID(sOrgID) & "' AND [ProfileSystem.ParentDN] = "
		sSQL = sSQL & "'" & sBaseURL & "'"
		REM -- set cn = Server.CreateObject("ADODB.Connection")
		REM -- cn.Open sGetProviderDSN(dState)

		bRet = bSafeExec(rs,cn,sSQL)
		if (rs.EOF or bRet = False) then
			bExit = True
		else
		    if Instr(1,rs.fields(0).Name,".") <> 0 then  
				g_sOrgName = rs("GeneralInfo.Name")
			else
			   	g_sOrgName = rs("Name")
			end if    	
		end if
		set rs = nothing
		REM -- cn.Close
		REM -- set cn = nothing
	else
	  	bExit = True
	end if
	
	if len(trim(sBaseURL)) = 0 then
		bExit = True
	end if
	REM --If bexit is true then we got no BaseContainer for users or no org name
	REM --so we need to put the user in the default container. Easiest way to do this is not to 
	REM --set ParentURL
	if bexit then
	    if len(trim(sBaseURL)) = 0 then
		    sGetParentURL = ""
		else
		    sGetparentURL =  sBaseURL 
		end if
	else
	    REM --We have a BaseURL and an OrgName so create the user under the appropriate org.
		sGetParentURL = "OU=" & g_sOrgName & "," &  sBaseURL  REM --& ";cn=" & sUserName
	end if
End Function

Function AddUser(sUserName, sOrgName)
	Dim sOrgAdminGroup
	Dim sOrgUserGroup
	Dim szUserWithDN
	Dim sDomainName
	Dim sProfileDSN 
	sProfileDSN = sGetProfileDSN()
	
	If g_bIsADInstallation and Len(sOrgName) > 0 Then
		sOrgAdminGroup = sOrgName & AD_ADMIN_GROUP_SUFFIX
		sOrgUserGroup = sOrgName & AD_USER_GROUP_SUFFIX
		szUserWithDN = "CN=" & sUserName & ",OU=" & g_sOrgName & "," &  sGetBaseURL()
		szUserWithDN = szUserWithDN & "," &  g_sDefaultNamingContext
		
		If g_iPartnerDeskRoleFlags = g_PartnerDeskAdmin Then
			Call RemoveUserFromGroup(sOrgUserGroup, szUserWithDN, sProfileDSN)
			Call AddUserToGroup(sOrgAdminGroup, szUserWithDN, sProfileDSN)
		Else
			Call RemoveUserFromGroup(sOrgAdminGroup, szUserWithDN, sProfileDSN)
			Call AddUserToGroup(sOrgUserGroup, szUserWithDN, sProfileDSN)
		End If
	End If
	
End Function


Function sDefaultNamingContext(oXMLDoc)
	Dim obizdata
	Dim sProfileDSN
	Dim oXMLNode
	
	If IsEmpty(Session("DefaultNamingContext")) then
		Set oXMLNode = oXMLdoc.DocumentElement.SelectSingleNode(".//" & sXMLTagDataSource & _
	      "[@" & sXMLAttrSourceType & "='" & AD_SOURCE_TYPE & "']//" & sXMLTagSourceInfo & _
	      "[@isDefault='1']")
	    If Not IsEmpty(oXMLNode) and Not (oXMLNode Is Nothing) then
		   sDefaultNamingContext = oXMLNode.SelectSingleNode(".//" & sXMLTagAttribute & _
								"[@" & sXMLAttrName & "='" & sXMLDefaultNamingContext & "']").GetAttribute("value")
		Else
		   sDefaultNamingContext = ""
		End If		
	    Session("DefaultNamingContext") = sDefaultNamingContext
	Else
		sDefaultNamingContext = Session("DefaultNamingContext")
	End If
End Function
%>