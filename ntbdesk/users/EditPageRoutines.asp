<SCRIPT LANGUAGE='VBScript'>
<!--

const L_NOTSAVED_TEXT = "Changes made, but not saved"
const L_PROMPTSAVE_TEXT = "Do you want to save changes?"

Sub OnChange()
   	SetDirty (L_PROMPTSAVE_TEXT)
End sub

Sub window_onload()
    dim sType
    sType = "<%=g_sType%>"
    if (sType ="add") OR (sType= "copy") OR (sType= "savenew")  then
       esUser0.focus()
       profilesform.ISINSERT.value="True"
    end if
   
    EnableTask "close"
End Sub


Function bAllRequiredFilledIn(byRef sMessage)
    dim sFieldStr
    dim oXMLFields, oXML, oXMLField, oXMLTest, oXMLEl, sName, sStr
    dim bDone, sSep
    bDone = True
    sFieldStr = ""
    sSep = ""
    sMessage =""
    sStr = ".//field[@required='yes' $and$ $not$ @hide]"
    set oxmLFields = UserMeta0.DocumentElement.SelectNodes(sStr)
 
    for each oXMLField in oXMLFields
        sName = oXMLField.GetAttribute("id")
        set oTest = document.all(sName)
        if NOT oTest is Nothing then
        if len(oTest.Value) > 0 then
            bDone = bDone AND True
        else
            bDone = False
            sFieldStr = sFieldStr & sSep & sName
            sSep = ", "
        end if
        end if
     next
    If sMessage ="" Then 
    	sMessage = sFormatString("<%= L_EnterRequiredField_Text %>", sFieldStr)
    End If	
    bAllRequiredFilledIn = bDone
End Function
   
Function checkSamAccountName()
	Dim elLogonName, sLogonName
	Dim sSamAccName
	Dim nLen
		
	checkSamAccountName = True
	If Not <%= g_bIsADInstallation %> then
		Exit Function
	End If
	If strcomp(profilesform.bBulkUpdate.value, "true", 1) = 0 or _
		 CInt(profilesform.numitems.value) <> 1 then _
	Exit Function
	set elLogonName = esUser0.field("logon_name")
	If Not elLogonName is Nothing or Not IsEmpty(elLogonName) Then
		sLogonName = elLogonName.value
		nLen = InStr(1, sLogonName, "@")
		If nLen > 0 Then
		    sSamAccName = Left(sLogonName, nLen - 1)
		End If
		nLen = InStr(1, sLogonName, "\")
		If nLen > 0 Then
		    sSamAccName = Mid(sLogonName, nLen + 1, Len(sLogonName) - nLen)
		End If
		If Len(Trim(sSamAccName)) = 0 Then
		    sSamAccName = sLogonName
		End If
		If Len(Trim(sSamAccName)) > 20 then
			Alert "<%= L_LogonName_ErrorMessage %>"
			checkSamAccountName = False
			EnableTask "back"
		End if
	End If	
End function   
	
Sub OnSaveNew()
    If checkSamAccountName then
	     profilesform.type.value = "savenew"
	     ShowWaitDisplay()
	     profilesform.submit
    end if	
End Sub
    
Sub OnSave()
	Dim bSave
	bSave = True
	If strcomp(profilesform.bBulkUpdate.value, "true", 1) = 0 or CInt(profilesform.numitems.value) > 1 then
	  bSave = Submit_UpdateQuery()
	End If
	If bSave and checkSamAccountName then
	  profilesform.type.value = "save"
	  ShowWaitDisplay()
	  profilesform.submit
	end if	
End Sub

Sub CallExtDialog(ByVal sProfileName, ByRef sVal)
	Dim sResponse, elOrgID, sOrgID, sUserID
	Dim sProfile
	Dim elUserID
'	alert (sVal)
'	alert (sProfileName)
	sUserID = "<%=Session("UserName") %>"
'	alert(sUserID)
	set elOrgID = esUser0.field("<%= XML_ATTR_ORGID%>")
	sOrgID = elOrgID.value
'	alert(sOrgID)
	if len(trim(sOrgID)) > 0 or _
		(Mid(sProfileName, Instr(1, sProfileName, ".")+1) = "<%= ORG_PROFILE %>") then
					
		sProfile = Mid(sProfileName, Instr(1, sProfileName, ".") + 1)  
'		alert (sProfile)
		if sProfile = "<%=ADDRESS_PROFILE%>" and Len(sUserID)= 0 then   
		    
            sVal = ""
			alert "<%= L_SaveUserFirst_Text %>"		
		else	
			sVal = window.showModalDialog("dlg_ObjManager.asp?Profile=" & sProfileName & _ 
	        "&OrgID=" & sOrgID & "&CurrentSel=" & sVal, _
	        sProfile, "status:no;help:no")
	    end if    
	else        
	  sVal = ""
	  alert "<%= L_SelectOrganization_Text %>"
	end if
end sub
    
Sub onBrowse()
	dim sName, sVal
	dim sNameOrig
	dim retArgs
 '  alert ("in browse")
	sName = window.event.srcElement.id
	nlen = InStr(1, sName, "_browse")
    sNameOrig = Left(sName, nlen - 1)

	sVal = esUser0.field(sNameOrig).value
	SELECT 	case (lcase(sNameOrig)) 
	<%= sHTMLCase %>
'sHTMLCase contains Case Statements like the following.
'     case "defaultblanketponumber"
'				call CallExtDialog("Profile Definitions.BlanketPOs", sVal)
'     case "defaultcostcenternumber"
'				call CallExtDialog("CostCenter")
    case else
	End Select
	
	if (len(trim(sVal)) > 0) AND (lcase(sVal) <> "nullchoice") then
			retArgs = split(sVal , "|||")
			esUser0.field(sNameOrig).value = retArgs(0) rem the underlying value
			esUser0.field(sName).value = retArgs(1) rem the text value
	elseif lcase(sVal) = "nullchoice" then
		  document.all(window.event.srcElement.id).value = ""		
	end if
End sub
    

Sub OnClose ()
	closeform.submit
End sub

Sub OnQBReady()
	divQueryBldr.Activate(TRUE)
End Sub

Function Submit_UpdateQuery()
    dim sStr
    sStr = divQueryBldr.GetExprBody()
    if (len(Trim(sStr)) = 0) OR IsNull(sStr) then 
       MsgBox "<%= L_ConstructUpdateQuery_Text %>",, sFormatString("<%= L_PROFILE_DialogTitle %>",Array("<%=g_sUMState %>"))
       Submit_UpdateQuery = False
       EnableTask "back"
    else
       profilesform.AdvQuery.value = sStr
       Submit_UpdateQuery = True
    end if
End Function

Sub EnableTaskMultiEdit()
    dim sStr
    sStr = divQueryBldr.GetExprBody()
    if (len(Trim(sStr)) = 0) OR (strcomp(Trim(sStr), "null", 1) = 0) then
	    DisableTask "save"
	    DisableTask "saveback"
    else
	    EnableTask "save"
	    EnableTask "saveback"
    end if
End Sub

 -->
 </SCRIPT>