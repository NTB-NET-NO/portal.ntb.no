<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='constants.asp' -->
<!--#INCLUDE FILE = "../common/UOCommonStrings.asp" -->
<!--#INCLUDE FILE='UOResources.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE = "../include/ASPUtil.asp" -->
<!--#INCLUDE FILE = "../include/HTTPXMLUtil.asp" -->
<!--#INCLUDE FILE = "../common/XMLRoutines.asp" -->
<!--#INCLUDE FILE = "../common/UserOrgListFns.asp" -->

<%

Dim iCurpage , iPrevPage
Dim sortCol , sortDir, g_dForm

Set g_dForm = dGetRequestXMLAsDict()

iCurPage = g_dForm("page")
iPrevPage = g_dForm("PrevPage")
'Session("PrevPage") = iCurPage
sortCol = g_dForm("column")
sortDir = g_dForm("direction")

Call PageChange()

Sub PageChange()
	Dim dState, aIDCol, query
	Dim rs, cn, bNoQuery, bRet
   
'	Set dState = dGetUMStateDictionary(g_sUMState)
'	aIDCol = Session(g_sIDColLabel)
	set cn = cnGetProviderConnection(dState)

'	if bIsDictEmpty(dState.Value("query")) then
'	  bNoQuery = True
'	else
'	  query = dState.Value("query")
'	  Session("UMQuery") = query
'	end if

    if IsEmpty(Session("AuxListQuery")) then 
       bNoQuery = True
    else
       query = Session("AuxListQuery")
    end if   
       
	if NOT bNoQuery then 
		bRet = bSafeExec(rs, cn, query)
		If Not bRet Then
			Response.Write getErrorXML(L_SearchFail_ErrorMessage)
		Else
			Call WriteOutRS2XML(rs, (iCurPage -1) * g_iListPageSize)
		End If	
	else
		Call WriteBlankXML()
	end if
	Set rs = Nothing
	Set cn = Nothing	 	
End Sub


Sub WriteOutRS2XML(rsQuery, nStartRecord)
	Dim xmlOutPut
	Dim xmlNode
	Dim xmlEmptyRec
	Dim nrec

	Set xmlOutPut = xmlGetXMLFromRSEx(rsQuery, nStartRecord, g_iListPageSize, TOO_BIG_TO_COUNT, NULL)
		
	If Not xmlOutPut Is Nothing then
		If xmlOutput.childNodes.Length = 0 then
			if (iCurpage - iPrevPage) = 1 then
				Call AddWarningNode(xmlOutput.ownerDocument, L_NoMorePages_ErrorMessage)
			else
				Call AddWarningNode(xmlOutput.ownerDocument, L_NoPage_ErrorMessage)
			end if	
		Else
			' filter out the sitename from the list of organizations. (AD returns site container too!!)
			Call xmlFilterSitename(xmlOutput)
			nrec = xmlOutPut.GetAttribute("recordcount")
			if nrec > 0 and  Session("ADInstallation")= True  then 
			    xmlOutPut.SetAttribute "recordcount" , nrec - 1 
			end if  
		End If
		Response.Write xmlOutPut.xml
	End if
end Sub

Sub WriteBlankXML()
	Dim sXML
	sXML= "<document><record/></document>"
	Response.Write sXML
	
End Sub

function getErrorXML(sErrorText)
  
  getErrorXML = "<document>"
  getErrorXML = getErrorXML & "<ERROR ID='0x" & hex(Err.number) & "' SOURCE='" & sErrorText & " - " & Err.source & "'>" & _
			"<![CDATA[" & Err.description & "]]></ERROR>"
  getErrorXML = getErrorXML & "</document>"
  
  Err.Clear
  
End function

%>