<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../common/UOCommonStrings.asp" -->
<%
	const L_Select_DialogTitle = "Selection Dialog"
	const L_CustomSelect_DialogTitle = "Select %1"
    dim args
   
    args = "Profile=" & Request.QueryString("Profile") & "&OrgID=" & _
        Request.QueryString("OrgID") & "&CurrentSel=" & _
        Request.QueryString("CurrentSel")
      
%>
<HTML>

<HEAD>
<TITLE></TITLE>
<STYLE>

	HTML
	{
	    WIDTH: 520px;
	    HEIGHT:450px;
	}

	
</STYLE>

	<SCRIPT LANGUAGE="VBScript">
	<!--
		Dim dTitle
		dTitle = window.dialogArguments
	'	alert(dTitle)
		if dTitle <> "" then
		    if dTitle <> "Organization" then
				document.title = sFormatString("<%= L_CustomSelect_DialogTitle %>", Array("<%=L_UserEntity_Text%>"))
			else	
				document.title = sFormatString("<%= L_CustomSelect_DialogTitle %>", Array("<%=L_OrgEntity_Text%>"))
			end if	
		else
			document.title = "<%= L_Select_DialogTitle %>"
		end if
		Sub window_onload()
			If dTitle <> "UserObject" and dTitle <> "Organization" then
				window.dialogHeight = "620px"
				window.dialogWidth  = "450px"
				'window.screen.width = "400px"
			End If	
		End Sub 

		Function sFormatString(sFormat, aArgs)
			dim nArg
			for nArg = LBound(aArgs) to UBound(aArgs)
				sFormat = Replace(sFormat, "%" & nArg + 1, aArgs(nArg))
			next
			sFormatString = sFormat
		End Function

		-->
	</SCRIPT>
</HEAD>

<BODY SCROLL='no' NORESIZE='RESIZE' >
<% 
	Dim nDlgHeight
	Dim nDlgWidth
	If Request.QueryString("Profile") = "Profile Definitions.UserObject" or _
		Request.QueryString("Profile") = "Profile Definitions.Organization" then 
		nDlgHeight	= 450
		nDlgWidth	= 520
	else 
		nDlgHeight	= 610
		nDlgWidth	= 450
	end if
%>
<IFRAME ID='IFrame1' ALIGN='LEFT' FRAMEBORDER='no' NORESIZE='RESIZE'   HEIGHT='<%= nDlgHeight %>' WIDTH='<%= nDlgWidth %>'
 SCROLLING=AUTO SRC="AuxListEdit.asp?<%=args%>" ></IFRAME>


</BODY>

</HTML>
