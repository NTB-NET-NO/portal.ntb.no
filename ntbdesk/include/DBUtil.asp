<%
' -- ADO command types
Const AD_CMD_TEXT			= &H0001
Const AD_CMD_TABLE			= &H0002
Const AD_CMD_STORED_PROC	= &H0004
Const AD_CMD_UNKNOWN		= &H0008
Const AD_CMD_FILE			= &H0100
Const AD_CMD_TABLE_DIRECT	= &H0200
Const AD_CMD_URL_BIND		= &H0400

' -- ADO cursor types
Const AD_OPEN_FORWARD_ONLY	= 0
Const AD_OPEN_KEYSET		= 1
Const AD_OPEN_DYNAMIC		= 2
Const AD_OPEN_STATIC		= 3

' -- ADO Cursor Options
Const AD_HOLD_RECORDS		= &H00000100
Const AD_MOVE_PREVIOUS		= &H00000200
Const AD_ADD_NEW			= &H01000400
Const AD_DELETE				= &H01000800
Const AD_UPDATE				= &H01008000
Const AD_BOOKMARK			= &H00002000
Const AD_APPROX_POSITION	= &H00004000
Const AD_UPDATE_BATCH		= &H00010000
Const AD_RESYNC				= &H00020000
Const AD_NOTIFY				= &H00040000
Const AD_FIND				= &H00080000
Const AD_SEEK				= &H00400000
Const AD_INDEX				= &H00800000

' -- ADO lock types
Const AD_LOCK_READ_ONLY			= 1
Const AD_LOCK_PESSIMISTIC		= 2
Const AD_LOCK_OPTIMISTIC		= 3
Const AD_LOCK_BATCH_OPTIMISTIC	= 4

' -- ADO parameter directions
Const AD_PARAM_UNKNOWN		= &H0000
Const AD_PARAM_INPUT		= &H0001
Const AD_PARAM_OUTPUT		= &H0002
Const AD_PARAM_INPUT_OUTPUT	= &H0003
Const AD_PARAM_RETURN_VALUE	= &H0004

' -- ADO persistence types
Const AD_PERSIST_ADTG	= 0
Const AD_PERSIST_XML	= 1

' -- ADO execute options
Const AD_ASYNC_EXECUTE				= &H00000010
Const AD_ASYNC_FETCH				= &H00000020
Const AD_ASYNC_FETCH_NON_BLOCKING	= &H00000040
Const AD_EXECUTE_NO_RECORDS			= &H00000080

' -- ADO connect options
Const AD_ASYNC_CONNECT = &H00000010

' -- ADO record status values
Const AD_REC_OK						= &H0000000
Const AD_REC_NEW					= &H0000001
Const AD_REC_MODIFIED				= &H0000002
Const AD_REC_DELETED				= &H0000004
Const AD_REC_UNMODIFIED				= &H0000008
Const AD_REC_INVALID				= &H0000010
Const AD_REC_MULTIPLE_CHANGES		= &H0000040
Const AD_REC_PENDING_CHANGES		= &H0000080
Const AD_REC_CANCELED				= &H0000100
Const AD_REC_CANT_RELEASE			= &H0000400
Const AD_REC_CONCURRENCY_VIOLATION	= &H0000800
Const AD_REC_INTEGRITY_VIOLATION	= &H0001000
Const AD_REC_MAXCHANGES_EXCEEDED	= &H0002000
Const AD_REC_OBJECT_OPEN			= &H0004000
Const AD_REC_OUT_OF_MEMORY			= &H0008000
Const AD_REC_PERMISSION_DENIED		= &H0010000
Const AD_REC_SCHEMA_VIOLATION		= &H0020000
Const AD_REC_DB_DELETED				= &H0040000

' -- ADO object state values
Const AD_STATE_CLOSED		= &H00000000
Const AD_STATE_OPEN			= &H00000001
Const AD_STATE_CONNECTING	= &H00000002
Const AD_STATE_EXECUTING	= &H00000004
Const AD_STATE_FETCHING		= &H00000008

' -- ADO field attribute values
Const AD_FLD_MAY_DEFER			= &H00000002
Const AD_FLD_UPDATABLE			= &H00000004
Const AD_FLD_UNKNOWN_UPDATABLE	= &H00000008
Const AD_FLD_FIXED				= &H00000010
Const AD_FLD_IS_NULLABLE		= &H00000020
Const AD_FLD_MAY_BE_NULL		= &H00000040
Const AD_FLD_LONG				= &H00000080
Const AD_FLD_ROWID				= &H00000100
Const AD_FLD_ROWVERSION			= &H00000200
Const AD_FLD_CACHE_DEFERRED		= &H00001000
Const AD_FLD_NEGATIVE_SCALE		= &H00004000
Const AD_FLD_KEY_COLUMN			= &H00008000

' -- ADO cursor location values
Const AD_USE_SERVER = 2
Const AD_USE_CLIENT = 3

' -- ADO filter group values
Const AD_FILTER_NONE				= 0
Const AD_FILTER_PENDING_RECORDS		= 1
Const AD_FILTER_AFFECTED_RECORDS	= 2
Const AD_FILTER_FETCHED_RECORDS		= 3
Const AD_FILTER_CONFLICTING_RECORDS	= 5

' -- ADO affect values
Const AD_AFFECT_CURRENT			= 1
Const AD_AFFECT_GROUP			= 2
Const AD_AFFECT_ALL_CHAPTERS	= 4

' -- ADO connect modes
Const AD_MODE_UNKNOWN			= &H00
Const AD_MODE_READ				= &H01
Const AD_MODE_WRITE				= &H02
Const AD_MODE_READ_WRITE		= &H03
Const AD_MODE_SHARE_DENY_READ	= &H04
Const AD_MODE_SHARE_DENY_WRITE	= &H08
Const AD_MODE_SHARE_EXCLUSIVE	= &H0C
Const AD_MODE_SHARE_DENY_NONE	= &H10
Const AD_MODE_RECURSIVE			= &H20

' -- Record Create Values
Const AD_CREATE_COLLECTION		= &H00002000
Const AD_CREATE_STRUCT_DOC		= &H80000000
Const AD_CREATE_NON_COLLECTION	= &H00000000
Const AD_OPEN_IF_EXISTS			= &H02000000
Const AD_CREATE_OVERWRITE		= &H04000000
Const AD_FAIL_IF_NOT_EXISTS		= -1

' -- ADO defaults for timeouts
const AD_CONNECTION_TIMEOUT	= 15
const AD_COMMAND_TIMEOUT	= 30

' -- ADO edit modes
const AD_EDIT_NONE			= &H0000
const AD_EDIT_IN_PROGRESS	= &H0001
const AD_EDIT_ADD			= &H0002
const AD_EDIT_DELETE		= &H0004

' -- ADO get rows option values
const AD_GET_ROWSREST = -1

' -- ADO position values
const AD_POS_UNKNOWN	= -1
const AD_POS_BOF		= -2
const AD_POS_EOF		= -3

' -- ADO bookmark values
const AD_BOOKMARK_CURRENT	= 0
const AD_BOOKMARK_FIRST		= 1
const AD_BOOKMARK_LAST		= 2

' -- ADO marshal options
const AD_MARSHAL_ALL			= 0
const AD_MARSHAL_MODIFIED_ONLY	= 1

' -- ADO resync values
const AD_RESYNC_UNDERLYING_VALUES	= 1
const AD_RESYNC_ALL_VALUES			= 2

' -- ADO compare values
const AD_COMPARE_LESS_THAN		= 0
const AD_COMPARE_EQUAL			= 1
const AD_COMPARE_GREATER_THAN	= 2
const AD_COMPARE_NOT_EQUAL		= 3
const AD_COMPARE_NOT_COMPARABLE	= 4

' -- ADO search direction values
const AD_SEARCH_FORWARD		= 1
const AD_SEARCH_BACKWARD	= -1

' -- ADO string format values
const AD_CLIP_STRING = 2

' -- ADO connect prompt values
const AD_PROMPT_ALWAYS				= 1
const AD_PROMPT_COMPLETE			= 2
const AD_PROMPT_COMPLETE_REQUIRED	= 3
const AD_PROMPT_NEVER				= 4

' -- ADO record open option values
const AD_OPEN_SOURCE		= &H00000080
const AD_OPEN_ASYNC			= &H00001000
const AD_DELAY_FETCH_STREAM	= &H00004000
const AD_DELAY_FETCH_FIELDS	= &H00008000

' -- ADO isolation level values
const AD_XACT_UNSPECIFIED		= &HFFFFFFFF
const AD_XACT_CHAOS				= &H00000010
const AD_XACT_READ_UNCOMMITTED	= &H00000100
const AD_XACT_BROWSE			= &H00000100
const AD_XACT_CURSOR_STABILITY	= &H00001000
const AD_XACT_READ_COMMITTED	= &H00001000
const AD_XACT_REPEATABLE_READ	= &H00010000
const AD_XACT_SERIALIZABLE		= &H00100000
const AD_XACT_ISOLATED			= &H00100000

' -- ADO XACT attribute values
const AD_XACT_COMMIT_RETAINING	= &H00020000
const AD_XACT_ABORT_RETAINING	= &H00040000

' -- ADO property attributes values
const AD_PROP_NOT_SUPPORTED	= &H0000
const AD_PROP_REQUIRED		= &H0001
const AD_PROP_OPTIONAL		= &H0002
const AD_PROP_READ			= &H0200
const AD_PROP_WRITE			= &H0400

' -- ADO error values
const AD_ERR_INVALID_ARGUMENT		= &HBB9
const AD_ERR_OPENING_FILE			= &HBBA
const AD_ERR_READ_FILE				= &HBBB
const AD_ERR_WRITE_FILE				= &HBBC
const AD_ERR_NO_CURRENT_RECORD		= &HBCD
const AD_ERR_ILLEGAL_OPERATION		= &HC93
const AD_ERR_IN_TRANSACTION			= &HCAE
const AD_ERR_FEATURE_NOT_AVAILABLE	= &HCB3
const AD_ERR_ITEM_NOT_FOUND			= &HCC1
const AD_ERR_OBJECT_IN_COLLECTION	= &HD27
const AD_ERR_OBJECT_NOT_SET			= &HD5C
const AD_ERR_DATA_CONVERSION		= &HD5D
const AD_ERR_OBJECT_CLOSED			= &HE78
const AD_ERR_OBJECT_OPEN			= &HE79
const AD_ERR_PROVIDER_NOT_FOUND		= &HE7A
const AD_ERR_BOUNDTO_COMMAND		= &HE7B
const AD_ERR_INVALID_PARAMINFO		= &HE7C
const AD_ERR_INVALID_CONNECTION		= &HE7D
const AD_ERR_NOT_REENTRANT			= &HE7E
const AD_ERR_STILL_EXECUTING		= &HE7F
const AD_ERR_OPERATION_CANCELLED	= &HE80
const AD_ERR_STILL_CONNECTING		= &HE81
const AD_ERR_UNSAFE_OPERATION		= &HE84

' -- ADO parameter attributes values
const AD_PARAM_SIGNED	= &H0010
const AD_PARAM_NULLABLE	= &H0040
const AD_PARAM_LONG		= &H0080

' -- ADO event status values
const AD_STATUS_OK				= &H0000001
const AD_STATUS_ERRORS_OCCURRED	= &H0000002
const AD_STATUS_CANT_DENY		= &H0000003
const AD_STATUS_CANCEL			= &H0000004
const AD_STATUS_UNWANTED_EVENT	= &H0000005

' -- ADO event reason values
const AD_RSN_ADD_NEW		= 1
const AD_RSN_DELETE			= 2
const AD_RSN_UPDATE			= 3
const AD_RSN_UNDO_UPDATE	= 4
const AD_RSN_UNDO_ADD_NEW	= 5
const AD_RSN_UNDO_DELETE	= 6
const AD_RSN_REQUERY		= 7
const AD_RSN_RESYNCH		= 8
const AD_RSN_CLOSE			= 9
const AD_RSN_MOVE			= 10
const AD_RSN_FIRST_CHANGE	= 11
const AD_RSN_MOVE_FIRST		= 12
const AD_RSN_MOVE_NEXT		= 13
const AD_RSN_MOVE_PREVIOUS	= 14
const AD_RSN_MOVE_LAST		= 15

' -- ADO schema values
const AD_SCHEMA_PROVIDER_SPECIFIC		= -1
const AD_SCHEMA_ASSERTS					= 0
const AD_SCHEMA_CATALOGS				= 1
const AD_SCHEMA_CHARACTER_SETS			= 2
const AD_SCHEMA_COLLATIONS				= 3
const AD_SCHEMA_COLUMNS					= 4
const AD_SCHEMA_CHECK_CONSTRAINTS		= 5
const AD_SCHEMA_CONSTRAINT_COLUMNUSAGE	= 6
const AD_SCHEMA_CONSTRAINT_TABLE_USAGE	= 7
const AD_SCHEMA_KEY_COLUMN_USAGE		= 8
const AD_SCHEMA_REFERENTIAL_CONSTRAINTS	= 9
const AD_SCHEMA_TABLE_CONSTRAINTS		= 10
const AD_SCHEMA_COLUMNS_DOMAIN_USAGE	= 11
const AD_SCHEMA_INDEXES					= 12
const AD_SCHEMA_COLUMN_PRIVILEGES		= 13
const AD_SCHEMA_TABLE_PRIVILEGES		= 14
const AD_SCHEMA_USAGE_PRIVILEGES		= 15
const AD_SCHEMA_PROCEDURES				= 16
const AD_SCHEMA_SCHEMATA				= 17
const AD_SCHEMA_SQL_LANGUAGES			= 18
const AD_SCHEMA_STATISTICS				= 19
const AD_SCHEMA_TABLES					= 20
const AD_SCHEMA_TRANSLATIONS			= 21
const AD_SCHEMA_PROVIDER_TYPES			= 22
const AD_SCHEMA_VIEWS					= 23
const AD_SCHEMA_VIEW_COLUMN_USAGE		= 24
const AD_SCHEMA_VIEW_TABLE_USAGE		= 25
const AD_SCHEMA_PROCEDURE_PARAMETERS	= 26
const AD_SCHEMA_FOREIGN_KEYS			= 27
const AD_SCHEMA_PRIMARY_KEYS			= 28
const AD_SCHEMA_PROCEDURE_COLUMNS		= 29
const AD_SCHEMA_DB_INFO_KEYWORDS		= 30
const AD_SCHEMA_DB_INFO_LITERALS		= 31
const AD_SCHEMA_CUBES					= 32
const AD_SCHEMA_DIMENSIONS				= 33
const AD_SCHEMA_HIERARCHIES				= 34
const AD_SCHEMA_LEVELS					= 35
const AD_SCHEMA_MEASURES				= 36
const AD_SCHEMA_PROPERTIES				= 37
const AD_SCHEMA_MEMBERS					= 38
const AD_SCHEMA_TRUSTEES				= 39

' -- ADO seek values
const AD_SEEK_FIRST_EQ	= &H1
const AD_SEEK_LAST_EQ	= &H2
const AD_SEEK_AFTER_EQ	= &H4
const AD_SEEK_AFTER		= &H8
const AD_SEEK_BEFORE_EQ	= &H10
const AD_SEEK_BEFORE	= &H20

' -- ADO update criteria values
const AD_CRITERIA_KEY			= 0
const AD_CRITERIA_ALL_COLS		= 1
const AD_CRITERIA_UPD_COLS		= 2
const AD_CRITERIA_TIME_STAMP	= 3

' -- ADO thread priority values
const AD_PRIORITY_LOWEST		= 1
const AD_PRIORITY_BELOW_NORMAL	= 2
const AD_PRIORITY_NORMAL		= 3
const AD_PRIORITY_ABOVE_NORMAL	= 4
const AD_PRIORITY_HIGHEST		= 5

' -- ADO CE resync values
const AD_RESYNC_NONE			= 0
const AD_RESYNC_AUTO_INCREMENT	= 1
const AD_RESYNC_CONFLICTS		= 2
const AD_RESYNC_UPDATES			= 4
const AD_RESYNC_INSERTS			= 8
const AD_RESYNC_ALL				= 15

' -- ADO recalc values
const AD_RECALC_UP_FRONT	= 0
const AD_RECALC_ALWAYS		= 1

' -- ADO move record options values
const AD_MOVE_OVER_WRITE		= 1
const AD_MOVE_DONT_UPDATE_LINKS	= 2
const AD_MOVE_ALLOW_EMULATION	= 4

' -- ADO copy record options values
const AD_COPY_OVER_WRITE		= 1
const AD_COPY_ALLOW_EMULATION	= 4
const AD_COPY_NON_RECURSIVE		= 2

' -- ADO stream type values
const AD_TYPE_BINARY	= 1
const AD_TYPE_TEXT		= 2

' -- ADO line separator values
const AD_LF		= 10
const AD_CR		= 13
const AD_CR_LF	= -1

' -- ADO stream open values
const AD_OPEN_STREAM_ASYNC			= 1
const AD_OPEN_STREAM_FROM_RECORD	= 4
const AD_OPEN_STREAM_FROM_URL		= 8

' -- ADO stream write values
const AD_WRITE_CHAR = 0
const AD_WRITE_LINE = 1

' -- ADO save option values
const AD_SAVE_CREATE_NOT_EXIST	= 1
const AD_SAVE_CREATE_OVER_WRITE	= 2

' -- ADO stream values
const AD_DEFAULT_STREAM	= -1
const AD_RECORD_URL		= -2

' -- ADO read values
const AD_READ_ALL	= -1
const AD_READ_LINE	= -2

' -- ADO record types
const AD_SIMPLE_RECORD		= 0
const AD_COLLECTION_RECORD	= 1
const AD_STRUCT_DOC			= 2

' -- ADO data types
const AD_EMPTY				= 0
const AD_TINYINT			= 16
const AD_SMALLINT			= 2
const AD_INTEGER			= 3
const AD_BIGINT				= 20
const AD_UNSIGNED_TINYINT	= 17
const AD_UNSIGNED_SMALLINT	= 18
const AD_UNSIGNED_INT		= 19
const AD_UNSIGNED_BIGINT	= 21
Const AD_SINGLE				= 4
Const AD_DOUBLE				= 5
Const AD_CURRENCY			= 6
Const AD_DECIMAL			= 14
Const AD_NUMERIC			= 131
Const AD_BOOLEAN			= 11
Const AD_ERROR				= 10
Const AD_USER_DEFINED		= 132
Const AD_VARIANT			= 12
Const AD_IDISPATCH			= 9
Const AD_IUNKNOWN			= 13
Const AD_GUID				= 72
Const AD_DATE				= 7
Const AD_DB_DATE			= 133
Const AD_DB_TIME			= 134
Const AD_DB_TIMESTAMP		= 135
Const AD_BSTR				= 8
Const AD_CHAR				= 129
Const AD_VAR_CHAR			= 200
Const AD_LONG_VARCHAR		= 201
Const AD_WCHAR				= 130
Const AD_VARWCHAR			= 202
Const AD_LONG_VARWCHAR		= 203
Const AD_BINARY				= 128
Const AD_VARBINARY			= 204
Const AD_LONG_VARBINARY		= 205
Const AD_CHAPTER			= 136
Const AD_FILETIME			= 64
Const AD_DB_FILETIME		= 137
Const AD_PROP_VARIANT		= 138
Const AD_VAR_NUMERIC		= 139
' -- special field flag for splitting a datetime field into two XML nodes
const COMMERCE_SPLIT_DATE	= -1

' locale data constants
const LOCALE_SDECIMAL = 14
const LOCALE_STHOUSAND = 15

'flag used for recordcount passed to conversion routines to keep from iterating over entire recordset
const TOO_BIG_TO_COUNT = -5

Dim g_oConn, g_oCmd, g_DBDefaultLocale, g_DBCurrencyLocale
g_DBDefaultLocale	= Application("MSCSDefaultLocale")
g_DBCurrencyLocale	= Application("MSCSCurrencyLocale")

' -- to set up a standard connection:
'set rsQuery = rsGetRecordset(sYourConnectionString, sYourQuery, AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC)

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set oADODBConnection = oGetADOConnection(sConnectionString)
'		returns an ADO connection object
'
'	Arguments:	sConnectionString	String. connection string for ADO connection object
'
'	Returns:						ADO connection object
'
function oGetADOConnection(sConnectionString)
	dim oConn
	set oGetADOConnection = nothing
	if sConnectionString = "" then exit function
	On error resume next
	Set oConn = Server.CreateObject("ADODB.Connection")
	if Err.number <> 0 then
		setError "", sGetErrorByID("Cant_create_object"), sGetScriptError(Err), ERROR_ICON_ALERT
		exit function
	end if
	oConn.ConnectionTimeout = AD_CONNECTION_TIMEOUT
	oConn.CommandTimeout = AD_COMMAND_TIMEOUT
	oConn.Open sConnectionString
	if oConn.Errors.count > 0 and isEmpty(Session("MSCSBDError")) then
		setError "", sGetErrorByID("Cant_connect_db"), sGetADOError(oConn.Errors(0)), ERROR_ICON_ALERT
	end if
	set oGetADOConnection = oConn
	on error goto 0
end	function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set oADODBCommand = oGetADOCommand(oConn, nCommandType)
'		returns an ADO command object
'
'	Arguments:	oConn			ADO connection object
'				nCommandType	Integer. ADO command type from constants above
'
'	Returns:					ADO command object
'
function oGetADOCommand(oConn, nCommandType)
	dim oCmd
	On error resume next
	Set oCmd = Server.CreateObject("ADODB.Command")
	if Err.number <> 0 then
		setError "", sGetErrorByID("Cant_create_object"), sGetScriptError(Err), ERROR_ICON_ALERT
		exit function
	end if
	oCmd.CommandType = nCommandType
	Set oCmd.ActiveConnection = oConn
	if oConn.Errors.count > 0 and isEmpty(Session("MSCSBDError")) then
		setError "", sGetErrorByID("Cant_connect_db"), sGetADOError(oConn.Errors(0)), ERROR_ICON_ALERT
	end if
	set oGetADOCommand = oCmd
	on error goto 0
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set rsADODBRecordset = rsGetRecordset(sConnectionString, sQuery)
'		returns an ADO recordset object using global connection and command objs
'
'	Arguments:	sConnectionString	String. connection string for ADO connection object
'				sQuery				String. query string
'				nCursorType			Integer. ADO recordset cursor type from constants above
'				nLockType			Integer. ADO recordset lock type from constants above
'
'	Returns:						ADO recordset object
'
function rsGetRecordset(sConnectionString, sQuery, nCursorType, nLockType)
	dim rsQuery, oConn, oCmd
	Set oConn = oGetADOConnection(sConnectionString)
	Set oCmd = oGetADOCommand(oConn, AD_CMD_TEXT)
	On error resume next
	oCmd.CommandText = sQuery
	Set rsQuery = Server.CreateObject("ADODB.Recordset")
	if Err.number <> 0 then
		setError "", sGetErrorByID("Cant_create_object"), sGetScriptError(Err), ERROR_ICON_ALERT
		exit function
	end if
	rsQuery.Open oCmd, , nCursorType, nLockType
	if oConn.Errors.count > 0 and isEmpty(Session("MSCSBDError")) then
		setError "", sGetErrorByID("Cant_connect_db"), sGetADOError(oConn.Errors(0)), ERROR_ICON_ALERT
		set oConn = nothing
		set oCmd = nothing
		set rsQuery = nothing
		set rsGetRecordset = nothing
	else
		set rsGetRecordset = rsQuery
	end if
	on error goto 0
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set xmlDocumentNode = xmlGetXMLFromQuery(sConnectionString, sQuery)
'		returns an XML document node converted from the recordset
'
'	Arguments:	sConnectionString	String. connection string for ADO connection object
'				sQuery				String. query string
'
'	Returns:						xml document containing converted records (or one empty record)
'
function xmlGetXMLFromQuery(sConnectionString, sQuery)
	set xmlGetXMLFromQuery = xmlGetXMLFromQueryEx(sConnectionString, sQuery, -1, -1, -1, null)
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set xmlDocumentNode = xmlGetXMLFromRS(rsQuery)
'		returns an XML document node converted from the recordset
'
'	Arguments:	rsQuery		Open ADO recordset
'
'	Returns:				xml document containing converted records (or one empty record)
'
function xmlGetXMLFromRS(rsQuery)
	set xmlGetXMLFromRS = xmlGetXMLFromRSEx(rsQuery, -1, -1, -1, null)
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set xmlDocumentNode = xmlGetXMLFromQueryEx(sConnectionString, sQuery, nStartRecord, nPageSize, nExRecordCount, dFormat)
'		returns an XML document node converted from the recordset
'
'	Arguments:	sConnectionString	String. connection string for ADO connection object
'				sQuery				String. query string
'				nStartRecord		Integer. start record index (-1 starts at beginning of RS)
'				nPageSize			Integer. number of records to get for a page
'				nExRecordCount		Iinteger. recordcount override - will be used instead of recordsets recordcount
'				dFormat				Dictionary. dictionary of fields to format. key is field name, value is format type
'							one of: 
'								AD_CURRENCY			-format as currency (without currency symbol)
'								AD_DATE, AD_DB_DATE	-format as date
'								AD_DB_TIME			-format as time
'								AD_DB_TIMESTAMP		-format as date with time
'								COMMERCE_SPLIT_DATE	-format as two separate fields; one time (id_time), one date (id_date)
'
'	Returns:						xml document containing converted records (or one empty record)
'
function xmlGetXMLFromQueryEx(sConnectionString, sQuery, nStartRecord, nPageSize, nExRecordCount, dFormats)
	dim rsQuery, xmlReturn, xmlRecord
	Set rsQuery = rsGetRecordset(sConnectionString, sQuery, AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY)
	if not rsQuery is nothing then
		set xmlReturn = xmlGetXMLFromRSEx(rsQuery, nStartRecord, nPageSize, nExRecordCount, dFormats)
		if rsQuery.state <> AD_STATE_CLOSED then rsQuery.close
		set rsQuery = nothing
	else
		set xmlReturn = Server.CreateObject("MSXML.DOMDocument")
		set xmlReturn.documentElement = xmlReturn.createElement("document")
		set xmlRecord = xmlReturn.documentElement.appendChild(xmlReturn.createElement("record"))
		xmlRecord.setAttribute "recordcount", 0
		set xmlReturn = xmlReturn.documentElement
	end if
	set xmlGetXMLFromQueryEx = xmlReturn
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set xmlDocumentNode = xmlGetXMLFromRSEx(rsQuery, nStartRecord, nPageSize, nExRecordCount, dFormats)
'		returns an XML document node converted from the recordset
'
'	Arguments:	rsQuery			Open ADO recordset
'				nStartRecord	Integer. start record index (-1 starts at beginning of RS)
'				nPageSize		Integer. number of records to get for a page
'				nExRecordCount	Integer. recordcount override - will be used instead of recordsets recordcount
'				dFormat			Dictionary. dictionary of fields to format. key is field name, value is format type
'						one of: 
'							AD_CURRENCY			-format as currency (without currency symbol)
'							AD_DATE, AD_DB_DATE	-format as date
'							AD_DB_TIME			-format as time
'							AD_DB_TIMESTAMP		-format as date with time
'							COMMERCE_SPLIT_DATE	-format as two separate fields; one time (id_time), one date (id_date)
'
'	Returns:					xml document element containing converted records (or one empty record)
'
function xmlGetXMLFromRSEx(rsQuery, nStartRecord, nPageSize, nExRecordCount, dFormats)
	dim xmlDoc, xmlDocNode, nRecordCount

	set xmlDoc = Server.CreateObject("MSXML.DOMDocument")
	set xmlDocNode = xmlDoc.createElement("document")
	set xmlDoc.documentElement = xmlDocNode
    
    if rsQuery.State = AD_STATE_CLOSED then
		'empty record
		AddEmptyRecordNode xmlDoc, rsQuery
		nRecordCount = 0
    elseif rsQuery.BOF and rsQuery.EOF then
		'empty record
		AddEmptyRecordNode xmlDoc, rsQuery
		nRecordCount = 0
	else
		'get records
	    nRecordCount = nConvertRS2XML(xmlDoc, rsQuery, nStartRecord, nPageSize, nExRecordCount, dFormats)
	end if
      
	'add pseudo-record for returning recordcount
	if nExRecordCount > 0 then
		xmlDocNode.setAttribute "recordcount", nExRecordCount
	elseif nExRecordCount = TOO_BIG_TO_COUNT then
		xmlDocNode.setAttribute "recordcount", -1
	else
		xmlDocNode.setAttribute "recordcount", nRecordCount
	end if
	 
	set xmlGetXMLFromRSEx = xmlDocNode
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set nRecordCount = nConvertRS2XML(xmlDoc, rsQuery, nStartRecord, nPageSize, dFormats)
'		returns the recordcount and an XML document node converted from the recordset 
'		in xmlDoc
'
'	Arguments:	xmlDoc			XML document object
'				rsQuery			Open ADO recordset
'				nStartRecord	Integer. start record index
'				nPageSize		Integer. number of records to get for a page (-1 unlimited)
'				dFormat			Dctionary. dictionary of fields to format. key is field name, value is format type
'						one of: 
'							AD_CURRENCY			-format as currency (without currency symbol)
'							AD_DATE, AD_DB_DATE	-format as date
'							AD_DB_TIME			-format as time
'							AD_DB_TIMESTAMP		-format as date with time
'							COMMERCE_SPLIT_DATE	-format as two separate fields; one time (id_time), one date (id_date)
'
'	Returns:					Integer. record count
'
function nConvertRS2XML(xmlDoc, rsQuery, nStartRecord, nPageSize, nExRecordCount, dFormats)
	dim nRecordCount, xmlDocNode, xmlRecord, xmlChildNode, fdField

	nRecordCount = 0
	set xmlDocNode = xmlDoc.documentElement
	do until rsQuery.EOF
		if nRecordCount >= nStartRecord and _
			((nRecordCount < (nPageSize + nStartRecord) and _
			nPageSize > 0) or nPageSize = -1) then
			'after start record and before page size (or page size = -1 for unlimited)
			'create new record and fill it
			set xmlRecord = xmlDoc.createElement("record")
			xmlDocNode.appendChild(xmlRecord)
			for each fdField in rsQuery.fields
				AddField xmlRecord, fdField, dFormats
			next
		elseif nRecordCount >= nStartRecord and _
				nExRecordCount = TOO_BIG_TO_COUNT then
			nConvertRS2XML = TOO_BIG_TO_COUNT
			exit do
		end if
		nRecordCount = nRecordCount + 1
		rsQuery.moveNext
	loop
	nConvertRS2XML = nRecordCount
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	adds a field node to xmlRecord formatted by field type or 
'						dFormat value
sub AddField(xmlRecord, fdField, dFormats)
	dim sName, sValue, sType
	sName = sStripGroupName(fdField.name)
	sValue = fdField.value
	sType = fdField.type
	if typename(dFormats) = "IDictionary" then
		select case dFormats(sName)
		case AD_DATE, AD_DB_DATE, AD_DB_TIMESTAMP, AD_DB_TIME
			sType = dFormats(sName)
		case COMMERCE_SPLIT_DATE
			'split into two fields
			AddFormattedNode xmlRecord, sName & "_date", sValue, AD_DB_DATE
			sName = sName & "_time"
			sType = AD_DB_TIME
		end select
	end if
	AddFormattedNode xmlRecord, sName, sValue, sType
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	adds a field node to xmlRecord formatted by field type or 
'						dFormat value
sub AddFormattedNode(xmlRecord, sName, sValue, sType)
	dim sFormattedText, xmlField, sThousands
	if not isNull(sValue) then
		select case sType
		case AD_BOOLEAN
			if CInt(sValue) = -1 Then
				sFormattedText = 1
			Else
				sFormattedText = 0
			End If
		case AD_DATE, AD_DB_DATE
			sFormattedText = g_MSCSDataFunctions.date(sValue, g_DBDefaultLocale)
		case AD_DB_TIMESTAMP
			sFormattedText = g_MSCSDataFunctions.datetime(sValue, g_DBDefaultLocale)
		case AD_DB_TIME
			sFormattedText = g_MSCSDataFunctions.time(sValue, g_DBDefaultLocale)
		case AD_VAR_NUMERIC, AD_DECIMAL, AD_DOUBLE, AD_SINGLE, AD_NUMERIC
			sThousands = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_STHOUSAND, g_DBDefaultLocale)
			sFormattedText = trim(replace(g_MSCSDataFunctions.float(cStr(sValue), g_DBDefaultLocale, true, true), sThousands, ""))
		case AD_CURRENCY	'use currency symbol of SPACE
			sThousands = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_STHOUSAND, g_DBCurrencyLocale)
			sFormattedText = trim(replace(g_MSCSDataFunctions.LocalizeCurrency(sValue, g_DBCurrencyLocale, " "), sThousands, ""))
		case else
			sFormattedText = sESCText(sValue)
		end select
		set xmlField = xmlRecord.ownerDocument.createElement(sName)
		xmlField.text = sFormattedText
		xmlRecord.appendChild(xmlField)
	else
		set xmlField = xmlRecord.ownerDocument.createElement(sName)
		xmlRecord.appendChild(xmlField)
	end if
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	escape CR
function sESCText(sText)
	sESCText = replace(sText, vbCrLf, "&#13;")
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	adds an empty record to the xmlDoc with empty field node 
'						per field
sub AddEmptyRecordNode(xmlDoc, rsQuery)
	dim xmlRecord, xmlDocNode, fdField, xmlField

	set xmlDocNode = xmlDoc.documentElement
	set xmlRecord = xmlDoc.createElement("record")
	for each fdField in rsQuery.fields
		set xmlField = xmlDoc.createElement(sStripGroupName(fdField.name))
		xmlRecord.appendChild xmlField
	next
	xmlDocNode.appendChild(xmlRecord)
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	strips the Group Name away from the field name   
'					
Function sStripGroupName(sFldName)
    Dim atmp
    atmp = Split(sFldName,".")
    if UBound(atmp) > 0 then
		sStripGroupName = atmp(1)
	else
		sStripGroupName = atmp(0)
	end if 	
end Function
%>