<%
dim g_MSCSEnv, g_bInDialog

' -- enviornment mode constants
Const PRODUCTION  = 1
Const DEVELOPMENT = 0

g_bInDialog = true

' -- get enviornment and turn off error display if PRODUCTION
g_MSCSEnv = Application("MSCSEnv")
if g_MSCSEnv = PRODUCTION then on error resume next
%>
<!--#INCLUDE FILE='DBUtil.asp' -->
<!--#INCLUDE FILE='ASPUtil.asp' -->
<!--#INCLUDE FILE='BDHALUtil.htm' -->
<!--#INCLUDE FILE='ActionPageUtil.asp' -->
