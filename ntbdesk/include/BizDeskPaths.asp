<%
Dim g_MSCSSiteName, g_MSCSBizDeskRoot, g_sBizDeskRoot, g_sBizDeskRootPage, _
	g_sBDConfigRoot, g_sBDConfigPath, g_sBDAssetRoot, g_sBDIncludeRoot, SHOW_DEBUG_TEXT

' -- file constant
const MASTER_START_PAGE = "bizdesk.asp"

SHOW_DEBUG_TEXT = CInt(Application("SHOW_DEBUG_TEXT"))

' -- get site name and bizdesk virtual root from application
g_MSCSSiteName = Application("MSCSSiteName")
g_MSCSBizDeskRoot = Application("MSCSBizDeskRoot")

' -- Set shared roots and paths
if len(g_MSCSBizDeskRoot) > 1 then
	'if not root web then add slash at end
	g_sBizDeskRoot = "/" & g_MSCSBizDeskRoot & "/"
else
	g_sBizDeskRoot = g_MSCSBizDeskRoot
end if
g_sBizDeskRootPage	= g_sBizDeskRoot & MASTER_START_PAGE
g_sBDConfigRoot		= g_sBizDeskRoot & "config/"
on error resume next
g_sBDConfigPath		= Server.MapPath(g_sBDConfigRoot) & "\"
on error goto 0
g_sBDAssetRoot		= g_sBizDeskRoot & "assets/"
g_sBDIncludeRoot	= g_sBizDeskRoot & "include/"
%>  