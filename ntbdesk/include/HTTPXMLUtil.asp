<%
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	AddErrorNode(xmlDOMDoc, sID, sSource)
'		adds an error node to the xml document
'
'	Arguments:	xmlDOMDoc	xml document to append error to
'				sID			string error id
'				sSource		string error message
'				sDetail		string error details
'
'	Returns:	xml document object with <document> documentElement
'
sub AddErrorNode(byRef xmlDOMDoc, byVal sID, byVal sSource, byVal sDetail)
	dim xmlDoc, xmlErrorNode
	set xmlDoc = xmlDOMDoc.documentElement
	set xmlErrorNode = xmlDoc.appendChild(xmlDOMDoc.createElement("ERROR"))
	xmlErrorNode.setAttribute "id", sID
	xmlErrorNode.setAttribute "source", sSource
	xmlErrorNode.text = sDetail
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	AddWarningNode(xmlDOMDoc, sMessage)
'		adds an warning node to the xml document
'
'	Arguments:	xmlDOMDoc	xml document to append warning to
'				sMessage	string warning message
'
'	Returns:	xml document object with <document> documentElement
'
sub AddWarningNode(byRef xmlDOMDoc, byVal sMessage)
	dim xmlDoc, xmlWarningNode
	set xmlDoc = xmlDOMDoc.documentElement
	set xmlWarningNode = xmlDoc.appendChild(xmlDOMDoc.createElement("WARNING"))
	xmlWarningNode.text = sMessage
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	AddItemNode(sName, sValue)
'		adds an item node to the xml document with name and value set
'
'	Arguments:	sName		string name
'				sValue		string value
'
'	Returns:	xml document object with <document> documentElement
'
sub AddItemNode(byVal sName, byVal sValue)
	dim xmlDoc, xmlItemNode
	set xmlDoc = g_xmlReturn.documentElement
	set xmlItemNode = xmlDoc.appendChild(g_xmlReturn.createElement("item"))
	xmlItemNode.setAttribute "name", sName
	xmlItemNode.setAttribute "value", sValue
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	AddCDATANode(sName, sValue)
'		adds a CDATA type item node to the xml document with name and value set
'
'	Arguments:	sName		string name
'				sValue		string value
'
'	Returns:	xml document object with <document> documentElement
'
sub AddCDATANode(byVal sName, byVal sValue)
	dim xmlDoc, xmlItemNode
	set xmlDoc = g_xmlReturn.documentElement
	set xmlItemNode = xmlDoc.appendChild(g_xmlReturn.createElement("item"))
	xmlItemNode.setAttribute "name", sName
	xmlItemNode.text = "<![CDATA[" & sValue & "]]>"
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	xmlGetXMLDOMDoc()
'		returns an empty document
'
'	Arguments:	none
'
'	Returns:	xml document object with empty <document> documentElement
'
function xmlGetXMLDOMDoc()
	dim xmlDOMDoc
	set xmlDOMDoc = CreateObject("MSXML.DOMDocument")
	set xmlDOMDoc.documentElement = xmlDOMDoc.createElement("document")
	set xmlGetXMLDOMDoc = xmlDOMDoc
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	xmlGetRequestXML()
'		returns the xml document from the request
'
'	Arguments:	none
'
'	Returns:	xml document object
'
function xmlGetRequestXML()
	dim xmlDOMDoc
	set xmlDOMDoc = Server.CreateObject("Microsoft.xmlDOM")
	xmlDOMDoc.async = False
	xmlDOMDoc.load Request
	set xmlGetRequestXML = xmlDOMDoc
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	dGetRequestXMLAsDict()
'		returns a dictionary from the converted xml request
'
'	Arguments:	none
'
'	Returns:	xml document object
'
function dGetRequestXMLAsDict()
	dim xmlDOMDoc
	set xmlDOMDoc = xmlGetRequestXML()
	set dGetRequestXMLAsDict = dDictFromXML(xmlDOMDoc)
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	dDictFromXML(xmlDOMDoc)
'		converts xml to dictionary; using child nodes of the document, each 
'		name and value attribute becomes a key/value pair in the dictionary
'
'	Arguments:	xmlDOMDoc	xml Document to convert
'
'	Returns:				Dictionary representing xml Document contents
'
function dDictFromXML(byRef xmlDOMDoc)
	dim dDict, xmlItemNode, sName, sValue
	set dDict = CreateObject("Commerce.Dictionary")
	for each xmlItemNode in xmlDOMDoc.documentElement.childNodes
		sName = xmlItemNode.getAttribute("name")
		sValue = xmlItemNode.getAttribute("value")
		if not isNull(sName) or sName = "" then dDict(sName) = sValue
	next
	set dDictFromXML = dDict
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	xmlXMLFromDict(dDict)
'		converts dictionary to xml; each key becomes a name attribute and 
'		each value becomes a value attribute on child nodes of the document
'
'	Arguments:	dDict	Dictionary to convert
'
'	Returns:			xml Document representing dictionary contents
'
function xmlXMLFromDict(byRef dDict)
	dim xmlDOMDoc, xmlDoc, sKey, xmlItemNode
	set xmlDOMDoc = xmlGetXMLDOMDoc()
	set xmlDoc = xmlDOMDoc.documentElement
	for each sKey in dDict
		set xmlItemNode = xmlDoc.appendChild(xmlDOMDoc.createElement("item"))
		xmlItemNode.setAttribute "name", sKey
		xmlItemNode.setAttribute "value", dDict(sKey)
	next
	set xmlXMLFromDict = xmlDOMDoc
end function
%>
