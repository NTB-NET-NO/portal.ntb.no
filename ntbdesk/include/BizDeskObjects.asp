<%
' -- localizable strings for verbose mode and errors
const L_ObjEmptyDictionary_Text = "<i>[Empty Dictionary]</i>"
const L_ObjEmptySimpleList_Text = "<i>[Empty SimpleList]</i>"
const L_ObjCategoryTree_Text = "CategoryTree object"
const L_ObjActionContainer_Text = "ActionContainer object"
const L_ObjProcessingAction_Text = "<b>processing <q>action</q>:</b> '%1'"
const L_ObjProcessingModuleConfig_Text = "<B>processing <q>moduleconfig</q>:</B> %1"
const L_ObjProcessingMasterConfig_Text = "<B>processing master config (<q>bizdesk.xml</q>):</B> %1"
const L_ObjAddingCategoryTree_Text = "<b>adding CategoryTree object</b>"
const L_ObjAddingActionContainer_Text = "<b>adding ActionContainer object</b>"
const L_ObjAddingObjects_Text = "<b>adding objects to application</b>"
const L_ObjAddingCategory_Text = "<B>adding <q>category</q>:</B> '%1'"
const L_ObjAddingTask_Text = "<B>adding <q>task</q>:</B> '%1'"
const L_ObjAddingModule_Text = "<B>adding <q>module</q>:</B> '%1'"
const L_ObjRemovedDuplicateAction_Text = "removed duplicate <q>action</q>: '%1' '%2'"
const L_ObjRemovedTaskNumPosttoGoto_Text = "removed <q>task</q>: number of <q>postto</q> and <q>goto</q> nodes must match if both greater than zero: '%1'"
const L_ObjRemovedPosttoTaskNeedsAction_Text = "removed <q>task</q>: postto node must have an <q>action</q>: '%1'"
const L_ObjRemovedGotoTaskNeedsAction_Text = "removed <q>task</q>: goto node must have an <q>action</q>: '%1'"
const L_ObjRemovedTaskNeedsFormname_Text = "removed <q>task</q>: postto node must have a <q>formname</q>: '%1'"
const L_ObjRemovedModuleNoName_Text = "removed <q>module</q>: module node must have a <q>name</q>: '%1'"
const L_ObjRemovedModuleNoAction_Text = "removed <q>module</q>: <q>module</q> node must have an <q>action</q>: '%1'"
const L_ObjRemovedModuleMissingAction_Text = "removed <q>module</q>: <q>module</q> node missing an <q>action</q>: '%1'"
const L_ObjRemovedPosttoNoAction_Text = "removed <q>postto</q>: missing <q>action</q>: '%1'"
const L_ObjRemovedGotoNoAction_Text = "removed <q>goto</q>: missing <q>action</q>: '%1'"
const L_ObjRemovedTaskNoPosttoGoto_Text = "removed <q>task</q>: missing <q>postto</q>/<q>goto</q>: '%1'"
const L_ObjBizDeskFrameworkError_ErrorMessage = "BizDesk Framework Error:"
const L_ObjBDErrorTaskNeedsGotoPostto_ErrorMessage = "<q>task</q> must have at least one <q>goto</q> or <q>postto</q> node"
const L_ObjBDErrorNoCategoriesNode_ErrorMessage = "&lt;<q>categories</q>&gt; node expected but not found in <q>bizdesk.xml</q>"
const L_ObjBDErrorNoActionsNode_ErrorMessage = "&lt;<q>actions</q>&gt; node expected but not found in %1"
const L_ObjBDErrorNoModulesNode_ErrorMessage = "&lt;<q>modules</q>&gt; node expected but not found in %1"
const L_ObjBDErrorCategoryNotFound_ErrorMessage = "cannot find <q>category</q> '%1' referenced in <q>moduleconfig</q>: '%2'"
const L_ObjBDErrorNoModuleConfigsFound_ErrorMessage = "&lt;<q>moduleconfigs</q>&gt; node expected but not found in <q>bizdesk.xml</q>"
const L_ObjXMLErrorWhileLoadingParsing_ErrorMessage = "while loading/parsing document"
const L_ObjVBErrorWhenAddingCategoryTree_ErrorMessage = "when adding CategoryTree object"
const L_ObjVBErrorWhenAddingActionContainer_ErrorMessage = "when adding ActionContainer object"
const L_ObjVBErrorWhenLoadingObjects_ErrorMessage = "when loading objects in Application"
const L_ObjVBErrorWhenLoading_ErrorMessage = "when loading %1"
const L_ObjVBScriptError_ErrorMessage = "VBScript Error:"
const L_ObjVBScriptErrorNumber_ErrorMessage = "number = 0x%1"
const L_ObjVBScriptErrorDescription_ErrorMessage = "description = '%1'"
const L_ObjVBScriptErrorSource_ErrorMessage = "source = '%1'"
const L_ObjXMLError_ErrorMessage = "XML Error:"
const L_ObjXMLErrorCode_ErrorMessage = "errorCode = 0x%1"
const L_ObjXMLErrorFilePos_ErrorMessage = "filePos = %1"
const L_ObjXMLErrorLine_ErrorMessage = "line = %1"
const L_ObjXMLErrorLinePos_ErrorMessage = "linePos = %1"
const L_ObjXMLErrorReason_ErrorMessage = "reason = '%1'"
const L_ObjXMLErrorSrcText_ErrorMessage = "srcText = '%1'"
const L_ObjXMLErrorURL_ErrorMessage = "URL = '%1'"
const L_ObjDone_Text = "Done!"

Dim g_slObjectErrors

sub LoadObjects()
	'dim dActionContainer, dNavigation, dState, xmlBDDoc
	dim dActionContainer, dNavigation, xmlBDDoc

	set Session("slObjectErrors") = Server.CreateObject("commerce.simplelist")
	set g_slObjectErrors = Session("slObjectErrors")

	set dActionContainer = Server.CreateObject("commerce.dictionary")
	set dActionContainer.actions = Server.CreateObject("commerce.dictionary")

	set dNavigation = Server.CreateObject("commerce.dictionary")
	set dNavigation.categories = Server.CreateObject("commerce.simplelist")

	ShowProgress sFormatString(L_ObjProcessingMasterConfig_Text, Array(g_sConfigXML)) & "<BR>"
	set xmlBDDoc = xmlLoadConfig(g_sConfigXML)
	if xmlBDDoc is nothing then exit sub
	LoadCategories dNavigation, xmlBDDoc
	LoadModuleConfigs dActionContainer, dNavigation, xmlBDDoc
	ValidateActions dActionContainer, dNavigation
	DumpResults dActionContainer, dNavigation
	PutObjectsInApp dActionContainer, dNavigation
	ShowProgress "<H3>" & L_ObjDone_Text & "</H3>"

	set Session("slObjectErrors") = g_slObjectErrors
	if VERBOSE_OUTPUT then Response.end
end sub

function xmlLoadConfig(sXML)
	dim xmldoc, bConfigLoaded
	set xmlLoadConfig = nothing
	on error resume next
		set xmldoc = server.CreateObject("MSXML.DOMDocument")
		xmldoc.async = false
		bConfigLoaded = xmldoc.load(sXML)
		XMLError xmldoc, L_ObjXMLErrorWhileLoadingParsing_ErrorMessage
		VBError Err, sFormatString(L_ObjVBErrorWhenLoading_ErrorMessage, Array(sModuleConfig))
	on error goto 0
	if not bConfigLoaded then exit function
	set xmlLoadConfig = xmldoc.documentElement
end function

function sGetChildText(xmlNode, sChildNodeName)
	dim xmlTemp
	sGetChildText = ""
	set xmlTemp = xmlNode.selectSingleNode(sChildNodeName)
	if not xmlTemp is nothing then sGetChildText = xmlTemp.text
end function

sub LoadCategories(ByRef dNavigation, ByRef xmlBDDoc)
	dim xmlCategories, xmlCategory, sID, dCategory
	set xmlCategories = xmlBDDoc.selectNodes("./categories/category")
	if xmlCategories.length = 0 then
	    BDError L_ObjBDErrorNoCategoriesNode_ErrorMessage
		exit sub
	end if
	for each xmlCategory in xmlCategories
		sID = xmlCategory.getAttribute("id")
		ShowProgress "&nbsp;&nbsp;&nbsp;&nbsp;" & sFormatString(L_ObjAddingCategory_Text, array(sID)) & "<BR>"
		dNavigation.categories.Add Server.CreateObject("commerce.dictionary")
		set dCategory = dNavigation.categories(dNavigation.categories.count - 1)
		dCategory.id = sID
		dCategory.name = sGetChildText(xmlCategory, "name")
		dCategory.key = sGetChildText(xmlCategory, "key")
		dCategory.tooltip = sGetChildText(xmlCategory, "tooltip")
		set dCategory.modules = Server.CreateObject("commerce.simplelist")
	next
end sub

function dGetCategory(ByRef dNavigation, ByVal sCategory)
	dim dCategory
	set dGetCategory = nothing
	for each dCategory in dNavigation.categories
		if dCategory.id = sCategory then
			set dGetCategory = dCategory
			exit for
		end if
	next
end function

sub LoadModuleConfigs(ByRef dActionContainer, ByRef dNavigation, ByRef xmlBDDoc)
	dim xmlModuleConfigs, xmlNode, sCategory, xmlModConfig, sModuleConfig
	set xmlModuleConfigs = xmlBDDoc.selectNodes("./moduleconfigs/moduleconfig")
	if xmlModuleConfigs.length = 0 then
	    BDError L_ObjBDErrorNoModuleConfigsFound_ErrorMessage
		exit sub
	end if
	for each xmlNode in xmlModuleConfigs
		sModuleConfig = g_sBDConfigPath & xmlNode.getAttribute("id")
		sCategory = xmlNode.getAttribute("category")
		ShowProgress "<P>" & sFormatString(L_ObjProcessingModuleConfig_Text, Array(sModuleConfig)) & "<BR>"
		set xmlModConfig = xmlLoadConfig(sModuleConfig)
		if not xmlModConfig is nothing then
			LoadActions dActionContainer, xmlModConfig, sModuleConfig
			LoadModules dNavigation, xmlModConfig, sCategory, sModuleConfig
		end if
	next
end sub

sub LoadActions(ByRef dActionContainer, ByRef xmlModConfig, sModuleConfig)
	dim xmlActions, xmlAction, sID, dAction, xmlTasks
	set xmlActions = xmlModConfig.selectNodes("./actions/action")
	if xmlActions.length = 0 then
	    BDError sFormatString(L_ObjBDErrorNoActionsNode_ErrorMessage, Array(sModuleConfig))
		exit sub
	end if
	for each xmlAction in xmlActions
		sID = xmlAction.getAttribute("id")
		if isNull(dActionContainer.actions(sID)) then
			ShowProgress "&nbsp;&nbsp;" & sFormatString(L_ObjProcessingAction_Text, Array(sID)) & "<BR>"
			set dActionContainer.actions(sID) = Server.CreateObject("commerce.dictionary")
			set dAction = dActionContainer.actions(sID)
			dAction.helptopic = xmlAction.getAttribute("helptopic")
			dAction.name = sGetChildText(xmlAction, "name")
			dAction.tooltip = sGetChildText(xmlAction, "tooltip")
			set dAction.tasks = Server.CreateObject("commerce.simplelist")
			set xmlTasks = xmlAction.selectNodes("./tasks/task")
			if xmlTasks.length > 0 then LoadTasks dAction, xmlTasks
		else
			BDError sFormatString(L_ObjRemovedDuplicateAction_Text, array(sID, sModuleConfig))
		end if
	next
end sub

sub LoadTasks(ByRef dAction, ByRef xmlTasks)
	dim xmlTask, bTaskOK, sID, dTask, xmlPostTos, xmlGoTos, xmlTypes, _
		xmlPostTo, xmlGoTo, xmlType, dPostTo, dGoTo
	set dAction.tasks = Server.CreateObject("commerce.simplelist")
	for each xmlTask in xmlTasks
		bTaskOK = true
		sID = sGetChildText(xmlTask, "name")
		ShowProgress "&nbsp;&nbsp;&nbsp;&nbsp;" & sFormatString(L_ObjAddingTask_Text, Array(sID)) & "<BR>"
		dAction.tasks.add Server.CreateObject("commerce.dictionary")
		set dTask = dAction.tasks(dAction.tasks.count - 1)
		set dTask.goto = Server.CreateObject("commerce.simplelist")
		set dTask.postto = Server.CreateObject("commerce.simplelist")
		set dTask.displayname = Server.CreateObject("commerce.simplelist")
		dTask.name = sID
		dTask.icon = xmlTask.getAttribute("icon")
		dTask.key = sGetChildText(xmlTask, "key")
		dTask.type = xmlTask.getAttribute("type")
		if not isNull(xmlTask.getAttribute("id")) then dTask.id = xmlTask.getAttribute("id")
		dTask.tooltip = sGetChildText(xmlTask, "tooltip")
		set xmlPostTos = xmlTask.selectNodes("./postto")
		set xmlGoTos = xmlTask.selectNodes("./goto")
		if xmlGoTos.length = 0 and xmlPostTos.length = 0 then
			BDError L_ObjBDErrorTaskNeedsGotoPostto_ErrorMessage
			bTaskOK = false
		end if
		if xmlGoTos.length > 0 and xmlPostTos.length > 0 then
			if xmlGoTos.length <> xmlPostTos.length then
				BDError sFormatString(L_ObjRemovedTaskNumPosttoGoto_Text, Array(sID))
				bTaskOK = false
			end if
		end if
		if bTaskOK then
			for each xmlPostTo in xmlPostTos
				dTask.postto.Add Server.CreateObject("commerce.dictionary")
				set dPostTo = dTask.postto(dTask.postto.count - 1)
				dPostTo.action = xmlPostTo.getAttribute("action")
				dPostTo.formname = xmlPostTo.getAttribute("formname")
				if dPostTo.action = "" then
					BDError sFormatString(L_ObjRemovedPosttoTaskNeedsAction_Text, Array(sID))
					bTaskOK = false
				end if
				if dPostTo.formname = "" then
					BDError sFormatString(L_ObjRemovedTaskNeedsFormname_Text, Array(sID))
					bTaskOK = false
				end if
				if dTask.type = "menu" or xmlPostTo.text <> "" then
					dTask.displayname.Add Server.CreateObject("commerce.dictionary")
					dTask.displayname(dTask.displayname.count - 1) = xmlPostTo.text
				end if
			next
			for each xmlGoTo in xmlGoTos
				dTask.goto.Add Server.CreateObject("commerce.dictionary")
				set dGoTo = dTask.goto(dTask.goto.count - 1)
				dGoTo.action = xmlGoTo.getAttribute("action")
				if dGoTo.action = "" then
					BDError sFormatString(L_ObjRemovedGotoTaskNeedsAction_Text, Array(sID))
					bTaskOK = false
				end if
				if dTask.type = "menu" or xmlGoTo.text <> "" then
					dTask.displayname.Add Server.CreateObject("commerce.dictionary")
					dTask.displayname(dTask.displayname.count - 1) = xmlGoTo.text
				end if
			next
		else
			' task is bad: delete it
			dAction.tasks.delete(dAction.tasks.count - 1)
		end if
	next
end sub

sub LoadModules(ByRef dNavigation, ByRef xmlModConfig, sCategory, sModuleConfig)
	dim xmlModules, xmlModule, dCategory, sID, nModule, dModule
	set xmlModules = xmlModConfig.selectNodes("modules/module")
	if xmlModules.length = 0 and right(sModuleConfig, 12) <> "bdmaster.xml" then
	    BDError sFormatString(L_ObjBDErrorNoModulesNode_ErrorMessage, Array(sModuleConfig))
		exit sub
	end if
	for each xmlModule in xmlModules
		sID = xmlModule.getAttribute("id")
		ShowProgress "&nbsp;&nbsp;" & sFormatString(L_ObjAddingModule_Text, Array(sID)) & "<BR>"

		set dCategory = dGetCategory(dNavigation, sCategory)
		if dCategory is nothing then
		    BDError sFormatString(L_ObjBDErrorCategoryNotFound_ErrorMessage, array(sCategory, sModuleConfig))
		    exit for
		else
			if sID <> "" then
				dCategory.modules.Add Server.CreateObject("commerce.dictionary")
				nModule = dCategory.modules.count - 1
				set dModule = dCategory.modules(nModule)
				dModule.action = sID
				dModule.name = sGetChildText(xmlModule, "name")
				dModule.tooltip = sGetChildText(xmlModule, "tooltip")
				if dModule.name = "" then
					BDError sFormatString(L_ObjRemovedModuleNoName_Text, Array(sID))
					sCategory.modules.delete(nModule)
				end if
			else
				BDError sFormatString(L_ObjRemovedModuleNoAction_Text, Array(sID))
				dCategory.modules.delete(nModule)
			end if
		end if
	next
end sub

sub ValidateActions(ByRef dActionContainer, ByRef dNavigation)
	dim dCategory, nIndex, dModule, sModule, dAction, _
		sAction, nOuterIndex, dTask, dPostTo, dGoTo
	for each dCategory in dNavigation.categories
		nIndex = 0
		for each dModule in dCategory.modules
			if isNull(dActionContainer.actions(dModule.action)) then
				BDError sFormatString(L_ObjRemovedModuleMissingAction_Text, Array(dModule.action))
				dCategory.modules.delete(nIndex)
			else
				nIndex = nIndex + 1
			end if
		next
	next
	for each sAction in dActionContainer.actions
		set dAction = dActionContainer.actions(sAction)
		nOuterIndex = 0
		for each dTask in dAction.tasks
			nIndex = 0
			for each dPostTo in dTask.postto
				if isNull(dActionContainer.actions(dPostTo.action)) then
					dTask.postto.delete(nIndex)
					BDError sFormatString(L_ObjRemovedPosttoNoAction_Text, Array(dPostTo.action))
					if dTask.goto.count > 0 then dTask.goto.delete(nIndex)
					if dTask.displayname.count > 0 then dTask.displayname.delete(nIndex)
				else
					nIndex = nIndex + 1
				end if
			next
			nIndex = 0
			for each dGoTo in dTask.goto
				if isNull(dActionContainer.actions(dGoTo.action)) then
					dTask.goto.delete(nIndex)
					BDError sFormatString(L_ObjRemovedGotoNoAction_Text, Array(dGoTo.action ))
					if dTask.postto.count > 0 then dTask.postto.delete(nIndex)
					if dTask.displayname.count > 0 then dTask.displayname.delete(nIndex)
				else
					nIndex = nIndex + 1
				end if
			next
			if dTask.postto.count = 0 and dTask.goto.count = 0 then
				BDError sFormatString(L_ObjRemovedTaskNoPosttoGoto_Text, Array(dTask.name))
				dAction.tasks.delete(nOuterIndex)
			else
				nOuterIndex = nOuterIndex + 1
			end if
		next
	next
end sub

sub DumpResults(ByRef dActionContainer, ByRef dNavigation)
	if VERBOSE_OUTPUT then 
	    SetDumpStyles()
	    Response.Write "<H3>" & L_ObjCategoryTree_Text & "</H3>"
	    DumpDict dNavigation
	    Response.Write "<H3>" & L_ObjActionContainer_Text & "</H3>"
	    DumpDict dActionContainer
	end if
end sub

sub PutObjectsInApp(dActionContainer, dNavigation)
	ShowProgress L_ObjAddingObjects_Text & "<BR>"
	on error resume next
	Application.Lock
	    ShowProgress L_ObjAddingCategoryTree_Text & "<BR>"
		set Application("MSCSNav") = dNavigation
	    if VBError(Err, L_ObjVBErrorWhenAddingCategoryTree_ErrorMessage) then Response.end
	    ShowProgress L_ObjAddingActionContainer_Text & "<BR>"
		set Application("MSCSActionContainer") = dActionContainer
	    if VBError(Err, L_ObjVBErrorWhenAddingActionContainer_ErrorMessage) then Response.end
	Application.UnLock
	if VBError(Err, L_ObjVBErrorWhenLoadingObjects_ErrorMessage) then Response.end
	on error goto 0
end sub

sub SetDumpStyles()
%>
	<STYLE>
	<!--
	TABLE.dictionary
	{
		BORDER-TOP: thin solid black;
	    FONT: 8pt verdana, arial, helvetica, sans-serif;
	}
	TH.dictionary
	{
		TEXT-ALIGN: left;
		BORDER-LEFT: thin solid black;
		BORDER-BOTTOM: thin solid black;
		BACKGROUND-COLOR: silver;
	}
	TD.dictionary
	{
		BORDER-LEFT: thin solid black;
		BORDER-RIGHT: thin solid black;
		BORDER-BOTTOM: thin solid black;
	}
	TABLE.simplelist
	{
		BORDER-TOP: thin solid red;
		COLOR: red;
	    FONT: 8pt verdana, arial, helvetica, sans-serif;
	}
	TD.simplelist
	{
		BORDER-LEFT: thin solid red;
		BORDER-RIGHT: thin solid red;
		BORDER-BOTTOM: thin solid red;
	}
	SPAN.simplelist
	{
		COLOR: red;
	}
	-->
	</STYLE>
<%
end sub

sub DumpSubObject(byRef oKey)
    Select case TypeName(oKey)
        case "IDictionary"
            'sub-dictonary
            call DumpDict(oKey)
        case "ISimpleList"
            'sub-simplelist
            call DumpList(oKey)
        case "String", "Long", "Int"
            'string
            Response.Write CStr(oKey) & "&nbsp;"
        case else
            'unknown objects
            Response.Write "<I>&lt;" & typename(oKey) & "&gt;</I>"
    end select
end sub

sub DumpDict(byRef dDict)
    Dim sKey
    if dDict.count = 0 then
        'empty dictionary
        Response.Write "<SPAN CLASS='dictionary'>" & L_ObjEmptyDictionary_Text & "</SPAN>"
    else
        'display table for dictionary
        Response.Write "<TABLE CELLPADDING='5' CELLSPACING='0' CLASS='dictionary'>"
        for each sKey in dDict
            'display key and value
            Response.Write "<TR><TH VALIGN='top' CLASS='dictionary'>" & sKey & _
                           "</TH><TD CLASS='dictionary'>"
            DumpSubObject(dDict.Value(sKey))
            Response.Write "</TD></TR>"
        next
        Response.Write "</TABLE>"
    end if
end sub

sub DumpList(byRef slList)
    Dim oItem
    if slList.count = 0 then
        'empty simplelist
        Response.Write "<SPAN CLASS='simplelist'>" & L_ObjEmptySimpleList_Text & "</SPAN>"
    else
        'display table for simplelist
        Response.Write "<TABLE CELLPADDING='5' CELLSPACING='0' CLASS='simplelist'>"
        for each oItem in slList
            'display list item
            Response.Write "<TR><TD CLASS='simplelist'>"
            DumpSubObject(oItem)
            Response.Write "</TD></TR>"
        next
        Response.Write "</TABLE>"
    end if
end sub

sub ShowProgress(byVal sText)
	if VERBOSE_OUTPUT then Response.write sText
end sub

sub BDError(byVal sText)
	if SHOW_OBJECT_ERRORS then
		g_slObjectErrors.add sText
	end if
	if VERBOSE_OUTPUT then
		Response.write "<UL>"
		Response.write "<H3>" & L_ObjBizDeskFrameworkError_ErrorMessage & "</H3>"
		Response.write "<STRONG>" & sText & "</STRONG><BR>"
		Response.write "</UL>"
	end if
end sub

function VBError(byRef Err, byVal sText)
	VBError = false
	if Err.number = 0 then exit function
	if SHOW_OBJECT_ERRORS then
		g_slObjectErrors.add L_ObjVBScriptError_ErrorMessage
		g_slObjectErrors.add "__" & sText
		g_slObjectErrors.add "__" & sFormatString(L_ObjVBScriptErrorNumber_ErrorMessage, Array(Hex(Err.number)))
		g_slObjectErrors.add "__" & sFormatString(L_ObjVBScriptErrorDescription_ErrorMessage, Array(Replace(Err.description, Chr(13) & Chr(10), "  ")))
		g_slObjectErrors.add "__" & sFormatString(L_ObjVBScriptErrorSource_ErrorMessage, Array(Err.source))
	end if
	if VERBOSE_OUTPUT then
		Response.write "<UL>"
		Response.write "<H3>" & L_ObjVBScriptError_ErrorMessage & "</H3>"
		Response.write "<STRONG>" & sText & "</STRONG><BR>"
		Response.write sFormatString(L_ObjVBScriptErrorNumber_ErrorMessage, Array(Hex(Err.number))) & "<BR>"
		Response.write sFormatString(L_ObjVBScriptErrorDescription_ErrorMessage, Array(Err.description)) & "<BR>"
		Response.write sFormatString(L_ObjVBScriptErrorSource_ErrorMessage, Array(Err.source))
		Response.write "</UL>"
	end if
	VBError = true
end function

function XMLError(byRef xmlBD, byVal sText)
	XMLError = false
	if xmlBD.parseError.errorCode = 0 then exit function
	if SHOW_OBJECT_ERRORS then
		g_slObjectErrors.add L_ObjXMLError_ErrorMessage
		g_slObjectErrors.add "__" & sText
		g_slObjectErrors.add "__" & sFormatString(L_ObjXMLErrorCode_ErrorMessage, Array(Hex(xmlBD.parseError.errorCode)))
		g_slObjectErrors.add "__" & sFormatString(L_ObjXMLErrorFilePos_ErrorMessage, Array(xmlBD.parseError.filePos))
		g_slObjectErrors.add "__" & sFormatString(L_ObjXMLErrorLine_ErrorMessage, Array(xmlBD.parseError.line))
		g_slObjectErrors.add "__" & sFormatString(L_ObjXMLErrorLinePos_ErrorMessage, Array(xmlBD.parseError.linePos))
		g_slObjectErrors.add "__" & sFormatString(L_ObjXMLErrorReason_ErrorMessage, Array(Replace(xmlBD.parseError.reason, Chr(13) & Chr(10), "  ")))
		g_slObjectErrors.add "__" & sFormatString(L_ObjXMLErrorSrcText_ErrorMessage, Array(xmlBD.parseError.srcText))
		g_slObjectErrors.add "__" & sFormatString(L_ObjXMLErrorURL_ErrorMessage, Array(xmlBD.parseError.URL))
	end if
	if VERBOSE_OUTPUT then
		Response.write "<UL>"
		Response.write "<H3>" & L_ObjXMLError_ErrorMessage & "</H3>"
		Response.write "<STRONG>" & sText & "</STRONG><BR>"
		Response.write sFormatString(L_ObjXMLErrorCode_ErrorMessage, Array(xmlBD.parseError.errorCode)) & "<BR>"
		Response.write sFormatString(L_ObjXMLErrorFilePos_ErrorMessage, Array(xmlBD.parseError.filePos)) & "<BR>"
		Response.write sFormatString(L_ObjXMLErrorLine_ErrorMessage, Array(xmlBD.parseError.line)) & "<BR>"
		Response.write sFormatString(L_ObjXMLErrorLinePos_ErrorMessage, Array(xmlBD.parseError.linePos)) & "<BR>"
		Response.write sFormatString(L_ObjXMLErrorReason_ErrorMessage, Array(xmlBD.parseError.reason)) & "<BR>"
		Response.write sFormatString(L_ObjXMLErrorSrcText_ErrorMessage, Array(xmlBD.parseError.srcText)) & "<BR>"
		Response.write sFormatString(L_ObjXMLErrorURL_ErrorMessage, Array(xmlBD.parseError.URL))
		Response.write "</UL>"
	end if
	XMLError = true
end function
%>
