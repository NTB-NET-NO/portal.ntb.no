<!--#INCLUDE FILE='BDHeader.asp' -->
<!--#INCLUDE FILE='SharedStrings.asp' -->
<!--#INCLUDE FILE='BizDeskPaths.asp' -->
<!--#INCLUDE FILE='ASPUtil.asp' -->
<%
const L_DialogTallHeight_Style = 290
const L_DialogShortHeight_Style = 120
const L_DialogWidth_Style = 400
%>
<HTML>
<HEAD>
	<STYLE>
		HTML
		{
		    WIDTH: <%= L_DialogWidth_Style %>px;
		    HEIGHT: <%= L_DialogShortHeight_Style %>px;
		}
		BODY
		{
		    PADDING: 0;
		    CURSOR: default;
		    BACKGROUND-COLOR: #cccccc;
			COLOR: black;
			MARGIN-TOP: 5px;
			MARGIN-LEFT: 0;
			MARGIN-RIGHT: 0;
			MARGIN-BOTTOM: 0;
		    FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 8pt;
		    TEXT-ALIGN: center;
		}
		#bdclienterror
		{
			WIDTH: <%= L_DialogWidth_Style - 80 %>px;
			FONT-SIZE: 8pt;
			HEIGHT: 40px;
			TEXT-ALIGN: left;
			PADDING: 0;
			MARGIN-TOP: 0;
			MARGIN-LEFT: 0;
			MARGIN-RIGHT: 0;
			MARGIN-BOTTOM: 10px;
			OVERFLOW: auto;
		}
		#bderroricon
		{
			MARGIN-TOP: 6px;
			MARGIN-LEFT: 16px;
			MARGIN-RIGHT: 12px;
			MARGIN-BOTTOM: 6px;
		}
		INPUT, BUTTON
		{
			WIDTH: 9em;
			BACKGROUND-COLOR: #cccccc;
		}
		INPUT, BUTTON, INPUT SPAN, BUTTON SPAN
		{
			FONT-SIZE: 8pt;
		}
		#bdmorearrow
		{
			FONT-SIZE: 10pt;
			LINE-HEIGHT: 8pt;
			FONT-FAMILY: webdings;
		}
		#bderrordetails
		{
		    OVERFLOW: auto;
		    WIDTH: 90%;
		    HEIGHT: 150px;
		    BORDER: thin inset #cccccc;
		    BACKGROUND-COLOR: white;
			COLOR: black;
		    TEXT-ALIGN: left;
		    PADDING: 5px;
		    MARGIN: 0;
		    FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 8pt;
		}
		#bderrordetails TABLE
		{
			TABLE-LAYOUT: fixed;
			WIDTH: 100%;
			PADDING: 0;
		}
		#bderrordetails TH
		{
			VERTICAL-ALIGN: top;
			TEXT-ALIGN: left;
			FONT-SIZE: 8pt;
		}
		#bderrordetails TD
		{
			VERTICAL-ALIGN: top;
			FONT-SIZE: 8pt;
		}
		#bderrordetails #bddetailtitle
		{
			FONT-SIZE: 10pt;
		    TEXT-ALIGN: center;
		    WIDTH: 100%;
		    BACKGROUND-COLOR: black;
		    COLOR: white;
		}
	</STYLE>
	<SCRIPT LANGUAGE='VBScript'>
	<!--
		const L_DefaultError_ErrorMessage = "Problems with this page might have prevented it from functioning properly. Please contact your system administrator."
		const L_BizDeskError_ErrorMessage = "Business Desk Error"
		const L_BizDeskWarning_ErrorMessage = "Business Desk"
		const L_BizDeskError_HTMLTitle = "Business Desk: %1"
		const ERROR_ICON_ALERT = 0
		const ERROR_ICON_CRITICAL = 1
		const ERROR_ICON_QUESTION = 2
		const ERROR_ICON_INFORMATION = 3

		dim g_dArgs

		set g_dArgs = window.dialogArguments
		if g_dArgs("errorTitle") <> "" then
			document.title = sFormatString(L_BizDeskError_HTMLTitle, array(g_dArgs("errorTitle")))
		else
			if g_dArgs("errorType") < 2 then
				document.title = L_BizDeskError_ErrorMessage
			else
				document.title = L_BizDeskWarning_ErrorMessage
			end if
		end if

		Function sFormatString(sFormat, aArgs)
			dim nArg
			for nArg = LBound(aArgs) to UBound(aArgs)
				sFormat = Replace(sFormat, "%" & nArg + 1, aArgs(nArg))
			next
			sFormatString = sFormat
		End Function

		sub closeWindow()
			window.event.returnValue = false
			window.close()
		end sub

		sub toggleDetails()
			if bderrordetails.style.display = "none" then
				window.dialogHeight = "<%= L_DialogTallHeight_Style %>px"
				bderrordetails.style.display = "block"
				bdmorebtntext.innerText = "<%= L_BDSSHideDetails_Text %>"
				bdmorearrow.innerText = "5"
			else
				window.dialogHeight = "<%= L_DialogShortHeight_Style %>px"
				bderrordetails.style.display = "none"
				bdmorebtntext.innerText = "<%= L_BDSSShowDetails_Text %>"
				bdmorearrow.innerText = "6"
			end if
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY SCROLL='no'>

<TABLE width='100%'>
<TR>
	<TD VALIGN='top'>
		<IMG ID='bderroricon' SRC='' ALIGN='left' HEIGHT='32' WIDTH='32'>
		<SCRIPT LANGUAGE='VBScript'>
		<!--
			select case g_dArgs("errorType")
				case ERROR_ICON_ALERT
					bderroricon.src = "<%= g_sBDAssetRoot %>errAlert.gif"
				case ERROR_ICON_CRITICAL
					bderroricon.src = "<%= g_sBDAssetRoot %>errCritical.gif"
				case ERROR_ICON_QUESTION
					bderroricon.src = "<%= g_sBDAssetRoot %>errQuestion.gif"
				case ERROR_ICON_INFORMATION
					bderroricon.src = "<%= g_sBDAssetRoot %>errInformation.gif"
				case else
					bderroricon.src = "<%= g_sBDAssetRoot %>errAlert.gif"
			end select
		'-->
		</SCRIPT>
	</TD>
	<TD width='100%'>
		<DIV ID='bdclienterror'>
			<SCRIPT LANGUAGE='VBScript'>
			<!--
				if g_dArgs("friendlyError") <> "" then
					document.write  g_dArgs("friendlyError")
				else
					document.write L_DefaultError_ErrorMessage
				end if
			'-->
			</SCRIPT>
		</DIV>
	</TD>
</TR>
<TR>
	<TD COLSPAN='2'>
	<TABLE width='100%'>
	<TR>
		<TD width='100%'></TD>
		<FORM ID='bdcontrolform'>
		<TD>
			<INPUT ID='bdokbtn' TYPE='SUBMIT' CLASS='bdbutton'
			       VALUE='<%= L_BDSSOK_Button %>'
			       LANGUAGE='VBScript' ONCLICK='closeWindow()'>
		</TD>
		<TD ID='bdmorebtncell' STYLE='DISPLAY: none'>
			<BUTTON ID='bdmorebtn' CLASS='bdbutton'
				LANGUAGE="VBScript" ONCLICK='toggleDetails()' ONDBLCLICK='toggleDetails()'>
				<SPAN ID='bdmorebtntext'><%= L_BDSSShowDetails_Text %></SPAN>
				<SPAN ID='bdmorearrow'>6</SPAN>
			</BUTTON>
		</TD>
		</FORM>
	</TR>
	</TABLE>
</TD>
</TR>
</TABLE>

<DIV ID='bderrordetails' STYLE='DISPLAY: none;'>
	<SCRIPT LANGUAGE='VBScript'>
	<!--
		dim iStart, iEnd, sDetails
		if g_dArgs("errorDetails") <> "" then
			if inStr(g_dArgs("errorDetails"), "<HTML>") and _
				inStr(g_dArgs("errorDetails"), "</HTML>") then
				'parse and write details if from the 500error.asp page
				' (this parses out everything but the error information)
				iStart = inStr(g_dArgs("errorDetails"), "bderrordetails")
				if iStart > 0 then
					iStart = inStr(g_dArgs("errorDetails"), "<li>")
					sDetails = mid(g_dArgs("errorDetails"), iStart)
					iEnd = inStr(sDetails, "</TD>") - 1
					sDetails = left(sDetails, iEnd)
					document.write sDetails
				else
					'not 500error.asp so display general error
					document.write L_ErrorsInPageLoad_ErrorMessage
				end if
			else
				'write out details
				document.write g_dArgs("errorDetails")
			end if
			'show more details button when there are details
			bdmorebtncell.style.display = "block"
		end if
	'-->
	</SCRIPT>
</DIV>

</BODY>
</HTML>

