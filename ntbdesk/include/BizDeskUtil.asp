<%
dim g_MSCSEnv, g_bInDialog, AUTO_REDIRECT_ENABLED

' -- environment mode constants
Const PRODUCTION  = 1
Const DEVELOPMENT = 0

g_bInDialog = false

' -- get enviornment and turn off error display if PRODUCTION
g_MSCSEnv = Application("MSCSEnv")
if g_MSCSEnv = PRODUCTION then on error resume next
%>
<!--#INCLUDE FILE='DBUtil.asp' -->
<!--#INCLUDE FILE='ASPUtil.asp' -->
<!--#INCLUDE FILE='BDHALUtil.htm' -->
<!--#INCLUDE FILE='ActionPageUtil.asp' -->
<%
AUTO_REDIRECT_ENABLED = CInt(Application("AUTO_REDIRECT_ENABLED"))
%>
<SCRIPT LANGUAGE='VBScript'>
<!--
	option explicit

	'force all module pages to run only in content frame
	if window is window.top and <%= CInt(AUTO_REDIRECT_ENABLED) %> then
		window.location.replace("<%= g_sBizDeskRootPage %>")
		document.close
	end if
'-->
</SCRIPT>
