<!--#INCLUDE FILE='BDXMLHeader.asp' -->
<!--#INCLUDE FILE='HTTPXMLUtil.asp' -->
<%
Response.Write(xmlGetResponseFromRequest().xml)

function xmlGetResponseXML(xmlDOMDoc)
	dim xmlStateNode, xmlDoc, sOp, sDictName, sName, sValue, sKey, dServerState

	set xmlDoc = xmlDOMDoc.documentElement

	sDictName = xmlDoc.getattribute("name")
	if isEmpty(session(sDictName)) then
		set session(sDictName) = Server.CreateObject("Commerce.Dictionary")
	end if
	set dServerState = session(sDictName)

	sOp = xmlDoc.getAttribute("op")
	xmlDoc.removeAttribute("op")
	select case sOp
		case "set"
			for each xmlStateNode in xmlDoc.childNodes
				sName = xmlStateNode.getattribute("name")
				sValue = xmlStateNode.getAttribute("value")
				if not isNull(sName) and sName <> "" then dServerState(sName) = sValue
				xmlDoc.removeChild(xmlStateNode)
			next
			for each sKey in dServerState
				set xmlStateNode = xmlDoc.appendChild(xmlDOMDoc.createElement("item"))
				xmlStateNode.setAttribute "name", sKey
				xmlStateNode.setAttribute "value", dServerState(sKey)
			next
		case "get"
			for each sKey in dServerState
				set xmlStateNode = xmlDoc.appendChild(xmlDOMDoc.createElement("item"))
				xmlStateNode.setAttribute "name", sKey
				xmlStateNode.setAttribute "value", dServerState(sKey)
			next
		case "clear"
			for each sKey in dServerState
				dServerState(sKey) = null
			next
		case else
			AddErrorNode xmlDOMDoc, "unknown_op", "operator must be 'set', 'get' or 'clear'", ""
	end select
	set xmlGetResponseXML = xmlDOMDoc
end function

function xmlGetResponseFromRequest()
	set xmlGetResponseFromRequest = xmlGetResponseXML(xmlGetRequestXML())
end function
%>
