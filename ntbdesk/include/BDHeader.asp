<%@ Language=VBScript %><%'@ Language=VBScript CodePage=1252 %><%

'switch the @ lines above to set a codepage that is different than the system locale

'	IE 5.5 supports the following settings for charset and codepage:
'		(these are recommended though not all have been tested)
'
'Locale							Charset									CodePage
'------------------------------+---------------------------------------+--------
'Arabic (ISO)					iso-8859-6								28596
'Arabic (Windows)				windows-1256							1256
'Baltic (ISO)					iso-8859-4								28594 
'Baltic (Windows)				windows-1257							1257
'Central European (ISO)			iso-8859-2								28592
'Central European (Windows)		windows-1250							1250
'Chinese Simplified (EUC)		EUC-CN									51936
'Chinese Traditional (Big5)		big5									950 
'Cyrillic (ISO)					iso-8859-5								28595
'Cyrillic (Windows)				windows-1251							1251 
'Europa							x-Europa								29001
'Greek (ISO)					iso-8859-7								28597
'Greek (Windows)				windows-1253							1253 
'Hebrew (ISO-Logical)			iso-8859-8-i							38598
'Hebrew (ISO-Visual)			iso-8859-8								28598
'Hebrew (Windows)				windows-1255							20420
'Japanese (EUC)					euc-jp									51932 
'Japanese (Shift-JIS)			shift_jis								932
'Korean							ks_c_5601-1987							949 
'Korean (EUC)					euc-kr									51949
'Korean (ISO)					iso-2022-kr								50225
'Latin 3 (ISO)					iso-8859-3								28593
'Latin 9 (ISO)					iso-8859-15								28605
'Norwegian (IA5)				x-IA5-Norwegian							20108
'Swedish (IA5)					x-IA5-Swedish							20107
'Thai (Windows)					windows-874								874
'Turkish (ISO)					iso-8859-9								28599
'Turkish (Windows)				windows-1254							1254 
'Unicode						unicode									1200 
'Unicode (Big-Endian)			unicodeFFFE								1201
'Unicode (UTF-7)				utf-7									65000
'Unicode (UTF-8)				utf-8									65001
'Vietnamese (Windows)			windows-1258							1258 
'Western European (ISO)			iso-8859-1								28591
'Western European (Windows)		Windows-1252							1252 

Option Explicit

if not isEmpty(Application("MSCSPageEncodingCharset")) then Response.Charset = Application("MSCSPageEncodingCharset")
Response.AddHeader "Pragma", "no-cache"
Response.ContentType = "text/html"
Response.Expires = -1
Response.Buffer = true
%>
