<!--#INCLUDE FILE='BizDeskPaths.asp' -->
<!--#INCLUDE FILE='HTTPXMLUtil.htm' -->
<!--#INCLUDE FILE='BizDeskStrings.asp' -->
<!--#INCLUDE FILE='SharedStrings.asp' -->
<%
' -- check if objects loaded in Application
if isEmpty(Application("MSCSNav")) or isEmpty(Application("MSCSActionContainer")) then
if g_bInDialog then
%>
	<h3><%= L_BizDeskReloadingDialog_Text %></h3>
	<script LANGUAGE="VBScript">
	<!--
		window.setTimeout "window.close()", 10000, "VBScript"
	'-->
	</script>
<%
else
%>
	<h2><%= L_BizDeskReloading_Text %></h2>
	<script LANGUAGE="VBScript">
	<!--
		window.setTimeout "parent.location.reload(true)", 5000, "VBScript"
	'-->
	</script>
<%
end if
	Response.End
end if

dim ALLOW_CONTEXT_MENUS, g_mscsPage, g_MSCSActionContainer, g_MSCSDefaultLocale, _
	g_MSCSCurrencyLocale, g_sActionId, g_dActionPage, g_sMSCSDateFormat, _
	g_sMSCSCurrencyFormat, g_sMSCSNumberFormat, g_sFindByTitle, g_imgRequired, _
	g_imgInvalid, g_imgLoadingAnimation, g_sMSCSWeekStartDay

' -- BizDesk content menu flag from application
ALLOW_CONTEXT_MENUS	= CInt(Application("ALLOW_CONTEXT_MENUS"))

' -- task button dimensions
const TASK_BTN_HEIGHT		= 22
const TASK_BTN_WIDTH		= 23
const TASK_MENUBTN_WIDTH	= 33
' -- BizDesk arrow characters (webdings):
const LEFT_ARROW	= "3"
const RIGHT_ARROW	= "4"
const UP_ARROW		= "5"
const DOWN_ARROW	= "6"

' -- xml node types
const NODE_ELEMENT					= 1
const NODE_ATTRIBUTE				= 2
const NODE_TEXT						= 3
const NODE_CDATA_SECTION			= 4
const NODE_ENTITY_REFERENCE			= 5
const NODE_ENTITY					= 6
const NODE_PROCESSING_INSTRUCTION	= 7
const NODE_COMMENT					= 8
const NODE_DOCUMENT					= 9
const NODE_DOCUMENT_TYPE			= 10
const NODE_DOCUMENT_FRAGMENT		= 11
const NODE_NOTATION					= 12

' -- create Commerce Server Page object
Set g_mscsPage = Server.CreateObject("Commerce.Page")

' -- get locale info for Datafunctions and formats for widgets
g_MSCSDefaultLocale		= Application("MSCSDefaultLocale")
g_MSCSCurrencyLocale	= Application("MSCSCurrencyLocale")
g_sMSCSDateFormat		= Application("MSCSDateFormat")
g_sMSCSWeekStartDay		= Application("MSCSWeekStartDay")
g_sMSCSCurrencyFormat	= Application("MSCSCurrencyFormat")
g_sMSCSNumberFormat		= Application("MSCSNumberFormat")

'image markup
g_imgRequired = "<img src='" & g_sBDAssetRoot & "required.gif' ALIGN='absmiddle' HEIGHT='12' WIDTH='9' BORDER='0' TITLE='" & L_BDSSRequiredValue_ToolTip & "'>"
g_imgInvalid = "<img src='" & g_sBDAssetRoot & "invalid.gif' ALIGN='absmiddle' HEIGHT='12' WIDTH='9' BORDER='0' TITLE='" & L_BDSSInvalidValue_ToolTip & "'>"
g_imgLoadingAnimation = "<img SRC='" & g_sBDAssetRoot & "statusanimation.gif' ALIGN='absmiddle' HEIGHT='18' WIDTH='18' BORDER='0' STYLE='FILTER: invert'>"

' -- get action container from application
set g_MSCSActionContainer = Application("MSCSActionContainer")
' -- construct URL with query string for this page relative to bizdesk root
g_sActionId = Mid(Request.ServerVariables("SCRIPT_NAME") & "?" & Request.ServerVariables("QUERY_STRING"), Len(g_sBizDeskRoot) + 1)
' -- remove ending '?' or '&' if necessary
if right(g_sActionId, 1) = "?" or right(g_sActionId, 1) = "&" then g_sActionId = left(g_sActionId, len(g_sActionId) - 1)
' -- get action dictionary for this page to use for displaying task buttons 
set g_dActionPage = dGetAction(g_sActionId)

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	set dAction = dGetAction(sLookupActionID)
'		gets current action page dictionary
'
'	Arguments:	sLookupActionID		String. Action page URL relative to BizDesk root
'
'	Returns:						Dictionary. Current action page dictionary:
'
'		dAction.helptopic		-help page to launch when help button clicked
'		dAction.name			-name with HTML markup for current page
'		dAction.tooltip			-tooltip text to display on links
'		dAction.tasks()			-simplelist of tasks for the action page
'		dAction.tasks().id				-short id for task (save, savenew, etc.)
'		dAction.tasks().tooltip			-tooltip text to display on links
'		dAction.tasks().name			-name with HTML markup for when no icon
'		dAction.tasks().icon			-gif in bizdesk/assets to use as button icon
'		dAction.tasks().key				-character for keyboard access
'		dAction.tasks().type			-'menu' or empty for most framework modules
'		dAction.tasks().goto()			-simplelist of gotos for the task
'		dAction.tasks().goto().action		-URL (relative to bizdesk root) for action page
'		dAction.tasks().postto()		-simplelist of posttos for the task
'		dAction.tasks().postto().action		-URL (relative to bizdesk root) for action page
'		dAction.tasks().postto().formname	-formname on current page to post for action
'		dAction.tasks().displayname()		-simplelist of displaynames for goto/postto menu items
'
function dGetAction(byVal sLookupActionID)
	' -- set empty dictionary for if find fails
	set dGetAction = Server.CreateObject("Commerce.Dictionary")
	set dGetAction.tasks = Server.CreateObject("Commerce.SimpleList")
	if isEmpty(g_MSCSActionContainer) or isNull(g_MSCSActionContainer.actions) then exit function
	dim sActionID
	' -- search for dictionary for the current page
	for each sActionID in g_MSCSActionContainer.actions
		' -- check if action id is contained in lookup id (NOT equals)
		if inStr(sLookupActionID, sActionID) then
			set dGetAction = g_MSCSActionContainer.actions(sActionID)
			exit for
		end if
	next
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' TASKBAR
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	InsertEditTaskBar(sPageTitle, sStatusText)
'		writes out client code for task bar, page title, status bar displayed in
'		edit mode (with left navigation tree menu hidden)
'
'	Arguments:	sPageTitle		String. Text to display as page title in title bar
'				sStatusText		String. Text to display initially in status bar
'
sub InsertEditTaskBar(sPageTitle, sStatusText)
	' -- insert task bar code for edit page
	InsertNonClientArea sPageTitle, sStatusText, true
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	InsertTaskBar(sPageTitle, sStatusText)
'		writes out client code for task bar, page title, status bar in list mode
'
'	Arguments:	sPageTitle		String. Text to display as page title in title bar
'				sStatusText		String. Text to display initially in status bar
'
sub InsertTaskBar(sPageTitle, sStatusText)
	' -- insert task bar code for list page
	InsertNonClientArea sPageTitle, sStatusText, false
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework: used to draw task buttons in task bar
sub InsertIconBtns()
	Dim dTask
	for each dTask in g_dActionPage.tasks
		InsertIconBtn dTask
	next
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework: used to draw task button
sub InsertIconBtn(dTask)
	Dim sOnClick, sClass, i, nBtnWidth, sInnerButton, sTooltip, sDebugText
	' -- set onclick handler text
	if dTask.id = "find" then
		sOnClick = "toggleFindBy()"
		if dTask.displayname.count = 1 then g_sFindByTitle = dTask.displayname(0)
		sDebugText = "find"
	elseif dTask.postto.count > 0 then
		sOnClick = "PosttoAction """ & g_sBizDeskRoot & dTask.postto(0).action & """, document.all(""" & dTask.postto(0).formname & """)"
		sDebugText = g_sBizDeskRoot & dTask.postto(0).action & vbCr & "&nbsp;&nbsp;formname=" & dTask.postto(0).formname
	elseif dTask.goto.count > 0 then
		sOnClick = "GotoAction(""" & g_sBizDeskRoot & dTask.goto(0).action & """)"
		sDebugText = g_sBizDeskRoot & dTask.goto(0).action
	end if
	sTooltip = sFormatString(L_TaskTooltipAccesskey_Tooltip, _
			array(replace(dTask.tooltip, """", "&quot;"), uCase(dTask.key)))
	if SHOW_DEBUG_TEXT then
		sTooltip = sTooltip & vbCr & "&nbsp;&nbsp;action=" & sDebugText
	end if
	' -- set class and disabled status (back always enabled)
	if dTask.id = "back" then
		sClass =  " CLASS='bdtaskbutton'"
		nBtnWidth = TASK_BTN_WIDTH
	elseif dTask.type = "menu" then
		sClass =  " CLASS='bdtaskbuttonmenu bdtaskdisabled' DISABLED"
		nBtnWidth = TASK_MENUBTN_WIDTH
	else
		sClass =  " CLASS='bdtaskbutton bdtaskdisabled' DISABLED"
		nBtnWidth = TASK_BTN_WIDTH
	end if
	' -- set inner text for button to be icon or text
	if dTask.icon = "" then
		sInnerButton = dTask.name
		sClass =  replace(sClass, "bdtaskbutton", "bdtasktextbutton")
	else
		sInnerButton = "<IMG SRC=""" & g_sBDAssetRoot & dTask.icon & """ " & _
			"ALIGN='absbottom' HEIGHT='" & TASK_BTN_HEIGHT & "' WIDTH='" & nBtnWidth & "'>"
	end if
	response.write "<TD WIDTH=" & nBtnWidth & " STYLE='PADDING-LEFT: 2px; PADDING-RIGHT: 2px'>" & _
			"<BUTTON ID='bdtaskbtn" & dTask.id & "' ACCESSKEY='" & dTask.key & _
			"' TYPE='" & dTask.type & "'" & sClass & " TITLE=""" & sTooltip &_
			""" LANGUAGE='VBScript' ONKEYDOWN='task_onKeyDown' ONFOCUS='task_onMouseOver' " & _
			"ONBLUR='task_onMouseOut' ONMOUSEDOWN='task_onMouseDown' ONMOUSEOVER='task_onMouseOver' " & _
			"ONMOUSEOUT='task_onMouseOut' ONCLICK='" & sOnClick & "'>" & sInnerButton & _
			"</BUTTON>"
	if dTask.type = "menu" then
		response.write "<div ID='bdtaskbtn" & dTask.id & "menu' CLASS='bdtaskmenu' " & _
			"LANGUAGE='vbscript' ONMOUSEMOVE='menu_onMouseMove' ONMOUSEOVER='menu_onMouseOver' " & _
			"ONKEYDOWN='menu_onKeyDown' ONCLICK='menu_onClick'>"
		sTooltip = ""
		for i = 0 to dTask.displayname.count - 1
			if dTask.postto.count > 0 then
				if SHOW_DEBUG_TEXT then
					sTooltip = " TITLE='action=" & g_sBizDeskRoot & dTask.postto(i).action & vbCr & "formname=" & dTask.postto(i).formname & "'"
				end if
				response.write "<DIV ID='" & g_sBizDeskRoot & dTask.postto(i).action & "'" & _
					" TABINDEX='0' NOWRAP CLASS=' bdtaskdisabled' INDEX='" & (i + 1) & _
					"' FORM='" & dTask.postto(i).formname & "'" & sTooltip & ">" & dTask.displayname(i) & _
					"</DIV>"
			else
				if SHOW_DEBUG_TEXT then
					sTooltip = " TITLE='action=" & g_sBizDeskRoot & dTask.goto(i).action & "'"
				end if
				response.write "<DIV ID='" & g_sBizDeskRoot & dTask.goto(i).action & "'" & _
					" TABINDEX='0' NOWRAP CLASS=' bdtaskdisabled' INDEX='" & (i + 1) & _
					"'" & sTooltip & ">" & dTask.displayname(i) & "</DIV>"
			end if
		next
		response.write "</TD></DIV>"
	end if
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework: used to draw help task button
sub InsertHelpIconBtn()
	dim sTooltip
	sTooltip = sFormatString(L_TaskTooltipAccesskey_Tooltip, _
			array(replace(L_ViewHelpForPage_Tooltip, """", "&quot;"), "?"))
	if SHOW_DEBUG_TEXT then
		sTooltip = sTooltip & vbCr & "&nbsp;&nbsp;helptopic=" & g_dActionPage.value("helptopic")
	end if
	response.write "<BUTTON ID='bdtaskbtnhelp' TYPE='help' ACCESSKEY='?' " & _
		"CLASS='bdtaskbutton' TITLE=""" & sTooltip & _
		""" LANGUAGE='VBScript' ONKEYDOWN='task_onKeyDown' ONFOCUS='task_onMouseOver' " & _
		"ONBLUR='task_onMouseOut' ONMOUSEDOWN='task_onMouseDown' ONMOUSEOVER='task_onMouseOver' " & _
		"ONMOUSEOUT='task_onMouseOut' ONCLICK='OpenHelp """ & g_dActionPage.value("helptopic") & _
		"""'><img SRC=""" & g_sBDAssetRoot & "taskhelp.gif"" ALIGN='absbottom' HEIGHT='" & _
		TASK_BTN_HEIGHT & "' WIDTH='" & TASK_BTN_WIDTH & "'></button>"
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework: common code to draw task bar, page title, status bar
sub InsertNonClientArea(sPageTitle, sStatusText, bEditMode)
	' -- set default find by title
	g_sFindByTitle = L_BDFindBy_Text
%>
<script LANGUAGE="VBScript">
<!--
	option explicit

	dim g_sConfirmMsg, g_sWarnMsg, g_bDirty, g_bRequired, g_bValid, g_nFindByHeight, g_bUnloading
	const FINDBY_TOP = 55		'top of find by area used to calculate body top margin
	g_bDirty = false			'dirty flag: true means an items was changed
	g_bRequired = false			'required flag: true means a required item is empty
	g_bValid = true				'valid flag: false means an item is invalid
	g_nFindByHeight = 150		'height of find by area when displayed
	g_bUnloading = false		'true when page unloading from task action

	<% if g_MSCSEnv = PRODUCTION then %>on error resume next<% end if %>
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' task button routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	EnableTask(sID)
	'		enables task button identified by sID
	'
	'	Arguments:	sID		String. Id of task (set in module configuration xml)
	'
	sub EnableTask(sID)
		dim elTaskBtn
		' -- id can be passed as "new" or "bdtaskbtnnew" and work
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID
		set elTaskBtn = document.all(sID)
		if not elTaskBtn is nothing then
			elTaskBtn.disabled = false
			elTaskBtn.className = replace(elTaskBtn.className, " bdtaskdisabled", "")
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	DisableTask(sID)
	'		disables task button identified by sID
	'
	'	Arguments:	sID		String. Id of task (set in module configuration xml)
	'
	sub DisableTask(sID)
		dim elTaskBtn
		' -- id can be passed as "new" or "bdtaskbtnnew" and work
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID
		set elTaskBtn = document.all(sID)
		if not elTaskBtn is nothing then
			elTaskBtn.disabled = true
			elTaskBtn.className = replace(elTaskBtn.className, " bdtaskover", "")
			elTaskBtn.className = replace(elTaskBtn.className, " bdtaskpress", "")
			elTaskBtn.className = elTaskBtn.className & " bdtaskdisabled"
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ToggleTask(sID)
	'		toggles disabled state of task button identified by sID
	'
	'	Arguments:	sID		String. Id of task (set in module configuration xml)
	'
	sub ToggleTask(sID)
		dim elTaskBtn
		' -- id can be passed as "new" or "bdtaskbtnnew" and work
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID
		set elTaskBtn = document.all(sID)
		if elTaskBtn.disabled then
			EnableTask(sID)
		else
			DisableTask(sID)
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	EnableAllTasks(bEnable)
	'		sets enabled state for all task buttons to bEnable
	'
	'	Arguments:	bEnable		Boolean. State to set. True = enabled, false = disabled.
	'
	sub EnableAllTasks(bEnable)
		dim elTaskBtn
		for each elTaskBtn in bdtaskarea.all.tags("BUTTON")
			if bEnable then
				EnableTask(elTaskBtn.id)
			else
				DisableTask(elTaskBtn.id)
				HideStatusAnimation()
			end if
		next
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	EnableTaskMenuItem(sID, nItem)
	'		enables task button menu item identified by sID and nItem index
	'
	'	Arguments:	sID		String. Id of task (set in module configuration xml)
	'				nItem	Integer. Index of menu item starting with 0.
	'
	sub EnableTaskMenuItem(sID, nItem)
		dim elTaskMenu
		'id can be passed as "new", "bdtaskbtnnew" or "bdtaskbtnnewmenu" and work
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID
		if Right(sID, 4) <> "menu" then sID = sID & "menu"
		set elTaskMenu = document.all(sID)
		if not elTaskMenu is nothing and elTaskMenu.children.length >= nItem then
			elTaskMenu.children(nItem).className = replace(elTaskMenu.children(nItem).className, " bdtaskdisabled", "")
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	DisableTaskMenuItem(sID, nItem)
	'		disables task button menu item identified by sID and nItem index
	'
	'	Arguments:	sID		String. Id of task (set in module configuration xml)
	'				nItem	Integer. Index of menu item starting with 0.
	'
	sub DisableTaskMenuItem(sID, nItem)
		dim elTaskMenu
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID & "menu"
		if sID = "bdtaskbtnback" then exit sub
		set elTaskMenu = document.all(sID)
		if not elTaskMenu is nothing and elTaskMenu.children.length >= nItem then
			elTaskMenu.children(nItem).className = elTaskMenu.children(nItem).className & " bdtaskdisabled"
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	EnableAllTaskMenuItems(sID, bEnable)
	'		sets the enabled state for all task button menu item identified by sID 
	'		 to bEnable
	'
	'	Arguments:	sID			String. Id of task (set in module configuration xml)
	'				bEnable		Boolean. State to set. True = enabled, false = disabled.
	'
	sub EnableAllTaskMenuItems(sID, bEnable)
		dim elTaskMenu, i
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID & "menu"
		set elTaskMenu = document.all(sID)
		if not elTaskMenu is nothing then
			for i = 0 to elTaskMenu.children.length -1
				if bEnable then
					EnableTaskMenuItem sID, i
				else
					DisableTaskMenuItem sID, i
				end if
			next
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	set elTaskBtn = elGetTaskBtn(sID)
	'		returns the element for the task button identified by sID
	'
	'	Arguments:	sID			String. Id of task (set in module configuration xml)
	'
	'	Returns:				Element. HTML BUTTON element for the task. Nothing if
	'							not found.
	'
	function elGetTaskBtn(sID)
		' -- id can be passed as "new" or "bdtaskbtnnew" and work
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID
		set elGetTaskBtn = document.all(sID)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetTaskTooltip(sID, sTooltip)
	'		sets the tooltip for the task button identified by sID 
	'
	'	Arguments:	sID			String. Id of task (set in module configuration xml)
	'				sTooltip	String. Text to display as tooltip on mouse over.
	'
	sub SetTaskTooltip(sID, sTooltip)
		dim elTaskBtn
		' -- id can be passed as "new" or "bdtaskbtnnew" and work
		if Left(sID, 9) <> "bdtaskbtn" then sID = "bdtaskbtn" & sID
		set elTaskBtn = document.all(sID)
		sTooltip = sFormatString("<%= L_TaskTooltipAccesskey_Tooltip %>", _
			array(sTooltip, uCase(elTaskBtn.accesskey)))
<%	if SHOW_DEBUG_TEXT then
%>		dim aString, sDebugText
		' -- preserve debug string after the CR
		aString = split(elTaskBtn.title, vbCR)
		sDebugText = Mid(elTaskBtn.title, Len(aString(0)) + 1)
		sTooltip = sTooltip & sDebugText
<%	end if
%>		elTaskBtn.title = sTooltip
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	OpenEditPage(sPage, fnCallback)
	'		opens a new nested edit page from the current edit page; when the nested 
	'		edit page is closed then the callback function will be called
	'
	'	Arguments:	sPage		String. URL of page to open.
	'				fnCallback	Function name string. Name of function to call when
	'							nested edit page closes.
	'
	sub OpenEditPage(sPage, fnCallback)
		EnableAllTasks(false)
		'call routine in parent (bizdesk.asp)
		parent.OpenEditPage sPage, fnCallback
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' find by routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetFindByHeight(nNewHeight)
	'		sets the height of the find by area (and moves down top margin of body)
	'		the next time the find by area is displayed
	'
	'	Arguments:	nNewHeight	Integer. Number of pixels in height.
	'
	sub SetFindByHeight(nNewHeight)
		g_nFindByHeight = nNewHeight
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ToggleFindBy()
	'		toggles the state of display for the find by area
	'
	sub ToggleFindBy()
		if bdfindby.style.display <> "none" then
			HideFindBy()
		else
			ShowFindBy()
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ShowFindBy()
	'		shows the find by area using the current value of g_nFindByHeight as the height
	'
	sub ShowFindBy()
		dim elfindByContent
		document.body.style.marginTop = (FINDBY_TOP + g_nFindByHeight) & "px"
		bdfindby.style.display = "block"
		bdfindby.style.zIndex = -1
		bdfindby.style.height = g_nFindByHeight & "px"
		bdfindbybody.style.height = (g_nFindByHeight - 27) & "px"
		set elfindByContent = document.all("bdfindbycontent")
		if not elfindByContent is nothing then
			elfindByContent.style.display = "block"
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	HideFindBy()
	'		hides the find by area
	'
	sub HideFindBy()
		dim elfindByContent
		bdfindby.style.display = "none"
		set elfindByContent = document.all("bdfindbycontent")
		if not elfindByContent is nothing then
			elfindByContent.style.display = "none"
		end if
		document.body.style.marginTop = FINDBY_TOP & "px"
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' client state routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetTemp(sName, oValue)
	'		saves a temporary value in a scripting dictionary in the parent frame
	'
	'	Arguments:	sName	String. Name to use as lookup key for value.
	'				oValue	Variant or Object. Value or reference to store.
	'
	sub SetTemp(sName, oValue)
		' -- call routine in parent (bizdesk.asp)
		parent.SetTemp sName, oValue
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	set oValue = GetTemp(sName)
	'		OR
	'	sValue = GetTemp(sName)
	'		retrieves a temporary value from a scripting dictionary in the parent frame
	'
	'	Arguments:	sName	String. Name to use as lookup key for value.
	'
	'	Returns:			Variant or Object. Value or reference retrieved.
	'
	function GetTemp(sName)
		' -- call routine in parent (bizdesk.asp)
		if isObject(parent.GetTemp(sName)) then
			set GetTemp = parent.GetTemp(sName)
		else
			GetTemp = parent.GetTemp(sName)
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ClearTemp(sName)
	'		removes a temporary value from a scripting dictionary in the parent frame
	'
	'	Arguments:	sName	String. Name to use as lookup key for value.
	'
	sub ClearTemp(sName)
		' -- call routine in parent (bizdesk.asp)
		parent.ClearTemp(sName)
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' master EditSheet event handler routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetValid(sWarnMsg)
	'		used as onValid handler for master EditSheet for an edit page
	'
	'	Arguments:	sWarnMsg	String. Text of warning message to display when
	'							invalid values are being saved. Empty string
	'							causes default warning to be used.
	'
	sub SetValid(sWarnMsg)
		g_bValid = window.event.valid
		g_sWarnMsg = sWarnMsg
		ResetSaveBtns()
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetRequired(sWarnMsg)
	'		used as onRequire handler for master EditSheet for an edit page
	'
	'	Arguments:	sWarnMsg	String. Text of warning message to display when
	'							required values are empty and an attempt to save is
	'							made. Empty string causes default warning to be used.
	'
	sub SetRequired(sWarnMsg)
		g_bRequired = window.event.required
		g_sWarnMsg = sWarnMsg
		ResetSaveBtns()
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetDirty(sConfirmMsg)
	'		used as onChange handler for master EditSheet for an edit page
	'
	'	Arguments:	sConfirmMsg		String. Text of confirmation message to display 
	'								when exiting a changed page without saving.
	'
	sub SetDirty(sConfirmMsg)
		g_bDirty = true
		g_sConfirmMsg = sConfirmMsg
		ResetSaveBtns()
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	resets save/savenew button state after each event (onChange,
	'						onRequire, onValid)
	sub ResetSaveBtns()
		if g_bDirty and not g_bRequired and g_bValid then
			' -- dirty and nothing required or invalid so enable saves
			EnableTask("save")
			EnableTask("saveback")
			EnableTask("savenew")
		else
			' -- not dirty or something required or invalid so disable saves
			DisableTask("save")
			DisableTask("saveback")
			DisableTask("savenew")
			HideWaitDisplay()
		end if
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' task button event handler routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	used to display inner text as tooltip on mouse over
	sub ShowTooltip()
		dim elSource
		set elSource = window.event.srcElement
		elSource.title = elSource.innerText
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse move handler for task button menu items
	sub menu_onMouseMove()
		' -- call routine in parent (bizdesk.asp)
		parent.menu_onMouseMove(window.event.srcElement)
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse over handler for task buttons
	sub task_onMouseOver()
		' -- call routine in parent (bizdesk.asp)
		parent.task_onMouseOver window, window.event
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse down handler for task buttons
	sub task_onMouseDown()
		dim elSource
		set elSource = window.event.srcElement
		'start wait display if save button clicked
		if elSource.id = "bdtaskbtnsave" or _
			elSource.id = "bdtaskbtnsavenew" or _
			elSource.id = "bdtaskbtnsaveback" then _
				onSaveShowWait()	
		' -- call routine in parent (bizdesk.asp)
		parent.task_onMouseDown window, elSource
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse out handler for task buttons
	sub task_onMouseOut()
		if not g_bUnloading then HideWaitDisplay()
		' -- call routine in parent (bizdesk.asp)
		parent.task_onMouseOut window, window.event.srcElement
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	key down handler for task button to treat enter like click
	sub task_onKeyDown()
		' -- call routine in parent (bizdesk.asp)
		parent.task_onKeyDown window, window.event
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	key down handler for help task menu to handle enter and arrows
	sub menu_onKeyDown()
		' -- call routine in parent (bizdesk.asp)
		parent.menu_onKeyDown window, window.event
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	closes open task button menu
	sub CloseMenu()
		' -- call routine in parent (bizdesk.asp)
		parent.CloseMenu(window)
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	selects a task button menu item
	sub SetSelection(elNewSelection)
		' -- call routine in parent (bizdesk.asp)
		parent.SetSelection(elNewSelection)
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	unselects a task button menu item
	sub RemoveSelection()
		' -- call routine in parent (bizdesk.asp)
		parent.removeSelection()
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' xml hierarchical merge routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	xmlMergeGroups2(xmlMaster, xmlDetail, sKeyId)
	'		merges 2 XML data documents into a 2 level hierarchy usable by ListSheet widget
	'
	'	Arguments:	xmlMaster	XML document. master XML document
	'				xmlDetail	XML document. detail XML document
	'				sKeyId		String. key field id to use for merge
	'
	'	Returns:				XML document. master document with detail items
	'							inserted under records that have the same sKeyId value
	'
	function xmlMergeGroups2(xmlMaster, xmlDetail, sKeyId)
		dim xmlRecord, xmlKeyNode, xmlSublist, xmlSubRecord

		for each xmlRecord in xmlMaster.selectNodes("record")
			set xmlKeyNode = xmlRecord.selectSingleNode(sKeyId)
			if not xmlKeyNode is nothing then
				set xmlSublist = xmlDetail.selectNodes("record[" & sKeyId & " $eq$ '" & xmlKeyNode.text & "']")
				for each xmlSubRecord in xmlSublist
					xmlRecord.appendChild(xmlSubRecord.cloneNode(true))
				next
			end if
		next
		set xmlMergeGroups2 = xmlMaster
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	xmlMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
	'		merges 3 XML data documents into a 3 level hierarchy usable by ListSheet widget
	'
	'	Arguments:	xmlMaster		XML document. master XML document
	'				xmlGroup		XML document. second level XML document
	'				xmlDetail		XML document. detail XML document
	'				sMasterKeyId	String. key field id to use for merge of master and group/detail
	'				sDetailKeyId	String. key field id to use for merge of group and detail
	'
	'	Returns:					XML document. master document with group and detail items
	'								inserted under records that have the same sXXXKeyId value
	'
	function xmlMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
		set xmlMergeGroups3 = xmlMergeGroups2(xmlMaster, xmlMergeGroups2(xmlGroup, xmlDetail, sDetailKeyId), sMasterKeyId)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	xmlMergeGroupsByID2(sMaster, sDetail, sKeyId)
	'		merges 2 XML data documents into a 2 level hierarchy usable by ListSheet widget
	'
	'	Arguments:	sMaster		String. master XML data island id
	'				sDetail		String. detail XML data island id
	'				sKeyId		String. key field id to use for merge
	'
	'	Returns:				XML document. master document with detail items
	'							inserted under records that have the same sKeyId value
	'
	function xmlMergeGroupsByID2(sMaster, sDetail, sKeyId)
		set xmlMergeGroupsByID2 = xmlMergeGroups2(document.all(sMaster).xmlDocument.documentElement, document.all(sDetail).xmlDocument.documentElement, sKeyId)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	xmlMergeGroupsByID3(sMaster, sGroup, sDetail, sMasterKeyId, sDetailKeyId)
	'		merges 3 XML data documents into a 3 level hierarchy usable by ListSheet widget
	'
	'	Arguments:	sMaster			String. master XML data island id
	'				sGroup			String. second level XML data island id
	'				sDetail			String. detail XML data island id
	'				sMasterKeyId	String. key field id to use for merge of master and group/detail
	'				sDetailKeyId	String. key field id to use for merge of group and detail
	'
	'	Returns:					XML document. master document with group and detail items
	'								inserted under records that have the same sXXXKeyId value
	'
	function xmlMergeGroupsByID3(sMaster, sGroup, sDetail, sMasterKeyId, sDetailKeyId)
		set xmlMergeGroupsByID3 = xmlMergeGroups3(document.all(sMaster).xmlDocument.documentElement, document.all(sGroup).xmlDocument.documentElement, document.all(sDetail).xmlDocument.documentElement, sMasterKeyId, sDetailKeyId)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	sMergeGroups2(xmlMaster, xmlDetail, sKeyId)
	'		merges 2 XML data documents into a 2 level hierarchy usable by ListSheet widget
	'
	'	Arguments:	xmlMaster	XML document. master XML document
	'				xmlDetail	XML document. detail XML document
	'				sKeyId		String. key field id to use for merge
	'
	'	Returns:				String. XML string of master document with detail items
	'							inserted under records that have the same sKeyId value
	'
	function sMergeGroups2(xmlMaster, xmlDetail, sKeyId)
		sMergeGroups2 = xmlMergeGroups2(xmlMaster, xmlDetail, sKeyId).xml
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	sMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
	'		merges 3 XML data documents into a 3 level hierarchy usable by ListSheet widget
	'
	'	Arguments:	xmlMaster		XML document. master XML document
	'				xmlGroup		XML document. second level XML document
	'				xmlDetail		XML document. detail XML document
	'				sMasterKeyId	String. key field id to use for merge of master and group/detail
	'				sDetailKeyId	String. key field id to use for merge of group and detail
	'
	'	Returns:					String. XML string of master document with group and detail items
	'								inserted under records that have the same sXXXKeyId value
	'
	function sMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
		sMergeGroups3 = xmlMergeGroups3(xmlMaster, xmlDetail, sMasterKeyId, sDetailKeyId).xml
	end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' framework task routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes a task button's postto or goto
	sub DoTask(elSource)
		dim bAbandonTask

		g_bUnloading = true
		if elSource.getAttribute("type") = "menu" then
			window.event.cancelBubble = true
			exit sub
		end if

		bAbandonTask = bCheckTask(elSource)
		if not bAbandonTask then
			parent.DoTask window, elSource
		else
			g_bUnloading = false
			HideWaitDisplay()
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes a task button's goto
	sub GotoAction(sHREF)
		dim bAbandonTask, elSource
		g_bUnloading = true
		set elSource = window.event.srcElement
		if elSource.getAttribute("type") = "menu" then
			window.event.cancelBubble = true
			exit sub
		end if

		bAbandonTask = bCheckTask(elSource)
		if not bAbandonTask then
			'send URL to parent for execution of task
			parent.GotoAction window, sHREF
		else
			g_bUnloading = false
			HideWaitDisplay()
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes a task button's postto
	sub PosttoAction(sHREF, elForm)
		dim bAbandonTask, elSource

		g_bUnloading = true
		set elSource = window.event.srcElement
		if elSource.getAttribute("type") = "menu" then
			window.event.cancelBubble = true
			exit sub
		end if

		bAbandonTask = bCheckTask(elSource)
		if not bAbandonTask then
			' -- send URL and form to parent for execution of task
			parent.PosttoAction window, sHREF, elForm
		else
			g_bUnloading = false
			HideWaitDisplay()
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	displays confirmation and warning messages for edit pages
	function bCheckTask(elSource)
		dim bAbandonTask, sResponse, elBtnSave, elBtnSaveBack, elBtnSaveNew, elBtnBack

		bAbandonTask = false
		sResponse = ""

		set elSource = window.event.srcElement
		set elBtnSave = elGetTaskBtn("save")
		set elBtnSaveBack = elGetTaskBtn("saveback")
		set elBtnSaveNew = elGetTaskBtn("savenew")
		set elBtnBack = elGetTaskBtn("back")

		if elSource is elBtnSave or elSource is elBtnSaveBack or elSource is elBtnSaveNew then
			'clicked save so check if required or invalid
			if g_bRequired or not g_bValid then
				'if fields required or invalid display "Unable to save because of incomplete changes" warning
				'Usually not possible to get this message if Save and SaveNew buttons disabled properly
				showModalDialog "<%= g_sBDIncludeRoot %>Dlg_Warn.asp?type=OKOnly", g_sWarnMsg, "status:no;help:no"
				parent.focus()
				bAbandonTask = true
			end if
			'remember to close when done if save and back
			if elSource is elBtnSaveBack then parent.g_bCloseAfterSave = true
		elseif elSource is elBtnBack then 
			'if dirty and required or invalid display "Discard incomplete changes?" prompt
			if g_bDirty and (g_bRequired or not g_bValid) then
				sResponse = showModalDialog("<%= g_sBDIncludeRoot %>Dlg_Warn.asp?type=OKCancel", "", "status:no;help:no")
				parent.focus()
				if sResponse = "cancel" or sResponse = "" then
					bAbandonTask = true
				end if
			elseif g_bDirty then
				'if dirty and complete display "Save changes before exit?" warning
				sResponse = showModalDialog("<%= g_sBDIncludeRoot %>Dlg_Confirm.asp", g_sConfirmMsg, "status:no;help:no")
				parent.focus()
				if sResponse = "yes" then
					'reenable save buttons and abandon task
					enableTask("save")
					enableTask("saveback")
					enableTask("savenew")
					if not elBtnSave is nothing then
						'remember to close when done 
						parent.g_bCloseAfterSave = true
						elBtnSave.click()
						if parent.g_bCloseAfterSave then bAbandonTask = true
					elseif not elBtnSaveNew is nothing then
						'remember to close when done 
						parent.g_bCloseAfterSave = true
						elBtnSaveNew.click()
						if parent.g_bCloseAfterSave then bAbandonTask = true
					end if
				elseif sResponse = "cancel" or sResponse = "" then
					bAbandonTask = true
				end if
			end if
		end if
		bCheckTask = bAbandonTask
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes when any save button is clicked
	sub onSaveShowWait()
		SetWaitDisplayText("<%= L_BDSSSaving_Text %>")
		ShowWaitDisplay()
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	executes a form's ONTASK routine
	sub ExecuteTask(sOnTask)
		execute(sOnTask)
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' status bar routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetStatusText(sText)
	'		hides loading text and animation and then displays new status text in status bar
	'
	'	Arguments:	sText	String. Text to display in status bar
	'
	sub SetStatusText(sText)
		HideStatusAnimation()
		bdstatustext.innerText = sText
		window.setTimeout "ClearStatusText()", 10000
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ClearStatusText()
	'		clears status text in status bar
	'
	'	Arguments:	none
	'
	sub ClearStatusText()
		on error resume next
		bdstatustext.innerText = ""
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	HideStatusAnimation()
	'		hides loading text and animation in status bar
	'
	sub HideStatusAnimation()
		bdstatusloading.style.display = "none"
		bdlegend.style.visibility = "visible"
		bdwaitdisplay.style.display = "none"
		window.setTimeout "ClearStatusText()", 10000
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	HideStatusLegend()
	'		hides legend in status bar
	'
	sub HideStatusLegend()
		bdlegend.style.display = "none"
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetPageTitleText(sText)
	'		displays new page title text in title bar
	'
	'	Arguments:	sText	String. Text to display in title bar
	'
	sub SetPageTitleText(sText)
		bdtitletext.innerText = sText
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	SetWaitDisplayText(sWaitText)
	'		displays new text in wait display
	'
	'	Arguments:	sWaitText	String. Text to display in wait display
	'
	sub SetWaitDisplayText(sWaitText)
		bdwaitdisplaytext.innerText = sWaitText
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	HideWaitDisplay()
	'		hides wait display after slight delay
	'
	sub HideWaitDisplay()
		bdwaitdisplay.style.display = "none"
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ShowWaitDisplay()
	'		shows wait display
	'
	sub ShowWaitDisplay()
		bdwaitdisplay.style.display = "block"
	end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' task event handler routines
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	menu click handler to close task button menus and select item
	sub menu_onClick
		dim elSource
		set elSource = window.event.srcElement
		CloseMenu()
		if right(elSource.parentElement.id, 4) = "menu" and _
			inStr(elSource.className, "bdtaskdisabled") = 0 then doTask elSource
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	menu mouse over handler to cancel bubble for menu
	sub menu_onMouseOver
		window.event.cancelBubble = true
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	sets edit mode display for title and status bars and
	'						hides left navigation tree menu
	sub SetEditMode(bSet)
		' -- call routine in parent (bizdesk.asp)
		parent.focus()
		parent.SetEditMode(bSet)
		on error resume next
<%	if not bEditMode then
%>		parent.frames(0).EnableCategoryTabIndexes()
<%	else
%>		parent.frames(0).DisableCategoryTabIndexes()
<%	end if
%>	end sub

	' -- turn body scrolling off; scrolling will be done in content div
	document.body.scroll = "no"

<%
if not bEditMode then
%>	parent.g_bCloseAfterSave = false
<%
end if
%>
	' -- set edit mode
	SetEditMode <%= cInt(bEditMode) %>

'-->
</SCRIPT>
<div ID="bdnonclientarea" class="<% if bEditMode then %>bdeditmode<% end if %>">
	<div ID="bdstatusbar" NOWRAP CLASS="bdcolorbar">
		<TABLE CELLSPACING='0' CELLPADDING='0' WIDTH='100%'>
		<TR>
			<TD NOWRAP WIDTH='100%'>
				<NOBR ID="bdstatusloading" LANGUAGE="VBScript" ONMOUSEOVER="showTooltip">
					<img SRC="<%= g_sBDAssetRoot %>statusanimation.gif" ALIGN="absmiddle" HEIGHT='18' WIDTH='18'>
					<%= L_BDSSLoadingField_Text %>
				</NOBR>
				<NOBR ID="bdstatustext" LANGUAGE="VBScript" ONMOUSEOVER="showTooltip"><%= sStatusText %></NOBR>
			</TD>
			<TD ID="bdlegend" NOWRAP STYLE='VISIBILITY: hidden'>
				<NOBR>
<%				if SHOW_DEBUG_TEXT then
					dim sDebug, sDebugTitle
					sDebug = "r:[" & Request.TotalBytes & "]" & Request.QueryString.Count & "/" & Request.Form.Count & "&nbsp;&nbsp;" & "s:" & Session.Contents.Count & "/" & Session.StaticObjects.Count & "&nbsp;&nbsp;" & "a:" & Application.Contents.Count & "/" & Application.StaticObjects.Count & "&nbsp;&nbsp;"
					sDebugTitle = "Request:" & vbCr & "&nbsp;&nbsp;.TotalBytes=" & Request.TotalBytes & vbCR & "&nbsp;&nbsp;.QueryString.Count=" & Request.QueryString.Count & vbCR & "&nbsp;&nbsp;.Form.Count=" & Request.Form.Count & vbCr & vbCr & "Session:" & vbCr & "&nbsp;&nbsp;.Contents.Count=" & Session.Contents.Count & vbCr & "&nbsp;&nbsp;.StaticObjects.Count=" & Session.StaticObjects.Count & vbCr & vbCr & "Application:" & vbCr & "&nbsp;&nbsp;.Contents.Count=" & Application.Contents.Count & vbCr & "&nbsp;&nbsp;.StaticObjects.Count=" & Application.StaticObjects.Count
%>					<SPAN CLASS='bdDebug' TITLE='<%= sDebugTitle %>'><%= sDebug %></SPAN>
<%				else
%>					<%= g_imgRequired %>&nbsp;<%= L_BDSSLegendRequired_Text %>&nbsp;&nbsp;
					<%= g_imgInvalid %>&nbsp;<%= L_BDSSLegendInvalid_Text %>
<%				end if
%>				</NOBR>
			</TD>
		</TR>
		</TABLE>
	</div>

	<div ID="bdtitlebar" NOWRAP CLASS="bdcolorbar">
		<img SRC="<%= g_sBDAssetRoot %>bdtitleicon.gif" ALIGN="absmiddle" HEIGHT='20' WIDTH='20'>
		<span ID="bdtitletext" LANGUAGE="VBScript" ONMOUSEOVER="showTooltip">
			<%= sPageTitle %>
		</span>
<%		if SHOW_DEBUG_TEXT then
%>			<SPAN CLASS='bdDebug'><%= L_DebugMode_Text %>&nbsp;<SPAN ID='bd_d' TITLE='g_bDirty'>d</SPAN> <SPAN ID='bd_r' TITLE='g_bRequired'>r</SPAN> <SPAN ID='bd_v' TITLE='g_bValid'>v</SPAN> <SPAN STYLE='FONT-SIZE: 75%'>(<SPAN STYLE='COLOR: green; FONT-SIZE: 100%'><%= UCase(cBool(1)) %></SPAN>/<SPAN STYLE='COLOR: red; FONT-SIZE: 100%'><%= UCase(cBool(0)) %></SPAN>)</SPAN></SPAN>
<%		end if
%>	</div>
	<div ID="bdfindby" NOWRAP STYLE='DISPLAY: none'>
		<div ID='bdfindbytitlebar' NOWRAP>
			<TABLE CELLSPACING='0' CELLPADDING='0' WIDTH='100%' STYLE='TABLE-LAYOUT: fixed'>
			<COLGROUP><COL><COL WIDTH='33'></COLGROUP>
			<TR>
				<TD ID="bdfindbytitle" NOWRAP>
					<%= g_sFindByTitle %>
				</TD>
				<TD ID="bdfindbyclose" NOWRAP>
					<button ID='bdtaskbtnfindclose' TYPE='close' CLASS='bdtaskbutton' TITLE="Close find pane." LANGUAGE='VBScript' ONKEYDOWN='task_onKeyDown' ONCLICK='toggleFindBy()'>X</button>
				</TD>
			</TR>
			</TABLE>
		</div>
		<div ID='bdfindbybody' NOWRAP></div>
	</div>
	<div ID="bdtaskbar" NOWRAP>
		<TABLE CELLSPACING='0' CELLPADDING='0' WIDTH='100%'>
		<COLGROUP><COL><COL WIDTH='33'></COLGROUP>
		<TR>
			<TD ID="bdtaskarea" NOWRAP>
				<TABLE CELLSPACING='0' CELLPADDING='0' BORDER='0' STYLE='POSITION: relative'>
				<TR>
<%					InsertIconBtns()
%>				</TR>
				</TABLE>
			</TD>
			<TD ID="bdhelparea" NOWRAP>
<%				InsertHelpIconBtn()
%>			</TD>
		</TR>
		</TABLE>
	</div>
	<div id='bdwaitdisplay' class="waitDisplay" nowrap><%= "<img SRC='/widgets/statusanimation.gif' ALIGN='absmiddle' HEIGHT='36' WIDTH='36' STYLE='FILTER: invert'> <span id=bdwaitdisplaytext>" & L_BDSSLoading_Text & "</span>" %></div>
</div>
<script LANGUAGE="VBScript">
<!--
	option explicit

	<% if g_MSCSEnv = PRODUCTION then %>on error resume next<% end if %>
	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	mouse over handler to close task button menus
	sub taskWindow_onMouseOver
		CloseMenu()
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	page unload handler to display prompt
	sub taskWindow_onBeforeUnload()
		if g_bUnloading or elGetTaskBtn("save") is nothing then exit sub

		'if dirty display prompt
		if g_bDirty then
			window.event.returnValue = "<%= L_UnsavedChanges_Text %>"
		end if
		g_bUnloading = false
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	page load handler to display errors and auto exit after save
	sub taskWindow_onLoad()
		dim elBtnBack, nNavWidth
		if parent.g_bCloseAfterSave and <%= cInt(isEmpty(Session("MSCSBDError"))) %> then
			parent.g_bCloseAfterSave = false
			' -- save and exit chosen and no errors while saving
			set elBtnBack = elGetTaskBtn("back")
			if not elBtnBack is nothing then
				if not elBtnBack.disabled then elBtnBack.click()
			end if
		end if
		parent.g_bCloseAfterSave = false

		if left(parent.document.all.tags("FRAMESET")(0).cols, 2) <> "0," and _
			parent.frames(0).document.readyState = "complete" then
			nNavWidth = parent.frames(0).document.body.clientWidth
			if nNavWidth > 5 then parent.g_nNavWidth = nNavWidth
		end if
<%
		dim dBDError, slBDError
		if not isEmpty(Session("MSCSBDError")) then
			set slBDError = Session("MSCSBDError")
			for each dBDError in slBDError
%>
	showErrorDlg "<%= dBDError("errortitle") %>", "<%= dBDError("friendlyerror") %>", "<%= dBDError("errordetails") %>", <%= dBDError("errortype") %>
<%			next
			Session("MSCSBDError") = empty
		end if
%>
		if not elGetTaskBtn("new") is nothing then EnableTask("new")
		if not elGetTaskBtn("find") is nothing then EnableTask("find")
		hideStatusAnimation()
	end sub

	' -- attach event handlers
	window.attachEvent "onload", GetRef("taskWindow_OnLoad")
	window.attachEvent "onbeforeunload", GetRef("taskWindow_onBeforeUnload")
	document.attachEvent "onmouseover", GetRef("taskWindow_onMouseOver")

	' -- set dynamic styles for task bar, status bar, find by area, and title bar
	bdstatusbar.style.setExpression "top", "document.body.clientHeight - 20"
	bdfindby.style.setExpression "width", "document.body.clientWidth - 5"
	bdfindbybody.style.backgroundImage = "url(<%= g_sBDAssetRoot %>imgFind.gif)"
	bdwaitdisplay.style.setExpression "top", "(document.body.clientHeight/2) - bdwaitdisplay.offsetHeight"
	bdwaitdisplay.style.setExpression "left", "(document.body.clientWidth/2) - (bdwaitdisplay.offsetWidth/2)"
<%	if SHOW_DEBUG_TEXT then
%>		bd_d.style.setExpression "color", "g_bDirty ? 'green' : 'red'", "jscript"
		bd_r.style.setExpression "color", "g_bRequired ? 'green' : 'red'", "jscript"
		bd_v.style.setExpression "color", "g_bValid ? 'green' : 'red'", "jscript"
<%	end if
%>'-->
</script>
<%
end sub
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	FRAMEWORK ROUTINES FOR EVERY ACTION PAGE
%>
<script LANGUAGE="VBScript">
<!--
	Option Explicit

	dim winHelpWindow

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' ERROR HANDLING
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	const L_XMLErrorTitle_ErrorMessage = "XML Error:"
	const L_XMLErrorErrorCode_ErrorMessage = "Error Code:"
	const L_XMLErrorFilePos_ErrorMessage = "File Pos:"
	const L_XMLErrorLine_ErrorMessage = "Line:"
	const L_XMLErrorLinePos_ErrorMessage = "Line Pos:"
	const L_XMLErrorReason_ErrorMessage = "Reason:"
	const L_XMLErrorSourceText_ErrorMessage = "Source Text:"
	const L_XMLErrorURL_ErrorMessage = "URL:"
	const L_ScriptErrorTitle_ErrorMessage = "Script Error:"
	const L_ScriptErrorNumber_ErrorMessage = "Number:"
	const L_ScriptErrorDescription_ErrorMessage = "Description"
	const L_ScriptErrorSource_ErrorMessage = "Source:"
	const L_ScriptErrorHelpFile_ErrorMessage = "Help File:"
	const L_ScriptErrorHelpContext_ErrorMessage = "Help Context:"

	' -- error types to determine which icon displays in error dialog
	const ERROR_ICON_ALERT = 0
	const ERROR_ICON_CRITICAL = 1
	const ERROR_ICON_QUESTION = 2
	const ERROR_ICON_INFORMATION = 3

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ShowErrorDlg(sErrorTitle, sFriendlyError, sErrorDetails, nErrorType)
	'		shows error dialog
	'
	'	Arguments:	sErrorTitle			String. error dialog title
	'				sFriendlyError		String. client error message
	'				sErrorDetails		String. error details that will be shown in
	'									expandable "more details" window
	'				nErrorType			Integer. error number for which icon to show
	'									One of:
	'										ERROR_ICON_ALERT
	'										ERROR_ICON_CRITICAL
	'										ERROR_ICON_QUESTION
	'										ERROR_ICON_INFORMATION
	'
	sub ShowErrorDlg(sErrorTitle, sFriendlyError, sErrorDetails, nErrorType)
		dim dictArgs
		Set dictArgs = CreateObject("Scripting.Dictionary")
		dictArgs.Item("errorTitle") =		sErrorTitle
		dictArgs.Item("friendlyError") =	sFriendlyError
		dictArgs.Item("errorDetails") =		sErrorDetails
		dictArgs.Item("errorType") =		nErrorType
		
		showModalDialog "<%= g_sBDIncludeRoot %>Dlg_Error.asp", dictArgs, "status:no;help:no"
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	sErrorDetails = sGetXMLError(oXMLDocParseError)
	'		returns error string populated from error object
	'
	'	Arguments:	oXMLDocParseError	Object. XML document.parseError object
	'
	'	Returns:						String. error string with object properties
	'									displayed in an HTML table
	'
	function sGetXMLError(oXMLDocParseError)
		dim sText
		sText = ""
		sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
		sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_XMLErrorTitle_ErrorMessage & "</TH></TR>"
		sText = sText & "<TR><TH>" & L_XMLErrorErrorCode_ErrorMessage & "</TH><TD>" &	oXMLDocParseError.errorCode & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_XMLErrorFilePos_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.filePos & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_XMLErrorLine_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.line & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_XMLErrorLinePos_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.linePos & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_XMLErrorReason_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.reason & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_XMLErrorSourceText_ErrorMessage & "</TH><TD>" &	oXMLDocParseError.srcText & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_XMLErrorURL_ErrorMessage & "</TH><TD>" &			oXMLDocParseError.URL & "</TD></TR>"
		sText = sText & "</TABLE>"
		sGetXMLError = sText
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	sErrorDetails = sGetScriptError(oErr)
	'		returns error string populated from error object
	'
	'	Arguments:	oErr	Object. VBScript Err object
	'
	'	Returns:			String. error string with object properties
	'						displayed in an HTML table
	'
	function sGetScriptError(oErr)
		dim sText
		sText = ""
		sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
		sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_ScriptErrorTitle_ErrorMessage & "</TH></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorNumber_ErrorMessage & "</TH><TD>0x" &		Hex(oErr.number) & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorDescription_ErrorMessage & "</TH><TD>" &	oErr.description & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorSource_ErrorMessage & "</TH><TD>" &		oErr.source & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorHelpFile_ErrorMessage & "</TH><TD>" &		oErr.helpFile & "</TD></TR>"
		sText = sText & "<TR><TH>" & L_ScriptErrorHelpContext_ErrorMessage & "</TH><TD>" &	oErr.helpContext & "</TD></TR>"
		sText = sText & "</TABLE>"
		sGetScriptError = sText
	end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' class routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	AddClass(elItem, sNewClass)
	'		adds the new class to the className string for the element
	'
	'	Arguments:	elItem		Element. Element to change class for
	'				sNewClass	String. New class name to add
	'
	sub AddClass(elItem, sNewClass)
		' -- exit if already in class
		if inStr(elItem.className, sNewClass) > 0 then exit sub
		' -- add class to classname
		if Len(elItem.className) > 1 then
			elItem.className = elItem.className & " " & sNewClass
		else
			elItem.className = sNewClass
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	RemoveClass(elItem, sOldClass)
	'		removes the old class from the className string for the element
	'
	'	Arguments:	elItem		Element. Element to change class for
	'				sOldClass	String. Old class name to remove
	'
	sub RemoveClass(elItem, sOldClass)
		' -- exit if not in class
		if inStr(elItem.className, sOldClass) = 0 then exit sub
		' -- remove class from classname and remove extra spaces if any
		elItem.className = Trim(Replace(Replace(elItem.className, sOldClass, ""), "  ", " "))
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	ReplaceClass(elItem, sOldClass, sNewClass)
	'		replaces the old class with the new class in the className string 
	'		for the element. If old class not in string then new class is not added.
	'
	'	Arguments:	elItem		Element. Element to change class for
	'				sNewClass	String. New class name
	'				sOldClass	String. Old class name
	'
	sub ReplaceClass(elItem, sOldClass, sNewClass)
		' -- exit if not in class
		if inStr(elItem.className, sOldClass) = 0 then exit sub
		' -- replace old class in classname with new class
		elItem.className = Replace(elItem.className, sOldClass, sNewClass)
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	bValue = bHasClass(elItem, sClass)
	'		returns true if the element's className string contains the class
	'
	'	Arguments:	elItem	Element. Element to check class for
	'				sClass	String. Class name
	'
	'	Returns:			Boolean. True if found, false if not
	'
	function bHasClass(elItem, sClass)
		' -- return true if class found in classname
		bHasClass = CBool(inStr(elItem.className, sClass) <> 0)
	end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' date conversion routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nDay = nGetDay(sValue)
	'		returns integer for day in formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Day value or -1 if not found
	'
	function nGetDay(sValue)
		dim nDayIndex
		nDayIndex = <%= Application("MSCSDayIndex") %>
		nGetDay = nGetDatePart(sValue, nDayIndex)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nMonth = nGetMonth(sValue)
	'		returns integer for m onthin formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Month value or -1 if not found
	'
	function nGetMonth(sValue)
		dim nMonthIndex
		nMonthIndex = <%= Application("MSCSMonthIndex") %>
		nGetMonth = nGetDatePart(sValue, nMonthIndex)
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nYear = nGetYear(sValue)
	'		returns integer for year in formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Year value or -1 if not found
	'
	function nGetYear(sValue)
		dim nYearIndex, nYear
		nYearIndex = <%= Application("MSCSYearIndex") %>
		nYear = nGetDatePart(sValue, nYearIndex)
		if nYear > 3000 then
			nGetYear = -1
		else
			nGetYear = nYear
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	gets date part based on index and separator
	function nGetDatePart(sValue, nPartIndex)
		dim sSeparator, aDate, sDatePart
		sSeparator = "<%= Application("MSCSDateSeparator") %>"
		nGetDatePart = -1
		if not isNull(sValue) then
			'strip off time part, if any
			sDatePart = split(sValue, " ")(0)
			aDate = split(sDatePart, sSeparator)
			if Ubound(aDate) = 2 then
				if aDate(nPartIndex) <> "" and isNumeric(aDate(nPartIndex)) then
					nGetDatePart = aDate(nPartIndex)
				end if
			end if
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	dtDate = dtGetDate(sValue)
	'		returns date variant for formatted value or current date if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Date. Date for value or today's date if not found
	'
	function dtGetDate(sValue)
		dim nYear, nMonth, nDay
		nYear = nGetYear(sValue)
		nMonth = nGetMonth(sValue)
		nDay = nGetDay(sValue)
		dtGetDate = date()
		if nYear > -1 and nMonth > -1 and nDay > -1 then
			dtGetDate = DateSerial(nYear, nMonth, nDay)
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	sDateText = sGetFormattedDate(nYear, nMonth, nDay)
	'		returns string date for day, month, and year in site's default locale
	'
	'	Arguments:	nYear	Integer. integer year
	'				nMonth	Integer. integer month
	'				nDay	Integer. integer day
	'
	'	Returns:			String. Date for day, month, and year in site's default locale
	'
	function sGetFormattedDate(nYear, nMonth, nDay)
		dim sMSCSDateFormat
		sMSCSDateFormat = "<%= Application("MSCSDateFormat") %>"
		sGetFormattedDate = ""
		if nYear > -1 and nMonth > -1 and nDay > -1 then
			sMSCSDateFormat = replace(sMSCSDateFormat, "yyyy", nYear)
			sMSCSDateFormat = replace(sMSCSDateFormat, "mm", nMonth)
			sMSCSDateFormat = replace(sMSCSDateFormat, "dd", nDay)
			sGetFormattedDate = sMSCSDateFormat
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	bValue = bIsDate(sValue)
	'		returns true if string is valid date formatted in site's default locale
	'
	'	Arguments:	sValue	String. string value to validate
	'
	'	Returns:			Boolean. true if valid date, false if not
	'
	function bIsDate(sValue)
		dim aMonthDays, nYear, nMonth, nDay
		' -- array of muber of days in month for day range checking
		aMonthDays = Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
		' -- assume ok unless find different
		bIsDate = true
		' -- get year, month and day from value if possible (-1 if invalid)
		nYear = nGetYear(sValue)
		nMonth = nGetMonth(sValue)
		nDay = nGetDay(sValue)
		if not (nYear > -1 and nMonth > -1 and nDay > -1) then
			' -- year, month, or day not valid
			bIsDate = false
		else
			' -- cast as integer
			nYear = CInt(nYear)
			nMonth = CInt(nMonth)
			nDay = CInt(nDay)

			if nYear < 50 then
				' -- assume 0-49 is 2000-2049
				nYear = nYear + 2000
			elseif nYear < 100 then
				' -- assume 50-99 is 1950-1999
				nYear = nYear + 1900
			end if

			' -- set number of days in Feb for leap years
			If nYear Mod 4 = 0 Then
				If nYear Mod 100 = 0 Then
					If nYear Mod 400 = 0 Then
						' -- century only a leap year if divisible by 400
						aMonthDays(1) = 29
					End If
				Else
					' -- non-century divisible by 4 is always a leap year
					aMonthDays(1) = 29
				End If
			End If

			if nMonth < 1 or nMonth > 12 then
				' -- month outside acceptable range
				bIsDate = false
			elseif nDay < 1 or nDay > aMonthDays(nMonth - 1) then
				' -- day outside acceptable range
				bIsDate = false
			end if
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	OpenHelp(strHelpFile)
	'		opens a help window displaying the specified help topic file (empty string is default)
	'
	'	Arguments:	strHelpFile	String. page name of help topic (empty string displays entry page)
	'
	'	Returns:				none
	'
	sub OpenHelp(strHelpFile)
		if typename(winHelpWindow) = "HTMLWindow2" then winHelpWindow.close
		set winHelpWindow = window.open("<%= g_sBizDeskRoot %>docs/default.asp?helptopic=" & strHelpFile, "winHelpWindow", _
						 "height=500,width=700,status=no,toolbar=yes,menubar:no,resizable=yes")
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	sText = sFormatString(sFormat, aArgs)
	'		constructs a string from a format string and substrings (for localization)
	'
	'	Arguments:	sFormat		String. Format string containing numbered arg 
	'							insertion points like "%1", "%2", etc.
	'				aArgs		Array. Array of variants to replace the "%#" string
	'							cooresponding to its index 
	'
	'	Returns:				String. Formatted as above.
	'
	Function sFormatString(sFormat, aArgs)
		dim nArg
		for nArg = LBound(aArgs) to UBound(aArgs)
			sFormat = Replace(sFormat, "%" & nArg + 1, aArgs(nArg))
		next
		sFormatString = sFormat
	End Function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	xmlSiteTermValue = xmlGetSiteTermsValue(xmlSiteTermNode, sSelected)
	'		displays a dialog so the user can choose a site term. Returns the selected
	'		attribute node from the site term the property node
	'
	'	Arguments:	xmlSiteTermNode	xmlNode. Site term property node 
	'				sSelected		String. display name for node initially selected
	'
	'	Returns:					xmlNode. Selected site term attribute node
	'
	function xmlGetSiteTermsValue(xmlSiteTermNode, sSelected)
		dim sHREF, dArgs

		set dArgs = CreateObject("Scripting.Dictionary")
		sitetermsample.async = false
		dArgs.Add "SiteTermNode", xmlSiteTermNode
		dArgs.Add "Selected", sSelected
		sHREF = "/widgets/edithtc/sitetermDlg.htm"
		set xmlGetSiteTermsValue = window.showModalDialog(sHREF, dArgs, "dialogHeight:260px;dialogWidth:400px;status:no;help:no")
	end function
'-->
</script>
<script LANGUAGE="VBScript">
<!--
	option explicit

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' event routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	context menu handler to turn off context menus
	sub doc_onContextMenu()
		' -- disallow default context menu unless in entry fields
		if not (window.event.srcElement.tagName = "INPUT" or window.event.srcElement.tagName = "TEXTAREA") then
			window.event.returnValue = <%= ALLOW_CONTEXT_MENUS %>
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	key down handler to turn off all but editing keys
	sub doc_onKeyDown()
		dim evt, elSource
		set evt = window.event
		set elSource = evt.srcElement
		if evt.ctrlKey then
			select case evt.keyCode
				case	L_KeyCodeShiftedCopy_Number, L_KeyCodeCopy_Number, _
						L_KeyCodeShiftedPaste_Number, L_KeyCodePaste_Number, _
						L_KeyCodeShiftedCut_Number, L_KeyCodeCut_Number
					' -- allow these keys
				case else
					' -- disallow all other control key combos except in inputs
					if not (elSource.tagName = "INPUT" or elSource.tagName = "TEXTAREA") then
						evt.returnValue = false
					end if
			end select
		else
			select case evt.keyCode
				case	KEYCODE_BACKSPACE
					if not (elSource.tagName = "INPUT" or elSource.tagName = "TEXTAREA") then
						evt.returnValue = false
					end if
				case	KEYCODE_LEFT, KEYCODE_UP, KEYCODE_RIGHT, KEYCODE_DOWN
					' -- disallow these keys
					if not (elSource.tagName = "INPUT" or _
						elSource.tagName = "TEXTAREA" or _
						elSource.tagName = "SELECT") then
						evt.returnValue = false
					end if
				case	KEYCODE_F1
					' -- open help
					OpenHelp ""
					' -- disallow all FN keys
					evt.returnValue = false
					evt.cancelBubble = true
				case	KEYCODE_F2, KEYCODE_F3, KEYCODE_F4, KEYCODE_F5, KEYCODE_F6, _
						KEYCODE_F7, KEYCODE_F8, KEYCODE_F9, KEYCODE_F10, KEYCODE_F11, KEYCODE_F12
					' -- disallow all FN keys
					evt.returnValue = false
					evt.cancelBubble = true
			end select
		end if
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	drag start handler to turn off dragging
	sub doc_onDragStart()
		' -- disable dragging
		window.event.returnValue = false
	end sub

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	BizDesk Framework:	select start handler to turn off selection except in inputs
	sub doc_onSelectStart()
		' -- disallow selection of text on the page except in edit boxes
		Dim elSource
		set elSource = window.event.srcElement
		if elSource.tagName = "TEXTAREA" or _
			elSource.tagName = "INPUT" then
			if (elSource.readonly or elSource.disabled) then window.event.returnValue = false
		else
			window.event.returnValue = false
		end if
	end sub

	' -- BizDesk keycodes for filtering:
	const L_KeyCodeShiftedCopy_Number	= 67	'shifted key code for copy: C
	const L_KeyCodeCopy_Number			= 86	'key code for cut: c
	const L_KeyCodeShiftedPaste_Number	= 88	'shifted key code for paste: V
	const L_KeyCodePaste_Number			= 99	'key code for paste: v
	const L_KeyCodeShiftedCut_Number	= 118	'shifted key code for cut: X
	const L_KeyCodeCut_Number			= 120	'key code for cut: x
	const KEYCODE_LEFT	= 37
	const KEYCODE_UP	= 38
	const KEYCODE_RIGHT	= 39
	const KEYCODE_DOWN	= 40
	const KEYCODE_BACKSPACE	= 8
	const KEYCODE_F1 = 112
	const KEYCODE_F2 = 113
	const KEYCODE_F3 = 114
	const KEYCODE_F4 = 115
	const KEYCODE_F5 = 116
	const KEYCODE_F6 = 117
	const KEYCODE_F7 = 118
	const KEYCODE_F8 = 119
	const KEYCODE_F9 = 120
	const KEYCODE_F10 = 121
	const KEYCODE_F11 = 122
	const KEYCODE_F12 = 123

	' -- BizDesk arrows:
	const LEFT_ARROW	= "3"
	const RIGHT_ARROW	= "4"
	const UP_ARROW		= "5"
	const DOWN_ARROW	= "6"

	'xml node types
	const NODE_ELEMENT					= 1
	const NODE_ATTRIBUTE				= 2
	const NODE_TEXT						= 3
	const NODE_CDATA_SECTION			= 4
	const NODE_ENTITY_REFERENCE			= 5
	const NODE_ENTITY					= 6
	const NODE_PROCESSING_INSTRUCTION	= 7
	const NODE_COMMENT					= 8
	const NODE_DOCUMENT					= 9
	const NODE_DOCUMENT_TYPE			= 10
	const NODE_DOCUMENT_FRAGMENT		= 11
	const NODE_NOTATION					= 12

	' -- attach document event handlers
	with window.document
		.attachEvent "oncontextmenu", getRef("doc_onContextMenu")
		.attachEvent "onkeydown", getRef("doc_onKeyDown")
		.attachEvent "ondragstart", getRef("doc_onDragStart")
		.attachEvent "onselectstart", getRef("doc_onSelectStart")
	end with
'-->
</script>
