<!--#INCLUDE FILE='BDHeader.asp' -->
<!--#INCLUDE FILE='SharedStrings.asp' -->
<!--#INCLUDE FILE='BizDeskStrings.asp' -->
<!--#INCLUDE FILE='BizDeskPaths.asp' -->
<!--#INCLUDE FILE='ASPUtil.asp' -->
<%
const L_DialogHeight_Style = 120
const L_DialogWidth_Style = 400
%>
<HTML>
<HEAD>
    <TITLE><%= L_SaveChanges_HTMLTitle %></TITLE>
	<STYLE>
		HTML
		{
		    WIDTH: <%= L_DialogWidth_Style %>px;
		    HEIGHT: <%= L_DialogHeight_Style %>px;
		}
		BODY
		{
		    PADDING: 0;
		    CURSOR: default;
		    BACKGROUND-COLOR: #cccccc;
			COLOR: black;
			MARGIN-TOP: 5px;
			MARGIN-LEFT: 0;
			MARGIN-RIGHT: 0;
			MARGIN-BOTTOM: 0;
		    FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 8pt;
		    TEXT-ALIGN: center;
		}
		#bdclienterror
		{
			WIDTH: 100%;
		    FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 8pt;
			HEIGHT: 40px;
			TEXT-ALIGN: left;
			PADDING: 0;
			MARGIN-TOP: 0;
			MARGIN-LEFT: 0;
			MARGIN-RIGHT: 0;
			MARGIN-BOTTOM: 10px;
			OVERFLOW: auto;
			DISPLAY: inline;
		}
		#bderroricon
		{
			MARGIN-TOP: 6px;
			MARGIN-LEFT: 16px;
			MARGIN-RIGHT: 12px;
			MARGIN-BOTTOM: 6px;
		}
		INPUT, BUTTON
		{
			WIDTH: 7em;
			BACKGROUND-COLOR: #cccccc;
			FONT-SIZE: 8pt
		}
	</STYLE>
	<SCRIPT LANGUAGE='VBScript'>
	<!--
		const L_DefaultConfirm_ErrorMessage = "Save changes before exiting?"

		sub closeWindow(sResponse)
			window.returnValue = sResponse
			window.event.returnValue = false
			window.close()
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY SCROLL='no' LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancelbtn.click">

<TABLE width='100%'>
<TR>
	<TD VALIGN='top'>
		<IMG ID='bderroricon' SRC='<%= g_sBDAssetRoot %>errQuestion.gif'
			ALIGN='left' HEIGHT='32' WIDTH='32'>
	</TD>
	<TD width='100%'>
		<DIV ID='bdclienterror'>
			<SCRIPT LANGUAGE='VBScript'>
			<!--
				dim g_sTextConfirm

				g_sTextConfirm = window.dialogArguments
				if g_sTextConfirm <> "" then
					document.write  g_sTextConfirm
				else
					document.write L_DefaultConfirm_ErrorMessage
				end if
			'-->
			</SCRIPT>
		</DIV>
	</TD>
</TR>
<TR>
	<TD COLSPAN='2'>
	<TABLE width='100%'>
	<TR>
		<TD width='100%'></TD>
		<TD>
			<INPUT ID='bdyesbtn' TYPE='SUBMIT' CLASS='bdbutton'
			       VALUE='<%= L_BDSSYes_Button %>'
			       LANGUAGE="VBScript" ONCLICK='closeWindow("yes")'>
		</TD>
		<TD>
			<BUTTON ID='bdnobtn' CLASS='bdbutton'
				LANGUAGE='VBScript' ONCLICK='closeWindow("no")'>
				<%= L_BDSSNo_Button %>
			</BUTTON>
		</TD>
		<TD>
			<BUTTON ID='bdcancelbtn' CLASS='bdbutton'
				LANGUAGE='VBScript' ONCLICK='closeWindow("cancel")'>
				<%= L_BDSSCancel_Button %>
			</BUTTON>
		</TD>
	</TR>
	</TABLE>
</TD>
</TR>
</TABLE>


</BODY>
</HTML>

