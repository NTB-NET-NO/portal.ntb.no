<!--#INCLUDE FILE='BDHeader.asp' -->
<!--#INCLUDE FILE='BizDeskStrings.asp' -->
<!--#INCLUDE FILE='SharedStrings.asp' -->
<!--#INCLUDE FILE='BizDeskPaths.asp' -->
<!--#INCLUDE FILE='ASPUtil.asp' -->
<%
const L_DialogHeight_Style = 120
const L_DialogWidth_Style = 400

Dim SType
sType = Request.QueryString("type")
%>
<HTML>
<HEAD>
<%	if sType = "OKCancel" then
%>    <TITLE><%= L_DiscardChanges_HTMLTitle %></TITLE>
<%	else	' sType = "OKOnly"
%>    <TITLE><%= L_CannotSave_HTMLTitle %></TITLE>
<%	end if
%>	<STYLE>
		HTML
		{
		    WIDTH: <%= L_DialogWidth_Style %>px;
		    HEIGHT: <%= L_DialogHeight_Style %>px;
		}
		BODY
		{
		    PADDING: 0;
		    CURSOR: default;
		    BACKGROUND-COLOR: #cccccc;
			COLOR: black;
			MARGIN-TOP: 5px;
			MARGIN-LEFT: 0;
			MARGIN-RIGHT: 0;
			MARGIN-BOTTOM: 0;
		    FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 8pt;
		    TEXT-ALIGN: center;
		}
		#bdclienterror
		{
		    FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 8pt;
			HEIGHT: 40px;
			PADDING: 0;
			MARGIN-TOP: 0;
			MARGIN-LEFT: 0;
			MARGIN-RIGHT: 0;
			MARGIN-BOTTOM: 10px;
			OVERFLOW: auto;
		}
		#bderroricon
		{
			MARGIN-TOP: 6px;
			MARGIN-LEFT: 16px;
			MARGIN-RIGHT: 12px;
			MARGIN-BOTTOM: 6px;
		}
		INPUT, BUTTON
		{
			WIDTH: 7em;
			BACKGROUND-COLOR: #cccccc;
			FONT-SIZE: 8pt
		}
	</STYLE>
	<SCRIPT LANGUAGE="VBScript">
	<!--
		sub closeWindow(sResponse)
			window.returnValue = sResponse
			window.event.returnValue = false
			window.close()
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY SCROLL='no'<%	if sType = "OKCancel" then
%> LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancelbtn.click"<% end if %>>

<TABLE width='100%'>
<TR>
	<TD VALIGN='top'>
		<IMG ID='bderroricon' SRC='<%= g_sBDAssetRoot %>errAlert.gif'
			ALIGN='left' HEIGHT='32' WIDTH='32'>
	</TD>
	<TD width='100%'>
		<DIV ID='bdclienterror'>
			<SCRIPT LANGUAGE='VBScript'>
			<!--
				option explicit

				dim g_sTextWarn

				g_sTextWarn = window.dialogArguments
				if g_sTextWarn <> "" then
					document.write  g_sTextWarn
				else
<%			if sType = "OKCancel" then
%>					document.write "<%= replace(L_DiscardChanges_ErrorMessage, """", """""") %>"
<%			else	' sType = "OKOnly"
%>					document.write "<%= replace(L_CannotSave_ErrorMessage, """", """""") %>"
<%			end if
%>				end if
			'-->
			</SCRIPT>
		</DIV>
	</TD>
</TR>
<TR>
	<TD COLSPAN='2'>
	<TABLE width='100%'>
	<TR>
		<TD width='100%'></TD>
		<TD>
			<INPUT ID='bdokbtn' TYPE='SUBMIT' CLASS='bdbutton'
			       VALUE='<%= L_BDSSOK_Button %>'
			       LANGUAGE="VBScript" ONCLICK='closeWindow("yes")'>
		</TD>
<%	if sType = "OKCancel" then
%>		<TD>
			<BUTTON ID='bdcancelbtn' CLASS='bdbutton'
				LANGUAGE='VBScript' ONCLICK='closeWindow("cancel")'>
				<%= L_BDSSCancel_Button %>
			</BUTTON>
		</TD>
<%	end if
%>	</TR>
	</TABLE>
</TD>
</TR>
</TABLE>


</BODY>
</HTML>

