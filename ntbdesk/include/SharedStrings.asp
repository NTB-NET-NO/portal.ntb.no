<%
'LOC: This file	contains all localizable strings shared accross BizDesk modules
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

'tooltips
const L_BDSSRequiredValue_ToolTip = "A value must be entered for this field"
const L_BDSSInvalidValue_ToolTip = "An invalid value must be corrected"

'misc
const L_BDSSViewBy_Text = "View:"
const L_BDSSResults_Text = "Results:"
const L_BDSSLegendRequired_Text = "= required"
const L_BDSSLegendInvalid_Text = "= not valid"
Const L_BDSSLoadingProperties_Text = "Loading properties, please wait..."
Const L_BDSSLoadingXProperties_Text = "Loading %1 properties, please wait..."
const L_BDSSLoadingField_Text = "loading..."
Const L_BDSSLoadingList_Text = "Loading list, please wait..."
Const L_BDSSLoading_Text = "Loading, please wait..."
Const L_BDSSSaving_Text = "Saving, please wait..."

'find pane
const L_BDSSFilter_Text = "<b>Filtered View</b> (use the Find command to modify the filter criteria)"
const L_BDSSFindNow_Button = "Find Now"
const L_BDSSSearchBy_Text = "Search on:"

'dialogs
const L_BDSSSaveChangesToX_Message = "Save changes to %1?"
const L_BDSSSaveChangesToXX_Message = "Save changes to %1 %2?"
Const L_BDSSDelete_Text = "Delete?"
const L_BDSSDeleteX_Message = "Delete %1?"
const L_BDSSDeleteThisX_Message = "Delete this %1?"
const L_BDSSDeleteXX_Message = "Delete %1: %2?"
Const L_BDSSRestore_Text = "Restore?"
const L_BDSSRestoreX_Message = "Restore %1?"
const L_BDSSRestoreThisX_Message = "Restore this %1?"
const L_BDSSRestoredXX_Message = "Restore %1: %2?"
const L_BDSSVAlidationErrorTitle_Text = "Validation Error"

'buttons
const L_BDSSOK_Button = "OK"
const L_BDSSAdd_Button = "Add"
const L_BDSSEdit_Button = "Edit"
const L_BDSSYes_Button = "Yes"
const L_BDSSNo_Button = "No"
const L_BDSSCancel_Button = "Cancel"
const L_BDSSClose_Button = "Close"
const L_BDSSDone_Button = "Done"
const L_BDSSSave_Button = "Save"
const L_BDSSReset_Button = "Reset"
const L_BDSSBrowse_Button = "Browse"
const L_BDSSSelectAll_Button = "Select All"
const L_BDSSDeselectAll_Button = "Deselect All"
const L_BDSSShowDetails_Text = "Show Details"
const L_BDSSHideDetails_Text = "Hide Details"
Const L_BDSSAddArrow_Button = "Add &gt;"
Const L_BDSSArrowRemove_Button = "&lt; Remove"

'page titles
const L_BDSSXcolonX_HTMLTitle = "%1: %2"
const L_BDSSXcolonXOpened_HTMLTitle = "%1: %2 opened"
const L_BDSSXcolonNew_HTMLTitle = "%1: New"

'confirmation messages
const L_BDSSSelectedXX_StatusBar = "Selected %1 %2"
const L_BDSSXSelectedX_StatusBar = "%1 selected: %2"
const L_BDSSNothingSelected_Text = "Nothing selected"
const L_BDSSNoXMatchThisSearch_StatusBar = "No %1 match this search"
const L_BDSSOneXMatchThisSearch_StatusBar = "1 %1 found to match your search"
const L_BDSSXXFoundMatchThisSearch_Text = "%1 %2 found to match your search"
const L_BDSSDeletedX_StatusBar = "Deleted %1"
const L_BDSSDeletedXX_StatusBar = "Deleted %1: %2"
const L_BDSSUnableToDeleteX_StatusBar = "Unable to delete: %1"
const L_BDSSUnableToDeleteXX_StatusBar = "Unable to delete %1: %2"
const L_BDSSSavedX_StatusBar = "Saved %1"
const L_BDSSSavedXX_StatusBar = "Saved %1: %2"
const L_BDSSUnableToSaveX_StatusBar = "Unable to save: %1"
const L_BDSSUnableToSaveXX_StatusBar = "Unable to save %1: %2"
const L_BDSSRestoredX_StatusBar = "Restored %1"
const L_BDSSRestoredXX_StatusBar = "Restored %1: %2"
const L_BDSSUnableToRestoreX_StatusBar = "Unable to restore: %1"
const L_BDSSUnableToRestoreXX_StatusBar = "Unable to restore %1: %2"
%>
