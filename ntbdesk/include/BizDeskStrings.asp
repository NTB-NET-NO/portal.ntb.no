<%
'LOC: This file	contains all localizable strings for the BizDesk container page	(default.asp)
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

'debug strings (only display when SHOW_DEBUG_TEXT set)
const L_DebugMode_Text = "DEBUG"
const L_DebugModuleError_Text = "ERROR"
const L_DebugModuleFiltered_Text = "FILTERED"

' -- Default.asp
const L_MSCommerceServerClientSetup_HTMLTitle = "Microsoft Commerce Server 2000 Business Desk Client Setup"

' -- ClientSetup.asp
const L_SitenameBizDesk_Text = "%1 Business Desk (%2)"

' -- BizDesk.asp
const L_RequiresIE55_ErrorMessage = "Microsoft Commerce Server Business Desk requires Internet Explorer 5.5 or later."
const L_MustInstallClient_ErrorMessage = "Microsoft Commerce Server Business Desk must be run from the installed shortcut."
const L_ErrorsInAppLoad_ErrorMessage = "Errors occurred while loading the Business Desk application."
const L_UnableToLoadBizDeskXML_ErrorMessage = "Microsoft Commerce Server Business Desk is incorrectly configured. (Unable to load bizdesk.xml). Please contact your system administrator."

' -- NavTree.asp
const L_ExpandCollapseCategory_ToolTip = "Display or hide the modules in this category"
const L_BizDeskMicrosoftCommerceServer_HTMLTitle = "Business Desk: %1-%2:%3 - Microsoft Commerce Server"
const L_AboutCommerceServerBizDesk_Tooltip = "About Microsoft Commerce Server 2000 Business Desk"
const L_LogoShowAbout_ToolTip = "Show About information"
const L_ScrollUp_ToolTip = "Scroll up"
const L_ScrollDown_ToolTip = "Scroll down"
const L_YourBrowserDoesNotSupportInlineFrames_Message = "Your browser does not support inline frames. To view this document correctly, you'll need to view it using Internet Explorer 5.0."
const L_CopyrightMicrosoftCompanyName_Text = "Microsoft Corporation"
const L_Copyright19992000MicrosoftCorporation_Text = "&copy; 1999-2000 Microsoft Corporation"
const L_VisitMicrosoftCorporationWebSite_Tooltip = "Visit Microsoft Corporation web site"
const L_WWWMicrosoftCom_Text = "http://www.microsoft.com/"
const L_BizDeskIncorrectlyConfigured_ErrorMessage = "Microsoft Commerce Server Business Desk is incorrectly configured. (No modules to display)"
const L_TheFollowingErrorsFound_ErrorMessage = "The following errors were found when loading the application:"

' -- Welcome.asp
const L_WelcomePage_Text = "Commerce Server: Welcome to Business Desk"
const L_WelcomeStatus_Text = "F1 to view help on working with Commerce Business Desk"
const L_CommerceServerBizDesk_Tooltip = "Microsoft Commerce Server 2000 Business Desk"
const L_TimeBombWarn_Text = ""

' -- include/ActionPageUtil.asp
const L_BDFindBy_Text = "Find"
const L_ViewHelpForPage_Tooltip = "View help for this section"
const L_TaskTooltipAccesskey_Tooltip = "%1 (ALT-%2)"	'shows tooltip text and accelerator for task
const L_BizDeskReloading_Text = "Commerce Business Desk has been restarted. Reloading..."
const L_BizDeskReloadingDialog_Text = "Commerce Business Desk has been restarted. Please close and restart the client application."
const L_UnsavedChanges_Text = "You have unsaved changes."

' -- include/dlg_Confirm.asp
const L_SaveChanges_HTMLTitle = "Business Desk: Changes not saved"

' -- include/dlg_Warn.asp
const L_CannotSave_HTMLTitle = "Business Desk: Cannot save"
const L_DiscardChanges_HTMLTitle = "Business Desk: Discard Changes?"
const L_CannotSave_ErrorMessage = "Cannot save until all required fields have values and fields that are not valid are corrected."
const L_DiscardChanges_ErrorMessage = "You have made incomplete changes to properties. Click 'OK' to discard the changes and exit."

' -- httpxmlUtil.htm
const L_HTTPError_HTMLTitle = "HTTP Error"
const L_ErrorPostingXML_ErrorMessage = "An error occurred while posting XML information to the server."

' -- 500error.asp
const L_ErrorsInPageLoad_ErrorMessage = "Errors occurred while loading part of the Business Desk."
%>
