<%
' -- error strings
const L_ADOErrorTitle_ErrorMessage = "ADO Error:"
const L_ADOErrorNumber_ErrorMessage = "Number:"
const L_ADOErrorDescription_ErrorMessage = "Description:"
const L_ADOErrorSource_ErrorMessage = "Source:"
const L_ADOErrorSQLState_ErrorMessage = "SQL State"
const L_ADOErrorNativeError_ErrorMessage = "Native Error:"
const L_XMLErrorTitle_ErrorMessage = "XML Error:"
const L_XMLErrorErrorCode_ErrorMessage = "Error Code:"
const L_XMLErrorFilePos_ErrorMessage = "File Pos:"
const L_XMLErrorLine_ErrorMessage = "Line:"
const L_XMLErrorLinePos_ErrorMessage = "Line Pos:"
const L_XMLErrorReason_ErrorMessage = "Reason:"
const L_XMLErrorSourceText_ErrorMessage = "Source Text:"
const L_XMLErrorURL_ErrorMessage = "URL:"
const L_ScriptErrorTitle_ErrorMessage = "Script Error:"
const L_ScriptErrorNumber_ErrorMessage = "Number:"
const L_ScriptErrorDescription_ErrorMessage = "Description"
const L_ScriptErrorSource_ErrorMessage = "Source:"
const L_ScriptErrorHelpFile_ErrorMessage = "Help File:"
const L_ScriptErrorHelpContext_ErrorMessage = "Help Context:"
const L_ASPErrorTitle_ErrorMessage = "ASP Error:"
const L_ASPErrorASPCode_ErrorMessage = "ASP Code:"
const L_ASPErrorNumber_ErrorMessage = "Number:"
const L_ASPErrorDescription_ErrorMessage = "Description:"
const L_ASPErrorSource_ErrorMessage = "Source:"
const L_ASPErrorFileName_ErrorMessage = "File Name:"
const L_ASPErrorLineNumber_ErrorMessage = "Line Number:"
const L_EventLogSiteId_ErrorMessage = "%1 BusinessDesk"
const L_CannotWriteErrorEvent_ErrorMessage = "Cannot write error to event log"

' -- error types to determine which icon displays in error dialog
const ERROR_ICON_ALERT = 0
const ERROR_ICON_CRITICAL = 1
const ERROR_ICON_QUESTION = 2
const ERROR_ICON_INFORMATION = 3

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sFormatString(sFormat, aArgs)
'		constructs a string from a format string and substrings (for localization)
'
'	Arguments:	sFormat		String. Format string containing numbered arg 
'							insertion points like "%1", "%2", etc.
'				aArgs		Array. Array of variants to replace the "%#" string
'							corresponding to its index 
'
'	Returns:				String. Formatted as above.
'
Function sFormatString(sFormat, aArgs)
	dim nArg
	for nArg = LBound(aArgs) to UBound(aArgs)
		if isNull(aArgs(nArg)) then aArgs(nArg) = ""
		sFormat = Replace(sFormat, "%" & nArg + 1, aArgs(nArg))
	next
	sFormatString = sFormat
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	GetSiteConfigField(sResourceName, sPropertyName)
'		gets the contents of a Site Config resource property
'
'	Arguments:	sResourceName	String. Name of resource in Site Config
'				sPropertyName	String. Name of property for resource
'
'	Returns:					String. contents of resource property or ""
'
Function GetSiteConfigField(sResourceName, sPropertyName)
	GetSiteConfigField = ""
	on error resume next
	GetSiteConfigField = g_MSCSConfiguration.Fields(CStr(sResourceName)).Value.Fields(CStr(sPropertyName)).Value
	on error goto 0
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sRelURL2AbsURL(sRelURL)
'		turns a relative URL into the corresponding absolute URL
'
'	Arguments:	sRelURL		String. URL relative to current page
'
'	Returns:				String. absolute URL
'
Function sRelURL2AbsURL(sRelURL)
    Dim sAbsURL

    sAbsURL = Request.ServerVariables("URL")
    sAbsURL = Left(sAbsURL, InStrRev(sAbsURL, "/"))
    sAbsURL = sAbsURL & sRelURL

    sRelURL2AbsURL = sAbsURL
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	xmlMergeGroups2(xmlMaster, xmlDetail, sKeyId)
'		merges 2 XML data documents into a 2 level hierarchy usable by ListSheet widget
'
'	Arguments:	xmlMaster	XML document. master XML document
'				xmlDetail	XML document. detail XML document
'				sKeyId		String. key field id to use for merge
'
'	Returns:				XML document. master document with detail items
'							inserted under records that have the same sKeyId value
'
function xmlMergeGroups2(xmlMaster, xmlDetail, sKeyId)
	dim xmlRecord, xmlKeyNode, xmlSublist, xmlSubRecord

	for each xmlRecord in xmlMaster.selectNodes("record")
		set xmlKeyNode = xmlRecord.selectSingleNode(sKeyId)
		if not xmlKeyNode is nothing then
			set xmlSublist = xmlDetail.selectNodes("record[" & sKeyId & " $eq$ '" & xmlKeyNode.text & "']")
			for each xmlSubRecord in xmlSublist
				xmlRecord.appendChild(xmlSubRecord.cloneNode(true))
			next
		end if
	next
	set xmlMergeGroups2 = xmlMaster
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	xmlMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
'		merges 3 XML data documents into a 3 level hierarchy usable by ListSheet widget
'
'	Arguments:	xmlMaster		XML document. master XML document
'				xmlGroup		XML document. second level XML document
'				xmlDetail		XML document. detail XML document
'				sMasterKeyId	String. key field id to use for merge of master and group/detail
'				sDetailKeyId	String. key field id to use for merge of group and detail
'
'	Returns:					XML document. master document with group and detail items
'								inserted under records that have the same sXXXKeyId value
'
function xmlMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
	set xmlMergeGroups3 = xmlMergeGroups2(xmlMaster, xmlMergeGroups2(xmlGroup, xmlDetail, sDetailKeyId), sMasterKeyId)
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sMergeGroups2(xmlMaster, xmlDetail, sKeyId)
'		merges 2 XML data documents into a 2 level hierarchy usable by ListSheet widget
'
'	Arguments:	xmlMaster	XML document. master XML document
'				xmlDetail	XML document. detail XML document
'				sKeyId		String. key field id to use for merge
'
'	Returns:				String. XML string of master document with detail items
'							inserted under records that have the same sKeyId value
'
function sMergeGroups2(xmlMaster, xmlDetail, sKeyId)
	sMergeGroups2 = xmlMergeGroups2(xmlMaster, xmlDetail, sKeyId).xml
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
'		merges 3 XML data documents into a 3 level hierarchy usable by ListSheet widget
'
'	Arguments:	xmlMaster		XML document. master XML document
'				xmlGroup		XML document. second level XML document
'				xmlDetail		XML document. detail XML document
'				sMasterKeyId	String. key field id to use for merge of master and group/detail
'				sDetailKeyId	String. key field id to use for merge of group and detail
'
'	Returns:					String. XML string of master document with group and detail items
'								inserted under records that have the same sXXXKeyId value
'
function sMergeGroups3(xmlMaster, xmlGroup, xmlDetail, sMasterKeyId, sDetailKeyId)
	sMergeGroups3 = xmlMergeGroups3(xmlMaster, xmlDetail, sMasterKeyId, sDetailKeyId).xml
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	ERROR HANDLING
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	SetError(sErrorTitle, sFriendlyError, sErrorDetails, nErrorType)
'		sets an error in the session for display on the next page
'
'	Arguments:	sErrorTitle			String. error dialog title
'				sFriendlyError		String. client error message
'				sErrorDetails		String. error details that will be shown in
'									expandable "more details" window
'				nErrorType			Integer. error number for which icon to show
'									One of:
'										ERROR_ICON_ALERT
'										ERROR_ICON_CRITICAL
'										ERROR_ICON_QUESTION
'										ERROR_ICON_INFORMATION
'
sub SetError(byVal sErrorTitle, byVal sFriendlyError, byVal sErrorDetails, byVal nErrorType)
	dim slBDError, sLogError
	' -- set in session variable for display in script on page
	if isEmpty(Session("MSCSBDError")) then
		set Session("MSCSBDError") = Server.CreateObject("commerce.simplelist")
	end if
	set slBDError = Session("MSCSBDError")
	slBDError.add(dGetErrorDict(sErrorTitle, sFriendlyError, sErrorDetails, nErrorType))
	if nErrorType < 2 then
		' -- log error to event log for critical and alert errors:
		on error resume next
		sLogError = vbCR & sFriendlyError & vbCR & string(80, "-") & vbCR & sErrorDetails
		g_MSCSAdminEventLog.WriteErrorEvent sFormatstring(L_EventLogSiteId_ErrorMessage, array(Application("MSCSSiteName"))), sStripHTML(sLogError)
		if Err.number <> 0 then
			slBDError.add(dGetErrorDict("", L_CannotWriteErrorEvent_ErrorMessage, sGetScriptError(Err), ERROR_ICON_ALERT))
		end if
	end if
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	strips html code for event log
function sStripHTML(sHTMLText)
	dim oRegEx
	sHTMLText = replace(sHTMLText, "</TR>", vbCR)
	set oRegEx = New RegExp
	oRegEx.IgnoreCase = true
	oRegEx.Global = true
	oRegEx.Pattern = "<[^<>]*>"
	sStripHTML = oRegEx.replace(sHTMLText, "")
	set oRegEx = nothing
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	places error strings in a dictionary
function dGetErrorDict(sErrorTitle, sFriendlyError, sErrorDetails, nErrorType)
	dim dBDError
	set dBDError = Server.CreateObject("commerce.dictionary")
	dBDError("errorTitle") = sErrorTitle
	dBDError("friendlyError") = sESCErrorText(sFriendlyError)
	dBDError("errorDetails") = sESCErrorText(sErrorDetails)
	dBDError("errorType") = nErrorType
	set dGetErrorDict = dBDError
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	replaces LF and CR with <BR>, and escapes double-quotes
function sESCErrorText(sText)
	sESCErrorText = replace(replace(replace(replace(sText, vbCRLF, "<br>"), vbCR, "<br>"), vbLF, "<br>"), """", """""")
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sGetADOError(oConnectionErrorsError)
'		returns error string populated from error object
'
'	Arguments:	oConnectionErrorsError	Object. ADO Connection.Errors.Error object
'
'	Returns:							String. error string with object properties
'										displayed in an HTML table
'
function sGetADOError(oConnectionErrorsError)
	dim sText
	sText = ""
	sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
	sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_ADOErrorTitle_ErrorMessage & "</TH></TR>"
	sText = sText & "<TR><TH>" & L_ADOErrorNumber_ErrorMessage & "</TH><TD>0x" &	Hex(oConnectionErrorsError.number) & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ADOErrorDescription_ErrorMessage & "</TH><TD>" &	oConnectionErrorsError.description & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ADOErrorSource_ErrorMessage & "</TH><TD>" &		oConnectionErrorsError.source & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ADOErrorSQLState_ErrorMessage & "</TH><TD>" &	oConnectionErrorsError.SQLState & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ADOErrorNativeError_ErrorMessage & "</TH><TD>" &	oConnectionErrorsError.NativeError & "</TD></TR>"
	sText = sText & "</TABLE>"
	sGetADOError = sText
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sGetXMLError(oXMLDocParseError)
'		returns error string populated from error object
'
'	Arguments:	oXMLDocParseError	Object. XML document.parseError object
'
'	Returns:						String. error string with object properties
'									displayed in an HTML table
'
function sGetXMLError(oXMLDocParseError)
	dim sText
	sText = ""
	sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
	sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_XMLErrorTitle_ErrorMessage & "</TH></TR>"
	sText = sText & "<TR><TH>" & L_XMLErrorErrorCode_ErrorMessage & "</TH><TD>" &	oXMLDocParseError.errorCode & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_XMLErrorFilePos_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.filePos & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_XMLErrorLine_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.line & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_XMLErrorLinePos_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.linePos & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_XMLErrorReason_ErrorMessage & "</TH><TD>" &		oXMLDocParseError.reason & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_XMLErrorSourceText_ErrorMessage & "</TH><TD>" &	oXMLDocParseError.srcText & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_XMLErrorURL_ErrorMessage & "</TH><TD>" &			oXMLDocParseError.URL & "</TD></TR>"
	sText = sText & "</TABLE>"
	sGetXMLError = sText
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sGetScriptError(oErr)
'		returns error string populated from error object
'
'	Arguments:	oErr	Object. VBScript Err object
'
'	Returns:			String. error string with object properties
'						displayed in an HTML table
'
function sGetScriptError(oErr)
	dim sText
	sText = ""
	sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
	sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_ScriptErrorTitle_ErrorMessage & "</TH></TR>"
	sText = sText & "<TR><TH>" & L_ScriptErrorNumber_ErrorMessage & "</TH><TD>0x" &		Hex(oErr.number) & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ScriptErrorDescription_ErrorMessage & "</TH><TD>" &	oErr.description & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ScriptErrorSource_ErrorMessage & "</TH><TD>" &		oErr.source & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ScriptErrorHelpFile_ErrorMessage & "</TH><TD>" &		oErr.helpFile & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ScriptErrorHelpContext_ErrorMessage & "</TH><TD>" &	oErr.helpContext & "</TD></TR>"
	
	sText = sText & "</TABLE>"
	sGetScriptError = sText
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sGetASPError(oServerGetLastError)
'		returns error string populated from error object
'
'	Arguments:	oServerGetLastError	Object. ASP Server.getLastError object
'
'	Returns:						String. error string with object properties
'									displayed in an HTML table
'
function sGetASPError(oServerGetLastError)
	dim sText
	sText = ""
	sText = sText & "<TABLE><COLGROUP><COL WIDTH='35%'><COL WIDTH='65%'></COLGROUP>"
	sText = sText & "<TR><TH ID='bddetailtitle' COLSPAN=2>" & L_ASPErrorTitle_ErrorMessage & "</TH></TR>"
	sText = sText & "<TR><TH>" & L_ASPErrorASPCode_ErrorMessage & "</TH><TD>" &		oServerGetLastError.ASPCode & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ASPErrorNumber_ErrorMessage & "</TH><TD>0x" &		Hex(oServerGetLastError.number) & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ASPErrorDescription_ErrorMessage & "</TH><TD>" &	oServerGetLastError.description & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ASPErrorSource_ErrorMessage & "</TH><TD>" &		oServerGetLastError.source & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ASPErrorFileName_ErrorMessage & "</TH><TD>" &		oServerGetLastError.fileName & "</TD></TR>"
	sText = sText & "<TR><TH>" & L_ASPErrorLineNumber_ErrorMessage & "</TH><TD>" &	oServerGetLastError.lineNumber & "</TD></TR>"
	sText = sText & "</TABLE>"
	sGetASPError = sText
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'	sGetErrorById(sErrorId)
'		returns error friendly string looked up from MessageManager in application
'
'	Arguments:	sErrorId	String. string id for looking up in MessageManager
'
'	Returns:				String. friendly error string
'
function sGetErrorById(sErrorId)
	' -- lookup message in MessageManager
	sGetErrorById = g_MSCSMessageManager.GetMessage(sErrorId, CStr(Application("MSCSDefaultLocale")))
end function
%>