//Production Version 2.0
//Last Modified: 9/14/00 by dvana
//9-19-00 Added Related Topic roll over
//9-20-00 Added Related Topics accessibility

//Image state
var imgState;

// Localized code
var L_NoDefAlertTitle_Text = "Definition Unavailable";
var L_NoDefAlert_Text = "The glossary term and definition do not exist in the glossary " +
	"file at this time.";
var L_See_Text = "<b>See:&nbsp;</b>"
var L_SeeAlso_Text = "See also:&nbsp;"
var L_GlossaryAlt_ToolTip = "View definition";
var L_SelectImgTitle = "Select an element";


// Glossary global variables

var ie4 = false;
var includeglossIMG = false;
var xmlFilePath = "../basics/gloss.xml";
var glossImgPath = "../basics/glossary.gif";
var popupDIV = "<div id='popupWindow' style='visibility:hidden; position:absolute; " +
	"top:0px; left:0px; background-color:#ffffcc; " +
	"border:solid 1 #333333'></div>";
var glossImg = "<IMG CLASS='PopUp' SRC='" + glossImgPath + "' HEIGHT='9' WIDTH='12' BORDER='0'>";
var ieX, ieY;

//Added by dvana for art
var ratioWidth = .40
var ratioHeight = .40
var imgSources

//Select Popup
var selectPopup = false

if (navigator.appName == "Microsoft Internet Explorer")
{
	var ver = navigator.appVersion;
	var inx = ver.indexOf(".", 0);
	var v = new Number(ver.substring(0,inx));
	if (v >= 4)
			ie4 = true;

}

if (ie4){
	window.onload = bodyOnLoad;}
else
	alert("MS Internet Explorer version 4 or greater is required.");


//Event Handlers

function document.onmouseover(){
	relatedTopic_onmouseover()
	var elem = event.srcElement
	if (elem.id == "PopUp" || elem.id == "In_Popup"){
		elem.style.color = "red"
	}
}

//Keyboard Accessibility
function document.onkeyup(){
	key = event.keyCode
	
	if (key == 27){
		hidePopups()
	}
}

function hidePopups(){
	
	try{
		if (popupWindow) popupWindow.style.visibility = "hidden";
		}
	catch (e) {}
	
	try{
		relatedTopics.style.display = "none"
	}
	catch (e) {}
	
	try{
		divSelect.style.display = "none"
	}
	catch (e) {}
	
	
	try {
	hideTip()
	hideSeeAlso()
	closeMenu()
		
	}
	catch (e) {}
}

function document.onmouseout(){
	relatedTopic_onmouseout()
	var elem = event.srcElement
	if (elem.id == "PopUp" || elem.id == "In_Popup"){
		elem.style.color = "#008000"
	}
}

//End Event Handlers

function bodyOnClick()
{
	
	var elem = window.event.srcElement;
	
	for (; elem; elem = elem.parentElement)
	{
		//if (elem.id == "PopUp" || elem.id == "In_Popup" || elem.className == "glossary")
		
		if (elem.id == "PopUp" || elem.id == "In_Popup" )
		{
			if (elem.className == "glossary" ){
				elem = elem.childNodes(0)
			}
			showGlossaryDef(elem, elem.id == "In_Popup");
			return;
		}
		
	}
	popupWindow.style.visibility = "hidden";
	
	
	//Added by dvana for Art
	//Move this to a function
	var elem = window.event.srcElement;
	
	//elemID = getUniqueIndex(elem.id, uniqueID)
	
	//sizeThumbnail(elem.id)
		
}

function sizeAllThumbnails(){
	if (!document.all.illustration.length){
		sizeThumbnail(image.childNodes(0).childNodes(0))
	}
	else{
	for (i=0; i < document.all.illustration.length; i++){
		sizeThumbnail(image(i).childNodes(0).childNodes(0))
		}
	}
}

function getUniqueObject(id, uniqueID){
	var objects = document.all(id)
	for (i=0; i < objects.length; i++){
		if (objects[i].uniqueID == uniqueID){
			return objects[i]
		}
	}
}

function getUniqueIndex(id, uniqueID){
	var objects = document.all(id)
	for (i=0; i < objects.length; i++){
		if (objects[i].uniqueID == uniqueID){
			return i
		}
	}
}

function getThumbnail(elem){
	//Loop until class="thumbnailImage" 
	
	while (elem)
	{
		if (elem.className.toLowerCase() == "thumbnail"){
				
		    return elem.childNodes(0);
		    }
		if (elem.tagName.toLowerCase() == "body" ){
			elem = null
			return false;
		}
		 elem = elem.parentElement;
	}
}


function sizeThumbnail(id, uniqueID){
	//getImage id
	
	var elem = document.all(id)
	if (elem){
	if(elem.parentElement.parentElement.className == "thumbnail" && elem.tagName == "IMG"){
			
		if (elem.parentElement.parentElement.style.height == ""){
			elem.parentElement.parentElement.style.height = 0
		}
		//Test for null
		
		i = parseInt(id.slice(10, id.length))
		
		elem.width = imgSources[i].originalWidth
		elem.height =imgSources[i].originalHeight
				
		elem.parentElement.parentElement.className = "large"}
	else if (elem.parentElement.parentElement.className == "large" && elem.tagName == "IMG"){
						
			elem.width = 150
			elem.height = 150
			
			elem.parentElement.parentElement.className = "thumbnail"
		}
	}

	//return (false)
}

function bodyOnLoad()
{
	
	//Added for CS
	try{
		thumbNailAll()
	}
	catch(e){}

	getRelated()
	
	changeAnchor()
	
	//Added by dvana for Art
	getImages();
		
	//Build DHTML Tree
	try{
		buildTree("none")}
	catch(e){}

	//Feedback mail
	try{
		Feedback_mail();}
	catch (e) {}
	
	document.body.onclick = bodyOnClick;
	document.body.insertAdjacentHTML('beforeEnd', popupDIV);
	var anchorTags = document.all.tags('A');
	for (var a = 0; a < anchorTags.length; a++)
		if (anchorTags[a].id.indexOf('PopUp') != -1)
		{
			anchorTags[a].title = L_GlossaryAlt_ToolTip;
			if (includeglossIMG)
				anchorTags[a].innerHTML = glossImg + anchorTags[a].innerHTML;
		}

}


function showGlossaryDef(elem, inPopup)
{
	var projectTerm = elem.getAttribute("term")
	//var projectTerm = elem.hash;
	
	try{
		divSelect.style.display = "none"
	}
	catch (e) {}
	
	popupWindow.style.height = 0;
	popupWindow.style.width = 200;
 	popupWindow.style.padding = 10;
	popupWindow.style.fontsize = 10;
       	popupWindow.innerHTML = '\t'+getXMLPopupContent(projectTerm);
	positionPopup(elem, inPopup);
			
	popupWindow.style.visibility = "visible";
}

function getXMLPopupContent(projectTerm)
{
	var term, termID;
	var entry;
	var scopeDefs;
	var scopes;
	var definition;
	var seeAlsos, seeAlsoID, seeAlsoTerm;
	var seeEntry, seeID, seeTerm;
	var outText;
	var i, j, k, l, m;
	var scopeFound;
	var noDef = "<h3><A style='text-decoration: none; color:black' href='javascript:void(0)'>" + L_NoDefAlertTitle_Text + "</a></h3><p>" + L_NoDefAlert_Text + "</p>";
	//var noDef = "<h3>" + L_NoDefAlertTitle_Text + "</h3><p>" + L_NoDefAlert_Text + "</p>";

	var xmlDOM;

	if (projectTerm.length > 1)
	{
		termID = projectTerm.substring(1, projectTerm.length);
		i = termID.indexOf(":");
		if (i > 0)
			term = termID.substring(i+1, termID.length);
		else
			return (noDef);
	}
	else{
		return (noDef);
	}
                    
	xmlDOM = new ActiveXObject("Microsoft.XMLDOM");
	xmlDOM.async = false;
	xmlDOM.validateOnParse = false;
	xmlDOM.load(xmlFilePath);
	  
	outText = noDef;

	entry = xmlDOM.nodeFromID(term);
	if (entry != null){
		scopeDefs = entry.selectNodes("scopeDef");
		scopeFound = false;
		for (i = 0; i < scopeDefs.length && !scopeFound; i++)
		{
			scopes = scopeDefs(i).selectNodes("scope");
			for (j = 0; j < scopes.length; j++)
			{
				if (scopes(j).attributes.getNamedItem("scopeTermID").text == termID)
				{
		  			scopeFound = true;
					outText = formatXMLTerm(entry.selectSingleNode("term").text);
					if (scopeDefs(i).selectSingleNode("def") != null)
					{
						definition = formatXMLDef(scopeDefs(i).selectSingleNode("def"));
						outText = outText + definition;
						seeAlsos = scopeDefs(i).selectNodes("seeAlso");
						seeAlsoID = "";
						seeAlsoTerm = "";
						for (k = 0; k < seeAlsos.length; k++)
						{
							seeAlsoID = seeAlsos(k).attributes.getNamedItem("seeAlsoID").text;
							l = seeAlsoID.indexOf(":");
							if (l > 0)
							{
								seeAlsoScope = seeAlsoID.substring(0, l + 1);
								seeAlsoID = seeAlsoID.substring(l + 1, seeAlsoID.length);
							}
							else
								seeAlsoScope = "";
							seeAlsoTerm = xmlDOM.nodeFromID(seeAlsoID).selectSingleNode("term").text;
							outText = outText + formatXMLSeeAlso(seeAlsoScope + seeAlsoID, seeAlsoTerm, (k == 0));}        
							if (k > 0)
								outText = outText + "</P>";
						}
					else
					{
						seeEntry = scopeDefs(i).selectSingleNode("seeEntry");
						seeID = seeEntry.attributes.getNamedItem("seeID").text;
						k = seeID.indexOf(":");
						if (k > 0)
						{
							seeScope = seeID.substring(0, k + 1);
							seeID = seeID.substring(k + 1, seeID.length);
						}
						else
							seeScope = "";
						seeTerm = xmlDOM.nodeFromID(seeID).selectSingleNode("term").text;
						outText = outText + formatXMLSee(seeScope + seeID, seeTerm);
					}
				}
			}
		}
	}
	else
		outText = noDef;
	return (outText);
}

function formatXMLTerm(theTerm)
{
	return ("<h3><a style='text-decoration: none; color:black' href='javascript:void(0)'>" + theTerm + "</a></h3>");
}


function formatXMLDef(def)
{
	var paras;
	var i;
	var defOut;

	paras = def.selectNodes("para");
	defOut = "";
	for (i = 0; i < paras.length; i++)
		defOut = defOut + "<p>" + paras(i).text + "</p>";
	return (defOut);
}


function formatXMLSee(seeTermID, seeTerm)
{
	var seeText;

	//seeText = "<a id='In_Popup' href='#" + seeTermID + "'>" + seeTerm + "</a>";
	seeSpan = "<span id='In_Popup' term='#" + seeTermID + "'>" + seeTerm + "</span>";
	
	//seeText = "<a style='text-decoration: none; color:black' href='JavaScript:void(0)'>" + seeSpan + "</a>"
	seeText = seeSpan

	return ("<p id='SeeDef'>" + L_See_Text + seeText);
}


function formatXMLSeeAlso(seeAlsoTermID, seeAlsoTerm, bFirstOne)
{
	var seeAlsoText;

	//seeAlsoText = "<a id='In_Popup' href='#" + seeAlsoTermID + "'>" + seeAlsoTerm + "</a>";
	seeAlsoSpan = "<span id='In_Popup' term='#" + seeAlsoTermID + "'>" + seeAlsoTerm + "</span>";
	
	//seeAlsoText = "<a style='text-decoration: none; color:black' href='JavaScript:void(0)'>" + seeAlsoSpan + "</a>"
	seeAlsoText = seeAlsoSpan
	
	if (bFirstOne)
		return ("<p id='OtherDefs'>" + L_SeeAlso_Text + seeAlsoText);
	else
		return (", " + seeAlsoText);
}


function positionPopup(e, inPopup)
{
	var pageBottom = document.body.scrollTop + document.body.clientHeight;
	var popHeight = popupWindow.offsetHeight;

	if (inPopup)
	{
		ieX = popupWindow.style.left;
		ieY = popupWindow.style.top;
	}
	else
	{
		if (e.offsetParent.tagName.toLowerCase() == 'body')
		{
			ieX = e.offsetLeft;
			ieY = ((e.offsetTop) + (e.offsetHeight) + 1);
		}
		else if (e.offsetParent.offsetParent.tagName.toLowerCase() == 'body')
		{
			ieX = ((e.offsetLeft) + (e.offsetParent.offsetLeft));
			ieY = ((e.offsetHeight) + (e.offsetTop) + (e.offsetParent.offsetTop) + (1));
		}
		else if (e.offsetParent.offsetParent.offsetParent.tagName.toLowerCase() == 'body')
		{
			ieX = ((e.offsetLeft) + (e.offsetParent.offsetLeft) + (e.offsetParent.offsetParent.offsetLeft));
			ieY = ((e.offsetHeight) + (e.offsetTop) + (e.offsetParent.offsetTop) + (e.offsetParent.offsetParent.offsetTop) + (1));
		}
		else
		{
			ieX = window.event.clientX;
			ieY = window.event.clientY + document.body.scrollTop;
		}
	}
 	var rightlimit = ieX + popupWindow.offsetWidth;
	if (rightlimit >= document.body.clientWidth)
	{
		ieX -= (rightlimit - document.body.clientWidth);
	}
	
	try{
		popupWindow.style.height = popHeight - 2 * (parseInt(popupWindow.style.borderWidth));
	}
	catch (e) {}
	
	if (popHeight + ieY >= pageBottom)
	{
		if (popHeight <= pageBottom)
		{
			ieY = pageBottom - popHeight;
		}
		else
		{
			ieY = 0;
		}
	}
	popupWindow.style.left = ieX;
	popupWindow.style.top = ieY;
	
}


function document.onclick()
{
	var obj = window.event.srcElement;
	
	//Related Topics
	relatedTopic_onclick()
	
	if (obj.className == "selectTag")
	{
		var elem = targetElement.previousSibling.previousSibling;
		
		var endTag = elem.nextSibling.innerText;
		
		if (endTag == "/>")
			elem.innerText = obj.innerText;
		else{
			var elemID = elem.id
			elem.innerText = obj.innerText;
			elem = document.all.item(elemID);
			elem(1).innerText = obj.innerText;}
	}
	
	try{
		if (divSelect.style.display == "none" && selectPopup){
			selectPopup = false
			divSelect.style.display = "block";}
		else
			
			if (obj.className != "nodeText"){
				divSelect.style.display = "none";
				selectPopup = false
				}
				
		if (obj.className == "nodeText" || obj.className == "bracket"){
			var objTarget = obj.parentElement.firstChild;
		
			if (objTarget.className == "select")
			{
			
				if (divSelect.style.display == "block")
					divSelect.style.display = "none";
				else {
					showSelect(objTarget);
					divSelect.style.display = "block"; }
			}
			else{
				var sub,plus,minus;
				divSelect.style.display = "none";
				sub = objTarget.id;
				var idPos = sub.indexOf("Plus");
				if (idPos != -1){
					var id = sub.substr(4,2);
				if (document.all(sub).style.display == "none")
					{
					collapse(id);
					}
				else
					{
					expand(id);
					}
				}
			}
		}
	}
	catch(e) {}
}

function node_old(textValue, display, image, depth, id)
{
	var nodeValue;
	var sDisplay = "";
	if (display.toUpperCase() == "XML")
	{
		
		if (depth == 0)
			sDisplay = "<IMG style='cursor:default;' hspace='1' SRC='../basics/none.bmp'/>" + "<SPAN style='cursor:default;' class='bracket'>&nbsp;&lt;</SPAN><SPAN style='cursor:default;' class='nodeText'>" + textValue + "</SPAN><SPAN style='cursor:default;' class='bracket'>/&gt;</SPAN>" +  image;

		else
			sDisplay = "<SPAN class='bracket'>&nbsp;&lt;</SPAN><SPAN class='nodeText' id='level" + id + "'>" + textValue + "</SPAN><SPAN class='bracket'>&gt;</SPAN>" + image ;
		
				
		return sDisplay;
	
	}
	else
	{
		sDisplay = image + "&nbsp;<SPAN class='nodeText'>" + textValue + "</SPAN>";
		return sDisplay;
	}
}

function window.onscroll()
{
	try {
	if (targetElement.className == "select"){
		var offsetTop = document.body.scrollTop + document.body.clientHeight - (document.body.style.border + 15);
		var boxWidth = targetElement.offsetTop + divSelect.style.pixelHeight;
		if (boxWidth > offsetTop)
			divSelect.style.pixelTop = targetElement.offsetTop - (divSelect.style.pixelHeight + 8);
		else
			divSelect.style.pixelTop = targetElement.offsetTop + 12;
		}
	}
	catch (e) {}
}

//Added by dvana for Art
function getImages()
{
	try{
		var imgLen = document.all.image.length;
		imgSources = new Array();
	
		if (imgLen == null){
				imgSources[0] = document.all.image
				imgSources[0] = newImage(imgSources[0], 0)
													
				loadImage(imgSources[0], null)
				}
		else {
			for (i = 0; i < imgLen; i++)
			{
				imgSources[i] = document.all.image[i];
				imgSources[i] = newImage(imgSources[i], i)
									
				loadImage(imgSources[i], i)
			}
		}
	}
	catch (e){ }
}

function newImage(elem, i){
	//<a href="javascript:sizeThumbnail('thumbnail.1')"><IMG class=thumbnailImage height=150 hspace=2 src="LAT_TLS_LoopPathMap.gif" width=150 vspace=2></a>
	
	if (elem.childNodes(0).className == "thumbnail"){
			var img = elem.getElementsByTagName("IMG")
			img(0).id = "thumbnail." + i
			var js = "javascript:sizeThumbnail('thumbnail." + i + "')"
			var newTag = "<a href=" + js + ">" + img(0).outerHTML + "</a>"
			img(0).outerHTML = newTag
			
			}
	
	return elem
}


function loadImage(source, index)
	//This works with image display: none
{
	if (source.childNodes(0).className = "thumbnail"){
		var img = source.getElementsByTagName("IMG")
		var src = img(0).src
	}
				
	var tmpImage = new Image
	tmpImage.src = src
		
	source.originalWidth = tmpImage.width
	source.originalHeight = tmpImage.height

	//Call for CS to make all large
	//sizeAllThumbnails()
}

function getImageCell(elem){
	var i = -1	
	while (elem)
	{
		i++
		if (elem.tagName.toLowerCase() == "td"){
				
		    return elem;
		    }
		 elem = elem.all(i);
	}
}

function showError(errorText){
	alert(errorText);
}


//BTS Related Topics Functionality
//Check for id = "relatedTopics"
function getRelatedTopics(oElem)
{
	while (oElem)
	{
		if (oElem.className.toLowerCase() == "relateditem"){
				
		    return oElem;
		    }
		if (oElem.tagName.toLowerCase() == "body" || oElem.id.toLowerCase() == "tree"){
			oElem = null
			return false;
		}
		 oElem = oElem.parentElement;
	}
}

function relatedTopic_onmouseover(){
	var elem = window.event.srcElement
	var Related = getRelatedTopics(elem)
	if (Related != null){
		try {
			Related.all(0).style.color="white"
			Related.style.backgroundColor="navy"
		}
		catch (e) {}
	}
}

function relatedTopic_onmouseout(){
	var elem = window.event.srcElement
	var Related = getRelatedTopics(elem)
	if (Related != null){
		try {
			Related.all(0).style.color="black"
			Related.style.backgroundColor = relatedTopics.style.backgroundColor
			}
		catch (e) {}
	}
}
function popup(){

}

function relatedTopic_onclick(){
	var elem = window.event.srcElement
	var relatedItems = document.all.item("relatedTopics")
	
	if (elem.className == "relatedItem"){
		window.navigate(elem.getAttribute("href"))
	}
	
	try {
		if (elem.className == "relatedTopic"){
			setPosition(elem, relatedItems)
			if (relatedItems.style.display == ""){
				relatedItems.style.display = "block"}
			else{
				relatedItems.style.display = ""}
			}
		else{
			if (relatedItems.style.display == "block"){
				relatedItems.style.display = ""}
		}
	}
	catch (e) {}

}

//This needs to be optimized
function getRelated(){
	for (i=1; i< 8; i++){
		tagName = "H" + i
		var heading = document.getElementsByTagName(tagName)

		if (heading.length > 0){
			//Use RegExp?
			for (x=0; x < heading.length; x++){
				if (heading(x).innerText.toLowerCase() == "related topics" || 
					heading(x).innerText.toLowerCase() == "related topic"){ // ||
					//heading(x).innerText.toLowerCase() == "see also"){
								
					//Added by Word
					heading(x).style.cursor = "hand"
					text = heading(x).innerText
					heading(x).innerHTML = "<a class='relatedTopic' href='javascript:void(0);'>" + text + "</a>"
					
					try{							
						var anchors = document.all.relatedTopics.getElementsByTagName("A")

						var s = ""
						for(iAnchor=0; iAnchor < anchors.length; iAnchor++){
								//?intWidth = relatedTopics.style.width 
								intWidth = "200px"
								relatedTopics.style.width = "205px"
								
								anchors(iAnchor).style.color = "black"
								anchors(iAnchor).style.textDecoration = "none"
								var href = anchors(iAnchor).href
								var text = anchors(iAnchor).outerHTML
								var a = anchors(iAnchor)
								//href
								if (iAnchor == anchors.length - 1){
									s += "<div class='relatedItem' style='border:0; text-decoration:none; width:" + intWidth + "'" + " href='" + href + "'>" + text + "</div>"
								}
								else{
									s += "<div class='relatedItem' style='text-decoration:none; width:" + intWidth + "'" + " href='" + href + "'>" + text + "</div>"								
								}							}
						relatedTopics.innerHTML = s
			
						relatedTopics.style.padding = "2"
						relatedTopics.style.height = 0
					}
					catch (e) {}
				}
				}
			}
		}
}

function setPosition(target, popup){
	scrollBottom = document.body.scrollTop + document.body.clientHeight - (document.body.style.border + 15);
	
	try {
		
		//Get Height
		var boxHeight = 0
		
		for (i = 0; i < popup.childNodes.length - 1 ; i++){
			boxHeight = 15 * i
		}
		
		popup.style.pixelTop = target.offsetTop + target.offsetHeight + 2
		if ((popup.style.pixelTop + boxHeight) > scrollBottom - 25)
			popup.style.pixelTop = (popup.style.pixelTop - boxHeight) - 50
				
		popup.style.pixelLeft = target.offsetLeft
		
		
	}	
	catch (e){}
}


//Glossary Correction 
//dvana 9/14/00
function changeAnchor(){
	//Loop
	var length
	var oldPopupTag = document.all.PopUp
	if (oldPopupTag != null){
		if (oldPopupTag.length == null){
			createSpan(oldPopupTag)
		}
	
		for (i=0;i < oldPopupTag.length; i++){
			createSpan(oldPopupTag(i))
		}
	}
}

function createSpan(elem){
	var parent = elem.parentElement
	parent.removeChild(elem)
	var newNode = document.createElement("SPAN")
	with (newNode){
		setAttribute("term", elem.hash)
		id = elem.id
		className = elem.className
		innerText = elem.innerText
	}
	var node =  parent.appendChild(newNode)
	
	//Accessibility code removed due to crashes
	//node.outerHTML = "<a style='text-decoration: none; color:black' class = 'glossary' href='JavaScript:void(0)'>" + node.outerHTML + "</a>"
	

}

