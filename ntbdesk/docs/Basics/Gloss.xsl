<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">
<HTML>
<HEAD><META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252"></META>
<TITLE>Glossary</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="coUA.css"></LINK>
</HEAD>
<BODY BGCOLOR="#FFFFFF" TEXT="#000000">
<xsl:apply-templates />
</BODY>
</HTML>
</xsl:template>

<xsl:template match="glossary">
<xsl:apply-templates match="//" />
</xsl:template>

<xsl:template match="glossSection">
<xsl:apply-templates />
</xsl:template>

<xsl:template match="glossLetter">
<A><xsl:attribute name="NAME"><xsl:value-of /></xsl:attribute>
<H1 STYLE="COLOR:#999999; FONT-SIZE:200%; FONT-WEIGHT:BOLD; MARGIN-LEFT:.5EM; MARGIN-TOP:.75EM; MARGIN-BOTTOM:0EM;"><xsl:value-of /></H1></A>
</xsl:template>

<xsl:template match="empty">
  <P STYLE="MARGIN-LEFT:3EM; MARGIN-TOP:1.25EM; FONT-SIZE:100%;"><xsl:value-of select="id('Empty')" /></P>
</xsl:template>

<xsl:template match="entry">
<xsl:for-each select="scopeDef">
<xsl:for-each select="scope">
<A><xsl:attribute name="NAME">#<xsl:value-of select="@scopeTermID"/></xsl:attribute></A>
</xsl:for-each>
<xsl:apply-templates select="ancestor(entry)/term" />
<DIV STYLE="MARGIN-LEFT:3EM; FONT-SIZE:100%;">
<xsl:apply-templates select="seeEntry" />
<xsl:apply-templates select="def" />
<P STYLE=" MARGIN-TOP:.25EM;">
<xsl:for-each select="seeAlso">
<xsl:if test="context()[index() = 0]"><xsl:value-of select="id('SeeAlso')" /> </xsl:if><A><xsl:attribute name="HREF">#<xsl:value-of select="@seeAlsoID"/></xsl:attribute><xsl:value-of select="id(@seeAlsoTermID)/term" /></A><xsl:if test="context()[not(end())]">, </xsl:if>





</xsl:for-each>
</P>
</DIV>
</xsl:for-each>
</xsl:template>

<xsl:template match="term">
<H3 STYLE="MARGIN-LEFT:3EM; MARGIN-TOP:1.25EM; FONT-SIZE:100%;"><xsl:value-of /></H3>
</xsl:template>

<xsl:template match="seeEntry">
<xsl:value-of select="id('See')" />
<A><xsl:attribute name="HREF">#<xsl:value-of select="@seeID"/></xsl:attribute><xsl:value-of select="id(@seeTermID)/term" /></A>
</xsl:template>

<xsl:template match="def">
<xsl:apply-templates />
</xsl:template>

<xsl:template match="para">
<P>
<xsl:apply-templates />
</P>
</xsl:template>

<xsl:template match="text()">
<xsl:value-of />
</xsl:template>

<xsl:template match="b">
<B>
<xsl:apply-templates />
</B>
</xsl:template>

<xsl:template match="i">
<I>
<xsl:apply-templates />
</I>
</xsl:template>

<xsl:template match="u">
<U>
<xsl:apply-templates />
</U>
</xsl:template>

</xsl:stylesheet>
