//Loc Strings
var L_MailQ = "Did you find this information useful? Please send your suggestions and comments about the documentation to " 
var L_MailTeam = "Commerce Server Documentation Feedback@microsoft.com" 


function getDocName()
{
	var aName;
	var name = document.location.href;
	aName = name.split("/");
	var iLen = aName.length;
	return aName[iLen - 1];
}


function showSelect(obj)
{
	//40 = min
	var height = 0;
	
	var obj = document.all(obj)
	selectPopup = true
	var s = "<DIV>Choose one:</DIV>";
	divSelect.style.pixelLeft = obj.parentElement.offsetLeft;
	//divSelect.style.marginLeft = obj.parentElement.style.marginLeft
	
	
	targetElement = obj;
	query = "//node[@text='" + obj.id + "']"
	var node = tocDOM.selectSingleNode(query);
	var node = node.childNodes(0);
	//The last node is not returning its childNodes?
	for (i = 0; i < node.childNodes.length; i++)
	{
		s += "<span class='bracket'>&lt;</span>";
		
		//New Structure
		//<item value=""/>
		tagValue = node.childNodes(i).attributes.getNamedItem("value").nodeValue
		
		//Old Structure
		//tagValue = node.childNodes(i).nodeName
	
		s += "<a class='selectTag' href='javascript:void hideSelect()' title='Select an element to use'>" + tagValue + "</a>";
		s += "<span class='bracket'>&gt;</span>\n";
	}
	divSelect.innerHTML = s;
	
	var offsetTop = document.body.scrollTop + document.body.clientHeight - (document.body.style.border + 15);
	var boxHeight = obj.offsetTop + divSelect.style.pixelHeight;
	
	if (boxHeight > offsetTop)
		divSelect.style.pixelTop = obj.offsetTop - (divSelect.style.pixelHeight + 8);
	else
		divSelect.style.pixelTop = obj.offsetTop + 12;
}

function hideSelect()
{
	divSelect.style.display = "none"
}


//Added by Dvana to support thumbnail printing
function thumbNailAll(){
	var images = document.all.image
	tmpImgSources = new Array();
	
	try	{
	if (images.length == null){
		var image = images.children(0).children(0)
		
		thumbnail(image, 0)	
	}
	else{	
		for (i = 0; i < images.length; i++){
			var image = images(i).children(0).children(0)
			
			thumbnail(image, i)
			}
		}
	}
	catch (e){}
	
	//Hide any popups
	hidePopups()
}

function thumbnail(image, i){
	//imgState = image.parentElement.className
	//Always shrink
	
	if (image.className == "thumbnailImage" || image.childNodes(0).className == "thumbnailImage"){
		tmpImgSources[i] = image.parentElement.className
		
		if (image.children.length > 0){
			image.children(0).width = 150
			image.children(0).height = 150}
		else{
			image.width = 150
			image.height = 150
		}
		image.parentElement.parentElement.className = "thumbnail"
		}
}

function large(image, i){
	if (tmpImgSources[i] == "large"){
		printLarge}
}

function printLarge(image, i){
	if (image.children.length > 0){
		image.children(0).width = imgSources[i].originalWidth
		image.children(0).height = imgSources[i].originalHeight
		image.parentElement.parentElement.className = "large"
		image.parentElement.marginLeft = 0
		}
	else{
		image.width = imgSources[i].originalWidth
		image.height = imgSources[i].originalHeight
		image.parentElement.parentElement.className = "large"
		image.parentElement.marginLeft = 0
		}
}


function node(textValue, display, image, depth, id)
{
	var nodeValue;
	var sDisplay = "";
	if (display.toUpperCase() == "XML")
	{
		
		if (depth == 0)
			sDisplay = "<IMG style='cursor:default;' hspace='1' SRC='../basics/none.bmp'/>" + "<SPAN style='cursor:default;' class='bracket'>&nbsp;&lt;</SPAN><span style='cursor:default;' class='nodeText'>" + textValue + "</span><SPAN style='cursor:default;' class='bracket'>/&gt;</SPAN>" +  image;

		else
			sDisplay = "<SPAN class='bracket'>&nbsp;&lt;</SPAN><A href='javascript:void(0)' class='nodeText' style='text-decoration:none' id='level" + id + "'>" + textValue + "</A><SPAN class='bracket'>&gt;</SPAN>" + image ;
		
				
		return sDisplay;
	
	}
	else
	{
		sDisplay = image + "&nbsp;<A href='javascript:void(0)' style='text-decoration:none' class='nodeText'>" + textValue + "</A>";
		return sDisplay;
	}
}

//Mail to by topic
function addID(){
var locID = document.location.href; 
var iHTM = locID.lastIndexOf(".htm");
var iName=locID.lastIndexOf("/");
     locID = locID.substring(iName+1,iHTM);	

	return locID;
}

function Feedback_mail(){
try { var location = addID();
var doc_title = document.title;
MailDiv.innerHTML="<hr><p  style='margin-top: 0pt;'><i>" + L_MailQ + "<a href='mailto:csdf@microsoft.com?subject=Feedback on the " + doc_title + " topic (" + location + ".htm)'>" + L_MailTeam + "</a></i>.</p>";}
catch (e) {}
}