<html>
<head>
    <title>ASP Search Form</title>

<meta NAME="ROBOTS" CONTENT="NOINDEX">

<meta HTTP-EQUIV="Content-Type" content="text/html; charset=UTF-8">


<script Language="JavaScript">
<!--
function ChangeList(y,z) {

window.location.href="search.asp?Searchset="+(y)+"&SearchString="+(z);

}

//-->
</script>

</head>

<body bgcolor="black" onload="Activate()" scroll="auto">
<font face="ms sans serif">

<% MachType=Request.ServerVariables("HTTP_UA-CPU")
BrsType=Request.ServerVariables("HTTP_User-Agent")

If MachType="Alpha" Then
	ContHref="contalph.asp"
Else
	ContHref="toc_com.htm"
End If

If InStr(BrsType, "MSIE") Then
	IndxHref="index_com.htm"
Else
	ContHref="Coflat.htm"
	IndxHref="Inflat.htm"
End If
 %>

<span STYLE="position:  absolute; left: 4; top: 4">
<a HREF="toc_com.htm"><img SRC="NoCont.gif" border="0" alt="Contents" width="65" height="25"></a>
</span>

<span STYLE="position:  absolute; left: 69; top: 4">
<a HREF="Index_com.htm"><img SRC="NoIndex.gif" border="0" alt="Index"></a>
</span>

<span STYLE="position:  absolute; left: 114; top: 4">
<a HREF="search.asp"><img SRC="Search.gif" border="0" alt="Search" width="55" height="25"></a>
</span>

<script Language="JavaScript">
<!--
function Activate() {
      document.iissrch.SearchString.focus();
}

//-->
</script>

<span STYLE="position:  relative; left: 0; top: 18">
<table bgcolor="#ffffff" width="100%" height="85%" border="0">
<% SearchString=Request.QueryString("SearchString")%>
<% If SearchString="undefined" Then SearchString="" %>

<% SearchSet=Request.QueryString("SearchSet")%>
<% if SearchSet="" then SearchSet=0%>
<form ACTION="Query.asp?SearchType=<%=SearchSet%>" name="iissrch" id="iissrch" target="main" METHOD="post">
  <tbody>
<tr border="0" bgcolor="#ffffff" valign="top"><td>
<font size="-1">Search for:<br>
<input NAME="SearchString" style="width:100%" MAXLENGTH="100" Value="<%=SearchString%>">
<table width="100%">
<tr><td width="100%"></td><td>
<input NAME="Action" TYPE="submit" VALUE="Search"><td></td>
<tr><td><font size="-1">Select type of search:</font></td></tr></table>
</span>


<%If SearchSet=0 Then%>
<select NAME="SearchType" ONCHANGE="ChangeList(SearchType.selectedIndex,SearchString.value)">
<option Selected Value="1">Standard Search
<option Value="2">Exact Phrase
<option Value="3">All Words
<option Value="4">Any Words
<option Value="5">Boolean Search</option> 
</select>
<%End If%>

<%If SearchSet=1 Then%>
<select NAME="SearchType" ONCHANGE="ChangeList(SearchType.selectedIndex,SearchString.value)">
<option Value="1">Standard Search
<option Selected Value="2">Exact Phrase
<option Value="3">All Words
<option Value="4">Any Words
<option Value="5">Boolean 
        Search</option> 
</select>
<%End If%>

<%If SearchSet=2 Then%>
<select NAME="SearchType" ONCHANGE="ChangeList(SearchType.selectedIndex,SearchString.value)">
<option Value="1">Standard Search
<option Value="2">Exact Phrase
<option Selected Value="3">All Words
<option Value="4">Any Words
<option Value="5">Boolean 
        Search</option> 
</select>
<%End If%>

<%If SearchSet=3 Then%>
<select NAME="SearchType" ONCHANGE="ChangeList(SearchType.selectedIndex,SearchString.value)">
<option Value="1">Standard Search
<option Value="2">Exact Phrase
<option Value="3">All Words
<option Selected Value="4">Any Words
<option Value="5">Boolean 
        Search</option> 
</select>
<%End If%>

<%If SearchSet=4 Then%>
<select NAME="SearchType" ONCHANGE="ChangeList(SearchType.selectedIndex,SearchString.value)">
<option Value="1">Standard Search
<option Value="2">Exact Phrase
<option Value="3">All Words
<option Value="4">Any Words
<option Selected Value="5">Boolean 
      Search</option> 
</select>
<%End If%>



<%If SearchSet=0 Then%>
<div style="MARGIN-LEFT: -0.25in">
<font size="-1">
<ul>
<li>
Type a phrase or a question.
<li>
All forms of a word are included.
<li>Usually returns a large number of hits.</li></ul>      
</div>
</font>
<%End If%>

<%If SearchSet=1 Then%>
<div style="MARGIN-LEFT: -0.25in">
<font size="-1">
<ul>
<li>
Literal search.
<li>
Not case-sensitive; capitalization is ignored.</li></ul>
    
</div>

</font>
<%End If%>

<%If SearchSet=2 Then%>
<div style="MARGIN-LEFT: -0.25in">
<font size="-1">
<ul>
<li>
Words can be in any order.
<li>
Usually returns a smaller number of hits.</li></ul>
      
</div>
</font>
<%End If%>

<%If SearchSet=3 Then%>
<div style="MARGIN-LEFT: -0.25in">
<font size="-1">
<ul>
<li>
Topics with more occurrences of the word are listed first.
<li>
Usually returns a large number of hits.</li></ul>
      
</div>
</font>

<%End If%>

<%If SearchSet=4 Then%>
<div style="MARGIN-LEFT: -0.25in">
<font size="-1">
<ul>
<li>
AND, OR, NEAR, and AND NOT operators are supported.
<li>
Use quotation marks for literal strings.
<li>
Use parentheses to group terms.</li></ul>
    
</div>

</font>

<%End If%>


<p>


<input TYPE="hidden" NAME="CiResultsSize" value="on"><br>
<br></p></font></td></tr></form></tbody></table></font>

<br>
<br>


</body>
</html>

