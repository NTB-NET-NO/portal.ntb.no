<%@	Language=VBScript %>
<%
Option Explicit

Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

dim sHelpTopic

const L_HelpBusinessDeskHelp_HTMLTitle = "Business Desk Help"

sHelpTopic = "htm/cs_bd_gettingstarted_ypyn.htm"
if Len(Request("helptopic")) > 0 then
	sHelpTopic = "htm/" & Request("helptopic")
end if
%>
<HTML>
<HEAD>
<TITLE><%= L_HelpBusinessDeskHelp_HTMLTitle %></TITLE>
</HEAD>
<FRAMESET ROWS="52,*" FRAMEBORDER="0" FRAMESPACING="0">
	<FRAME SRC="bar.htm" scrolling="no" APPLICATION='yes'>
	<FRAMESET COLS="277, *" frameborder="1" framespacing="2" border="2" bordercolor="#d6c610">
		<FRAME SRC="TOC_com.htm" name="Contents" border="1" 
			   scrolling="no" marginwidth="4" marginheight="10" APPLICATION='yes'>
		<FRAME ID='topicframe' SRC="<%= sHelpTopic %>" name="main" border="1" bordercolor="#d6c610"
		       scrolling="auto" marginwidth="10" marginheight="1" APPLICATION='yes'>
	</FRAMESET>
</FRAMESET>
<NOFRAMES></NOFRAMES>
</HTML>