<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE="../include/DBUtil.asp" -->
<!--#INCLUDE FILE="../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="./include/shipping_strings.asp" -->
<!--#INCLUDE FILE="./include/ShippingCommon_util.asp" -->

<%
	Dim sOrderBy, xmlShippingList, g_dRequest
	' Get form values from dictionary
	Set g_dRequest = dGetRequestXMLAsDict()	
	sOrderBy = g_dRequest("column") & " ASC"
	xmlShippingList = LoadShippingListData(sOrderBy)
	Response.Write xmlShippingList
%>