<!--#INCLUDE FILE='ShippingCommon_edit.asp' -->

<%
	Function GetActionPageName()
		GetActionPageName = "ChargeByWeight_edit.asp"
	End Function
	
	Function GetActionFriendlyName()
		GetActionFriendlyName = L_WEIGHT_STATICTEXT
	End Function
		
	Function GetTitle()
		Dim sMode
		sMode = Request.Form("mode")
		Select Case sMode
			Case "new"
				GetTitle = L_WeightNew_StaticText
			Case "open"
				GetTitle = sFormatString(L_WeightOpen_StaticText, Array(Request("shipping_method_name")))
			Case "savenew"
				GetTitle = L_WeightNew_StaticText
			Case "save"
				GetTitle = sFormatString(L_WeightSave_STATICTEXT, Array(Request("shipping_method_name")))
		End Select
	End Function
	
	Sub ProcessCustomShippingData(sMode, sMethodID)
	' Process the custom part of edit form.
		Select Case sMode
			Case "new"
				g_xmlShippingRates = "<document/>"
				Session("xmlRates") = g_xmlShippingRates
			Case "open"
				g_xmlShippingRates = LoadCustomShippingData(sMethodID)
				Session("xmlRates") = g_xmlShippingRates
			Case "save"
				Call SaveCustomShippingData(sMethodID)
				g_xmlShippingRates = LoadCustomShippingData(sMethodID)
				Session("xmlRates") = g_xmlShippingRates
			Case "savenew"
				Call SaveCustomShippingData(sMethodID)
				g_xmlShippingRates = "<document/>"
				Session("xmlRates") = g_xmlShippingRates
			Case "saveback"
				Call SaveCustomShippingData(sMethodID)
				g_xmlShippingRates = "<document/>"
				Session("xmlRates") = g_xmlShippingRates
		End Select
	End Sub
		
	Function LoadCustomShippingData(sMethodID)
	' Creates xml data from recordset for rendering a dynamic table widget.
		Dim oMarshaler, rs, xmlData
		
		On Error Resume Next			
		Set oMarshaler = Server.Createobject("commerce.dyntablemarshaler")
		Call oMarshaler.Initialize(g_MSCSTransactionConfigConnStr)
		Set rs = oMarshaler.GetAsRecordSet("TableShippingRates", Array("shipping_method_id"), Array(sMethodID), Array("maxval", "cy_price")) 
		If Err <> 0 Then 
			Call SetErr(L_GetData_DialogTitle, sGetErrorById("Bad_XML_data"))
		End If
		Set xmlData = xmlGetXMLFromRSEx(rs, -1, -1, -1, null)
		LoadCustomShippingData = xmlData.xml
		Set xmlData = Nothing
		Set oMarshaler = Nothing
	End Function
	
	Sub SaveCustomShippingData(sMethodID)
		Dim sProgID, oPipeLineComponent, oConfig, oMarshaler, xmlRates, xmlDoc, xmlNode
		
		On Error Resume Next		
		' Create the appropriate pipeline component
		sProgID = "commerce.stepwiseshipping"
		Set oPipeLineComponent = Server.CreateObject(sProgID)		
		' Set the component's configuration dictionary and chargeby type
		Set oConfig = oPipeLineComponent.GetConfigData
		oConfig.mode = 2
		
		' Save the component config (note: the component must support IPipelineComponentAdmin to set a cachable config...)
		g_oShipMethodMgr.SetCachableComponentConfig sProgID, oConfig, g_sActionPage, g_sActionFriendlyPage

		' Save xml data to the database
		xmlRates = Request.Form("rates")
		If Len(xmlRates) = 0 Then
			xmlRates = Session("xmlRates")
		Else
			Set xmlDoc = server.CreateObject("MSXML.DOMDocument")
			xmlDoc.async = false
			xmlDoc.loadXML(xmlRates)
			for each xmlNode in xmlDoc.selectNodes("//cy_price")
				xmlNode.text = g_MSCSDataFunctions.ConvertStringToCurrency(xmlNode.text, g_DBCurrencyLocale)
			next
			for each xmlNode in xmlDoc.selectNodes("//maxval")
				xmlNode.text = g_MSCSDataFunctions.ConvertStringToCurrency(xmlNode.text, g_DBDefaultLocale)
			next
			xmlRates = xmlDoc.xml
			Session("xmlRates") = xmlRates
		End If
		Set oMarshaler = Server.Createobject("commerce.dyntablemarshaler")
		With oMarshaler
			.Initialize(g_MSCSTransactionConfigConnStr)
			.SaveXMLToTable xmlRates, "TableShippingRates", Array("shipping_method_id"), Array(sMethodID)
		End With
		If Err <> 0 Then
			Call setErr(L_SaveShippingMethod_DialogTitle, L_SaveShippingMethod_ErrorMessage)
		End If
		' Cleanup
		Set oPipeLineComponent = Nothing
		Set oConfig = Nothing
		Set oMarshaler = Nothing
	End Sub
%>

<SCRIPT LANGUAGE=VBScript>

	Sub onDirtyDT()
		saveform.rates.value = window.event.XMLlist.xml
	End Sub

</SCRIPT>

<BR><DIV STYLE='TEXT-INDENT: 20px'><%= L_WeightError_StaticText%></DIV>
	
		<xml id = 'esMeta'>
			<editsheet>
				<global expanded='yes'>
					<name><%= L_Rates_Text%></name>
					<key><%= L_Rates_KeyName%></key>
				</global>
				<template register='dtRates'>
					<![CDATA[
					<DIV ID='dtRates' 
						 CLASS='dynamicTable'
						 DataXML='dtRatesData' 
						 MetaXML='dtRatesMeta' 
						 LANGUAGE='VBScript' 
						 ONCHANGE='onDirtyDT()'>
					</DIV>]]>
				</template>
			</editsheet>
		</xml>
	
		<xml id='dtRatesMeta'>
			<dynamictable>
			    <global required='yes' keycol='maxval' uniquekey='yes' 
					sortbykey='yes' overx='yes'></global>
			    <fields> 
					<numeric id='maxval' width='50' required='yes' subtype='float'
						min='0' max='10000'>
						<format><%= g_sMSCSNumberFormat %></format>
						<name><%= L_WeightUpto_Text%></name>
						<prompt><%= L_TypeYourEntry_Text%></prompt>
					    <error><%= L_WeightUpto_ErrorMessage%></error>
					</numeric>
					<numeric id='cy_price' width='50' required='yes' subtype='currency'
						min='0' max='999999999'>
						<name><%= L_ShippingRate_Text%></name>
						<prompt><%= L_TypeYourEntry_Text%></prompt>
						<format><%= g_sMSCSCurrencyFormat %></format>
					    <error><%= L_ShippingRate_ErrorMessage%></error>
					</numeric>
			    </fields>
			</dynamictable>
		</xml>

		<xml id='dtRatesData'>
			<%= g_xmlShippingRates %>
		</xml>

		<DIV CLASS='editSheet' 
			 ID='esRates' 
			 MetaXML='esMeta' 
			 DataXML='dtRatesData'
			 ONCHANGE='onDirty()'
			 ONREQUIRE='onRequiredValid()'
			 ONVALID='onRequiredValid()'>
		</DIV>

<p STYLE='TEXT-INDENT: 20px'>
<%= g_imgRequired %>&nbsp;<%= L_YouMustEnterARate_Message %>
</p>		 
		
		<INPUT type='hidden' id='rates' name='rates'>
	</DIV>

</FORM>	
</BODY>
</HTML>


