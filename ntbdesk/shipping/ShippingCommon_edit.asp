<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="./include/shipping_strings.asp" -->
<!--#INCLUDE FILE="./include/ShippingCommon_util.asp"-->

<% 
	g_sActionPage = GetActionPageName()
	g_sActionFriendlyPage = GetActionFriendlyName()
	Call ProcessShippingEditForm()
%>
	
<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">
<SCRIPT LANGUAGE="VBSCRIPT">
	Sub onDirty()
		setDirty("")
	End Sub
	
	Sub onRequiredValid()
		' Sets task buttons state independent of custom area of form.
		if window.event.type = "require" then g_bRequired = esShippingCommon.required or esRates.required
		if window.event.type = "valid" then g_bValid = esShippingCommon.valid and esRates.valid
		ResetSaveBtns()
	End Sub

	Sub onSave()
		with saveform
			if window.event.srcElement Is elGetTaskBtn("save") then
				.mode.value = "save"				
			elseif window.event.srcElement Is elGetTaskBtn("savenew") then
				.mode.value = "savenew"
			elseif window.event.srcElement is elGetTaskBtn("saveback") then
				.mode.value = "saveback"
			End If
			.submit()
		end with
	End Sub

	Sub window_onload()
		esShippingCommon.focus()
	End Sub
</SCRIPT>
</HEAD>

<BODY SCROLL="NO">

<% InsertEditTaskBar GetTitle(), g_sStatusText %>

<DIV ID="editSheetContainer" CLASS="editPageContainer">

	<FORM id="saveform" method="post" ONTASK="onSave">
		<INPUT type="hidden" id="mode" name="mode">
		<INPUT type="hidden" id="MethodID" name="MethodID" value="<%= g_dShipping.MethodID %>">

		<xml id="esShippingCommonMeta">
			<editsheet>
			    <global expanded="yes">
			        <name><%= L_ShippingMethodProperties_Text%></name>
					<key><%= L_ShippingMethodProperties_KeyName%></key>
			    </global>
			    <fields>
			        <text id="shipping_method_name" required="yes" maxlen="255">
			            <name><%= L_NameLabel_Text%></name>
			            <tooltip><%= L_ShippingMethodName_ToolTip%></tooltip>
			            <prompt><%= L_EnterShippingMethodName_Text%></prompt>
			            <error><%= L_ShippingMethodName_ErrorMessage%></error>
			        </text>
			        <text id="description" maxlen="255" subtype="long">
			            <name><%= L_DescriptionLabel_Text%></name>
			            <tooltip><%= L_Description_ToolTip%></tooltip>
			            <prompt><%= L_EnterShippingDescription_Text%></prompt>
			            <error><%= L_ShippingDescription_ErrorMessage%></error>
			        </text>
			        <boolean id="enabled" default="1">
			            <name><%= L_EnabledLabel_Text%></name>
			            <tooltip><%= L_Enabled_ToolTip%></tooltip>
			            <label><%= L_EnabledPrompt_Text%></label>
			        </boolean>
			    </fields>
			</editsheet>
		</xml>

		<xml id="esShippingCommonData">
			<%= g_xmlShippingCommonData %>
		</xml>
	
		<DIV ID='esShippingCommon' 
			 CLASS='editSheet'
			 MetaXML='esShippingCommonMeta'
			 DataXML='esShippingCommonData'
			 LANGUAGE='VBSCRIPT'
			 ONCHANGE='onDirty()'
			 ONREQUIRE='onRequiredValid()'
			 ONVALID='onRequiredValid()'>
			 <%= L_LoadingProperties_Text%>
		</DIV>
