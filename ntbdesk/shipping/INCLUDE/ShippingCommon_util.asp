<%	
	Dim g_MSCSTransactionConfigConnStr, g_oShipMethodMgr
	Dim g_sActionPage, g_sActionFriendlyPage, g_sStatusText, g_dShipping
	Dim g_xmlShippingCommonData, g_xmlShippingRates
	
	' Get connection string from SiteConfig)
	g_MSCSTransactionConfigConnStr= GetSiteConfigField("transaction config", "connstr_db_TransactionConfig")   
	If Err <> 0 Then 
		Call SetErr(L_GetSiteConfig_DialogTitle, L_SiteConfig_ErrorMessage)
		Err.Clear
	End If			
	' Create and Initialize ShippingMethodManager
	Set g_oShipMethodMgr = Server.CreateObject("commerce.ShippingMethodManager")
	If Err <> 0 Then 
		Call SetErr(L_CreateSCM_DialogTitle, sGetErrorById("Cant_create_object"))
		Err.Clear
	End If	
	g_oShipMethodMgr.Initialize(g_MSCSTransactionConfigConnStr)
	
	Function LoadShippingListData(sOrderBy)		
		Dim rs, xmlData, xmlList, node, i
		On Error Resume Next
		' Create data xml from recordset returned by Shipping Component Manager
		Set rs = g_oShipMethodMgr.GetInstalledMethodList("", sOrderBy, _
								Array("shipping_method_name", "action_friendly_name", _
								"enabled", "description", "action_page", "shipping_method_id"))
		If Err = 0 Then
			Set xmlData = xmlGetXMLFromRSEx(rs, -1, -1, -1, null)
			Set xmlList = xmlData.selectNodes("//enabled")
			For Each node in xmlList
				Select Case node.text
					Case "1"
						node.text = L_Yes_Text
					Case "0"
						node.text = L_No_Text
					Case Else
						node.text = ""
				End Select
			Next
			LoadShippingListData = xmlData.xml
			Set xmlList = Nothing
		Else
			LoadShippingListData = "<document/>"
		End If
		Set rs = Nothing
		Set xmlData = Nothing
	End Function
			
	Sub ProcessShippingEditForm()
	' This uses ShippingMethodManager object. Order of method calls is as follows:
	' 1-Initialize; 2-LoadMethodInstance/CreateMethodInstance/DeleteMethodInstance; 
	' 3-GetMethodInfo/SetMethodConfig; 4-SetCachableComponentConfig; 5-SaveMethodConfig.
		
		Set g_dShipping = Server.CreateObject("Commerce.Dictionary")
		With g_dShipping
			.Mode = Request.Form("mode")
			.MethodID = GetShippingMethodID(Request.Form("MethodID"))
			.MethodName = Request.Form("shipping_method_name")
			.Enabled = Request.Form("enabled")
			.Description = Request.Form("description")
		End With
		
		' In the Save modes, 'ProcessCustomShippingData' is included in the call to 'SaveShippingMethod';
		' All calls to 'ProcessCustomShippingData' can be replaced with custom function call(s)		
		Select Case g_dShipping.Mode
			Case "new"
				g_sStatusText = ""
				g_dShipping.MethodID = ""
				g_xmlShippingCommonData = "<document/>"
				Call ProcessCustomShippingData("new", g_dShipping.MethodID)
			Case "open"
				g_sStatusText = ""
				Call LoadShippingMethod(g_dShipping.MethodID)
				Call ProcessCustomShippingData("open", g_dShipping.MethodID)
			Case "save"
				Call SaveShippingMethod(g_dShipping)
				g_oShipMethodMgr.LoadMethodInstance(g_dShipping.MethodID)
				If Err = 0 Then
					g_sStatusText = L_Save_StatusBar
				End If
				Call LoadShippingMethod(g_dShipping.MethodID)
			Case "savenew"			
				Call SaveShippingMethod(g_dShipping)
				If Err = 0 Then
					g_sStatusText = L_NewMethod_StatusBar
					g_dShipping.MethodID = ""
					g_xmlShippingCommonData = "<document/>"
				End If
			Case "saveback"
				Call SaveShippingMethod(g_dShipping)
				If Err = 0 Then
					g_sStatusText = L_NewMethod_StatusBar
					g_dShipping.MethodID = ""
					g_xmlShippingCommonData = "<document/>"
				End If
		End Select
		If err <> 0 Then
			g_sStatusText = L_Error_StatusBar
			g_dShipping.MethodID = ""
		End If
	End Sub				
	
	Function GetShippingMethodID(sMethodID)	
	' Create a new methodID or load an existing one.
		If sMethodID = "" or sMethodID = "0" Then
			GetShippingMethodID = g_oShipMethodMgr.CreateMethodInstance(True)
			If Err <> 0 Then 
				Call SetErr(L_CreateInstance_DialogTitle, L_CreateInstance_ErrorMessage)
			End If				
		Else
			GetShippingMethodID = sMethodID
			g_oShipMethodMgr.LoadMethodInstance(sMethodID)	
			If Err <> 0 Then 
				Call SetErr(L_LoadInstance_DialogTitle, L_LoadInstance_ErrorMessage)
			End If
		End If
	End Function
	
	Sub LoadShippingMethod(sMethodID)
	' Create xml data for edit sheet.
		Dim rs, xmlData		
		Set rs = g_oShipMethodMgr.GetMethodInfo(Array("shipping_method_name", "description", "enabled"))
		Set xmlData = xmlGetXMLFromRSEx(rs, -1, -1, -1, null)
		g_xmlShippingCommonData = xmlData.xml
	End Sub
		
	Sub SaveShippingMethod(g_dShipping)
	'Save shipping method data.
		On Error Resume Next
		g_oShipMethodMgr.SetMethodConfig(Array("shipping_method_name", g_dShipping.MethodName, _
											"description", g_dShipping.Description, _
											"enabled", g_dShipping.Enabled))
		If Err <> 0 Then 
			Call SetErr(L_SaveCommonData_DialogTitle, L_SaveCommonData_ErrorMessage)
		Else
			' Custom function (ProcessCustomShippingData) has to be called BEFORE the SaveMethodConfig
			' to avoid a 'saved twice' error in the object
			Call ProcessCustomShippingData(g_dShipping.mode, g_dShipping.MethodID)
			If err = 0 Then
				g_oShipMethodMgr.SaveMethodConfig 
				If Err <> 0 Then 
					Call setErr(L_SaveMethodConfig_DialogTitle, L_SaveMethodConfig_ErrorMessage)
				End If
			End If
		End If
	End Sub
	
	Sub DeleteCustomShippingData(sMethodID)
	' Deletes related data from TableShippingRates; replace with custom function if required.
		Dim oMarshaler		

		Set oMarshaler = Server.CreateObject("commerce.dyntablemarshaler")
		With oMarshaler
			.Initialize(g_MSCSTransactionConfigConnStr)
			.Delete "TableShippingRates", Array("shipping_method_id"), Array(sMethodID)
		End With
		If Err <> 0 Then
			Call SetErr(L_DeleteShippingMethod_DialogTitle, L_DeleteShippingMethod_ErrorMessage)
		End If
		Set oMarshaler = Nothing
	End Sub
	
	Sub SetErr(sTitle, sMessage)
		Call setError(sTitle, sMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
	End Sub
%>