<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version


'List page Constants
Const L_ShippingMethods_StaticText = "Shipping Methods"
Const L_Method_Text = "Method"
Const L_ShippingType_Text = "Shipping Type"
Const L_Yes_Text = "Yes"
Const L_No_Text = "No"
Const L_LoadingList_Text = "Loading list, please wait..."

'Common Edit Sheet Constants
Const L_ShippingMethodProperties_Text = "&lt;U&gt;S&lt;/U&gt;hipping Method Properties"
Const L_ShippingMethodProperties_KeyName = "s"
Const L_Name_Text = "Name"
Const L_ShippingMethodName_ToolTip = "Shipping Method Name"
Const L_Description_ToolTip = "Shipping Method Description"
Const L_Enabled_ToolTip = "Shipping method enabled or not"
Const L_EnabledPrompt_Text = "Shipping method enabled"

'Const L_SaveConfirmationDialog_Text = "Save the shipping method before exiting?"
Const L_Required_Text = "Cannot save until all required fields have values."
Const L_Valid_Text = "Cannot save until all required fields have values and invalid fields are corrected."
const L_YouMustEnterARate_Message = "You must enter a rate value in the last row before you can save."
Const L_LoadingProperties_Text = "Loading properties, please wait..."

'Prompt Text Common Edit Sheet
Const L_EnterShippingMethodName_Text = "Enter a Shipping Method Name"
Const L_EnterShippingDescription_Text = "Enter a Shipping Method Description"
'Const L_SelectOption_Text = "Select an option"

'Error Messages Common Edit Sheet
Const L_ShippingMethodName_ErrorMessage = "The Name field must be less than 255 characters and cannot contain '&lt;', '&gt;', or '&amp;'."
Const L_ShippingDescription_ErrorMessage = "The Description field must be less than 255 characters and cannot contain  '&lt;', '&gt;', or '&amp;'."

'Custom Edit Sheet Constants
Const L_Weight_StaticText = "Charge by Weight"
Const L_Quantity_StaticText = "Charge by Quantity"
Const L_SubTotal_StaticText = "Charge by SubTotal"

Const L_WeightNew_StaticText = "Charge by Weight - New"
Const L_WeightOpen_StaticText = "Charge by Weight - Open %1"
Const L_WeightSave_StaticText = "Charge by Weight - Saved %1"
Const L_QuantityNew_StaticText = "Charge by Quantity - New"
Const L_QuantityOpen_StaticText = "Charge by Quantity - Open %1"
Const L_QuantitySave_StaticText = "Charge by Quantity - Saved %1"
Const L_SubTotalNew_StaticText = "Charge by SubTotal - New"
Const L_SubTotalOpen_StaticText = "Charge by SubTotal - Open %1"
Const L_SubTotalSave_StaticText = "Charge by SubTotal - Saved %1"


Const L_Rates_Text = "&lt;U&gt;R&lt;/U&gt;ates"
Const L_Rates_KeyName = "r"
Const L_WeightUpto_Text = "Weight, up to:"
Const L_QuantityUpto_Text = "Quantity, up to:"
Const L_PriceUpto_Text = "Price, up to:"
Const L_ShippingRate_Text = "Shipping Rate:"

'Custom Edit Sheet Prompt text
Const L_TypeYourEntry_Text = "Type your entry"

'Custom Edit Sheet Error Messages
Const L_WeightUpto_ErrorMessage = "Weight should be a positive decimal number between 0 and 10,000."
Const L_QuantityUpto_ErrorMessage = "Quantity should be a positive decimal number between 0 and 10,000."
Const L_PriceUpto_ErrorMessage = "Price must be positive and cannot exceed 999,999,999." 
Const L_ShippingRate_ErrorMessage = "Shipping Rate must be positive and cannot exceed 999,999,999."

'Constants for List column headings
Const L_Description_Text = "Description"
Const L_Enabled_Text = "Enabled"

'Constants for Edit sheet labels
Const L_NameLabel_Text = "Name:"
Const L_DescriptionLabel_Text = "Description:"
Const L_EnabledLabel_Text = "Enabled:"

'Status text
Const L_NewMethod_StatusBar = "New Shipping Method Opened"
Const L_Open_StatusBar = "Opened: [%1]"
Const L_Save_StatusBar = "Shipping Method Saved"
Const L_NoItemsSelected_StatusBar = "No Items Selected"
Const L_Error_StatusBar = "Error"
Const L_Delete_StatusBar = "Shipping Method deleted"
Const L_ErrorDelete_StatusBar = "Error Deleting Shipping Method"
Const L_OneItemSelected_StatusBar = "1 Item Selected [ %1 ]"
Const L_Delete_Message = "Delete %1?"	
Const L_WeightError_StaticText = "The 'Charge by Weight' Shipping Method requires that all catalog products specify a weight." 
'Error Handling Constants
Const L_GetSiteConfig_DialogTitle = "Get SiteConfig Field"
Const L_SiteConfig_ErrorMessage = "An error occurred while retrieving the configuration information. Please contact your system administrator."
Const L_CreateSCM_DialogTitle = "Create Component"
Const L_InitializeSCM_DialogTitle = "Initialize Component"
Const L_GetData_DialogTitle = "Get Data"
Const L_CreateInstance_DialogTitle = "Create Instance"
Const L_CreateInstance_ErrorMessage = "An error occurred while creating an instance of the Shipping Method."
Const L_LoadInstance_DialogTitle = "Load Instance"
Const L_LoadInstance_ErrorMessage = "An error occurred while loading the Shipping Method."
Const L_SaveMethodConfig_DialogTitle = "Save Method Config"
Const L_SaveMethodConfig_ErrorMessage = "An error occurred while saving configuration information for the Shipping Method."
Const L_DeleteMethod_DialogTitle = "Delete?"
Const L_DeleteMethodInstance_ErrorMessage = "An error occurred while deleting the Shipping method."
Const L_SaveCommonData_DialogTitle = "Save Common Data"
Const L_SaveCommonData_ErrorMessage = "An error occurred while saving common data."
Const L_SaveShippingMethod_ErrorMessage = "An error occurred while saving the Shipping method."
Const L_SaveShippingMethod_DialogTitle = "Save Shipping Method"
Const L_DeleteShippingMethod_ErrorMessage = "An error occurred while deleting the Shipping rates."
Const L_DeleteShippingMethod_DialogTitle = "Delete Shipping Method"

%>