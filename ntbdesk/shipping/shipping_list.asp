<!--#INCLUDE FILE="../Include/BDHeader.asp" -->
<!--#INCLUDE FILE="../Include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="./Include/shipping_strings.asp" -->
<!--#INCLUDE FILE="./Include/ShippingCommon_util.asp" -->

<% 
	Dim g_xmlShippingListData
	Call ProcessShippingListForm()
	
	Sub ProcessShippingListForm()
		Dim sMethodID, sOrderBy		
		
		If Request.Form("mode") = "delete" Then
			sMethodID = Request.Form("MethodID")
			Call DeleteShippingMethod(sMethodID)
			Call DeleteCustomShippingData(sMethodID)
		End If
		sOrderBy = Session("OrderBy")
		If IsEmpty(sOrderBy) Then 
			sOrderBy = "shipping_method_name ASC"
			Session("OrderBy") = sOrderBy
		End If		
		g_xmlShippingListData = LoadShippingListData(sOrderBy)
	End Sub
		
	Sub DeleteShippingMethod(sMethodID)
		g_oShipMethodMgr.LoadMethodInstance(sMethodId)
		If Err = 0 then
			g_oShipMethodMgr.DeleteMethodInstance()
			If Err <> 0 Then
				Call setErr(L_DeleteMethodInstance_DialogTitle, L_DeleteMethodInstance_ErrorMessage)
			End If
		Else
			Call setErr(L_LoadInstance_DialogTitle, L_LoadInstance_ErrorMessage)
		End If
		If err = 0 Then
			g_sStatusText = L_Delete_StatusBar
		Else
			g_sStatusText = L_ErrorDelete_StatusBar
		End If
	End Sub
%>

<HTML>
<HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="../Include/bizdesk.css" ID="mainstylesheet">

<SCRIPT LANGUAGE="VBScript">

	Option Explicit
	
	Sub onSelectRow()    		
		dim oSelection, sItem
		' get XML node for the selected row
		set oSelection = window.event.XMLrecord
		' copy values into selectform
		with selectform
			.shipping_method_name.value = oSelection.selectSingleNode("shipping_method_name").text
			.MethodID.value = oSelection.selectSingleNode("shipping_method_id").text
			.ActionPage.value = oSelection.selectSingleNode("action_page").text
		end with 
		' set status bar text
		sItem = oSelection.selectSingleNode("./shipping_method_name").text
		setStatusText sFormatString("<%= L_OneItemSelected_StatusBar%>", Array(sItem))               
		' handle task buttons and status bar
        EnableTask "delete"
        EnableTask "open"
	End Sub
	
	Sub onUnselectRow()
		dim oSelection, i
	    ' get XML node for row unselected
	    set oSelection = window.event.XMLrecord
	    ' reset selectform values		
		with selectform
			.shipping_method_name.value = ""
			.MethodID.value = "" 
		end with       
        ' handle task buttons
		DisableTask "delete"
		DisableTask "open"
		setStatusText "<%= L_NoItemsSelected_StatusBar%>"
    End Sub
			
    Sub onTask()        
		Dim sTitle, sPrompt, sMethod, sResponse
		
		sTitle = "<%= L_DeleteMethod_DialogTitle %>"
		if window.event.srcElement Is elGetTaskBtn("delete") then
			sMethod = selectform.shipping_method_name.value
			sPrompt = sFormatString("<%= L_Delete_Message%>",Array(sMethod)) 
			sResponse = MsgBox(sPrompt, vbYesNo, sTitle)		
			if sResponse = vbYes then
				with selectform
					.mode.value = "delete"
					.submit()
				end with
			Else
				EnableTask "delete"
				EnableTask "open"
				EnableTask "new"
			end if
		elseif window.event.srcElement Is elGetTaskBtn("open") then
			with selectform
				.mode.value = "open"
				.action = .ActionPage.value
				.submit()
			end with
		else
			with selectform
				.mode.value = "new"
				.submit()
			end with
		end if
    End Sub
					
	Sub window_onLoad()
		' handle task buttons
        DisableTask "open"
        DisableTask "delete"
        EnableAllTaskMenuItems "new", true
    End Sub

</SCRIPT>
</HEAD>

<BODY SCROLL=no LANGUAGE="VBScript">

<% InsertTaskBar L_ShippingMethods_StaticText, g_sStatusText %>
		
	<xml id="lsShippingMeta">
		<listsheet>
		    <global pagecontrols="no" sort="yes" selection="single"/>
		    <columns>
	   	        <column id="shipping_method_name" width="25"><%= L_Name_Text%></column>
	   	        <column id="action_friendly_name" width="25"><%= L_ShippingType_Text%></column>
				<column id="enabled" width="20"><%= L_Enabled_Text%></column>
				<column id="description" width="30"><%= L_Description_Text%></column>
				<column id="action_page" hide="yes"></column>
		        <column id="shipping_method_id" hide="yes"></column>    
		    </columns>
		    <operations>
				<sort formid="sortform"/>
				<delete formid="selectform"/>
			</operations>
		</listsheet>
	</xml>

	<xml id="lsShippingData"> 
		<%= g_xmlShippingListData %>
	</xml>
	
	<DIV id="bdcontentarea">
		<DIV id="lsShippingMethods" CLASS="listSheet"
			 STYLE="Margin:20px;HEIGHT:80%"
			 DataXML="lsShippingData"
			 MetaXML="lsShippingMeta"
			 LANGUAGE="VBScript"
			 OnRowSelect="onSelectRow()"
			 OnRowUnselect="onUnselectRow()"
			 <%= L_LoadingList_Text%>
		</DIV>	
	</DIV>
			 
	<FORM id ='selectform' method='post' ONTASK='onTask()'>
		<INPUT type='hidden' id='mode' name='mode'>
		<INPUT type='hidden' id='shipping_method_name' name='shipping_method_name'>
		<INPUT type='hidden' id='MethodID' name='MethodID'>
		<INPUT type='hidden' id='ActionPage' name='ActionPage'>
	</FORM>

	<FORM id='sortform' method='post' action='shipping_event.asp'>
	</FORM>

</BODY>
</HTML>
