<OBJECT RUNAT=Server SCOPE=Application ID=g_MSCSDataFunctions	PROGID="Commerce.DataFunctions"></OBJECT>
<OBJECT RUNAT=Server SCOPE=Application ID=g_MSCSAdminEventLog	PROGID="Commerce.AdminEventLog"></OBJECT>
<OBJECT RUNAT=Server SCOPE=Application ID=g_MSCSMessageManager	PROGID="Commerce.MessageManager"></OBJECT>
<OBJECT RUNAT=Server SCOPE=Application ID=g_MSCSBizDeskSecurity PROGID="Commerce.BizDeskSecurity"></OBJECT>
<OBJECT RUNAT=Server SCOPE=Application ID=g_MSCSConfiguration	PROGID="Commerce.SiteConfigReadOnly"></OBJECT>
<OBJECT RUNAT=Server SCOPE=Application ID=g_MSCSAppConfig		PROGID="Commerce.AppConfig"></OBJECT>

<SCRIPT LANGUAGE=VBScript RUNAT=Server>
    Option Explicit

	Const PRODUCTION  = 1
	Const DEVELOPMENT = 0

	Dim MSCSEnv, VERBOSE_OUTPUT, SHOW_OBJECT_ERRORS, AUTO_REDIRECT_ENABLED, _
		FORCE_HTA_ONLY, ALLOW_CONTEXT_MENUS, SHOW_DEBUG_TEXT

	MSCSEnv = PRODUCTION

	' -- BizDesk debug and control flags
	if MSCSEnv = PRODUCTION then
		' -- production settings
		VERBOSE_OUTPUT			= false		' true gives verbose output while loading objects
		SHOW_OBJECT_ERRORS		= false		' true displays all object load errors in a window after loading
		AUTO_REDIRECT_ENABLED	= true		' true forces each page to run only within BizDesk frame
		FORCE_HTA_ONLY			= true		' true displays error if BD not run as HTA
		ALLOW_CONTEXT_MENUS		= false		' true allows normal right-click context menus for debugging
		SHOW_DEBUG_TEXT			= false		' true shows debug text in status bar
	else
		' -- development settings
		VERBOSE_OUTPUT			= false	
		SHOW_OBJECT_ERRORS		= true
		AUTO_REDIRECT_ENABLED	= false
		FORCE_HTA_ONLY			= false
		ALLOW_CONTEXT_MENUS		= true
		SHOW_DEBUG_TEXT			= true
	end if

	' -- DataFunctions Constants
	const LOCALE_LANGUAGE		= 1
	const LOCALE_SSHORTDATE		= 31
	const LOCALE_IFIRSTDAYOFWEEK = 4108
	const LOCALE_INEGNUMBER		= 4112
	const LOCALE_INEGCURRENCY	= 28
	const LOCALE_SDECIMAL		= 14
	
	' -- Schema Version Increment Const
	'Version Increment
	const VER_INCREMENT = 0.01

	' -- CSAPPS.INI keys
	const CSAPPS_KEY_SITENAME		= "sitename"
	const CSAPPS_KEY_RELATIVEURL	= "relativeurl"

	' -- set the name of the site ini file
	const SITE_INITIALIZATION_FILE = "csapp.ini"

    Sub Application_OnStart
		Application("MSCSEnv") = MSCSEnv
	    Application("MSCSErrorInGlobalASA") = true
		Main()
		Application("MSCSErrorInGlobalASA") = false
    End Sub

    Sub Session_OnStart
		' --suppress all errors
		On Error Resume Next

	   'Session Flags
	    Session("MSCSProfileSchemaVer")= Application("MSCSProfileSchemaVer")
		Session("MSCSCatalogVer") = Application("MSCSCatalogVer")
		Session("MSCSUserXmlVer") = Application("MSCSUserXmlVer")
		Session("MSCSSiteTermVer")= Application("MSCSSiteTermVer")
	   'Give Catalogsets and Provider Connection Sessionscope
        Set Session("MSCSProviderConn")     = cnGetProviderConnection()
        Set Session("MSCSCatalogSets")      = rsGetCatalogSets()
    End Sub

    Sub Session_OnEnd
		' --suppress all errors
		On Error Resume Next

	   'Clear Catalogsets and Provider Connection
        Set Session("MSCSProviderConn")     = Nothing
        Set Session("MSCSCatalogSets")      = Nothing
    End Sub

    ' -- main application startup routine
    Sub Main
        Dim g_MSCSDataFunctions, g_MSCSAdminEventLog, g_MSCSMessageManager, _
			MSCSSiteName, MSCSBizDeskRoot

		GetSiteInfo MSCSSiteName, MSCSBizDeskRoot

		Application("MSCSSiteName")		= MSCSSiteName
		Application("MSCSBizDeskRoot")	= MSCSBizDeskRoot

		'Flag for Profile Schema Change . Set to True by app when Schema Changes
		'This flag used for refreshing Provider Connections which internally caches schema
		Application("MSCSProfileSchemaVer") = 1.00
		Application("MSCSCatalogVer") = 1.00
		Application("MSCSUserXmlVer") = 1.00
		Application("MSCSSiteTermVer")= 1.00
		Application("MSCSVerIncrement") =  VER_INCREMENT
		
		' -- get default locale used by DataFunctions and MessageManager,
		'    currency locale used by DataFunctions,
		'    and page encoding charset used in header files
		Application("MSCSCurrencyLocale") = sGetSiteConfigField(MSCSSiteName, "App Default Config", "i_BaseCurrencyLocale")
		Application("MSCSDefaultLocale") = sGetSiteConfigField(MSCSSiteName, "App Default Config", "i_SiteDefaultLocale")
		Application("MSCSPageEncodingCharset") = sGetSiteConfigField(MSCSSiteName, "App Default Config", "s_PageEncodingCharset")
        Set g_MSCSDataFunctions		= oInitDataFunctions()
        Set g_MSCSAdminEventLog		= oInitAdminEventLog()
		Set g_MSCSMessageManager	= oInitMessageManager()

		' -- initialize AppConfig instance for the current site
		g_MSCSAppConfig.Initialize MSCSSiteName


		Application("MSCSDateFormat")		= sGetDateFormat()
		Application("MSCSNumberFormat")		= sGetNumberFormat()
		Application("MSCSCurrencyFormat")	= sGetCurrencyFormat()
       
		'add flags to application
		Application("VERBOSE_OUTPUT")			= VERBOSE_OUTPUT
		Application("SHOW_OBJECT_ERRORS")		= SHOW_OBJECT_ERRORS
		Application("AUTO_REDIRECT_ENABLED")	= AUTO_REDIRECT_ENABLED
		Application("FORCE_HTA_ONLY")			= FORCE_HTA_ONLY
		Application("ALLOW_CONTEXT_MENUS")		= ALLOW_CONTEXT_MENUS
		Application("SHOW_DEBUG_TEXT")			= SHOW_DEBUG_TEXT
    End Sub

    ' -- Initialize Data Functions with locale:
	Function oInitDataFunctions()
		' -- Create a data functions object for validation
		g_MSCSDataFunctions.Locale = Application("MSCSDefaultLocale")
		Set oInitDataFunctions = g_MSCSDataFunctions
	End Function

    ' -- Initialize Admin EventLog object:
	Function oInitAdminEventLog()
		' -- Create a Admin EventLog object for error logging
		g_MSCSAdminEventLog.Initialize(sGetComputerName())
		Set oInitAdminEventLog = g_MSCSAdminEventLog
	End Function

    ' -- Get Site Name and BizDesk root from ini file:
	sub GetSiteInfo(MSCSSiteName, MSCSBizDeskRoot)
        Dim fso, MyFile, sIniPath, sLineText, aTokens, sURL, i
		Const FOR_READING = 1, FOR_WRITING = 2, FOR_APPENDING = 8

		MSCSSiteName = ""
		MSCSBizDeskRoot = ""
		sLineText = ""
		Set fso = Server.CreateObject("Scripting.FileSystemObject")
		sURL = SITE_INITIALIZATION_FILE
		for i = 0 to 2
			sIniPath = Server.MapPath(sURL)
			if fso.FileExists(sIniPath) then exit for
			sURL = "../" & sURL
		next
		if fso.FileExists(sIniPath) then
			Set MyFile = fso.OpenTextFile(sIniPath, FOR_READING)

			do while not MyFile.AtEndOfStream
				sLineText = MyFile.readLine()
				aTokens = Split(sLineText, "=")
				select case LCase(aTokens(0))
					case CSAPPS_KEY_SITENAME
						MSCSSiteName = aTokens(1)
					case CSAPPS_KEY_RELATIVEURL
						MSCSBizDeskRoot = aTokens(1)
				end select
			loop
			MyFile.Close()
		end if
		set fso = nothing
	End sub

	' -- initialize message manager with common error messages
	Function oInitMessageManager()
	    ' -- Use locale identifier as the name for the message set 
	    g_MSCSMessageManager.AddLanguage CStr(Application("MSCSDefaultLocale")), _
	        Application("MSCSDefaultLocale")

	    g_MSCSMessageManager.DefaultLanguage = CStr(Application("MSCSDefaultLocale"))

		Const L_CantConnectDB_ErrorMessage = "A connection to the database could not be established."
		Const L_RecordChanged_ErrorMessage = "The data that the system attempted to write has been changed."
		Const L_RecordDeleted_ErrorMessage = "The record that the system attempted to update had been deleted."
		Const L_CantCreateObject_ErrorMessage = "An object could not be created."
		Const L_BadXMLData_ErrorMessage = "The data used to initialize one of the controls on this page is malformed.  Please contact your system administrator."
		Const L_BadXMLMetaData_ErrorMessage = "The initialization parameter for one of the controls on this page is malformed.  Please contact your system administrator."
		Const L_InsufficientMemory_ErrorMessage = "Insufficient memory to perform operation."
		Const L_AccessDenied_ErrorMessage = "Access denied."
		Const L_CancelFailed_ErrorMessage = "Cancel operation failed."
		Const L_ExportFailed_ErrorMessage = "Export operation failed."
		Const L_ImportFailed_ErrorMessage = "Import operation failed."
		Const L_CantCreateList_ErrorMessage = "List creation failed."
		Const L_CantAddList_ErrorMessage = "List addition failed."
		Const L_CantSubtractList_ErrorMessage = "List subtraction failed."
		Const L_CantRenameList_ErrorMessage = "Cannot rename the list."
		Const L_CantChangeDescList_ErrorMessage = "Cannot change the list description."
		Const L_CantCopyList_ErrorMessage = "Cannot copy the list."
		Const L_CantConvertList_ErrorMessage = "Cannot extract the list as a Mailing List."
		Const L_CantDeleteList_ErrorMessage = "Cannot delete the list."

	    g_MSCSMessageManager.AddMessage "Cant_connect_db",		L_CantConnectDB_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Record_changed",		L_RecordChanged_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Record_deleted",		L_RecordDeleted_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Cant_create_object",	L_CantCreateObject_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Bad_XML_data",			L_BadXMLData_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Bad_XML_metadata",		L_BadXMLMetaData_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Insufficient_memory",	L_InsufficientMemory_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Access_denied",		L_AccessDenied_ErrorMessage
	    g_MSCSMessageManager.AddMessage "Cancel_failed",		L_CancelFailed_ErrorMessage
		g_MSCSMessageManager.AddMessage "Export_failed",		L_ExportFailed_ErrorMessage
		g_MSCSMessageManager.AddMessage "Import_failed",		L_ImportFailed_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_create_list",		L_CantCreateList_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_add_list",		L_CantAddList_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_subtract_list",	L_CantSubtractList_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_rename_list",		L_CantRenameList_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_changedesc_list",	L_CantChangeDescList_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_copy_list",		L_CantCopyList_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_convert_list",	L_CantConvertList_ErrorMessage
		g_MSCSMessageManager.AddMessage "Cant_delete_list",		L_CantDeleteList_ErrorMessage
		
	    Set oInitMessageManager = g_MSCSMessageManager
	End Function    

	' -- Get field from site configuration object
	Function sGetSiteConfigField(strSiteName, strResourceName, strPropertyName)
		sGetSiteConfigField = ""
		g_MSCSConfiguration.Initialize strSiteName
		sGetSiteConfigField = g_MSCSConfiguration.Fields(CStr(strResourceName)).Value.Fields(CStr(strPropertyName)).Value
	end function

	' -- Get name of this computer
	Function sGetComputerName()
	    Dim objWshShell, objEnv
	    
		Set objWshShell = Server.CreateObject("Wscript.Shell")
		Set objEnv = objWshShell.Environment("Process")
		sGetComputerName = objEnv("COMPUTERNAME")
	End Function

	' -- Get date format
	Function sGetDateFormat()
		dim sFormat, sSeparator, aFormat, i, sfirstChar, _
			nDayIndex, nMonthIndex, nYearIndex, nStartDay
		sFormat = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_SSHORTDATE)
		sSeparator = ""
		if inStr(sFormat, ".") then
			sSeparator = "."
		elseif inStr(sFormat, "-") then
			sSeparator = "-"
		else
			sSeparator = "/"
		end if

		aFormat = split(sFormat, sSeparator)
		nDayIndex = -1
		nMonthIndex = -1
		nYearIndex = -1
		for i = 0 to 2
			sfirstChar = LCase(left(aFormat(i), 1))
			if sfirstChar = "d" then
				nDayIndex = i
				aFormat(i) = "dd"
			elseif sfirstChar = "m" then
				nMonthIndex = i
				aFormat(i) = "mm"
			elseif sfirstChar = "y" then
				nYearIndex = i
				aFormat(i) = "yyyy"
			end if
		next
		sFormat = join(aFormat, sSeparator)

		nStartDay = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_IFIRSTDAYOFWEEK)
		nStartDay = nStartDay + 1
		if nStartDay > 6 then nStartDay = 0	'Sunday should be 0 instead of 6

		Application("MSCSDayIndex") =		nDayIndex
		Application("MSCSMonthIndex") =		nMonthIndex
		Application("MSCSYearIndex") =		nYearIndex
		Application("MSCSDateSeparator") =	sSeparator
		Application("MSCSWeekStartDay")	=	nStartDay
		sGetDateFormat = sFormat
	End Function

	' -- Get number format string
	Function sGetNumberFormat()
	    Dim sFormat, sDecimal

		sFormat = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_INEGNUMBER, Application("MSCSDefaultLocale"))
		sDecimal = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_SDECIMAL, Application("MSCSDefaultLocale"))
		select case sFormat
			case 0
				sFormat = "(1.1)"
			case 1, 2
				sFormat = "-1.1"
			case 3, 4
				sFormat = "1.1-"
		end select

		sGetNumberFormat = replace(sFormat, ".", sDecimal)
	End Function

	' -- Get currency format string
	Function sGetCurrencyFormat()
	    Dim sFormat, sDecimal

		sFormat = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_INEGCURRENCY, Application("MSCSCurrencyLocale"))
		sDecimal = g_MSCSDataFunctions.GetLocaleInfo(LOCALE_SDECIMAL, Application("MSCSCurrencyLocale"))
		select case sFormat
			case 0, 4, 14, 15
				sFormat = "(1.1)"
			case 1, 2, 5, 8, 9, 12
				sFormat = "-1.1"
			case 3, 6, 7, 10, 11, 13
				sFormat = "1.1-"
		end select

		sGetCurrencyFormat = replace(sFormat, ".", sDecimal)
	End Function
	
	Function cnGetProviderConnection()
		Dim cn
		Dim connstr
		' -- ADO object state values
		const AD_STATE_CLOSED	= &H00000000
	
		connstr= g_MSCSConfiguration.Fields("Biz Data Service").Value.Fields("s_CommerceProviderConnectionString").Value 
		set cn = Server.CreateObject("ADODB.Connection")
	
		if (cn.State = AD_STATE_CLOSED) then
			cn.Open CStr(connstr)
			set cnGetProviderConnection = cn
		end if
	End Function
	
	
	Function rsGetCatalogSets()
		Dim objCatalogSets
		Dim mscsCatalogConnStr,mscsTransactionConfigConnStr	
		'Keep this in the Session and refresh with publish profiles 
		mscsTransactionConfigConnStr = g_MSCSConfiguration.Fields("Transaction Config").Value.Fields("connstr_db_TransactionConfig").Value
		mscsCatalogConnStr = g_MSCSConfiguration.Fields("product catalog").Value.Fields("connstr_db_Catalog").Value
		set objCatalogSets = Server.CreateObject("Commerce.CatalogSets")
		objCatalogSets.Initialize mscsCatalogConnStr, mscsTransactionConfigConnStr
			
		set rsGetCatalogSets = objCatalogSets.GetCatalogSets
		Set objCatalogSets = Nothing
	End Function

</SCRIPT>
 
