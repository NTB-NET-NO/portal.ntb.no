<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='refreshstrings.asp' -->
<%
' global objects/variables
dim g_rsQuery, g_sQuery, g_sEntity, g_sStatusText, g_sPageTitle, _
	g_sTableName, g_sSortCol, g_sFindID, g_nCachedItems

'Added for selecting the appropriate resource
dim g_MSCSConnStr, g_sResource

 '********* SERVER SIDE CODE FOR DATA PROCESSING ************** 
'Start- Added to make the refresh page work according to the resource
 g_sResource = LCase(Trim(Request.QueryString("resource")))
 
Select Case g_sResource
	Case "catalogcache"
		g_sTableName = "catalogcache_virtual_directory"
		g_MSCSConnStr= GetSiteConfigField("transaction config", "connstr_db_TransactionConfig")
		g_sPageTitle = L_RefreshLabelCatalogs_Text
	Case "transactionconfig"
		g_sTableName = "txvirtual_directory"
		g_MSCSConnStr= GetSiteConfigField("transaction config", "connstr_db_TransactionConfig")   
		g_sPageTitle = L_RefreshLabelTransactions_Text
	Case "campaigns"
		g_sTableName = "virtual_directory"
		g_MSCSConnStr = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
		g_sPageTitle = L_RefreshLabelCampaigns_Text
End Select 

call Main

'END INLINE CODE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

sub Main
	dim sAction, sApplications, sCacheNames, sURLReturnTo

	' get actiontype, application and cache_name from Query String
	sAction = Request("actiontype")
	sApplications = Request("app_list")
	sCacheNames = Request("cache_list")
	sURLReturnTo = Request("urlreturnto")
	if sAction = "delete" then
		call DeleteApplication(sApplications, sCacheNames)
	elseif sAction = "refresh" or not isEmpty(session("prCacheNames")) then
		call RefreshCache(sApplications, sCacheNames, sURLReturnTo)
	end if
	g_sStatusText = session("prStatus")
	session("prStatus") = empty
end sub	

sub RefreshCache(sApplications, sCacheNames, sURLReturnTo)
	dim sCacheName, sApplication, nLength, aApplications

	'check if new post
	if isEmpty(sCacheNames) then
		'stop if no more items in session
		if isEmpty(session("prCacheNames")) then exit sub
		'otherwise get rest of list
		sApplications = session("prApplications")
		sCacheNames = session("prCacheNames")
		sURLReturnTo = session("prURLReturnTo")
	end if

	'get first item in each list
	sCacheName = split(sCacheNames, ",")(0)
	aApplications = split(sApplications, ",")
	sApplication = aApplications(0)

	'length of cachenames past current name
	nLength = len(sCacheNames) - len(sCacheName) - 1
	if nLength < 1 then
		'if this is the last one then clear the session items
		session("prCacheNames") = empty
		session("prApplications") = empty
		session("prURLReturnTo") = empty
	else
		'remove first item in each list and save in session
		session("prCacheNames") = Right(sCacheNames, nLength)
		nLength = len(sApplications) - len(sApplication) - 1
		session("prApplications") = Right(sApplications, nLength)
		session("prURLReturnTo") = sURLReturnTo
	end if

	if isEmpty(session("prStatus")) then
		if UBOUND(aApplications) + 1 > 1 then
			session("prStatus") = sFormatString(L_RefreshedApplications_Text, array(UBOUND(aApplications) + 1))
		else
			session("prStatus") = L_RefreshedOneApplication_Text
		end if
	end if
	'refresh the current cache
	Response.Redirect sApplication & "/BDRefresh.asp?cache_name=" & sCacheName & "&return_to=" & server.urlEncode(sURLReturnTo)
end sub

sub DeleteApplication(sApplications, sCacheNames)
	dim sSQLDeleteApplication, oCmd, sSQL, oParams, oParmApplication, oParmCacheName, _
		aCacheNames, aApplications, n, oConn
 	
	' open an ADO connection
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open(g_MSCSConnStr)

	aCacheNames = split(sCacheNames, ",")
	aApplications = split(sApplications, ",")
	sSQLDeleteApplication = _
	"DELETE FROM " &  g_sTableName & " WHERE u_vdir_url = ? AND u_vdir_cache_name = ?"
	
	' create ADO command
	set oCmd = Server.CreateObject("ADODB.Command")
	oCmd.CommandText = sSQLDeleteApplication
	oCmd.ActiveConnection = oConn

	' set up ADO parameters
	set oParams = oCmd.Parameters
	set oParmApplication = oCmd.CreateParameter("u_vdir_url", AD_BSTR, AD_PARAM_INPUT)
	set oParmCacheName = oCmd.CreateParameter("u_vdir_cache_name", AD_BSTR, AD_PARAM_INPUT)
	oParams.Append(oParmApplication)
	oParams.Append(oParmCacheName)

	'loop for each cache to delete
	for n = 0 to UBOUND(aCacheNames)

		oParmApplication.Size = len(aApplications(n))
		oParmApplication.Value = aApplications(n)
		oParmCacheName.Size = len(aCacheNames(n))
		oParmCacheName.Value = aCacheNames(n)

		' execute the command
		call oCmd.Execute '(null, null, AD_EXECUTE_NO_RECORDS)
	next

	if isEmpty(session("prStatus")) then
		if UBOUND(aApplications) + 1 > 1 then
			session("prStatus") = sFormatString(L_DeletedApplications_Text, array(UBOUND(aApplications) + 1))
		else
			session("prStatus") = L_DeletedOneApplication_Text
		end if
	end if
	set oConn = nothing
	set oCmd = nothing
end sub

sub TableGeneration
	dim sTarget, nStartSearchPosition, sSQLQueryApplications, xmlData

	'Unwanted string removal process
	sSQLQueryApplications = "SELECT " 
	sSQLQueryApplications = sSQLQueryApplications & " u_vdir_url AS application, u_vdir_cache_name AS cache_name, dt_vdir_last_refreshed AS last_refreshed "
	sSQLQueryApplications = sSQLQueryApplications & " FROM " &  g_sTableName
	
    'Check the lookforid property
    g_sFindID = ""
    if not IsEmpty(Request.Form("lookforid")) then
		g_sFindID = Request.Form("lookforid")
		Session("prlookforid") = g_sFindID
	ElseIf not IsEmpty(Session("prlookforid")) then
		g_sFindID = Session("prlookforid")
	End If	

    'If findby invoked do further SQL process
	if g_sFindID <> ""  then
		sTarget = ","
		nStartSearchPosition = inStr(g_sFindID, sTarget) + 1
		'In Case User select NULL in FindBy and default FindBy is Null so we need Null checking process
	    if trim(UCase(mid(g_sFindID, nStartSearchPosition))) <> "ALL" then
			'SQL reSetting
 			sSQLQueryApplications = sSQLQueryApplications & "  WHERE UPPER(u_vdir_cache_name) = '" & trim(UCase(mid(g_sFindID, nStartSearchPosition))) & "' "
 		end if
    end if
    
    'Check the sortcolumn property
    if not IsEmpty(Request.Form("SortCol")) then
		g_sSortCol = Request.Form("SortCol")
		Session("prSortCol") = g_sSortCol
	ElseIf not IsEmpty(Session("prSortCol")) then
		g_sSortCol = Session("prSortCol")
	End If	
	if g_sSortCol <> "" then 
		'Making sure passed value is not from other modules
		if g_sSortCol = "cache_name" or g_sSortCol = "application"  or g_sSortCol = "last_refreshed" then
			sSQLQueryApplications = sSQLQueryApplications & " ORDER BY " & g_sSortCol 
			g_sStatusText = sFormatString(L_SortedBy_Text, array(g_sSortCol))
		end if
	end if	
	Response.write "<?xml version=""1.0"" ?>"
	Set xmlData = xmlGetXMLFromQuery(g_MSCSConnStr, sSQLQueryApplications)
	g_nCachedItems = xmlData.GetAttribute("recordcount")
	Response.Write xmlData.xml
end sub
%>

<html>
<head>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<SCRIPT LANGUAGE="VBScript">
<!--	
	option explicit

	dim dSelected

	set dSelected = CreateObject("Scripting.Dictionary")

	Sub SelectRefresh
		dim xmlsource, sStatus, sCacheName, sApplication

		set xmlsource = window.event.XMLrecord	

		'add to selected
		sCacheName = Trim(xmlsource.selectSingleNode("cache_name").text)
		sApplication = Trim(xmlsource.selectSingleNode("application").text)
		if not dSelected.exists(sCacheName & "|" & sApplication) then dSelected.add sCacheName & "|" & sApplication, sApplication

		'display the status text
		if dSelected.Count = 1 then
			sStatus =  sFormatString("<%= L_PRSelectItem_Text %>", Array(sCacheName))
		else
			sStatus =  sFormatString("<%= L_PRSelectedXItems_Text %>", Array(dSelected.Count))
		end if
		SetStatusText(sStatus)

		'Enable buttons
		EnableTask "refresh"
		EnableTask "delete"
	End sub

	Sub UnselectRefresh
		dim xmlsource, sCacheName, sApplication, x, sStatus, aKeys

		'remove from selected
		set xmlsource = window.event.XMLrecord
		sCacheName = Trim(xmlsource.selectSingleNode("cache_name").text)
		sApplication = Trim(xmlsource.selectSingleNode("application").text)
		if dSelected.exists(sCacheName & "|" & sApplication) then dSelected.remove(sCacheName & "|" & sApplication)

		'display the status text
		if dSelected.Count = 1 then
			aKeys = dSelected.Keys
			sStatus =  sFormatString("<%= L_PRSelectItem_Text %>", Array(aKeys(0)))
		elseif dSelected.Count > 1 then
			sStatus =  sFormatString("<%= L_PRSelectedXItems_Text %>", Array(dSelected.Count))
		else		'count = 0
			sStatus =  "<%= L_PRNoItemsSelectedItems_Text %>"
		end if
		SetStatusText(sStatus)

		if dSelected.Count = 0 then
			'Disable buttons
			DisableTask "refresh"
			DisableTask "delete"
		end if
	End Sub

	Sub UnselectAllRefresh
		dim sStatus

		'clear selected
		set dSelected = CreateObject("Scripting.Dictionary")

		'display the status text
		sStatus =  "<%= L_PRNoItemsSelectedItems_Text %>"
		SetStatusText(sStatus)

		'Disable buttons
		DisableTask "refresh"
		DisableTask "delete"
	End Sub

	Sub GetCacheAndAppLists(sCacheList, sAppList)
		dim sCacheName, aCacheNames, aApplications, n
		sCacheList = ""
		sAppList = ""
		aCacheNames = dSelected.keys
		aApplications = dSelected.items
		for n=0 to UBOUND(aCacheNames)
			if n <> 0 then
				sCacheList = sCacheList & ","
				sAppList = sAppList & ","
			end if
			sCacheList = sCacheList & split(aCacheNames(n), "|")(0)
			sAppList = sAppList & aApplications(n)
		next
	End Sub

	Sub RefreshClick()
		dim sCacheList, sAppList

		'Getting the nessassary info
		call GetCacheAndAppLists(sCacheList, sAppList)
		refreshform.cache_list.value = sCacheList
		refreshform.app_list.value = sAppList
		refreshform.Submit()	
	End Sub

	Sub DeleteClick
		dim sCacheList, sAppList

		'Getting the nessassary info
		call GetCacheAndAppLists(sCacheList, sAppList)
		deleteform.cache_list.value = sCacheList
		deleteform.app_list.value = sAppList
		deleteform.Submit()	   
	End Sub

	sub FindByClick()
		findbyform.action = findbyform.action & "&findby=open"
	end sub

	sub OnHeaderClick()
		findbyform.Sortcol.value = window.event.sortcol
		if bdfindby.style.display <> "none" then
			'add findby=open to action if currently open
			findbyform.action = findbyform.action & "&findby=open"
		end if
		findbyform.submit()
	end sub  

	Sub Initialize
		EnableTask "find"
<%	if not IsEmpty(Request("findby")) then
%>		ShowFindBy()
<%	end if
%>	End sub
-->
</script>
</head>
<body SCROLL=no LANGUAGE='VBScript' ONLOAD='Initialize'>
<%
	InsertTaskBar g_sPageTitle, g_sStatusText	
%>

<xml id='RefreshMeta'>
	<listsheet>
	   <global selectall='no' curpage='1' pagesize='20' selection='multi' selectionbuttons='no'/>
		<columns>
	        <column id='cache_name' width='33'><%= L_ProductionCacheName_Text %></column>
	        <column id='application' width='33'><%= L_ProductionApplication_Text %></column>
	        <column id='last_refreshed' width='33'><%= L_ProductionLastRefreshed_Text %></column>
	    </columns>
	</listsheet>
</xml>

<xml id='RefreshDATA'> 
<% TableGeneration %>
</xml> 

<xml id="efMeta">
	<fields>
	    <select id="lookforid" default='ALL'>
		    <select id="lookforid">
				<option value='ALL'><%= L_ProductionAll_Text %></option>
				<option value='Advertising'><%= L_ProductionAdOption_Text %></option>
				<option value='Discounts'><%= L_ProductionDiscOption_Text %></option>
			</select>
	    </select>
	</fields>
</xml>

<xml id="efData">
	<document><record><lookforid><%= g_sFindID %></lookforid></record></document>
</xml>

<DIV ID="bdfindbycontent" CLASS='findByContent'>
	<FORM ID='findbyform' name='findbyform' ACTION='refresh.asp?resource=<%= g_sResource %>' method='POST'>
	    <INPUT TYPE='hidden' ID='SortCol' NAME='SortCol' value='<%= g_sSortCol %>'>
		<table style='margin-top: 5px'>
		<tr>
			<td width="10">&nbsp;</td>
			<td width="100"><%= L_ProductionLookFor_Text %></td>
			<td width="100"> 
				<div ID="lookforid" class="editField"
					MetaXML="efMeta" DataXML="efData"></div>
			</td>
			<td width="200">
				<input type="submit" value="<%= L_ProductionFindNow_Text %>" class='bdbutton'
					language=VBScript onclick='FindByClick'>
			</td> 
		</tr>	
		</table>
   </FORM>
</DIV>

<div id="bdcontentarea">	

<%	If g_sResource <> "transactionconfig" Then
%>		<div ID='filtertext' class='filtertext' style='MARGIN-TOP:20px; MARGIN-BOTTOM:0'><%= L_BDSSFilter_Text %></div>
		<p style="MARGIN-LEFT: 20px; MARGIN-TOP: 0; MARGIN-BOTTOM: 3px"><%= L_ProductionItems_Text %></p>
<%	else
%>		<p style="MARGIN-LEFT: 20px; MARGIN-TOP: 20px; MARGIN-BOTTOM: 3px"><%= L_ProductionItems_Text %></p>
<%	End If
%>
	<DIV ID='lsRefresh' STYLE="HEIGHT: 80%; MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px"
		CLASS='listSheet'
		DataXML='RefreshDATA'
		MetaXML='RefreshMETA'
		LANGUAGE='VBScript'
		OnRowSelect='SelectRefresh()'
		OnRowUnselect='UnselectRefresh()'
		OnAllRowsUnselect='UnselectAllRefresh()'
		OnHeaderClick='OnHeaderClick'
		ONNEWPAGE='CMNewPage()'><%= L_ProductionLoadingWidgets_Text %></DIV>
</div>

<SCRIPT LANGUAGE="VBScript">
<!--	
	if "<%= g_nCachedItems %>" = "0" then
		setStatusText("<%= L_NoCachedItems_Text %>")
	end if
-->
</SCRIPT>

<FORM id="selectform" name="selectform" >
	<INPUT type="hidden" id="cache_list" name="cache_list">
	<INPUT type="hidden" id="app_list" name="app_list">
</FORM>
<FORM id="refreshform" OnTask="RefreshClick" >
	<INPUT type="hidden" id="actiontype" name="actiontype" value='refresh'>
	<INPUT type="hidden" id="cache_list" name="cache_list">
	<INPUT type="hidden" id="app_list" name="app_list">
	<INPUT type="hidden" id="urlreturnto" name="urlreturnto" value="<%= g_MSCSPage.URLPrefix() %><%=Request.ServerVariables("SCRIPT_NAME")%>?resource=<%= g_sResource %>">
</FORM>
<FORM id="deleteform" name="deleteform" OnTask="DeleteClick">
	<INPUT type="hidden" id="actiontype" name="actiontype" value='delete'>
	<INPUT type="hidden" id="cache_list" name="cache_list">
	<INPUT type="hidden" id="app_list" name="app_list">
</FORM>
 
</body>
</html>

 
