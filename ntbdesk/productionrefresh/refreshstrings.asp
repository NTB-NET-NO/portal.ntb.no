<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

'Localizable Strings
const L_RefreshLabelCampaigns_Text = "Publish Campaigns"
const L_RefreshLabelTransactions_Text = "Publish Transactions"
const L_RefreshLabelCatalogs_Text = "Publish Catalogs"
Const L_ProductionItems_Text = "Production Items:"
Const L_ProductionLookFor_Text = "Look For: "
Const L_ProductionAdOption_Text = "Advertising"
Const L_ProductionDMOption_Text = "Direct Mail"
Const L_ProductionDiscOption_Text = "Discount"
Const L_ProductionLoadingWidgets_Text = "Loading Widgets..."
Const L_ProductionCacheName_Text = "Cache Name"
Const L_ProductionApplication_Text = "Application"
Const L_ProductionLastRefreshed_Text = "Last Published"
Const L_PRSelectItem_Text = "Production item selected: %1"
Const L_PRSelectedXItems_Text = "%1 production items selected"
Const L_PRNoItemsSelectedItems_Text = "No production items selected"
Const L_ProductionFindNow_Text = "Find Now"
Const L_ProductionAll_Text = "All"
Const L_NoCachedItems_Text = "There are currently no cached items to be published"
const L_SortedBy_Text = "Sorted by : %1"
const L_RefreshedApplications_Text = "Published %1 items"
const L_RefreshedOneApplication_Text = "Published 1 item"
const L_DeletedApplications_Text = "Deleted %1 items"
const L_DeletedOneApplication_Text = "Deleted 1 item"
%>
