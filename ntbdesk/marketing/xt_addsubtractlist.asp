<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<%
'parse the posted data
dim sListID1, sListID2, sListID3,  sType
dim oLM, aPostVal
dim sReturnVarID, sReturnVarOpID
dim rsPrimary
dim sFriendlyError, sScriptError
dim g_sCampaignConnectionStr

On Error resume next

if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

'Get the Selections made by the user
'aPostVal(0) = type of action: values - 'add' or 'subtract'
'aPostVal(1) = list_id of the second list
'aPostVal(2) = name of the destination list name
sListID1 = CStr(Request.Form("list_id"))
aPostVal = Split(Request.Form("listdata"), "|||", -1, 1)
sListID2 = CStr(aPostVal(1))

'check what action was choosen
if aPostVal(0) = "add" then
	sType = "add"
else
	sType = "sub"
End if

'create the ListManager object
set oLM = Server.CreateObject("Commerce.ListManager")

'Initialiase List Manager.Object
oLM.Initialize g_sCampaignConnectionStr
if err.Number <> 0 then
	sFriendlyError = sGetErrorByID("Cant_connect_db")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
	Set oLM = Nothing
	Response.redirect "listmanager.asp?type=addsub&status=0"	
end if

set rsPrimary = oLM.GetLists

'check to see if the list existed or not
sListID3 = ""
if len(trim(sListID3)) = 0 then
	'user has asked for a new proper list so create a emptylist.
	sReturnVarID = oLM.CreateEmpty(CStr(trim(aPostVal(2))), CStr(trim(aPostVal(3))), MAILINGLIST, 0)
	if err.Number <> 0 then
		sFriendlyError = sGetErrorByID("Cant_create_list")
		sScriptError = sGetScriptError(Err)
		setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		Set oLM = Nothing
		Response.redirect "listmanager.asp?type=" & sType & "&status=0"	
	end if
	sListID3 = sReturnVarID
end if

if sType = "add" then
	oLM.Union CStr(sListID1), CStr(sListID2), CStr(sListID3), 100, TRUE, sReturnVarOpID
	if err.Number <> 0 then
		sFriendlyError = sGetErrorByID("Cant_add_list")
		sScriptError = sGetScriptError(Err)
		setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		Set oLM = Nothing
		Response.redirect "listmanager.asp?type=add&status=0"	
	end if
else
	'Bug#15086 swap parm(list_id 1 from list_id 2)
	oLM.Subtract 	CStr(sListID2), CStr(sListID1), CStr(sListID3), 100, TRUE, sReturnVarOpID
	if err.Number <> 0 then
		sFriendlyError = sGetErrorByID("Cant_subtract_list")
		sScriptError = sGetScriptError(Err)
		setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		Set oLM = Nothing
		Response.redirect "listmanager.asp?type=sub&status=0"	
	end if
End if

Set oLM = nothing		
	Response.Redirect "listmanager.asp?type=" & sType & "&status=1"
%>