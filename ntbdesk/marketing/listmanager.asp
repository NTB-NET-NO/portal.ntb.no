<!--#INCLUDE FILE='../include/BDHeader.asp' -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='include/marketingstrings.asp' -->
<!--#INCLUDE FILE='include/list_db.asp' -->
<%
Server.ScriptTimeout = 3 * 60

Const PAGE_SIZE = 20

dim g_nPage, g_nStart, g_sStatusText
dim g_nNonWorking, g_nMailable, g_nGeneric, g_nListCount
dim g_sCampaignConnectionStr
dim g_sSortCol, g_sSortDir

on error resume next

if Len(Trim(Session("Campaign_ConnectionStr"))) = 0 then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

g_nPage = Request.Form("page")
If IsEmpty(g_nPage) Then g_nPage = 1

g_sSortCol = Request.Form("column")
if g_sSortCol = "" then
	g_sSortcol = "list_name"
end if

g_sSortDir = Request.Form("direction")
if g_sSortDir = "" then
	g_sSortDir = "asc"
end if

if Request("status") = "1" then
	'successful op
	select case Request("type")
		case "add"
			g_sStatusText = L_AddSuccess_StatusBar
		case "sub"
			g_sStatusText = L_SubSuccess_StatusBar
		case "cancel"
			g_sStatusText = L_CancelSuccess_StatusBar
		case "convert"
			g_sStatusText = L_ConvertSuccess_StatusBar
		case "copy"
			g_sStatusText = L_CopySuccess_StatusBar
		case "delete"
			g_sStatusText = L_DeleteSuccess_StatusBar
		case "export"
			g_sStatusText = L_ExportSuccess_StatusBar
		case "import"
			g_sStatusText = L_ImportSuccess_StatusBar
	end select
elseif Request("status") = "0" then
	'unsuccessful op
	select case Request("type")
		case "add"
			g_sStatusText = L_AddNoSuccess_StatusBar
		case "sub"
			g_sStatusText = L_SubNoSuccess_StatusBar
		case "convert"
			g_sStatusText = L_ConvertNoSuccess_StatusBar
		case "copy"
			g_sStatusText = L_CopyNoSuccess_StatusBar
		case "delete"
			g_sStatusText = L_DeleteNoSuccess_StatusBar
		case "export"
			g_sStatusText = L_ExportNoSuccess_StatusBar
		case "import"
			g_sStatusText = L_ImportNoSuccess_StatusBar
	end select
end if
%>
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<SCRIPT LANGUAGE='VBScript'>
	option explicit

	dim g_sStatus, g_sMailable, g_sGeneric, g_sKeyName, g_sDescr

'***********************************************************************
'	OnRowUnselect
'
'   clear values and reset buttons 
'
	sub OnRowUnselect()
		g_sStatus = ""
		g_sMailable = ""
		g_sGeneric = ""
		g_sKeyName = ""
		g_sDescr = ""

		SetButtons()

		cancelform.list_recent_operid.value = ""
		openform.list_id.value = ""
		convertform.list_id.value = ""
		addsubform.list_id.value = ""
		deleteform.list_id.value = ""
		copyform.list_id.value = ""
		exportform.list_id.value = ""

		' display the status text
        setStatusText  "<%= L_ListNoSelectionStatus_Text %>"
	end sub

'***********************************************************************
'	OnSelectRow
'
'   put id of selection in other forms, save important values, and set buttons
'
	sub OnSelectRow()
		dim xmlSelection
		dim sKeyValue

		set xmlSelection = window.event.XMLrecord
        With xmlSelection
			sKeyValue = .selectSingleNode("list_id").text
			g_sKeyName = .selectSingleNode("list_name").text
			g_sDescr = .selectSingleNode("list_description").text
			g_sStatus = .selectSingleNode("list_status").text
			g_sMailable = .selectSingleNode("list_mailable").text
			g_sGeneric = .selectSingleNode("list_generic").text
			cancelform.list_recent_operid.value = .selectSingleNode("list_recent_operid").text
		End With

		SetButtons()
	
		'set id in all forms
		openform.list_id.value = sKeyValue
		convertform.list_id.value = sKeyValue
		addsubform.list_id.value = sKeyValue
		deleteform.list_id.value = sKeyValue
		copyform.list_id.value = sKeyValue
		exportform.list_id.value = sKeyValue

		'display the status text
		setStatusText sFormatString("<%= L_ListSelectedStatus_Text %>", array(g_sKeyName))
    end sub


'***********************************************************************
'	SetButtons
'
'   set buttons  
'
    sub SetButtons ()
		EnableTask "import"	'always enabled
		Select Case g_sStatus
			Case "<%= L_ListStatusPending_Text %>"
				DisableTask "addsub"
				EnableTask  "cancel"
				DisableTask "convert"
				DisableTask "copy"
				DisableTask "delete"
				DisableTask "export"
				DisableTask "open"   
			Case "<%= L_ListStatusFailed_Text %>"
				DisableTask "addsub"
				DisableTask "cancel"
				DisableTask "convert"
				DisableTask "copy"
				EnableTask  "delete"
				DisableTask "export"
				DisableTask "open" 
			Case ""		'nothing selected
				DisableTask "addsub"
				DisableTask "cancel"
				DisableTask "convert"
				DisableTask "copy"
				DisableTask "delete"
				DisableTask "export"
				DisableTask "open"   
			Case Else	'something selected not failed or pending
				EnableTask "copy"
				EnableTask "delete"
				EnableTask "export"
				EnableTask "open"
				'add/sub enabled if mailable
				if g_sMailable = "<%= L_YES_TEXT %>" then
					EnableTask "addsub"
					EnableTask "convert"
				else	
					DisableTask "addsub"
					DisableTask "convert"
				end if
				'convert enabled if mailable and not already generic
				DisableTask "cancel"
		End Select
    end sub

'*************************************************************************
'	ShowImportDialog
'
'   show import dialog 
'
    sub ShowImportDialog ()
		dim sReturnValue

		sReturnValue = window.showModalDialog("dlg_importlist.asp", , "status:no;help:no")
		if sReturnValue = -1 or isEmpty(sReturnValue) then
			SetButtons()
		else
			importform.listdata.value = CStr(sReturnValue)
			importform.submit()
		end if    
	end sub

'************************************************************************
'	ShowExportDialog
'
'   show export dialog 
'
    sub ShowExportDialog ()
		dim sReturnValue

		sReturnValue = window.showModalDialog("dlg_exportlist.asp", , "status:no;help:no")
		if sReturnValue = -1 or isEmpty(sReturnValue) then
			SetButtons()
		else
			with exportform
				.listdata.value = sReturnValue
				.submit()
			end with
		end if
    end sub

'************************************************************************
'	ShowCopyDialog
'
'   show copy dialog 
'
    sub ShowCopyDialog ()
		dim sReturnValue

		sReturnValue = window.showModalDialog("dlg_copylist.asp", g_sKeyName, "status:no;help:no")
		if sReturnValue = -1 or isEmpty(sReturnValue) then
			SetButtons()
		else
			with copyform
				.listdata.value = sReturnValue
				.submit()
			end with
		end if    
    end sub

'************************************************************************
'	ShowDeleteDialog
'
'   show delete confirm dialog 
'
	sub ShowDeleteDialog ()
		dim sReturnValue

		sReturnValue = MsgBox (sFormatString ("<%= L_LMDelete_Text %>", Array (g_sKeyName)), vbOKCancel, "<%= L_LMDeleteList_Text %>")
		if sReturnValue = vbOK then
			deleteform.submit()
		else
			SetButtons()
		end if
	end sub

'*************************************************************************
'	bdaddsubtract_OnTask
'
'   if more than one mailable list then show add/sub dialog
'
	sub bdaddsubtract_OnTask()
		dim sReturnValue

		'if current list is the only mailable list show an error
		if selectform.nmailable.value <= 1 then 
			Msgbox "<%= L_NoWorkingList_ErrorMessage %>", vbokonly, "<%= L_AddSubList_Text %>"
			SetButtons()
			exit sub
		End If

		sReturnValue = window.showModalDialog("dlg_addsubtractlist.asp?list_id=" & addsubform.list_id.value, , "status:no;help:no")
		if sReturnValue = -1 or isEmpty(sReturnValue) then
			SetButtons()
		else
			with addsubform
				.listdata.value = sReturnValue
				.submit()
			end with
		end if
	end sub

'************************************************************************
'	ShowConvertDialog
'
'   Convert list for direct mail
'
	sub ShowConvertDialog()
		dim sReturnValue
		Dim sDlgParam
		
		sDlgParam = g_sKeyName & "|||" & g_sDescr

		sReturnValue = window.showModalDialog("dlg_convertlist.asp", sDlgParam, "status:no;help:no")	
		if sReturnValue = -1 or isEmpty(sReturnValue) then
			SetButtons()
		else
			with convertform
				.listdata.value = sReturnValue
				.submit()
			end with
		end if
	end sub


'************************************************************************
'	Window_onLoad
'
'   Enables Import task
'
	Sub Window_onLoad()
		EnableTask "import"
	end Sub
</SCRIPT>
</HEAD>
<BODY>
	<xml ID='listData1'>
		<%= sGetListXML %>
	</xml>
	<!-- product list meta data -->
	<xml id='listMeta'>
		<listsheet>
			<global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_nListCount %>' selection='single' />
			<columns>
		        <column id='list_id' hide='yes'>ID</column>
		        <column id='list_recent_operid' hide='yes'>list_recent_operid</column>
		        <column id='list_description' hide='yes'>Descr</column>
		        <column width='20' id='list_name' sortdir='asc'><%= L_LMColumn1_Text %></column>
		        <column width='10' id='list_size'><%= L_LMColumn2_Text %></column>
		        <column width='10' id='list_unique_guids'><%= L_LMColumn3_Text %></column>
		        <column width='10' id='list_unique_emails'><%= L_LMColumn4_Text %></column>
				<column width='20' id='list_created'><%= L_LMColumn5_Text %></column>
		        <column width='10' id='list_status'><%= L_LMColumn6_Text %></column>		        
		        <column width='10' id='recent_op_error_code'><%= L_LMColumn7_Text %></column>
		        <column width='20' id='recent_op_error_desc'><%= L_LMColumn8_Text %></column>
		      </columns>
		      <operations>  
				<newpage formid='NewForm'/>  
				<sort formid='NewForm'/>
			  </operations>
		</listsheet>
	</xml>
<%
InsertTaskBar L_LM_Text, g_sStatusText
%>
<DIV ID="bdcontentarea" CLASS="listSheetContainer">
	<DIV ID='lsListView' CLASS='listSheet' STYLE="HEIGHT: 80%; MARGIN: 20px"
		DataXML='listData1'
		MetaXML='listMeta'
		LANGUAGE='VBScript'
		OnRowSelect='OnSelectRow()'
		OnRowUnselect='OnRowUnselect()'><%= L_LoadingProperty_Text %></DIV>
</DIV>

<FORM ID='NewForm' ACTION='response_listmanager.asp' METHOD='POST'></FORM>

<FORM ID='selectform' NAME='selectform' >
	<INPUT TYPE='hidden' ID='list_count'	NAME='list_count'	VALUE='<%= g_nListCount %>'>
	<INPUT TYPE='hidden' ID='nmailable'		NAME='nmailable'	VALUE='<%= g_nMailable %>' >
	<INPUT TYPE='hidden' ID='nnonworking'	NAME='nnonworking'	VALUE='<%= g_nNonWorking %>' >
	<INPUT TYPE='hidden' ID='ngeneric'		NAME='ngeneric'		VALUE='<%= g_nGeneric %>'>
</FORM>

<FORM ID='copyform' NAME='copyform' ONTASK='ShowCopyDialog()'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='copy'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id' >
	<INPUT TYPE='hidden' ID='listdata' NAME='listdata' >
</FORM>

<FORM ID='deleteform' NAME='deleteform' ONTASK='ShowDeleteDialog()'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='delete'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id'>
</FORM>

<FORM ID='openform' NAME='openform'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='open'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id'>
</FORM>

<FORM ID='convertform' NAME='convertform' ONTASK='ShowConvertDialog()'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='convert'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id'>
	<INPUT TYPE='hidden' ID='listdata' NAME='listdata'>
</FORM>

<FORM ID='cancelform' NAME='cancelform'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='cancel'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id'>
	<INPUT TYPE='hidden' ID='listdata' NAME='listdata'>
	<INPUT TYPE='hidden' ID='list_recent_operid' NAME='list_recent_operid'>	
</FORM>

<FORM ID='importform' NAME='importform' ONTASK='ShowImportDialog()'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='import'>
	<INPUT TYPE='hidden' ID='listdata' NAME='listdata' >
</FORM>

<FORM ID='exportform' NAME='exportform' ONTASK='ShowExportDialog()'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='export'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id' >
	<INPUT TYPE='hidden' ID='listdata' NAME='listdata' >
</FORM>

<FORM ACTION='xt_addsubtractlist.asp' METHOD='POST' ID='addsubform' NAME='addsubform' ONTASK='bdaddsubtract_OnTask()'>
	<INPUT TYPE='hidden' ID='type' NAME='type' VALUE='export'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id' >
	<INPUT TYPE='hidden' ID='listdata' NAME='listdata' >
</FORM>

</BODY>
</HTML>
