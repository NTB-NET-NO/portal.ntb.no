<!-- #INCLUDE FILE='..\include\BDHeader.asp' -->
<!-- #INCLUDE FILE='..\include\BizDeskUtil.asp' -->
<!-- #INCLUDE FILE='include\marketingStrings.asp' -->
<%
Dim g_sCampaignConnectionStr, g_rsQuery, g_dFormValues, g_sQuery 
Dim g_sIndustryOptions, g_sCustomerXMLData, g_sTitle

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
Set g_rsQuery = Server.CreateObject("ADODB.Recordset")


'get form values
Set g_dFormValues = dGetFormValues()

g_sIndustryOptions = sGetIndustryOptions()
g_sCustomerXMLData = sGetCustomerXMLData()
g_sTitle = sGetTitle()

'all jobs are done
Set g_rsQuery = Nothing
 
'******************************************************************************
'	server side functions
'******************************************************************************
Function dGetFormValues()
	Dim dFormValues
	Set dFormValues = Server.CreateObject("Commerce.Dictionary")
	dFormValues("type") = Request.Form("type")
	If dFormValues("type") = "add" Then
		dFormValues("i_customer_id") = -1
		dFormValues("dbaction") = "insert"
	Else 'open
		dFormValues("i_customer_id") = CLng(Request.Form("i_customer_id"))
		dFormValues("dbaction") = "update"
	End If	
	Set dGetFormValues = dFormValues
End Function

Function sGetIndustryOptions()
	Dim sReturn, fdIndustryID, fdIndustryCode
	sReturn = "<option value='0'>" & L_None_Text & "</option>"
	g_sQuery = "SELECT i_industry_id, u_industry_code FROM industry_code ORDER BY u_industry_code"						
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	With g_rsQuery
		Set fdIndustryID = .Fields(0)
		Set fdIndustryCode = .Fields(1)
		Do While Not .EOF
			sReturn = sReturn & "<option value='" & fdIndustryID.Value & "'>" & _
				g_mscsPage.htmlEncode(fdIndustryCode.Value) & "</option>"
			.MoveNext
		Loop 
		.Close
	End With
	sGetIndustryOptions = sReturn
End Function

Function sGetCustomerXMLData()
	Dim xmlData, xmlNode
	g_sQuery = "SELECT u_customer_name, u_customer_address, i_customer_type, u_customer_url, i_industry_id, " &_
		"dt_customer_archived, u_customer_comments, u_customer_contact, u_customer_email, u_customer_phone, " &_
		"u_customer_fax FROM customer WHERE i_customer_id = " & g_dFormValues("i_customer_id")	
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	Set xmlData = xmlGetXMLFromRS(g_rsQuery)
	g_rsQuery.Close 
	With xmlData	
		'properly diaply State field
		Set xmlNode = .selectSingleNode("//dt_customer_archived")
		If Len(xmlNode.text) = 0 Then
			xmlNode.text = L_CuUnarchived_Text 
		Else
			xmlNode.text = L_CuArchived_Text
		End If
		'we need this to display on title bar
		g_dFormValues("u_customer_name") = .selectSingleNode("//u_customer_name").text
		sGetCustomerXMLData = .xml
	End With
End Function

Function sGetTitle()
	Dim sTitle
	'get taskbar title based on type 
	Select Case g_dFormValues("type")
		Case "add"
			sTitle = L_CuTaskBarNew_Text
		Case "open"
			sTitle = sFormatString(L_CuTaskBar_Text, Array(g_dFormValues("u_customer_name")))
	End Select
	sGetTitle = sTitle
End Function
%>
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<SCRIPT LANGUAGE='VBScript'>
Option Explicit

Sub window_onLoad()
	esMaster.focus()
End Sub

Sub OnTask()
	Dim elSource, xmlDOMDoc, xmlErrorNodes, xmlCustomerID, elBtnBack
	Set elSource = window.event.srcElement
	With saveform
		If elSource Is elGetTaskBtn("savenew") Then
			.type.value = "add"
		Else 'click save or back or saveback
			.type.value = "open" 'doesn't matter for back and saveback
		End If
	End With
	Set xmlDOMDoc = xmlPostFormViaXML(saveform)
	HideWaitDisplay()
	EnableTask("back")
	'successful save should return one item node and no errors
	Set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
	Set xmlCustomerID = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'customer_id']")
	If xmlErrorNodes.length <> 0 Or xmlDOMDoc.documentElement Is Nothing Or xmlCustomerID Is Nothing Then
		'report errors and set status text
		SetStatusText(sFormatString("<%= L_BDSSUnableToSaveX_StatusBar %>", Array(esMaster.field("u_customer_name").value)))
		ShowXMLNodeErrors(xmlErrorNodes)
		esMaster.resetDirty()
	Else 'save successfully
		SetStatusText(sFormatString("<%= L_BDSSSavedX_StatusBar %>", Array(esMaster.field("u_customer_name").value)))
		If elSource Is elGetTaskBtn("savenew") Then 
			saveform.i_customer_id.value = -1
			saveform.dbaction.value = "insert"
			esMaster.resetDefault()
			SetPageTitleText("<%= L_CuTaskBarNew_Text %>")
		Else 'save or back or saveback
			saveform.i_customer_id.value = xmlCustomerID.getAttribute("value")
			saveform.dbaction.value = "update"
			esMaster.resetDirty()
			SetPageTitleText(sFormatString("<%= L_CuTaskBar_Text %>", Array(esMaster.field("u_customer_name").value)))
		End If
		g_bDirty = false
		if parent.g_bCloseAfterSave then
			parent.g_bCloseAfterSave = false
			' -- save and exit chosen and no errors while saving
			set elBtnBack = elGetTaskBtn("back")
			if not elBtnBack is nothing then
				if not elBtnBack.disabled then elBtnBack.click()
			end if
		end if
		parent.g_bCloseAfterSave = false
	End If
	esMaster.focus()
	'To force the disabling all icons except back
	disabletask "savenew"
	disabletask "saveback"
	disabletask "save"
End Sub
</SCRIPT>
</HEAD>
<BODY>
<%
InsertEditTaskBar g_sTitle, ""
%>

<xml id='esCustomerMeta'>
	<editsheets>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_CuCustomerProperty_Text %></name>
				<key><%= L_CuCustomerProperty_Accelerator %></key>
			</global>
			<fields>
				<text id='u_customer_name' required='yes' maxlen='255'>
					<name><%= L_CuName_Text %></name>
					<tooltip><%= L_CuName_ToolTip %></tooltip>
					<prompt><%= L_CuNamePrompt_Text %></prompt>
					<error><%= L_CuName_ErrorMessage %></error>
					 <charmask>^(\s*\S+\s*)*$</charmask>
				</text>		
				<text id='u_customer_address' maxlen='255' subtype='long'>
					<name><%= L_CuAddress_Text %></name>
					<tooltip><%= L_CuAddress_ToolTip %></tooltip>
					<error><%= L_CuAddress_ErrorMessage %></error>
				</text>
				<select id='i_customer_type' default='2'>
					<name><%= L_CuType_Text %></name>
					<tooltip><%= L_CuType_ToolTip %></tooltip>
					<select>
						<option value='1'><%= L_CuTypeOptionSelf_Text %></option>
						<option value='2'><%= L_CuTypeOptionAdvertiser_Text %></option>
						<option value='3'><%= L_CuTypeOptionAgency_Text %></option>
					</select>
				</select>				
				<text id='u_customer_url' maxlen='255'>
					<name><%= L_CuURL_Text %></name>
					<tooltip><%= L_CuURL_ToolTip %></tooltip>
					<error><%= L_CuURL_ErrorMessage %></error>
				</text>
				<select id='i_industry_id' default='0'>
					<name><%= L_CuIndustry_Text %></name>
					<tooltip><%= L_CuIndustry_ToolTip %></tooltip>
					<select>
						<%= g_sIndustryOptions %>
					</select>		            
				</select>			        
				<text id='dt_customer_archived'  readonly='yes' maxlen='50'>
					<name><%= L_CuState_Text %></name>
					<tooltip><%= L_CuState_ToolTip %></tooltip>
				</text>	
				<text id='u_customer_comments' maxlen='255' subtype='long'>
					<name><%= L_CuComments_Text %></name>
					<tooltip><%= L_CuComments_ToolTip %></tooltip>
					<error><%= L_CuComments_ErrorMessage %></error>
				</text>	
			</fields>
		</editsheet>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_CuContactInfo_Text %></name>
				<key><%= L_CuContactInfo_Accelerator %></key>
			</global>
			<fields>
				<text id='u_customer_contact' maxlen='100'>
					<name><%= L_CuContact_Text %></name>
					<tooltip><%= L_CuContact_ToolTip %></tooltip>
					<error><%= L_CuContact_ErrorMessage %></error>
				</text>
				<text id='u_customer_email' maxlen='128'>
					<name><%= L_CuEmail_Text %></name>
					<tooltip><%= L_CuEmail_ToolTip %></tooltip>
					<error><%= L_CuEmail_ErrorMessage %></error>
					<charmask>^(\s*\S+\s*)*$</charmask>
				</text>
				<text id='u_customer_phone' maxlen='100'>
					<name><%= L_CuPhone_Text %></name>
					<tooltip><%= L_CuPhone_ToolTip %></tooltip>
					<error><%= L_CuPhone_ErrorMessage %></error>
				</text>
				<text id='u_customer_fax' maxlen='100'>
					<name><%= L_CuFax_Text %></name>
					<tooltip><%= L_CuFax_ToolTip %></tooltip>
					<error><%= L_CuFax_ErrorMessage %></error>
			    </text>					
			</fields>
		</editsheet>
	</editsheets>
</xml>

<xml id='esCustomerData'>
	<?xml version='1.0' ?>
	<%= g_sCustomerXMLData %>
</xml>

<DIV ID='bdContentArea'>

	<FORM ID='saveform' ACTION METHOD='post' ONTASK='OnTask()'>
		<INPUT TYPE='hidden' NAME='type'>
		<INPUT TYPE='hidden' NAME='i_customer_id' VALUE='<%= g_dFormValues("i_customer_id")%>'>
		<INPUT TYPE='hidden' NAME='dbaction' VALUE='<%= g_dFormValues("dbaction") %>'>
		<DIV ID='esMaster' CLASS='editSheet'
			LANGUAGE='VBScript' ONVALID='SetValid("")' ONCHANGE='SetDirty("")' ONREQUIRE='SetRequired("")'
			MetaXML='esCustomerMeta' DataXML='esCustomerData'><h3><%= g_imgLoadingAnimation %> <%= L_CuLoadingProperty_Text %></h3>
		</DIV>
	</FORM>
</DIV>


</BODY>
</HTML>