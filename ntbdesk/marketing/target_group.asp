<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!-- #include FILE="include\marketingStrings.asp" -->

<%
Dim g_sCampaignConnectionStr, g_sStatus

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	

on error resume next 
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr) 

if err.number <> 0 then 
	setError L_TgTargetGroup_Text, sGetErrorByID("Cant_connect_db"), sGetScriptError(Err), ERROR_ICON_ALERT 
end if 

If request("type") = "archive" then
	Call TargetGroupDelete()
End If

'******************************************************************************************
' Target Group DB Delete
'
'******************************************************************************************
Sub TargetGroupDelete
	Dim sqltext, rsCheck

	'Logic is to check here and see if this target group is used by anybody else

	REM - ********************************************************************************************************************************************
	'Initialize : 
	Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_STORED_PROC)

	REM - ********************************************************************************************************************************************
	'Check here to see if the targroup Id is used by any campaign item
	sqltext =" SELECT * FROM target_group_xref WHERE i_targroup_id = " & request("i_targroup_id")
	set rsCheck = Server.CreateObject("ADODB.Recordset")
	g_oCmd.CommandText = sqltext
	g_oCmd.CommandType = AD_CMD_TEXT
	rsCheck.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
	if err.number = 0 and rsCheck.EOF then	
		'then delete this target group, since nobody else is using it.
		'Begin transaction
		g_oConn.BeginTrans	

		'Step1 : Delete all the relating records from target table
		sqltext = " DELETE FROM target WHERE i_targroup_id = " & Request("i_targroup_id")
		g_oConn.Execute sqltext, , AD_CMD_TEXT

		REM - check for errors:
		if g_oConn.Errors.count > 0 then
			REM - roll back the transaction:
			g_oConn.RollbackTrans
			exit sub
		end if	

		'Step 2: Now Delete the corresponding rows from target_group table
		sqltext = " DELETE FROM target_group WHERE i_targroup_id = " & request("i_targroup_id")
		g_oConn.Execute sqltext, , AD_CMD_TEXT

		REM - check for errors:
		if g_oConn.Errors.count > 0 then
			REM - roll back the transaction:
			g_oConn.RollbackTrans
			exit sub
		end if	
		
		g_sStatus =  sFormatString(L_TgGroupDeleted_Text, array(request("u_targroup_name")))
		
		REM - commit  
		g_oConn.CommitTrans
		rsCheck.Close
	Else
		setError L_TgTargetGroup_Text, L_TgGroupCannotDelete_ErrorMessage, sFormatString(L_TgUsedByOtherCI_Text, array(request("u_targroup_name"))), ERROR_ICON_CRITICAL			
		g_sStatus =  sFormatString(L_TgGroupUnableToDelete_Text, array(request("u_targroup_name")))
	End if
	Set rsCheck = Nothing
End Sub
%>
<html>
<head>
<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">

<script language="vbscript">
<!--
	Sub SelectTargetGroup		
		dim xmlsource, sSourceName, g_sStatus
	
		set xmlsource = window.event.XMLrecord	
				
		if xmlsource.selectNodes("./i_targroup_id").length = 1 then
			selectform.i_targroup_id.value = CInt(xmlsource.selectSingleNode("./i_targroup_id").text)
			selectform.u_targroup_name.value = Trim(xmlsource.selectSingleNode("./u_targroup_name").text)
			sSourceName = selectform.u_targroup_name.value
			selectform.item_selected.value = "YES"
			'display the status text
 
			g_sStatus =  sFormatString("<%= L_TgGroupSelected_Text %>", Array(sSourceName))
			SetStatusText(g_sStatus)
 
		else
			'I am not sure when this case might occur - 5/14/99 at 1:58am
		end if
			
		'Enable buttons
		EnableTask "new"
		EnableTask "open"
		EnableTask "delete"

		set xmlsource = nothing			
	End sub

	Sub UnselectTargetGroup
		'Disable buttons
		EnableTask "new"
		DisableTask "open"
		DisableTask "delete"
		selectform.i_targroup_id.value = -1		
		selectform.u_targroup_name.value = ""
		selectform.item_selected.value = ""
		SetStatusText "<%=L_TgExprNoItemSelectedM_Text%>"		
	End Sub

	Sub ShowAddDialog	
		Dim sDialog

		addform.i_targroup_id.value = -1	'selectform.i_targroup_id.value
		addform.u_targroup_name.value = ""
		addform.submit()	
				
	End sub

	Sub ShowOpenDialog
		
		openform.i_targroup_id.value = selectform.i_targroup_id.value	
		openform.u_targroup_name.value = selectform.u_targroup_name.value	
		openform.submit()
	End sub
	
	Sub ShowDeleteDialog
		Dim msgresult, sDeleteText
		
		sDeleteText = sFormatString("<%=L_TgExprDeleteGroupMsgBox_ErrorMessage%>",Array(selectform.u_targroup_name.value))
		msgresult = Msgbox (sDeleteText, vbYesNo, "<%=L_TgExprDeleteGroupMsgBox2_ErrorMessage%>")
		if msgresult = VBYES then
			deleteform.i_targroup_id.value = selectform.i_targroup_id.value	
			deleteform.u_targroup_name.value = selectform.u_targroup_name.value
			deleteform.submit()
		else
			EnableTask "new"
			If selectform.item_selected.value = "YES" then
				EnableTask "delete"
				EnableTask "open"
			End If
		End If
	End sub
	
	Sub Initialize
		'when no customer exists, only the Add button should be exposed
		'at all other times, all the buttons should be disabled
		if document.all.newtarget.value = 1 then
			EnableTask "new"
			selectform.i_targroup_id.value = -1
		else
			EnableTask "new"
		end if
		DisableTask "open"
		DisableTask "delete"									
	End sub
'-->
</script>
</head>
<body SCROLL=no LANGUAGE='VBScript' ONLOAD='Initialize'>
<%
	InsertTaskBar L_TgTargetGroup_Text, g_sStatus
%>
<div ID="bdcontentarea">
<xml ID="lstTargetGroupMETA">
	<listsheet>
	    <global pagecontrols="no" selectionbuttons="no" selection="single" sort='no' />
	    <columns>
	        <column id="i_targroup_id" key="yes" hide="yes"><%=L_TgID_Text%>></column>
	        <column id="u_targroup_name" sortdir="asc" width="100"><%= L_TgTargetGroupName_Text %></column>
	    </columns>
	</listsheet>
</xml>

	<xml ID="lstTargetGroupDATA">
	<%	
		Dim rsCheck, g_bEmptyQuery, sqltext
		
		Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
		sqltext = "SELECT i_targroup_id, u_targroup_name FROM target_group ORDER BY u_targroup_name"
		g_oCmd.CommandText = sqltext
		set rsCheck = server.CreateObject("ADODB.Recordset")
		rsCheck.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
		if err.number <> 0 then
			Response.write "<document/>"
			g_bEmptyQuery = true
		else
			if rsCheck.EOF then
				g_bEmptyQuery = true
			else
				g_bEmptyQuery = false
			End if
			Response.Write xmlGetXMLFromRS(rsCheck).xml
		end if
	%>
	</xml>

	<% If g_bEmptyQuery then %>
		<INPUT type="hidden" name="newtarget" value=1>
	<% Else %>
		<INPUT type="hidden" name="newtarget" value=0>
	<% End if %>


<!-- *****************************Target Groups - END*********************************************-->

	<div ID="lstTargetGroup" CLASS="listsheet" STYLE="MARGIN:20px; HEIGHT: 80%"
			METAXML="lstTargetGroupMETA" 
			DATAXML="lstTargetGroupDATA" 
			LANGUAGE="VBScript" 
			ONROWSELECT="SelectTargetGroup()" 
			ONROWUNSELECT="UnselectTargetGroup()" ><%=L_LoadingProperty_Text%></div>	


<FORM action method="POST" id="selectform" name="selectform" >
	<INPUT type="hidden" id="type" name="type" value="select">
	<INPUT type="hidden" id="i_targroup_id" name="i_targroup_id">
	<INPUT type="hidden" id="u_targroup_name" name="u_targroup_name">
	<INPUT type="hidden" id="item_selected" name="item_selected">	
</FORM>

<FORM action method="POST" id="addform" name="addform" ONTASK="ShowAddDialog">
	<INPUT type="hidden" id="type" name="type" value="add">
	<INPUT type="hidden" id="i_targroup_id" name="i_targroup_id">
	<INPUT type="hidden" id="u_targroup_name" name="u_targroup_name">
</FORM>

<FORM action method="POST" id="openform" name="openform" ONTASK="ShowOpenDialog">
	<INPUT type="hidden" id="type" name="type" value="open">
	<INPUT type="hidden" id="i_targroup_id" name="i_targroup_id">
	<INPUT type="hidden" id="u_targroup_name" name="u_targroup_name">
</FORM>

<FORM action method="POST" id="deleteform" name="deleteform" ONTASK="ShowDeleteDialog">
	<INPUT type="hidden" id="type" name="type" value="archive">
	<INPUT type="hidden" id="i_targroup_id" name="i_targroup_id">
	<INPUT type="hidden" id="u_targroup_name" name="u_targroup_name">
</FORM>

</div>
</BODY>
</HTML>