<!--#INCLUDE FILE='../include/BDHeader.asp' -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='include/list_db.asp' -->
<%
Const PAGE_SIZE = 20

dim g_sCampaignConnectionStr
dim g_nPage, g_nStart, g_sSortCol, g_sSortDir
dim g_nMailable, g_nGeneric, g_nNonWorking, g_nListCount

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	

g_nPage = 1
g_sSortcol = "list_name"
g_sSortDir = "asc"
%>
<HTML>
<HEAD>
<TITLE><%= L_CopyListTitle_Text %></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
	HTML
	{
		HEIGHT: 475px;
		WIDTH: 550px;
	}
	BODY
	{
		PADDING: 15px;
		MARGIN: 0;
	}
	BUTTON
	{
		WIDTH: 6em;
	}
</STYLE>
<script language='VBScript'>
<!--
    option explicit

    sub OnRowUnselect()
		selectform.list_id.value = ""
		selectform.list_name.value = ""
        copytext.innerHTML = "&lt;<%= L_DefaultCopySelection_Text %>&gt;"

		list_source.disabled = false
        btnOK.disabled = true
    end sub

    sub OnSelectRow()
        dim xmlSelection
        set xmlSelection = window.event.XMLrecord
		
		' add the value for the item key to the form
        selectform.list_id.value = xmlSelection.selectSingleNode("list_id").text
		selectform.list_name.value = xmlSelection.selectSingleNode("list_name").text
		copytext.innerHTML = selectform.list_name.value

        list_source.disabled = true
        btnOK.disabled = false
    end sub
 
 	Sub OnCancel()
 		window.returnvalue = -1
		window.close()	
	end Sub

	sub OnOK() 		
		dim sListName, aReturnValues(2)

		if Len(Trim(selectform.list_id.value)) = 0 then
			sListName = list_source.value
			aReturnValues(0) = "NAME"
			aReturnValues(1) = list_source.value
		else
			sListName = selectform.list_name.value
			aReturnValues(0) = "ID"
			aReturnValues(1) = selectform.list_id.value
		end if
		'error and abort if the same name is used
		if Ucase(Trim(sListName)) = Ucase(Trim(window.dialogArguments)) then
			msgbox "<%=L_CopyListSame_ErrorMessage%>", VBOKOnly, "<%=L_CopyListTitle_Text%>"
			list_source.focus
			exit sub
		end if

		window.returnvalue = Join(aReturnValues, "|||")
		window.close()
	end sub

	Sub FormChangeStatus()
		OnKeyUp	
	End sub	

	Sub OnKeyUp()
		if Len(Trim(list_source.value)) > 0 then
			copytext.innerHTML = list_source.value
			btnOK.disabled = false
		else
			copytext.innerHTML =  "&lt;<%=L_DefaultCopySelection_Text%>&gt;"
			btnOK.disabled = true
		end if
	End Sub

	Sub onNameChange()
		btnOK.disabled = cbool(not list_source.valid or list_source.required)
	end sub

	Sub Window_onLoad()
		list_source.focus
		window.dialogHeight = (bodyarea.scrollHeight + 60) & "px"
	end Sub
'-->
</SCRIPT>
</HEAD>
<BODY SCROLL=no LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then btnCancel.click">

	<div id=bodyarea>
	<xml ID='listData1'>
		<%= sGetListXML %>	
	</xml>

	<!-- product list meta data -->
	<xml id='listMeta'>
		<listsheet>
		    <global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_nListCount %>'
				selection='single'/>
		    <columns>
		        <column width='0' id='list_id' key='yes' hide='yes'>ID</column>
		        <column width='70' id='list_name' sortcol='yes' sortdir='asc'><%= L_ListMangerTab1_Text %></column>
		        <column width='15' id='list_size' ><%= L_ListMangerTab2_Text %></column>
		        <column width='15' id='list_status' ><%= L_ListMangerTab3_Text %></column>
		    </columns>
		    <operations>  
				<newpage formid='selectform'/>  
				<sort formid='selectform'/>
			  </operations>
		</listsheet>
	</xml>

	<xml id="efMeta">
		<field>
			<text id='list_source' required="yes" maxlen="64" subtype="short" onchange="onNameChange">
		        <error><%= L_ListnameLong_ErrorMessage %></error>
				<charmask>.*</charmask>
		    </text>
		</field>
	</xml>

	<xml id="efdata">
		<document/>
	</xml>

	<%= L_Copy_Text %> <span style="color: red"><SCRIPT LANGUAGE="vbscript"> 
		document.write(window.dialogArguments) 
		</SCRIPT></span> <%= L_To_Text %>
	<span ID="copytext" style="color: blue">&lt;<%= L_DefaultCopySelection_Text %>&gt;</span><br>
	<br>
    <%= L_ExistingList_Text %> 
	<DIV ID='lsListView' CLASS='listSheet' STYLE="margin-left: 0; margin-right: 0; margin-top:4px; margin-bottom:4px"
		DataXML='listData1' 
		MetaXML='listMeta'
		LANGUAGE='VBScript'
		OnRowSelect='OnSelectRow()'
		OnRowUnselect='OnRowUnselect()'> <%= L_LoadingProperty_Text %></DIV>
	<p>
	<fieldset ID='list_type_1' name="list_type_1" class='visiblesection' STYLE="WIDTH:100%">
		<legend><label for="list_source" TITLE="<%= L_TypeEntry_ToolTip %>"><%= L_ListName_Text %></label></legend><BR>
		<table width="100%">
		<tr cellpadding=0 cellspacing=0>
		<td colspan=2>
		<DIV ID="list_source" CLASS="editField" MetaXML="efMeta" DataXML="efData" onchange='FormChangeStatus()' OnKeyUp='OnKeyUp'></DIV>
		</td>
		</tr>
		</table>
		<BR>
	</fieldset>

<FORM ID='selectform' ACTION='response_listmanager.asp' METHOD='POST'>
	<INPUT TYPE='hidden' ID='list_id' NAME='list_id' >	
	<INPUT TYPE='hidden' ID='list_name' NAME='list_name' >	
	<INPUT TYPE='hidden' ID='listcount' NAME='listcount' value='<%= g_nListCount %>'>
</FORM>
<br>
<table border=0 cellpadding=0 cellspacing=1 width="100%">
	<tr>
		<td align=right><nobr>
			<BUTTON ID='btnOK' CLASS='bdbutton' onclick = 'OnOK' disabled><%= L_OK_Text %></BUTTON>
			<BUTTON ID='btnCancel' CLASS='bdbutton' onclick = 'OnCancel'><%= L_Cancel_Text %></BUTTON>
			<BUTTON ID='bdhelp' CLASS='bdbutton'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "CS_BD_Campaigns_WQWA.htm"'><%= L_Help_Text %></BUTTON>
		</nobr></td>
	</tr>
</table>
</div>
</BODY>
</HTML>
