<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!-- #INCLUDE FILE='..\include\BizDeskUtil.asp' -->
<!-- #INCLUDE FILE='include\marketingStrings.asp' -->
<%
Dim g_sCampaignConnectionStr, g_sExpStore_ConnectionStr, g_dFormValues, g_rsQuery, g_sQuery, _ 
    g_bSuccess, g_oExprStore, g_sStatusText, formValues, g_sStatus, g_opType, g_bErrorsOccured

'--GET CONNECTION STRINGS
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	
if IsEmpty(Session("ExpStore_ConnectionStr")) Then
	Session("ExpStore_ConnectionStr") = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")
end if
g_sExpStore_ConnectionStr = Session("ExpStore_ConnectionStr")	

on error resume next
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
set g_rsQuery = Server.CreateObject("ADODB.Recordset")

'Added for trapping the non-SQL server error
if err.number <> 0 then
    setError L_TgTargetGroup_Text, sGetErrorByID("Cant_connect_db"), sGetScriptError(Err), ERROR_ICON_ALERT
end if

'Setup dictionary variables
set formValues = Server.CreateObject("Commerce.Dictionary")
formValues("type") = Request("type")
formValues("i_targroup_id") = request("i_targroup_id")
formValues("u_targroup_name") = request("u_targroup_name")

'Choose appropriate type for new form loading
Select case formValues("type")
	case "add"
		formValues("dbaction") = "new" 'insert operation in DB for new posting
		g_sStatus = L_TgCreateNewTargetGroup_Text
	case "open"
	    formValues("dbaction") = "edit" 'update operation in DB
	case "newsavenew", "addsavenew"
		formValues("type") = "add"
	    formValues("dbaction") = "new"
	case "editsavenew", "opensavenew"
		formValues("type") = "open"
	    formValues("dbaction") = "edit"
End Select

Select case Request("dbaction")
	case "new" 
		call TargetGroupInsert()
		'if no errors then set to new
		If instr(Request("type"), "savenew") and isEmpty(Session("MSCSBDError")) then
			formValues("type") = "add"
			formValues("dbaction") = "new"
			formValues("i_targroup_id") = -1
			formValues("u_targroup_name") = ""	
		End If
	case "edit"
		call TargetGroupUpdate()
		'if no errors then set to new
		If instr(Request("type"), "savenew") and isEmpty(Session("MSCSBDError")) then
			formValues("type") = "add"
			formValues("dbaction") = "new"
			formValues("i_targroup_id") = -1
			formValues("u_targroup_name") = ""	
		End If
End Select
g_bErrorsOccured = cbool(not isEmpty(Session("MSCSBDError")))
 
If formValues("type") = "add" then
	g_opType = L_TgGroupTargetopTypeNew_Text
Else 
	g_opType = formValues("u_targroup_name")
End If

Sub CleanUpParameters(byRef oCmd)
	Dim i
	With oCmd.Parameters
		For i = 0 To .Count -1
			.Delete 0
		Next
	End With
End Sub

Function SQLQuote(sText)	
	If IsNull(sText) Or IsEmpty(sText) Or sText = "" Then Exit Function
	SQLQuote = Replace(sText, "'", "''")
End function

'********************************************************************
' Function for Target Group INSERT Display
'
'   Get the expression id , expr name and TRES
'   use this to create a target group
'
'   LOGIC: to create a target group
'   Step 1: Insert a record into target_group table with the expression name
'   Step 2: Insert a record into target table with targroup_id obtained
'	from the previous insert
'********************************************************************
Sub TargetGroupInsert
	Dim sqltext, paramReturn, param1, param2, param3, param4
	Dim g_targroup_id, g_target_id, sTg_name
    sTg_name = SQLQuote(trim(Request("u_targroup_name")))
	'Check to see if any discount is using the same expression
	g_sQuery = "SELECT Count(*) as NameCount FROM  target_group  WHERE u_targroup_name = '" & sTg_name & "'"

	g_oCmd.CommandText = g_sQuery	
	g_rsQuery.Open  g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
	if err.number <> 0 then
		setError L_TgTargetGroup_Text, L_TgCannotSave_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL		
		exit sub
	End If
	if not g_rsQuery.EOF then
		if g_rsQuery("NameCount") > 0 then
			'sCreateNewCustomCatalog = Err.Description
			setError L_TgTargetGroup_Text, L_TgCannotSave_ErrorMessage, L_TgDuplicateName_ErrorMessage, ERROR_ICON_CRITICAL		
			g_rsQuery.close
			Exit sub
		End if
	End if
	g_rsQuery.Close

 
	REM - ********************************************************************************************************************************************
	'Initialize : 

	Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_STORED_PROC)

	'Begin transaction
	g_oConn.BeginTrans	

	REM - ********************************************************************************************************************************************	
	'Step 1:  Insert a record into target_group table with the expression name

	'Insert record into target_group table	
	g_oCmd.CommandText = "sp_InsertTargetGroupMain"	
	g_oCmd.CommandType = AD_CMD_STORED_PROC
	
	''initialize parameters
	set paramReturn = g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
	g_oCmd.Parameters.Append paramReturn
	set param1 = g_oCmd.CreateParameter("u_targroup_name", AD_BSTR, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param1	
	set param2 = g_oCmd.CreateParameter("o_targroup_id", AD_INTEGER, AD_PARAM_OUTPUT )
	g_oCmd.Parameters.Append param2
	
	'set the param values
	param1.size = Len(trim(Request("u_targroup_name")))
	param1.value = trim(Request("u_targroup_name"))
		
	''execute stored proc
	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		setError L_TgTargetGroup_Text, L_TgCannotSave_ErrorMessage, sGetADOError(g_oConn.Errors(0)), ERROR_ICON_CRITICAL		
		exit sub
	End If
 
	g_targroup_id = param2.value
	formValues("i_targroup_id") = g_targroup_id
	CleanUpParameters(g_oCmd)
	
	REM - ********************************************************************************************************************************************
	'Step 2:Insert a record into target table with targroup_id obtained
	'				from the previous insert

	'Insert record into target table	
	g_oCmd.CommandText = "sp_InsertTargetMain"	
	g_oCmd.CommandType = AD_CMD_STORED_PROC

	'Initialize parameters
	set paramReturn = g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
	g_oCmd.Parameters.Append paramReturn
	set param1 = g_oCmd.CreateParameter("i_targroup_id", AD_INTEGER, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param1	
	set param2 = g_oCmd.CreateParameter("i_target_expr_id", AD_INTEGER, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param2	
	set param3 = g_oCmd.CreateParameter("i_target_action", AD_INTEGER, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param3	
	set param4 = g_oCmd.CreateParameter("o_target_id", AD_INTEGER, AD_PARAM_OUTPUT )
	g_oCmd.Parameters.Append param4


	Dim xmlTargetGroup, xmlRecords, xmlRecord
	dim xmlError, xmlTGDoc
	Dim xmltext, keytext, actiontext
	
	On Error resume next
	xmltext = Trim(Cstr(Request.Form("targetgroup")))

	if len(Trim(Request("targetgroup"))) > 0  then		
		set xmlTargetGroup = Server.CreateObject("Microsoft.XMLDOM")		
		bLoad = xmlTargetGroup.loadXML(xmltext) 			
 			
		if bLoad then
			'if XML loaded properly then
			set xmlTGDoc = xmlTargetGroup.documentElement
									
			set xmlRecords = xmlTGDoc.selectNodes("record")
			set rsCheck = Server.CreateObject("ADODB.Recordset")				
			for each xmlRecord in xmlRecords
				sState = LCase(xmlRecord.getAttribute("state"))
				keytext = xmlRecord.selectSingleNode("./i_target_expr_id").text
				actiontext = xmlRecord.selectSingleNode("./i_target_action").text
	
				''set values	
				param1.value = g_targroup_id
				param2.value = CInt(keytext)
				param3.value = CInt(actiontext)		
	
				''execute stored proc
				g_oCmd.Execute	
				If g_oConn.Errors.count > 0 Then
					setError L_TgTargetGroup_Text, L_TgCannotSave_ErrorMessage, sGetADOError(g_oConn.Errors(0)), ERROR_ICON_CRITICAL		
					g_oConn.RollbackTrans
					exit sub
				End If
				g_target_id = param4.value
			Next
			formValues("type") = "open"
			formValues("dbaction") = "edit"
			g_sStatus = sFormatString(L_TGSaved_Text, array(formValues("u_targroup_name")))
 
			REM - commit and redirect:
			g_oConn.CommitTrans
		else
			setError L_TgTargetGroup_Text, L_TgGroupFailToLoadXml_ErrorMessage, sGetXMLError(xmlTargetGroup.parseError), ERROR_ICON_ALERT
			g_oConn.RollbackTrans
		End if
		''Cleanup all parameters
		CleanUpParameters(g_oCmd)
	End if
End Sub

'*************************************************************************
' Target Group DB Update
'
'   Get the expression id , expr name and TRES
'   use this to create a target group
'
'   LOGIC: to create a target group
'   Step 1: Update a record into target_group table with the expression name
'   Step 2: Update a record into target table with targroup_id obtained
'	from the previous insert
'*************************************************************************
Sub TargetGroupUpdate
	Dim sqltext, paramReturn, param1, param2, param3, param4
	Dim g_targroup_id, g_target_id
	Dim sQueryString
 
 
	'Begin transaction
	g_oConn.BeginTrans	

	REM - ********************************************************************************************************************************************	
	'Step 1:  Update a record into target_group table with the expression name

	'Update record in target_group table	
	g_oCmd.CommandText = "sp_UpdateTargetGroupMain"	
	g_oCmd.CommandType = AD_CMD_STORED_PROC
	
	''initialize parameters
	set paramReturn = g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
	g_oCmd.Parameters.Append paramReturn
	set param1 = g_oCmd.CreateParameter("u_targroup_name", AD_BSTR, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param1	
	set param2 = g_oCmd.CreateParameter("i_targroup_id", AD_INTEGER, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param2
	
	'set the param values
	param1.size = Len(trim(Request("u_targroup_name")))
	param1.value = Trim(Request("u_targroup_name"))
	param2.value = Request("i_targroup_id")
		
	''execute stored proc
	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		setError L_TgTargetGroup_Text, L_TgCannotSave_ErrorMessage, sGetADOError(g_oConn.Errors(0)), ERROR_ICON_CRITICAL		
		exit sub
    End If
	''Cleanup all parameters
	CleanUpParameters(g_oCmd)
	
	
	REM - ********************************************************************************************************************************************
	'Step 2:Update a record in target table with targroup_id obtained
	'NOTE when multiple targets are returned for a target group, logic would be to 
	'delete the existing set of expressions from Target table for this targroup_id
	'and the insert the returned ones, in its place.
	
	'delete all existing target from target table for this targroup_id
	sqltext = " DELETE FROM target WHERE i_targroup_id = " & Request("i_targroup_id")
	g_oConn.Execute sqltext, , AD_CMD_TEXT		
	
	'Insert record into target table	
	g_oCmd.CommandText = "sp_InsertTargetMain"	
	g_oCmd.CommandType = AD_CMD_STORED_PROC

	''initialize parameters
	set paramReturn = g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
	g_oCmd.Parameters.Append paramReturn
	set param1 = g_oCmd.CreateParameter("i_targroup_id", AD_INTEGER, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param1	
	set param2 = g_oCmd.CreateParameter("i_target_expr_id", AD_INTEGER, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param2	
	set param3 = g_oCmd.CreateParameter("i_target_action", AD_INTEGER, AD_PARAM_INPUT )
	g_oCmd.Parameters.Append param3	
	set param4 = g_oCmd.CreateParameter("o_target_id", AD_INTEGER, AD_PARAM_OUTPUT )
	g_oCmd.Parameters.Append param4


	Dim xmlTargetGroup, xmlRecords, xmlRecord
	dim xmlError, xmlTGDoc, sState
	Dim xmltext, keytext, actiontext
	
	On Error resume next
	xmltext = Trim(Cstr(Request.Form("targetgroup")))
 
	if len(Trim(Request("targetgroup"))) > 0  then		
		set xmlTargetGroup = Server.CreateObject("Microsoft.XMLDOM")		
		bLoad = xmlTargetGroup.loadXML(xmltext) 			
			
		if bLoad then
			'if XML loaded properly then		
			set xmlTGDoc = xmlTargetGroup.documentElement									
			set xmlRecords = xmlTGDoc.selectNodes("record")			
			for each xmlRecord in xmlRecords				
				sState = LCase(xmlRecord.getAttribute("state"))
				if not sState = "deleted" then
					keytext = xmlRecord.selectSingleNode("./i_target_expr_id").text
					actiontext = xmlRecord.selectSingleNode("./i_target_action").text
	
					''set values	
					param1.value = Request("i_targroup_id")
					param2.value = CInt(keytext)
					param3.value = CInt(actiontext)		
	
					''execute stored proc
					g_oCmd.Execute	
					If g_oConn.Errors.count > 0 Then
						setError L_TgTargetGroup_Text, L_TgCannotSave_ErrorMessage, sGetADOError(g_oConn.Errors(0)), ERROR_ICON_CRITICAL		
						g_oConn.RollbackTrans
						exit sub
					End IF
					g_target_id = param4.value
				End if
			Next
			g_sStatus = sFormatString(L_TGUpdated_Text, array(request("u_targroup_name")))
			REM - commit
			g_oConn.CommitTrans	
		else
			setError L_TgTargetGroup_Text, L_TgGroupFailToLoadXml_ErrorMessage, sGetXMLError(xmlTargetGroup.parseError), ERROR_ICON_ALERT
			g_oConn.RollbackTrans
		End if
		''Cleanup all parameters
		CleanUpParameters(g_oCmd)
	End if
End Sub
%>
<html>
<head>

	<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">


<script LANGUAGE="VBScript">
<!--
	sub LoadTargetGroup()
		postform.all("targetgroup").value = window.event.XMLlist.xml
	end sub	

	sub SaveXMLList()
		dim sXML, xmlList
		dim sExprId, sTargetGroupName

		set xmlList = DynTargetGroup.XMLlist
		sExprId = xmlList.selectSingleNode ("//i_target_expr_id").text
		sXML = xmlList.xml
		sTargetGroupName = document.all.u_targroup_name.value
		
		if window.event.srcElement is elGetTaskBtn("savenew") then
			postform.type.value = "<%= formValues("type") %>savenew"
		end if		
		postform.all("targetgroup").value = sXML
		postform.submit
	end sub
	
	sub CreateExprOnClick
		dim urlstr, paramstr
		dim retvalue, retarry
	
		'if edit add postform.i_disc_cond_expr.value or new add nothing
		paramstr = "catalog_expr_id=-1"
		urlstr = "dlg_targetexpr.asp?"
	
		retvalue = window.showModalDialog(urlstr & paramstr, ,"dialogHeight:325px;dialogWidth:550px;status:no;help:no")		
		If retvalue = -1 or retvalue = "" then
		    EnableTask "new"
			Exit Sub
		Else 
		    'setting database action "disabling" mode
		    postform.all("dbaction").value = "no"
			postform.all("targetgroup").value = DynTargetGroup.XMLlist.xml
			g_bUnloading = true
			postform.submit()	
		End If
	end sub

	sub window_onLoad ()
		EnableTask "back"
 		esMasterControl.focus()
<%	if g_bErrorsOccured then
%>		'errors occured on save so set dirty bit
		SetDirty("")
<%	end if
%>	end sub
'-->
</script>
</head>

<body SCROLL="no">
<%
 
' Create the Expression Store object and connect to the database.
Dim objExprStore
Dim rsExprInfo, strName, strProfiles, strCategory
Dim bDisabled, sNewButton

on error resume next
Set objExprStore = Server.CreateObject("Commerce.ExpressionStore")
Call objExprStore.Connect(g_sExpStore_ConnectionStr)
if Err.number <> 0 then
	setError "", L_ExprStoreConnect_ErrorMessage, sGetScriptError(Err), ERROR_ICON_ALERT
else
	strCategory = "Campaigns"
	Set rsExprInfo = objExprStore.Query(strName, strCategory, strprofiles)
	if Err.number <> 0 then
		setError "", L_ExprStoreConnect_ErrorMessage, sGetScriptError(Err), ERROR_ICON_ALERT
		bDisabled = true
	else
		bDisabled = rsExprInfo.EOF
	end if
	'controling new button for empty dynamic table
	If bDisabled then
		sNewButton = "no"
	Else
		sNewButton = "yes"
	End If
end if

InsertEditTaskBar  sFormatString(L_TgTargetGroupTitle_Text, array(g_opType)), g_sStatus
%>

<!-- Insert fields needed for expressions here-->
<xml id="TargetGroupMeta">
	<dynamictable>
	    <global required="yes" newbutton="<%=sNewButton%>" keycol="i_target_expr_id" sortbykey="yes" />
	    <fields>
			<select id="i_target_expr_id" width="70" required="yes" <% If bDisabled Then %>readonly="yes"<% End If %>>
				<name><%= L_TgGroupExpression_Text %></name>
				<prompt><%= L_TgGroupExpressionPrompt_Text %></prompt>
			    <select id="i_target_expr_id">
<%				 'check here to see if there are any expressions in expr. store.
				 if Err.number = 0 then
					Do While Not rsExprInfo.EOF
%>						<option value="<%= rsExprInfo("ExprID") %>"><%= rsExprInfo("ExprName") %></option>
<%						rsExprInfo.MoveNext
					 Loop										
					 rsExprInfo.Close
					 Set rsExprInfo = Nothing

					 Call objExprStore.Disconnect()
					 Set objExprStore = Nothing
				 end if
%>			    </select>
			</select>
			<select id="i_target_action" width="30" required="yes" <% If bDisabled Then %>readonly="yes"<% End If %>>
				<name><%= L_TgGroupExpressionAction_Text %></name>
				<prompt><%= L_TgGroupExpressionSelect_Text %></prompt>
			    <select id="i_target_action">
	 				<option value="1"><%= L_TgGroupTarget_Text %></option>
					<option value="2"><%= L_TgGroupRequire_Text %></option>
					<option value="3"><%= L_TgGroupExclude_Text %></option>
					<option value="4"><%= L_TgGroupSponsor_Text %></option>
			    </select>
			</select>
	    </fields>
	</dynamictable>
</xml>
<xml id="TargetGroupData">
<%
	if Request("dbaction") = "no" or g_bErrorsOccured then
		Response.write Request("targetgroup")
	elseif formValues("i_targroup_id") <> -1 then
		Dim sqltext
		sqltext = "SELECT i_target_expr_id, i_target_action FROM target WHERE i_targroup_id = " & formValues("i_targroup_id")
		Response.Write xmlGetXMLFromQuery(g_sCampaignConnectionStr, sqltext).xml
	else
%>		<document/>
<%	end if
%>
</xml>

<xml id='esMasterData'>
	<document><record><u_targroup_name><%= g_mscsPage.HTMLEncode(formValues("u_targroup_name")) %></u_targroup_name></record></document>
</xml>

<xml id='esMasterMeta'>
	<editsheet>
	    <global title='no'/>
		<fields>
		    <text id='u_targroup_name' subtype='short' required='yes' maxlen='50'>
		        <prompt><%= L_TgEnterNamePrompt_Text %></prompt>
		    </text>
		</fields>
	    <template fields='u_targroup_name' register='DynTargetGroup'><![CDATA[
			<table border=0><tr><td width='20'><%= g_imgRequired %></td><td>
			<%= L_TgGroupName_Text %>
			</td><td width='200'>
			<DIV ID='u_targroup_name'><%= L_BDSSLoadingField_Text %></DIV>
			</td></tr></table>
			<br><br>
 
			<div ID="DynTargetGroup" CLASS="dynamicTable" MetaXML="TargetGroupMeta" DataXML="TargetGroupData" LANGUAGE="VBScript" ONCHANGE="LoadTargetGroup()"></div>
 
			<p>
			<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Button CLASS='bdbutton' id='CreateExprButton' name= 'CreateExprButton' OnClick='CreateExprOnClick()'><%=L_TgGroupCreateNewExpr_Text%></button></div>
			&nbsp;
	    ]]></template>
	</editsheet>
</xml>
 
<div ID="editSheetContainer" CLASS="editPageContainer">
<form ID="postform"	ACTION="targetgrp.asp" METHOD="post" ONTASK="SaveXMLList()">
	<input TYPE="hidden" NAME="type" VALUE="<%= formValues("type") %>">
	<input TYPE="hidden" NAME="dbaction" VALUE="<%= formValues("dbaction") %>">	
	<input TYPE="hidden" NAME="targetgroup" VALUE>
	<input TYPE="hidden" NAME="i_targroup_id" Value="<%= formValues("i_targroup_id") %>">
	<div ID='esMasterControl' class='editSheet'
			MetaXML='esMasterMeta'
			DataXML='esMasterData'
			LANGUAGE="VBScript"
<%		If not bDisabled Then
%>			ONCHANGE="setDirty(&quot;&quot;)"
<%		End IF
%>			ONREQUIRE="setRequired(&quot;&quot;)"
			ONVALID="setValid(&quot;&quot;)"
			><h3><%= g_imgLoadingAnimation %> <%= sFormatString(L_BDSSLoadingXProperties_Text, array(L_TgTargetGroup_Text)) %></h3></div>
</div>		  
</form>
</body>

</html>

