<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<%
dim sListID, sListName, sListDesc
dim oLM, aPostVal
dim retvarId, sReturnVarOpID
dim rsPrimary
dim sFriendlyError, sScriptError
dim g_sCampaignConnectionStr

if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

On Error resume next

'Get the Selections made by the user
'aPostVal(0) = type of action: values - 'add' or 'subtract'
'aPostVal(1) = sListID of the second list
'aPostVal(2) = name of the destination list name
sListID = Request.Form("list_id")
aPostVal = Split(Request.Form("listdata"), "|||", -1, 1)
sListName = aPostVal(0)
sListDesc = aPostVal(1)

'create the ListManager object
set oLM = Server.CreateObject("Commerce.ListManager")

'Initialiase List Manager.Object
oLM.Initialize g_sCampaignConnectionStr
if err.Number <> 0 then
	sFriendlyError = sGetErrorByID("Cant_connect_db")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
	Set oLM = Nothing
	Response.redirect "listmanager.asp?type=convert&status=0"	
end if

oLM.ExtractMailingList sListID, sListName, sListDesc, 0, sReturnVarOpID, false
if err.Number <> 0 then
	sFriendlyError = sGetErrorByID("Cant_convert_list")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
	Set oLM = Nothing
	Response.redirect "listmanager.asp?type=convert&status=0"	
end if	

Set oLM = nothing		
Response.Redirect "listmanager.asp?type=convert&status=1"
%>