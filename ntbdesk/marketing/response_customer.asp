<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE="include/marketingStrings.asp" -->
<%
Dim g_dForm, g_sCampaignConnectionStr, g_rsQuery, g_sQuery, g_xmlReturn

Set g_dForm = dGetRequestXMLAsDict()
Set g_xmlReturn = xmlGetXMLDOMDoc()

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
if g_oConn.State = AD_STATE_CLOSED then
	AddErrorNode g_xmlReturn, "1", sGetErrorByID("Cant_connect_db"), ""
	'clear errors set in session
	Session("MSCSBDError") = empty
else
	Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

	Select Case g_dForm("dbaction")
		Case "insert"
			InsertCustomer()
		Case "update"
			UpdateCustomer()
		Case Else
			AddErrorNode g_xmlReturn, "1", "", ""
	End Select
end if

Response.Write g_xmlReturn.xml

Sub RollBackAndReportError(byRef oConn, byVal sErrorText)
	Dim oError
	For Each oError In oConn.Errors
		AddErrorNode g_xmlReturn, "1", sErrorText, "<![CDATA[" & sGetADOError(oError) & "]]>"
	Next
	'roll back the transaction:
	oConn.RollbackTrans
End Sub

Sub InsertCustomer()
	Dim sModifiedBy
	g_dForm("i_industry_id") = CLng(g_dForm("i_industry_id"))
	If g_dForm("i_industry_id") = 0 Then g_dForm("i_industry_id") = Null
	sModifiedBy = Request.ServerVariables("LOGON_USER")
	With g_oCmd	
		.CommandText = "sp_InsertCustomer"
		.CommandType = AD_CMD_STORED_PROC
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("industry_id", AD_INTEGER, AD_PARAM_INPUT, , g_dForm("i_industry_id"))
			.Append g_oCmd.CreateParameter("cust_name", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_name")), g_dForm("u_customer_name"))
			.Append g_oCmd.CreateParameter("cust_contact", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_contact")), g_dForm("u_customer_contact"))
			.Append g_oCmd.CreateParameter("cust_url", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_url")), g_dForm("u_customer_url"))
			.Append g_oCmd.CreateParameter("cust_address", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_address")), g_dForm("u_customer_address"))
			.Append g_oCmd.CreateParameter("cust_type", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dForm("i_customer_type")))
			.Append g_oCmd.CreateParameter("cust_phone", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_phone")), g_dForm("u_customer_phone"))
			.Append g_oCmd.CreateParameter("cust_fax", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_fax")), g_dForm("u_customer_fax"))
			.Append g_oCmd.CreateParameter("cust_email", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_email")), g_dForm("u_customer_email"))
			.Append g_oCmd.CreateParameter("cust_modified_by", AD_BSTR, AD_PARAM_INPUT, Len(sModifiedBy), sModifiedBy)
			.Append g_oCmd.CreateParameter("cust_comments", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_comments")), g_dForm("u_customer_comments"))
			.Append g_oCmd.CreateParameter("o_cust_id", AD_INTEGER, AD_PARAM_OUTPUT )
		End With
	End With

	g_oConn.BeginTrans
	On Error Resume Next
	g_oCmd.Execute

	If g_oConn.Errors.Count > 0 Then
		RollBackAndReportError g_oConn, L_CuUnableToSave_ErrorMessage
		Exit Sub
	Else
		g_oConn.CommitTrans
	End If
	On Error Goto 0
		
	AddItemNode "customer_id", g_oCmd.Parameters("o_cust_id").Value
End Sub

Sub UpdateCustomer()
	Dim sModifiedBy
	g_dForm("i_industry_id") = CLng(g_dForm("i_industry_id"))
	If g_dForm("i_industry_id") = 0 Then g_dForm("i_industry_id") = Null
	sModifiedBy = Request.ServerVariables("LOGON_USER")
	With g_oCmd	
		.CommandText = "sp_UpdateCustomer"
		.CommandType = AD_CMD_STORED_PROC
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("cust_id", AD_INTEGER, AD_PARAM_INPUT, , g_dForm("i_customer_id"))
			.Append g_oCmd.CreateParameter("industry_id", AD_INTEGER, AD_PARAM_INPUT, , g_dForm("i_industry_id"))
			.Append g_oCmd.CreateParameter("cust_name", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_name")), g_dForm("u_customer_name"))
			.Append g_oCmd.CreateParameter("cust_contact", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_contact")), g_dForm("u_customer_contact"))
			.Append g_oCmd.CreateParameter("cust_url", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_url")), g_dForm("u_customer_url"))
			.Append g_oCmd.CreateParameter("cust_address", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_address")), g_dForm("u_customer_address"))
			.Append g_oCmd.CreateParameter("cust_type", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dForm("i_customer_type")))
			.Append g_oCmd.CreateParameter("cust_phone", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_phone")), g_dForm("u_customer_phone"))
			.Append g_oCmd.CreateParameter("cust_fax", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_fax")), g_dForm("u_customer_fax"))
			.Append g_oCmd.CreateParameter("cust_email", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_email")), g_dForm("u_customer_email"))
			.Append g_oCmd.CreateParameter("cust_modified_by", AD_BSTR, AD_PARAM_INPUT, Len(sModifiedBy), sModifiedBy)
			.Append g_oCmd.CreateParameter("cust_comments", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_customer_comments")), g_dForm("u_customer_comments"))
		End With
	End With

	g_oConn.BeginTrans
	On Error Resume Next
	g_oCmd.Execute

	If g_oConn.Errors.Count > 0 Then
		RollBackAndReportError g_oConn, L_CuUnableToSave_ErrorMessage
		Exit Sub
	Else
		g_oConn.CommitTrans
	End If
	On Error Goto 0

	AddItemNode "customer_id", g_dForm("i_customer_id")
End Sub
%>