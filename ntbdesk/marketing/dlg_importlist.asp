<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->

<%
dim g_sCampaignConnectionStr
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

const L_DialogType1Height_Style = "300px"
const L_DialogType2Height_Style = "400px"
%>

<HTML>
<HEAD>
<TITLE><%=L_ImportListTitle_Text%></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
HTML
{
    WIDTH: 440px;
    HEIGHT: <%= L_DialogType1Height_Style %>;
}
BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
BUTTON
{
	WIDTH: 6em;
}
</STYLE>
<SCRIPT LANGUAGE='VBScript'>
<!--
	
	Sub ListTypeChange()
		select case list_creation_type.value
			case "1"
				window.dialogHeight = "<%= L_DialogType1Height_Style %>"
				list_type_1.style.display ="block"
				list_source_sql.style.display = "none"
				list_type_2.style.display = "none"
			case "2"
				window.dialogHeight = "<%= L_DialogType2Height_Style %>"
				list_source_sql.style.display = "block"
				list_type_2.style.display = "block"
				list_type_1.style.display ="none"
		end select
	End sub

	Function bdcancel_OnClick()
		window.returnvalue = -1
		window.close()
	end function

	sub bdcontinue_OnClick() 
		dim aReturnValues(5), nListType

		'get the list type; Static or Dynamic
		if list_type(0).checked then
			aReturnValues(0) = list_type(0).value
		else
			aReturnValues(0) = list_type(1).value
		End if

		'get the name of list
		aReturnValues(1) = list_name.value

		'get the list_creation_type					
		nListType = list_creation_type.value
		aReturnValues(2) = nListType

		'get the values for corresponding list_creation_type
		select case nListType
			case 1		'Import from A File
				aReturnValues(3) = list_source_file.value
			Case 2		'Import from SQL
				aReturnValues(3) = list_source_sql.value
				aReturnValues(4) = list_query.value
		End select

		window.returnvalue = Join(aReturnValues, "|||")
		window.close()
	end sub

	Sub onChange()
		on error resume next
		if list_name.valid and not list_name.required then
			if list_creation_type.value = 1 and _
				list_source_file.valid and not list_source_file.required then
				bdcontinue.disabled = false
			elseif list_creation_type.value = 2 and _
				list_source_sql.valid and not list_source_sql.required and _
				list_query.valid and not list_query.required then
				bdcontinue.disabled = false
			else
				bdcontinue.disabled = true
			end if
		else
			bdcontinue.disabled = true
		end if
	End sub

	Sub window_onload()
		list_name.focus()
	end sub
'-->
</SCRIPT>

</HEAD>
<BODY SCROLL=no LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<xml id="efMeta">
	<fields>
		<text id='list_name' required="yes" maxlen="64" subtype="short" onrequire='OnChange' onvalid='OnChange'>
	        <name><%= L_ListName_Text %></name>
	        <tooltip><%= L_TypeEntry_ToolTip %></tooltip>
	        <error><%= L_ListnameLong_ErrorMessage %></error>
			<charmask>.*</charmask>
	    </text>
	    <select id='list_creation_type' required="yes" default='1' onchange='OnChange'>
	       <name>"<%= L_ListSource_Text %>"</name>
	        <select id='list_creation_type'>
				<option value="1"><%= L_ListCreationOption1_Text %></option>
				<option value="2"><%= L_ListCreationOption2_Text %></option>						
		    </select>
	    </select>
	    
	    <text id='list_source_file' required="yes" maxlen="256" subtype="short" onrequire='OnChange' onvalid='OnChange'>
	        <name><%= L_PathName_Text %></name>
	        <tooltip><%= L_TypeEntry_ToolTip %></tooltip>
	        <error><%= L_PathName_ErrorMessage %></error>
	        <charmask>[^?"|&lt;&gt;]*</charmask>
	    </text>
	    
		<text id='list_source_sql' required="yes" maxlen="260" subtype="short" onrequire='OnChange' onvalid='OnChange'>
	        <charmask>(\S*\s*\S*)*</charmask>
	        <name><%= L_ConnectionString_Text %></name>
	        <tooltip><%= L_TypeEntry_ToolTip %></tooltip>
	        <error><%= L_ConnString_ErrorMessage %></error>
	    </text>

	    <text id='list_query' required="yes" maxlen='1024' subtype='long' onrequire='OnChange' onvalid='OnChange'>
	        <charmask>(\S*\s*\S*)*</charmask>
			<name><%= L_SQLQuery_Text %></name>
			<tooltip><%= L_TypeEntry_ToolTip %></tooltip>
			<error><%= L_SQLQuery_ErrorMessage %></error>
		</text>
	    
	</fields> 
</xml>
<xml id="efData">
	<document/>
</xml>

<%= L_ListType_Text %> 
<table border='0' width="100%" style="TABLE-LAYOUT: fixed">
	<tr>
		<td width='15'>&nbsp;</td>
		<td>
			<INPUT type='radio' id="Static" name='list_type' VALUE='0' CHECKED
				STYLE="BACKGROUND: #cccccc">
			<label FOR="Static"><%= L_ListType1_Text %></label>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<INPUT type='radio' id="Dynamic" name='list_type' VALUE='2'
				STYLE="BACKGROUND: #cccccc">
			<label FOR="Dynamic"><%= L_ListType2_Text %></label>
		</td>
	</tr>
</table>

<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
<tr>
      <td><%= L_ListName_Text %></td>
</tr>
<tr>
      <td>
		<DIV ID="list_name" CLASS="editField"
			MetaXML="efMeta" DataXML="efData"></DIV>
	  </td>
</tr>
<tr>
      <td><%= L_ListSource_Text %></td>
</tr>
<tr>
      <td>		
		<DIV ID="list_creation_type" CLASS="editField"
			MetaXML="efMeta" DataXML="efData"
			OnChange='ListTypeChange()'></DIV><br>&nbsp;
	  </td>
</tr>

<tr>   
	<td valign=top>
	<div ID='list_type_1'>
		<%= L_FileProperty_Text %>
		<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
			<tr>
				<td>
					<label for='list_source_file'><%= L_PathName_Text %></label>
				</td>
			</tr>
			<tr>
				<td>
					<DIV ID="list_source_file" CLASS="editField"
						MetaXML="efMeta" DataXML="efData"></DIV>
				</td>
			</tr>
		</table>	
	</div>
	<div ID="list_type_2" style='display:none'>
		<%= L_DatabaseProperty_Text %>
		<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
			<tr>
				<td>
					<label for='list_source_sql'><%= L_ConnectionString_Text %></label>
				</td>
			</tr>
			<tr>
				<td>
					<DIV ID='list_source_sql' CLASS='editField'
						METAXML='efMeta' DATAXML='efdata'></DIV>
				</td>
			</tr>
			<tr>
				<td><label for='list_query'><%= L_SQLQuery_Text %></label></td>
			</tr>
			<tr>
			    <td>		
				  	<DIV ID='list_query' CLASS='editField'
				  		METAXML='efMeta' DATAXML='efdata'></DIV>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>		
	</td>
</tr>
</table>
</FORM>

<table border=0 cellpadding=0 cellspacing=1 width="100%">
	<tr>
	  <td width="100%">&nbsp;</td>
		<td><nobr>
			<BUTTON ID='bdcontinue' CLASS='bdbutton' DISABLED><%= L_OK_Text %></BUTTON>
			<BUTTON ID='bdcancel' CLASS='bdbutton'><%= L_Cancel_Text %></BUTTON>
			<BUTTON ID='bdhelp' CLASS='bdbutton'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "cs_bd_campaigns_LORI.htm"'><%= L_Help_Text %></BUTTON>
		</nobr></td>
	</tr>
</table>

</BODY>
</HTML>
