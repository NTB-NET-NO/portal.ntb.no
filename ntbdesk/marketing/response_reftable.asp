<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE="include/marketingStrings.asp" -->
<%
Dim g_dForm, g_sCampaignConnectionStr, g_rsQuery, g_sQuery, g_xmlReturn, g_dBadDeletes

Set g_dForm = dGetRequestXMLAsDict()
Set g_xmlReturn = xmlGetXMLDOMDoc()

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
if g_oConn.State = AD_STATE_CLOSED then
	AddErrorNode g_xmlReturn, "1", sGetErrorByID("Cant_connect_db"), ""
	'clear errors set in session
	Session("MSCSBDError") = empty
else
	Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

	SaveRefTables()
end if

Response.Write g_xmlReturn.xml

Sub RollBackAndReportError(byRef oConn, byVal sErrorText)
	Dim oError
	For Each oError In oConn.Errors
		AddErrorNode g_xmlReturn, "1", sErrorText, "<![CDATA[" & sGetADOError(oError) & "]]>"
	Next
	'roll back the transaction:
	oConn.RollbackTrans
End Sub

Sub SaveRefTables()
	'Dim xmlContentSize, xmlIndustryCode, xmlPageGroup, xmlDoc, xmlRecords, xmlRecord
	Dim xmlDoc
	Set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")
	set g_dBadDeletes = server.CreateObject("Commerce.Dictionary")
	SaveContentSize g_dForm("xmlContentSize"), xmlDoc
	SaveIndustryCode g_dForm("xmlIndustryCode"), xmlDoc
	SavePageGroup g_dForm("xmlPageGroup"), xmlDoc
	set session("rtBadDeletes") = g_dBadDeletes
	Set xmlDoc = Nothing
End Sub

Sub SaveContentSize(xmlContentSize, xmlDoc)
	Dim xmlRecords, xmlRecord
	If xmlDoc.loadXML(xmlContentSize) Then
		Set xmlRecords = xmlDoc.selectNodes("//document/record")
		For Each xmlRecord In xmlRecords
			Select Case xmlRecord.getAttribute("state")
				Case "new"
					InsertContentSize(xmlRecord)
				Case "changed"
					UpdateContentSize(xmlRecord)
				Case "deleted"
					DeleteContentSize(xmlRecord)
				Case Else
					'do nothing
			End Select
		Next
	End If
End Sub

Sub SaveIndustryCode(xmlIndustryCode, xmlDoc)
	Dim xmlRecords, xmlRecord
	If xmlDoc.loadXML(xmlIndustryCode) Then
		Set xmlRecords = xmlDoc.selectNodes("//document/record")
		For Each xmlRecord In xmlRecords
			Select Case xmlRecord.getAttribute("state")
				Case "new"
					InsertIndustryCode(xmlRecord)
				Case "changed"
					UpdateIndustryCode(xmlRecord)
				Case "deleted"
					DeleteIndustryCode(xmlRecord)
				Case Else
					'do nothing
			End Select
		Next
	End If
End Sub

Sub SavePageGroup(xmlPageGroup, xmlDoc)
	Dim xmlRecords, xmlRecord
	If xmlDoc.loadXML(xmlPageGroup) Then
		Set xmlRecords = xmlDoc.selectNodes("//document/record")
		For Each xmlRecord In xmlRecords
			Select Case xmlRecord.getAttribute("state")
				Case "new"
					InsertPageGroup(xmlRecord)
				Case "changed"
					UpdatePageGroup(xmlRecord)
				Case "deleted"
					DeletePageGroup(xmlRecord)
				Case Else
					'do nothing
			End Select
		Next
	End If
End Sub

function Quote(sText)
	If IsNull(sText) Or IsEmpty(sText) Or sText = "" Then Exit Function
	Quote = replace(sText, "'", "\'")
end function

Sub InsertContentSize(xmlRecord)
	Dim sSizeName, sSizeTag, nSizeWidth, nSizeHeight
	With xmlRecord
		sSizeName = Quote(.selectSingleNode("./u_size_name").text)
		sSizeTag = Quote(.selectSingleNode("./u_size_tag").text)
		nSizeWidth = CInt(.selectSingleNode("./i_size_width").text)
		nSizeHeight = CInt(.selectSingleNode("./i_size_height").text)
	End With
	g_sQuery = "INSERT INTO creative_size (u_size_name, u_size_tag, i_size_width, i_size_height) " & _
		"VALUES	('" & Quote(sSizeName) & "', '" & Quote(sSizeTag) & "', " & nSizeWidth & ", " & nSizeHeight & ")"
	executeQuery g_oConn, g_sQuery, L_ReUnableToSaveContentSize_ErrorMessage
End Sub

Sub InsertIndustryCode(xmlRecord)
	Dim sIndustryCode
	sIndustryCode = xmlRecord.selectSingleNode("./u_industry_code").text
	g_sQuery = "INSERT INTO industry_code (u_industry_code) " & _
		"VALUES ('" & Quote(sIndustryCode) & "')"
	executeQuery g_oConn, g_sQuery, L_ReUnableToSaveIndustryCode_ErrorMessage
End Sub

Sub InsertPageGroup(xmlRecord)
	Dim sPGTag, sPGDescription
	With xmlRecord
		sPGTag = Quote(.selectSingleNode("./u_pg_tag").text)
		sPGDescription = Quote(.selectSingleNode("./u_pg_description").text)
	End With
	g_sQuery = "INSERT INTO page_group (u_pg_tag, u_pg_description) " & _
		"VALUES ('" & Quote(sPGTag) & "', '" & Quote(sPGDescription) & "')"
	executeQuery g_oConn, g_sQuery, L_ReUnableToSavePageGroup_ErrorMessage
End Sub

Sub UpdateContentSize(xmlRecord)
	Dim nCreativeSizeID, sSizeName, sSizeTag, nSizeWidth, nSizeHeight
	With xmlRecord
		nCreativeSizeID = CLng(.selectSingleNode("./i_creative_size_id").text)
		sSizeName = Quote(.selectSingleNode("./u_size_name").text)
		sSizeTag = Quote(.selectSingleNode("./u_size_tag").text)
		nSizeWidth = CInt(.selectSingleNode("./i_size_width").text)
		nSizeHeight = CInt(.selectSingleNode("./i_size_height").text)
	End With
	g_sQuery = "UPDATE creative_size SET u_size_name = '" & Quote(sSizeName) & "', u_size_tag = '" & Quote(sSizeTag) & _
		"', i_size_width = " & nSizeWidth & ", i_size_height = " & nSizeHeight & _
		" WHERE i_creative_size_id = " & nCreativeSizeID
	executeQuery g_oConn, g_sQuery, L_ReUnableToSaveContentSize_ErrorMessage
End Sub

Sub UpdateIndustryCode(xmlRecord)
	Dim nIndustryID, sIndustryCode
	With xmlRecord
		nIndustryID = CLng(.selectSingleNode("./i_industry_id").text)
		sIndustryCode = Quote(.selectSingleNode("./u_industry_code").text)
	End With
	g_sQuery = "UPDATE industry_code SET u_industry_code = '" & Quote(sIndustryCode) & "' " & _
		"WHERE i_industry_id = " & nIndustryID 
	executeQuery g_oConn, g_sQuery, L_ReUnableToSaveIndustryCode_ErrorMessage
End Sub

Sub UpdatePageGroup(xmlRecord)
	Dim nPGID, sPGTag, sPGDescription
	With xmlRecord
		nPGID = CLng(.selectSingleNode("./i_pg_id").text)
		sPGTag = Quote(.selectSingleNode("./u_pg_tag").text)
		sPGDescription = Quote(.selectSingleNode("./u_pg_description").text)
	End With
	g_sQuery = "UPDATE page_group SET u_pg_tag = '" & Quote(sPGTag) & "', u_pg_description = '" & Quote(sPGDescription) & "' " & _
		"WHERE i_pg_id = " & nPGID
	executeQuery g_oConn, g_sQuery, L_ReUnableToSavePageGroup_ErrorMessage
End Sub

Sub DeleteContentSize(xmlRecord)
	Dim nCreativeSizeID, bSuccess, sName
	nCreativeSizeID = CLng(xmlRecord.selectSingleNode("./i_creative_size_id").text)
	g_sQuery = "DELETE FROM creative_size WHERE i_creative_size_id = " & nCreativeSizeID
	sName = xmlRecord.selectSingleNode("./u_size_name").text
	bSuccess = executeQuery(g_oConn, g_sQuery, sFormatString(L_ReDeleteContentSizeConflict_ErrorMessage, Array(sName)))
	if not bSuccess Then
		g_dBadDeletes(nCreativeSizeID) = sName
	end if
End Sub

Sub DeleteIndustryCode(xmlRecord)
	Dim nIndustryID, bSuccess, sName
	nIndustryID = CLng(xmlRecord.selectSingleNode("./i_industry_id").text)
	g_sQuery = "DELETE FROM industry_code WHERE i_industry_id = " & nIndustryID
	sName = xmlRecord.selectSingleNode("./u_industry_code").text
	bSuccess = executeQuery(g_oConn, g_sQuery, sFormatString(L_ReDeleteIndustryCodeConflict_ErrorMessage, Array(sName)))
	if not bSuccess Then
		g_dBadDeletes(nIndustryID) = sName
	end if
End Sub

Sub DeletePageGroup(xmlRecord)
	Dim nPGID, bSuccess, sName
	nPGID = CLng(xmlRecord.selectSingleNode("./i_pg_id").text)
	g_sQuery = "DELETE FROM page_group WHERE i_pg_id = " & nPGID
	sName = xmlRecord.selectSingleNode("./u_pg_tag").text
	bSuccess = executeQuery(g_oConn, g_sQuery, sFormatString(L_ReDeletePageGroupConflict_ErrorMessage, Array(sName)))
	if not bSuccess Then
		g_dBadDeletes(nPGID) = sName
	end if
End Sub

function executeQuery(oConn, sQuery, sErrorMsg)
	oConn.BeginTrans
	On Error Resume Next
	oConn.Execute sQuery, , AD_CMD_TEXT
	If oConn.Errors.Count > 0 Then
		executeQuery = false
		RollBackAndReportError oConn, sErrorMsg	
	Else
		executeQuery = true
		oConn.CommitTrans
	End If	
	On Error Goto 0
End function
%>