<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<%
dim aPostVal
dim sListID
dim LM
dim sReturnVarID, sReturnVarOpID
dim sFriendlyError, sScriptError
dim g_sCampaignConnectionStr

on error resume next

sListID = Request.Form("list_id")
aPostVal = Split(Request.Form("listdata"), "|||", -1, 1)
'just for notes sake
'aPostVal(0) = list_creation_type -- export to File or SQL 	
'based on aPostVal(0)
'aPostVal(1) = list_source_xxx - can be a File name or a Connection string
'aPostVal(2) = list_table	-- when exporting to a SQL, gives the table name
'declare any variables needed

'create the ListManager object
set LM = Server.CreateObject("Commerce.ListManager")

if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

'Initialie List Manager.Object
LM.Initialize g_sCampaignConnectionStr
if err.Number <> 0 then
	Set LM = Nothing
		sFriendlyError = sGetErrorByID("Cant_connect_db")
		sScriptError = sGetScriptError(Err)
		setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		Response.redirect "listmanager.asp?type=export&status=0"	
end if	

'based on input
select case CInt(aPostVal(0))
	case 1		'Export to File			
		sReturnVarID = LM.ExportToFile(sListID, CStr(aPostVal(1)), FALSE, CBool(aPostVal(2)), TRUE, sReturnVarOpID )
		if err.Number <> 0 then
			Set LM = Nothing
			sFriendlyError = sGetErrorByID("Export_failed")
			sScriptError = sGetScriptError(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
			Response.redirect "listmanager.asp?type=export&status=0"	
		end if			
	case 2		'export  to database  SQL
		sReturnVarID = LM.ExportToSQL(sListID, Cstr(aPostVal(1)), CStr(aPostVal(2)), CBool(aPostVal(3)), 0, TRUE, sReturnVarOpID )	
		if err.Number <> 0 then
			Set LM = Nothing
			sFriendlyError = sGetErrorByID("Export_failed")
			sScriptError = sGetScriptError(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
			Response.redirect "listmanager.asp?type=export&status=0"	
		end if			
End select

set LM = Nothing
Response.Redirect "listmanager.asp?type=export&status=1"
%>