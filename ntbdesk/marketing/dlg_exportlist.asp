<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->

<%
dim g_sCampaignConnectionStr
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

const L_DialogType1Height_Style = "225px"
const L_DialogType2Height_Style = "270px"
%>

<HTML>
<HEAD>
<TITLE>Export a List</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
HTML
{
    WIDTH: 440px;
    HEIGHT: <%= L_DialogType1Height_Style %>;
}
BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
BUTTON
{
	WIDTH: 6em;
}
</STYLE>
<SCRIPT LANGUAGE='VBScript'>
<!--
	option explicit

	Sub ListTypeChange()
	
		select case document.all("list_creation_type").value
		case "1"
			window.dialogHeight = "<%= L_DialogType1Height_Style %>"
			document.all("list_type_1").style.display ="block"
			document.all("list_source_sql").style.display = "none"
			document.all("list_table").style.display = "none"
			document.all("list_type_2").style.display = "none"
		case "2"
			window.dialogHeight = "<%= L_DialogType2Height_Style %>"
			document.all("list_source_sql").style.display = "block"
			document.all("list_table").style.display = "block"
			document.all("list_type_2").style.display = "block"
			document.all("list_type_1").style.display ="none"
		end select

	End sub

	Function bdcancel_OnClick()
		window.returnvalue = -1
		window.close()
	end function

	sub bdcontinue_OnClick() 
		dim aReturnValues(3), nListType

		'get the list_creation_type					
		nListType = document.all.list_creation_type.value
		aReturnValues(0) = nListType

		'get the values for corresponding list_creation_type
		select case nListType
			case 1		'Export to A File
				aReturnValues(1) = document.all("list_source_file").value
				aReturnValues(2) = CBool(document.all.file_bOverWrite.checked)
			Case 2		'Export to SQL
				aReturnValues(1) = document.all("list_source_sql").value
				aReturnValues(2) = document.all("list_table").value
				aReturnValues(3) = CBool(document.all.db_bOverWrite.checked)
		End select

		window.returnvalue = Join(aReturnValues, "|||")
		window.close()
	end sub

	Sub onChange()
		on error resume next
		if list_creation_type.value = 1 and _
			list_source_file.valid and not list_source_file.required then
			bdcontinue.disabled = false
		elseif list_creation_type.value = 2 and _
			list_source_sql.valid and not list_source_sql.required and _
			list_table.valid and not list_table.required then
			bdcontinue.disabled = false
		else
			bdcontinue.disabled = true
		end if
	End sub

	Sub window_onload()
		list_creation_type.focus()
	end sub
'-->
</SCRIPT>
</HEAD>
<BODY SCROLL=no LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<xml id="efMeta">
	<fields>
	    
	    <select id='list_creation_type' required="yes" default='1' onchange='onChange()'>
	       <name>"<%= L_ExportList_Text%>"</name>
	        <select id='list_creation_type'>
				<option value="1"><%=L_ExportListOption1_Text%></option>
				<option value="2"><%=L_ExportListOption2_Text%></option>						
		    </select>
	    </select>
	    
	    <text id='list_source_file' required="yes" maxlen="256" subtype="short" onrequire='OnChange' onvalid='OnChange'>
	        <name><%=L_PathName_Text%></name>
	        <tooltip><%=L_TypeEntry_ToolTip%></tooltip>
	        <error><%=L_PathName_ErrorMessage%></error>
	    </text>
	    
		<text id='list_source_sql' required="yes" maxlen="256" subtype="short" onrequire='OnChange' onvalid='OnChange'>
	        <charmask>(\S*\s*\S*)*</charmask>
	        <name><%=L_ConnectionString_Text%></name>
	        <tooltip><%=L_TypeEntry_ToolTip%></tooltip>
	        <error><%=L_ConnString_ErrorMessage%></error>
	    </text>

	    <text id='list_table' required="yes" maxlen='256' subtype="short" onrequire='OnChange' onvalid='OnChange'>
			<name><%=L_TableName_Text%></name>
			<tooltip><%=L_TypeEntry_ToolTip%></tooltip>
			<error><%=L_TableName_ErrorMessage%></error>
		</text>
	    
	</fields> 
</xml>
<xml id="efData">
	<document>
		<record/>
	</document>
</xml>


<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
<tr>
      <td><%=L_ExportList_Text%></td>
</tr>
<tr>
      <td>		
		<DIV ID="list_creation_type" CLASS="editField"
			MetaXML="efMeta" DataXML="efData"
			OnChange='ListTypeChange()'></DIV><br>&nbsp;
	  </td>
</tr>
<tr>   
	<td valign=top>
	<div ID='list_type_1'>
		<%=L_FileProperty_Text %>
		<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
			<tr>
				<td>
					<label for='list_source_file'><%=L_PathName_Text%></label>
				</td>
			</tr>
			<tr>
				<td>
					<DIV ID="list_source_file" CLASS="editField"
						MetaXML="efMeta" DataXML="efData"></DIV>
				</td>
			</tr>
			<tr>
				<td>
					 <input type='checkbox' id='file_bOverWrite' value='1' style='BACKGROUND-COLOR: transparent'><label for='file_bOverWrite'><%= L_OverWriteFile_Text %></label>
				</td>
			</tr>
		</table>	
	</div>
	<div ID="list_type_2" style='display:none'>
		<%=L_DatabaseProperty_Text%>
		<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
			<tr>
				<td>
					<label for='list_source_sql'><%=L_ConnectionString_Text%></label>
				</td>
			</tr>
			<tr>
				<td>
					<DIV ID='list_source_sql' CLASS='editField'
						METAXML='efMeta' DATAXML='efdata'></DIV>
				</td>
			</tr>
			<tr>
				<td>
					<label for='list_table'><%=L_TableName_Text%></label>
				</td>
			</tr>
			<tr>
				<td>
					<DIV ID='list_table' CLASS='editField'
						METAXML='efMeta' DATAXML='efdata'></DIV>
				</td>
			</tr>
			<tr>
				<td>
					 <input type='checkbox' id='db_bOverWrite' value='1' style='BACKGROUND-COLOR: transparent'><label for='db_bOverWrite'><%= L_OverWriteDB_Text %></label>
				</td>
			</tr>
		</table>
	</div>
	</td>
</tr>
<tr>
	<td>
		<table border=0 width='100%'>
			<tr>
			  <td width="100%"></td>
			  <td>
				<nobr>
					<BUTTON ID='bdcontinue' CLASS='bdbutton' DISABLED><%=L_OK_Text%></BUTTON>
					<BUTTON ID='bdcancel' CLASS='bdbutton'><%=L_Cancel_Text%></BUTTON>
					<BUTTON ID='bdhelp' CLASS='bdbutton'
						LANGUAGE='VBScript'
						ONCLICK='openHelp "cs_bd_campaigns_LORI.htm"'><%= L_Help_Text %></BUTTON>
				</nobr>
			  </td>
			</tr>
		</table>
	</td>
</tr>
</table>	


</BODY>
</HTML>
