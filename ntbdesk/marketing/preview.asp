<!--#INCLUDE FILE='../include/BDHeader.asp' -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->
<!--#INCLUDE FILE="include/marketingStrings.asp" -->
<%
dim g_sCampaignConnectionStr

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Function sGetContentPreview()
'
' Parmeters:
'   nItemID:         item_id to get values for
'   sRedirectURL:    redirect url to use for click handling
'
' Description:
'   Entry point for previewing a content item.  Generates and returns
'   an HTML string representation of the content item.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
function sGetContentPreview(nItemID, sRedirectURL)
    dim dDictValues, dDictContext, dTemplateDict, oConn

    ' create an ADO connection
    set oConn = Server.CreateObject("ADODB.Connection")
    oConn.Open g_sCampaignConnectionStr
        
    ' get campaign item properties dictionary
    set dDictValues = dGetValuesDictionary(nItemID, oConn)
        
    ' create a Context dictionary
    set dDictContext = Server.CreateObject("Commerce.Dictionary")
    dDictContext("CacheName") = "NULL"
    dDictContext("RedirectUrl") = sRedirectURL
    dDictContext("TargetFrame") = "_new"
        
    ' get the Format Template dictionary
    set dTemplateDict = dGetTemplateDictionary(dDictValues("creative_id"), oConn)
        
    ' generate the HTML string using the CSF FormatTemplate component
    sGetContentPreview = GeneratePreview(dDictValues, dDictContext, dTemplateDict)
end function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Function dGetValuesDictionary()
'
' Parmeters:
'   nItemID:    item_id to get values for
'   oConn:		open ADO connection
'
' Description:
'   Creates a dictionary of name/value pairs for all campaign item properties.
'   There is a second query made for advertisements and discounts to retrieve
'   properties specific to those types of campaign items.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
function dGetValuesDictionary(nItemID, oConn)
	dim sQuery, rsQuery, dDictValues, sGUIDCampType
	    
    ' create the dictionary we will be populating
    set dDictValues = Server.CreateObject("Commerce.Dictionary")
	    
    ' get properties that all campaign items share
	sQuery = _
	    "SELECT ci.i_campitem_id AS item_id," & _
	    "       ci.u_campitem_name AS name," & _
	    "       ci.dt_campitem_start AS date_start," & _
	    "       ci.dt_campitem_end AS date_end," & _
	    "       ci.i_camp_id AS campaign_id," & _
	    "       ci.guid_campitem_type AS campitem_type," & _
	    "       cr.i_creative_id AS creative_id," & _
	    "       sz.i_size_width AS width, " & _
	    "       sz.i_size_height AS height " & _
	    "FROM campaign_item as ci " & _
	    "INNER JOIN creative AS cr ON ci.i_creative_id = cr.i_creative_id " & _
	    "INNER JOIN creative_size AS sz ON cr.i_creative_size_id = sz.i_creative_size_id " & _
	    "WHERE ci.i_campitem_id = "
    set dDictValues = dGetQueryProperties(nItemID, oConn, sQuery, dDictValues)
    sGUIDCampType = dDictValues("campitem_type")

    if sGUIDCampType = GUID_TYPE_AD then
		' add in properties specific to advertisements
		sQuery = _
		    "SELECT i.u_industry_code AS industry " & _
		    "FROM   industry_code AS i " & _
		    "INNER JOIN ad_item AS ad ON ad.i_industry_id = i.i_industry_id " & _
		    "WHERE ad.i_campitem_id = "
        set dDictValues = dGetQueryProperties(nItemID, oConn, sQuery, dDictValues)    
    elseif sGUIDCampType = GUID_TYPE_DISC then
		' add in properties specific to discounts
		sQuery = _
		    "SELECT u_disc_description AS description " & _
		    "FROM order_discount " & _
		    "WHERE i_campitem_id = "
        set dDictValues = dGetQueryProperties(nItemID, oConn, sQuery, dDictValues)    
    end if	    
	    	    
    ' get name/value pairs from the creative_property_values table

	sQuery = _
	    "SELECT cp.u_cp_name AS name," & _
	    "       cpv.text_cpv_value AS value " & _
	    "FROM creative_property_value AS cpv " & _
	    "INNER JOIN creative_property AS cp ON cpv.i_cp_id = cp.i_cp_id " & _
	    "WHERE cpv.i_creative_id = " & cstr(dDictValues("creative_id"))
    set rsQuery = oConn.Execute (sQuery)
    while not rsQuery.EOF            
        dDictValues(rsQuery("name").Value) = rsQuery("value").Value
        rsQuery.MoveNext
    wend
        
    ' clean up
    rsQuery.close
    set rsQuery = nothing
        
    ' return the dictionary we've built
    set dGetValuesDictionary = dDictValues
end function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub dGetQueryProperties()
'
' Parmeters:
'   nItemID:	   creative_id of the item being formatted
'   oConn:			open ADO connection
'   sQuery:         the SQL query to execute
'   dDictValues:    a commerce.dictionary object to put the properties into
'
' Description:
'   Executes the given query, appending the nItemID at the end, and then
'   converts all values in the first record (it's assumed the query only returns
'   a single record) into name/value pairs in the dictionary.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
function dGetQueryProperties(nItemID, oConn, sQuery, dDictValues)
    dim rsQuery, oField
    set rsQuery = oConn.Execute (sQuery & cstr(nItemID))
    if not rsQuery.EOF then
        for each oField in rsQuery.Fields
            dDictValues(oField.Name) = oField.Value
        next
    end if
    rsQuery.Close
    set dGetQueryProperties = dDictValues
end function

    
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Function dGetTemplateDictionary()
'
' Parmeters:
'   nCreativeID:    creative_id of the item being formatted
'   oConn:			open ADO connection
'
' Description:
'   Sets up a Format Template Dictionary with the values needed to format
'   the given creative.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
function dGetTemplateDictionary(nCreativeID, oConn)        
    dim sQuery, rsQuery, sName, dTemplateDict, nCreativeTypeID

    ' create the Format Template Dictionary we will be populating
    set dTemplateDict = Server.CreateObject("Commerce.Dictionary")
        
    ' get the creative_type id and the text of the template
	sQuery = _
	    "SELECT ct.i_creative_type_id AS creative_type_id, " & _
	    "       text_ct_template AS text " & _
	    "FROM creative_type AS ct " & _
	    "INNER JOIN creative AS cr ON ct.i_creative_type_id = cr.i_creative_type_id " & _
	    "WHERE i_creative_id = " & cstr(nCreativeID)
    set rsQuery = oConn.Execute (sQuery)
    if not rsQuery.EOF then
        nCreativeTypeID = rsQuery("creative_type_id")
        dTemplateDict("text") = rsQuery("text")            
    end if       
    rsQuery.Close
        
    ' query the creative_property table for attributes of creative_properties
    ' referenced in the template
	sQuery = _
	   "SELECT u_cp_name AS name, " & _
	   "       u_cp_default AS deflt, " & _
	   "       i_cp_source AS source, " & _
	   "       i_cp_encoding AS encoding " & _
	   "FROM creative_property WHERE i_creative_type_id = " & cstr(nCreativeTypeID)
    set rsQuery = oConn.Execute (sQuery)
    while NOT rsQuery.EOF
        sName = rsQuery("name")
        dTemplateDict(sName & "_default") = rsQuery("deflt")
        dTemplateDict(sName & "_source") = rsQuery("source")
        dTemplateDict(sName & "_encoding") = rsQuery("encoding")
        rsQuery.MoveNext
    wend
        
    ' clean up
    rsQuery.Close
    set rsQuery = nothing
        
    set dGetTemplateDictionary = dTemplateDict
end function
    
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Function GeneratePreview()
'
' Parmeters:
'   dDictValues: populated with all values from the creative_property_value
'                table for the item.  The 'Width', 'Height', and 'nCreativeID'
'                keys should also be set.
'   dDictContext: should contain the key 'RedirectUrl'
'
' Description:
'   After the Order, Context, and Format Template dictionaries have been
'   set up, this function is called.  It uses the FormatTemplate CSF component
'   to generate the final HTML string
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function GeneratePreview(dDictValues, dDictContext, dTemplateDict)
    Dim oFormatTemplate, dDictOrder, oFactory, oContentList, oSchema, oFields, _
		nErrLvl, nCreativeID, listFormatted, oMicroPipe

    Const CLREAD_ONLY = 1
    Const CLPRIVATE = 2
    Const CLCOL_DISPATCH = 9
    Const CLCOL_I4 = 3
        
    ' This is the item we are formatting
    nCreativeID = dDictValues("item_id")
        
    ' Set up the schema and shared data in the factory        
    Set oFactory = Server.CreateObject("Commerce.ContentListFactory")
    Set oSchema = oFactory.Schema
    oSchema.Add "Values", CLCOL_DISPATCH, CLREAD_ONLY
    oSchema.Add "item_id", CLCOL_I4, CLREAD_ONLY
    oSchema.Add "dummy", CLCOL_I4, CLPRIVATE 
    oFactory.count = 1
    ' Set the "Values" entry in the contentlist object
    Set oFields = oFactory.Fields(0)
    Set oFields.values = dDictValues
    oFields.item_id = nCreativeID

    ' Create a ContentList
    Set oContentList = oFactory.CreateNewContentList
        
    ' Create an Order dictionary to pass the pipeline
    Set dDictOrder = Server.CreateObject("Commerce.Dictionary")        
    Set dDictOrder("_content") = oContentList
    Set dDictOrder("FormatTemplate") = dTemplateDict
    dDictOrder("_winners") = nCreativeID

    ' Create the Micropipe and specify the component to execute
    Set oMicroPipe = Server.CreateObject("Commerce.MicroPipe")
    Set oFormatTemplate = Server.CreateObject("Commerce.CSFFormatTemplate")
    Call oMicroPipe.SetComponent(oFormatTemplate)

    ' execute the FormatTemplate component
    nErrLvl = oMicroPipe.Execute(dDictOrder, dDictContext, 0)

    ' get formatted HTML in SimpleList
    Set listFormatted = dDictOrder("_formatted")
    If listFormatted.Count > 0 Then
        GeneratePreview = listFormatted(0)
    End If
End Function
%>
<HTML>
<HEAD>
	<LINK REL='stylesheet' TYPE='text/css' HREF='include/bizdesk.css' ID='mainstylesheet'>
	<STYLE>
		BODY
		{
			MARGIN: 0;
			PADDING: 0;
			BACKGROUND-COLOR: white
		}
	</STYLE>
<SCRIPT LANGUAGE=VBScript>
<!--
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	context menu handler to turn off context menus
sub document_onContextMenu()
	window.event.returnValue = <%= CInt(ALLOW_CONTEXT_MENUS) %>
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	key down handler to turn off keys
sub document_onKeyDown()
	dim evt, elSource
	set evt = window.event
	set elSource = evt.srcElement
	if evt.ctrlKey then
		select case evt.keyCode
			case	KEYCODE_LEFT, KEYCODE_UP, KEYCODE_RIGHT, KEYCODE_DOWN, _
					L_KeyCodeShiftedCopy_Number, L_KeyCodeCopy_Number, _
					L_KeyCodeShiftedUndo_Number, L_KeyCodeUndo_Number, _
					L_KeyCodeShiftedPaste_Number, L_KeyCodePaste_Number, _
					L_KeyCodeShiftedCut_Number, L_KeyCodeCut_Number
				' -- allow these keys
			case else
				' -- disallow all other control key combos
				evt.returnValue = false
		end select
	elseif evt.keyCode = KEYCODE_BACKSPACE and _
		not (elSource.tagName = "INPUT" or elSource.tagName = "TEXTAREA") then
		evt.returnValue = false
	end if
	select case evt.keyCode
		case KEYCODE_F1
			' -- open help
			parent.OpenHelp ""
			' -- disallow all FN keys
			evt.returnValue = false
			evt.cancelBubble = true
		case KEYCODE_F2, KEYCODE_F3, KEYCODE_F4, KEYCODE_F5, KEYCODE_F6, _
			KEYCODE_F7, KEYCODE_F8, KEYCODE_F9, KEYCODE_F10, KEYCODE_F11, KEYCODE_F12
			' -- disallow all FN keys
			evt.returnValue = false
			evt.cancelBubble = true
	end select
end sub
'-->
</SCRIPT>
</HEAD>
<BODY>

<%	If Request("campitem_id") <> "" and Request("campitem_id") <> "-1" Then
%>		<%= sGetContentPreview(Request("campitem_id"), "./redir.asp") %>
<%	end if
%>
</BODY>
</HTML>
