<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='../exprarch/ExprUtil.asp' -->
<!-- #include FILE="include\marketingStrings.asp" -->
<%
'Server Side code for variable setting & functions
Dim g_sStatus, g_sCampaignConnectionStr, g_sExprType, g_sTitle, g_sSelectedStatus, g_bEmptyQuery, _
	g_oExprStore, g_sDeleteTitle, g_DeletedStatus, g_sQuery, g_rsQuery, g_sExpStore_ConnectionStr, _
	g_sExpressionData

g_sExprType = Request("exprtype")
if g_sExprType = "CATALOG" then
	g_sTitle = L_CatExprTitle_Text
	g_sSelectedStatus = L_TgExprExpressionSelected_Text
	g_sDeleteTitle = L_TgExprCatDeleteMsgBox_DialogTitle
	g_DeletedStatus = sFormatString(L_TgExprDeletedCataExpression_Text, array(Request("ExprName")))
else
	g_sExprType = "CAMPAIGNS"
	g_sTitle = L_TgExprTitle_Text
	g_sSelectedStatus = L_TgExprTaExpressionSelected_Text
	g_sDeleteTitle = L_TgExprDeleteMsgBox_DialogTitle
	g_DeletedStatus = sFormatString(L_TgExprDeletedTgExpression_Text, array(Request("ExprName")))
end if

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

If IsEmpty(Session("ExpStore_ConnectionStr")) Then
	Session("ExpStore_ConnectionStr") = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")
End If
g_sExpStore_ConnectionStr = Session("ExpStore_ConnectionStr")

on error resume next
Set g_oExprStore = Server.CreateObject("Commerce.ExpressionStore")
g_oExprStore.Connect(g_sExpStore_ConnectionStr)
if Err.number <> 0 then
	setError "", L_ExprStoreConnect_ErrorMessage, sGetScriptError(Err), ERROR_ICON_ALERT
end if

Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
g_bEmptyQuery = true

'Added for trapping the non-SQL server error
if Err.number <> 0 then
    setError g_sTitle, sGetErrorByID("Cant_connect_db"), sGetScriptError(Err), ERROR_ICON_ALERT
	g_sExpressionData = "<document/>"
else
	'delete operation call delete function
	'Product picker saving operation
	Select case  request("type")
		'Case of deletion
		case "archive"  
			Call TargetExprDelete()
		'Case of productpicker data selected then save it !!
		case "save"
		    ProductPickerSaving(Request.Form("PickedExpression"))
	End Select

	g_sExpressionData = sGetExpressionData()
	g_oExprStore.Disconnect	
end if

'*******************************************************************************
'	Function TargetExprdelete
'	Logic for Delete
'	Step 1. Check to see if any discount/target group item is using this Expression
'	Step 2. If not, then invoke the Expression Store object
'	Step 3. Call the DeleteExpression on Expr. Store object
'*******************************************************************************
Function TargetExprDelete()
	dim sDiscList, fdName

	If g_oExprStore Is Nothing Then Exit Function
	Select Case Request("editor_type")
		Case "CAMPAIGNS" 		
			'Check to see if any discount is using the same expression
			g_sQuery = "SELECT Count(*) as ExprCount FROM order_discount_expression WHERE i_discexpr_expr = " & Request("ExprID")
			set g_rsQuery = Server.CreateObject("ADODB.Recordset")
			g_oCmd.CommandText = g_sQuery	
			g_rsQuery.Open  g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC

			if g_oConn.Errors.Count > 0 then
				setError g_sTitle, L_TgExprNoDelete_Text, sGetADOError(g_oConn.Errors(0)), ERROR_ICON_CRITICAL		
				g_sStatus = sFormatString(L_TgGroupUnableToDelete_Text, array(Request("ExprName")))
				g_rsQuery.close
				Exit function
			elseif not g_rsQuery.EOF then
				if g_rsQuery("ExprCount") > 0 then
					g_sQuery = "SELECT ci.u_campitem_name FROM order_discount_expression ode, order_discount od, campaign_item ci WHERE ode.i_disc_id = od.i_disc_id AND ci.i_campitem_id = od.i_campitem_id AND i_discexpr_expr = " & Request("ExprID")
					set g_rsQuery = Server.CreateObject("ADODB.Recordset")
					g_oCmd.CommandText = g_sQuery	
					g_rsQuery.Open  g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
					sDiscList = ""
					set fdName = g_rsQuery("u_campitem_name")
					do while not g_rsQuery.EOF
						sDiscList = sDiscList & vbCR & "<li>" & fdName.value
						g_rsQuery.moveNext
					loop
					setError g_sTitle, L_TgExprNoDelete_Text, sFormatString(L_TgExprDiscountIntegrityError_ErrorMessage, Array(sDiscList)), ERROR_ICON_CRITICAL		
					g_sStatus = sFormatString(L_TgGroupUnableToDelete_Text, array(Request("ExprName")))
					g_rsQuery.close
					Exit function
				End if
			End if
			g_rsQuery.Close
 
			'chect to see if any target group is using the same target expression
			g_sQuery = "SELECT Count(*) as ExprCount FROM target WHERE i_target_expr_id = " & Request("ExprID")
			set g_rsQuery = Server.CreateObject("ADODB.Recordset")
			g_oCmd.CommandText = g_sQuery	
			g_rsQuery.Open  g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
			if g_oConn.Errors.Count > 0 then
				setError g_sTitle, L_TgExprNoDelete_Text, sGetADOError(g_oConn.Errors(0)), ERROR_ICON_CRITICAL		
				g_sStatus = sFormatString(L_TgGroupUnableToDelete_Text, array(Request("ExprName")))
				g_rsQuery.close
				Exit function
			elseif not g_rsQuery.EOF then
				if g_rsQuery("ExprCount") > 0 then
					g_sQuery = "SELECT tg.u_targroup_name FROM target t, target_group tg WHERE tg.i_targroup_id = t.i_targroup_id AND t.i_target_expr_id = " & Request("ExprID")
					set g_rsQuery = Server.CreateObject("ADODB.Recordset")
					g_oCmd.CommandText = g_sQuery	
					g_rsQuery.Open  g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
					sDiscList = ""
					set fdName = g_rsQuery("u_targroup_name")
					do while not g_rsQuery.EOF
						sDiscList = sDiscList & vbCR & "<li>" & fdName.value
						g_rsQuery.moveNext
					loop
					'sCreateNewCustomCatalog = Err.Description
					setError g_sTitle, L_TgExprNoDelete_Text, sFormatString(L_TgExprTgGroupIntegrityError_ErrorMessage, Array(sDiscList)), ERROR_ICON_CRITICAL		
					g_sStatus = sFormatString(L_TgGroupUnableToDelete_Text, array(Request("ExprName")))
					g_rsQuery.close
					Exit function
				End if
			End if
			g_sStatus = g_DeletedStatus
			g_rsQuery.Close

		Case "CATALOG"
			'chect to see if any discount are using the same catalog/product expression
			g_sQuery = "SELECT Count(*) as ExprCount FROM order_discount WHERE i_disc_cond_expr = " & Request("ExprID") & " OR i_disc_award_expr = " & Request("ExprID") 
			set g_rsQuery = Server.CreateObject("ADODB.Recordset")
			g_oCmd.CommandText = g_sQuery	
			g_rsQuery.Open  g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC

			if g_oConn.Errors.Count > 0 then
				setError g_sTitle, L_TgExprNoDelete_Text, sGetADOError(g_oConn.Errors(0)), ERROR_ICON_CRITICAL		
				g_sStatus = sFormatString(L_TgGroupUnableToDelete_Text, array(Request("ExprName")))
				g_rsQuery.close
				Exit function
			elseif not g_rsQuery.EOF then
				if g_rsQuery("ExprCount") > 0 then
					g_sQuery = "SELECT ci.u_campitem_name FROM order_discount od, campaign_item ci WHERE od.i_campitem_id = ci.i_campitem_id AND i_disc_cond_expr = " & Request("ExprID") & " OR i_disc_award_expr = " & Request("ExprID") 
					set g_rsQuery = Server.CreateObject("ADODB.Recordset")
					g_oCmd.CommandText = g_sQuery	
					g_rsQuery.Open  g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
					sDiscList = ""
					set fdName = g_rsQuery("u_campitem_name")
					do while not g_rsQuery.EOF
						sDiscList = sDiscList & vbCR & "<li>" & fdName.value
						g_rsQuery.moveNext
					loop
					setError g_sTitle, L_TgExprNoDelete_Text, sFormatString(L_TgExprDiscountIntegrityError_ErrorMessage, Array(sDiscList)), ERROR_ICON_CRITICAL		
					g_sStatus = sFormatString(L_TgGroupUnableToDelete_Text, array(Request("ExprName")))	
					g_rsQuery.close
					Exit function
				End if
			End if		
			g_sStatus = g_DeletedStatus
			g_rsQuery.close
	End Select

	On Error Resume Next	
	'delete the expression from Expr. Store	
	g_oExprStore.DeleteExpression Clng(Request("ExprID")), Clng(0)
	IF Err.number <> 0 THEN
	  setError g_sTitle, L_TgExprNoDelete_Text, sGetScriptError(Err), ERROR_ICON_CRITICAL	
	  g_sStatus = sFormatString(L_TgGroupUnableToDelete_Text, array(Request("ExprName")))
	  Err.clear	
	END IF			
End Function

Function bMatchSameExpression(sExpr)
	dim  rsExpression, sType, aTemp, aExprBody, sProperty, sValue			

	Set rsExpression = g_oExprStore.Query(,"CATALOG")
	aTemp = Split(sExpr, "|||")
	sType = aTemp(1)
	bMatchSameExpression = false
	aExprBody = Split (aTemp(0), "=")
	sProperty = RTrim(aExprBody(0))
	sValue = LTrim(aExprBody(1))
	while NOT rsExpression.Eof
		if rsExpression("exprname") = sValue Then
			bMatchSameExpression = True
		End If
		rsExpression.MoveNext
	Wend	
	rsExpression.close	
End Function

Function ProductPickerSaving(sExpr)
	Dim sProperty, sValue, sType
	Dim aExprBody, aTemp
	Dim rsNewExpr, rsProfDeps

	on error resume next
	'Get string with concatenated new expression names
	'The Name is the Expression
	If sExpr = "" or g_oExprStore Is Nothing Then Exit Function
	If bMatchSameExpression(sExpr) then
		setError g_sTitle, L_TgExprCntRetrieve_ErrorMessage, L_TgExpreCntRetrieveDetails_ErrorMessage, ERROR_ICON_ALERT
		g_sStatus = L_TgExpreCntRetrievePicker_ErrorMessage
		exit Function
	End If

	'Build the Corresponding XML
	aTemp = Split(sExpr, "|||")
	sType = aTemp(1)

	aExprBody = Split (aTemp(0), "=")
	sProperty = RTrim(aExprBody(0))
	sValue = LTrim(aExprBody(1))
	' Remove the Catalog Name (!?)
	aTemp = Split(sProperty, ".")
	sProperty = aTemp(1)

    'Name.String = John T shirts|||cloth
	' Save the Expression into Expression Store
	Set rsNewExpr = g_oExprStore.NewExpression
	If (Err.Number <> 0) Then Exit Function
	' ExprName should be less than 128 chars
	rsNewExpr.Fields("ExprName") = Left(sValue,30)
	' Generate the Expression Body XML
	if sProperty = "CategoryName" then
		rsNewExpr.Fields("ExprBody") = _
			"<CLAUSE OPER='contains'>" & _
				"<Property ID='product._product_categories' TYPE='" & UCase(sType) & "' MULTIVAL='True'/>" & _
				"<IMMED-VAL TYPE='" & LCase(sType) & "'>" & sValue & "</IMMED-VAL>" & _
			"</CLAUSE>"
	else	'product name:
		rsNewExpr.Fields("ExprBody") = _
			"<CLAUSE OPER='equal'>" & _
				"<Property ID='product._product_" & sProperty & "' TYPE='" & UCase(sType) & "'/>" & _
				"<IMMED-VAL TYPE='" & LCase(sType) & "'>" & sValue & "</IMMED-VAL>" & _
			"</CLAUSE>"
	end if
	rsNewExpr.Fields("Category") = "CATALOG"
	rsNewExpr.Fields("ExprDesc") = NULL
	' Set the Profile Dependencies for the New Expression
	Set rsProfDeps = rsNewExpr.Fields("rsProfDeps").Value
	rsProfDeps.AddNew
	rsProfDeps.Fields("ProfDep") = "product"
	' S A V E It
	g_oExprStore.SaveExpression(rsNewExpr)
	IF Err.number <> 0 THEN
		if isEmpty(Session("MSCSBDError")) then setError "", Err.Description, "", ERROR_ICON_ALERT
		err.clear
	end if

	rsNewExpr.Close
	Set rsNewExpr = Nothing
End Function

function sGetExpressionData()
	DIM rsExprInfo, strName, strProfiles

	If g_oExprStore Is Nothing or Err.number <> 0 Then Exit Function
	if g_sExprType = "CATALOG" then
		SET rsExprInfo = g_oExprStore.Query(strName, "CATALOG", strprofiles)
	else
		SET rsExprInfo = g_oExprStore.Query(strName, "CAMPAIGNS", strprofiles)
	end if
	IF Err.number <> 0 THEN
		if isEmpty(Session("MSCSBDError")) then setError "", sGetErrorByID("Cant_connect_db"), sGetScriptError(Err), ERROR_ICON_ALERT
		sGetExpressionData = "<?xml version=""1.0"" ?><document/>"
	else
		'check here to see if there are any expressions in expr. store.
		if not rsExprInfo.EOF then
			g_bEmptyQuery = false
			rsExprInfo.SORT = "ExprName ASC"
		End if		
 
		'Call xmlGetXMLFromRS in ../include/BizDeskUtil.asp file and pass in the opened recordset
		'this recordset is the one returned by the Expr store object.
		sGetExpressionData = "<?xml version=""1.0"" ?>" & xmlGetXMLFromRS(rsExprInfo).xml

		rsExprInfo.Close
		set rsExprInfo = nothing				
	END IF
end function
%>
<html>
<head>
<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">

<script language="vbscript">
<!--
	option explicit

	Sub SelectTargetExpr		
		dim xmlsource, sStatus, sSourceName

		set xmlsource = window.event.XMLrecord	

		if xmlsource.selectNodes("./ExprID").length = 1 then
			'item_selected value is to detect for calceling operation-button enabling
			selectform.ExprID.value = CInt(xmlsource.selectSingleNode("./ExprID").text)
			selectform.ExprName.value = Trim(xmlsource.selectSingleNode("./ExprName").text)
			selectform.editor_type.value = "<%= g_sExprType %>"
			'item_selected value is to detect for calceling operation-button enabling
			selectform.item_selected.value = "YES"
			sSourceName = selectform.ExprName.value

			'display the status text
			sStatus =  sFormatString("<%= g_sSelectedStatus %>", Array(sSourceName))
			SetStatusText(sStatus)
		end if

		'Enable buttons
		EnableTask "new"
		EnableTask "view"
		EnableTask "open" 
		EnableTask "delete"

		set xmlsource = nothing		
	End sub

	Sub UnselectTargetExpr
		'Disable buttons
		EnableTask "new"
		EnableTask "view"
		DisableTask "open"
		DisableTask "delete"
		selectform.ExprID.value = -1		
		selectform.ExprName.value = ""
		selectform.item_selected.value = ""
		SetStatusText "<%= L_TgExprNoItemSelectedM_Text %>"
	End Sub

	Sub ShowProductPickerDlg
		dim urlstr 
		dim retvalue

		urlstr = "../Catalogs/Editor/dlg_ProductPicker.asp?mode=CAMPAIGNS"
		retvalue = window.showModalDialog(urlstr, ,"dialogHeight:545px;dialogWidth:700px;status:no;help:no")		

		If retvalue = -1 or retvalue = "" then
			EnableTask "view"
			EnableTask "new"
			If selectform.item_selected.value = "YES" then
				EnableTask "delete"
				EnableTask "open"
			End If
			Exit Sub
		Else
			prodpickform.PickedExpression.value = retvalue
			prodpickform.submit()	
		End If
	End Sub

	Sub ShowExpressionDlg()
		dim urlstr, paramstr
		dim retvalue 

		paramstr = "catalog_expr_id=&exprtype=<%= g_sExprType %>"
		urlstr = "dlg_targetexpr.asp?"

		retvalue = window.showModalDialog(urlstr & paramstr, ,"status:no;help:no")		
		If retvalue = -1 or retvalue = "" then
		    EnableTask "new"			
			EnableTask "view"
			If selectform.item_selected.value = "YES" then
				EnableTask "delete"
				EnableTask "open"
			End If
			Exit Sub
		Else 
			exprform.submit()	
		End If
	end Sub

	Sub ShowOpenDialog
		dim urlstr, paramstr
		dim retvalue

		'launch corresponding Editor
		openform.ExprID.value = selectform.ExprID.value	
		openform.ExprName.value = selectform.ExprName.value	 
		openform.editor_type.value = selectform.editor_type.value 
	
		'if edit add postform.i_disc_cond_expr.value or new add nothing
		paramstr = "catalog_expr_id=" & selectform.ExprID.value & "&exprtype=<%= g_sExprType %>"
		urlstr = "dlg_targetexpr.asp?"

		retvalue = window.showModalDialog(urlstr & paramstr, ,"status:no;help:no")		

		If retvalue = -1 or retvalue = "" then
			EnableTask "view"
		    EnableTask "new"
			If selectform.item_selected.value = "YES" then
				EnableTask "delete"
				EnableTask "open"
			End If
			Exit Sub
		End If
		openform.submit()
	End sub
	
	Sub ShowDeleteDialog
		Dim msgresult, sDeleteText

		sDeleteText = sFormatString("<%= L_TgExprDeleteMsgBox_ErrorMessage %>", Array(selectform.ExprName.value))
		msgresult = Msgbox (sDeleteText, vbYesNo, "<%= g_sDeleteTitle %>")
		if msgresult = VBYES then
			deleteform.ExprID.value = selectform.ExprID.value	
			deleteform.ExprName.value = selectform.ExprName.value
			deleteform.editor_type.value = selectform.editor_type.value 
			deleteform.submit()
		else
			EnableTask "view"
			EnableTask "new"
			If selectform.item_selected.value = "YES" then
				EnableTask "delete"
				EnableTask "open"
			End If
		end if
	End sub

	Sub Initialize
		EnableAllTaskMenuItems "new", true
		EnableAllTaskMenuItems "view", true

		'when no expressions exists, only the Add button should be exposed
		'at all other times, all the buttons should be disabled
		if IDSelector.newtarget.value = 1 then
			selectform.ExprID.value = -1
		end if
		EnableTask "new"
		EnableTask "view"
		DisableTask "open"
		DisableTask "delete"
	End sub
'-->
</script>
</head>
<body SCROLL=no LANGUAGE='VBScript' ONLOAD='Initialize'>
<%
	InsertTaskBar sFormatString(L_CampaignExprTitle_Text, array(g_sTitle)), g_sStatus
%>

	<xml ID="lstTargetExprMETA">
		<listsheet>
		    <global pagecontrols="no" selectionbuttons="no" selection="single" sort='no' />
		    <columns>
				<column id="ExprCatagory" hide="yes"><%= L_TgExprCatagoryName_Text %></column>
		        <column id="ExprID" key="yes" hide="yes"><%= L_TgID_Text %></column>
<%			if g_sExprType = "CATALOG" then
%>		        <column id="ExprName" sortdir="asc" width="40"><%= L_TgExprCatalogExpressions_Text %></column>
<%			else
%>		        <column id="ExprName" sortdir="asc" width="40"><%= L_TgExprTargetExpressions_Text %></column>
<%			end if
%>		        <column id="ExprDesc"  width="60"><%= L_TgExprDescription_Text %></column>
		    </columns>
		</listsheet>
	</xml>

	<xml ID="lstTargetExprDATA">
		<%= g_sExpressionData %>
	</xml>
<form name="IDSelector">
<%	If g_bEmptyQuery then
%>		<INPUT type="hidden" name="newtarget" value=1>
<%	Else
%>		<INPUT type="hidden" name="newtarget" value=0>
<%	End if
%></form>
<!-- *****************************Target Expressions - END*********************************************-->
<div ID="bdcontentarea">
<!-- *****************************Target Groups - START*********************************************-->
	<div ID="lstTargetExpr" CLASS="listsheet" STYLE='HEIGHT: 80%;MARGIN: 20px'
			METAXML="lstTargetExprMETA" 
			DATAXML="lstTargetExprDATA" 
			LANGUAGE="VBScript" 
			ONROWSELECT="SelectTargetExpr()" 
			ONROWUNSELECT="UnselectTargetExpr()" ><%= L_LoadingProperty_Text %>
	</div>	
</div>
		
<FORM method="POST" id="selectform">
	<INPUT type="hidden" id="type" name="type" value="select">
	<INPUT type="hidden" id="ExprID" name="ExprID">
	<INPUT type="hidden" id="ExprName" name="ExprName">
	<INPUT type="hidden" id="editor_type" name="editor_type">
	<INPUT type="hidden" id="item_selected" name="item_selected">	
</FORM>


<FORM method="POST" id="prodpickform" OnTask="ShowProductPickerDlg()">
	<INPUT type="hidden" id="type" name="type" value="save">
	<INPUT type="hidden" id="ExprID" name="ExprID">
	<INPUT type="hidden" id="ExprName" name="ExprName">
	<INPUT type="hidden" id="editor_type" name="editor_type">
	<INPUT type="hidden" id="PickedExpression" name="PickedExpression">
</FORM>

<FORM method="POST" id="exprform" OnTask="ShowExpressionDlg()">
	<INPUT type="hidden" id="type" name="type" value="reload">
	<INPUT type="hidden" id="ExprID" name="ExprID">
	<INPUT type="hidden" id="ExprName" name="ExprName">
	<INPUT type="hidden" id="editor_type" name="editor_type">
</FORM>

<FORM method="POST" id="openform" ONTASK="ShowOpenDialog">
	<INPUT type="hidden" id="type" name="type" value="open">
	<INPUT type="hidden" id="ExprID" name="ExprID">
	<INPUT type="hidden" id="ExprName" name="ExprName">
	<INPUT type="hidden" id="editor_type" name="editor_type">
</FORM>

<FORM method="POST" id="deleteform" ONTASK="ShowDeleteDialog">
	<INPUT type="hidden" id="type" name="type" value="archive">
	<INPUT type="hidden" id="ExprID" name="ExprID">
	<INPUT type="hidden" id="ExprName" name="ExprName">
	<INPUT type="hidden" id="editor_type" name="editor_type">
</FORM>

</BODY>
</HTML>
