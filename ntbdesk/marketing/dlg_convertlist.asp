<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->

<%
dim g_sCampaignConnectionStr
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
%>

<HTML>
<HEAD>
<TITLE><%=L_ConvertListTitle_Text%></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<STYLE>
HTML
{
    WIDTH: 400px;
    HEIGHT: 220px;
}
BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
DIV
{
	MARGIN-TOP: 4px;
	MARGIN-BOTTOM: 4px;
}
BUTTON
{
	WIDTH: 6em;
}
</STYLE>
<SCRIPT LANGUAGE='VBScript'>

	Sub onOK ()
		dim aReturnValues(2)

		'get the name and description
		aReturnValues(0) = Trim(document.all.list_name.value)
		aReturnValues(1) = Trim (document.all.list_desc.value)

		window.returnvalue = Join(aReturnValues, "|||")
		window.close()
	end sub
 
	Sub Oncancel()
		window.returnvalue = -1
		window.close()
	end Sub
 
	Sub OnChange()
		on error resume next
		btnOK.disabled = not (list_name.valid and not list_name.required)
	End sub	
 
	sub onHelp ()
	on error resume next
		sHREF = "<%=g_sBizDeskRoot%>docs/default.asp?helptopic=CS_BD_Campaigns_aryz.htm"
		window.open sHREF, "winHelpWindow","status:no;toolbar:yes;resizeable:yes;menubar:no;width:700;height:500"
	end sub

	Sub window_onload()
		list_name.focus()
	end sub
</SCRIPT>
</HEAD>

<BODY SCROLL='NO' LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then btnCancel.click">

<xml id="efMeta">
	<fields>
	  <text id='list_name' maxlen='64' subtype='short' required='yes' onrequire='OnChange' onvalid='OnChange'>
			<name><%=L_NewListName_Text%></name>
			<tooltip><%=L_NewListName_Text%></tooltip>
			<error><%=L_ListnameLong_ErrorMessage%></error>
			<charmask>.*</charmask>
	  </text>
	  <text id='list_desc' maxlen='256' subtype='long'>
			<name><%=L_Descr_Text%></name>
			<tooltip><%=L_Descr_Text%></tooltip>
			<error><%=L_ListdescLong_ErrorMessage%></error>
			<charmask>(\S*\s*\S*)*</charmask>
	  </text>
	</fields>
</xml>

<xml id="efData">
	<document><record/></document>
</xml>

<%= L_ListName_Text %><BR>
<DIV ID='list_name' CLASS='editField' style="width:100%" METAXML='efMeta' DATAXML='efdata'></DIV>
<BR><%= L_ListDesc_Text %><BR>
<DIV ID='list_desc' CLASS='editField' METAXML='efMeta' DATAXML='efdata'></DIV>
<BR><BR>
<TABLE BORDER='0' FRAME='0' CELLPADDING='0' CELLSPACING='0' width="100%">
	<TR>
		<TD width="100%">&nbsp;</TD>
		<TD><nobr>
			<BUTTON ID='btnOK' CLASS="bdbutton" DISABLED ONCLICK='onOK()'><%= L_OK_Text %></BUTTON>
			<BUTTON ID='btnCancel' CLASS="bdbutton" ONCLICK='onCancel()'><%= L_Cancel_Text %></BUTTON>
			<BUTTON ID='bdhelp' CLASS='bdbutton'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "cs_bd_campaigns_HSYK.htm"'><%= L_Help_Text %></BUTTON>
		</nobr></TD>
	</TR>
</TABLE>

</BODY>
</HTML>