<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<%
dim aPostVal
dim oLM, sDWConnectStr, sOLAPConnectStr
dim sFriendlyError, sScriptError
Dim nListType
Dim sListName
Dim nListCreationType
Dim sListSource
Dim sListQuery
Dim bDmExport
Dim bUpmExport
dim sReturnVarID, sReturnVarOpID
dim g_sCampaignConnectionStr
dim dRequestXMLAsDict
dim xmlReturn
dim sListdata
dim retvarID

const CREATIONTYPE_FILE = 1
const CREATIONTYPE_SQL = 2
const CREATIONTYPE_DW = 3

const LISTFLAG_USERLIST = 4
const LISTFLAG_GENERIC = 8
const LISTFLAG_MAILABLE = 16

on error resume next

If instr(1, Request.ServerVariables("HTTP_REFERER"), "analysis_reports.asp", 1) > 0 then
	' Request form passed as XML object from Analysis
	Set dRequestXMLAsDict = dGetRequestXMLAsDict()
	sListdata = dRequestXMLAsDict("listdata")
	aPostVal = Split(slistdata, "|", -1, 1)
else
	aPostVal = Split(Request.Form("listdata"), "|||", -1, 1)
end if

'just for notes sake
'aPostVal(0) = list_type -- static or dynamic
'aPostVal(1) = list_name
'aPostVal(2) = list_creation_type -- from File, SQL or DW Calc		
'based on aPostVal(2)
'aPostVal(3) = list_source_xxx - can be a File name, Connection string, DW Calc name
'aPostVal(4) = list_query	-- when creating from SQL, givesthe query.
'aPostVal(5) = DmExport flag
'aPostVal(6) = UpmExport flag
nListType = CLng(aPostVal(0))
sListName = aPostVal(1)
nListCreationType = CInt(aPostVal(2))
sListSource = aPostVal(3)
sListQuery = replace(replace(replace(replace(aPostVal(4), "&amp;", "&"), "&lt;", "<"), "&gt;", ">"), "&#13;", vbCR)
if aPostVal(5) = "" then aPostVal(5) = 0
bDmExport = cbool(aPostVal(5))
If UBOUND(aPostVal) > 5 Then
	if aPostVal(6) = "" then aPostVal(6) = 0
    bUpmExport = cbool(aPostVal(6))
Else
    bUpmExport = False
End If

if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

'create the ListManager object
set oLM = Server.CreateObject("Commerce.ListManager")

'Initialie List Manager.Object
oLM.Initialize g_sCampaignConnectionStr
if err.Number <> 0 then
	Set oLM = Nothing
	sFriendlyError = sGetErrorByID("Cant_connect_db")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
	If nListCreationType = CREATIONTYPE_DW Then
		Response.write "sFriendlyError=" & sFriendlyError & "|sScriptError=" & sScriptError  & "|"
	Else
		Response.redirect "listmanager.asp?type=import&status=0"
	End If
end if

'based on inp
select case nListCreationType
	case CREATIONTYPE_FILE		'Create from File			
		sReturnVarID = oLM.CreateFromFile(sListName, "", nListType, 0, sListSource, CLng(100) ,TRUE, sReturnVarOpID )
		if err.Number <> 0 then
			Set oLM = Nothing
			sFriendlyError = sGetErrorByID("Import_failed")
			sScriptError = sGetScriptError(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
			Response.redirect "listmanager.asp?type=import&status=0"
		end if			
	case CREATIONTYPE_SQL		'Create From SQL
		sReturnVarID = oLM.CreateFromSQL(sListName, "", nListType, 0, sListSource, sListQuery, CLng(100), TRUE, sReturnVarOpID )	
		if err.Number <> 0 then
			Set oLM = Nothing
			sFriendlyError = sGetErrorByID("Import_failed")
			sScriptError = sGetScriptError(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
			Response.redirect "listmanager.asp?type=import&status=0"
		end if		
	case CREATIONTYPE_DW		'Create from DW
		sDWConnectStr = Session("MSCSDataWarehouseSQLConnStr")
		sOLAPConnectStr = Session("MSCSDataWarehouseOLAPConnStr")  

		' All lists exported from Analysis are Generic lists
        Dim nFlags
        nFlags = LISTFLAG_GENERIC + nListType  ' REM LISTFLAG_GENERIC + static or dynamic
        If bUpmExport Then nFlags = nFlags + LISTFLAG_USERLIST  'REM set USERLIST flag if list contains rcp_guid field
        If bDMExport Then nFlags = nFlags + LISTFLAG_MAILABLE
        ' Run the import
		retvarID = oLM.CreateFromDWCalc(sListName, "", nFlags, 0, sDWConnectStr, sOLAPConnectStr, sListSource, TRUE, sReturnVarOpID )

		' Return XML object
		set xmlReturn = xmlGetXMLDOMDoc()

		if err.Number <> 0 then
			Set oLM = Nothing
			sFriendlyError = sGetErrorByID("Export_failed")
			sScriptError = sGetScriptError(Err)

			' Add error node.
			If instr(1, Request.ServerVariables("HTTP_REFERER"), "analysis_reports.asp", 1) > 0 then
				AddErrorNode xmlReturn, "1", sFriendlyError, sScriptError
			else
				AddErrorNode xmlReturn, "1", sScriptError, ""
			end if
				
		end if
		Response.Write xmlReturn.xml
End select

Set oLM = nothing
If nListCreationType <> CREATIONTYPE_DW Then
	Response.redirect "listmanager.asp?type=import&status=1"
End If
%>
