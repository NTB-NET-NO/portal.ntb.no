<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<%
dim sListID
dim oLM
dim sFriendlyError, sScriptError
dim g_sCampaignConnectionStr
	
on error resume next
	
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
	
sListID = Request.Form("list_id")
	
'create List Manager object 
set oLM = Server.CreateObject("Commerce.ListManager")
	
'Initialie List Manager.Object
oLM.Initialize g_sCampaignConnectionStr
if err.Number <> 0 then
	Set oLM = Nothing
	'ReportError
	sFriendlyError = sGetErrorByID("Cant_connect_db")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
	Response.redirect "listmanager.asp?type=delete&status=0"	
end if	
'use the sListID passed on from selection form
oLM.Delete CStr(sListID)
if err.Number <> 0 then
	Set oLM = Nothing
	'ReportError
	sFriendlyError = sGetErrorByID("Cant_delete_list")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
	Response.redirect "listmanager.asp?type=delete&status=0"
end if
			
set oLM = Nothing
Response.Redirect "listmanager.asp?type=delete&status=1"
%>