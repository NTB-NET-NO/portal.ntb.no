<%
sub InsertDMItem()
	Dim guidJobID	
	on error resume next
	g_oConn.BeginTrans

	g_oCmd.CommandType = AD_CMD_STORED_PROC
	g_oCmd.CommandText = "sp_InsertCampaignItem"

	With g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("creative_id", AD_INTEGER, AD_PARAM_INPUT, , null)
		.Append g_oCmd.CreateParameter("guid_type", AD_GUID, AD_PARAM_INPUT, , GUID_TYPE_DM)
		.Append g_oCmd.CreateParameter("campitem_name", AD_BSTR, AD_PARAM_INPUT, Len(g_sCampItemName), g_sCampItemName)
		.Append g_oCmd.CreateParameter("campitem_active", AD_BOOLEAN, AD_PARAM_INPUT, , 1)
		.Append g_oCmd.CreateParameter("exposure_limit", AD_INTEGER, AD_PARAM_INPUT, , 0)
		.Append g_oCmd.CreateParameter("date_start", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , CDate(g_sStartDate))
		.Append g_oCmd.CreateParameter("date_end", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , CDate(g_sEndDate))
		.Append g_oCmd.CreateParameter("campaign_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(Request.Form("i_camp_id")))
		.Append g_oCmd.CreateParameter("date_modified", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , Now())
		.Append g_oCmd.CreateParameter("modified_by", AD_BSTR, AD_PARAM_INPUT, Len(request.ServerVariables("LOGON_USER")), request.ServerVariables("LOGON_USER"))
		.Append g_oCmd.CreateParameter("campitem_comments", AD_BSTR, AD_PARAM_INPUT, Len(Request.Form("u_dmitem_desc")), Request.Form("u_dmitem_desc"))
		.Append g_oCmd.CreateParameter("o_Campitem_id", AD_INTEGER, AD_PARAM_OUTPUT)
	end with

	g_oConn.Errors.clear
	g_oCmd.Execute 
	If g_oConn.Errors.Count > 0 Then
		RollBackAndReportError g_oConn, L_DMUnableToSave_ErrorMessage
		exit sub
	End If
	g_nCampItemID = g_oCmd.Parameters("o_Campitem_id").Value 
	CleanUpParameters(g_oCmd)

	'Create new entry in dm_item table
	If g_nCampItemID <> "" Then
		'Create command object to run stored procedure
		g_oCmd.CommandType = AD_CMD_STORED_PROC
		g_oCmd.CommandText = "sp_dm_item_new"

		'Assign campaign item id
		With g_oCmd.Parameters
			.Append g_oCmd.CreateParameter("i_campitem_id", AD_INTEGER, AD_PARAM_INPUT, , Clng(g_nCampItemID))
			.Append g_oCmd.CreateParameter("g_dmitem_listid", AD_GUID, AD_PARAM_INPUT, , g_sListID)
			.Append g_oCmd.CreateParameter("g_dmitem_testlistid", AD_GUID, AD_PARAM_INPUT, , g_sTestListID)
			.Append g_oCmd.CreateParameter("u_dmitem_desc", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sDesc)), g_sDesc)
			.Append g_oCmd.CreateParameter("u_dmitem_from", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sFrom)), g_sFrom)
			.Append g_oCmd.CreateParameter("u_dmitem_subj", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sSubj)), g_sSubj)
			.Append g_oCmd.CreateParameter("u_dmitem_replyto", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sReplyTo)), g_sReplyTo)
			.Append g_oCmd.CreateParameter("u_dmitem_source", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sSource)), g_sSource)
			.Append g_oCmd.CreateParameter("u_dmitem_attachment", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sAttachment)), g_sAttachment)
			.Append g_oCmd.CreateParameter("b_dmitem_personalized", AD_BOOLEAN, AD_PARAM_INPUT, , g_bPersonalized)
			.Append g_oCmd.CreateParameter("l_dmitem_defformat", AD_SMALLINT, AD_PARAM_INPUT, , g_sDefFormat)
			.Append g_oCmd.CreateParameter("i_dmitem_userflags", AD_INTEGER, AD_PARAM_INPUT, , cLng(g_sLeaveMsgBlank))
			.Append g_oCmd.CreateParameter("i_dmitem_id", AD_INTEGER, AD_PARAM_OUTPUT)
		end with

		g_oConn.Errors.clear
		g_oCmd.Execute 
		If g_oConn.Errors.Count > 0 Then
			RollBackAndReportError g_oConn, L_DMUnableToSave_ErrorMessage
			exit sub
		End If
		g_nDMItemID = g_oCmd.Parameters("i_dmitem_id").Value
		CleanUpParameters(g_oCmd)
						
		If g_nDMItemID > 0 Then
			guidJobID = CreateSqlServerAgentJob()
		End If

		'finally, update the schedule ID in dm_item
		If g_nDMItemID > 0 and guidJobID <> "" then
			g_oCmd.CommandType = AD_CMD_STORED_PROC
			g_oCmd.CommandText = "sp_cs_mail_schedid"
			
			g_oCmd.Parameters.Append g_oCmd.CreateParameter("i_dmitem_id", AD_INTEGER, AD_PARAM_INPUT, , cLng(g_nDMItemID))
			g_oCmd.Parameters.Append g_oCmd.CreateParameter("u_dmitem_scheduleid", AD_GUID, AD_PARAM_INPUT, , guidJobID)
			
			g_oConn.Errors.Clear
			g_oCmd.Execute
			if g_oConn.Errors.count > 0 then
				RollBackAndReportError g_oConn, L_DMUnableToSave_ErrorMessage
				exit sub
			else
				g_oConn.CommitTrans
			end if
			CleanUpParameters(g_oCmd)
		End If
	End If
	If cbool(Request.Form("test_mail_flag")) Then
		TestDMItem()
	End If
end sub

sub UpdateDMItem()
	
	on error resume next
	g_oConn.BeginTrans

	g_oCmd.CommandType = AD_CMD_STORED_PROC
	g_oCmd.CommandText = "sp_UpdateCampaignItem"

	With g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.append g_oCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT, , cLng(g_nCampItemID))
		.append g_oCmd.CreateParameter("campitem_name", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sCampItemName)), g_sCampItemName)
		.append g_oCmd.CreateParameter("campitem_active", AD_BOOLEAN, AD_PARAM_INPUT, , 1)
		.append g_oCmd.CreateParameter("exposure_limit", AD_INTEGER, AD_PARAM_INPUT, , 0)
		.append g_oCmd.CreateParameter("date_start", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , cDate(g_sStartDate))
		.append g_oCmd.CreateParameter("date_end", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , cDate(g_sEndDate))
		.Append g_oCmd.CreateParameter("date_modified", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , Now())
		.Append g_oCmd.CreateParameter("modified_by", AD_BSTR, AD_PARAM_INPUT, Len(request.ServerVariables("LOGON_USER")), request.ServerVariables("LOGON_USER"))
		.append g_oCmd.CreateParameter("campitem_comments", AD_BSTR, AD_PARAM_INPUT, , Request.Form("u_dmitem_desc"))
	end with

	g_oConn.Errors.clear
	g_oCmd.Execute 
	If g_oConn.Errors.Count > 0 Then
		RollBackAndReportError g_oConn, L_DMUnableToSave_ErrorMessage
		exit sub
	End If
	CleanUpParameters(g_oCmd)

	g_oCmd.CommandType = AD_CMD_STORED_PROC
	g_oCmd.CommandText = "sp_dm_item_edit"
	
	With g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("g_dmitem_listid", AD_GUID, AD_PARAM_INPUT, , g_sListID)
		.Append g_oCmd.CreateParameter("g_dmitem_testlistid", AD_GUID, AD_PARAM_INPUT, , g_sTestListID)
		.Append g_oCmd.CreateParameter("u_dmitem_desc", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sDesc)), g_sDesc)
		.Append g_oCmd.CreateParameter("u_dmitem_from", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sFrom)), g_sFrom)
		.Append g_oCmd.CreateParameter("u_dmitem_subj", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sSubj)), g_sSubj)
		.Append g_oCmd.CreateParameter("u_dmitem_replyto", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sReplyTo)), g_sReplyTo)
		.Append g_oCmd.CreateParameter("u_dmitem_source", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sSource)), g_sSource)
		.Append g_oCmd.CreateParameter("u_dmitem_attachment", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_sAttachment)), g_sAttachment)
		.Append g_oCmd.CreateParameter("b_dmitem_personalized", AD_BOOLEAN, AD_PARAM_INPUT, , g_bPersonalized)
		.Append g_oCmd.CreateParameter("l_dmitem_defformat", AD_SMALLINT, AD_PARAM_INPUT, , g_sDefFormat)
		.Append g_oCmd.CreateParameter("i_dmitem_userflags", AD_INTEGER, AD_PARAM_INPUT, , cLng(g_sLeaveMsgBlank))
		.Append g_oCmd.CreateParameter("i_dmitem_id", AD_INTEGER, AD_PARAM_INPUT, , cLng(g_nDMItemID))
		.Append g_oCmd.CreateParameter("u_dmitem_scheduleid", AD_GUID, AD_PARAM_OUTPUT)
	end with

	g_oConn.Errors.clear
	g_oCmd.Execute 
	If g_oConn.Errors.Count > 0 Then
		RollBackAndReportError g_oConn, L_DMUnableToSave_ErrorMessage
		exit sub
	End If
	g_sScheduleID = g_oCmd.Parameters("u_dmitem_scheduleid")
	CleanUpParameters(g_oCmd)
	
	UpdateSqlServerAgentJobSchedule()
			
	If Request.form("archivedflag") Then
		DisableSQLJob g_nCampItemID , false
	End If
		
	g_oConn.CommitTrans
			
	If cbool(Request.Form("test_mail_flag")) Then
		TestDMItem()
	End If
End sub

Sub RollBackAndReportError(byRef oConn, byVal sErrorText)
	Dim oError
	For Each oError In oConn.Errors
		if oError.number = 0 then
			setError "", oError.description, "", ERROR_ICON_INFORMATION
			'clear warning
			oConn.Errors.clear
			Err.clear
		else
			setError "", sErrorText, sGetADOError(oError), ERROR_ICON_ALERT
			'roll back the transaction if not just warning:
			oConn.RollbackTrans
		end if
	Next
End Sub

Sub CleanUpParameters(byRef oCmd)
	Dim i
	With oCmd.Parameters
		For i = 0 To .Count -1
			.Delete 0
		Next
	End With
End Sub

Sub AssignGlobalVariables()
	g_nDMItemID = Request.Form("i_dmitem_id")
	g_sCampItemName = Request.Form("u_campitem_name")
	
	g_sListID = Request.Form("g_dmitem_listid")
	If g_sListID = "" Then g_sListID = NULL
	If (g_nDMItemID <> "") Then 
		g_sScheduleID = Request.Form("u_dmitem_scheduleid")
	End If

	g_sTestListID = Request.Form("g_dmitem_testlistid")
	If g_sTestListID = "" or g_sTestListID = "0" Then g_sTestListID = NULL
	
	g_sDesc = Request.Form("u_dmitem_desc")
	g_sFrom = Request.Form("u_dmitem_from")
	g_sSubj = Request.Form("u_dmitem_subj")
	g_sReplyTo = Request.Form("u_dmitem_replyto")
	'If the ReplyTo is empty just default to From field.
	If Len(g_sReplyTo) = 0 Then g_sReplyTo = g_sFrom
	
	g_sSource = Request.Form("u_dmitem_source")
	g_sAttachment = Request.Form("u_dmitem_attachment")
	g_sDefFormat = Request.Form("l_dmitem_defformat")
	g_sStartDate = g_MSCSDataFunctions.ConvertDateString(CStr(Request.Form("txtStartDate")), g_DBDefaultLocale)
	g_sEndDate = g_MSCSDataFunctions.ConvertDateString(CStr(Request.Form("txtEndDate")), g_DBDefaultLocale)
	g_sStartTime = Request.Form("txtStartTime")
	g_iMailingInterval = cLng(Request.Form("txtMailingInterval"))
	g_sLeaveMsgBlank = 0 
    g_bPersonalized = CBool(Request.Form("b_dmitem_personalized"))	
End Sub

sub TestDMItem()
	oDMCmd.CommandType = AD_CMD_STORED_PROC
	oDMCmd.CommandText = "sp_create_dml_sqlagent_job"

	With oDMCmd.Parameters			
		.Append oDMCmd.CreateParameter("site_name", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_MSCSSiteName)), g_MSCSSiteName)
		.Append oDMCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT, , Clng(g_nCampItemID))
		.Append oDMCmd.CreateParameter("dmitem_id", AD_INTEGER, AD_PARAM_INPUT, , Clng(g_nDMItemID))
		.Append oDMCmd.CreateParameter("start_date", AD_INTEGER, AD_PARAM_INPUT, , null)
		.Append oDMCmd.CreateParameter("end_date", AD_INTEGER, AD_PARAM_INPUT, , null)
		.Append oDMCmd.CreateParameter("start_time", AD_INTEGER, AD_PARAM_INPUT, , null)
		.Append oDMCmd.CreateParameter("mscs_path", AD_BSTR, AD_PARAM_INPUT, 0, "")			
		.Append oDMCmd.CreateParameter("css_job_type", AD_SMALLINT, AD_PARAM_INPUT, , 2)
	end with
	
	oDMdbConn.Errors.clear
	oDMCmd.Execute 
	If oDMdbConn.Errors.Count > 0 Then
		RollBackAndReportError oDMdbConn, L_DMUnableToSave_ErrorMessage
	End If
	CleanUpParameters(oDMCmd)
end sub  


function CreateSqlServerAgentJob()
	Dim dStartDate, dEndDate, dStartTime, iIndex, iWeekDays
	Dim iJobFreqInterval, iJobFreqRelativeInterval, iJobFreqRecurrenceFactor

	CreateSqlServerAgentJob = ""
	oDMdbConn.BeginTrans

	oDMCmd.CommandType = AD_CMD_STORED_PROC
	oDMCmd.CommandText = "sp_create_dml_sqlagent_job"
						
	'dates must be a long in yyyymmdd format
	'times must be a long in hhmmss formatyhu
	dStartDate = FixDate(g_sStartDate)
	dEndDate = FixDate(g_sEndDate)
	dStartTime = FixTime(g_sStartTime)

	Select Case g_iMailingInterval			
		Case SQLDMOFreq_Daily 
			iJobFreqInterval = Request.Form("txtDays")
			iJobFreqRelativeInterval = Request.Form("txtDays")					
			
		Case SQLDMOFreq_Weekly
			iWeekDays = 0
			for iIndex = 1 to 7
				iWeekDays = iWeekDays + Cint(Request.Form("chkWeekDay" + CStr(iIndex)))
			next															
			iJobFreqInterval = iWeekDays
			iJobFreqRecurrenceFactor = Request.Form("txtWeeks")
			
		Case SQLDMOFreq_Monthly					
			iJobFreqInterval = Request.Form("txtDayOfMonth")
			iJobFreqRecurrenceFactor = Request.Form("txtMonthSpan")					
			
		Case SQLDMOFreq_MonthlyRelative
			iJobFreqInterval = Request.Form("relDay")
			iJobFreqRelativeInterval = Request.Form("relWeekType")			
			iJobFreqRecurrenceFactor = Request.Form("relRecur")    
	End Select	
	
	With oDMCmd.Parameters			
		.Append oDMCmd.CreateParameter("site_name", AD_BSTR, AD_PARAM_INPUT, Len(trim(g_MSCSSiteName)), g_MSCSSiteName)
		.Append oDMCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT, , Clng(g_nCampItemID))
		.Append oDMCmd.CreateParameter("dmitem_id", AD_INTEGER, AD_PARAM_INPUT, , Clng(g_nDMItemID))
		.Append oDMCmd.CreateParameter("start_date", AD_INTEGER, AD_PARAM_INPUT, , Clng(dStartDate))
		.Append oDMCmd.CreateParameter("end_date", AD_INTEGER, AD_PARAM_INPUT, , Clng(dEndDate))
		.Append oDMCmd.CreateParameter("start_time", AD_INTEGER, AD_PARAM_INPUT, , Clng(dStartTime))
		.Append oDMCmd.CreateParameter("mscs_path", AD_BSTR, AD_PARAM_INPUT, 0, "")			
		.Append oDMCmd.CreateParameter("css_job_type", AD_SMALLINT, AD_PARAM_INPUT, , 1)
		.Append oDMCmd.CreateParameter("job_freq_type", AD_INTEGER, AD_PARAM_INPUT, , Clng(g_iMailingInterval))
		.Append oDMCmd.CreateParameter("job_freq_interval", AD_INTEGER, AD_PARAM_INPUT, , Clng(iJobFreqInterval))
		.Append oDMCmd.CreateParameter("job_freq_relative_interval", AD_INTEGER, AD_PARAM_INPUT, , Clng(iJobFreqRelativeInterval))
		.Append oDMCmd.CreateParameter("job_freq_recurrence_factor", AD_INTEGER, AD_PARAM_INPUT, , Clng(iJobFreqRecurrenceFactor))
		.Append oDMCmd.CreateParameter("job_id_param", AD_GUID, AD_PARAM_OUTPUT)
	end with
	
	oDMdbConn.Errors.clear
	oDMCmd.Execute 
	If oDMdbConn.Errors.Count > 0 Then
		RollBackAndReportError oDMdbConn, L_DMUnableToSave_ErrorMessage
		CleanUpParameters(oDMCmd)
		exit function
	End If
	CreateSqlServerAgentJob = oDMCmd.Parameters("job_id_param").Value
	CleanUpParameters(oDMCmd)
	oDMdbConn.CommitTrans
end function

sub UpdateSqlServerAgentJobSchedule()
	on error resume next

	Dim dStartDate, dEndDate, dStartTime, iIndex, iWeekDays
	Dim iJobFreqInterval, iJobFreqRelativeInterval, iJobFreqRecurrenceFactor
	Dim sJobScheduleName, guidJobID
	
	'generate job schedule name
	sJobScheduleName = sGetScheduleName(sGetJobName(g_nCampItemID, g_MSCSSiteName))

	'try to update job	
	oDMCmd.CommandType = AD_CMD_STORED_PROC
	oDMCmd.CommandText = "msdb.dbo.sp_update_jobschedule"

	'dates must be a long in yyyymmdd format
	'times must be a long in hhmmss format
	dStartDate = FixDate(g_sStartDate)
	dEndDate = FixDate(g_sEndDate)
	dStartTime = FixTime(g_sStartTime)

	Select Case g_iMailingInterval
	Case SQLDMOFreq_Daily 
		iJobFreqInterval = Request.Form("txtDays")
		iJobFreqRelativeInterval = Request.Form("txtDays")

	Case SQLDMOFreq_Weekly
		iWeekDays = 0
		for iIndex = 1 to 7
			iWeekDays = iWeekDays + Cint(Request.Form("chkWeekDay" + CStr(iIndex)))
		next
		iJobFreqInterval = iWeekDays
		iJobFreqRecurrenceFactor = Request.Form("txtWeeks")

	Case SQLDMOFreq_Monthly
		iJobFreqInterval = Request.Form("txtDayOfMonth")
		iJobFreqRecurrenceFactor = Request.Form("txtMonthSpan")

	Case SQLDMOFreq_MonthlyRelative
		iJobFreqInterval = Request.Form("relDay")
		iJobFreqRelativeInterval = Request.Form("relWeekType")			
		iJobFreqRecurrenceFactor = Request.Form("relRecur")    
	End Select	

	With oDMCmd.Parameters
		.Append oDMCmd.CreateParameter("job_id", AD_GUID, AD_PARAM_INPUT, , g_sScheduleID)
		.Append oDMCmd.CreateParameter("job_name", AD_BSTR, AD_PARAM_INPUT, 0, null)
		.Append oDMCmd.CreateParameter("name", AD_BSTR, AD_PARAM_INPUT, Len(trim(sJobScheduleName)), sJobScheduleName)
		.Append oDMCmd.CreateParameter("new_name", AD_BSTR, AD_PARAM_INPUT, 0, null)
		.Append oDMCmd.CreateParameter("enabled", AD_SMALLINT, AD_PARAM_INPUT, , 1)
		.Append oDMCmd.CreateParameter("freq_type", AD_INTEGER, AD_PARAM_INPUT, , Clng(g_iMailingInterval))
		.Append oDMCmd.CreateParameter("freq_interval", AD_INTEGER, AD_PARAM_INPUT, , Clng(iJobFreqInterval))
		.Append oDMCmd.CreateParameter("freq_subday_type", AD_INTEGER, AD_PARAM_INPUT, , null)
		.Append oDMCmd.CreateParameter("freq_subday_interval", AD_INTEGER, AD_PARAM_INPUT, , null)
		.Append oDMCmd.CreateParameter("freq_relative_interval", AD_INTEGER, AD_PARAM_INPUT, , Clng(iJobFreqRelativeInterval))
		.Append oDMCmd.CreateParameter("freq_recurrence_factor", AD_INTEGER, AD_PARAM_INPUT, , Clng(iJobFreqRecurrenceFactor))				
		.Append oDMCmd.CreateParameter("active_start_date", AD_INTEGER, AD_PARAM_INPUT, , Clng(dStartDate))
		.Append oDMCmd.CreateParameter("active_end_date", AD_INTEGER, AD_PARAM_INPUT, , Clng(dEndDate))
		.Append oDMCmd.CreateParameter("active_start_time", AD_INTEGER, AD_PARAM_INPUT, , Clng(dStartTime))
		.Append oDMCmd.CreateParameter("active_end_time", AD_INTEGER, AD_PARAM_INPUT, , null)		
	end with

	oDMdbConn.Errors.clear
	oDMCmd.Execute 
	If oDMdbConn.Errors.Count > 0 Then
		'if error when updating the job then try to recreate:
		oDMdbConn.Errors.clear
		CleanUpParameters(oDMCmd)
		guidJobID = CreateSqlServerAgentJob()
		'finally, update the schedule ID in dm_item
		If g_nDMItemID > 0 and guidJobID <> "" and g_oConn.Errors.Count = 0 then
			CleanUpParameters(g_oCmd)
			g_oCmd.CommandType = AD_CMD_STORED_PROC
			g_oCmd.CommandText = "sp_cs_mail_schedid"

			g_oCmd.Parameters.Append g_oCmd.CreateParameter("i_dmitem_id", AD_INTEGER, AD_PARAM_INPUT, , cLng(g_nDMItemID))
			g_oCmd.Parameters.Append g_oCmd.CreateParameter("u_dmitem_scheduleid", AD_GUID, AD_PARAM_INPUT, , guidJobID)

			g_oConn.Errors.Clear
			g_oCmd.Execute
			if g_oConn.Errors.Count > 0 then
				RollBackAndReportError g_oConn, L_DMUnableToSave_ErrorMessage
			end if
		else
			RollBackAndReportError oDMdbConn, L_DMUnableToSave_ErrorMessage
		End If
	End If	
	CleanUpParameters(oDMCmd)
end sub

sub DisableSQLJob(sCampItemID,bEnableJob)
	Dim sJobName
	sJobName = sGetJobName(sCampItemID, g_MSCSSiteName)
	
	oDMCmd.CommandType = AD_CMD_STORED_PROC
	oDMCmd.CommandText = "msdb.dbo.sp_update_job"
	 
	With oDMCmd.Parameters			
		.Append oDMCmd.CreateParameter("job_id", AD_GUID, AD_PARAM_INPUT, , null)
		.Append oDMCmd.CreateParameter("job_name", AD_BSTR, AD_PARAM_INPUT, Len(trim(sJobName)), sJobName)
		.Append oDMCmd.CreateParameter("new_name", AD_BSTR, AD_PARAM_INPUT, 0, null)
		if bEnableJob = true then
			.Append oDMCmd.CreateParameter("enabled", AD_SMALLINT, AD_PARAM_INPUT, ,1)
		else
			.Append oDMCmd.CreateParameter("enabled", AD_SMALLINT, AD_PARAM_INPUT, ,0)
		end if
	end with

	oDMdbConn.Errors.clear
	oDMCmd.Execute 
	If oDMdbConn.Errors.Count > 0 Then
		RollBackAndReportError oDMdbConn, L_DMUnableToSave_ErrorMessage
	End If	
	CleanUpParameters(oDMCmd)
end sub
%>
