<%
sub InsertPreviewDisplayMeta()
%>	<xml id="DisplayMeta">
		<editsheet>
			<global title="no" />
			<template><![CDATA[
				<div STYLE="WIDTH: 100%; HEIGHT: 253px; OVERFLOW-X: auto;  OVERFLOW-Y: hidden">
				<TABLE COLS='4' ROWS='2' BORDER="0" CELLPADDING='0' CELLSPACING='0'>
					<COLGROUP><COL WIDTH=23><COL WIDTH=823></COLGROUP>
					<TR>
						<TD ROWSPAN="2" STYLE="PADDING: 0; HEIGHT: 237;">
							<img SRC="images/vrule.gif" HEIGHT="253" WIDTH="23" BORDER=0 STYLE="MARGIN: 0; PADDING: 0;">
						</TD>
						<TD STYLE="PADDING: 0; HEIGHT: 16px;">
							<img SRC="images/hrule.gif" HEIGHT="16" WIDTH="823" BORDER=0 STYLE="MARGIN: 0; PADDING: 0;">
						</TD>
					</TR>
					<TR>
						<TD VALIGN="top" STYLE="PADDING: 0; HEIGHT: 237;">
						<IFRAME ID='previewframe' TABINDEX=-1 SRC="preview.asp?campitem_id=<%= g_dFormValues("i_campitem_id") %>" FRAMEBORDER=0 APPLICATION=yes STYLE="MARGIN: 0; PADDING: 0; BORDER: none; WIDTH: 100%; HEIGHT: 100%"></IFRAME>
						</TD>
					</TR>
				</table>
				</div>
		   ]]></template>
		</editsheet>
	</xml>		 
<%
end sub

Sub GetCreativeTypeProperties(byVal sGUIDType)
	Dim fdCTID, fdCPName, fdCPType, fdCPLabel, fdCPRequired, fdCPVValue
	Dim nCTIDHolder, sMetaXML, sDataXML, bListen, sDisplayState, dCPValue, sCPLabel, sDefault
	Dim sRequired, sType, sSubType, sCharMask, sMax, sCPVValue, sNodeID, sPromptText
	set dCPValue = Server.CreateObject("Commerce.Dictionary")
	g_sQuery = "SELECT ct.i_creative_type_id, cp.u_cp_name, " & _
		"cp.i_cp_type, cp.u_cp_label, cp.b_cp_required, cpv.text_cpv_value " & _
		"FROM creative_type AS ct INNER JOIN creative_type_xref AS ctx ON " & _
		"ct.i_creative_type_id = ctx.i_creative_type_id INNER JOIN creative_property AS cp ON " & _
		"ct.i_creative_type_id = cp.i_creative_type_id LEFT OUTER JOIN " & _
		"creative_property_value AS cpv ON cp.i_cp_id = cpv.i_cp_id AND " & _
		"cpv.i_creative_id = " & g_dFormValues("i_creative_id") & _
		" WHERE ctx.guid_campitem_type = '" & sGUIDType & "' AND cp.i_cp_type IS NOT NULL " & _
		" ORDER BY ct.i_creative_type_id"
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	With g_rsQuery
		Set fdCTID = .Fields("i_creative_type_id")
		Set fdCPName = .Fields("u_cp_name")
		Set fdCPType = .Fields("i_cp_type")
		Set fdCPLabel = .Fields("u_cp_label")
		Set fdCPRequired = .Fields("b_cp_required")
		Set fdCPVValue = .Fields("text_cpv_value") 
	End With
	'loop to get dict of initial values shared by all creative types
	Do While Not g_rsQuery.EOF 
		if fdCPVValue.value <> "" then dCPValue(LCase(fdCPName.value)) = fdCPVValue.value
		g_rsQuery.MoveNext 
	loop
	if not g_rsQuery.BOF then g_rsQuery.moveFirst()
	'loop again to get the current selection's values
	Do While Not g_rsQuery.EOF 
		if fdCTID.value = cLng(g_dFormValues("i_creative_type_id")) then dCPValue(fdCPName.value) = fdCPVValue.value
		g_rsQuery.MoveNext 
	loop
	if not g_rsQuery.BOF then g_rsQuery.moveFirst()
	Do While Not g_rsQuery.EOF 
		nCTIDHolder = fdCTID.Value
		sMetaXML = "<xml ID='imgtype" & nCTIDHolder & "Meta'><editsheet><global title='no'/><fields>"
		sDataXML = "<xml ID='imgtype" & nCTIDHolder & "Data'><?xml version=""1.0"" ?><document><record>"
		'decide which creative type to display and listen to
		bListen = CBool(nCTIDHolder = g_dFormValues("i_creative_type_id"))
		If bListen Then
			'display as EditSheet
			sDisplayState = " CLASS='editSheet'"
		Else
			'hide and don't load until needed later
			sDisplayState = " STYLE='DISPLAY: none'"
		End If
		Do While fdCTID.Value = nCTIDHolder
			sRequired = ""
			sType = ""
			sSubType = ""
			sCharMask = ""
			sMax = ""
			sCPVValue = fdCPVValue.Value
			If IsNull(sCPVValue) Then sCPVValue = ""
			'sReturn = sReturn & "<TR>"
			If fdCPRequired.value Then
				sRequired = "'yes'"
			Else
				sRequired = "'no'"
			End If
			Select Case fdCPType.value
				Case 1, "text", "txt_opt"
					sType = "text"
					sSubtype = "'short'"
					If fdCPName.Value = "imgURL" Or fdCPName.Value = "clickURL" Then
						sCharMask = "<charmask>(\S*\s*\S*)*</charmask>"
					Else
						sCharMask = ""
					End If
					sMax = " maxlen='3200'"
					If fdCPName.Value = "imgURL" then
						sPromptText = "<prompt>" & L_DCCmpItemImageURL_Text & "</prompt>"
					ElseIf fdCPName.Value = "clickURL" then
						sPromptText = "<prompt>" & L_DCCmpItemClickURL_Text & "</prompt>"
					ElseIf fdCPName.Value = "altText" then
						sPromptText = "<prompt>" & L_DCCmpItemalttext_Text & "</prompt>"
					End If
				Case 2, "textarea", "txta_opt"
					sType = "text"
					sSubtype = "'long'"
					sCharMask = "<charmask>(\S*\s*\S*)*</charmask>"
					sMax = ""
					sPromptText = ""
					sCPVValue = Replace(Replace(Replace(sCPVValue, "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
				Case 3, "number", "num_opt"
					sType = "numeric"
					sSubtype = "'integer'"
					sCharMask = ""
					sMax = " max='99999999'"
					sPromptText = ""
			End select   
			If Trim(fdCPLabel.Value) = "Alt Text" then
				sCPLabel = L_CMAltText_Text
			Else
				sCPLabel = fdCPLabel.Value
			End If
			If fdCPName.Value = "clickURL" then
				sDefault = " default='" & g_dFormValues("u_customer_url") & "' "
			else
				sDefault = ""
			end if
			sNodeID = fdCPName.Value & nCTIDHolder 
			sMetaXML = sMetaXML & "<" & sType & " id='" & sNodeID & "' " & sDefault & " required=" & sRequired & " subtype=" & sSubtype & sMax & " onchange='UpdatePropertyDict(" & nCTIDHolder & ")'>" &_
				"<name>" & sCPLabel & ":</name><tooltip>" & sCPLabel & "</tooltip>" & sCharMask & sPromptText & "</" & sType & ">"
			sDataXML = sDataXML & "<" & sNodeID & "><![CDATA[" & trim(dCPValue(LCase(fdCPName.Value))) & "]]></" & sNodeID & ">"
			g_rsQuery.MoveNext 
			If g_rsQuery.EOF Then Exit Do
		Loop
		sMetaXML = sMetaXML & "</fields></editsheet></xml>"
		sDataXML = sDataXML & "</record></document></xml>"
		g_sCreativeTypesXML = g_sCreativeTypesXML & sMetaXML & vbCR & sDataXML & vbCR & vbCR
		g_sCreativeTypes = g_sCreativeTypes & "<DIV ID='imgtype" & nCTIDHolder & "'" & sDisplayState &_
			" MetaXML='imgtype" & nCTIDHolder & "Meta' DataXML='imgtype" & nCTIDHolder & "Data'>" &_
			L_BDSSLoadingProperties_Text & "</DIV>" & vbCR
		If bListen Then
			g_sCreativeTypes = g_sCreativeTypes & "<DIV ID='listener' ECHO='imgtype" & nCTIDHolder & "' STYLE='BEHAVIOR: url(echo.htc)'></DIV>" & vbCR
		End If
	Loop
	g_rsQuery.Close
End Sub

Function sGetCreativeSizeOptions()
	Dim sReturn, fdCreativeSizeID, fdSizeName, fdSizeWidth, fdSizeHeight
	sReturn = ""
	g_sQuery = "SELECT i_creative_size_id, u_size_name, i_size_width, i_size_height FROM creative_size"						
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	With g_rsQuery
		Set fdCreativeSizeID = .Fields(0)
		Set fdSizeName = .Fields(1)
		Set fdSizeWidth = .Fields(2)
		Set fdSizeHeight = .Fields(3)
		Do While Not .EOF
			sReturn = sReturn & "<option value='" & fdCreativeSizeID.value & "'>" & _
				g_mscsPage.htmlEncode(fdSizeName.value & " (" & fdSizeWidth.value & "x" & fdSizeHeight.value & ")") & "</option>"
			.MoveNext
		Loop
		.close
	End With
	sGetCreativeSizeOptions = sReturn
End Function

Function sGetCreativeTypeOptions(byVal sGUIDType)
	Dim sReturn, fdCreativeTypeID, fdCTName
	sReturn = ""
	'each campaign item type (AD, Discount, DM...) has its own set of creative types
	g_sQuery = "SELECT ct.i_creative_type_id, ct.u_ct_name FROM creative_type AS ct " & _
		"INNER JOIN creative_type_xref as ctx ON ct.i_creative_type_id = ctx.i_creative_type_id " & _
		"WHERE ctx.guid_campitem_type = '" & sGUIDType & "'" 						
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	With g_rsQuery
		Set fdCreativeTypeID = .Fields(0)
		Set fdCTName = .Fields(1)
		Do While Not .EOF
			sReturn = sReturn & "<option value='" & fdCreativeTypeID.value & "'>" & _
				g_mscsPage.htmlEncode(fdCTName.value) & "</option>"
			.MoveNext
		Loop
		.close
	End With
	sGetCreativeTypeOptions = sReturn
End Function

Function sGetPageTargetGroups()
	Dim sRunOfSiteChecked, sRunOfSiteDisabled, sReturn, fdPGChecked, fdPGID, _
		fdPGDesc, fdTGChecked, fdTargGroupID, sColorStyle, _
		fdTargGroupName, sAvailableOptions, sAssignedOptions
	'get page groups
	sAvailableOptions = ""
	sAssignedOptions = ""
	g_sQuery = "SELECT pg.i_pg_id, pg.u_pg_description, pgx.i_pg_id AS pgChecked FROM page_group AS pg " & _
		"LEFT OUTER JOIN page_group_xref AS pgx ON pg.i_pg_id = pgx.i_pg_id "
	If g_dFormValues("type") = "add" Then
		g_sQuery = g_sQuery  & "AND pgx.i_campitem_id = NULL"
	Else
		g_sQuery = g_sQuery  & "AND pgx.i_campitem_id = " & g_dFormValues("i_campitem_id")
	End If
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	Set fdPGID = g_rsQuery("i_pg_id")
	Set fdPGDesc = g_rsQuery("u_pg_description")
	Set fdPGChecked = g_rsQuery("pgChecked")

	'create HTML
	sReturn = "<TABLE border='0' WIDTH='95%' CELLPADDING='0' CELLSPACING='0' STYLE='MARGIN-TOP: 5px; TABLE-LAYOUT: fixed'><TR>" & _
		"<COLGROUP><COL width='5%'><COL width='15%'><COL width='30%'><COL width='20%'><COL width='30%'></COLGROUP>" & _
		"<TD VALIGN='top'>" & g_imgRequired & "</TD><TD VALIGN='top' width='25%' TITLE='" & L_CMPageGroups_Tooltip & "'>" & L_CMPageGroups_Text & "</TD>" & _
		"<TD COLSPAN=3 VALIGN='top'>"
	'special section for the first page group to include a description 'Ad Placement'
	sRunOfSiteChecked = ""
	sRunOfSiteDisabled = ""
	If Not g_rsQuery.EOF Then
		If (g_dFormValues("type") = "add" Or Not IsNull(fdPGChecked.Value)) Then
			sRunOfSiteChecked = " CHECKED "
			sRunOfSiteDisabled = " lbDisabled"
		End If
	End If
	sReturn = sReturn & "<TABLE border=0 CELLPADDING=0 cellspacing=0 width='100%'><TR><TD VALIGN=top width='20'><INPUT TYPE='checkbox' VALUE='" & fdPGID.Value & _
			"' ONCLICK='RunOfSiteOnClick()' ID='RunOfSite' name='RunOfSite' " & _
			sRunOfSiteChecked & " style='BACKGROUND-COLOR: transparent'></TD><TD><LABEL FOR='RunOfSite' TITLE='" & L_CMRunOnAllPageGroups_Text & "'>" & L_CMRunOnAllPageGroups_Text & "</LABEL></TD></TR></TABLE>"
	g_rsQuery.MoveNext
	Do While Not g_rsQuery.EOF
		If IsNull(fdPGChecked.Value) Then 'belongs to available options
			sAvailableOptions = sAvailableOptions & "<DIV VALUE='" & fdPGID.value & "'>" & fdPGDesc.value & "</DIV>"
		Else 'belongs to assigned options
			sAssignedOptions = sAssignedOptions & "<DIV VALUE='" & fdPGID.value & "'>" & fdPGDesc.value & "</DIV>"
		End If
		g_rsQuery.MoveNext
	Loop
	g_rsQuery.Close		
	if sAssignedOptions = "" and sRunOfSiteDisabled = "" then
		sColorStyle = " style='background-color: #ffff99' "
	else
		sColorStyle = ""
	end if
	sReturn = sReturn & "</TD></TR><TR><TD colspan=5>&nbsp;</TD></TR><TR>" & _
			"<TD colspan=2>&nbsp;</TD>" & _
			"<TD VALIGN='top' TITLE='" & L_CMAvailable_Tooltip & "'>" & L_CMAvailable_Text & "<br>" & _
			"<DIV CLASS='listBox" & sRunOfSiteDisabled & "' onselect='listOnSelect()' id='PageGroupList' " & _
			"style='width: 100%' selection='multi'>" & _
			sAvailableOptions & "</DIV></TD><TD ALIGN=center>" & _
			"<button TYPE='button' id='btnAdd1' name='btnAdd1' CLASS='bdbutton' " & _
			"style='width: 7em' onClick='MoveItems()' " & _
			"DISABLED TITLE='" & L_BDSSAddArrow_Button & "'>" & L_BDSSAddArrow_Button & "</button>" & _
			"<br><button TYPE='button' id='btnRemove1' name='btnRemove1' CLASS='bdbutton' " & _
			"style='WIDTH: 7em' onclick='MoveItems()' " & _
			"DISABLED TITLE='" & L_BDSSArrowRemove_Button & "'>" & L_BDSSArrowRemove_Button & "</button>" & _
			"</TD><TD VALIGN='top' TITLE='" & L_CMAssigned_Tooltip & "'>" & L_CMAssigned_Text & "<br>" & _
			"<DIV CLASS='listBox" & sRunOfSiteDisabled & "' onselect='listOnSelect()' onchange='listOnChange()' " & sColorStyle & " id='PageSelectedList' " & _
			"style='WIDTH: 100%' selection='multi'>" & sAssignedOptions & "</DIV></TD></TR>"

	'get target groups
	sAvailableOptions = ""
	sAssignedOptions = ""
	g_sQuery = "SELECT tg.*, tgx.i_targroup_id AS tgChecked FROM target_group AS tg " & _
		"LEFT OUTER JOIN target_group_xref AS tgx ON tg.i_targroup_id = tgx.i_targroup_id "
	If g_dFormValues("type") = "add" Then
		g_sQuery = g_sQuery  & "AND tgx.i_campitem_id = NULL"
	Else
		g_sQuery = g_sQuery  & "AND tgx.i_campitem_id = " & g_dFormValues("i_campitem_id")
	End If
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	'get field objects
	set fdTargGroupID = g_rsQuery("i_targroup_id")
	set fdTargGroupName = g_rsQuery("u_targroup_name")
	set fdTGChecked = g_rsQuery("tgChecked")
	do while Not g_rsQuery.EOF
		If IsNull(fdTGChecked.Value) Then 'belongs to available options
			sAvailableOptions = sAvailableOptions & "<DIV VALUE='" & fdTargGroupID.value & "'>" & fdTargGroupName.value & "</DIV>"
		Else 'belongs to assigned options
			sAssignedOptions = sAssignedOptions & "<DIV VALUE='" & fdTargGroupID.value & "'>" & fdTargGroupName.value & "</DIV>"
		End If
		g_rsQuery.MoveNext
	loop
	g_rsQuery.close		
	sReturn = sReturn & "<TR><TD>&nbsp;</TD><TD VALIGN='top' TITLE='" & L_CMTargetGroups_Tooltip & "'>" & L_CMTargetGroups_Text & "</TD>" & _
			"<TD VALIGN='top' TITLE='" & L_CMAvailable_Text & "'>" & L_CMAvailable_Text & "<br>" & _
			"<DIV CLASS='listBox' onselect='listOnSelect()' id='TargetGroupList' " & _
			"style='WIDTH: 100%' selection='multi'>" & _
			sAvailableOptions & "</DIV></TD><TD ALIGN=center>" & _
			"<button TYPE='button' id='btnAdd2' name='btnadd2' CLASS='bdbutton' " & _
			"style='WIDTH: 7em' onClick='MoveItems()' " & _
			"DISABLED TITLE='" & L_BDSSAddArrow_Button & "'>" & L_BDSSAddArrow_Button & "</button><br>" & _
			"<button TYPE='button' id='btnRemove2' name='btnremove2' CLASS='bdbutton' " & _
			"style='WIDTH: 7em' onclick='MoveItems()' " & _
			"DISABLED TITLE='" & L_BDSSArrowRemove_Button & "'>" & L_BDSSArrowRemove_Button & "</button></TD>" & _
			"<TD VALIGN='top' TITLE='" & L_CMAssigned_Text & "'>" & L_CMAssigned_Text & "<br>" & _
			"<DIV CLASS='listBox' onselect='listOnSelect()' id='TargetSelectedList' " & _
			"style='WIDTH: 100%' selection='multi'>" & sAssignedOptions & "</DIV></TD></TR></table>"
	sGetPageTargetGroups = sReturn
end function

Sub UpdateTotalImpressions(byVal nCampaignID)
	Dim oConn, sQuery, rsQuery, iImpTotal
	Set oConn = oGetADOConnection(g_sCampaignConnectionStr)
	'only undeleted active Paid Ads affect total impressions
	sQuery = "SELECT SUM(a.i_aditem_events_scheduled) " & _
		"FROM ad_item a INNER JOIN campaign_item c ON " & _
		"a.i_campitem_id = c.i_campitem_id " & _
		"WHERE c.i_camp_id = " & nCampaignID & " AND a.i_aditem_type = 1 " & _
		"AND c.dt_campitem_archived IS NULL AND c.b_campitem_active = 1"
	Set rsQuery = oConn.Execute(sQuery, , AD_CMD_TEXT)
	iImpTotal = rsQuery(0).Value
	rsQuery.Close
	If IsNull(iImpTotal) Then iImpTotal = 0
	sQuery = "Update campaign Set i_camp_events_sched = " & iImpTotal & _
		" WHERE i_camp_id = " & nCampaignID
	oConn.Execute sQuery, , AD_CMD_TEXT
End Sub

Sub DistributeTotalImpressions(byVal nCampaignID)
    Dim oConn, oCmd, sQuery, rsQuery, tWeightedTotalRemaining
    Dim iImpTotal, iRows, iServedTotal, i, iImpTotalRemaining
	Dim fdWeight, fdEvents, fdServed, fdDStart, fdDEnd, fdTStart, fdTEnd, fdMask
    Dim aiServed(), atRemaining(), dtToday, dtStart, dtEnd

	on error resume next
	Set oConn = oGetADOConnection(g_sCampaignConnectionStr)
	Set oCmd = oGetADOCommand(oConn, AD_CMD_TEXT)

	'get total impressions from campaign table
	sQuery = "SELECT i_camp_events_sched FROM campaign WHERE i_camp_id = " & nCampaignID
	Set rsQuery = oConn.Execute(sQuery, , AD_CMD_TEXT)
	iImpTotal = rsQuery.Fields(0).Value
	rsQuery.Close

	'select undeleted active Paid Ads
	sQuery = "SELECT ci.i_campitem_id, ci.u_campitem_name, " &_ 
		"ci.dt_campitem_start, ci.dt_campitem_end, ai.i_aditem_id, " &_
	    "ai.i_aditem_time_of_day_start, ai.i_aditem_time_of_day_end, " &_
	    "ai.i_aditem_days_of_week_mask, ai.i_aditem_weight, " &_
	    "ai.i_aditem_events_scheduled, pt.i_perftot_served " &_
		"FROM campaign_item ci INNER JOIN " &_
	    "ad_item ai ON ci.i_campitem_id = ai.i_campitem_id AND " &_
	    "ci.i_camp_id = " & nCampaignID & " AND ai.i_aditem_type = 1 AND " &_
	    "ci.dt_campitem_archived IS NULL AND " &_
	    "ci.b_campitem_active = 1 INNER JOIN " &_
	    "campaign c ON ci.i_camp_id = c.i_camp_id INNER JOIN " &_
	    "event_type et ON " &_
	    "c.i_event_type_id = et.i_event_type_id LEFT OUTER JOIN " &_
	    "performance_total pt ON " &_
	    "ci.i_campitem_id = pt.i_campitem_id AND " &_
	    "et.u_event_name = pt.u_perftot_event_name " &_
		"ORDER BY ci.i_campitem_id"
	oCmd.CommandText = sQuery
	rsQuery.Open oCmd, , AD_OPEN_KEYSET, AD_LOCK_BATCH_OPTIMISTIC, AD_CMD_TEXT	
	'if no ads then exit
	If rsQuery.EOF Then
		rsQuery.Close
		Exit Sub
	End If
	With rsQuery
		Set fdWeight = .Fields("i_aditem_weight")
		Set fdEvents = .Fields("i_aditem_events_scheduled")
		Set fdServed = .Fields("i_perftot_served")
		Set fdDStart = .Fields("dt_campitem_start")
		Set fdDEnd   = .Fields("dt_campitem_end")
		Set fdTStart = .Fields("i_aditem_time_of_day_start")
		Set fdTEnd   = .Fields("i_aditem_time_of_day_end")
		Set fdMask   = .Fields("i_aditem_days_of_week_mask")
	End With
	
	iRows = rsQuery.RecordCount - 1
	iServedTotal = 0
	'strip time for compare
	dtToday = DateSerial(Year(Date()), Month(Date()), Day(Date()))
	ReDim aiServed(iRows), atRemaining(iRows)
	For i = 0 To iRows
		'get amt served (null = 0)
		If IsNull(fdServed.Value) Then
			aiServed(i) = 0
		else
			aiServed(i) = CLng(fdServed.Value)
		end if
		'strip time for compare
		dtStart = DateSerial(Year(fdDStart.Value), Month(fdDStart.Value), Day(fdDStart.Value))
		dtEnd = DateSerial(Year(fdDEnd.Value), Month(fdDEnd.Value), Day(fdDEnd.Value))
		'get remaining time intervals
		If dtToday <= dtStart Then 'this Ad has not started yet
			atRemaining(i) = iGetHoursInDateRange(fdDStart.Value, fdDEnd.Value, fdTStart.Value, fdTEnd.Value, fdMask.Value)
		ElseIf dtToday < dtEnd Then 'this Ad has partial time left
			atRemaining(i) = iGetHoursInDateRange(Now(), fdDEnd.Value, fdTStart.Value, fdTEnd.Value, fdMask.Value)
		Else 'this Ad is finished
			atRemaining(i) = 0
		End If	
		'add to weighted intervals remaining and served total 
		tWeightedTotalRemaining = tWeightedTotalRemaining + (CLng(fdWeight.Value) * atRemaining(i))
        iServedTotal = iServedTotal + aiServed(i)
		rsQuery.MoveNext
	Next
    iImpTotalRemaining = iImpTotal - iServedTotal

	rsQuery.MoveFirst
	'avoid divided by zero
	oConn.BeginTrans
    If tWeightedTotalRemaining = 0 or iImpTotalRemaining = 0 Then 
		'no more time remaining or no more impressions remaining
		For i = 0 To iRows
		    fdEvents.Value = aiServed(i)
			rsQuery.MoveNext
		Next
	else		
		For i = 0 To iRows
		    fdEvents.Value = CLng(aiServed(i) + ((CLng(fdWeight.Value) * atRemaining(i)) / tWeightedTotalRemaining) * iImpTotalRemaining)
			rsQuery.MoveNext
		Next
    End If
	rsQuery.UpdateBatch
	If oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, L_AdUnableToDistributeImpressions_ErrorMessage
		Exit Sub
	else
		oConn.CommitTrans
	End If
	rsQuery.Close
End Sub

Function iGetHoursInDateRange(byVal dtStart, byVal dtEnd, byVal iTODStart, byVal iTODEnd, byVal iDOWMask)
    Dim aMask, iDays, iDOW, iIPD, i, aRunFor(), iTotal, sMSCSWeekStartDay
    iTotal = 0
    
    ' Array of bitmasks for each day of the week (mon-sun)
	Const DAY_MON = &H0040
	Const DAY_TUE = &H0020
	Const DAY_WED = &H0010
	Const DAY_THU = &H0008
	Const DAY_FRI = &H0004
	Const DAY_SAT = &H0002
	Const DAY_SUN = &H0001
    aMask = Array(DAY_MON, DAY_TUE, DAY_WED, DAY_THU, DAY_FRI, DAY_SAT, DAY_SUN)
	sMSCSWeekStartDay = Application("MSCSWeekStartDay")
    
    ' Compute number of days in the date range (count both begin and end date)
    iDays = DateDiff("d", dtStart, dtEnd) + 1
    
    ' Compute the first day of the week
    iDOW = DatePart("w", dtStart, vbMonday) + (sMSCSWeekStartDay - 1) - 1 ' convert for configured start day
    if iDOW > 6 then iDOW = iDOW - 7
    if iDOW < 0 then iDOW = iDOW + 7
    
    ' Compute the remains of today, if today is a run day, and the change happens after service started
    If (iDOWMask And aMask(iDOW)) And Date() >= dtStart Then
        iTotal = iGetHoursLeftInTimeRange(iTODStart, iTODEnd, DatePart("h", Now()))
    End If
    
    ' Compute number of intervals per day
    If (iTODEnd > iTODStart) Then
        iIPD = iTODEnd - iTODStart
    Else
        iIPD = 24 - (iTODStart - iTODEnd)
    End If

    ' Calculate array of intervals per day
    ReDim aRunFor(6)
    For i = 0 To 6
        If iDOWMask And aMask(i) Then
            aRunFor(i) = iIPD
        Else
            aRunFor(i) = 0
        End If
    Next

    ' Sum the number of intervals for each day in the range
    ' Today is calculated, so start from tomorrow
    For i = 1 To iDays
        iDOW = iDOW + 1
        If iDOW > 6 Then iDOW = 0
        iTotal = iTotal + aRunFor(iDOW)
    Next
    iGetHoursInDateRange = iTotal
End Function

Function iGetHoursLeftInTimeRange(byVal iTODStart, byVal iTODEnd, byVal iHour)
    Dim iReturn
    If iTODStart < iTODEnd Then
        If iHour <= iTODStart Then
            iReturn = iTODEnd - iTODStart
        ElseIf iHour < iTODEnd Then
            iReturn = iTODEnd - iHour
        Else
            iReturn = 0
        End If
    ElseIf iTODStart = iTODEnd Then
        iReturn = 24 - iHour
    Else 'iTODStart > iTODEnd
        If iHour <= iTODEnd Then
            iReturn = (iTODEnd - iHour) + (24 - iTODStart)
        ElseIf iHour < iTODStart Then
            iReturn = 24 - iTODStart
        Else
            iReturn = 24 - iHour
        End If
    End If
    iGetHoursLeftInTimeRange = iReturn
End Function

Function bGetCampLevelGoal(nCampID)
	Dim oConn, sQuery, rsQuery
	Set oConn = oGetADOConnection(g_sCampaignConnectionStr)
	sQuery = "SELECT b_camp_level_goal FROM campaign WHERE i_camp_id = " & nCampID
	Set rsQuery = oConn.Execute(sQuery, , AD_CMD_TEXT)
	If Not rsQuery.EOF Then
		bGetCampLevelGoal = rsQuery.Fields(0).Value
	Else
		bGetCampLevelGoal = True
	End If
	rsQuery.Close
End Function
%>
