<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE="../../include/ASPUtil.asp" -->
<%
'On Error Resume Next

' ------ CATALOG DataType Enum
Const INTEGER_TYPE = 0
Const BIGINTEGER_TYPE = 1
Const FLOAT_TYPE = 2
Const DOUBLE_TYPE = 3
Const BOOLEAN_TYPE = 4
Const STRING_TYPE = 5
Const DATETIME_TYPE = 6
Const CURRENCY_TYPE = 7
Const FILEPATH_TYPE = 8
Const ENUMERATION_TYPE = 9

' ------ PUBLIC Declarations
Public g_sCatalogInitString
g_sCatalogInitString = GetSiteConfigField("Product Catalog", "connstr_db_Catalog")

Public g_oCatMgr ' the catalog manager object

InitializeCatalogObject()

%>
<Document>
  <Catalog displayName="ProductCatalogXML" name="ProductCatalogXML">
    <Profile name="product" displayName="Product">
		<%= sGetCatalogProfileXML() %>
    </Profile>
  </Catalog>
</Document>

<%

' ##################################################################
' Sub: InitializeCatalogObject
'	Create and initialize a global Catalog Manager Object
'
Sub InitializeCatalogObject ()

	'On Error Resume Next
	Set g_oCatMgr = Nothing

	If IsObject(Session("CatalogManager")) Then
		Set g_oCatMgr = Session("CatalogManager")
	End If

	If g_oCatMgr Is Nothing Then
		Set g_oCatMgr = Server.CreateObject("Commerce.CatalogManager")
		Call g_oCatMgr.Initialize (g_sCatalogInitString, True)
		Set Session("CatalogManager") = g_oCatMgr
	End If
End Sub

' #####################################################################
' Function: sConvertToQBTypes
'	Converts from product catalog data types to query builder data types
'
' Input:
'	iType
'
' Returns:
'	String query builder type
'
Function sConvertToQBTypes (ByVal iType)

    Select Case iType
		Case STRING_TYPE
			sConvertToQBTypes = "string"
		Case INTEGER_TYPE
			sConvertToQBTypes = "NUMBER"
		Case CURRENCY_TYPE
			sConvertToQBTypes = "currency"
		Case DATETIME_TYPE
			sConvertToQBTypes = "date"
		Case ENUMERATION_TYPE
			sConvertToQBTypes = "siteterm"
		Case Else
			sConvertToQBTypes = "string"
	End Select
End Function

' #####################################################################
' Function: sGetCatalogProfileXML
'
' Input:
'	none
'
' Returns:
'	String xml for properties
'
function sGetCatalogProfileXML()
	Dim rsProperties ' list of all Catalog PROPERTIES
	Dim sTxt, sPropertyName, sPropertyType, fdPropertyName, fdDataType

	Set rsProperties = g_oCatMgr.Properties

	sTxt = "<Property name=""_product_categories"" displayName=""Categories"" " & _
			"propType=""STRING"" isMultiValued=""1"" />"
	set fdPropertyName = rsProperties("PropertyName")
	set fdDataType = rsProperties("DataType")
	While Not rsProperties.EOF
		sPropertyName = Server.HTMLEncode (fdPropertyName.Value)
		sPropertyType = sConvertToQBTypes (fdDataType.Value)
	 
        sTxt = sTxt & "<Property name=""" & "_product_" & sPropertyName & """ displayName=""" & sPropertyName & _
				""" propType=""" & sPropertyType & """"

		If (sPropertyType = "siteterm") Then
			sTxt = sTxt & " referenceString=""" & "MSCSEnumValues." & sPropertyName & """"
		End If
		sTxt = sTxt & " />"

		rsProperties.MoveNext
	Wend
	sGetCatalogProfileXML = sTxt & "<Property name=""_product_cy_list_price"" " & _
			"displayName=""Price"" propType=""currency"" />"

	rsProperties.Close
	Set rsProperties = Nothing
	Set g_oCatMgr = Nothing
end function
%>