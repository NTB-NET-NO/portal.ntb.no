<%
	function sGetListXML()
		dim oLM, oXMLDoc
		Dim rsListPrimary, rsListFinal
		Dim iListType
		dim sFriendlyError, sScriptError

		'Setting up resources for lm_
		Set oLM = Server.CreateObject("Commerce.ListManager")
		if Err.number <> 0 then
			sFriendlyError = sGetErrorByID("Cant_create_object")
			sScriptError = sGetsScriptErroror(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		else
			oLM.Initialize g_sCampaignConnectionStr
			if Err.number <> 0 then
				sFriendlyError = sGetErrorByID("Cant_connect_db")
				sScriptError = sGetsScriptErroror(Err)
				setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
			Else
			'get the recordset from listMaster table
				Set rsListPrimary = oLM.GetLists
				if err.number <> 0 then
					sFriendlyError = sGetErrorByID("Cant_create_object")
					sScriptError = sGetsScriptErroror(Err)
					setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
				end if
			End if
		end if

		if Err.number = 0 and not isEmpty(rsListprimary) then
			if (g_sSortCol <> "") and (g_sSortCol <> "list_flag_user") and (g_sSortCol <> "list_mailable") and (g_sSortCol <> "process_state") then 
				rsListPrimary.Sort = g_sSortCol & " " & g_sSortDir
			end if

			'here create a recordset and also append the fields as designed in the List sheet.
			Set rsListFinal = Server.CreateObject("ADODB.recordset")
			with rsListFinal.Fields
				'set fields attributes & append fields as defined in the listsheet
				.Append "list_id", AD_VARWCHAR, 50 , -1
				'This is operational id for cancel job
				.Append "list_recent_operid", AD_VARWCHAR, 50, -1
				.Append "list_name", AD_VARWCHAR, 64 , -1
				.Append "list_size", AD_VARWCHAR, 50, -1
				.Append "list_created", AD_DB_TIMESTAMP, 256 , -1
				.Append "list_status", AD_VARWCHAR, 25, -1
				.Append "list_flag_user", AD_VARWCHAR, 8 , -1
				.Append "list_generic", AD_VARWCHAR, 8 , -1
				.Append "list_mailable", AD_VARWCHAR, 8 , -1
				.Append "list_description", AD_VARWCHAR, 256 , -1

				.Append "list_unique_guids", AD_VARWCHAR, 50, -1
				.Append "list_unique_emails", AD_VARWCHAR, 50 , -1
				.Append "recent_op_error_code", AD_VARWCHAR, 256 , -1
				.Append "recent_op_error_desc", AD_VARWCHAR, 256 , -1
			end with

			'open recordset after insertions are complete
			rsListFinal.Open
 			g_nNonWorking = 0
 			g_nMailable = 0
			g_nGeneric = 0
			g_nListCount = 0

			If (Not rsListPrimary.EOF)  Then
				'get the starting record
				g_nStart = PAGE_SIZE * (g_nPage - 1)

				'count the non-hidden list elements and get rows for this page
				Do While (Not rsListPrimary.EOF)
					iListType = rsListPrimary("list_flags")

					'don't count hidden lists
					if Not ((iListType And  HIDDENLIST) = HIDDENLIST) then
						'get all rows within current page
						if g_nListCount >= g_nStart and g_nListCount < g_nStart + PAGE_SIZE then
							GetRow rsListFinal, rsListPrimary, iListType
						End If
						g_nListCount = g_nListCount + 1
					End If

					rsListPrimary.MoveNext
				Loop
				if Not(rsListFinal.BOF) then
					rsListFinal.MoveFirst
				end if
			End if
		End if	

		if Err.number = 0 and not isEmpty(rsListprimary) then
			Set oXMLDoc = xmlGetXMLFromRSEx(rsListFinal, 0, PAGE_SIZE, g_nListCount, NULL)
			sGetListXML = oXMLDoc.xml
			set oXMLDoc = nothing
		else
			sGetListXML = "<document/>"
		end if
		Set rsListPrimary = Nothing
		Set rsListFinal = Nothing
		set oLM = nothing
	end function

	sub GetRow(byRef rsListFinal, byRef rsListPrimary, byVal iListType)
		dim nRecentErrCode, sRecentErrDesc

		rsListFinal.AddNew
		rsListFinal("list_id") = Trim(CStr(rsListPrimary("list_id")))
		If IsNull(rsListPrimary.fields("list_recent_operid").value) then
			rsListFinal("list_recent_operid") = ""
		Else
			rsListFinal("list_recent_operid") = rsListPrimary("list_recent_operid")
		End If
		rsListFinal("list_name") = trim(rsListPrimary("list_name"))
		rsListFinal("list_description") = trim(rsListPrimary("list_description"))
		rsListFinal("list_created") = trim(rsListPrimary("list_created"))
		if rsListPrimary("list_unique_guids") = -1 then
			rsListFinal("list_unique_guids") = L_DMNotApplicable_Text
		else
			rsListFinal("list_unique_guids") = rsListPrimary("list_unique_guids")
		end if
		if rsListPrimary("list_unique_emails") = -1 then
			rsListFinal("list_unique_emails") = L_DMNotApplicable_Text
		else
			rsListFinal("list_unique_emails") = rsListPrimary("list_unique_emails")
		end if

		' Get valid Err Code and Description
		nRecentErrCode = rsListPrimary("recent_op_error_code")
		sRecentErrDesc = rsListPrimary("recent_op_error_desc")
		If IsNull(nRecentErrCode) Or IsEmpty(nRecentErrCode) Then
			rsListFinal("recent_op_error_code").value = ""
		Else
			nRecentErrCode = CLng(nRecentErrCode)
			If nRecentErrCode <> 0 Then
				rsListFinal("recent_op_error_code").value = "0x" & Hex(nRecentErrCode)
			Else
				rsListFinal("recent_op_error_code").value = ""
			End If
		End If						
		If IsNull(sRecentErrDesc) Or IsEmpty(sRecentErrDesc) Then
			rsListFinal("recent_op_error_desc").value = ""
		Else
			rsListFinal("recent_op_error_desc").value = sRecentErrDesc
		End If

		if ((iListType And MAILABLELIST) = MAILABLELIST) then
			rsListFinal("list_mailable") = L_YES_TEXT			
			g_nMailable = g_nMailable + 1
		else
			rsListFinal("list_mailable") = L_NO_TEXT
		End If

		if ((iListType And GENERICLIST) = GENERICLIST) then
			rsListFinal("list_size") = Trim(rsListPrimary("list_size"))
			rsListFinal("list_generic") = L_YES_TEXT			
			g_nGeneric = g_nGeneric + 1
		else
			rsListFinal("list_size") = Trim(rsListPrimary("list_size"))
			rsListFinal("list_generic") = L_NO_TEXT
		End if

		if ((iListType And USERLIST) = USERLIST) then
			rsListFinal("list_flag_user") = L_YES_TEXT
		else
			rsListFinal("list_flag_user") = L_NO_TEXT
		end if

		if ((iListType And DYNAMICLIST) = DYNAMICLIST) then
			rsListFinal("list_size") = L_DynamicList_Text
		end if

		'Set the status of the list						
		Select  case  rsListPrimary("list_status")
			Case LM_STATUS_NEW
				rsListFinal("list_status") = L_ListStatusNewList_Text
			Case LM_STATUS_IDLE
				rsListFinal("list_status") = L_ListStatusIdle_Text
			Case LM_STATUS_PENDING
				rsListFinal("list_status") = L_ListStatusPending_Text
				g_nNonWorking = g_nNonWorking + 1
			Case LM_STATUS_FAILED
				rsListFinal("list_status") = L_ListStatusFailed_Text
			Case Else
				rsListFinal("list_status") = L_ListStatusUnknown_Text
		End Select

		rsListFinal.Update
	end sub
%>
