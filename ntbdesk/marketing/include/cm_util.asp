<%
'declare all variables need to run CM
dim g_nPage, g_sCustomerName, g_sFromDate, g_sToDate, g_sSQLFromDate, g_sSQLToDate, _
	g_sStatus, g_sState, g_sItemType, dtDate

sub GetPageGlobals()
	' handle paging
	if IsEmpty(g_dRequest("page")) or IsNull(g_dRequest("page")) then
		g_nPage = 1
	else
		g_nPage = CInt(g_dRequest("page"))
	end if

	'filter criteria
	if IsEmpty(g_dRequest("custname")) or IsNull(g_dRequest("custname")) then
		g_sCustomerName = session("cm_custname")
	else
		g_sCustomerName = g_dRequest("custname")
		'reset page if new search
		if not isEmpty(session("cm_custname")) and g_sCustomerName <> session("cm_custname") then g_nPage = 1
		session("cm_custname") = g_sCustomerName
	end if

	'filter criteria
	if IsEmpty(g_dRequest("fromdate")) or IsNull(g_dRequest("fromdate")) then
		g_sFromDate = session("cm_fromdate")
		if g_sFromDate = "" then
			dtDate = DateAdd("d", -7, Date())
			g_sFromDate = g_MSCSDataFunctions.date(dtDate, g_DBDefaultLocale)
			g_sSQLFromDate = Year(dtDate) & "-" & Month(dtDate) & "-" & Day(dtDate)
		end if
	else
		g_sFromDate = g_dRequest("fromdate")
		dtDate = g_MSCSDataFunctions.ConvertDateString(g_sFromDate, g_DBDefaultLocale)
		g_sSQLFromDate = Year(dtDate) & "-" & Month(dtDate) & "-" & Day(dtDate)
		'reset page if new search
		if not isEmpty(session("cm_fromdate")) and g_sFromDate <> session("cm_fromdate") then g_nPage = 1
		session("cm_fromdate") = g_sFromDate
	end if

	if IsEmpty(g_dRequest("todate")) or IsNull(g_dRequest("todate")) then
		g_sToDate = session("cm_todate")
		if g_sToDate = "" then
			dtDate = DateAdd("d", 14, Date())
			g_sToDate = g_MSCSDataFunctions.date(dtDate, g_DBDefaultLocale)
			g_sSQLToDate = Year(dtDate) & "-" & Month(dtDate) & "-" & Day(dtDate)
		end if
	else
		g_sToDate = g_dRequest("todate")
		dtDate = g_MSCSDataFunctions.ConvertDateString(g_sToDate, g_DBDefaultLocale)
		g_sSQLToDate = Year(dtDate) & "-" & Month(dtDate) & "-" & Day(dtDate)
		'reset page if new search
		if not isEmpty(session("cm_todate")) and g_sToDate <> session("cm_todate") then g_nPage = 1
		session("cm_todate") = g_sToDate
	end if	

	if IsEmpty(g_dRequest("status")) or IsNull(g_dRequest("status")) then
		g_sStatus = session("cm_status")
		if g_sStatus = "" then g_sStatus = "2"
	else
		g_sStatus = g_dRequest("status")
		'reset page if new search
		if not isEmpty(session("cm_status")) and g_sStatus <> session("cm_status") then g_nPage = 1
		session("cm_status") = g_sStatus
	end if

	if IsEmpty(g_dRequest("state")) or IsNull(g_dRequest("state")) then
		g_sState = session("cm_state")
		if g_sState = "" then g_sState = "1"
	else
		g_sState = g_dRequest("state")
		'reset page if new search
		if not isEmpty(session("cm_state")) and g_sState <> session("cm_state") then g_nPage = 1
		session("cm_state") = g_sState
	end if

	if IsEmpty(g_dRequest("itemtype")) or IsNull(g_dRequest("itemtype")) then 
		g_sItemType = session("cm_itemtype")
		if g_sItemType = "" then g_sItemType = "0"
	else
		g_sItemType = g_dRequest("itemtype")
		'reset page if new search
		if not isEmpty(session("cm_itemtype")) and g_sItemType <> session("cm_itemtype") then g_nPage = 1
		session("cm_itemtype") = g_sItemType
	end if
end sub

'**********************************************************************************************
'based on the g_nPage, get the list of 1st page of customers who are based 
'on From Date, To Date, g_sStatus and g_sState and g_sItemType.

' Subroutine to perform queries and generate data islands for the main campaign
' manager list sheet
'
function GetListSheetDataIsland()

	dim sSQLText, xmlData1, xmlData2, xmlMerged, dFormat, xmlNode, xmlDataDoc, xmlEmptyRecord

	'set format dictionary for sedcond and third queries
	set dFormat = server.CreateObject("commerce.dictionary")
	dFormat.CTo = AD_DATE
	dFormat.CFrom = AD_DATE

    '************************************************** DATA1 *****************
	'get customers (scheduled, requests, clicks and click% are 0 until rollups done)
	sSQLText = "SELECT cu.i_customer_id, cu.u_customer_name, type_abr='" & L_CMTypeCustomerAbbr_Text & "', " & _
			"Cfrom='-', Cto='-', scheduled='-', requests='-', clicks='-', click_pct='-', " & _
			"(CASE WHEN cu.dt_customer_archived IS NOT NULL THEN '" & L_CMDeleted_Text & _
			"' ELSE '-' END) active, " & _
			"cu.dt_customer_archived, customer_flag='foo' FROM customer AS cu "

	' g_sState (0=yes, 1=no, 2=both)
	if g_sState = 0 then ' yes 
		sSQLText = sSQLText & " WHERE cu.dt_customer_archived IS NOT NULL "
	elseif g_sState = 1 then 'no
		sSQLText = sSQLText & " WHERE cu.dt_customer_archived IS NULL "
	end if	
	if g_sCustomerName <> "" then ' looking for filtered customers
		if g_sState = 2 then
			sSQLText = sSQLText & " WHERE "
		else
			sSQLText = sSQLText & " AND "
		end if 
		sSQLText = sSQLText & "LOWER(cu.u_customer_name) LIKE LOWER(N'%" & replace(g_sCustomerName, "'", "''") & "%') "
	end if
	sSQLText = sSQLText & " ORDER BY cu.u_customer_name "

	'get data for one page of data starting based on current page
	set xmlData1 = xmlGetXMLFromQueryEx(g_sCampaignConnectionStr, sSQLText, ((g_nPage - 1) * PAGE_SIZE), PAGE_SIZE, -1, null)

	'get customer list to limit second and third queries
	dim sCustomerList, xmlCustomer
	sCustomerList = "("
	for each xmlCustomer in xmlData1.selectNodes("//i_customer_id")
		if sCustomerList <> "(" then sCustomerList = sCustomerList & ","
		sCustomerList = sCustomerList & xmlCustomer.text
	next
	if sCustomerList <> "(" then
		sCustomerList = sCustomerList & ")"
	else
		sCustomerList = "(-1)"
	end if

    '************************************************** DATA2 *****************
	'get campaigns (scheduled, requests, clicks and click% are 0 until rollups done)
	sSQLText = "SELECT cu.i_customer_id, ca.i_camp_id , ca.u_camp_name, " & _
			"(CASE WHEN ca.dt_camp_archived IS NOT NULL THEN '" & L_CMDeleted_Text & _
			"' WHEN ca.b_camp_active = 1 THEN '" & L_CMActiveOption_Text & "' ELSE '" & _
			L_CMInactiveOption_Text & "' END) active, " & _
			"ca.dt_camp_start as Cfrom, ca.dt_camp_end as Cto, ca.dt_camp_archived, " & _
			"type_abr='" & L_CMTypeCampaignAbbr_Text & "', ca.i_camp_events_sched as scheduled, requests=0, clicks=0, " & _
			"click_pct='0', camp_flag = 'foo' " & _
			"FROM customer AS cu, campaign AS ca " & _
			"WHERE cu.i_customer_id = ca.i_customer_id " & _
			"AND cu.i_customer_id IN " & sCustomerList

    if g_sSQLToDate <> "" AND g_sSQLFromDate <> "" then
		' start and end dates
		sSQLText = sSQLText & " AND DATEDIFF(d, ca.dt_camp_start, CONVERT(datetime,'" & g_sSQLToDate & "',20)) >= 0 " & _
				" AND DATEDIFF(d, CONVERT(datetime,'" & g_sSQLFromDate & "',20), ca.dt_camp_end) >= 0 "
	end if

	' g_sStatus (0=active, 1=inactive, 2=all)
	if g_sStatus = 0 then ' active
		sSQLText = sSQLText & " AND ca.b_camp_active = 1 "
	elseif g_sStatus = 1 then	' inactive 
		sSQLText = sSQLText & " AND ca.b_camp_active = 0 "
	end if	

	' g_sState (0=yes, 1=no, 2=both)
	if g_sState = 0 then ' yes 
		sSQLText = sSQLText & " AND ca.dt_camp_archived IS NOT NULL "
	elseif g_sState = 1 then 'no
		sSQLText = sSQLText & " AND ca.dt_camp_archived IS NULL "
	end if	
	sSQLText = sSQLText & " ORDER BY ca.u_camp_name "
	set xmlData2 = xmlGetXMLFromQueryEx(g_sCampaignConnectionStr, sSQLText, -1, -1, -1, dFormat)

	'need to add an empty record under each campaign otherwise
	'  binding attaches to level above
	set xmlDataDoc = Server.CreateObject("MSXML.DOMDocument")
	set xmlDataDoc.documentElement = xmlDataDoc.createElement("record")
	set xmlEmptyRecord = xmlDataDoc.documentElement
	with xmlEmptyRecord
		.appendChild(xmlDataDoc.createElement("i_customer_id"))
		.appendChild(xmlDataDoc.createElement("i_camp_id"))
		.appendChild(xmlDataDoc.createElement("u_campitem_name"))
		.appendChild(xmlDataDoc.createElement("i_campitem_id"))
		.appendChild(xmlDataDoc.createElement("type_abr"))
		.appendChild(xmlDataDoc.createElement("b_campitem_active"))
		.appendChild(xmlDataDoc.createElement("Cfrom"))
		.appendChild(xmlDataDoc.createElement("Cto"))
		.appendChild(xmlDataDoc.createElement("campitem_flag"))
		.appendChild(xmlDataDoc.createElement("camptype_id"))
		.appendChild(xmlDataDoc.createElement("scheduled"))
		.appendChild(xmlDataDoc.createElement("requests"))
		.appendChild(xmlDataDoc.createElement("active"))
		.appendChild(xmlDataDoc.createElement("clicks"))
		.appendChild(xmlDataDoc.createElement("click_pct"))
		.appendChild(xmlDataDoc.createElement("dt_campitem_archived"))
	end with

	for each xmlNode in xmlData2.selectNodes("record")
		xmlNode.appendChild(xmlEmptyRecord.cloneNode(true))
	next

	'Merge all data together into hierarchy
	set xmlMerged = xmlMergeGroups2(xmlData1, xmlData2, "i_customer_id")

	for each xmlNode in xmlMerged.selectNodes("record")
		if xmlNode.selectSingleNode("record") is nothing then xmlNode.appendChild(xmlEmptyRecord.cloneNode(true))
	next

	GetListSheetDataIsland = xmlMerged.xml
end function
%>