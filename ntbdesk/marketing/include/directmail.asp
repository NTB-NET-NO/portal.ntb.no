<%
on error resume next

'frequency constants
Const SQLDMOFreq_OneTime = 1
Const SQLDMOFreq_Daily = 4
Const SQLDMOFreq_Weekly = 8
Const SQLDMOFreq_Monthly = 16
Const SQLDMOFreq_MonthlyRelative = 32

'days of week constants
Const SQLDMOWeek_Sunday = 1
Const SQLDMOWeek_Monday = 2
Const SQLDMOWeek_Tuesday = 4
Const SQLDMOWeek_Wednesday = 8
Const SQLDMOWeek_Thursday = 16
Const SQLDMOWeek_Friday = 32
Const SQLDMOWeek_Saturday = 64
Const SQLDMOWeek_EveryDay = 127
Const SQLDMOWeek_WeekDays = 62
Const SQLDMOWeek_WeekEnds = 65


'###############################################################
' Function: sGetJobName
'	Generates a SQL Scheduling Agent job name from the campaign
'	item ID and direct mail item ID
'
' Input: 
'	sCampItemID
'
' Returns:
'	string - job name
'###############################################################
Function sGetJobName(sCampItemID, sSiteName)
	sGetJobName = "CS_DM_" & sSiteName & "_" & sCampItemID
End Function

'###############################################################
' Function: sGetScheduleName
'	Generates a SQL Scheduling Agent schedule name from the campaign
'	item ID and direct mail item ID
'
' Input: 
'	sJobName
'
' Returns:
'	string - schedule name
'###############################################################
Function sGetScheduleName(sJobName)
	sGetScheduleName = sJobName & "_Schedule"
End Function

'###############################################################
' Function: sDMODateToScreenDate
'	Transforms date from DMO long to string
'
' Input: 
'	iDMODate - long int returned from DMO
'
' Returns:
'	string - date formatted as string
'###############################################################

Function sDMODateToScreenDate(ByRef iDMODate)
	Dim sTempDate, dtNewDate

	sTempDate = cStr(iDMODate)
	dtNewDate = DateSerial(CInt(Left(sTempDate,4)), CInt(Mid(sTempDate,5,2)), CInt(Right(sTempDate,2)))

	sDMODateToScreenDate = g_MSCSDataFunctions.date(dtNewDate, g_DBDefaultLocale)
End Function

'###############################################################
' Function: sDMOTimeToScreenTime
'	Transforms time from DMO long to string
'
' Input: 
'	iDMOTime - long int returned from DMO
'
' Returns:
'	string - time formatted as string
'###############################################################

Function sDMOTimeToScreenTime(ByRef iDMOTime)
	Dim sTempTime, sNewTime

	if iDMOTime = 0 then
		sDMOTimeToScreenTime = "0000"
	else
		sTempTime = cStr(iDMOTime)
		'strip seconds
		sTempTime = Left(sTempTime, Len(sTempTime) - 2)
		'add leading zero if needed
		if len(sTempTime) = 3 then sTempTime = "0" & sTempTime
		sDMOTimeToScreenTime = sTempTime
	end if
End Function

' routine to fix time value to be compatible with SQL Agent
Function FixTime(sTime)
    FixTime = CLng(sTime & "00")
End Function

'routine to fix date value to be compatible with SQL Agent
Function FixDate(sDate)
    Dim iDay
    Dim iMonth
    Dim iYear
    Dim sResult
    on error resume next
    iDay = Day(sDate)
    iMonth = Month(sDate)
    iYear = Year(sDate)
    sResult = CStr(iYear)
    if iMonth < 10 Then
		sResult = sResult & "0" 
	end if
	sResult = sResult & iMonth
	if iDay < 10 then
		sResult = sResult & "0" 
	end if
	sResult = sResult & iDay	
    FixDate = CLng(sResult)
End Function

%>
