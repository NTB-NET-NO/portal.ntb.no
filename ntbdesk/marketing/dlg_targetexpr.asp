<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!-- #include FILE="include/marketingStrings.asp" -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->

<%
	dim g_sExprType, g_opType

	g_sExprType = Request("exprtype")

	if request("catalog_expr_id") <> "" and request("catalog_expr_id") <> "-1" then
		g_opType = "edit"
	else
	    g_opType = "new"
	end if
%>

<html>
<head>
<%	if g_sExprType = "CATALOG" then
%>		<TITLE><%= L_TgExprHeadingForCatalog_Text %></TITLE>
<%	else
%>		<TITLE><%= L_TgExprHeadingforTarget_Text %></TITLE>
<%	end if
%>	<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">
	<STYLE>
		HTML
		{
			WIDTH: 550px;
			HEIGHT: 360px;
		}
		BODY
		{
			PADDING: 15px;
			MARGIN: 0;
		}
	</STYLE>
	<SCRIPT LANGUAGE="VBScript">
		sub ExprBldronReady()
			call divExprBldr.Activate(postform.exprmode.value, postform.ExprID.value)  
		end sub	

		sub ExprBldronComplete()
			'check here to see if the expression is saved
			window.returnValue = -1
			if divExprBldr.GetResults.Item("SavedExpr") then
				postform.all.ExprID.value = divExprBldr.GetResults.Item("ExprID")	
				postform.all.ExprName.value = divExprBldr.GetResults.Item("ExprName")	
				postform.all.exprmode.value = "edit"
				window.returnValue = postform.all.ExprID.value & "|" & postform.all.ExprName.value
			else
				window.returnValue = -1
			End if
					
			'deactivate after each save or cancel
			divExprBldr.Deactivate
	
			window.close
		end sub
	</SCRIPT>
</head>
<body SCROLL='no' LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then postform.btnCancelExpr.click">

<!--********Expression Builder XML Config ********************************************-->
<%	if g_sExprType = "CATALOG" then
%>	<XML ID="xmlisConfig">
	<EBCONFIG>
		<DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
		<CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
		<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
		<DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
		<!-- The presence of the DEBUG node tells EB to output debug info.
		<DEBUG /> -->
		<ADVANCED /> 
		<EXPR-ASP><%= sRelURL2AbsURL("../exprarch") %></EXPR-ASP>
		<HELPTOPIC><%= sRelURL2AbsURL("../docs/default.asp?helptopic=cs_bd_campaigns_ZHMX.htm") %></HELPTOPIC>
		<SITE-TERMS URL='<%= sRelURL2AbsURL("../catalogs/editor/qbenumeratedvalues.asp") %>' />
		<TYPESOPS><%= sRelURL2AbsURL("../catalogs/editor/QBTypesOps.xml") %></TYPESOPS>

	    <!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
		<PROFILE-LIST>
			<PROFILE SRC="<%= sRelURL2AbsURL("include/qbproduct.asp") %>" /> 
		</PROFILE-LIST>
	    <SAVE-CATEGORY>CATALOG</SAVE-CATEGORY>
	</EBCONFIG>
	</XML>
<%	else ' Camapaigns
%>	<XML ID="xmlisConfig">
	<EBCONFIG>
		<DATE-FORMAT><%= g_sMSCSDateFormat %></DATE-FORMAT>
		<CURRENCY-FORMAT><%= g_sMSCSCurrencyFormat %></CURRENCY-FORMAT>
		<NUMBER-FORMAT><%= g_sMSCSNumberFormat %></NUMBER-FORMAT>
		<DATE-FIRSTDAY><%= g_sMSCSWeekStartDay %></DATE-FIRSTDAY>
		<!-- The presence of the DEBUG node tells EB to output debug info.
		<DEBUG /> -->
		<ADVANCED /> 
		<EXPR-ASP><%= sRelURL2AbsURL("../exprarch") %></EXPR-ASP>
		<HELPTOPIC><%= sRelURL2AbsURL("../docs/default.asp?helptopic=cs_bd_campaigns_CDLT.htm") %></HELPTOPIC>

		<SITE-TERMS URL="<%= sRelURL2AbsURL("../common/QBSiteTermsTransform.asp?LoadCatalogSets=1") %>" />

		<!-- The PROFILE-LIST and PROFILE nodes specify the profiles EB uses. -->
		<PROFILE-LIST>
		<PROFILE SRC="<%= sRelURL2AbsURL("../common/QBProfileTransform.asp?CURR_PROFILE=Profile Definitions.UserObject") %>"></PROFILE>
			<PROFILE>Profile Definitions.TargetingContext</PROFILE>
		</PROFILE-LIST>
		<SAVE-CATEGORY>CAMPAIGNS</SAVE-CATEGORY>
	</EBCONFIG>
	</XML>
<%	end if
%>
<form ID="postform" ACTION METHOD="post" >
	<input TYPE="hidden" NAME="ExprID" value="<% = request("catalog_Expr_ID")%>" >
	<input TYPE="hidden" NAME="ExprName" value="" >
<%	if g_opType = "new" then
%>		<input TYPE="hidden" NAME="exprmode" value="create" >
<%	Else
%>		<input TYPE="hidden" NAME="exprmode" value="edit" >
<%	End if
%>

<!--********DIV Definitions *********************************************************-->

	<DIV ID="divExprBldr" CLASS="clsExprBldr" 
	  STYLE="behavior:url(/widgets/exprbldrhtc/ExprBldr.htc); width:100%" XMLCfgID="xmlisConfig"
	  LANGUAGE="VBScript" ONREADY="ExprBldronReady" ONCOMPLETE="ExprBldronComplete">
	  <H3 ALIGN="center"><%= L_TgExprLoading_Text %></H3>
	</DIV>

</form>
</body>
</html>
