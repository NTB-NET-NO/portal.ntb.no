<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE="include/marketingStrings.asp" -->
<!--#INCLUDE FILE="include/campitem_util.asp" -->

<%
'CONST Declarations
Const SEPARATOR_BARS = "|||"
Const SEPARATOR_BANGS = "!!!"

dim g_dFormValues, g_sCampaignConnectionStr, g_rsQuery, g_sQuery, g_xmlReturn, g_sMSCSWeekStartDay

set g_dFormValues = dGetRequestXMLAsDict()
set g_xmlReturn = xmlGetXMLDOMDoc()
g_sMSCSWeekStartDay		= Application("MSCSWeekStartDay")

'convert dates to system
g_dFormValues("dt_campitem_start") = g_MSCSDataFunctions.ConvertDateString(g_dFormValues("dt_campitem_start"), g_DBDefaultLocale)
'add time component if add starting today
if DateDiff("d", Now(), CDate(g_dFormValues("dt_campitem_start"))) = 0 then g_dFormValues("dt_campitem_start") = Now()
g_dFormValues("dt_campitem_end") = g_MSCSDataFunctions.ConvertDateString(g_dFormValues("dt_campitem_end"), g_DBDefaultLocale)
'add time of 23:59:59 to end date so last day is not missed
g_dFormValues("dt_campitem_end") = DateAdd("s", ((60 * 60 * 24) - 1), g_dFormValues("dt_campitem_end"))
 
'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
if g_oConn.State = AD_STATE_CLOSED then
	AddErrorNode g_xmlReturn, "1", sGetErrorByID("Cant_connect_db"), ""
	'clear errors set in session
	Session("MSCSBDError") = empty
else
	Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

	Select Case g_dFormValues("campitem_type")
		case GUID_TYPE_AD
			Select Case g_dFormValues("dbaction")
				Case "insert"
					InsertAdItem()
				Case "update"
					UpdateAdItem()
				case else
					AddErrorNode g_xmlReturn, "1", L_InvalidDBAction_ErrorMessage, ""
			End Select
		case GUID_TYPE_DISC
			Select Case g_dFormValues("dbaction")
				Case "insert"
					InsertDiscountItem()
				Case "update"
					UpdateDiscountItem()
				case else
					AddErrorNode g_xmlReturn, "1", L_InvalidDBAction_ErrorMessage, ""
			End Select
		case else
			AddErrorNode g_xmlReturn, "1", sFormatString(L_InvalidCampItemGUID_ErrorMessage, array(g_dFormValues("campitem_type"))), ""
	End Select
end if

Response.Write g_xmlReturn.xml

Sub RollBackAndReportError(byRef oConn, byVal sErrorText)
	Dim oError
	For Each oError In oConn.Errors
		AddErrorNode g_xmlReturn, "1", sErrorText, "<![CDATA[" & sGetADOError(oError) & "]]>"
	Next
	'roll back the transaction:
	oConn.RollbackTrans
End Sub

Sub CleanUpParameters(byRef oCmd)
	Dim i
	With oCmd.Parameters
		For i = 0 To .Count -1
			.Delete 0
		Next
	End With
End Sub

Function rsGetCreativeTypes(byVal sCreativeTypeID)
	Dim oConn
	g_sQuery = "SELECT i_cp_id, u_cp_name, i_cp_type, u_cp_label, b_cp_required " & _
		"FROM creative_property WHERE i_creative_type_id = " & sCreativeTypeID & _
		" AND i_cp_order IS NOT NULL ORDER BY i_cp_order"
	Set oConn = oGetADOConnection(g_sCampaignConnectionStr)
	Set rsGetCreativeTypes = oConn.Execute(g_sQuery, , AD_CMD_TEXT)
End function

REM  ---------------------------------------------------------------
REM
REM		Saves the New Expressions into the Expressions Store
REM
REM
REM		INPUTS:
REM			sExprID:		expressions variable ID
REM
REM		RETURNS:
REM			aSetExpression:	an array of expression IDs
REM
Function aSetExpressions(ByRef sSelected, ByRef sExp)

	Dim sExpr
	Dim sProperty, sPropertys, sValue, sType
	Dim aExprs, aExprBody, aTemp
	Dim oExpressionStore
	Dim rsNewExpr, rsProfDeps

	'Get string with concatenated new expression names
	'The Name is the Expression
	sExpr = sExp
	If sExpr = "" Then Exit Function

	'Create ExpressionStore object
	On Error Resume Next
	Set oExpressionStore = Server.CreateObject ("Commerce.ExpressionStore")
	If oExpressionStore Is Nothing then Exit Function
	oExpressionStore.Connect(g_sCampaignConnectionStr)
	If Err.Number <> 0 Then Exit Function

	aExprs = Split(sExpr, SEPARATOR_BANGS)
	For Each sExpr In aExprs
		' Build the Corresponding XML
		aTemp = Split(sExpr, SEPARATOR_BARS)
		sType = aTemp(1)
		aExprBody = Split (aTemp(0), "=")
		sProperty = RTrim(aExprBody(0))
		sValue = LTrim(aExprBody(1))
		' Remove the Catalog Name (!?)
		aTemp = Split(sProperty, ".")
		sPropertys = trim(aTemp(1))

		' Save the Expression into Expression Store
		Set rsNewExpr = oExpressionStore.NewExpression
		If Err.Number <> 0 Then Exit Function
		' ExprName should be less than 128 chars
		rsNewExpr.Fields("ExprName") = Left(sValue,30)
		' Generate the Expression Body XML
		if sPropertys = "CategoryName" then
			rsNewExpr.Fields("ExprBody") = _
				"<CLAUSE OPER='contains'>" & _
					"<Property ID='product._product_categories' TYPE='" & UCase(sType) & "' MULTIVAL='True'/>" & _
					"<IMMED-VAL TYPE='" & LCase(sType) & "'>" & sValue & "</IMMED-VAL>" & _
				"</CLAUSE>"
		else	'product name:
			rsNewExpr.Fields("ExprBody") = _
				"<CLAUSE OPER='equal'>" & _
					"<Property ID='product._product_" & sPropertys & "' TYPE='" & UCase(sType) & "'/>" & _
					"<IMMED-VAL TYPE='" & LCase(sType) & "'>" & sValue & "</IMMED-VAL>" & _
				"</CLAUSE>"
		end if
		rsNewExpr.Fields("Category") = "CATALOG"
		rsNewExpr.Fields("ExprDesc") = ""
		' Set the Profile Dependencies for the New Expression
		Set rsProfDeps = rsNewExpr.Fields("rsProfDeps").Value
	    rsProfDeps.AddNew
		rsProfDeps.Fields("ProfDep") = "Product"
		' S A V E It
		oExpressionStore.SaveExpression(rsNewExpr)
 
		rsNewExpr.Close
		Set rsNewExpr = Nothing
	Next
	oExpressionStore.disconnect
	Set oExpressionStore = Nothing

	aSetExpressions = aSetExpr(sSelected)
End Function

Function aSetExpr(ByRef sSelected)
	dim objExprStore, rsExprInfo, sSelectedID, strCategory
	set objExprStore = Server.CreateObject("Commerce.ExpressionStore")
	call objExprStore.Connect(g_sCampaignConnectionStr)

	strCategory = "CATALOG"
	set rsExprInfo = objExprStore.Query(, strCategory)
	do While Not rsExprInfo.EOF
		IF Trim(rsExprInfo("ExprName").value) = Trim(sSelected) then
			sSelectedID = rsExprInfo("ExprID").value
			exit do
		End IF
		rsExprInfo.movenext
	loop
	rsExprInfo.close
	objExprStore.Disconnect 
 
	aSetExpr = sSelectedID
End function

'This function get the whole expression set ex..(stuff.name = CD 34 ||| name ) to parse for use
Function aSetExprWhole(ByRef sExpr)
	dim objExprStore, rsExprInfo, sSelectedID, strCategory, aExprBody, aTemp, sSelected
	

	set objExprStore = Server.CreateObject("Commerce.ExpressionStore")
	call objExprStore.Connect(g_sCampaignConnectionStr)

	aTemp = Split(sExpr, SEPARATOR_BARS)
	aExprBody = Split(aTemp(0), "=")
	sSelected = Trim(aExprBody(1))
	strCategory = "CATALOG"
	set rsExprInfo = objExprStore.Query(, strCategory)
	While Not rsExprInfo.EOF
		IF Trim(rsExprInfo("ExprName").value) = Trim(sSelected) then
			sSelectedID = rsExprInfo("ExprID").value
		End IF
		rsExprInfo.movenext
	Wend
	rsExprInfo.close
	objExprStore.Disconnect 
 
	aSetExprWhole = sSelectedID
End function

Sub LoadSelExprTODB(Byval sSelList, ByVal iDiscountID)
	dim aExprList, sInsert, sItem

	iDiscountID = CLng(iDiscountID)
	'Convert the string to array
	aExprList = Split(sSelList, "|", -1, 1)
	on error resume next
	g_sQuery = "DELETE FROM order_discount_expression WHERE i_disc_id = " & iDiscountID
	g_oConn.Execute g_sQuery
	
	'If Selected Expr is not empty insert newly selected items
	If sSelList <> "" then
		sInsert = "INSERT INTO order_discount_expression VALUES( %1 )"
		For each sItem in aExprList
			if Len(sItem) > 0 Then
				g_sQuery = replace(sInsert, "%1", iDiscountID & "," & sItem)
				g_oConn.Execute g_sQuery
				If Err <> 0 Then
					Exit For
				End If
			End If 
		Next
	End If
End Sub

function nInsertCreative(ByVal sErrorMsg)
	With g_oCmd	
		.CommandText = "sp_InsertCreative"	
		.CommandType = AD_CMD_STORED_PROC
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("customer_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_customer_id")))
			.Append g_oCmd.CreateParameter("creative_type_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_creative_type_id")))
			.Append g_oCmd.CreateParameter("creative_size_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_creative_size_id")))
			.Append g_oCmd.CreateParameter("creative_received", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , Date())
			.Append g_oCmd.CreateParameter("o_CreativeId", AD_INTEGER, AD_PARAM_OUTPUT)

			On Error Resume Next
			g_oCmd.Execute
			If g_oConn.Errors.Count > 0 Then
				RollBackAndReportError g_oConn, sErrorMsg
				nInsertCreative = -1
			else
				nInsertCreative = .item("o_CreativeId").Value	
			End If
			on error goto 0
		End With		
	End With
	CleanUpParameters(g_oCmd)
End function

function bInsertCPValue(ByVal sErrorMsg, ByVal nCreativeID)
	dim rsCreativeTypes, fdCTCPID, fdCTCPName, sCPName
	g_oCmd.CommandText = "sp_InsertCPValue"
	g_oCmd.CommandType = AD_CMD_STORED_PROC

	'get all property values for the selected creative_type_id
	Set rsCreativeTypes = rsGetCreativeTypes(CLng(g_dFormValues("i_creative_type_id")))

	'initialize parameters
	With g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("cp_id", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("creative_id", AD_INTEGER, AD_PARAM_INPUT )
		.Append g_oCmd.CreateParameter("cpv_value", AD_BSTR, AD_PARAM_INPUT)	

		Set fdCTCPID = rsCreativeTypes("i_cp_id")
		Set fdCTCPName = rsCreativeTypes("u_cp_name")
		Do While Not rsCreativeTypes.EOF
			'set values
			.item("cp_id").Value = fdCTCPID.value
			.item("creative_id").Value = nCreativeID
			sCPName = Trim(fdCTCPName.value & g_dFormValues("i_creative_type_id"))
			.item("cpv_value").Size = Len(Trim(g_dFormValues(sCPName)))
			.item("cpv_value").value = Trim(g_dFormValues(sCPName))
			On Error Resume Next
			g_oCmd.Execute
			If g_oConn.Errors.count > 0 Then
				RollBackAndReportError g_oConn, sErrorMsg
				bInsertCPValue = false
				Exit function
			End If
			on error goto 0
			rsCreativeTypes.MoveNext
		Loop
	End With	
	rsCreativeTypes.Close
	CleanUpParameters(g_oCmd)
	bInsertCPValue = true
End function

function nInsertCampaignItem(ByVal sErrorMsg, ByVal nCreativeID, byVal sGUID)
	dim sLogonUser
	sLogonUser = Request.ServerVariables("LOGON_USER")
	g_oCmd.CommandText = "sp_InsertCampaignItem"
	g_oCmd.CommandType = AD_CMD_STORED_PROC
	'initialize parameters
	With g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("creative_id", AD_INTEGER, AD_PARAM_INPUT, , nCreativeID)
		.Append g_oCmd.CreateParameter("guid_type", AD_GUID, AD_PARAM_INPUT, , sGUID)
		.Append g_oCmd.CreateParameter("campitem_name", AD_BSTR, AD_PARAM_INPUT, Len(g_dFormValues("u_campitem_name")), g_dFormValues("u_campitem_name"))
		.Append g_oCmd.CreateParameter("campitem_active", AD_BOOLEAN, AD_PARAM_INPUT, , CBool(g_dFormValues("b_campitem_active")))
		.Append g_oCmd.CreateParameter("exposure_limit", AD_INTEGER, AD_PARAM_INPUT, , CInt(g_dFormValues("i_campitem_exposure_limit")))
		.Append g_oCmd.CreateParameter("date_start", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , CDate(g_dFormValues("dt_campitem_start")))
		.Append g_oCmd.CreateParameter("date_end", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , CDate(g_dFormValues("dt_campitem_end")))
		.Append g_oCmd.CreateParameter("campaign_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_camp_id")))
		.Append g_oCmd.CreateParameter("date_modified", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , Now())
		.Append g_oCmd.CreateParameter("modified_by", AD_BSTR, AD_PARAM_INPUT, Len(sLogonUser), sLogonUser)
		.Append g_oCmd.CreateParameter("campitem_comments", AD_BSTR, AD_PARAM_INPUT, Len(g_dFormValues("u_campitem_comments")), g_dFormValues("u_campitem_comments"))
		.Append g_oCmd.CreateParameter("o_Campitem_id", AD_INTEGER, AD_PARAM_OUTPUT)

		On Error Resume Next
		g_oCmd.Execute
		If g_oConn.Errors.count > 0 Then
			RollBackAndReportError g_oConn, sErrorMsg
			nInsertCampaignItem = -1
		else
			nInsertCampaignItem = .item("o_Campitem_id").Value
		End If
		on error goto 0
	End With
	CleanUpParameters(g_oCmd)
End function

function bInsertTargetGroup(ByVal sErrorMsg, ByVal nCampItemID, ByRef aTargetSelect)
	dim i

	bInsertTargetGroup = true
	g_oCmd.CommandText = "sp_InsertTargetGroup"
	g_oCmd.CommandType = AD_CMD_STORED_PROC
	'initialize parameters
	With g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("targroup_id", AD_INTEGER, AD_PARAM_INPUT)
		For i = 0 To UBound(aTargetSelect) - 1
			.item("campitem_id").value = nCampItemID
			.item("targroup_id").value = CLng(aTargetSelect(i))
			On Error Resume Next
			g_oCmd.Execute
			If g_oConn.Errors.count > 0 Then
				RollBackAndReportError g_oConn, sErrorMsg
				bInsertTargetGroup = false
				Exit for
			End If
			on error goto 0
		Next
	End With
	CleanUpParameters(g_oCmd)
End function

function bInsertPageGroup(ByVal sErrorMsg, ByVal nCampItemID, ByRef aPageSelect)
	dim i

	bInsertPageGroup = true
	g_oCmd.CommandText = "sp_InsertPageGroup"
	g_oCmd.CommandType = AD_CMD_STORED_PROC
	'initialize parameters
	With g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("pagegroup_id", AD_INTEGER, AD_PARAM_INPUT)
		'If the Run of Site check box is checked, gotta insert one more record
		If g_dFormValues("RunOfSite") <> "" Then
			.item("campitem_id").value = nCampItemID
			.item("pagegroup_id").value = CLng(g_dFormValues("RunOfSite"))
			On Error Resume Next
			g_oCmd.Execute
			If g_oConn.Errors.count > 0 Then
				RollBackAndReportError g_oConn, sErrorMsg
				bInsertPageGroup = false
				Exit function
			End If
			on error goto 0
		End If

		For i = 0 To UBound(aPageSelect) - 1
			.item("campitem_id").value = nCampItemID
			.item("pagegroup_id").value = CLng(aPageSelect(i))
			On Error Resume Next
			g_oCmd.Execute
			If g_oConn.Errors.count > 0 Then
				RollBackAndReportError g_oConn, sErrorMsg
				bInsertPageGroup = false
				Exit for
			End If
			on error goto 0
		Next
	End With
	CleanUpParameters(g_oCmd)
End function

function bUpdateCreative(ByVal sErrorMsg)
	With g_oCmd
		.CommandText = "sp_UpdateCreative"
		.CommandType = AD_CMD_STORED_PROC
	
		'initialize parameters
		with .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("creative_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_creative_id")))
			.Append g_oCmd.CreateParameter("creative_type_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_creative_type_id")))
			.Append g_oCmd.CreateParameter("creative_size_id", AD_INTEGER, AD_PARAM_INPUT, , cLng(g_dFormValues("i_creative_size_id")))
		End With
	end with

	on error resume next
	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, sErrorMsg
		bUpdateCreative = false
	else
		bUpdateCreative = true
	End If
	on error goto 0
	CleanUpParameters(g_oCmd)
End function

function bUpdateCPValue(ByVal sErrorMsg)
	dim rsCreativeProperties, rsCreativeTypes, fdCPCPID, fdCTCPID, fdCTCPName, sCPName
	Dim sQuery
	bUpdateCPValue = False
	sQuery = "SELECT i_cp_id FROM creative_property_value WHERE i_creative_id = " & g_dFormValues("i_creative_id")
	Set rsCreativeProperties = Server.CreateObject("ADODB.Recordset")
	rsCreativeProperties.Open sQuery, g_oConn, AD_OPEN_DYNAMIC, AD_LOCK_PESSIMISTIC, AD_CMD_TEXT
	Set fdCPCPID = rsCreativeProperties("i_cp_id")

	'get all property values for the selected creative_type_id
	set rsCreativeTypes = rsGetCreativeTypes(g_dFormValues("i_creative_type_id"))
	Set fdCTCPID = rsCreativeTypes("i_cp_id")
	Set fdCTCPName = rsCreativeTypes("u_cp_name")

	g_oCmd.CommandText = "sp_InsertCPValue"
	g_oCmd.CommandType = AD_CMD_STORED_PROC
	'initialize parameters
	with g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("cp_id", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("creative_id", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("cpv_value", AD_BSTR, AD_PARAM_INPUT)
	
		'loop thru the existing cp_ids to find a match to new ons being added.
		'if a match is found, delete the existing cp_id from creative_property_value table
		'and insert the new cp_id.
		do while not rsCreativeTypes.EOF
			'Check for a match between the value from rsCreativeTypes and rsCreativeProperties
			do while not rsCreativeProperties.EOF
				if fdCPCPID.value = fdCTCPID.value then
					'Delete this particular record by executing from the Connection object
					'sql query for deleting records from creative_property_value table when a particular
					'i_cp_id need to be updated. Instead of updating it is easier to delete and insert
					g_sQuery = "DELETE FROM creative_property_value " & _
						"WHERE i_creative_id = " & CLng(g_dFormValues("i_creative_id")) & " AND i_cp_id = " & fdCPCPID.value
					g_oConn.Execute g_sQuery, , AD_CMD_TEXT
					Exit do
				End if
				rsCreativeProperties.MoveNext
			Loop
 			If Not rsCreativeProperties.BOF Then rsCreativeProperties.MoveFirst
	
			'set values				
			.item("cp_id").value = fdCTCPID.value
			.item("creative_id").value = CLng(g_dFormValues("i_creative_id"))
			sCPName = Trim(fdCTCPName.value & g_dFormValues("i_creative_type_id"))
			.item("cpv_value").Size = Len(trim(g_dFormValues(sCPName)))
			.item("cpv_value").value = Trim(g_dFormValues(sCPName))

			On Error Resume Next
			g_oCmd.Execute
			If g_oConn.Errors.count > 0 Then
				RollBackAndReportError g_oConn, sErrorMsg
				bUpdateCPValue = false
				Exit do
			End If
			on error goto 0
			rsCreativeTypes.MoveNext
		Loop
	end with
	rsCreativeTypes.Close
	rsCreativeProperties.Close
	CleanUpParameters(g_oCmd)
	bUpdateCPValue = True
End function

function bUpdateTargetGroup(ByVal sErrorMsg, ByRef aTargetSelect)
	'delete all existing target groups from target_group_xref table for this campaign_item
	g_sQuery = " DELETE FROM target_group_xref WHERE i_campitem_id = " & CLng(g_dFormValues("i_campitem_id"))
	g_oConn.Execute g_sQuery, , AD_CMD_TEXT	
	
	bUpdateTargetGroup = bInsertTargetGroup(sErrorMsg, CLng(g_dFormValues("i_campitem_id")), aTargetSelect)
End function

function bUpdatePageGroup(ByVal sErrorMsg, ByRef aPageSelect)
	'delete all existing page groups from page_group_xref table for this campaign_item
	g_sQuery = " DELETE FROM page_group_xref WHERE i_campitem_id = " & CLng(g_dFormValues("i_campitem_id"))
	g_oConn.Execute g_sQuery, , AD_CMD_TEXT	
	
	bUpdatePageGroup = bInsertPageGroup(sErrorMsg, CLng(g_dFormValues("i_campitem_id")), aPageSelect)
End function

function bUpdateCampItem(ByVal sErrorMsg)
	With g_oCmd
		.CommandType = AD_CMD_STORED_PROC
		.CommandText = "sp_UpdateCampaignItem"
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_campitem_id")))
			.Append g_oCmd.CreateParameter("campitem_name", AD_BSTR, AD_PARAM_INPUT, Len(g_dFormValues("u_campitem_name")), g_dFormValues("u_campitem_name"))
			.Append g_oCmd.CreateParameter("campitem_active", AD_BOOLEAN, AD_PARAM_INPUT, , CBool(g_dFormValues("b_campitem_active")))
			.Append g_oCmd.CreateParameter("exposure_limit", AD_INTEGER, AD_PARAM_INPUT, , CInt(g_dFormValues("i_campitem_exposure_limit")))
			.Append g_oCmd.CreateParameter("date_start", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , CDate(g_dFormValues("dt_campitem_start")))
			.Append g_oCmd.CreateParameter("date_end", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , CDate(g_dFormValues("dt_campitem_end")))
			.Append g_oCmd.CreateParameter("date_modified", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , Now())
			.Append g_oCmd.CreateParameter("modified_by", AD_BSTR, AD_PARAM_INPUT, Len(request.ServerVariables("LOGON_USER")), request.ServerVariables("LOGON_USER"))
			.Append g_oCmd.CreateParameter("campitem_comments", AD_BSTR, AD_PARAM_INPUT, Len(g_dFormValues("u_campitem_comments")), g_dFormValues("u_campitem_comments"))
		End With
	End With	
	On Error Resume Next
	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, sErrorMsg
		bUpdateCampItem = false
	else
		bUpdateCampItem = true
	End If
	on error goto 0
	CleanUpParameters(g_oCmd)
End function

function nGetDaysOfWeek()
	dim nDaysOfWeek
	nDaysOfWeek = 0
	if isNumeric(g_dFormValues("day_sun")) then nDaysOfWeek = nDaysOfWeek + CInt(g_dFormValues("day_sun"))
	if isNumeric(g_dFormValues("day_mon")) then nDaysOfWeek = nDaysOfWeek + CInt(g_dFormValues("day_mon"))
	if isNumeric(g_dFormValues("day_tue")) then nDaysOfWeek = nDaysOfWeek + CInt(g_dFormValues("day_tue"))
	if isNumeric(g_dFormValues("day_wed")) then nDaysOfWeek = nDaysOfWeek + CInt(g_dFormValues("day_wed"))
	if isNumeric(g_dFormValues("day_thu")) then nDaysOfWeek = nDaysOfWeek + CInt(g_dFormValues("day_thu"))
	if isNumeric(g_dFormValues("day_fri")) then nDaysOfWeek = nDaysOfWeek + CInt(g_dFormValues("day_fri"))
	if isNumeric(g_dFormValues("day_sat")) then nDaysOfWeek = nDaysOfWeek + CInt(g_dFormValues("day_sat"))
	nGetDaysOfWeek = nDaysOfWeek
End function

'******************************************************************************
' AD DB Insert 
'	The following are the steps to make a complete insert of an Ad item. 
'   Step 1. Insert record into Creative table. Get the creative_id and use it in Step 2 and 3.
'   Step 2. Insert records into Creative_property_values table
'   Step 3. Insert records into Campitem table. Get the camppaign_item_id from this insert
'   Step 4. Insert records into Aditem table
'   Step 5. Insert records into target_group_xref table
'   Step 6. Insert record into page_group_xref table
'	Step 7. Calculate goaling
'******************************************************************************
Sub InsertAdItem()
	Dim nCreativeID, bSuccess, nCampItemID, nDaysOfWeek, aTargetSelect, aPageSelect

	On Error Resume Next
	g_oConn.BeginTrans

	'**************************************************************************
	'1. Insert record into Creative table. 
	'Get the creative_id and use it in Step 2 and 3.
	nCreativeID = nInsertCreative(L_AdUnableToSave_ErrorMessage)
	if nCreativeID < 0 then exit sub

	'**************************************************************************
	'2. Insert records into Creative_property_values table
	bSuccess = bInsertCPValue(L_AdUnableToSave_ErrorMessage, nCreativeID)
	if not bSuccess then exit sub

	'**************************************************************************
	'3. Insert record into Campitem table.
	'Get the campaign_item_id from this insert
	nCampItemID = nInsertCampaignItem(L_AdUnableToSave_ErrorMessage, nCreativeID, GUID_TYPE_AD)
	if nCampItemID < 0 then exit sub

	'**************************************************************************
	'4. Insert record into Aditem table
	nDaysOfWeek = nGetDaysOfWeek()
	With g_oCmd
		.CommandText = "sp_InsertAdItem"
		.CommandType = AD_CMD_STORED_PROC
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("industry_id", AD_INTEGER, AD_PARAM_INPUT)
			.Append g_oCmd.CreateParameter("aditem_type", AD_SMALLINT, AD_PARAM_INPUT, , CInt(g_dFormValues("i_aditem_type")))
			.Append g_oCmd.CreateParameter("days_of_week", AD_SMALLINT, AD_PARAM_INPUT, , nDaysOfWeek)
			.Append g_oCmd.CreateParameter("time_start", AD_SMALLINT, AD_PARAM_INPUT, , CInt(g_dFormValues("i_aditem_time_of_day_start")))
			.Append g_oCmd.CreateParameter("time_end", AD_SMALLINT, AD_PARAM_INPUT, , CInt(g_dFormValues("i_aditem_time_of_day_end")))
			.Append g_oCmd.CreateParameter("events_scheduled", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_aditem_events_scheduled")))
			.Append g_oCmd.CreateParameter("ad_weight", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_aditem_weight")))
			.Append g_oCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT, , nCampItemID)
			.Append g_oCmd.CreateParameter("o_aditem_id", AD_INTEGER, AD_PARAM_OUTPUT)
			If CLng(g_dFormValues("i_industry_id")) = 0 Then
				.item("industry_id").value = NULL
			else
				.item("industry_id").value = CLng(g_dFormValues("i_industry_id"))
			end if
		End With
	End With

	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, L_AdUnableToSave_ErrorMessage
		Exit Sub
	End If
	CleanUpParameters(g_oCmd)

	'********************************************************************************************************************************************	
	'5. Insert records into target_group_xref table
	aTargetSelect = Split(g_dFormValues("TargetSelect"), "|", -1, 1)
	bSuccess = bInsertTargetGroup(L_AdUnableToSave_ErrorMessage, nCampItemID, aTargetSelect)
	if not bSuccess then exit sub

	'********************************************************************************************************************************************	
	'6. Insert record into page_group_xref table
	'Need to parse thru the form variables in the g_dFormValues object to look for a pattern called "_adpg" which stands for Ad Page Groups 
	aPageSelect = Split(g_dFormValues("PageSelect"), "|", -1, 1)
	bSuccess = bInsertPageGroup(L_AdUnableToSave_ErrorMessage, nCampItemID, aPageSelect)
	if not bSuccess then exit sub

	if Err.number <> 0 then
		AddErrorNode g_xmlReturn, "1", L_AdUnableToSave_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
	else
		g_oConn.CommitTrans

		'set new camp id and creative id so they can be displayed
		AddItemNode "campitem_id", nCampItemID
		AddItemNode "creative_id", nCreativeID
		AddItemNode "disc_cond_option", g_dFormValues("u_disc_cond_option")
		AddItemNode "disc_award_option", g_dFormValues("u_disc_award_option")
	End If

	'**************************************************************************
	'7. Calculate goaling - this has to be outside of transaction
	If CBool(g_dFormValues("b_camp_level_goal")) Then
		if CBool(g_dFormValues("requires_checkpoint")) Then DistributeTotalImpressions(CLng(g_dFormValues("i_camp_id")))
	Else 'Ad Item goal
		UpdateTotalImpressions(CLng(g_dFormValues("i_camp_id")))
	End If

End sub

'******************************************************************************
' Ad DB Update
'	
'	When data is clean and no errors have been found
'	do the actual update process of this Ad item. The following steps illustrate the logic used
'	Step 1. Update creative table.
'	Step 2. Update the creative_property_value table. 
'  		* Here the existing items gets updated and new ones get added, so that when a user 
'		* goes back to the original Creative Type, the existing data gets filled in automatically	
'	Step 3.Update the target_group_xref table.
'		* Here we delete the old target groups in the target_group_xref table and insert the newly selected ones
'	Step 4.  Update the page_group_xref table.
'		* Here we delete the old page groups in the page_group_xref table and insert the newly selected ones
'	Step 5. Update the campaign_item table
'	Step 6. Update the ad_item table
'	Step 7. Calculate goaling
'******************************************************************************
Sub UpdateAdItem()
	Dim bSuccess, nDaysOfWeek, aPageSelect, aTargetSelect

	On Error Resume Next
	g_oConn.BeginTrans

	'**************************************************************************
	'1. Update the creative table.
	bSuccess = bUpdateCreative(L_AdUnableToSave_ErrorMessage)
	if not bSuccess then exit sub

	'**************************************************************************
	'2. Update the creative_property_value table. 
	'	* Here the existing items gets updated and new ones get added, so that when a user 
	'	* goes back to the original Creative Type, the existing data gets filled in automatically	
	bSuccess = bUpdateCPValue(L_AdUnableToSave_ErrorMessage)
	if not bSuccess then exit sub

	'**************************************************************************
	'3.Update the target_group_xref table.
	'  * Here we delete the old target groups in the target_group_xref table and insert the newly selected ones
	aTargetSelect = Split(g_dFormValues("TargetSelect"), "|", -1, 1)
	bSuccess = bUpdateTargetGroup(L_AdUnableToSave_ErrorMessage, aTargetSelect)
	if not bSuccess then exit sub

	'**************************************************************************
	'4. Update the page_group_xref table.
	'	* Here we delete the old page groups in the page_group_xref table and insert the newly selected ones
	aPageSelect = Split(g_dFormValues("PageSelect"), "|", -1, 1)
	bSuccess = bUpdatePageGroup(L_AdUnableToSave_ErrorMessage, aPageSelect)
	if not bSuccess then exit sub

	'**************************************************************************
	'5. Update the campaign_item table
	bSuccess = bUpdateCampItem(L_AdUnableToSave_ErrorMessage)
	if not bSuccess then exit sub

	'**************************************************************************
	'6. Update the ad_item table
	nDaysOfWeek = nGetDaysOfWeek()
	With g_oCmd
		.CommandType = AD_CMD_STORED_PROC
		.CommandText = "sp_UpdateAdItem"
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("industry_id", AD_INTEGER, AD_PARAM_INPUT)
			.Append g_oCmd.CreateParameter("aditem_type", AD_SMALLINT, AD_PARAM_INPUT, , CInt(g_dFormValues("i_aditem_type")))
			.Append g_oCmd.CreateParameter("days_of_week", AD_SMALLINT, AD_PARAM_INPUT, , nDaysOfWeek)
			.Append g_oCmd.CreateParameter("time_start", AD_SMALLINT, AD_PARAM_INPUT, , CInt(g_dFormValues("i_aditem_time_of_day_start")))
			.Append g_oCmd.CreateParameter("time_end", AD_SMALLINT, AD_PARAM_INPUT, , CInt(g_dFormValues("i_aditem_time_of_day_end")))
			.Append g_oCmd.CreateParameter("events_scheduled", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_aditem_events_scheduled")))
			.Append g_oCmd.CreateParameter("ad_weight", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_aditem_weight")))
			.Append g_oCmd.CreateParameter("campitem_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_campitem_id")))

			If CLng(g_dFormValues("i_industry_id")) = 0 Then
				.item("industry_id").value = NULL
			else
				.item("industry_id").value = CLng(g_dFormValues("i_industry_id"))
			end if
		End With
	End With

	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, L_AdUnableToSave_ErrorMessage
		Exit Sub
	End If
	CleanUpParameters(g_oCmd)

	if Err.number <> 0 then
		AddErrorNode g_xmlReturn, "1", L_AdUnableToSave_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
	else
		g_oConn.CommitTrans

		'set new camp id and creative id so they can be displayed
		AddItemNode "campitem_id", g_dFormValues("i_campitem_id")
		AddItemNode "creative_id", g_dFormValues("i_creative_id")
	End If

	'**************************************************************************
	'7. Calculate goaling - this has to be outside of transaction
	If CBool(g_dFormValues("b_camp_level_goal")) Then
		if CBool(g_dFormValues("requires_checkpoint")) Then DistributeTotalImpressions(CLng(g_dFormValues("i_camp_id")))
	Else 'Ad Item goal
		UpdateTotalImpressions(CLng(g_dFormValues("i_camp_id")))
	End If

End sub

'*********************************************************************************************************************
' Discount DB Insert process. 
'
' The following are the steps to make a complete insert of an Discount item. 
' Initialize : setup the connection object and Command Object
' Step 1. Insert record into Creative table. Get the creative_id and use it in Step 2 and 3.
' Step 2. Insert records into Creative_property_values table
' Step 3. Insert records into Campitem table. Get the camppaign_item_id from this insert
' Step 4. Insert records into Order Discount table
' Step 5. Insert records into order_discuont_misc table
' Step 6. Insert records into target_group_xref table
' Step 7. Insert record into page_group_sref table
'
'*********************************************************************************************************************
Sub InsertDiscountItem()
	dim nCreativeID, bSuccess, nCampItemID, nDiscountID, aTargetSelect, aPageSelect, _
		sExprID, sTagForUpProcessing

	on error resume next
	g_oConn.BeginTrans

	'**************************************************************************
	'1. Insert record into Creative table. 
	'Get the creative_id and use it in Step 2 and 3.
	nCreativeID = nInsertCreative(L_DiscUnableToSave_ErrorMessage)
	if nCreativeID < 0 then exit sub
	 
	'**************************************************************************
	'2. Insert records into Creative_property_values table
	bSuccess = bInsertCPValue(L_DiscUnableToSave_ErrorMessage, nCreativeID)
	if not bSuccess then exit sub

	'**************************************************************************
	'3. Insert record into Campitem table.
	'Get the campaign_item_id from this insert
	nCampItemID = nInsertCampaignItem(L_DiscUnableToSave_ErrorMessage, nCreativeID, GUID_TYPE_DISC)
	if nCampItemID < 0 then exit sub
	
	'********************************************************************************************************************************************
	'4. Insert records into Discount table and also get nDiscountId for next step
	g_oCmd.CommandText = "sp_InsertDiscount"
	g_oCmd.CommandType = AD_CMD_STORED_PROC
 
	'initialize parameters
	with g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("i_campitem_id", AD_INTEGER, AD_PARAM_INPUT, , nCampItemID)
		.Append g_oCmd.CreateParameter("b_disc_expression_used_for_display", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_template", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_rank", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_rank")))
		.Append g_oCmd.CreateParameter("i_disc_limit", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_limit")))
		.Append g_oCmd.CreateParameter("b_disc_click_required", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("b_disc_disjoint_cond_award", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_cond_expr", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_cond_basis", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_cond_basis")))
		.Append g_oCmd.CreateParameter("mny_disc_cond_min", AD_CURRENCY, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_qty_cond_min", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_award_expr", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_award_max", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_offer_type", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_offer_type")))
		.Append g_oCmd.CreateParameter("mny_disc_offer_value", AD_CURRENCY, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("b_disc_order_level_discount", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("u_disc_special_offer_type", AD_BSTR, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("u_disc_description", AD_BSTR, AD_PARAM_INPUT, Len(Trim(g_dFormValues("u_disc_description"))), Trim(g_dFormValues("u_disc_description")))
		.Append g_oCmd.CreateParameter("o_disc_id", AD_INTEGER, AD_PARAM_OUTPUT)

		'set the values for the params
		if g_dFormValues("DiscUsedForDisplay") = "addition" then
			.item("b_disc_expression_used_for_display").value = 1
		else
			.item("b_disc_expression_used_for_display").value = 0
		end if

		if IsNull(g_dFormValues("i_disc_template")) then
			.item("i_disc_template").value = 0
		else
			.item("b_disc_click_required").value = cLng(g_dFormValues("i_disc_template"))
		end if	

 		If Instr(g_dFormValues("b_disc_click_required"),"on") > 0 Then 
			.item("b_disc_click_required").value = 1
		Else
			.item("b_disc_click_required").value = 0		
		End If

		if g_dFormValues("disjoint_award_cond" ) = "condandaward" then
			.item("b_disc_disjoint_cond_award").value = 0
		else			'g_dFormValues("disjoint_award_cond" ) = "awardonly"
			.item("b_disc_disjoint_cond_award").value = 1
		end if
 
		'If there was new expressions created from product picker
		'Save new expressions to ExpressionStore
		sTagForUpProcessing = "NO"
		IF g_dFormValues("u_disc_cond_option") <> "" then
			'If award_option data has more options means you want to save the complete list
			If g_dFormValues("u_disc_award_option") <> "" then
				If Len(g_dFormValues("u_disc_cond_option")) < Len(g_dFormValues("u_disc_award_option")) then
				   g_dFormValues("u_disc_cond_option") = g_dFormValues("u_disc_award_option")
				End If
			End IF
			'aSetExpressions function split expression strings and save each 
			'param(selected expression, expression strings) returns selected items's exID
			sExprID = aSetExpressions(CStr(g_dFormValues("u_disc_cond_expr")), CStr(g_dFormValues("u_disc_cond_option")))
			sTagForUpProcessing = "YES"
		end if
 
		'Disc_cond_expr
		If g_dFormValues("i_disc_cond_type") <> "" Then
			If g_dFormValues("i_disc_cond_type") = 1 Then	 'Any Product = 0; Catalog Expression = 1; Free Shipping = 2;
				.item("i_disc_cond_expr").value = CLng(g_dFormValues("i_disc_cond_expr"))
			elseif g_dFormValues("i_disc_cond_type") = 2 Then 	
				'If sExprID is empty means no product picker selected made
				If 	sExprID = "" then
					.item("i_disc_cond_expr").value = CLng(g_dFormValues("i_disc_cond_expr"))
				else
					.item("i_disc_cond_expr").value = CLng(sExprID)
					g_dFormValues("i_disc_cond_expr") = sExprID
					sExprID = ""
				end if
			else
				.item("i_disc_cond_expr").value = NULL
			end if
		Else
			.item("i_disc_cond_expr").value = NULL
		End if
	
		if g_dFormValues("i_disc_cond_basis") = 1 then	'Price = 1; Qty = 2
			If IsNumeric(g_dFormValues("i_disc_cond_amt")) then
			'if Instr(1, g_dFormValues("i_disc_cond_amt"), ".") > 0 then
				.item("mny_disc_cond_min").value = CDbl(g_dFormValues("i_disc_cond_amt"))
				.item("i_disc_qty_cond_min").value = NULL
			end if
		else
			if IsNumeric(g_dFormValues("i_disc_cond_amt")) then		
				.item("mny_disc_cond_min").value = NULL
				.item("i_disc_qty_cond_min").value = CLng(g_dFormValues("i_disc_cond_amt"))
			end if		
		End if			
 
		'For no requirement checked do the follwoing business data-fill for component 
 		If g_dFormValues("b_no_requirement") = "1" Then 
			.item("i_disc_cond_expr").value = NULL
			.item("i_disc_cond_basis").value = 2
			.item("i_disc_qty_cond_min").value = 0
		End If 

		if g_dFormValues("u_disc_award_option") <> "" then
			'aSetExpressions function split expression strings and save each 
			'param(selected expression, expression strings) returns selected items's exID
			'If cond already taken care of expression DB saving it just pull the appropriate expression from expression store.
			If sTagForUpProcessing <> "YES" then
				sExprID = aSetExpressions(CStr(g_dFormValues("u_disc_award_expr")), CStr(g_dFormValues("u_disc_award_option")))
			else
				sExprID = aSetExpr(CStr(g_dFormValues("u_disc_award_expr")))
			End If
		end if

		'Deleting options value for update
		g_dFormValues("u_disc_cond_option") = ""
		g_dFormValues("u_disc_award_option") = ""

		'Disc_award_expr 
		if g_dFormValues("i_disc_award_type") <> "" then
			if g_dFormValues("i_disc_award_type") = 1 Then	 'Any Product = 0; Catalog Expression = 1; Product Picker = 2; Free Shipping = 3;
				.item("i_disc_award_expr").value = CLng(g_dFormValues("i_disc_award_expr"))
			elseif g_dFormValues("i_disc_award_type") = 2 Then 	
				'If sExprID is empty means no product picker selected made
				If 	sExprID = "" then
					'If the cond/award using the same product pickers
					IF IsNumeric(g_dFormValues("i_disc_award_expr")) then
						.item("i_disc_award_expr").value = CLng(g_dFormValues("i_disc_award_expr"))
					Else
						.item("i_disc_award_expr").value = aSetExprWhole(g_dFormValues("i_disc_award_expr"))
					End If
				else
					.item("i_disc_award_expr").value = CLng(sExprID)
					g_dFormValues("i_disc_award_expr") = sExprID
				end if
			else
				.item("i_disc_award_expr").value = NULL
			end if
		else
			.item("i_disc_award_expr").value = NULL
		end if
  
		if UCase(g_dFormValues("i_disc_award_max")) = UCase(L_DiscAwardMaxDefault_Text) then
			.item("i_disc_award_max").value = 0
		else
			.item("i_disc_award_max").value = CLng(g_dFormValues("i_disc_award_max"))
		end if
	
		if IsNumeric(g_dFormValues("mny_disc_offer_value")) then
			.item("mny_disc_offer_value").value = g_dFormValues("mny_disc_offer_value")
		end if
	
		'if checkbox("b_freeshipping") = checked then
		'Any Product = 0; Catalog Expression = 1; Product picker =2; Free Shipping = 3;	
		if g_dFormValues("b_freeshipping") = "1" then  
			.item("i_disc_award_expr").value = NULL
			.item("i_disc_award_max").value = 0
			.item("i_disc_offer_type").value = 2
			.item("mny_disc_offer_value").value = 100
			.item("b_disc_order_level_discount").value = 1
			.item("u_disc_special_offer_type").Size = Len("Shipping_Discount") 
			.item("u_disc_special_offer_type").value = "Shipping_Discount"
		else
			.item("b_disc_order_level_discount").value = 0
			g_oCmd.Parameters("u_disc_special_offer_type").value = NULL
		end if				
	end with

	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, L_DiscUnableToSave_ErrorMessage
		exit sub
	End If
	nDiscountID = g_oCmd.Parameters("o_disc_id").value
	CleanUpParameters(g_oCmd)

	'***********************************************************************************************************************
	'Loading all newly selected targexpress items to database				
	'If g_dFormValues("SelDiscExpr") <> "" then
		LoadSelExprTODB g_dFormValues("SelDiscExpr"), Clng(nDiscountID)
	'End If
	'********************************************************************************************************************************************
	'5. Insert records into order_discuont_misc table
	g_oCmd.CommandText = "sp_InsertDiscountMisc"
	g_oCmd.CommandType = AD_CMD_STORED_PROC

	'initialize parameters
	with g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("i_disc_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(nDiscountID))
		.Append g_oCmd.CreateParameter("i_disc_cond_type", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_award_type", AD_INTEGER, AD_PARAM_INPUT)
	
		'Assign 3 if checkbox for no_requirement/freeshipping is checked
		if g_dFormValues("b_no_requirement") = "1" then 'If b_no_requirement is checked
			.item("i_disc_cond_type").value = 3
		else
			.item("i_disc_cond_type").value = g_dFormValues("i_disc_cond_type")
		end if

		if g_dFormValues("b_freeshipping") = "1" then 'If b_freeshipping is checked
			.item("i_disc_award_type").value = 3
		else
			.item("i_disc_award_type").value = g_dFormValues("i_disc_award_type")
		end if
	end with

	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, L_DiscUnableToSave_ErrorMessage
		exit sub
	End If
	CleanUpParameters(g_oCmd)

	'********************************************************************************************************************************************	
	'6. Insert records into target_group_xref table
	aTargetSelect = Split(g_dFormValues("TargetSelect"), "|", -1, 1)
	bSuccess = bInsertTargetGroup(L_DiscUnableToSave_ErrorMessage, nCampItemID, aTargetSelect)
	if not bSuccess then exit sub

	'********************************************************************************************************************************************	
	'7. Insert record into page_group_xref table
	'Need to parse thru the form variables in the g_dFormValues object to look for a pattern called "_discpg" which stands for Ad Page Groups 
	aPageSelect = Split(g_dFormValues("PageSelect"), "|", -1, 1)
	bSuccess = bInsertPageGroup(L_DIscUnableToSave_ErrorMessage, nCampItemID, aPageSelect)
	if not bSuccess then exit sub
	
	if Err.number <> 0 then
		AddErrorNode g_xmlReturn, "1", L_DiscUnableToSave_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
	else
		g_oConn.CommitTrans

		'set new camp id and creative id so they can be displayed
		AddItemNode "campitem_id", nCampItemID
		AddItemNode "creative_id", nCreativeID
		AddItemNode "disc_id", nDiscountID
		AddItemNode "disc_cond_option", g_dFormValues("u_disc_cond_option")
		AddItemNode "disc_award_option", g_dFormValues("u_disc_award_option")
		AddItemNode "disc_cond_expr", g_dFormValues("i_disc_cond_expr")
		AddItemNode "disc_award_expr", g_dFormValues("i_disc_award_expr")
	end if
End sub

 '******************************************************************************************************************
' Discount DB Update 
'  
' Do Data Validation and Error handling here
' Initialize objects for the above purpose
'
' When data is clean and no errors have been found
' do the actual update process of this Ad item. The following steps illustrate the logic used
' Step 1. Update creative table.
' Step 2. Update the creative_property_value table. 
'	* Here the existing items gets updated and new ones get added, so that when a user 
'   * goes back to the original Creative Type, the existing data gets filled in automatically
' Step 3.Update the target_group_xref table.
'   * Here we delete the old page groups in the target_group_xref table and insert the newly selected ones
' Step 4.  Update the page_group_xref table.
'   * Here we delete the old page groups in the page_group_xref table and insert the newly selected ones
' Step 5. Update the campaign_item table
' Step 6. Update the order_discount table
' Step 7. Update the order_discount_misc table
'******************************************************************************************************************
Sub UpdateDiscountItem()
	dim bSuccess, aPageSelect, aTargetSelect, sExprID, sTagForUpProcessing

	On Error Resume Next
	g_oConn.BeginTrans
 
	'**************************************************************************
	'1. Update the creative table.
	bSuccess = bUpdateCreative(L_DiscUnableToSave_ErrorMessage)
	if not bSuccess then exit sub

	'**************************************************************************
	'2. Update the creative_property_value table. 
	'	* Here the existing items gets updated and new ones get added, so that when a user 
	'	* goes back to the original Creative Type, the existing data gets filled in automatically	
	bSuccess = bUpdateCPValue(L_DiscUnableToSave_ErrorMessage)
	if not bSuccess then exit sub

	'**************************************************************************
	'3.Update the target_group_xref table.
	'  * Here we delete the old target groups in the target_group_xref table and insert the newly selected ones
	aTargetSelect = Split(g_dFormValues("TargetSelect"), "|", -1, 1)
	bSuccess = bUpdateTargetGroup(L_DiscUnableToSave_ErrorMessage, aTargetSelect)
	if not bSuccess then exit sub
 
	'**************************************************************************
	' 4.  Update the page_group_xref table.
	'  * Here we delete the old page groups in the page_group_xref table and insert the newly selected ones
	aPageSelect = Split(g_dFormValues("PageSelect"), "|", -1, 1)
	bSuccess = bUpdatePageGroup(L_DiscUnableToSave_ErrorMessage, aPageSelect)
	if not bSuccess then exit sub

	'********************************************************************************************************************************************
	'5. Update the campaign_item table
	bSuccess = bUpdateCampItem(L_DiscUnableToSave_ErrorMessage)
	if not bSuccess then exit sub

	'********************************************************************************************************************************************	
	'6. Update the order_discount table
	g_oCmd.CommandText = "sp_UpdateDiscount"
	g_oCmd.CommandType = AD_CMD_STORED_PROC

	'initialize parameters
	with g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("i_campitem_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_campitem_id")))
		.Append g_oCmd.CreateParameter("b_disc_expression_used_for_display", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_template", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_rank", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_rank")))
		.Append g_oCmd.CreateParameter("i_disc_limit", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_limit")))
		.Append g_oCmd.CreateParameter("b_disc_click_required", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("b_disc_disjoint_cond_award", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_cond_expr", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_cond_basis", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_cond_basis")))
		.Append g_oCmd.CreateParameter("mny_disc_cond_min", AD_CURRENCY, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_qty_cond_min", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_award_expr", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_award_max", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_offer_type", AD_INTEGER, AD_PARAM_INPUT )
		.Append g_oCmd.CreateParameter("mny_disc_offer_value", AD_CURRENCY, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("b_disc_order_level_discount", AD_BOOLEAN, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("u_disc_special_offer_type", AD_BSTR, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("u_disc_description", AD_BSTR, AD_PARAM_INPUT, Len(Trim(g_dFormValues("u_disc_description"))), Trim(g_dFormValues("u_disc_description")))
	
		'set the values for the params
		if g_dFormValues("DiscUsedForDisplay") = "addition" then
			.item("b_disc_expression_used_for_display").value = 1
		else
			.item("b_disc_expression_used_for_display").value = 0
		end if

		if IsNull(g_dFormValues("i_disc_template")) then
			.item("i_disc_template").value = 0
		else
			.item("b_disc_click_required").value = cLng(g_dFormValues("i_disc_template"))
		end if	

 		If Instr(g_dFormValues("b_disc_click_required"),"on") > 0 Then 
			.item("b_disc_click_required").value = 1
		Else
			.item("b_disc_click_required").value = 0		
		End If

		if g_dFormValues("disjoint_award_cond" ) = "condandaward" then
			.item("b_disc_disjoint_cond_award").value = 0
		else			'g_dFormValues("disjoint_award_cond" ) = "awardonly"
			.item("b_disc_disjoint_cond_award").value = 1
		end if

		'If there was new expressions created from product picker
		'Save new expressions to ExpressionStore
		sTagForUpProcessing = "NO"
		
		IF g_dFormValues("u_disc_cond_option") <> "" then
			'If award_option data has more options means you want to save the complete list
			If g_dFormValues("u_disc_award_option") <> "" then
				If Len(g_dFormValues("u_disc_cond_option")) < Len(g_dFormValues("u_disc_award_option")) then
				   g_dFormValues("u_disc_cond_option") = g_dFormValues("u_disc_award_option")
				End If
			End IF

			'aSetExpressions function split expression strings and save each 
			'param(selected expression, expression strings) returns selected items's exID
			sExprID = aSetExpressions(CStr(g_dFormValues("u_disc_cond_expr")), CStr(g_dFormValues("u_disc_cond_option")))
			sTagForUpProcessing = "YES"				
 		end if

		'Disc_cond_expr
		If g_dFormValues("i_disc_cond_type") <> "" Then
			If g_dFormValues("i_disc_cond_type") = 1 Then	 'Any Product = 0; Catalog Expression = 1; Free Shipping = 2;
				.item("i_disc_cond_expr").value = CLng(g_dFormValues("i_disc_cond_expr"))
			elseif g_dFormValues("i_disc_cond_type") = 2 Then 	
				'If sExprID is empty means no product picker selected made
				If sExprID = "" then
				    'If i_disc_cond_expr is not numeric means the con/award pick the same values
					IF IsNumeric(g_dFormValues("i_disc_cond_expr")) then
						.item("i_disc_cond_expr").value = CLng(g_dFormValues("i_disc_cond_expr"))
					Else
						.item("i_disc_cond_expr").value = aSetExprWhole(g_dFormValues("i_disc_cond_expr"))
					End If
				else
					.item("i_disc_cond_expr").value = CLng(sExprID)
					g_dFormValues("i_disc_cond_expr") = sExprID
					sExprID = ""
				end if
			else
				.item("i_disc_cond_expr").value = NULL
			end if
		Else
			.item("i_disc_cond_expr").value = NULL
		End if

		if g_dFormValues("i_disc_cond_basis") = 1 then	'Price = 1; Qty = 2
			If IsNumeric(g_dFormValues("i_disc_cond_amt")) then
				.item("mny_disc_cond_min").value = CDbl(g_dFormValues("i_disc_cond_amt"))
				.item("i_disc_qty_cond_min").value = NULL
			end if
		else
			if IsNumeric(g_dFormValues("i_disc_cond_amt")) then		
				.item("mny_disc_cond_min").value = NULL
				.item("i_disc_qty_cond_min").value = CLng(g_dFormValues("i_disc_cond_amt"))
			end if		
		End if			
		 
		'For no requirement checked do the follwoing business data-fill for component 
 		If g_dFormValues("b_no_requirement") = "1" Then 
			.item("i_disc_cond_expr").value = NULL
			.item("i_disc_cond_basis").value = 2
			.item("i_disc_qty_cond_min").value = 0
		End If 
 
		IF g_dFormValues("u_disc_award_option") <> "" then
			'aSetExpressions function split expression strings and save each 
			'param(selected expression, expression strings) returns selected items's exID
			
			'If cond already taken care of expression DB saving it just pull the appropriate expression from expression store.
			If sTagForUpProcessing <> "YES" then
				sExprID = aSetExpressions(CStr(g_dFormValues("u_disc_award_expr")), CStr(g_dFormValues("u_disc_award_option")))
			else
				sExprID = aSetExpr(CStr(g_dFormValues("u_disc_award_expr")))
			End If			
 		end if

		'Deleting options value for update
		g_dFormValues("u_disc_cond_option") = ""
		g_dFormValues("u_disc_award_option") = ""

		'Disc_award_expr 
		IF g_dFormValues("i_disc_award_type") <> "" then
			if g_dFormValues("i_disc_award_type") = 1 Then	 'Any Product = 0; Catalog Expression = 1; Product Picker = 2; Free Shipping = 3;
				.item("i_disc_award_expr").value = CLng(g_dFormValues("i_disc_award_expr"))
			elseif g_dFormValues("i_disc_award_type") = 2 Then 	
				'If sExprID is empty means no product picker selected made
				If 	sExprID = "" then
					'If the cond/award using the same product pickers
					IF IsNumeric(g_dFormValues("i_disc_award_expr")) then
						.item("i_disc_award_expr").value = CLng(g_dFormValues("i_disc_award_expr"))
					Else
						.item("i_disc_award_expr").value = aSetExprWhole(g_dFormValues("i_disc_award_expr"))
					End If
				else
					.item("i_disc_award_expr").value = CLng(sExprID)
					g_dFormValues("i_disc_award_expr") = sExprID
				end if
			else
				.item("i_disc_award_expr").value =  NULL
			end if
			
		else
			.item("i_disc_award_expr").value = NULL
		end if

		'Since i_disc_award_max field is text so ... fix the data type
		if UCase(g_dFormValues("i_disc_award_max")) = UCase(L_DiscAwardMaxDefault_Text)  then
			.item("i_disc_award_max").value = 0
		else
			.item("i_disc_award_max").value = CLng(g_dFormValues("i_disc_award_max"))
		end if
	
		.item("i_disc_offer_type").value = CLng(g_dFormValues("i_disc_offer_type"))	
		if IsNumeric(g_dFormValues("mny_disc_offer_value")) then
			.item("mny_disc_offer_value").value = g_dFormValues("mny_disc_offer_value")
		end if
	
		'if checkbox("b_freeshipping") = checked then
		'Any Product = 0; Catalog Expression = 1; Product picker =2; Free Shipping = 3;	
		if g_dFormValues("b_freeshipping") = "1" then  
			.item("i_disc_award_expr").value = NULL
			.item("i_disc_award_max").value = 0
			.item("i_disc_offer_type").value = 2
			.item("mny_disc_offer_value").value = 100
			.item("b_disc_order_level_discount").value = 1
			.item("u_disc_special_offer_type").Size = Len("Shipping_Discount") 
			.item("u_disc_special_offer_type").value = "Shipping_Discount"
		else
			.item("b_disc_order_level_discount").value = 0
			.item("u_disc_special_offer_type").value = NULL
		end if				
	end with

	g_oCmd.Execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, L_DiscUnableToSave_ErrorMessage
		exit sub
	End If

	CleanUpParameters(g_oCmd)
	
	'***********************************************************************************************************************	
	'Loading all newly selected targexpress items to database				
	'If g_dFormValues("SelDiscExpr") <> "" then
		LoadSelExprTODB g_dFormValues("SelDiscExpr"), CLng(g_dFormValues("i_disc_id"))
	'End If
	'**************************************************************************

	'7. Update records in order_discuont_misc table.
	g_oCmd.CommandText = "sp_UpdateDiscountMisc"
	g_oCmd.CommandType = AD_CMD_STORED_PROC
 
	'initialize parameters
	with g_oCmd.Parameters
		.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
		.Append g_oCmd.CreateParameter("i_disc_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dFormValues("i_disc_id")))
		.Append g_oCmd.CreateParameter("i_disc_cond_type", AD_INTEGER, AD_PARAM_INPUT)
		.Append g_oCmd.CreateParameter("i_disc_award_type", AD_INTEGER, AD_PARAM_INPUT)
 
		if g_dFormValues("b_no_requirement") = "1" then 'If b_no_requirement is checked
			.item("i_disc_cond_type").value = 3
		else
			If g_dFormValues("i_disc_cond_type") = "" then
				.item("i_disc_cond_type").value = NULL
			else
				.item("i_disc_cond_type").value =  g_dFormValues("i_disc_cond_type")
			end if
		end if

		'This is about saving flag in checkbox of freeshipping
		if g_dFormValues("b_freeshipping") = "1" then 'If b_freeshipping is checked
			.item("i_disc_award_type").value = 3
		else
			If g_dFormValues("i_disc_award_type") = "" then
				.item("i_disc_award_type").value = NULL
			else
				.item("i_disc_award_type").value =  g_dFormValues("i_disc_award_type")
			end if
		end if
	end with

	g_oCmd.execute
	If g_oConn.Errors.count > 0 Then
		RollBackAndReportError g_oConn, L_DiscUnableToSave_ErrorMessage
		exit sub
	End If

	CleanUpParameters(g_oCmd)

	if Err.number <> 0 then
		AddErrorNode g_xmlReturn, "1", L_DiscUnableToSave_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
	else
		g_oConn.CommitTrans	

		'set new camp id and creative id so they can be displayed
		AddItemNode "campitem_id", g_dFormValues("i_campitem_id")
		AddItemNode "creative_id", g_dFormValues("i_creative_id")
		AddItemNode "disc_id", g_dFormValues("i_disc_id")
		AddItemNode "disc_cond_option", g_dFormValues("u_disc_cond_option")
		AddItemNode "disc_award_option", g_dFormValues("u_disc_award_option")
		AddItemNode "disc_cond_expr", g_dFormValues("i_disc_cond_expr")
		AddItemNode "disc_award_expr", g_dFormValues("i_disc_award_expr")
	end if
End sub
%>
