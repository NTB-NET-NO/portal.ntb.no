<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="include/cm_util.asp" -->
<!--#INCLUDE FILE="include/marketingStrings.asp" -->

<%
Server.ScriptTimeout = 3 * 60
'page size for ListSheet
const PAGE_SIZE = 10
'task button indexes for the page
const TASK_ADD =				0
const TASK_VIEW_PROPERTIES =	1
const TASK_COPY =				2
const TASK_DELETE =				3
const TASK_RESTORE =			4
const TASK_REPORT =				5
const TASK_SEARCH =				6

dim g_sCampaignConnectionStr, g_dRequest, g_sXMLData

set g_dRequest = Request.form

GetPageGlobals()

'get connection string and put it in Session:
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

g_sXMLData = "<script language='xml' id='lsData'>" & GetListSheetDataIsland() & "</script>"

function sGetPostToArray(sType, nTaskIndex)
	'make code for an array of postto actions for task in nTaskIndex position
	Dim slPostTos, nIndex, sArray, sArrayName
	set slPostTos = g_dActionPage.tasks(nTaskIndex).postto
	sArrayName = "g_a" & sType & "Actions"
	sArray = "dim " & sArrayName & vbCr
	sArray = sArray & sArrayName & " = Array("""", "
	for nIndex = 0 to slPostTos.count - 1
		sArray = sArray & """" & slPostTos(nIndex).action & """"
		if nIndex < slPostTos.count - 1 then sArray = sArray & ", "
	next
	sGetPostToArray = sArray & ")" & vbCr
end function

function sGetListManagerEntries()
	'Check whether there are list manager entries present before bringing up
	'the direct mailer form	
	Dim oLM, rsListPrimary	
	Dim nfailed		
	on error resume next
	nfailed = 0
	Set oLM = Server.CreateObject("Commerce.ListManager")
	if Err.number <> 0 then
		setError "", sGetErrorByID("Cant_create_object"), sGetScriptError(Err), ERROR_ICON_ALERT
		exit function
	end if
	Err.clear
	oLM.Initialize g_sCampaignConnectionStr		
	if Err.number <> 0 then
		setError "", sGetErrorByID("Cant_connect_db"), sGetScriptError(Err), ERROR_ICON_ALERT
		exit function
	end if
	set rsListPrimary = oLM.GetLists
	If err.number <> 0 then
		setError "", sGetErrorByID("Cant_create_object"), sGetScriptError(Err), ERROR_ICON_ALERT
		exit function
	elseif (rsListPrimary.EOF) then
		Response.write "Msgbox """ & L_CMNeedListsForDM_Message & """, vbOKOnly, """ & L_CMNeedListsForDM_DialogTitle & """" & vbCr
		Response.write "ResettaskButtons()" & vbCr
		Response.write "exit sub" & vbCr
	end if
	
	Do while (rslistprimary.EOF <> True)
		If rsListPrimary("list_status")= 3 then
			nfailed = nfailed + 1	
		End If
		rslistprimary.movenext
	Loop
	if (rslistprimary.recordcount = nfailed ) then
		Response.write "Msgbox """ & L_CMNeedListsForDM_Message & """, vbOKOnly, """ & L_CMNeedListsForDM_DialogTitle & """" & vbCr
		Response.write "ResettaskButtons()" & vbCr
		Response.write "exit sub" & vbCr
	End If
	on error goto 0
end function

%>

<html>
<head>
	<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">
	<style>
		BUTTON		{WIDTH: 7em}
		DIV.esContainer, .esEditgroup DIV.esContainer
		{
		    BACKGROUND-COLOR: transparent;
		}

	</style>
	<script LANGUAGE="VBScript">
	<!--	
		option explicit

		const TYPE_CUST = "1"
		const TYPE_CAMP = "2"
		const TYPE_AD = "3"
		const TYPE_DM = "4"
		const TYPE_DISC= "5"

		const KEYCODE_ENTER	= 13

		dim g_bOpenEnable, g_bCopyEnable, g_bDeleteEnable, g_bRestoreEnable, g_sItemName, g_sStatus, g_sStatusText
		dim g_bResetingDate
		
		g_bResetingDate = false
		g_bOpenEnable = false
		g_bCopyEnable = false
		g_bDeleteEnable = false
		g_bRestoreEnable = false

<%		'asp code to create g_aOpenAction array of post actions for task routine below
%>		<%= sGetPostToArray("copy", TASK_COPY) %>
		<%= sGetPostToArray("Open", TASK_VIEW_PROPERTIES) %>

		Sub OnSelectRow()
			dim xmlSrcRecord, bIsCustomer, bIsCampaign, bIsCampItem, sType, sQueryID, _
				bIsCustomerDeleted, bIsCampaignDeleted, bIsCampItemDeleted
				
			set xmlSrcRecord = window.event.XMLrecord		

			bIsCustomer = cBool(xmlSrcRecord.selectNodes("customer_flag").length >= 1)
			bIsCampaign = cBool(xmlSrcRecord.selectNodes("camp_flag").length >= 1)
			bIsCampItem = cBool(xmlSrcRecord.selectNodes("campitem_flag").length >= 1)

			'make sure add and find always available
			EnableTask("new")
			EnableTask("find")
			EnableTask("report")
			if bIsCustomer or bIsCampaign or bIsCampItem then
				selectform.i_customer_id.value = xmlSrcRecord.selectSingleNode("i_customer_id").text
			end if

			if bIsCustomer then
				'here we have ourselves a Customer
				'stuff selectform
				with selectform
					.i_camp_id.value = -1
					.i_campitem_id.value = -1
					.campitem_type.value = ""
					.editor_type.value = TYPE_CUST 
				end with
				'save item name
				g_sItemName = xmlSrcRecord.selectSingleNode("u_customer_name").text
				'initially disable all items in add menu
				EnableAllTaskMenuItems "new", false
				'enable first two menu items in add menu
				EnableTaskMenuItem "new", 0
				EnableTaskMenuItem "new", 1
				SetDeleteRestoreBtns xmlSrcRecord, "dt_customer_archived"
				sType = "<%= L_CMTypeCustomerLabel_Text %>"
			elseif bIsCampaign then
				'check if parent customer is deleted
				sQueryID = xmlSrcRecord.selectSingleNode("bd_queryid").text
				selectform.delparentcust.value = cbool(lsCampaignmanager.xmlList.selectSingleNode("record[record/bd_queryid = '" & sQueryID & "' and dt_customer_archived = '']") is nothing)
				'here we have ourselves a Campaign
				'stuff selectform
				with selectform
					.i_camp_id.value = xmlSrcRecord.selectSingleNode("i_camp_id").text
					.i_campitem_id.value = -1
					.campitem_type.value = ""
					.editor_type.value = TYPE_CAMP 
				end with
				'save item name
				g_sItemName = xmlSrcRecord.selectSingleNode("u_camp_name").text					
				'enable all menu items in add menu except customer (1st item)
				EnableAllTaskMenuItems "new", true
				DisableTaskMenuItem "new", 0
				SetDeleteRestoreBtns xmlSrcRecord, "dt_camp_archived"
				sType = "<%= L_CMTypeCampaignLabel_Text %>"
			elseif bIsCampItem then
				'check if parent customer and campaign are deleted
				sQueryID = xmlSrcRecord.selectSingleNode("bd_queryid").text
				selectform.delparentcust.value = cbool(lsCampaignmanager.xmlList.selectSingleNode("record[record/record/bd_queryid = '" & sQueryID & "' and dt_customer_archived = '']") is nothing)
				selectform.delparentcamp.value = cbool(lsCampaignmanager.xmlList.selectSingleNode("record/record[record/bd_queryid = '" & sQueryID & "' and dt_camp_archived = '']") is nothing)
				'here we have a campaign item.
				'stuff selectform (other ontask routines stuff their forms from here)
				with selectform
					.i_camp_id.value = xmlSrcRecord.selectSingleNode("i_camp_id").text
					.i_campitem_id.value = xmlSrcRecord.selectSingleNode("i_campitem_id").text
					.campitem_type.value = xmlSrcRecord.selectSingleNode("campitem_type").text					
				end with
				'save item name
				g_sItemName = xmlSrcRecord.selectSingleNode("u_campitem_name").text
				select case selectform.campitem_type.value
				case "<%= GUID_TYPE_AD %>"
					'Ad type
					selectform.editor_type.value = TYPE_AD
					sType = "<%= L_CMTypeAdLabel_Text %>"
				case "<%= GUID_TYPE_DM %>"
					'Direct Mail type (no restore) 
					selectform.editor_type.value = TYPE_DM
					sType = "<%= L_CMTypeMailLabel_Text %>"
				case "<%= GUID_TYPE_DISC %>"
					'Discounts type
					selectform.editor_type.value = TYPE_DISC
					sType = "<%= L_CMTypeDiscountLabel_Text %>"
				End select
				SetDeleteRestoreBtns xmlSrcRecord, "dt_campitem_archived"
				EnableTask("copy")
				'enable all items except customer and campaign in add menu
				EnableAllTaskMenuItems "new", true
				DisableTaskMenuItem "new", 0
				DisableTaskMenuItem "new", 1
			end if

			if bIsCustomer or bIsCampaign or bIsCampItem then
				EnableTask("open")
				g_sStatusText = sFormatString("<%= L_BDSSXSelectedX_StatusBar %>", Array(sType, g_sItemName))
				SetStatusText(g_sStatusText)
			end if
			'save button state
			SaveButtonState()
	 	end Sub

		Sub SetDeleteRestoreBtns(xmlSrcRecord, sArchivedID)
			if not Len(xmlSrcRecord.selectSingleNode(sArchivedID).text) > 0 Then
				DisableTask("restore")
				EnableTask("delete")
			else
				DisableTask("delete")
				EnableTask("restore")
			End If	 
	 	end Sub

		Sub OnUnSelectRow()
			'set selectform values
			with selectform
				.editor_type.value = TYPE_CUST
				.i_customer_id.value = -1
				.i_camp_id.value = -1
				.i_campitem_id.value = -1
				.campitem_type.value = ""
			end with
			'clear item name
			g_sItemName = ""

			'disable all but first add menu items
			EnableAllTaskMenuItems "new", false
			EnableTaskMenuItem "new", 0

			'disable buttons
			DisableTask("open")
			DisableTask("restore")
			DisableTask("delete")
			DisableTask("copy")
			'save button state
			SaveButtonState()
			SetStatusText("<%= L_BDSSNothingSelected_Text %>")
		End Sub

		sub ClearButtonState()
			g_bOpenEnable = false
			g_bCopyEnable = false
			g_bDeleteEnable = false
			g_bRestoreEnable = false
		End Sub

		sub SaveButtonState()
			g_bOpenEnable = cBool(not elGetTaskBtn("open").disabled)
			g_bCopyEnable = cBool(not elGetTaskBtn("copy").disabled)
			g_bDeleteEnable = cBool(not elGetTaskBtn("delete").disabled)
			g_bRestoreEnable = cBool(not elGetTaskBtn("restore").disabled)
		End Sub

		Sub OnGroupOpen()
			dim xmlSrcRecord, xmlSubRecord, bIsCampaign
			set xmlSrcRecord = window.event.XMLrecord		

			bIsCampaign = cBool(xmlSrcRecord.selectNodes("camp_flag").length >= 1)
			set xmlSubRecord = xmlSrcRecord.selectSingleNode("record")
			if bIsCampaign and not xmlSubRecord is nothing then
				if xmlSubRecord.selectNodes("*[. != '']").length = 1 then
					filterform.i_camp_id.value = xmlSrcRecord.selectSingleNode("i_camp_id").text
					lsCampaignmanager.reload("sublist")
					xmlSrcRecord.removeChild(xmlSubRecord)
					doRollup(xmlSrcRecord.childNodes)
				end if
			end if
		End Sub

		sub doRollup(xmlRecords)
			dim xmlRecord, xmlParentRequests, xmlParentClicks, xmlParentClickPct, _
				xmlParentNode, xmlRequests, xmlClicks, xmlClickPct, xmlCustomerID, _
				nParentRequests, nParentClicks, nRequests, nClicks
			for each xmlRecord in xmlRecords
				Set xmlCustomerID = xmlRecord.selectSingleNode("i_customer_id")	
				If not xmlCustomerID is nothing then
					if Len(xmlCustomerID.text) > 0 then
						'get nodes
						set xmlRequests = xmlRecord.selectSingleNode("requests")
						set xmlClicks = xmlRecord.selectSingleNode("clicks")
						set xmlClickPct = xmlRecord.selectSingleNode("click_pct")
						'get parent nodes
						set xmlParentNode = xmlRecord.parentNode
						set xmlParentRequests = xmlParentNode.selectSingleNode("requests")
						set xmlParentClicks = xmlParentNode.selectSingleNode("clicks")
						set xmlParentClickPct = xmlParentNode.selectSingleNode("click_pct")
						'get values cast as longs
						nParentRequests = nGetLongFromNode(xmlParentRequests)
						nParentClicks = nGetLongFromNode(xmlParentClicks)
						nRequests = nGetLongFromNode(xmlRequests)
						nClicks = nGetLongFromNode(xmlClicks)
						'calculate new totals and percents
						xmlParentRequests.text = FormatNumber(nParentRequests + nRequests, 0, vbTrue, vbFalse, vbFalse)
						xmlParentClicks.text =  FormatNumber(nParentClicks + nClicks, 0, vbTrue, vbFalse, vbFalse)
						if xmlParentRequests.text > 0 then xmlParentClickPct.text = (xmlParentClicks.text / xmlParentRequests.text) * 100
						xmlParentClickPct.text = FormatNumber(xmlParentClickPct.text, 2)
						xmlRequests.text = FormatNumber(nRequests, 0, vbTrue, vbFalse, vbFalse)
						xmlClicks.text =  FormatNumber(nClicks, 0, vbTrue, vbFalse, vbFalse)
						if nRequests > 0 then xmlClickPct.text = (nClicks / nRequests) * 100
						xmlClickPct.text = FormatNumber(xmlClickPct.text, 2, vbTrue, vbFalse, vbFalse)
					end if
				end if
			next
		end sub

		function nGetLongFromNode(xmlNumberNode)
			if isNumeric(xmlNumberNode.text) then
				nGetLongFromNode = Fix(cDbl(xmlNumberNode.text))
			else
				nGetLongFromNode = 0
			end if
		end function

		Sub OnChangeFind()
			if g_bResetingDate then exit sub
			dim dtStart, dtEnd

			'show validation error if start after end
			'18931 - Campaign Manager: Filter date: error checking Same day 
			dtStart = dtGetDate(fromdate.value)
			dtEnd = dtGetDate(todate.value)
			if not fromdate.required and not todate.required and _
				dateDiff("d", dtStart, dtEnd) < 0 then
				msgbox "<%= L_CMStartDateBeforeEndDate_ErrorMessage %>", vbOKOnly, "<%= L_BDSSVAlidationErrorTitle_Text %>"
			end if

			'disable find button if invalid, missing or conflicting dates
			document.all.btnFindNow.disabled = cBool(not (fromdate.valid _
				and todate.valid and not fromdate.required and not todate.required) or _
				dateDiff("d", dtStart, dtEnd) < 0)
		end sub

		Sub OnFilter()
			dim nRecordCount

			lsCampaignmanager.page = 1
			lsCampaignmanager.reload("findby")
			SetFilterTextDisplay()
			nRecordCount = lsCampaignmanager.recordcount
			if nRecordCount <= 0 then
				g_sStatusText = "<%= sFormatString(L_BDSSNoXMatchThisSearch_StatusBar, array(L_CMTypeCustomersLabel_Text)) %>"
			elseif nRecordCount = 1 then
				g_sStatusText = "<%= sFormatString(L_BDSSOneXMatchThisSearch_StatusBar, array(L_CMTypeCustomerLabel_Text)) %>"
			else
				g_sStatusText =  sFormatString("<%= L_BDSSXXFoundMatchThisSearch_Text %>", array(nRecordCount, "<%= L_CMTypeCustomersLabel_Text %>"))
			end if
			SetStatusText(g_sStatusText)
			selectform.editor_type.value = -1
			ResetTaskButtons()
		End sub

		Sub OnFilterReset()
			dim dtStart, dtEnd
			dtStart = date() - 7
			dtEnd = date() + 14
			custname.value = ""
		g_bResetingDate = true
			fromdate.value = sGetFormattedDate(Year(dtStart), Month(dtStart), Day(dtStart))
			todate.value = sGetFormattedDate(Year(dtEnd), Month(dtEnd), Day(dtEnd))
		g_bResetingDate = false
			state.value = 1
			document.all.status.value = 2
			itemtype.value = 0
			SetFilterTextDisplay()
		End sub

		Sub onTaskAdd()
			dim sReturnValue, aReturnValue, nIndex, sURL, sType

			'copy values from select form
			CopySelectformElements(addform)

			'get the postto action from the menu choice
			addform.action = window.event.srcElement.id
			'get the index of the menu choice
			nIndex = window.event.srcElement.getAttribute("index")
		
			select case nIndex
				case TYPE_CUST
					'no dialog for new customer; just set values in form and submit
					with addform
						.i_customer_id.value = -1
						.i_camp_id.value = -1
						.i_campitem_id.value = -1
					end with
				case TYPE_CAMP
					addform.campitem_type.value = ""
				case TYPE_DM
					<% sGetListManagerEntries() %>
			End select		
			addform.Submit()
		End Sub
		
		Sub onTaskOpen()
			'copy values from select form
			CopySelectformElements(openform)

			'select the postto action that matches the editortype in the form
			openform.action = "../" & g_aOpenActions(cint(selectform.editor_type.value))
			
			openform.Submit()		
		End Sub

		Sub onTaskCopy()
			dim sURL, aReturn, sReturnValue, nIndex

			'copy values from select form
			CopySelectformElements(copyform)

			'select the postto action that matches the editortype in the form
			'  (2 items less in this array since can't copy customer or campaign)
			copyform.action = "../" & g_aCopyActions(cint(selectform.editor_type.value) - 2)
			sURL = "dlg_cmcopyitem.asp?type=" & selectform.editor_type.value & _
					"&i_customer_id=" & selectform.i_customer_id.value & _
					"&i_camp_id=" & selectform.i_camp_id.value
			sReturnValue = window.showModalDialog(sURL, ,"status:no;help:no")
				    
			if not isEmpty(sReturnValue) and sReturnValue <> "cancel" then
				'set form elements based on dialog return values
				aReturn = Split(sReturnValue, "|")
				with copyform
					.i_customer_id.value = aReturn(1)
					.i_camp_id.value = aReturn(2)
					.submit		
				end with
			else
				'if returned value is cancel, reenable buttons and abort
				ResetTaskButtons()
			end if
		End Sub

		Sub onTaskDelete()
			dim sReturnValue, bSuccess, sType

			'copy values from select form
			CopySelectformElements(deleteform)

			select case selectform.editor_type.value
				case TYPE_CUST
					sReturnValue = sPromptDelete("<%= L_BDSSDelete_Text %>", "<%= L_BDSSDeleteX_Message %>", "<%= L_CMTypeCustomerLabel_Text %>")
					sType = "<%= L_CMTypeCustomerLabel_Text %>"
				case TYPE_CAMP
					sReturnValue = sPromptDelete("<%= L_BDSSDelete_Text %>", "<%= L_BDSSDeleteX_Message %>", "<%= L_CMTypeCampaignLabel_Text %>")
					sType = "<%= L_CMTypeCampaignLabel_Text %>"
				case TYPE_AD
					sReturnValue = sPromptDelete("<%= L_BDSSDelete_Text %>", "<%= L_BDSSDeleteX_Message %>", "<%= L_CMTypeAdLabel_Text %>")
					sType = "<%= L_CMTypeAdLabel_Text %>"
				case TYPE_DM
					sReturnValue = sPromptDelete("<%= L_BDSSDelete_Text %>", "<%= L_BDSSDeleteX_Message %>", "<%= L_CMTypeMailLabel_Text %>")
					sType = "<%= L_CMTypeMailLabel_Text %>"
				case TYPE_DISC
					sReturnValue = sPromptDelete("<%= L_BDSSDelete_Text %>", "<%= L_BDSSDeleteX_Message %>", "<%= L_CMTypeDiscountLabel_Text %>")
					sType = "<%= L_CMTypeDiscountLabel_Text %>"
				case else			
			end select
			if sReturnValue = vbYes then
				bSuccess = lsCampaignmanager.reload("delete")
				SetFilterTextDisplay()
				if bSuccess then
					g_sStatusText = sFormatString("<%= L_BDSSDeletedXX_StatusBar %>", Array(sType, g_sItemName))
				else
					g_sStatusText = sFormatString("<%= L_BDSSUnableToDeleteXX_StatusBar %>", Array(sType, g_sItemName))
				end if
				SetStatusText(g_sStatusText)
				ClearButtonState()
			end if	
			'Enable buttons
			selectform.editor_type.value = -1
			ResetTaskButtons()
		End Sub
	 
		Sub onTaskRestore()
			dim sReturnValue, bSuccess, sType

			'copy values from select form
			CopySelectformElements(restoreform)

			select case selectform.editor_type.value
				case TYPE_CUST
					sReturnValue = sPromptDelete("<%= L_BDSSRestore_Text %>", "<%= L_BDSSRestoreX_Message %>", "<%= L_CMTypeCustomerLabel_Text %>")
					sType = "<%= L_CMTypeCustomerLabel_Text %>"
				case TYPE_CAMP
					if selectform.delparentcust.value then
						msgBox "<%= L_CMUnableToRestoreCustChild_ErrorMessage %>", vbOKOnly, "<%= L_CMUnableToRestoreCamp_DialogTitle %>"
						exit sub
					end if
					sReturnValue = sPromptDelete("<%= L_BDSSRestore_Text %>", "<%= L_BDSSRestoreX_Message %>", "<%= L_CMTypeCampaignLabel_Text %>")
					sType = "<%= L_CMTypeCampaignLabel_Text %>"
				case TYPE_AD
					if bParentOfCampItemDeleted() then exit sub
					sReturnValue = sPromptDelete("<%= L_BDSSRestore_Text %>", "<%= L_BDSSRestoreX_Message %>", "<%= L_CMTypeAdLabel_Text %>")
					sType = "<%= L_CMTypeAdLabel_Text %>"
				case TYPE_DM
					if bParentOfCampItemDeleted() then exit sub
					sReturnValue = sPromptDelete("<%= L_BDSSRestore_Text %>", "<%= L_BDSSRestoreX_Message %>", "<%= L_CMTypeMailLabel_Text %>")
					sType = "<%= L_CMTypeMailLabel_Text %>"
				case TYPE_DISC
					if bParentOfCampItemDeleted() then exit sub
					sReturnValue = sPromptDelete("<%= L_BDSSRestore_Text %>", "<%= L_BDSSRestoreX_Message %>", "<%= L_CMTypeDiscountLabel_Text %>")
					sType = "<%= L_CMTypeDiscountLabel_Text %>"
				case else
			end select
			if sReturnValue = vbYes then			
				bSuccess = lsCampaignmanager.reload("restore")
				SetFilterTextDisplay()
				if bSuccess then
					g_sStatusText = sFormatString("<%= L_BDSSRestoredXX_StatusBar %>", Array(sType, g_sItemName))
				else
					g_sStatusText = sFormatString("<%= L_BDSSUnableToRestoreXX_StatusBar %>", Array(sType, g_sItemName))
				end if
				SetStatusText(g_sStatusText)
				ClearButtonState()
			end if	
			'Enable buttons
			selectform.editor_type.value = -1
			ResetTaskButtons()
		End Sub	

		function bParentOfCampItemDeleted()
			bParentOfCampItemDeleted = false
			if selectform.delparentcust.value and selectform.delparentcamp.value then
				msgBox "<%= L_CMUnableToRestoreCustCampChild_ErrorMessage %>", vbOKOnly, "<%= L_CMUnableToRestoreCampItem_DialogTitle %>"
				bParentOfCampItemDeleted = true
			elseif selectform.delparentcust.value and not selectform.delparentcamp.value then
				msgBox "<%= L_CMUnableToRestoreCustGChild_ErrorMessage %>", vbOKOnly, "<%= L_CMUnableToRestoreCampItem_DialogTitle %>"
				bParentOfCampItemDeleted = true
			elseif not selectform.delparentcust.value and selectform.delparentcamp.value then
				msgBox "<%= L_CMUnableToRestoreCampChild_ErrorMessage %>", vbOKOnly, "<%= L_CMUnableToRestoreCampItem_DialogTitle %>"
				bParentOfCampItemDeleted = true
			end if
		End function	

		Sub ResetTaskButtons()
			EnableTask("new")
			EnableAllTaskMenuItems "new", false
			select case selectform.editor_type.value
				case "-1"		'no selection
					EnableTaskMenuItem "new", 0
				case TYPE_CUST	'customer selected
					EnableTaskMenuItem "new", 0
					EnableTaskMenuItem "new", 1
				case TYPE_CAMP	'campaign selected
					EnableAllTaskMenuItems "new", true
					DisableTaskMenuItem "new", 0
				case TYPE_AD, TYPE_DM, TYPE_DISC
					'campaign item selected
					EnableAllTaskMenuItems "new", true
					DisableTaskMenuItem "new", 0
					DisableTaskMenuItem "new", 1
			End select
			'reset enable state of task buttons after aborted task
			EnableTask("find")
			EnableTask("report")
			if g_bOpenEnable then EnableTask("open")
			if g_bCopyEnable then EnableTask("copy")
			if g_bDeleteEnable then EnableTask("delete")
			if g_bRestoreEnable then EnableTask("restore")
		End Sub	

		Sub CopySelectformElements(elForm)
			with elForm
				.i_customer_id.value =	selectform.i_customer_id.value
				.i_camp_id.value =		selectform.i_camp_id.value
				.i_campitem_id.value =	selectform.i_campitem_id.value
				.campitem_type.value =	selectform.campitem_type.value
			end with
		End Sub	
		
		function sPromptDelete(sTitle, sPrompt, sObject)
			sPrompt = sFormatString(sPrompt, Array(g_sItemName, sObject))
			sPromptDelete = Msgbox (sPrompt, VBYesNo, sTitle)
		End function	
			
		Sub SetFilterTextDisplay()
			filtertext.title = sFormatString("<%= L_CMCampaignsFilter_Tooltip %>", _
				Array(itemtype.displayvalue, custname.value, document.all.status.displayvalue, _
					state.displayvalue, fromdate.value, todate.value))
		End sub	

		sub onEnterSubmit()
			if window.event.keyCode = KEYCODE_ENTER and not filterform.btnFindNow.disabled then
				onFilter()
			end if
		end sub
			
		Sub Initialize()
			'enable tasks if they haven't been by onrowselect already
			if elGetTaskBtn("find").disabled then
				'Enable buttons
				EnableTask("new") 
				EnableTaskMenuItem "new", 0   
				EnableTask("find")
				EnableTask("report")	
			end if
			SetFindByHeight(170)
		End sub
	'-->
	</script>
</head>
<body LANGUAGE="VBScript" ONLOAD="Initialize">
<%
	InsertTaskBar L_CMCampaignManager_Text, ""
%>
<%= g_sXMLData %>

<!-- meta data -->
<xml id="searchMETA">
<editsheet>
    <global title='no'/>
	<fields>
	    <select id="itemtype">
		    <select id="itemtype">
				<option value="0"><%= L_CMTypeAllOption_Text %></option>
				<option value="1"><%= L_CMTypeAdOption_Text %></option>
				<option value="2"><%= L_CMTypeDirectMailOption_Text %></option>
				<option value="3"><%= L_CMTypeDiscountOption_Text %></option>
			</select>
	    </select>
		<text id="custname" maxlen='255' required="no" subtype="short">
	        <prompt><%= L_CMFieldPromptCustomerName_Text %></prompt>
	        <charmask>.*</charmask>
	        <error><%= L_CMFieldErrorCustomerName_Text %></error>
		</text>
		<date id="fromdate" required="yes" onchange="OnChangeFind" firstday='<%= g_sMSCSWeekStartDay %>'>
	        <format><%= g_sMSCSDateFormat %></format>
	    </date>
	    <date id="todate" required="yes" onchange="OnChangeFind" firstday='<%= g_sMSCSWeekStartDay %>'>
	        <format><%= g_sMSCSDateFormat %></format>
	    </date>
	    <select id="status">
		    <select id="status">
				<option value="0"><%= L_CMStatusActiveOption_Text %></option>
				<option value="2"><%= L_CMStatusAllOption_Text %></option>
		    </select>
	    </select>
	    <select id="state">
		    <select id="state">
				<option value="1"><%= L_CMDeletedNoOption_Text %></option>
				<option value="2"><%= L_CMDeletedBothOption_Text %></option>
		    </select>
	    </select>
	</fields>
    <template fields='itemtype custname fromdate todate status state'><![CDATA[
	<table width="100%" STYLE="TABLE-LAYOUT: fixed" LANGUAGE="VBScript" ONKEYPRESS="onEnterSubmit()">
		<colgroup><col width="20"><col><col><col width="20"><col><col><col width="30%"></colgroup>
		<tr TITLE="<%= L_CMFindTooltipItemTypeName_Text %>">
			<td></td>
			<td><%= L_CMFieldLabelItemtype_Text %></td>
			<td colspan="4">
				<div ID="itemtype"><%= L_BDSSLoadingField_Text %></div>
			</td> 
			<td>
				<button title="<%=L_CMFindTooltipFindByName_Text%>"id="btnFindNow" type="button" LANGUAGE="VBScript" ONCLICK="OnFilter()"><%= L_BDSSFindNow_Button %></button>
			</td>
		</tr>
		<tr TITLE="<%= L_CMFindTooltipCustomerName2_Text %>">
			<td></td>
			<td><%= L_CMFieldLabelCustomerName_Text %></td>
			<td colspan="4">
				<div ID="custname"><%= L_BDSSLoadingField_Text %></div>
			</td>
			<td>
				<button title="<%=L_CMFindTooltipResetName_Text%>"id="btnResetFind" type="button" LANGUAGE="VBScript" ONCLICK="OnFilterReset()"><%= L_BDSSReset_Button %></button>
			</td>
		</tr>
		<tr TITLE="<%= L_CMFindTooltipStatusName_Text %>">
			<td></td>
			<td><%= L_CMFieldLabelStatus_Text %></td>
			<td colspan="4">
				<div ID="status"><%= L_BDSSLoadingField_Text %></div>
			</td>
			<td></td>
		</tr>
		<tr TITLE="<%= L_CMFindTooltipDeletedName_Text %>">
			<td></td>
			<td><%= L_CMFieldLabelState_Text %></td>
			<td colspan="4">
				<div ID="state"><%= L_BDSSLoadingField_Text %></div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><%= g_imgRequired %></td>
			<td title="<%=L_CMFindTooltipFromName_Text%>"><%= L_CMFieldLabelFrom_Text %></td>
			<td>
				<div ID="fromdate"><%= L_BDSSLoadingField_Text %></div>
			</td>
			<td><%= g_imgRequired %></td>
			<td title="<%=L_CMFindTooltipToName_Text%>"><%= L_CMFieldLabelTo_Text %></td>
			<td>
				<div ID="todate"><%= L_BDSSLoadingField_Text %></div>
			</td>
			<td></td>
		</tr>
	</table>
    ]]></template>
</editsheet>
</xml>

<!-- Data for filling the search fields.  -->
<xml id="searchDATA">
	<document>
		<record>
			<custname><%= g_mscsPage.HTMLencode(g_sCustomerName) %></custname>
			<fromdate><%= g_sFromDate %></fromdate>
			<todate><%= g_sToDate %></todate>
			<status><%= g_sStatus %></status>
			<state><%= g_sState %></state>
			<itemtype><%= g_sItemType %></itemtype>
		</record>
	</document>
</xml>

<form action="response_cm.asp" method="post" id="filterform">
	<input type="hidden" id="i_camp_id" name="i_camp_id" value="-1">

	<div ID="bdfindbycontent" CLASS="findByContent">
		<div ID="esFind" class="editSheet"
			MetaXML="searchMETA" DataXML="searchDATA"></div>
	</div> 
</form>

<div id="bdcontentarea">	

<xml id="lsCampMgrMETA">
	<listsheet>
	    <global curpage="<%= g_nPage %>" pagesize="<%= PAGE_SIZE %>" sort="no" select="single" />
	    <columns>
	        <column width="50" id="u_customer_name" id2="u_camp_name" id3="u_campitem_name"><%= L_CMColHeadName_Text %></column> 
	        <column width="15" id="type_abr" id2="type_abr" id3="type_abr"><%= L_CMColHeadType_Text %></column>
	        <column width="30" id="Cfrom" id2="Cfrom" id3="Cfrom"><%= L_CMColHeadFrom_Text %></column>
	        <column width="30" id="Cto" id2="Cto" id3="Cto"><%= L_CMColHeadTo_Text %></column>
	        <column width="30" id="scheduled" id2="scheduled" id3="scheduled"><%= L_CMColHeadScheduled_Text %></column>
	        <column width="20" id="requests" id2="requests" id3="requests"><%= L_CMColHeadRequests_Text %></column>
	        <column width="20" id="clicks" id2="clicks" id3="clicks"><%= L_CMColHeadClicks_Text %></column>	  
	        <column width="20" id="click_pct" id2="click_pct" id3="click_pct"><%= L_CMColHeadClickPercent_Text %></column>	  
	        <column width="20" id="active" id2="active" id3="active"><%= L_CMColHeadStatus_Text %></column>	  
	    </columns>  
	    <operations>  
			<findby formid="filterform" />  
			<newpage formid="filterform" />  
			<sublist formid="filterform" />  
			<delete formid="deleteform" />  
			<restore formid="restoreform" />  
		</operations>  
	</listsheet>
</xml>

<div ID="filtertext" class="filtertext" style="MARGIN-TOP: 20px"><%= L_BDSSFilter_Text %></div>

<div ID="lsCampaignmanager" CLASS="listSheet" STYLE="MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px; HEIGHT: 80%" DataXML="lsData" MetaXML="lsCampMgrMETA" LANGUAGE="VBScript" OnRowSelect="OnSelectRow()" OnRowUnselect="OnUnSelectRow()" OnGroupOpen="OnGroupOpen()"><%= L_BDSSLoadingList_Text %></div>

<br>
<div style="text-indent:25px"><%= sFormatString(L_CMListKey_Text, array(L_CMTypeCustomerAbbr_Text, L_CMTypeCampaignAbbr_Text, L_CMTypeAdAbbr_Text, L_CMTypeMailAbbr_Text, L_CMTypeDiscountAbbr_Text)) %></div>

<p>

<form id="selectform">
	<input type="hidden" id="type" name="type" value="select">
	<input type="hidden" id="i_customer_id" name="i_customer_id" value="-1">
	<input type="hidden" id="i_camp_id" name="i_camp_id" value="-1">
	<input type="hidden" id="i_campitem_id" name="i_campitem_id" value="-1">
	<input type="hidden" id="campitem_type" name="campitem_type" value>
	<input type="hidden" id="editor_type" name="editor_type" value="1">
	<input type="hidden" id="delparentcust" name="delparentcust">
	<input type="hidden" id="delparentcamp" name="delparentcamp">
</form>

<form id="addform" ONTASK="onTaskAdd()">
	<input type="hidden" id="type" name="type" value="add">
	<input type="hidden" id="i_customer_id" name="i_customer_id">
	<input type="hidden" id="i_camp_id" name="i_camp_id">
	<input type="hidden" id="i_campitem_id" name="i_campitem_id">
	<input type="hidden" id="campitem_type" name="campitem_type">
</form>

<form id="openform" ONTASK="onTaskOpen()">
	<input type="hidden" id="type" name="type" value="open">
	<input type="hidden" id="i_customer_id" name="i_customer_id">
	<input type="hidden" id="i_camp_id" name="i_camp_id">
	<input type="hidden" id="i_campitem_id" name="i_campitem_id">
	<input type="hidden" id="campitem_type" name="campitem_type">
</form>

<form id="copyform" ONTASK="onTaskCopy()">
	<input type="hidden" id="type" name="type" value="copy">
	<input type="hidden" id="i_customer_id" name="i_customer_id">
	<input type="hidden" id="i_camp_id" name="i_camp_id">
	<input type="hidden" id="i_campitem_id" name="i_campitem_id">
	<input type="hidden" id="campitem_type" name="campitem_type">
</form>

<form id="deleteform" ONTASK="onTaskDelete()">
	<input type="hidden" id="i_customer_id" name="i_customer_id">
	<input type="hidden" id="i_camp_id" name="i_camp_id">
	<input type="hidden" id="i_campitem_id" name="i_campitem_id">
	<input type="hidden" id="campitem_type" name="campitem_type">
</form>

<form id="restoreform" ONTASK="onTaskRestore()">
	<input type="hidden" id="i_customer_id" name="i_customer_id">
	<input type="hidden" id="i_camp_id" name="i_camp_id">
	<input type="hidden" id="i_campitem_id" name="i_campitem_id">
	<input type="hidden" id="campitem_type" name="campitem_type">
</form>

</div>
</body>
</html>
