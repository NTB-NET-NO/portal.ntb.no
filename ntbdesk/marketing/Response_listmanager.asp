<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='include/marketingstrings.asp' -->
<!--#INCLUDE FILE='include/list_db.asp' -->
<%
on error resume next
const PAGE_SIZE = 20

dim g_sCampaignConnectionStr, g_dRequest, g_sXML, g_xmlReturn
dim g_nPage, g_nStart, g_sSortCol, g_sSortDir
dim g_nMailable, g_nGeneric, g_nNonWorking, g_nListCount

if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	

set g_dRequest = dGetRequestXMLAsDict()

g_nPage = g_dRequest("page")
if g_nPage <= 0 or g_nPage= "" then g_nPage = 1

g_sSortCol = g_dRequest("column")
if g_sSortCol = "" then
	g_sSortcol = "list_name"
end if

g_sSortDir = g_dRequest("direction")
if g_sSortDir = "" then
	g_sSortDir = "asc"
end if
g_sXML = sGetListXML()

if g_sXML = "" then
	Set g_xmlReturn = xmlGetXMLDOMDoc()
	AddErrorNode g_xmlReturn, "1", sGetErrorByID("Cant_connect_db"), ""
	'clear errors set in session
	Session("MSCSBDError") = empty
	g_sXML = g_xmlReturn.xml
end if
Response.write g_sXML
%>