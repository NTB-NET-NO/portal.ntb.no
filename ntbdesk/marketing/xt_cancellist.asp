<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<%	
dim oLM
dim sFriendlyError, sScriptError
dim g_sCampaignConnectionStr

on error resume next

if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

'create the ListManager object
Set oLM = Server.CreateObject("Commerce.ListManager")

'Initialie List Manager.Object
oLM.Initialize g_sCampaignConnectionStr 
if Err.number <> 0 then
	sFriendlyError = sGetErrorByID("Cant_connect_db")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
Else
	if 	Request.Form("list_recent_operid") = "" then
	'Basically this is the case we do not want to expect
	Else
		oLM.CancelOperation Cstr(Request.Form("list_recent_operid"))
		If err.Number <> 0 Then
			sFriendlyError = sGetErrorByID("Cancel_failed")
			sScriptError = sGetScriptError(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		End If
	End If
End if

Set oLM = Nothing
Response.Redirect "listmanager.asp?type=cancel&status=1"
%>	