<!-- #INCLUDE FILE='..\include\BDHeader.asp' -->
<!-- #INCLUDE FILE='..\include\BizDeskUtil.asp' -->
<!-- #INCLUDE FILE='include\marketingStrings.asp' -->
<%
Dim g_sCampaignConnectionStr, g_rsQuery, g_dFormValues, g_sQuery 
Dim g_sEventOptions, g_sCampaignXMLData, g_sAdItemXMLData, g_sTitle
'for initial display style of goaling fields
Dim g_sCampImpHide, g_sWeightReadOnly, g_sAdImpReadOnly, g_sGoalReadOnly

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

'get form values
Set g_dFormValues = dGetFormValues()

g_sEventOptions = sGetEventOptions()
g_sCampaignXMLData = sGetCampaignXMLData()
g_sAdItemXMLData = sGetAdItemXMLData()
g_sTitle = sGetTitle()

'all jobs are done
Set g_rsQuery = Nothing

'******************************************************************************
'	server side functions
'******************************************************************************
Function dGetFormValues()
	Dim dFormValues
	Set dFormValues = Server.CreateObject("Commerce.Dictionary")
	dFormValues("type") = Request.Form("type")
	dFormValues("i_customer_id") = Request.Form("i_customer_id")
	If dFormValues("type") = "add" Then
		dFormValues("i_camp_id") = -1
		dFormValues("dbaction") = "insert"
	Else 'open
		dFormValues("i_camp_id") = Request.Form("i_camp_id")
		dFormValues("dbaction") = "update"
	End If	
	Set dGetFormValues = dFormValues
End Function

Function sGetEventOptions()
	Dim sReturn, fdEventTypeID, fdEventName
	sReturn = ""
	g_sQuery = "SELECT i_event_type_id, u_event_description FROM event_type ORDER BY i_event_type_id"
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	With g_rsQuery
		Set fdEventTypeID = .Fields(0)
		Set fdEventName = .Fields(1)
		Do While Not .EOF
			sReturn = sReturn & "<option value='" & fdEventTypeID.Value & "'>" & _
				g_mscsPage.htmlEncode(fdEventName.Value) & "</option>"  
			.MoveNext
		Loop 
		.Close
	End With
	sGetEventOptions = sReturn
End Function

Function sGetCampaignXMLData()
	Dim xmlData, xmlNode
	g_sQuery = "SELECT cu.u_customer_name, ca.u_camp_name, ca.dt_camp_start, " & _
		"ca.dt_camp_end, CAST(ca.b_camp_active AS int) b_camp_active, ca.dt_camp_archived, ca.u_camp_comments, " & _
		"ca.i_event_type_id, CAST(ca.b_camp_level_goal AS int) b_camp_level_goal, ca.i_camp_events_sched " & _
		"FROM customer cu LEFT OUTER JOIN campaign ca ON " & _
		"cu.i_customer_id = ca.i_customer_id AND ca.i_camp_id = " & g_dFormValues("i_camp_id") & _
		" WHERE cu.i_customer_id = " & g_dFormValues("i_customer_id")
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	Set xmlData = xmlGetXMLFromRS(g_rsQuery)
	g_rsQuery.Close 
	With xmlData
		'properly display State field
		Set xmlNode = .selectSingleNode("//dt_camp_archived")
		If Len(xmlNode.text) = 0 Then
			xmlNode.text = L_CaUnarchived_Text 
		Else
			xmlNode.text = L_CaArchived_Text
		End If
		'set default values for new campaign 
		If g_dFormValues("type") = "add" Then
			.selectSingleNode("//b_camp_level_goal").text = 1 ' default to campaign goaling
			.selectSingleNode("//i_camp_events_sched").text = 0 ' default to zero impressions
		End If
		g_dFormValues("b_camp_level_goal") = .selectSingleNode("//b_camp_level_goal").text
		'set initial display style strings of goaling fields
		If g_dFormValues("b_camp_level_goal") Then 'goaling at campaign level
			g_sCampImpHide = "no"
			g_sWeightReadOnly = "no"
			g_sAdImpReadOnly = "yes"
		Else
			g_sCampImpHide = "yes"
			g_sWeightReadOnly = "yes"
			g_sAdImpReadOnly = "no"
		End If
		'we need this to display on title bar
		g_dFormValues("u_customer_name") = .selectSingleNode("//u_customer_name").text
		g_dFormValues("u_camp_name") = .selectSingleNode("//u_camp_name").text
		'need this for client side script
		g_dFormValues("i_camp_events_sched") = CLng(.selectSingleNode("//i_camp_events_sched").text)
		sGetCampaignXMLData = .xml
	End With
End Function

Function sGetAdItemXMLData()
	g_sGoalReadOnly = "no"
	Select Case g_dFormValues("type")
		Case "add"
			sGetAdItemXMLData = "<document><record/></document>"
		Case "open"
			'select undeleted active Paid Ads to list in dynamic table
			g_sQuery = "SELECT ci.i_campitem_id, ci.u_campitem_name, " &_ 
				"ci.dt_campitem_start, ci.dt_campitem_end, ai.i_aditem_id, " &_
			    "ai.i_aditem_time_of_day_start, ai.i_aditem_time_of_day_end, " &_
			    "ai.i_aditem_days_of_week_mask, ai.i_aditem_weight, " &_
			    "ai.i_aditem_events_scheduled, pt.i_perftot_served " &_
				"FROM campaign_item ci INNER JOIN " &_
			    "ad_item ai ON ci.i_campitem_id = ai.i_campitem_id AND " &_
			    "ci.i_camp_id = " & g_dFormValues("i_camp_id") & " AND ai.i_aditem_type = 1 AND " &_
			    "ci.dt_campitem_archived IS NULL AND " &_
			    "ci.b_campitem_active = 1 INNER JOIN " &_
			    "campaign c ON ci.i_camp_id = c.i_camp_id INNER JOIN " &_
			    "event_type et ON " &_
			    "c.i_event_type_id = et.i_event_type_id LEFT OUTER JOIN " &_
			    "performance_total pt ON " &_
			    "ci.i_campitem_id = pt.i_campitem_id AND " &_
			    "et.u_event_name = pt.u_perftot_event_name " &_
				"ORDER BY ci.i_campitem_id"
			Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
			sGetAdItemXMLData = xmlGetXMLFromRS(g_rsQuery).xml
			g_rsQuery.Close 
	End Select
End Function

Function sGetTitle()
	Dim sTitle
	'get taskbar title based on type 
	Select Case g_dFormValues("type")
		Case "add"
			sTitle = sFormatString(L_CaTaskBarNew_Text, Array(g_mscsPage.HTMLEncode(g_dFormValues("u_customer_name"))))
		Case "open"
			sTitle = sFormatString(L_CaTaskBar_Text, Array(g_mscsPage.HTMLEncode(g_dFormValues("u_customer_name")), g_dFormValues("u_camp_name")))
	End Select
	sGetTitle = sTitle
End Function
%>
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<SCRIPT LANGUAGE='VBScript'>
Option Explicit

Dim bFromOnTask, g_DateError

bFromOnTask = False

Sub window_onLoad()
	esMaster.focus()
End Sub

Sub ResetAdItemTable()
	Dim xmlDoc
	Set xmlDoc = dtAdItem.xmlList
	Do While xmlDoc.hasChildNodes
		xmlDoc.removeChild(xmlDoc.childNodes(0))	
	Loop
End Sub

Sub OnTask()
	Dim elSource, xmlDOMDoc, xmlErrorNodes, xmlWarnNodes, xmlCampID, elEditSheet, elBtnBack, xmlWarnNode
	'Check Date error if there is then go back to original status
	DateChange()
	If g_DateError then
 		g_DateError = false
		Enabletask "back"
		exit Sub		
	End If

	bFromOnTask = True

	Set elSource = window.event.srcElement
	With saveform
		If elSource Is elGetTaskBtn("savenew") Then
			.type.value = "add"
		Else 'click save or back or saveback
			.type.value = "open"
		End If
		.xmlAdItem.value = dtAdItem.xmlList.xml
	End With
	Set xmlDOMDoc = xmlPostFormViaXML(saveform)
	HideWaitDisplay()
	EnableTask("back")
	'successful save should return one item node and no errors
	Set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
	Set xmlWarnNodes = xmlDOMDoc.selectNodes("//WARNING")
	Set xmlCampID = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'camp_id']")
	If xmlErrorNodes.length <> 0 Or xmlDOMDoc.documentElement Is Nothing Or xmlCampID Is Nothing Then
		'report errors and set status text
		SetStatusText(sFormatString("<%= L_BDSSUnableToSaveX_StatusBar %>", Array(esMaster.field("u_camp_name").value)))
		ShowXMLNodeErrors(xmlErrorNodes)
		For Each elEditSheet in document.all.urns("Commerce.EditSheet")
			elEditSheet.resetDirty()
		Next
	Else
		'display warnings, if any
		if xmlWarnNodes.length <> 0 then
			for each xmlWarnNode in xmlWarnNodes
				showErrorDlg "", xmlWarnNode.text, "", ERROR_ICON_INFORMATION
			next
		end if
		SetStatusText(sFormatString("<%= L_BDSSSavedX_StatusBar %>", Array(esMaster.field("u_camp_name").value)))
		If elSource Is elGetTaskBtn("savenew") Then
			saveform.i_camp_id.value = -1
			saveform.dbaction.value = "insert"
			esCampGoal.resetDefault()
			esMaster.resetDefault()
			Call ResetAdItemTable 
			SetPageTitleText(sFormatString("<%= L_CaTaskBarNew_Text %>", Array(saveform.u_customer_name.value)))
		Else 'save or back or saveback
			saveform.i_camp_id.value = xmlCampID.getAttribute("value")
			saveform.dbaction.value = "update"
			esCampGoal.resetDirty()
			esMaster.resetDirty()
			SetPageTitleText(sFormatString("<%= L_CaTaskBar_Text %>", Array(saveform.u_customer_name.value, esMaster.field("u_camp_name").value)))
		End If
		g_bDirty = false
		if parent.g_bCloseAfterSave then
			parent.g_bCloseAfterSave = false
			' -- save and exit chosen and no errors while saving
			set elBtnBack = elGetTaskBtn("back")
			if not elBtnBack is nothing then
				if not elBtnBack.disabled then elBtnBack.click()
			end if
		end if
		parent.g_bCloseAfterSave = false
	End If
	esMaster.focus()
	'To force the disabling all icons except back
	disabletask "savenew"
	disabletask "saveback"
	disabletask "save"

	bFromOnTask = False
End Sub

Sub GoalLevelChange()
	Dim bCampLevelGoal, nEvents
	Dim xmlDoc, xmlWeights, xmlEvents
	Dim iRows, i
	Set xmlDoc = dtAdItem.xmlList
	Set xmlWeights = xmlDoc.selectNodes("//i_aditem_weight")
	Set xmlEvents = xmlDoc.selectNodes("//i_aditem_events_scheduled")
	bCampLevelGoal = esMaster.Field("b_camp_level_goal").Value
	'when user switches goaling level from Ad Item to Campaign,
	'we force each Ad Item's Weight to be the same value as Impression. 
	If bCampLevelGoal Then
	    iRows = xmlEvents.Length - 1
		nEvents = 0
		For i = 0 to iRows
			if clng(xmlEvents(i).text) > 0 then xmlWeights(i).text = xmlEvents(i).text
			nEvents = nEvents + clng(xmlEvents(i).text)
		Next
		esCampGoal.show("i_camp_events_sched")
		With esMaster
			.Field("i_camp_events_sched").value = nEvents
			.Field("i_aditem_weight").readonly = False
			.Field("i_aditem_events_scheduled").readonly = True
			'if not editing in DynamicTable then make sure weight stays disabled
			if .Field("i_aditem_events_scheduled").disabled and .Field("i_aditem_weight").disabled then
				.Field("i_aditem_weight").disabled = True
			end if
		End With
	Else
		esCampGoal.hide("i_camp_events_sched")
		With esMaster
			.Field("i_aditem_weight").readonly = True
			.Field("i_aditem_events_scheduled").readonly = False
			'if not editing in DynamicTable then make sure quantity stays disabled
			if .Field("i_aditem_events_scheduled").disabled and .Field("i_aditem_weight").disabled then
				.Field("i_aditem_events_scheduled").disabled = True
			end if
		End With
	End If
	esMaster.field("b_camp_level_goal").focus()
End Sub

Sub AdWeightOrImpChange()
	If esMaster.Field("i_aditem_weight").readonly = True Then 'the change is Impression
		AdImpChange()
	Else 'the change is Weight
		CampImpChange()	
	End If
End Sub

Sub AdImpChange()
    Dim xmlDoc, xmlEvents
    Dim iRows, i, iImpTotal 

	Set xmlDoc = dtAdItem.xmlList
	Set xmlEvents = xmlDoc.selectNodes("//i_aditem_events_scheduled")

    iRows = xmlEvents.Length - 1
	For i = 0 to iRows
		iImpTotal = iImpTotal + CLng(xmlEvents(i).text)
	Next
    esMaster.Field("i_camp_events_sched").value = iImpTotal
End Sub

Sub CampImpChange()
    Dim iServedTotal, iImpTotalScheduled, iImpTotalRemaining, tTotalRemaining
	Dim xmlDoc, xmlWeights, xmlEvents, xmlServed, xmlDStarts, xmlDEnds, xmlTStarts, xmlTEnds, xmlMasks
    Dim iRows, i, dtStart, dtEnd, atRemaining(), dtToday
	
	'This happens in Ad Level Goaling stage. If user modifies Ad Impressions, Campaign Impressions Ordered
	'is changed too. But that will in turn fire this event handler. To avoid that, stop it here. 
	If esMaster.hidden("i_camp_events_sched") Then Exit Sub

	Set xmlDoc = dtAdItem.xmlList
	If Not xmlDoc.hasChildNodes Then Exit Sub 'No Ad items, don't bother to calculate.
	With xmlDoc
		Set xmlWeights = .selectNodes("record/i_aditem_weight")
		Set xmlEvents =  .selectNodes("record/i_aditem_events_scheduled")
		Set xmlServed=   .selectNodes("record/i_perftot_served")
		Set xmlDStarts = .selectNodes("record/dt_campitem_start")
		Set xmlDEnds =   .selectNodes("record/dt_campitem_end")
		Set xmlTStarts = .selectNodes("record/i_aditem_time_of_day_start")
		Set xmlTEnds =   .selectNodes("record/i_aditem_time_of_day_end")
		Set xmlMasks =   .selectNodes("record/i_aditem_days_of_week_mask")
	End With
	iImpTotalScheduled = CLng(esMaster.Field("i_camp_events_sched").value)

    'calculate time remaining for items and sum time remaining and served
    iRows = xmlEvents.Length - 1
    iServedTotal = 0
    ReDim atRemaining(iRows)
	dtToday = DateSerial(Year(Date()), Month(Date()), Day(Date()))
    For i = 0 to iRows
		If xmlServed(i).text = "" Then xmlServed(i).text = 0
		dtStart = dtGetDate(xmlDStarts(i).text)
		dtEnd = dtGetDate(xmlDEnds(i).text)
		If dtToday <= dtStart Then 'this Ad has not started yet
			atRemaining(i) = iGetHoursInDateRange(dtStart, dtEnd, CInt(xmlTStarts(i).text), CInt(xmlTEnds(i).text), CInt(xmlMasks(i).text))
		ElseIf dtToday < dtEnd Then
			atRemaining(i) = iGetHoursInDateRange(Now(), dtEnd, CInt(xmlTStarts(i).text), CInt(xmlTEnds(i).text), CInt(xmlMasks(i).text))
		Else
			atRemaining(i) = 0
		End If	
		tTotalRemaining = tTotalRemaining + CLng(xmlWeights(i).text) * atRemaining(i)
        iServedTotal = iServedTotal + CLng(xmlServed(i).text)
    Next
    iImpTotalRemaining = iImpTotalScheduled - iServedTotal

    'force scheduled to be at least amount served
    If iImpTotalScheduled < iServedTotal And Not bFromOnTask Then
        MsgBox sFormatString("<%= L_CaQuantityOrderedLowerThanServed_Message %>", Array(esMaster.Field("i_camp_events_sched").value, iServedTotal)), vbExclamation, "<%= L_CaCampaign_Text %>" 
        esMaster.Field("i_camp_events_sched").value = iServedTotal
        Exit Sub
    End If

	'avoid dividing by zero
    If tTotalRemaining = 0 or iImpTotalRemaining = 0 Then 
		'no more time remaining or no more impressions remaining
		For i = 0 To iRows
			xmlEvents(i).text = CLng(xmlServed(i).text)
		Next
    else
		For i = 0 To iRows
		    xmlEvents(i).text = CLng(CLng(xmlServed(i).text) + ((CLng(xmlWeights(i).text) * atRemaining(i)) / tTotalRemaining) * iImpTotalRemaining)
		Next
    End If
End Sub	

Function iGetHoursInDateRange(dtStart, dtEnd, iTODStart, iTODEnd, iDOWMask)
    Dim aMask, iDays, iDOW, iIPD, i, aRunFor(), iTotal
    iTotal = 0
    
    ' Array of bitmasks for each day of the week (mon-sun)
	Const DAY_MON = &H0040
	Const DAY_TUE = &H0020
	Const DAY_WED = &H0010
	Const DAY_THU = &H0008
	Const DAY_FRI = &H0004
	Const DAY_SAT = &H0002
	Const DAY_SUN = &H0001
    aMask = Array(DAY_MON, DAY_TUE, DAY_WED, DAY_THU, DAY_FRI, DAY_SAT, DAY_SUN)
    
    ' Compute number of days in the date range (count both begin and end date)
    iDays = DateDiff("d", dtStart, dtEnd) + 1
    
    ' Compute the first day of the week
    iDOW = DatePart("w", dtStart, vbMonday) + (<%= g_sMSCSWeekStartDay %> - 1) - 1 ' convert for configured start day
    if iDOW > 6 then iDOW = iDOW - 7
    if iDOW < 0 then iDOW = iDOW + 7
    
    ' Compute the remains of today, if today is a run day, and the change happens after service started
    If (iDOWMask And aMask(iDOW)) And Date() >= dtStart Then
        iTotal = iGetHoursLeftInTimeRange(iTODStart, iTODEnd, DatePart("h", Now()))
    End If
    
    ' Compute number of intervals per day
    If (iTODEnd > iTODStart) Then
        iIPD = iTODEnd - iTODStart
    Else
        iIPD = 24 - (iTODStart - iTODEnd)
    End If

    ' Calculate array of intervals per day
    ReDim aRunFor(6)
    For i = 0 To 6
        If iDOWMask And aMask(i) Then
            aRunFor(i) = iIPD
        Else
            aRunFor(i) = 0
        End If
    Next

    ' Sum the number of intervals for each day in the range
    ' Today is calculated, so start from tomorrow
    For i = 1 To iDays
        iDOW = iDOW + 1
        If iDOW > 6 Then iDOW = 0
        iTotal = iTotal + aRunFor(iDOW)
    Next
    iGetHoursInDateRange = iTotal
End Function

Function iGetHoursLeftInTimeRange(iTODStart, iTODEnd, iHour)
    Dim iReturn
    If iTODStart < iTODEnd Then
        If iHour <= iTODStart Then
            iReturn = iTODEnd - iTODStart
        ElseIf iHour < iTODEnd Then
            iReturn = iTODEnd - iHour
        Else
            iReturn = 0
        End If
    ElseIf iTODStart = iTODEnd Then
        iReturn = 24 - iHour
    Else 'iTODStart > iTODEnd
        If iHour <= iTODEnd Then
            iReturn = (iTODEnd - iHour) + (24 - iTODStart)
        ElseIf iHour < iTODStart Then
            iReturn = 24 - iTODStart
        Else
            iReturn = 24 - iHour
        End If
    End If
    iGetHoursLeftInTimeRange = iReturn
End Function

Sub DateChange()
	Dim dtStart, dtEnd
	g_DateError = False
	If bFromOnTask Then Exit Sub
	'show validation error if start after end
	dtStart = dtGetDate(dt_camp_start.value)
	dtEnd = dtGetDate(dt_camp_end.value)
	If Not dt_camp_start.required And Not dt_camp_end.required And DateDiff("d", dtStart, dtEnd) < 0 Then
		msgbox "<%= L_CMStartDateBeforeEndDate_ErrorMessage %>", vbOKOnly, "<%= L_BDSSVAlidationErrorTitle_Text %>"
		g_DateError = True
		DisableTask("save")
		DisableTask("saveback")
		DisableTask("savenew")
	else
		ResetSaveBtns()
	End If
End Sub

</SCRIPT>
</HEAD>
<BODY>
<%
InsertEditTaskBar g_sTitle, ""
%>

<xml id='esCampGoalingMeta'>
	<editsheet>
		<global title='no' />
	    <fields>
			<select id='b_camp_level_goal' readonly='<%= g_sGoalReadOnly %>' default='1' onchange='GoalLevelChange()'>
				<name><%= L_CaSetGoalBy_Text %></name>
		        <tooltip><%= L_CaSetGoalBy_ToolTip %></tooltip>
			    <select>
					<option value='1'><%= L_CaSetGoalOptionCamp_Text %></option>
					<option value='0'><%= L_CaSetGoalOptionAd_Text %></option>
				</select>
	        </select>
			<numeric id='i_camp_events_sched' required='yes' hide='<%= g_sCampImpHide %>' min='0' max='2000000000' default='0' onchange='CampImpChange()'>				
				<name><%= L_CaQuantityOrdered_Text %></name>
			    <tooltip><%= L_CaQuantityOrdered_ToolTip %></tooltip>
	            <error><%= L_CaQuantityOrdered_ErrorMessage %></error>
			</numeric>				    
			<select id='i_event_type_id' default='1'>
				<name><%= L_CaScheduledBy_Text %></name>
		        <tooltip><%= L_CaScheduledBy_ToolTip %></tooltip>
			    <select>
					<%= g_sEventOptions %>
				</select>
	        </select>				                
		</fields>
	</editsheet>
</xml>

<xml id='dtAdItemMeta'>
	<dynamictable>
	    <global keycol='u_campitem_name' sortbykey='yes' newbutton='no' removebutton='no' />
		<fields>
			<numeric id='i_campitem_id' hide='yes' />
			<text id='u_campitem_name' readonly='yes' width='50'>
				<name><%= L_CaGoalTableColHeadItem_Text %></name>
			</text>			
			<numeric id='i_aditem_weight' required='yes' readonly='<%= g_sWeightReadOnly %>' min='0' max='2000000000' width='20'>
				<name><%= L_CaGoalTableColHeadWeight_Text %></name>
				<error><%= L_AdWeight_ErrorMessage %></error>
			</numeric>				    
			<numeric id='i_aditem_events_scheduled' required='yes' readonly='<%= g_sAdImpReadOnly %>' min='0' max='2000000000' width='30'>				
				<name><%= L_CaGoalTableColHeadQuantity_Text %></name>
				<error><%= L_CaQuantity_ErrorMessage %></error>
			</numeric>
		</fields>
	</dynamictable>
</xml>

<xml id='esMasterMeta'>
	<editsheets>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_CaCampaignProperty_Text %></name>
				<key><%= L_CaCampaignProperty_Accelerator %></key>
		    </global>
			<fields>
				<text id='u_camp_name' required='yes' maxlen='50'>
					<name><%= L_CaCampaignName_Text %></name>
					<tooltip><%= L_CaCampaignName_ToolTip %></tooltip>
			        <prompt><%= L_CaCampaignNamePrompt_Text %></prompt>
			        <error><%= L_CaCampaignName_ErrorMessage %></error>
			    </text>		
				<date id='dt_camp_start' required='yes' firstday='<%= g_sMSCSWeekStartDay %>' default='<%= g_MSCSDataFunctions.Date(Date(), g_MSCSDefaultLocale) %>' onchange='DateChange()'>
					<name><%= L_CaStartDate_Text %></name>
			        <tooltip><%= L_CaStartDate_ToolTip %></tooltip>
			        <error><%= L_CaStartDate_ErrorMessage %></error>
			        <format><%= g_sMSCSDateFormat %></format>
			    </date>
				<date id='dt_camp_end' required='yes' firstday='<%= g_sMSCSWeekStartDay %>' default='<%= g_MSCSDataFunctions.Date(DateAdd("m", 3, Date()), g_MSCSDefaultLocale) %>' onchange='DateChange()'>
					<name><%= L_CaEndDate_Text %></name>
			        <tooltip><%= L_CaEndDate_ToolTip %></tooltip>
			        <error><%= L_CaEndDate_ErrorMessage %></error>
			        <format><%= g_sMSCSDateFormat %></format>
				</date>	
				<select id='b_camp_active' default='1'>
					<name><%= L_CaStatus_Text %></name>
			        <tooltip><%= L_CaStatus_ToolTip %></tooltip>
				    <select>
						<option value='1'><%= L_CaStatusOptionActive_Text %></option>
						<option value='0'><%= L_CaStatusOptionInactive_Text %></option>
					</select>
			    </select>				                
				<text id='dt_camp_archived' readonly='yes' maxlen='50'>
					<name><%= L_CaState_Text %></name>
					<tooltip><%= L_CaState_ToolTip %></tooltip>
			    </text>		
				<text id='u_camp_comments' maxlen='255' subtype='long'>
					<name><%= L_CaComments_Text %></name>
					<tooltip><%= L_CaComments_ToolTip %></tooltip>
			        <error><%= L_CaComments_ErrorMessage %></error>
			    </text>				        
			</fields>
		</editsheet>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_CaCampaignGoaling_Text %></name>
				<key><%= L_CaCampaignGoaling_Accelerator %></key>
		    </global>
		    <template register='esCampGoal dtAdItem'><![CDATA[
				<DIV ID='esCampGoal' CLASS='editsheet'
					MetaXML='esCampGoalingMeta' DataXML='esCampaignData'>
				</DIV>		    				
				<DIV ID='dtAdItem' CLASS='dynamicTable' ONCHANGE='AdWeightOrImpChange()' 
					MetaXML='dtAdItemMeta' DataXML='dtAdItemData'>
				</DIV>
			]]></template>
		</editsheet>
	</editsheets>		
</xml>

<xml id='esCampaignData'>
	<?xml version='1.0' ?>
	<%= g_sCampaignXMLData %>
</xml>							

<xml id='dtAdItemData'>
	<?xml version='1.0' ?>
	<%= g_sADItemXMLData %>
</xml>

<DIV ID='bdContentArea'>

	<FORM ID='saveform' ACTION METHOD='post' ONTASK='OnTask()'>
		<INPUT TYPE='hidden' NAME='type'>
		<INPUT TYPE='hidden' NAME='u_customer_name' VALUE='<%= g_dFormValues("u_customer_name") %>'>
		<INPUT TYPE='hidden' NAME='i_customer_id' VALUE='<%= g_dFormValues("i_customer_id") %>'>
		<INPUT TYPE='hidden' NAME='i_camp_id' VALUE='<%= g_dFormValues("i_camp_id") %>'>
		<INPUT TYPE="hidden" NAME='dbaction' VALUE='<%= g_dFormValues("dbaction") %>'>
		<INPUT TYPE='hidden' NAME='xmlAdItem'>
		<DIV ID='esMaster' CLASS='editSheet'
			LANGUAGE='VBScript' ONVALID='SetValid("")' ONCHANGE='SetDirty("")' ONREQUIRE='SetRequired("")'
			MetaXML='esMasterMeta' DataXML='esCampaignData'><h3><%= g_imgLoadingAnimation %> <%= L_CaLoadingProperty_Text %></h3>
		</DIV>
	</FORM>
</DIV>

</BODY>
</HTML>
