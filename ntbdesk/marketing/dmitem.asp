<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../Include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='include/directmail.asp' -->
<!--#INCLUDE FILE='include/dmitem_db.asp' -->

<%
Dim g_sStatus, g_rsQuery, g_sQuery, g_aDaysOfWeekLabel, g_aDaysOfWeekValue, g_aDaysChecked
Dim g_bEdit, g_nCampID, g_nCampItemID, g_nDMItemID, g_sOpType, g_sDBAction
Dim g_sCustomerName, g_sCampName, g_sCampItemName
dim btndisabled
Dim g_sFrequencyType, g_sFrequencyInterval, g_sStartDate, g_sEndDate, _
	g_sStartTime, g_sRecurrenceFactor, g_sRelativeInterval, _
	oSQLServer, oJobServer, g_sJobName, g_sDefaultStartDate, g_sDefaultEndDate, _
	g_sRecurDailyInterval, g_sRecurWeeklyInterval, g_sRecurWeeklyDays, _
	g_sRecurMonthlyInterval, g_sRecurMonthlyDayOfMonth, _
	g_sRecurMonthlyRelInterval, g_sRecurMonthlyRelDayOfWeek, _
	g_sRecurMonthlyRelWeekType, g_sSunday, g_sMonday, g_sTuesday, g_sWednesday, _
	g_sThursday, g_sFriday, g_sSaturday	
Dim oDMdbConn, oDMCmd


Dim g_sArchived
g_sArchived = False
btndisabled = False
	
g_aDaysChecked = array(false, false, false, false, false, false, false)

g_aDaysOfWeekLabel = array(L_DMSunday_Text, L_DMMonday_Text, L_DMTuesday_Text, L_DMWednesday_Text, L_DMThursday_Text, L_DMFriday_Text, L_DMSaturday_Text)
g_aDaysOfWeekValue = array(SQLDMOWeek_Sunday, SQLDMOWeek_Monday, SQLDMOWeek_Tuesday, _
					SQLDMOWeek_Wednesday, SQLDMOWeek_Thursday, SQLDMOWeek_Friday, SQLDMOWeek_Saturday)

'determine whether to create a new record or edit an existing record
Dim g_sCampaignConnectionStr, g_sDMConnectionString
'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	
If IsEmpty(Session("DM_ConnectionStr")) Then
	Session("DM_ConnectionStr") = GetSiteConfigField("Direct Mail", "connstr_db_directmail")
End If
g_sDMConnectionString = Session("DM_ConnectionStr")	

Set oDMdbConn = oGetADOConnection(g_sDMConnectionString)
Set oDMCmd = oGetADOCommand(oDMdbConn, AD_CMD_TEXT)
if oDMdbConn.State = AD_STATE_CLOSED or not isEmpty(Session("MSCSBDError")) then
	Session("MSCSBDError") = empty
	setError "", L_DMResourceNotAvailable_ErrorMessage, "", ERROR_ICON_ALERT
	Response.Redirect "cmanager.asp"
end if

Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

'this is the key value passed in from the campaign manager
g_nCampItemID = Request("i_campitem_id")
g_nCampID = Request("i_camp_id")

g_sOpType = Request("type")


Dim g_sListID, g_sDesc, g_sFrom, g_sSubj, g_sReplyTo, g_sDiabledBT
Dim g_sSource, g_sLeaveMsgBlank, g_bPersonalized, g_sDefFormat, g_sScheduleID
Dim g_iMailingInterval, g_sAttachment, g_sTestListID, g_bErrorsInSave
dim sCUName, sCUFrom, sCUSubject, sCUReplyTo, sCUSource, sCUAttach, sCUListID, sCUDefFormat, _
	sCUDesc, sCUUserFlags, g_b_dmitem_personalized

'Retrieve posted form data and assign to variables
AssignGlobalVariables()

If Request("dbaction") = "insert" Then
	InsertDMItem()
ElseIf Request("dbaction") = "update" Then
	UpdateDMItem()
end if
g_oCmd.CommandType = AD_CMD_TEXT

g_sStatus = ""
g_bErrorsInSave = cbool(not isEmpty(Session("MSCSBDError")))
Select Case g_sOpType
	Case "add"
		g_bEdit = false
		g_nCampItemID = "-1"
		g_sOpType = "add"
		g_sDBAction = "insert"
	Case "savenew"
		If g_bErrorsInSave then
			'failed savenew is like original action
			If Request("dbaction") = "insert" Then
				g_bEdit = false
				g_nCampItemID = "-1"
				g_sOpType = "add"
				g_sDBAction = "insert"
			ElseIf Request("dbaction") = "update" Then
				g_bEdit = true
				g_sOpType = "open"
				g_sDBAction = "update"
			end if
		else
			g_bEdit = false
			g_sStatus = L_DMStatusSaved_Text
			g_nCampItemID = "-1"
			g_sOpType = "add"
			g_sDBAction = "insert"
		end if
	Case "copy"
		g_bEdit = true
		g_sStatus = ""
		g_sDBAction = "insert"
	Case "save"
		If g_bErrorsInSave then
			'failed save is like original action
			If Request("dbaction") = "insert" Then
				g_bEdit = false
				g_nCampItemID = "-1"
				g_sOpType = "add"
				g_sDBAction = "insert"
			ElseIf Request("dbaction") = "update" Then
				g_bEdit = true
				g_sOpType = "open"
				g_sDBAction = "update"
			end if
		else
			g_bEdit = true
			g_sStatus = L_DMStatusSaved_Text
			g_sOpType = "open"
			g_sDBAction = "update"
		end if
	Case "open"
		g_bEdit = true
		g_sStatus = ""
		g_sOpType = "open"
		g_sDBAction = "update"
End Select

'***********************************************************************
'Get globals from database
'***********************************************************************

'Use the different query for edit and new case	
if g_sOpType = "add" then
	g_sQuery = "SELECT cu.u_customer_name, ca.u_camp_name, ca.dt_camp_start, ca.dt_camp_end FROM Customer as cu, Campaign as ca " & _
				" WHERE ca.i_camp_id = " & g_nCampID & " AND cu.i_customer_id = ca.i_customer_id " 	
	g_oCmd.CommandText = g_sQuery
	g_rsQuery.Open g_oCmd, , AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY 
	if not g_rsQuery.EOF then
	   g_sCustomerName = g_rsQuery.Fields("u_customer_name").Value 
	   g_sCampName = g_rsQuery.Fields("u_camp_name").Value 
	   g_sCampItemName = "" 
	end if
	g_sDefaultStartDate = g_rsQuery.Fields("dt_camp_start")
	g_sDefaultEndDate = g_rsQuery.Fields("dt_camp_end")
else
	g_sCustomerName = Request("u_customer_name")
	g_sCampName = Request("u_camp_name")

	g_sQuery = " Select cu.u_customer_name, ca.u_camp_name, ci.u_campitem_name, cu.dt_customer_archived, ca.dt_camp_archived, ci.dt_campitem_archived FROM Customer AS cu, Campaign AS ca, " & _
				" campaign_item AS ci WHERE ca.i_camp_id = " & g_nCampID & " AND cu.i_customer_id = ca.i_customer_id " & _
				" AND ci.i_campitem_id = " & g_nCampItemID
	g_oCmd.CommandText = g_sQuery
	g_rsQuery.Open g_oCmd, , AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY 
	if not g_rsQuery.EOF then
	   g_sCustomerName = g_rsQuery.Fields("u_customer_name").Value 
	   g_sCampName = g_rsQuery.Fields("u_camp_name").Value 
	   g_sCampItemName = g_rsQuery.Fields("u_campitem_name").Value 
	end if
	'This is toggling for test button and sql job
	g_sDiabledBT = false

	If Not(ISNull(g_rsQuery.Fields("dt_customer_archived").Value)) or Not(ISNull(g_rsQuery.Fields("dt_camp_archived").Value)) or Not(ISNull(g_rsQuery.Fields("dt_campitem_archived").Value)) Then
		g_sArchived = True
		'g_sDiabledBT = "DISABLED"
		btndisabled = True
		g_sDiabledBT = True
	End If
	g_rsQuery.close()

	g_sQuery = "SELECT i_dmitem_id FROM dm_item WHERE i_campitem_id = " & g_nCampItemID
	g_oCmd.CommandText = g_sQuery
	g_rsQuery.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
	If Not g_rsQuery.EOF Then g_nDMItemID = cStr(g_rsQuery("i_dmitem_id"))
	g_rsQuery.Close 

	'Default start and end dates to campaign start and end dates.
	g_sQuery = "SELECT dt_camp_start,dt_camp_end FROM campaign WHERE i_camp_id = " & g_nCampID
	g_oCmd.CommandText = g_sQuery
	g_rsQuery.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
	If Not g_rsQuery.EOF Then 
		g_sDefaultStartDate = cStr(g_rsQuery("dt_camp_start"))
		g_sDefaultEndDate	 = cStr(g_rsQuery("dt_camp_end"))
	End If 
end if
g_rsQuery.Close 

'Retrieve scheduling information
RetrieveScheduleInfo()

if g_bErrorsInSave then
	'if errors then use posted values
	sCUName = request("u_campitem_name")
	sCUFrom = request("u_dmitem_from")
	sCUSubject = request("u_dmitem_subj")
	sCUReplyTo = request("u_dmitem_replyto")
	sCUSource = request("u_dmitem_source")
	sCUAttach = request("u_dmitem_attachment")
	sCUListID = request("g_dmitem_listid")
	sCUDefFormat = request("l_dmitem_defformat")
	sCUDesc = request("u_dmitem_desc")
	sCUUserFlags = request("i_dmitem_userflags")
else
	if g_sOpType = "add" or g_sOpType = "copy" then
		g_sQuery = "SELECT u_campitem_name='', "
	else
		g_sQuery = "SELECT ci.u_campitem_name, "
	end if
	g_sQuery = g_sQuery & " di.u_dmitem_from, di.u_dmitem_subj, di.u_dmitem_replyto, " & _
				" di.u_dmitem_source, di.u_dmitem_attachment, di.g_dmitem_listid, " & _
				" di.b_dmitem_personalized,di.l_dmitem_defformat, di.u_dmitem_desc, di.i_dmitem_userflags " & _
				" FROM dm_item di, campaign_item ci " & _
				" WHERE di.i_campitem_id = ci.i_campitem_id AND ci.i_campitem_id = " & g_nCampItemID
	Set rsQuery = rsGetRecordset(g_sCampaignConnectionStr, g_sQuery, AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY)
	'Converting boolean to int
	If (rsQuery("b_dmitem_personalized") = True) Then
		g_b_dmitem_personalized = "1"
	Else
		g_b_dmitem_personalized = "0"
	End If   
	sCUName = trim(rsQuery("u_campitem_name").value)
	sCUFrom = trim(rsQuery("u_dmitem_from").value)
	sCUSubject = trim(rsQuery("u_dmitem_subj").value)
	sCUReplyTo = trim(rsQuery("u_dmitem_replyto").value)
	sCUSource = trim(rsQuery("u_dmitem_source").value)
	sCUAttach = trim(rsQuery("u_dmitem_attachment").value)
	sCUListID = trim(rsQuery("g_dmitem_listid").value)
	sCUDefFormat = trim(rsQuery("l_dmitem_defformat").value)
	if sCUDefFormat = "" then sCUDefFormat = 1
	sCUDesc = trim(rsQuery("u_dmitem_desc").value)
	sCUUserFlags = trim(rsQuery("i_dmitem_userflags").value)
	Set rsQuery = nothing
End If   

Sub RetrieveScheduleInfo()
	On Error Resume Next
	
	Dim sJobScheduleName
	if g_nCampItemID = -1 then'In case of new
		exit sub
	end if

	g_sJobName = sGetJobName(g_nCampItemID, g_MSCSSiteName)
	sJobScheduleName = sGetScheduleName(g_sJobName)

	'Code block uses sql agent stored procedure sp_help_job_schedule.
	oDMCmd.CommandType = AD_CMD_STORED_PROC
	oDMCmd.CommandText = "msdb.dbo.sp_help_jobschedule" 

	With oDMCmd.Parameters			
		.Append oDMCmd.CreateParameter("job_id", AD_GUID, AD_PARAM_INPUT, , null)
		.Append oDMCmd.CreateParameter("job_name", AD_BSTR, AD_PARAM_INPUT, Len(g_sJobName), g_sJobName)
		.Append oDMCmd.CreateParameter("schedule_name", AD_BSTR, AD_PARAM_INPUT, Len(sJobScheduleName), sJobScheduleName)
		.Append oDMCmd.CreateParameter("schedule_id", AD_INTEGER, AD_PARAM_INPUT, , null)
	end with

	g_rsQuery.Open oDMCmd, , ,AD_LOCK_READ_ONLY,4
	if not g_rsQuery.EOF then
		g_sFrequencyType = g_rsQuery.Fields("freq_type").Value 
		g_sFrequencyInterval = g_rsQuery.Fields("freq_interval").Value 
		g_sStartDate = sDMODateToScreenDate(g_rsQuery.Fields("active_start_date").Value)
		g_sEndDate = sDMODateToScreenDate(g_rsQuery.Fields("active_end_date").Value)
		g_sStartTime = sDMOTimeToScreenTime(g_rsQuery.Fields("active_start_time").Value)
		g_sRecurrenceFactor = g_rsQuery.Fields("freq_recurrence_factor").Value 
		g_sRelativeInterval = g_rsQuery.Fields("freq_relative_interval").Value 
	end if	'End of code block.
	If CInt(g_sFrequencyType) <= 1 Then
		g_sEndDate = sStartDate
	End If 	
	if g_sFrequencyType = "" then g_sFrequencyType = "1"
	if g_sStartTime = "" then g_sStartTime = "0800"

	g_sRecurDailyInterval = "1"
	g_sRecurWeeklyInterval = "1"
	g_sRecurWeeklyDays = "0"
	g_sRecurMonthlyInterval = "1"
	g_sRecurMonthlyDayOfMonth = "1"
	g_sRecurMonthlyRelInterval = "1"
	g_sRecurMonthlyRelDayOfWeek = "1"
	g_sRecurMonthlyRelWeekType = "1"

	Select Case CInt(g_sFrequencyType)
		Case SQLDMOFreq_Daily
			g_sRecurDailyInterval = g_sFrequencyInterval
		Case SQLDMOFreq_Weekly
			g_sRecurWeeklyInterval = g_sRecurrenceFactor
			'Assign weekday values
			g_sRecurWeeklyDays = g_sFrequencyInterval
			checkDays(g_sRecurWeeklyDays)
		Case SQLDMOFreq_Monthly
			g_sRecurMonthlyInterval = g_sRecurrenceFactor
			g_sRecurMonthlyDayOfMonth = g_sFrequencyInterval
		Case SQLDMOFreq_MonthlyRelative
			g_sRecurMonthlyRelInterval = g_sRecurrenceFactor
			g_sRecurMonthlyRelDayOfWeek = g_sFrequencyInterval
			g_sRecurMonthlyRelWeekType = g_sRelativeInterval
	End Select
End Sub

Sub checkDays(ByRef dayInt)
	dim i
	for i = 6 to 0 step -1
		If dayInt >= g_aDaysOfWeekValue(i) Then
			g_aDaysChecked(i) = true
			dayInt = dayInt - g_aDaysOfWeekValue(i)
		End If
	next
End Sub
%>

<html>
<head>
	<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">
<script LANGUAGE="VBScript">
	public g_sInterval

	g_sInterval = <%=g_sFrequencyType %>
	
	Sub OnChangeDate()
	on error resume next
		if txtenddate.value <> "" and txtstartdate.value <> "" then
			if txtenddate.disabled = False Then
				If dtGetDate(txtenddate.value) < dtGetDate(txtstartdate.value) Then
					Msgbox "<%=L_DMStartDateLarge_ErrorMessage%>",vbOKOnly,"<%=L_DirectMail_DialogTitle%>"
				End If
			End if	
		End If
		If txtenddate.disabled = True then
			If dtgetdate(txtstartdate.value) < dtgetdate(Now) Then
				Msgbox "<%=L_StartDatePast_ErrorMessage%>",vbOKOnly,"<%=L_DirectMail_DialogTitle%>"
				txtstartdate.value = ""
			End If
			txtenddate.value = txtstartdate.value
		End If	
	End Sub

	sub OnChangeFrom()
	on error resume next
		with esMaster
			.field("u_dmitem_replyto").value = .field("u_dmitem_from").value
		end with
	end sub

	sub OnChangeInterval()
	on error resume next
		ApplyStyle g_sInterval, "none"
		g_sInterval = esSchedule.field("txtMailingInterval").value
		ApplyStyle g_sInterval, "block"
		If g_sInterval = "1" Then
			txtenddate.value = txtstartdate.value
			txtenddate.disabled = True
		Else
			txtenddate.disabled = False
		End If
	end sub

	sub OnChangeTest()
	on error resume next
	<% If not g_sDiabledBT Then %>
		 If (g_dmitem_testlistid.value <> "0") and not g_brequired and g_bvalid and (g_dmitem_listid.value <> "") Then
		    dmItemForm.testButton.disabled = False
		 Else
		    dmItemForm.testButton.disabled = True
		 End If
		 If (g_dmitem_listid.Value <> "")  Then
		    setDirty("")		
		 End If
	  <% End if %>		
	end sub

	sub ApplyStyle(ByVal sInterval, ByVal sStyle)
	on error resume next
		dim oItem
		select case sInterval
			case "4"
				set oItem = bd_cmdm_recur_day
			case "8"
				set oItem = bd_cmdm_recur_week
			case "16"
				set oItem = row1
			case "32"
				set oItem = row2
		end select

		oItem.style.display = sStyle
	end sub

	sub onClickTestBtn()	
	on error resume next
		dim iMsgReturn
		if esMaster.dirty then
			'changes have been made but not saved 
			iMsgReturn = msgbox("<%= L_DMSaveChanges_Message %>", vbYesNo, "<%= L_DirectMail_DialogTitle %>")
		else
			iMsgReturn = msgbox("<%= L_DMSendMail_Message %>", vbYesNo, "<%= L_DirectMail_DialogTitle %>")
		End If
		If iMsgReturn = vbYes and bValidateData() = True then
			dmItemForm.test_mail_flag.value = 1
			'set unloading flag so window unload message not displayed
			g_bUnloading = true 
			dmItemForm.submit()
		End If
	end sub

	sub u_dmitem_desc_onClick
	on error resume next
		recurrence_table.style.display = "block"
	end sub
	
	sub u_campitem_name_onChange()
	on error resume next
		ESTest.style.display = "none"
	end sub

	Sub setTestButtonState()
	on error resume next
	<%If not g_sDiabledBT Then %>
		if window.event.srcElement.readystate = "complete" then
			if not g_bRequired and g_bValid and (g_dmitem_testlistid.value <> "0") and (g_dmitem_listid.Value <> "") then
				' -- dirty and nothing required or invalid so enable preview
				dmItemForm.testButton.disabled = false
			else
				' -- not dirty or something required or invalid so disable preview
				dmItemForm.testButton.disabled = true
			end if
		End if
	<% end if %>
	end sub

	Sub onValidMasterES()
		setValid("")
		setTestButtonState()
	end sub

	Sub onRequireMasterES()
		setRequired("")
		setTestButtonState()
	end sub

	Sub onChangeMasterES()
		setDirty("")
		setTestButtonState()
	End Sub

	Sub OnTaskSave()
	on error resume next
		if window.event.srcElement Is elGetTaskBtn("savenew") then
			dmItemForm.type.value = "savenew"
		end if	

		if (bValidateData() = True) then
			dmItemForm.submit()
		else
			enableTask "back"
			enableTask "saveback"
			enableTask "save"
			enableTask "savenew"
		End If
	End Sub

	Function Quote(sText)
		Quote = replace(Quote, "'", "''")
	End Function

	Function  bValidateData()
	on error resume next	
		bValidateData = True
		if txtenddate.value <> "" and txtstartdate.value <> "" then
			if txtenddate.disabled = False Then
				If dtGetDate(txtenddate.value) < dtGetDate(txtstartdate.value) Then
					Msgbox "<%= L_DMStartDateLarge_ErrorMessage %>",vbOKOnly,"<%= L_DirectMail_DialogTitle %>"
					bvalidatedata = False
				End If
			End if	
		End If
		'if u_dmitem_source.value = "" then
		'	Msgbox "<%= L_DMMessageEmpty_ErrorMessage %>",vbOKOnly,"<%= L_DirectMail_DialogTitle %>"
		'	bvalidatedata = False
		'end if
		select case esSchedule.Field("txtMailingInterval").Value
			case "<%= SQLDMOFreq_Daily %>"
				if Trim(txtDays.Value) = "" Then
					Msgbox "<%= L_DMDaysFieldEmpty_ErrorMessage %>", vbOKOnly, "<%= L_DirectMail_DialogTitle %>"
					bValidateData = FALSE
					Exit Function
				End If
			case "<%= SQLDMOFreq_Weekly %>"
				if Trim(txtWeeks.Value) = "" Then
					Msgbox "<%= L_DMWeeksFieldEmpty_ErrorMessage %>", vbOKOnly, "<%= L_DirectMail_DialogTitle %>"
					bValidateData = FALSE
					Exit Function
				End If 

				If (not dmItemForm.chkWeekDay1.checked and _
					not dmItemForm.chkWeekDay2.checked and  _
					not dmItemForm.chkWeekDay3.checked and  _
					not dmItemForm.chkWeekDay4.checked and _
					not dmItemForm.chkWeekDay5.checked and  _
					not dmItemForm.chkWeekDay6.checked and _
					not dmItemForm.chkWeekDay7.checked) Then

					Msgbox "<%= L_DMWeekdaySelect_ErrorMessage %>", vbOKOnly, "<%= L_DirectMail_DialogTitle %>"
					bValidateData = FALSE
					Exit Function
				End If
			case "<%= SQLDMOFreq_Monthly %>"
				if Trim(txtDayOfMonth.Value) = "" Then
					Msgbox "<%= L_DMDayEmpty_ErrorMessage %>", vbOKOnly, "<%= L_DirectMail_DialogTitle %>"
					bValidateData = FALSE
					Exit  Function
				End If

				If Trim(txtMonthSpan.Value) = "" Then
					Msgbox "<%= L_DMSpanEmpty_ErrorMessage %>", vbOKOnly, "<%= L_DirectMail_DialogTitle %>"
					bValidateData = FALSE
					Exit  Function
				End If
			case "<%= SQLDMOFreq_MonthlyRelative %>"
				if Trim(relRecur.Value) = "" Then
					Msgbox "<%= L_DMMonthEmpty_ErrorMessage %>",vbOKOnly,"<%= L_DirectMail_DialogTitle %>"
					bValidateData = FALSE
					Exit Function		
				End If
		End select
	End Function

	sub window_onload()
		esMaster.focus()
	end sub
</script>
</head>

<body SCROLL="no">

<%
'INSERTING TASKBAR
select case g_sOpType
	case "add", "copy"
		InsertEditTaskBar sFormatString(L_DMCampItemPageTitleNew_Text, Array(g_mscsPage.HTMLEncode(g_sCustomerName), g_sCampName)), g_sStatus
	case "open"
		InsertEditTaskBar sFormatString(L_DMCampItemPageTitle_Text, Array(g_mscsPage.HTMLEncode(g_sCustomerName), g_mscsPage.HTMLEncode(g_sCampName), g_sCampItemName, g_nCampItemID)), g_sStatus
end select
%>


<!-- note the image group now uses a template and the meta xml is 	truncated after the global information -->
<xml id="DMScheduleMeta">
	<editsheet>
	    <global title="no"/>
	    <fields>
        	<date id="txtstartdate" required="yes" firstday='<%= g_sMSCSWeekStartDay %>' onchange="OnChangeDate()" default='<%= g_MSCSDataFunctions.date(g_sDefaultStartDate, g_MSCSDefaultLocale) %>'>
        	    <name><%= L_DMStartDate_Text %></name>
	            <tooltip><%= L_DMStartDate_Tooltip %></tooltip>
	            <error><%= L_DMStartDate_ErrorMessage %></error>
	            <format><%= g_sMSCSDateFormat %></format>
	        </date>
        	<date id="txtenddate" <%if CInt(g_sFrequencyType) = 1 then %> disabled="yes"<% end if %> firstday='<%= g_sMSCSWeekStartDay %>' onchange="OnChangeDate()" default='<%= g_MSCSDataFunctions.date(g_sDefaultEndDate, g_MSCSDefaultLocale) %>'>
	            <name><%= L_DMEndDate_Text %></name>
	            <tooltip><%= L_DMEndDate_Tooltip %></tooltip>
	            <error><%= L_DMEndDate_ErrorMessage %></error>
	            <format><%= g_sMSCSDateFormat %></format>
	           </date>
		    <select id="txtstarttime" default='<%= g_sStartTime %>'>
	            <name><%= L_DMStartTime_Text %></name>
	            <tooltip><%= L_DMStartTime_Tooltip %></tooltip>
				<select>
					<option value="0000">00:00</option>
					<option value="0100">01:00</option>
					<option value="0200">02:00</option>
					<option value="0300">03:00</option>
					<option value="0400">04:00</option>
					<option value="0500">05:00</option>
					<option value="0600">06:00</option>
					<option value="0700">07:00</option>
					<option value="0800">08:00</option>
					<option value="0900">09:00</option>
					<option value="1000">10:00</option>
					<option value="1100">11:00</option>
					<option value="1200">12:00</option>
					<option value="1300">13:00</option>
					<option value="1400">14:00</option>
					<option value="1500">15:00</option>
					<option value="1600">16:00</option>
					<option value="1700">17:00</option>
					<option value="1800">18:00</option>
					<option value="1900">19:00</option>
					<option value="2000">20:00</option>
					<option value="2100">21:00</option>
					<option value="2200">22:00</option>
					<option value="2300">23:00</option>
				</select>
	        </select>
	        <select id="txtMailingInterval" onchange="OnChangeInterval()" default='1'>
	            <name><%= L_DMMailingInterval_Text %></name>
	            <tooltip><%= L_DMMailingInterval_Tooltip %></tooltip>
				<select>
					<option value="1"><%= L_DMOneTime_Text %></option>
					<option value="4"><%= L_DMDaily_Text %></option>				
					<option value="8"><%= L_DMWeekly_Text %></option>				
					<option value="16"><%= L_DMMonthlyFixed_Text %></option>
					<option value="32"><%= L_DMMonthlyVarying_Text %></option>
				</select>
	        </select>
		</fields>
	</editsheet>
</xml>
	
<xml id="DMTestMeta">
	<editsheet>
	    <global title="no"/>
	    <fields>
<%	dim rsQuery, fdTestListID, sTestID
	const FAILED_LIST = 3
	g_sQuery = "SELECT di.g_dmitem_testlistid " & _
				"FROM dm_item di, campaign_item ci " & _
				"WHERE di.i_campitem_id = ci.i_campitem_id AND ci.i_campitem_id = " & g_nCampItemID

	Set rsQuery = rsGetRecordset(g_sCampaignConnectionStr, g_sQuery, AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY)

	set fdTestListID = rsQuery("g_dmitem_testlistid")
	If isNull(fdTestListID.value) Then 
		sTestID = "0"
		btndisabled = True
	else
		sTestID = fdTestListID.value
		'Removing setting false b/c it is defalting(false) now
		'btndisabled = False
	End If
	rsQuery.close
	set rsQuery = nothing
	Dim oLM, rsListPrimary			
	Set oLM = Server.CreateObject("Commerce.ListManager")
	oLM.Initialize g_sCampaignConnectionStr		
	set rsListPrimary = oLM.GetLists
	rsListPrimary.sort = "list_name"
%>	        <select id="g_dmitem_testlistid" onchange="OnChangeTest()" default="<%= sTestID %>">
	            <name><%= L_DMTestList_Text %></name>
	            <tooltip><%= L_DMTestList_Tooltip %></tooltip>
	            <select>
	            <option value="0"><%= L_DMSelectList_Text %></option>
<%				If not rsListPrimary.EOF   Then
					do While not rsListPrimary.EOF
						If ((rsListPrimary("list_flags") and MAILABLELIST) = MAILABLELIST) _
							and (rsListPrimary("list_unique_emails") > 0 or ((rsListPrimary("list_flags") And DYNAMICLIST) = DYNAMICLIST)) _
							and rsListPrimary("list_status") <> FAILED_LIST Then							
							Response.write("<option value='" & rsListPrimary("list_id").value & "'>" & _
								g_mscsPage.htmlEncode(rsListPrimary("list_name").value) & "</option>" & vbCrLf)
						End if 
						rsListPrimary.Movenext
					Loop
				End if
%>		        </select>
			</select>
		</fields>
	</editsheet>
</xml>

<xml id="MasterMeta">
	<editsheets>
		<editsheet>
			<global expanded="yes" >
				<name><%= L_DirectMailProperties_Text %></name>
				<key><%= L_DMProperties_Accelerator %></key>
			</global>
			<fields>
				<text id="u_campitem_name" default="<%= Quote(sCUName) %>" required="yes" maxlen="50" subtype="short">
			        <name><%= L_DMName_Text %></name>
			        <tooltip><%= L_DMName_ToolTip %></tooltip>
			        <charmask>.*</charmask>
			        <error><%= L_DirectMail_ErrorMessage %></error>
			        <prompt><%= L_DMCmpItemName_Text%></prompt>
			    </text>
			    <text id="u_dmitem_from" default="<%= Quote(sCUFrom) %>" required="yes" maxlen="256" subtype="short" onchange="OnChangeFrom()">
			        <name><%= L_DMFrom_Text %></name>
			        <tooltip><%= L_DMFrom_ToolTip %></tooltip>
			        <charmask>.*</charmask>
			        <error><%= L_DMFrom_ErrorMessage %></error>
			        <prompt><%= L_DMCmpItememail_Text%></prompt>
			    </text>
			    <text id="u_dmitem_replyto" default="<%= Quote(sCUReplyTo) %>" maxlen="256" subtype="short">
			        <name><%= L_DMReplyTo_Text %></name>
			        <tooltip><%= L_DMReplyTo_ToolTip %></tooltip>
			        <charmask>.*</charmask>
			        <error><%= L_DMReplyTo_ErrorMessage %></error>
			        <prompt><%= L_DMCmpItemreply_Text%></prompt>
			    </text>
			    <text id="u_dmitem_subj" default="<%= Quote(sCUSubject) %>" maxlen="256" subtype="short">
			        <name><%= L_DMMessageSubject_Text %></name>
			        <tooltip><%= L_DMMessageSubject_ToolTip %></tooltip>
			        <charmask>.*</charmask>
			        <error><%= L_DMMessageSubject_ErrorMessage %></error>
			        <prompt><%= L_DMCmpItemsubject_Text%></prompt>
			    </text>
			    <text id="u_dmitem_source" default="<%= Quote(sCUSource) %>" maxlen="1024" subtype="short">
			        <name><%= L_DMBody_Text %></name>
			        <tooltip><%= L_DMBody_ToolTip %></tooltip>
			        <charmask>[^#\^]*</charmask>
			        <error><%= L_DMBody_ErrorMessage %></error>
			        <prompt><%= L_DMEnterFilepath_Text %></prompt>
			    </text>
			    <text id="u_dmitem_attachment" default="<%= Quote(sCUAttach) %>" maxlen="1024" subtype="short">
			        <name><%= L_DMAttachment_Text %></name>
			        <tooltip><%=L_DMAttachment_ToolTip %></tooltip>
			         <charmask>.*</charmask>
			        <error>L_DMAttachment_ErrorMessage</error>
			        <prompt><%= L_DMCmpItemattachment_Text%></prompt>
			    </text>
			    <select id="g_dmitem_listid" default="<%= sCUListID %>" required="yes" >
			        <name><%= L_DMMailingList_Text %></name>
			        <tooltip><%= L_DMMailingList_Tooltip %></tooltip>
			        <select id="g_dmitem_listid">		            
<%					If not rsListPrimary.EOF or not rsListPrimary.BOF Then 
						rsListPrimary.MoveFirst
					End if
					If not rsListPrimary.EOF Then
						do While not rsListPrimary.EOF
							If ((rsListPrimary("list_flags") and MAILABLELIST) = MAILABLELIST) _
								and (rsListPrimary("list_unique_emails") > 0 or ((rsListPrimary("list_flags") And DYNAMICLIST) = DYNAMICLIST)) _
								and rsListPrimary("list_status") <> FAILED_LIST Then							
								Response.write("<option value='" & rsListPrimary("list_id").value & "'>" & _
									g_mscsPage.htmlEncode(rsListPrimary("list_name").value) & "</option>" & vbCrLf)
							End if 
							rsListPrimary.Movenext
						Loop
					End if
				Set rsListPrimary = nothing
				Set oLM = nothing
%>			        </select>
			    </select>
				<boolean id='b_dmitem_personalized' default="<%= g_b_dmitem_personalized %>" readonly='no'>
				    <name><%= L_DMPersonalization_Text %></name>
			        <tooltip><%= L_DMPersonalization_Tooltip %></tooltip>
				    <label><![CDATA[  ]]></label>
				</boolean>
			    <select id="l_dmitem_defformat" default="<%= sCUDefFormat %>">
			        <name><%= L_DMMailFormat_Text %></name>
			        <tooltip><%= L_DMMailFormat_Tooltip %></tooltip>
					<select id="l_dmitem_defformat">
						<option value="1"><%= L_TEXT_LISTBOX %></option>
						<option value="3"><%= L_MHTML_LISTBOX %></option>
						<option value="2"><%= L_MIME_LISTBOX %></option>
			        </select>
			    </select>
			    <text id="u_dmitem_desc" default="<%= Quote(sCUDesc) %>" subtype="long" maxlen="128">
			        <name><%= L_DMComments_Text %></name>
			        <tooltip><%= L_DMComments_Tooltip %></tooltip>
			        <charmask>^(\s*\S+\s*)*$</charmask>
			        <error><%= L_DMComments_ErrorMessage %></error>
			    </text>
			</fields>
		</editsheet>
		<editsheet>
			<global expanded="no" >
				<name><%= L_DMMailSchedule_Text %></name>
				<key><%= L_DMMailSchedule_Accelerator %></key>
			</global>
		    <fields>
				<numeric id="txtDays" min="1" max="1000" subtype="integer" default="<%= g_sRecurDailyInterval %>">
				    <name><%= L_DMEvery_Text %></name>
				    <tooltip><%= L_DMEvery_Text %></tooltip>
				    <error><%= L_DMtxtDays_ErrorMessage %></error>
				</numeric>
				<numeric id="txtWeeks" min="1" max="100" subtype="integer" default="<%= g_sRecurWeeklyInterval %>">
				    <name><%= L_DMEvery_Text %></name>
				    <tooltip><%= L_DMEvery_Text %></tooltip>
				    <error><%= L_DMEvery_ErrorMessage %></error>
				</numeric>
				<numeric id="txtDayOfMonth" min="1" max="31" subtype="integer" default="<%= g_sRecurMonthlyDayOfMonth %>">
				    <name><%= L_DMDay_Text %></name>
				    <tooltip><%= L_DMDay_Text %></tooltip>
				    <error><%= L_DMDayOfMonth_ErrorMessage %></error>
				</numeric>
				<numeric id="txtMonthSpan" min="1" max="30" subtype="integer" default="<%= g_sRecurMonthlyInterval %>">
				    <name><%= L_DMOfEvery_Text %></name>
				    <tooltip><%= L_DMOfEvery_Text %></tooltip>
				    <error><%= L_DMOfEvery_ErrorMessage %></error>
				</numeric>
				<numeric id="relRecur" min="1" max="30" subtype="integer" default="<%= g_sRecurMonthlyRelInterval %>">
				    <name><%= L_DMOfEvery_Text %></name>
				    <tooltip><%= L_DMOfEvery_Text %></tooltip>
				    <error><%= L_DMOfEvery_ErrorMessage %></error>
				</numeric>
				<select id="relWeekType" default="<%= g_sRecurMonthlyRelWeekType %>">
				    <name><%= L_DMThe_Text %></name>
				    <tooltip><%= L_DMThe_Text %></tooltip>
				    <select id="relWeekType">
						<option id="1" value="1"><%= L_DMFirst_Text %> </option>
						<option id="2" value="2"><%= L_DMSecond_Text %> </option>
						<option id="4" value="4"><%= L_DMThird_Text %> </option>
						<option id="8" value="8"><%= L_DMFourth_Text %> </option>
						<option id="16" value="16"><%= L_DMLast_Text %> </option>					
					</select>
				</select>               
				<select id="relDay" default="<%= g_sRecurMonthlyRelDayOfWeek %>">
		            <name><%=L_DMThe_Text%></name>
		            <tooltip><%=L_DMThe_Text%></tooltip>
		            <select id="relDay">
<%					dim nDay, i
			        for i = Cint(g_sMSCSWeekStartDay) to Cint(g_sMSCSWeekStartDay) + 6
						nDay = i
						if nDay > 6 then nDay = nDay - 7
%>						<option id="<%= nDay + 1 %>" value="<%= nDay + 1 %>"> <%= g_aDaysOfWeekLabel(nday) %></option>
<%					next
%>					</select>
				</select>               
		    </fields>
			<template fields="txtDays txtWeeks txtDayOfMonth txtMonthSpan relWeekType relDay relRecur" register="esSchedule"><![CDATA[
				<DIV ID='esSchedule' CLASS='editSheet'
					MetaXML='DMScheduleMeta' DataXML='dmMasterGroup'> <%= L_DMLoadProperties_Text %></div>
					<div id="recurrence_table" STYLE="MARGIN-LEFT: 40%">
				        <!-- day recurrence table -->
				        <div id="bd_cmdm_recur_day" <% if CInt(g_sFrequencyType) <> 4 then %> style="display:none" <% end if %>>
							<%= L_DMEvery_Text %>
							<SPAN ID="txtDays" style="HEIGHT: 22px; WIDTH: 38px"></SPAN>
							<%= L_DMDays_Text %>
				        </div>
				        <!-- end of day recurrence table -->
					 		                
				        <!-- week recurrence table -->
				        <div id="bd_cmdm_recur_week" <% if CInt(g_sFrequencyType) <> 8 then %> style="display:none" <% end if %>>
							<%= L_DMEvery_Text %>
							<SPAN ID="txtWeeks" style="HEIGHT: 22px; WIDTH: 30px"></SPAN> 
							<%= L_DMWeeks_Text %>
							<table BORDER="0" CELLPADDING="1" CELLSPACING="0">
								<tr>
<%									dim nRows
								nRows = 0
								for i = Cint(g_sMSCSWeekStartDay) to Cint(g_sMSCSWeekStartDay) + 6
									nDay = i
									if nDay > 6 then nDay = nDay - 7
%>										<td WIDTH="100"><label FOR="chkWeekDay<%= nDay + 1 %>"><%= g_aDaysOfWeekLabel(nDay) %></label></td>
									<td WIDTH="30"><input TYPE="checkbox" STYLE="BACKGROUND: transparent" ID="chkWeekDay<%= nDay + 1 %>" NAME="chkWeekDay<%= nDay + 1 %>" VALUE="<%= g_aDaysOfWeekValue(nDay) %>" <% if g_aDaysChecked(nDay) Then %> CHECKED <% End If %> ONCLICK="setDirty(&quot;&quot;)"></td>
<%										nRows = nRows + 1
									if nRows = 2 and i < Cint(g_sMSCSWeekStartDay) + 6 then
										'start new row
%>											</tr><tr>
<%											nRows = 0
									end if
								next
%>									</tr>
							</table>
				        </div>
				        <!-- end of week recurrence table -->

				        <!-- month recurrence table -->
				        <div id="bd_cmdm_recur_month">
						    <div id="row1" <% if CInt(g_sFrequencyType) <> 16 then %> style="display:none" <% end if %>>
								<%=	L_DMDay_Text %>
								<SPAN ID="txtDayOfMonth" style="HEIGHT: 22px; WIDTH: 24px"></SPAN>
								<%= L_DMOfEvery_Text %>
								<SPAN ID="txtMonthSpan" style="HEIGHT: 22px; WIDTH: 24px"></SPAN>
								<%= L_DMMonths_Text %>
							 </div>
						     <div id="row2" <%if CInt(g_sFrequencyType) <> 32 then %> style="display:none" <% end if %>>
								<%= L_DMThe_Text %> 				            								     
								<SPAN ID="relWeekType" style="HEIGHT: 20px; WIDTH: 100px"></SPAN> 
								&nbsp;
								<SPAN ID="relDay" style="HEIGHT: 20px; WIDTH: 140px"></SPAN> 
								&nbsp; <%= L_DMOfEvery_Text %>
								<SPAN ID="relRecur" style="HEIGHT: 20px; WIDTH: 24px"></SPAN>
								<%= L_DMMonths_Text %>
						    </div>
				 		</div>
						<!-- end of month recurrence table -->
					</div>
			]]></template>	
		</editsheet>
		<editsheet>
			<global expanded="no" >
				<name><%= L_DMTest_Text %></name>
				<key><%= L_DMTest_Accelerator %></key>
			</global>
			<template register="esTest testButton"><![CDATA[
				<DIV ID='esTest' CLASS='editSheet'
					MetaXML='DMTestMeta' DataXML='dmMasterGroup'><%= L_DMLoadProperties_Text %></div>
				<input id="testButton" name="testButton" type="Button" <% if btndisabled Then %> DISABLED <% end if %> 
				class="bdbutton" style="MARGIN-LEFT: 40%" value="<%= L_DMSendTestMail_Button %>" language="VBScript" 
				onclick="onClickTestBtn()">	
			]]></template>	
		</editsheet>
	</editsheets>	
</xml>

<!--***************************************************************************-->

<!-- Data xml -->

<xml id="dmMasterGroup">
	<?xml version="1.0" ?>
	<document>
		<record>
			<u_campitem_name><%= Server.HTMLEncode(sCUName) %></u_campitem_name>
			<u_dmitem_from><%= Server.HTMLEncode(sCUFrom) %></u_dmitem_from>
			<u_dmitem_subj><%= Server.HTMLEncode(sCUSubject) %></u_dmitem_subj>
			<u_dmitem_replyto><%= Server.HTMLEncode(sCUReplyTo) %></u_dmitem_replyto>
			<u_dmitem_source><%= Server.HTMLEncode(sCUSource) %></u_dmitem_source>
			<u_dmitem_attachment><%= Server.HTMLEncode(sCUAttach) %></u_dmitem_attachment>
			<g_dmitem_listid><%= sCUListID %></g_dmitem_listid>
			<b_dmitem_personalized><%= g_b_dmitem_personalized  %></b_dmitem_personalized>
			<l_dmitem_defformat><%= sCUDefFormat %></l_dmitem_defformat>
			<u_dmitem_desc><%= Server.HTMLEncode(sCUDesc) %></u_dmitem_desc>
			<i_dmitem_userflags><%= sCUUserFlags %></i_dmitem_userflags>
			<txtstartdate><%= g_MSCSDataFunctions.date(g_sStartDate, g_MSCSDefaultLocale) %></txtstartdate>
			<txtenddate><%= g_MSCSDataFunctions.date(g_sEndDate, g_MSCSDefaultLocale) %></txtenddate>
			<txtstarttime><%= g_sStartTime %></txtstarttime>
			<txtmailinginterval><%= g_sFrequencyType %></txtmailinginterval>
		</record>
	</document>
</xml>

<!--*******************************************************************************************-->

<div ID="bdcontentarea">

	<form METHOD="POST" ACTION='dmitem.asp' ID="dmItemForm" ONTASK="OnTaskSave()">
		<input TYPE="hidden" NAME="i_campitem_id" VALUE="<%= g_nCampItemID %>">	
		<input TYPE="hidden" NAME="u_customer_name" VALUE="<%= g_sCustomerName %>">	
		<input TYPE="hidden" NAME="u_camp_name" VALUE="<%= g_sCampName %>">	
		<input TYPE="hidden" NAME="return_to_camp_flag" VALUE="0">
		<input TYPE="hidden" NAME="test_mail_flag" VALUE="0">
		<input TYPE="hidden" NAME="type" VALUE="save">
		<input TYPE="hidden" NAME="dbaction" VALUE="<%= g_sDBAction %>">
		<input TYPE="hidden" NAME="i_camp_id" VALUE="<%= g_nCampID %>">	
		<INPUT TYPE='hidden' NAME='i_dmitem_id' VALUE='<%= g_nDMItemID %>'>
		<INPUT TYPE='hidden' NAME='archivedflag' VALUE='<%= g_sArchived %>'>
		<div ID="esMaster" CLASS="editSheet"
			MetaXML="MasterMeta" DataXML="dmMasterGroup"
			LANGUAGE="VBScript"
			ONCHANGE="onChangeMasterES()"
			ONREQUIRE="onRequireMasterES()"
			ONVALID="onValidMasterES()"><h3><%= g_imgLoadingAnimation %> <%= L_DMLoadingProperties_Text %></h3></div>
	</form>
</div>

</body>
</html>


