<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<%
dim sListID, aPostVal
dim oLM
dim sReturnVarID, sReturnVarOpID
dim sFriendlyError, sScriptError
dim g_sCampaignConnectionStr

on error resume next
	
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
	
	
sListID = Request.Form("list_id")
aPostVal = Split(Request.Form("listdata"), "|||", -1, 1)
'just for notes sake
'aPostVal(0) - denotes the ID of the List, If an existing list or a new one
'aPostVal(1) - contains the actual List ID or a new list name
'create the ListManager object
set oLM = Server.CreateObject("Commerce.ListManager")
	
'Initialie List Manager.Object
oLM.Initialize g_sCampaignConnectionStr
if err.Number <> 0 then
	Set oLM = Nothing
	'ReportError
	sFriendlyError = sGetErrorByID("Cant_connect_db")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
	Response.redirect "listmanager.asp?type=copy&status=0"
end if	
		
if CStr(aPostVal(0)) = "NAME" then
	'create a new empty list
	sReturnVarID = oLM.CreateEmpty(CStr(trim(aPostVal(1))), "", 1, 0)
	if err.Number <> 0 then
		Set oLM = Nothing
'		SetError L_LM_Text,L_CopyListTitle_Text, L_ListCreateEmptyFail_ErrorMessage
		sFriendlyError = sGetErrorByID("Cant_create_list")
		sScriptError = sGetScriptError(Err)
		setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		Response.redirect "listmanager.asp?type=copy&status=0"
	end if				
	'use the newly created list id to do the copy
	oLM.Copy sListID, sReturnVarID, TRUE, sReturnVarOpID
	if err.Number <> 0 then
		Set oLM = Nothing
		'ReportError
		sFriendlyError = sGetErrorByID("Cant_copy_list")
		sScriptError = sGetScriptError(Err)
		setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		Response.redirect "listmanager.asp?type=copy&status=0"
	end if		
else
	'use the list_id passed on from selection form
	oLM.Copy sListID, aPostVal(1), TRUE, sReturnVarOpID 
	if err.Number <> 0 then
		Set oLM = Nothing
		'ReportError
		sFriendlyError = sGetErrorByID("Cant_copy_list")
		sScriptError = sGetScriptError(Err)
		setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		Response.redirect "listmanager.asp?type=copy&status=0"
	end if		
end if 

set oLM = Nothing
Response.Redirect "listmanager.asp?type=copy&status=1"
%>