<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->

<%
dim g_sCampaignConnectionStr, g_sStatusText, oLM, sID, sName, sDesc

if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
		
Set oLM = Server.CreateObject("Commerce.ListManager")
oLM.Initialize g_sCampaignConnectionStr

if Err.Number <> 0 Then 
	sFriendlyError = sGetErrorByID("Cant_connect_db")
	sScriptError = sGetScriptError(Err)
	setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
end if

sID = Request("list_id")

if Request("type") = "save" and Err.number = 0 then
	sName = Request("list_name")
	sDesc = Request("list_desc")
	SaveListInfo()
else
	sName = oLM.getListName(sID)
	sDesc = oLM.getListProperty(sID, "list_description")
end if

Set oLM = nothing		

sub SaveListInfo()
	dim bNameModified, bDescrModified
	dim sFriendlyError, sScriptError
	On Error Resume Next
	
	bNameModified = Request.Form("bnamemodified")
	bDescrModified = Request.Form("bdescrmodified")
	
	'If the name of the list is modified 
	if bNameModified = 1 and Err.number = 0 Then
		oLM.Rename cstr(sID), Trim(sName) 
		if  Err.number <> 0 Then
			sFriendlyError = sGetErrorByID("Cant_rename_list")
			sScriptError = sGetScriptError(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		end if
	end if
	
	'If the Description of the list is modified 
	if bDescrModified = 1 and Err.number = 0 Then
		oLM.SetDesc cstr(sID), Trim(sDesc) 
		if Err.number <> 0 Then
			sFriendlyError = sGetErrorByID("Cant_changedesc_list")
			sScriptError = sGetScriptError(Err)
			setError L_LM_Text, sFriendlyError, sScriptError, ERROR_ICON_ALERT
		end if
	end if
	
	if Err.number = 0 Then
		g_sStatusText = L_SavedListProperties_StatusBar
	end if
end sub
%>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<SCRIPT LANGUAGE='VBScript'>
	sub onNameChange ()
		saveform.bnamemodified.value = 1
		CheckName()	
	end sub	
	
	sub onDescChange ()
		saveform.bdescrmodified.value = 1
		CheckName()
	end sub
 
	Sub CheckName()
		on error resume next
		btnOK.disabled = not (list_name.valid and not list_name.required)
	End sub	
</SCRIPT>
</HEAD>

<BODY SCROLL='NO'>
<%
	InsertEditTaskBar sFormatString(L_ListProperties_Text, array(sName)), g_sStatusText
%>
<xml id="esMeta">
	<editsheet>
		<global title="no"/>
		<fields>
		  <text id='list_name' maxlen='64' subtype='short' required='yes' onchange='OnNameChange' onrequired="CheckName" onvalid="CheckName">
				<name><%= L_OpenListName_Text %></name>
				<prompt><%= L_OpenListNamePrompt_Text %></prompt>
				<tooltip><%= L_OpenListName_Tooltip %></tooltip>
				<error><%= L_ListnameLong_ErrorMessage %></error>
				<charmask>.*</charmask>
		  </text>
		  <text id='list_desc' maxlen='256' subtype='long' onchange='onDescChange'>
				<name><%= L_OpenListDescr_Text %></name>
				<tooltip><%= L_OpenListDescr_Tooltip %></tooltip>
				<error><%= L_ListdescLong_ErrorMessage %></error>
				<charmask>(\S*\s*\S*)*</charmask>
		  </text>
		</fields>
	</editsheet>
</xml>

<xml id="esData">
	<document>
		<record>
			<list_name><%= g_mscsPage.HTMLEncode(sName) %></list_name>
			<list_desc><%= g_mscsPage.HTMLEncode(sDesc) %></list_desc>
		</record>
	</document>
</xml>

<div ID="bdcontentarea">
<form id="saveform">
	<input type=hidden id=type name=type value="save">
	<input type=hidden id=list_id name=list_id value="<%= sID %>">
	<input type=hidden id=bnamemodified name=bnamemodified value="0">
	<input type=hidden id=bdescrmodified name=bdescrmodified value="0">
	<DIV ID='esMaster' CLASS='editSheet'
		METAXML='esMeta' DATAXML='esData'
		language="VBScript"
		onchange='SetDirty("")'
		onrequire='SetRequired("")'
		onvalid='SetVAlid("")'></DIV>
</form>
</div>

</BODY>
</HTML>