<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!-- #INCLUDE FILE='..\include\BizDeskUtil.asp' -->
<!-- #INCLUDE FILE='include\marketingStrings.asp' -->
<!-- #INCLUDE FILE='include\campitem_util.asp' -->
<!-- #INCLUDE FILE='include\campitem_util.htm' -->
<%
Dim g_sCampaignConnectionStr, g_dFormValues, g_rsQuery, g_sQuery, g_nDayMask, _
	g_bSuccess, g_xmlCustomerCampaignData, g_nCampItemID, g_sCreativeTypes, _
	g_sCreativeTypesXML, g_sStatusText, aDaysOfWeek, aDaysOfWeekLabel, aDaysOfWeekValue
Dim g_sIndustryOptions, g_sTimeOptions, g_sCreativeSizeOptions, g_sCreativeTypeOptions, _
	g_sAdXMLData, g_sPageTargetGroups, g_sCreativeTypeProperties, g_sTitle
Dim g_sEventsDisabled, g_sWeightDisabled 'for initial display style of Events and Weight field

Const DAY_MON = &H0040
Const DAY_TUE = &H0020
Const DAY_WED = &H0010
Const DAY_THU = &H0008
Const DAY_FRI = &H0004
Const DAY_SAT = &H0002
Const DAY_SUN = &H0001
Const DAY_ALL = &H007F

aDaysOfWeek = array("day_sun", "day_mon", "day_tue", "day_wed", "day_thu", "day_fri", "day_sat")
aDaysOfWeekLabel = array(L_AdSun_Text, L_AdMon_Text, L_AdTue_Text, L_AdWed_Text, L_AdThu_Text, L_AdFri_Text, L_AdSat_Text)
aDaysOfWeekValue = array(DAY_SUN, DAY_MON, DAY_TUE, DAY_WED, DAY_THU, DAY_FRI, DAY_SAT)

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

'get form values
Set g_dFormValues = dGetFormValues()

'get other pickers and data from db
g_sIndustryOptions = sGetIndustryOptions()
g_sTimeOptions = sGetTimeOptions()
g_sCreativeSizeOptions = sGetCreativeSizeOptions()
g_sCreativeTypeOptions = sGetCreativeTypeOptions(GUID_TYPE_AD)
g_sAdXMLData = sGetAdXMLData()
g_sPageTargetGroups = sGetPageTargetGroups()
g_sTitle = sGetTitle()
'fills g_sCreativeTypesXML and g_sCreativeTypes
GetCreativeTypeProperties(GUID_TYPE_AD)

'all jobs are done
Set g_oConn = Nothing
Set g_rsQuery = Nothing

'~~~~~~~~~~~end if inline code - begin of functions~~~~~~~~~~~~~~~~~~~~

Function dGetFormValues()
	Dim dFormValues
	Set dFormValues = Server.CreateObject("Commerce.Dictionary")
	dFormValues("type") = Request.Form("type")
	dFormValues("i_customer_id") = CLng(Request.Form("i_customer_id"))
	dFormValues("i_camp_id") = CLng(Request.Form("i_camp_id"))
	dformValues("u_customer_name") = Request("u_customer_name")
	dformValues("u_camp_name") = Request("u_camp_name")
	Select Case dFormValues("type")
		Case "add"
			dFormValues("i_campitem_id") = -1
			dFormValues("dbaction") = "insert"
		Case "copy"
			dFormValues("i_campitem_id") = CLng(Request.Form("i_campitem_id"))
			dFormValues("dbaction") = "insert"
		Case "open"
			dFormValues("i_campitem_id") = CLng(Request.Form("i_campitem_id"))
			dFormValues("dbaction") = "update"
	End Select
	'prepare necessary info of parent
	g_sQuery = "SELECT cu.u_customer_name, cu.i_industry_id, cu.u_customer_url, ca.u_camp_name, ca.dt_camp_start, " & _
		"ca.dt_camp_end, ca.b_camp_level_goal, et.u_event_description FROM campaign AS ca " & _
		"INNER JOIN event_type AS et ON ca.i_event_type_id = et.i_event_type_id " & _
		"INNER JOIN customer AS cu ON ca.i_customer_id = cu.i_customer_id " & _
		"WHERE ca.i_camp_id = " & dFormValues("i_camp_id") 
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	If Not g_rsQuery.EOF Then 'it can't be EOF, or something is wrong		
		dFormValues("u_customer_name") = g_rsQuery("u_customer_name")
		dFormValues("i_industry_id") = g_rsQuery("i_industry_id")
		dFormValues("u_camp_name") = g_rsQuery("u_camp_name")
		dFormValues("dt_camp_start") = g_rsQuery("dt_camp_start")
		dFormValues("dt_camp_end") = g_rsQuery("dt_camp_end")
		dFormValues("b_camp_level_goal") = g_rsQuery("b_camp_level_goal")
		dFormValues("u_event_description") = g_rsQuery("u_event_description")
		dFormValues("u_customer_url") = g_rsQuery("u_customer_url")
		If IsNull(dFormValues("i_industry_id")) Then dFormValues("i_industry_id") = 0
	End If
	g_rsQuery.Close
	Set dGetFormValues = dFormValues
End Function

Function sGetIndustryOptions()
	Dim sReturn, fdIndustryID, fdIndustryCode
	sReturn = "<option value='0'>" & L_None_Text & "</option>"
	g_sQuery = "SELECT i_industry_id, u_industry_code FROM industry_code ORDER BY u_industry_code"						
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	With g_rsQuery
		Set fdIndustryID = .Fields(0)
		Set fdIndustryCode = .Fields(1)
		Do While Not .EOF
			sReturn = sReturn & "<option value='" & fdIndustryID.Value & "'>" & _
				g_mscsPage.htmlEncode(fdIndustryCode.Value) & "</option>"
			.MoveNext
		Loop 
		.Close
	End With
	sGetIndustryOptions = sReturn
End Function

Function sGetTimeOptions()
	Dim sReturn, i
	sReturn = ""
	For i = 0 to 9
		sReturn = sReturn & "<option value='" & i & "'>0" & i & ":00</option>"
	Next
	For i = 10 to 23
		sReturn = sReturn & "<option value='" & i & "'>" & i & ":00</option>"
	Next
	sGetTimeOptions = sReturn
End Function

Function sGetAdXMLData()
	Dim xmlData, xmlNode, dFormat
	'main ad XML data must be munged a bit before setting in data island
	Select Case g_dFormValues("type")
		Case "copy"
			'when copying make name empty string
			g_sQuery = "SELECT u_campitem_name='', i_campitem_id = '', "
		Case Else
			g_sQuery = "SELECT ci.u_campitem_name, ci.i_campitem_id, "
	End Select 

	Set dFormat = server.CreateObject("commerce.dictionary")
	dFormat("dt_campitem_start") = AD_DATE
	dFormat("dt_campitem_end") = AD_DATE
	g_sQuery = g_sQuery & "ci.dt_campitem_archived, CAST(ci.b_campitem_active AS int) b_campitem_active, " & _
		"ai.i_aditem_type, ai.i_aditem_events_scheduled, ai.i_aditem_weight, " & _
		"ci.i_campitem_exposure_limit, cr.dt_creative_received, ai.i_industry_id, " & _
		"ci.u_campitem_comments, ci.dt_campitem_start, ci.dt_campitem_end, " & _
		"ai.i_aditem_days_of_week_mask, ai.i_aditem_time_of_day_start, ai.i_aditem_time_of_day_end, " & _
		"cr.i_creative_id, cr.i_creative_type_id, cr.i_creative_size_id " & _
		"FROM campaign_item AS ci INNER JOIN ad_item AS ai ON ci.i_campitem_id = ai.i_campitem_id " & _
		"INNER JOIN creative AS cr ON ci.i_creative_id = cr.i_creative_id " & _
		"WHERE ci.i_campitem_id = " & g_dFormValues("i_campitem_id")
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	Set xmlData = xmlGetXMLFromRSEx(g_rsQuery, -1, -1, -1, dFormat)
	g_rsQuery.Close 

	With xmlData
		'properly display State field
		Set xmlNode = xmlData.selectSingleNode("//dt_campitem_archived")
		If Len(xmlNode.text) = 0 Then
			xmlNode.text = L_CMUnarchived_Text 
		Else
			xmlNode.text = L_CMArchived_Text
		End If

		'set default values for new ad
		If g_dFormValues("type") = "add" Then
			.selectSingleNode("//i_aditem_days_of_week_mask").text = DAY_ALL 'default to every day		
			.selectSingleNode("//i_creative_id").text = -1 'default to nothing
			.selectSingleNode("//i_creative_type_id").text = 1 'default to image
			.selectSingleNode("//i_aditem_type").text = 1 'default to Paid Ad
			.selectSingleNode("//i_industry_id").text = g_dFormValues("i_industry_id") 'default to the same as customer's
		End If

		'no matter add, copy, or open, display industry id properly
		Set xmlNode = .selectSingleNode("//i_industry_id")
		If Len(xmlNode.text) = 0 Then xmlNode.text = 0 'dafault to None (or interpret to None)
			
		'need this for masking Days checkbox
		g_dFormValues("i_aditem_days_of_week_mask") = CInt(.selectSingleNode("//i_aditem_days_of_week_mask").text)
			
		'need this to retrieve creative property values in sGetCreativeTypeProperties()
		g_dFormValues("i_creative_id") = CLng(.selectSingleNode("//i_creative_id").text)
			
		'need this for the initial diaplay of template
		g_dFormValues("i_creative_type_id") = CLng(.selectSingleNode("//i_creative_type_id").text)
			
		'set initial diaplay style of Ad Type, Requests/Clicks, and Weight
		If g_dFormValues("b_camp_level_goal") Then
			g_sEventsDisabled = "yes"
			g_sWeightDisabled = "no"
		Else
			If .selectSingleNode("//i_aditem_type").text = 1 Then
				g_sEventsDisabled = "no"
				g_sWeightDisabled = "yes"
			Else
				g_sEventsDisabled = "yes"
				g_sWeightDisabled = "no"
			End If	
		End If

		'need this to display on title bar
		g_dFormValues("u_campitem_name") = .selectSingleNode("//u_campitem_name").text

		sGetAdXMLData = .xml
	End With
End Function

Function sGetTitle()
	Dim sTitle
	'get Taskbar Title according to option type 
	Select Case g_dFormValues("type")
		Case "add", "copy"
			sTitle = sFormatString(L_AdCampItemPageTitleNew_Text, Array(g_mscsPage.HTMLEncode(g_dFormValues("u_customer_name")), g_dFormValues("u_camp_name"))) 
		Case "open"
			sTitle = sFormatString(L_AdCampItemPageTitle_Text, Array(g_mscsPage.HTMLEncode(g_dFormValues("u_customer_name")), g_mscsPage.HTMLEncode(g_dFormValues("u_camp_name")), g_dFormValues("u_campitem_name"), g_dFormValues("i_campitem_id")))
	End Select

	sGetTitle = sTitle
End Function
%>
<html>
<head>
	<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">
	<script language="VBScript">
		Option Explicit

		const ADTYPE_PAID = 1
		const ADTYPE_HOUSE = 2
	</script>
	<script language="VBScript" DEFER>
		option explicit
		
		Dim bFromOnTaskSave
		bFromOnTaskSave = False
		
		Sub AdTypeChange()
			Dim bCampLevelGoal, nAdType

			bCampLevelGoal = CBool(saveform.b_camp_level_goal.value)
			nAdType = CInt(esMaster.field("i_aditem_type").value)
			If bCampLevelGoal Then
				With esMaster
					.hide("i_aditem_events_scheduled")
					.show("i_aditem_weight")
					.field("i_aditem_events_scheduled").value = 0 
				End With
			Else
				Select Case nAdType
					Case ADTYPE_PAID
						With esMaster
							.show("i_aditem_events_scheduled")
							.hide("i_aditem_weight")
							.field("i_aditem_weight").value = 1
						End With
					Case ADTYPE_HOUSE
						With esMaster
							.hide("i_aditem_events_scheduled")
							.show("i_aditem_weight")
							.field("i_aditem_events_scheduled").value = 0 
						End With
				End Select
			End If
		End Sub
	
		Sub OnTaskSave()
			dim elSource, sTemp, sSeparator, elDiv, xmlDOMDoc, xmlErrorNodes, xmlCampItem, _
				xmlCreativeItem, elEditSheet, elCheckbox, elBtnBack, nDay, bDaySet, aDaysOfWeek
			bFromOnTaskSave = True
			Set elSource = window.event.srcElement
			sTemp = ""
			sSeparator = "|"
			aDaysOfWeek = array("day_sun", "day_mon", "day_tue", "day_wed", "day_thu", "day_fri", "day_sat")

			if cInt(b_campitem_active.value) = 1 then
				bDaySet = false
				for nDay = 0 to 6
					if document.all(aDaysOfWeek(nDay)).checked then bDaySet = true
				next
				if not bDaySet then
					msgbox "<%= L_AdMustCheckDay_ErrorMessage %>", vbOKOnly, "<%= L_AdMustCheckDay_DialogTitle %>"
					exit sub
				end if
			end if

			'collect data from list box
			For Each elDiv in PageSelectedList.children
				sTemp = sTemp & elDiv.getAttribute("value") & sSeparator
			Next
			saveform.PageSelect.value = sTemp

			sTemp = ""
			For Each elDiv in TargetSelectedList.children
				sTemp = sTemp & elDiv.getAttribute("value") & sSeparator
			Next
			saveform.TargetSelect.value = sTemp

			With saveform
				If elSource Is elGetTaskBtn("savenew") Then
					.type.value = "add"
				Else 'click save or back or saveback
					.type.value = "open" 'doesn't matter for back and saveback
				End If
			End With

			set xmlDOMDoc = xmlPostFormViaXML(saveform)
			HideWaitDisplay()
			EnableTask("back")
			'successful save should return one item node and no errors
			set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
			set xmlCampItem = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'campitem_id']")
			set xmlCreativeItem = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'creative_id']")
			if xmlErrorNodes.length <> 0 or xmlDOMDoc.documentElement is nothing _
				or xmlCampItem is nothing or xmlCreativeItem is nothing then
				'report errors and set status text
				SetStatusText(sFormatString("<%= L_BDSSUnableToSaveX_StatusBar %>", Array(esMaster.field("u_campitem_name").value)))
				ShowXMLNodeErrors(xmlErrorNodes)
				'reset dirty bit for all EditSheets
				for each elEditSheet in document.all.urns("Commerce.EditSheet")
					elEditSheet.resetDirty()
				next
			else
				'set status text
				SetStatusText(sFormatString("<%= L_BDSSSavedX_StatusBar %>", Array(esMaster.field("u_campitem_name").value)))
				If elSource Is elGetTaskBtn("savenew") Then
					'set creative id (campitem id will be cleared by resetdefault below) 
					saveform.requires_checkpoint.value = 1
					saveform.i_creative_id.value = -1
					saveform.dbaction.value = "insert"
					dt_creative_received.readonly = false
					set g_dCProp = CreateObject("Scripting.Dictionary")

					'move pages and groups and reset runofsite
					saveform.btnAdd1.disabled = True
					saveform.btnRemove1.disabled = True
					saveform.RunOfSite.checked = true
					if saveform.RunOfSite.checked then saveform.RunOfSite.click()
					PageSelectedList.selectAll()
					PageGroupList.add(PageSelectedList.remove())
					if not saveform.RunOfSite.checked then saveform.RunOfSite.click()
					saveform.btnAdd2.disabled = True
					saveform.btnRemove2.disabled = True
					TargetSelectedList.selectAll()
					TargetGroupList.add(TargetSelectedList.remove())

					'reset day checkboxes
					for each elCheckbox in daycheckboxcontainer.all.tags("INPUT")
						elCheckbox.checked = true
					next

					'reset dirty bit and defaults for all EditSheets
					for each elEditSheet in document.all.urns("Commerce.EditSheet")
						elEditSheet.resetDefault()
					next
					esMaster.resetDefault()

					'clear preview
					document.frames("previewframe").location.replace "preview.asp"

					'reset title for new
					SetPageTitleText(sFormatString("<%= L_AdCampItemPageTitleNew_Text %>", Array(saveform.u_customer_name.value, saveform.u_camp_name.value)))
				Else 'save or back or saveback
					'set campitem id and creative id
					saveform.requires_checkpoint.value = 0
					saveform.i_campitem_id.value = xmlCampItem.getAttribute("value")
					saveform.i_creative_id.value = xmlCreativeItem.getAttribute("value")
					saveform.dbaction.value = "update"
					dt_creative_received.readonly = true

					'reset dirty bit for all EditSheets
					for each elEditSheet in document.all.urns("Commerce.EditSheet")
						elEditSheet.resetDirty()
					next
					esMaster.resetDirty()

					'show preview
					document.frames("previewframe").location.replace "preview.asp?campitem_id=" & saveform.i_campitem_id.value

					'reset title for item saved
					SetPageTitleText(sFormatString("<%= L_AdCampItemPageTitle_Text %>", Array(saveform.u_customer_name.value, saveform.u_camp_name.value, esMaster.field("u_campitem_name").value, saveform.i_campitem_id.value)))
				End If
				g_bDirty = false
				if parent.g_bCloseAfterSave then
					parent.g_bCloseAfterSave = false
					' -- save and exit chosen and no errors while saving
					set elBtnBack = elGetTaskBtn("back")
					if not elBtnBack is nothing then
						if not elBtnBack.disabled then elBtnBack.click()
					end if
				end if
				parent.g_bCloseAfterSave = false
			End If
			esMaster.focus()
		
			'To force the disabling all icons except back
			disabletask "savenew"
			disabletask "saveback"
			disabletask "save"
		
			bFromOnTaskSave = False
		End Sub

		Sub DateChange()
			Dim dtStart, dtEnd, iDateDiff, bNoGo
			If bFromOnTaskSave Then Exit Sub
			'show validation error if start after end
			dtStart = dtGetDate(dt_campitem_start.value)
			dtEnd = dtGetDate(dt_campitem_end.value)
			bNoGo = False
			If Not dt_campitem_start.required And Not dt_campitem_end.required Then
				iDateDiff = DateDiff("d", dtStart, dtEnd)
				If iDateDiff < 0 Then
					msgbox "<%= L_CMStartDateBeforeEndDate_ErrorMessage %>", vbOKOnly, "<%= L_BDSSVAlidationErrorTitle_Text %>"
					bNoGo = True
				End If
			End If
			If bNoGo Then
				g_bDirty = False
				DisableTask("save")
				DisableTask("saveback")
				DisableTask("savenew")
			Else
				g_bDirty = True
				EnableTask("save")
				EnableTask("saveback")
				EnableTask("savenew")
			End If
			SetRequiresCheckpoint()
		End Sub

		Sub SetRequiresCheckpoint()
			saveform.requires_checkpoint.value = 1
		End Sub

		Sub OnChangeDay()
			SetRequiresCheckpoint()
			SetDirty("")
		End Sub
	</script>

	<script LANGUAGE='VBScript'>
		Sub window_onload()
			dim sImageType
			sImageType = "imgtype<%= g_dFormValues("i_creative_type_id") %>"
			set g_elSelectedImageType = document.all(sImageType)
			esMaster.focus()
			saveform.btnPreview.setExpression "disabled", "bdtaskbtnsave.disabled"
			on error resume next
			listener.setListenElement(g_elSelectedImageType)
		End Sub
	</script>
</head>
<body SCROLL="no">
<%
	InsertEditTaskBar g_sTitle, g_sStatusText
%>

<!-- display the customer and campaign outside the Editsheet -->
<%
'After the changes to how we add a new item, by choosing the campaign we want it to.
%><!--*****META*****META*****META*****META*****META*****META*****META*****META*****META*****-->

<!-- Start creative types -->
<%= g_sCreativeTypesXML %>
<!-- End creative types -->

<!-- Special Meta Data template for Ad Schedule -->
<xml id='esAdScheduleDateMeta'>
	<editsheet>
	    <global title="no" />
		<fields>
			<date id="dt_campitem_start" required="yes" firstday='<%= g_sMSCSWeekStartDay %>' default='<%= g_MSCSDataFunctions.date(g_dFormValues("dt_camp_start"), g_MSCSDefaultLocale) %>' onchange='DateChange()'>
				<name><%= L_AdStartDate_Text %></name>
	            <tooltip><%= L_AdStartDate_ToolTip %></tooltip>
	            <error><%= L_AdStartDate_ErrorMessage %></error>
	            <format><%= g_sMSCSDateFormat%></format>
	        </date>
			<date id="dt_campitem_end" required="yes" firstday='<%= g_sMSCSWeekStartDay %>' default='<%= g_MSCSDataFunctions.date(g_dFormValues("dt_camp_end"), g_MSCSDefaultLocale) %>' onchange='DateChange()'>
				<name><%= L_AdEndDate_Text %></name>
	            <tooltip><%= L_AdEndDate_ToolTip %></tooltip>
	            <error><%= L_AdEndDate_ErrorMessage %></error>
	            <format><%= g_sMSCSDateFormat%></format>
	        </date>
		</fields>
	</editsheet>
</xml>

<xml id='esAdScheduleTimeMeta'>
	<editsheet>
	    <global title="no" />
		<fields>
			<select id="i_aditem_time_of_day_start" default='0' onchange='DateChange()'>
				<name><%= L_AdStartTime_Text %></name>
	            <tooltip><%= L_AdStartTime_ToolTip %></tooltip>
	            <select id="i_aditem_time_of_day_start">
					<%= g_sTimeOptions %>
	            </select>
	        </select>
			<select id="i_aditem_time_of_day_end" default='0' onchange='DateChange()'>
				<name><%= L_AdEndTime_Text %></name>
	            <tooltip><%= L_AdEndTime_ToolTip %></tooltip>
	            <select id="i_aditem_time_of_day_end">
					<%= g_sTimeOptions %>
	            </select>
	        </select>
		</fields>
	</editsheet>
</xml>

<!-- Display Meta Data Template -->
<%
	InsertPreviewDisplayMeta
%>

<!-- master container Meta Data -->
<xml id="MasterMeta">
	<editsheets>
		<editsheet>
			<global expanded="yes">
				<name><%= L_AdAdProperty_Text %></name>
				<key><%= L_AdAdProperty_Accelerator %></key>
			</global>
			<fields>
				<text id="u_campitem_name" required="yes" maxlen="50" subtype="short">
					<name><%= L_AdAdName_Text %></name>
			        <tooltip><%= L_AdAdName_ToolTip %></tooltip>
			        <prompt><%= L_AdAdNamePrompt_Text %></prompt>
			        <error><%= L_AdAdName_ErrorMessage %></error>
			    </text>		    
				<select id="b_campitem_active" default='0' onchange='SetRequiresCheckpoint()'>
					<name><%= L_AdStatus_Text %></name>
			        <tooltip><%= L_AdStatus_ToolTip %></tooltip>
			        <select id="b_campitem_active">
						<option value="1"><%= L_AdStatusOptionActive_Text %></option>
						<option value="0"><%= L_AdStatusOptionInactive_Text %></option>
			        </select>
			    </select>
				<text id="dt_campitem_archived" readonly="yes" default='<%= L_CMUnarchived_Text %>'>
					<name><%= L_AdState_Text %></name>
			        <tooltip><%= L_AdState_ToolTip %></tooltip>
			    </text>
				<select id="i_aditem_type" default='1' onchange="AdTypeChange()">
					<name><%= L_AdType_Text %></name>
			        <tooltip><%= L_AdType_ToolTip %></tooltip>
			        <select id="i_aditem_type">
						<option value="1"><%= L_AdTypeOptionPaidAd_Text %></option>
						<option value="2"><%= L_AdTypeOptionHouseAd_Text %></option>
			        </select>
			    </select>
			    <numeric id="i_aditem_events_scheduled" required="yes" default='0' hide="<%= g_sEventsDisabled %>" min="0" max="2000000000">
					<name><%= g_dFormValues("u_event_description") %>:</name>
					<tooltip><%= g_dFormValues("u_event_description") %></tooltip>
					<error><%= L_AdEvents_ErrorMessage %></error>
				</numeric>
				<numeric id="i_aditem_weight" required="yes" default='1' hide="<%= g_sWeightDisabled %>" min="0" max="2000000000"
					onchange='SetRequiresCheckpoint()'>
					<name><%= L_AdWeight_Text %></name>
			        <tooltip><%= L_AdWeight_ToolTip %></tooltip>
					<error><%= L_AdWeight_ErrorMessage %></error>
			    </numeric>
				<select id="i_campitem_exposure_limit" default='0'>
					<name><%= L_AdExposureLimit_Text %></name>
			        <tooltip><%= L_AdExposureLimit_ToolTip %></tooltip>
			        <select id="i_campitem_exposure_limit">
						<option value="0"><%= L_None_Text %></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
			    </select>
				<date id="dt_creative_received" <% If g_dFormValues("type") = "open" Then %> readonly='yes' <% end if %>
					firstday='<%= g_sMSCSWeekStartDay %>' default='<%= g_MSCSDataFunctions.date(Date(), g_MSCSDefaultLocale) %>'>
					<name><%= L_AdDateReceived_Text %></name>
			        <tooltip><%= L_AdDateReceived_ToolTip %></tooltip>
			        <error><%= L_AdDateReceived_ErrorMessage %></error>
			        <format><%= g_sMSCSDateFormat%></format>
			    </date>
				<select id="i_industry_id" default='<%= g_dFormValues("i_industry_id") %>'>
					<name><%= L_AdIndustry_Text %></name>
			        <tooltip><%= L_AdIndustry_ToolTip %></tooltip>
					<select id="i_industry_id">
						<%= g_sIndustryOptions %>
					</select>
			    </select>
				<text id="u_campitem_comments" maxlen="255" subtype="long">
					<name><%= L_AdComments_Text %></name>
			        <tooltip><%= L_AdComments_ToolTip %></tooltip>
			        <error><%= L_AdComments_ErrorMessage %></error>
				</text>
			</fields>
		</editsheet>
		<editsheet>
		    <global expanded="no">
				<name><%= L_AdAdSchedule_Text %></name>
				<key><%= L_AdAdSchedule_Accelerator %></key>
		    </global>
		    <template register="esAdSchedDate esAdSchedTime day_sun day_mon day_tue day_wed day_thu day_fri day_sat"><![CDATA[
				<div ID="esAdSchedDate" CLASS="editsheet"
					MetaXML="esAdScheduleDateMeta"
					DataXML="esAdData"></div>
				<table CELLPADDING="0" CELLSPACING="4" class="esTableStyle" style="TABLE-LAYOUT:fixed" width=95%> 
				<tr>
					<td class="reqicon"></td>
					<td class="esLabelCell" TITLE="<%= L_AdDays_ToolTip %>"><%= L_AdDays_Text %></td>
					<td class="esDataCell efReadWrite">
						<table BORDER="0" CELLPADDING="0" CELLSPACING="0">
							<tr>
<%							dim nDay, i
							for i = CInt(g_sMSCSWeekStartDay) to CInt(g_sMSCSWeekStartDay) + 6
								nDay = i
								if nDay > 6 then nDay = nDay - 7
%>								<td WIDTH="30" ALIGN="center"><label FOR="<%= aDaysOfWeek(nDay) %>" TITLE='<%= aDaysOfWeekLabel(nDay) %>'><%= aDaysOfWeekLabel(nDay) %></label></td>
<%							next
%>							</tr>
							<tr ID='daycheckboxcontainer'>
<%							for i = CInt(g_sMSCSWeekStartDay) to CInt(g_sMSCSWeekStartDay) + 6
								nDay = i
								if nDay > 6 then nDay = nDay - 7
%>								<td WIDTH="30" ALIGN="center"><input TYPE="checkbox" STYLE="BACKGROUND: transparent" ID="<%= aDaysOfWeek(nDay) %>" NAME="<%= aDaysOfWeek(nDay) %>" VALUE="<%= aDaysOfWeekValue(nDay) %>" <% If g_dFormValues("i_aditem_days_of_week_mask") And aDaysOfWeekValue(nDay) Then %>CHECKED<% End If %> ONCLICK="OnChangeDay()"></td>
<%							next
%>
							</tr>
						</table>
					</td>
				</tr>	
				</table>
				<div ID="esAdSchedTime" CLASS="editsheet"
					MetaXML="esAdScheduleTimeMeta"
					DataXML="esAdData"></div>
		    ]]></template>
		</editsheet>
		<editsheet>
		    <global expanded="no">
		        <name><%= L_AdAdTarget_Text %></name>
				<key><%= L_AdAdTarget_Accelerator %></key>
		    </global>
		    <template register="RunOfSite"><![CDATA[
				<div ID="divPageGroups" style="border-width: 0px; bottom: 0px">	
					<%= g_sPageTargetGroups %>
				</div>
			]]></template>
		</editsheet>
		<editsheet>
		   <global expanded="no">
				<name><%= L_AdAdDisplay_Text %></name>
				<key><%= L_AdAdDisplay_Accelerator %></key>
		   </global>
		   <fields>
				<select id="i_creative_size_id" default='1'>
					<name><%= L_AdSize_Text %></name>
		            <tooltip><%= L_AdSize_ToolTip %></tooltip>
		            <select id="i_creative_size_id">
						<%= g_sCreativeSizeOptions %>
					</select>
		        </select>
				<select id="i_creative_type_id" default='1' onchange="OnChangeCreativeType()">
					<name><%= L_AdType_Text %></name>
		            <tooltip><%= L_AdType_ToolTip %></tooltip>
		            <select id="i_creative_type_id">
						<%= g_sCreativeTypeOptions %>
					</select>
		        </select>
		   </fields>
		   <template fields='i_creative_size_id i_creative_type_id' register="esDisplay listener"><![CDATA[
				<table ID="esAdDisplay2" cellspacing="4" cellpadding="0" class="esTableStyle" STYLE="WIDTH:95%">
					<tr>
						<td class="reqicon"></td>
						<td class="esLabelCell" TITLE="<%= L_AdSize_ToolTip %>"><%= L_AdSize_Text %></td>
						<td ID="i_creative_size_id" class="esDataCell"></td>
					</tr>

					<tr>
						<td class="reqicon"></td>
						<td class="esLabelCell" TITLE="<%= L_AdType_ToolTip %>"><%= L_AdType_Text %></td>
						<td ID="i_creative_type_id" class="esDataCell"></td>
					</tr>
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr>
						<td ID='creativetypecontainer' colspan="3" style="BORDER: inset 1px; PADDING: 2px; MARGIN-TOP: 0">
							<%= g_sCreativeTypes %>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<input id='btnPreview' type="button" CLASS="bdbutton" style="MARGIN: 4px" LANGUAGE="VBScript" ONCLICK="PreviewAd" 
								TITLE='<%= L_CMPreview_Button %>' value="<%= L_CMPreview_Button %>" disabled>
						</td>
					</tr>
					</table>
					<div ID="esDisplay" CLASS="editSheet" MetaXML="DisplayMeta" DataXML="emptyData"></div>
				</div>
		   ]]></template>
		</editsheet>
	</editsheets>
</xml>		 

<!--*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****-->

<!-- Ad Group DATA -->
<xml id="esAdData">
	<?xml version="1.0" ?>
	<%= g_sAdXMLData %>
</xml> 

<!-- empty record -->
<xml id="emptyData">
	<?xml version="1.0" ?>
	<document><record/></document>
</xml>

<!--******DISPLAY******DISPLAY******DISPLAY******DISPLAY******DISPLAY******DISPLAY******-->

<div ID="bdcontentarea">

	<form ID="saveform" METHOD="post" ONTASK="OnTaskSave()">
		<input TYPE="hidden" NAME="type">
		<input TYPE="hidden" NAME="i_customer_id" VALUE="<%= g_dFormValues("i_customer_id") %>">
		<input TYPE="hidden" NAME="i_camp_id" VALUE="<%= g_dFormValues("i_camp_id") %>">	
		<input TYPE="hidden" NAME="i_campitem_id" VALUE="<%= g_dformValues("i_campitem_id") %>">	
		<input TYPE="hidden" NAME="u_customer_name" VALUE="<%= g_dformValues("u_customer_name") %>">	
		<input TYPE="hidden" NAME="u_camp_name" VALUE="<%= g_dformValues("u_camp_name") %>">	
		<input TYPE="hidden" NAME="campitem_type" VALUE="<%= GUID_TYPE_AD %>">	
		<input TYPE="hidden" NAME="dbaction" VALUE="<%= g_dFormValues("dbaction") %>">
		<input TYPE="hidden" Name="PageSelect">
		<input TYPE="hidden" Name="TargetSelect">
		<INPUT TYPE='hidden' NAME='b_camp_level_goal' VALUE='<%= g_dFormValues("b_camp_level_goal") %>'>
		<INPUT TYPE='hidden' NAME='u_customer_url' VALUE='<%= g_dFormValues("u_customer_url") %>'>
		<input TYPE="hidden" NAME="i_creative_id" VALUE="<%= g_dFormValues("i_creative_id") %>">	
<%	Select Case g_dFormValues("type")
		Case "add", "copy"
%>		<input TYPE="hidden" NAME="requires_checkpoint" VALUE="1">	
<%		Case else
%>		<input TYPE="hidden" NAME="requires_checkpoint" VALUE="0">	
<%	end select
%>		<div ID="esMaster" CLASS="editSheet" MetaXML="MasterMeta" DataXML="esAdData" LANGUAGE="VBScript" ONCHANGE='setDirty("")' ONREQUIRE='setRequired("")' ONVALID='setValid("")'><h3><%= g_imgLoadingAnimation %> <%= L_AdLoadingProperties_Text %></h3></div>
	</form>
</div>

</body>
</html>
