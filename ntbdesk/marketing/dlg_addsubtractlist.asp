<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->

<%
dim g_sCampaignConnectionStr
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

dim g_nListID, g_sListData, g_sListName, g_sDefaultList
g_nListID = request("list_id")

g_sListData = sGetListData

Function sGetListData()
	dim oLM, sText
	dim rsListPrimary, fdListId, fdListStatus, fdListName, fdListFlags
		
	sText = ""
	Set oLM = Server.CreateObject("Commerce.ListManager")
	
	oLM.Initialize g_sCampaignConnectionStr
	set rsListPrimary = oLM.GetLists 					
	set fdListId = rsListPrimary("list_id")
	set fdListStatus = rsListPrimary("list_status")
	set fdListName = rsListPrimary("list_name")
	set fdListFlags = rsListPrimary("list_flags")
	g_sDefaultList = ""
	do while not rsListPrimary.EOF
		if g_nListID =  fdListId.value then g_sListName = fdListName.value	'get name of current list
		if g_nListID <> fdListId.value and _
			(fdListFlags.value And  MAILABLELIST) = MAILABLELIST and _
			Not ((fdListFlags.value And  HIDDENLIST) = HIDDENLIST) and _
			fdListStatus.value <> LM_STATUS_PENDING and _
			fdListStatus.value <> LM_STATUS_FAILED then 
			if g_sDefaultList = "" then g_sDefaultList = fdListId.value
			sText = sText & "<option value='" & fdListId.value & "'>" & fdListName.value & "</option>"
		end if
		rsListPrimary.Movenext
	Loop
	set oLM = nothing
	sGetListData = sText
end function

function Quote(sText)
	Quote = replace(sText, "'", "''")
end function
%>

<html>
<head>
<title><%=L_AddSubList_Text%></title>
<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">
<style>
HTML
{
    WIDTH: 440px;
    HEIGHT: 310px;
}
BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
BUTTON
{
	WIDTH: 6em;
}
</style>
<script language="vbscript">
<!--
	dim statechange
	
 	Sub bdcancel_OnClick()
		window.returnvalue = -1
		window.close()	
	end Sub

	sub bdcontinue_OnClick() 			
		dim aReturnValues(3)
		
		'get the list action, 'add' or 'subtract'
		aReturnValues(0) = list_action.value
		'get the id, name and description
		aReturnValues(1)  = list_id.value
		aReturnValues(2) = list_name.value
		aReturnValues(3) = list_description.value

		window.returnvalue = Join(aReturnValues, "|||")
		window.close()
	end sub
 
	Sub OnChange()
		on error resume next
		bdcontinue.disabled = not (list_name.valid and not list_name.required)
	End sub	
		

	Sub window_onload()
		list_action.focus()
	end sub
 '-->
</script> 
</head>

<body SCROLL="no" LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<xml id="efMeta">
	<fields>
	  <text id="orig_list_name" subtype="short" default='<%= Quote(g_sListName) %>' readonly="yes"/>
	  <text id="list_name" maxlen="64" subtype="short" required="yes" onchange='onChange()' onrequire='onChange()'>
			<error><%= L_ListnameLong_ErrorMessage %></error>
			<charmask>.*</charmask>
	  </text>
	  <text id="list_description" maxlen="256" subtype="long">
			<error><%= L_ListdescLong_ErrorMessage %></error>
			<charmask>(\S*\s*\S*)*</charmask>
	  </text>
	  <select id="list_action" default="add">
		<select id="list_action">
			<option value="add"><%= L_Operation1_Text %></option>
			<option value="subtract"><%= L_Operation2_Text %></option>
		</select>
	  </select>
	  <select id="list_id" default="<%= g_sDefaultList %>">
		<select id="list_id">
			<%= g_sListData %>
		</select>
	  </select>
	</fields>
</xml>

<xml id="efData">
	<document/>
</xml>

<table cellpadding="1" cellspacing="1" width="100%" border="0" style="TABLE-LAYOUT: fixed">
	<colgroup>
		<col width="40%"><col width="60%">
	</colgroup>
	<tr>
		<td><%= L_CurrentList_Text %> </td>
		<td><div ID="orig_list_name" CLASS="editField" style="width:100%" METAXML="efMeta" DATAXML="efdata"></div></td>
	</tr>
	<tr>
		<td><%= L_Operation_Text %></td>
		<td><div ID="list_action" CLASS="editField" style="width:100%" METAXML="efMeta" DATAXML="efdata"></div></td>
	</tr>
	<tr>
		<td><%= L_TargetList_Text %></td>
		<td><div ID="list_id" CLASS="editField" style="width:100%" METAXML="efMeta" DATAXML="efdata"></div></td>
	</tr>
</table>
<p>
<fieldset ID="savelistas" name="savelistas">
	<legend><%= L_SaveList_Text %></legend>
	<table border="0" cellspacing="1" cellpadding="1" width="100%" style="TABLE-LAYOUT: fixed">
		<tr>
			<td width="40%"><%= L_NewListName_Text %></td>
			<td width="60%"><div ID="list_name" CLASS="editField" style="width:100%" METAXML="efMeta" DATAXML="efdata"></div></td>
		</tr>
		<tr>
			<td valign=top><%=L_Descr_Text%></td>
			<td><div ID="list_description" CLASS="editField" METAXML="efMeta" DATAXML="efdata"></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</fieldset>		
<p>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="100%">
		</td>
		<td><nobr>
			<button ID="bdcontinue" CLASS="bdbutton" DISABLED><%= L_OK_Text%></button>
			<button ID="bdcancel" CLASS="bdbutton"><%=L_Cancel_Text%></button>
			<BUTTON ID='bdhelp' CLASS='bdbutton'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "cs_bd_campaigns_DEZC.htm"'><%= L_Help_Text %></BUTTON>
		</nobr></td>
	</tr>
</table>

</body>
</html>
