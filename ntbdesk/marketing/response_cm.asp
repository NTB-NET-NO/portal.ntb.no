<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE="../include/BizDeskPaths.asp" -->
<!--#INCLUDE FILE="../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="../include/DBUtil.asp" -->
<!--#INCLUDE FILE="include/cm_util.asp" -->
<!--#INCLUDE FILE="include/campitem_util.asp" -->
<!--#INCLUDE FILE="include/marketingStrings.asp" -->
<!--#INCLUDE FILE="include/directMail.asp" -->
<!--#INCLUDE FILE='include/dmitem_db.asp' -->
<%
'page size for ListSheet
const PAGE_SIZE = 10
Dim aDMList, i, g_dRequest, g_sCampaignConnectionStr, g_sDMConnectionString
Dim oDMdbConn, oDMCmd, g_xmlReturn

set g_dRequest = dGetRequestXMLAsDict()

'get connection string and put it in Session:
if isEmpty(Session("Campaign_ConnectionStr")) then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
end if
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")

'get DM connection if deleting or restoring DM item
if g_dRequest("campitem_type") = GUID_TYPE_DM and _
	(g_dRequest("op") = "delete" or g_dRequest("op") = "restore") then
	If IsEmpty(Session("DM_ConnectionStr")) Then
		Session("DM_ConnectionStr") = GetSiteConfigField("Direct Mail", "connstr_db_directmail")
	End If
	g_sDMConnectionString = Session("DM_ConnectionStr")	

	Set oDMdbConn = oGetADOConnection(g_sDMConnectionString)
	Set oDMCmd = oGetADOCommand(oDMdbConn, AD_CMD_TEXT)
	if oDMdbConn.State = AD_STATE_CLOSED then
		set g_xmlReturn = xmlGetXMLDOMDoc()
		AddErrorNode g_xmlReturn, "1", sGetErrorByID("Cant_connect_db"), ""
		'clear errors set in session
		Session("MSCSBDError") = empty
		Response.Write g_xmlReturn.xml
		Response.end
	end if
end if

GetPageGlobals()
select case g_dRequest("op")
	case "delete"
		if CLng(g_dRequest("i_campitem_id")) <> -1 then
			ExecStoredProc "sp_DeleteCampaignItem", "campitem_id", _
				g_dRequest("i_campitem_id"), L_CMUnableToDeleteCampaignItem_ErrorMessage
			if g_dRequest("campitem_type") = GUID_TYPE_DM then
				DisableSQLJob g_dRequest("i_campitem_id"), False
			elseif g_dRequest("campitem_type") = GUID_TYPE_AD Then
				'goaling calculation
				If bGetCampLevelGoal(CLng(g_dRequest("i_camp_id"))) Then
					DistributeTotalImpressions(CLng(g_dRequest("i_camp_id")))
				Else
					UpdateTotalImpressions(CLng(g_dRequest("i_camp_id")))
				End If
			End If	
		elseif CLng(g_dRequest("i_camp_id")) <> -1 then
			ExecStoredProc "sp_DeleteCampaign", "camp_id", _
				g_dRequest("i_camp_id"), L_CMUnableToDeleteCampaign_ErrorMessage
			'disable SQL job of undeleted direct mails of the campaign
			aDMList = aGetAllDMIDsInCampaign(CLng(g_dRequest("i_camp_id")))
			For i = LBound(aDMList, 2) To UBound(aDMList, 2)
				If IsNull(aDMList(1, i)) Then
					DisableSQLJob aDMList(0, i), False 
				End If
			Next
		elseif CLng(g_dRequest("i_customer_id")) <> -1 then
			ExecStoredProc "sp_DeleteCustomer", "cust_id", _
				g_dRequest("i_customer_id"), L_CMUnableToDeleteCustomer_ErrorMessage
			'disable SQL job of undeleted direct mails of the customer
			aDMList = aGetAllDMIDsInCustomer(CLng(g_dRequest("i_customer_id")))
			For i = LBound(aDMList, 2) To UBound(aDMList, 2)
				If IsNull(aDMList(1, i)) Then
					DisableSQLJob aDMList(0, i), False
				End If
			Next
		end if
	case "restore"
		if CLng(g_dRequest("i_campitem_id")) <> -1 then
			ExecStoredProc "sp_UnDeleteCampaignItem", "campitem_id", _
					g_dRequest("i_campitem_id"), L_CMUnableToRestoreCampaignItem_ErrorMessage
			if g_dRequest("campitem_type") = GUID_TYPE_DM then
				DisableSQLJob g_dRequest("i_campitem_id"), True
			elseif g_dRequest("campitem_type") = GUID_TYPE_AD Then
				'goaling calculation
				If bGetCampLevelGoal(CLng(g_dRequest("i_camp_id"))) Then
					DistributeTotalImpressions(CLng(g_dRequest("i_camp_id")))
				Else
					UpdateTotalImpressions(CLng(g_dRequest("i_camp_id")))
				End If
			End If	
		elseif CLng(g_dRequest("i_camp_id")) <> -1 then
			ExecStoredProc "sp_UndeleteCampaign", "camp_id", _
				g_dRequest("i_camp_id"), L_CMUnableToRestoreCampaign_ErrorMessage
			'enable SQL job of undeleted direct mails of the campaign
			aDMList = aGetAllDMIDsInCampaign(CLng(g_dRequest("i_camp_id")))
			For i = LBound(aDMList, 2) To UBound(aDMList, 2)
				If IsNull(aDMList(1, i)) Then
					DisableSQLJob aDMList(0, i), True 
				End If
			Next
		elseif CLng(g_dRequest("i_customer_id")) <> -1 then
			ExecStoredProc "sp_UndeleteCustomer", "cust_id", _
				g_dRequest("i_customer_id"), L_CMUnableToRestoreCustomer_ErrorMessage
			'ensable SQL job of undeleted direct mails of the customer
			aDMList = aGetAllDMIDsInCustomer(CLng(g_dRequest("i_customer_id")))
			For i = LBound(aDMList, 2) To UBound(aDMList, 2)
				If IsNull(aDMList(1, i)) Then
					DisableSQLJob aDMList(0, i), True
				End If
			Next
		end if
	case "sublist"
		Response.Write GetCampaignItems()
	case else
		Response.Write GetListSheetDataIsland()
end select

'~~~~~~~~~~~~~~~~~~~~

function Quote(sText)
	If IsNull(sText) Or IsEmpty(sText) Or sText = "" Then Exit Function
	Quote = replace(sText, "'", "\'")
end function

Function aGetAllDMIDsInCampaign(nCampId)
	Dim oConn, rsQuery, sQuery 
	Set oConn = oGetADOConnection(g_sCampaignConnectionStr)
	Set rsQuery = Server.CreateObject("ADODB.Recordset")
	sQuery = "SELECT i_campitem_id, dt_campitem_archived FROM campaign_item WHERE " &_
		"i_camp_id = " & nCampID & " AND guid_campitem_type = '" & GUID_TYPE_DM & "'"
	Set rsQuery = oConn.Execute(sQuery, , AD_CMD_TEXT)
	aGetAllDMIDsInCampaign = rsQuery.GetRows()
	rsQuery.Close
End Function

Function aGetAllDMIDsInCustomer(nCustomerID)
	Dim oConn, rsQuery, sQuery 
	Set oConn = oGetADOConnection(g_sCampaignConnectionStr)
	Set rsQuery = Server.CreateObject("ADODB.Recordset")
	sQuery = "SELECT ci.i_campitem_id, ci.dt_campitem_archived FROM campaign c INNER JOIN campaign_item ci " &_
		"ON c.i_camp_id = ci.i_camp_id WHERE c.i_customer_id = " & nCustomerID &_
		" AND ci.guid_campitem_type = '" & GUID_TYPE_DM & "'"
	Set rsQuery = oConn.Execute(sQuery, , AD_CMD_TEXT)
	aGetAllDMIDsInCustomer = rsQuery.GetRows()
	rsQuery.Close
End Function

function GetCampaignItems()
    dim dFormat, sSQLText, xmlData3

	const TYPE_AD = "1"
	const TYPE_DM = "2"
	const TYPE_DISC= "3"

	'set format dictionary for sedcond and third queries
	set dFormat = server.CreateObject("commerce.dictionary")
	dFormat.CTo = AD_DATE
	dFormat.CFrom = AD_DATE

    '************************************************** DATA3 *****************
	'get campaign items
	sSQLText = "SELECT cu.i_customer_id, ca.i_camp_id, ci.u_campitem_name, ci.i_campitem_id, " & _
			" (CASE WHEN ci.dt_campitem_archived IS NOT NULL THEN '" & L_CMDeleted_Text & _
			"' WHEN ci.b_campitem_active = 1 THEN '" & L_CMActiveOption_Text & "' " & _
			"ELSE '" & L_CMInactiveOption_Text & "' END) active, ci.dt_campitem_start AS " & _
			"Cfrom, ci.dt_campitem_end AS Cto,  campitem_flag='foo', ci.guid_campitem_type campitem_type, (CASE " & _
			"WHEN ci.guid_campitem_type = '" & GUID_TYPE_AD & "' THEN '" & L_CMTypeAdAbbr_Text & "' " & _
			"WHEN ci.guid_campitem_type = '" & GUID_TYPE_DM & "' THEN '" & L_CMTypeMailAbbr_Text & "' " & _
			"WHEN ci.guid_campitem_type = '" & GUID_TYPE_DISC & "' THEN '" & L_CMTypeDiscountAbbr_Text & "' " & _
			"END) type_abr, " & _
			"(SELECT IsNull(ad.i_aditem_events_scheduled, 0) FROM ad_item AS ad " & _
			"WHERE ci.i_campitem_id = ad.i_campitem_id) AS scheduled, " & _
			"(SELECT IsNull(SUM(i_perftot_served), 0) FROM performance_total AS p " & _
			"WHERE p.i_campitem_id = ci.i_campitem_id AND p.u_perftot_event_name = 'REQUEST' ) AS requests, " & _
			"(SELECT IsNull(SUM(i_perftot_served), 0) FROM performance_total AS p " & _
			"WHERE p.i_campitem_id = ci.i_campitem_id AND p.u_perftot_event_name = 'CLICK' ) AS clicks, " & _
			"click_pct='0', ci.dt_campitem_archived " & _
			"FROM campaign_item AS ci, campaign AS ca, customer as cu " & _
			"WHERE ca.i_camp_id = ci.i_camp_id  AND ca.i_customer_id = cu.i_customer_id " & _
			"AND ci.i_camp_id = " & g_dRequest("i_camp_id")

    if g_sToDate <> "" AND g_sFromDate <> "" then
		' start and end dates
		sSQLText = sSQLText & " AND DATEDIFF(d, ci.dt_campitem_start, CONVERT(datetime,'" & g_sSQLToDate & "',20)) >= 0 " & _
				" AND DATEDIFF(d, CONVERT(datetime,'" & g_sSQLFromDate & "',20), ci.dt_campitem_end) >= 0 "
	end if

	' g_sStatus (0=active, 1=inactive, 2=all)
	if g_sStatus = 0 then ' active
		sSQLText = sSQLText & "AND ci.b_campitem_active = 1 "
	elseif g_sStatus = 1 then	' inactive 
		sSQLText = sSQLText & "AND ci.b_campitem_active = 0 "
	end if	

	' state (0=yes, 1=no, 2=both)
	if g_sState = 0 then
		sSQLText = sSQLText & "AND ci.dt_campitem_archived IS NOT NULL "
	elseif g_sState = 1 then
		sSQLText = sSQLText & "AND ci.dt_campitem_archived IS NULL "
	end if

	' item type (1=Ad, 2=Direct Mail, 3=Discount)
	select case g_sItemType
		case TYPE_AD:
			sSQLText = sSQLText & " AND ci.guid_campitem_type = '" & GUID_TYPE_AD & "' "
		case TYPE_DM:
			sSQLText = sSQLText & " AND ci.guid_campitem_type = '" & GUID_TYPE_DM & "' "
		case TYPE_DISC:
			sSQLText = sSQLText & " AND ci.guid_campitem_type = '" & GUID_TYPE_DISC & "' "
		case else	'all
	end select
	sSQLText = sSQLText & " ORDER BY ci.u_campitem_name"

	set xmlData3 = xmlGetXMLFromQueryEx(g_sCampaignConnectionStr, sSQLText, -1, -1, -1, dFormat)

	GetCampaignItems = xmlData3.xml
end function

function GetErrorAndRollBack(oConn, sErrorText)
	dim oError, sReturn
	sReturn = "<document>"
	for each oError in oConn.Errors
		sReturn = sReturn & "<ERROR ID='0x" & hex(oError.number) & "' SOURCE='" & Quote(sErrorText) & "'>" & _
			"<![CDATA[" & sGetADOError(oError) & "]]></ERROR>"
	next
	GetErrorAndRollBack = sReturn & "</document>"
	'roll back the transaction:
	oConn.RollbackTrans
End function

sub ExecStoredProc(sStoredProc, sParamID, sParamValue, sErrorMessage)
	dim oConn, oCmd
	Set oConn = oGetADOConnection(g_sCampaignConnectionStr)
	Set oCmd = oGetADOCommand(oConn, AD_CMD_STORED_PROC)

	oCmd.CommandText = sStoredProc

	'initialize parameter
	oCmd.Parameters.Append oCmd.CreateParameter(sParamID, AD_INTEGER, AD_PARAM_INPUT, , CLng(sParamValue))

	On Error Resume Next
	oConn.BeginTrans
	oCmd.Execute

	If oConn.Errors.Count > 0 Then
		Response.Write GetErrorAndRollBack(oConn, sErrorMessage)
	Else
		oConn.CommitTrans
		Response.Write GetListSheetDataIsland()
	End If
	On Error Goto 0
end sub
%>