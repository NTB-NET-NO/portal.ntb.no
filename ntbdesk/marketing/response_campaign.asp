<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE="include/marketingStrings.asp" -->
<%
Dim g_dForm, g_sCampaignConnectionStr, g_rsQuery, g_sQuery, g_xmlReturn

Set g_dForm = dGetRequestXMLAsDict()
Set g_xmlReturn = xmlGetXMLDOMDoc()

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
if g_oConn.State = AD_STATE_CLOSED then
	AddErrorNode g_xmlReturn, "1", sGetErrorByID("Cant_connect_db"), ""
	'clear errors set in session
	Session("MSCSBDError") = empty
else
	Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

	Select Case g_dForm("dbaction")
		Case "insert"
			InsertCampaign()
		Case "update"
			UpdateCampaign()
		Case Else
			AddErrorNode g_xmlReturn, "1", "", ""
	End Select
end if

Response.Write g_xmlReturn.xml

Sub RollBackAndReportError(byRef oConn, byVal sErrorText)
	Dim oError
	For Each oError In oConn.Errors
		AddErrorNode g_xmlReturn, "1", sErrorText, "<![CDATA[" & sGetADOError(oError) & "]]>"
	Next
	'roll back the transaction:
	oConn.RollbackTrans
End Sub

Sub InsertCampaign()
	Dim sModifiedBy

	On Error Resume Next
	sModifiedBy = Request.ServerVariables("LOGON_USER")
	With g_oCmd
		.CommandText = "sp_InsertCampaign"
		.CommandType = AD_CMD_STORED_PROC
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("dt_camp_start", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , g_MSCSDataFunctions.ConvertDateString(CStr(g_dForm("dt_camp_start")), g_DBDefaultLocale))
			.Append g_oCmd.CreateParameter("dt_camp_end", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , g_MSCSDataFunctions.ConvertDateString(CStr(g_dForm("dt_camp_end")), g_DBDefaultLocale))
			.Append g_oCmd.CreateParameter("camp_name", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_camp_name")), g_dForm("u_camp_name"))
			.Append g_oCmd.CreateParameter("event_type_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dForm("i_event_type_id")))
			.Append g_oCmd.CreateParameter("camp_events_sched", AD_INTEGER, AD_PARAM_INPUT, , Fix(cDbl(g_dForm("i_camp_events_sched"))))
			.Append g_oCmd.CreateParameter("camp_level_goal", AD_BOOLEAN, AD_PARAM_INPUT, , CBool(g_dForm("b_camp_level_goal")))
			.Append g_oCmd.CreateParameter("customer_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dForm("i_customer_id")))
			.Append g_oCmd.CreateParameter("camp_modified_by", AD_BSTR, AD_PARAM_INPUT, Len(sModifiedBy), sModifiedBy)
			.Append g_oCmd.CreateParameter("camp_active", AD_BOOLEAN, AD_PARAM_INPUT, , CBool(g_dForm("b_camp_active")))
			.Append g_oCmd.CreateParameter("camp_comments", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_camp_comments")), g_dForm("u_camp_comments"))
			.Append g_oCmd.CreateParameter("o_camp_id", AD_INTEGER, AD_PARAM_OUTPUT )
		End With
	End With	
	
	g_oConn.BeginTrans
	g_oCmd.Execute

	if err.number <> 0 then
		AddErrorNode g_xmlReturn, "1", L_CaUnableToSave_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
		Exit Sub
	elseIf g_oConn.Errors.Count > 0 Then
		RollBackAndReportError g_oConn, L_CaUnableToSave_ErrorMessage
		Exit Sub
	End If
	g_oConn.CommitTrans
	On Error Goto 0

	AddItemNode "camp_id", g_oCmd.Parameters("o_camp_id").Value
End Sub

Sub UpdateCampaign()
	Dim sModifiedBy

	On Error Resume Next
	sModifiedBy = Request.ServerVariables("LOGON_USER")
	'if campaign scheduled event total is too large then set to zero and display warning to user
	if cDbl(g_dForm("i_camp_events_sched")) > 2000000000 then
		g_dForm("i_camp_events_sched") = 0
		AddWarningNode g_xmlReturn, L_CaScheduledTooLarge_ErrorMessage
	end if
	With g_oCmd	
		.CommandText = "sp_UpdateCampaign"
		.CommandType = AD_CMD_STORED_PROC
		'initialize parameters
		With .Parameters
			.Append g_oCmd.CreateParameter("Return", AD_INTEGER, AD_PARAM_RETURN_VALUE)
			.Append g_oCmd.CreateParameter("camp_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dForm("i_camp_id")))
			.Append g_oCmd.CreateParameter("dt_camp_start", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , g_MSCSDataFunctions.ConvertDateString(CStr(g_dForm("dt_camp_start")), g_DBDefaultLocale))
			.Append g_oCmd.CreateParameter("dt_camp_end", AD_DB_TIMESTAMP, AD_PARAM_INPUT, , g_MSCSDataFunctions.ConvertDateString(CStr(g_dForm("dt_camp_end")), g_DBDefaultLocale))
			.Append g_oCmd.CreateParameter("camp_name", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_camp_name")), g_dForm("u_camp_name"))
			.Append g_oCmd.CreateParameter("event_type_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dForm("i_event_type_id")))
			.Append g_oCmd.CreateParameter("camp_events_sched", AD_INTEGER, AD_PARAM_INPUT, , Fix(cDbl(g_dForm("i_camp_events_sched"))))
			.Append g_oCmd.CreateParameter("camp_level_goal", AD_BOOLEAN, AD_PARAM_INPUT, , CBool(g_dForm("b_camp_level_goal")))
			.Append g_oCmd.CreateParameter("customer_id", AD_INTEGER, AD_PARAM_INPUT, , CLng(g_dForm("i_customer_id")))
			.Append g_oCmd.CreateParameter("camp_modified_by", AD_BSTR, AD_PARAM_INPUT, Len(sModifiedBy), sModifiedBy)
			.Append g_oCmd.CreateParameter("camp_active", AD_BOOLEAN, AD_PARAM_INPUT, , CBool(g_dForm("b_camp_active")))
			.Append g_oCmd.CreateParameter("camp_comments", AD_BSTR, AD_PARAM_INPUT, Len(g_dForm("u_camp_comments")), g_dForm("u_camp_comments"))
		End With
	End With
	
	g_oConn.BeginTrans
	g_oCmd.Execute

	if err.number <> 0 then
		AddErrorNode g_xmlReturn, "1", L_CaUnableToSave_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
		Exit Sub
	elseIf g_oConn.Errors.Count > 0 Then
		RollBackAndReportError g_oConn, L_CaUnableToSave_ErrorMessage
		Exit Sub
	Else
		UpdateAdItem() 'if needed
		If g_oConn.Errors.Count > 0 Then
			RollBackAndReportError g_oConn, L_CaUnableToSave_ErrorMessage
			Exit Sub
		Else
			g_oConn.CommitTrans
		End If
	End If
	On Error Goto 0

	AddItemNode "camp_id", g_dForm("i_camp_id")
End Sub

Sub UpdateAdItem()
	Dim xmlText, xmlDoc, xmlRecords, xmlRecord
	Dim sQuery, iWeight, iEvents, iAdItemID 
	xmlText = g_dForm("xmlAdItem")
	Set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")		
	If xmlDoc.loadXML(xmlText) Then
		Set xmlRecords = xmlDoc.selectNodes("//document/record")
		For Each xmlRecord In xmlRecords
			With xmlRecord
				iWeight =   CLng(.selectSingleNode("i_aditem_weight").text)
				iEvents =   CLng(.selectSingleNode("i_aditem_events_scheduled").text)
				iAdItemID = CLng(.selectSingleNode("i_aditem_id").text)
				sQuery = "UPDATE ad_item SET i_aditem_weight = " & iWeight & _
					" , i_aditem_events_scheduled = " & iEvents & _
					" WHERE i_aditem_id = " & iAdItemID
				g_oConn.Execute sQuery, , AD_CMD_TEXT
			End With
			If g_oConn.Errors.Count > 0 Then
				RollBackAndReportError g_oConn, L_CaUnableToSave_ErrorMessage
				Exit for
			end if
		Next
	End If
End Sub
%>  