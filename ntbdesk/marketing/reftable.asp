<!-- #INCLUDE FILE='..\include\BDHeader.asp' -->
<!-- #INCLUDE FILE='..\include\BizDeskUtil.asp' -->
<!-- #INCLUDE FILE='include\marketingStrings.asp' -->
<%
Dim g_sCampaignConnectionStr, g_rsQuery, g_sQuery 
Dim g_sContentSizeXMLData, g_sIndustryCodeXMLData, g_sPageGroupXMLData

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	
Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

g_sContentSizeXMLData = sGetContentSizeXMLData()
g_sIndustryCodeXMLData = sGetIndustryCodeXMLData()
g_sPageGroupXMLData = sGetPageGroupXMLData()

'all jobs are done
Set g_rsQuery = Nothing

'******************************************************************************
'	server side functions
'******************************************************************************
Function sGetContentSizeXMLData()
	Dim xmlData
	g_sQuery = "SELECT * FROM creative_size"
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	Set xmlData = xmlGetXMLFromRS(g_rsQuery)
	g_rsQuery.Close 
	sGetContentSizeXMLData = xmlData.xml	
End Function

Function sGetIndustryCodeXMLData()
	Dim xmlData
	g_sQuery = "SELECT * FROM industry_code "
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	Set xmlData = xmlGetXMLFromRS(g_rsQuery)
	g_rsQuery.Close 
	sGetIndustryCodeXMLData = xmlData.xml	
End Function

Function sGetPageGroupXMLData()
	Dim xmlData
	g_sQuery = "SELECT * FROM page_group"
	Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	Set xmlData = xmlGetXMLFromRS(g_rsQuery)
	g_rsQuery.Close 
	sGetPageGroupXMLData = xmlData.xml	
End Function
%>
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<SCRIPT LANGUAGE='VBScript'>
Option Explicit

Sub window_onLoad()
	on error resume next
	elGetTaskBtn("back").focus()
	elGetTaskBtn("back").blur()
End Sub

Sub OnTask()
	Dim xmlDOMDoc, xmlErrorNodes
	With saveform
		.xmlContentSize.value = dtContent.xmlList.xml
		dtContent.Cleanup()
		.xmlIndustryCode.value = dtIndustry.xmlList.xml
		dtIndustry.Cleanup()
		.xmlPageGroup.value = dtPage.xmlList.xml
		dtPage.Cleanup()
	End With	
	Set xmlDOMDoc = xmlPostFormViaXML(saveform)
	HideWaitDisplay()
	EnableTask("back")
	'successful save should return no error node
	Set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
	If xmlErrorNodes.length <> 0 Or xmlDOMDoc.documentElement Is Nothing Then
		'report errors and set status text
		SetStatusText(sFormatString("<%= L_BDSSUnableToSaveX_StatusBar %>", Array("")))
		ShowXMLNodeErrors(xmlErrorNodes)
		if dGetServerState("rtBadDeletes").count > 0 then
			dClearServerState("rtBadDeletes")
			location.reload
		end if
	Else
		SetStatusText(sFormatString("<%= L_BDSSSavedX_StatusBar %>", Array("")))
		esMaster.resetDirty()
		g_bDirty = False
		If parent.g_bCloseAfterSave Then
			parent.g_bCloseAfterSave = False
			elGetTaskBtn("back").click()
		End If
		parent.g_bCloseAfterSave = False
	End If
End Sub
</SCRIPT>
</HEAD>
<BODY>
<%
InsertEditTaskBar L_ReTaskBar_Text, ""
%>
<FORM ID='saveform' ACTION METHOD='post' ONTASK='OnTask()'>
	<INPUT TYPE='hidden' NAME='xmlContentSize'>
	<INPUT TYPE='hidden' NAME='xmlIndustryCode'>
	<INPUT TYPE='hidden' NAME='xmlPageGroup'>
			
<xml id='dtContentSizeMeta'>
	<dynamictable>
		<global required='yes' keycol='u_size_tag' uniquekey='yes' sortbykey='yes' />
		<fields>		
			<numeric id='i_creative_size_id' hide='yes' />
			<text id='u_size_tag' required='yes' maxlen='20' width='20'>
				<name><%= L_ReContentTableColHeadTag_Text %></name>
				<error><%= L_ReContentID_ErrorMessage %></error>
				<charmask>^[^&lt;&gt;\\]*$</charmask>
			</text>						
			<text id='u_size_name' required='yes' maxlen='50' width='50'>	 
				<name><%= L_ReContentTableColHeadName_Text %></name>
				<error><%= L_ReContentName_ErrorMessage %></error>
				<charmask>^[^&lt;&gt;\\]*$</charmask>
			</text>
			<numeric id='i_size_width' required='yes' min='1' max='9999' width='15'>	 
				<name><%= L_ReContentTableColHeadWidth_Text %></name>
				<error><%= L_ReContentWidth_ErrorMessage %></error>
			 </numeric>	
			 <numeric id='i_size_height' required='yes' min='1' max='9999' width='15'>					 
				<name><%= L_ReContentTableColHeadHeight_Text %></name>
				<error><%= L_ReContentHeight_ErrorMessage %></error>
			 </numeric>						
		</fields>
	</dynamictable> 
</xml>

<xml id='dtIndustryCodeMeta'>
	<dynamictable>
		<global required='yes' keycol='u_industry_code' uniquekey='yes' sortbykey='yes' />
		<fields>			
			<numeric id='i_industry_id' hide='yes' />
			<text id='u_industry_code' required='yes' maxlen='50'>
				<name><%= L_ReIndustryTableColHeadIndustryCode_Text %></name>
				<charmask>^[^&lt;&gt;\\]*$</charmask>
				<error><%= L_ReIndustryTableIndustryCode_ErrorMessage %></error>
			</text>	
		</fields>
	</dynamictable> 
</xml>

<xml id='dtPageGroupMeta'>
	<dynamictable>
		<global required='yes' keycol='u_pg_tag' uniquekey='yes' sortbykey='yes' />
		<fields>			
			<numeric id='i_pg_id' hide='yes' />
			<text id='u_pg_tag' required='yes' maxlen='25' width='30'>
				<name><%= L_RePageTableColHeadPageGroup_Text %></name>
				<error><%= L_RePageID_ErrorMessage %></error>
				<charmask>^[^&lt;&gt;\\]*$</charmask>
			</text>						
			<text id='u_pg_description' required='yes' maxlen='255' width='70'>
				<name><%= L_RePageTableColHeadDescription_Text %></name>
			</text>									
		</fields>
	</dynamictable> 
</xml>

<xml id='esMasterMeta'>
	<editsheets>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_ReContentSize_Text %></name>
				<key><%= L_ReContentSize_Accelerator %></key>
		    </global>
		    <template register='dtContent'><![CDATA[
				<DIV ID='dtContent' CLASS='dynamictable'
					MetaXML='dtContentSizeMeta' DataXML='dtContentSizeData'>
				</DIV>		    				
			]]></template>
		</editsheet>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_ReIndustryCode_Text %></name>
				<key><%= L_ReIndustryCode_Accelerator %></key>
		    </global>
		    <template register='dtIndustry'><![CDATA[
				<DIV ID='dtIndustry' CLASS='dynamictable'
					MetaXML='dtIndustryCodeMeta' DataXML='dtIndustryCodeData'>
				</DIV>		    				
			]]></template>
		</editsheet>
		<editsheet>
			<global expanded='yes'>
				<name><%= L_RePageGroup_Text %></name>
				<key><%= L_RePageGroup_Accelerator %></key>
		    </global>
		    <template register='dtPage'><![CDATA[
				<DIV ID='dtPage' CLASS='dynamictable'
					MetaXML='dtPageGroupMeta' DataXML='dtPageGroupData'>
				</DIV>		    				
			]]></template>
		</editsheet>
	</editsheets>
</xml>

<xml id='dtContentSizeData'>
	<?xml version='1.0' ?>
	<%= g_sContentSizeXMLData %>
</xml>

<xml id='dtIndustryCodeData'>
	<?xml version='1.0' ?>
	<%= g_sIndustryCodeXMLData %>
</xml>

<xml id='dtPageGroupData'>
	<?xml version='1.0' ?>
	<%= g_sPageGroupXMLData %>
</xml>

<DIV ID='bdContentArea'>
	<DIV ID='esMaster' CLASS='editSheet'
		LANGUAGE='VBScript' ONVALID='SetValid("")' ONCHANGE='SetDirty("")' ONREQUIRE='SetRequired("")'
		MetaXML='esMasterMeta' DataXML='dtContentSizeData'><h3><%= g_imgLoadingAnimation %> <%= L_ReLoadingProperty_Text %></h3>
	</DIV>
</DIV>

</FORM>

</BODY>
</HTML>