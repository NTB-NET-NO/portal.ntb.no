<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!-- #INCLUDE FILE='..\include\BizDeskUtil.asp' -->
<!-- #INCLUDE FILE='include\marketingStrings.asp' -->
<!-- #INCLUDE FILE='include\campitem_util.asp' -->
<!-- #INCLUDE FILE='include\campitem_util.htm' -->
<%
Dim g_sCampaignConnectionStr, g_ExpStore_ConnectionStr, g_dFormValues, g_rsQuery, g_sQuery, _
	g_nDayMask, g_bSuccess, g_oExprStore, _
	g_sCreativeTypes, g_sCreativeTypesXML, g_sStatusText, g_nCampItemID, g_sDiscountScheduleDataSet

const DISCOUNTS_EXPR_CATALOG = "Catalog"
const DISCOUNTS_EXPR_CAMPAIGNS = "CAMPAIGNS"
const DISCOUNT_EXPR_TABLE  = "order_discount_expression"
const DISC_EXPR_ID_COL = "i_discexpr_expr"

'constants for building tables
const TABLE_START = "<TABLE WIDTH='95%' BORDER='0' CELLSPACING='0' CELLPADDING='0'>"
const TABLE_END = "</TABLE>"
const ROW_START = "<TR>"
const ROW_END = "</TR>"
const EMPTY_CELL = ""
const LABEL_ONLY = null
const AUTO_FIT_WIDTH = ""

'--GET CONNECTION STRINGS
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	
if IsEmpty(Session("ExpStore_ConnectionStr")) Then
	Session("ExpStore_ConnectionStr") = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")
end if
g_ExpStore_ConnectionStr = Session("ExpStore_ConnectionStr")	

'get global expression store object
set g_oExprStore = Server.CreateObject("Commerce.ExpressionStore")
call g_oExprStore.Connect(g_ExpStore_ConnectionStr)

Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
Set g_rsQuery = Server.CreateObject("ADODB.Recordset")

'--GET FORM values
set g_dFormValues = GetFormValues()

'fills g_sCreativeTypesXML and g_sCreativeTypes
GetCreativeTypeProperties(GUID_TYPE_DISC)

g_sDiscountScheduleDataSet = sGetDiscountScheduleDataSet()

'~~~~~~~~~~~end if inline code - begin of functions~~~~~~~~~~~~~~~~~~~~

Function SQLQuote(sText)	
	If IsNull(sText) Or IsEmpty(sText) Or sText = "" Then Exit Function
	SQLQuote = Replace(sText, "'", "''")
End function

function GetFormValues()
	dim dFormValues
	set dFormValues = Server.CreateObject ("Commerce.Dictionary")

	dFormValues("i_customer_id") = cLng(request("i_customer_id"))
	dFormValues("i_camp_id") = cLng(request("i_camp_id"))
	dFormValues("type") = request("type")
	Select Case dFormValues("type")
		Case "add"
			dFormValues("i_campitem_id") = -1
			dFormValues("dbaction") = "insert"
		Case "copy"
			dFormValues("i_campitem_id") = CLng(Request.Form("i_campitem_id"))
			dFormValues("dbaction") = "insert"
		Case "open"
			dFormValues("i_campitem_id") = CLng(Request.Form("i_campitem_id"))
			dFormValues("dbaction") = "update"
	End Select
	dFormValues("save") = request("save")
	dFormValues("savenew") = request("savenew")		

	dFormValues("i_creative_id") = -1
	
	' Copy from some other discount or open
	If dFormValues("i_campitem_id") > 0 Then		
		'get the creative_type from creative table for this campaign item
		g_sQuery = "SELECT cu.u_customer_name, cu.u_customer_url, ca.u_camp_name, ci.u_campitem_name, " & _
					" creative.i_creative_id, i_creative_type_id, i_creative_size_id, dt_creative_received " & _
					" FROM customer AS cu, campaign AS ca, campaign_item AS ci  INNER JOIN creative ON " & _
					" ci.i_creative_id = creative.i_creative_id " & _
					" AND ci.i_campitem_id = " & dFormValues("i_campitem_id") & _
					" WHERE ci.i_camp_id = ca.i_camp_id AND ca.i_customer_id = cu.i_customer_id "	

		Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
		if not g_rsQuery.EOF then
			dFormValues("i_creative_id") =			cLng(g_rsQuery("i_creative_id").value)
			dFormValues("i_creative_type_id") =		cLng(g_rsQuery("i_creative_type_id").value)
			dFormValues("i_creative_size_id") =		cLng(g_rsQuery("i_creative_size_id").value)
			dFormValues("dt_creative_received") =	g_rsQuery("dt_creative_received").value
			dFormValues("u_customer_name") =		g_rsQuery("u_customer_name")
			dFormValues("u_camp_name") =			g_rsQuery("u_camp_name")
			dFormValues("u_campitem_name") =		g_rsQuery("u_campitem_name")
			dFormValues("u_customer_url") =			g_rsQuery("u_customer_url")
		end if
		g_rsQuery.close

		'get the condition and award type from order_discount_misc table
		g_sQuery = "SELECT odm.i_disc_cond_type, odm.i_disc_award_type, odm.i_disc_id " & _
					" FROM order_discount_misc odm INNER JOIN order_discount od " & _
					" ON od.i_disc_id = odm.i_disc_id " & _
					" AND od.i_campitem_id = " & dFormValues("i_campitem_id")
	
		Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
		if not g_rsQuery.EOF then
			dFormValues("i_disc_cond_type") =	g_rsQuery("i_disc_cond_type")
			dFormValues("i_disc_award_type") =	g_rsQuery("i_disc_award_type")		
			dFormValues("i_disc_id") =			g_rsQuery("i_disc_id")
		end if
		g_rsQuery.close

		REM - Get the discount_id, cond_expr_id and award_expr_id from order_discount table
		g_sQuery = "Select i_disc_id,i_disc_cond_expr, i_disc_award_expr FROM order_discount WHERE i_campitem_id = " & dFormValues("i_campitem_id")
		Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
	
		If not g_rsQuery.EOF then
			dFormValues("i_disc_id") = g_rsQuery("i_disc_id")
			If IsNull(g_rsQuery("i_disc_cond_expr")) then
				dFormValues("i_disc_cond_expr") = -1
			Else	
				dFormValues("i_disc_cond_expr") = g_rsQuery("i_disc_cond_expr")
			End If
					
			If IsNull(g_rsQuery("i_disc_award_expr")) then
				dFormValues("i_disc_award_expr") = -1
			Else
				dFormValues("i_disc_award_expr") = g_rsQuery("i_disc_award_expr")
			End if
		End If
		g_rsQuery.close

		REM - Make call to get expr namer using the ID
		if dFormValues("i_disc_cond_expr") > 0 then
			dFormValues("u_disc_cond_expr") = g_oExprStore.GetExprName(CLng(dFormValues("i_disc_cond_expr")))	
		end if
		if dFormValues("i_disc_award_expr") > 0 then
			dFormValues("u_disc_award_expr") = g_oExprStore.GetExprName(Clng(dFormValues("i_disc_award_expr")))
		end if
	Else
		'get the creative_type from creative table for this campaign item
		g_sQuery = "SELECT u_customer_name, u_customer_url " & _
					" FROM customer " & _
					" WHERE i_customer_id = " & dformValues("i_customer_id")

		Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
		if not g_rsQuery.EOF then
			dFormValues("u_customer_name") =	g_rsQuery("u_customer_name")
			dFormValues("u_customer_url") =		g_rsQuery("u_customer_url")
		end if
		g_rsQuery.close
		dformValues("u_camp_name") = Request("u_camp_name")
		'during an add, default is that all days are selected
		dFormValues("i_creative_type_id") = 1
		dFormValues("i_creative_size_id") = 1		
		'get the expression id for Condition and award
		'for new discount set the expression id to -1
		dFormValues("i_disc_cond_expr") = -1
		dFormValues("i_disc_award_expr") = -1
		'Set the default to be all products for  disc condition and award condition type
		dFormValues("i_disc_cond_type") = 0
		dFormValues("i_disc_award_type") = 0
	End if
	set GetFormValues = dFormValues
End function

function sGetExprOptions()
	'TO DO: CHANGE THESE PICKERS TO SELECTS WITH BROWSE
	Dim sReturn, rsExpression, fdExpressionID, fdExpressionName

	'Get the product expressions from the expression store	
	Set rsExpression = g_oExprStore.Query( ,DISCOUNTS_EXPR_CATALOG)
	If Err.number <> 0 Then
		SetError L_DiscCampManagerErrorBubble_ErrorMessage, L_DiscErrorGettingExpressions_ErrorMessage, sGetScriptError(Err), ERROR_ICON_ALERT
	End if

	set fdExpressionID = rsExpression("exprid")
	set fdExpressionName = rsExpression("exprname")
	sReturn = ""
	while NOT rsExpression.Eof
		sReturn = sReturn & "<option value='" & fdExpressionID.value & "'>" & _
					fdExpressionName.value & "</option>"
		rsExpression.MoveNext
	Wend
	sGetExprOptions = sReturn
End function

'****************************************************************************************
' sGetTargetGroupList3 function
'
'****************************************************************************************
function sGetTargetGroupList3(ByVal iDiscountID)
	Dim sReturn, rsExpression, rsDiscountRel, fdExpressionID, fdExpressionName

	'Get all the expression from the expression store	
	Set rsExpression = g_oExprStore.Query( , DISCOUNTS_EXPR_CAMPAIGNS)
	if  Err.number <> 0  Then
		SetError L_DiscCampManagerErrorBubble_ErrorMessage, L_DiscErrorGettingExpressions_ErrorMessage, sGetScriptError(Err), ERROR_ICON_ALERT
	End If

	set fdExpressionID = rsExpression("exprid")
	set fdExpressionName = rsExpression("exprname")
	'if the discountID is set  then set the filter
	sReturn = ""
	if iDiscountID <> 0 Then
		'open the RecordSet for the Expression already selected
		'Read  the Expression
		Set rsDiscountRel =  Server.CreateObject("ADODB.Recordset")

		rsDiscountRel.Filter = "i_disc_id = " & iDiscountID   
		rsDiscountRel.Open DISCOUNT_EXPR_TABLE, g_sCampaignConnectionStr, AD_OPEN_FORWARD_ONLY

		while NOT rsExpression.EOF
			'Before adding to the list check whether it was already selected
			if  NOT bIsIDPresent(rsDiscountRel, fdExpressionID.value) Then
				sReturn = sReturn & "<div VALUE='" & fdExpressionID.value & "'>" & _
						 fdExpressionName.value & "</div>"
			End if
			rsExpression.MoveNext
		Wend
		rsDiscountRel.Close
	Else
		'Its a new Discount so nothing will be selected
		while not rsExpression.Eof
			sReturn = sReturn & "<div VALUE='" & fdExpressionID.value & "'>" & _
					fdExpressionName.value & "</div>"
			rsExpression.MoveNext
		Wend
	End If
	sGetTargetGroupList3 = sReturn
End function

Function bIsIDPresent(ByRef rsDiscountRel, ByRef iExprID)
	dim fdIDCol
	'initialize the value to FALSE(ID Not found)
	bIsIDPresent = false

	on error resume next
	rsDiscountRel.MoveFirst
 
	if rsDiscountRel.Eof Then
		Err.Clear
		Exit Function
	End If

	set fdIDCol = rsDiscountRel(DISC_EXPR_ID_COL)
	while Not rsDiscountRel.Eof
		if fdIDCol.Value = iExprID Then
			bIsIDPresent = true
			Exit Function
		End If
	
		rsDiscountRel.MoveNext()
	Wend
End Function

Function sGetTargetSelectedList3(BYVal  iDiscountID)
	Dim sReturn, rsDiscountRel, rsExpression, fdExpressionID
	
	'Read  the Expression which are already selected 
	Set rsDiscountRel =  Server.CreateObject("ADODB.Recordset")
	
	'Requery to make sure that only required records are deleted
	rsDiscountRel.Filter = "i_disc_id = " & iDiscountID   

	rsDiscountRel.Open DISCOUNT_EXPR_TABLE, g_sCampaignConnectionStr, AD_OPEN_FORWARD_ONLY
		
	sReturn = ""
	set fdExpressionID = rsDiscountRel(DISC_EXPR_ID_COL)
	While Not rsDiscountRel.Eof
		Set rsExpression = g_oExprStore.GetExpression(fdExpressionID.Value)
		sReturn = sReturn & "<div VALUE=" &  fdExpressionID.Value & _
				">" & rsExpression("exprname").Value & "</div>"

		Set rsExpression = nothing
		rsDiscountRel.MoveNext
	Wend

	rsDiscountRel.Close()
	sGetTargetSelectedList3 = sReturn	
End Function	

function sGetMasterData()
	dim xmlDoc, xmlData, xmlRecord, xmlNode
	'main ad XML data must be munged a bit before setting in data island
	Select Case g_dFormValues("type")
		Case "copy"
			'when copying make name empty string
			g_sQuery = "SELECT u_campitem_name = '', "
		Case Else
			g_sQuery = "SELECT campaign_item.u_campitem_name, "
	End Select 
	g_sQuery = g_sQuery & "campaign_item.i_campitem_id, CAST(campaign_item.b_campitem_active AS int) b_campitem_active, " & _
			"campaign_item.dt_campitem_archived, campaign_item.i_campitem_exposure_limit, " & _
			"creative.dt_creative_received, campaign_item.u_campitem_comments " & _
			"FROM campaign_item INNER JOIN " & _
			"creative ON campaign_item.i_creative_id = creative.i_creative_id " & _
			"WHERE campaign_item.i_campitem_id = " & g_dFormValues("i_campitem_id")

	set xmlData = xmlGetXMLFromQuery(g_sCampaignConnectionStr, g_sQuery)
	set xmlDoc = xmlData.ownerDocument
	set xmlRecord = xmlData.firstChild

	'create i_creative_type_id node and set value
	set xmlNode = xmlRecord.appendChild(xmlDoc.createElement("i_creative_type_id"))
	xmlNode.text = g_dFormValues("i_creative_type_id")

	'create i_creative_size_id node and set value
	set xmlNode = xmlRecord.appendChild(xmlDoc.createElement("i_creative_size_id"))
	xmlNode.text = g_dFormValues("i_creative_size_id")
	
	sGetMasterData = replace(xmlData.xml, "</record>", sGetDiscountDefinitionDataSet() & "</record>")
end function

function sGetEventEnable()
	dim sEventEnable
	'Determine whether Requests/Clicks are enabled when opening or adding a new item.
	sEventEnable = ""
	If cBool(g_dFormValues("camp_level_goal")) Then		'Goal at Ad Level
		If g_dFormValues("type") <> "add" and g_dFormValues("aditem_type") <> 1 Then
			'House Ad
			sEventEnable = " disabled='yes' "
		End If
	Else
		'Goal at Campaign Level - by default the it is a Paid Ad, hence enable Event Input only
		sEventEnable = " disabled='yes' "
	End IF
	sGetEventEnable = sEventEnable
end function

function sGetTitle()
	Dim sCampitemName, sTitle
 
	'Copy or new case	
	If g_dFormValues("type") <> "open" then	
		g_sQuery = "SELECT cu.u_customer_name, ca.u_camp_name FROM Customer as cu, Campaign as ca " & _
					" WHERE ca.i_camp_id = " & g_dformvalues("i_camp_id") & _
					" AND cu.i_customer_id = ca.i_customer_id " 	
		Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
		If not g_rsQuery.EOF then
		   g_dFormValues("u_customer_name") =	g_rsQuery("u_customer_name").Value 
		   g_dFormValues("u_camp_name") =		g_rsQuery("u_camp_name").Value 
		   sCampitemName =	"" 
		End if
	Else 'edit  
		g_sQuery = "SELECT cu.u_customer_name, ca.u_camp_name, ci.u_campitem_name " & _
					"FROM Customer AS cu, Campaign AS ca, " & _
					" campaign_item AS ci WHERE ca.i_camp_id = " & _
					g_dformvalues("i_camp_id") & " AND cu.i_customer_id = ca.i_customer_id " & _
					"AND ci.i_campitem_id = " & g_dformvalues("i_campitem_id")
 
		Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)
		If not g_rsQuery.EOF then
		   g_dFormValues("u_customer_name") =	g_rsQuery("u_customer_name").Value 
		   g_dFormValues("u_camp_name") =		g_rsQuery("u_camp_name").Value 
		   sCampitemName =						g_rsQuery("u_campitem_name").Value 
		End if
	End If
 
	'INSERTING TASKBAR
	Select case g_dformvalues("type")
		case "add", "copy"
			sTitle =  sFormatString(L_DiscCampItemPageTitleNew_Text, Array(g_mscsPage.HTMLEncode(g_dFormValues("u_customer_name")), g_mscsPage.HTMLEncode(g_dFormValues("u_camp_name")))) 
	    case else
			sTitle =  sFormatString(L_DiscCampItemPageTitle_Text, Array(g_mscsPage.HTMLEncode(g_dFormValues("u_customer_name")), g_mscsPage.HTMLEncode(g_dFormValues("u_camp_name")), sCampitemName, g_dFormValues("i_campitem_id"))) 
	End select
	g_rsQuery.Close 

	sGetTitle = sTitle
End function

Function sGetDiscountItemDataSet()
	'Getting archived value	
	dim  sCheck, xmlData
 
	'check to see if the changed customer name exists.
	g_sQuery = " SELECT dt_campitem_archived FROM campaign_item " & _
				" WHERE i_campitem_id = " & g_dformValues("i_campitem_id") 
	set g_rsQuery = Server.CreateObject("ADODB.Recordset")
	g_rsQuery.Open g_sQuery, g_oConn, AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY, AD_CMD_TEXT
	
	if not g_rsQuery.EOF Then
		if IsNull(g_rsQuery("dt_campitem_archived").value) then
			sCheck = L_CMUnarchived_Text  
		else
	        sCheck = L_CMArchived_Text 
	    end if
	end if
	g_rsQuery.close

	Select Case g_dformValues("type")
		Case "copy"
			g_sQuery = " SELECT ca.u_camp_name, i_campitem_id = '', " & _
					   "u_campitem_name = '', CAST(b_campitem_active AS int) b_campitem_active, dt_campitem_archived = " & _
					   "'" & SQLQuote(L_CMUnarchived_Text) & "', i_campitem_exposure_limit, u_disc_description, "
		Case "add"
			sGetDiscountItemDataSet = "<?xml version='1.0' ?><document><record>" & _
					   "<u_campitem_id/><u_campitem_name/><b_campitem_active>" & _
					   "0</b_campitem_active><dt_campitem_archived>" & L_CMUnarchived_Text & "</dt_campitem_archived>" & _
					   "<i_campitem_exposure_limit>0</i_campitem_exposure_limit>" & _
					   "<u_disc_description/><dt_creative_received/><i_disc_rank>10</i_disc_rank>" & _
					   "<u_campitem_comments/></record></document>"
		Case Else
			g_sQuery = " SELECT cu.u_customer_name, ca.u_camp_name, ci.i_campitem_id, " & _
						"ci.u_campitem_name, CAST(b_campitem_active AS int) b_campitem_active, dt_campitem_archived = '" & _
						SQLQuote(sCheck) & "', i_campitem_exposure_limit, u_disc_description, "
	End Select

	If g_dformValues("type") <> "add" Then
		g_sQuery = g_sQuery & " dt_creative_received, i_disc_rank, u_campitem_comments "  & _
					" FROM Customer AS cu, Campaign AS ca, order_discount INNER JOIN campaign_item AS ci ON " & _
					" order_discount.i_campitem_id = ci.i_campitem_id INNER JOIN creative ON " & _
					" ci.i_creative_id = creative.i_creative_id  " & _
					" WHERE ci.i_campitem_id = " & g_dformValues("i_campitem_id") & _
					" AND cu.i_customer_id = ca.i_customer_id AND ci.i_camp_id = ca.i_camp_id "
 
 		set xmlData = xmlGetXMLFromQuery(g_sCampaignConnectionStr, g_sQuery)
		sGetDiscountItemDataSet = xmlData.xml
	End If
End Function

Function sGetDiscountScheduleDataSet()
	Dim xmlData, dFormat

	set dFormat = server.CreateObject("commerce.dictionary")
	dFormat("dt_campitem_start") = AD_DATE
	dFormat("dt_campitem_end") = AD_DATE
	if g_dformvalues("type") = "add" then
	'get the default values form Campaign it will belong to
		g_sQuery = " SELECT dt_camp_start as dt_campitem_start, " & _
				   "dt_camp_end as dt_campitem_end FROM Campaign " & _
				   "WHERE i_camp_id = " & g_dformValues("i_camp_id")
	else
		g_sQuery = " SELECT ci.dt_campitem_start, ci.dt_campitem_end " & _
				   "FROM campaign_item as ci " & _
				   "WHERE ci.i_campitem_id  = " & g_dformValues("i_campitem_id")						
	end if
	set xmlData = xmlGetXMLFromQueryEx(g_sCampaignConnectionStr, g_sQuery, -1, -1, -1, dFormat)
	g_dFormValues("dt_campitem_start") = xmlData.selectSingleNode("//dt_campitem_start").text
	g_dFormValues("dt_campitem_end") = xmlData.selectSingleNode("//dt_campitem_end").text
	sGetDiscountScheduleDataSet = xmlData.xml
End Function

Function sGetDiscountDefinitionDataSet()
	Dim xmlData, sDiscMaxAward

 	If g_dformvalues("type") <> "add" Then
		'check to see if the changed customer name exists.
		g_sQuery = "SELECT i_disc_award_max " & _
				" FROM creative INNER JOIN campaign_item ON " & _
				"creative.i_creative_id = campaign_item.i_creative_id INNER JOIN " & _
				"order_discount ON campaign_item.i_campitem_id = order_discount.i_campitem_id " & _
				"INNER JOIN order_discount_misc ON order_discount.i_disc_id = " & _
				"order_discount_misc.i_disc_id " & _
				"WHERE campaign_item.i_campitem_id = " & g_dformvalues("i_campitem_id")	
		
		g_rsQuery.Open g_sQuery, g_oConn, AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY, AD_CMD_TEXT
		if not g_rsQuery.EOF  Then
			if g_rsQuery("i_disc_award_max").value = 0 then
				sDiscMaxAward = L_DiscAwardMaxDefault_Text
			else
				sDiscMaxAward = g_rsQuery("i_disc_award_max").value
			end if
		End If
		g_rsQuery.close
	End If

	If g_dformvalues("type") = "add" Then
		sGetDiscountDefinitionDataSet =  _
				 "<dt_creative_received/><i_disc_cond_amt>1</i_disc_cond_amt>" & _
	 			"<b_disc_click_required/><i_disc_template/><i_disc_cond_expr/>" & _
	 			"<i_disc_cond_basis>1</i_disc_cond_basis><i_disc_award_expr/>" & _
				"<i_disc_limit>0</i_disc_limit><i_disc_award_max>" & L_DiscAwardMaxDefault_Text & "</i_disc_award_max>" & _
	 			"<i_disc_offer_type>1</i_disc_offer_type><mny_disc_offer_value>1</mny_disc_offer_value>" & _
	 			"<QualifyingExprList>x0</QualifyingExprList><AwardExprList>x0</AwardExprList><i_disc_award_type>0</i_disc_award_type>"
	else
		g_sQuery = " SELECT dt_creative_received, IsNull(cast(i_disc_qty_cond_min as money), " & _
	 			"mny_disc_cond_min)  AS i_disc_cond_amt, b_disc_click_required, i_disc_limit, i_disc_template, " & _
	 			"i_disc_cond_expr, i_disc_cond_basis, i_disc_award_expr, i_disc_award_max = '" & _
	 			sDiscMaxAward & "' , i_disc_offer_type, mny_disc_offer_value, QualifyingExprList=1, AwardExprList=1 " & _
	 			"FROM creative INNER JOIN campaign_item ON " & _
	 			"creative.i_creative_id = campaign_item.i_creative_id INNER JOIN " & _
	 			"order_discount ON campaign_item.i_campitem_id = order_discount.i_campitem_id " & _
	 			"INNER JOIN order_discount_misc ON order_discount.i_disc_id = order_discount_misc.i_disc_id " & _
				" WHERE campaign_item.i_campitem_id = " & g_dformvalues("i_campitem_id")	
		
		set xmlData = xmlGetXMLFromQuery(g_sCampaignConnectionStr, g_sQuery)
		if g_dformvalues("i_disc_cond_expr") = - 1 then
			xmlData.selectSingleNode("//QualifyingExprList").text = "x0"
		else
			xmlData.selectSingleNode("//QualifyingExprList").text = g_dformvalues("i_disc_cond_expr")
		end if
		if g_dformvalues("i_disc_award_expr") = - 1 then
			xmlData.selectSingleNode("//AwardExprList").text = "x0"
		else
			xmlData.selectSingleNode("//AwardExprList").text = g_dformvalues("i_disc_award_expr")
		end if
		sGetDiscountDefinitionDataSet =  replace(replace(xmlData.firstChild.xml, "<record>", ""), "</record>", "")
	end if
End Function

sub GetDiscDefCell(sField, nWidth)
	dim sCellDisplay, sFieldDisplay

	if isNull(nWidth) then
		Response.Write "<TD NOWRAP>" & sField & "</TD>"
	else
		sCellDisplay = ""
		sFieldDisplay = ""
		select case sField
			case "b_no_requirement", "b_freeShipping"
				sCellDisplay = " COLSPAN=4"
				sFieldDisplay = " STYLE='BORDER: none; BACKGROUND-COLOR: transparent'"
		end select
		Response.Write "<TD NOWRAP VALIGN='top'" & sCellDisplay & " WIDTH='" & nWidth & "'>"
		Response.Write "<span ID='" & sField & "'" & sFieldDisplay & ">" & L_BDSSLoadingField_Text & "</SPAN>"
		Response.Write "</TD>"
	end if
End sub

sub GetFormattedRow(aCells)
	dim x
	Response.Write ROW_START
	for x = LBOUND(aCells) to UBOUND(aCells) step 2
		GetDiscDefCell aCells(x), aCells(x + 1)
	next
	Response.Write "<TD width='100%'></TD>" & ROW_END
End sub

sub GetLabelRow(sLabel)
	response.write ROW_START
	response.write "<TD NOWRAP COLSPAN='4'>" & sLabel & "</TD>"
	response.write ROW_END
End sub

sub GetQualifyingTable()
	dim aCells
	aCells = Array( _
					EMPTY_CELL,				LABEL_ONLY,	_
					"i_disc_cond_basis",	100,		_
					EMPTY_CELL,				LABEL_ONLY,	_
					"i_disc_cond_amt",		100,		_
					L_DiscOf_Text,			LABEL_ONLY,	_
					"QualifyingExprList",	250,		_
					EMPTY_CELL,				LABEL_ONLY	_
					)
	response.write TABLE_START
	GetLabelRow(L_DiscBuy_Text)

	response.write ROW_START
	GetDiscDefCell "b_no_requirement", AUTO_FIT_WIDTH
	response.write ROW_END

	GetFormattedRow(aCells)
	response.write TABLE_END
End sub

sub GetAwardTable()
	dim aCells
	aCells = Array( _
					EMPTY_CELL,				LABEL_ONLY,	_
					"i_disc_award_max",		100,		_
					L_DiscOf_Text,			LABEL_ONLY,	_
					"AwardExprList",		250,		_
					L_DiscAt_Text,			LABEL_ONLY,	_
					"i_disc_offer_type",	200,		_
					EMPTY_CELL,				LABEL_ONLY,	_
					"mny_disc_offer_value",	100,		_
					L_DiscOff_Text,			LABEL_ONLY	_
					)
	response.write TABLE_START
	GetLabelRow(L_DiscGet_Text)

	response.write ROW_START
	GetDiscDefCell "b_freeShipping", AUTO_FIT_WIDTH
	response.write ROW_END

	GetFormattedRow(aCells)
	response.write TABLE_END
End sub

sub GetLimitTable()
	dim aCells
	aCells = Array( _
					L_DiscLimit_Text,		LABEL_ONLY,	_
					"i_disc_limit",			150,		_
					EMPTY_CELL,				LABEL_ONLY	_
					)
	response.write TABLE_START
	GetFormattedRow(aCells)
	response.write TABLE_END
End sub
%>
<html>
<head>
	<link REL="stylesheet" TYPE="text/css" HREF="../include/bizdesk.css" ID="mainstylesheet">
	<script language="VBScript" DEFER>
		option explicit
        dim mny_disc_offer_value_Session
        dim mny_disc_offer_percentage_session
		Dim bFromOnTaskSave
		bFromOnTaskSave = False

	    const ANY_PRODUCT = 0
	    const CATALOG_EXPRESSION = 1
	    const PRODUCT_PICKER = 2
        
		Sub OnTaskSave()		
			dim sTemp, elDiv, xmlDOMDoc, xmlErrorNodes, xmlCampItem, _
				xmlDiscount, xmlCreativeItem, elEditSheet, xmlCondOption, _
				xmlAwardOption, xmlCondExpr, xmlAwardExpr, elBtnBack
			bFromOnTaskSave = True
			'Error Handling for Award Quantity Field
            If i_disc_award_max.value <> "" then
				If Not IsNumeric(i_disc_award_max.value) then 
					If UCase(i_disc_award_max.value) <> UCase("<%= L_DiscAwardMaxDefault_Text %>") then
						Msgbox "<%= L_DiscUnlimitedField_ErrorMessage %>", VbOKOnly, "<%= L_TgGroupWarning_ErrorMessage %>"						
						i_disc_award_max.value = "<%= L_DiscAwardMaxDefault_Text %>"
						i_disc_award_max.focus()
						Exit Sub
					End If
				End If
			Else
				Msgbox "<%= L_DiscUnlimitedField_ErrorMessage %>", VbOKOnly, "<%= L_TgGroupWarning_ErrorMessage %>"
				i_disc_award_max.value = "<%= L_DiscAwardMaxDefault_Text %>"
				i_disc_award_max.focus()
				Exit Sub
			End If
						
			sTemp = ""
				
			'collect data from list box
			For Each elDiv in PageSelectedList.children
				sTemp = sTemp & elDiv.getAttribute("value") & SEPARATOR_BAR
			Next
			saveform.PageSelect.value = sTemp

			sTemp = ""
			For Each elDiv in TargetSelectedList.children
				sTemp = sTemp & elDiv.getAttribute("value") & SEPARATOR_BAR
			Next
			saveform.TargetSelect.value = sTemp

			sTemp = ""
			For Each elDiv in TargetSelectedList3.children
				sTemp = sTemp & elDiv.getAttribute("value") & SEPARATOR_BAR
			Next
			saveform.SelDiscExpr.value = sTemp

			saveform.save_name.value = esMaster.field("u_campitem_name").value

			'change to savenew if needed
			if window.event.srcElement is elGetTaskBtn("save") then
				saveform.type.value = "open"
			Else 'click "savenew"
				saveform.type.value = "add"
			end if
 
			set xmlDOMDoc = xmlPostFormViaXML(saveform)
			HideWaitDisplay()
			EnableTask("back")

 
			'successful save should return one item node and no errors
			set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
			set xmlCampItem = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'campitem_id']")
			set xmlCreativeItem = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'creative_id']")
			set xmlDiscount = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'disc_id']")
			set xmlCondOption = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'disc_cond_option']")
			set xmlAwardOption = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'disc_award_option']")
			set xmlCondExpr = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'disc_cond_expr']")
			set xmlAwardExpr = xmlDOMDoc.selectSingleNode("//item[@name $ieq$ 'disc_award_expr']")
		
			if xmlErrorNodes.length <> 0 or xmlDOMDoc.documentElement is nothing _
				or xmlCampItem is nothing or xmlCreativeItem is nothing or xmlDiscount is nothing then
				'report errors and set status text
				SetStatusText(sFormatString("<%= L_BDSSUnableToSaveX_StatusBar %>", Array(saveform.save_name.value)))
				ShowXMLNodeErrors(xmlErrorNodes)
				'reset dirty bit for all EditSheets
				for each elEditSheet in document.all.urns("Commerce.EditSheet")
					elEditSheet.resetDirty()
				next
			else
				with esMaster
					'set status text
					SetStatusText(sFormatString("<%= L_BDSSSavedX_StatusBar %>", Array(saveform.save_name.value)))
					If window.event.srcElement Is elGetTaskBtn("save") Then
						'set campitem id and creative id
						saveform.i_campitem_id.value = xmlCampItem.getAttribute("value")
						saveform.i_disc_id.value = xmlDiscount.getAttribute("value")
						saveform.i_creative_id.value = xmlCreativeItem.getAttribute("value")
						saveform.u_disc_cond_option.value = xmlCondOption.getAttribute("value")
						saveform.u_disc_award_option.value = xmlAwardOption.getAttribute("value")
						saveform.i_disc_cond_expr.value = xmlCondExpr.getAttribute("value")
						saveform.i_disc_award_expr.value = xmlAwardExpr.getAttribute("value")												
						saveform.dbaction.value = "update"
						
						'If newly selected items are saved update options value from string to index id 
						UpdateExprID QualifyingExprList, AwardExprList, "cond"
						UpdateExprID AwardExprList, QualifyingExprList, "award"

						'reset dirty bit for all EditSheets
						for each elEditSheet in document.all.urns("Commerce.EditSheet")
							elEditSheet.resetDirty()
						next
						esMaster.resetDirty()

						'show preview
						document.frames("previewframe").location.replace "preview.asp?campitem_id=" & saveform.i_campitem_id.value

						'reset title for item saved
						SetPageTitleText(sFormatString("<%= L_DiscCampItemPageTitle_Text %>", Array(saveform.u_customer_name.value, saveform.u_camp_name.value, .field("u_campitem_name").value, saveform.i_campitem_id.value)))
					Else 'click "savenew"
						'reset dictionary
						set g_dCProp = CreateObject("Scripting.Dictionary")
						'set creative id (campitem id will be cleared by resetdefault below) 
						saveform.i_creative_id.value = -1
						saveform.i_disc_id.value = -1
						saveform.dbaction.value = "insert"
						saveform.u_disc_cond_option.value = ""
						saveform.u_disc_award_option.value = ""
						saveform.i_disc_cond_type.value = 0
						saveform.i_disc_award_type.value = 0
						'move pages and groups and reset runofsite
						saveform.btnAdd1.disabled = True
						saveform.btnRemove1.disabled = True
						if saveform.RunOfSite.checked then saveform.RunOfSite.click()
						PageSelectedList.selectAll()
						PageGroupList.add(PageSelectedList.remove())
						if not saveform.RunOfSite.checked then saveform.RunOfSite.click()
						saveform.btnAdd2.disabled = True
						saveform.btnRemove2.disabled = True
						TargetSelectedList.selectAll()
						TargetGroupList.add(TargetSelectedList.remove())

						'reset discount checkboxes, radio buttons
						TargetSelectedList3.selectAll()
						TargetGroupList3.add(TargetSelectedList3.remove())
						saveform.btnAdd3.disabled = True
						saveform.btnRemove3.disabled = True
						saveform.disjoint_award_cond1.click()
						saveform.DiscUsedForDisplay1.click()
						b_no_requirement.value = 0
						b_freeShipping.value = 0
						saveform.b_disc_click_required.checked = false

						'reset dirty bit and defaults for all EditSheets
						for each elEditSheet in document.all.urns("Commerce.EditSheet")
							elEditSheet.resetDefault()
						next
						esMaster.resetDefault()

						'clear preview
						document.frames("previewframe").location.replace "preview.asp"

						'reset title for new
						SetPageTitleText(sFormatString("<%= L_DiscCampItemPageTitleNew_Text %>", Array(saveform.u_customer_name.value, saveform.u_camp_name.value)))
					End If

				end with
				g_bDirty = false
				if parent.g_bCloseAfterSave then
					parent.g_bCloseAfterSave = false
					' -- save and exit chosen and no errors while saving
					set elBtnBack = elGetTaskBtn("back")
					if not elBtnBack is nothing then
						if not elBtnBack.disabled then elBtnBack.click()
					end if
				end if
				parent.g_bCloseAfterSave = false
			End If
			esDiscountItem.focus()
		
			'To force the disabling all icons except back
			disabletask "savenew"
			disabletask "saveback"
			disabletask "save"
			disabletask "new"
			bFromOnTaskSave = false
		End Sub

		Sub UpdateExprID(elList1, elList2, sFormKey)
			dim sNewValue, sOldValue, sDisplayValue, elOption, bSelected
			if cInt(saveform.elements("i_disc_" & sFormKey & "_type").Value) = PRODUCT_PICKER then
				sNewValue = saveform.elements("i_disc_" & sFormKey & "_expr").value
				If sNewValue  <> "" then
					sOldValue = elList1.value
					sDisplayValue = elList1.displayvalue
					If trim(sOldValue) <> trim(sNewValue) then
						elList1.removeItem(sOldValue)
						elList1.addItem sNewValue, sDisplayValue
						elList1.value = sNewValue
					End If

					for each elOption in elList2.options
						If trim(elOption.innerText) = trim(sNewValue) then	
							bSelected = cBool(elList2.value = sOldValue)
							elList2.removeItem(sOldValue)
							elList2.addItem sNewValue, sDisplayValue
							if bSelected then elList2.value = sNewValue
							exit for
						End If
					next
				End If
			end If
		End Sub

		Sub selectOnChange3()
			dim elSelectBox, bDisabled
			set elSelectBox = window.event.srcElement
			if elSelectBox.selected is nothing then
				bDisabled = true
			else
				bDisabled = false
			end if
			Select Case elSelectBox.id
				Case "TargetGroupList3"
					saveform.btnAdd3.disabled = bDisabled
				Case "TargetSelectedList3"
					saveform.btnRemove3.disabled = bDisabled
			End Select
		End Sub

		Sub MoveItems3()
			Dim elFromSelect, elToSelect, elButton
				
			set elButton = window.event.srcElement
			Select Case elButton.id
				Case "btnAdd3"
					Set elFromSelect = TargetGroupList3
					Set elToSelect = TargetSelectedList3
				Case "btnRemove3"
					Set elFromSelect = TargetSelectedList3
					Set elToSelect = TargetGroupList3
			End Select		

			elButton.disabled = True
				
			'If any element is selected Then remove and add that
			'to the Selected Target list
			elToSelect.add(elFromSelect.remove())
			setDirty("")
		End Sub

		sub onChangeQualifyingExprList()
			saveform.i_disc_cond_expr.value = QualifyingExprList.Value
			saveform.u_disc_cond_expr.value = QualifyingExprList.displayvalue
			if isNumeric(QualifyingExprList.Value) then
				saveform.i_disc_cond_type.value = CATALOG_EXPRESSION
			elseif Left(QualifyingExprList.Value, 1) = "x" and Len(QualifyingExprList.Value) = 2 then
				saveform.i_disc_cond_type.value = Right(QualifyingExprList.Value, 1)
				select case Cint(saveform.i_disc_cond_type.value)
					case ANY_PRODUCT
						'do nothing (leave selected)
					case PRODUCT_PICKER
						'open product picker
						openProductPicker QualifyingExprList, "cond"
					case CATALOG_EXPRESSION
						'open expression builder
						openExpressionBuilder QualifyingExprList, "cond"
				end select
			else
				saveform.i_disc_cond_type.value = PRODUCT_PICKER
			end if
		end sub    

		sub onChangeAwardExprList()
			saveform.i_disc_award_expr.value = AwardExprList.Value
			saveform.u_disc_award_expr.value = AwardExprList.displayvalue
			if isNumeric(AwardExprList.Value) then
				saveform.i_disc_award_type.value = CATALOG_EXPRESSION
			elseif Left(AwardExprList.Value, 1) = "x" and Len(AwardExprList.Value) = 2 then
				saveform.i_disc_award_type.value = Right(QualifyingExprList.Value, 1)
				select case Cint(Right(AwardExprList.Value, 1))
					case ANY_PRODUCT
						'do nothing (leave selected)
					case PRODUCT_PICKER
						'open product picker
						openProductPicker AwardExprList, "award"
					case CATALOG_EXPRESSION
						'open expression builder
						openExpressionBuilder AwardExprList, "award"
				end select
			else
				saveform.i_disc_award_type.value = PRODUCT_PICKER
			end if
		end sub  

		'***************************************************************************
		' OnClick Event CONTROL for b_no_requirement
		'
		' This is for updating the below controls dynamically accoroding to boolean
		'***************************************************************************
		Sub OnClickNoRequirement()
			if window.event.srcElement.value = 1 then
				 i_disc_cond_basis.value = 2
				 i_disc_cond_basis.readonly = true
				 i_disc_cond_amt.value = 0
				 i_disc_cond_amt.readonly = true
				 saveform.i_disc_cond_type.value = ANY_PRODUCT
				 QualifyingExprList.value = "x0"
				 QualifyingExprList.readonly = true
			else
				 i_disc_cond_basis.value = 1
				 i_disc_cond_basis.readonly = false
				 i_disc_cond_amt.value = 1
				 i_disc_cond_amt.readonly = false
				 saveform.i_disc_cond_type.value = ANY_PRODUCT
				 QualifyingExprList.readonly = false
			end if
		End Sub

		'***************************************************************************
		' OnClick Event CONTROL for b_freeShipping
		'
		' This is for updating the below controls dynamically accoroding to boolean
		'***************************************************************************
		sub OnClickFreeShipping()
			if window.event.srcElement.value = 1 then
				 i_disc_award_max.value = "<%= L_DiscAwardMaxDefault_Text %>"
				 i_disc_award_max.readonly = true
				 mny_disc_offer_value.value = 100
				 mny_disc_offer_value.readonly = true
				 i_disc_offer_type.value = 2
				 i_disc_offer_type.readonly = true
				 saveform.i_disc_award_type.value = ANY_PRODUCT
				 AwardExprList.value = "x0"
				 AwardExprList.readonly = true
			else
				 i_disc_award_max.value = "<%= L_DiscAwardMaxDefault_Text %>"
				 i_disc_award_max.readonly = false
				 mny_disc_offer_value.value = 1
				 mny_disc_offer_value.readonly = false
				 i_disc_offer_type.value = 1
				 i_disc_offer_type.readonly = false
				 saveform.i_disc_award_type.value = ANY_PRODUCT
				 AwardExprList.readonly = false
			end if
		End sub

		sub checkDefault()
			if i_disc_award_max.value = "" then
				i_disc_award_max.value = "<%= L_DiscAwardMaxDefault_Text %>"
			elseif isNumeric(i_disc_award_max.value) then
				if CDbl(i_disc_award_max.value) = 0 then i_disc_award_max.value = "<%= L_DiscAwardMaxDefault_Text %>"
			end if
			If Not IsNumeric(i_disc_award_max.value) and UCase(i_disc_award_max.value) <> UCase("<%= L_DiscAwardMaxDefault_Text %>") then
				Msgbox "<%= L_DiscUnlimitedFieldImmed_ErrorMessage %>", VbOKOnly, "<%= L_TgGroupWarning_ErrorMessage %>"			
				i_disc_award_max.focus()
				i_disc_award_max.value = "<%= L_DiscAwardMaxDefault_Text %>"
			End If
		End sub

		sub AddToList(byVal sName, byVal iDiscID, byRef elList)
			Dim elItem
			for each elItem in elList.options
				if elItem.value = iDiscID Then				   
					Exit sub
				end if
			Next
			elList.addItem trim(iDiscID), trim(sName)
		End sub 

		'This function checks dup item with ID saved
		Function CheckForDuplicate_Text(byVal sName, byRef elList)
			Dim elItem
			
			for each elItem in elList.options
 				If trim(elItem.innertext) = trim(sName) Then
					CheckForDuplicate_Text = elItem.value
					exit function
				end if
			Next
			
			CheckForDuplicate_Text = ""
		End function 

		Function CheckForUpdate(byVal sName, byVal sValue)
			CheckForUpdate = CheckForUpdateStatus(sName, sValue, QualifyingExprList)
			CheckForUpdate = CheckForUpdateStatus(sName, sValue, AwardExprList)
		End Function

		Function CheckForUpdateStatus(byVal sName, byVal sValue, byRef elList)
			Dim elItem
			
			for each elItem in elList.options
 				If elItem.value = sValue Then
					'elItem.innerText = sName
					'elItem.Text = sName
					elList.RemoveItem trim(elItem.value)  
					elList.addItem trim(elItem.value), trim(sName)					
					CheckForUpdateStatus = trim(elItem.value)
					exit function
				end if
			Next
			CheckForUpdateStatus = ""
		End Function

		sub AddToAllExprLists(byVal sName, byVal iDiscID)
			 AddToList sName, iDiscID, AwardExprList
			 AddToList sName, iDiscID, QualifyingExprList
		End sub

		'**************************************************************
		'	Function sJoinList(elList)
		'	This function grab all options in product picker and 
		'   return string delimited by SEPARATOR_BANGS (!!!)
		'**************************************************************
		Function sJoinList(elList)
			Dim elItem, sTemp, sExpr

			for each elItem in elList.options
				if Len(elItem.value) > 5 Then
					' ExprName + AttributeType
					sTemp = elItem.value
					if (sExpr = "") then
						sExpr = sTemp
					else
						sExpr = sExpr & SEPARATOR_BANGS & sTemp
					end if
				end if
			Next
			sJoinList = sExpr
		End Function  

		dim g_bRounded, g_bRounded2
		g_bRounded = false
		g_bRounded2 = false

        Sub OnRounding()
			if g_bRounded then
				g_bRounded = false
				exit sub
			end if
			g_bRounded = true
			If i_disc_cond_basis.value = "2" then
				 i_disc_cond_amt.value = Round(i_disc_cond_amt.value)
			Else
				 i_disc_cond_amt.value = Round(i_disc_cond_amt.value, 2)
			End If 
        End Sub

        Sub OnRounding2()
			If g_bRounded2 then
				g_bRounded2 = false
				exit sub
			End if
			g_bRounded2 = true
			mny_disc_offer_value.value = Round(mny_disc_offer_value.value, 2)
        End Sub

		Sub OnCheckForOverlimit()
			Dim sOfferValue, sOfferType
		 
			sOfferValue = mny_disc_offer_value.value  
			sOfferType = i_disc_offer_type.value  
			'Choose percentage
			If sOfferValue <> "" and sOfferType = "2" then
				IF CDbl(sOfferValue) < 0 OR CDbl(sOfferValue) > 100 then
					MsgBox "<%= L_DiscOfferValueCC_ErrorMessage %>", VbOKOnly, "<%= L_TgGroupWarning_ErrorMessage %>"

					mny_disc_offer_value.value = 1
					mny_disc_offer_value.focus()
				End If 
				mny_disc_offer_percentage_session = mny_disc_offer_value.value
			End If
			'Choose value in type selection
			If sOfferValue <> "" and sOfferType = "1" then
				mny_disc_offer_value_session = mny_disc_offer_value.value
			'else
			'	mny_disc_offer_value.value = 1
			End If		
		End Sub

		Sub OnBrowseCondExpr()
		    openExpressionBuilder QualifyingExprList, "cond"
		End Sub

		Sub OnBrowseAwardExpr()
		    openExpressionBuilder AwardExprList, "award"
		End Sub

		Sub openProductPicker(elList, sFormKey)
			dim sURL, sReturnValue, aReturnValues, aReturnValues1, aReturnCheck
			sURL = "../Catalogs/Editor/dlg_ProductPicker.asp?mode=CAMPAIGNS"
			sReturnValue = showModalDialog(sURL, , "dialogHeight:545px;dialogWidth:700px;status:no;help:no")		
			if not (sReturnValue = -1 or isEmpty(sReturnValue) or sReturnValue = "") then

			 	aReturnValues = Split(sReturnValue, SEPARATOR_BARS)
				aReturnValues1 = Split(aReturnValues(0), "=")

				'This is check for duplicate item already ID saved, if true get the selecting ID
				aReturnCheck = CheckForDuplicate_Text(aReturnValues1(1), elList)
				If aReturnCheck = "" then
					AddToAllExprLists aReturnValues1(1), sReturnValue
					'select the expression
					elList.value = sReturnValue
					'Get all new option items 
					saveform.elements("u_disc_" & sFormKey & "_option").value = sJoinList(elList)
					saveform.elements("i_disc_" & sFormKey & "_expr").value = trim(aReturnValues1(1))
		 		else
					elList.value = ""
					elList.value = aReturnCheck
				end if
			else
				elList.value = "x0"
			End If
		End Sub

		Sub openExpressionBuilder(elList, sFormKey)
			dim strExprID, sURL, sReturnValue, aReturnValues, aReturnCheck
			strExprID = saveform.elements("i_disc_" & sFormKey & "_expr").value
			'In case not-saved product picker picked for cataExpr edit mode
			If strExprID <> "" and not IsNumeric(strExprID) then
			   strExprID = ""
			End If

			sURL = "dlg_targetexpr.asp?catalog_expr_id=" & strExprID & "&exprtype=CATALOG"
			sReturnValue = showModalDialog(sURL, , "status:no;help:no")		
			aReturnValues = Split(sReturnValue, SEPARATOR_BAR)					
			if not (sReturnValue = -1 or isEmpty(sReturnValue) or sReturnValue = "") then
				'If this is update just change the value
				saveform.elements("i_disc_" & sFormKey & "_expr").value = cLng(aReturnValues(0))
				aReturnCheck = CheckForUpdate(aReturnValues(1), aReturnValues(0)) 
				If aReturnCheck = "" then	
					AddToAllExprLists aReturnValues(1), aReturnValues(0)
					'select the expression
					elList.value = aReturnValues(0)					    
				else
					elList.value = ""
					elList.value = aReturnCheck
				end If
			else
				if not IsNumeric(strExprID) then elList.value = "x0"
			end if
		End Sub
	
		sub OnChangeOfferValue()
			Dim sOfferValue, sOfferType
		 
			sOfferValue = mny_disc_offer_value.value  
			sOfferType = i_disc_offer_type.value  
			'Switch to percentage
			If sOfferValue <> "" and sOfferType = "2" then
				'If session value has something insert it or assign default value
				If mny_disc_offer_percentage_Session <> "" then
					mny_disc_offer_value.value = mny_disc_offer_percentage_Session
				else
					mny_disc_offer_value.value = 1
				end If
				mny_disc_offer_value.focus()
			End If
			'Choose value in type selection
			If sOfferValue <> "" and sOfferType = "1" then
				If mny_disc_offer_value_Session <> "" then
					mny_disc_offer_value.value = mny_disc_offer_value_Session
				else
					mny_disc_offer_value.value = 1
				End If
				mny_disc_offer_value.focus()
			End If
		End sub

		Sub DateChange()
			Dim dtStart, dtEnd, iDateDiff, bNoGo
			If bFromOnTaskSave Then Exit Sub
			'show validation error if start after end
			dtStart = dtGetDate(dt_campitem_start.value)
			dtEnd = dtGetDate(dt_campitem_end.value)
			bNoGo = False
			If Not dt_campitem_start.required And Not dt_campitem_end.required Then
				iDateDiff = DateDiff("d", dtStart, dtEnd)
				If iDateDiff < 0 Then
					msgbox "<%= L_CMStartDateBeforeEndDate_ErrorMessage %>", vbOKOnly, "<%= L_BDSSVAlidationErrorTitle_Text %>"
					bNoGo = True
				End If
			End If
			If bNoGo Then
				g_bDirty = False
				DisableTask("save")
				DisableTask("saveback")
				DisableTask("savenew")
			Else
				g_bDirty = True
				EnableTask("save")
				EnableTask("saveback")
				EnableTask("savenew")
			End If
		End Sub

		Sub CheckForImageType()
		     'If non-clickabel image type selected give warning message
		      If esMaster.field("i_creative_type_id").value = "4" then
				 MsgBox "<%=L_DiscCannotSelectClickRequired_ErrorMessage%>", 48, "<%=L_DiscWarningMesssage_ErrorMessage%>"
                 saveform.b_disc_click_required.checked = false
		      elseif esMaster.field("i_creative_type_id").value = "10" then
				 MsgBox "<%=L_DiscCannotSelectClickRequired2_ErrorMessage%>", 48, "<%=L_DiscWarningMesssage_ErrorMessage%>"
                 saveform.b_disc_click_required.checked = false		      
		      end if
		End Sub

	</script>
	<script language="VBScript">
		' ---------------------------------------------------------------
		'
		'		CONST Declarations
		'
		Const SEPARATOR_BAR = "|"
		Const SEPARATOR_BARS = "|||"
		Const SEPARATOR_BANGS = "!!!"

		Sub onDiscountReadyStateChange()
			if esDiscountItem.readyState = "complete" then esDiscountItem.focus()
		End Sub

		Sub window_onload()
			dim sImageType
			sImageType = "imgtype<%= g_dFormValues("i_creative_type_id") %>"
			set g_elSelectedImageType = document.all(sImageType)
			saveform.btnPreview.setExpression "disabled", "bdtaskbtnsave.disabled"
			on error resume next
			listener.setListenElement(g_elSelectedImageType)
		End Sub
	</script>
</head>
<body SCROLL="no">
<%
	InsertEditTaskBar sGetTitle(), g_sStatusText
%>

<!-- display the customer and campaign outside the Editsheet -->
<%
'After the changes to how we add a new item, by choosing the campaign we want it to.
%><!--*****META*****META*****META*****META*****META*****META*****META*****META*****META*****-->

<!-- Start creative types -->
<%= g_sCreativeTypesXML %>
<!-- End creative types -->

<!-- Display Meta Data Template -->
<%
	InsertPreviewDisplayMeta
%>

<!-- Display Discount item Meta Data Template -->
	<xml id="DiscountItemMETA">
		<editsheet>
		    <global title="no" />
			<fields>
				<text id="u_campitem_name" required="yes" maxlen="50" subtype="short">
					<name><%= L_DiscCampItemName_Text %></name>
				    <tooltip><%= L_DiscCampItemName_ToolTip %></tooltip>
				    <error><%= L_DiscCampItemName_ErrorMessage %></error>
				    <prompt><%= L_DCCmpItememail_Text%></prompt>
				</text>
				<select id="b_campitem_active" default='0'>
					<name><%= L_DiscStatus_Text %></name>
				    <tooltip><%= L_DiscStatus_ToolTip %></tooltip>
					<select>
						<option value="1"><%= L_DiscStatusOptionActive_Text %></option>
						<option value="0"><%= L_DiscStatusOptionInactive_Text %></option>
					</select>
				</select>
				<text id="dt_campitem_archived" readonly="yes" maxlen="50" subtype="short" default='<%= L_CMUnarchived_Text %>'>
					<name><%= L_DiscCampItemNameState_Text %></name>
				    <tooltip><%= L_DiscCampItemNameState_ToolTip %></tooltip>
				</text>															        
				<select id="i_disc_rank" default="10">
					<name><%= L_DiscRank_Text %></name>
				      <tooltip><%= L_DiscRankName_ToolTip %></tooltip>
				      <select>
						<option value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>							            
						<option value="40">40</option>
						<option value="50">50</option>
						<option value="60">60</option>
						<option value="70">70</option>
						<option value="80">80</option>							            
						<option value="90">90</option>
						<option value="100">100</option>
						<option value="110">110</option>
						<option value="120">120</option>
						<option value="130">130</option>							            
						<option value="140">140</option>
						<option value="150">150</option>
						<option value="160">160</option>
						<option value="170">170</option>
						<option value="180">180</option>							            
						<option value="190">190</option>
						<option value="200">200</option>							
					</select>
				</select>			
				<text id="u_campitem_comments" maxlen="255" subtype="long">
					<name><%= L_DiscComments_Text %></name>
				      <tooltip><%= L_DiscComments_ToolTip %></tooltip>
				      <error><%= L_DiscComments_ErrorMessage %></error>
				</text>											        		
			</fields>		        		    
		</editsheet>
	</xml>

	<xml id="DiscountScheduleMETA">
		<editsheet>
		    <global title="no" />		
			<fields>
				<date id="dt_campitem_start" required="yes" firstday='<%= g_sMSCSWeekStartDay %>'
					onchange='DateChange'
					default='<%= g_MSCSDataFunctions.date(g_dFormValues("dt_campitem_start"), g_MSCSDefaultLocale) %>'>
					<name><%= L_DiscStartDate_Text %></name>
		            <tooltip><%= L_DiscStartDate_ToolTip %></tooltip>
		            <error><%= L_DiscStartDate_ErrorMessage %></error>
		            <format><%= g_sMSCSDateFormat%></format>
		        </date>
				<date id="dt_campitem_end" required="yes" firstday='<%= g_sMSCSWeekStartDay %>'
					onchange='DateChange'
					default='<%= g_MSCSDataFunctions.date(g_dFormValues("dt_campitem_end"), g_MSCSDefaultLocale) %>'>
					<name><%= L_DiscEndDate_Text %></name>
		            <tooltip><%= L_DiscEndDate_ToolTip %></tooltip>
		            <error><%= L_DiscEndDate_ErrorMessage %></error>
		            <format><%= g_sMSCSDateFormat %></format>
		        </date>				
			</fields>
		</editsheet>
	</xml>

	<xml ID="DiscountDisplayMETA2">
		<editsheet>
			<global title="no" />		
			<fields>
				<select id="i_campitem_exposure_limit" default="0">
					<name><%= L_DiscExposureLimit_Text %></name>
					<tooltip><%= L_DiscExposureLimit_ToolTip %></tooltip>
					<select id="i_campitem_exposure_limit">
						<option value="0"><%= L_DiscNone_Text %></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>							            
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>							            
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</select>											        
				<text id="u_disc_description" required="yes" subtype="long">
					<name><%= L_DiscBasketDisplay_Text %></name>
				    <tooltip><%= L_DiscBasketDisplay_Tooltip %></tooltip>
				    <error><%= L_DiscBasketDisplay_ErrorMessage %></error>
				    <prompt><%= L_DCCmpItemBasketDisplay_Text%></prompt>
				</text>
			</fields>
		</editsheet>
	</xml>

<!-- master container Meta Data -->
	<xml id="MasterMeta">
	<editsheets>
		<editsheet>
			<global expanded="yes">
			    <name><%= L_DiscNamingProperties_Text %></name>
				<key><%= L_DiscNamingProperties_Accelerator %></key>
		    </global>
		    <template register="esDiscountItem esDiscountSchedule"><![CDATA[
				<div ID="esDiscountItem" class="editsheet"
					DATAXML="DiscountItemDATA"
					METAXML="DiscountItemMETA"
					LANGUAGE='VBScript'
					ONREADYSTATECHANGE='onDiscountReadyStateChange()'><%= L_BDSSLoadingProperties_Text %></div>
				<div ID="esDiscountSchedule" class="editsheet"
					DATAXML="DiscountScheduleDATA"
					METAXML="DiscountScheduleMETA"><%= L_BDSSLoadingProperties_Text %></div>				
	        ]]></template>	
		</editsheet>
		<editsheet>
			<global expanded="no">
			    <name><%= L_DiscDefinition_Text %></name>
				<key><%= L_DiscDefintion_Accelerator %></key>
			</global>		
			<fields>
<%				'According to cond_type (3 means checking the checkbox)
				'It has to be treated as separate from disc_award_type
				dim sDefault, sListDefault, sReadOnly, sRequired
 
				If g_dformvalues("i_disc_cond_type") = 3 Then
					sReadOnly = " readonly='yes' "
					sRequired = ""
					sDefault = " default='1' "
					sListDefault = " default='-1' "
				else
					sReadOnly = ""
					sRequired = " required='yes' "
					sDefault = " default='0' "
					sListDefault = " default='x0' "
				end if
%>				<boolean id="b_no_requirement" onchange="OnClickNoRequirement()" <%= sDefault %>>
				    <label><%= L_NoRequirement_Text %></label>
				</boolean>	
				<select id="i_disc_cond_basis" <%= sReadOnly %> default="1" onchange="OnRounding()">
					<select id="i_disc_cond_basis">
						<option value="1"><%= L_DiscPriceOption_Text %></option>
						<option value="2"><%= L_DiscUnitOption_Text %></option>
					</select>
				</select>
				<numeric  id="i_disc_cond_amt"  <%= sRequired %> <%= sReadOnly %> min="0" max='99999999' subtype="float" default="1" onchange="OnRounding()">
				    <error><%= L_DiscQuantity_ErrorMessage %></error>
			        <format><%= g_sMSCSNumberFormat %></format>
				</numeric>	
				<select id="QualifyingExprList" <%= sReadOnly %> <%= sListDefault %>
					onbrowse='OnBrowseCondExpr()'  onchange="OnChangeQualifyingExprList()">
				    <prompt><%= L_DiscNewObject_Text %></prompt>
					<select id="QualifyingExprList">
						<option value="x0">[<%= L_DiscAnyProductOption_Text %>]</option>
						<option value="x2">[<%= L_DiscProductionPickerOption_Text %>]</option>
						<option value="x1">[<%= L_DiscCatalogExpressionOption_Text %>]</option>		
						<%= sGetExprOptions() %>
					</select>
				</select>
<%				'According to award_type (3 means checking the checkbox)
				'It has to be treated as separate from disc_cond_type
				If g_dformvalues("i_disc_award_type") = 3 Then
					sReadOnly = " readonly='yes' "
					sRequired = ""
					sDefault = " default='1' "
					sListDefault = " default='-1' "
				else
					sReadOnly = ""
					sRequired = " required='yes' "
					sDefault = " default='0' "
					sListDefault = " default='x0' "
				end if
%>				<boolean id="b_freeShipping" onchange="OnClickFreeShipping()" <%= sDefault %>>
					<label><%= L_FreeShipping_Text %></label>
				</boolean>		
				<text id="i_disc_award_max" <%= sRequired %> <%= sReadOnly %>  onchange="checkDefault()"
					maxlen="9" subtype="short" default="<%= L_DiscAwardMaxDefault_Text %>">
				    <error><%= L_DiscQuantity_ErrorMessage %></error>
				</text>		
				<select id="AwardExprList" <%= sReadOnly %> <%= sListDefault %>
					onbrowse='OnBrowseAwardExpr()'  onchange="OnChangeAwardExprList()">
				    <prompt><%= L_DiscNewObject_Text %></prompt>
					<select id="AwardExprList" >
						<option value="x0">[<%= L_DiscAnyProductOption_Text %>]</option>
						<option value="x2">[<%= L_DiscProductionPickerOption_Text %>]</option>
						<option value="x1">[<%= L_DiscCatalogExpressionOption_Text %>]</option>		
						<%= sGetExprOptions() %>
					</select>
				</select>
				<select id="i_disc_offer_type" <%= sReadOnly %> default="1"
					onchange='OnChangeOfferValue()'>
					<select id="i_disc_offer_type">
						<option value="1"><%= L_DiscPriceOption_Text %></option>
						<option value="2"><%= L_DiscPercentOptoin_Text %></option>
					</select>
				</select>				
				<numeric id="mny_disc_offer_value" <%= sRequired %> <%= sReadOnly %>
					min="0" max='99999999' subtype="float" default="1" onchange="OnRounding2():OnCheckForOverlimit()">
					<name><%= L_DiscValue_Text %></name>
				    <tooltip><%= L_DiscValue_ToolTip %></tooltip>
				    <error><%= L_DiscValue_ErrorMessage %></error>
			        <format><%= g_sMSCSNumberFormat %></format>
				</numeric>																																									
				<select id="i_disc_limit" default="0">
					<select id="i_disc_limit">
						<option value="0"><%= L_DiscNone_Text %></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</select>
			</fields>
		    <template fields="b_no_requirement i_disc_cond_basis i_disc_cond_amt b_freeShipping i_disc_award_max i_disc_offer_type mny_disc_offer_value i_disc_limit QualifyingExprList AwardExprList"><![CDATA[				
<p>
<!-- CONDITION -->
<% GetQualifyingTable() %>

<p>
<!-- AWARD -->
<% GetAwardTable() %>

<p>
<!-- LIMIT -->
<% GetLimitTable() %>

<p>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="95%" STYLE="TABLE-LAYOUT: fixed">
<COLGROUP><COL WIDTH='25'><COL></COLGROUP>
<%	dim sAwardOnly, sCondAndAward
	g_sQuery = "SELECT b_disc_disjoint_cond_award FROM order_discount WHERE i_campitem_id = " & g_dformvalues("i_campitem_id")
	g_oCmd.CommandText = g_sQuery
	set g_rsQuery = Server.CreateObject("ADODB.Recordset")
	g_rsQuery.Open g_oCmd, , AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY

	if not g_rsQuery.EOF then
		if g_rsQuery("b_disc_disjoint_cond_award") then
			sAwardOnly	 = "CHECKED"
			sCondAndAward = ""
		else
			sAwardOnly	 = ""
			sCondAndAward = "CHECKED"
		end if		
	else
		sAwardOnly	 = "CHECKED"
		sCondAndAward = ""
	end if			
	g_rsQuery.Close
%><TR>
	<TD COLSPAN='2'>
		<%= L_ProductSame_Text %>
	</TD>
</TR>
	<TD VALIGN='top'>
		<INPUT TYPE="radio" ID="disjoint_award_cond1" NAME="disjoint_award_cond"
			<%= sAwardOnly %> VALUE="awardonly" Title="<%=L_ApplyAwardAlone_Text%>" 
			STYLE='BACKGROUND-COLOR: transparent'
			LANGUAGE="VBScript"
			ONCLICK='SetDirty("")'>
	</TD>
	<TD>
		<LABEL Title="<%=L_ApplyAwardAlone_Text%>" FOR="disjoint_award_cond1"><%= L_ApplyAwardAlone_Text %></LABEL>
	</TD>
</TR>
<TR>
	<TD VALIGN='top'>
		<INPUT TYPE="radio" ID="disjoint_award_cond2" NAME="disjoint_award_cond"
			<%= sCondAndAward %> VALUE="condandaward" title="<%= L_ApplyAwardAndQualifyingProd_Text %>"  
			STYLE='BACKGROUND-COLOR: transparent'
			LANGUAGE="VBScript"
			ONCLICK='SetDirty("")'>
	</TD>
	<TD>
		<LABEL title="<%= L_ApplyAwardAndQualifyingProd_Text %>" FOR="disjoint_award_cond2"><%= L_ApplyAwardAndQualifyingProd_Text %></LABEL>
	</TD>
</tr>
<%	Dim sDiscClickReq					
	if not g_dformvalues("type") = "add" then
		g_sQuery = "Select b_disc_click_required FROM order_discount " & _
				"WHERE i_campitem_id = " & g_dformvalues("i_campitem_id")							
		g_oCmd.CommandText = g_sQuery
		g_rsQuery.Open g_oCmd, , AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY
		if not g_rsQuery.EOF then
			if g_rsQuery("b_disc_click_required") then
				sDiscClickReq = "CHECKED"
			else
				sDiscClickReq = ""
			End If
		else
			sDiscClickReq = ""
		end if
		g_rsQuery.close
	else
		sDiscClickReq = ""
	end if	
%><TR><TD>&nbsp;</TD></TR>
<TR>
	<TD VALIGN='top'>
		<INPUT TITLE="<%= L_DiscClickRequired_Text %>" TYPE="checkbox" ID="b_disc_click_required" NAME="b_disc_click_required"
			<%= sDiscClickReq %>
			STYLE='BACKGROUND-COLOR: transparent'
			LANGUAGE="VBScript"
			ONCLICK="setDirty(&quot;&quot;):CheckForImageType()">
	</TD>
	<TD>
		<LABEL TITLE="<%= L_DiscClickRequired_Text %>" FOR="b_disc_click_required"><%= L_DiscClickRequired_Text %></LABEL>
	</TD>
</TR>
</TABLE>

<p>	
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH='95%' STYLE="TABLE-LAYOUT: fixed">						 
<COLGROUP><COL WIDTH='30%'><COL WIDTH='20%'><COL WIDTH='30%'><COL></COLGROUP>
<TR>
	<TD VALIGN="TOP" COLSPAN=3><%= L_DiscEligibilityRequirements_Text %></TD>
</TR>
<TR>
	<TD VALIGN="top">
		<span TITLE="<%= L_DiscExpressionAvalilable_ToolTip %>"><%= L_DiscExpressionAvalilable_Text %></span><BR>
		<div TITLE="<%= L_DiscExpressionAvalilable_ToolTip %>" ID="TargetGroupList3" SELECTION="multi"
			CLASS="listBox" STYLE="width:100%"
			LANGUAGE="vbscript"
			ONSELECT="selectOnChange3()">
			<%= sGetTargetGroupList3(g_dformvalues("i_disc_id")) %>
		</div>
	</td>
	<td align=center>
		<button title="<%= L_BDSSAddArrow_Button %>" TYPE="button" ID="btnAdd3" DISABLED
			CLASS="bdbutton" STYLE="WIDTH: 7em"
			LANGUAGE='VBScript'
			ONCLICK="MoveItems3()">
			<%= L_BDSSAddArrow_Button %>
		</button>
		<br>
		<button title="<%= L_BDSSArrowRemove_Button %>" TYPE="button" ID="btnRemove3" DISABLED
			CLASS="bdbutton" STYLE="WIDTH: 7em"
			LANGUAGE='VBScript'
			ONCLICK="MoveItems3()">
			<%= L_BDSSArrowRemove_Button %>	
		</button> 
	</td>
	<td valign="top">
		<span title="<%= L_DiscExpressionSelected_ToolTip %>"><%= L_DiscExpressionSelected_Text %></span><BR>
		<div title="<%= L_DiscExpressionSelected_ToolTip %>" ID="TargetSelectedList3" SELECTION="multi"
			CLASS="listBox" STYLE="width:100%"
			LANGUAGE="VBScript"
			ONSELECT="selectOnChange3()">
<%			if g_dformvalues("type") <> "add" then %>
				<%= sGetTargetSelectedList3(g_dformvalues("i_disc_id")) %>
<%			End if
%>		</div>
	</TD>   
</TR>
</TABLE>
		    ]]></template>
		</editsheet>
		<editsheet>
			<global expanded="no">
			    <name><%= L_DiscTarget_Text %></name>
				<key><%= L_DiscTarget_Accelerator %></key>
			</global>
		    <template register="RunOfSite"><![CDATA[
<%		    	'determine what to select
		    	Dim addition, instead
				g_sQuery = "SELECT b_disc_expressions_used_for_display FROM order_discount WHERE i_campitem_id = " & g_dformvalues("i_campitem_id")
				g_oCmd.CommandText = g_sQuery
				g_rsQuery.Open g_oCmd, , AD_OPEN_FORWARD_ONLY, AD_LOCK_READ_ONLY
					
				if not g_rsQuery.EOF then
					if g_rsQuery("b_disc_expressions_used_for_display") then
						addition = "CHECKED"
						instead = ""
					else
						addition = ""
						instead = "CHECKED"
		 			end if		
				else
					addition = "CHECKED"
					instead = ""
				end if			
				g_rsQuery.Close									
%>				<p><strong><%= L_DiscWhenDiscDisplayed_Text %></strong><p>
				<table border='0' WIDTH='95%' CELLPADDING='0' CELLSPACING='0' STYLE='TABLE-LAYOUT: fixed'>
				<tr><td title="<%=L_DiscTargetOptionsTitle_ToolTip%>" valign="top" STYLE='WIDTH:20%'><%= L_DiscTargetOptionsTitle_Text %></td>
				<td> 
					<TABLE border="0" CELLPADDING="0" cellspacing="0" width="100%"><TR><TD VALIGN=top width='20'>
					<input type="radio" id="DiscUsedForDisplay1" title="<%=L_DiscTargetAdditionSelect_Text%>" name="DiscUsedForDisplay" <%= addition %> value="addition" LANGUAGE="VBScript" ONCLICK='SetDirty("")' style='BACKGROUND-COLOR: transparent'>
					</TD><TD>
					<label for="DiscUsedForDisplay1" style="WIDTH: 90%" title="<%=L_DiscTargetAdditionSelect_Text%>"><%= L_DiscTargetAdditionSelect_Text %></label>
					</TD></TR><TR><TD VALIGN=top>
					<input type="radio" id="DiscUsedForDisplay2" title="<%= L_DiscTargetInsteadSelect_Text %>" name="DiscUsedForDisplay" <%= instead %> value="instead" LANGUAGE="VBScript" ONCLICK='SetDirty("")' style='BACKGROUND-COLOR: transparent'>
					</TD><TD>
					<label for="DiscUsedForDisplay2" style="WIDTH: 90%" title="<%= L_DiscTargetInsteadSelect_Text %>"><%= L_DiscTargetInsteadSelect_Text %></label>
					</TD></TR></TABLE>
				</td></tr></table>
				<div ID="divPageGroups" style="border-width: 0px; bottom: 0px">	
					<%= sGetPageTargetGroups() %>
				</div>
			]]></template>
		</editsheet>
		<editsheet>
			<global expanded="no">
			    <name><%= L_DiscDisplayName_Text %></name>
				<key><%= L_DiscDisplayName_Accelerator %></key>
			</global>		
		   <fields>
				<select id="i_creative_size_id" default="1">
		            <select id="i_creative_size_id">
						<%= sGetCreativeSizeOptions %>
					</select>
		        </select>
				<select id="i_creative_type_id" onchange="OnChangeCreativeType()" default="1">
		            <select id="i_creative_type_id">
						<%= sGetCreativeTypeOptions(GUID_TYPE_DISC) %>
					</select>
		        </select>
		   </fields>
		   <template fields='i_creative_size_id i_creative_type_id' register="esDiscountDisplay esDisplay listener"><![CDATA[
				<div ID="esDiscountDisplay" class="editsheet" DATAXML="DiscountItemDATA" METAXML="DiscountDisplayMETA2"></div>
				<table ID="esAdDisplay2" cellspacing="4" cellpadding="0" class="esTableStyle" STYLE="WIDTH:95%">
					<tr>
						<td class="reqicon"></td>
						<td title="<%= L_DiscContentSize_ToolTip %>" class="esLabelCell"><%= L_DiscContentSize_Text %></td>
						<td ID="i_creative_size_id" class="esDataCell"></td>
					</tr>
					<tr>
						<td class="reqicon"></td>
						<td title="<%= L_DiscContentType_ToolTip %>" class="esLabelCell"><%= L_DiscContentType_Text %></td>
						<td ID="i_creative_type_id" class="esDataCell"></td>
					</tr>
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr>
						<td ID='creativetypecontainer' colspan="3" style="BORDER: inset 1px; PADDING: 2px; MARGIN-TOP: 0">
							<%= g_sCreativeTypes %>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<input title="<%= L_CMPreview_Button %>" id='btnPreview' type="button" CLASS="bdbutton" style="MARGIN: 4px" LANGUAGE="VBScript" ONCLICK="PreviewAd()" value="<%= L_CMPreview_Button %>" disabled>
						</td>
					</tr>
					</table>
					<div ID="esDisplay " CLASS="editSheet" MetaXML="DisplayMeta" DataXML="emptyData"></div>
					</div>
		   ]]></template>
		</editsheet>
	</editsheets>
	</xml>		 
 

<!--*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****DATA*****-->

<!-- Discount Item XML DATA -->
	<xml ID="DiscountItemDATA">
		<%= sGetDiscountItemDataSet	%>
	</xml>

<!-- Discount Schedule XML DATA -->	
	<xml id="DiscountScheduleDATA">
		<%= g_sDiscountScheduleDataSet	%>
	</xml>

<!-- Ad Group DATA -->
	<xml id="MasterData">
		<?xml version="1.0" ?>
		<%= sGetMasterData() %>
	</xml>

<!-- empty record -->
	<xml id="emptyData">
		<?xml version="1.0" ?>
		<document><record/></document>
	</xml>

<!--******DISPLAY******DISPLAY******DISPLAY******DISPLAY******DISPLAY******DISPLAY******-->

<div ID="bdcontentarea">

	<form ID="saveform" METHOD="post" ONTASK="OnTaskSave()">
		<input TYPE="hidden" NAME="type">
		<input TYPE="hidden" NAME="i_customer_id" VALUE="<%= g_dformValues("i_customer_id") %>">
		<input TYPE="hidden" NAME="i_camp_id" VALUE="<%= g_dformValues("i_camp_id") %>">	
		<input TYPE="hidden" NAME="i_campitem_id" VALUE="<%= g_dformValues("i_campitem_id") %>">	
		<input TYPE="hidden" NAME="u_customer_name" VALUE="<%= g_dformValues("u_customer_name") %>">	
		<input TYPE="hidden" NAME="u_camp_name" VALUE="<%= g_dformValues("u_camp_name") %>">	
		<input TYPE="hidden" NAME="i_disc_id" VALUE="<%= g_dformValues("i_disc_id") %>">	
		<input TYPE="hidden" NAME="save_name">	
		<input TYPE="hidden" NAME="campitem_type" VALUE="<%= GUID_TYPE_DISC %>">	
		<input TYPE="hidden" NAME="dbaction" VALUE="<%= g_dFormValues("dbaction") %>">
		<input TYPE="hidden" NAME="i_disc_cond_expr" VALUE="<%= g_dformValues("i_disc_cond_expr") %>">
		<input TYPE="hidden" NAME="i_disc_award_expr" VALUE="<%= g_dformValues("i_disc_award_expr") %>">
		<input TYPE="hidden" NAME="u_disc_award_option" VALUE="<%= g_dformValues("u_disc_award_option") %>">
		<input TYPE="hidden" NAME="u_disc_cond_option" VALUE="<%= g_dformValues("u_disc_cond_option") %>">
		<input TYPE="hidden" NAME="u_disc_award_expr">
		<input TYPE="hidden" NAME="u_disc_cond_expr">
		<input TYPE="hidden" Name="PageSelect">
		<input type="hidden" Name="TargetSelect">
		<input TYPE="hidden" NAME="SelDiscExpr">
		<INPUT TYPE='hidden' NAME='u_customer_url' VALUE='<%= g_dFormValues("u_customer_url") %>'>
		<input TYPE="hidden" NAME="i_creative_id" VALUE="<%= g_dformValues("i_creative_id") %>">	
		<input TYPE="hidden" NAME="i_disc_award_type" VALUE="<%= g_dformValues("i_disc_award_type") %>">	
		<input TYPE="hidden" NAME="i_disc_cond_type" VALUE="<%= g_dformValues("i_disc_cond_type") %>">	
			<div ID="esMaster" CLASS="editSheet" MetaXML="MasterMeta" DataXML="MasterData" LANGUAGE="VBScript" ONCHANGE='setDirty("")' ONREQUIRE='setRequired("")' ONVALID='setValid("")'><h3><%= g_imgLoadingAnimation %> <%= L_DiscLoadingProperties_Text %></h3></div>
	</form>

</div>

</body>
</html>
