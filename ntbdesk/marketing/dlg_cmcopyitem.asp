<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->
<!--#INCLUDE FILE='include/marketingStrings.asp' -->
<%
Dim g_sCampaignConnectionStr, g_sCustId, g_sCampId, g_sQuery, g_rsQuery, _
	g_sCustomerAndCampaignPickers, sDialogTitle

const TYPE_AD = "3"
const TYPE_DM = "4"
const TYPE_DISC= "5"

'get connection string
If IsEmpty(Session("Campaign_ConnectionStr")) Then
	Session("Campaign_ConnectionStr") = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
End If
g_sCampaignConnectionStr = Session("Campaign_ConnectionStr")	

g_sCustId = Request("i_customer_id")
g_sCampId = Request("i_camp_id")

Set g_oConn = oGetADOConnection(g_sCampaignConnectionStr)
Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)

g_sQuery = "SELECT cu.i_customer_id, cu.u_customer_name, ca.i_camp_id, ca.u_camp_name " & _
		" FROM customer AS cu, campaign AS ca " & _
		" WHERE ca.i_customer_id = cu.i_customer_id AND cu.dt_customer_archived IS NULL " & _
		" AND ca.dt_camp_archived IS NULL " & _
		" ORDER BY cu.u_customer_name, ca.u_camp_name "
Set g_rsQuery = g_oConn.Execute(g_sQuery, , AD_CMD_TEXT)

g_sCustomerAndCampaignPickers = sGetCustomerAndCampaignPickers(g_rsQuery)
g_rsQuery.close

select case Request("type")
	case TYPE_AD
		sDialogTitle = L_CMCopyAd_DialogTitle
	case TYPE_DM
		sDialogTitle = L_CMCopyDirectMail_DialogTitle
	case TYPE_DISC
		sDialogTitle = L_CMTypeCopyDiscount_DialogTitle
End select		


'Create Customer item drop-down
function sGetCustomerAndCampaignPickers(rsQuery)
	dim sID, fdCustID, fdCustName, sCustReturn, fdCampName, fdCampID, sCampReturn
	sID = -1
    set fdCustID = rsQuery.fields("i_customer_id")
    set fdCustName = rsQuery.fields("u_customer_name")
	set fdCampID = rsQuery.fields("i_camp_id")
	set fdCampName = rsQuery.fields("u_camp_name")
	'create picker used to load other campaign lists
	sCampReturn = "<select id='efCampaign'><select id='campaign'></select></select>"
	'start picker for customer
	sCustReturn = "<select id='efCustomer'><select id='customer'>"
	Do While Not rsQuery.EOF
		if sID <> fdCustID.Value then
			'add item in customer list
			sCustReturn = sCustReturn & "<option value='" & Quote(fdCustID.Value) & "'>" & g_mscsPage.HTMLEncode(fdCustName.Value) & "</option>"
			'start picker for campaign
			sCampReturn = sCampReturn & "<select id='" & fdCustID.Value & "'><select id='" & fdCustID.Value & "'>"
			sID = fdCustID.Value
		end if
		'add item in campaign list
		sCampReturn = sCampReturn & "<option value='" & Quote(fdCampID.Value) & "'>" & g_mscsPage.HTMLEncode(fdCampName) & "</option>"
		sID = fdCustID.Value 
		g_rsQuery.movenext
		if not g_rsQuery.EOF then
			if sID <> fdCustID.Value then
				'finish picker for campaign
				sCampReturn = sCampReturn & "</select></select>"
			end if
		else
			'finish picker for campaign
			sCampReturn = sCampReturn & "</select></select>"
		end if 
	loop
	sCustReturn = sCustReturn & "</select></select>"
	sGetCustomerAndCampaignPickers = sCustReturn & vbCR & sCampReturn
end function

function Quote(sText)
	If IsNull(sText) Or IsEmpty(sText) Or sText = "" Then Exit Function
	Quote = replace(sText, "'", "\'")
end function
%>
<HTML>
<HEAD>
<TITLE><%= sDialogTitle %></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
	HTML
	{
		Height: 227px;
		Width: 290px;
	}
	BODY
	{
		PADDING: 15px;
		MARGIN: 0;
		MARGIN-TOP: 10px;
		MARGIN-BOTTOM: 10px;
	}
	BUTTON
	{
		WIDTH: 6em;
	}
</STYLE>
<SCRIPT LANGUAGE='VBScript'>
<!--
 	sub setCampaignPicker()
		btnOK.disabled = true
		efCampaign.reload(efCustomer.value)
	end sub

 	sub onChangeCampaignPicker()
		dim elOptions
		set elOptions = efCampaign.options
		if not efCampaign.valid and elOptions.length > 0 then
			efCampaign.value = elOptions(0).getAttribute("value")
		end if
		if efCustomer.valid and efCampaign.valid then btnOK.disabled = false
	end sub

	sub btnCancel_OnClick()
		window.returnValue = "cancel"
		window.close()
	end sub

	sub btnOK_OnClick()		
		dim aReturnValue(2)

		aReturnValue(1) = efCustomer.value
		aReturnValue(2) = efCampaign.value
		 
 		window.returnValue = Join(aReturnValue, "|")
		window.close()
	end sub

	sub setupPickers()
		if efCustomer.valid then
			efCampaign.reload("<%= g_sCustId %>")
			efCampaign.value = "<%= g_sCampId %>"
		end if
		if efCustomer.valid and efCampaign.valid then
			btnOK.disabled = false
			btnOK.focus()
		end if
	end sub

	sub checkPickers()
		if not efCampaign is nothing and not efCustomer is nothing then
			if efCampaign.readystate = "complete" and efCustomer.readystate = "complete" then
				setupPickers()
			else
				window.setTimeout "checkPickers()", 1000
			end if
		end if
	end sub
'-->
</SCRIPT>
</HEAD>
<BODY scroll=no LANGUAGE="VBScript" ONKEYUP="if window.event.keyCode = 27 then btnCancel.click" onload="checkPickers">
	<xml id='efMeta'>
		<editfield>
			<%= g_sCustomerAndCampaignPickers %>
		</editfield>
	</xml>

	<xml id='efData'>
		<document>
			<record>
				<efCustomer><%= g_sCustId %></efCustomer>
			</record>
		</document>
	</xml>

  <table border="0" width="100%" style='TABLE-LAYOUT: fixed'>
    <tr>
      <td><%= L_CMSelectTable_Text %></td>
    </tr>
    <tr>
      <td>
		<DIV ID='efCustomer' CLASS='editField'
			MetaXML='efMeta'
			DataXML='efData'
			LANGUAGE='VBScript'
			onchange='setCampaignPicker'><%= L_LoadingProperty_Text %></DIV>
	  </td>
    </tr>
    <tr>
      <td><%= L_CMSelectCampaigns_Text %></td>
    </tr>
    <tr>
      <td>
		<DIV ID='efCampaign' CLASS='editField'
			MetaXML='efMeta'
			DataXML='efData'
			LANGUAGE='VBScript'
			onchange='onChangeCampaignPicker'><%= L_LoadingProperty_Text %></DIV>
	  </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
      <td>
		<table border="0" width="100%">
		    <td width='100%'>&nbsp;</td>
		    <td><nobr>
				<BUTTON ID='btnOK' CLASS='bdbutton' DISABLED><%= L_BDSSOK_Button %></BUTTON>
				<BUTTON ID='btnCancel' CLASS='bdbutton'><%= L_BDSSCancel_Button %></BUTTON>
			</nobr></td>
		  </tr>
		</table>
    </td>
    </tr>
  </table>

</BODY>
</HTML>