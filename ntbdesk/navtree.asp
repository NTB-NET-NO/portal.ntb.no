<!--#INCLUDE FILE='include/BDHeader.asp' -->
<!--#INCLUDE FILE='include/BizDeskPaths.asp' -->
<!--#INCLUDE FILE='include/BizDeskStrings.asp' -->
<!--#INCLUDE FILE='include/ASPUtil.asp' -->
<%
Dim g_sNavMenu, g_sStartingActionId, g_MSCSActionContainer, _
	ALLOW_CONTEXT_MENUS

ALLOW_CONTEXT_MENUS	= Application("ALLOW_CONTEXT_MENUS")

' -- BizDesk arrow characters (webdings):
const LEFT_ARROW	= "3"
const RIGHT_ARROW	= "4"
const UP_ARROW		= "5"
const DOWN_ARROW	= "6"

g_sNavMenu = sGetNavMenu()

' -- set starting action: either action from URL or default
g_sStartingActionId = Request.QueryString("action")

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	get action dictionary
function dGetAction(byVal sLookupActionID)
	dim MSCSActionContainer, sActionID
	' -- set empty dictionary for if find fails
	set dGetAction = Server.CreateObject("Commerce.Dictionary")
	set dGetAction.tasks = Server.CreateObject("Commerce.SimpleList")
	' -- get action container from application
	set MSCSActionContainer = Application("MSCSActionContainer")
	if isEmpty(MSCSActionContainer) or isNull(MSCSActionContainer.actions) then exit function
	' -- search for dictionary for the current page
	for each sActionID in MSCSActionContainer.actions
		' -- check if action id is contained in lookup id (NOT equals)
		if inStr(sLookupActionID, sActionID) then
			set dGetAction = MSCSActionContainer.actions(sActionID)
			exit for
		end if
	next
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	get HTML markup for a module
function sGetModule(dModule)
	Dim sActionID, sLabel, sTooltip, sOnClick, sClass, sScript
	sActionID	= dModule("action")
	sLabel		= dModule("name")
	sClass		= "bdmodule"
	sScript		= "ONFOCUS='HighlightItem()' ONBLUR='UnhighlightItem()'"
	if SHOW_DEBUG_TEXT then
		sTooltip = dModule("tooltip") & vbCr & "&nbsp;&nbsp;action=" & sActionID
	else
		sTooltip = dModule("tooltip")
	end if
	sGetModule = ""
	
	' -- check security to see if we should display this module
	on error resume next
	if g_MSCSBizDeskSecurity.CanUserAccess(Server.MapPath(g_sBizDeskRoot & split(sActionID, "?")(0))) <> 0 then
		if err.number = 0 then
			' -- add module if no error
			sGetModule = "<DIV ID='" & sActionID & "' NAME='bdmodule' NOWRAP " & _
							"TABINDEX=0 TITLE=""" & replace(sTooltip, """", "&quot;") & """ " & _
							"CLASS='" & sClass & "' LANGUAGE='VBScript' " & _
							sScript & ">" & sLabel & "</DIV>" & vbCr
		elseif SHOW_DEBUG_TEXT then
			' -- show disabled module if error accessing file and debug set
			sClass = "bdmodule bdDebug"
			sTooltip = sTooltip & vbCR & "&nbsp;&nbsp;" & L_DebugModuleError_Text
			sScript = "ONCLICK='window.event.cancelBubble = true'"
			sGetModule = "<DIV ID='" & sActionID & "' NAME='bdmodule' NOWRAP " & _
							"TABINDEX=0 TITLE=""" & replace(sTooltip, """", "&quot;") & """ " & _
							"CLASS='" & sClass & "' LANGUAGE='VBScript' " & _
							sScript & ">" & sLabel & "</DIV>" & vbCr
		end if
	elseif SHOW_DEBUG_TEXT then
		' -- show disabled module if filtered by ACLs and debug set
		sClass = "bdmodule bdDebug"
		sTooltip = sTooltip & vbCR & "&nbsp;&nbsp;" & L_DebugModuleFiltered_Text
		sScript = "ONCLICK='window.event.cancelBubble = true'"
		sGetModule = "<DIV ID='" & sActionID & "' NAME='bdmodule' NOWRAP " & _
						"TABINDEX=0 TITLE=""" & replace(sTooltip, """", "&quot;") & """ " & _
						"CLASS='" & sClass & "' LANGUAGE='VBScript' " & _
						sScript & ">" & sLabel & "</DIV>" & vbCr
	end if
	on error goto 0
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	get HTML markup for a category
function sGetCategory(dCategory)
	Dim sLabel, sKey, sTooltip, slModules, nModule, sModuleText

	sLabel		= dCategory("name")
	sKey		= dCategory("key")
	sTooltip	= dCategory("tooltip")

	set slModules = dCategory("modules")
	sGetCategory = ""
	if slModules.count > 0 then
		' -- add each module
		sModuleText = ""
		for nModule = 0 to slModules.count -1
			sModuleText = sModuleText & sGetModule(slModules(nModule))
		next
		if sModuleText <> "" then
			' -- add category
			sGetCategory = "<DIV ID='" & dCategory.id & "' NAME='bdcategory' " & _
							"NOWRAP TABINDEX=0 LANGUAGE='VBScript' " & _
							"ONFOCUS='HighlightItem()' ONBLUR='UnhighlightItem()' " & _
							"ACCESSKEY='" & sKey & "' OLDACCESSKEY='" & sKey & "' CLASS='bdcategory collapsed' " & _
							"TITLE=""" & replace(sTooltip, """", "&quot;") & """><NOBR STYLE='width:100%'>" & _
							"<SPAN CLASS='bdcollapseicon' " & _
							"TITLE=""" & replace(L_ExpandCollapseCategory_Tooltip, """", "&quot;") & _
							""">" & RIGHT_ARROW & "</SPAN>" & _
							"<SPAN CLASS='bdexpandicon' " & _
							"TITLE=""" & replace(L_ExpandCollapseCategory_Tooltip, """", "&quot;") & """" & _
							">" & DOWN_ARROW & "</SPAN>" & _
							sLabel & "</NOBR>" & vbCr & _
							"<UL CLASS='bdmodulelist' " & _
							"LANGUAGE='VBScript' ONCLICK='DisplayModule()'>" & vbCr & _
							sModuleText & "</UL></DIV>" & vbCr
		end if
	end if
end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	get HTML markup for the navigation tree menu
function sGetNavMenu()
	Dim dNav, slCategories, dCategory, sNavMenu, sCategories
	sNavMenu = ""
	set dNav = Application("MSCSNav")
	set slCategories = dNav.categories
	sNavMenu = sNavMenu & "<DIV ID='bdscrollwindow' CLASS='bdscrollwindow'>"
	sNavMenu = sNavMenu & "<DIV ID='bdmenu' CLASS='bdmenu'>"
	sNavMenu = sNavMenu & "<UL ID='bdcategories' LANGUAGE='VBScript' " & _
					"ONCLICK='ToggleCategory()' ONDBLCLICK='ToggleCategory()' " & _
					"ONMOUSEOVER='HighlightItem()' ONMOUSEOUT='UnhighlightItem()'>"

	sCategories = ""
	' -- display each category
	for each dCategory in slCategories
		sCategories = sCategories & sGetCategory(dCategory)
	next
	if sCategories = "" then sCategories = "<b style='color:red'>" & L_BizDeskIncorrectlyConfigured_ErrorMessage & "</b>"
	sNavMenu = sNavMenu & sCategories & "</UL></DIV></DIV>"
	sNavMenu = sNavMenu & "<DIV ID='bdmenuup' CLASS='scrollbtn' LANGUAGE='VBScript' " & _
					"ONMOUSEUP='onmouseup_ScrollBtn()' ONMOUSEDOWN='onmousedown_ScrollBtn()' " & _
					"ONMOUSEOVER='onmouseover_ScrollBtn()' ONMOUSEOUT='onmouseout_ScrollBtn()' " & _
					"ONFOCUS='onmouseover_ScrollBtn()' ONBLUR='onmouseout_ScrollBtn()' " & _
					"TITLE=""" & replace(L_ScrollUp_ToolTip, """", "&quot;") & """>" & UP_ARROW & "</DIV>"
	sNavMenu = sNavMenu & "<DIV ID='bdmenudown' CLASS='scrollbtn' LANGUAGE='VBScript' " & _
					"ONMOUSEUP='onmouseup_ScrollBtn()' ONMOUSEDOWN='onmousedown_ScrollBtn()' " & _
					"ONMOUSEOVER='onmouseover_ScrollBtn()' ONMOUSEOUT='onmouseout_ScrollBtn()' " & _
					"ONFOCUS='onmouseover_ScrollBtn()' ONBLUR='onmouseout_ScrollBtn()' " & _
					"TITLE=""" & replace(L_ScrollDown_ToolTip, """", "&quot;") & """>" & DOWN_ARROW & "</DIV>"
	sGetNavMenu = sNavMenu
end	function
%>
<HTML>
<HEAD>
	<LINK REL='stylesheet' TYPE='text/css' HREF='include/navtree.css' ID='mainstylesheet'>
	<SCRIPT LANGUAGE='VBScript'>
	<!--
		Option Explicit

		' -- constants for screen dimensions
		const LOGO_HEIGHT		= 55
		const SCROLLBTN_HEIGHT	= 15
		const FOOTER_HEIGHT		= 20
		const SCROLL_AMOUNT		= 20
		' -- constants for key codes
		const KEYCODE_BACKSPACE	= 8
		const KEYCODE_ENTER		= 13
		const KEYCODE_UP		= 38
		const KEYCODE_DOWN		= 40
		const KEYCODE_TAB		= 9
		const KEYCODE_F1 =  112
		const KEYCODE_F2 =  113
		const KEYCODE_F3 =  114
		const KEYCODE_F4 =  115
		const KEYCODE_F5 =  116
		const KEYCODE_F6 =  117
		const KEYCODE_F7 =  118
		const KEYCODE_F8 =  119
		const KEYCODE_F9 =  120
		const KEYCODE_F10 = 121
		const KEYCODE_F11 = 122
		const KEYCODE_F12 = 123

		Dim g_oSelectionModule, g_oSelectionCategory, g_oScrollTimer, winHelpWindow

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	get container with method/property = value
		function elGetContainer(elElement, sKey, sValue)
			set elGetContainer = elElement
			if elElement is nothing then exit function
			dim sEval
			sEval = "elElement." & sKey & " = """ & sValue & """"
			do while elElement.tagName <> "BODY"
				if eval(sEval) then
					set elGetContainer = elElement
					exit function
				end if
				set elElement = elElement.parentElement
			loop
		end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' class routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	add class to element's classname
		sub AddClass(elItem, sNewClass)
			' -- exit if already in class
			if inStr(elItem.className, sNewClass) > 0 then exit sub
			' -- add class to classname
			if Len(elItem.className) > 1 then
				elItem.className = elItem.className & " " & sNewClass
			else
				elItem.className = sNewClass
			end if
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	removes class from element's classname
		sub RemoveClass(elItem, sOldClass)
			' -- exit if not in class
			if inStr(elItem.className, sOldClass) = 0 then exit sub
			' -- remove class from classname and remove extra spaces if any
			elItem.className = Trim(Replace(Replace(elItem.className, sOldClass, ""), "  ", " "))
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	replaces class in element's classname
		sub ReplaceClass(elItem, sOldClass, sNewClass)
			' -- exit if not in class
			if inStr(elItem.className, sOldClass) = 0 then exit sub
			' -- replace old class in classname with new class
			elItem.className = Replace(elItem.className, sOldClass, sNewClass)
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	checks if class is in element's classname
		function bHasClass(elItem, sClass)
			' -- return true if class found in classname
			bHasClass = CBool(inStr(elItem.className, sClass) <> 0)
		end function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' nav tree routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	enable categories' tab index to enable accesskeys across frames
		sub EnableCategoryTabIndexes()
			Dim elCategorys, elCategory
			set elCategorys = document.getElementsByTagName("DIV")
			for each elCategory in elCategorys
				if elCategory.getAttribute("name") = "bdcategory" then elCategory.accesskey = elCategory.getAttribute("oldaccesskey")
			next
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	disable categories' tab index to disable accesskeys across frames
		sub DisableCategoryTabIndexes()
			Dim elCategorys, elCategory
			set elCategorys = document.getElementsByTagName("DIV")
			for each elCategory in elCategorys
				if elCategory.getAttribute("name") = "bdcategory" then elCategory.accesskey = ""
			next
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	toggle a categories' collapsed state
		sub ToggleCategory()
			Dim elCategory
			set elCategory = elGetContainer(window.event.srcElement, "tagName", "DIV")
			if isEmpty(elCategory) then exit sub
			if elCategory is nothing or elCategory.getAttribute("NAME") <> "bdcategory" then exit sub
			if bHasClass(elCategory, "expanded") then
				ReplaceClass elCategory, "expanded", "collapsed"
			else
				ReplaceClass elCategory, "collapsed", "expanded"
			end if
			'workaround: if we don't try to access the category's offsetHeight then menu height is not updated soon enough
			dim foo
			foo = elCategory.offsetHeight
			FixScrollBtns()
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	expand a category
		sub ExpandCategory(sCategory)
			dim elCategory
			set elCategory = document.all(sCategory)
			if not elCategory is nothing then
				ReplaceClass elCategory, "collapsed", "expanded"
				' -- workaround: if we don't try to access the category's offsetHeight 
				'				then menu height is not updated soon enough
				dim foo
				foo = elCategory.offsetHeight
			end if
			FixScrollBtns()
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	get a categories' current collapsed state
		function bIsCollapsedCategory(sCategory)
			dim elCategory
			bIsCollapsedCategory = false
			set elCategory = document.all(sCategory)
			if not elCategory is nothing then
				bIsCollapsedCategory = bHasClass(elCategory, "collapsed")
			end if
		end function

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	select a module and open it's category if necessary
		sub SelectModule(elModule)
			Dim elCategory, oModules, oMod
			set elCategory = elGetContainer(elModule.parentElement, "getAttribute(""name"")", "bdcategory")
			if isEmpty(elCategory) then exit sub
			if elCategory is nothing then exit sub
			' -- unselect old category & module
			ClearSelection()
			' -- select new category
			if bIsCollapsedCategory(elCategory.id) then ExpandCategory(elCategory.id)
			addClass elCategory, "categoryselect"
			set oModules = elCategory.all.tags("UL")(0).children
			for each oMod in oModules
				addClass oMod, "categoryselect"
			next
			set	g_oSelectionCategory = elCategory
			' -- select new module
			addClass elModule, "moduleselect"
			set g_oSelectionModule = elModule
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	clear module selection
		sub ClearSelection()
			dim oModules, oMod
			if g_oSelectionCategory.getAttribute("NAME") = "bdcategory" then
				removeClass g_oSelectionCategory, "categoryselect"
				set oModules = g_oSelectionCategory.all.tags("UL")(0).children
				for each oMod in oModules
					removeClass oMod, "categoryselect"
					removeClass oMod, "modulehighlight"
				next
			end if
			' -- unselect old module
			removeClass g_oSelectionModule, "moduleselect"
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	select and display a module in the right pane
		sub DisplayModule()
			Dim elModule, sModuleId
			set elModule = elGetContainer(window.event.srcElement, "tagName", "DIV")
			if isEmpty(elModule) then exit sub
			if elModule is nothing then exit sub
			if elModule.getAttribute("NAME") = "bdmodule" then
				sModuleId = elModule.id
				SelectModule(elModule)
				window.event.srcElement.blur()
				parent.focus()
				parent.bdbody1.location = sModuleId
			end if
			window.event.returnValue = false
			window.event.cancelBubble = true
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	mouse over: highlight item
		sub	HighlightItem()
			Dim elItem
			set elItem = elGetContainer(window.event.srcElement, "tagName", "DIV")
			if isEmpty(elItem) then exit sub
			if elItem is nothing then exit sub
			if elItem.getAttribute("NAME") = "bdmodule" then
				AddClass elItem, "modulehighlight"
			elseif elItem.getAttribute("NAME") = "bdcategory" then
				AddClass elItem, "categoryhighlight"
			end if
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	mouse out or blur: unhighlight item
		sub UnhighlightItem()
			Dim elItem
			set elItem = elGetContainer(window.event.srcElement, "tagName", "DIV")
			if elItem is nothing then exit sub
			if elItem.getAttribute("NAME") = "bdmodule" then
				RemoveClass elItem, "modulehighlight"
			elseif elItem.getAttribute("NAME") = "bdcategory" then
				RemoveClass elItem, "categoryhighlight"
			end if
		end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' scroll button routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	check to see if need to change scroll button display
		sub FixScrollBtns()
			document.recalc(true)
			if bdmenu.offsetHeight < bdscrollwindow.offsetHeight then
				bdmenu.style.marginTop = 0
				bdmenu.style.Top = 0
			end if
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	scroll up as long as mouse down on "up" button
		sub ScrollUp()
			Dim nMarginTop
			if bdmenu.style.pixelTop < 0 then
				nMarginTop = bdmenu.style.pixelTop + SCROLL_AMOUNT
				if nMarginTop < 0 then
					bdmenu.style.Top = nMarginTop
					g_oScrollTimer = window.setTimeout("ScrollUp()",100)
				else
					bdmenu.style.Top = 0
				end if
			end if
			FixScrollBtns()
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	scroll down as long as mouse down on "down" button
		sub ScrollDown()
			Dim nMarginTop, nAmountMenuLonger
			if bdmenu.offsetHeight > bdscrollwindow.offsetHeight then
				nMarginTop = bdmenu.style.pixelTop - SCROLL_AMOUNT
				nAmountMenuLonger = bdscrollwindow.offsetHeight - bdmenu.offsetHeight
				if nMarginTop >= nAmountMenuLonger then
					bdmenu.style.Top = nMarginTop
					g_oScrollTimer = window.setTimeout("ScrollDown()",100)
				else
					bdmenu.style.Top = nAmountMenuLonger
				end if
			end if
			FixScrollBtns()
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	make scroll button 3D on mouse over
		sub onmouseover_ScrollBtn()
			Dim elBtn
			set elBtn = window.event.srcElement
			elBtn.style.color = "black"
			elBtn.style.border = "thin outset"
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	flatten border on scroll button on mouse over
		sub onmouseout_ScrollBtn()
			Dim elBtn
			set elBtn = window.event.srcElement
			elBtn.style.color = ""
			elBtn.style.border = "thin solid #cccccc"
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	show scroll button pressed on mouse down
		sub onmousedown_ScrollBtn()
			Dim elBtn
			set elBtn = window.event.srcElement
			elBtn.style.color = "black"
			elBtn.style.border = "thin outset"
			if elBtn.id = "bdmenuup" then ScrollUp() else ScrollDown()
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	show scroll button unpressed on mouse up
		sub onmouseup_ScrollBtn()
			Dim elBtn
			set elBtn = window.event.srcElement
			elBtn.style.color = "black"
			elBtn.style.border = "thin outset"
			window.clearTimeout(g_oScrollTimer)
		end sub

		sub openAbout()
			window.showModalDialog "about_dlg.asp", "center: No; help: No; resizable: No; status: No;"
		end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' event handler routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	context menu handler to turn off context menus
		sub document_onContextMenu()
			' -- disallow default context menu unless in entry fields
			if not (window.event.srcElement.tagName = "INPUT" or window.event.srcElement.tagName = "TEXTAREA") then
				window.event.returnValue = <%= CInt(ALLOW_CONTEXT_MENUS) %>
			end if
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	drag start handler to turn off dragging
		sub document_onDragStart()
			' -- disable dragging
			window.event.returnValue = false
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	select start handler to turn off selection except in inputs
		sub document_onSelectStart()
			' -- disallow selection of text on the page except in edit boxes
			Dim elSource
			set elSource = window.event.srcElement
			if elSource.tagName = "TEXTAREA" or _
				elSource.tagName = "INPUT" then
				if (elSource.readonly or elSource.disabled) then window.event.returnValue = false
			else
				window.event.returnValue = false
			end if
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	open help window
		sub OpenHelp()
			if typename(winHelpWindow) = "HTMLWindow2" then winHelpWindow.close
			set winHelpWindow = window.open("<%= g_sBizDeskRoot %>docs/default.asp?helptopic=", "winHelpWindow", _
							 "height=500,width=700,status=no,toolbar=yes,menubar:no,resizable=yes")
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	trap keys to allow arrows in tree
		sub document_onKeyDown()
			dim evt, elSource

			set evt = window.event
			' -- disallow all control key combos
			if evt.ctrlKey then evt.returnValue = false

			set elSource = elGetContainer(evt.srcElement, "tagName", "DIV")
			if bdmenu.contains(elSource) then
				select case evt.keyCode
					case KEYCODE_ENTER
						' -- enter same as click item
						if not elSource is nothing then elSource.click()
					case KEYCODE_BACKSPACE
						' -- disable backspace (browser history.back())
						evt.returnValue = false
					case KEYCODE_UP
						dim i
						' -- search parents children for current item
						for i = 0 to elSource.parentElement.children.length -1
							if elSource.parentElement.children(i) is elSource then
								' -- current item found
								if i = 0 then
									' -- if current item is first in list set focus on parent
									set elSource = elGetContainer(elSource.parentElement, "tagName", "DIV")
									elSource.focus()
								else
									' -- if current item not first in list get previous item in list
									set elSource = elSource.parentElement.children(i - 1)
									if bHasClass(elSource, "expanded") then
										' -- if category is expanded then set focus on last child
										elSource.all.tags("DIV")(elSource.all.tags("DIV").length - 1).focus()
									else
										' -- if category not expanded then set focus on previous item
										elSource.focus()
									end if
								end if
								exit for
							end if
						next
					case KEYCODE_DOWN
						' -- change key to tab
						evt.keyCode = KEYCODE_TAB
					case else
				end select
			end if
			select case evt.keyCode
				case KEYCODE_F1
					' -- open help
					OpenHelp()
					' -- disallow all FN keys
					evt.returnValue = false
					evt.cancelBubble = true
				case KEYCODE_F2, KEYCODE_F3, KEYCODE_F4, KEYCODE_F5, KEYCODE_F6, _
					KEYCODE_F7, KEYCODE_F8, KEYCODE_F9, KEYCODE_F10, KEYCODE_F11, KEYCODE_F12
					' -- disallow all FN keys
					evt.returnValue = false
					evt.cancelBubble = true
			end select
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	recalc scroll buttons and frame width on resize
		sub window_onResize()
			dim nWidth
			FixScrollBtns()
			nWidth = parent.frames(0).document.body.clientWidth
			if nWidth > 5 then parent.g_nNavWidth = nWidth
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	on load display errors and set dynamic properties
		sub window_onLoad()
<%		if g_sStartingActionId <> "" then
%>			' -- set starting module
			parent.bdbody1.location.replace("<%= g_sStartingActionId %>")
<%		end if
		' -- insert code to display errors if flag set
		dim g_slObjectErrors, sError
		if not isEmpty(Session("slObjectErrors")) then
			set g_slObjectErrors = Session("slObjectErrors")
			if g_slObjectErrors.count > 0 then
%>			dim sError
			sError = "<h3><%= L_TheFollowingErrorsFound_ErrorMessage %></h3><ul>"
<%			for each sError in g_slObjectErrors
				if Left(sError, 2) = "__" then
					sError = "<BR>" & Replace(Replace(Replace(Replace(sError, "__", "&nbsp;&nbsp;"), """", "&quot;"), "<", "&lt;"), ">", "&gt;")
				else
					sError = "<LI>" & sError
				end if
%>			sError = sError & "<%= sError %>"
<%			next
%>			sError = sError & "</ul>"
			dim winError
			set winError = window.open("", "winError", "height:300;width:300;menubar=no,scrollbars=yes,toolbar=no,location=no,resizable=yes")
			winError.document.title = "BizDesk Object Errors"
			winError.document.body.innerHTML = sError
<%			end if
		end if
%>			FixScrollBtns()
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY SCROLL='NO' TABINDEX=-1>

<DIV ID='anchor'></DIV>
<SCRIPT LANGUAGE='VBScript'>
<!--
	' -- set selections to an object so they will test as objects if mouse over menu while loading
	set g_oSelectionModule = anchor
	set g_oSelectionCategory = anchor
'-->
</SCRIPT>

<DIV ID='bdlogo' CLASS='bdlogo'>
	<A HREF='javascript:openAbout()' TABINDEX='0' 
		TITLE='<%= replace(L_AboutCommerceServerBizDesk_Tooltip, """", "&quot;") %>'><!--
		--><IMG ID='BDLogoimg' SRC='assets/bdlogo.gif'
			HEIGHT='55' WIDTH='164' BORDER='0'
			TITLE='<%= replace(L_AboutCommerceServerBizDesk_Tooltip, """", "&quot;") %>'><!--
		--></a>
</DIV>

<%= g_sNavMenu %>

<DIV ID='bdfooter' CLASS='bdfooter'>
	<DIV ID='bdcs4'	CLASS='bdcs4'>
		<A HREF='<%= L_WWWMicrosoftCom_Text %>' TABINDEX='0' 
			TITLE="<%= replace(L_VisitMicrosoftCorporationWebSite_Tooltip, """", "&quot;") %>"
			TARGET='new'>
<%			if SHOW_DEBUG_TEXT then
%>				<SPAN CLASS='bdDebug' STYLE='TEXT-ALIGN: center; WIDTH: 80%'><%= L_DebugMode_Text %></SPAN>
<%			else
%>			<%= replace(L_CopyrightMicrosoftCompanyName_Text, """", "&quot;") %>
<%			end if
%>		</A>
	</DIV>
</DIV>
<SCRIPT LANGUAGE='VBScript'>
<!--
	' -- set dynamic properties for menu resizing and button display
	bdscrollwindow.style.setExpression	"height",	"document.body.clientHeight - FOOTER_HEIGHT - LOGO_HEIGHT"
	bdscrollwindow.style.setExpression	"width",	"document.body.clientWidth + 20"
	bdscrollwindow.style.setExpression	"top",		"LOGO_HEIGHT"
	bdfooter.style.setExpression		"top",		"document.body.clientHeight - FOOTER_HEIGHT"
	bdfooter.style.setExpression		"width",	"document.body.clientWidth + 20"
	bdfooter.style.setExpression		"height",	"FOOTER_HEIGHT"
	bdlogo.style.setExpression			"width",	"document.body.clientWidth + 20"
	bdlogo.style.setExpression			"height",	"LOGO_HEIGHT"
	bdmenuup.style.setExpression		"top",		"LOGO_HEIGHT"
	bdmenuup.style.setExpression		"width",	"document.body.clientWidth + 20"
	bdmenuup.style.setExpression		"height",	"SCROLLBTN_HEIGHT"
	bdmenuup.style.setExpression		"display",	"((bdmenu.style.pixelTop < 0) && (bdmenu.offsetHeight >= bdscrollwindow.offsetHeight)) ? ""block"" : ""none"""
	bdmenudown.style.setExpression		"top",		"LOGO_HEIGHT + bdscrollwindow.offsetHeight - SCROLLBTN_HEIGHT"
	bdmenudown.style.setExpression		"width",	"document.body.clientWidth + 20"
	bdmenudown.style.setExpression		"height",	"SCROLLBTN_HEIGHT"
	bdmenudown.style.setExpression		"display",	"((bdmenu.style.pixelTop != (bdscrollwindow.offsetHeight - bdmenu.offsetHeight)) && (bdmenu.offsetHeight > bdscrollwindow.offsetHeight)) ? ""inline"" : ""none"""
'-->
</SCRIPT>

</BODY>
</HTML>
