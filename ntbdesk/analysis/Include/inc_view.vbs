<SCRIPT LANGUAGE='VBScript'>
Option Explicit
'---
const GLOBAL_DELIMITER1 = ";"
'---
const L_SaveChangesTo_Text = "Save changes to "
const L_QuestionMark_Text = "?"
const L_Error_Text = "Error: "
const L_SaveName_Text = "Save %1"
'---
public g_bDirty

   ' ---------------------------------------------------------------------
	Sub Window_OnLoad()
'		setTasktooltip "save", document.forms(0).all("title").value
'		g_bDirty = 0
	End Sub
   ' ---------------------------------------------------------------------
	Sub Window_OnBeforeUnload()
'		if g_bDirty then
'			window.event.returnValue = "If you want to save your changes before leaving the page, click Cancel below and then click the Save icon at the top of the page."
'		end if
	End Sub
   ' ---------------------------------------------------------------------
	Sub OnClose ()
		' inhibit OnBeforeUnload
		g_bDirty = 0
		selectform.submit ()
	End Sub
   ' ---------------------------------------------------------------------
	Sub OnSave (ByVal iEntityType)
		' inhibit OnBeforeUnload
		g_bDirty = 0
		saveform.submit()
	End Sub
   ' ---------------------------------------------------------------------
	Sub OnChange ()
		dim sName
		dim bIsRequired

		g_bDirty = 1

		setTaskTooltip "save", sFormatString(L_SaveName_Text, Array(sName))
		if (bIsRequired = 1) then
			DisableTask "save"
		else
			EnableTask "save"
		end if
	End Sub
   ' ---------------------------------------------------------------------
	Sub OnError ()
		MsgBox L_Error_Text & err.description
	End Sub
</SCRIPT>
