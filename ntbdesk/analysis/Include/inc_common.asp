<%

	Const PAGE_SIZE = 20

	' Analysis Modules
	Const ID_REPORTS		    = 22
	Const ID_REPORT_VIEWER   = 32
	
	' Report Types
	Const Dynamic_OWC_SQL			= 0
	Const Dynamic_OWC_MDX			= 1
	Const Static_SQL					= 2
	Const Static_MDX					= 3

	' Constant declarations for Static reports

	' Report execution states
	Const Report_None							= 0
	Const Report_Failed						= 1
	Const Report_Pending						= 2
	Const Report_Completed					= 3

	' Param Data Types
	Const DataType_boolean					= 0
	Const DataType_date						= 1
	Const DataType_integer					= 2
	Const DataType_float						= 3
	Const DataType_currency					= 4
	Const DataType_text						= 5
	Const DataType_time						= 6

	' Param Types 
	Const ParamType_SingleValue			= 0
	Const ParamType_Expression				= 1
	Const ParamType_SelectOrder			= 2
	Const ParamType_DateRange				= 3
	Const ParamType_SiteName				= 4

	' Param Type Expression Operands
	Const ExpOpnd_None						= 0
	Const ExpOpnd_Equals						= 1
	Const ExpOpnd_NotEquals					= 2
	Const ExpOpnd_IsNull						= 3
	Const ExpOpnd_IsNotNull					= 4
	' Numeric data types only
	Const ExpOpnd_LessThan					= 5
	Const ExpOpnd_LessThanOrEquals		= 6
	Const ExpOpnd_GreaterThan				= 7
	Const ExpOpnd_GreaterThanOrEquals	= 8
	' Char data type only
	Const ExpOpnd_Like						= 9
	Const ExpOpnd_NotLike					= 10

	' Param Type Site Name Operands
	Const SiteNameOpnd_All					= 0
	Const SiteNameOpnd_Equals				= 1
	Const SiteNameOpnd_NotEquals			= 2
	Const SiteNameOpnd_Like					= 3
	Const SiteNameOpnd_NotLike				= 4

	' Param Type Select Order Operands
	Const SelectOrderOpnd_All				= 0
	Const SelectOrderOpnd_Distinct		= 1
	Const SelectOrderOpnd_Top				= 2
	Const SelectOrderOpnd_Bottom			= 3

	' Param Type Date Range Operands
	Const DateRangeOpnd_All					= 0
	Const DateRangeOpnd_From				= 1
	Const DateRangeOpnd_OnOrBefore		= 2
	Const DateRangeOpnd_Today				= 3
	Const DateRangeOpnd_ThisWeek			= 4
	Const DateRangeOpnd_ThisMonth			= 5
	Const DateRangeOpnd_ThisQuarter		= 6
	Const DateRangeOpnd_ThisYear			= 7
	Const DateRangeOpnd_Yesterday			= 8
	Const DateRangeOpnd_LastWeek			= 9
	Const DateRangeOpnd_LastMonth			= 10
	Const DateRangeOpnd_LastQuarter		= 11
	Const DateRangeOpnd_LastYear			= 12

	' Public declarations
	Public g_MSCSDataWarehouseSQLConnStr			' SQL DW connection string
	Public g_MSCSDataWarehouseOLAPConnStr			' OLAP DW connection string
	Public g_sReportLocation							' Default directory for completed static reports
	
	Public g_HTTPServer									' localhost
	Public g_sStartURL									' URL protocol
	Public g_sUser											' bizdesk user
	Dim AUser

	' ----------------------------------------------------------------
	'--- Common Public Declarations
	Public g_nItems				' Number of items in the listsheet
	Public g_nPage					' Listsheet page
	Public g_sSortCol				' Sort column
	Public g_sSortOrd				' Sort order
										'	ASC = 1
										'	DESC = 2
	
	' Strings
	Const L_AnalysisConfigError_DialogTitle = "DW/Analysis configuration error"
	Const L_DBConnStrings_ErrorMessage = "An error occurred while reading the connection strings."
	Const L_AnalysisNotInstalled_ErrorMessage = "Analysis and Data Warehouse components may not be installed."
	Const L_Anonymous_User = "Anonymous"
	
	' ----------------------------------------------------------------
	' Get Datawarehouse Connection Strings and Site properties
	Dim ConnStr
	Dim iIndex
	Dim iLength
	Dim sSiteName
	Dim sDateFormat
	Dim sNumberFormat
	Dim sCurrencyFormat
	Dim sThousandSeparator
	
	On Error Resume Next

	sSiteName = Application("MSCSSiteName")
'	sDateFormat = Application("MSCSDateFormat")
'	sNumberFormat = Application("MSCSNumberFormat")
'	sCurrencyFormat = Application("MSCSCurrencyFormat")

'	Const myLOCALE_STHOUSAND = 15
'	sThousandSeparator = g_MSCSDataFunctions.GetLocaleInfo(myLOCALE_STHOUSAND, Application("MSCSCurrencyLocale"))

	' Get/Set the DW connection string.   
	If (IsEmpty(Session("MSCSDataWarehouseSQLConnStr"))) then   
		g_MSCSDataWarehouseSQLConnStr = GetSiteConfigField("Global Data Warehouse", "connstr_db_dw")
		Session("MSCSDataWarehouseSQLConnStr") = g_MSCSDataWarehouseSQLConnStr
	Else   
		g_MSCSDataWarehouseSQLConnStr = Session("MSCSDataWarehouseSQLConnStr")
	End If

	' Get/Set the OLAP connection string.   
	If (IsEmpty(Session("MSCSDataWarehouseOLAPConnStr"))) then   
		g_MSCSDataWarehouseOLAPConnStr = GetSiteConfigField("Global Data Warehouse", "ConnStr_OLAP")
		Session("MSCSDataWarehouseOLAPConnStr") = g_MSCSDataWarehouseOLAPConnStr
	Else   
		g_MSCSDataWarehouseOLAPConnStr = Session("MSCSDataWarehouseOLAPConnStr")
	End If

	g_HTTPServer = Request.ServerVariables("SERVER_NAME") & ":" & Request.ServerVariables("SERVER_PORT")

	' Get the bizdesk user
	If Request.ServerVariables("REMOTE_USER") <> "" Then
		AUser = Split (Request.ServerVariables("REMOTE_USER"), "\")
		g_sUser = AUser (UBound(AUser))
	Else
		g_sUser = L_Anonymous_User
	End If
	
	If UCase(Request.ServerVariables("HTTPS")) = "ON" then
		g_sStartURL = "https://"
	Else
		g_sStartURL = "http://"
	End if

	If Err.Number <> 0 then
		Call setError(L_AnalysisConfigError_DialogTitle, L_DBConnStrings_ErrorMessage, _
				sGetScriptError(Err), ERROR_ICON_ALERT)
	End if
	
	If g_MSCSDataWarehouseSQLConnStr = "" or g_MSCSDataWarehouseSQLConnStr = "" then 
		Call setError(L_AnalysisConfigError_DialogTitle, L_DBConnStrings_ErrorMessage, _ 
				L_AnalysisNotInstalled_ErrorMessage, ERROR_ICON_ALERT) 
	End if 

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	' date conversion routines:
	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nDay = nGetDay(sValue)
	'		returns integer for day in formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Day value or -1 if not found
	'
	function nGetDay(sValue)
		dim nDayIndex
		nDayIndex = Application("MSCSDayIndex")
		nGetDay = nGetDatePart(sValue, nDayIndex)
		if Left(nGetDay, 1) <> "0" And nGetDay > 0 and nGetDay < 10 then
			nGetDay = "0" & nGetDay
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nMonth = nGetMonth(sValue)
	'		returns integer for m onthin formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Month value or -1 if not found
	'
	function nGetMonth(sValue)
		dim nMonthIndex
		nMonthIndex = Application("MSCSMonthIndex")
		nGetMonth = nGetDatePart(sValue, nMonthIndex)
		if Left(nGetMonth, 1) <> "0" And nGetMonth > 0 and nGetMonth < 10 then
			nGetMonth = "0" & nGetMonth
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'
	'	nYear = nGetYear(sValue)
	'		returns integer for year in formatted value or -1 if invalid format
	'
	'	Arguments:	sValue	String. Date value formatted for site's default locale
	'
	'	Returns:			Integer. Year value or -1 if not found
	'
	function nGetYear(sValue)
		dim nYearIndex, nYear
		nYearIndex = Application("MSCSYearIndex")
		nYear = nGetDatePart(sValue, nYearIndex)
		if nYear > 3000 then
			nGetYear = -1
		else
			nGetYear = nYear
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	gets date part based on index and separator
	function nGetDatePart(sValue, nPartIndex)
		dim sSeparator, aDate
		sSeparator = Application("MSCSDateSeparator")
		nGetDatePart = -1
		if not isNull(sValue) then
			aDate = split(sValue, sSeparator)
			if Ubound(aDate) = 2 then
				if aDate(nPartIndex) <> "" and isNumeric(aDate(nPartIndex)) then
					nGetDatePart = aDate(nPartIndex)
				end if
			end if
		end if
	end function

	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	'	gets international yyyymmdd format date
	function nGetIntlDate(sValue)
		dim nDay, nMonth, nYear 
		nDay = nGetDay(sValue)
		nMonth = nGetMonth(sValue)
		nYear = nGetYear(sValue)
		
		if nDay <> -1 and nMonth <> -1 and nYear <> -1 then
			nGetIntlDate = nYear & nMonth & nDay
		else
			nGetIntlDate = -1
		end if
	end function
	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
	'	gets international yyyymmdd hh:mm:ss format datetime
	function nGetCurrentIntlDateTime()
		dim nDay, nMonth, nYear, nHour, nMinute, nSecond 
		nDay = DatePart("d", Now())
		nMonth = DatePart("m", Now())
		nYear = DatePart("yyyy", Now())
		nHour = DatePart("h", Now())
		nMinute = DatePart("n", Now())
		nSecond = DatePart("s", Now())
	
		if nDay < 10 then nDay = "0" & nDay
		if nMonth < 10 then nMonth = "0" & nMonth
		if nHour < 10 then nHour = "0" & nHour
		if nMinute < 10 then nMinute = "0" & nMinute
		if nSecond < 10 then nSecond = "0" & nSecond
		
		nGetCurrentIntlDateTime = nYear & nMonth & nDay & " " & nHour & ":" & nMinute & ":" & nSecond
	end function
	'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
%>