<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator			AddressHTML		AlternativeText	ArgumentName
'	Button				Checkbox			ComboBox				Command
'	Comment				CustomControl	DialogTitle			DriveControl
'	EditBox				ErrorMessage	FileName				FolderControl
'	FolderName			FontName			FunctionName		General
'	GroupBox				GroupBoxTitle	HTMLText				HTMLTitle
'	KeyName				Keyword			ListBox				MacroAction
'	MenuItem				Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton			ScrollBar		SliderBar			StaticText
'	StatusBar			Style				TabControl			Text
'	ToolTip				TopicTitle		TranslationRecord	Unknown
'	VBScriptText		Version


'Page Title
Const L_Reports_StaticText = "Reports"
Const L_CompletedReports_StaticText = "Completed Reports"

'ListSheet Constants
Const L_Loading_Text = "Loading..."
Const L_LoadingList_Text = "Loading List, Please wait..."

' Status text
Const L_AllDeleted_StatusBar = "All items deleted"
Const L_ErrorDelete_StatusBar = "Error Deleting Record"
Const L_Delete_StatusBar = " %1  Item(s) Deleted"
Const L_ItemsSelected_StatusBar = " %1  Item(s) Selected" 
Const L_OneItemSelected_StatusBar = "1 Item Selected [ %1 ]"
Const L_AllSelected_StatusBar = "All Items Selected"

' Text
Const L_FindBy_Text = "Find By:"
Const L_Delete_Text = "<U>D</U>elete"
Const L_Open_Text = "<U>O</U>pen"
Const L_Done_Text = "<U>D</U>one"
Const L_New_Text = "<U>N</U>ew"
Const L_New2_Text = "New"
Const L_Add_Text = "Add"
Const L_Remove_Text = "Remove"
Const L_Items_Text = "Items:"
Const L_Attributes_Text = "Attributes:"
Const L_Name3_Text = "Name:"
Const L_Type3_Text = "Type:"
Const L_Image_Text = "Image"
Const L_Document_Text = "Document"
Const L_No_Text = "No"
Const L_Yes_Text = "Yes"
Const L_Found_Text = " found"
Const L_Saved_Text = " saved"
Const L_Deleted_Text = " deleted"
Const L_Opened_Text = " opened"
Const L_ItemViewPage_Text = "Item View Page: "

' Find by (Reports and Completed Reports)
Const L_SelectOne_Text = "Select one"
Const L_EnterText_Text = "Enter text"
Const L_ReportName_Text = "Name:"
Const L_ReportCategory_Text = "Category:"
Const L_ReportCreatedBy_Text = "Created By:"
Const L_RunBy_Text = "Run By:"
Const L_DateRun_Text = "Date Run:"
Const L_ReportType_Text = "Type:"
Const L_ReportStatus_Text = "Status:"
Const L_FindNow_Button = "Find Now"
Const L_To_Text = "To:"
Const L_StartDate_Text = "Start Date"
Const L_EndDate_Text = "End Date"

' Report combos
Const L_NameR_ComboBox = "Name"
Const L_Category_ComboBox = "Category"
Const L_CreatedBy_ComboBox = "Created By"
Const L_Type_ComboBox = "Type"
Const L_AnyInfoR_ComboBox = "Any Information"
Const L_AllReportsR_ComboBox = "All Reports"
Const L_Dynamic_ComboBox = "Dynamic"
Const L_Static_ComboBox = "Static"

' Completed report combos
Const L_NameCR_ComboBox = "Name"
Const L_RunBy_ComboBox = "Run By"
Const L_RunDate_ComboBox = "Date Run"
Const L_Status_ComboBox = "Status"
Const L_AnyInfoCR_ComboBox = "Any Information"
Const L_AllReportsCR_ComboBox = "All Reports"
Const L_From_ComboBox = "From"
Const L_OnOrBefore_ComboBox = "On or Before"
Const L_Pending_ComboBox = "Pending"
Const L_Completed_ComboBox = "Completed"
Const L_Failed_ComboBox = "Failed"

' Report column names
Const L_Rpt_Name_Col_Text = "Name"
Const L_Rpt_Category_Col_Text = "Category"
Const L_Rpt_CreatedBy_Col_Text = "Created By"
Const L_Rpt_Type_Col_Text = "Type"

' Completed report column names
Const L_CompRpt_Name_Col_Text = "Name"
Const L_CompRpt_DateRun_Col_Text = "Date Run"
Const L_CompRpt_TimeRun_Col_Text = "Time Run"
Const L_CompRpt_RunBy_Col_Text = "Run By"
Const L_CompRpt_Status_Col_Text = "Status"

' Report types
Const L_ReportDynamic_Text = "Dynamic"
Const L_ReportStatic_Text = "Static"
Const L_ReportGrouped_Text ="Grouped"
Const L_ReportUnknown_Text ="Unknown"

' Report execution states
Const L_ReportPending_Text = "Pending"
Const L_ReportCompleted_Text = "Completed"
Const L_ReportFailed_Text = "Failed"

' dlg_ExportReport.asp strings
Const L_ExportReport_HTMLTitle = "Export Report as a List"
Const L_ListNameEmpty_Message = "List Name cannot be empty."
Const L_ListNameMissing_Message = "Missing list name"
Const L_InvalidListName_Message = "Invalid list name"
Const L_InvalidCharInListName_Message = "List name can not contain '?', '|', '<', '>', '""""', '&', '+', or '%' characters."
Const L_ListType_HTMLText = "List Type:"
Const L_Static_OptionButton = "Static"
Const L_Dynamic_OptionButton = "Dynamic"
Const L_ListName_EditBox = "List name:"
Const L_DWReportName_HTMLText = "Report name:"

' dlg_SaveReport.asp strings
Const L_SaveReport_HTMLTitle = "Save a report"
Const L_Save_RadioButton = "Save"
Const L_SaveAs_RadioButton = "Save As"
Const L_ReportNameEmpty1_Message = "Report name cannot be empty."
Const L_ReportNameMissing1_Message = "Missing report name"
Const L_ReportNameTooLong_Message = "Report name cannot exceed 128 characters."
Const L_ReportNameInvalidChar_Message = "Report name can not contain '<', '>', '|', '""""', or '&' characters."
Const L_ReportDescriptionEmpty1_Message = "Report description cannot be empty."
Const L_ReportDescriptionMissing1_Message = "Missing report description"
Const L_ReportDescriptionTooLong_Message = "Report description cannot exceed 128 characters."
Const L_ReportDescriptionInvalidChar_Message = "Report description can not contain '<', '>', '|', '""""', or '&' characters."
Const L_ReportDefinitionTooLong_Message = "Report long description cannot exceed 64,000 characters."
Const L_ReportDefinitionInvalidChar_Message = "Report long description can not contain '|', '""""', or '&' characters."
Const L_SaveReport_HTMLText = "Save Report:"
Const L_ReportName1_HTMLText = "Report Name:"
Const L_ReportDescription1_HTMLText = "Report description:"
Const L_ReportDefinition1_HTMLText = "Report long description:"

' dlg_ReportParams.asp strings
Const L_RunReport_HTMLTitle = "Run a report"
Const L_ReportParam_EditBox = "Report Parameters"
Const L_ParamName_EditBox = "Parameter Name: "
Const L_ParamDescription_EditBox = "Parameter Description: "
Const L_ParamDateRangeInvalid_ErrorMessage = "The end date must be later than or the same as the start date."
Const L_ParamTopCountMin_ErrorMessage = "The top or bottom count value must be greater than zero."
Const L_ParamTopCountMax_ErrorMessage = "The top or bottom count value can not exceed 2,000,000,000."

' report_edit.asp strings
Const L_ReportProps2_Text = "&lt;U&gt;R&lt;/U&gt;eport Properties"
Const L_R2_Accelerator = "R"
Const L_Name2_EditBox = "Name:"
Const L_Name2_ToolTip = "Name"
Const L_Name2_ErrorMessage = "error for Name"
Const L_Description2_EditBox = "Description:"
Const L_Description2_ToolTip = "Description"
Const L_Description2_ErrorMessage = "error for Description"
Const L_Type2_EditBox = "Report type:"
Const L_Type2_ToolTip = "Report type"
Const L_Type2_ErrorMessage = "error for Report type"
Const L_DateCreated2_EditBox = "Date created:"
Const L_DateCreated2_ToolTip = "Date created"
Const L_DateCreated2_ErrorMessage = "error for Date created"
Const L_CreatedBy2_EditBox = "Created by:"
Const L_CreatedBy2_ToolTip = "Created by"
Const L_CreatedBy2_ErrorMessage = "error for Created by"
Const L_DateModified2_EditBox = "Date modified:"
Const L_DateModified2_ToolTip = "Date modified"
Const L_DateModified2_ErrorMessage = "error for Date modified"
Const L_ModifiedBy2_EditBox = "Modified by:"
Const L_ModifiedBy2_ToolTip = "Modified by"
Const L_ModifiedBy2_ErrorMessage = "error for Modified By"
Const L_Query2_EditBox = "Query:"
Const L_Query2_ToolTip = "Query"
Const L_Query2_ErrorMessage = "error for Query"
Const L_DM2_EditBox = "Export as mailing list?"
Const L_DM2_ToolTip = "Export as mailing list?"
Const L_DM2_ErrorMessage = "error for Export as mailing list"
Const L_UPM2_EditBox = "Export as user list?"
Const L_UPM2_ToolTip = "Export as user list?"
Const L_UPM2_ErrorMessage = "error for Export as user list"
Const L_Loading2_Message = " loading properties, please wait..."
Const L_DynamicSQL_Text ="Dynamic SQL"
Const L_DynamicOLAP_Text ="Dynamic OLAP"
Const L_StaticSQL_Text ="Static SQL"
Const L_StaticOLAP_Text ="Static OLAP"

'Common dialog items
Const L_OK_Button = "OK"
Const L_Cancel_Button = "Cancel"
Const L_Help_Button = "Help"
Const L_Advanced_Button = "Advanced"

'Edit Sheet Constants
Const L_LineItems_Text = "Line Items"
Const L_View_StatusBar = "Report: %1 "

'Error Strings
Const L_GetDetails_DialogTitle = "Get Details"
Const L_ErrorsInPage_ErrorMessage = "<BR>An error occurred while loading!"

Const L_DeleteError_DialogTitle = "Error in Deleting"
Const L_Delete_ErrorMessage = "The record(s) could not be deleted"
Const L_MaxDelete_ErrorMessage = "A maximum of 5000 completed reports can be deleted at one time. Please refine your search query and try again."

Const L_GetCompletedReports_DialogTitle = "Get completed reports"
Const L_GetCompletedReports_ErrorMessage = "An error occurred while reading the completed reports."

Const L_GetReports_DialogTitle = "Get reports"
Const L_GetReports_ErrorMessage = "An error occurred while reading the reports."

%>
