<%	

	dim oSearchInfo
	dim oResultInfo
	dim g_rsCurrentPage
	dim oConn1
	
	' #####################################################################
	' function: IntializeReportGroup
	'	Creates the Simple Find Objects 
	'
	' Input:
	'			
	' Returns:
	'	
	sub IntializeReportGroup()
		On Error Resume Next
		set oSearchInfo = Server.CreateObject("Commerce.Dictionary")
		set oResultInfo = Server.CreateObject("Commerce.Dictionary")
		Set oConn1 = oGetADOConnection(g_MSCSDataWarehouseSQLConnStr)
		On Error GoTo 0
	end sub
		
	' #####################################################################
	' function: rsGetDetails
	'	Gets the Report Details
	'
	' Input:
	'	g_sSubKeyValue - GUID of the report to get details for 
	'		
	' Returns:
	'	The Report Details in a recordset
	'		
	function rsGetDetails(byVal g_sSubKeyValue)
		Dim sQuery
		Dim rsItem
		
		On Error Resume Next
		' Build the queries
		sQuery = "SELECT ReportID GUID, DisplayName, Description, ReportType, " & _
		"CreatedTime, CreatedBy, ModifiedTime, LastModifiedBy, Query, convert(integer, DmExport) DmExport, " & _
		"convert(integer, UpmExport) UpmExport FROM Report Where ReportID = '" & g_sSubKeyValue & "'" 				
        
		' Run query for item properties
		Set rsItem = Server.CreateObject("ADODB.Recordset")
		If IsEmpty(Session("MSCSBDError")) Then
			rsItem.CursorType = AD_OPEN_STATIC
			rsItem.LockType = AD_LOCK_READ_ONLY
			rsItem.Open sQuery, oConn1 
		End if

		set rsGetDetails = rsItem
		Set rsItem = Nothing
		
		if Err.Number <> 0 then 
			Call setError(L_GetDetails_DialogTitle, sGetErrorById("Bad_XML_data"), _
						sGetScriptError(Err), ERROR_ICON_ALERT)
		end if
		On Error GoTo 0
	end function

		
	' #####################################################################
	' function: rsGetRecords
	'	Returns recordset contining Report (definitions) matching the search criteria
	'
	' Input:
	'	dReports			- Session variable containing reports module search criteria 
	'  pagesize			- Current PageSize
	'	nRecordCount	- Number of records returned	
	' Returns:
	'	Recordset containing Reports (definitions) for the Report Module 
	'		
	function rsGetRecords(byval dReports,byval pagesize,byRef nRecordCount)
		Dim out_num_records
		
		On Error Resume Next

		oSearchInfo.Name = ""
		oSearchInfo.Category = ""
		oSearchInfo.CreatedBy = ""
		oSearchInfo.Type = ""
		
		oResultInfo.PageNumber = dReports.Page
		oResultInfo.PageSize = pagesize
		oResultInfo.PageType = dReports.pagetype

		if (dReports.column<>"") then
			oResultInfo.ReportGroupSortColumn = dReports.column
			oResultInfo.SortDirection = dReports.direction
		end if
		   
		select case dReports.selfindby
			case "Name"
				oSearchInfo.Name = dReports.fbreportname
			case "Category"
				oSearchInfo.Category = dReports.fbreportcategory
		   case "CreatedBy"
				oSearchInfo.CreatedBy = dReports.fbcreatedby
			case "Type"
				oSearchInfo.Type = dReports.fbreporttype
			case "Any"								 
				oSearchInfo.Name = dReports.fbreportname
				oSearchInfo.Category = dReports.fbreportcategory
				oSearchInfo.CreatedBy = dReports.fbcreatedby
				oSearchInfo.Type = dReports.fbreporttype
		end select

		set rsGetRecords = rsGetCurrentPage(out_num_records)
		
		nRecordCount = out_num_records
		If Err.Number <> 0 then
			Call setError(L_GetReports_DialogTitle, sGetErrorById("Bad_XML_data"), _
						   sGetScriptError(Err), ERROR_ICON_ALERT)
		End if
		On Error GoTo 0
	end function
	   
	   
	' #####################################################################
	' function: rsGetCompletedReports
	'	Gets the list of completed reports matching the search criteria
	'
	' Input:
	'	dCompletedReports	- Session variable containing completed reports search criteria 
	'  pagesize			- Current PageSize
	'	nRecordCount	- Number of records returned	
	' Returns:
	'	Recordset containing completed reports for the Completed Reports Module 
	'
	function rsGetCompletedReports(byVal dCompletedReports,byval pagesize,byRef nRecordCount)
		Dim out_num_records

		On Error Resume Next

		oSearchInfo.Name = ""
		oSearchInfo.RunBy = ""
		oSearchInfo.RunDateTime = ""
		oSearchInfo.SearchDateTimeStart = ""
		oSearchInfo.SearchDateTimeEnd = ""
		oSearchInfo.ExecutionState = ""

		oResultInfo.PageNumber = dCompletedReports.Page
		oResultInfo.PageSize = pagesize
		oResultInfo.PageType = dCompletedReports.pagetype
		
		if (dCompletedReports.column<>"") then
			oResultInfo.ReportGroupSortColumn = dCompletedReports.column
			oResultInfo.SortDirection = dCompletedReports.direction
		end if
	    
		select case dCompletedReports.selfindby
			case "Name"
				oSearchInfo.Name = dCompletedReports.fbreportname
			case "RunBy"
				oSearchInfo.RunBy = dCompletedReports.fbrunby
			case "RunDateTime"
				oSearchInfo.RunDateTime = dCompletedReports.fbdate
				select case dCompletedReports.fbdate
					case "from"
						oSearchInfo.SearchDateTimeStart = nGetIntlDate(dCompletedReports.start_date)
						oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.end_date)
					case "onbefore"
						oSearchInfo.SearchDateTimeStart = ""
						oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.start_date)
				end select																 
		   case "ExecutionState"
				oSearchInfo.ExecutionState = dCompletedReports.fbreportstatus
			case "Any"								 
				oSearchInfo.Name = dCompletedReports.fbreportname
				oSearchInfo.RunBy = dCompletedReports.fbrunby
				oSearchInfo.RunDateTime = dCompletedReports.fbdate
				select case dCompletedReports.fbdate
					case "from"
						oSearchInfo.SearchDateTimeStart = nGetIntlDate(dCompletedReports.start_date)
						oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.end_date)
					case "onbefore"
						oSearchInfo.SearchDateTimeStart = ""
						oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.start_date)
				end select																 
				oSearchInfo.ExecutionState = dCompletedReports.fbreportstatus
		end select
		   
		set rsGetCompletedReports = rsGetCurrentPage(out_num_records)
		nRecordCount = out_num_records
		
		If Err.Number <> 0 then
			Call setError(L_GetCompletedReports_DialogTitle, sGetErrorById("Bad_XML_data"), _
						   sGetScriptError(Err), ERROR_ICON_ALERT)
		End if
		On Error GoTo 0									 
	end function
		   
	' #####################################################################
	' function: deleteRecords
	'	Deletes one or more reports based on the input ReportIDs
	'
	' Input:
	'	items		- Array containg reportid's of reports to be deleted
	'  thisPage - Page from which the delete operation was called
	' Returns:
	'	
	function deleteRecords(Byval items, ByVal thisPage)
		Dim count
		Dim i
		Dim sWhere
		Dim sQuery
		Dim fs
		Dim rs

		On Error Resume Next
		Select Case thisPage
			Case "analysis_reports.asp"
				sWhere = "Where ReportID"
			Case "analysis_report_viewer.asp"
				sWhere = "Where ReportStatusID"
		End Select
		sWhere = sWhere & " In (" & join(items, ",") & ")"

		If IsEmpty(Session("MSCSBDError")) Then
			Select Case thisPage
				Case "analysis_reports.asp"

					'Delete report definitions
					oConn1.Execute("Delete From Report " & sWhere)
					oConn1.Execute("Delete From ReportDimension " & sWhere)
					oConn1.Execute("Delete From ReportParam " & sWhere)
					oConn1.Execute("Delete From ReportDbObject " & sWhere)
				Case "analysis_report_viewer.asp"
					sQuery = "Select LocationPath + LocationName From ReportStatus " & sWhere
					Set rs = Server.CreateObject("ADODB.Recordset")

					rs.CursorType = AD_OPEN_KEYSET
					rs.LockType = AD_LOCK_PESSIMISTIC
					rs.Open sQuery, oConn1 

					Set fs = Server.CreateObject("Scripting.FileSystemObject")
					If Err.Number = 0 then
						' Delete the completed static report files 
						While not rs.EOF
							fs.DeleteFile (rs.fields(0))
							rs.movenext
						Wend
						Err.Clear
					End If
					Set fs = nothing
					set rs = nothing
					
					' Delete completed reports
					oConn1.Execute("Delete From ReportStatus " & sWhere)
					oConn1.Execute("Delete From ReportInstanceParam " & sWhere)
					oConn1.Execute("Delete From ReportInstanceDbObject " & sWhere)
			End Select 
		End If

		If Err.Number <> 0 then
			Call setError(L_DeleteError_DialogTitle, L_Delete_ErrorMessage, _
						   sGetScriptError(Err), ERROR_ICON_ALERT)
		End if
		deleteRecords = Err.Number
		On Error GoTo 0									 
	end function

	' #####################################################################
	' function: deleteAllRecords
	'	Deletes all the records depending on the search criteria
	'
	' Input:
	'	dSearch	- Dictionary contining the current search criteria
	'  thisPage - Page from which the delete operation was called
	' Returns:
	'

	function deleteAllRecords(byVal dSearch,byVal thisPage)
		Dim rs
		Dim nRecordCount
		Dim sValue
		Dim nResult
		Dim item
		Dim sQuery
		Dim sSubQuery
		Dim sWhere
		Dim fs

		On Error Resume Next

		deleteAllRecords = 0

		select case thisPage
			case "analysis_reports.asp"
				' Delete all not provided for report definitions
			case "analysis_report_viewer.asp"
				oSearchInfo.Name = ""
				oSearchInfo.RunBy = ""
				oSearchInfo.RunDateTime = ""
				oSearchInfo.SearchDateTimeStart = ""
				oSearchInfo.SearchDateTimeEnd = ""
				oSearchInfo.ExecutionState = ""
				 
				select case dSearch.selfindby
					case "Name"
						oSearchInfo.Name = dSearch.fbreportname
					case "RunBy"
						oSearchInfo.RunBy = dSearch.fbrunby
					case "RunDateTime"
						oSearchInfo.RunDateTime = dSearch.fbdate
						select case dSearch.fbdate
							case "from"
								oSearchInfo.SearchDateTimeStart = nGetIntlDate(dCompletedReports.start_date)
								oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.end_date)
							case "onbefore"
								oSearchInfo.SearchDateTimeStart = ""
								oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.start_date)
						end select																 
				   case "ExecutionState"
						oSearchInfo.ExecutionState = dSearch.fbreportstatus
					case "Any"								 
						oSearchInfo.Name = dSearch.fbreportname
						oSearchInfo.RunBy = dSearch.fbrunby
						oSearchInfo.RunDateTime = dSearch.fbdate
						select case dSearch.fbdate
							case "from"
								oSearchInfo.SearchDateTimeStart = nGetIntlDate(dCompletedReports.start_date)
								oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.end_date)
							case "onbefore"
								oSearchInfo.SearchDateTimeStart = ""
								oSearchInfo.SearchDateTimeEnd = nGetIntlDate(dCompletedReports.start_date)
						end select																 
						oSearchInfo.ExecutionState = dSearch.fbreportstatus
				end select

				' Build Where clause - only show static reports which were not exported
				sWhere = " Where ReportStatus.Exported = 0 "

				If oSearchInfo.Name <> "" Then
					sWhere = sWhere & " And ReportStatus.DisplayName Like " & chr(39) & replace(oSearchInfo.Name, "'", "''") & "%" & chr(39)
				End If
					
				If oSearchInfo.RunBy <> "" Then
					sWhere = sWhere & " And ReportStatus.RunBy Like " & chr(39) & replace(oSearchInfo.RunBy, "'", "''") & "%" & chr(39)
				End If

				If oSearchInfo.RunDateTime <> "" Then
					Select Case oSearchInfo.RunDateTime
						Case "from"
							sWhere = sWhere & " And ReportStatus.RunDateTime >= " & chr(39) & oSearchInfo.SearchDateTimeStart & chr(39) & _
							    " And ReportStatus.RunDateTime < DateAdd(day, 1, " & chr(39) & oSearchInfo.SearchDateTimeEnd & chr(39) & ")"
						Case "onbefore"
							sWhere = sWhere & " And ReportStatus.RunDateTime < DateAdd(day, 1, " & chr(39) & oSearchInfo.SearchDateTimeEnd & chr(39) & ")"
					End Select
				End If

				If oSearchInfo.ExecutionState <> "" Then
					Select Case oSearchInfo.ExecutionState
						Case "pending"
							sWhere = sWhere & " And ReportStatus.ExecutionState = " & Report_Pending
						Case "completed"
							sWhere = sWhere & " And ReportStatus.ExecutionState = " & Report_Completed
						Case "failed"
							sWhere = sWhere & " And ReportStatus.ExecutionState <= " & Report_Failed
					End Select
				End If 

				' Delete completed reports
				sSubQuery = " (Select ReportStatusID from ReportStatus" & sWhere & ")"
				sQuery = "Select LocationPath + LocationName From ReportStatus Where ReportStatusID In " & sSubQuery

				Set rs = Server.CreateObject("ADODB.Recordset")
					
				rs.CursorType = AD_OPEN_STATIC
				rs.LockType = AD_LOCK_READ_ONLY
				rs.Open sQuery, oConn1 
				If Err.Number = 0 then 
					If rs.recordcount > 5000 then 
						Call setError(L_DeleteError_DialogTitle, L_Delete_ErrorMessage, _
									   L_MaxDelete_ErrorMessage, ERROR_ICON_INFORMATION)
						deleteAllRecords = 1		' Non-zero to indicate an error condition
					Else
						Set fs = Server.CreateObject("Scripting.FileSystemObject")
						' Delete the completed static reports
						While not rs.EOF
							fs.DeleteFile (rs.fields(0))
							rs.movenext
						Wend
						Err.Clear
						Set fs = nothing
					
						' Delete completed report
 						oConn1.Execute("Delete From ReportInstanceParam Where ReportStatusID In " & sSubQuery)
 						oConn1.Execute("Delete From ReportInstanceDbObject Where ReportStatusID In " & sSubQuery)
 						oConn1.Execute("Delete From ReportStatus Where ReportStatusID In " & sSubQuery)
 					End If
 				End If
				set rs = nothing
		end select

		If Err.Number <> 0 then
			Call setError(L_DeleteError_DialogTitle, L_Delete_ErrorMessage, _
						   sGetScriptError(Err), ERROR_ICON_ALERT)
			deleteAllRecords = Err.Number
		End if
		On Error GoTo 0
	end function

	  
	' #####################################################################
	' function: rsGetCurrentPage
	'	
	'
	' Input:
	'
	' Returns:
	'
	Function rsGetCurrentPage (ByRef nItems)
	' Gets a resultset page
	Dim iIndex
	Dim rs
	Dim rsFields
	Dim bSuccess
	Dim sQuery
	Dim sSQL
	Dim sWhere
	Dim sOrderBy
	Dim AClause
	Dim sType
	Dim oConn
	Dim oCmd
	Dim nPage
	Dim sErrorDialog
	Dim sErrorMessage
		
	On Error Resume Next

	nPage = oResultInfo.PageNumber

	' Build the queries
	Select Case oResultInfo.PageType

		Case "reports"
			sErrorDialog = L_GetReports_DialogTitle
			sErrorMessage = L_GetReports_ErrorMessage
			
			If oSearchInfo.Name <> "" Then
				sWhere = " Where DisplayName Like " & chr(39) & replace(oSearchInfo.Name, "'", "''") & "%" & chr(39)
			End If
				
			If oSearchInfo.Category <> "" Then
				If SWhere <> "" Then
					sWhere = sWhere & " And Category Like " & chr(39) & replace(oSearchInfo.Category, "'", "''") & "%" & chr(39)
				Else
					sWhere = " Where Category Like " & chr(39) & replace(oSearchInfo.Category, "'", "''") & "%" & chr(39)
				End If
			End If
				
			If oSearchInfo.CreatedBy <> "" Then
				If SWhere <> "" Then
					sWhere = sWhere & " And CreatedBy Like " & chr(39) & replace(oSearchInfo.CreatedBy, "'", "''") & "%" & chr(39)
				Else
					sWhere = " Where CreatedBy Like " & chr(39) & replace(oSearchInfo.CreatedBy, "'", "''") & "%" & chr(39)
				End If
			End If

			If oSearchInfo.Type <> "" Then
				sType = " ReportType "
				Select Case oSearchInfo.Type
					Case "dynamic"
						sType = sType & ">= " & Dynamic_OWC_SQL & " And" & sType & "<= " & Dynamic_OWC_MDX 
					Case "static"
						sType = sType & ">= " & Static_SQL & " And" & sType & "<= " & Static_MDX 
				End Select
				If SWhere <> "" Then
					sWhere = sWhere & " And" & sType
				Else
					sWhere = " Where" & sType
				End If
			End If 

			' Build Select and From clauses
 			sQuery = "SELECT ReportID GUID, DisplayName, Category, CreatedBy, ReportType, DmExport, UpmExport, Protected " & _
                  "FROM Report"
				
			' Complete query 	   
			sOrderBy = " Order By " & oResultInfo.ReportGroupSortColumn & " " & oResultInfo.SortDirection
			sQuery = sQuery & sWhere & sOrderBy

		Case "completedreports"
			sErrorDialog = L_GetCompletedReports_DialogTitle
			sErrorMessage = L_GetCompletedReports_ErrorMessage

			' Build Where clause - only show static reports which were not exported
			sWhere = " Where Exported = 0 "

			If oSearchInfo.Name <> "" Then
				sWhere = sWhere & " And DisplayName Like " & chr(39) & replace(oSearchInfo.Name, "'", "''") & "%" & chr(39)
			End If
				
			If oSearchInfo.RunBy <> "" Then
				sWhere = sWhere & " And RunBy Like " & chr(39) & replace(oSearchInfo.RunBy, "'", "''") & "%" & chr(39)
			End If

			If oSearchInfo.RunDateTime <> "" Then
				Select Case oSearchInfo.RunDateTime
					Case "from"
						sWhere = sWhere & " And RunDateTime >= " & chr(39) & oSearchInfo.SearchDateTimeStart & chr(39) & _
						    " And RunDateTime < DateAdd(day, 1, " & chr(39) & oSearchInfo.SearchDateTimeEnd & chr(39) & ")"
					Case "onbefore"
						sWhere = sWhere & " And RunDateTime < DateAdd(day, 1, " & chr(39) & oSearchInfo.SearchDateTimeEnd & chr(39) & ")"
				End Select
			End If

			If oSearchInfo.ExecutionState <> "" Then
				Select Case oSearchInfo.ExecutionState
					Case "pending"
						sWhere = sWhere & " And ExecutionState = " & Report_Pending
					Case "completed"
						sWhere = sWhere & " And ExecutionState = " & Report_Completed
					Case "failed"
						sWhere = sWhere & " And ExecutionState <= " & Report_Failed
				End Select
			End If 

			' Build Select and From clauses
 			sQuery = "SELECT ReportStatusID GUID, DisplayName Name, " & _
 						"LocationPath, LocationName, RunDateTime, RunBy, ExecutionState " & _
                  "FROM ReportStatus"

			' Complete query
			If oResultInfo.ReportGroupSortColumn = "RunDateTime_date" Or oResultInfo.ReportGroupSortColumn = "RunDateTime_time" Then
				' Sort by date and time together
				sOrderBy = " Order By RunDateTime " & oResultInfo.SortDirection
			Else
				sOrderBy = " Order By " & oResultInfo.ReportGroupSortColumn & " " & oResultInfo.SortDirection
			End If
			sQuery = sQuery & sWhere & sOrderBy

		End Select

		' Response.Write (sQuery & "<BR><BR>")

		Set g_rsCurrentPage = Server.CreateObject("ADODB.Recordset")
		g_rsCurrentPage.CursorLocation = AD_USE_CLIENT
		     
		' Add fields to g_rsCurrentPage that are specific to this entity type
		call PrepareRS
		g_rsCurrentPage.Open     

		' Run FindBy/SearchOn query
		Set rs = Server.CreateObject("ADODB.Recordset")

		nItems = 0

		rs.CursorType = AD_OPEN_STATIC
		rs.LockType = AD_LOCK_READ_ONLY
		rs.Open sQuery, oConn1
			 
		If Err.Number = 0 then		' Check for error opening resultset
			' Get desired page
			If Not rs.EOF Then
			   nItems = rs.RecordCount
			   If nPage > 1 Then
					If Not (nItems > (PAGE_SIZE * (nPage - 1))) Then
						nPage = nPage - 1
					End If
					rs.Move (PAGE_SIZE * (nPage - 1))
				End If
			   For iIndex = 1 To PAGE_SIZE
					Call GetCurrentRecord (rs)
			      rs.MoveNext
			      If rs.EOF then exit for
			   Next
				g_rsCurrentPage.UpdateBatch
			   g_rsCurrentPage.MoveFirst
			End If
		End if

		Set rsGetCurrentPage = g_rsCurrentPage
		Set rs = Nothing
      
		If Err.Number <> 0 then
			Call setError(sErrorDialog, sErrorMessage, _
						   sGetScriptError(Err), ERROR_ICON_ALERT)
		End if
		On Error GoTo 0

	End Function

	' #####################################################################
	' sub: PrepareRS
	'
	Sub PrepareRS ()
		' Appends fields to the resultset
      Select Case oResultInfo.PageType
            
			Case "reports"
				g_rsCurrentPage.Fields.Append "GUID", AD_BSTR, 128
 				g_rsCurrentPage.Fields.Append "DisplayName", AD_BSTR, 128
				g_rsCurrentPage.Fields.Append "Category", AD_BSTR, 128
				g_rsCurrentPage.Fields.Append "CreatedBy", AD_BSTR, 128
				g_rsCurrentPage.Fields.Append "ReportType", AD_TINYINT
				g_rsCurrentPage.Fields.Append "iReportType", AD_TINYINT
				g_rsCurrentPage.Fields.Append "DmExport", AD_BOOLEAN
				g_rsCurrentPage.Fields.Append "UpmExport", AD_BOOLEAN
				g_rsCurrentPage.Fields.Append "Protected", AD_BOOLEAN

			Case "completedreports"
				g_rsCurrentPage.Fields.Append "GUID", AD_BSTR, 128
 				g_rsCurrentPage.Fields.Append "Name", AD_BSTR, 128
 				g_rsCurrentPage.Fields.Append "LocationName", AD_BSTR, 255
 				g_rsCurrentPage.Fields.Append "LocationPath", AD_BSTR, 255
				g_rsCurrentPage.Fields.Append "RunDateTime", AD_DB_TIMESTAMP
				g_rsCurrentPage.Fields.Append "RunBy", AD_BSTR, 255
				g_rsCurrentPage.Fields.Append "ExecutionState", AD_TINYINT

      End Select
   End Sub

	' #####################################################################
	' sub: GetCurrentRecord
	'	
	' Input:
	'
   Sub GetCurrentRecord (ByRef rs)
		' Fills in one row in the current page from a row in the resultset
		Dim dMod, sTxt, iRefC
        
		g_rsCurrentPage.AddNew
		g_rsCurrentPage.Fields("GUID").Value = rs.Fields("GUID").value

      Select Case oResultInfo.PageType
			Case "reports"  
				g_rsCurrentPage.Fields("DisplayName").Value = rs.Fields("DisplayName").value
				g_rsCurrentPage.Fields("Category").Value = rs.Fields("Category").value
				g_rsCurrentPage.Fields("CreatedBy").Value = rs.Fields("CreatedBy").value
				g_rsCurrentPage.Fields("ReportType").Value = rs.Fields("ReportType").value
				g_rsCurrentPage.Fields("iReportType").Value = rs.Fields("ReportType").value
				g_rsCurrentPage.Fields("DmExport").Value = rs.Fields("DmExport").value
				g_rsCurrentPage.Fields("UpmExport").Value = rs.Fields("UpmExport").value
				g_rsCurrentPage.Fields("Protected").Value = rs.Fields("Protected").value
			Case "completedreports"
				g_rsCurrentPage.Fields("Name").Value = rs.Fields("Name").value
				g_rsCurrentPage.Fields("LocationName").Value = rs.Fields("LocationName").value
				g_rsCurrentPage.Fields("LocationPath").Value = rs.Fields("LocationPath").value
				g_rsCurrentPage.Fields("RunDateTime").Value = rs.Fields("RunDateTime").value
				g_rsCurrentPage.Fields("RunBy").Value = rs.Fields("RunBy").value
				g_rsCurrentPage.Fields("ExecutionState").Value = rs.Fields("ExecutionState").value
      End Select
   End Sub

	' #####################################################################
	' sub: CleanUp
	'	Cleans up the objects
	'
	sub CleanUp()
		On Error Resume Next
		set oSearchInfo = Nothing
		set oResultInfo = Nothing
		oConn1.close
		set oConn1 = Nothing
		Session("MSCSBDError") = empty
	end sub
	
%>