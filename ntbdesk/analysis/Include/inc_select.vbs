<SCRIPT LANGUAGE='VBScript'>
<!--

Option Explicit

	const L_Open_Text = "Open"
	const L_Run_Text = "Run"
	const L_Export_Text = "Export"
	const L_Delete_Text = "Delete"
	const L_ReportsToDelete_Text = "Reports to delete: %1"
	const L_ReportsToOpen_Text = "Reports to open: %1"
	const L_OpenName_Text = "Open %1"
	const L_DeleteName_Text = "Delete %1"
	const L_RunName_Text = "Run %1"
	const L_ExportName_Text = "Export %1"
	const L_AllReportsSelected_Text = "All reports selected"
	const L_ReportSelected_Text = "Report selected: "
	const L_ReportsSelected_Text = "Reports selected: "
	Const L_AllDelete_Message = "Delete all reports?"
	Const L_DeleteReport_Message = "Delete %1?"	
	Const L_DeleteReports_Message = "Delete %1 reports?"	
	Const L_ReportExecutionFailed_Text = "Report execution failed: %1"
	Const L_ExecutingStaticReport_Text = "Executing static report: %1"
	Const L_ReportExportFailed_Text = "Report export failed: %1"
	Const L_ReportExportedAsList_Text = "Report exported as list: %2"
	Const L_DeleteReport_DialogTitle = "Delete Reports?"
	Const L_InvalidDateRange_ErrorMessage = "The end date must be greater than or equal to the start date."
	Const L_ReportError_DialogTitle = "Report error"
	Const L_ReportExportError_ErrorMessage = "Error during report export. "
	Const L_ReportExecError_ErrorMessage = "Error during report execution. "
	Const L_IISCommFailure_ErrorMessage = "IIS communications failure."

   Dim m_nItemsSelected				' Number of items selected in the listsheet
   Dim m_ASelectedItems ()			' Holds GUID of selected items (Reports, Calcs, etc.)
   Dim m_ASelectedNames ()			' Holds name of selected items
   Dim m_ASelectedLocNames ()		' Holds LocationName of selected completed reports
   Dim m_ASelectedLocPaths ()		' Holds LocationPath of selected completed reports
   Dim m_sReportType					' Report type text
   Dim m_iReportType					' Report type number
   Dim m_bDmExport					' Report contains "email" field
   Dim m_bUpmExport					' Report contains "guid" field
   Dim m_bProtected					' Indicates if report is protected
	Dim m_bAll							' Flag for all reports selected
	Dim m_nNonViewable				' Number of selected completed reports that can't be viewed

   ReDim m_ASelectedItems (<% =PAGE_SIZE %>)
   ReDim m_ASelectedNames (<% =PAGE_SIZE %>)
   ReDim m_ASelectedLocNames (<% =PAGE_SIZE %>)
	ReDim m_ASelectedLocPaths (<% =PAGE_SIZE %>)

   '------------------------------------------------------------
   Sub AddItem (ByVal iEntityType)
      Dim iIndex
      Dim oSelection
      Dim sKeyValue
      Dim sSubKeyValue 
        
      Set oSelection = window.event.XMLrecord
      sKeyValue = oSelection.selectSingleNode("./<%= g_sKey %>").text			' GUID
		sSubKeyValue = oSelection.selectSingleNode("./<%= g_sSubKey %>").text	' Name
      For iIndex = 0 to <% =PAGE_SIZE %> - 1
         If (m_ASelectedItems(iIndex) = empty) then
            m_ASelectedItems(iIndex) = sKeyValue
            m_ASelectedNames(iIndex) = sSubKeyValue
            If iEntityType = <% =ID_REPORTS %> Then
					m_sReportType = oSelection.selectSingleNode("./ReportType").text
					m_iReportType = oSelection.selectSingleNode("./iReportType").text
					m_bProtected = oSelection.selectSingleNode("./Protected").text
   				' Export flags apply to static reports only.
   				m_bDmExport = oSelection.selectSingleNode("./DmExport").text				
					m_bUpmExport = oSelection.selectSingleNode("./UpmExport").text				
            End If
            If iEntityType = <% =ID_REPORT_VIEWER %> Then
            	m_ASelectedLocNames(iIndex) = oSelection.selectSingleNode("./LocationName").text
					m_ASelectedLocPaths(iIndex) = oSelection.selectSingleNode("./LocationPath").text
					If oSelection.selectSingleNode("./ExecutionState").text <> "<% =L_ReportCompleted_Text %>" Then
						m_nNonViewable = m_nNonViewable + 1
					End If
					m_bAll = 0
					deleteform.elements.selectall.value = 0
            End If
            Exit For
         End If
      Next
	   m_nItemsSelected = m_nItemsSelected + 1      
   End Sub
   '------------------------------------------------------------
   Sub RemoveItem (ByVal iEntityType)
      Dim iIndex
      Dim oSelection
      Dim sKeyValue
        
      set oSelection = window.event.XMLrecord
      sKeyValue = oSelection.selectSingleNode("./<%= "GUID" %>").text
      For iIndex = 0 to <% =PAGE_SIZE %> - 1
         If (m_ASelectedItems(iIndex) = sKeyValue) then
            m_ASelectedItems(iIndex) = empty
            m_ASelectedNames(iIndex) = empty
            If iEntityType = <% =ID_REPORTS %> Then
					m_sReportType = empty
					m_iReportType = empty
					m_bProtected = empty
   				m_bDmExport = empty			
					m_bUpmExport = empty				
            End If
            If iEntityType = <% =ID_REPORT_VIEWER %> Then
					m_ASelectedLocNames(iIndex) = empty
					m_ASelectedLocPaths(iIndex) = empty
					If oSelection.selectSingleNode("./ExecutionState").text <> "<% =L_ReportCompleted_Text %>" Then
						m_nNonViewable = m_nNonViewable - 1
					End If
					m_bAll = 0
					deleteform.elements.selectall.value = 0
            End If
            Exit For
         End If
      Next
	   m_nItemsSelected = m_nItemsSelected - 1      
   End Sub
   '------------------------------------------------------------
	Sub AddAllItems(ByVal iEntityType)
		Call RemoveAllItems (iEntityType)

      If iEntityType = <% =ID_REPORT_VIEWER %> Then
			m_bAll = 1
			m_nNonViewable = 0
			deleteform.elements.selectall.value = 1
		End If
	End Sub
   '------------------------------------------------------------
	Sub RemoveAllItems(ByVal iEntityType)
		Dim iIndex
		
      For iIndex = 0 to <% =PAGE_SIZE %> - 1
         m_ASelectedItems(iIndex) = empty
         m_ASelectedNames(iIndex) = empty
         If iEntityType = <% =ID_REPORT_VIEWER %> Then
				m_ASelectedLocNames(iIndex) = empty
				m_ASelectedLocPaths(iIndex) = empty
         End If 
      Next

      If iEntityType = <% =ID_REPORT_VIEWER %> Then
			m_bAll = 0
			m_nNonViewable = 0
			deleteform.elements.selectall.value = 0
		End If
	   m_nItemsSelected = 0
	End Sub
   '------------------------------------------------------------
	Function GUIDOfSelectedItem ()
		Dim iIndex
		
		For iIndex=0 to <% =PAGE_SIZE %> - 1
			If (empty <> m_ASelectedItems (iIndex)) then
				GUIDOfSelectedItem = m_ASelectedItems (iIndex)
				Exit Function
			End If
		Next	
	End Function
   '------------------------------------------------------------
	Function NameOfSelectedItem ()
		Dim iIndex
		
		If (m_nItemsSelected > 1 or m_nItemsSelected = 0) then
			NameOfSelectedItem = ""
			Exit Function
		End If
		
		For iIndex=0 to <% =PAGE_SIZE %> - 1
			If (empty <> m_ASelectedItems (iIndex)) then
				NameOfSelectedItem = m_ASelectedNames (iIndex)
				Exit Function
			End If
		Next	
	End Function
   '------------------------------------------------------------
	Sub OnSelectRow(ByVal iEntityType)
      Call AddItem (iEntityType)
		SetTaskButtons iEntityType     
		DisplaySelectedCount iEntityType
   End Sub
   '------------------------------------------------------------
	Sub OnRowUnselect(ByVal iEntityType)
      Call RemoveItem (iEntityType)
		SetTaskButtons iEntityType     
		DisplaySelectedCount iEntityType
   End Sub
   '------------------------------------------------------------
   Sub OnAllRowsSelect (ByVal iEntityType)
		Call AddAllItems (iEntityType)
		SetTaskButtons iEntityType     
		DisplaySelectedCount iEntityType
   End Sub
   '------------------------------------------------------------
   Sub OnAllRowsUnselect (ByVal iEntityType)
		Call RemoveAllItems (iEntityType)
		SetTaskButtons iEntityType     
		DisplaySelectedCount iEntityType
   End Sub
   '------------------------------------------------------------
	Sub SetTaskButtons (ByVal iEntityType)     
		Dim sName

		If m_nItemsSelected <= 0 Then
			Select Case iEntityType
				Case <% =ID_REPORTS %>
					DisableTask "open"
					DisableTask "run"
					DisableTask "delete"
					DisableTask "export"
					EnableTask "find"
					setTaskTooltip "open", L_Open_Text
					setTaskTooltip "run", L_Run_Text
					setTaskTooltip "delete", L_Delete_Text
					setTaskTooltip "export", L_Export_Text
				Case <% =ID_REPORT_VIEWER %>
					If m_bAll = 1 then
						EnableTask "delete"
					Else
						DisableTask "delete"
					End If
					DisableTask "open"
					EnableTask "find"
					setTaskTooltip "delete", L_Delete_Text
					setTaskTooltip "open", L_Open_Text
			End Select
		ElseIf m_nItemsSelected = 1 Then			
			sName = NameOfSelectedItem ()		
			Select Case iEntityType
				Case <% =ID_REPORTS %>
					EnableTask "open"
					EnableTask "run"
					If m_bProtected Then
						DisableTask "delete"
						setTaskTooltip "delete", L_Delete_Text
					Else
						EnableTask "delete"
						setTaskTooltip "delete", sFormatString(L_DeleteName_Text, array(sName))
					End If 
					If m_sReportType = "<% =L_ReportStatic_Text %>" Then
						EnableTask "export"
						setTaskTooltip "export", sFormatString(L_ExportName_Text, array(sName))
					Else
					DisableTask "export"
						setTaskTooltip "export", L_Export_Text
					End If
					EnableTask "find"
					setTaskTooltip "open", sFormatString(L_OpenName_Text, array(sName))
					setTaskTooltip "run", sFormatString(L_RunName_Text, array(sName))
				Case <% =ID_REPORT_VIEWER %>
					EnableTask "delete"
					If m_nNonViewable = 0 Then
						EnableTask "open"
						setTaskTooltip "open", sFormatString(L_OpenName_Text, array(sName))
					Else
						DisableTask "open"
						setTaskTooltip "open", L_Open_Text
					End If
					EnableTask "find"
					setTaskTooltip "delete", sFormatString(L_DeleteName_Text, array(sName))
			End Select
		Else
			Select Case iEntityType
				Case <% =ID_REPORTS %>
					DisableTask "open"
					DisableTask "run"
					If m_bProtected Then
						DisableTask "delete"
						setTaskTooltip "delete", L_Delete_Text
					Else
						EnableTask "delete"
						setTaskTooltip "delete", sFormatString(L_ReportsToDelete_Text, Array(m_nItemsSelected))
					End If 
					DisableTask "export"
					setTaskTooltip "open", L_Open_Text
					setTaskTooltip "run", L_Run_Text
					setTaskTooltip "export", L_Export_Text
				Case <% =ID_REPORT_VIEWER %>
					EnableTask "delete"
					If m_nNonViewable = 0 Then
						EnableTask "open"
						setTaskTooltip "open", sFormatString(L_ReportsToOpen_Text, array(m_nItemsSelected))
					Else
						DisableTask "open"
						setTaskTooltip "open", L_Open_Text
					End If
					setTaskTooltip "delete", sFormatString(L_ReportsToDelete_Text, array(m_nItemsSelected))
			End Select
		End If
		
	End Sub
   '------------------------------------------------------------
	sub EnableFind(byVal thisPage)
		Dim sValue
		
		sValue = selfindby.value
		
		select case thispage
			case "analysis_reports.asp"
				select case sValue
					case "Name"
						If fbreportname.value <> "" And fbreportname.valid Then
							document.all.btnfindby.disabled = 0
						Else
							document.all.btnfindby.disabled = 1
						End If
					case "Category"
						If fbreportcategory.value <> "" And fbreportcategory.valid Then
							document.all.btnfindby.disabled = 0
						Else
							document.all.btnfindby.disabled = 1
						End If
					case "CreatedBy"
						If fbcreatedby.value <> "" And fbcreatedby.valid Then
							document.all.btnfindby.disabled = 0
						Else
							document.all.btnfindby.disabled = 1
						End If
					case "Type"
						If fbreporttype.valid Then
							document.all.btnfindby.disabled = 0
						Else
							document.all.btnfindby.disabled = 1
						End If
					case "Any"
						if fbreportname.valid = 0 Or fbreportcategory.valid = 0 Or fbcreatedby.valid = 0 Then
							document.all.btnfindby.disabled = 1
						elseif (fbreportname.value <> "" Or fbreportcategory.value <> "" Or fbcreatedby.value <> "" Or fbreporttype.valid) Then
							document.all.btnfindby.disabled = 0
						else
							document.all.btnfindby.disabled = 1
						end if
				end select
			case "analysis_report_viewer.asp"
				select case sValue
					case "Name"
						If fbreportname.value <> "" And fbreportname.valid Then
							document.all.btnfindby.disabled = 0
						Else
							document.all.btnfindby.disabled = 1
						End If
					case "RunBy"
						If fbrunby.value <> "" And fbrunby.valid Then
							document.all.btnfindby.disabled = 0
						Else
							document.all.btnfindby.disabled = 1
						End If
					case "RunDateTime"
						if (fbdate.value = "from") then
							if end_date.valid and start_date.valid and (not end_date.required) and (not start_date.required) then
								if dtGetDate(end_date.value) - dtGetDate(start_date.value) >= 0 then
									' Disable find if end_date < start_date
									document.all.btnfindby.disabled = 0
								else
									document.all.btnfindby.disabled = 1
								end if
							else
								document.all.btnfindby.disabled = 1
							end if	
						elseif (fbdate.value = "onbefore") then
							if start_date.valid and (not start_date.required) then
								document.all.btnfindby.disabled = 0
							else
								document.all.btnfindby.disabled = 1
							end if
						else
							document.all.btnfindby.disabled = 1
						end if
					case "ExecutionState"
						If fbreportstatus.valid Then
							document.all.btnfindby.disabled = 0
						Else
							document.all.btnfindby.disabled = 1
						End If
					case "Any"
						if fbreportname.valid = 0 Or fbrunby.valid = 0 Then
							document.all.btnfindby.disabled = 1
						elseif (fbdate.value = "from") then
							if end_date.valid and start_date.valid  and (not end_date.required) and (not start_date.required) then
								if dtGetDate(end_date.value) - dtGetDate(start_date.value) >= 0 then
									' Disable find if end_date < start_date
									document.all.btnfindby.disabled = 0
								else
									document.all.btnfindby.disabled = 1
								end if
							else
								document.all.btnfindby.disabled = 1
							end if	
						elseif (fbdate.value = "onbefore") then
							if start_date.valid and (not start_date.required) then
								document.all.btnfindby.disabled = 0
							else
								document.all.btnfindby.disabled = 1
							end if
						elseif fbreportname.value <> "" Or fbrunby.value <> "" Or fbreportstatus.valid Then
							document.all.btnfindby.disabled = 0
						else
							document.all.btnfindby.disabled = 1
						end if
				end select
		end select
   end sub	
   '------------------------------------------------------------
	sub CheckDate(byVal thisPage)
		Dim sValue
		
		sValue = selfindby.value
		
		if thisPage = "analysis_report_viewer.asp" then
			if sValue = "RunDateTime" Or sValue = "Any" then
				if (fbdate.value = "from") then
					if end_date.valid and start_date.valid then
						if dtGetDate(end_date.value) - dtGetDate(start_date.value) < 0 then
							msgbox(L_InvalidDateRange_ErrorMessage)
						end if
					end if	
				end if
			end if
		end if
		
		EnableFind thisPage
		
   end sub	
   '------------------------------------------------------------
   Sub OnOpen (ByVal iEntityType)
		Dim objNewWindow
		Dim iIndex

		Select Case iEntityType
			Case <% =ID_REPORTS %>
            For iIndex=0 to <% =PAGE_SIZE %> - 1
               If (m_ASelectedItems (iIndex) <> empty) then
						openform.elements.<%= g_sKey %>.value = m_ASelectedItems (iIndex)
						openform.elements.<%= g_sSubKey %>.value = m_ASelectedNames (iIndex)
                  Exit For
               End If
            Next
				openform.submit ()
			Case <% =ID_REPORT_VIEWER %>
            For iIndex=0 to <% =PAGE_SIZE %> - 1
               If (m_ASelectedLocNames (iIndex) <> empty and m_ASelectedLocPaths (iIndex) <> empty) then

					   Set objNewWindow = Window.Open("<%= g_sStartURL %><%= g_HTTPServer %><% =g_sBizDeskRoot %>Analysis/Completed%20Reports/" & m_ASelectedLocNames (iIndex), "Static" & sRandom, "")
						' Window display options:
						'menubar				Show the default browser menus
						'scrollbars			Show vertical and horizontal scrollbars
						'resizeable			Window can or cannot be resized by user
						'status				Show the status bar at the bottom of the window 
						'toolbar				Show the default browser toolbars
						'location			Show the URL address text box
						'directories		Show directory buttons
						'fullscreen			Maximize new window or leave default size 
						'channelmode		Show channel controls
						'width				Width of the window (in pixels)
						'height				Height of the window (in pixels)
						'top					Top position of the window (in pixels)
						'left					Left position of the window (in pixels)

               End If
            Next
		End Select
		SetTaskButtons iEntityType     
	End Sub
   '------------------------------------------------------------
	Sub OnExport ()
		Dim winargs
		Dim Awinargs
		Dim retvalue
		Dim iReportStatusID		
		Dim iParams					' Number of report params
		Dim sReportName
		Dim iReportID
		Dim ReportParams
		Dim ExportParams
		Dim iIndex
		Dim sErrMsg
		Dim xmlDOMDoc
		Dim xmlErrorNodes
		Dim dResponseXMLAsDict

		On Error Resume Next

      For iIndex=0 to <% =PAGE_SIZE %> - 1
         If (m_ASelectedItems (iIndex) <> empty) then
            iReportID = m_ASelectedItems (iIndex)
            sReportName = Trim(m_ASelectedNames (iIndex))
            Exit For
         End If
      Next

		' Create a ReportStatus entry
		RunOrExportXMLform("ReportID").value = iReportID
		RunOrExportXMLform("Export").value = 1
		RunOrExportXMLform("Add").value = 1
		RunOrExportXMLform.action = "analysis_report_instance.asp"
		set xmlDOMDoc = xmlPostFormViaXML(RunOrExportXMLform)

		If err <> 0 then
			sErrMsg = Err.description
			If err = 424 or err = 438 then sErrMsg = L_IISCommFailure_ErrorMessage	
			msgbox L_ReportExportError_ErrorMessage & sErrMsg, , L_ReportError_DialogTitle
			DeleteReportInstance iReportStatusID
			exit sub
		End If

		' Process any returned error node.
		set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
		if xmlErrorNodes.length <> 0 or xmlDOMDoc.documentElement is nothing then
			'report errors and set status text
			SetStatusText(sFormatString(L_ReportExportFailed_Text, array(sReportName)))
			ShowXMLNodeErrors(xmlErrorNodes)
			SetTaskButtons <% =ID_REPORTS %>
			exit sub
		end if

		' Get the responses
		Set dResponseXMLAsDict = dDictFromXML(xmlDOMDoc)
		iReportStatusID = dResponseXMLAsDict("ReportStatusID")
		iParams = dResponseXMLAsDict("Params")

		If iReportStatusID > 0 Then
			If iParams > 0 Then
				' Get report params from operator
				winargs = "ReportID=" & iReportID & "&ReportStatusID=" & iReportStatusID
				ReportParams = window.showModalDialog("dlg_report_params.asp?" & winargs, , "status:no;help:no")
								
				If ReportParams = -1 Or ReportParams = Empty then		' Cancel pressed
					DeleteReportInstance iReportStatusID
					Exit Sub
				End If
			End If
			
			' Show export dialog
			Awinargs = Split(iReportStatusID & "|" & sReportName & "|" & m_bDmExport & "|" & m_bUpmExport, "|")
			ExportParams = window.showModalDialog("dlg_ExportReport.asp?", Awinargs, "dialogHeight:440px;dialogWidth:440px;status:no;help:no")

			If ExportParams = -1 Or ExportParams = Empty then
				DeleteReportInstance iReportStatusID
				exit sub
			End If 
		
			' Export report
			exportform("listdata").value = Cstr(ExportParams)
			set xmlDOMDoc = xmlPostFormViaXML(exportform)

			If err <> 0 then
				sErrMsg = Err.description
				If err = 424 or err = 438 then sErrMsg = L_IISCommFailure_ErrorMessage	
				msgbox L_ReportExportError_ErrorMessage & sErrMsg, , L_ReportError_DialogTitle
				DeleteReportInstance iReportStatusID
				exit sub
			End If

			' Process any returned error node.
			set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
			if xmlErrorNodes.length <> 0 or xmlDOMDoc.documentElement is nothing then
				'report errors and set status text
				SetStatusText(sFormatString(L_ReportExportFailed_Text, array(sReportName)))
				ShowXMLNodeErrors(xmlErrorNodes)
			else
				SetStatusText(sFormatString(L_ReportExportedAsList_Text, split(exportform.listdata.value, "|")))
			end if

		End If

		SetTaskButtons <% =ID_REPORTS %>    

	End Sub
   '------------------------------------------------------------
	Sub DeleteReportInstance(ReportStatusID)
		Dim xmlDOMDoc

		On Error Resume Next

		' Delete report instance
		RunOrExportXMLform("ReportStatusID").value = ReportStatusID
		RunOrExportXMLform("Add").value = 0
		RunOrExportXMLform.action = "analysis_report_instance.asp"
		set xmlDOMDoc = xmlPostFormViaXML(RunOrExportXMLform)

		SetTaskButtons <% =ID_REPORTS %>     

	End Sub
   '------------------------------------------------------------
	Sub OnRun ()
		Dim objNewWindow
		Dim iReportID
		Dim sReportName
		Dim iReportStatusID
		Dim iParams					' Number of report params
		Dim winargs
		Dim retvalue
		Dim ReportParams
		Dim iIndex
		Dim sErrMsg
		Dim xmlDOMDoc
		Dim xmlErrorNodes
		Dim dResponseXMLAsDict
		
		On Error Resume Next
 
      For iIndex=0 to <% =PAGE_SIZE %> - 1
         If (m_ASelectedItems (iIndex) <> empty) then
            iReportID = m_ASelectedItems (iIndex)
            sReportName = m_ASelectedNames (iIndex)
            Exit For
         End If
      Next

		If m_sReportType = "<% =L_ReportDynamic_Text %>" Then
			' Open a new window which calls an asp page, passes the ReportID  
			' as a query string parameter and executes the dynamic report. 
						
			Set objNewWindow = Window.Open("<%= g_sStartURL %><%= g_HTTPServer %><% =g_sBizDeskRoot %>analysis/analysis_dynamic_reporting.asp?ReportID=" & iReportID, "Dynamic" & sRandom, "")
			' Window display options:
			'menubar				Show the default browser menus
			'scrollbars			Show vertical and horizontal scrollbars
			'resizeable			Window can or cannot be resized by user
			'status				Show the status bar at the bottom of the window 
			'toolbar				Show the default browser toolbars
			'location			Show the URL address text box
			'directories		Show directory buttons
			'fullscreen			Maximize new window or leave default size 
			'channelmode		Show channel controls
			'width				Width of the window (in pixels)
			'height				Height of the window (in pixels)
			'top					Top position of the window (in pixels)
			'left					Left position of the window (in pixels)
					
		Else		' Static report so get any report parameters

			' Create a ReportStatus entry
			RunOrExportXMLform("ReportID").value = iReportID
			RunOrExportXMLform("Export").value = 0
			RunOrExportXMLform("Add").value = 1
			RunOrExportXMLform.action = "analysis_report_instance.asp"
			set xmlDOMDoc = xmlPostFormViaXML(RunOrExportXMLform)

			If err <> 0 then
				sErrMsg = Err.description
				If err = 424 or err = 438 then sErrMsg = L_IISCommFailure_ErrorMessage	
				msgbox L_ReportExecError_ErrorMessage & sErrMsg, , L_ReportError_DialogTitle
				DeleteReportInstance iReportStatusID
				exit sub
			End If

			' Process any returned error node.
			set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
			if xmlErrorNodes.length <> 0 or xmlDOMDoc.documentElement is nothing then
				'report errors and set status text
				SetStatusText(sFormatString(L_ReportExecutionFailed_Text, array(sReportName)))
				ShowXMLNodeErrors(xmlErrorNodes)
				SetTaskButtons <% =ID_REPORTS %>
				exit sub
			end if

			' Get the responses
			Set dResponseXMLAsDict = dDictFromXML(xmlDOMDoc)
			iReportStatusID = dResponseXMLAsDict("ReportStatusID")
			iParams = dResponseXMLAsDict("Params")

			if iReportStatusID > 0 Then
				If iParams > 0 Then
					' Get report params from operator
					winargs = "ReportID=" & iReportID & "&ReportStatusID=" & iReportStatusID
					ReportParams = window.showModalDialog("dlg_report_params.asp?" & winargs, , "status:no;help:no")

					If ReportParams = -1 Or ReportParams = Empty then		' Cancel pressed
						DeleteReportInstance iReportStatusID
						Exit Sub
					End If
				End If

				RunOrExportXMLform("ReportStatusID").value = iReportStatusID
				RunOrExportXMLform("iReportType").value = m_iReportType
				RunOrExportXMLform.action = "analysis_run_static.asp"
				set xmlDOMDoc = xmlPostFormViaXML(RunOrExportXMLform)

				If err <> 0 then
					sErrMsg = Err.description
					If err = 424 or err = 438 then sErrMsg = L_IISCommFailure_ErrorMessage	
					msgbox L_ReportExecError_ErrorMessage & sErrMsg, , L_ReportError_DialogTitle
					DeleteReportInstance iReportStatusID
					exit sub
				End If

				' Process any returned error node.
				set xmlErrorNodes = xmlDOMDoc.selectNodes("//ERROR")
				if xmlErrorNodes.length <> 0 or xmlDOMDoc.documentElement is nothing then
					'report errors and set status text
					SetStatusText(sFormatString(L_ReportExecutionFailed_Text, array(sReportName)))
					ShowXMLNodeErrors(xmlErrorNodes)
				else
					SetStatusText(sFormatString(L_ExecutingStaticReport_Text, array(sReportName)))
				end if
			end if
			
		End If
		SetTaskButtons <% =ID_REPORTS %>
		     
	End Sub
   '------------------------------------------------------------
	Sub OnDelete (ByVal iEntityType)
		Dim sResponse
      Dim sText

		' Confirm any deletes
		if m_nItemsSelected = 1 Then
			sText = sFormatString(L_DeleteReport_Message,Array(NameOfSelectedItem))
		elseif m_nItemsSelected > 1 then
			sText = sFormatString(L_DeleteReports_Message,Array(m_nItemsSelected)) 
		elseif m_bAll then
			sText = L_AllDelete_Message
		else
			SetTaskButtons iEntityType
			Exit Sub
		end if
		sResponse = MsgBox(sText, vbYesNo, L_DeleteReport_DialogTitle)
		
		if sResponse = vbYes then
			if m_nItemsSelected >= 1 then
				ReDim Preserve m_ASelectedItems(m_nItemsSelected - 1)
				deleteform.elements.sel.value = Join(m_ASelectedItems,",")
			end if
			deleteform.elements.type.value = "delete"
			deleteform.submit()
		else
			SetTaskButtons iEntityType
		end if

	End Sub
   '------------------------------------------------------------
   Sub DisplaySelectedCount (ByVal iEntityType)
      ' Display the selected item count 
		If m_bAll Then
			setStatusText L_AllReportsSelected_Text
		Elseif m_nItemsSelected = 0 then
			setStatusText ""
		Elseif m_nItemsSelected = 1 then
			setStatusText L_ReportSelected_Text & NameOfSelectedItem
		Else
			setStatusText L_ReportsSelected_Text & m_nItemsSelected
		End If
   End Sub 
   '------------------------------------------------------------
	Function sRandom()
		Randomize
		sRandom = cStr(Rnd*1000000000)
		sRandom = Replace(sRandom, ",", "")
		sRandom = Replace(sRandom, ".", "")
	End Function
   '------------------------------------------------------------
'-->
</SCRIPT>
