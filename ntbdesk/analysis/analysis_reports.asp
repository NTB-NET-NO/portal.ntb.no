<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='./include/analysisStrings.asp' -->
<!--#INCLUDE FILE='./include/inc_common.asp' -->
<!--#INCLUDE FILE='./include/reports_util.asp' -->
<%
	dim thisPage
	dim g_sModuleName
	dim g_sKey
	dim g_sSubKey
	dim g_sStatustext
	dim dReports
	thisPage = "analysis_reports.asp"
	g_sModuleName = L_Reports_StaticText

 	'Initializes the ReportGroup
 	IntializeReportGroup

	'Implementation for saving states
	set dReports = Server.CreateObject("Commerce.Dictionary")
	if not IsEmpty(Session("dReports")) then
		set dReports = Session("dReports")
	else
		' Automatically display the first page of reports
		dReports.selfindby = "All"
		dReports.pagetype = "reports"
		dreports.column = "DisplayName" 
		dreports.direction = "asc" 
		dreports.page = 1
		set Session("dReports") = dReports
	end if
	
	g_sKey = "GUID"
	g_sSubKey = "DisplayName"

	'Implementation for deleting reports
	if Request.Form("type") = "delete" then
		Dim items, count, i, nResult
		items = Split(Request.Form("sel"),",")
		count = ubound(items) + 1
		nResult = deleteRecords(items,thisPage)
		if nResult = 0 then
			g_sStatusText = sFormatString(L_Delete_StatusBar,Array(count))
		else
			g_sStatusText = L_ErrorDelete_StatusBar
		end if
	end if
%>

<HTML>
<HEAD>
    <TITLE></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
	DIV.esContainer, .esEditgroup DIV.esContainer
	{
	    BACKGROUND-COLOR: transparent;
	}
</STYLE>

<!--#INCLUDE FILE='./include/inc_select.vbs' -->

<SCRIPT LANGUAGE='VBScript'>

   sub window_OnLoad()
		setSelect()
		document.all.btnfindby.disabled = 0
		SetTaskButtons <%= ID_REPORTS %>
	end sub
    
	sub changeSelect()
		setSelect()
		ShowFindBy()
	end sub
	'Called when the find by selection changes
	'Decides what are the fields to be displayed
	sub setSelect()
		On Error Resume Next		' In case form is not fully loaded yet
		rnrow.style.display = "none"
		rcrow.style.display = "none"
		rcbrow.style.display = "none"
		rtrow.style.display = "none"
		select case document.all.selfindby.value
			case "Name"
				setFindByHeight(150)
				rnrow.style.display = "block"
			case "Category"
				setFindByHeight(150)
				rcrow.style.display = "block"
			case "CreatedBy"
				setFindByHeight(150)
				rcbrow.style.display = "block"
			case "Type"
				setFindByHeight(150)
				rtrow.style.display = "block"
			case "Any"
				setFindByHeight(170)
				rnrow.style.display = "block"
				rcrow.style.display = "block"
				rcbrow.style.display = "block"
				rtrow.style.display = "block"
			case "All"
				setFindByHeight(150)
				document.all.btnfindby.disabled = 0
		end select
		On Error Goto 0
	end sub

	'Called when the Find Now button is clicked
	sub reloadListSheet()
		lsReports.page = 1
		lsReports.reload("findby")
		OnAllRowsUnselect <%= ID_REPORTS %>
	end sub
</SCRIPT>

</HEAD>
<BODY LANGUAGE='VBScript'>

<%
	if IsEmpty(Session("MSCSBDError")) Then
		if not IsEmpty(Session("dReports")) then
			dim g_rsQuery, nRecordCount
			dim xmlReports, xmlList, node
			
			g_nPage = dReports.page

			set g_rsQuery = rsGetRecords(dReports,PAGE_SIZE,nRecordCount)
			g_nItems = nRecordCount
				
			if not isEmpty(g_nItems) then
				set xmlReports = Server.Createobject("Microsoft.XMLDOM")
				xmlReports.loadXML xmlGetXMLFromRSEx(g_rsQuery, -1, -1, nRecordCount, null).xml
			end if
		end if
	end if

	InsertTaskBar g_sModuleName, g_sStatusText
%>

<!-- Start FindBy Area -->
<xml id="fbmeta">
<editsheet>
    <global title='no'/>
	<fields>
		<select id='selfindby' onchange='changeSelect'>
			<select id='selfindby'>
				<option value='Name'><%= L_NameR_ComboBox %></option>
				<option value='Category'><%= L_Category_ComboBox %></option>
				<option value='CreatedBy'><%= L_CreatedBy_ComboBox %></option>
				<option value='Type'><%= L_Type_ComboBox %></option>
				<option value='Any'><%= L_AnyInfoR_ComboBox %></option>
				<option value='All'><%= L_AllReportsR_ComboBox %></option>
			</select>
		</select>
		<text id='fbreportname' subtype='short' maxlen='128'>
		</text>
		<text id='fbreportcategory' subtype='short' maxlen='128'>
		</text>
		<text id='fbcreatedby' subtype='short' maxlen='128'>
		</text>
		<select id='fbreporttype'>
			<select id='fbreporttype'>
				<option value='dynamic'><%= L_Dynamic_ComboBox %></option> 
				<option value='static'><%= L_Static_ComboBox %></option>
			</select>
		</select>
	</fields>
    <template fields='selfindby fbreportname fbreportcategory fbcreatedby fbreporttype'><![CDATA[
		<TABLE WIDTH='100%' STYLE='TABLE-LAYOUT: fixed'>
			<TR>
				<TD WIDTH='100'>
					<SPAN ID='findbylabel'><%= L_FindBy_Text %></SPAN>
				</TD>
				<TD WIDTH='200'>
					<DIV ID='selfindby'
						ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
				<TD>
					<BUTTON CLASS='bdbutton' LANGUAGE='VBScript' ONCLICK='reloadListSheet' 
							ID='btnfindby' NAME='btnfindby' DISABLED><%= L_FindNow_Button %></BUTTON>
				</TD>
			</TR>
			<TR ID='rnrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='rnlabel'><%= L_ReportName_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='fbreportname'
						ONKEYUP='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='rcrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='rclabel'><%= L_ReportCategory_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='fbreportcategory'
						ONKEYUP='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='rcbrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='rcblabel'><%= L_ReportCreatedBy_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='fbcreatedby'
						ONKEYUP='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='rtrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='rtlabel'><%= L_ReportType_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='fbreporttype'
						ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
		</TABLE>		
    ]]></template>
</editsheet>
</xml>

<xml id='fbData'>
	<document>
		<record>
			<selfindby><%= dReports.selfindby %></selfindby>
			<fbreportname><%= dReports.fbreportname %></fbreportname>
			<fbreportcategory><%= dReports.fbreportcategory %></fbreportcategory>
			<fbcreatedby><%= dReports.fbcreatedby %></fbcreatedby>
			<fbreporttype><%= dReports.fbreporttype %></fbreporttype>
		</record>
	</document>
</xml>

<FORM ID='searchform' ACTION='reports_event.asp'>
	<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE='reports'>
	<DIV ID='bdfindbycontent' CLASS='findbycontent'>
		<div ID="esFind" class="editSheet"
			METAXML='fbMeta' DATAXML='fbData'></div>
	</DIV>
</FORM>
<!-- End FindBy Area -->

<FORM ID='neweventform' NAME='neweventform' ACTION='reports_event.asp'>
	<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE='reports'>
	<INPUT TYPE='hidden' ID='page' NAME='page' VALUE='<%= g_nPage %>'>
</FORM>

<FORM ID='selectform' NAME='selectform' ACTION='analysis_reports.asp' METHOD='POST'>
	<INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>'>
	<INPUT TYPE='hidden' ID='<%= g_sSubKey %>' NAME='<%= g_sSubKey %>'>
</FORM>
	
<FORM ID='deleteform' NAME='deleteform' ACTION='analysis_reports.asp' METHOD='POST' ONTASK='OnDelete <%= ID_REPORTS %>'>
	<INPUT TYPE='hidden' ID='type' NAME='type'>
	<INPUT TYPE='hidden' ID='sel' NAME='sel' VALUE=''>
	<INPUT TYPE='hidden' ID='selectall' NAME='selectall'>
</FORM>
	
<FORM ID='openform' NAME='openform' ACTION='report_edit.asp' METHOD='POST' ONTASK='OnOpen <%= ID_REPORTS %>'>
	<INPUT TYPE='hidden' ID='sel' NAME='sel' VALUE=''>
	<INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>'>
	<INPUT TYPE='hidden' ID='<%= g_sSubKey %>' NAME='<%= g_sSubKey %>'>
</FORM>
	
<FORM ID='runform' NAME='runform' ACTION='analysis_reports.asp' METHOD='POST' ONTASK='OnRun'>
	<INPUT TYPE='hidden' ID='type' NAME='type'>
	<INPUT TYPE='hidden' ID='sel' NAME='sel' VALUE=''>
	<INPUT TYPE='hidden' ID='selname' NAME='selname' VALUE=''>
	<INPUT TYPE='hidden' ID='ReportType' NAME='ReportType' VALUE=''>
	<INPUT TYPE='hidden' ID='ReportStatusID' NAME='ReportStatusID' VALUE=''>
</FORM>
	
<FORM ID='exportform' NAME='exportform' ACTION='marketing/xt_importlist.asp' METHOD='POST' ONTASK='OnExport'>
	<INPUT TYPE='hidden' ID='sel' NAME='sel' VALUE=''>
	<INPUT TYPE='hidden' ID='selname' NAME='selname' VALUE=''>
	<INPUT TYPE='hidden' ID='listdata' NAME='listdata' VALUE=''>	
</FORM>

<FORM ID='RunOrExportXMLform' NAME='RunOrExportXMLform' METHOD='POST'>
	<INPUT TYPE='hidden' ID='ReportID' NAME='ReportID'>
	<INPUT TYPE='hidden' ID='ReportStatusID' NAME='ReportStatusID'>
	<INPUT TYPE='hidden' ID='Export' NAME='Export'>
	<INPUT TYPE='hidden' ID='Add' NAME='Add'>
	<INPUT TYPE='hidden' ID='iReportType' NAME='iReportType'>
</FORM>

<DIV ID='bdcontentarea'>
   <xml id='reportsList'>
	<%	
		if not IsEmpty(Session("dReports")) and not isEmpty(g_nItems) then

			' Map report type codes to text for display
			set xmlList = xmlReports.selectNodes("//ReportType")
			for each node in xmlList
				If isNumeric(node.text) then
					Select Case CInt(node.text)
						Case Dynamic_OWC_SQL, Dynamic_OWC_MDX
							node.text = L_ReportDynamic_Text					
						Case Static_SQL, Static_MDX
							node.text = L_ReportStatic_Text				
						Case Else		
							node.text = L_ReportUnknown_Text
					End Select
				End If
			next
			Response.write xmlReports.xml
			set xmlReports = Nothing
			set xmlList = Nothing
			set g_rsQuery = Nothing
		else
			Response.Write("<document><record/></document>")
		end if
	%>
   </xml>

   <xml id='reportsMeta'>
      <listsheet>
         <global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_nItems %>'
            selectionbuttons='no' />
            <columns>
               <column id='GUID' hide='yes'></column>
               <column id='DisplayName'  width='40'><%= L_Rpt_Name_Col_Text %></column>
               <column id='Category'  width='20'><%= L_Rpt_Category_Col_Text %></column>
               <column id='CreatedBy'  width='20'><%= L_Rpt_CreatedBy_Col_Text %></column>
               <column id='ReportType'  width='20'><%= L_Rpt_Type_Col_Text %></column>
               <column id='DmExport'  hide='yes'></column>
               <column id='UpmExport'  hide='yes'></column>
               <column id='Protected'  hide='yes'></column>
               <column id='iReportType'  hide='yes'></column>
            </columns>
			 <operations>
				<newpage formid='neweventform'/>
				<sort    formid='neweventform'/>
				<findby  formid='searchform'/>
			</operations>
      </listsheet>
   </xml>

<div ID="filtertext" class="filtertext" style="MARGIN-TOP: 20px"><%= L_BDSSFilter_Text %></div>

   <DIV ID='lsReports' 
		CLASS='listSheet' STYLE="MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px; HEIGHT: 80%"
		DataXML='reportsList' 
		MetaXML='reportsMeta'
		LANGUAGE='VBScript'
		OnRowSelect='OnSelectRow <%= ID_REPORTS %>'
		OnRowUnselect='OnRowUnselect <%= ID_REPORTS %>'
		OnAllRowsSelect='OnAllRowsSelect <%= ID_REPORTS %>'
		OnAllRowsUnselect='OnAllRowsUnselect <%= ID_REPORTS %>'><%= L_LoadingList_Text%></DIV>

</DIV>
</BODY>
</HTML>
<% Cleanup %>

