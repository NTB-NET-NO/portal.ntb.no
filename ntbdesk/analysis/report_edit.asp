<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='./include/analysisStrings.asp' -->
<!--#INCLUDE FILE='./include/inc_common.asp' -->
<!--#INCLUDE FILE="./include/reports_util.asp" -->
<html>
<head>
<title></title>
<link REL="stylesheet" TYPE="text/css" HREF="../../widgets/bizdesk.css" ID="mainstylesheet">
<!--#INCLUDE FILE='./include/inc_view.vbs' -->
</head>
<body>
<%

Dim g_sKeyValue
Dim g_sSubKeyValue

g_sKeyValue = Request.Form("GUID")
g_sSubKeyValue = Request.form("DisplayName")

'Initializes the ReportGroup
IntializeReportGroup
 	
' G-L-O-B-A-L  E-R-R-O-R   H-A-N-D-L-I-N-G
' (for those errors not treated in place)
If (Err.Number <> 0) Then
	Call setError ("", L_ErrorsInPage_ErrorMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
	Err.Clear
End If

InsertEditTaskBar sFormatString(L_View_StatusBar,Array(g_sSubKeyValue)),""
%>

<xml id="ReportMeta">
  <editsheets>
       <editsheet>
  	    <global expanded="yes" title="no">
  	        <name localize="yes"><% =L_ReportProps2_Text %></name>
  		     <key localize="yes"><% =L_R2_Accelerator %></key>
  	    </global>
  	    <fields>
             <text id="DisplayName" required="no" readonly="yes" maxlen="128">
  	            <name localize="yes"><% =L_Name2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_Name2_ToolTip %></tooltip>
  	            <charmask localize="yes"></charmask>
  	            <error localize="yes"><% =L_Name2_ErrorMessage %></error>
  	        </text>
  	        <text id="Description" required="no" readonly="yes" maxlen="2000" subtype="long">
  	            <name localize="yes"><% =L_Description2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_Description2_ToolTip %></tooltip>
  	            <charmask localize="yes"></charmask>
  	            <error localize="yes"><% =L_Description2_ErrorMessage %></error>
  	        </text>
  	        <text id="ReportType" required="no" readonly="yes" maxlen="128">
  	            <name localize="yes"><% =L_Type2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_Type2_ToolTip %></tooltip>
  	            <charmask localize="yes"></charmask>
  	            <error localize="yes"><% =L_Type2_ErrorMessage %></error>
  	        </text>
  	        <date id="CreatedTime" required="no" readonly="yes" maxlen="32" firstday='<%= g_sMSCSWeekStartDay %>'>
  	            <name localize="yes"><% =L_DateCreated2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_DateCreated2_ToolTip %></tooltip>
  	            <format><% =g_sMSCSDateFormat %></format>
  	            <error localize="yes"><% =L_DateCreated2_ErrorMessage %></error>
  	        </date>
             <text id="CreatedBy" required="no" readonly="yes" maxlen="128">
  	            <name localize="yes"><% =L_CreatedBy2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_CreatedBy2_ToolTip %></tooltip>
  	            <charmask localize="yes"></charmask>
  	            <error localize="yes"><% =L_CreatedBy2_ErrorMessage %></error>
  	        </text>
  	        <date id="ModifiedTime" required="no" readonly="yes" maxlen="32" firstday='<%= g_sMSCSWeekStartDay %>'>
  	            <name localize="yes"><% =L_DateModified2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_DateModified2_ToolTip %></tooltip>
  	            <format><% =g_sMSCSDateFormat %></format>
  	            <error localize="yes"><% =L_DateModified2_ErrorMessage %></error>
  	        </date>
  	        <text id="LastModifiedBy" required="no" readonly="yes" maxlen="32">
  	            <name localize="yes"><% =L_ModifiedBy2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_ModifiedBy2_ToolTip %></tooltip>
  	            <charmask localize="yes"></charmask>
  	            <error localize="yes"><% =L_ModifiedBy2_ErrorMessage %></error>
  	        </text>
  	        <text id="Query" required="no" readonly="yes" maxlen="2000" subtype="long">
  	            <name localize="yes"><% =L_Query2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_Query2_ToolTip %></tooltip>
  	            <charmask localize="no">^[]*</charmask>
  	            <error localize="yes"><% =L_Query2_ErrorMessage %></error>
  	        </text>
             <text id="DmExport" required="no" readonly="yes" maxlen="32">
  	            <name localize="yes"><% =L_DM2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_DM2_ToolTip %></tooltip>
  	            <charmask localize="yes"></charmask>
  	            <error localize="yes"><% =L_DM2_ErrorMessage %></error>
  	        </text>
             <text id="UpmExport" required="no" readonly="yes" maxlen="32">
  	            <name localize="yes"><% =L_UPM2_EditBox %></name>
  	            <tooltip localize="yes"><% =L_UPM2_ToolTip %></tooltip>
  	            <charmask localize="yes"></charmask>
  	            <error localize="yes"><% =L_UPM2_ErrorMessage %></error>
  	        </text>
  	  	</fields>
      </editsheet>
   </editsheets>
</xml>

<xml id="ReportGroup">
<%
  	dim g_rsQuery
  	dim xmlReport,xmlList,node

  	set g_rsQuery = rsGetDetails(g_sKeyValue)

  	If not g_rsQuery.eof then

  		set xmlReport = Server.Createobject("Microsoft.XMLDOM")
  		xmlReport.loadXML xmlGetXMLFromRSEx(g_rsQuery,-1,-1,-1,null).xml
  		set xmlList = xmlReport.selectNodes("//ReportType")

  		for each node in xmlList
  			If isNumeric(node.text) then
  				Select Case CInt(node.text)
  					Case Dynamic_OWC_SQL
  						node.text = L_DynamicSQL_Text					
  					Case Dynamic_OWC_MDX
  						node.text = L_DynamicOLAP_Text					
  					Case Static_SQL
  						node.text = L_StaticSQL_Text					
  					Case Static_MDX
  						node.text = L_StaticOLAP_Text					
  					Case Else						
  						node.text = L_ReportUnknown_Text
  				End Select
  			end if
  		next

  		set xmlList = xmlReport.selectNodes("//DmExport")
  		for each node in xmlList
  			Select Case node.text
  				Case 1
  					node.text = L_Yes_Text					
  				Case else	' 0
  					node.text = L_No_Text					
  			End Select
  		next

  		set xmlList = xmlReport.selectNodes("//UpmExport")
  		for each node in xmlList
  			Select Case node.text
  				Case 1
  					node.text = L_Yes_Text					
  				Case else	' 0
  					node.text = L_No_Text
  			End Select
  		next

  		Response.write xmlReport.xml
  		Set xmlReport = Nothing
  		Set xmlList = Nothing
  	Else
  		' Report could not be found				
  		Response.Redirect "analysis_reports.asp"
  	End If
			
  	Set g_rsQuery = Nothing
			
%>
</xml>

<div ID="editSheetContainer" CLASS="editPageContainer">

	<form ID="saveform" NAME="saveform" METHOD="POST" ACTION="Report_Edit.asp" ONTASK="OnSave <%= ID_REPORTS %>">
	   <input TYPE="hidden" ID="type" VALUE>

    <div ID="esReport" CLASS="editSheet" MetaXML="ReportMeta" DataXML="ReportGroup"><% =L_Loading2_Messsage %></div>
	</form>
</div>


<form ID="selectform" NAME="selectform" METHOD="POST" ACTION="analysis_reports.asp" ONTASK="OnClose">
</form>

</body>
</html>
<script LANGUAGE="VBSCRIPT">
	<%
		dim dBDError, slBDError
		if not isEmpty(Session("MSCSBDError")) then
			set slBDError = Session("MSCSBDError")
			for each dBDError in slBDError
	%>
	showErrorDlg "<%= dBDError("errortitle") %>", "<%= dBDError("friendlyerror") %>", "<%= dBDError("errordetails") %>", <%= dBDError("errortype") %>
	<%		next
			Session("MSCSBDError") = empty
		end if
	%>
</script>
<% Cleanup %>
