<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->
<!--#INCLUDE FILE='./include/inc_common.asp' -->
<!--#INCLUDE FILE='include/analysisStrings.asp' -->

<HTML ID='dlg_report_params'>
<HEAD>

<TITLE><% =L_RunReport_HTMLTitle %></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
HTML
(
	Height: 500px;
	Width: 440px;
)
   .txtwidth {width: 350px } 
BODY
{
	PADDING: 15px;
	MARGIN: 0px;
}
</STYLE>
<%
	Dim iReportID, iReportStatusID, sReportName, iReportType
	Dim rs, sQuery, sReportParams
	Dim iParamID, sParamID
	Dim bExit, Start_Date, End_Date
	Dim sXMLParamMeta, sXMLParamData, sXMLText
	Dim iDefaultTopCount, sValues, sTopValues
	Dim sDateOrdinals, sOrdinal, sAllOrdinals, sFieldOrdinals, sTag, sDescription
	Dim iParamType, iDataType, iOpnd1, iOpnd2, sVal1, sVal2
	Dim sFieldName, bAllowNull, bAutomatic, sReportQuery

	iDefaultTopCount = 100

	' Text constants used for XML creation
	Const L_ReportProperties_HTMLText = "Report properties"
	Const L_ParamTag_HTMLText = "Parameter tag:"
	Const L_ParamField_HTMLText = "Field name:"
	Const L_ParamValue_HTMLText = "Parameter value:"
	Const L_ToColon_HTMLText = "To:"
	Const L_SelectColon_HTMLText = "Select:"
	Const L_DateColon_HTMLText = "Date:"
	Const L_OperandColon_HTMLText = "Operand:"
	Const L_ReportNameColon_HTMLText = "Report name:"
	Const L_QueryColon_HTMLText = "Query:"

	Const L_Today_ComboBox = "Today"
	Const L_ThisWeek_ComboBox = "This week"
	Const L_ThisMonth_ComboBox = "This month"
	Const L_ThisQuarter_ComboBox = "This quarter"
	Const L_ThisYear_ComboBox = "This year"
	Const L_Yesterday_ComboBox = "Yesterday"
	Const L_LastWeek_ComboBox = "Last week"
	Const L_LastMonth_ComboBox = "Last month"
	Const L_LastQuarter_ComboBox = "Last quarter"
	Const L_LastYear_ComboBox = "Last year"
	
	Const L_All_ComboBox = "All"
	Const L_Top_ComboBox = "Top"
	Const L_Bottom_ComboBox = "Bottom"
	Const L_Distinct_ComboBox = "Distinct"
	Const L_None_ComboBox = "None"
	Const L_Equals_ComboBox = "Equals"
	Const L_NotEquals_ComboBox = "Not equal to"
	Const L_IsNull_ComboBox = "Is Null"
	Const L_IsNotNull_ComboBox = "Is not Null"
	Const L_LessThan_ComboBox = "Less than"
	Const L_LessThanOrEquals_ComboBox = "Less than or equals"
	Const L_GreaterThan_ComboBox = "Greater than"
	Const L_GreaterThanOrEquals_ComboBox = "Greater than or equals"
	Const L_Like_ComboBox = "Like"
	Const L_NotLike_ComboBox = "Not like"

	iReportID = Request("ReportID")
	iReportStatusID = Request("ReportStatusID")

	Start_Date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)
	End_Date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)

	' Get the report query	
	sQuery = "Select DisplayName, ReportType, Query from Report Where ReportID = " & iReportID

	Set g_oConn = oGetADOConnection(g_MSCSDataWarehouseSQLConnStr)
	Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
	g_oCmd.CommandText = sQuery
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC

	If not (rs.BOF and rs.EOF) then
		sReportName = rs.Fields("DisplayName").value
		iReportType = rs.Fields("ReportType").value
		sReportQuery = rs.Fields("Query").value
		sReportQuery = replace(replace(replace(sReportQuery, "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
	End If
	rs.Close
	
	' Get the list of parameters	
	sQuery = "Select * from ReportParam Where ReportID = " & iReportID & " Order by Ordinal ASC"
	g_oCmd.CommandText = sQuery
	rs.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC

	If (rs.BOF and rs.EOF) then
		bExit = 1
	Else
		bExit = 0
	End If
	
	'	CREATE TABLE [dbo].[ReportParam] (
	'		[ReportParamID] [int] identity ,		
	'		[ReportID] [int] NOT NULL ,
	'		[ParamName] [nvarchar] (128) NOT NULL ,
	'		[ParamDescription] [nvarchar] (128) NOT NULL ,
	'		[Ordinal] [tinyint] NULL ,	
	'		[ParamType] [tinyint] NOT NULL Default 0 ,   			 	
	'		[DataType] [tinyint] NULL ,	
	'		[Opnd1] [tinyint] NULL ,
	'		[Val1] [nvarchar] (128) NULL ,
	'		[Opnd2] [tinyint] NULL ,
	'		[Val2] [nvarchar] (128) NULL ,
	'		[FieldName] [nvarchar] (128) NULL ,
	'		[AllowNull] [bit] NOT NULL Default 0 ,		/* 0 = False, 1 = True */
	'		[Automatic] [bit] NOT NULL Default 0		/* 0 = False, 1 = True (does not display in UI) */
		
	'	CREATE TABLE [dbo].[ReportInstanceParam] (
	'		[ReportParamID] [int] ,
	'		[ReportStatusID] [int] NOT NULL ,
	'		[ParamName] [nvarchar] (128) NULL ,
	'		[Ordinal] [tinyint] NULL ,
	'		[ParamType] [tinyint] NULL ,   				
	'		[DataType] [nvarchar] (128) NULL ,
	'		[Opnd1] [tinyint] NULL ,
	'		[Val1] [nvarchar] (128) NULL ,
	'		[Opnd2] [tinyint] NULL ,
	'		[Val2] [nvarchar] (128) NULL ,
	'		[FieldName] [nvarchar] (128) NULL ,
	'		[AllowNull] [bit] NOT NULL Default 0 ,		/* 0 = False, 1 = True */
	'		[Automatic] [bit] NOT NULL Default 0		/* 0 = False, 1 = True (does not display in UI) */
			
	' Dynamically build the paramater UI code (Old Code)
	sReportParams = "<legend><label>" & L_ReportParam_EditBox & "</label></legend><br>"
	iParamID = 0	
	
	' Dynamically build the XML islands for the editsheets
	sXMLParamMeta = "<editsheets>" & _
							  "<editsheet>" & _
							    "<global><name>" & L_ReportProperties_HTMLText & "</name><key></key></global>" & _
							    "<fields>" & _
							        "<text id=""DisplayName"" required=""no"" readonly=""yes"" maxlen=""128"" subtype=""long"">" & _
							            "<name>" & L_ReportNameColon_HTMLText & "</name><charmask localize=""no"">^[]*</charmask><error></error>" & _
							        "</text>" & _
							        "<text id=""Query"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""2000"" subtype=""long"">" & _
							            "<name>" & L_QueryColon_HTMLText & "</name><charmask localize=""no"">^[]*</charmask><error></error>" & _
							        "</text>" & _
							        "<numeric id=""ReportID"" required=""no"" hide=""yes"" readonly=""yes"" subtype=""integer"">" & _
							            "<name>ReportID:</name>" & _
							        "</numeric>" & _
							        "<numeric id=""ReportStatusID"" required=""no"" hide=""yes"" readonly=""yes"" subtype=""integer"">" & _
							            "<name>ReportStatusID:</name>" & _
							        "</numeric>" & _
							        "<numeric id=""NumParams"" required=""no"" hide=""yes"" readonly=""yes"" subtype=""integer"">" & _
							            "<name>Number of params:</name>" & _
							        "</numeric>" & _
							        "<numeric id=""ReportType"" required=""no"" hide=""yes"" readonly=""yes"" subtype=""integer"">" & _
							            "<name>ReportType:</name>" & _
							        "</numeric>" & _
								  "</fields>" & _
							  "</editsheet>"
									
	While not rs.EOF
		iParamID = iParamID + 1
		
			' Param Data Types 
		'	Const DataType_boolean					= 0
		'	Const DataType_date						= 1
		'	Const DataType_integer					= 2
		'	Const DataType_float						= 3
		'	Const DataType_currency					= 4
		'	Const DataType_text						= 5
		'	Const DataType_time						= 6
		
			' Param Types 
		'	Const ParamType_SingleValue			= 0
		'	Const ParamType_Expression				= 1
		'	Const ParamType_SelectOrder			= 2
		'	Const ParamType_DateRange				= 3
		'	Const ParamType_SiteName				= 4

			' Param Type Expression Operands
		'	Const ExpOpnd_None						= 0
		'	Const ExpOpnd_Equals						= 1
		'	Const ExpOpnd_NotEquals					= 2
		'	Const ExpOpnd_IsNull						= 3
		'	Const ExpOpnd_IsNotNull					= 4
			' Numeric data types only
		'	Const ExpOpnd_LessThan					= 5
		'	Const ExpOpnd_LessThanOrEquals		= 6
		'	Const ExpOpnd_GreaterThan				= 7
		'	Const ExpOpnd_GreaterThanOrEquals	= 8
			' Char data type only
		'	Const ExpOpnd_Like						= 9
		'	Const ExpOpnd_NotLike					= 10

			' Param Type Site Name Operands
		'	Const SiteNameOpnd_All					= 0
		'	Const SiteNameOpnd_Equals				= 1
		'	Const SiteNameOpnd_NotEquals			= 2
		'	Const SiteNameOpnd_Like					= 3
		'	Const SiteNameOpnd_NotLike				= 4

			' Param Type Select Order Operands
		'	Const SelectOrderOpnd_All				= 0
		'	Const SelectOrderOpnd_Top				= 1
		'	Const SelectOrderOpnd_Distinct		= 2

			' Param Type Date Range Operands
		'	Const DateRangeOpnd_All					= 0
		'	Const DateRangeOpnd_From				= 1
		'	Const DateRangeOpnd_OnOrBefore		= 2
		'	Const DateRangeOpnd_Today				= 3
		'	Const DateRangeOpnd_ThisWeek			= 4
		'	Const DateRangeOpnd_ThisMonth			= 5
		'	Const DateRangeOpnd_ThisQuarter		= 6
		'	Const DateRangeOpnd_ThisYear			= 7
		'	Const DateRangeOpnd_Yesterday			= 8
		'	Const DateRangeOpnd_LastWeek			= 9
		'	Const DateRangeOpnd_LastMonth			= 10
		'	Const DateRangeOpnd_LastQuarter		= 11
		'	Const DateRangeOpnd_LastYear			= 12

		sOrdinal = CStr(rs.Fields("Ordinal").value)
		sTag = rs.Fields("ParamName").value
		sDescription  = rs.Fields("ParamDescription").value
		iParamType = rs.Fields("ParamType").value
		iDataType = rs.Fields("DataType").value
		iOpnd1 = rs.Fields("Opnd1").value
		iOpnd2 = rs.Fields("Opnd2").value
		sVal1 = rs.Fields("Val1").value
		sVal2 = rs.Fields("Val2").value
		sFieldName = rs.Fields("FieldName").value
		bAllowNull = rs.Fields("AllowNull").value
		bAutomatic = rs.Fields("Automatic").value
		
		sAllOrdinals = sAllOrdinals & sOrdinal & "|"

		If iReportType = Static_SQL Then

			' Construct XML islands for edit sheets and edit fields
			Select Case rs.Fields("ParamType").Value
				Case ParamType_SelectOrder

					' Set default values for dates if none provided
					If sVal1 = "" Or IsNull(sVal1) Then sVal1 = iDefaultTopCount
					If IsNull(iOpnd1) Then iOpnd1 = SelectOrderOpnd_All
					If iOpnd1 < SelectOrderOpnd_All Or iOpnd1 > SelectOrderOpnd_Top Then iOpnd1 = SelectOrderOpnd_All

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>" & _
										 "<select id=""P" & sOrdinal & "Opnd1"" onchange=""SelectOperandChange(" & sOrdinal & ")"">" & _
										 "<prompt>" & L_SelectOne_Text & "</prompt><name>" & L_SelectColon_HTMLText & "</name>" & _
										 "<select id=""P" & sOrdinal & "Opnd1"">" & _
										 "<option value=""" & SelectOrderOpnd_All & """>" & L_All_ComboBox & "</option>" & _
										 "<option value=""" & SelectOrderOpnd_Top & """>" & L_Top_ComboBox & "</option>" & _
										 "<option value=""" & SelectOrderOpnd_Distinct & """>" & L_Distinct_ComboBox & "</option>" & _
										 "</select></select><numeric id=""P" & sOrdinal & "Val1"" required=""yes"" "
										 
					' Hide Val1 field if select option = "All"
					If iOpnd1 = SelectOrderOpnd_All Or iOpnd1 = SelectOrderOpnd_Distinct Then sXMLParamMeta = sXMLParamMeta & "hide=""yes"" "
										 
					sXMLParamMeta = sXMLParamMeta & "readonly=""no"" onchange=""CheckTopCount(" & sOrdinal & ")""><name>" & L_ParamValue_HTMLText & "</name></numeric></fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Opnd1>" & iOpnd1 & "</P" & sOrdinal & "Opnd1>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>"

					sTopValues = sTopValues & "P" & sOrdinal & "Val1|"
				Case ParamType_DateRange

					' Set default values for dates if none provided
					If sVal1 = "" Or IsNull(sVal1) Then sVal1 = Start_Date
					If sVal2 = "" Or IsNull(sVal2) Then sVal2 = End_Date
					If IsNull(iOpnd1) Then iOpnd1 = DateRangeOpnd_All
					If iOpnd1 < DateRangeOpnd_All Or iOpnd1 > DateRangeOpnd_LastYear Then iOpnd1 = DateRangeOpnd_All

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>" & _
										 "<text id=""P" & sOrdinal & "Field"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamField_HTMLText & "</name></text>" & _
										 "<select id=""P" & sOrdinal & "Opnd1"" onchange=""DateOperandChange(" & sOrdinal & ")""><prompt>" & L_SelectOne_Text & _
										 "</prompt><name>" & L_DateColon_HTMLText & "</name><select id=""P" & sOrdinal & "Opnd1"">" & _
										 "<option value=""" & DateRangeOpnd_All & """>" & L_All_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_From & """>" & L_From_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_OnOrBefore & """>" & L_OnOrBefore_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_Today & """>" & L_Today_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_ThisWeek & """>" & L_ThisWeek_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_ThisMonth & """>" & L_ThisMonth_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_ThisQuarter & """>" & L_ThisQuarter_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_ThisYear & """>" & L_ThisYear_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_Yesterday & """>" & L_Yesterday_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_LastWeek & """>" & L_LastWeek_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_LastMonth & """>" & L_LastMonth_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_LastQuarter & """>" & L_LastQuarter_ComboBox & "</option>" & _
										 "<option value=""" & DateRangeOpnd_LastYear & """>" & L_LastYear_ComboBox & "</option>" & _
										 "</select></select><date id=""P" & sOrdinal & "Val1"" required=""yes"" firstday='" & g_sMSCSWeekStartDay & "' "

					' Hide Start date if select option <> "From" and select option <> "On or Before"
					If iOpnd1 <> DateRangeOpnd_From And iOpnd1 <> DateRangeOpnd_OnOrBefore Then sXMLParamMeta = sXMLParamMeta & "hide=""yes"" "
										 
					sXMLParamMeta = sXMLParamMeta & "readonly=""no"" onchange=""CheckDateRange(" & sOrdinal & ")""><format>" & g_sMSCSDateFormat & "</format></date>" & _
										 "<date id=""P" & sOrdinal & "Val2"" required=""yes"" firstday='" & g_sMSCSWeekStartDay & "' "
										 
					' Hide End date if select option <> "From"
					If iOpnd1 <> DateRangeOpnd_From Then sXMLParamMeta = sXMLParamMeta & "hide=""yes"" "
										 
					sXMLParamMeta = sXMLParamMeta & "readonly=""no"" onchange=""CheckDateRange(" & sOrdinal & ")""><format>" & g_sMSCSDateFormat & "</format><name>" & _
										 L_ToColon_HTMLText & "</name></date></fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Field>" & sFieldName & "</P" & sOrdinal & "Field>" & _
										"<P" & sOrdinal & "Opnd1>" & iOpnd1 & "</P" & sOrdinal & "Opnd1>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>" & _
										"<P" & sOrdinal & "Val2>" & sVal2 & "</P" & sOrdinal & "Val2>"

					sValues = sValues & "P" & sOrdinal & "Val1|"
					sValues = sValues & "P" & sOrdinal & "Val2|"
					
					sDateOrdinals = sDateOrdinals & sOrdinal & "|"
					sFieldOrdinals = sFieldOrdinals & sOrdinal & "|"
					
				Case ParamType_Expression
					
					If IsNull(iOpnd1) Then iOpnd1 = ExpOpnd_None
					If IsNull(iOpnd2) Then iOpnd2 = ExpOpnd_None

					' Set default values for dates if none provided
					Select Case iDataType
						Case DataType_boolean
							' Not supported
						Case DataType_date
							' Not supported
						Case DataType_integer, DataType_float, DataType_currency
							If sVal1 = "" Or IsNull(sVal1) Then sVal1 = 0
							If sVal2 = "" Or IsNull(sVal2) Then sVal2 = 0
							If iOpnd1 < ExpOpnd_None Or iOpnd1 > ExpOpnd_GreaterThanOrEquals Then iOpnd1 = ExpOpnd_None
							If iOpnd2 < ExpOpnd_None Or iOpnd2 > ExpOpnd_GreaterThanOrEquals Then iOpnd2 = ExpOpnd_None
						Case DataType_text
							If IsNull(sVal1) Then sVal1 = ""
							If IsNull(sVal2) Then sVal2 = ""
							If iOpnd1 < ExpOpnd_None Or iOpnd1 > ExpOpnd_NotLike Then iOpnd1 = ExpOpnd_None
							If iOpnd1 >= ExpOpnd_LessThan And iOpnd1 =< ExpOpnd_GreaterThanOrEquals Then iOpnd1 = ExpOpnd_None
							If iOpnd2 < ExpOpnd_None Or iOpnd2 > ExpOpnd_NotLike Then iOpnd2 = ExpOpnd_None
							If iOpnd2 >= ExpOpnd_LessThan And iOpnd2 =< ExpOpnd_GreaterThanOrEquals Then iOpnd2 = ExpOpnd_None
						Case DataType_time
							' Not supported
					End Select				

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>" & _
										 "<text id=""P" & sOrdinal & "Field"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamField_HTMLText & "</name></text>" & _
										 "<select id=""P" & sOrdinal & "Opnd1"" onchange=""ExpOperandChange(" & sOrdinal & ", 1, " & iDataType & ")""><prompt>" & L_SelectOne_Text & _
										 "</prompt><name>" & L_OperandColon_HTMLText & "</name><select id=""P" & sOrdinal & "Opnd1"">" & _
										 "<option value=""" & ExpOpnd_None & """>" & L_None_ComboBox & "</option>" & _
										 "<option value=""" & ExpOpnd_Equals & """>" & L_Equals_ComboBox & "</option>" & _
										 "<option value=""" & ExpOpnd_NotEquals & """>" & L_NotEquals_ComboBox & "</option>"
										 
					If ((iDataType <> DataType_boolean) And (iDataType <> DataType_text)) Then
						sXMLParamMeta = sXMLParamMeta & "<option value=""" & ExpOpnd_LessThan & """>" & L_LessThan_ComboBox & "</option>" & _
																	"<option value=""" & ExpOpnd_LessThanOrEquals & """>" & L_LessThanOrEquals_ComboBox & "</option>" & _
																	"<option value=""" & ExpOpnd_GreaterThan & """>" & L_GreaterThan_ComboBox & "</option>" & _
																	"<option value=""" & ExpOpnd_GreaterThanOrEquals & """>" & L_GreaterThanOrEquals_ComboBox & "</option>"
					End If

					If iDataType = DataType_text Then
						sXMLParamMeta = sXMLParamMeta & "<option value=""" & ExpOpnd_Like & """>" & L_Like_ComboBox & "</option>" & _
																  "<option value=""" & ExpOpnd_NotLike & """>" & L_NotLike_ComboBox & "</option>"
					End If
										 
					sXMLParamMeta = sXMLParamMeta & "<option value=""" & ExpOpnd_IsNull & """>" & L_IsNull_ComboBox & "</option>" & _
															  "<option value=""" & ExpOpnd_IsNotNull & """>" & L_IsNotNull_ComboBox & "</option>" & _
															  "</select></select>"
					
					' Hide Val1 if select1 option = "None", "Is Null", "Is not Null"
					If iOpnd1 = ExpOpnd_None Or iOpnd1 = ExpOpnd_IsNull Or iOpnd1 = ExpOpnd_IsNotNull Then 
						sXMLText = " id=""P" & sOrdinal & "Val1"" required=""yes"" hide=""yes"" readonly=""no"" onchange=""EnableOK()"" "
					Else
						sXMLText = " id=""P" & sOrdinal & "Val1"" required=""yes"" readonly=""no"" onchange=""EnableOK()"""
					End If

					Select Case iDataType
						Case DataType_boolean
							' Not supported
						Case DataType_date
							' Not supported
						Case DataType_integer
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & "><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_float
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""float""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_currency
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""currency""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_text
							sXMLParamMeta = sXMLParamMeta & "<text" & sXMLText & " maxlen=""128""><name>" & L_ParamValue_HTMLText & "</name></text>"
						Case DataType_time
							' Not supported
					End Select				
					
					sXMLParamMeta = sXMLParamMeta & "<select id=""P" & sOrdinal & "Opnd2"" "
										 
					' Hide Opnd2 if if select1 option = "None", "Equals", "Is Null", "Is not Null"
					If iOpnd1 = ExpOpnd_None Or iOpnd1 = ExpOpnd_Equals Or iOpnd1 = ExpOpnd_IsNull Or iOpnd1 = ExpOpnd_IsNotNull Then sXMLParamMeta = sXMLParamMeta & "hide=""yes"" "
										 
					sXMLParamMeta = sXMLParamMeta & "onchange=""ExpOperandChange(" & sOrdinal & ", 2, " & iDataType & ")""><prompt>" & L_SelectOne_Text & _
										 "</prompt><name>" & L_OperandColon_HTMLText & "</name><select id=""P" & sOrdinal & "Opnd2"">" & _
										 "<option value=""" & ExpOpnd_None & """>" & L_None_ComboBox & "</option>" & _
										 "<option value=""" & ExpOpnd_NotEquals & """>" & L_NotEquals_ComboBox & "</option>"
										 
					If ((iDataType <> DataType_boolean) And (iDataType <> DataType_text)) Then
						sXMLParamMeta = sXMLParamMeta & "<option value=""" & ExpOpnd_LessThan & """>" & L_LessThan_ComboBox & "</option>" & _
																	"<option value=""" & ExpOpnd_LessThanOrEquals & """>" & L_LessThanOrEquals_ComboBox & "</option>" & _
																	"<option value=""" & ExpOpnd_GreaterThan & """>" & L_GreaterThan_ComboBox & "</option>" & _
																	"<option value=""" & ExpOpnd_GreaterThanOrEquals & """>" & L_GreaterThanOrEquals_ComboBox & "</option>"
					End If

					If iDataType = DataType_text Then
						sXMLParamMeta = sXMLParamMeta & "<option value=""" & ExpOpnd_Like & """>" & L_Like_ComboBox & "</option>" & _
																  "<option value=""" & ExpOpnd_NotLike & """>" & L_NotLike_ComboBox & "</option>"
					End If
										 
					sXMLParamMeta = sXMLParamMeta & "</select></select>"
					
					' Hide Val2 if select1 option = "None", "Equals", "Is Null", "Is not Null" Or select2 option = "None"
					If iOpnd1 = ExpOpnd_None Or iOpnd1 = ExpOpnd_Equals Or iOpnd1 = ExpOpnd_IsNull Or iOpnd1 = ExpOpnd_IsNotNull Or iOpnd2 = ExpOpnd_None Then
						sXMLText = " id=""P" & sOrdinal & "Val2"" required=""yes"" hide=""yes"" readonly=""no"" onchange=""EnableOK()"" "
					Else
						sXMLText = " id=""P" & sOrdinal & "Val2"" required=""yes"" readonly=""no"" onchange=""EnableOK()"""
					End If

					Select Case iDataType
						Case DataType_boolean
							' Not supported
						Case DataType_date
							' Not supported
						Case DataType_integer
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & "><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_float
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""float""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_currency
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""currency""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_text
							sXMLParamMeta = sXMLParamMeta & "<text" & sXMLText & " maxlen=""128""><name>" & L_ParamValue_HTMLText & "</name></text>"
						Case DataType_time
							' Not supported
					End Select				
					sXMLParamMeta = sXMLParamMeta & "</fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Field>" & sFieldName & "</P" & sOrdinal & "Field>" & _
										"<P" & sOrdinal & "Opnd1>" & iOpnd1 & "</P" & sOrdinal & "Opnd1>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>" & _
										"<P" & sOrdinal & "Opnd2>" & iOpnd2 & "</P" & sOrdinal & "Opnd2>" & _
										"<P" & sOrdinal & "Val2>" & sVal2 & "</P" & sOrdinal & "Val2>"

					sValues = sValues & "P" & sOrdinal & "Val1|"
					sValues = sValues & "P" & sOrdinal & "Val2|"
					
					sFieldOrdinals = sFieldOrdinals & sOrdinal & "|"
					
				Case ParamType_SiteName

					If IsNull(iOpnd1) Then iOpnd1 = ExpOpnd_None

					If IsNull(sVal1) Then sVal1 = sSiteName
					If iOpnd1 < SiteNameOpnd_All Or iOpnd1 > SiteNameOpnd_NotLike Then iOpnd1 = SiteNameOpnd_All

					' Always text
					iDataType = DataType_text

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>" & _
										 "<text id=""P" & sOrdinal & "Field"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamField_HTMLText & "</name></text>" & _
										 "<select id=""P" & sOrdinal & "Opnd1"" onchange=""SiteOperandChange(" & sOrdinal & ", 1, " & iDataType & ")""><prompt>" & L_SelectOne_Text & _
										 "</prompt><name>" & L_OperandColon_HTMLText & "</name><select id=""P" & sOrdinal & "Opnd1"">" & _
										 "<option value=""" & SiteNameOpnd_All & """>" & L_All_ComboBox & "</option>" & _
										 "<option value=""" & SiteNameOpnd_Equals & """>" & L_Equals_ComboBox & "</option>" & _
										 "<option value=""" & SiteNameOpnd_NotEquals & """>" & L_NotEquals_ComboBox & "</option>" & _
										 "<option value=""" & SiteNameOpnd_Like & """>" & L_Like_ComboBox & "</option>" & _
										 "<option value=""" & SiteNameOpnd_NotLike & """>" & L_NotLike_ComboBox & "</option></select></select>"
										 
					' Hide Val1 if Opnd1 option = "All"
					sXMLText = ""
					If iOpnd1 = SiteNameOpnd_All Then sXMLText = " hide=""yes"""

					sXMLParamMeta = sXMLParamMeta & "<text id=""P" & sOrdinal & "Val1"" required=""yes"" readonly=""no""" & sXMLText & " maxlen=""128"" onchange=""EnableOK()""><name>" & _
							L_ParamValue_HTMLText & "</name></text>"

					sXMLParamMeta = sXMLParamMeta & "</fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Field>" & sFieldName & "</P" & sOrdinal & "Field>" & _
										"<P" & sOrdinal & "Opnd1>" & iOpnd1 & "</P" & sOrdinal & "Opnd1>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>"

					sValues = sValues & "P" & sOrdinal & "Val1|"
					sFieldOrdinals = sFieldOrdinals & sOrdinal & "|"
				
				Case ParamType_SingleValue

					' Set default values for dates if none provided
					If IsNull(sVal1) Then sVal1 = ""

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>"

					sXMLText = " id=""P" & sOrdinal & "Val1"" required=""no"" onchange=""EnableOK()"""
					Select Case iDataType
						Case DataType_boolean
							' Not supported
						Case DataType_date
							' Not supported
						Case DataType_integer
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & "><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_float
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""float""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_currency
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""currency""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_text
							sXMLParamMeta = sXMLParamMeta & "<text" & sXMLText & " maxlen=""128""><name>" & L_ParamValue_HTMLText & "</name></text>"
						Case DataType_time
							' Not supported
					End Select				
					sXMLParamMeta = sXMLParamMeta & "</fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>"

					sValues = sValues & "P" & sOrdinal & "Val1|"
			End Select
		ElseIf iReportType = Static_MDX Then
			' Construct XML islands for edit sheets and edit fields
			Select Case rs.Fields("ParamType").Value
				Case ParamType_SelectOrder

					' Set default values for dates if none provided
					If sVal1 = "" Or IsNull(sVal1) Then sVal1 = iDefaultTopCount
					If IsNull(iOpnd1) Then iOpnd1 = SelectOrderOpnd_Top
					If iOpnd1 < SelectOrderOpnd_Top Or iOpnd1 > SelectOrderOpnd_Bottom Then iOpnd1 = SelectOrderOpnd_Top

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>" & _
										 "<select id=""P" & sOrdinal & "Opnd1"" onchange=""SelectOperandChange(" & sOrdinal & ")"">" & _
										 "<prompt>" & L_SelectOne_Text & "</prompt><name>" & L_SelectColon_HTMLText & "</name>" & _
										 "<select id=""P" & sOrdinal & "Opnd1"">" & _
										 "<option value=""" & SelectOrderOpnd_Top & """>" & L_Top_ComboBox & "</option>" & _
										 "<option value=""" & SelectOrderOpnd_Bottom & """>" & L_Bottom_ComboBox & "</option>" & _
										 "</select></select><numeric id=""P" & sOrdinal & "Val1"" required=""yes"" "
										 										 
					sXMLParamMeta = sXMLParamMeta & "readonly=""no"" onchange=""CheckTopCount(" & sOrdinal & ")""><name>" & L_ParamValue_HTMLText & "</name></numeric></fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Opnd1>" & iOpnd1 & "</P" & sOrdinal & "Opnd1>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>"

					sTopValues = sTopValues & "P" & sOrdinal & "Val1|"
				Case ParamType_DateRange
					' Not supported for MDX
				Case ParamType_Expression
					' Not supported for MDX
				Case ParamType_SiteName

					' Set current site as the default if none specified.
					If IsNull(sVal1) Then sVal1 = sSiteName

					' Always text
					iDataType = DataType_text

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>" & _
										 "<text id=""P" & sOrdinal & "Field"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamField_HTMLText & "</name></text>"
										 
					sXMLText = " id=""P" & sOrdinal & "Val1"" required=""yes"" readonly=""no"" onchange=""EnableOK()"""

					sXMLParamMeta = sXMLParamMeta & "<text" & sXMLText & " maxlen=""128""><name>" & L_ParamValue_HTMLText & "</name></text>"

					sXMLParamMeta = sXMLParamMeta & "</fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>"

					sValues = sValues & "P" & sOrdinal & "Val1|"
				
				Case ParamType_SingleValue

					' Set default values for dates if none provided
					If IsNull(sVal1) Then sVal1 = ""

					sXMLParamMeta = sXMLParamMeta & "<editsheet><global><name>" & sDescription & _
										 "</name><key></key></global><fields><text id=""P" &_
										 sOrdinal & "Tag"" required=""no"" hide=""yes"" readonly=""yes"" maxlen=""128""><name>" & L_ParamTag_HTMLText & "</name></text>"

					sXMLText = " id=""P" & sOrdinal & "Val1"" required=""no"" onchange=""EnableOK()"""
					Select Case iDataType
						Case DataType_boolean
							' Not supported
						Case DataType_date
							' Not supported
						Case DataType_integer
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & "><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_float
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""float""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_currency
							sXMLParamMeta = sXMLParamMeta & "<numeric" & sXMLText & " subtype=""currency""><name>" & L_ParamValue_HTMLText & "</name></numeric>"
						Case DataType_text
							sXMLParamMeta = sXMLParamMeta & "<text" & sXMLText & " maxlen=""128""><name>" & L_ParamValue_HTMLText & "</name></text>"
						Case DataType_time
							' Not supported
					End Select				
					sXMLParamMeta = sXMLParamMeta & "</fields></editsheet>"

					sXMLParamData = sXMLParamData & "<P" & sOrdinal & "Tag>" & sTag & "</P" & sOrdinal & "Tag>" & _
										"<P" & sOrdinal & "Val1>" & sVal1 & "</P" & sOrdinal & "Val1>"

					sValues = sValues & "P" & sOrdinal & "Val1|"
			End Select
		Else
			' Invalid report type
		End If
		rs.MoveNext
	Wend
	rs.close
	
	sXMLParamMeta = sXMLParamMeta & "</editsheets>"

	' Add header and footer
	sXMLParamData = "<document><record>" & _
							"<DisplayName>" & sReportName & "</DisplayName>" & _
							"<Query>" & sReportQuery & "</Query>" & _
							"<ReportStatusID>" & iReportStatusID & "</ReportStatusID>" & _
							"<ReportID>" & iReportID & "</ReportID>" & _
							"<ReportType>" & iReportType & "</ReportType>" & _
							"<NumParams>" & iParamID & "</NumParams>" & sXMLParamData & "</record></document>"

%>
<SCRIPT LANGUAGE='VBScript'>
	Dim bShowAdvanced	
	bShowAdvanced = 0
	
	Const sEmpty = "___empty___"
	Const L_AdvButtonSize_Style = "width: 8em"
	
	Sub Window_OnLoad()
		dlg_report_params.style.display = ""
		If <% =bExit %> Then 
			Call bdOK_OnClick()
			Exit Sub
		End If
		dlg_report_params.style.display = ""
		bdOK.focus() 
	End Sub

	' Called when any date value is changed
	Sub CheckDateRange(iParam)
		Dim sOpnd1, sVal1, sVal2
		sOpnd1 = "P" & CStr(iParam) & "Opnd1"
		sVal1 = "P" & CStr(iParam) & "Val1"			' StartDate
		sVal2 = "P" & CStr(iParam) & "Val2"			' EndDate
		With esReport
			If (not .field(sVal1).required) And (not .field(sVal2).required) Then	
				If .field(sOpnd1).value = <% =DateRangeOpnd_From %> Then
					If dtGetDate(.field(sVal2).value) - dtGetDate(.field(sVal1).value) < 0 Then
						msgbox "<% =L_ParamDateRangeInvalid_ErrorMessage %>"
						.field(sOpnd1).focus()				' Trick to make the focus appear
						.field(sVal2).focus()
					End If
				End If
			End If
		End With
		EnableOK()
	End Sub

	' Called when any top count value is changed
	Sub CheckTopCount(iParam)
		Dim sOpnd1, sVal1
		sOpnd1 = "P" & CStr(iParam) & "Opnd1"
		sVal1 = "P" & CStr(iParam) & "Val1"			' Top count
		With esReport
			If not .field(sVal1).required Then	
				If .field(sVal1).value <= 0 Then
					msgbox "<% =L_ParamTopCountMin_ErrorMessage %>"
					.field(sOpnd1).focus()					' Trick to make the focus appear
					.field(sVal1).focus()
				ElseIf .field(sVal1).value > 2000000000 Then
					msgbox "<% =L_ParamTopCountMax_ErrorMessage %>"
					.field(sOpnd1).focus()					' Trick to make the focus appear
					.field(sVal1).focus()
				End If
			End If
		End With
		EnableOK()
	End Sub

	' Called when any operand or value is changed
	Sub EnableOK()
		Dim i, aryTopVals, aryVals, aryDateOrdinals
		With esReport

			' Verify that all top fields have valid values
			aryTopVals = split("<% =sTopValues %>", "|")
			For i = 0 to ubound(aryTopVals) - 1
				If (Not .field(aryTopVals(i)).valid) Or .field(aryTopVals(i)).required Then
					document.all.bdOK.disabled = 1
					Exit Sub
				End If
				If .field(aryTopVals(i)).value <= 0 Or .field(aryTopVals(i)).value > 2000000000 Then
					document.all.bdOK.disabled = 1
					Exit Sub
				End If
			Next

			' Verify that all fields have valid values
			aryVals = split("<% =sValues %>", "|")
			For i = 0 to ubound(aryVals) - 1
				If (Not .field(aryVals(i)).valid) Or  .field(aryVals(i)).required Then
					document.all.bdOK.disabled = 1
					Exit Sub
				End If
			Next

			' Verify that all date ranges have To date >= From date
			aryDateOrdinals = split("<% =sDateOrdinals %>", "|")
			For i = 0 to ubound(aryDateOrdinals) - 1
				If .field("P" & aryDateOrdinals(i) & "Opnd1").value = <% =DateRangeOpnd_From %> Then
					If dtGetDate(.field("P" & aryDateOrdinals(i) & "Val2").value) - dtGetDate(.field("P" & aryDateOrdinals(i) & "Val1").value) < 0 Then		
						document.all.bdOK.disabled = 1
						Exit Sub
					End If
				End If
			Next
		End With
		document.all.bdOK.disabled = 0
	End Sub

	' Called when Advanced pressed
	Sub bdAdvanced_OnClick() 
		Dim i, aryOrdinals, aryFieldOrdinals
		aryOrdinals = split("<% =sAllOrdinals %>", "|")
		aryFieldOrdinals = split("<% =sFieldOrdinals %>", "|")
		With esReport
			If bShowAdvanced Then
				bShowAdvanced = 0
				.hide("Query")
				For i = 0 to ubound(aryOrdinals) - 1
					.hide("P" & CStr(aryOrdinals(i)) & "Tag")
				Next
				For i = 0 to ubound(aryFieldOrdinals) - 1
					.hide("P" & CStr(aryFieldOrdinals(i)) & "Field")
				Next
			Else
				bShowAdvanced = 1
				.show("Query")
				For i = 0 to ubound(aryOrdinals) - 1
					.show("P" & CStr(aryOrdinals(i)) & "Tag")
				Next
				For i = 0 to ubound(aryFieldOrdinals) - 1
					.show("P" & CStr(aryFieldOrdinals(i)) & "Field")
				Next
			End If
		End With
	End Sub

	' Called when OK pressed
	Sub bdOK_OnClick() 
		' post saveform to response_runstatic.asp as XML object
		xmlPostFormViaXML(saveform)
		window.returnvalue = 1
		window.close()
	End Sub

	' Called when Cancel pressed
	Function bdcancel_OnClick()
		window.returnValue = -1
		window.close()
	End function

	'Called when the date criteria selection is changed
	Sub DateOperandChange(iParam)
		Dim sOpnd1, sVal1, sVal2
		sOpnd1 = "P" & CStr(iParam) & "Opnd1"
		sVal1 = "P" & CStr(iParam) & "Val1"			' StartDate
		sVal2 = "P" & CStr(iParam) & "Val2"			' EndDate
		With esReport
			select case .field(sOpnd1).value
				case <% =DateRangeOpnd_All %>
					HideAndMakeValid sVal1, <% =DataType_date %>, "<% =Start_Date %>"
					HideAndMakeValid sVal2, <% =DataType_date %>, "<% =End_Date %>"
				case <% =DateRangeOpnd_From %>
					.show(sVal1)
					.show(sVal2)
					CheckDateRange(iParam)
					Exit Sub
				case <% =DateRangeOpnd_OnOrBefore %>
					.show(sVal1)
					HideAndMakeValid sVal2, <% =DataType_date %>, "<% =End_Date %>"
				case else	' All relative date options (Today, This week, etc.) 
					HideAndMakeValid sVal1, <% =DataType_date %>, "<% =Start_Date %>"
					HideAndMakeValid sVal2, <% =DataType_date %>, "<% =End_Date %>"
			end select
		End With
		EnableOK()
	end sub

	'Called when the expression operand selection is changed
	Sub ExpOperandChange(iParam, iOpnd, iType)
		Dim sOpnd1, sOpnd2, sVal1, sVal2
		sOpnd1 = "P" & CStr(iParam) & "Opnd1"
		sOpnd2 = "P" & CStr(iParam) & "Opnd2"
		sVal1 = "P" & CStr(iParam) & "Val1"
		sVal2 = "P" & CStr(iParam) & "Val2"
		If iOpnd = 1 Then
			With esReport
				Select Case .field(sOpnd1).value
					case <% =ExpOpnd_None %>, <% =ExpOpnd_IsNull %>, <% =ExpOpnd_IsNotNull %>
						HideAndMakeValid sVal1, iType, empty
						.hide(sOpnd2)
						HideAndMakeValid sVal2, iType, empty
					case <% =ExpOpnd_Equals %>
						If iType = <% =DataType_text %> And .field(sVal1).value = sEmpty Then .field(sVal1).value = "" 
						.show(sVal1)
						.hide(sOpnd2)
						HideAndMakeValid sVal2, iType, empty
					case else
						If iType = <% =DataType_text %> And .field(sVal1).value = sEmpty Then .field(sVal1).value = "" 
						.show(sVal1)
						.show(sOpnd2)
						Select Case .field(sOpnd2).value
							case <% =ExpOpnd_None %>
								HideAndMakeValid sVal2, iType, empty
							case else
								If iType = <% =DataType_text %> And .field(sVal2).value = sEmpty Then .field(sVal2).value = "" 
								.show(sVal2)
						End Select
				End Select
			End With
		Else
			With esReport
				Select Case .field(sOpnd2).value
					case <% =ExpOpnd_None %>
						HideAndMakeValid sVal2, iType, empty
					case else
						If iType = <% =DataType_text %> And .field(sVal2).value = sEmpty Then .field(sVal2).value = "" 
						.show(sVal2)
				End Select
			End With
		End If
		EnableOK()
	End Sub

	' Called when the Site name operand selection is changed.
	Sub SiteOperandChange(iParam, iOpnd, iType)
		Dim sOpnd1, sVal1
		sOpnd1 = "P" & CStr(iParam) & "Opnd1"
		sVal1 = "P" & CStr(iParam) & "Val1"
		With esReport
			Select Case .field(sOpnd1).value
				case <% =SiteNameOpnd_All %>
					HideAndMakeValid sVal1, iType, "<% =sSiteName %>"
				case <% =SiteNameOpnd_Equals %>
					.show(sVal1)
				case else
					.show(sVal1)
			End Select
		End With
		EnableOK()
	End Sub

	'Called when the SQL select operand selection is changed
	Sub SelectOperandChange(iParam)
		Dim sOpnd1, sVal1
		sOpnd1 = "P" & CStr(iParam) & "Opnd1"
		sVal1 = "P" & CStr(iParam) & "Val1"
		With esReport
			Select Case .field(sOpnd1).value
				case <% =SelectOrderOpnd_All %>, <% =SelectOrderOpnd_Distinct %>
					HideAndMakeValid sVal1, <% =DataType_integer %>, <% =iDefaultTopCount %>
				case <% =SelectOrderOpnd_Top %>
					.show(sVal1)
			End Select
		End With
		EnableOK()
	End Sub

	' Hides the given field and if it does not contain a valid value one is inserted
	Sub HideAndMakeValid(sVal, iType, vDefault)
		With esReport
			.hide(sVal)
			If (not .field(sVal).valid) or .field(sVal).required then
				If not IsEmpty(vDefault) Then
					.field(sVal).value = vDefault
				Else
					Select Case iType
						Case <% =DataType_integer %>, <% =DataType_float %>, <% =DataType_currency %>
							.field(sVal).value = 0
						Case <% =DataType_text %>
							.field(sVal).value = sEmpty
						Case <% =DataType_date %>
							.field(sVal).value = "<% =Start_Date %>"
						Case <% =DataType_boolean %>
							' Not supported
						Case <% =DataType_time %>
							' Not supported
					End Select
				End If
			End If
		End With
	End Sub
</SCRIPT>

</HEAD>
<BODY LANGUAGE="VBScript" style='OVERFLOW-X: hidden; OVERFLOW-Y: hidden' ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<form ID='rundlgform' name='rundlgform'>
	<input type="hidden" ID='ReportName' name='ReportName' value='<% =sReportName %>'>
	<input type="hidden" ID='ReportStatusID' name='ReportStatusID' value='<% =iReportStatusID %>'>
	<input type="hidden" ID='NumParams' name='NumParams' value='<% =iParamID %>'>
</form>

<form ID="saveform" NAME="saveform" style='Height: 405; OVERFLOW-X: hidden; OVERFLOW-Y: hidden' METHOD="POST" ACTION="response_runstatic.asp">
   <input TYPE="hidden" ID="type" VALUE>
	<div ID="editSheetContainer" CLASS="editPageContainer" style='OVERFLOW-X: hidden; OVERFLOW-Y: auto'>
	   <xml id="ParamMeta">
			<% =sXMLParamMeta %>
		</xml>
		<xml id="ParamData">
			<% =sXMLParamData %>
		</xml>
		<div ID="esReport" CLASS="editSheet" MetaXML="ParamMeta" DataXML="ParamData"><% =L_Loading2_Message %></div>
		<BR><BR><BR><BR><BR><BR><BR>
	</div>
</form>

<BR><BR>
<TABLE WIDTH="100%">
	<TR>
		<TD WIDTH="100%"></TD>
		<td>
			<BUTTON ID='bdAdvanced' CLASS='bdbutton' STYLE=L_AdvButtonSize_Style><%= L_Advanced_Button %></BUTTON>
		</td>
		<TD>
			<BUTTON ID='bdok' CLASS='bdbutton' STYLE='WIDTH:6em'><%= L_OK_Button %></BUTTON>
		</TD>
		<TD>
			<BUTTON ID='bdcancel' CLASS='bdbutton' STYLE='WIDTH:6em'><%= L_Cancel_Button %></BUTTON>
		</TD>
		<TD>
			<BUTTON ID='bdhelp' CLASS='bdbutton' STYLE='WIDTH:6em'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "cs_bd_analysis_TSJD.htm"'><%= L_Help_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>
