<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='./include/inc_common.asp' -->

<%
	Dim sOutputReportName
	Dim sOutputReportPath
	Dim oConn
	Dim rs
	Dim sQuery
	Dim oError
	Dim iReportID
	Dim iReportType
	Dim iParams
	Dim iReportStatusID
	Dim bExported
	Dim bAdd
	Dim sDisplayName
	dim dRequestXMLAsDict
	dim g_xmlReturn

	Const L_StaticReportExecutionError_ErrorMessage = "Error executing static report."

	On Error Resume Next

	Session("MSCSBDError") = Empty

	iParams = 0
	iReportStatusID = 0

	Set dRequestXMLAsDict = dGetRequestXMLAsDict()
	iReportID = dRequestXMLAsDict("ReportID")
	iReportStatusID = dRequestXMLAsDict("ReportStatusID")
	bExported = dRequestXMLAsDict("Export")
	bAdd = dRequestXMLAsDict("Add")

	Set oConn = oGetADOConnection(g_MSCSDataWarehouseSQLConnStr)

	If IsEmpty(Session("MSCSBDError")) Then

		If bAdd = 1 Then	' Add a report instance
	
			' Get report definition
			sQuery = "Select DisplayName, Description, ReportType, Query, newid() GUID " & _
			         "From Report Where ReportID = " & iReportID

			Set rs = CreateObject("ADODB.Recordset")
			rs.Open sQuery, oConn
			
			' Create output directory for the reports if it does not exist already

			sOutputReportPath =  Server.MapPath("/" & Application("MSCSBizDeskRoot") & "/") & "\Analysis\Completed Reports\"

			sDisplayName = rs.Fields("DisplayName").value
			sDisplayName = replace(sDisplayName, "'", "''")		 
			sOutputReportName = sDisplayName & "_" & rs.Fields("GUID").value & ".Htm"
			iReportType = rs.Fields("ReportType").value
				
			rs.close
				
			' Create a new report instance
			sQuery = "Insert Into ReportStatus " & _
					"(DisplayName, ReportID, LocationPath, LocationName, RunDateTime, RunBy, Exported) " & _
					"Values ('" & sDisplayName & "', " & iReportID & ", '" & sOutputReportPath & "', '" & _ 
					sOutputReportName & "', GetDate(), '" & g_sUser & "', " & bExported & ")"

			oConn.Execute(sQuery)
										
			rs.Open "Select @@Identity", oConn

			iReportStatusID = rs.Fields(0).value
						
			rs.Close
		
			' Determine if there are any params
			sQuery = "Select Count(*) From ReportParam Where ReportID = " & iReportID
		
			rs.Open sQuery, oConn

			iParams = rs.Fields(0).value
			
			rs.close
			Set rs = Nothing
			
		Else	' Delete the report instance
		
			sQuery = "Delete from ReportStatus Where ReportStatusID = " & iReportStatusID
			oConn.Execute(sQuery)
			
		End If

	End If
	
	' Return XML object with Error node
	set g_xmlReturn = xmlGetXMLDOMDoc()

	AddItemNode "ReportStatusID", iReportStatusID
	AddItemNode "Params", iParams

	If oConn.Errors.count > 0 Then
		For Each oError In oConn.Errors
			AddErrorNode g_xmlReturn, "1", L_StaticReportExecutionError_ErrorMessage, "<![CDATA[" & sGetADOError(oError) & "]]>"
		Next
	Elseif Err.number <> 0 then
		AddErrorNode g_xmlReturn, "1", L_StaticReportExecutionError_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
	End if

	Response.Write g_xmlReturn.xml

	Set oConn = Nothing

	Session("MSCSBDError") = Empty

	On Error GoTo 0
	
%>
