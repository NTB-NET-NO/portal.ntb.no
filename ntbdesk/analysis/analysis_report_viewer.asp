<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='./include/analysisStrings.asp' -->
<!--#INCLUDE FILE='./include/inc_common.asp' -->
<!--#INCLUDE FILE='./include/reports_util.asp' -->
<%
	dim thisPage
	dim g_sModuleName
	dim g_sKey
	dim g_sSubKey
	dim g_sStatustext
	dim dCompletedReports
	
	thisPage = "analysis_report_viewer.asp"
	g_sModuleName = L_CompletedReports_StaticText

 	'Initializes the ReportGroup Object
 	IntializeReportGroup
 	
	'Implementation for saving states
	set dCompletedReports = Server.CreateObject("Commerce.Dictionary")
	if not IsEmpty(Session("dCompletedReports")) then
		set dCompletedReports = Session("dCompletedReports")
	else
		' Automatically display the first page of reports
		dCompletedReports.selfindby = "All"
		dCompletedReports.pagetype = "completedreports"
		dCompletedReports.column = "RunDateTime_date" 
		dCompletedReports.direction = "desc" 
		dCompletedReports.page = 1 
		set Session("dCompletedReports") = dCompletedReports
	end if
	
	g_sKey = "GUID"
	g_sSubKey = "Name"
	
	'Implementation for deleting completed reports
	if Request.Form("type") = "delete" then
		Dim items, count, i, nResult
		if Request.Form("selectall") then
			nResult = deleteAllRecords(dCompletedReports,thisPage)
			if nResult = 0 then
				g_sStatusText = L_AllDeleted_StatusBar
			else
				g_sStatusText = L_ErrorDelete_StatusBar
			end if
		else
			items = Split(Request.Form("sel"),",")
			count = ubound(items) + 1
			nResult = deleteRecords(items,thisPage)
			if nResult = 0 then
				g_sStatusText = sFormatString(L_Delete_StatusBar,Array(count))
			else
				g_sStatusText = L_ErrorDelete_StatusBar
			end if
		end if
	end if
%>

<HTML>
<HEAD>
    <TITLE></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<LINK REL='stylesheet' TYPE='text/css' HREF='../../widgets.css' ID='widgetstylesheet'>
<STYLE>
	.hide {
	   DISPLAY: none;
	}
	DIV.esContainer, .esEditgroup DIV.esContainer
	{
	    BACKGROUND-COLOR: transparent;
	}
</STYLE>

<!--#INCLUDE FILE='./include/inc_select.vbs' -->

<SCRIPT LANGUAGE='VBScript'>

   sub window_OnLoad()
		setSelect()
		document.all.btnfindby.disabled = 0
		SetTaskButtons <%= ID_REPORT_VIEWER %>		
	end sub
    
	sub changeSelect()
		setSelect()
		ShowFindBy()
	end sub
	'Called when the find by selection changes
	'Decides what are the fields to be displayed
	sub setSelect()
		On Error Resume Next		' In case form is not fully loaded yet
		' Report name
		rnrow.style.display = "none"
		fbreportname.style.display = "none"				
		' Date range
		rbrow.style.display = "none"				
		fbdate.style.display = "none"					
		start_date.style.display = "none"
		torow.style.display = "none"
		end_date.style.display = "none"
		' Run by
		fbrow.style.display = "none"
		fbrunby.style.display = "none"
		' Report status
		rsrow.style.display = "none"
		fbreportstatus.style.display = "none"
		select case selfindby.value
			case "Name"
				setFindByHeight(150)
				rnrow.style.display = "block"
				fbreportname.style.display = "block"
			case "RunBy"
				setFindByHeight(150)
				rbrow.style.display = "block"
				fbrunby.style.display = "block"
			case "RunDateTime"
				setFindByHeight(150)
				fbrow.style.display = "block"
				fbdate.style.display = "block"
				changeDate()
			case "ExecutionState"
				setFindByHeight(150)
				rsrow.style.display = "block"
				fbreportstatus.style.display = "block"
			case "Any"
				setFindByHeight(200)
				rnrow.style.display = "block"
				fbreportname.style.display = "block"
				rbrow.style.display = "block"
				fbrunby.style.display = "block"
				fbrow.style.display = "block"
				fbdate.style.display = "block"
				rsrow.style.display = "block"
				fbreportstatus.style.display = "block"
				changeDate()
			case "All"
				setFindByHeight(150)
				document.all.btnfindby.disabled = 0
		end select
		On Error Goto 0
	end sub
	
	'Called when the date criteria selection is changed
	sub changeDate()
		select case fbdate.value
			case "from"
				torow.style.display = "block"
				start_date.style.display = "block"
				end_date.style.display = "block"
			case "onbefore"
				torow.style.display = "none"
				start_date.style.display = "block"
				end_date.style.display = "none"
			case else
				torow.style.display = "none"
				start_date.style.display = "none"
				end_date.style.display = "none"
		end select
	end sub

	'Called when the Find Now button is clicked
	sub reloadListSheet()
		lsReports.page = 1
		lsReports.reload("findby")
		OnAllRowsUnselect <%= ID_REPORT_VIEWER %>
	end sub
</SCRIPT>

</HEAD>
<BODY LANGUAGE='VBScript'>

<%
	dim dBDErrorx, slBDErrorx, bSevereError
	bSevereError = False
		
	if not isEmpty(Session("MSCSBDError")) then
		set slBDErrorx = Session("MSCSBDError")
		for each dBDErrorx in slBDErrorx
			If dBDErrorx("errortype") < ERROR_ICON_QUESTION then bSevereError = True
		next
	end if
	
	if (IsEmpty(Session("MSCSBDError")) Or (not isEmpty(Session("MSCSBDError")) And bSevereError = False)) Then
		if not IsEmpty(Session("dCompletedReports")) then
			dim g_rsQuery, nRecordCount
			dim xmlReports, xmlList, node, oFormat
				
			set oFormat = Server.CreateObject("Commerce.Dictionary")
			oFormat.RunDateTime = COMMERCE_SPLIT_DATE

			g_nPage = dCompletedReports.page
				
			set g_rsQuery = rsGetCompletedReports(dCompletedReports,PAGE_SIZE,nRecordCount)
			g_nItems = nRecordCount
				
			if not isEmpty(g_nItems) then
				set xmlReports = Server.Createobject("Microsoft.XMLDOM")
				xmlReports.loadXML xmlGetXMLFromRSEx(g_rsQuery, -1, -1, nRecordCount, oFormat).xml
			end if
		end if
	end if		

	InsertTaskBar g_sModuleName, g_sStatusText

	'Set default findby dates if null
	if isNull(dCompletedReports.Start_Date) or isNull(dCompletedReports.End_Date) then
	   dCompletedReports.Start_Date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)
		dCompletedReports.End_Date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)
	end if
%>

<!-- Start FindBy Area -->
<xml id="fbmeta">
<editsheet>
    <global title='no'/>
	<fields>
		<select id='selfindby' onchange='changeSelect'>
			<select id='selfindby'>
				<option value='Name'><%= L_NameCR_ComboBox %></option>
				<option value='RunDateTime'><%= L_RunDate_ComboBox %></option>
				<option value='RunBy'><%= L_RunBy_ComboBox %></option>
				<option value='ExecutionState'><%= L_Status_ComboBox %></option>
				<option value='Any'><%= L_AnyInfoCR_ComboBox %></option>
				<option value='All'><%= L_AllReportsCR_ComboBox %></option>
			</select>
		</select>
		<text id='fbreportname' subtype='short' maxlen='128'>
		</text>
		<select id='fbdate' onchange='changeDate'>
			<select id='fbdate'>
				<option value='from'><%= L_From_ComboBox%></option> 
				<option value='onbefore'><%= L_OnOrBefore_ComboBox%></option>
			</select>
		</select>
	    <date id='start_date' required='yes' firstday='<%= g_sMSCSWeekStartDay %>'>
	        <format><%= g_sMSCSDateFormat%></format>
	    </date>
	    <date id='end_date' required='yes' firstday='<%= g_sMSCSWeekStartDay %>'>
	        <format><%= g_sMSCSDateFormat%></format>
	    </date>
		<text id='fbrunby' subtype='short' maxlen='128'>
		</text>
		<select id='fbreportstatus'>
			<select id='fbreportstatus'>
				<option value='pending'><%= L_Pending_ComboBox %></option> 
				<option value='completed'><%= L_Completed_ComboBox %></option>
				<option value='failed'><%= L_Failed_ComboBox %></option>
			</select>
		</select>
	</fields>
    <template fields='selfindby fbreportname fbdate start_date end_date fbrunby fbreportstatus'><![CDATA[
		<TABLE WIDTH='100%' STYLE='TABLE-LAYOUT: fixed'>
			<COLGROUP><COL WIDTH='100'><COL WIDTH='120'><COL WIDTH='200'><COL></COLGROUP>
			<TR>
				<TD>
					<SPAN ID='findbylabel'><%= L_FindBy_Text %></SPAN>
				</TD>
				<TD COLSPAN=2>
					<DIV ID='selfindby'
						ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
				<TD>
					<BUTTON CLASS='bdbutton' LANGUAGE='VBScript' ONCLICK='reloadListSheet' 
							ID='btnfindby' NAME='btnfindby' DISABLED><%= L_FindNow_Button %></BUTTON>
				</TD>
			</TR>
			<TR ID='rnrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='rnlabel'><%= L_ReportName_Text %></SPAN>
				</TD>
				<TD COLSPAN=2>
					<DIV ID='fbreportname'
						ONKEYUP='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='fbrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='datelabel'><%= L_DateRun_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='fbdate'
						ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
				<TD>
					<DIV ID='start_date' STYLE='DISPLAY: none'
						 ONCHANGE='CheckDate("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='torow' STYLE='DISPLAY: none'>
				<TD></TD>
				<TD>
					<SPAN ID='tolabel'><%= L_To_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='end_date'
						 ONCHANGE='CheckDate("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='rbrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='rblabel'><%= L_RunBy_Text %></SPAN>
				</TD>
				<TD COLSPAN=2>
					<DIV ID='fbrunby'
						ONKEYUP='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='rsrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='rslabel'><%= L_ReportStatus_Text %></SPAN>
				</TD>
				<TD COLSPAN=2>
					<DIV ID='fbreportstatus'
						ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
		</TABLE>		
    ]]></template>
</editsheet>
</xml>
	
<xml id='fbData'>
	<document>
		<record>
			<selfindby><%= dCompletedReports.selfindby %></selfindby>
			<fbreportname><%= dCompletedReports.fbreportname %></fbreportname>
			<fbrunby><%= dCompletedReports.fbrunby %></fbrunby>
			<fbdate><%= dCompletedReports.fbdate %></fbdate>
			<start_date><%= dCompletedReports.Start_Date %></start_date>
			<end_date><%= dCompletedReports.End_Date %></end_date>
			<fbreportstatus><%= dCompletedReports.fbreportstatus %></fbreportstatus>
		</record>
	</document>
</xml>

<FORM ID='searchform' ACTION='reports_event.asp'>
	<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE='completedreports'>
	<DIV ID='bdfindbycontent' CLASS='findbycontent'>
		<div ID="esFind" class="editSheet"
			METAXML='fbMeta' DATAXML='fbData'></div>
	</DIV>
</FORM>
<!-- End FindBy Area -->

<FORM ID='neweventform' NAME='neweventform' ACTION='reports_event.asp'>
	<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE='completedreports'>
	<INPUT TYPE='hidden' ID='page' NAME='page' VALUE='<%= g_nPage %>'>
</FORM>

<FORM ID='deleteform' NAME='deleteform' ACTION='analysis_report_viewer.asp' METHOD='POST' ONTASK='OnDelete <%= ID_REPORT_VIEWER %>'>
	<INPUT TYPE='hidden' ID='type' NAME='type'>
	<INPUT TYPE='hidden' ID='sel' NAME='sel' VALUE=''>
	<INPUT TYPE='hidden' ID='selectall' NAME='selectall'>
</FORM>
	
<FORM ID='openform' NAME='openform' ACTION='analysis_report_viewer.asp' METHOD='POST' ONTASK='OnOpen <%= ID_REPORT_VIEWER %>'>
	<INPUT TYPE='hidden' ID='sel' NAME='sel' VALUE=''>
	<INPUT TYPE='hidden' ID='selectall' NAME='selectall'>
	<INPUT TYPE='hidden' ID='LocationNames' NAME='LocationNames' VALUE=''>
	<INPUT TYPE='hidden' ID='LocationPaths' NAME='LocationPaths' VALUE=''>
</FORM>

<DIV ID='bdcontentarea'>
   <xml id='reportsList'>
	<%	
		if not IsEmpty(Session("dCompletedReports")) and not isEmpty(g_nItems) then
			' Map execution state codes to text for display
			set xmlList = xmlReports.selectNodes("//ExecutionState")
			for each node in xmlList
				If isNumeric(node.text) then
					Select Case CInt(node.text)
						Case Report_None
							node.text = L_ReportFailed_Text					
						Case Report_Pending
							node.text = L_ReportPending_Text				
						Case Report_Completed
							node.text = L_ReportCompleted_Text
						Case Report_Failed
							node.text = L_ReportFailed_Text
						Case Else	
							node.text = L_ReportFailed_Text
					End Select
				End If
			next
			Response.write xmlReports.xml
			set g_rsQuery = Nothing
			set xmlReports = Nothing
			set xmlList = Nothing
			set oFormat = Nothing
		else
			Response.Write("<document><record/></document>")
		end if
	%>	
	</xml>
	
   <xml id='reportsMeta'>
      <listsheet>
         <global curpage='<%= g_nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_nItems %>'
            selection='multi' />
            <columns>
               <column id='GUID' hide='yes'></column>
               <column id='Name'  width='33'><%= L_CompRpt_Name_Col_Text %></column>
               <column id='LocationName' hide='yes'></column>
               <column id='LocationPath' hide='yes'></column>
               <column id='RunDateTime_date'  width='17'><%= L_CompRpt_DateRun_Col_Text %></column>
               <column id='RunDateTime_time'  width='16'><%= L_CompRpt_TimeRun_Col_Text %></column>
               <column id='RunBy'  width='17'><%= L_CompRpt_RunBy_Col_Text %></column>
               <column id='ExecutionState'  width='17'><%= L_CompRpt_Status_Col_Text %></column>
            </columns>
			 <operations>
				<newpage formid='neweventform'/>
				<sort    formid='neweventform'/>
				<findby  formid='searchform'/>
			</operations>
      </listsheet>
   </xml>

<div ID="filtertext" class="filtertext" style="MARGIN-TOP: 20px"><%= L_BDSSFilter_Text %></div>

   <DIV ID='lsReports' 
		CLASS='listSheet' STYLE="MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px; HEIGHT: 80%"
		DataXML='reportsList' 
		MetaXML='reportsMeta'
		LANGUAGE='VBScript'
		OnRowSelect='OnSelectRow <%= ID_REPORT_VIEWER%>'
		OnRowUnselect='OnRowUnselect <%= ID_REPORT_VIEWER%>'
		OnAllRowsSelect='OnAllRowsSelect <%= ID_REPORT_VIEWER%>'
		OnAllRowsUnselect='OnAllRowsUnselect <%= ID_REPORT_VIEWER%>'><%= L_LoadingList_Text%></DIV>

</DIV>
</BODY>
</HTML>
<% Cleanup %>

