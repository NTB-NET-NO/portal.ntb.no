<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE='../include/BizDeskPaths.asp' -->
<!--#INCLUDE FILE="../include/DBUtil.asp" -->
<!--#INCLUDE FILE="../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="include/inc_common.asp"-->
<!--#INCLUDE FILE='include/analysisStrings.asp' -->
<!--#INCLUDE FILE="include/reports_util.asp" -->
<%	
	dim nPage
	dim nRecordCount
	dim column
	dim direction
	dim pageType
	dim g_rsQuery
	dim g_sQuery
	dim item
	dim xmlReports
	dim xmlList
	dim node
	dim g_dRequest
	dim dBDError, slBDError

	set g_dRequest = dGetRequestXMLAsDict()

	'Initialize the ReportGroup
	IntializeReportGroup
	
	nPage = g_dRequest("page")
	column = g_dRequest("column")
	direction = g_dRequest("direction")
	pageType = g_dRequest("pagetype")

	'generation of xml for various operations and pagetypes
	select case pageType
		case "reports"	
			On Error Resume Next
			dim dreports
			
			'implementation to store the search criteria in the session
			set dreports = Server.CreateObject("Commerce.Dictionary")

			if not IsEmpty(Session("dreports")) then 
				set dreports = Session("dreports")
			else
				dReports.pagetype = "reports"
				dreports.column = "DisplayName"
				dreports.direction = "asc"
				dreports.page = 1
			end if

			if g_dRequest("op") = "findby" then 
				for each item in g_dRequest
					dreports.value(item) = g_dRequest(item)
				next
				if dreports.column = "" then dreports.column = "DisplayName" 
				if dreports.direction = "" then dreports.direction = "asc" 
				dreports.page = 1
			elseif g_dRequest("op") = "sort" then 
				if column <> "" and direction <> "" then
					if (dreports.column = column) and (dreports.direction = direction) then 	
						' Reverse sort direction
						if direction = "asc" Then
							direction = "desc"
						else
							direction = "asc"
						end if
					end if
					dreports.column = column
					dreports.direction = direction
				end if
				dreports.page = 1
			elseif g_dRequest("op") = "newpage" then 
				If nPage <> "" Then 
					dreports.page = nPage
				else
					dreports.page = 1
				end if 
			end if
			set Session("dreports") = dreports
			
			set xmlReports = Server.Createobject("Microsoft.XMLDOM")

			set g_rsQuery = rsGetRecords(dreports,PAGE_SIZE,nRecordCount)

			if not isEmpty(Session("MSCSBDError")) then
				set slBDError = Session("MSCSBDError")
				for each dBDError in slBDError
					Response.Write getErrorXML(0, dBDError("friendlyerror"), dBDError("errordetails"))
					exit for
				next
			else
				xmlReports.loadXML xmlGetXMLFromRSEx(g_rsQuery, -1, -1, nRecordCount, null).xml

				'map codes to text for display
				set xmlList = xmlReports.selectNodes("//ReportType")
				for each node in xmlList
					If isNumeric(node.text) then
						Select Case CInt(node.text)
							Case Dynamic_OWC_SQL, Dynamic_OWC_MDX
								node.text = L_ReportDynamic_Text					
							Case Static_SQL, Static_MDX
								node.text = L_ReportStatic_Text				
							Case Else		' Should not occur.
								node.text = L_ReportUnknown_Text
						End Select
					End If
				next

		 		if Err.number <> 0 then
		 			Response.Write getErrorXML(Err.number, Err.source, Err.description)
				else
					Response.write xmlReports.xml
				end if
			end if
			
		case "completedreports"
			On Error Resume Next
			dim dCompletedReports
			dim oFormat
			
			set oFormat = Server.CreateObject("Commerce.Dictionary")
			oFormat.RunDateTime = COMMERCE_SPLIT_DATE

			'implementation to store the search criteria in the session
			set dCompletedReports = Server.CreateObject("Commerce.Dictionary")
			
			if not IsEmpty(Session("dCompletedReports")) then 
				set dCompletedReports = Session("dCompletedReports")
			else
				dCompletedReports.pagetype = "completedreports"
				dCompletedReports.column = "RunDateTime_date"
				dCompletedReports.direction = "desc"
				dCompletedReports.page = 1
			end if

			if g_dRequest("op") = "findby" then 
				for each item in g_dRequest
					dCompletedReports.value(item) = g_dRequest(item)
				next
				if dCompletedReports.column = "" then dCompletedReports.column = "RunDateTime_date" 
				if dCompletedReports.direction = "" then dCompletedReports.direction = "desc" 
				dCompletedReports.page = 1
			elseif g_dRequest("op") = "sort" then 
				if column <> "" and direction <> "" then
					if column = "RunDateTime_time" then column = "RunDateTime_date" 
					if (dCompletedReports.column = column) and (dCompletedReports.direction = direction) then 	
						' Reverse sort direction
						if direction = "asc" Then
							direction = "desc"
						else
							direction = "asc"
						end if
					end if
					dCompletedReports.column = column
					dCompletedReports.direction = direction
				end if
				dCompletedReports.page = 1
			elseif g_dRequest("op") = "newpage" then 
				If nPage <> "" Then 
					dCompletedReports.page = nPage
				else
					dCompletedReports.page = 1
				end if 
			end if
			set Session("dCompletedReports") = dCompletedReports

			set xmlReports = Server.Createobject("Microsoft.XMLDOM")
			set g_rsQuery = rsGetCompletedReports(dCompletedReports,PAGE_SIZE,nRecordCount)
			
			if not isEmpty(Session("MSCSBDError")) then
				set slBDError = Session("MSCSBDError")
				for each dBDError in slBDError
					Response.Write getErrorXML(0, dBDError("friendlyerror"), dBDError("errordetails"))
					exit for
				next
			else
				xmlReports.loadXML xmlGetXMLFromRSEx(g_rsQuery, -1, -1, nRecordCount, oFormat).xml

				' Map execution state codes to text for display
				set xmlList = xmlReports.selectNodes("//ExecutionState")
				for each node in xmlList
					If isNumeric(node.text) then
						Select Case CInt(node.text)
							Case Report_None
								node.text = L_ReportFailed_Text					
							Case Report_Pending
								node.text = L_ReportPending_Text				
							Case Report_Completed
								node.text = L_ReportCompleted_Text
							Case Report_Failed
								node.text = L_ReportFailed_Text
							Case Else	
								node.text = L_ReportFailed_Text
						End Select
					End If
				next

		 		if Err.number <> 0 then
		 			Response.Write getErrorXML(Err.number, Err.source, Err.description)
				else
					Response.write xmlReports.xml
				end if
			end if
			set oFormat = Nothing
	end select
	
	set g_rsQuery = Nothing
	set xmlreports = Nothing
	set xmlList = Nothing
	On Error GoTo 0			

	call Cleanup ()
	
	function getErrorXML(hRes, strSrc, strDesc)
		hRes = hRes And 65535
		getErrorXML = "<document><ERROR ID='" & hRes & "' SOURCE=""" & strSrc & """>" & _
		    "<![CDATA[" & strDesc & "]]></ERROR></document>"
	end function
	
%>