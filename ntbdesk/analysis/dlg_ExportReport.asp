<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/analysisStrings.asp' -->
<!--#INCLUDE FILE='../include/DialogUtil.asp' -->

<HTML>
<HEAD>
<TITLE><% =L_ExportReport_HTMLTitle %></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
BUTTON
{
	WIDTH: 5em;
}
</STYLE>
<SCRIPT LANGUAGE='VBScript'>
<!--	
	sub window_onload()
		window.dialogHeight = "275px"
		Dim aDlgArgs
		aDlgArgs = window.dialogArguments
		importdlgform("ReportStatusID").value = aDlgArgs(0)
		ReportName.value = aDlgArgs(1)
		importdlgform("DmExport").value = aDlgArgs(2)
		importdlgform("UpmExport").value = aDlgArgs(3)
	end sub
	
	Function bdcancel_OnClick()
		window.returnValue = -1
		window.close()
	end function

	sub bdOk_OnClick() 
		dim retval(6), aryInvalid, n

		'get the list type; Static or Dynamic
		if document.all.list_type(0).checked then
			retval(0) = document.all.list_type(0).value
		else
			retval(0) = document.all.list_type(1).value
		End if
		
		'get the list name
		retval(1) = Trim(list_name.value)
		if Len(retval(1)) = 0 then
			msgbox "<% =L_ListNameEmpty_Message %>", vbOKOnly, "<% =L_ListNameMissing_Message %>"
			list_name.focus
			exit sub		
		End if
		
		aryInvalid = Array("?","|","<",">","""","&","+","%")
		for n = 0 to ubound(aryInvalid)			
			if Instr(1, retval(1), aryInvalid(n)) <> 0 Then
				msgbox "<% =L_InvalidCharInListName_Message %>", VBOkOnly, "<% =L_InvalidListName_Message %>"
				list_name.focus
				exit sub				
			end if
		next
		
		'get the list_creation_type	 - DW Calc Type : 3						
		retval(2) = importdlgform("list_creation_type").value

		'get the unique ID for this export instance
		retval(3) = importdlgform("ReportStatusID").value
		retval(4) = " "			' Used for SQL query		
		retval(5) = importdlgform("DmExport").value
		retval(6) = importdlgform("UpmExport").value
		
		window.returnvalue = Join(retval, "|")
		window.close()
	end sub
-->
</SCRIPT>

</HEAD>
<BODY LANGUAGE='VBScript' ONLOAD='bdOk.focus()' ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<xml id='efMeta'>
	<fields>
	    <text id='list_name' required="yes" maxlen='255' subtype='short'>
	        <name><%= L_ListName_EditBox %></name>
	    </text>
	    <text id='ReportName' readonly='yes' subtype='short'>
	        <name><%= L_DWReportName_HTMLText %></name>
	    </text>
	</fields>
</xml>
<xml id='efData'>
	<document/>
</xml>

<FORM action='' method='post' ID='importdlgform' >
	<input type='hidden' name='ReportStatusID'>
	<input type='hidden' name='DmExport'>
	<input type='hidden' name='UpmExport'>
	<input type='hidden' name='list_creation_type' value='3'>
</FORM>

<% =L_ListType_HTMLText %>
<table border='0'>
	<tr>
		<td width='15'>&nbsp;</td>
		<td><INPUT type='radio' id='list_type0' name='list_type' VALUE='0' CHECKED STYLE="BACKGROUND-COLOR: transparent"><label for='list_type0'><% =L_Static_OptionButton %></label></td>
	</tr>
	<tr>
		<td width='15'>&nbsp;</td>
		<td><INPUT type='radio' id='list_type2' name='list_type' VALUE='2' STYLE="BACKGROUND-COLOR: transparent"><label for='list_type2'><% =L_Dynamic_OptionButton %></label></td>
	</tr>
</table>
<br>

<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
	<tr>
			<td><% =L_ListName_EditBox %></td>
	</tr>
	<tr>
			<td>
			<DIV ID='list_name' CLASS='editField'
				MetaXML='efMeta' DataXML='efData'></DIV>
			</td>
	</tr>
	<tr>
			<td><% =L_DWReportName_HTMLText %></td>
	</tr>
			<td>
			<DIV ID='ReportName' CLASS='editField'
				MetaXML='efMeta' DataXML='efData'></DIV>
			</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<TABLE border=0 cellpadding=0 cellspacing=1 width="100%">
	<TR>
		<TD WIDTH="100%">&nbsp;</TD>
		<TD><nobr>
			<BUTTON ID='bdok' CLASS='bdbutton'><%= L_OK_Button %></BUTTON>
			<BUTTON ID='bdcancel' CLASS='bdbutton'><%= L_Cancel_Button %></BUTTON>
			<BUTTON ID='bdhelp' CLASS='bdbutton'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "cs_bd_analysis_YHTD.htm"'><%= L_Help_Button %></BUTTON>
		</nobr></TD>
	</TR>
</TABLE>

</BODY>
</HTML>
