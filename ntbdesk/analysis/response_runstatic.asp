<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE='./include/inc_common.asp' -->
<%

	dim dRequestXMLAsDict
	dim sQuery
	dim iParamID
	dim sKey
	dim rs
	dim iReportID
	dim iReportType
	dim iReportStatusID
	dim sOrdinal
	dim iDataType
	dim iParamType
	dim sTag
	dim iOpnd1
	dim iOpnd2
	dim sVal1
	dim sVal2
	dim sFieldName

	Set dRequestXMLAsDict = dGetRequestXMLAsDict()

	iReportID = dRequestXMLAsDict("ReportID")
	iReportType = CInt(dRequestXMLAsDict("ReportType"))
	iReportStatusID = dRequestXMLAsDict("ReportStatusID")

	Set g_oConn = oGetADOConnection(g_MSCSDataWarehouseSQLConnStr)
	Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)

'	' Get the ReportID
'	sQuery = "Select ReportID From ReportStatus Where ReportStatusID = " & iReportStatusID
'	g_oCmd.CommandText = sQuery
'	Set rs = Server.CreateObject("ADODB.Recordset")
'	rs.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
'
'	iReportID = rs.fields("ReportID").value
'	rs.close
'
	' Query to get the param defs for this report
	sQuery = "Select * From ReportParam Where ReportParam.ReportID = " & iReportID

	g_oCmd.CommandText = sQuery
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open g_oCmd, , AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC

	sQuery = ""

	' Build insert statements for params 
	While not rs.EOF
		sOrdinal = CStr(rs.Fields("Ordinal").Value)
		iDataType = rs.Fields("DataType").Value
		iParamType = rs.Fields("ParamType").Value
		sTag = dRequestXMLAsDict("P" & sOrdinal & "Tag")
		sFieldName = rs.Fields("FieldName").Value

		If iReportType = Static_SQL Then	
			Select Case iParamType
				Case ParamType_SelectOrder
					iOpnd1 = dRequestXMLAsDict("P" & sOrdinal & "Opnd1")
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Opnd1, Val1) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", " & iOpnd1 & ", '" & sVal1 & "')" & vbcrlf

					'	P1Opnd1	1
					'	P1Tag	[$SelectOrder]
					'	P1Desc	Number of users
					'	P1Val1	25

				Case ParamType_DateRange
					iOpnd1 = dRequestXMLAsDict("P" & sOrdinal & "Opnd1")
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					If sVal1 <> "" Then sVal1 = nGetIntlDate(sVal1)
					sVal2 = dRequestXMLAsDict("P" & sOrdinal & "Val2")
					If sVal2 <> "" Then sVal2 = nGetIntlDate(sVal2)
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Opnd1, Val1, Val2, FieldName) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", " & iOpnd1 & ", '" & sVal1 & "', '" & sVal2 & "', '" & sFieldName & "')" & vbcrlf

					'	P2Val2	4/1/2000
					'	P2Opnd1	6
					'	P2Desc	User registration date
					'	P2Val1	3/1/2000
					'	P2Tag	[$DateRange]
				
				Case ParamType_Expression
					iOpnd1 = dRequestXMLAsDict("P" & sOrdinal & "Opnd1")
					iOpnd2 = dRequestXMLAsDict("P" & sOrdinal & "Opnd2")
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					sVal1 = Replace(sVal1, "'", "''")
					sVal2 = dRequestXMLAsDict("P" & sOrdinal & "Val2")
					sVal2 = Replace(sVal2, "'", "''")
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Opnd1, Val1, Opnd2, Val2, FieldName) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", " & iOpnd1 & ", '" & sVal1 & "', " & iOpnd2 & ", '" & sVal2 & "', '" & sFieldName & "')" & vbcrlf

					'	P3Desc	User type
					'	P3Opnd2	6
					'	P3Tag	[$Expression]
					'	P3Val1	0
					'	P3Opnd1	4
					'	P3Val2	1
				
				Case ParamType_SiteName
					iOpnd1 = dRequestXMLAsDict("P" & sOrdinal & "Opnd1")
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					sVal1 = Replace(sVal1, "'", "''")
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Opnd1, Val1, FieldName) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", " & iOpnd1 & ", '" & sVal1 & "', '" & sFieldName & "')" & vbcrlf

					'	P5Desc	Site Name
					'	P5Tag	[$SiteName]
					'	P5Val1	Retail
					'	P5Opnd1	1
				
				Case ParamType_SingleValue
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					sVal1 = Replace(sVal1, "'", "''")
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Val1) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", '" & sVal1 & "')" & vbcrlf

					'	P4Tag	[$SimpleValue]
					'	P4Val1	And Email is not Null
					'	P4Desc	Email qualifier
				
			End Select
			
		ElseIf iReportType = Static_MDX Then	
			
			Select Case iParamType
				Case ParamType_SelectOrder
					iOpnd1 = dRequestXMLAsDict("P" & sOrdinal & "Opnd1")
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Opnd1, Val1) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", " & iOpnd1 & ", '" & sVal1 & "')" & vbcrlf

					'	P1Opnd1	1
					'	P1Tag	[$SelectOrder]
					'	P1Desc	Number of users
					'	P1Val1	25

				Case ParamType_SiteName
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					sVal1 = Replace(sVal1, "'", "''")
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Val1) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", '" & sVal1 & "')" & vbcrlf

					'	P5Desc	Site Name
					'	P5Tag	[$SiteName]
					'	P5Val1	Retail
				
				Case ParamType_SingleValue
					sVal1 = dRequestXMLAsDict("P" & sOrdinal & "Val1")
					sVal1 = Replace(sVal1, "'", "''")
					sQuery = sQuery & "Insert into ReportInstanceParam (ReportStatusID, ParamName, ParamType, DataType, Val1) " & _
											"Values (" & iReportStatusID & ", '" & sTag & "', " & iParamType & ", " & iDataType & ", '" & sVal1 & "')" & vbcrlf

					'	P4Tag	[$SimpleValue]
					'	P4Val1	And Email is not Null
					'	P4Desc	Email qualifier
				
			End Select
		End If
			
		rs.MoveNext
	Wend

	rs.close

	'	CREATE TABLE [dbo].[ReportParam] (
	'		[ReportParamID] [int] identity ,		
	'		[ReportID] [int] NOT NULL ,
	'		[ParamName] [nvarchar] (128) NOT NULL ,
	'		[ParamDescription] [nvarchar] (128) NOT NULL ,
	'		[Ordinal] [tinyint] NULL ,	
	'		[ParamType] [tinyint] NOT NULL Default 0 ,   			 	
	'		[DataType] [tinyint] NULL ,	
	'		[Opnd1] [tinyint] NULL ,
	'		[Val1] [nvarchar] (128) NULL ,
	'		[Opnd2] [tinyint] NULL ,
	'		[Val2] [nvarchar] (128) NULL ,
	'		[ParamDefaultValue] [nvarchar] (128) NULL ,  /* This will be removed */
	'		[FieldName] [nvarchar] (128) NULL ,
	'		[AllowNull] [bit] NOT NULL Default 0 ,		/* 0 = False, 1 = True */
	'		[Automatic] [bit] NOT NULL Default 0		/* 0 = False, 1 = True (does not display in UI) */
		

	'	CREATE TABLE [dbo].[ReportInstanceParam] (
	'		[ReportParamID] [int] ,
	'		[ReportStatusID] [int] NOT NULL ,
	'		[ParamName] [nvarchar] (128) NULL ,
	'		[ParamValue] [nvarchar] (128) NULL ,		/* This field will be replaced by Value1 */	
	'		[Ordinal] [tinyint] NULL ,						/* Only used for UI presentation, not needed here??? */
	'		[ParamType] [tinyint] NULL ,   				
	'		[DataType] [tinyint] NULL ,
	'		[Opnd1] [tinyint] NULL ,
	'		[Val1] [nvarchar] (128) NULL ,
	'		[Opnd2] [tinyint] NULL ,
	'		[Val2] [nvarchar] (128) NULL ,
	'		[FieldName] [nvarchar] (128) NULL ,
	'		[AllowNull] [bit] NOT NULL Default 0 ,		/* 0 = False, 1 = True */
	'		[Automatic] [bit] NOT NULL Default 0		/* 0 = False, 1 = True (does not display in UI) */

	'	NumParams	4
	'	DisplayName	Registered User Properties Test
	'	ReportStatusID	13
	'	ReportID	114
	
	'<document>
	'<record>
	'<DisplayName>Registered User Properties Test</DisplayName>
	'<ReportStatusID>31</ReportStatusID>
	'<ReportID>110</ReportID>
	'<NumParams>0</NumParams>
	'<P1Desc>Number of users</P1Desc>
	'<P1Tag>[$SelectOrder]</P1Tag>
	'<P1Opnd1>1</P1Opnd1>
	'<P1Val1>25</P1Val1>
	'<P2Desc>User registration date</P2Desc>
	'<P2Tag>[$DateRange]</P2Tag>
	'<P2Opnd1>6</P2Opnd1>
	'<P2Val1>00000401</P2Val1>
	'<P2Val2>00000407</P2Val2>
	'<P3Desc>User type</P3Desc>
	'<P3Tag>[$Expression]</P3Tag>
	'<P3Opnd1>4</P3Opnd1>
	'<P3Val1>0</P3Val1>
	'<P3Opnd2>6</P3Opnd2>
	'<P3Val2>1</P3Val2>
	'<P4Desc>Email qualifier</P4Desc>
	'<P4Tag>[$SimpleValue]</P4Tag>
	'<P4Val1>And Email is not Null</P4Val1>
	'</record>
	'</document>

	g_oConn.Execute sQuery
	g_oConn.close

' Return XML object with Error node
set g_xmlReturn = xmlGetXMLDOMDoc()
Response.Write g_xmlReturn.xml

%>
