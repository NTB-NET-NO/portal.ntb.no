<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE='./include/inc_common.asp' -->

<%
	Dim iReportStatusID
	Dim iReportType
	Dim objReport 
	dim g_MSCSDefaultLocale
	Dim sRunDateTime
	dim dRequestXMLAsDict
	dim g_xmlReturn
	dim bResponse

	Const L_ReportExecutionError_ErrorMessage = "Error executing static report."
	Const L_ReportRenderingError_ErrorMessage = "Report rendering error."

	On Error Resume Next

	Set dRequestXMLAsDict = dGetRequestXMLAsDict()
	iReportStatusID = dRequestXMLAsDict("ReportStatusID")
	iReportType = dRequestXMLAsDict("iReportType")

	sRunDateTime = g_MSCSDataFunctions.datetime(Now(), g_MSCSDefaultLocale)

	If iReportType = cstr(Static_MDX) Then		' Always run static MDX reports sync
		RunSync	' Synchronous report execution
	Else
		' RunSync	' Synchronous report execution
		  RunAsync	' Asynchronous report execution
	End If

	Sub RunSync()
		bResponse = False

		''' Synchronous report execution
		Set objReport = CreateObject("Commerce.ReportRenderer")
		' bResponse = True on success (VB component)
		bResponse = objReport.Init(iReportStatusID & "|" & g_MSCSDataWarehouseSQLConnStr & "|" & g_MSCSDataWarehouseOLAPConnStr & "|||" & sRunDateTime)

		Set objReport = nothing
	
		' Return XML object with Error node
		set g_xmlReturn = xmlGetXMLDOMDoc()

		if Err.number <> 0 then
			AddErrorNode g_xmlReturn, "1", L_ReportExecutionError_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
		end if
	
		if not bResponse then
			AddErrorNode g_xmlReturn, "1", L_ReportExecutionError_ErrorMessage, L_ReportRenderingError_ErrorMessage
		End if

		Response.Write g_xmlReturn.xml
	End Sub
	
	Sub RunAsync()
		bResponse = True
	
		''' Asynchronous report execution
		Set objReport = CreateObject("Commerce.AsyncReportMgr")
		' bResponse = False on success (C++ component)
		bResponse = objReport.RunReport(iReportStatusID & "|" & g_MSCSDataWarehouseSQLConnStr & "|" & g_MSCSDataWarehouseOLAPConnStr & "|||" & sRunDateTime)

		Set objReport = nothing
	
		' Return XML object with Error node
		set g_xmlReturn = xmlGetXMLDOMDoc()

		if Err.number <> 0 then
			AddErrorNode g_xmlReturn, "1", L_ReportExecutionError_ErrorMessage, "<![CDATA[" & sGetScriptError(Err) & "]]>"
		end if
	
		if bResponse then
			AddErrorNode g_xmlReturn, "1", L_ReportExecutionError_ErrorMessage, L_ReportRenderingError_ErrorMessage
		End if

		Response.Write g_xmlReturn.xml
	End Sub
	
'	Dim a, fs
'	Set fs = Server.CreateObject("Scripting.FileSystemObject")
'	Set a = fs.CreateTextFile("C:\Program Files\Microsoft Commerce Server\documentation\testfile.txt", True)
'	a.WriteLine("Report executed, reporting object called")
'	a.WriteLine(iReportStatusID & "|" & g_MSCSDataWarehouseSQLConnStr & "|" & g_MSCSDataWarehouseOLAPConnStr & "|||" & sRunDateTime)
'	a.WriteLine("ReportType: " & iReportType)
'	a.WriteLine("Error number: " & err.number)
'	a.WriteLine("Error description: " & err.description)
'	a.WriteLine("Response code: " & bResponse)
'	a.Close					

%>
