<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/DBUtil.asp' -->
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='../include/BizDeskPaths.asp' -->
<!--#INCLUDE FILE='../include/BDHALUtil.htm' -->
<!--#INCLUDE FILE='include/analysisStrings.asp' -->

<HTML>
<HEAD>

<TITLE><% =L_SaveReport_HTMLTitle %></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>

    .visibleSection {display: block}
    .hiddenSection {display: none}
    .dlgwidth {width: 400px }
    .txtwidth {width: 350px } 

BODY
{
	PADDING: 15px;
	MARGIN: 0;
}
BUTTON
{
	WIDTH: 5em;
}
</STYLE>

<SCRIPT LANGUAGE='VBScript'>
<!--

	dim statechange
	statechange = 0
			
	sub window_onload()
		Dim sReportName, sReportDescription, sReportDefinition, bProtected
		Dim aDlgArgs
		
		window.dialogHeight = "400px"

		aDlgArgs = window.dialogArguments
		
		sReportName = aDlgArgs(0)
		sReportDescription = aDlgArgs(1)
		sReportDefinition = aDlgArgs(2)
		bProtected = aDlgArgs(3)

		SaveDlgForm("ReportName").value = sReportName
		SaveDlgForm("ReportDescription").value = sReportDescription
		SaveDlgForm("ReportDefinition").value = sReportDefinition

		If bProtected Then 
			document.all.SaveType(1).checked = 1		' Save As
			document.all.SaveType(0).disabled = 1		' Save 
			document.all.SaveType(1).disabled = 1		' Save As
		Else
			document.all.SaveType(0).checked = 1		' Save
		End If

		NewReportName.value = sReportName
		NewReportDescription.value = sReportDescription
		NewReportDefinition.value = sReportDefinition

		NewReportName.focus
	end sub

	Function bdcancel_OnClick()
		window.returnValue = -1
		window.close()
	end function

	sub bdOK_OnClick() 
		dim retval(3), aryInvalidRptName, aryInvalidRptDesc, aryInvalidRptDef, n

		'get the save type
		if document.all.SaveType(0).checked then
			retval(0) = document.all.SaveType(0).value
		else
			retval(0) = document.all.SaveType(1).value
		End if

		'get the report name
		retval(1) = Trim(NewReportName.value)
		retval(1) = Replace(retval(1), Chr(10), "")
		retval(1) = Replace(retval(1), Chr(13), "")

		if Len(retval(1)) = 0 then
			msgbox "<% =L_ReportNameEmpty1_Message %>", vbOKOnly, "<% =L_ReportNameMissing1_Message %>"
			NewReportName.focus
			exit sub		
		elseif Len(retval(1)) > 128 then
			msgbox "<% =L_ReportNameTooLong_Message %>", vbOKOnly
			NewReportName.focus
			exit sub
		End if

		aryInvalidRptName = Array("|","<",">","""","&")
		for n = 0 to ubound(aryInvalidRptName)			
			if Instr(1, retval(1), aryInvalidRptName(n)) <> 0 Then
				msgbox "<% =L_ReportNameInvalidChar_Message %>", vbOKOnly
				NewReportName.focus
				exit sub				
			end if
		next
		
		'get the report description
		retval(2) = Trim(NewReportDescription.value)
		retval(2) = Replace(retval(2), Chr(10), "")
		retval(2) = Replace(retval(2), Chr(13), "")
		if Len(retval(2)) = 0 then
			msgbox "<% =L_ReportDescriptionEmpty1_Message %>", VBOkOnly, "<%= L_ReportDescriptionMissing1_Message %>"
			NewReportDescription.focus
			exit sub
		elseif Len(retval(2)) > 128 then
			msgbox "<% =L_ReportDescriptionTooLong_Message %>", VBOkOnly
			NewReportDescription.focus
			exit sub
		end if
		
		aryInvalidRptDesc = Array("|","<",">","""","&")
		for n = 0 to ubound(aryInvalidRptDesc)			
			if Instr(1, retval(2), aryInvalidRptDesc(n)) <> 0 Then
				msgbox "<% =L_ReportDescriptionInvalidChar_Message %>", VBOkOnly
				NewReportDescription.focus
				exit sub				
			end if
		next

		'get the report definition
		retval(3) = Trim(NewReportDefinition.value)

		if Len(retval(3)) > 64000 then
			msgbox "<% =L_ReportDefinitionTooLong_Message %>", VBOkOnly
			NewReportDefinition.focus
			exit sub
		end if

		aryInvalidRptDef = Array("|","""","&")
		for n = 0 to ubound(aryInvalidRptDef)			
			if Instr(1, retval(3), aryInvalidRptDef(n)) <> 0 Then
				msgbox "<% =L_ReportDefinitionInvalidChar_Message %>", VBOkOnly
				NewReportDefinition.focus
				exit sub				
			end if
		next

		window.returnvalue = Join(retval, "|")
		window.close()
	end sub
	
	Sub FormChangeStatus()
		statechange = 1		
	End sub
	
	' Click handler for help task button
	sub OpenHelp(strHelpFile)
		if typename(winHelpWindow) = "HTMLWindow2" then winHelpWindow.close
		set winHelpWindow = window.open("<%= g_sBizDeskRoot %>docs/default.asp?helptopic=" & strHelpFile, "winHelpWindow", _
						 "height=500,width=700,status=no,toolbar=yes,menubar:no,resizable=yes")
	end sub
	
'-->
</SCRIPT>

</HEAD>
<BODY LANGUAGE='VBScript' ONLOAD='bdOK.focus()' ONKEYUP="if window.event.keyCode = 27 then bdCancel.click">

<xml id='efMeta'>
	<fields>
	    <text id='NewReportName' required="yes" maxlen='128' subtype='short'>
	        <name><%= L_ReportName1_HTMLText %></name>
	    </text>
	    <text id='NewReportDescription' required='yes' maxlen='128' subtype='short'>
	        <name><%= L_ReportDescription1_HTMLText %></name>
	    </text>
	    <text id='NewReportDefinition' required='no' subtype='long'>
	        <name><%= L_ReportDefinition1_HTMLText %></name>
	    </text>
	</fields>
</xml>
<xml id='efData'>
	<document/>
</xml>

<FORM action="" method='post' ID='SaveDlgForm' >
<input type="hidden" name="ReportName">
<input type="hidden" name="ReportDescription">
<input type="hidden" name="ReportDefinition">
</FORM>
<br>

<% =L_SaveReport_HTMLText %>
<table id=tblSaveType border='0'>
	<tr>
		<td width='15'>&nbsp;</td>
		<td><INPUT type='radio' tabindex=1 name='SaveType' VALUE='0' STYLE="BACKGROUND-COLOR: transparent"><% =L_Save_RadioButton %></td>
	</tr>
	<tr>
		<td width='15'>&nbsp;</td>
		<td><INPUT type='radio' name='SaveType' VALUE='1' STYLE="BACKGROUND-COLOR: transparent"><% =L_SaveAs_RadioButton %></td>
	</tr>
</table>
<br>

<table border=0 width="100%" style="TABLE-LAYOUT: fixed">
	<tr>
			<td><% =L_ReportName1_HTMLText %></td>
	</tr>
	<tr>
			<td>
			<DIV ID='NewReportName' CLASS='editField'
				MetaXML='efMeta' DataXML='efData'></DIV>
			</td>
	</tr>
	<tr>
			<td><% =L_ReportDescription1_HTMLText %></td>
	</tr>
	<tr>
			<td>
			<DIV ID='NewReportDescription' CLASS='editField'
				MetaXML='efMeta' DataXML='efData'></DIV>
			</td>
	</tr>
	<tr>
			<td><% =L_ReportDefinition1_HTMLText %></td>
	</tr>
	<tr>
			<td>
			<DIV ID='NewReportDefinition' CLASS='editField'
				MetaXML='efMeta' DataXML='efData'></DIV>
			</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<TABLE WIDTH="100%">
	<TR>
		<TD WIDTH="100%"></TD>
		<TD>
			<BUTTON ID='bdok' Tabindex=5 CLASS='bdbutton'><%= L_OK_Button %></BUTTON>
		</TD>
		<TD>
			<BUTTON ID='bdcancel' Tabindex=6 CLASS='bdbutton'><%= L_Cancel_Button %></BUTTON>
		</TD>
		<TD>
			<BUTTON ID='bdhelp' Tabindex=7 CLASS='bdbutton' STYLE='WIDTH:6em'
				LANGUAGE='VBScript'
				ONCLICK='openHelp "cs_bd_analysis_SLBF.htm"'><%= L_Help_Button %></BUTTON>
		</TD>
	</TR>
</TABLE>

</BODY>
</HTML>
