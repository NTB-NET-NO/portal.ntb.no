<!--#INCLUDE FILE='include/BDHeader.asp' -->
<!--#INCLUDE FILE='include/BizDeskPaths.asp' -->
<HTML>
<HEAD>
	<LINK REL='stylesheet' TYPE='text/css' HREF='include/bizdesk.css' ID='mainstylesheet'>
	<STYLE>
		BODY
		{
			MARGIN: 0;
			PADDING: 0;
			BACKGROUND-COLOR: white
		}
		#L_BizDeskClientSetup_Title
		{
			FONT-SIZE: 12pt;
			FONT-WEIGHT: bold;
		}
	</STYLE>
	<SCRIPT LANGUAGE=VBScript>
	<!--
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		' event handler routines:
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	context menu handler to turn off context menus
		sub document_onContextMenu()
			' -- disallow default context menu unless in entry fields
			if not (window.event.srcElement.tagName = "INPUT" or window.event.srcElement.tagName = "TEXTAREA") then
				window.event.returnValue = false
			end if
		end sub

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		'	BizDesk Framework:	select start handler to turn off selection except in inputs
		sub document_onSelectStart()
			' -- disallow selection of text on the page except in edit boxes
			Dim elSource
			set elSource = window.event.srcElement
			if elSource.tagName = "TEXTAREA" or _
				elSource.tagName = "INPUT" then
				if (elSource.readonly or elSource.disabled) then window.event.returnValue = false
			else
				window.event.returnValue = false
			end if
		end sub

		sub window_onload()
			parent.bdframe.rows = (document.body.scrollHeight) & ",*"
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY>
	<TABLE>
	<TR>
		<TD STYLE="BACKGROUND-COLOR: black">
			<IMG ID='BDLogoimg' SRC='assets/bdlogo.gif'HEIGHT='55' WIDTH='164' BORDER='0'>
		</TD>
		<TD>
			<div ID='L_BizDeskClientSetup_Title'>Business Desk Client Setup</div>
			<div ID='L_LaunchingBizDesk_Text'>Launching Business Desk client setup program...</div>
		</TD>
	</TR>
	</TABLE>
</BODY>
</HTML>
