<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="include/segviewerStrings.asp" -->
<!--#INCLUDE FILE="include/functions.inc" -->
<!--#INCLUDE FILE="../include/DialogUtil.asp" -->

<HTML>
<HEAD>
<TITLE><%= L_RenamingSegmentTitle_Text %></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<SCRIPT LANGUAGE='VBScript'>
Sub initDialog
    window.returnValue = Null

    txtNewName.Value = window.dialogArguments
	txtNewName.select
	
    btnOK.disabled = True
    btnOK.value = "<%= Quote(L_OK_Text) %>"  
End Sub

Sub OnOK
    Dim strNewModelName
    strNewModelName = Trim(txtNewName.Value)

    window.returnValue = strNewModelName
    window.close
End Sub

Sub OnPropertyChange
    Dim strNewModelName
   
    strNewModelName = Trim(txtNewName.Value)
    If Len(strNewModelName) = 0 Then
        btnOK.disabled = True
    Else
        btnOK.disabled = False
    End If 
End Sub

Sub OnCancel
    window.returnValue = Null
    window.close
End Sub

Sub OnHelp
    OpenHelp "cs_bd_segment_cpfn.htm"
End Sub

</SCRIPT>
</HEAD>

<BODY STYLE="margin:0px;padding:16px;overflow:hidden" onload="initDialog()">
<SPAN><%= L_NewName_Text %></SPAN>
<BR>
<INPUT ID='txtNewName' TYPE="TEXT" WIDTH=30 MAXLENGTH=128 onpropertychange='OnPropertyChange()'>
<P>
<INPUT STYLE="width: 7em" ID='btnOK' TYPE="SUBMIT" CLASS='bdbutton' onclick='OnOK()'>
<BUTTON STYLE="width: 7em" ID='btnCancel' CLASS='bdbutton' onclick='OnCancel()'><%= L_Cancel_Button %></BUTTON>
<BUTTON STYLE="width: 7em" ID='btnHelp' CLASS='bdbutton' onclick='OnHelp()'><%= L_Help_Button %></BUTTON>
</BODY>
</HTML>
