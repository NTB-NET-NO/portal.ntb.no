<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/segviewerStrings.asp' -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='include/functions.inc' -->

<%
Dim g_strModelName
Dim g_strDWConn
Dim g_strLMConn
Dim g_strSaveLabelsURLEncoded
Dim g_strXMLModelSummaryURL
Dim g_strDateCreated
Dim g_strModelSize

g_strLMConn = GetSiteConfigField("Campaigns", "connstr_db_Campaigns")
g_strModelName = Request.Form("ModelName")
g_strDWConn = Request.Form("ConnStr")
g_strDateCreated = Request.Form("DateCreated")
g_strModelSize = Request.Form("ModelSize")
g_strSaveLabelsURLEncoded = "include/savelabels.asp?ModelName=" & Server.URLEncode(g_strModelName) & "&ConnStr=" & Server.URLEncode(g_strDWConn)
g_strXMLModelSummaryURL = "include/getmodelsummary.asp?ModelName=" & Server.URLEncode(g_strModelName) & "&ConnStr=" & Server.URLEncode(g_strDWConn)

%>

<HTML>
<HEAD>
    <TITLE>BizDesk</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
SELECT {WIDTH: 200px}
DIV.XMLTree {behavior: url(/widgets/profilebldrHTC/XMLView.htc)}
</STYLE>
</HEAD>
<BODY id="myBody">
<SCRIPT LANGUAGE="VBScript">
Option Explicit

'LOCALIZABLE STRINGS TAKEN FROM SERVER SIDE SYMBOLS
const L_NoModelLoaded_Text = "<%= Quote(L_NoModelLoaded_Text) %>"
const L_LoadingModel_StatusBar = "<%= Quote(L_LoadingModel_StatusBar) %>"
const L_RenamingSegmentTitle_Text = "<%= Quote(L_RenamingSegmentTitle_Text) %>"
const L_SelectedSegment_Text = "<%= Quote(L_SelectedSegment_Text) %>"

const L_SummaryCaptionTempl_Text = "<%= Quote(L_SummaryCaptionTempl_Text) %>"

const L_CompareCaptionTempl_Text = "<%= Quote(L_CompareCaptionTempl_Text) %>"

const L_ModelSummaryTitle_Text = "<%= Quote(L_ModelSummaryTitle_Text) %>"

const L_ModelSummaryTempl_Text = "<%= Quote(L_ModelSummaryTempl_Text) %>"

const L_SaveConfirmationDialog_Text = "<%= Quote(L_SaveConfirmationDialog_Text) %>"
const L_ExportSegmentTitle_Text = "<%= Quote(L_ExportSegmentTitle_Text) %>"
const L_ErrorInRename_Text = "<%= Quote(L_ErrorInRename_Text) %>"
const L_ErrorInRenameBody_Text = "<%= Quote(L_ErrorInRenameBody_Text) %>"
const L_ErrorInRenameDescription_Text = "<%= Quote(L_ErrorInRenameDescription_Text) %>"
const L_ErrorOpeningModel_Text = "<%= Quote(L_ErrorOpeningModel_Text) %>"
const L_UnableToSave_ErrorMessage = "<%= Quote(L_UnableToSave_ErrorMessage) %>"
const L_ErrorOpeningModelBody_Text = "<%= Quote(L_ErrorOpeningModelBody_Text) %>"
const L_UnknownError_ErrorMessage = "<%= Quote(L_UnknownError_ErrorMessage) %>"
const L_ExportToListFailed_ErrorMessage = "<%= Quote(L_ExportToListFailed_ErrorMessage) %>"

Dim g_iCurrentSegID
Dim g_strCurrentPath
Dim g_strCurrentSegLabel
Dim g_iCurrentSegWeight
Dim g_strModelName
Dim g_strModelNameURL
Dim g_strDWConn
Dim g_strDWConnURL
Dim g_strLMConn
Dim g_strDateCreated
Dim g_strModelSize
Dim g_strSaveLabelsURLEncoded
Dim g_fIsSegmentGroup
Dim g_fGetModelSummaryFailed

g_fIsSegmentGroup = False
g_strModelName = "<%= Server.HTMLEncode(g_strModelName) %>"
g_strModelNameURL = "<%= Server.URLEncode(g_strModelName) %>"
g_strDWConn = "<%= Quote(g_strDWConn) %>"
g_strDWConnURL = "<%= Server.URLEncode(g_strDWConn) %>" 
g_strLMConn = "<%= Quote(g_strLMConn) %>"
g_strDateCreated = "<%= Server.HTMLEncode(g_strDateCreated) %>"
g_strModelSize = "<%= Server.HTMLEncode(g_strModelSize) %>"
g_strSaveLabelsURLEncoded = "<%= Quote(g_strSaveLabelsURLEncoded) %>"
g_fGetModelSummaryFailed = False

Sub UpdateButtons()
    If g_fGetModelSummaryFailed Then
        EnableTask "back"
        DisableTask "report"
        DisableTask "export"
        DisableTask "save"
        DisableTask "saveback"
    Else
        EnableTask "report"
        If g_fIsSegmentGroup Then
            DisableTask "export"
        Else
            EnableTask "export"
        End If
        EnableTask "back"
        If g_bDirty Then
            EnableTask "save"
	    EnableTask "saveback"
        End If
    End If
End Sub

Sub OnTreeDataComplete()
    'Select the top level node.

    Dim xmlTopNode
    If IsObject(xmlisSegmentTreeData.XMLDocument) Then
    	Set xmlTopNode =  xmlisSegmentTreeData.XMLDocument.documentElement
        If StrComp(xmlTopNode.nodeName, "ERROR", 1) = 0 Then
            g_fGetModelSummaryFailed = True
            UpdateButtons
	    ShowErrorDlg L_ErrorOpeningModel_Text, L_ErrorOpeningModelBody_Text, xmlTopNode.text, ERROR_ICON_ALERT
        Else
            tableMain.disabled = False
            btnRename.disabled = False
            compareComboBox.disabled = False
            divSegmentTree.disabled = False
    	    divSegmentTree.SelectItem xmlTopNode.GetAttribute("SEGID"), True
            call OnTreeSelectItem()
        End If
    End If

    UpdateButtons
End Sub

Sub SaveLabels()
    Dim strNewXML
    Dim oXMLResponse
    Dim arNewLabels
    Dim elBtnBack
    Dim xmlErrorNodes
    
    strNewXML = xmlisSegmentTreeData.XMLDocument.transformNode(xmlisSegmentLabelsXSL.XMLDocument)

    formSaveLabels.inputNewLabelsXML.Value = strNewXML
    formSaveLabels.action = g_strSaveLabelsURLEncoded 
    Set oXMLResponse = xmlPostFormViaXML(formSaveLabels)
    ' BUGBUG
    ' Actually the wait display isn't showing yet, but if
    ' I don't do this it will show forever???
    HideWaitDisplay()
    If oXMLResponse.documentElement Is Nothing Then         
        ShowErrorDlg "", L_UnableToSave_ErrorMessage, L_UnknownError_ErrorMessage, ERROR_ICON_ALERT
        UpdateButtons
        Exit Sub
    End If

    Set xmlErrorNodes = oXMLResponse.selectNodes("//ERROR")
    If xmlErrorNodes.length <> 0 Then
        ShowXMLNodeErrors(xmlErrorNodes)
        UpdateButtons
        Exit Sub
    End If

    EnableTask("back")
    g_bDirty = false
    If parent.g_bCloseAfterSave Then
	parent.g_bCloseAfterSave = false
	' -- save and exit chosen and no errors while saving
	set elBtnBack = elGetTaskBtn("back")
	If Not elBtnBack is nothing Then
	    If Not elBtnBack.disabled Then
                elBtnBack.click()
            End If
        End If
    End If
    UpdateButtons
End Sub

'
' We don't want to be able to deselect a segment,
' so if a deselect is attempted, reselect the last
' segment selected.
'
Sub OnTreeDeselectItem()
    divSegmentTree.SelectItem g_strCurrentPath, True
End Sub

Sub OnTreeSelectItem()
    Dim rgstrSelected
    Dim MyItem
    Dim OldItem
    Dim strResult
    Dim xmlNewDocHack
	Dim strComboBox

    If divSegmentTree.GetSelectedCount() = 1 Then
        rgstrSelected = divSegmentTree.GetSelectedItems()
        g_strCurrentPath = rgstrSelected(0)
        Set MyItem = getItemXMLFromPath(g_strCurrentPath)
        If IsObject(MyItem) And IsNull(MyItem.getAttribute("SELECTED")) Then
            ' Stupid hack because transformNode on a Element is broken
            ' in the version of MSXML.DLL on W2K RTM
            ' We create a new XML Document and copy the selected item node
            ' into its documentElement
            Set xmlNewDocHack = CreateObject("Microsoft.XMLDOM")
            xmlNewDocHack.async = false
            xmlNewDocHack.loadXML MyItem.xml
    
            strResult = xmlNewDocHack.transformNode(xmlisSegmentSummaryXSL.XMLDocument)
            Set xmlNewDocHack = Nothing
            divSummary.innerHTML = strResult
            g_strCurrentSegLabel = MyItem.getAttribute("LABEL")
            If IsNull(g_strCurrentSegLabel) Then
				g_strCurrentSegLabel = L_SelectedSegment_Text
			End if
			g_iCurrentSegID = MyItem.getAttribute("SEGID")
			g_iCurrentSegWeight = MyItem.getAttribute("WEIGHT")

            spanSummaryCaption.innerText = sFormatString(L_SummaryCaptionTempl_Text , Array(g_strCurrentSegLabel))
			spanCompareCaption.innerText = sFormatString(L_CompareCaptionTempl_Text , Array(g_strCurrentSegLabel))
            Set OldItem = xmlisSegmentTreeData.XMLDocument.selectSingleNode("//*[@SELECTED]")
            If Not OldItem Is Nothing Then
                OldItem.removeAttribute("SELECTED")
            End If
            MyItem.setAttribute "SELECTED", "True"
			' Populate the combo box.
            strComboBox = xmlisSegmentTreeData.XMLDocument.transformNode(xmlisSegmentComboBoxXSL.XMLDocument)
            divCompareComboBox.innerHTML = strComboBox
            divComparison.innerHTML = ""

            If StrComp(MyItem.nodeName, "SEGMENTGROUP", 1) = 0 Then
                g_fIsSegmentGroup = True
            Else
                g_fIsSegmentGroup = False
            End If 
        End If
    End If

    UpdateButtons
End Sub

Sub OnRename()
	Dim strNewLabel
	Dim xmlnodeCurrent
	Dim rgstrSelected
	Dim strComboBox
    Dim xmlItem
    Dim strXSL

    strNewLabel = window.showModalDialog("dlgrename.asp", g_strCurrentSegLabel, "dialogWidth:300px;dialogHeight:150px;status:no;help:no")
	If Not (IsEmpty(strNewLabel) Or IsNull(strNewLabel) Or (StrComp(strNewLabel, g_strCurrentSegLabel, 1) = 0)) Then
        ' Check to see if this label has already been used.
        strXSL = "//*[@LABEL $ieq$ '" & strNewLabel & "']"
        Set xmlItem = xmlisSegmentTreeData.XMLDocument.selectSingleNode(strXSL)
        If Not xmlItem Is Nothing Then
            ShowErrorDlg L_ErrorInRename_Text, L_ErrorInRenameBody_Text, L_ErrorInRenameDescription_Text, ERROR_ICON_ALERT
            Exit Sub
        End If
		rgstrSelected = divSegmentTree.GetSelectedItems()
		Set xmlnodeCurrent = getItemXMLFromPath(rgstrSelected(0))
		xmlnodeCurrent.setAttribute "LABEL", strNewLabel
		divSegmentTree.Refresh
		If IsNull(strNewLabel) Then
			strNewLabel = L_SelectedSegment_Text
		End If
		spanSummaryCaption.innerText = sFormatString(L_SummaryCaptionTempl_Text , Array(strNewLabel))
		spanCompareCaption.innerText = sFormatString(L_CompareCaptionTempl_Text , Array(strNewLabel))
		' Repopulate the combo box.
        strComboBox = xmlisSegmentTreeData.XMLDocument.transformNode(xmlisSegmentComboBoxXSL.XMLDocument)
        divCompareComboBox.innerHTML = strComboBox
        SetDirty L_SaveConfirmationDialog_Text
        UpdateButtons
        g_strCurrentSegLabel = strNewLabel
	End if
End Sub

Function getItemXMLFromPath(strItemPath)
    Dim xmlItem
    Dim rgstrPathParts
    Dim i
    Dim strXSL

    rgstrPathParts = Split(strItemPath, ".")

    For i = 0 To UBound(rgstrPathParts)
        strXSL = strXSL & "/*[@SEGID='" & rgstrPathParts(i) & "']"
    Next
    Set xmlItem = xmlisSegmentTreeData.XMLDocument.selectSingleNode(strXSL)
    'BUGBUG Error check here
    Set getItemXMLFromPath = xmlItem
End Function

Sub OnComparison (striCG2,strLabel2)
	Dim strURL
	Dim strTableHTML

	If striCG2="-2" or g_iCurrentSegID=striCG2 then
		divComparison.innerHTML = ""
	ElseIf Not IsNull(g_strModelName) Then
		strURL = "include/getclustergroupcompare.asp?ModelName=" & g_strModelNameURL & "&iCG1=" & g_iCurrentSegID & "&iCG2=" & striCG2 & "&ConnStr=" & g_strDWConnURL
		xmlisComparisonData.async = false
		xmlisComparisonData.load(strURL)
		strTableHTML = xmlisComparisonData.XMLDocument.transformNode (xmlisComparisonXSL.XMLDocument)
		divComparison.innerHTML = strTableHTML
		thCompareSelectedSegment.innerText = g_strCurrentSegLabel
		thCompareOtherSegment.innerText = strLabel2
	End if
End Sub

Sub ReportView()
	formReportSubmit.inputTree.Value = xmlisSegmentTreeData.XMLDocument.xml
	formReportSubmit.inputXSL1.Value = xmlisSegmentReport1XSL.XMLDocument.xml
	formReportSubmit.inputXSL2.Value = xmlisSegmentReport2XSL.XMLDocument.xml
    formReportSubmit.inputDateCreated.Value = g_strDateCreated
    formReportSubmit.inputModelSize.Value = g_strModelSize
    formReportSubmit.action = "include/report.asp?ModelName=" & g_strModelNameURL
	formReportSubmit.submit
   
    UpdateButtons 
End Sub

Sub ExportToList()
    Dim arListInfo
    Dim oXMLResponse
    Dim xmlErrorNodes

    arListInfo = window.showModalDialog("dlgexport.asp", L_ExportSegmentTitle_Text, "dialogWidth:300px;dialogHeight:200px;status:no;help:no")

    If IsArray(arListInfo) Then
        formExportToList.inputSegmentName.Value = g_strCurrentSegLabel
        formExportToList.inputListName.Value = arListInfo(0)
        formExportToList.inputConnStr.Value = g_strDWConn
        formExportToList.inputLMConnStr.Value = g_strLMConn
        formExportToList.inputMinProbability.Value = arListInfo(1)
        formExportToList.action = "include/exporttolist.asp?ModelName=" & g_strModelNameURL

        Set oXMLResponse = xmlPostFormViaXML(formExportToList)

        If oXMLResponse.documentElement Is Nothing Then         
            ShowErrorDlg "", L_ExportToListFailed_ErrorMessage, L_UnknownError_ErrorMessage, ERROR_ICON_ALERT
        End If

        Set xmlErrorNodes = oXMLResponse.selectNodes("//ERROR")
        If xmlErrorNodes.length <> 0 Then
            ShowXMLNodeErrors(xmlErrorNodes)
        End If
    End If

    UpdateButtons
End Sub
</SCRIPT>

<%
InsertEditTaskBar sFormatString(L_SegmentViewerTitleTempl_Text, Array(Server.HTMLEncode(g_strMOdelName))),  ""
%>
<FORM ID="formSaveLabels" NAME="formSaveLabels" METHOD="POST" ontask="SaveLabels()">
<INPUT TYPE="hidden" ID="inputNewLabelsXML" NAME="inputNewLabelsXML">
</FORM>
<FORM ID="formReportView" NAME="formReportView" METHOD="POST" ontask="ReportView()">
</FORM>
<FORM ID="formReportSubmit" NAME="formReportSubmit" METHOD="POST" TARGET="_blank">
<INPUT TYPE="hidden" ID="inputTree" NAME="inputTree">
<INPUT TYPE="hidden" ID="inputXSL1" NAME="inputXSL1">
<INPUT TYPE="hidden" ID="inputXSL2" NAME="inputXSL2">
<INPUT TYPE="hidden" ID="inputDateCreated" NAME="inputDateCreated">
<INPUT TYPE="hidden" ID="inputModelSize" NAME="inputModelSize">
</FORM>
<FORM ID="formExportToList" NAME="formExportToList" METHOD="POST" ontask="ExportToList()" TARGET="_blank">
<INPUT TYPE="hidden" ID="inputListName" NAME="inputListName">
<INPUT TYPE="hidden" ID="inputSegmentName" NAME="inputSegmentName">
<INPUT TYPE="hidden" ID="inputConnStr" NAME="inputConnStr">
<INPUT TYPE="hidden" ID="inputLMConnStr" NAME="inputLMConnStr">
<INPUT TYPE="hidden" ID="inputMinProbability" NAME="inputMinProbability">
</FORM>

<xml id="xmlisSegmentLabelsXSL">
    <?xml version="1.0"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
        <xsl:template match="/">
        <LABELS>
            <xsl:for-each select="//*[@SEGID]" order-by="@SEGID">
               <xsl:copy>
               <xsl:attribute name="LABEL"><xsl:value-of select="@LABEL" /></xsl:attribute>
               <xsl:attribute name="SEGID"><xsl:value-of select="@SEGID" /></xsl:attribute>
               </xsl:copy>
           </xsl:for-each>
        </LABELS>
        </xsl:template>
    </xsl:stylesheet>
</xml>

<xml id="xmlisSegmentTreeData" src="<%= g_strXMLModelSummaryURL %>" ondatasetcomplete="OnTreeDataComplete()" />
<xml id="xmlisComparisonData" />

<xml id="xmlisComparisonXSL">
	<?xml version='1.0'?>
	<xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">
		<xsl:script>SetLocale("en-us")</xsl:script>
		<xsl:template match="/">
			<TABLE ID="tblComparison" RULES="all" STYLE="width: 100%">
			<THEAD></THEAD>
			<TR STYLE="height: 30">
				<TH STYLE='text-align: left'><%= L_PropertyColumnHeading_Text %></TH>
				<TH STYLE='text-align: left'><%= L_ValueColumnHeading_Text %></TH>
				<TH WIDTH="100px" ID="thCompareSelectedSegment" STYLE='text-align: left'><%= L_SelectedSegmentColumnHeading_Text %></TH>
				<TH WIDTH="100px" ID="thCompareOtherSegment" STYLE='text-align: left'></TH>
			</TR>
			<xsl:for-each select="*/ATTRIBUTE">
			
				<xsl:if expr='Int(100*Abs(CDbl(getAttribute("SCORE")))) > 0'>
					<TR>
						<TD><xsl:value-of select="@NAME"/></TD>
						<xsl:choose>
							<xsl:when expr='getAttribute("OP") = "EQ"'>
								<TD><xsl:value-of select="@VALUE"/></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "LT"'>
								<TD><%=sFormatString(L_LessThanTempl_Text , Array("<xsl:value-of select='@VALUE'/>")) %></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "GT"'>
								<TD><%=sFormatString(L_GreaterThanTempl_Text , Array("<xsl:value-of select='@VALUE'/>"))%></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "BTW"'>
								<TD><%= sFormatString(L_BetweenTempl_Text , Array("<xsl:value-of select='@VALUELOW'/>"  ,  "<xsl:value-of select='@VALUEHIGH'/>"))  %></TD>
							</xsl:when>
							<xsl:otherwise>
								<TD></TD>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
							<xsl:when expr='CDbl(getAttribute("SCORE")) > 0'>
								<TD STYLE="text-align:right">
									<DIV>
										<xsl:attribute name="STYLE">
											<xsl:eval >
												"background-color: yellow; width: " + CStr(Int(100*CDbl(getAttribute("SCORE"))))
											</xsl:eval>
										</xsl:attribute>
									</DIV>
								</TD>
								<TD>
								</TD>
							</xsl:when>
							<xsl:otherwise>
								<TD>
								</TD>
								<TD STYLE="text-align:left">
									<DIV>
										<xsl:attribute name="STYLE">
											<xsl:eval >
												"background-color: blue; width: " + CStr(Int(-100*CDbl(getAttribute("SCORE"))))
											</xsl:eval>
										</xsl:attribute>
									</DIV>
								</TD>
							</xsl:otherwise>
						</xsl:choose>
					</TR>
				</xsl:if>
			</xsl:for-each>
			</TABLE>
        </xsl:template>
    </xsl:stylesheet>
</xml>

<xml id="xmlisSegmentSummaryXSL">
    <?xml version='1.0'?>
    <xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">
	<xsl:script>SetLocale("en-us")</xsl:script>
        <xsl:template match="/">
			<TABLE ID="tblSummary" RULES="all" STYLE="width: 100%">
			<THEAD></THEAD>
			<TR STYLE="height: 30">
				<TH STYLE='text-align: left'><%= L_PropertyColumnHeading_Text %></TH>
				<TH STYLE='text-align: left'><%= L_ValueColumnHeading_Text %></TH>
				<TH STYLE='text-align: left' WIDTH="100"><%= L_ScoreColumnHeading_Text %></TH>
			</TR>
			<xsl:for-each select="*/ATTRIBUTE">				
				<xsl:if expr='Int(100*CDbl(getAttribute("SCORE"))) > 0'>
					<TR>
						<TD><xsl:value-of select="@NAME"/></TD>
						<xsl:choose>
							<xsl:when expr='getAttribute("OP") = "EQ"'>
								<TD><xsl:value-of select="@VALUE"/></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "LT"'>
								<TD><%=sFormatString(L_LessThanTempl_Text  , Array("<xsl:value-of select='@VALUE'/>")) %></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "GT"'>
								<TD><%=sFormatString (L_GreaterThanTempl_Text  , Array("<xsl:value-of select='@VALUE'/>")) %></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "BTW"'>
								<TD><%=sFormatString (L_BetweenTempl_Text  , Array("<xsl:value-of select='@VALUELOW'/>" , "<xsl:value-of select='@VALUEHIGH'/>"))  %></TD>
							</xsl:when>
							<xsl:otherwise>
								<TD></TD>
							</xsl:otherwise>
						</xsl:choose>
						<TD> 
							<DIV>
								<xsl:attribute name="STYLE">
									<xsl:eval >
										"background-color: red; width: " + CStr(Int(100*CDbl(getAttribute("SCORE"))))
									</xsl:eval>
								</xsl:attribute>
							</DIV>
						</TD>
					</TR>
				</xsl:if>
			</xsl:for-each>
			</TABLE>
        </xsl:template>
    </xsl:stylesheet>
</xml>

<xml id="xmlisSegmentReport1XSL">
    <?xml version='1.0'?>
    <xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">
	<xsl:script>SetLocale("en-us")</xsl:script>
        <xsl:template match="/">
			<DIV STYLE="font-face: Times New Roman; font-size:12pt">
				<xsl:apply-templates />
			</DIV>
        </xsl:template>
        <xsl:template match="SEGMENTGROUP">
			<DIV STYLE="height: 25">
				<A><xsl:attribute name="HREF">#SEG<xsl:value-of select="@SEGID"/></xsl:attribute>
					<strong><xsl:value-of select="@LABEL"/></strong>
				</A>
				(<xsl:eval >Round(CDbl(getAttribute("WEIGHT"))*100)</xsl:eval>%)
			</DIV>

			<DIV STYLE="margin-left: 3%">
				<xsl:apply-templates />
			</DIV>

        </xsl:template>

        <xsl:template match="SEGMENT">
			<DIV STYLE="height: 25">
				<A><xsl:attribute name="HREF">#SEG<xsl:value-of select="@SEGID"/></xsl:attribute>
					<xsl:value-of select="@LABEL"/>
				</A>
				(<xsl:eval >Round(CDbl(getAttribute("WEIGHT"))*100)</xsl:eval>%)
			</DIV>
        </xsl:template>
    </xsl:stylesheet>
</xml>

<xml id="xmlisSegmentReport2XSL">
    <?xml version='1.0'?>
    <xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">
	<xsl:script>SetLocale("en-us")</xsl:script>
        <xsl:template match="/">
			<DIV STYLE="font-face: Times New Roman; font-size:12pt">
				<xsl:apply-templates />
			</DIV>
        </xsl:template>
        <xsl:template match="SEGMENTGROUP">
			<DIV STYLE="height: 10"></DIV>
			<TABLE RULES="all" STYLE="width: 100%; font-face:Arial; font-size:11pt;" BORDER="0" CELLSPACING="0">
			<THEAD></THEAD>
			<TR>
			<TH bgcolor='#C0C0C0' STYLE='text-align: left;'>
				<A><xsl:attribute name="NAME">SEG<xsl:value-of select="@SEGID"/></xsl:attribute>
					<xsl:value-of select="@LABEL"/>
				</A>
			</TH>
			<TH bgcolor='#C0C0C0' STYLE='text-align: right;'><%= L_MembershipPercentageHeading_Text %><xsl:eval >Round(CDbl(getAttribute("WEIGHT"))*100)</xsl:eval>%
			</TH>
			</TR>
			</TABLE>
			<DIV STYLE="height: 5"></DIV>
			<TABLE RULES="all" STYLE="width: 100%">
			<THEAD></THEAD>
			<TR STYLE="height: 30">
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold'><%= L_PropertyColumnHeading_Text%></TH>
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold' WIDTH="25%"><%= L_ValueColumnHeading_Text %></TH>
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold' WIDTH="100"><%= L_ScoreColumnHeading_Text %></TH>
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold' WIDTH="100"><%= L_ScoreValueColumnHeading_Text %></TH>
			</TR>
			<xsl:for-each select="ATTRIBUTE">
				<xsl:if expr='Int(100*CDbl(getAttribute("SCORE"))) > 0'>
					<TR>
						<TD STYLE="font-face:Times New Roman; font-size:10pt"><xsl:value-of select="@NAME"/></TD>
						<xsl:choose>
							<xsl:when expr='getAttribute("OP") = "EQ"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><xsl:value-of select="@VALUE"/></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "LT"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><%=sFormatString(L_LessThanTempl_Text  , Array("<xsl:value-of select='@VALUE'/>")) %></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "GT"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><%=sFormatString(L_GreaterThanTempl_Text  , Array("<xsl:value-of select='@VALUE'/>"))  %></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "BTW"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><%=sFormatString(L_BetweenTempl_Text  , Array("<xsl:value-of select='@VALUELOW'/>" , "<xsl:value-of select='@VALUEHIGH'/>"))  %></TD>
							</xsl:when>
							<xsl:otherwise>
								<TD></TD>
							</xsl:otherwise>
						</xsl:choose>
						<TD> 
							<DIV>
								<xsl:attribute name="STYLE">
								<xsl:eval >
									"background-color: red; width: " + CStr(Int(100*CDbl(getAttribute("SCORE"))))
								</xsl:eval>
							</xsl:attribute>
							</DIV>
						</TD>
						<TD STYLE="font-face:Times New Roman; font-size:10pt"><xsl:eval >CStr(Int(100*CDbl(getAttribute("SCORE"))))</xsl:eval></TD>
					</TR>
				</xsl:if>
			</xsl:for-each>
			</TABLE>

			<xsl:apply-templates />

        </xsl:template>

        <xsl:template match="SEGMENT">
			<DIV STYLE="height: 10"></DIV>
			<TABLE RULES="all" STYLE="width: 100%; font-face:Arial; font-size:11pt;" BORDER="0" CELLSPACING="0">
			<THEAD></THEAD>
			<TR>
			<TH bgcolor='#C0C0C0' STYLE='text-align: left;'>
				<A><xsl:attribute name="NAME">SEG<xsl:value-of select="@SEGID"/></xsl:attribute>
					<xsl:value-of select="@LABEL"/>
				</A>
			</TH>
			<TH bgcolor='#C0C0C0' STYLE='text-align: right;'><%= L_MembershipPercentageHeading_Text %><xsl:eval >Round(CDbl(getAttribute("WEIGHT"))*100)</xsl:eval>%
			</TH>
			</TR>
			</TABLE>
			<DIV STYLE="height: 5"></DIV>
			<TABLE RULES="all" STYLE="width: 100%">
			<THEAD></THEAD>
			<TR STYLE="height: 30;">
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold'><%= L_PropertyColumnHeading_Text %></TH>
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold' WIDTH="25%"><%= L_ValueColumnHeading_Text %></TH>
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold' WIDTH="100"><%= L_ScoreColumnHeading_Text %></TH>
				<TH STYLE='text-align: left; font-face:Times New Roman; font-size:10pt; font-weight:bold' WIDTH="100"><%= L_ScoreValueColumnHeading_Text %></TH>
			</TR>
			<xsl:for-each select="ATTRIBUTE">			
				<xsl:if expr='Int(100*CDbl(getAttribute("SCORE"))) > 0'>
					<TR>
						<TD STYLE="font-face:Times New Roman; font-size:10pt"><xsl:value-of select="@NAME"/></TD>
						<xsl:choose>
									<xsl:when expr='getAttribute("OP") = "EQ"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><xsl:value-of select="@VALUE"/></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "LT"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><%=sFormatString(L_LessThanTempl_Text  , Array("<xsl:value-of select='@VALUE'/>"))  %></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "GT"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><%=sFormatString(L_GreaterThanTempl_Text , Array("<xsl:value-of select='@VALUE'/>"))  %></TD>
							</xsl:when>
							<xsl:when expr='getAttribute("OP") = "BTW"'>
								<TD STYLE="font-face:Times New Roman; font-size:10pt"><%=sFormatString(L_BetweenTempl_Text , Array("<xsl:value-of select='@VALUELOW'/>"  , "<xsl:value-of select='@VALUEHIGH'/>"))  %></TD>
							</xsl:when>
							<xsl:otherwise>
								<TD></TD>
							</xsl:otherwise>
						</xsl:choose>
						<TD>
							<DIV>
								<xsl:attribute name="STYLE">
									<xsl:eval >
										"background-color: red; width: " + CStr(Int(100*CDbl(getAttribute("SCORE"))))
									</xsl:eval>
								</xsl:attribute>
							</DIV>
						</TD>
						<TD STYLE="font-face:Times New Roman; font-size:10pt"><xsl:eval >CStr(Int(100*CDbl(getAttribute("SCORE"))))</xsl:eval></TD>
					</TR>
				</xsl:if>
			</xsl:for-each>
			</TABLE>
        </xsl:template>
    </xsl:stylesheet>
</xml>

<xml id="xmlisSegmentTreeXSL">
    <?xml version='1.0'?>
    <xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">
        <xsl:script><![CDATA[
            Function WeightToPercent(strWeight)
                SetLocale("en-us")
				WeightToPercent = CStr(Round(CDbl(strWeight)*100)) & "%"
            End Function
        ]]>
        </xsl:script>
        <xsl:template match="/">
            <DIV TABINDEX='0' ID="xvRoot">
                <xsl:apply-templates />
            </DIV>
        </xsl:template>

        <!-- Handle Segment -->
        <xsl:template match="SEGMENT">
            <DIV TABINDEX='0' CLASS='clsXVItem'>
                <xsl:attribute name="ID"><xsl:value-of select="@SEGID"/></xsl:attribute>
					<xsl:attribute name="TITLE"><xsl:value-of select="@LABEL"/></xsl:attribute>
                <NOBR>
                <SPAN ID='xvMarker'><xsl:comment>foo</xsl:comment></SPAN>
				<xsl:value-of select="@LABEL"/>
				(<xsl:eval>WeightToPercent(getAttribute("WEIGHT"))</xsl:eval>)
                </NOBR>
            </DIV>
        </xsl:template>

        <!-- Handle Segment Group -->
        <xsl:template match="SEGMENTGROUP">
            <DIV TABINDEX='0' CLASS='clsXVGroup'>
                <xsl:attribute name="ID"><xsl:value-of select="@SEGID"/></xsl:attribute>
				<xsl:attribute name="TITLE"><xsl:value-of select="@LABEL"/></xsl:attribute>
                <NOBR>
                <SPAN ID='xvMarker'>4</SPAN>
				<xsl:value-of select="@LABEL"/>
				(<xsl:eval>WeightToPercent(getAttribute("WEIGHT"))</xsl:eval>)
                </NOBR>

                <DIV ID="xvGroup" CLASS="clsXVClosed">
                    <xsl:apply-templates />
                </DIV>
            </DIV>
        </xsl:template>
    </xsl:stylesheet>
</xml>

<xml id="xmlisSegmentComboBoxXSL">
    <?xml version='1.0'?>
    <xsl:stylesheet language="VBScript" xmlns:xsl="http://www.w3.org/TR/WD-xsl">
        <xsl:template match="/">
            <SELECT ID="compareComboBox" TABINDEX="4" OnChange="OnComparison (this.options[this.selectedIndex].value, this.options[this.selectedIndex].innerText)">
				<OPTION VALUE="-2"><%= L_NoSegment_Text %></OPTION>
				<xsl:apply-templates />
			</SELECT>
        </xsl:template>

        <!-- Handle Segment -->
        <xsl:template match="SEGMENT">
            <xsl:choose>
                <xsl:when test="@SELECTED" />
                <xsl:otherwise>
                    <OPTION>
                        <xsl:attribute name="VALUE">
                            <xsl:value-of select="@SEGID"/>
                        </xsl:attribute>
                        <xsl:value-of select="@LABEL"/>
                    </OPTION>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:template>

        <!-- Handle Segment Group -->
        <xsl:template match="SEGMENTGROUP">
            <xsl:choose>
                <xsl:when test="@SELECTED" />
                <xsl:otherwise>
                    <OPTION>
                        <xsl:attribute name="VALUE">
                            <xsl:value-of select="@SEGID"/>
                        </xsl:attribute>
                        <xsl:value-of select="@LABEL"/>
                    </OPTION>
                </xsl:otherwise>
            </xsl:choose>
			<xsl:apply-templates />
		</xsl:template>
	</xsl:stylesheet>
</xml>

<TABLE DISABLED ID="tableMain" width="100%" STYLE="height: 100%; table-layout: fixed">
	<TR>
		<TD width="20%">
			<TABLE width="100%" STYLE="height: 100%; table-layout: fixed">
				<TR STYLE="height: 5%">
					<TD>
						<SPAN STYLE="font-weight: bold"><%= L_SegmentsCaption_Text %></SPAN>
					</TD>
				</TR>
				<TR STYLE="height: 90%">
					<TD>
						<DIV DISABLED ID="divSegmentTree" CLASS="XMLTree" XMLDataID="xmlisSegmentTreeData" XSLViewID="xmlisSegmentTreeXSL" onSelectItem="OnTreeSelectItem()" onDeSelectItem="OnTreeDeselectItem()" TABINDEX="0"
							STYLE="width: 100%; border: 2px inset; background-color: white; height: 100%; overflow: auto"></DIV>
					</TD>
				</TR>
				<TR STYLE="height: 5%">
					<TD>
						<BUTTON DISABLED ID="btnRename" OnClick="OnRename()" TABINDEX="2"><%= L_Rename_Button %></BUTTON>
					</TD>
				</TR>
			</TABLE>
		</TD>
		<TD width="80%">
			<TABLE width="100%" STYLE="height: 100%; table-layout: fixed">
				<TR STYLE="height: 5%">
					<TD>
						<SPAN ID="spanSummaryCaption" STYLE="font-weight: bold; text-align: left">Summary of Selected Segment:</SPAN>
					</TD>
				</TR>
				<TR STYLE="height: 45%">
					<TD>
						<DIV ID="divSummary" STYLE="width: 100%; height: 100%; background-color: white; overflow: auto" TABINDEX="3"></DIV>
					</TD>
				</TR>
				<TR STYLE="height: 10%">
					<TD>
						<SPAN ID="spanCompareCaption" STYLE="font-weight: bold; text-align: left">Compare Selected Segment To:</SPAN>
						<DIV STYLE="margin-top: 4px" ID="divCompareComboBox"><SELECT DISABLED ID="compareComboBox"></SELECT></DIV>
					</TD>
				</TR>
				<TR STYLE="height: 40%">
					<TD>
						<DIV ID="divComparison" STYLE="width: 100%; height: 100%; background-color: white; overflow: auto" TABINDEX="5"></DIV>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>
