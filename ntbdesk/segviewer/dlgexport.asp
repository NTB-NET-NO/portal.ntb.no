<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/DialogUtil.asp" -->
<!--#INCLUDE FILE="include/segviewerStrings.asp" -->
<!--#INCLUDE FILE="include/functions.inc" -->
<!--#INCLUDE FILE="include/constants.inc" -->

<HTML>
<HEAD>
<TITLE><%= L_ExportSegmentTitle_Text %></TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='/widgets/bizdesk.css' ID='mainstylesheet'>

<SCRIPT LANGUAGE='VBScript'>

Sub initDialog
	window.returnValue = vbNull
    btnOK.disabled = True
    btnOK.value = "<%= Quote(L_OK_TEXT) %>"
    txtMinProb.Value = "<%= g_MSCSDataFunctions.Float(cStr(DEFAULT_EXPORT_MINPROB)) %>"
End Sub

Sub OnOK
    Dim arListInfo

    If Validate Then
		txtMinProb.select
		Exit Sub
	End If
   
    arListInfo = Array(Trim(txtSegmentName.Value), Trim(txtMinProb.Value))
    window.returnValue = arListInfo
    window.close
End Sub

Sub OnCancel
    window.returnValue = Null
    window.close
End Sub

Sub OnPropertyChange
    Dim strListName

    strListName = Trim(txtSegmentName.Value)
    If Len(strListName) = 0 Then
        btnOK.disabled = True
    Else
        btnOK.disabled = False
    End If
End Sub

Function Validate()
    Dim strProb
    Dim dblProb
    Dim locale 

    locale = <%= Application("MSCSDefaultLocale") %>
    SetLocale(locale)

    strProb = Trim(txtMinProb.Value)

    If Not IsNumeric(strProb) Then
        alert "<%= Quote(L_ProbRangeError_Text) %>"
        Validate = True
        Exit Function
    End If

    dblProb = CDbl(strProb)

    txtMinProb.Value = CStr(dblProb)

    If dblProb < 0 Or dblProb > 1 Then
        alert "<%= Quote(L_ProbRangeError_Text) %>"
        Validate = True
        Exit Function
    End If
    Validate = False
End Function

Sub OnHelp
    OpenHelp "cs_bd_segment_zfjv.htm"
End Sub

</SCRIPT>
</HEAD>

<BODY STYLE="margin:0px;padding:16px;overflow:hidden" onload="initDialog()">
<SPAN><%= L_EnterListName_Text %></SPAN>
<BR>
<INPUT ID='txtSegmentName' onpropertychange='OnPropertyChange()' TYPE="TEXT" WIDTH=30 MAXLENGTH=64>
<P>
<SPAN><%= L_MinimumProbability_Text %></SPAN>
<BR>
<INPUT ID='txtMinProb' TYPE="TEXT" WIDTH=5 MAXLENGTH=5>
<P>
<INPUT STYLE="width: 7em" ID='btnOK' class='bdbutton' TYPE="SUBMIT" onclick='OnOK()'>
<BUTTON STYLE="width: 7em" ID='btnCancel' class='bdbutton' onclick='OnCancel()'><%= L_Cancel_Button %></BUTTON>
<BUTTON STYLE="width: 7em" ID='btnHelp' class='bdbutton' onclick='OnHelp()'><%= L_Help_Button %></BUTTON>
</BODY>
</HTML>
