<!--#INCLUDE FILE="../../include/BDHeader.asp" -->
<!-- #INCLUDE FILE='segviewerStrings.asp' -->
<!--#INCLUDE FILE='../../include/ASPUtil.asp' -->
<%
	Dim strXMLTree
	Dim strXMLXSL1
	Dim strXMLXSL2

	Dim xmlTree
	Dim xmlXSL1
	Dim xmlXSL2

    Dim strModelName
    Dim strDateCreated
    Dim strModelSize
    
	strXMLTree = Request.Form("inputTree")
	strXMLXSL1 = Request.Form("inputXSL1")
	strXMLXSL2 = Request.Form("inputXSL2")
	strDateCreated = Request.Form("inputDateCreated")
	strModelSize = Request.Form("inputModelSize")
    strModelName = Request.QueryString("ModelName")
	
	Set xmlTree = Server.CreateObject("Microsoft.XMLDOM")
	Set xmlXSL1 = Server.CreateObject("Microsoft.XMLDOM")
	Set xmlXSL2 = Server.CreateObject("Microsoft.XMLDOM")

	xmlTree.loadXML strXMLTree
	xmlXSL1.loadXML strXMLXSL1
	xmlXSL2.loadXML strXMLXSL2

	Response.Write "<HTML><HEAD><TITLE>" & L_ModelSummaryTitle_Text & "</TITLE></HEAD><BODY>"
	Response.Write "<DIV STYLE='text-align:center; font-face:Arial; font-size:18pt; font-weight:bold'>" & sFormatString(L_ModelSummaryTempl_Text ,Array( "<I>" & Server.HTMLEncode(strModelName) & "</I>")) & "</DIV>"
	Response.Write "<BR><BR>"
	Response.Write "<TABLE STYLE=""width: 100%; font-face:Arial; font-size:12pt;"" Border=0><TH style=""border-bottom:2 solid #000000"" bgcolor=""#C0C0C0"" ALIGN=left>" & L_ReportSummary_Text & "</TH></TABLE>"
	Response.Write "<TABLE STYLE=""width: 80%; font-face:Arial; font-size:12pt;"" Border=0>"
	Response.Write "<TR><TD>" & L_PreparedOnHeader_Text & "</TD><TD>" & Cstr(CDate(strDateCreated)) & "</TD><TR>"
	Response.Write "<TR><TD>" & L_ModelNameHeader_Text & "</TD><TD>" & Server.HTMLEncode(strModelName) & "</TD><TR>"
	Response.Write "<TR><TD>" & L_ModelSizeHeader_Text & "</TD><TD>" & strModelSize & L_Bytes_Text & "</TD><TR>"
	Response.Write "</TABLE>"
	Response.Write "<BR>"
	Response.Write "<TABLE STYLE=""width: 100%"" Border=0><TH style=""border-bottom:2 solid #000000; font-face:Arial; font-size:12pt;"" bgcolor=""#C0C0C0"" ALIGN=left>" & L_TableOfContents_Text & "</TH></TABLE>"
	Response.Write xmlTree.transformNode(xmlXSL1)
	Response.Write xmlTree.transformNode(xmlXSL2)
	Response.Write "</BODY></HTML>"

%>
