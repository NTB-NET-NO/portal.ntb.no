<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!-- #INCLUDE FILE='constants.inc' -->
<!-- #INCLUDE FILE='segviewerStrings.asp' -->
<!-- #INCLUDE FILE='functions.inc' -->
<%
'
' Main
'

'
' Parse script parameters
'
Dim strModelName   ' Model name to load
Dim nDepth         ' Depth of tree return in XML
Dim nAttributes    ' Number of attributes per segment to return in XML
Dim strXML         ' XML to return
Dim strDWConn      ' Connection string for Data warehouse

strModelName = Request.QueryString("ModelName")
strDWConn = Request.QueryString("ConnStr")

nDepth =  Request.QueryString("Depth")
If nDepth = "" Then
    nDepth = DEFAULT_TREE_DEPTH
End If

nAttributes = Request.QueryString("Attributes")
If nAttributes = "" Then
    nAttributes = DEFAULT_ATTRIBUTE_LIMIT
End If

'
' Return the summary
'

strXML = GetModelSummary(strModelName, strDWConn, nDepth, nAttributes)
Response.Write strXML

%>
