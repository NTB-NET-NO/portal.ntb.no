<!--#INCLUDE FILE="../../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE="../../include/HTTPXMLUtil.asp" -->
<!-- #INCLUDE FILE='constants.inc' -->
<!-- #INCLUDE FILE="segviewerStrings.asp" -->
<%

'
'
' LabelsXMLToArray
'
' PARAMETERS:
'
' xmlDoc - Input XML Document in the following format:
'
'   <LABELS>
'   <SEGMENT SEGID="10" LABEL="Foo" />
'   <SEGMENTGROUP SEGID="14" LABEL="Bar" />
'   ...
'   </LABELS>
'
' RETURNS:
'
' An array of strings which can passed to the SegmentLabels method
' on the Predictor.Service object's IPredictorService interface.
'
'
Function LabelXMLToArray(xmlDoc)
    Dim arNewLabels()
    Dim nodeList
    Dim nLabels
    Dim i
    Dim SegID

    Set nodeList = xmlDoc.selectNodes("//*[@SEGID]")
    nLabels = nodeList.length
    ReDim arNewLabels(nLabels - 1)
    For i = 0 to nLabels - 1
        SegID = nodeList.item(i).getAttribute("SEGID")
        arNewLabels(SegID) = nodeList.item(i).getAttribute("LABEL")
    Next

    LabelXMLToArray = arNewLabels
End Function

Dim oPredServ
Dim strModelName
Dim xmlDoc
Dim strDWConn
Dim strHost
Dim xmlReturn
Dim dArguments
Dim strXMLNewLabels

Set dArguments = dGetRequestXMLAsDict()
Set xmlReturn = xmlGetXMLDOMDoc()

'
' Parse script URL arguments
'
strModelName = Request.QueryString("ModelName")
strDWConn = Request.QueryString("ConnStr")
strHost = Request.QueryString("Host")
strXMLNewLabels  = dArguments("inputNewLabelsXML")

If IsEmpty(strHost) Then
    strHost = Application("MSCSPredictorServiceHostName")
End If

'
' Load the Request Body into an XML Document
'
Set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")
xmlDoc.async = false
xmlDoc.loadXML strXMLNewLabels

'
' Connect to the Predictor Service
'
Set oPredServ = CreateObject("Commerce.PredictorSiteAdmin", strHost)
oPredServ.Init Application("MSCSSiteName"), strDWConn

'
' Set the new labels
'

On Error Resume Next 
oPredServ.SegmentLabels(strModelName, DEFAULT_TREE_DEPTH) = LabelXMLToArray(xmlDoc)

If Err.Number <> 0 Then
    AddErrorNode xmlReturn, Err.Number, L_UnableToSave_ErrorMessage, "<![CDATA[" & Err.Description & "]]>"
End If

On Error GoTo 0

'
' The model has changed, so we need to invalidate the cached version.
'
Set Session("CurrentSegmentModel") = Nothing
Session("CurrentSegmentModel") = Empty

Response.Write xmlReturn.xml

%>
