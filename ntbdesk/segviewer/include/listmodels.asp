<%

Dim g_bPredictorNotInstalled

'
' List models of a particular type
' known by the Predictor service running on the host
' strPredictorHost
'
' Returns: xml document containing info for models of a given type
'
Function ListModels(strPredictorHost, nType)

    Dim objPredServ
    Dim dictModelInfo
    Dim slModels
    Dim xmlModels
    Dim strKey
    Dim nodeTop
    Dim nodeRecord
    Dim nodeProp
    Dim nodeText
    Dim strElementName
    Dim strValueText
    Dim strConn
    Dim strResourceName
    Dim objGlobalConfig

    Set xmlModels = Server.CreateObject("Microsoft.XMLDOM")
    Set objGlobalConfig = Server.CreateObject("Commerce.GlobalConfig")
    Set nodeTop = xmlModels.createElement("document")
    Set xmlModels.documentElement = nodeTop

    Set ListModels = xmlModels


    On Error Resume Next
    Err.Clear

    Set objPredServ = CreateObject("Commerce.PredictorServiceAdmin", strPredictorHost)
    objGlobalConfig.Initialize
    
    If Err.Number <> 0 Then
        g_bPredictorNotInstalled = True
        Exit Function 
    End If

    g_bPredictorNotInstalled = False

    Set slModels = objPredServ.slModels
    
    If Err.Number <> 0 Then
        SetError "", L_ErrorListingModels_ErrorMessage, sGetScriptError(Err), ERROR_ICON_ALERT
        Err.Clear
        Exit Function
    End If

    ReDim arstrModels(slModels.Count)
    For Each dictModelInfo In slModels
        If (IsNull(nType) Or (nType = dictModelInfo("ModelType"))) Then
            Set nodeRecord = xmlModels.createElement("record")
            For Each strKey In dictModelInfo
                If StrComp(strKey, "DWResourceName") = 0 Then
                    ' Lookup connection string for this resource.
                    strElementName = "ConnStr"
                    strResourceName = dictModelInfo(strKey)
                    strConn = objGlobalConfig.Fields(strResourceName).Value.Fields("connstr_db_dw")
                    If IsEmpty(strConn) Then
                        ' Could be an external DB that's not the Date Warehouse.
                        strConn = objGlobalConfig.Fields(strResourceName).Value.Fields("connstr_db_predictor_external")
                    End If
                    strValueText = strConn
                Else
                    strElementName = strKey
                    strValueText = CStr(dictModelInfo(strKey))
                End If
                Set nodeProp = xmlModels.createElement(strElementName)
                Set nodeText = xmlModels.createTextNode(strValueText)
                nodeProp.appendChild(nodeText)
                nodeRecord.appendChild(nodeProp)
            Next
            nodeTop.appendChild(nodeRecord)
        End If
    Next

End Function

%>
