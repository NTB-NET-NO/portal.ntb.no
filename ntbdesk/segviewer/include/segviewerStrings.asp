<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

'LOCALIZABLE STRINGS
const L_SegmentViewerTitle_Text = "Segment Viewer"
const L_SegmentViewerTitleTempl_Text = "Segment Viewer: %1"
const L_ModelSummaryTitle_Text = "Model Summary"

const L_ModelSummaryTempl_Text = "Model %1 Summary Report"

const L_ReportSummary_Text = "Report Summary"
const L_TableOfContents_Text = "Table of Contents"
const L_PreparedOnHeader_Text = "Prepared on:"
const L_ModelNameHeader_Text = "Model:"
const L_ModelSizeHeader_Text = "Model Size:"
const L_Bytes_Text = " bytes"
const L_PropertyColumnHeading_Text = "Property"
const L_ValueColumnHeading_Text = "Value"
const L_SelectedSegmentColumnHeading_Text = "Selected Segment"
const L_ScoreColumnHeading_Text = "Score"
const L_ScoreValueColumnHeading_Text = "Score Value"
const L_MembershipPercentageHeading_Text = "Membership percentage: "
const L_SegmentGroupDefaultLabel_Text = "Segment Group"
const L_SegmentDefaultLabel_Text = "Segment"
const L_NoSegment_Text = "None"
const L_NoModelLoaded_Text = "No model loaded."
const L_OpenModelTitle_Text = "Open Model"
const L_LoadingModel_StatusBar = "Loading..."
const L_RenamingSegmentTitle_Text = "Rename Segment"
const L_SelectedSegment_Text = "Selected Segment"

const L_SummaryCaptionTempl_Text = "Summary of %1:"

const L_CompareCaptionTempl_Text = "Compare %1 to:"
 
const L_LessThanTempl_Text = "Less than %1"
 
const L_GreaterThanTempl_Text = "Greater than %1"
 
const L_BetweenTempl_Text = "Between %1 and %2"

const L_ListingModels_Text = "Listing models..."
const L_ModelName_Text = "Name"
const L_Size_Text = "Size"
const L_DateCreated_Text = "Date Created"
const L_SaveConfirmationDialog_Text = "Save changes before exiting?"
const L_EnterListName_Text = "Enter list name:"
const L_OK_Text = "OK"
const L_Cancel_Button = "Cancel"
const L_Help_Button = "Help"
const L_NewName_Text = "Rename to:"
const L_ExportSegmentTitle_Text = "Export Segment to List"
const L_ErrorInRename_Text = "Error Renaming Segment"
const L_ErrorInRenameBody_Text = "Duplicate segment name"
const L_ErrorInRenameDescription_Text = "You cannot give two segments the same name."
const L_ErrorListingModels_Text = "Error Listing Models"
const L_PredictorNotInstalled_Text = "Predictor Service is not installed."
const L_PredictorNotInstalledDescription_Text = "Unable to connect to predictor service to list models.  Perhaps it is not installed."
const L_MinimumProbability_Text = "Minimum probability:"
const L_ProbRangeError_Text = "Minimum probability must be between 0.0 and 1.0"
const L_ErrorOpeningModel_Text = "Error Opening Model"
const L_ErrorOpeningModelBody_Text = "Error Opening Model.  Press Back button to return to Model List."
const L_OpeningModelWait_Text = "Opening model, please wait..."
const L_UnableToSave_ErrorMessage = "Error Saving Model"
const L_SegmentsCaption_Text = "Segment hierarchy:"
const L_Rename_Button = "Rename"
const L_UnknownError_ErrorMessage = "Unknown Error"
const L_ListExists_ErrorMessage = "A list by that name already exists."
const L_ExportToListFailed_ErrorMessage = "Export to list failed."
const L_ErrorListingModels_ErrorMessage = "Error listing models"
%>
