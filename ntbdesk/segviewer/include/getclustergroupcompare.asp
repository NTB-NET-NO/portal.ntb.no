<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!-- #INCLUDE FILE='constants.inc' -->
<!-- #INCLUDE FILE='segviewerStrings.asp' -->
<!-- #INCLUDE FILE='functions.inc' -->
<%
'
' Returns an XML string comparing two cluster groups
'
Function GetClusterGroupCompare(strModelName, strDWConn, nAttributes, iClusterGroup1, iClusterGroup2)
	Dim oDataModel

	Set oDataModel = GetModelCached(strModelName, strDWConn)
    GetClusterGroupCompare = oDataModel.XMLClusterGroupCompare(iClusterGroup1,iClusterGroup2,nAttributes)
End Function

'
' Main
'

'
' Parse script parameters
'
Dim strModelName   ' Model name to load
Dim nAttributes    ' Number of attributes per segment to return in XML
Dim iClusterGroup1 ' Which cluster or group?
Dim iClusterGroup2 ' Which cluster or group?
Dim strXML         ' XML to return
Dim strDWConn

strModelName = Request.QueryString("ModelName")
strDWConn = Request.QueryString("ConnStr")

nAttributes = Request.QueryString("Attributes")
If nAttributes = "" Then
    nAttributes = DEFAULT_ATTRIBUTE_LIMIT
End If

iClusterGroup1 = Request.QueryString("iCG1")
If iClusterGroup1 = "" Then
    iClusterGroup1 = -1
End If

iClusterGroup2 = Request.QueryString("iCG2")
If iClusterGroup2 = "" Then
    iClusterGroup2 = -1
End If

'
' Return the summary
'
strXML = GetClusterGroupCompare(strModelName, strDWConn, nAttributes, iClusterGroup1, iClusterGroup2)
Response.Write strXML

%>
