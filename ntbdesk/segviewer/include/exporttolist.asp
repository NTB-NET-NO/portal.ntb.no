<!--#INCLUDE FILE='../../include/BDXMLHeader.asp' -->
<!-- #INCLUDE FILE='constants.inc' -->
<!-- #INCLUDE FILE='segviewerStrings.asp' -->
<!-- #INCLUDE FILE='functions.inc' -->
<!-- #INCLUDE FILE='../../include/HTTPXMLUtil.asp' -->
<%
'
' Main
'

'
' Parse script parameters
'

Dim strSegmentName
Dim strListName
Dim strModelName
Dim dArguments
Dim strLMConn
Dim strHostName
Dim strDWConn
Dim fpMinProbability
Dim xmlReturn

Set dArguments = dGetRequestXMLAsDict()

strModelName = Request.QueryString("ModelName")

strSegmentName = dArguments("inputSegmentName")
strListName = dArguments("inputListName")
strDWConn = dArguments("inputConnStr")
strHostName = Application("MSCSPredictorServiceHostName")
strLMConn = dArguments("inputLMConnStr")
fpMinProbability = g_MSCSDataFunctions.ConvertFloatString(dArguments("inputMinProbability"))


'
' Save to list.
'

Set xmlReturn = ExportSegmentToList(strLMConn, strListName, strModelName, strSegmentName, strHostName, strDWConn, fpMinProbability)
Response.Write xmlReturn.xml

%>
