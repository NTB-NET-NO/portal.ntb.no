<?xml version = '1.0' encoding='windows-1252' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
    <xsl:template match="/">
        <SELECT ID="selModelList" SIZE="10">
        <xsl:for-each select="MODELS/MODEL">
            <OPTION><xsl:value-of select="@NAME"/></OPTION>
        </xsl:for-each>
        </SELECT>
    </xsl:template>
</xsl:stylesheet>
