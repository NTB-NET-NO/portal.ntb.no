<%

' Try to display a tree this depth on the left hand side of segment viewer
Const DEFAULT_TREE_DEPTH = 3

' Default number of attributes to display in a segment summary
Const DEFAULT_ATTRIBUTE_LIMIT = 10

Dim DEFAULT_EXPORT_MINPROB

DEFAULT_EXPORT_MINPROB = g_MSCSDATAFunctions.ConvertFloatString("0.5", 1033)

Const E_FAIL = &H80004005

%>
