<%

Function GetModelCached(strModelName, strDWConn)
    Dim oPredClient
    Dim oDataModel
    Dim strSiteName

    strSiteName = Application("MSCSSiteName")

    If IsEmpty(Session("CurrentSegmentModel")) Or strModelName <> Session("CurrentSegmentModelName") Then
        Set oPredClient = Server.CreateObject("Commerce.PredictorClient")
        oPredClient.LoadModelFromDB strModelName, strDWConn
        Set oDataModel = oPredClient.DataModel
        Set Session("CurrentSegmentModel") = oDataModel
        Session("CurrentSegmentModelName") = strModelName
        Set GetModelCached = oDataModel
    Else
        Set GetModelCached = Session("CurrentSegmentModel")
    End If

End Function

'
' Returns an XML string summarizing a given cluster model
'
Function GetModelSummary(strModelName, strDWConn, nDepth, nAttributes)
	Dim oDataModel

        On Error Resume Next
	Set oDataModel = GetModelCached(strModelName, strDWConn)
        If Err.Number <> 0 Then
            GetModelSummary = "<ERROR>" & Err.Description & "</ERROR>"
            Err.Clear
            Exit Function
        End If
	oDataModel.ClusterTreeDepth = nDepth
	GetModelSummary = oDataModel.XMLModelSummary(nAttributes)
        If Err.Number <> 0 Then
            GetModelSummary = "<ERROR>" & Err.Description & "</ERROR>"
            Err.Clear    
        End If
End Function

Function ExportSegmentToList(strLMConn, strListName, strModelName, strSegmentName, strHostName, strDWConn, fpMinProbability)
    Dim oLM
    Dim idList
    Dim idOp
    Dim bAsync
    Dim listFlags
    Dim varListID
    Dim xmlReturn

    Set xmlReturn = xmlGetXMLDOMDoc()
    Set oLM = Server.CreateObject("Commerce.ListManager")

    bAsync = True
    listFlags = 12 'Generic User list

    oLM.Initialize strLMConn    
    On Error Resume Next
    varID = oLM.GetListID(strListName)
    If Err.Number = 0 Then
        AddErrorNode xmlReturn, E_FAIL, L_ExportToListFailed_ErrorMessage, L_ListExists_ErrorMessage
    Else
        Err.Clear
        idList = oLM.CreateFromSegment(strListName, "", listFlags, 0, strModelName, strSegmentName, strHostName, strDWConn, fpMinProbability, bAsync, idOp)
        If Err.Number <> 0 Then
            AddErrorNode xmlReturn, Err.Number, L_ExportToListFailed_ErrorMessage, Err.Description
        End If
    End If

    Set ExportSegmentToList = xmlReturn
End Function

Function Quote(str)
    Quote = Replace(str, """", """""")
End Function

%>

