<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='include/segviewerStrings.asp' -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='include/constants.inc' -->
<!--#INCLUDE FILE='include/listmodels.asp' -->
<!--#INCLUDE FILE='include/functions.inc' -->
<%
    Dim strSiteName
    Dim strHostName
    Dim xmlModels

    strSiteName = Application("MSCSSiteName")
    strHostName = GetSiteConfigField("Predictor", "s_MachineName")
    Application("MSCSPredictorServiceHostName") = strHostName
    Set xmlModels = ListModels(strHostName, 1) 
%>


<HTML>
<HEAD>
    <TITLE>BizDesk</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
</HEAD>
<BODY id="myBody">
<SCRIPT FOR='window' EVENT='onload' LANGUAGE="VBScript">
	DisableTask "open"
</SCRIPT>
<SCRIPT LANGUAGE="VBScript">

Option Explicit

Dim g_strModelName
Dim g_strSiteName
Dim g_strConnStr
Dim g_strDateCreated
Dim g_strModelSize
Dim g_bPredictorNotInstalled
Dim g_bOpening

g_bPredictorNotInstalled = "<%= g_bPredictorNotInstalled %>"
g_bOpening = False

Sub OpenModel
    DisableTask "open"
    g_bOpening = True
    SetStatusText "<%= L_OpeningModelWait_Text %>"
    formOpenModel.inputModelName.value = g_strModelName
    formOpenModel.inputSiteName.value = g_strSiteName
    formOpenModel.inputConnStr.value = g_strConnStr 
    formOpenModel.inputDateCreated.value = g_strDateCreated
    formOpenModel.inputModelSize.value = g_strModelSize       

    formOpenModel.submit()
End Sub

Sub OnSelectRow
    If g_bOpening Then
        Exit Sub
    End If

    EnableTask "open"
    Dim xmlobjSelection
    Dim strModelName
    Dim nodeTmp
    
    Set xmlobjSelection = window.event.XMLRecord
    Set nodeTmp = xmlobjSelection.selectSingleNode("./ModelName")
    If nodeTmp Is Nothing Then
        'BUGBUG handle error
    Else
        g_strModelName = nodeTmp.Text
    End If
    Set nodeTmp = xmlobjSelection.selectSingleNode("./SiteName")
    If nodeTmp Is Nothing Then
        g_strSiteName = ""
    Else
        g_strSiteName = nodeTmp.Text
    End If
    Set nodeTmp = xmlobjSelection.selectSingleNode("./ConnStr")
    If nodeTmp Is Nothing Then
        'BUGBUG: handle error
    Else
        g_strConnStr = nodeTmp.Text
    End If 
    Set nodeTmp = xmlobjSelection.selectSingleNode("./DateCreated")
    If nodeTmp Is Nothing Then
        'BUGBUG: handle error
    Else
        g_strDateCreated = nodeTmp.Text
    End If 
    Set nodeTmp = xmlobjSelection.selectSingleNode("./Size")
    If nodeTmp Is Nothing Then
        'BUGBUG: handle error
    Else
        g_strModelSize = nodeTmp.Text
    End If 
End Sub

Sub OnUnselectRow
    DisableTask "open"
    g_strModelName = ""
    g_strSiteName = ""
    g_strConnStr = ""
    g_strDateCreated = ""
    g_strModelSize = ""
End Sub

Sub OnReadyStateChange
    If lsModels.readystate = "complete" Then
        If CBool(g_bPredictorNotInstalled) Then
            ShowErrorDlg "<%= Quote(L_ErrorListingModels_Text) %>", "<%= Quote(L_PredictorNotInstalled_Text) %>", "<%= Quote(L_PredictorNotInstalledDescription_Text) %>", ERROR_ICON_ALERT
        End If
    End If
End Sub

</SCRIPT>

<%
InsertTaskBar L_SegmentViewerTitle_Text, ""
%>


<!-- Model meta data -->
<xml id='lsModelMeta'>
    <listsheet>
        <global selection='single' pagecontrols='no' />
        <columns>
            <column id='ModelName' width='50'><%= L_ModelName_Text %></column>
            <column id='Size' width='10'><%= L_Size_Text %></column>
            <column id='DateCreated' width='20'><%= L_DateCreated_Text %></column>
        </columns>
        <operations>
        </operations>
    </listsheet>
</xml>

<!-- Model data -->
<xml id='lsModelData'>
<%= xmlModels.documentElement.xml %>
</xml>

<div id="bdcontentarea" class="listPageContainer">	
<DIV ID='lsModels'
    CLASS='listSheet' STYLE="MARGIN: 20px; HEIGHT: 80%"
    DataXML='lsModelData'
    MetaXML='lsModelMeta'
    LANGUAGE='VBScript'
    OnRowSelect='OnSelectRow()'
    OnRowUnselect='OnUnSelectRow()'
    OnReadyStateChange='OnReadyStateChange()'>
    <%= L_ListingModels_Text %>
</DIV>
</div>

<FORM ID="formOpenModel" NAME="formOpenModel" METHOD="POST" ontask="OpenModel()">
    <INPUT ID='inputModelName' NAME='ModelName' TYPE='hidden'>
    <INPUT ID='inputSiteName' NAME='SiteName' TYPE='hidden'>
    <INPUT ID='inputConnStr' NAME='ConnStr' TYPE='hidden'>
    <INPUT ID='inputDateCreated' NAME='DateCreated' TYPE='hidden'>
    <INPUT ID='inputModelSize' NAME='ModelSize' TYPE='hidden'>
</FORM>
</BODY>

</HTML>
