<!--#INCLUDE FILE='include/BDHeader.asp' -->
<!--#INCLUDE FILE='include/BizDeskUtil.asp' -->
<HTML>
<HEAD>
	<LINK REL='stylesheet' TYPE='text/css' HREF='include/bizdesk.css' ID='mainstylesheet'>
	<STYLE>
		BODY
		{
			PADDING: 0
		}
		#bdwelcome
		{
			MARGIN: 0
			OVERFLOW: hidden;
			BACKGROUND-COLOR: white;
			HEIGHT: 224px;
			WIDTH: 100%;
		}
		#welcomedoc
		{
			HEIGHT: expression(document.body.clientHeight - 226 - 55 - 20);
			WIDTH: expression(document.body.clientWidth);
			BORDER: none;
		}
		#bdwelcomeimg
		{
			MARGIN: 0
		}
	</STYLE>
	<SCRIPT LANGUAGE='VBScript'>
	<!--
		sub window_onLoad()
			parent.ClearNavSelection()
			HideStatusLegend()
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY SCROLL='no' TABINDEX=-1>
<%
InsertTaskBar L_WelcomePage_Text, L_WelcomeStatus_Text
%>
<DIV ID='bdcontentarea' STYLE="OVERFLOW: hidden; PADDING: 0; MARGIN: 0">
<DIV ID='bdwelcome'>
	<IMG ID='bdwelcomeimg' BORDER=0 HEIGHT='224' WIDTH='531' SRC="assets/welcomegraphic.gif" TITLE='<%= L_CommerceServerBizDesk_Tooltip %>'>
<%	if L_TimeBombWarn_Text <> "" then
%>		<!-- REMOVE/REPLACE BELOW POST-BETA -->
		<div STYLE='POSITION: absolute; TOP: 160px; LEFT: 160px' nowrap><%= L_TimeBombWarn_Text %></div>
		<!-- REMOVE/REPLACE ABOVE POST-BETA -->
<%	end if
%>	<IFRAME ID="welcomedoc" SRC="docs/welcomehelp.htm" TABINDEX=-1 BORDER=0 frameborder=0 framespacing=0></IFRAME>
</DIV>
</DIV>

</BODY>
</HTML>
