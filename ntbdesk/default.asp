<!--#INCLUDE FILE='include/BDHeader.asp' -->
<!--#INCLUDE FILE='include/BizDeskPaths.asp' -->
<!--#INCLUDE FILE='include/BizDeskStrings.asp' -->
<HTML>
<HEAD>
	<TITLE><%= L_MSCommerceServerClientSetup_HTMLTitle %></TITLE>
	<STYLE>
		BODY
		{
			MARGIN-TOP: 10px;
			MARGIN-LEFT: 10px;
			BACKGROUND-COLOR: white;
			cursor: default
		}
		TD
		{
			VERTICAL-ALIGN: top
		}
		#bdicon
		{
			BACKGROUND-COLOR: black
		}
		#bderror
		{
			FONT-FAMILY: verdana, arial, helvetica, sans-serif;
			FONT-SIZE: 12pt;
			FONT-WEIGHT: bold;
			COLOR: red
		}
	</STYLE>
<SCRIPT LANGUAGE=VBScript>
<!--
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' event handler routines:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	context menu handler to turn off context menus
sub document_onContextMenu()
	' -- disallow default context menu unless in entry fields
	if not (window.event.srcElement.tagName = "INPUT" or window.event.srcElement.tagName = "TEXTAREA") then
		window.event.returnValue = false
	end if
end sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'	BizDesk Framework:	select start handler to turn off selection except in inputs
sub document_onSelectStart()
	' -- disallow selection of text on the page except in edit boxes
	Dim elSource
	set elSource = window.event.srcElement
	if elSource.tagName = "TEXTAREA" or _
		elSource.tagName = "INPUT" then
		if (elSource.readonly or elSource.disabled) then window.event.returnValue = false
	else
		window.event.returnValue = false
	end if
end sub
'-->
</SCRIPT>
</HEAD>
<%
dim aUserAgent, sUserAgent
aUserAgent = split(Request.ServerVariables("HTTP_USER_AGENT"), ";")	'e.g., Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)
sUserAgent = trim(aUserAgent(1))
' aUserAgent(1) equals something like "MSIE 5.5"
if inStr(aUserAgent(1), "MSIE") = 0 then
	AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
else
	aUserAgent = split(sUserAgent, " ")	'separate MSIE from version
	sUserAgent = trim(aUserAgent(1))
	aUserAgent = split(sUserAgent, ".")	'separate version major from minor
	'aUserAgent(0) is the major version and aUserAgent(1) is the minor version
	if aUserAgent(0) < 5 then
		AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
	elseif aUserAgent(0) = 5 then
		if not isNumeric(aUserAgent(1)) then
			'just in case there is a 5.5b or something
			AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
		elseif aUserAgent(1) < 5 then
			AbortWithTerminalError(L_RequiresIE55_ErrorMessage)
		end if
	end if
end if

sub AbortWithTerminalError(sErrorMsg)
%>
	<BODY>
		<TABLE>
		<TR>
			<TD ID='bdicon'><IMG SRC='assets/bdlogo.gif' HEIGHT='55' WIDTH='164' BORDER='0'></TD>
			<TD>&nbsp</TD>
			<TD ID="bderror"><%= sErrorMsg %></TD>
		</TR>
		</TABLE>
	</BODY>
	</HTML>
<%
	Response.end
end sub
%>
<FRAMESET ID="bdframe" rows='60,*' FRAMESPACING='0' FRAMEBORDER='0' BORDER='0' TABINDEX='-1'>
	<FRAME NAME='defaulttop' SRC='defaulttop.asp'
		BORDER='0' FRAMEBORDER='0' FRAMESPACING='0' SCROLLING='no' TABINDEX='-1'>
	<FRAME NAME='clientsetup' SRC='clientsetup.asp'
		BORDER='0' FRAMEBORDER='0' FRAMESPACING='0' SCROLLING='auto' TABINDEX='-1'>
	<NOFRAMES>
		<BODY>
			<TABLE>
			<TR>
				<TD ID='bdicon'><IMG SRC='assets/bdlogo.gif' HEIGHT='55' WIDTH='164' BORDER='0'></TD>
				<TD>&nbsp</TD>
				<TD ID='bderror'><%= L_RequiresIE55_ErrorMessage %></TD>
			</TR>
			</TABLE>
		</BODY>
	</NOFRAMES>

</FRAMESET>

</HTML>
