<!--#INCLUDE FILE='include/BDHeader.asp' -->
<!--#INCLUDE FILE='include/BizDeskPaths.asp' -->
<%
const L_DialogHeight_Style = "250px"
const L_DialogWidth_Style = "500px"

dim oInstaller, sVersion
on error resume next
set oInstaller = Server.CreateObject("WindowsInstaller.Installer")
sVersion = oInstaller.ProductInfo("{705CB382-C149-11D2-973D-00C04F79E4B3}", "VersionString")
set oInstaller = nothing
%>
<HTML>
<HEAD>
	<TITLE ID='L_AboutBizDesk_Title'>About Microsoft Commerce Server 2000 Business Desk</TITLE>
	<LINK REL='stylesheet' TYPE='text/css' HREF='include/bizdesk.css' ID='mainstylesheet'>
	<STYLE>
		HTML
		{
			WIDTH: <%= L_DialogWidth_Style %>;
			HEIGHT: <%= L_DialogHeight_Style %>;
		}
		BODY
		{
			MARGIN: 10px;
			BACKGROUND: #cccccc
		}
		BUTTON
		{
			WIDTH: 8em;
		}
	</STYLE>
	<SCRIPT LANGUAGE='VBScript'>
	<!--
		sub closeAbout()
			window.returnValue = false
			window.close()
		end sub
	'-->
	</SCRIPT>
</HEAD>
<BODY SCROLL='no'>

<TABLE width='100%'>
<TR>
	<TD VALIGN='top'>
		<IMG SRC='<%= g_sBDAssetRoot %>bdlogoIcon.gif'
			HEIGHT='42' WIDTH='42' BORDER='0' ALIGN='top'>
	</TD>
	<TD width='100%'>
		<ID id='L_AboutBizDeskName_text'><B>Microsoft&reg; Commerce Server 2000 Business Desk</B></ID>
		<BR>
		<ID id='L_AboutVersion_text'>Version:&nbsp;<%= sVersion %></ID>
		<BR>
		<ID id='L_AboutBizDeskCopyright_text'>&copy; 1999-2000 Microsoft Corporation. All rights reserved.</ID>
		<P>
		<HR>
		<ID id='L_AboutBizDeskWarning_text'>Warning: This computer program is protected by copyright law and 
		international treaties. Unauthorized reproduction or distribution 
		of this program, or any portion of it, may result in severe civil 
		and criminal penalties, and will be prosecuted to the maximum 
		extent possible under the law.</ID>
	</TD>
</TR>
<TR>
	<TD COLSPAN='2'>
	<TABLE width='100%'>
	<TR>
		<TD width='100%'></TD>
		<TD>
			<BUTTON ID='L_AboutOK_Button' CLASS='bdbutton'
				LANGUAGE='VBScript' ONCLICK='window.close()'>OK</BUTTON>
		</TD>
	</TR>
	</TABLE>
</TD>
</TR>
</TABLE>

</BODY>
</HTML>
