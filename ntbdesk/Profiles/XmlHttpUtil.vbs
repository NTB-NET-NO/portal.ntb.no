<!-- Client-side utility functions for XMLHTTP object. -->


<SCRIPT LANGUAGE="VBScript">
Option Explicit

' Localizable strings
Const L_HTTPError_DialogTitle = "An HTTP error occurred."
Const L_XMLError_DialogTitle = "An XML error occurred."
Const L_VBScriptError_DialogTitle = "A VBScript error occurred."
Const L_ASPError_DialogTitle = "The ASP page reported an error."
Const L_ErrHTTP_Text = "HTTP error "
Const L_ErrURL_Text = "URL: "
Const L_ErrCode_Text = "Code: "
Const L_ErrReason_Text = "Reason: " 
Const L_ErrSource_Text = "Source: "
Const L_ErrDescription_Text = "Description: "
Const L_ErrServerUnReachable_Text = "The server cannot be reached.  Please contact the system administrator.  Error Code: "

Function sURLEncode(strInput)
    Dim strEncoded
    Dim regExp
    Dim chCurr
    Dim strHexVal
    Dim objMatch, objMatches

    strEncoded = strInput

    ' First, replace all % signs with the encoded value.
    strEncoded = Replace(strEncoded, "%", "%25")

    ' Next, replace all non-alphanumeric characters with their encoded
    ' values, EXCEPT FOR the spaces (" ").
    Set regExp = New RegExp
    regExp.Pattern = "[^%A-Za-z0-9 ]"
    regExp.Global = False
    Do
        Set objMatches = regExp.Execute(strEncoded)
        If objMatches.Count <> 0 Then
            ' Get the character that matched.
            Set objMatch = objMatches.Item(0)

            ' Create the URL-Encoded version.
            chCurr = objMatch.Value
            strHexVal = Hex(Asc(chCurr))
            strHexVal = String(2 - Len(strHexVal), "0") & strHexVal

            ' Replace the character with the URL-Encoded version.
            strEncoded = Replace(strEncoded, chCurr, "%" & strHexVal)
        End If
    Loop While objMatches.Count <> 0

    ' Finally, replace spaces.
    strEncoded = Replace(strEncoded, " ", "+")

    sURLEncode = strEncoded
End Function


Function sOldURLEncode(strInput)
    Dim strEncoded
    Dim i
    Dim chCurr
    Dim strHexVal
    
    For i = 1 To Len(strInput)
        chCurr = Mid(strInput, i, 1)
        If chCurr = " " Then
            strEncoded = strEncoded & "+"
        Elseif (chCurr >= "a" And chCurr <= "z") Or _
            (chCurr >= "A" And chCurr <= "z") Or _
            (chCurr >= "0" And chCurr <= "9") Then
            strEncoded = strEncoded & chCurr
        Else
            strHexVal = Hex(Asc(chCurr))
            strHexVal = String(2 - Len(strHexVal), "0") & strHexVal
            strEncoded = strEncoded & "%" & strHexVal
        End If
    Next

    sOldURLEncode = strEncoded
End Function


Function xmlPOSTToServer(strURL, strRequest)
    Dim xmlhttpObj
    Dim bSuccess
    Dim xmlResponse
    Dim oParseError
    Dim xmlError

	' initialize to success
	bSuccess = True
	
    ' Create an XMLHTTP object to POST to the server.  Set the operation to
    ' be synchronous.  Set up the header so IIS will grok the request body.
    Set xmlhttpObj = CreateObject("MSXML2.XMLHTTP.2.6")
    xmlhttpObj.open "POST", strURL, False
    xmlhttpObj.setRequestHeader "Content-Type", _
        "application/x-www-form-urlencoded"
	
	
	' Send the request.  Get a response.
	on error resume next
    xmlhttpObj.send strRequest
    
	' if there was an error on the client side (ie network failure)
    if err.number <> 0 then
		MsgBox L_ErrServerUnReachable_Text & Hex(err.number), vbOKOnly, L_VBScriptError_DialogTitle
        bSuccess = False
    end if
    err.clear
	on error goto 0
	
	' Analyze the response now.
    ' Check for HTTP errors.
    If bSuccess And xmlhttpObj.status <> 200 Then
        ' An HTTP error took place
        Call ShowErrorDialog(L_HTTPError_DialogTitle, L_ErrHTTP_Text & _
            xmlhttpObj.status & ": " & xmlhttpObj.statusText & _
            "<BR>" & L_ErrURL_Text & strURL)

        bSuccess = False
    End If

    ' Check for XML parse errors.
    If bSuccess Then
        Set xmlResponse = xmlhttpObj.responseXML
        Set oParseError = xmlResponse.parseError

        If oParseError.errorCode <> 0 Then
            Call ShowErrorDialog(L_XMLError_DialogTitle, _
                L_ErrCode_Text & "0x" & Hex(oParseError.errorCode) & _
                "<BR>" & L_ErrReason_Text & oParseError.reason & _
                "<BR>" & L_ErrURL_Text & strURL)

            bSuccess = False
        End If
    End If

    ' Check for ASP errors (reported using <ERROR> format).
    If bSuccess Then
        Set xmlError = xmlResponse.selectSingleNode("ERROR")
        If Not (xmlError Is Nothing) Then
            Call ShowErrorDialog(xmlError.text, L_ASPError_DialogTitle & "<BR>" & _
                L_ErrCode_Text & "0x" & Hex(CLng(xmlError.getAttribute("ID"))) & _
                "<BR>" & L_ErrSource_Text & xmlError.getAttribute("SOURCE"))

            bSuccess = False
        End If
    End If

    ' If we failed, return Nothing.
    If Not(bSuccess) Then
        Set xmlResponse = Nothing
    End If

    Set xmlPOSTToServer = xmlResponse
End Function


' Displays a simple error dialog with a simple error message and a "details"
' area that can be shown if desired.
Sub ShowErrorDialog(strError, strDetails)
    Dim dictArgs

    Set dictArgs = CreateObject("Scripting.Dictionary")

    dictArgs.Item("error") = strError
    dictArgs.Item("details") = strDetails

    Call showModalDialog("/widgets/exprbldrHTC/DlgError.htm", dictArgs, _
        "displayWidth:300px;displayHeight:200px;status:no;help:no")
End Sub


</SCRIPT>

