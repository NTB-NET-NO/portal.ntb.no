<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!-- #INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!-- #INCLUDE FILE='BDAOUtil.asp' -->
<%
Const L_Catalog_ErrorMessage      = "The catalog '%1' does not exist in the data store."    
                                  '%1 is the profile catalog (string)
Const L_ProfileLoad_ErrorMessage  = "An error occurred while loading the Profile."
Const L_Profiles_Text             = "Profiles" 'Title for page in bizdesk framework
%>
<HTML>
<HEAD>
<TITLE ID='L_ProfileSel_HTMLTitle'>Modify a Profile</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<!-- remove behavior from listSheet class style so that i can control that -->
<STYLE>
.listSheet
{
    height:200px;margin-left:20px;behavior:none
}
</STYLE>

<!-- #INCLUDE FILE='XmlHttpUtil.vbs' -->

<SCRIPT LANGUAGE="VBScript">
Option Explicit

' Localizable strings
Const L_StatusItems_Text        = "Items selected:  "
Const L_StatusOpening_Text      = "Opening profile, please wait..."
Const L_StatusDeleted_Text      = " deleted"
Const L_StatusMultiDeleted_Text = "Profiles deleted:  "

Const L_TaskMultiDelete_Text = "Delete Profiles:  "
Const L_TaskOpen_Text        = "View "
Const L_TaskDelete_Text      = "Delete "
Const L_TaskOpenNone_Text    = "View Profile"
Const L_TaskDeleteNone_Text  = "Delete Profile"

Const L_DisplayName_Text = "Name"

' Client-side constants:
Const CATALOG_NAME = "<%= PROFILE_CATALOG %>"
Const CATEGORY_NAME = ""        ' Future:  fill this in with category to filter.

Const gc_sDocumentTag = "<%= gc_sDocumentTag %>"
Const gc_sCatalogTag  = "<%= gc_sCatalogTag  %>"
Const gc_sProfileTag  = "<%= gc_sProfileTag  %>"

Dim g_nListSheetID
Dim g_dictSelected          ' Currently selected items.

'----------------------------------------------------------------------------
' init
'
' Description:
'     Programmatically inserts the XML data-islands into the document
'----------------------------------------------------------------------------

Sub init()

  Dim elXML
  Dim elDiv


  ' Build XML islands programatically
  Set elXML = document.body.appendChild(document.createElement("XML"))
    elXML.id = "xiListsheetMeta"
    elXML.XMLDocument.loadXML "<listsheet>" &_
      "<global sort='no' pagecontrols='no' grouped='no' subobject='no' selection='multi' />" &_
      "<columns>" &_
        "<column id='name' key='yes' width='0' hide='yes'></column>" &_
        "<column id='displayName' width='50'>" & L_DisplayName_Text & "</column>" &_
      "</columns>" & _
    "</listsheet>"
    
    
    Set g_dictSelected = CreateObject("Scripting.Dictionary")

    ' Add button is always enabled, so just do that here.
    'EnableTask "new"
    UpdateCatalogXML
    UpdateStatusText
    UpdateTaskButtons

End Sub

'----------------------------------------------------------------------------
' RowSelectHandler
'
' Description:
'     When a row is selected, this sub adds it to the list of selected items
'     that the page maintains.  Also, the task buttons are updated.
'----------------------------------------------------------------------------
Sub RowSelectHandler()
    Dim xmlItem
    Set xmlItem = window.event.XMLrecord
    If recordIsProfile(xmlItem) Then
        ' This is a profile.
        g_dictSelected.Add xmlItem.selectSingleNode("name").text, xmlItem
    End If

    UpdateStatusText
    UpdateTaskButtons
End Sub


'----------------------------------------------------------------------------
' RowUnselectHandler
'
' Description:
'     When a row is unselected, this sub removes it from the list of selected
'     items that the page maintains.  Also, the task buttons are updated.
'----------------------------------------------------------------------------
Sub RowUnselectHandler()
    Dim xmlItem

    Set xmlItem = window.event.XMLrecord
    If recordIsProfile(xmlItem) Then
        ' This is a profile.
        g_dictSelected.Remove xmlItem.selectSingleNode("name").text
    End If

    UpdateStatusText
    UpdateTaskButtons
End Sub


Function recordIsProfile(xmlRecord)
    recordIsProfile = isNull(xmlRecord.getAttribute("isCategory")) Or _
        xmlRecord.getAttribute("isCategory") <> "1"
End Function


'----------------------------------------------------------------------------
' AllRowsSelectHandler
'
' Description:
'     When all rows are selected, this sub puts all profile items into the
'     list of selected items that the page maintains.  Also, the task buttons
'     are updated.
'----------------------------------------------------------------------------
Sub AllRowsSelectHandler()
    Dim xmllistRecords
    Dim xmlRecord

    ' First, clear out the current list.
    g_dictSelected.RemoveAll

    ' Next, go through all profiles in the XML data-island, and add them to
    ' the selected list.

    Set xmllistRecords = xiCatalogESData.XMLDocument.selectNodes("//record")
    For Each xmlRecord In xmllistRecords
        ' If the list is empty, we still have to put a dummy record element
        ' into it.  This check makes sure we don't accidently try to use the
        ' dummy record.
        If xmlRecord.hasChildNodes Then
            g_dictSelected.Add xmlRecord.selectSingleNode("name").text, xmlRecord
        End If
    Next

    ' Update UI buttons.
    UpdateStatusText
    UpdateTaskButtons
End Sub


'----------------------------------------------------------------------------
' AllRowsUnselectHandler
'
' Description:
'     When all rows are unselected, this sub clears the list of selected
'     items that the page maintains.  Also, the task buttons are updated.
'----------------------------------------------------------------------------
Sub AllRowsUnselectHandler()
    g_dictSelected.RemoveAll
    UpdateStatusText
    UpdateTaskButtons
End Sub


Sub UpdateStatusText()
    If g_dictSelected.Count > 0 Then
        SetStatusText L_StatusItems_Text & g_dictSelected.Count
    Else
        SetStatusText ""
    End If
End Sub


'----------------------------------------------------------------------------
' UpdateTaskButtons
'
' Description:
'     This sub updates the BizDesk task buttons based on what is currently
'     selected from the profile-list.
'----------------------------------------------------------------------------
Sub UpdateTaskButtons()
    Dim bProfile
    Dim strProfileName
    Dim rgxmlItems

    If g_dictSelected.Count = 0 Then
        ' Nothing is selected.

        DisableTask "open"
		setTaskTooltip "open", L_TaskOpenNone_Text

        DisableTask "delete"
		setTaskTooltip "delete", L_TaskDeleteNone_Text
    Elseif g_dictSelected.Count = 1 Then
        ' One profile is selected.

        rgxmlItems = g_dictSelected.Items
        strProfileName = getProfileDisplayName(rgxmlItems(0))

        EnableTask "open"
		setTaskTooltip "open", sFormatString(L_TaskOpen_Text, array(strProfileName))

        EnableTask "delete"
		setTaskTooltip "delete", sFormatString(L_TaskDelete_Text, array(strProfileName))
    Else
        ' Multiple profiles are selected.

        DisableTask "open"
		setTaskTooltip "open", L_TaskOpenNone_Text

        EnableTask "delete"
		setTaskTooltip "delete", sFormatString(L_TaskMultiDelete_Text, array(g_dictSelected.Count))
    End If
End Sub


Sub ReplaceCatalogXML(xmlNewCatalog)
    Dim xmlCatalog

    Set xmlCatalog = xiCatalog.XMLDocument
    xmlCatalog.loadXML xmlNewCatalog.xml
End Sub


Sub UpdateCatalogXML
    Dim xmlCatalogsDoc, xmlCatalogES, xmlDocES
    Dim xlRecs, xmlRecord
    Dim xmlNode
    Dim xlProfiles, xmlProf

    Set xmlCatalogsDoc = xiCatalog.XMLDocument
    Set xmlCatalogES = xiCatalogESData.XMLDocument
    Set xmlDocES = xmlCatalogES.selectSingleNode("document")

    ' Remove current list-sheet, if there is one.
    If Not isEmpty(g_nListSheetID) Then
        listSheetDiv.removeBehavior(g_nListSheetID)
    End If

    ' Clear all current selections.
    g_dictSelected.RemoveAll

    ' Remove all the old records from the catalog editsheet XML.
    Set xlRecs = xmlCatalogES.selectNodes("document/record")
    For Each xmlRecord In xlRecs
        xmlRecord.parentNode.removeChild xmlRecord
    Next

    ' Convert the profile-catalog XML document into editsheet format.
    Set xlProfiles = xmlCatalogsDoc.selectNodes(gc_sDocumentTag & "/" & _
		gc_sCatalogTag & "[@name='" & CATALOG_NAME & "']/" & gc_sProfileTag)
    If xlProfiles.length > 0 Then
        For Each xmlProf In xlProfiles
            ' Create a new entry for the profile.

            Set xmlRecord = xmlCatalogES.createElement("record")

            Set xmlNode = xmlCatalogES.createElement("name")
            xmlNode.text = xmlProf.getAttribute("name")
            xmlRecord.appendChild xmlNode

            Set xmlNode = xmlCatalogES.createElement("displayName")
            xmlNode.text = xmlProf.getAttribute("displayName")
            xmlRecord.appendChild xmlNode

            xmlDocES.appendChild xmlRecord
        Next
    Else
        ' Put a dummy record into the XML to make listsheet happy.
        Set xmlRecord = xmlCatalogES.createElement("record")
        xmlDocES.appendChild xmlRecord
    End If

    ' Attach new list-sheet.
    listSheetDiv.innerHTML = ""
    g_nListSheetID = listSheetDiv.addBehavior("/widgets/listHTC/listsheet.htc")

End Sub

Sub OpenProfile()
    Dim rgxmlItems

    SetStatusText L_StatusOpening_Text

    ' Get out the currently selected items.  THERE SHOULD BE ONLY ONE ITEM,
    ' if the button activation/deactivation code is correct.
    rgxmlItems = g_dictSelected.Items

    ' Set up the open-profile FORM.  Then submit it.
    formOpenProfile.profPath.value = getProfilePath(rgxmlItems(0))
    formOpenProfile.submit
End Sub


Function getProfilePath(xmlRecProfile)
  getProfilePath = CATALOG_NAME & "." & xmlRecProfile.selectSingleNode("name").text
End Function


Function getProfileDisplayName(xmlRecProfile)
    getProfileDisplayName = xmlRecProfile.selectSingleNode("displayName").text
End Function


'----------------------------------------------------------------------------
' DeleteProfile
'
' Description:
'     This sub handles the profile-deletion task.  IT DOES NO CHECKING FOR
'     WHETHER A PROFILE IS SELECTED, because the 'delete' button should only
'     be enabled when a profile is selected.
'
'     This sub also verifies the user's intent, so that deletions aren't done
'     without the user meaning to do it.
'----------------------------------------------------------------------------
Sub DeleteProfile()
    Dim bDelete
    Dim rgxmlItems      ' List of profiles to delete.
    Dim strProfPath
    Dim strProfDisplayName
    Dim i
    Dim strDelRequest
    Dim xmlResponse

    ' Get out the currently selected items.
    rgxmlItems = g_dictSelected.Items

    ' Make sure the user really wants to delete the profiles.
    bDelete = window.showModalDialog("DlgDeleteProfile.htm", _
        g_dictSelected.Count, _
        "dialogWidth:300px;dialogHeight:100px;status:no;help:no")

    If bDelete Then
        ' Create a request to send to the BizDataServices ASP page.

        strDelRequest = "BizDataServices.asp?profOper=deleteProfile"
        For i = 0 To UBound(rgxmlItems)
            strProfPath = getProfilePath(rgxmlItems(i))
            strDelRequest = strDelRequest & "&profPath=" & sURLEncode(strProfPath)
        Next

        ' Send the request.  Get a response.  Whoa, rocket-science.

        Set xmlResponse = xmlPOSTToServer(strDelRequest, "")
        If Not(xmlResponse Is Nothing) Then
            ' Profiles were successfully deleted.

            ' Update status-text.
            If g_dictSelected.Count = 1 Then
                SetStatusText getProfileDisplayName(rgxmlItems(0)) & L_StatusDeleted_Text

            Else
                SetStatusText L_StatusMultiDeleted_Text & g_dictSelected.Count
            End If

            ' Refresh the page.
            ReplaceCatalogXML xmlResponse
            UpdateCatalogXML
        End If
    End If
    'Update task buttons whether we deleted or not
    UpdateTaskButtons

End Sub


</SCRIPT>
</HEAD>

<%
Dim xmlCatalog, xmlError, xmlCatNode
Dim strErrorg
Dim elXML, elBody
Dim rgArgs(0)

' Retrieve the catalog, and verify that everything is good with it.
Set xmlCatalog = getCatalogsXML()

Set xmlError = xmlCatalog.selectSingleNode("ERROR")
If Not(xmlError Is Nothing) Then
    ' An error occurred.  Report it.
    Response.Write "<BODY>"
    OutputXMLError L_ProfileLoad_ErrorMessage, xmlError
    Response.End
End If

Set xmlCatNode = xmlCatalog.selectSingleNode(gc_sDocumentTag & "/" & _
	gc_sCatalogTag & "[@name='" & PROFILE_CATALOG & "']")
If xmlCatNode Is Nothing Then
    ' The specified catalog doesn't exist!  Report this to the user.
    ' (This is a fatal error.)
    Response.Write "<BODY>"
    rgArgs(0) = PROFILE_CATALOG
    strError = sFormatString(L_Catalog_ErrorMessage, rgArgs)
    Response.Write strError
    Response.End
End If

Response.Write "<BODY onLoad='init'>"

InsertTaskBar L_Profiles_Text, ""

Response.Write "<XML ID='xiCatalog'>"
Response.Write xmlCatalog.xml
Response.Write "</XML>"

Response.Write "<XML ID='xiCatalogESData'>"
Response.Write "<?xml version=""1.0"" ?>" &_
      "<document>" &_
          "<record />" &_
      "</document>"

Response.Write "</XML>"

%>
<DIV ID="bdcontentarea">
	<DIV ID="listSheetDiv" STYLE="margin: 20px; height: 80%"
		LANGUAGE="VBScript"
		onRowSelect="RowSelectHandler"
		onRowUnselect="RowUnselectHandler"
		onAllRowsSelect="AllRowsSelectHandler"
		onAllRowsUnselect="AllRowsUnselectHandler"
		DataXML="xiCatalogESData"
		MetaXML="xiListsheetMeta"><SPAN ID="L_ProfileLoad_Text">Loading profile list, please wait...</SPAN>
	</DIV>
</DIV>

<FORM ID="formDelProfile" NAME="formDelProfile" METHOD="POST" ontask="DeleteProfile()">
</FORM>

<FORM ID="formOpenProfile" NAME="formOpenProfile" METHOD="POST" ontask="OpenProfile()">
    <INPUT TYPE="hidden" ID="profPath" NAME="profPath" VALUE=""></INPUT>
</FORM>



</BODY>
</HTML>