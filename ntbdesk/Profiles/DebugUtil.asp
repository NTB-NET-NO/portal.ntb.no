<%

Dim g_objDbgFile
Set g_objDbgFile = Nothing

Sub DebugInit()
    Const ForAppending = 8

    If DEBUG_MODE Then
        Dim objFSO
        Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
        Set g_objDbgFile = objFSO.OpenTextFile(Server.MapPath(DBGOUT_FILE), ForAppending, True, -1)

		If Err.number = 0 Then
			g_objDbgFile.WriteLine
			g_objDbgFile.WriteLine "---------------------------------------------------------------"
			g_objDbgFile.WriteLine "BEGIN " & Server.MapPath(DEBUG_FILE) & " at " & CStr(Now())
        End If
    End If
End Sub


Sub DebugOut(strMessage)
    If DEBUG_MODE Then
        g_objDbgFile.WriteLine strMessage
        g_objDbgFile.WriteLine
    End If
End Sub


Sub DebugDone()
    If DEBUG_MODE Then
        g_objDbgFile.WriteLine "END " & Server.MapPath(DEBUG_FILE) & " at " & CStr(Now())
        g_objDbgFile.Close
    End If
End Sub


Sub ReportXMLError(strContext, xmlErrNode)
    Response.Write xmlErrNode.xml

    If DEBUG_MODE Then
        g_objDbgFile.WriteLine strContext
        g_objDbgFile.WriteLine "  Number:  0x" & Hex(xmlErrNode.getAttribute("ID"))
        g_objDbgFile.WriteLine "  Reason:  " & xmlErrNode.text
        g_objDbgFile.WriteLine "  Source:  " & xmlErrNode.getAttribute("SOURCE")
        g_objDbgFile.WriteLine
    End If
End Sub


Sub ReportObjError(strContext, nCode, strSource, strDesc)
    Response.Write getErrorXML(nCode, strSource, strDesc)

    If DEBUG_MODE Then
        g_objDbgFile.WriteLine strContext
        g_objDbgFile.WriteLine "  Number:  0x" & Hex(nCode)
        g_objDbgFile.WriteLine "  Reason:  " & strDesc
        g_objDbgFile.WriteLine "  Source:  " & strSource
        g_objDbgFile.WriteLine
    End If
End Sub


Sub ReportASPError(strDesc)
    Response.Write getErrorXML(-1, Server.MapPath(DEBUG_FILE), strDesc)

    If DEBUG_MODE Then
        g_objDbgFile.WriteLine "ASP Error:  " & strDesc
        g_objDbgFile.WriteLine
    End If
End Sub

%>