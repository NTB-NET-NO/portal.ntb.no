<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->


<%
Const L_Refresh_Text				= "Publish Profiles" 'Used as page title
Const L_RefreshSucceeded_Text		= "All profiles published to production successfully!"
                                                 'Response - Successful
Const L_RefreshFailed_Text			= "Failed to publish profiles."
                                                 'Response - Error
Const L_ProfileRefreshWidget_Text	= "Publishing profiles... " '
Const L_ProfileRefreshStatus1_Text	= "    Waiting.. "
Const L_ProfileRefreshStatus2_Text	= "    Published to production "
Const L_ProfileRefreshStatus3_Text	= "    Publish failed "
Const L_PortNumber_Text				= "Port Number"
Const L_WebserverName_Text			= "Web Server"
Const L_WebserverNumber_Text		= "Web Server#"
Const L_ApplicationName_Text		= "Application Name"
Const L_ApplicationStatus_Text		= "Application Status"
Const L_SiteName_Text				= "Site Name"
   

Dim oPS, MSCSPage
Dim bSuccess
Dim sError
Dim sWebServerName , sServerName , sSecureHost
Dim sWebServerMachine ,sWebServerFullName 
Dim sWebServerInstance
Dim sVirtualRootName
Dim sFullAppPath
Dim sFullHttpPath
Dim sFullHttpRefreshPath
Dim iDim , sIndex
Dim sWebSrvObj
Dim sBizDeskRoot
Dim iIndex
Dim sUrlReturnTo 
Dim bRefreshed 
Dim bStatus
Dim sXML
Dim sServer
Dim sSiteResourceName
Dim vStatusIndex 

On Error Resume Next
Set MSCSPage = Server.CreateObject("Commerce.Page")

'bSuccess = RefreshCatalogSets()        'Check for Err Handling 
'bSuccess = RefreshProviderConnection() 'Check Err Handling here

Call RefreshSessionCache() 

bSuccess   = False
bStatus    = False

'Check for Empty ...This is for implementation of status info
sIndex = Request.QueryString("sIndex")
Rem Add empty check for this var.
bRefreshed = Request.QueryString("bRefreshed")
Rem Empty check for brefreshed
If IsEmpty(bRefreshed) Then 
	 bRefreshed = False
End If

bStatus = Request.QueryString("bStatus")
Rem Empty check for bstatus
If IsEmpty(bStatus) Then 
	bStatus = False
End If

'wPath = Request.Form("sWebPath")
sServer = Request.Form("sServer")

If Request.Form("sSessionReset") Then
   ResetSessionRefresh()
End IF   


'Enumerates the list of Web Servers in the Web Farm which is then displayed in the 
'List HTC in the follwoing order { Server Number, Host M/C Name, Web Server Name ,
'Status of refresh of WebServers in the current Session.

Function GetServerList
	Dim sSiteName
	Dim sServerPath
	Dim vRoot
	Dim sSiteResourceName
	Dim vStatusIndex
	Dim sServerPort , sPort
	Dim iCnt
	Dim bEmpty
	Dim gIndex
	Dim ulimit
	Dim iServers
	Dim sTotal
	Dim MSCSConfiguration
	
	gIndex = 0
	sTotal = 0
    
	On Error Resume Next

	Dim vRootList
	
	sBizDeskRoot = Application("MSCSBizDeskRoot")
	sSiteName = Application("MSCSSiteName")

    vStatusIndex = Session("StatusArray")
    bEmpty = IsEmpty(vStatusIndex)
        		
'	g_MSCSConfiguration.Initialize(sSiteName)  
	Set MSCSConfiguration = CreateObject("Commerce.SiteConfigReadOnly")
	'MSCSConfiguration.Initialize(CStr(sSiteName))
	vRootList = MSCSConfiguration.GetAppsInSite(CStr(sSiteName)) 
	Set MSCSConfiguration = nothing 
	If Err.number <> 0 then 
	 '  Err.Clear()
	   GetServerList = GetBlankXML()   
	   Exit Function
	End IF   
	
	
	'Calculate total number of Servers here 
	For Each vRoot In vRootList
	   If vRoot <> sBizDeskRoot Then 
	'	iServers   = g_MSCSConfiguration.Fields(CStr(vRoot)).Value.Fields(CStr("s_NumberOfServers")).Value
		iServers = GetSiteConfigField(CStr(vRoot), "s_NumberOfServers")
		sTotal = sTotal + iServers
	   End If
    Next	
	
	
	sXML = "<document>"
	
	For Each vRoot In vRootList
	   If vRoot <> sBizDeskRoot Then 
	   'Except for this Bizdesk root we need to enumerate all the applications with their servers 
	   'running under this site .We kind of need another loop on top of this 
		
		sSiteResourceName = vRoot
	   'Major Enumeration ..Create XML stuff here 		
		sWebServerMachine  = g_MSCSConfiguration.GetIfCollection(g_MSCSConfiguration.Fields(CStr(sSiteResourceName)).Value.Fields(CStr("s_WebServerMachine")).Value)
		sWebServerInstance = g_MSCSConfiguration.GetIfCollection(g_MSCSConfiguration.Fields(CStr(sSiteResourceName)).Value.Fields(CStr("s_WebServerInstance")).Value)
		sWebServerName     = g_MSCSConfiguration.GetIfCollection(g_MSCSConfiguration.Fields(CStr(sSiteResourceName)).Value.Fields(CStr("s_WebServerName")).Value)
		sWebServerFullName = g_MSCSConfiguration.GetIfCollection(g_MSCSConfiguration.Fields(CStr(sSiteResourceName)).Value.Fields(CStr("s_WebServerFullName")).Value)
		sVirtualRootName   = g_MSCSConfiguration.Fields(CStr(sSiteResourceName)).Value.Fields(CStr("s_VirtualRootName")).Value
		sServerPort        = g_MSCSConfiguration.GetIfCollection(g_MSCSConfiguration.Fields(CStr(sSiteResourceName)).Value.Fields(CStr("s_ServerBindings")).Value)
		'sSecureHost        = g_MSCSConfiguration.Fields(CStr(sSiteResourceName)).Value.Fields(CStr("s_SecureHostName")).Value
	 
	 	iDim = UBound(sWebServerMachine) 
 		ReDim sFullAppPath(iDim)
		Redim sFullHttpPath(iDim)
		ReDim sFullHttpRefreshPath(iDim)
		'Update status
    
		If  bEmpty Then
			'Refresh Waiting
			Redim vStatusIndex(sTotal-1)
			For iCnt = 0 To sTotal 
				vStatusIndex(iCnt)= L_ProfileRefreshStatus1_Text
			Next
		Else
			ReDim Preserve vStatusIndex(sTotal-1)
		End If
        
	    If (bRefreshed = "True" )  And ( Not IsEmpty(sIndex)) Then
			vStatusIndex(sIndex-1) = L_ProfileRefreshStatus2_Text
		Else
			If (bRefreshed = "False" ) And ( Not IsEmpty(sIndex)) Then
				vStatusIndex(sIndex -1) = L_ProfileRefreshStatus3_Text
			End If	
		End If  

		For iIndex = LBound(sWebServerMachine) To UBound(sWebServerMachine)
		    Dim sPorts
		    Dim idx
		    'sFullAppPath(iIndex) = "IIS://" & CStr(sWebServerMachine(iIndex)) & "/W3SVC/" & CStr(sWebServerInstance(iIndex)) & "/ROOT/" & CStr(sVirtualRootName)  
			sPorts = Split(sServerPort(iIndex),":")
			If UBound(sPorts) > 0 Then sPort = sPorts(1)
			
			if CStr(sVirtualRootName) = "" then
				sFullHttpPath(iIndex) = "http://" & CStr(sWebServerMachine(iIndex)) & ":" & sPort & "/"
			else
				sFullHttpPath(iIndex) = "http://" & CStr(sWebServerMachine(iIndex)) & ":" & sPort & "/" & CStr(sVirtualRootName) & "/"
			end if
			sFullHttpRefreshPath(iIndex) = sFullHttpPath(iIndex) & "RefreshApp.asp"   
			sServerPath = CStr(sWebServerMachine(iIndex)) '& "/" & CStr(sVirtualRootName)
			gIndex = gIndex + 1    
	    
			sXML = sXML & "<record><serverinstance>" & gIndex  & "</serverinstance>" 
			sXML = sXML & "<site>" & sSiteName & "</site>"    
			sXML = sXML & "<port>" & sPort & "</port>" 
			sXML = sXML & "<application>" & sSiteResourceName & "</application>"    
			sXML = sXML & "<webserver>" & sServerPath & "</webserver>"  
			sXML = sXML & "<refreshstatus>" & CStr(vStatusIndex(gIndex-1)) & "</refreshstatus></record>"
		Next
		
		End If 
	Next
	
	'Return StatusArry  to Session 
    Session("StatusArray") =  vStatusIndex 
    
	'End of Enumeration of All Apps and their web Servers
	sXML = sXML & "</document>"
	GetServerList = sXML

End Function


Sub RefreshSessionCache()

 Session("MSCSProfileSchemaVer")= ""
 Session("MSCSCatalogVer") = ""
 Session("MSCSUserXmlVer") = ""
 Session("MSCSSiteTermVer")= ""

 Set Session("MSCSProviderConn")     = Nothing		
 Set Session("MSCSCatalogSets")      = Nothing

End Sub


Function GetNumberOfWebServers()
   On Error Resume Next
   Dim vRootList
   Dim sTotal
   sBizDeskRoot = Application("MSCSBizDeskRoot")
   sSiteName = Application("MSCSSiteName")
   sTotal = 0
        		
   g_MSCSConfiguration.Initialize(sSiteName)  
   vRootList = g_MSCSConfiguration.GetAppsInSite(CStr(sSiteName))  
   For Each vRoot In vRootList
	   If vRoot <> sBizDeskRoot Then 
		sNumberOfServers   = g_MSCSConfiguration.Fields(CStr(vRoot)).Value.Fields(CStr("s_NumberOfServers")).Value
		sTotal = sTotal + sNumberOfServers
	   End If
   Next	
   On Error Goto 0
   GetNumberOfWebServers = sTotal 
End Function

Function RefreshCatalogSets()
	Dim objCatalogSets
	dim mscsCatalogConnStr,mscsTransactionConfigConnStr	
	
	On Error Resume Next
		mscsTransactionConfigConnStr = GetSiteConfigField("Transaction Config", "connstr_db_TransactionConfig")   
		mscsCatalogConnStr = GetSiteConfigField("product catalog", "connstr_db_Catalog")
		set objCatalogSets = Server.CreateObject("Commerce.CatalogSets")
		objCatalogSets.Initialize mscsCatalogConnStr, mscsTransactionConfigConnStr
		set Session("MSCSCatalogSets") = objCatalogSets.GetCatalogSets
		
		If Err.number <> 0 then
			Set Session("MSCSCatalogSets") = Nothing
			RefreshCatalogSets = False
			Exit Function
		End If
		Set objCatalogSets = Nothing
			
	On Error Goto 0
		RefreshCatalogSets = True
End Function		


Function RefreshProviderConnection()
    dim cn
    dim connstr
    connstr= GetSiteConfigField("Biz Data Service", "s_CommerceProviderConnectionString") 
	set cn = Server.CreateObject("ADODB.Connection")
	if (cn.State = AD_STATE_CLOSED) then
	On Error Resume Next
	cn.Open CStr(connstr)
			
	If Err.number <> 0 then
		rem Call ShowErrDlg?
	    call Abort(L_BadConnection_ErrorMessage) 
	'	Response.Write L_BadConnection_ErrorMessage
	'	Response.End
		RefreshProviderConnection = False
		Exit Function
	End If
	Err.Clear
	if IsObject(Session("MSCSProviderConn")) then 
		set Session("MSCSProviderConn") = Nothing
		set Session("MSCSProviderConn") = cn
	end if	
	RefreshProviderConnection = True
	On Error Goto 0	
	end if

End Function


Function ResetWebServer( sServerPath )
   On Error Resume Next
   Rem To be implemented	    
   On Error Goto 0
   ResetWebServer = True 
End Function

Function ResetSessionRefresh
   Dim uLimit
   Dim statArray
   Dim iCnt
   On Error Resume Next
   statArray = Session("StatusArray")
   uLimit = UBound(Session("StatusArray"))
   For iCnt = 0 To uLimit
		statArray(iCnt)= L_ProfileRefreshStatus1_Text
   Next
   Session("StatusArray") = statArray
          
   On Error Goto 0
   If Err.number <> 0 Then
      ResetSessionRefresh = False
   Else
      ResetSessionRefresh = True
   End If   
End Function

Function  GetBlankXML()
	Dim sXML
	sXML= "<document><record/></document>"
	GetBlankXML = sXML
End Function

Function GetComputerName()
    Dim objWshShell, objEnv
    
	Set objWshShell = Server.CreateObject("Wscript.Shell")
	Set objEnv = objWshShell.Environment("Process")
	GetComputerName = objEnv("COMPUTERNAME")
End Function 
 
If Err.number = 0 Then
   bSuccess = True
  'Do something
Else
   sError = Err.description
   Response.Write sError
End If

On Error Goto 0
%>

<HTML>
<HEAD>
<TITLE ID='L_Refresh_HTMLTitle'>Publish Profiles</TITLE>
<SCRIPT LANGUAGE="VBScript">

   Sub Initialize
   		DisableTask "new"
		DisableTask "refresh"			
   End sub

   Sub UnSelectRefresh
	   'Disable buttons
		SetStatusText ""
		DisableTask "refresh"
		DisableTask "new"
		refreshform.bNeedRefresh.value = "False"
		refreshform.sWebPath.value = ""
		
	End Sub
	
	Sub SelectRefresh
		
		Dim oSelection
		Dim sUrlReturnTo
	    Dim iServIndex
	    Dim sPort
		set oSelection = window.event.XMLrecord
		SetStatusText ""
		EnableTask "refresh"
		'EnableTask "new"
		DisableTask "new"
		refreshform.bNeedRefresh.value = "True" 
		sPort = oSelection.SelectSingleNode(".//port").text
		if oSelection.SelectSingleNode(".//application").text = "" then
			refreshform.sWebPath.value = "http://" & oSelection.SelectSingleNode(".//webserver").text & ":" & sPort & "/" & "RefreshApp.asp"
		else
			refreshform.sWebPath.value = "http://" & oSelection.SelectSingleNode(".//webserver").text & ":" & sPort & "/" & oSelection.SelectSingleNode(".//application").text & "/" & "RefreshApp.asp"
		end if
		refreshform.sServIndex.value =  oSelection.SelectSingleNode(".//serverinstance").text 
		if oSelection.SelectSingleNode(".//application").text = "" then
			restartform.sWebPath.value = "http://" & oSelection.SelectSingleNode(".//webserver").text & ":" & sPort & "/" & "RefreshApp.asp"
		else
			restartform.sWebPath.value = "http://" & oSelection.SelectSingleNode(".//webserver").text & ":" & sPort & "/" & oSelection.SelectSingleNode(".//application").text & "/" & "RefreshApp.asp"
		end if
		'alert refreshform.sWebPath.value
		refreshform.sServer.value = oSelection.SelectSingleNode(".//webserver").text
		restartform.sServer.value = oSelection.SelectSingleNode(".//webserver").text
		refreshform.sUrlReturnTo.value = "<%=MSCSPage.URLPrefix()%><%=Request.ServerVariables("SCRIPT_NAME")%>"
		'alert refreshform.sUrlReturnTo.value	
	End Sub
	
	Sub RefreshClick
		Dim sUrlReturnTo
		sUrlReturnTo = "<%=MSCSPage.URLPrefix()%><%=Request.ServerVariables("SCRIPT_NAME")%>"
		'alert sUrlReturnTo
        'Setting action route 
		refreshform.action = refreshform.sWebPath.value & "?bNeedRefresh=" & "True" & "&return_to=" & sUrlReturnTo & "&sIndex=" & refreshform.sServIndex.value
		'alert refreshform.action
		refreshform.Submit()	
	End Sub
	
	Sub RestartClick
        Dim sUrlGoTo 
        Dim bRet  
     	sUrlGoTo = "<%=MSCSPage.URLPrefix()%><%=Request.ServerVariables("SCRIPT_NAME")%>"
		'alert sUrlGoTo
        'Setting action route 
        restartform.sSessionReset.value = True  
		restartform.action = sUrlGoTo
		'alert refreshform.action
		restartform.Submit()	
    End Sub
			
	
</SCRIPT>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<body SCROLL=no LANGUAGE='VBScript' ONLOAD='Initialize'>

</HEAD>
<BODY>

<%
InsertTaskBar L_Refresh_Text, " "	
%>

<div id="bdcontentarea">	
<DIV ID='lsRefresh'
	CLASS='listSheet' STYLE="MARGIN: 20px; HEIGHT: 80%"
	MetaXML='metaProfRefresh'
	DataXML='dtRefreshProfile'
	LANGUAGE='VBScript'
	OnRowSelect='SelectRefresh()'
	OnRowUnSelect='UnSelectRefresh()'
	><%=L_ProfileRefreshWidget_Text%></DIV>
 
<p>


<xml id='metaProfRefresh'>
	<listsheet>
	   <global selectall='no' curpage='1' pagesize='20'/>    
		<columns>
		    <column id='serverinstance'><%= L_WebserverNumber_Text %></column>
		    <column id='site'><%=L_SiteName_Text%></column>
		    <column id='port' hide='yes'><%=L_PortNumber_Text%></column> 
		    <column id='application'><%=L_ApplicationName_Text%></column>
	        <column id='webserver'><%=L_WebserverName_Text%></column>
	        <column id='refreshstatus'><%=L_ApplicationStatus_Text%></column>
	    </columns>
	    <operations>
			<findby formid="findbyform"/>
	    </operations>
	</listsheet>
</xml>

<xml id='dtRefreshProfile'> 
	<%=GetServerList %>
</xml> 




<FORM method="POST" ID="refreshform" name="refreshform" OnTask="RefreshClick()">
<INPUT TYPE=HIDDEN  ID='bRefreshed'  name='bNeedRefresh' >
<INPUT TYPE=HIDDEN  ID='sWebPath'    name='sWebPath' >
<INPUT TYPE=HIDDEN  ID='sServer'    name='sServer' >
<INPUT TYPE=HIDDEN  ID='sPort'    name='sPort' >
<INPUT TYPE=HIDDEN  ID='sServIndex'    name='sServIndex' >
<INPUT TYPE=HIDDEN  ID='sUrlReturnTo' name='sUrlReturnTo' >
</FORM>

<FORM method="POST" ID="restartform" name="restartform" OnTask="RestartClick()">
<INPUT TYPE=HIDDEN  ID='sWebPath'    name='sWebPath' >
<INPUT TYPE=HIDDEN  ID='sServer'    name='sServer' >
<INPUT TYPE=HIDDEN  ID='sSessionReset'    name='sSessionReset' >
</FORM>

</div>
</BODY>
</HTML>

