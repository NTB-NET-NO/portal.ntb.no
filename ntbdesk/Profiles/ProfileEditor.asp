<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<%
Const L_Profile_Text    = "Profile:  %1"		' Module-page title for profile editing.
Const L_SiteTerms_Text  = "Site Terms Editor"   ' Site-terms display-name.

Const L_LoadProfile_ErrorMessage   = "An error occurred while loading the Profile."
Const L_LoadSiteTerms_ErrorMessage = "An error occurred while loading the Site-terms."
Const L_LoadCatalog_ErrorMessage   = "An error occurred while loading the catalog."
%>

<!--#INCLUDE FILE='../include/BizDeskUtil.asp' -->
<!--#INCLUDE FILE='BDAOUtil.asp' -->

<HTML>
<HEAD>
<TITLE ID='L_ProfEdit_HTMLTitle'>BizDesk Profile Editor</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<!--#INCLUDE FILE='XmlHttpUtil.vbs' -->

</HEAD>
<%
Dim strProfPath
Dim strProfType
Dim xmlProfile, xmlSiteTerms, xmlCatalog, xmlError, xmlCustomAttributes

Dim strTitle
Dim strProfDisplayName
Dim rgstrArgs(0)

' Get arguments submitted to the page.

' profType = "profile" (default) | "siteterm"
If Request.QueryString("profType").Count = 1 Then
    strProfType = Request.QueryString("profType")
End If
If strProfType = "siteterm" Then
    ' It's site-terms.  We know the path to that.
    strProfPath = SITETERMS_COMMERCE_PATH
Else
    ' It's a profile.  Get the path to the profile from the POST arguments.
    strProfType = "profile"
    strProfPath = Request.Form("profPath")
End If

' Try to retrieve the requested profile.

Set xmlProfile = getProfileXML(strProfPath)
Set xmlError = xmlProfile.selectSingleNode("ERROR")
If Not (xmlError Is Nothing) Then
    ' An error occurred loading the profile
    OutputXMLError L_LoadProfile_ErrorMessage, xmlError
    Response.End
End If

' Try to retrieve site-terms.

If strProfType = "siteterm" Then
    ' We already loaded site-terms once.
    Set xmlSiteTerms = xmlProfile
Else
    Set xmlSiteTerms = getProfileXML(SITETERMS_COMMERCE_PATH)
End If
Set xmlError = xmlSiteTerms.selectSingleNode("ERROR")
If Not (xmlError Is Nothing) Then
    ' An error occurred loading the site-terms
    OutputXMLError L_LoadSiteTerms_ErrorMessage, xmlError
    Response.End
End If


' Try to retrieve the list of profiles in the catalog

Set xmlCatalog = getCatalogsXML()
Set xmlError = xmlCatalog.selectSingleNode("ERROR")
If Not (xmlError Is Nothing) Then
    ' An error occurred loading the catalogs
    OutputXMLError L_LoadCatalog_ErrorMessage, xmlError
    Response.End
End If


' Figure out what string(s) to use for the task-bar, then make the task-bar.

If strProfType = "profile" Then
    ' Get profile display-name and put it into the task-bar area.
    strProfDisplayName = xmlProfile.selectSingleNode(gc_sDocumentTag & "/" & _
		gc_sCatalogTag & "/" & gc_sProfileTag).getAttribute("displayName")
    rgstrArgs(0) = strProfDisplayName
    strTitle = sFormatString(L_Profile_Text, rgstrArgs)
Elseif strProfType = "siteterm" Then
    ' Use the hard-coded site-terms display-name for the title.
    strProfDisplayName = L_SiteTerms_Text
    strTitle = L_SiteTerms_Text
End If


If strProfType = "profile" Then
	' Try to load the Custom Attributes
	xmlCustomAttributes = xmlLoadCustomAttributes(Server.MapPath("CustomAttributes.xml"))
End If

%>

<BODY>
<SCRIPT LANGUAGE='xml' ID="xmlisProfData">
<%
' Write in the XML for the specified profile.
Response.Write xmlProfile.xml
%>
</SCRIPT>
<SCRIPT LANGUAGE='xml' ID="xmlisSiteTermsData">
<%
' Write in the XML for the site-terms.
Response.Write xmlSiteTerms.xml
%>
</SCRIPT>
<SCRIPT LANGUAGE='xml' ID="xmlisCatalogData">
<%
' Write in the XML for the list of profiles in the catalog.
Response.Write xmlCatalog.xml
%>
</SCRIPT>

<SCRIPT LANGUAGE='xml' ID="xmlisPDConfig">
<%
' ProfileDesigner config XML
Response.Write "<profiledesigner docid='xmlisProfData' sitetermsid='xmlisSiteTermsData' " & vbCr
Response.Write "  catalogid='xmlisCatalogData' type='" & strProfType & "'>" & vbCr
If strProfType = "profile" Then
  Response.Write xmlCustomAttributes & vbCr
End If
Response.Write "<helppath>" & g_sBizDeskRoot & "</helppath>" & vbCr
Response.Write "</profiledesigner>" & vbCr
%>
</SCRIPT>


<%
InsertEditTaskBar strTitle, ""
%>

<FORM ID="formSaveProfile" NAME="formSaveProfile" METHOD="POST" ontask="SaveProfile()">
</FORM>

<DIV ID='divProfDesigner' CLASS='clsProfDesigner' ConfigXML='xmlisPDConfig'
    STYLE='width:100%;height:100%;padding-bottom:20px;behavior:url(/widgets/profilebldrhtc/ProfileDesigner.htc)'
    onChange='PD_ChangeHandler()' onStatus='PD_StatusHandler()'>
<H3 ID='L_LoadEditor_Text' ALIGN='center'>Loading the editor, please wait.</H3>
</DIV>


<SCRIPT FOR='window' EVENT='onload' LANGUAGE='VBScript'>
    ' Do one-time initialization.

    Const L_Close_Button = "Close" 
    Const L_Save_Button  = "Save"
    Const L_SaveBack_Button = "Save and close."

    g_strProfType = "<%= strProfType %>"
    g_strProfPath = "<%= strProfPath %>"
    g_strProfDisplayName = "<%= strProfDisplayName %>"

    ' Set up task buttons.

	setTaskTooltip "save", L_Save_Button
	setTaskTooltip "back", L_Close_Button
	setTaskTooltip "saveback", L_SaveBack_Button
		
</SCRIPT>


<SCRIPT LANGUAGE="VBScript">
Option Explicit

Const L_ProfileDirty_Text  = "Profile changes made but not saved"
Const L_SiteTermDirty_Text = "Site-Term changes made but not saved"
Const L_ProfileSaved_Text  = "Profile saved"
Const L_SiteTermSaved_Text = "Site-Terms saved"

Const L_SaveFailed_ErrorMessage     = "An error occurred while saving the Profile."

Dim g_strProfPath
Dim g_strProfDisplayName
Dim g_strProfType


' This sub gets called when the Profile-Designer reports a change in the
' profile document.  It simply enables the "Save" taskbutton.
Sub PD_ChangeHandler()
    Dim strStatusText

    If g_strProfType = "siteterm" Then
        strStatusText = L_SiteTermDirty_Text
    Else
        strStatusText = L_ProfileDirty_Text
    End If
    SetStatusText strStatusText

    SetDirty("")
End Sub


' This sub gets called when the Profile-Designer reports a change in its
' current status.  The sub simply updates the BizDesk status area.
Sub PD_StatusHandler()
    SetStatusText window.event.getAttribute("statusText")
End Sub

Sub SaveProfile()
    Dim strSaveRequest
    Dim xmlRequest, xmlResponse, xmlProfile
    Dim strStatusText
    Dim elGroup, elProp
    Dim bError, elBtnBack

    EnableTask  "back"

    If divProfDesigner.DoApply Then
      'First check to make sure the profile is valid
      If Not(g_strProfType = "siteterm") Then
        If Not divProfDesigner.CheckValid Then
          'Profile is not valid -- don't save
          bError = True
        End If
      End If
      
      If Not bError Then
        ' Simplify the profile XML to the minimal amount that needs to be sent.
        ' This will keep IIS from whining.
    '    Set xmlRequest = SimplifyProfileDoc(xmlisProfData.XMLDocument)
        Set xmlRequest = xmlisProfData.XMLDocument  ' Take out simplification for now.
        If g_strProfType = "siteterm" Then
          Set xmlProfile = xmlRequest.selectSingleNode("//Profile")
          xmlProfile.setAttribute "isProfile", "0"
        End If
        
        ' Create the request body.
        strSaveRequest = "BizDataServices.asp?profOper=updateProfile"
        strSaveRequest = strSaveRequest & "&profPath=" & sURLEncode(g_strProfPath)
        
        ' Send the request.  Get a response.  Whoa, rocket-science.
        Set xmlResponse = xmlPOSTToServer(strSaveRequest, xmlRequest)

        ' Analyze the response now.
        ' If no errors occurred, hooray!
        If Not(xmlResponse Is Nothing) Then
            ' Update task-buttons, and update Profile-Designer since there may be
            ' some new info returned by the BizDataServices ASP page.

            xmlisProfData.XMLDocument.loadXML xmlResponse.documentElement.xml
            divProfDesigner.Refresh
            divProfDesigner.isDirty = False
           	g_bDirty = False 

            If g_strProfType = "siteterm" Then
                strStatusText = L_SiteTermSaved_Text
            Else
                strStatusText = L_ProfileSaved_Text
            End If
            SetStatusText strStatusText
            
            'If we were supposed to go back, do it now
	          If parent.g_bCloseAfterSave Then
	          	' Save and exit chosen and no errors while saving
	          	Set elBtnBack = elGetTaskBtn("back")
	          	If Not elBtnBack Is Nothing Then
	          		If Not elBtnBack.disabled Then
	          		 Call elBtnBack.click()
	          	  End If
	          	End If
	          End If
             
        Else
          SetStatusText L_SaveFailed_ErrorMessage 
        End If
      Else
        SetStatusText L_SaveFailed_ErrorMessage
      End If
    Else
      EnableTask "save"
      EnableTask "saveback"
    End If

    
  	parent.g_bCloseAfterSave = False

End Sub


'----------------------------------------------------------------------------
' SimplifyProfileDoc
'
' Description:
'     This function takes the profile XML document and simplifies it in
'     preparation to be sent across the wire.  This is done because profiles
'     are large, and IIS complains when you POST large amounts of data to it.
'     This function trims out all nodes that don't need to be sent.  Nodes
'     that need to be sent are the following:
'         1)  Nodes with { @isNew | @isModified | @isDeleted } = '1'
'         2)  Siblings of (1).
'         3)  Descendants of (1).
'         4)  All nodes between (1) and the document root.
Function SimplifyProfileDoc(xmlStartingDoc)
    Dim xmlOutputDoc        ' Output profile doc, simplified.
    Dim xmllistNodes        ' List of XML nodes.
    Dim xmlModNode, xmlTemp ' For temp use.
    Dim xmllistDescendants

    ' Clone the document so we don't mess up the original.
    Set xmlOutputDoc = CreateObject("Microsoft.XMLDOM")
    Set xmlOutputDoc.documentElement = xmlStartingDoc.documentElement.cloneNode(True)

    ' Get all nodes that need to be sent.
    Set xmllistNodes = xmlOutputDoc.selectNodes("//*[@isNew='1' || @isModified='1' || @isDeleted='1']")

    ' These nodes need to be saved, and so do their parent-nodes.  Mark them
    ' as such, with a throw-away attribute that BDAO will ignore.
    For Each xmlModNode In xmllistNodes
        ' Keep this node!  (DUH...)
        xmlModNode.setAttribute "bKeep", "1"

        ' Keep all of the node's descendants.
        Set xmllistDescendants = xmlModNode.selectNodes(".//*")
        For Each xmlTemp In xmllistDescendants
            xmlTemp.setAttribute "bKeep", "1"
        Next

        ' Keep all of the node's siblings.  This is to preserve the ordering
        ' of profile elements.
        Set xmlModNode = xmlModNode.parentNode
        For Each xmlTemp In xmlModNode.childNodes
            xmlTemp.setAttribute "bKeep", "1"
        Next

        Do
            xmlModNode.setAttribute "bKeep", "1"
            Set xmlModNode = xmlModNode.parentNode
        Loop While Not (xmlModNode Is xmlOutputDoc)
    Next

    ' Now that all necessary nodes are marked, delete unnecessary ones.
    ' (I guess this piece of code could be called the b-Keeper... DOH!)

    Set xmllistNodes = xmlOutputDoc.selectNodes("//*[not(@bKeep)]")
    For Each xmlTemp In xmllistNodes
        xmlTemp.parentNode.removeChild xmlTemp
    Next

    ' Finally, remove the bKeep attribute from all nodes.

    Set xmllistNodes = xmlOutputDoc.selectNodes("//*")
    For Each xmlTemp In xmllistNodes
        xmlTemp.removeAttribute "bKeep"
    Next

    Set SimplifyProfileDoc = xmlOutputDoc
End Function

</SCRIPT>

</BODY>
</HTML>
