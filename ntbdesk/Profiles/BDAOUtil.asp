<%
' These are the currently-defined catalog names for Commerce Server 4.0.
Const PROFILE_CATALOG         = "Profile Definitions"
Const SITETERMS_CATALOG       = "Site Terms"
Const SITETERMS_COMMERCE_PATH = "Site Terms.MSCommerce"

' Localizable String
Const L_ErrCode_Text = "Error Code:"
Const L_ErrDescription_Text = "Description:"
Const L_ErrSource_Text = "Source:"
Const L_ConnectionFailure_Text = "Connection Failure"

' XML tag-name constants:
Const gc_sDocumentTag = "Document"
Const gc_sCatalogTag  = "Catalog"
Const gc_sProfileTag  = "Profile"
Const gc_sGroupTag    = "Group"
Const gc_sPropertyTag = "Property"


'----------------------------------------------------------------------------
' Function getBDAO
'
' Description:
'     Retrieves a BizDataAdmin object that is connected to the appropriate
'     BizDataStore.
'
'     This function is guaranteed to return an XML DOM with valid XML in
'     it.  (If an error occurs, error XML is returned.)
'----------------------------------------------------------------------------
Function getBDAO()
    Dim objBizData      ' BizDataAdmin object
    Dim strBDSConnStr   ' BizDataStore connection-string
    Set objBizData = Nothing

    On Error Resume Next

    If isObject(Session("BizData")) Then
        ' We already have a successfully-created, successfully-connected BDAO.

        Set objBizData = Session("BizData")
    Else
        ' We need to create and connect the BDAO.

        Set objBizData = Server.CreateObject("Commerce.BusinessDataAdmin")

        If Err.number = 0 Then
            ' Get connection string.
            If isEmpty(Session("BizDataConnStr")) Then
                strBDSConnStr = GetSiteConfigField("Biz Data Service", "s_BizDataStoreConnectionString")                
                Session("BizDataConnStr") = strBDSConnStr
                
            Else
                strBDSConnStr = Session("BizDataConnStr")
            End If
        End If

        If Err.number = 0 Then
            ' Everything is good so far...  Try to connect!
            objBizData.Connect CStr(strBDSConnStr)
        End If

        If Err.number = 0 Then
            ' Everything went well, so cache the BDAO.
            Set Session("BizData") = objBizData
        Else
            Set objBizData = Nothing
        End If
    End If

    Set getBDAO = objBizData
End Function


'----------------------------------------------------------------------------
' Function getProfileXML
'
' Description:
'     Retrieves the XML of an existing profile definition from the BDAO and
'     returns it as an XML DOM object.
'
'     This function is guaranteed to return an XML DOM with valid XML in
'     it.  (If an error occurs, error XML is returned.)
'----------------------------------------------------------------------------
Function getProfileXML(strProfilePath)
    Dim objBizData      ' BizDataAdmin object
    Dim xmlProfile
    Dim oParseError

    On Error Resume Next

    ' Get a configured BizDataAdmin object.
    Set objBizData = getBDAO()
    If Err.number <> 0 Then
        Set xmlProfile = Server.CreateObject("Microsoft.XMLDOM")
        xmlProfile.loadXML getErrorXML(Err.number, Err.source, Err.description)
    Else
        ' Get requested profile's XML.

        Set xmlProfile = objBizData.GetProfile(CStr(strProfilePath))
        If Err.number <> 0 Then
            Set xmlProfile = Server.CreateObject("Microsoft.XMLDOM")
            xmlProfile.loadXML getErrorXML(Err.number, Err.source, Err.description)
        Elseif xmlProfile.parseError.errorCode <> 0 Then
            Set oParseError = xmlProfile.parseError
            xmlProfile.loadXML getErrorXML(oParseError.errorCode, "BDAOUtil.asp", _
                oParseError.reason)
        End If
    End If

    ' If an error occurred, blow away the BDAO.
    If Err.number <> 0 Then Session("BizData") = Empty

    Set getProfileXML = xmlProfile
End Function


'----------------------------------------------------------------------------
' Function getCatalogsXML
'
' Description:
'     Retrieves the XML of the current catalog of profiles from the BDAO
'     and returns it as an XML DOM object.
'
'     This function is guaranteed to return an XML DOM with valid XML in
'     it.  (If an error occurs, error XML is returned.)
'----------------------------------------------------------------------------
Function getCatalogsXML()
    Dim objBizData      ' BizDataAdmin object
    Dim xmlCatalogs
    Dim oParseError

    On Error Resume Next

    ' Get a configured BizDataAdmin object.
    Set objBizData = getBDAO()
    If Err.number <> 0 Then
        Set xmlCatalogs = Server.CreateObject("Microsoft.XMLDOM")
        xmlCatalogs.loadXML getErrorXML(Err.number, Err.source, Err.description)
    Else
        ' Get catalogs XML.

        Set xmlCatalogs = objBizData.GetCatalogs()
        If Err.number <> 0 Then
            Set xmlCatalogs = Server.CreateObject("Microsoft.XMLDOM")
            xmlCatalogs.loadXML getErrorXML(Err.number, Err.source, Err.description)
        Elseif xmlCatalogs.parseError.errorCode <> 0 Then
            Set objParseError = xmlCatalogs.parseError
            xmlCatalogs.loadXML getErrorXML(objParseError.errorCode, "BDAOUtil.asp", _
                objParseError.reason)
        End If
    End If

    ' If an error occurred, blow away the BDAO.
    If Err.number <> 0 Then Session("BizData") = Empty

    Set getCatalogsXML = xmlCatalogs
End Function


'----------------------------------------------------------------------------
' Function getErrorXML
'
' Description:
'     Returns the XML representation for an error.
'
' Arguments:
'     hRes    = The HRESULT of the error.
'     strSrc  = The source of the error.
'     strDesc = The description of the error.
'
' Return Value:
'     The XML representation of the error, as a string.
'----------------------------------------------------------------------------
Function getErrorXML(hRes, strSrc, strDesc)
    hRes = hRes And 65535

    getErrorXML = "<ERROR ID='" & hRes & "' SOURCE='" & strSrc & "'>" & _
        "<![CDATA[" & strDesc & "]]></ERROR>"
End Function


Sub OutputXMLError(strContext, xmlErrorNode)
%>
<H2><%=strContext%></H2>
<P>
<TABLE>
<TR><TD><%=L_ErrCode_Text%><TD><%=xmlErrorNode.getAttribute("ID")%>
<TR><TD><%=L_ErrDescription_Text%><TD><%=xmlErrorNode.text%>
<TR><TD><%=L_ErrSource_Text%><TD><%=xmlErrorNode.getAttribute("SOURCE")%>
</TABLE>
<%
End Sub

'----------------------------------------------------------------------------
' sProfileXML2EditSheetXML
'
' Description:
'     Takes an XML node from the profile schema and translates it to the
'     corresponding editsheet record schema.  This basically involves taking
'     each attribute of the profile-node, and making the attribute into an
'     element with the contents being the attribute's value.  (This kind of
'     transformation is not currently possible in XSL.)
'----------------------------------------------------------------------------
Function sProfileXML2EditSheetXML(xmlProfElem)
    Dim strESXML    ' Editsheet XML
    Dim xmllistAttrs
    Dim xmlAttr

    strESXML = ""

    If Not (xmlProfElem Is Nothing) Then
        strESXML = "<document><record>"

        Set xmllistAttrs = xmlProfElem.attributes
        For Each xmlAttr In xmllistAttrs
            strESXML = strESXML & "<" & xmlAttr.name & ">" & _
                xmlAttr.value & "</" & xmlAttr.name & ">"
        Next

        strESXML = strESXML & "</record></document>"
    End If

    sProfileXML2EditSheetXML = strESXML
End Function


'----------------------------------------------------------------------------
' xmlLoadCustomAttributes
'
' Description:
'     Takes a URL to a file containing the custom attributes XML and
'     returns the XML string.
'----------------------------------------------------------------------------
Function xmlLoadCustomAttributes(strURL)
  Dim xmlDoc
  Dim bSuccess
  
  Set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")
  If Err.Number <> 0 Then
	xmlLoadCustomAttributes = ""
  Else
	xmlDoc.async = false
	bSuccess = xmlDoc.load(strURL)
	If bSuccess = true Then
		xmlLoadCustomAttributes = xmlDoc.documentElement.xml
	Else
		xmlLoadCustomAttributes = ""
	End If
  End If
  
End Function


%>