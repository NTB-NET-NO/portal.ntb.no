<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<%
On Error Resume Next

Const DEBUG_MODE = False         ' Set to False for production mode.
Const DEBUG_FILE = "BizDataServices.asp"    ' File being debugged.
Const DBGOUT_FILE = "bds.log"   ' File to dump debug messages to.

' Localizable strings
Const L_DebugBytesReceived_Text   = "Received %1 bytes of form data." '%1 is a number
Const L_DebugData_Text            = "Data:  '%1'"
Const L_DebugResetBDAO_Text       = "An error occurred in processing.  Resetting BDAO."
Const L_DebugRequestOper_Text     = "Requested operation:  '%1'"
Const L_DebugGetProfile_Text      = "Retrieved profile: '%1'"
Const L_DebugUpdateProfile_Text   = "Updating profile."
Const L_DebugSucceeded_Text       = "Update succeeded.  Refetching profile."
Const L_DebugFetchCatalog_Text    = "Refetching catalog."
Const L_DebugFetchedCatalog_Text  = "Fetched catalog: '%1'"
Const L_DebugGetCatalog_Text      = "Retrieved catalog: '%1'"
Const L_DebugCopyProfile_Text     = "Got source profile to copy: '%1'"
Const L_DebugNewProfile_Text      = "New profile: '%1'"
Const L_DebugCopySucceeded_Text   = "Copy succeeded.  Fetching catalogs."
Const L_DebugCopyFailed_Text      = "Copy Profile:  UpdateProfile failed."
Const L_DebugGetCatalogs_Text     = "Retrieved catalogs: '%1'"
Const L_DebugCreateProfile_Text   = "Creating new profile: '%1'"
Const L_DebugCreateSucceeded_Text = "Create succeeded.  Fetching catalogs."
Const L_DebugOperComplete_Text    = "Operation complete."

Const L_ASPMissingPostArg_ErrorMessage    = "Missing or multiple 'profOper' POST arguments."
Const L_ASPMultiplePostArg_ErrorMessage   = "Missing or multiple 'profPath' arguments."
Const L_ASPMultiplePostArg2_ErrorMessage  = "Missing or multiple 'profData' arguments."
Const L_ASPUnrecognizedOper_ErrorMessage  = "Unrecognized operation."

Const L_ObjXMLParse_ErrorMessage         = "Update profile XML parse error."
Const L_ObjUpdateFailed_ErrorMessage     = "An error occurred while updating the profile."
Const L_ObjCopyUpdateFailed_ErrorMessage = "Copy Profile:  UpdateProfile failed."

Const L_XMLGetProfFailed_ErrorMessage = "Copy Profile:  GetProfile failed."
Const L_FileSystemObject_ErrorMessage = "FileSystemObject Error"
%>
<!--#INCLUDE FILE='../include/ASPUtil.asp' -->
<!--#INCLUDE FILE='BDAOUtil.asp' -->
<!--#INCLUDE FILE='DebugUtil.asp' -->
<%
Dim strOperation        ' Operation to be performed by this page.
Dim objBDAO
Dim ErrorXML
Dim ErrorNode

DebugInit

If Err.number = 0 Then
	DebugOut sFormatString(L_DebugBytesReceived_Text, Split(Request.TotalBytes, ","))
	DebugOut sFormatString(L_DebugData_Text, Split(Request.QueryString, ","))

	If Request.QueryString("profOper").Count <> 1 Then
	    ReportASPError L_ASPMissingPostArg_ErrorMessage
	Else
	    ' Get a BizDataAdmin object that is configured and connected to the
	    ' BizDataStore.
	    Set objBDAO = getBDAO()   
	    
	    If Err.number <> 0 Then
	        DebugOut L_DebugResetBDAO_Text
	        Session("BizData") = Empty
	    Else
			doOperation objBDAO, Request.QueryString("profOper")
	    End If
	End If

	DebugDone
Else
	' an error occurred while trying to open the debug file, send error back to client
	ReportObjError L_FileSystemObject_ErrorMessage, Err.number, Err.Source, Err.Description
End If

Response.End

'----------------------------------------------------------------------------
' Sub doOperation
'
' Description:
'     This subroutine takes a configured bizdata admin object and performs
'     the requested operation.  Depending on the operation, this sub will
'     access the other Request.Form data POSTed to the page.
'----------------------------------------------------------------------------
Sub doOperation(objBizData, strOper)
    Dim xmlProfile, xmlNode
    Dim objParseError
    Dim strProfPath
    Dim rgstrPathParts
    Dim i

    DebugOut sFormatString(L_DebugRequestOper_Text, Split(strOper, ","))

    On Error Resume Next
    Err.Clear

    Select Case strOper

    Case "updateProfile"    ' Write specified profile data back to database.

        If Request.QueryString("profPath").Count <> 1 Then
            ReportASPError L_ASPMultiplePostArg_ErrorMessage
       ' Elseif Request.QueryString("profData").Count <> 1 Then
       '     ReportASPError L_ASPMultiplePostArg2_ErrorMessage
        Else
            ' Get necessary arguments.
            strProfPath = Request.QueryString("profPath")

            ' Load specified profile into an XML document.
            Set xmlProfile = Server.CreateObject("Microsoft.XMLDOM")
            xmlProfile.load Request
            If xmlProfile.parseError.errorCode <> 0 Then
                Set objParseError = xmlProfile.parseError
                ReportObjError L_ObjXMLParse_ErrorMessage, _
                    objParseError.errorCode, objParseError.url, objParseError.reason
            Else
                DebugOut sFormatString(L_DebugGetProfile_Text, Split(xmlProfile.xml, ","))

                DebugOut L_DebugUpdateProfile_Text

                objBizData.UpdateCatalog CStr(xmlProfile.xml)
                If Err.number <> 0 Then
                    ReportObjError L_ObjUpdateFailed_ErrorMessage, Err.number, Err.source, Err.description
                Else
                    DebugOut L_DebugSucceeded_Text

                    Set xmlProfile = getProfileXML(CStr(strProfPath))
                    Response.Write xmlProfile.xml

                    DebugOut sFormatString(L_DebugGetProfile_Text, Split(xmlProfile.xml, ","))
                End If
            End If
        End If

    Case "deleteProfile"    ' Delete specified profile or profiles.

        If Request.QueryString("profPath").Count = 0 Then
            ReportASPError L_ASPMultiplePostArg_ErrorMessage
        Else
            For Each strProfPath In Request.QueryString("profPath")
                objBizData.DeleteProfile CStr(strProfPath)
            Next

            DebugOut L_DebugFetchCatalog_Text

            ' Write new catalog of profiles out.
            Set xmlProfile = getCatalogsXML()
            Response.Write xmlProfile.xml

            DebugOut sFormatString(L_DebugGetCatalog_Text, Split(xmlProfile.xml, ","))
        End If

    Case Else
        ReportASPError L_ASPUnrecognizedOper_ErrorMessage

    End Select

    'Let BizDesk know that we have updated the schema
    Application.Lock()
    Application("MSCSProfileSchemaVer")  = Application("MSCSProfileSchemaVer") + Application("MSCSVerIncrement") 
    Application("MSCSUserXmlVer") = Application("MSCSUserXmlVer") + Application("MSCSVerIncrement")
	Application("MSCSSiteTermVer") = Application("MSCSSiteTermVer") + Application("MSCSVerIncrement")
    Application.UnLock() 

    DebugOut L_DebugOperComplete_Text
End Sub

%>
