<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!-- #INCLUDE FILE = "../include/bizdeskutil.asp" -->
<!-- #INCLUDE FILE = "./include/application_strings.asp" -->
<!-- #INCLUDE FILE = "./include/application_util.asp" -->

<%	
	Call ProcessListForm()
	
	Sub ProcessListForm()
		Dim sQuery, sMode, sDeleteType, aItems, iCount, aGroups, rs, xmlData

		' Create dictionary for form values
		Set g_dDataCodes = Server.CreateObject("Commerce.Dictionary")
		With g_dDataCodes
			.ListType = Request("type")
			If .ListType = "" Then .ListType = ORDER_STATUS
			.Page = 1
			.Sortcol = "u_name ASC"
			sQuery = BuildDataCodesQuery(.ListType, .Sortcol)
		End With

		' Process delete
		sMode = Request.Form("mode")
		sDeleteType = Request.Form("seltype")
		If sMode = "delete" Then			
			If sDeleteType = "all" Then
				DeleteDataCodes True, g_dDataCodes.ListType, aItems, aGroups
			Else
				' Create array(s) of selected items
				aItems = Split(Request.Form("key"))
				iCount = Ubound(aItems) + 1
				aGroups = Split(Request.Form("group"))
				DeleteDataCodes False, g_dDataCodes.ListType, aItems, aGroups
			End If			
			' Set status text
			If Err = 0 Then
				g_sStatusText = sFormatString(L_ItemDelete_StatusBar, Array(iCount))
			Else
				g_sStatusText = L_ErrorItemDelete_StatusBar
			End If			
		 End If

		' Build title text
		Select Case cInt(g_dDataCodes.ListType)
			Case ORDER_STATUS
				g_sKey = "i_Code"
				g_sTitleText = L_OrderStatusCodes_StaticText
			Case COUNTRY
				g_sKey = "u_Name"
				g_sTitleText = L_CountryCodes_StaticText
			Case STATE_REGION
				g_sKey = "u_Name"
				g_sTitleText = L_StateCodes_StaticText
		End Select

		' Create xml meta and data
		Set rs = GetRecordset(sQuery)
		If err <> 0 Then
			g_xmlCodeListData = "<document/>"
			g_xmlCodeListMeta = GetXMLCodeListMeta(g_dDataCodes)
		Else
			With g_dDataCodes			
				.RecCount = rs.RecordCount
				.StartRecord = (.Page-1)* PAGE_SIZE
				' Set up the xml meta and data
				g_xmlCodeListMeta = GetXMLCodeListMeta(g_dDataCodes)
				If .RecCount > 0 Then
					Set xmlData = xmlGetXMLFromRSEx(rs, .StartRecord, PAGE_SIZE, .RecCount, Null)
					g_xmlCodeListData = xmlData.xml
				else
					g_xmlCodeListData = "<document/>"
				End If
			End With
		End If
		Set xmlData = Nothing
		Session("EditMode") = ""
	End Sub
%>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
</HEAD>

<BODY SCROLL='NO'>

<!-- #INCLUDE FILE = "./include/application_select.vbs" -->

<% InsertTaskBar g_sTitleText, g_sStatusText %>

<SCRIPT LANGUAGE="VBScript">

	Option Explicit

    Sub window_OnLoad()
		' Handle task buttons
		EnableTask "new"
		DisableTask "open"
		EnableTask "view"
		EnableAllTaskMenuItems "view", true
    End Sub   

	Sub onTask()
		if window.event.srcElement Is elGetTaskBtn("delete") then
			call ConfirmDelete
		elseif window.event.srcElement Is elGetTaskBtn("open") then
			with selectform
				.mode.value = "open"
				.key.value = g_sKeyString1
				.group.value = g_sKeyString2
				.submit()
			end with
		else
			selectform.submit()
		end if
	End Sub

	Sub ConfirmDelete()
       	dim iListType, sTitle, sResponse, sText, bDelete
		
		iListType = "<%= g_dDataCodes.ListType %>"
		bDelete = false
		' Text for dialog title
		select case iListType
			case 1	' Order Status
				sTitle = "<%=L_OrderStatusCodes_StaticText%>"
			case 2	' Country
				sTitle = "<%=L_CountryCodes_StaticText%>"
			case 3	' State/Region
				sTitle = "<%=L_StateCodes_StaticText%>"
		end select
		' Special warning for deleting country with related states/regions
		if iListType = 2 then
			sResponse = MsgBox("<%=L_DeleteRelated_Message%>", vbOkCancel+vbExclamation, "<%=L_DeleteRelated_DialogTitle%>")
			if sResponse = vbOk then
				if selectform.seltype.value = "all" then
					sText = "<%= L_DeleteAll_Message%>"
				elseif g_nItemsSelected =1 then
					sText = sFormatString("<%= L_DeleteOne_Message%>",Array(g_nItemsSelected))
				elseif g_nItemsSelected >1 then
					sText = sFormatString("<%= L_DeleteMultiple_Message%>",Array(g_nItemsSelected))
				end if
				sResponse = MsgBox(sText, vbOKCancel, sTitle)
				if sResponse = vbOK then
					bDelete = true
				end if
			end if
		else	
			' Check for number of items selected; display dialog
			if g_nItemsSelected = 1 then
				sText = "<%= L_DeleteOne_Message%>"
				sResponse = MsgBox(sText, vbOKCancel, sTitle) 
			elseif selectform.seltype.value = "all" then
				sText = "<%= L_DeleteAll_Message%>"
				sResponse = MsgBox(sText, vbOKCancel, sTitle)
			elseif g_nItemsSelected > 1 then
				sText = sFormatString("<%= L_DeleteMultiple_Message%>",Array(g_nItemsSelected))
				sResponse = MsgBox(sText, vbOKCancel, sTitle)
			end if
			if sResponse = vbOK then
				bDelete = true
			end if
		end if
		' Check if okay to delete...
		if bDelete then
			with selectform
				.mode.value = "delete"
				.key.value = g_sKeyString1
				.group.value = g_sKeyString2
				.submit()
			end with
		else
			EnableTask "open"
			EnableTask "delete"
		end if
    End Sub

</SCRIPT>

	<xml id='lsCodesMeta'>
		<%= g_xmlCodeListMeta %>
	</xml>
								
	<xml id='lsCodesData'>
		<%= g_xmlCodeListData %>
	</xml>

<DIV ID="bdcontentarea">
	<DIV ID='lsCodes' CLASS='listSheet' STYLE='MARGIN:20px; HEIGHT:80%'
		DataXML='lsCodesData'
		MetaXML='lsCodesMeta'
		LANGUAGE='VBScript'
		OnRowSelect='OnSelectRow()'
		OnRowUnselect='OnUnSelectRow()'
		OnAllRowsUnselect='OnUnSelectAllRows()'
		OnAllRowsSelect='OnSelectAllRows()'>
		<%= L_LoadingList_Text%>
	</DIV>			
</DIV>

<FORM ID='selectform' METHOD='post' ONTASK='onTask()'>
	<input type='hidden' id='mode' name='mode'>
	<input type='hidden' id='seltype' name='seltype'>
	<input type='hidden' id='key' name='key'>
	<input type='hidden' id='group' name='group'>
</FORM>

<FORM ID='sortform' METHOD='POST' ACTION='datacodes_event.asp'>
	<input type='hidden' id='listtype' name='listtype' value="<%= g_dDataCodes.ListType %>">
</FORM>

</BODY>
</HTML>
