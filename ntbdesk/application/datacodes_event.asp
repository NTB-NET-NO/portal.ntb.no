<!--#INCLUDE FILE="../include/BDXMLHeader.asp" -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE="../include/DBUtil.asp" -->
<!--#INCLUDE FILE="../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="./include/application_util.asp" -->
	
<%	
	Dim g_dRequest, dDataCodes, sQuery, rs, xmlData
	
	' Get form values from dictionary
	Set g_dRequest = dGetRequestXMLAsDict()	
	Set dDataCodes = Server.CreateObject("Commerce.Dictionary")
	With dDataCodes
		.ListType = g_dRequest("listtype")
		.Page = g_dRequest("page")
		If IsEmpty(.Page) Then .Page = 1		
		.Sortcol = g_dRequest("column")
		If .Sortcol <> "" Then
			.Sortcol = .Sortcol & " ASC"
		Else
			.Sortcol = "u_Name" & " ASC"
		End If
	End With
	
	' Build query, convert to rs then to xml
	sQuery = BuildDataCodesQuery(dDataCodes.ListType, dDataCodes.Sortcol)
	Set rs = GetRecordset(sQuery)
	If Not rs.EOF Or rs.BOF Then
		rs.MoveFirst
		With dDataCodes			
			.RecCount = rs.RecordCount
			.StartRecord = (.Page-1)* PAGE_SIZE
			Set xmlData = xmlGetXMLFromRSEx(rs, .StartRecord, PAGE_SIZE, .RecCount, Null)
			If err = 0 Then
				Response.Write xmlData.xml
			End If
		End With
	Else
		Response.Write "<document/>"
	End If
	Set xmlData = Nothing
%>