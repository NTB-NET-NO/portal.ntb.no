<%
'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

'Dialog box strings
Const L_DataCodes_DialogTitle = "Data Codes"
Const L_DeleteCountry_DialogTitle = "Delete Country/Region"
Const L_DeleteRegion_DialogTitle = "Delete State/Province"
Const L_DeleteOrderStatus_DialogTitle = "Delete Country/Region"
Const L_DeleteRelated_DialogTitle = "Delete Related Codes"
Const L_SaveData_DialogTitle = "Save Data Code"
Const L_DeleteOne_Message = "Delete 1 item?"
Const L_DeleteMultiple_Message = "Delete %1 items?"
Const L_DeleteAll_Message = "Delete all?"
Const L_DeleteRelated_Message = "Deleting a country/region will automatically delete any states or provinces mapped to it. Proceed?" 

'Error strings
Const L_SiteConfig_ErrorMessage = "An error occurred while reading the configuration information. Please contact your system administrator."
Const L_Delete_ErrorMessage = "An error occurred while deleting the data codes."
Const L_SaveData_ErrorMessage = "An error occurred while saving the data code."
Const L_DuplicateCode_ErrorMessage = "The code already exists."
Const L_DuplicateCode_DialogTitle = "Duplicate Code"

'ListSheet strings
Const L_LoadingList_Text = "Loading list, please wait..."
Const L_CodeName_Text = "Code Name"
Const L_Code_Text = "Code"
Const L_Country_Text = "Country/Region"
Const L_DisplayName_Text = "Display Name"
Const L_CodeDescription_Text = "Description"

Const L_ItemDelete_StatusBar = " %1 items deleted"
Const L_ErrorItemDelete_StatusBar = "Error deleting items"
Const L_ItemsSelected_StatusBar = "%1 item(s) selected"
Const L_OneItem_StatusBar = "1 item selected [ %1 ]"
Const L_AllItems_StatusBar = "All items selected"

'EditSheet title strings
Const L_OrderStatusCodes_StaticText = "Order Status Codes"
Const L_CountryCodes_StaticText = "Country/Region Codes"
Const L_StateCodes_StaticText = "State/Province Codes"
Const L_OrderStatusCodesName_StaticText = "Order Status Codes - %1"
Const L_CountryCodesName_StaticText = "Country Codes - %1"
Const L_StateCodesName_StaticText = "State/Province Codes - %1"

'EditSheet fields
Const L_IntegerCodeLabel_Text = "Code:"
Const L_CodeNameLabel_Text = "Code Name:"
Const L_CodeDescriptionLabel_Text = "Description:"
Const L_CountryCodeLabel_Text = "Country/Region code:"
Const L_CountryLabel_Text = "Country/Region:"
Const L_DisplayNameLabel_Text = "Display name:"
Const L_StateCodeLabel_Text = "State/Province code:"

Const L_SelectCountry_Text = "Select a country/region"
Const L_EnterIntegerCode_Text = "Enter an integer code"
Const L_EnterCodeName_Text = "Enter a code name"
Const L_EnterDescription_Text = "Enter description"
Const L_EnterCountryCode_Text = "Enter a country/region code"
Const L_EnterDisplayName_Text = "Enter a display name"
Const L_EnterStateCode_Text = "Enter a state/province code"

Const L_IntegerCode_ToolTip = "Integer code"
Const L_CodeName_ToolTip = "Friendly name for the code"
Const L_CodeDescription_ToolTip = "Description of the code"
Const L_CountryCode_ToolTip = "Country/Region code"
Const L_DisplayNameCountry_ToolTip = "Display name of country/region code"
Const L_DisplayNameState_ToolTip = "Display name of state/province code"
Const L_Country_ToolTip = "Country of origin for the state/province"
Const L_StateCode_ToolTip = "State/Province code"

Const L_IntegerCode_ErrorMessage = "The data code must be an integer between 0 and 9999."
'Const L_CodeName_ErrorMessage = "Friendly Name for the Code must be less than 64 characters"
Const L_CodeDescription_ErrorMessage = "The description for the code must be less than 256 characters."
Const L_CountryCode_ErrorMessage = "The country/region code must be less than 3 characters."
Const L_DisplayNameCountry_ErrorMessage = "The display name for country/region must be less than 64 characters."
Const L_StateCode_ErrorMessage = "The state/province code must be less than 3 characters."
Const L_DisplayNameState_ErrorMessage = "The display name for state/province must be less than 64 characters."
Const L_InvalidText_ErrorMessage = "You must enter valid text in the required field before proceeding."

'EditSheet statusbar strings
Const L_DataSave_StatusBar = "Data code saved"
Const L_DataSaveError_StatusBar = "Error saving data code"
Const L_DataDuplicateError_StatusBar = "Error: Data code already exists"

Const L_SaveConfirmationDialog_Text = "Save data code before exiting?"
Const L_Required_Text = "Cannot save until all required fields have values."
Const L_Valid_Text = "Cannot save until all required fields have values and invalid fields are corrected."
Const L_LoadingProperties_Text = "Loading properties, please wait..."

%>