
<%		
	const ORDER_STATUS	= 1
	const COUNTRY		= 2
	const STATE_REGION	= 3	

	Const PAGE_SIZE = 20

	Dim g_iListType, g_sStatusText, g_sTitleText, g_xmlCodeEditData, g_xmlCodeEditMeta
	Dim g_dDataCodes, g_sKey, g_xmlCodeListData, g_xmlCodeListMeta
	Dim g_MSCSTransactionConfigConnStr

	'g_MSCSSiteName = Application("MSCSSiteName")
	g_MSCSTransactionConfigConnStr= GetSiteConfigField("transaction config", "connstr_db_TransactionConfig")   
	Set g_oConn = oGetADOConnection(g_MSCSTransactionConfigConnStr)
	Set g_oCmd = oGetADOCommand(g_oConn, AD_CMD_TEXT)
		
	Function BuildDataCodesQuery(iListType, sOrderBy)		
		Select Case cInt(iListType)
			Case ORDER_STATUS
				BuildDataCodesQuery = "Select * from Decode where i_type = 1 ORDER BY " & sOrderBy
			Case COUNTRY
				BuildDataCodesQuery = "Select * from Region where i_type = 1 ORDER BY " & sOrderBy
			Case STATE_REGION
				BuildDataCodesQuery = "Select * from Region where i_type = 2 ORDER BY " & sOrderBy
		End Select
	End Function

	Function GetXMLCodeListMeta(ByVal dCodes)
		Dim sBuffer
		sBuffer = "<listsheet>"
		sBuffer = sBuffer & "<global curpage='" & dCodes.Page & "' pagesize='" & PAGE_SIZE & "' recordcount='" & dCodes.RecCount & "' selection='multi' />"
		sBuffer = sBuffer & "<columns>"		
		Select Case cInt(dCodes.ListType)
			Case ORDER_STATUS
				sBuffer = sBuffer & "<column id='i_code' width='25'>" & L_Code_Text & "</column>"
				sBuffer = sBuffer & "<column id='u_name' width='25'>" & L_CodeName_Text & "</column>"
				sBuffer = sBuffer & "<column id='u_description' width='25'>" & L_CodeDescription_Text & "</column>"
			Case COUNTRY
				sBuffer = sBuffer & "<column id='u_name' width='25'>" & L_CodeName_Text & "</column>"
				sBuffer = sBuffer & "<column id='u_displayName' width='20'>" & L_DisplayName_Text & "</column>"
				sBuffer = sBuffer & "<column id='u_description' width='25'>" & L_CodeDescription_Text & "</column>"
			Case STATE_REGION
				sBuffer = sBuffer & "<column id='u_name' width='25'>" & L_CodeName_Text & "</column>"
				sBuffer = sBuffer & "<column id='u_group' width='20'>" & L_Country_Text & "</column>"
				sBuffer = sBuffer & "<column id='u_displayName' width='20'>" & L_DisplayName_Text & "</column>"
				sBuffer = sBuffer & "<column id='u_description' width='25'>" & L_CodeDescription_Text & "</column>"
		End Select
		sBuffer = sBuffer & "</columns>"
		sBuffer = sBuffer & "<operations>"
		sBuffer = sBuffer & "<newpage formid='sortform'/>"
		sBuffer = sBuffer & "<sort formid='sortform'/>"
		sBuffer = sBuffer & "<delete formid='selectform'/>"
		sBuffer = sBuffer & "</operations>"
		GetXMLCodeListMeta = sBuffer & "</listsheet>"
	End Function

	Sub DeleteDataCodes(bAll, iListType, aItems, aGroups)
	' This function deletes selected (bAll=false) or all records (bAll=true).
		Dim i, sCommandText
	
		' Build command text string
		If bAll = True Then		'Delete all items
			Select Case cInt(iListType)
				Case ORDER_STATUS
					sCommandText = "Delete from Decode where i_Type=1" 
				Case COUNTRY,STATE_REGION
					' Deleting all countries or regions will clear all entries from table
					sCommandText = "Delete FROM Region"
			End Select
		Else					'Delete only items passed in array (aItems and/or aGroups)
			' Build command text with criteria clause
			For i = 0 To UBound(aItems)
				aItems(i) = SQLQuote(aItems(i))
				Select Case cInt(iListType)
					Case ORDER_STATUS
						If i = 0 Then
							sCommandText = "DELETE FROM Decode WHERE i_Type=1 and i_Code='" & aItems(0) & "'"
						Else
							sCommandText = sCommandText & " OR i_Code='" & aItems(i) & "'"
						End If
					Case COUNTRY
						' Delete selected countries with associated regions
						If i = 0 Then
							sCommandText = " DELETE FROM Region WHERE u_Name='" & aItems(0) & "' OR u_group='" & aItems(0) & "'"
						Else
							sCommandText = sCommandText & " OR u_name='" & aItems(i) & "' OR u_group='" & aItems(i) & "'"
						End If 
					Case STATE_REGION
						aGroups(i) = SQLQuote(aGroups(i))
						If i = 0 Then
							sCommandText = " DELETE FROM Region WHERE i_Type=2 and u_Name='" & aItems(0)& "' AND u_Group = '" & aGroups(0) & "'"
						Else
							sCommandText = sCommandText & " OR u_Name='" & aItems(i) & "' AND u_Group = '" & aGroups(i) & "'"
						End If
				End Select
			Next
		End If

		' Execute the delete query
		On Error Resume Next
		g_oCmd.CommandText = sCommandText
		g_oCmd.Execute
		If Err <> 0 then
			Call setErr(L_DeleteCodes_DialogTitle, L_Delete_ErrorMessage)
		End If
	End Sub

	Sub GetCodeEditData(dCodes, sDisplayName)
		Dim sQuery, rs

		On Error Resume Next
		' Check data for single quotes
		With dCodes
			.Name = SQLQuote(.Name)
			.Group = SQLQuote(.Group)
		End With
		Select Case cInt(g_iListType)
			Case ORDER_STATUS
				sQuery = "SELECT * FROM Decode WHERE i_type = 1 AND i_code = " & dCodes.Code
				Set rs = GetRecordset(sQuery)
				If Not rs.EOF or rs.BOF Then
					dCodes.Code = rs.fields("i_code").value
					dCodes.Name = rs.fields("u_name").value
					dCodes.Description = rs("u_description").value
				End If
				sDisplayName = dCodes.Name
			Case COUNTRY
				sQuery = "SELECT * FROM Region WHERE i_type = 1 AND u_name = '" & dCodes.Name & "'"
				Set rs = GetRecordset(sQuery)
				If Not rs.EOF Or rs.BOF Then
					dCodes.Name = rs.fields("u_name").value
					dCodes.DisplayName = rs.fields("u_displayname").value
					dCodes.Description = rs.fields("u_description").value
				End If
				sDisplayName = dCodes.DisplayName
			Case STATE_REGION
				sQuery = "SELECT * FROM Region WHERE u_Group = '" & dCodes.Group & "' AND " & "u_Name = '" & dCodes.Name & "'"
				Set rs = GetRecordset(sQuery)
				If Not rs.EOF Or rs.BOF Then
					dCodes.Name = rs.fields("u_name").value
					dCodes.Group = rs.fields("u_group").value
					dCodes.Displayname = rs.fields("u_displayname").value
					dCodes.Description = rs.fields("u_description").value
				End If
				sDisplayName = dCodes.DisplayName
			End Select
		If err <> 0 Then
			'$$error message
		End If
	End Sub

	Function BuildCodeEditInsertCommand(dCodes)
		Dim sCommandText

		Select Case cInt(g_iListType)
			Case ORDER_STATUS
				sCommandText = "Insert into Decode Values(1, " & dCodes.Code & ", " _
							& "'" & SQLQuote(dCodes.Name) & "', " & "'" & SQLQuote(dCodes.Description) & "', " _
							& "NewID()," & "GetDate(),"  & "Getdate())"
			Case COUNTRY
				dCodes.Group = Null
				sCommandText = "Insert into Region Values(1, '"  & SQLQuote(dCodes.Name) & "', " _
							& "'" & SQLQuote(dCodes.Group) & "', " & "'" & SQLQuote(dCodes.DisplayName) & "', " _
							& "'" & SQLQuote(dCodes.Description) & "', " _
							& "NewID()," & "GetDate()," &	"Getdate())"
			Case STATE_REGION
				sCommandText = "Insert into Region Values(2, '" & SQLQuote(dCodes.Name) & "', " _
							& "'" & SQLQuote(dCodes.Group) & "', " & "'" & SQLQuote(dCodes.DisplayName) & "', " _
							& "'" & SQLQuote(dCodes.Description) & "', " _
							& "NewID()," & "GetDate()," & "Getdate())"
		End Select
		BuildCodeEditInsertCommand = sCommandText	
	End Function

	Function BuildCodeEditUpdateCommand(dCodes)
		Dim sCommandText

		Select Case cInt(g_iListType)
			Case ORDER_STATUS
				sCommandText = "Update Decode Set i_Type=1, i_Code=" & dCodes.Code & ", " _
							& "u_Name='" & SQLQuote(dCodes.Name) & "', " & "u_Description='" & SQLQuote(dCodes.Description) & "', "  _
							& "g_UserIDChangedBy= NewID()," & "d_DateLastChanged= GetDate()" _
							& "where i_Code = '" & dCodes.Code & "' and i_Type = 1"
			Case COUNTRY
				dCodes.Group = Null
				sCommandText = "Update Region Set i_Type=1, u_Name='" & SQLQuote(dCodes.Name) & "', " _
							& "u_Group='" & dCodes.Group & "', " & "u_DisplayName='" & SQLQuote(dCodes.DisplayName) & "', " _
							& "u_Description='" & SQLQuote(dCodes.Description) & "', " _
							& "g_UserIDChangedBy= NewID()," & "d_DateLastChanged= GetDate()" _
							& "where u_Name = '" & SQLQuote(dCodes.Name) & "' and i_Type = 1"
			Case STATE_REGION
				sCommandText = "Update Region Set i_Type=2, u_Name='" & SQLQuote(dCodes.Name) & "', " _
							& "u_Group='" & SQLQuote(dCodes.Group) & "', " & "u_DisplayName='" & SQLQuote(dCodes.DisplayName) & "', " _
							& "u_Description='" & SQLQuote(dCodes.Description) & "', " _
							& "g_UserIDChangedBy= NewID()," & "d_DateLastChanged= GetDate()" _
							& " where u_Name = '" & SQLQuote(dCodes.Name) & "'" _
							& " and i_Type = 2 and u_Group = '" & SQLQuote(dCodes.Group) & "'"			
		End Select
		BuildCodeEditUpdateCommand = sCommandText
	End Function

	Function CheckForExistingCode(ByVal dCodes)
		Dim sName, sGroup, sQuery, rs	

		On Error Resume Next
		CheckForExistingCode = False
		sName = SQLQuote(dCodes.name)
		sGroup = SQLQuote(dCodes.group)
		Select Case cInt(g_iListType)
			Case ORDER_STATUS
				sQuery = "SELECT * FROM Decode WHERE i_type = 1 AND i_code = " & dCodes.Code
			Case COUNTRY
				sQuery = "SELECT * FROM Region WHERE i_type = 1 AND u_name = '" & sName & "'"
			Case STATE_REGION
				sQuery = "SELECT * FROM Region WHERE u_Group = '" & sGroup & "' AND " & "u_Name = '" & sName & "'"
				Set rs = GetRecordset(sQuery)
		End Select
		Set rs = GetRecordset(sQuery)
		If Not (rs.EOF Or rs.BOF) Then 
			CheckForExistingCode = True
		End If
	End Function

	Function GetXMLCodeEditData(dCodes)
		Dim sBuffer
		Select Case dCodes.Mode
			Case "new", "savenew", "saveback"
				sBuffer = "<document/>" 
			Case "savenew"
				sBuffer = "<document>"
				sBuffer = sBuffer & "<record>"
				sBuffer = SBuffer & "<u_Group>" & dCodes.Group & "</u_Group>"
				sBuffer = sBuffer & "</record>"
				sBuffer = sBuffer & "</document>"
			Case "open", "save", "err"
				sBuffer = "<document>"
				sBuffer = sBuffer & "<record>"
				Select Case cInt(g_iListType)
					Case ORDER_STATUS
						sBuffer = sBuffer & "<i_Code>" & dCodes.Code & "</i_Code>"
						sBuffer = sBuffer & "<u_Name>" & dCodes.Name & "</u_Name>"
						sBuffer = sBuffer & "<u_Description>" & dCodes.Description & "</u_Description>"
					Case COUNTRY
						sBuffer = sBuffer & "<u_Name>" & dCodes.Name & "</u_Name>"
						sBuffer = sBuffer & "<u_DisplayName>" & dCodes.DisplayName & "</u_DisplayName>"
						sBuffer = sBuffer & "<u_Description>" & dCodes.Description & "</u_Description>"
					Case STATE_REGION
						sBuffer = sBuffer & "<u_Name>" & dCodes.Name & "</u_Name>"
						sBuffer = sBuffer & "<u_DisplayName>" & dCodes.DisplayName & "</u_DisplayName>"
						sBuffer = SBuffer & "<u_Group>" & dCodes.Group & "</u_Group>"
						sBuffer = sBuffer & "<u_Description>" & dCodes.Description & "</u_Description>"
				End Select
				sBuffer = sBuffer & "</record>"
				sBuffer = sBuffer & "</document>"
		End Select
		GetXMLCodeEditData = sBuffer
	End Function

	Function GetXMLCodeEditMeta(bReadOnly)
		Dim sBuffer
		sBuffer = "<editsheet>"
		sBuffer = sBuffer & "<global title='no'/>"
		sBuffer = sBuffer & "<fields>"
		Select Case cInt(g_iListtype)
			Case ORDER_STATUS
				If bReadOnly Then
					sBuffer = sBuffer & "<numeric id='i_Code' readonly='yes'>"
				Else	
					sBuffer = sBuffer & "<numeric id='i_Code' required='yes' min='0' max='9999'>"
				End If
				sBuffer = sBuffer & BuildXMLEditFieldString(L_IntegerCodeLabel_Text, _
															L_IntegerCode_ToolTip, _
															L_IntegerCode_ErrorMessage, _
															L_EnterIntegerCode_Text)
				sBuffer = sBuffer & "</numeric>"

				sBuffer = sBuffer & "<text id='u_Name' required='yes' maxlen='64'>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_CodeNameLabel_Text, _
															L_CodeName_ToolTip, _
															L_InvalidText_ErrorMessage, _
															L_EnterCodeName_Text)
				sBuffer = sBuffer & "</text>"

				sBuffer = sBuffer & "<text id='u_Description' maxlen='256' subtype='short'>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_CodeDescriptionLabel_Text, _
															L_CodeDescription_ToolTip, _
															L_CodeDescription_ErrorMessage, _
															L_EnterDescription_Text)
				sBuffer = sBuffer & "</text>"
			Case COUNTRY
				If bReadOnly Then
					sBuffer = sBuffer & "<text id='u_Name' readonly='yes'>"
				Else
					sBuffer = sBuffer & "<text id='u_Name' required='yes' maxlen='3'>"
				End If
				sBuffer = sBuffer & "<charmask>[^'""&gt;&lt;&amp;]*</charmask>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_CountryCodeLabel_Text, _
															L_CountryCode_ToolTip, _
															L_InvalidText_ErrorMessage, _
															L_EnterCountryCode_Text)
				sBuffer = sBuffer & "</text>"				

				sBuffer = sBuffer & "<text id='u_DisplayName' maxlen='64'>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_DisplayNameLabel_Text, _
															L_DisplayNameCountry_ToolTip, _
															L_InvalidText_ErrorMessage, _
															L_EnterDisplayName_Text)
				sBuffer = sBuffer & "</text>"

				sBuffer = sBuffer & "<text id='u_Description' maxlen='256' subtype='short'>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_CodeDescriptionLabel_Text, _
															L_CodeDescription_ToolTip, _
															L_CodeDescription_ErrorMessage, _
															L_EnterDescription_Text)
				sBuffer = sBuffer & "</text>"
			Case STATE_REGION
				If bReadOnly Then
					sBuffer = sBuffer & "<text id='u_Name' readonly='yes'>"
				Else
					sBuffer = sBuffer & "<text id='u_Name' required='yes' maxlen='64'>"
				End If
				sBuffer = sBuffer & "<charmask>[^'""&gt;&lt;&amp;]*</charmask>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_StateCodeLabel_Text, _
															L_StateCode_ToolTip, _
															L_InvalidText_ErrorMessage, _
															L_EnterStateCode_Text)
				sBuffer = sBuffer & "</text>"

				sBuffer = sBuffer & "<text id='u_DisplayName' maxlen='64'>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_DisplayNameLabel_Text, _
															L_DisplayNameState_ToolTip, _
															L_InvalidText_ErrorMessage, _
															L_EnterDisplayName_Text)
				sBuffer = sBuffer & "</text>"

				sBuffer = sBuffer & "<select id='u_Group'>"
				sBuffer = sBuffer & "<name>" & L_CountryLabel_Text & "</name>"
				sBuffer = sBuffer & "<tooltip>" & L_Country_ToolTip & "</tooltip>"
				sBuffer = sBuffer & "<prompt>" & L_SelectCountry_Text & "</prompt>"
				sBuffer = sBuffer & "<select id='u_Group'>"
				sBuffer = sBuffer & GetOptionValues()
				sBuffer = sBuffer & "</select>"
				sBuffer = sBuffer & "</select>"

				sBuffer = sBuffer & "<text id='u_Description' maxlen='256' subtype='short'>"
				sBuffer = sBuffer & BuildXMLEditFieldString(L_CodeDescriptionLabel_Text, _
															L_CodeDescription_ToolTip, _
															L_InvalidText_ErrorMessage, _
															L_EnterDescription_Text)
				sBuffer = sBuffer & "</text>"
		End Select
		sBuffer = sBuffer & "</fields>"
		sBuffer = sBuffer & "</editsheet>"
		GetXMLCodeEditMeta = sBuffer		
	End Function	

	Function BuildXMLEditFieldString(sName, sTooltip, sError, sPrompt)
		Dim sBuffer
		sBuffer = "<name>" & sName & "</name>"
		sBuffer = sBuffer & "<tooltip>" & sTooltip & "</tooltip>"
		sBuffer = sBuffer & "<error>" & sError & "</error>"
		sBuffer = sBuffer & "<prompt>" & sPrompt & "</prompt>"
		BuildXMLEditFieldString = sBuffer
	End Function
	
	Function GetOptionValues()
		Dim sQuery, rs, sBuffer, sName, sDisplayName
		On Error Resume Next
		sQuery = "SELECT u_Name, u_DisplayName FROM Region WHERE i_type = 1 ORDER BY u_DisplayName"
		Set rs = GetRecordset(sQuery)
		If rs.EOF Or rs.BOF Then 
			sBuffer = sBuffer & "<option value=''></option>"
		Else
			rs.MoveFirst
			Do While Not rs.EOF
				sName = rs("u_Name").value
				sDisplayName = rs("u_DisplayName").value
				sBuffer = sBuffer & "<option value='" & sName & "'>" & g_mscsPage.htmlEncode(sDisplayName) & "</option>"
				rs.MoveNext
			Loop
		End If
		GetOptionValues = sBuffer
	End Function

	Function GetRecordset(sQuery)
		Dim rs
		On Error Resume Next
		'get connection string from siteconfig   
		g_oCmd.CommandText = sQuery
		Set rs = Server.CreateObject("ADODB.Recordset")
		With rs
			.CursorLocation = AD_USE_CLIENT
			.open g_oCmd,, AD_OPEN_KEYSET, AD_LOCK_PESSIMISTIC
		End With
		Set GetRecordset = rs
		Set rs = Nothing
	End Function

	Function SQLQuote(sText)	
		If IsNull(sText) Or IsEmpty(sText) Or sText = "" Then Exit Function
		SQLQuote = Replace(sText, "'", "''")
	End function

	Sub SetErr(sTitle, sMessage)
		Call setError(sTitle, sMessage, sGetScriptError(Err), ERROR_ICON_CRITICAL)
	End Sub
%>



