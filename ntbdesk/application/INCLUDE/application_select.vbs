<SCRIPT LANGUAGE='VBScript'>

	Option Explicit
		
	Public g_aSelectedItems1
	Public g_aSelectedItems2
	Public g_nItemsSelected
	Public g_sKeyString1
	Public g_sKeyString2
	Redim g_aSelectedItems1(0)
	Redim g_aSelectedItems2(0)
			
	Sub AddItem()
	' note: g_nItemsSelected is 1-based, array items are 0-based
		dim oSelection, sKey, sGroup, iCount	
		
		ReDim Preserve g_aSelectedItems1(g_nItemsSelected)
		ReDim Preserve g_aSelectedItems2(g_nItemsSelected)
		set oSelection = window.event.XMLrecord
		sKey = oSelection.selectSingleNode("./<%= g_sKey %>").text
		iCount = UBound(g_aSelectedItems1)
		g_aSelectedItems1(iCount) = sKey
		g_nItemsSelected = g_nItemsSelected + 1
		g_sKeyString1 = Join(g_aSelectedItems1)
		' Add group value if in the region list
		if "<%= g_dDataCodes.listtype %>" = "3" then
			sGroup = oSelection.selectSingleNode("./u_Group").text
			g_aSelectedItems2(iCount) = sGroup
			g_sKeyString2 = Join(g_aSelectedItems2)
		end if
	End Sub
	
	Sub RemoveItem()
		dim oSelection, sKey, iCount, i
	
	    set oSelection = window.event.XMLrecord
	    sKey = oSelection.selectSingleNode("./<%= g_sKey %>").text
	    iCount = UBound(g_aSelectedItems1)
	    for i = 0 to iCount
		    ' Remove item from the array and copy the last item into the empty space
		    if g_aSelectedItems1(i) = sKey then
			    g_aSelectedItems1(i) = empty
			    g_aSelectedItems1(i) = g_aSelectedItems1(iCount)
			    redim preserve g_aSelectedItems1(iCount -1)
			    g_nItemsSelected = g_nItemsSelected -1
			    g_sKeyString1 = Join(g_aSelectedItems1)
				' Remove group value if in region list
				if "<%= g_dDataCodes.listtype %>" = "3" then
					g_aSelectedItems2(i) = empty
					g_aSelectedItems2(i) = g_aSelectedItems2(iCount)
					redim preserve g_aSelectedItems2(iCount -1)
					g_sKeyString2 = Join(g_aSelectedItems2)
				end if
	            exit for
	        end if
	    next
	End Sub

	sub OnSelectRow()
		selectform.seltype.value = ""
		call AddItem()
		call SetTaskButtons()     
	end sub

	sub OnSelectAllRows()
		redim g_aSelectedItems1(0)
		redim g_aSelectedItems2(0)
		g_nItemsSelected = 0
		g_sKeyString1 = ""
		g_sKeyString2 = ""
		selectform.seltype.value = "all"
		call SetTaskButtons()
	end sub
	  
	Sub OnUnSelectRow()
		selectform.seltype.value = ""
        Call RemoveItem()
		call SetTaskButtons()
	End Sub
	
	Sub OnUnSelectAllRows()
		selectform.seltype.value = ""
		redim g_aSelectedItems1(0)
		redim g_aSelectedItems2(0)
		g_nItemsSelected = 0
		g_sKeyString1 = ""
		g_sKeyString2 = ""
		call SetTaskButtons()
	End Sub   
	
	Sub SetTaskButtons()
		dim sText
		select case g_nItemsSelected
			case 0
				DisableTask "open"
				DisableTask "delete"
				EnableTask "new"
				EnableTask "view"
				if selectform.seltype.value = "all" then
					EnableTask "delete"
					DisableTask "new"
					DisableTask "open"
					EnableTask "view"
					sText = "<%= L_AllItems_StatusBar%>"
				end if
			case 1
				EnableTask "open"
				EnableTask "delete"
				EnableTask "new"
				EnableTask "View"
				sText = sFormatString("<%= L_OneItem_StatusBar%>",Array(g_sKeyString1))
			case else
				DisableTask "open"
				EnableTask "new"
				EnableTask "delete"
				EnableTask "view" 
				sText = sFormatString("<%= L_ItemsSelected_StatusBar%>",Array(g_nItemsSelected))
		end select
		setStatusText sText
	End Sub
	
</SCRIPT>
