<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="./include/application_strings.asp" -->
<!--#INCLUDE FILE="./include/application_util.asp"-->

<% Call ProcessEditForm()

	Sub ProcessEditForm()
		Dim dCodes, bCodeExists, sQuery, sCommandText, sDisplayName
		
		g_iListType = Request("type")

		' Process modes
		Set dCodes = Server.CreateObject("Commerce.Dictionary")
		dCodes.Mode = Request.Form("mode")
		If IsEmpty(dCodes.Mode) Then dCodes.Mode = "new" 
		Select Case dCodes.Mode
			Case "open"
				dCodes.Key = Request.Form("key")
				If cInt(g_iListType) = ORDER_STATUS Then
					dCodes.Code = Request.Form("key")
				Else
					dCodes.Name = Request.Form("key")
				End If
				dCodes.Group = Request.Form("group")
				Call GetCodeEditData(dCodes, sDisplayName)	' sDisplayName is an output parameter
				g_sStatusText = ""
				Session("EditMode") = "open"
			Case "new"		' No query processing required so exit
				dCodes.Key = "-1"
				g_sStatusText = ""
				Session("EditMode") = "new"
			Case "save", "savenew", "saveback"
				With dCodes
					.Code = Request.Form("i_code")
					.Name = Request.Form("u_name")
					.Description = Request.Form("u_description")
					.DisplayName = Request.Form("u_DisplayName")
					.Group = Request.Form("u_group")
				End With
				' Set display name for title
				If dCodes.DisplayName <> "" Then
					sDisplayName = dCodes.DisplayName
				Else
					sDisplayName = dCodes.Name
				End If
				if dCodes.Mode = "savenew" then sDisplayName = ""
				
				' start the status bar assuming success
				g_sStatusText = L_DataSave_StatusBar
				
				' Build update or insert query
				If Session("EditMode") = "new" Then		' Insert
					bCodeExists = CheckForExistingCode(dCodes)
					If bCodeExists = False Then
						sCommandText = BuildCodeEditInsertCommand(dCodes)
						g_oCmd.CommandText = sCommandText
						g_oCmd.Execute
					Else
						' Error message (duplicate record)
						Call setErr(L_DuplicateCode_DialogTitle, L_DuplicateCode_ErrorMessage)
						g_sStatusText = L_DataSaveError_StatusBar
						dCodes.Mode = "err"
					End If
				Else									' Update
					sCommandText = BuildCodeEditUpdateCommand(dCodes)
					g_oCmd.CommandText = sCommandText
					g_oCmd.Execute
				End If
				If Err <> 0 Then 
					Call setErr(L_SaveData_DialogTitle, L_SaveData_ErrorMessage)
					g_sStatusText = L_DataSaveError_StatusBar
					dCodes.Key = "-1"
					dCodes.Mode = "err"
				End If

				' Set edit mode						
				If dCodes.Mode = "save" Then
					Session("EditMode") = "open"
				ElseIf dCodes.Mode = "savenew" Then
					Session("EditMode") = "new"
				End If
		End Select
		
		' Set title text
		If Not IsEmpty(sDisplayName) and sDisplayName <> "" Then
			Select Case cInt(g_iListType)
				Case ORDER_STATUS
					g_sTitleText = sFormatString(L_OrderStatusCodesName_StaticText, array(sDisplayName))
				Case COUNTRY
					g_sTitleText = sFormatString(L_CountryCodesName_StaticText, array(sDisplayName))
				Case STATE_REGION
					g_sTitleText = sFormatString(L_StateCodesName_StaticText, array(sDisplayName))
			End Select
		else
			Select Case cInt(g_iListType)
				Case ORDER_STATUS
					g_sTitleText = L_OrderStatusCodes_StaticText
				Case COUNTRY
					g_sTitleText = L_CountryCodes_StaticText
				Case STATE_REGION
					g_sTitleText = L_StateCodes_StaticText
			End Select
		end if
		
		' Create xml meta and data
		If Session("EditMode") = "new" Then			' Adding a new code so allow editing key field
			g_xmlCodeEditMeta = GetXMLCodeEditMeta(False)
		Else
			g_xmlCodeEditMeta = GetXMLCodeEditMeta(True)	' Disallow editing key field
		End If
		g_xmlCodeEditData = GetXMLCodeEditData(dCodes)
	End Sub
 %>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>

<SCRIPT LANGUAGE = 'VBSCRIPT'>

	OPTION EXPLICIT 

	const ORDER_STATUS	= 1
	const COUNTRY		= 2
	const STATE_REGION	= 3	
	
	Sub onSave()
		dim iListType, bValid
		iListType = "<%=g_iListType%>"
		' Validate required field text and set key value
		bValid = true
		Select Case cInt(iListType)
			Case ORDER_STATUS
				if len(Trim(esDataCodes.field("i_code").value)) = "" then
					esDataCodes.field("i_code").value = ""
					bValid = false
				else
					saveform.key.value = esDataCodes.field("i_code").value
				end if
			Case COUNTRY, STATE_REGION
				if len(Trim(esDataCodes.field("u_name").value)) = 0 then
					esDataCodes.field("u_name").value = ""
					bValid = false
				else
					saveform.key.value = esDataCodes.Field("u_name").value
				end if
		End Select
		if bValid = false then
			MsgBox "<%=L_InvalidText_ErrorMessage%>", vbOKOnly
		else		
			if window.event.srcElement Is elGetTaskBtn("save") then
				saveform.mode.value = "save"
			elseif window.event.srcElement Is elGetTaskBtn("savenew") then
				saveform.mode.value = "savenew"
			elseif window.event.srcElement Is elGetTaskBtn("saveback") then
				saveform.mode.value = "saveback"
			end if
			saveform.submit()
		end if	
	End Sub

</SCRIPT>
</HEAD>

<BODY SCROLL=no LANGUAGE="VBScript" ONLOAD="esDataCodes.focus()">
	<% InsertEditTaskBar g_sTitleText , g_sStatusText %>

	<xml id='esMeta'>
		<%= g_xmlCodeEditMeta %>
	</xml>

	<xml id='esData'>
		<%= g_xmlCodeEditData %>
	</xml>

	<FORM ID='saveform' METHOD='POST' ONTASK='onSave'>
		<INPUT TYPE='hidden' ID='mode' NAME='mode'>
		<INPUT TYPE='hidden' ID='key' NAME='key'>
		<INPUT TYPE='hidden' ID='group' NAME='group'>

		<DIV ID="editSheetContainer" CLASS="editPageContainer">			
			<DIV ID='esDataCodes' 
				 CLASS='editSheet' 
				 MetaXML='esMeta'
				 DataXML='esData'
				 LANGUAGE="VBScript"
				 ONCHANGE='setDirty("<%= L_SaveConfirmationDialog_Text%>")'
			  	 ONREQUIRE='setRequired("")'
				 ONVALID='setValid("")'>
				 <%= L_LoadingProperties_Text%>
			</DIV>			
		</DIV>
	</FORM>

	<FORM ID='backform' METHOD='POST'>
	</FORM>

</BODY>
</HTML>
