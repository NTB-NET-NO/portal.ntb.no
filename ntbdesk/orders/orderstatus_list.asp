<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="include/requisition_util.asp" -->
<!--#INCLUDE FILE="include/inc_common.asp"-->
<!--#INCLUDE FILE="include/ordersStrings.asp"-->

<%	 
	dim thisPage ,g_sModuleName
	thisPage = "orderstatus_list.asp"
	g_sModuleName = L_OrderStatus_StaticText

	dim g_sKey,g_sSubKey,g_sStatustext,dOrders
 	
 	'Initializes the OrderGroup Object
 	IntializeOrderGroupObject
 	
	'Implementation for saving states
	set dOrders = Server.CreateObject("Commerce.Dictionary")
	if not IsEmpty(Session("dOrders")) then
		set dOrders = Session("dOrders")
	end if
 	dOrders.StatusFilter = 0			
	
	g_sKey = "order_number"
	g_sSubKey = "ordergroup_id"
	 
	'Implementation for deleting orders
	if Request.Form("type") = "del" then
		
		Dim items,count, i,nResult
		if Request.Form("selectall") then
			nResult = deleteAllRecords(dOrders,thisPage)
			if nResult = 0 then
				g_sStatusText = L_AllDeleted_StatusBar
			else
				g_sStatusText = L_ErrorDelete_StatusBar
			end if
		else
			items = Split(Request.Form("selection"),",")
			count = ubound(items) + 1
			nResult = deleteRecord(dOrders, items)
			if nResult = 0 then
				g_sStatusText = sFormatString(L_Delete_StatusBar,Array(count))
			else
				g_sStatusText = L_ErrorDelete_StatusBar
			end if
		end if
	end if
 
%>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
	.hide {
	   DISPLAY: none;
	}
	DIV.esContainer, .esEditgroup DIV.esContainer
	{
	    BACKGROUND-COLOR: transparent;
	}
</STYLE>
<!--#INCLUDE FILE='include/orders_util.vbs' -->
<SCRIPT LANGUAGE='VBScript'>

    sub window_OnLoad()
		EnableTask "find"
		showFindBy()
		changeSelect()
	end sub
				
	'Called when the find by selection changes
	'Decides what are the fields to be displayed
	sub changeSelect()
		document.all.btnfindby.disabled = true
		startdaterow.className = "hide"
		enddaterow.className = "hide"
		firstnamerow.className = "hide"
		lastnamerow.className = "hide"
		ordernumrow.className = "hide"
        orgrow.className = "hide"
		ponrow.className = "hide"
		itemskurow.className = "hide"
		itemdescrow.className = "hide"
		select case selfindby.value
			case "daterange"
				setFindByHeight(150)
				startdaterow.className = ""
				changeDate()
			case "username"
				setFindByHeight(150)
				lastnamerow.className = ""
				firstnamerow.className = ""
			case "ordernumber"
				setFindByHeight(150)
				ordernumrow.className = ""
			case "organization"
				setFindByHeight(150)
				orgrow.className = ""
			case "ponumber"
				setFindByHeight(150)
				ponrow.className = ""
			case "itemsku"
				setFindByHeight(150)
				itemskurow.className = ""
			case "itemdesc"
				setFindByHeight(150)
				itemdescrow.className = ""
			case "advance"
				setFindByHeight(290)
				startdaterow.className = ""
				firstnamerow.className = ""
				lastnamerow.className = ""
				ordernumrow.className = ""
				orgrow.className = ""
				ponrow.className = ""
				itemskurow.className = ""
				itemdescrow.className = ""
				changeDate()
				document.all.btnfindby.disabled = False
			case "all"
				setFindByHeight(150)
				document.all.btnfindby.disabled = False
		end select
		showFindBy()
	end sub
	
	'Called when the date criteria selection is changed
	sub changeDate()
		select case fbdate.value
			case "from"
				start_date.style.display = "block"
				enddaterow.className = ""
			case "onbefore"
				start_date.style.display = "block"
				enddaterow.className = "hide"
		end select
	end sub
	
	'Called when the Find Now button is clicked
	sub reloadListSheet()
		onNoSelectedRows
		lsOrders.page = 1
		lsOrders.reload("findby")
	end sub
</SCRIPT>
</HEAD>
<BODY SCROLL='NO'>

<%
InsertTaskBar g_sModuleName, g_sStatustext
'Checking to see if there is a date in the session, if not setting the findby date
'for the current date
if isNull(dOrders.start_date)or isNull(dOrders.end_date) then
	dOrders.start_date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)
	dOrders.end_date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)
end if
%>

<!---Start Find By Area  ---->
<xml id="fbmeta">
<editsheet>
    <global title='no'/>
	<fields>
		<select id='selfindby' onchange='changeSelect'>
			<prompt><%= L_SelectFindBy_Text %></prompt>
			<select id='selfindby'>
				<option value='daterange'><%= L_OrderDate_ComboBox %></option>
				<option value='username'><%= L_UserName_ComboBox %></option>
				<option value='ordernumber'><%= L_OrderNumber_ComboBox %></option>
				<option value='organization'><%= L_Organization_ComboBox %></option>
				<option value='ponumber'><%= L_PONumber_ComboBox %></option>
				<option value='itemsku'><%= L_ItemPartNumber_ComboBox %></option>
				<option value='itemdesc'><%= L_ItemDescription_ComboBox %></option>
				<option value='advance'><%= L_AnyInformation_ComboBox %></option>
				<option value='all'><%= L_AllOrders_ComboBox %></option>
			</select>
		</select>
		<select id='fbdate' onchange='changeDate'>
			<prompt><%= L_SelectFindBy_Text %></prompt>
			<select id='fbdate'>
				<option value='from'><%= L_From_ComboBox %></option> 
				<option value='onbefore'><%= L_OnOrBefore_ComboBox %></option>
			</select>
		</select>
	    <date id='start_date' firstday='<%= g_sMSCSWeekStartDay %>'>
	        <name><%= L_StartDate_Text %></name>
	        <format><%= g_sMSCSDateFormat %></format>
	    </date>
	    <date id='end_date' firstday='<%= g_sMSCSWeekStartDay %>'>
	        <name><%= L_EndDate_Text %></name>
	        <format><%= g_sMSCSDateFormat %></format>
	    </date>
		<text id='fbfirstname' subtype='short' maxlen='64'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
		<text id='fblastname' subtype='short' maxlen='64'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
		<text id='fbordernumber' subtype='short' maxlen='32'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
	    <text id='fborganization' subtype='short' maxlen='64'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
		<text id='fbponumber' subtype='short' maxlen='32'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
		<text id='fbitemsku' subtype='short' maxlen='50'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
		<text id='fbitemdesc' subtype='short' maxlen='255'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
	</fields>
    <template fields='selfindby fbdate start_date end_date fbfirstname fblastname fbordernumber fborganization fbponumber fbitemsku fbitemdesc'><![CDATA[
		<TABLE WIDTH='100%' STYLE='TABLE-LAYOUT: fixed'>
			<COLGROUP><COL WIDTH='100'><COL WIDTH='120'><COL WIDTH='200'><COL></COLGROUP>
			<TR>
				<TD>
					<SPAN ID='findbylabel'><%= L_FindBy_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='selfindby'><%= L_Loading_Text %></DIV>
				</TD>
				<TD>
					<BUTTON ID='btnfindby' NAME='btnfindby' DISABLED
						CLASS='bdbutton'
						LANGUAGE='VBScript'
						ONCLICK='reloadListSheet'><%= L_FindNow_Button %></BUTTON>
				</TD>
			</TR>
			<TR ID='startdaterow' CLASS='hide'>
				<TD>
					<SPAN ID='datelabel'><%= L_Date_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='fbdate'><%= L_Loading_Text %></DIV>
				</TD>
				<TD>
					<DIV ID='start_date' STYLE='DISPLAY: none'
						 LANGUAGE="VBScript"
						 ONCLICK='EnableFind("<%= thisPage %>")'
						 ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='enddaterow' CLASS='hide'>
				<TD></TD>
				<TD>
					<SPAN ID='tolabel'><%= L_To_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='end_date'
						 LANGUAGE="VBScript"
						 ONCLICK='EnableFind("<%= thisPage %>")'
						 ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='lastnamerow' CLASS='hide'>
				<TD>
					<SPAN ID='lnlabel'><%= L_LastName_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='fblastname'
						LANGUAGE='VBScript'
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='firstnamerow' CLASS='hide'>
				<TD>
					<SPAN ID='fnlabel'><%= L_FirstName_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='fbfirstname'
						LANGUAGE='VBScript'
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='ordernumrow' CLASS='hide'>
				<TD>
					<SPAN ID='ordernumlabel'><%= L_OrderNumber_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='fbordernumber'
						LANGUAGE='VBScript'
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='orgrow' CLASS='hide'>
				<TD>
					<SPAN ID='orglabel'><%= L_Organization_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='fborganization'
						LANGUAGE='VBScript'
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='ponrow' CLASS='hide'>
				<TD>
					<SPAN ID='ponlabel'><%= L_PONumber_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='fbponumber'
						LANGUAGE='VBScript'
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='itemskurow' CLASS='hide'>
				<TD>
					<SPAN ID='itemskulabel'><%= L_ItemPartNumber_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='fbitemsku'
						LANGUAGE='VBScript'
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='itemdescrow' CLASS='hide'>
				<TD>
					<SPAN ID='itemdesclabel'><%= L_Description_Text %></SPAN>
				</TD>
				<TD colspan=2>
					<DIV ID='fbitemdesc'
						LANGUAGE='VBScript'
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
		</TABLE>		
    ]]></template>
</editsheet>
</xml>

<xml id='fbData'>
	<document>
		<record>
			<selfindby><%= dOrders.selfindby %></selfindby>
			<fbdate><%= dOrders.fbdate %></fbdate>
			<start_date><%= dOrders.start_date %></start_date>
			<end_date><%= dOrders.end_date %></end_date>
			<fbfirstname><%= dOrders.fbfirstname %></fbfirstname>
			<fblastname><%= dOrders.fblastname %></fblastname>
			<fbordernumber><%= dOrders.fbordernumber %></fbordernumber>
			<fborganization><%= dOrders.fborganization %></fborganization>
			<fbponumber><%= dOrders.fbponumber %></fbponumber>
			<fbitemsku><%= dOrders.fbitemsku %></fbitemsku>
			<fbitemdesc><%= dOrders.fbitemdesc %></fbitemdesc>
		</record>
	</document>
</xml>
<FORM ID='searchform' ACTION='orders_event.asp'>
	<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE='orders'>
	<DIV ID='bdfindbycontent' CLASS='findByContent'>
		<div ID="esFind" class="editSheet"
			METAXML='fbMeta' DATAXML='fbData'></div>
	</DIV>
</FORM>
<!-- End FindBy Area ----->

<DIV ID='bdcontentarea'>
	<FORM ID='neweventform' NAME='neweventform' ACTION='orders_event.asp'>
		<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE="orders">
	</FORM>


	<FORM ID='selectform' ACTION='' METHOD='POST'>
		<INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>'>
		<INPUT TYPE='hidden' ID='<%= g_sSubKey %>' NAME='<%= g_sSubKey %>'>
	</FORM>

	<FORM ID='delform' ACTION='' METHOD='POST' LANGUAGE='VBSCRIPT' ONTASK='OnDelete("<% = thisPage %>")'>
		<INPUT TYPE='hidden' ID='type' NAME='type'>
		<INPUT TYPE='hidden' ID='selection' NAME='selection'>
		<INPUT TYPE='hidden' ID='selectall' NAME='selectall'>
	</FORM> 


	<!-- order list meta and data -->

	<xml id='lsOrdersData'>
	<%	
		if not IsEmpty(Session("dOrders")) then
			dim g_rsQuery, g_sQuery,nRecordCount,nPage
			dim xmlOrders, xmlList, node
			
			if nPage = "" then nPage = 1
			set g_rsQuery = rsGetRecords(dOrders,nPage,PAGE_SIZE,nRecordCount)
			g_nItems = nRecordCount			
			set xmlOrders = Server.Createobject("Microsoft.XMLDOM")
			xmlOrders.loadXML xmlGetXMLFromRSEx(g_rsQuery, -1, -1, nRecordCount, null).xml
			set xmlList = xmlOrders.selectNodes("//order_status_code")
			
			for each node in xmlList
				if node.text <> "" then
					node.text = g_MSCSAppConfig.DecodeStatusCode(node.text)
				end if
			next
			Response.write xmlOrders.xml
			
			set g_rsQuery = Nothing
			set xmlOrders = Nothing
			set xmlList = Nothing
			
		else
			Response.Write("<document><record/></document>")
		end if
		
	%>

	</xml>

	<xml id='lsOrdersMeta'>
		<listsheet>
			<global curpage='<%= nPage %>' pagesize='<%= PAGE_SIZE %>' 
				recordcount='<%= g_nItems %>' selection='multi'/>
			<columns>
				<column id='ordergroup_id' hide='yes'></column>
			    <column id='order_number' width='15'><%= L_OrderNumber_Text %></column>
			    <column id='user_last_name'  width='15'><%= L_LastName_Text %></column>
			    <column id='user_first_name'  width='15'><%= L_FirstName_Text %></column>
			    <column id='order_create_date' width='15'><%= L_OrderDate_Text %></column>
			    <column id='order_status_code' width='10'><%= L_OrderStatus_Text %></column>
			    <column id='saved_cy_total_total'  width='15'><%= L_TotalAmount_Text %></column>
			    <column id='user_org_name'  width='15'><%= L_Organization_Text %></column>
			 </columns>
			 <operations>
				<newpage formid='neweventform'/>
				<sort    formid='neweventform'/>
				<findby  formid='searchform'/>
			</operations>
		 </listsheet>
	</xml>

<div ID="filtertext" class="filtertext" style="MARGIN-TOP: 20px"><%= L_BDSSFilter_Text %></div>
		
	<DIV ID='lsOrders' 
		 CLASS='listSheet' STYLE="MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px; HEIGHT: 80%"
		 DataXML='lsOrdersData'
		 MetaXML='lsOrdersMeta'
		 LANGUAGE='VBScript'
		 OnRowSelect='OnSelectRow()'
		 OnRowUnselect='OnUnSelectRow()'
		 OnAllRowsUnselect='OnNoSelectedRows()'
		 OnAllRowsSelect='OnAllSelectedRows()'><%= L_LoadingList_Text %></DIV>

</DIV>
</BODY>
</HTML>	
<SCRIPT LANGUAGE='VBSCRIPT'>
	<%
		dim dBDError
		if not isEmpty(Session("MSCSBDError")) then
			set g_slBDError = Session("MSCSBDError")
			for each dBDError in g_slBDError
	%>
	showErrorDlg "<%= dBDError("errortitle") %>", "<%= dBDError("friendlyerror") %>", "<%= dBDError("errordetails") %>", <%= dBDError("errortype") %>
	<%		next
			Session("MSCSBDError") = empty
		end if
	%>
</SCRIPT>
<% Cleanup %>
