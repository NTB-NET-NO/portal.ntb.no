<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="include/requisition_util.asp" -->
<!--#INCLUDE FILE="include/inc_common.asp"-->
<!--#INCLUDE FILE="include/ordersStrings.asp"-->


<%	Dim thisPage ,g_sModuleName
	thisPage = "basket_list.asp"
	g_sModuleName = L_Basket_StaticText
	
	dim g_sKey,g_sStatusText,g_sSubKey,dBasket, g_slBDError
	
	'Initializes the OrderGroup Object
	IntializeOrderGroupObject
	
	'Implementation for saving states
	set dBasket = Server.CreateObject("Commerce.Dictionary")
	if not IsEmpty(Session("dBasket")) then
		set dBasket = Session("dBasket")
	end if
	
	dBasket.StatusFilter = 1			
	g_sKey = "user_first_name"
	g_sSubKey = "ordergroup_id"
 
	'Deleting Baskets
	if Request.Form("type") = "del" then
		Dim items,count,i,nResult
		if Request.Form("selectall") then
			nResult = deleteAllRecords(dBasket,thisPage)
			if nResult = 0 then
				g_sStatusText = L_AllDeleted_StatusBar 
			else
				g_sStatusText = L_ErrorDelete_StatusBar
			end if 
		else
			items = Split(Request.Form("selection"),",")
			count = ubound(items) + 1
			nResult = deleteRecord(dBasket, items)
			if nResult = 0 then
				g_sStatusText = sFormatString(L_Delete_StatusBar,Array(count))
			else
				g_sStatusText = L_ErrorDelete_StatusBar
			end if
		end if
	end if
 
 
%>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
<STYLE>
	.hide {
	   DISPLAY: none;
	}
	DIV.esContainer, .esEditgroup DIV.esContainer
	{
	    BACKGROUND-COLOR: transparent;
	}
</STYLE>
<!--#INCLUDE FILE='include/orders_util.vbs' -->

<SCRIPT LANGUAGE='VBScript'>
	sub window_OnLoad()
		setSelect()
		showfindBy()
	end sub
	
	sub changeSelect()
		setSelect()
		ShowFindBy()
	end sub
	'Called when the find by selection changes
	'Decides what are the fields to be displayed
	sub setSelect()
		document.all.btnfindby.disabled = true
		daterow.className = "hide"
		torow.className = "hide"
        orgrow.style.display = "none"
		select case selfindby.value
			case "daterange"
				daterow.className = ""
				changeDate()
			case "organization"
				orgrow.style.display = "block"
			case "all"
				document.all.btnfindby.disabled = false
		end select
		
	end sub
	
	'Called when the date criteria selection is changed
	sub changeDate()
		select case fbdate.value
			case "from"
				start_date.style.display = "block"
				torow.className = ""
			case "onbefore"
				start_date.style.display = "block"
				torow.className = "hide"
		end select
	end sub
	
	'Called when the Find Now button is clicked
	sub reloadListSheet()
		onNoSelectedRows
		lsBaskets.page = 1
		lsBaskets.reload("findby")
	end sub
</SCRIPT>
</HEAD>
<BODY SCROLL='NO'>
<%
	InsertTaskBar g_sModuleName, g_sStatusText
	'Checking to see if there is a date in the session, if not setting the findby date
	'for the currenct date
	if isNull(dBasket.start_date)or isNull(dBasket.end_date) then
	   	dBasket.start_date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)
		dBasket.end_date = g_MSCSDataFunctions.date(Now(), g_MSCSDefaultLocale)
	end if
%>

<!---Start Find By Area  ---->
<xml id="fbmeta">
<editsheet>
    <global title='no'/>
	<fields>
		<select id='selfindby' onchange='changeSelect'>
			<prompt><%= L_SelectFindBy_Text %></prompt>
			<select id='selfindby'>
				<option value='daterange'><%= L_DateLastAccessed_ComboBox %></option>
				<option value='organization'><%= L_Organization_ComboBox %></option>
				<option value='all'><%= L_AllBaskets_ComboBox %></option>
			</select>
		</select>
		<select id='fbdate' onchange='changeDate'>
			<prompt><%= L_SelectDateCriteria_Text %></prompt>
			<select id='fbdate'>
				<option value='from'><%= L_From_ComboBox %></option>
				<option value='onbefore'><%= L_OnOrBefore_ComboBox %></option>
			</select>
		</select>
	    <date id='start_date' firstday='<%= g_sMSCSWeekStartDay %>'>
	        <name><%= L_StartDate_Text %></name>
	        <format><%= g_sMSCSDateFormat %></format>
	    </date>
	    <date id='end_date' firstday='<%= g_sMSCSWeekStartDay %>'>
	        <name><%= L_EndDate_Text %></name>
	        <format><%= g_sMSCSDateFormat %></format>
	    </date>
	    <text id='fborganization' subtype='short' maxlen='64'>
			<prompt><%= L_TypeYourEntry_Text %></prompt>
		</text>
	</fields>
    <template fields='selfindby fbdate start_date end_date fborganization'><![CDATA[
		<TABLE WIDTH='100%' STYLE='TABLE-LAYOUT: fixed'>
			<COLGROUP><COL WIDTH='100'><COL WIDTH='120'><COL WIDTH='200'><COL></COLGROUP>
			<TR>
				<TD>
					<SPAN ID='findbylabel'><%= L_FindBy_Text %></SPAN>
				</TD>
				<TD COLSPAN=2>
					<DIV ID='selfindby'><%= L_Loading_Text %></DIV>
				</TD>
				<TD>
					<BUTTON CLASS='bdbutton' LANGUAGE='VBScript' ONCLICK='reloadListSheet' 
							ID='btnfindby' NAME='btnfindby' DISABLED><%= L_FindNow_Button %></BUTTON>
				</TD>
			</TR>		
			<TR ID='daterow' CLASS='hide'>
				<TD>
					<SPAN ID='datelabel'><%= L_Date_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='fbdate' LANGUAGE='VBScript' 
						ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
				<TD>
					<DIV ID='start_date' LANGUAGE='VBScript' 
						STYLE='DISPLAY: none'
						ONCLICK='EnableFind("<%= thisPage %>")'
						ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>		
			<TR ID='torow' CLASS='hide'>
				<TD></TD>
				<TD>
					<SPAN ID='tolabel'><%= L_To_Text %></SPAN>
				</TD>
				<TD>
					<DIV ID='end_date' LANGUAGE='VBScript' 
						ONCLICK='EnableFind("<%= thisPage %>")'
						 ONCHANGE='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
			<TR ID='orgrow' STYLE='DISPLAY: none'>
				<TD>
					<SPAN ID='orglabel'><%= L_Organization_Text %></SPAN>
				</TD>
				<TD COLSPAN=2>
					<DIV ID='fborganization' LANGUAGE='VBScript' 
						ONCLICK='EnableFind("<%= thisPage %>")' 
						ONKEYDOWN='EnableFind("<%= thisPage %>")'><%= L_Loading_Text %></DIV>
				</TD>
			</TR>
		</TABLE>		
    ]]></template>
</editsheet>
</xml>

<xml id='fbData'>
	<document>
		<record>
			<selfindby><%= dBasket.selfindby %></selfindby>
			<fbdate><%= dBasket.fbdate %></fbdate>
			<start_date><%= dBasket.start_date %></start_date>
			<end_date><%= dBasket.end_date %></end_date>
			<fborganization><%= dBasket.fborganization %></fborganization>
		</record>
	</document>
</xml>
<FORM ID='searchform' ACTION='orders_event.asp'>
	<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE='baskets'>
	<DIV ID='bdfindbycontent' CLASS='findByContent'>
		<div ID="esFind" class="editSheet"
			METAXML='fbMeta' DATAXML='fbData'></div>
	</DIV>
</FORM>
<!-- End FindBy Area ----->

<DIV ID='bdcontentarea'>

	<FORM ID='neweventform' NAME='neweventform' ACTION='orders_event.asp'>
		<INPUT TYPE='hidden' ID='pagetype' NAME='pagetype' VALUE='baskets'>
	</FORM>

	<FORM ID='selectform' ACTION='' METHOD='POST'>
		<INPUT TYPE='hidden' ID='<%= g_sKey %>' NAME='<%= g_sKey %>'>
		<INPUT TYPE='hidden' ID='<%= g_sSubKey %>' NAME='<%= g_sSubKey %>'>
	</FORM>


	<FORM ID='delform' ACTION='' METHOD='POST' LANGUAGE='VBSCRIPT' ONTASK='OnDelete("<%= thisPage %>")'>
		<INPUT TYPE='hidden' ID='type' NAME='type'>
		<INPUT TYPE='hidden' ID='selection' NAME='selection'>
		<INPUT TYPE='hidden' ID='selectall' NAME='selectall'>
	</FORM>


	<!-- basket listsheet data and meta -->

	<xml id = 'lsBasketsData'>
		<?xml version="1.0" ?>
		<document>
			<record/>
		</document>
	</xml>
	<xml id='lsBasketsMasterData'>
	<%	
		if not IsEmpty(Session("dBasket")) then
			dim g_rsQuery,nPage,g_sQuery,nRecordCount
			if nPage = "" then nPage = 1
			set g_rsQuery = rsGetRecords(dBasket,nPage,PAGE_SIZE,nRecordCount)
			g_nItems = nRecordCount 
			%><%= xmlGetXMLFromRSEx(g_rsQuery, -1, -1, nRecordCount, null).xml %><%
			set g_rsQuery = Nothing
			
		else
			Response.Write("<document><record/></document>")
		end if
	%>
	</xml>

	<xml id='lsBasketsDetailData'>
	<%	
		if not IsEmpty(Session("dBasket")) then
			On Error Resume Next
			set g_rsQuery = rsGetAllLineItems()
			%><%= xmlGetXMLFromRSEx(g_rsQuery, -1, -1, -1, null).xml %><%
		else
			Response.Write("<document><record/></document>")
		end if
		
		On Error GoTo 0
	%>
	</xml>

	<xml id='lsbasketsMeta'>
		<listsheet>
			<global curpage='<%= nPage %>' pagesize='<%= PAGE_SIZE %>' recordcount='<%= g_nItems %>' 
				selection='multi' grouped='no'/>
			<columns>
				<column id='ordergroup_id' hide='yes'></column>
			    <column id='user_last_name' width='15'><%= L_LastName_Text %></column>
			    <column id='user_first_name' width='15'><%= L_FirstName_Text %></column>
			    <column id='d_datelastchanged' width='15'><%= L_DateLastAccessed_Text %></column>
			    <column id='total_lineitems' width='15' ><%= L_TotalLineItems_Text %></column>
			    <column id='user_org_name' width='15' ><%= L_Organization_Text %></column>
			</columns>
			<operations>
				<newpage formid='neweventform'/>
				<sort formid='neweventform'/>
				<findby formid='searchform'/>
			</operations>
		 </listsheet>
	</xml>
	
	<SCRIPT LANGUAGE='VBSCRIPT'>
		'Merge data for grouped list
		dim xmlMerged
		set xmlMerged = xmlMergeGroupsByID2("lsBasketsMasterData", "lsBasketsDetailData", "ordergroup_id")
		lsBasketsData.xmlDocument.loadXML(xmlMerged.xml)
	</SCRIPT>

<div ID="filtertext" class="filtertext" style="MARGIN-TOP: 20px"><%= L_BDSSFilter_Text %></div>
	
	<DIV ID='lsBaskets' 
		 CLASS='listSheet' STYLE="MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px; HEIGHT: 80%"
		 DataXML='lsBasketsData'
		 MetaXML='lsBasketsMeta'
		 LANGUAGE='VBScript'
		 OnRowSelect='OnSelectRow()'
		 OnRowUnselect='OnUnSelectRow()'
		 OnAllRowsUnselect='OnNoSelectedRows()'
		 OnAllRowsSelect='OnAllSelectedRows()'><%= L_LoadingList_Text %></DIV>
		
</DIV>
</BODY>
</HTML>	
<SCRIPT LANGUAGE='VBSCRIPT'>
	<%
		dim dBDError
		if not isEmpty(Session("MSCSBDError")) then
			set g_slBDError = Session("MSCSBDError")
			for each dBDError in g_slBDError
	%>
	showErrorDlg "<%= dBDError("errortitle") %>", "<%= dBDError("friendlyerror") %>", "<%= dBDError("errordetails") %>", <%= dBDError("errortype") %>
	<%		next
			Session("MSCSBDError") = empty
		end if
	%>
</SCRIPT>
<% Cleanup %>
