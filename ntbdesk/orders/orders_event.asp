<!--#INCLUDE FILE='../include/BDXMLHeader.asp' -->
<!--#INCLUDE FILE='../include/HTTPXMLUtil.asp' -->
<!--#INCLUDE FILE='../include/BizDeskPaths.asp' -->
<!--#INCLUDE FILE="../include/DBUtil.asp" -->
<!--#INCLUDE FILE="../include/ASPUtil.asp" -->
<!--#INCLUDE FILE="include/requisition_util.asp" -->
<!--#INCLUDE FILE="include/inc_common.asp"-->
	
<%	
	dim nPage,nRecordCount,column,direction,pageType
	dim g_rsQuery, g_sQuery
	dim item
	dim xmlMerged,masterXML,detailXML
	dim g_dRequest
	dim g_MSCSDefaultLocale
	set g_dRequest = dGetRequestXMLAsDict()
	
	'Initialize the OrderGroup Object
	IntializeOrderGroupObject
	
	nPage = g_dRequest("page")
	column = g_dRequest("column")
	direction = g_dRequest("direction")
	pageType = g_dRequest("pagetype")
	
		
	'generation of xml for various operations and pagetypes
	select case pageType
		case "orders"	
			On Error Resume Next
			dim dOrders
			dim objAppConfig
			dim xmlOrders, xmlList, node
			
			'implementation to store the search criteria in the session
			set dOrders = Server.CreateObject("Commerce.Dictionary")
			dOrders.StatusFilter = 0
			if g_dRequest("op") = "findby" then 
				for each item in g_dRequest
					dOrders.value(item) = g_dRequest(item)
				next
				if dOrders.column = "" then dOrders.column = "ordergroup_id" 
				if dOrders.direction = "" then dOrders.direction = "asc" 
				set Session("dOrders") = dOrders
			elseif not IsEmpty(Session("dOrders")) then
				set dOrders = Session("dOrders")
			end if
			'end search criteria
			'sort criteria
			if column <> "" and direction <> "" then
				dOrders.column = column
				dOrders.direction = direction
				set Session("dOrders") = dOrders
			end if
			
			if nPage = "" then nPage = 1		
			
			set g_rsQuery = rsGetRecords(dOrders,nPage,PAGE_SIZE,nRecordCount)
			g_nItems = nRecordCount
			set xmlOrders = Server.Createobject("Microsoft.XMLDOM")
			xmlOrders.loadXML xmlGetXMLFromRSEx(g_rsQuery, -1, -1, nRecordCount, null).xml
			set xmlList = xmlOrders.selectNodes("//order_status_code")
			for each node in xmlList
				if node.text <> "" then
					node.text = g_MSCSAppConfig.DecodeStatusCode(node.text)
				end if
			next
			
		 	if Err.number <> 0 then
		 		Response.Write getErrorXML(Err.number, Err.source, Err.description)
			else
				Response.write xmlOrders.xml
			end if
			
			set g_rsQuery = Nothing
			set objAppConfig = Nothing
			set xmlOrders = Nothing
			set xmlList = Nothing
			On Error GoTo 0			
		case "baskets"
			On Error Resume Next
			dim dBasket
			'implementation to store the search criteria in the session
			set dBasket = Server.CreateObject("Commerce.Dictionary")
			dBasket.StatusFilter = 1
			if g_dRequest("op") = "findby" then 
				for each item in g_dRequest
					dBasket.value(item) = g_dRequest(item)
				next
				if dBasket.column = "" then dBasket.column = "ordergroup_id" 
				if dBasket.direction = "" then dBasket.direction = "asc" 
				set Session("dBasket") = dBasket
			elseif not IsEmpty(Session("dBasket")) then
				set dBasket = Session("dBasket")
			end if
			'end search criteria
			'sort criteria
			if column <> "" and direction <> "" then
				dBasket.column = column
				dBasket.direction = direction
				set Session("dBasket") = dBasket
			end if
			
			if nPage = "" then nPage = 1
			set g_rsQuery = rsGetRecords(dBasket,nPage,PAGE_SIZE,nRecordCount)
			g_nItems = nRecordCount
			set masterXML = xmlGetXMLFromRSEx(g_rsQuery, -1, -1, g_nItems, null)
			set g_rsQuery = rsGetAllLineItems()
			set detailXML = xmlGetXMLFromRSEx(g_rsQuery, -1, -1, -1, null)
			'Merge the master and detail data for the grouped list
			xmlMerged = sMergeGroups2(masterXML,detailXML,"ordergroup_id")
			if Err.number <> 0 then
		 		Response.Write getErrorXML(Err.number, Err.source, Err.description)
			else
				Response.Write xmlMerged
			end if
			set g_rsQuery = Nothing
			On Error Goto 0
		case else
			Response.Write "<document/>"
	end select
	
	function getErrorXML(hRes, strSrc, strDesc)
		hRes = hRes And 65535
		getErrorXML = "<ERROR ID='" & hRes & "' SOURCE="""" & strSrc & """">" & _
		    "<![CDATA[" & strDesc & "]]></ERROR>"
	end function
	
	call Cleanup ()
%>