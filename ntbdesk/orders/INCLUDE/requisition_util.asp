<%	
	dim objOrderGroupManager,oSearchInfo,oResultInfo
	
	' #####################################################################
	' function: IntializeOrderGroupObject
	'	Creates and Initializes the OrderGroup Object.
	'	Creates the Simple Find Structs 
	'
	' Input:
	'			
	' Returns:
	'	
	
	sub IntializeOrderGroupObject()
		On Error Resume Next
		dim g_MSCSTransactionConnStr
		g_MSCSTransactionConnStr= GetSiteConfigField("transactions", "connstr_db_Transactions")   
		if Err.Number <> 0 then 
			Call setError(L_InitializeOrderGroup_DialogTitle, L_SiteConfig_ErrorMessage, _
						sGetScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
		end if
		set objOrderGroupManager = Server.CreateObject("Commerce.OrderGroupManager")
	    set oSearchInfo = Server.CreateObject("Commerce.SimpleFindSearchInfo")
		set oResultInfo = Server.CreateObject("Commerce.SimpleFindResultInfo")
		if Err.Number <> 0 then 
			Call setError(L_InitializeOrderGroup_DialogTitle, sGetErrorById("Cant_create_object"), _
						sGetScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
		end if
		
	    objOrderGroupManager.Initialize(g_MSCSTransactionConnStr)
		if Err.Number <> 0 then 
			Call setError(L_InitializeOrderGroup_DialogTitle, sGetErrorById("Cant_connect_db"), _
						sGetScriptError(Err), ERROR_ICON_CRITICAL)
			Err.Clear
		end if
		On Error GoTo 0
	end sub
		
	' #####################################################################
	' function: rsGetDetails
	'	Gets the Order Details
	'
	' Input:
	'	g_sSubKeyValue - The OrderGroup_Id of the order whose details have 
	'	to be retrieved
	'		
	' Returns:
	'	The Order Details in a recordset
	'		
	'
	   
	function rsGetDetails(byVal g_sSubKeyValue)
		On Error Resume Next
		oSearchInfo.ordergroup_id = g_sSubKeyValue
		oResultInfo.JoinOrderFormInfo = true
		oResultInfo.JoinLineItemInfo = false
		set rsGetDetails = objOrderGroupManager.SimpleFind(oSearchInfo,oResultInfo)
		if Err.Number <> 0 then 
			Call setError(L_GetDetails_DialogTitle, sGetErrorById("Bad_XML_data"), _
						sGetScriptError(Err), ERROR_ICON_CRITICAL)
		end if
		On Error GoTo 0
	end function

		
	' #####################################################################
	' function: rsGetLineItems
	'	Gets the Order Details
	'
	' Input:
	'	g_sSubKeyValue - The OrderGroup_Id of the order whose details have 
	'	to be retrieved
	'		
	' Returns:
	'	The LineItem Details in a recordset
	'		
	'
	function rsGetLineItems(ByVal g_sSubKeyValue)
		On Error Resume Next
		oSearchInfo.ordergroup_id = g_sSubKeyValue
		oResultInfo.JoinOrderFormInfo=true
		oResultInfo.JoinLineItemInfo=true
		set rsGetLineItems = objOrderGroupManager.SimpleFind(oSearchInfo,oResultInfo)
		if Err.Number <> 0 then 
			Call setError(L_GetLineItems_DialogTitle, sGetErrorById("Bad_XML_data"), _
						sGetScriptError(Err), ERROR_ICON_CRITICAL)
		end if
		On Error GoTo 0
	end function
	
		
	' #####################################################################
	' function: rsGetAllLineItems
	'	Gets the Order Details
	'
	' Input:
	'			
	' Returns:
	'	The LineItem Details in a recordset
	'		
	'
	   
	function rsGetAllLineItems()
		On Error Resume Next
		oSearchInfo.ordergroup_id = ""
		oSearchInfo.order_number = ""
		oSearchInfo.user_firstname = ""
		oSearchInfo.user_lastname = ""
		oSearchInfo.user_org_name = ""
		oSearchInfo.product_id = ""
		oSearchInfo.description=""
		oSearchInfo.po_number=""
		oSearchInfo.SearchDateTimeColumn=""
		oSearchInfo.SearchDateTimeStart=""
		oSearchInfo.SearchDateTimeEnd=""
		oResultInfo.JoinOrderFormInfo=false
		oResultInfo.JoinLineItemInfo=false
		oSearchInfo.StatusFilter = "1"
				   
		set rsGetAllLineItems =  objOrderGroupManager.SimpleFind(oSearchInfo,oResultInfo)
		if Err.Number <> 0 then 
			Call setError(L_GetLineItems_DialogTitle, sGetErrorById("Bad_XML_data"), _
						sGetScriptError(Err), ERROR_ICON_CRITICAL)
		end if
		On Error GoTo 0
	end function
	
	
		
	' #####################################################################
	' function: SetSearchCriteria
	'	Set the search criteria for OrderGroupSearchInfo
	'
	' Input:
	'			
	' Returns:
	'	
	'		
	'		   
	Sub SetSearchCriteria(byval dOrders,byval nPage,byval pagesize)

		oSearchInfo.ordergroup_id = ""
		oSearchInfo.order_number = ""
		oSearchInfo.user_firstname = ""
		oSearchInfo.user_lastname = ""
		oSearchInfo.user_org_name = ""
		oSearchInfo.product_id = ""
		oSearchInfo.description=""
		oSearchInfo.po_number=""
		oSearchInfo.SearchDateTimeColumn=""
		oSearchInfo.SearchDateTimeStart=""
		oSearchInfo.SearchDateTimeEnd=""
		if dOrders.StatusFilter = 1 Then           'basket
			oSearchInfo.StatusFilter = 1
			oResultInfo.JoinOrderFormInfo=false
			oResultInfo.JoinLineItemInfo=false
		else  
			if dOrders.StatusFilter = 0 Then					   'order
				oSearchInfo.StatusFilter= 4 or 8 or 16 or 32 or 64
				oResultInfo.JoinOrderFormInfo=false
				oResultInfo.JoinLineItemInfo=false
			End If
		End If
		oResultInfo.PageNumber = nPage
		oResultInfo.PageSize = pagesize
		if (dOrders.column<>"") then
			oResultInfo.OrderGroupSortColumn = dOrders.column
			oResultInfo.SortDirection = dOrders.direction
		end if
		   
		select case dOrders.selfindby
			case "daterange"
				select case dOrders.fbdate
					case "from"
						oSearchInfo.SearchDateTimeColumn = "order_create_date"
						oSearchInfo.SearchDateTimeStart = dOrders.start_date
						oSearchInfo.SearchDateTimeEnd = DateAdd("d",1,dOrders.end_date)
						oSearchInfo.SearchDateTimeEnd = g_MSCSDataFunctions.Date(oSearchInfo.SearchDateTimeEnd, _
																				 g_MSCSDefaultLocale)
					case "onbefore"
						oSearchInfo.SearchDateTimeColumn = "order_create_date"
						oSearchInfo.SearchDateTimeStart = ""
						oSearchInfo.SearchDateTimeEnd = DateAdd("d",1,dOrders.start_date)
						oSearchInfo.SearchDateTimeEnd = g_MSCSDataFunctions.Date(oSearchInfo.SearchDateTimeEnd, _
																				 g_MSCSDefaultLocale)
				end select
			case "username"
				oSearchInfo.user_firstname = dOrders.fbfirstname
				oSearchInfo.user_lastname = dOrders.fblastname
			case "ordernumber"
				oSearchInfo.order_number = dOrders.fbordernumber
		    case "organization"
				oSearchInfo.user_org_name = dOrders.fborganization
			case "ponumber"
				oSearchInfo.po_number = dOrders.fbponumber
			case "itemsku"
			    oSearchInfo.product_id = dOrders.fbitemsku
			case "itemdesc"
				oSearchInfo.description = dOrders.fbitemdesc
			case "advance"								 
				select case dOrders.fbdate
					case "from"
						oSearchInfo.SearchDateTimeColumn = "order_create_date"
						oSearchInfo.SearchDateTimeStart = dOrders.start_date
						oSearchInfo.SearchDateTimeEnd = DateAdd("d",1,dOrders.end_date)
						oSearchInfo.SearchDateTimeEnd = g_MSCSDataFunctions.Date(oSearchInfo.SearchDateTimeEnd, _
																				 g_MSCSDefaultLocale)
					case "onbefore"
						oSearchInfo.SearchDateTimeColumn = "order_create_date"
						oSearchInfo.SearchDateTimeStart = ""
						oSearchInfo.SearchDateTimeEnd = DateAdd("d",1,dOrders.start_date)
						oSearchInfo.SearchDateTimeEnd = g_MSCSDataFunctions.Date(oSearchInfo.SearchDateTimeEnd, _
																				 g_MSCSDefaultLocale)
				end select
				oSearchInfo.user_firstname = dOrders.fbfirstname
				oSearchInfo.user_lastname = dOrders.fblastname
				oSearchInfo.order_number = dOrders.fbordernumber
				oSearchInfo.user_org_name = dOrders.fborganization
				oSearchInfo.po_number = dOrders.fbponumber
				oSearchInfo.product_id = dOrders.fbitemsku
				oSearchInfo.description = dOrders.fbitemdesc
		end select
	end Sub

	' #####################################################################
	' function: rsGetRecords
	'	Gets the list of Orders matching the search criteria
	'
	' Input:
	'	dOrders - The session variable which has the search criteria for the
	'			  orders module
	'	nPage   -   The Currenct page number.
	'   pagesize -  The currenct PageSize.
	'	nRecordCount - The no. of records that are got by any query	
	' Returns:
	'	The List of Orders in a recordset
	'		
	'

	function rsGetRecords(byval dOrders,byval nPage,byval pagesize,byRef nRecordCount)
	
		On Error Resume Next
		call SetSearchCriteria(dOrders,nPage,pagesize)
	
		Dim out_num_records
	
		set rsGetRecords = objOrderGroupManager.SimpleFind(oSearchInfo,oResultInfo,out_num_records)
		nRecordCount = out_num_records
		If Err.Number <> 0 then
			if dOrders.StatusFilter = 1 Then
				Call setError(L_GetBaskets_DialogTitle, sGetErrorById("Bad_XML_data"), _
						   sGetScriptError(Err), ERROR_ICON_CRITICAL)
			else 
				if dOrders.StatusFilter = 0 Then
					Call setError(L_GetOrders_DialogTitle, sGetErrorById("Bad_XML_data"), _
						   sGetScriptError(Err), ERROR_ICON_CRITICAL)
				End If
			End if
		End if
		On Error GoTo 0
	
	end function	   
	   
	function rsDeleteRecords(byval dOrders,byval nPage,byval pagesize)
	
		On Error Resume Next

		call SetSearchCriteria(dOrders, nPage, pagesize)	
		Dim out_num_records

		call objOrderGroupManager.SimpleDelete(oSearchInfo,oResultInfo)
		nRecordCount = out_num_records
		If Err.Number <> 0 then
			Call setError(L_GetOrders_DialogTitle, sGetErrorById("Bad_XML_data"), _
						   sGetScriptError(Err), ERROR_ICON_CRITICAL)
		End if
		On Error GoTo 0
	rsDeleteRecords = Err.Number

	end function
	   	
		   
	' #####################################################################
	' function: deleteRecord
	'	Deletes a record depending on the OrderID
	'
	' Input:
	'	items - array which consists of the orderid's of the records that
	'			need to be deleted
	' Returns:
	'	
	  
	function deleteRecord(ByVal dStatus, Byval items)
		On Error Resume Next
		Dim count,sValue,i
		count = ubound(items)
		for i = 0 to count
			sValue = items(i)
			If (dStatus.StatusFilter = 1) Then
				objOrderGroupManager.DeleteOrderGroupFromDisk(sValue)
			Else
				objOrderGroupManager.DeleteReceiptGroupFromDisk(sValue)
			End If
		next
		If Err.Number <> 0 then
			Call setError(L_DeleteError_DialogTitle, L_Delete_ErrorMessage, _
						   sGetScriptError(Err), ERROR_ICON_CRITICAL)
		End if
		deleteRecord = Err.Number
		On Error GoTo 0									 
	end function
	
	   
	' #####################################################################
	' function: deleteAllRecords
	'	Deletes all the records depending on the search criteria
	'
	' Input:
	'	dSearch - dictionary which consists of the current search criteria
	'   thisPage - the page from which the delete was called
	' Returns:
	'
	
	   
	function deleteAllRecords(byVal dSearch,byVal thisPage)
		On Error Resume Next
		Dim page,pagesize
		page = 1
		pagesize = -1

		call rsDeleteRecords(dSearch, page, pagesize)
		If Err.Number <> 0 then
			Call setError(L_DeleteError_DialogTitle, L_Delete_ErrorMessage, _
						   sGetScriptError(Err), ERROR_ICON_CRITICAL)
		End if
		deleteAllRecords = Err.Number
		On Error GoTo 0
	end function
	
	  
	' #####################################################################
	' function: CleanUp
	'	Cleans up the objects
	'
	' Input:
	'
	' Returns:
	'
	sub CleanUp()
		set objOrderGroupManager = Nothing
		set oSearchInfo = Nothing
		set oResultInfo = Nothing
	end sub
%>