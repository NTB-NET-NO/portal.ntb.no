<%

'LOC: This file	contains all localizable strings for the ANLY pages	
'	  All strings should conform to	the	following format:
'		const L_NAME_TYPE =	""
'	  NAMEs	should be in mixed case, and as	descriptive	as possible
' TYPEs:
'	Accelerator		AddressHTML		AlternativeText		ArgumentName
'	Button			Checkbox		ComboBox			Command
'	Comment			CustomControl	DialogTitle			DriveControl
'	EditBox			ErrorMessage	FileName			FolderControl
'	FolderName		FontName		FunctionName		General
'	GroupBox		GroupBoxTitle	HTMLText			HTMLTitle
'	KeyName			Keyword			ListBox				MacroAction
'	MenuItem		Message			Number				OptionButton
'	PageInformation	PageTitle		ProgressBar			Property
'	RadioButton		ScrollBar		SliderBar			StaticText
'	StatusBar		Style			TabControl			Text
'	ToolTip			TopicTitle		TranslationRecord	Unknown
'	VBScriptText	Version

'ListSheet Constants
Const L_Loading_Text = "Loading..."
Const L_LoadingList_Text = "Loading list, please wait..."

'Page Title
Const L_OrderStatus_StaticText = "Order Status"
Const L_Basket_StaticText = "Basket Manager"

'Status text
Const L_AllDeleted_StatusBar = "All items deleted"
Const L_ErrorDelete_StatusBar = "Error deleting record"
Const L_Delete_StatusBar = " %1  item(s) deleted"
Const L_ItemsSelected_StatusBar = " %1  item(s) selected" 
Const L_OneItemSelected_StatusBar = "1 item selected"
Const L_AllSelected_StatusBar = "All items selected"

'Find by Constants
Const L_SelectFindBy_Text = "Select One"
Const L_SelectDateCriteria_Text = "Select a date criteria"
Const L_TypeYourEntry_Text = "Type your entry"
Const L_OrderDate_ComboBox = "Order date"
Const L_UserName_ComboBox = "User name"
Const L_OrderNumber_ComboBox = "Order number"
Const L_Organization_ComboBox = "Organization"
Const L_PONumber_ComboBox = "PO number"
Const L_ItemPartNumber_ComboBox = "Product ID"
Const L_ItemDescription_ComboBox = "Item description"
Const L_AnyInformation_ComboBox = "Any information"
Const L_From_ComboBox = "From"
Const L_OnOrBefore_ComboBox = "On or Before"
Const L_DateLastAccessed_ComboBox = "Date last accessed"
Const L_AllOrders_ComboBox = "All orders"
Const L_AllBaskets_ComboBox = "All baskets"
Const L_StartDate_Text = "Start date"
Const L_EndDate_Text = "End date"
Const L_FindBy_Text = "Find By:"
Const L_FindNow_Button = "Find Now"
Const L_Date_Text = "Date:"
Const L_To_Text = "To:"

'Message Box Constants
Const L_Delete_Text = "Delete?"	
Const L_AllDelete_Message = "Delete all items?"
Const L_DeleteOne_Message = "Delete 1 item?"	
Const L_DeleteMany_Message = "Delete %1 items?"	

'Constants for ListSheet and EditSheet
Const L_FirstName_Text = "First Name"
Const L_LastName_Text = "Last Name"
Const L_OrderNumber_Text = "Order Number"
Const L_Organization_Text = "Organization"
Const L_PONumber_Text = "PO Number"
Const L_ItemPartNumber_Text = "Product ID"
Const L_ProductVariantId_Text = "Variant ID"
Const L_Catalog_Text = "Catalog"
Const L_Description_Text = "Description"
Const L_OrderDate_Text = "Order Date"
Const L_OrderStatus_Text = "Order Status"
Const L_TotalAmount_Text = "Total Amount"
Const L_DateLastAccessed_Text = "Date Last Accessed"
Const L_TotalLineItems_Text = "Total LineItems"
Const L_Quantity_Text = "Quantity"
Const L_UnitPrice_Text = "Unit Price"
Const L_Summary_Text = "Order Summary"
Const L_OrderSummary_Accelerator = "s"
Const L_OrderDetail_Text = "Order Detail"
Const L_OrderDetail_Accelerator = "d"
Const L_Address_Text = "Addresses"
Const L_Address_Accelerator = "a"
Const L_ShippingMethodName_Text = "Ship Method"
Const L_PaymentMethodLabel_Text = "Payment Method:"
Const L_PaymentMethod_ToolTip = "Payment Method"
Const L_CreditCardPayment_Text = "Credit Card"
Const L_POPayment_Text = "Purchase Order"
Const L_LineItemTotal_Text = "Item Total"
Const L_ProductName_Text = "Name"
Const L_ProductDescription_Text = "Description"
Const L_Messages_Text = "Messages"
Const L_ShipAddress_Text = "Ship Address"

'Edit Sheet Constants
Const L_LineItems_Text = "Line Items"
Const L_View_StatusBar = "Order: %1 "
Const L_OrderNumber_ToolTip = "Order number"
Const L_FirstName_ToolTip = "First name"
Const L_LastName_ToolTip = "Last name"
Const L_OrderDate_ToolTip = "Order date"
Const L_OrderStatus_ToolTip = "Order status"
Const L_DateLastAccessed_ToolTip = "Date last accessed"
Const L_PONumber_ToolTip = "PO number"
Const L_Subtotal_ToolTip = "Subtotal"
Const L_ShippingCharge_ToolTip = "Shipping charge"
Const L_Tax_ToolTip = "Tax"
Const L_TotalAmount_ToolTip = "Total amount"
Const L_AddressLine1_ToolTip = "Address line 1"
Const L_AddressLine2_ToolTip = "Address line 2"
Const L_City_ToolTip = "City"
Const L_Region_ToolTip = "Region"
Const L_PostalCode_Tooltip = "Postal code"
Const L_CountryCode_ToolTip = "Country/Region code"
Const L_BillingAddress_ToolTip = "Billing address name"
Const L_LoadingProperties_Text = "Loading properties, please wait..."
Const L_AddressName_ToolTip = "Address name"
Const L_UnknownAddressName_Text = "Unknown/Invalid"
Const L_ShippingDiscount_ToolTip = "Shipping discount description"

'Edit sheet labels
Const L_OrderNumberLabel_Text = "Order Number:"
Const L_FirstNameLabel_Text = "First Name:"
Const L_LastNameLabel_Text = "Last Name:"
Const L_OrderDateLabel_Text = "Order Date:"
Const L_OrderStatusLabel_Text = "Order Status:"
Const L_PONumberLabel_Text = "PO Number:"
Const L_SubtotalLabel_Text = "Subtotal:"
Const L_ShippingChargeLabel_Text = "Shipping Charge:"
Const L_TaxLabel_Text = "Tax:"
Const L_TotalAmountLabel_Text = "Total Amount:"
Const L_AddressLine1Label_Text = "Address Line 1:"
Const L_AddressLine2Label_Text = "Address Line 2:"
Const L_CityLabel_Text = "City:"
Const L_RegionLabel_Text = "Region:"
Const L_PostalCodeLabel_Text = "Postal Code:"
Const L_CountryCodeLabel_Text = "Country/Region Code:"
Const L_BillingAddressLabel_Text = "Billing Address Name:"
Const L_AddressNameLabel_Text = "Address Name:"
Const L_ShippingDiscountLabel_Text = "Shipping Discount:"

'Error Strings
Const L_SiteConfig_ErrorMessage = "An error occurred while retrieving the configuration information. Please contact your system administrator."
Const L_InitializeOrderGroup_DialogTitle = "Order Group Initialization"
Const L_GetDetails_DialogTitle = "Get Details"
Const L_GetLineItems_DialogTitle = "Get LineItems"
Const L_GetOrders_DialogTitle = "Get Orders"
Const L_DeleteError_DialogTitle = "Error in Deleting"
Const L_Delete_ErrorMessage = "The record(s) could not be deleted."
%>