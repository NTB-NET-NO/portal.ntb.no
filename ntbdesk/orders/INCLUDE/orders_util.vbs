<SCRIPT LANGUAGE='VBScript'>
	Option Explicit
	
	Const PAGE_SIZE = 2
	Public g_nItemsSelected		
	Public g_ASelectedItems ()		
	Public g_sSelectedItems
	ReDim  g_ASelectedItems(0)
	
	g_nItemsSelected = 0
	

	
	'-------Add selected Item to g_ASelectedItems
	sub AddItem ()
		dim iIndex, oSelection, sKey
		
		ReDim Preserve g_ASelectedItems(g_nItemsSelected)
		set oSelection = window.event.XMLrecord
		sKey = oSelection.selectSingleNode("./<%= g_sSubKey %>").text
		g_ASelectedItems(UBound(g_ASelectedItems)) = sKey
		
	end sub

	'--- Remove DeSelected Item From g_ASelectedItems
	'
	sub RemoveItem ()
		dim iIndex, oSelection, sKey

	    set oSelection = window.event.XMLrecord
	    sKey = oSelection.selectSingleNode("./<%= g_sSubKey %>").text
	    
	    for iIndex = 0 to (UBound(g_ASelectedItems))
		    if (g_ASelectedItems(iIndex) = sKey) then
			    g_ASelectedItems(iIndex) = empty
	            exit for
	        end if
	    next
	end sub
    
    '-------removes all items from g_ASelectedItems
    sub RemoveAllItems ()
		dim iIndex
		for iIndex = 0 to UBound(g_ASelectedItems)
			g_ASelectedItems(iIndex) = empty
		next
	end sub
	
	'---------DeSelectAll------------
    sub OnNoSelectedRows()
	 	Call RemoveAllItems
	 	g_nItemsSelected = 0
	 	
	    ' disable d(elete) button on the task bar
		DisableTask "delete"
		' clear the value for the item key in the selection form
        delform.elements.selectall.value = false
       
		'Specific case for orderstatus module,disable the open task button         
        if "<%= thisPage%>" = "orderstatus_list.asp" then
			DisableTask "open"
			selectform.elements.<%= g_sKey %>.value = ""
			selectform.elements.<%= g_sSubKey %>.value = ""				
		end if
		
		 ' display the status text
        setStatusText sFormatString("<%= L_ItemsSelected_StatusBar%>",Array(g_nItemsSelected)) 
    end sub
    
    '----------SelectAll--------------------------
    sub OnAllSelectedRows()
		EnableTask "delete"
		delform.elements.selectall.value = true
		'disable the open task button in the case of the orderstatus module
		if "<%= thisPage%>" = "orderstatus_list.asp" then DisableTask "open"
		setStatusText "<%= L_AllSelected_StatusBar%>"
	end sub	
	
	
	'----------OnSelect Row----------------------
    sub OnSelectRow()
    		
		dim oSelection
		dim sKeyValue,sSubKeyValue
		
		'Add the selected Item to the array
		Call AddItem()
		g_nItemsSelected = g_nItemsSelected + 1
		g_sSelectedItems = Join(g_ASelectedItems,",")
        
        delform.elements.selection.value = g_sSelectedItems
        delform.elements.selectall.value = false
        
			
		' get XML node for the selected row
		set oSelection = window.event.XMLrecord
        	
		' get the key value
		sKeyValue = oSelection.selectSingleNode("./<%= g_skey%>").text
        sSubKeyValue = oSelection.selectSingleNode("./<%= g_sSubkey%>").text
	    
        
        EnableTask "delete"
        
        'Special Case for the orderstatus module, enable the open task button
        if "<%= thisPage%>" = "orderstatus_list.asp" then
			if g_nItemsSelected = 1 then
				EnableTask "open"
				selectform.elements.<%= g_sKey %>.value = sKeyValue
				selectform.elements.<%= g_sSubKey %>.value = sSubKeyValue
			else
				DisableTask "open"
			end if
		end if
		
		' display the status text
		if g_nItemsSelected = 1 then
			setStatusText sFormatString("<%= L_OneItemSelected_StatusBar%>",Array(sKeyValue))
		else
			setStatusText sFormatString("<%= L_ItemsSelected_StatusBar%>",Array(g_nItemsSelected)) 
		end if
		
	end sub
	
	'--------------------OnUnselect Row--------------------------
	   
    sub OnUnSelectRow()
		dim oSelection
		'remove the deselected item from the array
		Call RemoveItem()
		
		if g_nItemsSelected > 0 then g_nItemsSelected = g_nItemsSelected - 1
		g_sSelectedItems = Join(g_ASelectedItems)
        delform.elements.selection.value = g_sSelectedItems
		delform.elements.selectall.value = false
       
        if g_nItemsSelected = 0 then 
			DisableTask "delete"
			DisableTask "open"
		end if        
        'Special case for the orderstatus module,to enable the open task button
        if "<%= thisPage%>" = "orderstatus_list.asp" then
			if g_nItemsSelected = 1 then EnableTask "open"
		end if
        setStatusText sFormatString("<%= L_ItemsSelected_StatusBar%>",Array(g_nItemsSelected))
    end sub
	
	   
	'---------Enabling and disbling of Find button------------
	
	sub EnableFind(byVal thisPage)
		'If the field is a data field comparing the start date and end date, 
		'to disable the FindNow button if the end date is lesser than the start date
		
		Dim sValue,dDate,sDateValue
		select case thispage
			case "orderstatus_list.asp"
				sValue = selfindby.value
				select case sValue
					case "daterange"
						sDateValue = fbdate.value
						dDate = DateDiff("d", dtGetDate(start_date.value), dtGetDate(end_date.value))
						if sDateValue = "onbefore" then dDate = 1
						if dDate < 0 then
							document.all.btnfindby.disabled = true
						else
							document.all.btnfindby.disabled = false
						end if
					case "advance"
						sDateValue = fbdate.value
						dDate = DateDiff("d", dtGetDate(start_date.value), dtGetDate(end_date.value))
						if sDateValue = "onbefore" then dDate = 1
						if dDate < 0 then
							document.all.btnfindby.disabled = true
						else
							document.all.btnfindby.disabled = false
						end if
					case else
						document.all.btnfindby.disabled = false
				end select
			case "basket_list.asp"
				sValue = selfindby.value
				select case sValue
					case "daterange"
						sDateValue = fbdate.value
						dDate = DateDiff("d", dtGetDate(start_date.value), dtGetDate(end_date.value))
						if sDateValue = "onbefore" then dDate = 1
						if dDate < 0 then
							document.all.btnfindby.disabled = true
						else
							document.all.btnfindby.disabled = false
						end if
					case else
						document.all.btnfindby.disabled = false
				end select
		end select
			
    end sub	
    
    '-------Nag message for more than 1 deletes-----------
    sub OnDelete(byVal sPageName)
   
		Dim sResponse
        Dim sText
        
		if g_nItemsSelected = 1 then
			sText = "<%= L_DeleteOne_Message %>" 
			sResponse = MsgBox(sText,vbYesNo,"<%= L_Delete_Text %>")
		elseif g_nItemsSelected > 1 then
			sText = sFormatString("<%= L_DeleteMany_Message %>",Array(g_nItemsSelected)) 
			sResponse = MsgBox(sText,vbYesNo,"<%= L_Delete_Text %>")
		elseif g_nItemsSelected = 0 then
			sText = "<%= L_AllDelete_Message %>"
			sResponse = MsgBox(sText,vbYesNo,"<%= L_Delete_Text %>")
		else
			sResponse = vbYes
		end if
		
		if sResponse = vbYes then
			delform.elements.type.value = "del"
			delform.submit()
		else
			EnableTask "find"
			EnableTask "delete"
		end if
    end sub
    
</SCRIPT>
