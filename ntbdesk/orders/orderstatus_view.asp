<!--#INCLUDE FILE="../include/BDHeader.asp" -->
<!--#INCLUDE FILE="../include/BizDeskUtil.asp" -->
<!--#INCLUDE FILE="./include/ordersStrings.asp"-->
<!--#INCLUDE FILE="./include/inc_common.asp" -->
<!--#INCLUDE FILE="./include/requisition_util.asp" -->

<% 
	Dim g_oOrderGroup, g_sOrderGroupId, g_nOrderNumber
	' Variables to hold unique field names for address blocks
	Dim g_sAddressName, g_sLastName, g_sFirstName, g_sAddress1, g_sAddress2, g_sCity, g_sRegion, g_sPostalCode, g_sCountryCode
	Dim g_xmlOrderSummaryData, g_xmlOrderDetailData, g_xmlOrderAddressMeta, g_xmlOrderAddressData
	Dim g_iCount
	
	Call GetOrderFormData()

	Sub GetOrderFormData()
		Dim sMSCSTransactionConnStr, oRandomOrderForm
		
		' Get connection string
		sMSCSTransactionConnStr = GetSiteConfigField("transactions", "connstr_db_Transactions")   
		If Err <> 0 Then 
			Call setErr(L_InitializeOrderGroup_DialogTitle, L_SiteConfig_ErrorMessage)
		End If
		
		' Create order group, intialize and load ordergroup_id passed from list form
		g_sOrderGroupId = Request.form("ordergroup_id")
		g_nOrderNumber = Request.Form("order_number")		
		Set g_oOrderGroup = CreateObject("Commerce.OrderGroup")
		With g_oOrderGroup
			.Initialize sMSCSTransactionConnStr, g_sOrderGroupId
			.LoadOrder g_sOrderGroupId
		End With
		
		Call AddDiscountMessages()
				
		'  This code gets some fields which are assumed the same on each orderform (using a RandomOrderform)
		'  If this assumption does not hold true, you'll need to iterate the orderforms
		Set oRandomOrderform = GetAnyOrderForm()
		g_xmlOrderSummaryData = GetXMLOrderSummaryData(oRandomOrderForm)
		g_xmlOrderDetailData = GetXMLOrderDetailData
		Call BuildOrderAddressXMLMetaAndData		
	End Sub
		
	Function GetXMLOrderSummaryData(RandomOrderForm)
		Dim sBuffer
		sBuffer = "<document>"
		sBuffer = sBuffer & "<record>"
		sBuffer = SBuffer & "<order_number>" & g_nOrderNumber & "</order_number>"
		sBuffer = sBuffer & "<user_last_name>" & g_oOrderGroup.Value("user_last_name") & "</user_last_name>"
		sBuffer = sBuffer & "<user_first_name>" & g_oOrderGroup.Value("user_first_name") & "</user_first_name>"
		sBuffer = sBuffer & "<address_name>" & GetAddressName(RandomOrderform.billing_address_id) & "</address_name>"
		sBuffer = sBuffer & "<order_create_date>" & g_oOrderGroup.Value("order_create_date") & "</order_create_date>"
		sBuffer = sBuffer & "<order_status_code>" & g_MSCSAppConfig.DecodeStatusCode(g_oOrderGroup.Value("order_status_code")) & "</order_status_code>"
		sBuffer = sBuffer & "<subtotal>" & g_oOrderGroup.Value("saved_cy_oadjust_subtotal") & "</subtotal>"
		sBuffer = sBuffer & "<shipping>" & g_oOrderGroup.Value("saved_cy_shipping_total") & "</shipping>"
		If IsNull(RandomOrderform.Value("saved_shipping_discount_description")) Then
		    sBuffer = sBuffer & "<discount/>"
		Else
		    sBuffer = sBuffer & "<discount>" & RandomOrderform.Value("saved_shipping_discount_description") & "</discount>"
		End If
		sBuffer = sBuffer & "<tax>" & g_oOrderGroup.Value("saved_cy_tax_total") & "</tax>"
		sBuffer = sBuffer & "<saved_cy_total_total>" & g_oOrderGroup.Value("saved_cy_total_total") & "</saved_cy_total_total>"
		sBuffer = sBuffer & "<payment_method>" & GetPaymentMethodString(RandomOrderform.payment_method) & "</payment_method>"
		sBuffer = sBuffer & "</record>"
		sBuffer = sBuffer & "</document>"
		GetXMLOrderSummaryData = sBuffer
	End Function

	Function GetXMLOrderDetailData()
		Dim sBuffer, sOrderFormName, Item
		sBuffer = "<document>"
		For Each sOrderFormName In g_oOrderGroup.Value.ORDERFORMS
		    For Each Item In g_oOrderGroup.Value.ORDERFORMS(sOrderFormName).Items
		        sBuffer = sBuffer & "<record>"
		        sBuffer = sBuffer & "<ordergroup_id>" & g_sOrderGroupId & "</ordergroup_id>"
		        sBuffer = sBuffer & "<product_id>" & setText(Item.Value("product_id")) & "</product_id>"
		        sBuffer = sBuffer & "<product_variant_id>" & setText(Item.Value("product_variant_id")) & "</product_variant_id>"
		        sBuffer = sBuffer & "<name>" & setText(Item.Value("saved_product_name")) & "</name>"
		        sBuffer = sBuffer & "<description>" & setText(Item.Value("description")) & "</description>"
		        sBuffer = sBuffer & "<product_catalog>" & Item.Value("product_catalog") & "</product_catalog>"
		        sBuffer = sBuffer & "<u_address_name>" & setText(GetAddressName(Item.Value("shipping_address_id"))) & "</u_address_name>"
		        sBuffer = sBuffer & "<shipping_method_name>" & setText(Item.Value("shipping_method_name")) & "</shipping_method_name>"
		        sBuffer = sBuffer & "<quantity>" & Item.Value("quantity") & "</quantity>"
		        sBuffer = sBuffer & "<cy_unit_price>" & Item.Value("cy_unit_price") & "</cy_unit_price>"
		        sBuffer = sBuffer & "<cy_lineitem_total>" & Item.Value("cy_lineitem_total") & "</cy_lineitem_total>"
		        sBuffer = sBuffer & "<messages>" & setText(Item.Value("_messages")) & "</messages>"
		        sBuffer = sBuffer & "</record>"
		    Next
		Next
		sBuffer = sBuffer & "</document>"
		GetXMLOrderDetailData = sBuffer
	End Function
	
	Function setText(sValue)
		If IsNull(sValue) Then 
			sValue = ""
		Else			
			setText = replace(replace(replace(replace(sValue, "&", "&amp;"), "<", "&lt;"), ">", "&gt;"), vbCrLf, "&#13;")
		End If
	End Function
	
	Sub BuildOrderAddressXMLMetaAndData()
		Dim addr_id, sMetaBuffer, sDataBuffer, dAddressData
		
		sDataBuffer = "<document>"
		sDataBuffer = sDataBuffer & "<record>"
				
		For Each addr_id In g_oOrderGroup.Value("Addresses")
			Call BuildUniqueFieldNames(addr_id)	' creates unique names for this address and puts into global vars
			' Create meta for this address
			sMetaBuffer = sMetaBuffer & "<editsheet>"
			sMetaBuffer = sMetaBuffer & "<global expanded='yes' title='yes'>"
			sMetaBuffer = sMetaBuffer & "<name>" & setText(GetAddressName(addr_id)) & "</name>"
			sMetaBuffer = sMetaBuffer & "<key></key>"
			sMetaBuffer = sMetaBuffer & "</global>"
			sMetaBuffer = sMetaBuffer & "<fields>"
			' Build the edit fields
			sMetaBuffer = sMetaBuffer & BuildXMLOrderAddressMeta()
			sMetaBuffer = sMetaBuffer & "</fields>"
			sMetaBuffer = sMetaBuffer & "</editsheet>"
			' Create data for this address
			Set dAddressData = g_oOrderGroup.Value("Addresses").Value(addr_id)
			sDataBuffer = sDataBuffer & BuildXMLOrderAddressData(dAddressData)
		Next
		
		sDataBuffer = sDataBuffer & "</record>"
		sDataBuffer = sDataBuffer & "</document>"		
		' Put the results into global vars for xml islands
		g_xmlOrderAddressMeta = sMetaBuffer
		g_xmlOrderAddressData = sDataBuffer
    End Sub

    Function BuildXMLOrderAddressMeta()
	' Create meta xml for edit fields.
		Dim sBuffer
		If g_MSCSDataFunctions.locale = "1044" Then
			sBuffer = sBuffer & CreateXMLString(g_sAddressName, L_AddressNameLabel_Text, L_AddressName_ToolTip)
			sBuffer = sBuffer & CreateXMLSTring(g_sCountryCode, L_CountryCodeLabel_Text, L_CountryCode_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sPostalCode, L_PostalCodeLabel_Text, L_PostalCode_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sRegion, L_RegionLabel_Text, L_Region_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sCity, L_CityLabel_Text, L_City_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sAddress1, L_AddressLine1Label_Text, L_AddressLine1_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sAddress2, L_AddressLine2Label_Text, L_AddressLine2_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sLastName, L_LastNameLabel_Text, L_LastName_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sFirstName, L_FirstNameLabel_Text, L_FirstName_ToolTip)
		Else	
			sBuffer = sBuffer & CreateXMLString(g_sAddressName, L_AddressNameLabel_Text, L_AddressName_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sLastName, L_LastNameLabel_Text, L_LastName_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sFirstName, L_FirstNameLabel_Text, L_FirstName_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sAddress1, L_AddressLine1Label_Text, L_AddressLine1_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sAddress2, L_AddressLine2Label_Text, L_AddressLine2_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sCity, L_CityLabel_Text, L_City_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sRegion, L_RegionLabel_Text, L_Region_ToolTip)
			sBuffer = sBuffer & CreateXMLString(g_sPostalCode, L_PostalCodeLabel_Text, L_PostalCode_ToolTip)
			sBuffer = sBuffer & CreateXMLSTring(g_sCountryCode, L_CountryCodeLabel_Text, L_CountryCode_ToolTip)
		End If
		BuildXMLOrderAddressMeta = sBuffer		
	End Function

    Function BuildXMLOrderAddressData(dAddress)
	' Single xml address record
		Dim sBuffer
		If g_MSCSDataFunctions.locale = "1044" Then
			sBuffer = sBuffer & "<" & g_sAddressName & "><![CDATA[" & dAddress.address_name & "]]></" & g_sAddressName & ">"
			sBuffer = sBuffer & "<" & g_sCountryCode & "><![CDATA[" & dAddress.country_code & "]]></" & g_sCountryCode & ">"
			sBuffer = sBuffer & "<" & g_sPostalCode & "><![CDATA[" & dAddress.postal_code & "]]></" & g_sPostalCode & ">"
			sBuffer = sBuffer & "<" & g_sRegion & "><![CDATA[" & dAddress.region_code & "]]></" & g_sRegion & ">"
			sBuffer = sBuffer & "<" & g_sCity & "><![CDATA[" & dAddress.city & "]]></" & g_sCity & ">"
			sBuffer = sBuffer & "<" & g_sAddress1 & "><![CDATA[" & dAddress.address_line1 & "]]></" & g_sAddress1 & ">"
			sBuffer = sBuffer & "<" & g_sAddress2 & "><![CDATA[" & dAddress.address_line2 & "]]></" & g_sAddress2 & ">"
			sBuffer = sBuffer & "<" & g_sLastName & "><![CDATA[" & dAddress.last_name & "]]></" & g_sLastName & ">"
			sBuffer = sBuffer & "<" & g_sFirstName & "><![CDATA[" & dAddress.first_name & "]]></" & g_sFirstName & ">"
		Else
			sBuffer = sBuffer & "<" & g_sAddressName & "><![CDATA[" & dAddress.address_name & "]]></" & g_sAddressName & ">"
			sBuffer = sBuffer & "<" & g_sLastName & "><![CDATA[" & dAddress.last_name & "]]></" & g_sLastName & ">"
			sBuffer = sBuffer & "<" & g_sFirstName & "><![CDATA[" & dAddress.first_name & "]]></" & g_sFirstName & ">"
			sBuffer = sBuffer & "<" & g_sAddress1 & "><![CDATA[" & dAddress.address_line1 & "]]></" & g_sAddress1 & ">"
			sBuffer = sBuffer & "<" & g_sAddress2 & "><![CDATA[" & dAddress.address_line2 & "]]></" & g_sAddress2 & ">"
			sBuffer = sBuffer & "<" & g_sCity & "><![CDATA[" & dAddress.city & "]]></" & g_sCity & ">"
			sBuffer = sBuffer & "<" & g_sRegion & "><![CDATA[" & dAddress.region_code & "]]></" & g_sRegion & ">"
			sBuffer = sBuffer & "<" & g_sPostalCode & "><![CDATA[" & dAddress.postal_code & "]]></" & g_sPostalCode & ">"
			sBuffer = sBuffer & "<" & g_sCountryCode & "><![CDATA[" & dAddress.country_code & "]]></" & g_sCountryCode & ">"
		End If
		BuildXMLOrderAddressData = sBuffer
	End Function
	    	
	Function CreateXMLString(sField, sLabel, sTooltip)
		Dim sBuffer
		sBuffer = "<text id='" & sField & "'"  & " readonly='yes'>"
		sBuffer = sBuffer & "<name>" & sLabel & "</name>"
		sBuffer = sBuffer & "<tooltip>" & sTooltip & "</tooltip>"
		sBuffer = sBuffer & "</text>"
		CreateXMLString = sBuffer
	End Function		
	
	Sub BuildUniqueFieldNames(addr_id)
	' Fills global vars with unique field names based on address id.
		Dim sAddressName
		g_iCount = g_iCount + 1	'Increment the count to create unique field name
		sAddressName = "Addr" & g_iCount
		g_sAddressName = sAddressName & "_" & "address_name" 
		g_sLastname = sAddressName & "_" & "last_name"
		g_sFirstname = sAddressName & "_" & "first_name"
		g_sAddress1 = sAddressName & "_" & "address1"
		g_sAddress2 = sAddressName & "_" & "address2"
		g_sCity = sAddressName & "_" & "city"
		g_sRegion = sAddressName & "_" & "region"
		g_sPostalCode = sAddressName & "_" & "postal_code"
		g_sCountryCode = sAddressName & "_" & "country_code"
	End Sub
	    
   	Function GetAddressName(addr_id)
   		If IsNull(addr_id) Then
   			GetAddressName = ""
   		Else
			Dim dictAddress
			Set dictAddress = g_oOrderGroup.GetAddress(addr_id)
			If Not dictAddress Is Nothing Then
				GetAddressName = dictAddress.Value("address_name")
			Else
				GetAddressName = L_UnknownAddressName_Text
			End If
		End If
	End Function

	'<from std_lib.asp>
	Function GetAnyOrderForm()
	    Dim sOrderFormName    
	    For Each sOrderFormName In g_oOrderGroup.Value.ORDERFORMS
	        Set GetAnyOrderForm = g_oOrderGroup.Value.ORDERFORMS.Value(sOrderFormName)
	        Exit For
	    Next
	End Function
			
	Function GetPaymentMethodString(sMethodID)
		If sMethodID = "credit_card" Then
			GetPaymentMethodString = L_CreditCardPayment_Text
		ElseIf sMethodID = "purchase_order" Then
			GetPaymentMethodString = L_POPayment_Text
		Else
			GetPaymentMethodString = sMethodID
		End If
	End Function

	'<from basket.asp>
	' This function adds a footnote symbol for each discount applied to each
	'   line item.  The footnote symbol will be "D" + the campaign id
	Sub AddDiscountMessages()
	    Dim oItem, oOrderForm, sOrderFormName, ciid
	    For Each sOrderFormName In g_oOrderGroup.Value.ORDERFORMS
	        Set oOrderForm = g_oOrderGroup.Value.ORDERFORMS.Value(sOrderFormName)
	        For Each oItem In oOrderForm.Items
	            If IsObject(oItem.discounts_applied) Then
	                For Each ciid In oItem.discounts_applied
	                    oItem.Value("_messages") = oItem.Value("_messages") & "D" & CStr(ciid) & " "
	                Next
	            Else
	                oItem.Value("_messages") = " "
	            End If
	        Next
	    Next
	End Sub
%>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../include/bizdesk.css' ID='mainstylesheet'>
</HEAD>
<BODY SCROLL='NO'>

<% InsertEditTaskBar sFormatString(L_View_StatusBar, Array(g_nOrderNumber)),"" %>

	<xml id='esOrderSummaryMeta'>
		<editsheet>
		    <global expanded='yes' title='no'/>
		    <fields>
		        <numeric id='order_number' readonly='yes'>
		            <name><%= L_OrderNumberLabel_Text%></name>
		            <tooltip><%= L_OrderNumber_ToolTip%></tooltip>
		        </numeric>
		        <text id='user_last_name' readonly='yes'>
		            <name><%= L_LastNameLabel_Text%></name>
		            <tooltip><%= L_LastName_ToolTip%></tooltip>
		        </text>
		        <text id='user_first_name' readonly='yes'>
		            <name><%= L_FirstNameLabel_Text%></name>
		            <tooltip><%= L_FirstName_ToolTip%></tooltip>
		        </text>	
		        <text id='address_name' readonly='yes'>
		            <name><%= L_BillingAddressLabel_Text%></name>
		            <tooltip><%= L_BillingAddress_ToolTip%></tooltip>
		        </text>
		        <date id='order_create_date' readonly='yes' firstday='<%= g_sMSCSWeekStartDay %>'>
		            <name><%= L_OrderDateLabel_Text%></name>
		            <tooltip><%= L_OrderDate_ToolTip%></tooltip>
		            <format><%= g_sMSCSDateFormat%></format>
		        </date>
		        <text id='order_status_code' readonly='yes'>
		            <name><%= L_OrderStatusLabel_Text%></name>
		            <tooltip><%= L_OrderStatus_ToolTip%></tooltip>
		        </text>		        
		        <numeric id='subtotal' readonly='yes' subtype='currency'>
		            <name><%= L_SubtotalLabel_Text%></name>
		            <tooltip><%= L_Subtotal_ToolTip%></tooltip>
		            <format><%= g_sMSCSCurrencyFormat%></format>
		        </numeric>
		        <numeric id='shipping' readonly='yes' subtype='currency'>
		            <name><%= L_ShippingChargeLabel_Text%></name>
		            <tooltip><%= L_ShippingCharge_ToolTip%></tooltip>
		            <format><%= g_sMSCSCurrencyFormat%></format>
		        </numeric>
		        <text id='discount' readonly='yes'>
		            <name><%= L_ShippingDiscountLabel_Text%></name>
		            <tooltip><%= L_ShippingDiscount_ToolTip%></tooltip>
		        </text>
		        <numeric id='tax' readonly='yes' subtype='currency'>
		            <name><%= L_TaxLabel_Text%></name>
		            <tooltip><%= L_Tax_ToolTip%></tooltip>
		            <format><%= g_sMSCSCurrencyFormat%></format>
		        </numeric>
		        <numeric id='saved_cy_total_total' readonly='yes' subtype='currency'>
		            <name><%= L_TotalAmountLabel_Text%></name>
		            <tooltip><%= L_TotalAmount_ToolTip%></tooltip>
		            <format><%= g_sMSCSCurrencyFormat%></format>
		        </numeric>
		        <text id='payment_method' readonly='yes'>
		            <name><%= L_PaymentMethodLabel_Text%></name>
		            <tooltip><%= L_PaymentMethod_ToolTip%></tooltip>
		        </text>
		    </fields>
		</editsheet>
	</xml>	
	<xml id='esOrderSummaryData'>
		<%= g_xmlOrderSummaryData %>
	</xml>


	<!-- Get the LineItem Details for the order -->
	<xml id = 'esMasterMeta'>
		<editsheets>
			<editsheet>
			    <global expanded='yes'>
					<name><%= L_Summary_Text%></name>
					<key><%= L_OrderSummary_Accelerator%></key>
			    </global>
				<template register='esOrderSummary'>
					<![CDATA[
					<DIV ID='esOrderSummary' 
						 CLASS='editSheet' 
						 MetaXML='esOrderSummaryMeta'
						 DataXML='esOrderSummaryData'><%= L_LoadingProperties_Text%>
						 </DIV>
					]]>
				</template>
			</editsheet>
			<editsheet>
			    <global expanded='no'>
					<name><%= L_OrderDetail_Text %></name>
					<key><%= L_OrderDetail_Accelerator %></key>
				</global>
				<template register='lsOrderDetail'>
					<![CDATA[
					<DIV ID='lsOrderDetail' 
						 CLASS='listSheet' style="margin:10px"
						 MetaXML='lsOrderDetailMeta' 
						 DataXML='lsOrderDetailData'><%= L_LoadingProperties_Text%>
						 </DIV>
					]]>
				</template>
			</editsheet>
			<editsheet>
			    <global expanded='no'>
					<name><%= L_Address_Text%></name>
					<key><%= L_Address_Accelerator%></key>
			    </global>
				<template register='esOrderAddress'>
					<![CDATA[
						<DIV ID='esOrderAddress' 
						CLASS='editSheet'
						MetaXML='esOrderAddressMeta'
						DataXML='esOrderAddressData'><%= L_LoadingProperties_Text%>
						</DIV>
					]]>
				</template>
			</editsheet>
		</editsheets>
	</xml>		
	<xml id='lsOrderDetailMeta'>
		<listsheet>
			<global pagecontrols='no' sort='no'></global>
			<columns>
				<column id='ordergroup_id' key='yes' hide='yes'></column>
			    <column id='product_id' width='25' sortdir='asc'><%= L_ItemPartNumber_Text%></column>
			    <column id='product_variant_id' width='15'><%= L_ProductVariantId_Text%></column>
			    <column id='name' width='15'><%= L_ProductName_Text%></column>
			    <column id='description' width='15'><%= L_ProductDescription_Text%></column>
			    <column id='product_catalog' width='15'><%= L_Catalog_Text%></column>
			    <column id='u_address_name' width='25'><%= L_ShipAddress_Text%></column>
			    <column id='shipping_method_name' width='25'><%= L_ShippingMethodName_Text%></column>
			    <column id='quantity'  width='25' ><%= L_Quantity_Text%></column>
			    <column id='cy_unit_price' width='25' format='float'><%= L_UnitPrice_Text%></column>
				<column id='cy_lineitem_total' width='25' format='float'><%= L_LineItemTotal_Text%></column>
			    <column id='messages' width='25'><%= L_Messages_Text%></column>
			</columns>
		 </listsheet>
	</xml>	
	<xml id='lsOrderDetailData'>
		<%= g_xmlOrderDetailData %>
	</xml>
	<xml id='esMasterData'>
		<document/>
	</xml>


	<!-- Get addresses for the order -->
	<xml id='esOrderAddressMeta'>
		<editsheets>
			<%= g_xmlOrderAddressMeta %>
		</editsheets>
	</xml>	 
 	<xml id='esOrderAddressData'>
		<%= g_xmlOrderAddressData %>
	</xml>
   		
	<!-- putting edit sheets within this container allows for scroling without 
		losing the task buttons off the top of the screen -->
	<DIV ID='editSheetContainer' CLASS='editPageContainer'>
		<FORM ID='ORDERFORM' ACTION METHOD='POST'>
			<DIV ID='esMaster' 
				 CLASS='editSheet' 
				 MetaXML='esMasterMeta'
				 DataXML='esMasterData'><%= L_LoadingProperties_Text%></DIV>
		</FORM>
	</DIV>
</BODY>
</HTML>